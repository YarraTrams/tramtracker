//
//  FavouriteStop.h
//  tramTRACKER
//
//  Created by Robert Amos on 21/07/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Stop;
@class Filter;

/**
 * Describes a Favourite Stop.
**/
@interface FavouriteStop : NSObject {

	/**
	 * The actual Stop being favourited
	**/
	Stop *stop;

	/**
	 * Any saved filter applied to this stop.
	**/
	Filter *filter;

	/**
	 * This favourite stop's position in the favourite tree
	**/
	NSIndexPath *indexPath;
}

@property (nonatomic, readonly) Stop *stop;
@property (nonatomic, readonly) Filter *filter;
@property (nonatomic, readonly) NSIndexPath *indexPath;

/**
 * Initialise with a specific stop filter and index path
 *
 * @param	aStop				A Stop Object
 * @param	aFilter				A Filter object
 * @param	anIndexPath			A NSIndexPath object describing this favourite's position
**/
- (id)initWithStop:(Stop *)aStop filter:(Filter *)aFilter indexPath:(NSIndexPath *)anIndexPath;

/**
 * Compares the index position of this favourite stop with the specified favourite stop.
 *
 * @param	otherStop			Another Favourite Stop
 * @return						A NSComparisonResult
**/
- (NSComparisonResult)compareWithFavouriteStop:(FavouriteStop *)otherStop;

@end
