//
//  SearchListController.m
//  tramTRACKER
//
//  Created by Robert Amos on 6/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "SearchListController.h"


@implementation SearchListController

@synthesize stopList;


- (void)viewDidLoad {
    [super viewDidLoad];

	// we need to override the navbar ITem with a search control
	UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
	[searchBar setDelegate:self];

	// set the bar style to match the navigation bar
	[searchBar setBarStyle:UIBarStyleBlackOpaque];
	[searchBar setTintColor:[UIColor colorWithRed:TTNavigationBarTintColorRed green:TTNavigationBarTintColorGreen blue:TTNavigationBarTintColorBlue alpha:0.8]];
	
	// set the placeholder text and a cancel button
	[searchBar setPlaceholder:NSLocalizedString(@"search-placeholder", "Stop Name or Suburb")];
	[searchBar setShowsCancelButton:YES];
	
	// add that to the view
	[self.navigationItem setTitleView:searchBar];
	[self.navigationItem.titleView setFrame:CGRectMake(0, 0, 320, 44)];
	[searchBar release];
	
	// change the background colour of the table
	[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
	
	// and if we put a blank footer in there it won't draw all the empty cells
	[self.tableView setTableFooterView:[self tableFooterView]];
}

//
// Create the table footer
//
- (UIView *)tableFooterView
{
	CGRect frame = CGRectMake(0, 0, self.tableView.frame.size.width, 10);
	UnderBackgroundView *ub = [[UnderBackgroundView alloc] initWithFrame:frame];
	return [ub autorelease];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [stopList count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"SearchResultCell";
    
    SearchResultCell *cell = (SearchResultCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        cell = [[[SearchResultCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    }
    
    // Set up the cell...
	Stop *stop = [stopList objectAtIndex:[indexPath indexAtPosition:1]];
	[cell.name setText:[stop formattedName]];
	[cell.routeDescription setText:[stop formattedRouteDescription]];
	
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	// Get the stop
	Stop *stop = [stopList objectAtIndex:[indexPath indexAtPosition:1]];
	
	// Start the pid view
	PIDViewController *PID = nil;
    if ([[UIScreen mainScreen] bounds].size.height == 568) {
        PID = [[PIDViewController alloc] initWithNibName:@"PIDView-iPhone5" bundle:[NSBundle mainBundle]];
    }
    else{
        PID = [[PIDViewController alloc] initWithNibName:@"PIDView" bundle:[NSBundle mainBundle]];
    }
	
	// get the stop info
	[PID startWithStop:stop];
	
	// push it onto the stack
	[self.navigationController pushViewController:PID animated:YES];
	[PID release];
}



#pragma mark Search Bar methods

//
// When they've started searching
//
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
	// if we have less than three characters don't bother
	if ([searchText length] < 3)
		[self setStopList:[NSArray array]];
	
	else
		// find matching stops
		[self setStopList:[[StopList sharedManager] stopsBySearchingNameOrSuburb:searchText]];

	// reload the table data
	[self.tableView reloadData];
}

//
// When we've hit the cancel button
//
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
	// clear the search item
	[searchBar setText:nil];

	// dismiss the keyboard
	[searchBar resignFirstResponder];
}

- (void)dealloc {
	[stopList release];
    [super dealloc];
}


@end

