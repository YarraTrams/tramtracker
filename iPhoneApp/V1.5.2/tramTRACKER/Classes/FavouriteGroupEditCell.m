//
//  FavouriteGroupEditCell.m
//  tramTRACKER
//
//  Created by Robert Amos on 21/07/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "FavouriteGroupEditCell.h"


@implementation FavouriteGroupEditCell

@synthesize textField;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {

		// create a UILabel that takes up the whole room
		textField = [[UITextField alloc] initWithFrame:CGRectMake(10, 10, 280, 30)];
		[textField setDelegate:self];
		[textField setPlaceholder:NSLocalizedString(@"favourites-groups-edit-placeholder", nil)];
		[self.contentView addSubview:textField];
	}
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)dealloc {
	[textField release];
    [super dealloc];
}


@end
