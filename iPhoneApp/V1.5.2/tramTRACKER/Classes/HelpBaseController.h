//
//  HelpBaseController.h
//  tramTRACKER
//
//  Created by Robert Amos on 4/05/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UnderBackgroundView.h"


@interface HelpBaseController : UITableViewController {
}

- (UIView *)tableFooterView;

@end
