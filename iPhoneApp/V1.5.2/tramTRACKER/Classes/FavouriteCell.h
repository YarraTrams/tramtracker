//
//  FavouriteCell.h
//  tramTRACKER
//
//  Created by Robert Amos on 6/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GradientBackgroundCell.h"

/**
 * @ingroup CustomTableCells
**/

/**
 * A custom UITableViewCell that displays a favourite stop
**/
@interface FavouriteCell : UITableViewCell {
	/**
	 * The label for the stops name.
	**/
	UILabel *name;
	
	/**
	 * The label for the description of stops running through that route
	**/
	UILabel *routeDescription;
	
	BOOL lowFloorOnly;
	UIImageView *lowFloorOnlyImageView;
}

@property (nonatomic, retain) UILabel *name;
@property (nonatomic, retain) UILabel *routeDescription;
@property (nonatomic) BOOL lowFloorOnly;

/**
 * Creates and positions the name label
**/
- (UILabel *)newLabelForName;

/**
 * Creates and positions the route description label
**/
- (UILabel *)newLabelForRouteDescription;

/**
 * Creates the UIImageView for the low floor only icon
**/
- (UIImageView *)newLowFloorImage;

@end
