//
//  SyncController.h
//  tramTRACKER
//
//  Created by Robert Amos on 3/08/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BackgroundSynchroniserDelegate.h"

@class BackgroundSynchroniser;
@class BackgroundSynchroniserDelegate;
@class Stop;
@class Route;
@class UpdatesController;

@interface SyncController : UITableViewController <BackgroundSynchroniserDelegate> {
	BOOL isSyncing;
	BOOL isFinished;
	BOOL isSaving;
    BOOL wasAborted;
	NSNumber *totalUpdates;
	NSNumber *completedUpdates;
	NSInteger detectedOperations;
	
	NSMutableDictionary *stopChanges;
	NSMutableDictionary *routeChanges;
	
	UITableViewCell *progressCounter;
	UIProgressView *progressView;
	UITextView *textView;
	BackgroundSynchroniser *syncManager;
	UpdatesController *parentUpdatesController;
	NSMutableArray *updateList;
	NSDictionary *params;
	NSString *errorMessage;
}

@property (nonatomic, retain) UITableViewCell *progressCounter;
@property (nonatomic, retain) UIProgressView *progressView;
@property (nonatomic, retain) UITextView *textView;
@property (nonatomic, assign) UpdatesController *parentUpdatesController;

- (id)initWithNumberOfStops:(NSNumber *)aStopCount numberOfRoutes:(NSNumber *)aRouteCount shouldUpdateTicketOutlets:(BOOL)updateTicketOutlets updateCache:(NSArray *)updates;
- (UIView *)tableFooterView;
- (void)cancel;
- (void)close;
- (BOOL)isActivityInProgress;
- (void)showLoadingIndicator;
- (void)updateProgress;
- (void)resetTotalUpdates:(NSNumber *)newNumber;

- (void)syncDidStart;
- (void)syncDidFinishAtTime:(NSDate *)finishTime;
- (void)syncDidStartSaving;
- (void)syncDidStartStop:(Stop *)stop;
- (void)syncDidFinishStop:(Stop *)stop;
- (void)syncDidStartRoute:(Route *)route;
- (void)syncDidFinishRoute:(Route *)route;
- (void)syncDidAddOperation:(NSOperation *)operation;
- (void)syncDidRemoveOperation;

- (void)updateMessageForStop:(Stop *)stop;
- (void)updateMessageForRoute:(Route *)route;

@end
