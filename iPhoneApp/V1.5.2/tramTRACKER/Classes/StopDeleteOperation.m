//
//  StopDeleteOperation.m
//  tramTRACKER
//
//  Created by Robert Amos on 7/08/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "StopDeleteOperation.h"
#import "Stop.h"
#import "BackgroundSynchroniser.h"
#import "Route.h"

@implementation StopDeleteOperation

@synthesize stop, syncManager;

- (id)initWithStop:(Stop *)aStop
{
	if (self = [super init])
	{
		stop = aStop;
		[stop retain];
		
		[self willChangeValueForKey:@"isExecuting"];
		executing = NO;
		[self didChangeValueForKey:@"isExecuting"];
		
		[self willChangeValueForKey:@"isFinished"];
		finished = NO;
		[self didChangeValueForKey:@"isFinished"];
		
		// set our priority
		[self setQueuePriority:NSOperationQueuePriorityNormal];
	}
	return self;
}

- (BOOL)isConcurrent
{
	return NO;
}

- (BOOL)isExecuting
{
	return executing;
}

- (BOOL)isFinished
{
	return finished;
}

/**
 * Actually start processing the update
 **/
- (void)main
{
	[self start];
}
- (void)start
{
	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}
	
	//NSLog(@"[STOPDELETE] Deleting stop");
	// we start by getting the destinations for this route
	[self willChangeValueForKey:@"isExecuting"];
	executing = YES;
	[self didChangeValueForKey:@"isExecuting"];
	
	// send a notification
	if (self.syncManager != nil && self.syncManager.delegate != nil && ![self isCancelled] && [self.syncManager.delegate respondsToSelector:@selector(deleteDidStartStop:)])
		[self.syncManager.delegate performSelectorOnMainThread:@selector(deleteDidStartStop:) withObject:stop waitUntilDone:NO];
	
	// delete the stop
	[self deleteStop];
	
	// sigh, poll until its finished
	while (![self isFinished])
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
}

- (void)deleteStop
{
	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}
	
	if (self.syncManager != nil)
	{
		// have the stop removed from the context, it will take care of itself
		NSManagedObjectContext *context = [self.syncManager managedObjectContext];
		[context deleteObject:stop];
        
        /*
        // open the defaults to see if we have a list
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        // do we have anything?
        NSArray *favouriteStopsFromDefaults = [defaults arrayForKey:@"favouriteStopsList"];
        
        NSMutableArray *newFavArray = [NSMutableArray arrayWithArray:favouriteStopsFromDefaults];
        
        if(newFavArray != nil && [newFavArray count] > 0){
            for (NSDictionary *favourite in newFavArray){
                NSNumber *trackerID = (NSNumber *)[favourite objectForKey:@"trackerid"];
                if(trackerID == stop.trackerID){
                    [newFavArray removeObject:favourite];
                }
            }
            [defaults setObject:[NSArray arrayWithArray:newFavArray] forKey:@"favouriteStopsList"];
        }
         */
	}
	
	[self finish];
}

- (void)finish
{
	[self willChangeValueForKey:@"isExecuting"];
	executing = NO;
	[self didChangeValueForKey:@"isExecuting"];
	
	[self willChangeValueForKey:@"isFinished"];
	finished = YES;
	[self didChangeValueForKey:@"isFinished"];
	
	// send a notification
	if (self.syncManager != nil && self.syncManager.delegate != nil && ![self isCancelled] && [self.syncManager.delegate respondsToSelector:@selector(deleteDidFinishStop:)])
		[self.syncManager.delegate performSelectorOnMainThread:@selector(deleteDidFinishStop:) withObject:stop waitUntilDone:NO];
	
	//NSLog(@"Stop deleted: %@", stop);
}

- (void)dealloc
{
	[stop release];
	[super dealloc];
}

@end
