    //
//  NearbyViewController.m
//  tramTRACKER
//
//  Created by Robert Amos on 25/07/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "NearbyViewController.h"
#import "NearbyListController.h"

@implementation NearbyViewController

@synthesize segmentedTitleControl, list, map, refreshButton, ticketsButton, listViewRefreshing;

- (id)initNearbyViewController
{
    self = [super initWithNibName:nil bundle:nil];
	if (self)
	{
		[self.navigationItem setTitleView:self.segmentedTitleControl];
		
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	// we're ready to show the view, add the appropriate view to the stack
	if (self.segmentedTitleControl.selectedSegmentIndex == 0)
	{
		[self showListView];
	} else
	{
		// Nearby Map
		[self showMapView];
	}
}

- (void)applicationDidBecomeActive
{
	// are we on the list? refresh location
	if (self.segmentedTitleControl.selectedSegmentIndex == 0)
	{
		if (list != nil)
			[self refreshLocation];
	} else if (map != nil)
	   [map startTracking];
		
}

/*
- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];

	// pass it through to our child view controller
	if (segmentedTitleControl.selectedSegmentIndex == 0)
		[list viewWillAppear:animated];
	else
		[map viewWillAppear:animated];

}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];

	// pass it through to our child view controller
	if (segmentedTitleControl.selectedSegmentIndex == 0)
		[list viewDidAppear:animated];
	else
		[map viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	// pass it through to our child view controller
	if (segmentedTitleControl.selectedSegmentIndex == 0)
		[list viewWillDisappear:animated];
	else
		[map viewWillDisappear:animated];

	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	// pass it through to our child view controller
	if (segmentedTitleControl.selectedSegmentIndex == 0)
		[list viewDidDisappear:animated];
	else
		[map viewDidDisappear:animated];

	[super viewDidDisappear:animated];
} */

#pragma mark -
#pragma mark Controls for the Segmented Controller

- (UISegmentedControl *)segmentedTitleControl
{
	// has this been set before?
	if (segmentedTitleControl == nil)
	{

		segmentedTitleControl = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:NSLocalizedString(@"title-nearby-list", @"List"), NSLocalizedString(@"title-nearby-map", @"Map"), nil]];

		[segmentedTitleControl setSegmentedControlStyle:UISegmentedControlStyleBar];
		[segmentedTitleControl setTintColor:[UIColor colorWithRed:TTNavigationBarTintColorRed green:TTNavigationBarTintColorGreen blue:TTNavigationBarTintColorBlue alpha:0.8]];

		[segmentedTitleControl setWidth:80 forSegmentAtIndex:0];
		[segmentedTitleControl setWidth:80 forSegmentAtIndex:1];
		
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		
		// segment 0 is List, segment 1 is Map
		[segmentedTitleControl setSelectedSegmentIndex:([[defaults valueForKey:@"nearbyTabDefaultSegment"] isEqualToString:@"map"] ? 1 : 0)];
		
		// add the action
		[segmentedTitleControl addTarget:self action:@selector(segmentedTitleControlDidChangeValue) forControlEvents:UIControlEventValueChanged];
	}

	// return the control
	return segmentedTitleControl;
}


- (void)segmentedTitleControlDidChangeValue
{
	// segment 0 is List, segment 1 is Map
	NSString *selectedSegment = (segmentedTitleControl.selectedSegmentIndex == 0 ? @"list" : @"map");
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject:selectedSegment forKey:@"nearbyTabDefaultSegment"];
	[defaults synchronize];
	
	// change it up, only if something has changed
	if (segmentedTitleControl.selectedSegmentIndex == 0 && list == nil)
	{
		if (map != nil)
		 	[self hideMapView];
		[self showListView];
	} else if (segmentedTitleControl.selectedSegmentIndex == 1 && map == nil)
	{
		if (list != nil)
			[self hideListView];
		[self showMapView];
	}
}

#pragma mark -
#pragma mark List View Methods

- (void)showListView
{
	// Nearby List
	list = [[NearbyListController alloc] initNearbyListWithNearbyViewController:self];
	
	// reset frame because we're not worried about the status bar
	CGRect newFrame = list.view.frame;
	newFrame.origin.y = 0;
	list.view.frame = newFrame;
	
	// add the view to the stack and tell the list we've done so
	[list viewWillAppear:NO];
	[self.view addSubview:list.view];
	[list viewDidAppear:NO];

	// we need to show the refresh button
	UIBarButtonItem *refresh = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshLocation)];
	[self setRefreshButton:refresh];
	[self.navigationItem setRightBarButtonItem:refresh];
	[refresh release];
}

- (void)hideListView
{
	// remove the refresh button
	[self.navigationItem setRightBarButtonItem:nil];
	[self setRefreshButton:nil];
	
	// remove the view from the stack
	[list viewWillDisappear:NO];
	[list.view removeFromSuperview];
	[list viewDidDisappear:NO];
	[list release];
	list = nil;
}

- (void)pushPIDForStop:(Stop *)stop withFilter:(Filter *)f animated:(BOOL)animated
{
	if (segmentedTitleControl.selectedSegmentIndex == 0 && self.list != nil)
		[self.list pushPIDForStop:stop withFilter:f animated:animated];
    else if (segmentedTitleControl.selectedSegmentIndex == 1 && self.map != nil)
        [self.map pushPIDForStop:stop];
}

- (void)refreshLocation
{
	if (segmentedTitleControl.selectedSegmentIndex == 0 && self.list != nil)
		[self.list refreshLocation];
}


- (void)setListViewRefreshing:(BOOL)refreshing
{
	// if we're refreshing we need to hide the refresh button, and likewise show it if not
	if (segmentedTitleControl.selectedSegmentIndex == 0 && self.refreshButton != nil)
		[self.refreshButton setEnabled:!refreshing];
}

#pragma mark -
#pragma mark Map View Methods

- (void)showMapView
{
	// Nearby Map
	map = [[MapViewController alloc] initNearbyMapWithNearbyViewController:self];
	
	// reset frame because we're not worried about the status bar
	CGRect newFrame = map.view.frame;
	newFrame.origin.y = 0;
	map.view.frame = newFrame;

	// Add the view to the stack
	[map viewWillAppear:NO];
	[self.view addSubview:map.view];
	[map viewDidAppear:NO];
	
	// we need to show the tickets button
	UIBarButtonItem *tickets = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"nearby-button-tickets", @"Tickets")
																style:UIBarButtonItemStylePlain
															   target:self
															   action:@selector(toggleTicketRetailers)];
	[self setTicketsButton:tickets];
	[self.navigationItem setRightBarButtonItem:tickets];
	[tickets release];
}

- (void)hideMapView
{
	// remove the ticket button
	[self.navigationItem setRightBarButtonItem:nil];
	[self setTicketsButton:nil];
	
	// remove the map view from the stack
	[map viewWillDisappear:NO];
	[map.view removeFromSuperview];
	[map viewDidDisappear:NO];
	
	// kill the map view controller
	[map release];
	map = nil;
}

- (void)toggleTicketRetailers
{
	// we show or hide tickets based on the map view and what it currently thinks
	if (map != nil)
	{
		// show or hide?
		if (map.showTicketRetailers)
		{
			// Currently showing ticket retailers, lets hide them
			[map setShowTicketRetailers:NO];
			[self.ticketsButton setStyle:UIBarButtonItemStylePlain];
		} else
		{
			// Currently not showing ticket retailers, show them
			[map setShowTicketRetailers:YES];
			[self.ticketsButton setStyle:UIBarButtonItemStyleDone];
		}
	}
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)dealloc {

    [segmentedTitleControl removeTarget:self action:NULL forControlEvents:UIControlEventValueChanged];
	[segmentedTitleControl release]; segmentedTitleControl = nil;
	[list release]; list = nil;
	[map release]; map = nil;
	[refreshButton release]; refreshButton = nil;
	[ticketsButton release]; ticketsButton = nil;

	[super dealloc];
}


@end
