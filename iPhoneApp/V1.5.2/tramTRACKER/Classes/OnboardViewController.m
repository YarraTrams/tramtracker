//
//  OnboardViewController.m
//  tramTRACKER
//
//  Created by Robert Amos on 21/04/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "OnboardViewController.h"

const NSInteger RouteLabelHeight = 73;
const NSInteger RouteLabelHeightWithMessage = 93;
const NSInteger OnboardRefreshInterval = 10;

#define PINPOINT_FOR_SECONDS	15

@implementation OnboardViewController

@synthesize listViewController, service, routeInfo, hasMessage, locationManager, timerRefresh, locateButton,
			stopButton, activityController, atLayover, startedLocating, accuracyLevel, isStopped;

// initialisation
- (id)init
{
    self = [super init];
	if (self)
	{
		OnboardListView *onboardList = [[OnboardListView alloc] initWithParentViewController:self];
		[self setListViewController:onboardList];
		[onboardList.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
		[onboardList.tableView setTableFooterView:[onboardList tableFooterView]];
		[onboardList release];

		// go go!
		LocationManager *l = [[LocationManager alloc] init];
		[l setDelegate:self.listViewController];
		
		[self setLocationManager:l];
		[self.listViewController setLocationManager:l];
		[l release];
		
		NSInteger height = ([self hasMessage] ? RouteLabelHeightWithMessage : RouteLabelHeight);
		RouteInfoView *r = [[RouteInfoView alloc] initWithFrame:CGRectMake(0, 0, 320, height)];
		[self setRouteInfo:r];
		[r release];
		
		nilJourneyCount = 0;
		accuracyLevel = 0;
		updateRequestCount = 0;
		seenGPSTipCount = [[NSUserDefaults standardUserDefaults] integerForKey:@"seenGPSTipCount"];
		seenMapTipCount = [[NSUserDefaults standardUserDefaults] integerForKey:@"seenMapTipCount"];
	}
	return self;
}

// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
    
    UIView *containerView = nil;
    
    if ([[UIScreen mainScreen] bounds].size.height == 568){
        
        containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 475)];
    }
    else{
        containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 367)];
    }
    
	[containerView addSubview:self.routeInfo];

	NSInteger height = ([self hasMessage] ? RouteLabelHeightWithMessage: RouteLabelHeight);
    
    if ([[UIScreen mainScreen] bounds].size.height == 568){
        [self.listViewController.view setFrame:CGRectMake(0, height, 320, 475 - height)];
    }else{
        [self.listViewController.view setFrame:CGRectMake(0, height, 320, 367 - height)];
    }
	
	[containerView addSubview:self.listViewController.view];
	
	[self setView:containerView];
	[containerView release];
	
	// the "locate me" button"
	UIBarButtonItem *locate = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nearbybutton.png"]
															   style:UIBarButtonItemStylePlain
															  target:self
															  action:@selector(startUpdatingImmediately)];
	[self setLocateButton:locate];
	[locate release];

	// the "stop locating me" button
	ActivityBarButtonController *activityBarButton = [[ActivityBarButtonController alloc] initWithNibName:nil bundle:nil target:self action:@selector(stopUpdating)];
	UIBarButtonItem *stop = [[UIBarButtonItem alloc] initWithCustomView:activityBarButton.view];
	[self setStopButton:stop];
	[self.navigationItem setRightBarButtonItem:self.stopButton animated:YES];
	[self setActivityController:activityBarButton];
	[activityBarButton release];
	[stop release];
}

// View appeared, start locating us
- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	[self.listViewController viewDidAppear:animated];
	
	// if we have an accuracy level, pass it through
	if (self.accuracyLevel > 0)
		[self.activityController setAccuracyLevel:self.accuracyLevel];
	
	[[UIApplication sharedApplication] setIdleTimerDisabled:YES];
	
}

// View did disappear, stop trying to find us
- (void)viewDidDisappear:(BOOL)animated
{
	[self stopUpdating];
	[self.locationManager stopUpdatingLocation];
	[self.locationManager setDelegate:nil];
	
	// kill off the pids service
	[self.service setDelegate:nil];
	[self setService:nil];

	[[UIApplication sharedApplication] setIdleTimerDisabled:NO];
}

- (void)startUpdating
{
	// create the pids service if its not already there
	if (self.service == nil)
	{
		PidsService *s = [[PidsService alloc] init];
		[s setDelegate:self];
		[self setService:s];
		[s release];
	}
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSInteger interval = [defaults integerForKey:@"onboard_refresh"];
	
	[self setTimerRefresh:[NSTimer scheduledTimerWithTimeInterval:(interval <= 0 ? OnboardRefreshInterval : interval)
														   target:self
														 selector:@selector(refreshPredictions:)
														 userInfo:nil
														  repeats:YES]];
	
	isStopped = NO;
	[self.navigationItem setRightBarButtonItem:self.stopButton animated:YES];
	if (self.listViewController.stopList != nil && [self.listViewController.stopList count] > 0)
		[self.listViewController.tableView reloadData];
	[self setAccuracyLevel:0];
}
- (void)startUpdatingImmediately
{
	[self startUpdating];
	[self.timerRefresh fire];
}

- (void)stopUpdating
{
	[self.timerRefresh invalidate];
	[self setTimerRefresh:nil];
	isStopped = YES;
	
	if (self.locationManager != nil)
		[self.locationManager stopUpdatingLocation];

	[self.navigationItem setRightBarButtonItem:self.locateButton animated:YES];
	if (self.listViewController.stopList != nil && [self.listViewController.stopList count] > 0)
		[self.listViewController.tableView reloadData];
	[self updateHeadboardMessage:YES];
}

- (void)refreshPredictions:(NSTimer *)aTimer
{
	[self.service getJourneyForTramNumber:self.journey.tram.number];
	updateRequestCount++;
	
	/*if (updateRequestCount > 1)
	{
		NSError *outError;
		AVAudioPlayer *audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"a" ofType:@"m4a"]] error:&outError];
		[audioPlayer setDelegate:self];
		[audioPlayer play];
		//NSLog(@"%@", outError);
	}*/
}

// return the journey
- (Journey *)journey
{
    return journey;
}


// overload the set journey
- (void)setJourney:(JourneyStub *)newJourneyStub
{
    // Convert the stub to a full journey
    Journey *newJourney = [Journey journeyForStub:newJourneyStub];
    
	updateRequestCount = 0;

	// if we haven't been given a new journey then drop the result - if we've gotten here we already have *a* result to work off
	if (newJourney == nil)
	{
		// we increment the nil journey count
		if ((journey != nil || [journey.stops count] == 0) && nilJourneyCount < 3)
			nilJourneyCount++;

		// and update the message - three or more nil journeys will trigger a not found message
		[self updateHeadboardMessage:YES];
		
		return;
	}
	nilJourneyCount = 0;
	[newJourney retain];
	
	// the very first time we load a journey we need to configure the headboard and the stop list
	if (journey == nil)
	{
		[self setTitle:[newJourney.tram name]];
		[self.routeInfo setImage:[newJourney.tram image]];
		[self.routeInfo setURLOfGongSound:[newJourney.tram URLOfGongSound]];
		
		// set the layover status
		atLayover = [newJourney.tram isAtLayover];

		// start the tracking
		[self startUpdating];
		
		// set the start time
		[self setStartedLocating:[NSDate date]];
	}

	// set the route details in the headboard (if they've changed only)
	if (journey == nil || [self hasTramChangedRoute:newJourney.tram] || [journey.stops count] != [newJourney.stops count])
	{
		// deal with the unusuals first - route zero
		if ([newJourney.tram isRouteZero])
		{
			// set the route number
			[self.routeInfo setRouteNumber:newJourney.tram.headboardRouteNumber];
			
			// set the direction text
			[self.routeInfo setDestination:NSLocalizedString(@"onboard-depotdestination", @"Depot Destination")];
		}
		

		// is it an unknown route?
		else if ([newJourney.tram isUnknownRoute])
		{
			// set the route number as the headboard
			[self.routeInfo setRouteNumber:newJourney.tram.headboardRouteNumber];
			
			// set the direction text to unknown
			[self.routeInfo setDestination:NSLocalizedString(@"onboard-unknowndestination", @"Unknown Destination")];
		}
		
		// is it route 35?
		else if ([newJourney.tram isCityCircle])
		{
			// set the route's name
			[self.routeInfo setRouteNumberName:NSLocalizedString(@"onboard-route-citycircle", nil)];
			
			// and set the directional text
			[self.routeInfo setDirection:[newJourney.tram destination]];
		}
		
		// otherwise its just a plain old ordinary route
		else
		{
			// set the route number
			[self.routeInfo setRouteNumber:(newJourney.tram.headboardRouteNumber == nil ? newJourney.tram.route.headboardNumber : newJourney.tram.headboardRouteNumber)];

			// if we have stops set the destination to include a 'via' message
			if ([newJourney.stops count] > 0)
			{
				Stop *s = [(JourneyStop *)[newJourney.stops objectAtIndex:0] stop];
				[self.routeInfo setDestination:[newJourney.tram.route formattedDestinationDepartingFromStop:s upDirection:[newJourney.tram upDirection]]];

			
			// otherwise its just an ordinary route
			} else
			{
				[self.routeInfo setDestination:[newJourney.tram destination]];
			}
		}
	}
	
	// have we just changed into layover?
	if (journey != nil && ![self isAtLayover] && [newJourney.tram isAtLayover] && ![journey.tram isAtLayover])
		[self setAtLayover:YES];
	
	// pass it to the list
	[self.listViewController setJourney:newJourney];

	// save the new one
	BOOL animateHeadboardMessage = (journey != nil);
	if (journey != nil)
		[journey release];
	journey = newJourney;
	
	// and finally, update the headboard message based on the current conditions
	[self updateHeadboardMessage:animateHeadboardMessage];
}
	
- (void)updateHeadboardMessage:(BOOL)animated
{
	// these are ranked in order of message priority
	[self.routeInfo setBadConnection:NO];
	if ([self isStopped])
		[self.routeInfo setMessage:nil];
	
	// 1. Tram can not be found.
	else if (![self hasBadConnection] && nilJourneyCount >= 3)
		[self.routeInfo setMessage:[NSString stringWithFormat:NSLocalizedString(@"onboard-tramnolongerfound", @"Tram no longer found"), [journey.tram name]]];

	// 2. Tram is travelling on an unknown route
	else if ([journey.tram isUnknownRoute])
		[self.routeInfo setMessage:[NSString stringWithFormat:NSLocalizedString(@"onboard-unknown-destination", @"Tram Not Found"), [journey.tram name]]];

	// 3. the Tram is at layover
	else if ([self isAtLayover])
		[self.routeInfo setMessage:[NSString stringWithFormat:NSLocalizedString(@"onboard-message-atlayover", @"At Layover"), [journey.tram name]]];

	// 4. The tram is returning to the depot
	else if ([journey.tram isRouteZero])
		[self.routeInfo setMessage:[NSString stringWithFormat:NSLocalizedString(@"onboard-tramreturntodepot", @"Returning to depot"), [journey.tram name]]];

	// 5. Tram is travelling on a disrupted route
	else if ([journey.tram isDisrupted])
		[self.routeInfo setMessage:[NSString stringWithFormat:NSLocalizedString(@"onboard-message-disrupted", @"Disrupted route"), [journey.tram headboardRouteNumber]]];
	
	// 6. Tram may be affected by a special event
	else if ([journey.tram hasSpecialEvent])
		[self.routeInfo setMessage:[NSString stringWithFormat:NSLocalizedString(@"onboard-message-specialevent", @"Special Event"), [journey.tram headboardRouteNumber]]];

	// 7. Arrival times are not available for this route
	else if (![journey.tram isAvailable])
		[self.routeInfo setMessage:NSLocalizedString(@"onboard-message-nonpublicroute", @"Non-Public Route")];

	// 8. Missing updates from the server
	else if ([self hasBadConnection])
	{
		[self.routeInfo setMessage:[NSString stringWithFormat:NSLocalizedString(@"onboard-bad-connection", @"Bad Connection"), [[UIDevice currentDevice] model]]];
		if (updateRequestCount > 12)
			[self.routeInfo setBadConnection:YES];
	}

	// 9. Core Location is disabled
	else if (![self.locationManager locationServicesEnabled])
	{
		[self.routeInfo setMessage:NSLocalizedString(@"onboard-corelocation-enable", @"Enable Location Services")];
		[self setAccuracyLevel:5];
	}
	
	// 10. You're in the CBD - not realtime.
	else if ([journey.stops count] > 0 && [[(JourneyStop *)[journey.stops objectAtIndex:0] stop] isCityStop])
		[self.routeInfo setMessage:[NSString stringWithFormat:NSLocalizedString(@"onboard-cbd", @"CBD BAD!"), [[UIDevice currentDevice] model]]];
	
	// 11. Cannot accurately identify location
	else if ([journey.stops count] > 0 && self.locationManager.location != nil && [LocationManager distanceFromLocation:self.locationManager.location toLocation:[[(JourneyStop *)[journey.stops objectAtIndex:0] stop] location]] > 500)
		[self.routeInfo setMessage:[NSString stringWithFormat:NSLocalizedString(@"onboard-corelocation-accuracy", @"GPS BAD!"), [[UIDevice currentDevice] model]]];

	// 12. We've just started pinpointing your location
	else if ([[NSDate date] timeIntervalSinceDate:self.startedLocating] < PINPOINT_FOR_SECONDS)
		[self.routeInfo setMessage:NSLocalizedString(@"onboard-initial-message", @"Pinpointing location")];
	
	// 13. bad BAD GPS.
	else if (self.accuracyLevel > 3)
		[self.routeInfo setMessage:[NSString stringWithFormat:NSLocalizedString(@"onboard-corelocation-accuracy", @"GPS BAD!"), [[UIDevice currentDevice] model]]];

	// 14. No messages? Show the Map Tip
	else if (seenMapTipCount < 2 && (mapTipFirstShownDate == nil || [[NSDate date] timeIntervalSinceDate:mapTipFirstShownDate] < 30))
	{
		[self.routeInfo setMessage:NSLocalizedString(@"onboard-map-tip", @"Map Tip")];
		
		// check to see if we have incremented this yet or not, if we have then update it
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		if ([defaults integerForKey:@"seenMapTipCount"] == seenMapTipCount)
		{
			[defaults setInteger:seenMapTipCount+1 forKey:@"seenMapTipCount"];
		}
		
		// first time its been shown?
		if (mapTipFirstShownDate == nil)
			mapTipFirstShownDate = [[NSDate alloc] init];
	}
	
	// 15. No messages? show the GPS tip.
	else if (seenGPSTipCount < 2 && (tipFirstShownDate == nil || [[NSDate date] timeIntervalSinceDate:tipFirstShownDate] < 30))
	{
		[self.routeInfo setMessage:NSLocalizedString(@"onboard-gps-tip", @"GPS Tip")];
		
		// check to see if we have incremented this yet or not, if we have then update it
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		if ([defaults integerForKey:@"seenGPSTipCount"] == seenGPSTipCount)
		{
			[defaults setInteger:seenGPSTipCount+1 forKey:@"seenGPSTipCount"];
		}

		// first time its been shown?
		if (tipFirstShownDate == nil)
			tipFirstShownDate = [[NSDate alloc] init];
	}
	
	// nothing else could be there
	else if (self.routeInfo.message != nil)
		[self.routeInfo setMessage:nil];
	
	// hide the bad connection icon if its no longer there
	if (updateRequestCount < 12)
		[self.routeInfo setBadConnection:NO];
	
	// finally, make sure the message is showing or hidden based on need
	if ([self hasMessage] != (self.routeInfo.message != nil))
		[self setHasMessage:(self.routeInfo.message != nil) animated:animated];
}

- (BOOL)hasBadConnection
{
	return updateRequestCount > 3;
}

- (BOOL)hasTramChangedRoute:(Tram *)tram
{
	// if the route itself has changed, then yes
	if (![tram.route isEqual:journey.tram.route])
		return YES;
	
	// if the headboard details have changed, then yes
	if (![tram.headboardRouteNumber isEqualToString:journey.tram.headboardRouteNumber])
		return YES;
	
	// if the direction has changed, then yes
	if (tram.upDirection != journey.tram.upDirection)
		return YES;
	
	// otherwise no
	return NO;
}

- (void)toggleRouteInfo
{
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.5];
	[UIView setAnimationDelegate:self];
	
	CGRect routeInfoFrame = self.routeInfo.frame;
	CGRect listFrame = self.listViewController.view.frame;
	if (routeInfoFrame.size.height == 0)
	{
		// show the route info
		routeInfoFrame.size.height = [self hasMessage] ? RouteLabelHeightWithMessage : RouteLabelHeight;
		listFrame.origin.y = [self hasMessage] ? RouteLabelHeightWithMessage : RouteLabelHeight;
		[UIView setAnimationDidStopSelector:@selector(toggleRouteInfoStopped:finished:context:)];
	} else {
		routeInfoFrame.size.height = 0;
		listFrame.origin.y = 0;
        
        if ([[UIScreen mainScreen] bounds].size.height == 568){
            listFrame.size.height = 475;
        }else{
            listFrame.size.height = 367;
        }
		
	}
	
	[self.routeInfo setFrame:routeInfoFrame];
	[self.listViewController.view setFrame:listFrame];
	[UIView commitAnimations];
}

- (void)toggleRouteInfoStopped:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
	CGRect listFrame = self.listViewController.view.frame;
	listFrame.size.height = 367 - listFrame.origin.y;
    
    if ([[UIScreen mainScreen] bounds].size.height == 568){
        listFrame.size.height = 475 - listFrame.origin.y;
    }else{
        listFrame.size.height = 367 - listFrame.origin.y;
    }
	[self.listViewController.view setFrame:listFrame];
}

- (void)setHasMessage:(BOOL)newHasMessage animated:(BOOL)animated
{
	[self setHasMessage:newHasMessage];

	if (animated)
	{
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:0.5];
		[UIView setAnimationDelegate:self];
		[UIView setAnimationDidStopSelector:@selector(toggleRouteInfoStopped:finished:context:)];
	}
	CGRect routeInfoFrame = self.routeInfo.frame;
	CGRect listFrame = self.listViewController.view.frame;
	routeInfoFrame.size.height = newHasMessage ? RouteLabelHeightWithMessage : RouteLabelHeight;
	listFrame.origin.y = newHasMessage ? RouteLabelHeightWithMessage : RouteLabelHeight;
	[self.routeInfo setFrame:routeInfoFrame];
	[self.listViewController.view setFrame:listFrame];

	if (animated)
		[UIView commitAnimations];
}

- (void)toggleLocating:(NSNumber *)isLocating
{
	// if we stop the locating then we have a few things to kill off
	if ([isLocating boolValue])
	{
		[self startUpdating];
		[self.timerRefresh fire];
		if (self.locationManager != nil && [self.locationManager locationServicesEnabled])
			[self.locationManager startUpdatingLocation];
	} else
		[self stopUpdating];
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (void)setAccuracyLevel:(NSInteger)anAccuracyLevel
{
	accuracyLevel = anAccuracyLevel;
	if (self.activityController != nil)
		[self.activityController setAccuracyLevel:accuracyLevel];
}

- (void)applicationDidEnterBackground
{
	[self stopUpdating];
	[[UIApplication sharedApplication] setIdleTimerDisabled:NO];
}
- (void)applicationDidBecomeActive
{
	[self startUpdatingImmediately];
	[[UIApplication sharedApplication] setIdleTimerDisabled:YES];
}

- (void)setMessage:(NSString *)aMessage
{
	[self.routeInfo setMessage:aMessage];
}
- (NSString *)message
{
	return self.routeInfo.message;
}

- (void)dealloc {
	[routeInfo release];
	[listViewController release];
	[journey release];
	[service release];
	[timerRefresh release];
	[locationManager release];
	
    [super dealloc];
}


@end
