//
//  TicketRetailerAnnotationView.h
//  tramTRACKER
//
//  Created by Robert Amos on 25/07/10.
//  Copyright 2010 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>


@interface TicketRetailerAnnotationView : MKPinAnnotationView {

}

- (void)didTouchTicketCalloutAccessory:(id)sender;

@end
