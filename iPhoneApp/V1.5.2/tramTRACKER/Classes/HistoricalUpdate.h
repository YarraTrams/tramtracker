//
//  HistoricalUpdate.h
//  tramTRACKER
//
//  Created by Robert Amos on 5/08/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <CoreData/CoreData.h>

extern NSString * const HistoricalUpdateFileName;

/**
 * Represents an update that has occured to the local database.
**/
@interface HistoricalUpdate :  NSObject <NSCoding>  
{
	/**
	 * The name of the update
	**/
	NSString *name;
	
	/**
	 * The message
	**/
	NSString *message;
	
	/**
	 * The date and time that the update occured
	**/
	NSDate *updatedDate;
}

/**
 * Whether the application has historical updates
**/
+ (BOOL)hasHistoricalUpdates;

/**
 * Returns an array of all historical updates
**/
+ (NSArray *)getHistoricalUpdatesList;

/**
 * Saves a new historical updates list to the storage
**/
+ (void)saveHistoricalUpdatesList:(NSArray *)updateList;

/**
 * Clears the historical updates list from storage
**/
+ (void)clearHistoricalUpdatesList;

- (id)initHistoricalUpdate;

- (id)initWithCoder:(NSCoder *)coder;
- (void)encodeWithCoder:(NSCoder *)coder;

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * message;
@property (nonatomic, retain) NSDate * updatedDate;

@end



