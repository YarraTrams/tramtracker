//
//  SimpleCell.h
//  tramTRACKER
//
//  Created by Robert Amos on 31/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef UITableViewCellStyle TTSimpleCellStyle;
extern TTSimpleCellStyle const TTSimpleCellStyleDefault;
extern TTSimpleCellStyle const TTSimpleCellStyleValue1;
extern TTSimpleCellStyle const TTSimpleCellStyleMultiLine;
extern TTSimpleCellStyle const TTSimpleCellStyleSubtitle;

@interface SimpleCell : UITableViewCell {
	BOOL multiLine;
}

- (id)initWithCellStyle:(TTSimpleCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;
- (void)setTextLabelText:(NSString *)string;
- (void)setDetailTextLabelText:(NSString *)string;
- (void)setMultiLine:(BOOL)multiLine;

@end
