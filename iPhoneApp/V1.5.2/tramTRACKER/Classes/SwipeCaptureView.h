//
//  SwipeCaptureView.h
//  tramTRACKER
//
//  Created by Robert Amos on 6/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PIDViewController.h"

/**
 * A custom UIButton that sits on top of a PID and captures touch events to send to the delegate
**/
@interface SwipeCaptureView : UIButton {

	/**
	 * A PID to act as our delegate. Captured touch events are sent to it.
	**/
	PIDViewController *delegate;
}

@property (nonatomic, retain) IBOutlet PIDViewController *delegate;

@end
