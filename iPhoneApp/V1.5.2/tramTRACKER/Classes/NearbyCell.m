//
//  NearbyCell.m
//  tramTRACKER
//
//  Created by Robert Amos on 4/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "NearbyCell.h"


@implementation NearbyCell

@synthesize distance, name, routeDescription;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier]) {
		GradientBackgroundCell *background = [[GradientBackgroundCell alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height) cellStyle:OnboardCellStyleNone];
		[self setBackgroundView:background];
		[background release];
		
		// get a new distance label
		distance = [self newLabelForDistance];
		[self.contentView addSubview:self.distance];
		
		// and the name label
		name = [self newLabelForName];
		[self.contentView addSubview:self.name];
		
		// and the route description
		routeDescription = [self newLabelForRouteDescription];
		[self.contentView addSubview:self.routeDescription];
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


//
// Create a new distance label
//
- (UILabel *)newLabelForDistance
{
	// position it
	CGRect frame = CGRectMake(230, 6, 65, 30);
	UILabel *newLabel = [[UILabel alloc] initWithFrame:frame];
	
	// make some happy happy changes
	[newLabel setBackgroundColor:[UIColor clearColor]];					// background colour
	[newLabel setTextColor:[UIColor blackColor]];						// text colour
	[newLabel setHighlightedTextColor:[UIColor whiteColor]];			// text colour when the cell is selected (background goes blue)
	[newLabel setOpaque:NO];											// no need for transparency here
	[newLabel setFont:[UIFont systemFontOfSize:18]];					// and a 18pt font
	[newLabel setTextAlignment:UITextAlignmentRight];					// right align the text (so it sits against the right side)
	[newLabel setLineBreakMode:UILineBreakModeHeadTruncation];			// truncate at the start
	
	return newLabel;
}

//
// Create a new name label
//
- (UILabel *)newLabelForName
{
	// position it
	CGRect frame = CGRectMake(10, 2, 215, 22);
	UILabel *newLabel = [[UILabel alloc] initWithFrame:frame];
	
	// make some happy happy changes
	[newLabel setBackgroundColor:[UIColor clearColor]];					// background colour
	[newLabel setTextColor:[UIColor blackColor]];						// text colour
	[newLabel setHighlightedTextColor:[UIColor whiteColor]];			// text colour when the cell is selected (background goes blue)
	[newLabel setOpaque:NO];											// no need for transparency here
	[newLabel setFont:[UIFont boldSystemFontOfSize:16]];				// and a 16pt font
	[newLabel setTextAlignment:UITextAlignmentLeft];					// right align the text (so it sits against the right side)
	[newLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];		// Let the UI automatically resize the width when necessary (ie editing)
	
	return newLabel;
}

//
// Create a new route description label
//
- (UILabel *)newLabelForRouteDescription
{
	// position it
	CGRect frame = CGRectMake(10, 23, 215, 18);
	UILabel *newLabel = [[UILabel alloc] initWithFrame:frame];
	
	// make some happy happy changes
	[newLabel setBackgroundColor:[UIColor clearColor]];					// background colour
	[newLabel setTextColor:[UIColor grayColor]];						// text colour
	[newLabel setHighlightedTextColor:[UIColor whiteColor]];			// text colour when the cell is selected (background goes blue)
	[newLabel setOpaque:NO];											// no need for transparency here
	[newLabel setFont:[UIFont systemFontOfSize:14]];					// and a 14pt font
	[newLabel setTextAlignment:UITextAlignmentLeft];					// right align the text (so it sits against the right side)
	[newLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];		// Let the UI automatically resize the width when necessary (ie editing)
	
	return newLabel;
}

- (void)dealloc {

	// release properties
	[distance release];
	[name release];
	[routeDescription release];
	
	[super dealloc];
}


@end
