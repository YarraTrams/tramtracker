//
//  StopUpdateOperation.h
//  tramTRACKER
//
//  Created by Robert Amos on 29/07/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Stop;
@class PidsService;
@class BackgroundSynchroniser;

/**
 * An update operation to a stop
 *
 * @ingroup Sync
 **/
@interface StopUpdateOperation : NSOperation {
	
	/**
	 * The Stop to be updated.
	**/
	Stop *stop;

	/**
	 * An instance of the PidsService to retrieve route information from.
	 **/
	PidsService *service;
	
	/**
	 * The BackgroundSynchroniser object that is managing this update operation
	 **/
	BackgroundSynchroniser *syncManager;
	
	/**
	 * Status - YES if the operation is executing, NO otherwise
	 **/
	BOOL executing;
	
	/**
	 * Status - YES if the operation was finished (or cancelled), NO otherwise
	 **/
	BOOL finished;
}

@property (nonatomic, readonly) Stop *stop;
@property (nonatomic, assign) BackgroundSynchroniser *syncManager;

/**
 * Initialises a stop update operation for the specified Stop.
 *
 * @param	aStop			The stop to be updated.
 * @return					An initialisde StopUpdateOperation
**/
- (id)initWithStop:(Stop *)aStop;

/**
 * Required by the NSOperationQueue, whether to execute this operation concurrently.
 **/
- (BOOL)isConcurrent;

/**
 * NSOperationQueue wrapper for the executing ivar.
 **/
- (BOOL)isExecuting;

/**
 * NSOperationQueue wrapper for the finished ivar.
 **/
- (BOOL)isFinished;

/**
 * Starts the Stop Update Operation
 **/
- (void)start;

/**
 * Updates the stop information
**/
- (void)updateInformation;

/**
 * Finishes the Update operation and handles notifications
 **/
- (void)finish;


@end
