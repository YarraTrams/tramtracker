//
//  DepartureTimePicker.m
//  tramTRACKER
//
//  Created by Robert Amos on 6/04/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "DepartureTimePicker.h"


@implementation DepartureTimePicker

@synthesize departureDate, departureTime, scheduledDepartureTime, picker;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil scheduledDepartureTime:(NSDate *)date target:(id)aTarget action:(SEL)selector {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
		
		[self setHidesBottomBarWhenPushed:YES];
		
		// save the departure time
		[self setScheduledDepartureTime:date];
		
		target = aTarget;
		action = selector;
	}
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

	// set our text
	[self setTitle:NSLocalizedString(@"scheduled-title-departuretime", @"Select Date & Time")];
	
	// and the buttons
	UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelDatePicker)];
	[self.navigationItem setLeftBarButtonItem:cancelButton animated:NO];
	[cancelButton release];
	
	UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneDatePicker)];
	[self.navigationItem setRightBarButtonItem:doneButton animated:NO];
	[doneButton release];

	// update the picker
	[self.picker setDate:self.scheduledDepartureTime animated:NO];
	[self.picker setMinimumDate:[NSDate date]];
	[self.picker setMaximumDate:[NSDate dateWithTimeIntervalSinceNow:604800]];
	[self updateDateAndTimeButtons];
}

- (IBAction)pickerDidChangeValue:(id)sender
{
	// update the date and time locally
	[self setScheduledDepartureTime:self.picker.date];
	
	[self updateDateAndTimeButtons];
}

- (void)cancelDatePicker
{
	// just cancel
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)doneDatePicker
{
	// tell the target about us
	if (target != nil && [target respondsToSelector:action])
	{
		[target performSelector:action withObject:self.scheduledDepartureTime];
	}
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)updateDateAndTimeButtons
{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"EEEE, d MMMM yyyy"];
	[self.departureDate setTitle:[dateFormatter stringFromDate:self.scheduledDepartureTime] forState:UIControlStateDisabled];
	
	//[dateFormatter setDateFormat:@"hh:mm a"]; - use built in style to allow for 24 hour time setting
	[dateFormatter setDateStyle:NSDateFormatterNoStyle];
	[dateFormatter setTimeStyle:NSDateFormatterShortStyle];
	[self.departureTime setTitle:[dateFormatter stringFromDate:self.scheduledDepartureTime] forState:UIControlStateDisabled];
	[dateFormatter release];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
	
	[departureDate release];
	[departureTime release];
	[scheduledDepartureTime release];
	[picker release];
	
    [super dealloc];
}


@end
