//
//  UpdatesController.m
//  tramTRACKER
//
//  Created by Robert Amos on 3/08/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "UpdatesController.h"
#import "BackgroundSynchroniser.h"
#import "SyncController.h"
#import "HistoricalUpdate.h"
#import "HistoricalUpdatesController.h"
#import "PidsService.h"

#define STOP_UPDATE_BYTES 765
#define ROUTE_UPDATES_BYTES 7026

@implementation UpdatesController

- (id)initUpdatesController
{
	if (self = [super initWithStyle:UITableViewStyleGrouped])
	{
		hasLoaded = NO;
		hasAppeared = NO;
		isSyncing = NO;
		hasFinished = NO;
		isCheckingForUpdates = NO;
		
		// set the title
		[self setTitle:NSLocalizedString(@"title-updates", nil)];
		
		// set the background colour
		[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
		[self.tableView setAllowsSelection:NO];
	}
	return self;
}

- (BOOL)hasFinished
{
	return hasFinished;
}
- (BOOL)hasAppeared
{
	return hasAppeared;
}

- (void)checkForUpdatesDidStart
{
	isCheckingForUpdates = YES;
	hasFinished = NO;

	[self showLoadingIndicator];
	
	if (errorMessage != nil)
	{
		[errorMessage release];
		errorMessage = nil;
	}
	
	if (hasLoaded)
		[self.tableView setTableFooterView:[self tableFooterView]];
}
- (void)checkForUpdatesDidFinish:(NSDictionary *)updatesFound
{
	// did we find updates?
	if (updatesFound != nil)
	{
		numberOfStopUpdates = [[updatesFound objectForKey:TTSyncKeyStops] retain];
		numberOfRouteUpdates = [[updatesFound objectForKey:TTSyncKeyRoutes] retain];
		hasTicketOutletUpdates = [[updatesFound objectForKey:TTSyncKeyTickets] boolValue];
		ticketOutletFileSize = [[updatesFound objectForKey:TTSyncKeyTicketFileSize] retain];
		updates = [[updatesFound objectForKey:TTSyncKeyUpdates] retain];

		int totalUpdates = ([numberOfStopUpdates integerValue] + [numberOfRouteUpdates integerValue] + [[updatesFound objectForKey:TTSyncKeyTickets] integerValue]);
		tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];

        // save the sync time
        NSDate *finishTime = nil;
        finishTime = [updatesFound objectForKey:TTSyncKeyServerTime];
		
        // do we even have updates?
		if (totalUpdates > 0)
		{
			NSString *badgeValue = [NSString stringWithFormat:@"%d", totalUpdates];

			[self.navigationController.tabBarItem setBadgeValue:badgeValue];
			
			// are we under the more navigationcontroller?
			if ([[d.tabBar viewControllers] indexOfObject:self.navigationController] >= 4)
			{
				// we're in the more one, make sure to badge it too
				[[[d.tabBar moreNavigationController] tabBarItem] setBadgeValue:badgeValue];
			}
		}
		
		// no updates, clear stuff
		else
		{
			// clear the badge value
			[self.navigationController.tabBarItem setBadgeValue:nil];
			
			// are we under the more navigationcontroller?
			tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
			if ([[d.tabBar viewControllers] indexOfObject:self.navigationController] >= 4)
			{
				// we're in the more one, make sure to badge it too
				[[[d.tabBar moreNavigationController] tabBarItem] setBadgeValue:nil];
			}	
			
			// tell the app delegate to save this as our last synchronisation time
			[d setLastSyncDate:finishTime];
			hasFinished = YES;
		}

	} else
	{

		numberOfStopUpdates = [[NSNumber numberWithInt:0] retain];
		numberOfRouteUpdates = [[NSNumber numberWithInt:0] retain];
		hasTicketOutletUpdates = NO;
		updates = nil;
		
		// clear the badge value
		[self.navigationController.tabBarItem setBadgeValue:nil];
		
		// are we under the more navigationcontroller?
		tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
		if ([[d.tabBar viewControllers] indexOfObject:self.navigationController] >= 4)
		{
			// we're in the more one, make sure to badge it too
			[[[d.tabBar moreNavigationController] tabBarItem] setBadgeValue:nil];
		}	

		// tell the app delegate to save this as our last synchronisation time
		[d setLastSyncDate:nil];
		hasFinished = YES;
	}
	
	isCheckingForUpdates = NO;
	[self showLoadingIndicator];
	
	if (hasLoaded)
	{
		[self.tableView reloadData];
		[self.tableView setTableFooterView:[self tableFooterView]];
		[self.tableView setTableHeaderView:[self tableHeaderView]];
	}
}
- (void)syncDidStart
{
	hasFinished = NO;
}
- (void)syncDidFinishAtTime:(NSDate *)finishTime
{
	hasFinished = YES;

	// clear the badge value
	[self.navigationController.tabBarItem setBadgeValue:nil];
	[self.tableView reloadData];
	[self.tableView setTableFooterView:[self tableFooterView]];
	[self.tableView setTableHeaderView:[self tableHeaderView]];
	[self showLoadingIndicator];
}
- (void)syncDidFailWithError:(NSError *)error
{
	// if its a connection error display the message
	if ([error code] == PIDServiceErrorNotReachable)
		errorMessage = [error localizedDescription];
	else
		errorMessage = NSLocalizedString(@"updates-unknown-error-check", nil);
	
	[errorMessage retain];
	[self checkForUpdatesDidFinish:nil];
}

- (BOOL)isActivityInProgress
{
	return isCheckingForUpdates || isSyncing;
}

- (void)showLoadingIndicator
{
	if ([self isActivityInProgress])
	{
		// set a loading thing
		UIActivityIndicatorView *loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
		[loading startAnimating];
		UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithCustomView:loading];
		[loading release];
		[self.navigationItem setRightBarButtonItem:button animated:YES];
		[button release];
	} else if ([HistoricalUpdate hasHistoricalUpdates])
	{
		UIBarButtonItem *historyButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"updates-history-button", nil)
																		  style:UIBarButtonItemStylePlain
																		 target:self
																		 action:@selector(showHistory)];
		[self.navigationItem setRightBarButtonItem:historyButton animated:YES];
		[historyButton release];
	} else
	{
		[self.navigationItem setRightBarButtonItem:nil animated:YES];
	}
}


- (void)viewDidLoad
{
	hasLoaded = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
	[self.tableView setTableFooterView:[self tableFooterView]];
	[self.tableView setTableHeaderView:[self tableHeaderView]];
}
- (void)viewDidAppear:(BOOL)animated
{
	hasAppeared = YES;
	[self showLoadingIndicator];
}

- (UIView *)tableFooterView
{
	// our footer view is just a button
	UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
	
	UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
	
	// button label depends on how far into it we are
	if ([self isActivityInProgress])
	{
		[button setTitle:NSLocalizedString(@"updates-cancel", nil) forState:UIControlStateNormal];
		[button addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
		[button setBackgroundImage:[UIImage imageNamed:@"RedButton.png"] forState:UIControlStateNormal];
	}
	else if ([self doesHaveUpdates] && !hasFinished)
	{
		[button setTitle:NSLocalizedString(@"updates-sync", nil) forState:UIControlStateNormal];
		[button addTarget:self action:@selector(synchronise) forControlEvents:UIControlEventTouchUpInside];
		[button setBackgroundImage:[UIImage imageNamed:@"GreenButton.png"] forState:UIControlStateNormal];
	} else
	{
		[button setTitle:NSLocalizedString(@"updates-check", nil) forState:UIControlStateNormal];
		[button addTarget:self action:@selector(checkForUpdates) forControlEvents:UIControlEventTouchUpInside];
		[button setBackgroundImage:[UIImage imageNamed:@"GreenButton.png"] forState:UIControlStateNormal];
	}
	
	[button.titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
	[button.titleLabel setShadowOffset:CGSizeMake(0, -1.0)];
	[button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[button setTitleShadowColor:[UIColor blackColor] forState:UIControlStateNormal];
	[button setFrame:CGRectMake(10, 10, 300, 44)];
	[footer addSubview:button];

	return [footer autorelease];
}

- (UIView *)tableHeaderView
{
	// do we already have updates? no message required
	if ([self doesHaveUpdates])
		return nil;

	// our header view is the info about updates
	NSString *message = errorMessage != nil ? errorMessage : [NSString stringWithFormat:NSLocalizedString(@"updates-message", nil), NSLocalizedString(@"updates-check", nil)];

	// have we already checked for updates but found none?
	if (numberOfStopUpdates != nil && numberOfRouteUpdates != nil && [numberOfStopUpdates integerValue] == 0 && [numberOfRouteUpdates integerValue] == 0)
		message = [NSString stringWithFormat:NSLocalizedString(@"updates-uptodate-message", nil), NSLocalizedString(@"updates-check", nil)];
	
	// find the size of the message
	CGSize sizeOfMessage = [message sizeWithFont:[UIFont systemFontOfSize:16] constrainedToSize:CGSizeMake(300, 400) lineBreakMode:UILineBreakModeWordWrap];

	// create the header and its label
	UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, sizeOfMessage.height+20)];
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, sizeOfMessage.height)];
	[label setText:message];
	[label setBackgroundColor:[UIColor clearColor]];
	[label setOpaque:NO];
	[label setTextColor:[UIColor blackColor]];
	[label setFont:[UIFont systemFontOfSize:16]];
	[label setNumberOfLines:0];
	[label setLineBreakMode:UILineBreakModeWordWrap];
	[header addSubview:label];
	[label release];
	return [header autorelease];
}

- (void)synchronise
{
	// pop up a modal view controller
	SyncController *sync = [[SyncController alloc] initWithNumberOfStops:numberOfStopUpdates numberOfRoutes:numberOfRouteUpdates shouldUpdateTicketOutlets:hasTicketOutletUpdates updateCache:updates];
	[sync setParentUpdatesController:self];
	UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:sync];
	[nav.navigationBar setTintColor:[UIColor colorWithRed:TTNavigationBarTintColorRed green:TTNavigationBarTintColorGreen blue:TTNavigationBarTintColorBlue alpha:1.0]];
	[sync release];
    [self.navigationController presentViewController:nav animated:YES completion:NULL];
	[nav release];
}


- (void)checkForUpdates
{
	// clear out existing numbers if there are any
	[numberOfStopUpdates release]; numberOfStopUpdates = nil;
	[numberOfRouteUpdates release]; numberOfRouteUpdates = nil;
	hasTicketOutletUpdates = NO;
	[ticketOutletFileSize release]; ticketOutletFileSize = nil;
	[updates release]; updates = nil;

	if (syncManager != nil)
	{
		[syncManager release];
		syncManager = nil;
	}
	
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	syncManager = [[BackgroundSynchroniser alloc] initWithLastSynchronisationDate:[d lastSyncDate] ticketFileDate:[d lastTicketSyncDate] persistentStoreCoordinator:[d persistentStoreCoordinator]];
	[syncManager setDelegate:self];
	
	[syncManager performSelectorInBackground:@selector(checkForUpdatesInBackgroundThread:) withObject:nil];
}

- (void)cancel
{
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (BOOL)doesHaveUpdates
{
	if ((numberOfStopUpdates != nil && [numberOfStopUpdates integerValue] > 0) ||
		(numberOfRouteUpdates != nil && [numberOfRouteUpdates integerValue] > 0) || hasTicketOutletUpdates)
		return YES;
	return NO;
}

- (void)showHistory
{
	HistoricalUpdatesController *history = [[HistoricalUpdatesController alloc] initHistoricalUpdatesController];
	[self.navigationController pushViewController:history animated:YES];
	[history release];
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	// do we have anything?
	return [self doesHaveUpdates] ? (hasFinished ? 3 : 4) : 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	if ([self tableView:tableView numberOfRowsInSection:section] > 0)
		return NSLocalizedString((hasFinished ? @"updates-section-title-completed" : @"updates-section-title"), nil);
	return nil;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
    }
    
	if (indexPath.row == 0)
	{
		if (hasFinished)
			[cell.textLabel setText:NSLocalizedString(@"updates-stops-updated", nil)];
		else
			[cell.textLabel setText:NSLocalizedString(@"updates-stops-tobe-updated", nil)];

		if (numberOfStopUpdates == nil)
			[cell.detailTextLabel setText:@"0"];
		else
			[cell.detailTextLabel setText:[numberOfStopUpdates stringValue]];

	} else if (indexPath.row == 1)
	{
		if (hasFinished)
			[cell.textLabel setText:NSLocalizedString(@"updates-routes-updated", nil)];
		else
			[cell.textLabel setText:NSLocalizedString(@"updates-routes-tobe-updated", nil)];

		if (numberOfRouteUpdates == nil)
			[cell.detailTextLabel setText:@"0"];
		else
			[cell.detailTextLabel setText:[numberOfRouteUpdates stringValue]];

	} else if (indexPath.row == 2)
	{
		if (hasFinished)
			[cell.textLabel setText:NSLocalizedString(@"updates-tickets-updated", nil)];
		else
			[cell.textLabel setText:NSLocalizedString(@"updates-tickets-tobe-updated", nil)];
		
		[cell.detailTextLabel setText:(hasTicketOutletUpdates ? NSLocalizedString(@"updates-tickets-yes", nil) : NSLocalizedString(@"updates-tickets-no", nil))];
		
	} else if (indexPath.row == 3)
	{
		[cell.textLabel setText:NSLocalizedString(@"updates-size-updates", nil)];
		
		// work out the approximate size
		CGFloat size = (([numberOfStopUpdates doubleValue] * STOP_UPDATE_BYTES) + ([numberOfRouteUpdates doubleValue] * ROUTE_UPDATES_BYTES)) / 1024;
		
		// ticket outlets?
		if (hasTicketOutletUpdates && [ticketOutletFileSize integerValue] != 0)
			size += ([ticketOutletFileSize doubleValue] / 1024);

		[cell.detailTextLabel setText:[NSString stringWithFormat:@"%.2f KB", size]];
	}
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
	// AnotherViewController *anotherViewController = [[AnotherViewController alloc] initWithNibName:@"AnotherView" bundle:nil];
	// [self.navigationController pushViewController:anotherViewController];
	// [anotherViewController release];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


- (void)dealloc {
	[numberOfStopUpdates release];
	[numberOfRouteUpdates release];
	[updates release];
	[errorMessage release];
	[ticketOutletFileSize release];
    [super dealloc];
}


@end

