//
//  VoiceOverButton.h
//  tramTRACKER
//
//  Created by Robert Amos on 13/10/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Prediction;


@interface VoiceOverButton : UIButton {
	Prediction *prediction;
}

@property (nonatomic, retain) Prediction *prediction;

@end
