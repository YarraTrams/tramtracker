//
//  RouteList.h
//  tramTRACKER
//
//  Created by Robert Amos on 6/04/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Route.h"


@interface RouteList : NSObject {
	/**
	 * The singleton instance of ourselves
	 **/
	RouteList *sharedRouteList;
}

/**
 * Singleton manager. Call [StopList sharedManager] to get a copy of the singleton
 **/
+ (RouteList *)sharedManager;

/**
 * Get a copy of the route list, this is the public routes only
**/
- (NSArray *)getRouteList;
- (NSArray *)getAllRouteList;

/**
 * Get the destination for a particular route and direction
**/
- (NSString *)destinationForRouteNumber:(NSString *)routeNumber isUpDirection:(BOOL)upDirection;

/**
 * Get the destination for a particular route and stop
**/
- (NSString *)destinationForRouteNumber:(NSString *)routeNumber trackerID:(NSNumber *)trackerID;

- (Route *)routeForRouteNumber:(NSString *)routeNumber;
- (Route *)routeForRouteNumber:(NSString *)routeNumber headboardRouteNumber:(NSString *)headboardRouteNumber;

@end
