//
//  NearbyListController.m
//  tramTRACKER
//
//  Created by Robert Amos on 4/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "NearbyListController.h"
#import <objc/runtime.h>
#import "SimpleCell.h"

#define NEARBY_THRESHOLD_200M 7
#define NEARBY_THRESHOLD_500M 14
#define NEARBY_THRESHOLD_MAX 18

@implementation NearbyListController

@synthesize stopList, locationManager, accuracyLabel, locatingStarted, nearestButton, locatingImage, nearestLabel, nearbyViewController;

- (id)initNearbyListWithNearbyViewController:(NearbyViewController *)pVC
{
	if ((self = [self initWithStyle:UITableViewStylePlain]))
	{
		// set the parent view controller, we deliberately don't retain this.
		nearbyViewController = pVC;

		sameStopResult = 0;
		
		// the view has loaded, fire up the location manager to find our nearest stop
		LocationManager *manager = [[LocationManager alloc] init];
		[self setLocationManager:manager];
		[manager release];
		
		[self.locationManager setDelegate:self];
		[self.locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
	}
	return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
	
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

	// Reset our background colour
	[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
	[self.tableView setTableFooterView:[self tableFooterView]];
}

// Control the location manager based on when we can be seen
- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	
	if (self.stopList == nil || [self.stopList count] == 0)
	{
		[self refreshLocation];
	}
	[self.nearbyViewController.navigationItem setLeftBarButtonItem:nil];
}
- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
	[self.locationManager stopUpdatingLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}


//
// Create the table footer
//
- (UIView *)tableFooterView
{
	CGRect frame = CGRectMake(0, 0, self.tableView.frame.size.width, 150);
	UnderBackgroundView *ub = [[UnderBackgroundView alloc] initWithFrame:frame];
	
	// if there are no location services just display an error.
	if (![self.locationManager locationServicesEnabled])
	{
		// Text to say you can touch the tab again
		UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 70, 280, 44)];
		[label setText:NSLocalizedString(@"nearby-corelocation-required", @"Core Location Required")];
		[label setTextColor:[UIColor darkGrayColor]];
		[label setFont:[UIFont systemFontOfSize:14]];
		[label setTextAlignment:UITextAlignmentCenter];
		[label setNumberOfLines:0];
		[label setOpaque:NO];
		[label setBackgroundColor:[UIColor clearColor]];
		[ub addSubview:label];
		[label release];
	} else
	{
		// locating image
        UIImageView *image = nil;
        
        if ([[UIScreen mainScreen] bounds].size.height == 568) {
            image = [[UIImageView alloc] initWithFrame:CGRectMake(130, 168, 60, 60)];
        }
        else{
            image = [[UIImageView alloc] initWithFrame:CGRectMake(130, 75, 60, 60)];
        }
		//UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(130, 75, 60, 60)];

		[image setAnimationImages:[NSArray arrayWithObjects:[UIImage imageNamed:@"radar-frame1.png"], [UIImage imageNamed:@"radar-frame2.png"],
								   [UIImage imageNamed:@"radar-frame3.png"], [UIImage imageNamed:@"radar-frame4.png"],
								   [UIImage imageNamed:@"radar-frame5.png"], [UIImage imageNamed:@"radar-frame6.png"],
								   [UIImage imageNamed:@"radar-frame7.png"], [UIImage imageNamed:@"radar-frame8.png"],
								   [UIImage imageNamed:@"radar-frame9.png"], [UIImage imageNamed:@"radar-frame10.png"],
								   [UIImage imageNamed:@"radar-frame11.png"], [UIImage imageNamed:@"radar-frame12.png"],
								   [UIImage imageNamed:@"radar-frame13.png"], [UIImage imageNamed:@"radar-frame14.png"],
								   [UIImage imageNamed:@"radar-frame15.png"], [UIImage imageNamed:@"radar-frame16.png"],
								   [UIImage imageNamed:@"radar-frame17.png"], [UIImage imageNamed:@"radar-frame18.png"],
								   [UIImage imageNamed:@"radar-frame19.png"], [UIImage imageNamed:@"radar-frame20.png"],
								   [UIImage imageNamed:@"radar-frame21.png"], [UIImage imageNamed:@"radar-frame22.png"],
								   [UIImage imageNamed:@"radar-frame23.png"], [UIImage imageNamed:@"radar-frame24.png"],
								   [UIImage imageNamed:@"radar-frame25.png"], [UIImage imageNamed:@"radar-frame26.png"],
								   [UIImage imageNamed:@"radar-frame27.png"], [UIImage imageNamed:@"radar-frame28.png"],
								   [UIImage imageNamed:@"radar-frame29.png"], [UIImage imageNamed:@"radar-frame30.png"],
								   nil]];
		[image setAnimationDuration:2];
		[ub addSubview:image];
		[self setLocatingImage:image];
		[image release];
		
		UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 5, 280, 40)];
		[label setHidden:YES];
		[label setTextColor:[UIColor darkGrayColor]];
		[label setFont:[UIFont systemFontOfSize:14]];
		[label setTextAlignment:UITextAlignmentCenter];
		[label setNumberOfLines:0];
		[label setOpaque:NO];
		[label setBackgroundColor:[UIColor clearColor]];
		[ub addSubview:label];
		[self setAccuracyLabel:label];
		[label release];
	}
	return [ub autorelease];
}

//- (UIView *)tableHeaderView;

- (void)refreshLocation
{
	[self setStopList:nil];

	// if we're allowed
	if ([self.locationManager locationServicesEnabled])
	{
		[self.locationManager startUpdatingLocation];

		if (self.nearbyViewController != nil)
			[self.nearbyViewController setListViewRefreshing:YES];

		[self.nearestButton setHidden:YES];
		[self.nearestLabel setHidden:YES];
		[self.accuracyLabel setHidden:YES];
		[self setLocatingStarted:[NSDate date]];

		// show the animation
		[self.locatingImage startAnimating];
		[self.locatingImage setHidden:NO];
		
		// set a timer to display results
		[NSTimer scheduledTimerWithTimeInterval:NEARBY_THRESHOLD_200M target:self selector:@selector(displayStopsFromTimer:) userInfo:nil repeats:NO];
		[NSTimer scheduledTimerWithTimeInterval:NEARBY_THRESHOLD_500M target:self selector:@selector(displayStopsFromTimer:) userInfo:nil repeats:NO];
		[NSTimer scheduledTimerWithTimeInterval:NEARBY_THRESHOLD_MAX target:self selector:@selector(displayStopsFromTimer:) userInfo:nil repeats:NO];

		// fade the nearest stop button out
		if (self.nearestButton != nil)
			[self.nearestButton setEnabled:NO];
	}
}
#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (!self.stopList)
		return 0;
	return [self.stopList count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
	static NSString *CellIdentifier = @"NearbyCell";

	NearbyCell *cell = (NearbyCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
        cell = [[[NearbyCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		
		[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
	}
	
	// Set up the cell...
	StopDistance *stop = [self.stopList objectAtIndex:[indexPath indexAtPosition:1]];

	// set the distance to the stop
	[cell.distance setText:[stop formattedDistance]];

	// and the stop name
	[cell.name setText:[stop.stop formattedName]];
	
	// and the route description
	[cell.routeDescription setText:[stop.stop formattedRouteDescription]];

	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	StopDistance *stop = [stopList objectAtIndex:[indexPath indexAtPosition:1]];
	[self pushPIDForStop:stop.stop];
}

- (void)pushPIDForStop:(Stop *)stop
{
	// Start the pid view
	PIDViewController *PID = nil;
    if ([[UIScreen mainScreen] bounds].size.height == 568) {
        PID = [[PIDViewController alloc] initWithNibName:@"PIDView-iPhone5" bundle:[NSBundle mainBundle]];
    }
    else{
        PID = [[PIDViewController alloc] initWithNibName:@"PIDView" bundle:[NSBundle mainBundle]];
    }
	
	// get the stop info
	[PID startWithStop:stop];
	
	// push it onto the stack
	if (self.nearbyViewController != nil)
	{
		[self.nearbyViewController.navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
		[self.nearbyViewController.navigationController pushViewController:PID animated:YES];
	} else
	{
		[self.navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
		[self.navigationController pushViewController:PID animated:YES];
	}
	[PID release];
}

- (void)pushPIDForStop:(Stop *)stop withFilter:(Filter *)filter animated:(BOOL)animated
{
	// Start the pid view
	PIDViewController *PID = nil;
    if ([[UIScreen mainScreen] bounds].size.height == 568) {
        PID = [[PIDViewController alloc] initWithNibName:@"PIDView-iPhone5" bundle:[NSBundle mainBundle]];
    }
    else{
        PID = [[PIDViewController alloc] initWithNibName:@"PIDView" bundle:[NSBundle mainBundle]];
    }
	
	// get the stop info
	[PID setFilter:filter];
	[PID startWithStop:stop];
	
	// push it onto the stack
	if (self.nearbyViewController != nil)
	{
		[self.nearbyViewController.navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
		[self.nearbyViewController.navigationController pushViewController:PID animated:animated];
	} else
	{
		[self.navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
		[self.navigationController pushViewController:PID animated:animated];
	}
	[PID release];
}

- (void)pushPIDForStop:(Stop *)stop withFilter:(Filter *)filter
{
	[self pushPIDForStop:stop withFilter:filter animated:YES];
}


- (void)setStopList:(NSArray *)list
{
	[list retain];
	[stopList release];
	stopList = list;
	
	[self.tableView reloadData];
}

// Flip over to show a map view
/*- (void)flipToMapView
{
	// build a stop list based on ourselves
	NSMutableArray *stops = [[NSMutableArray alloc] initWithCapacity:0];
	for (StopDistance *sd in self.stopList)
		[stops addObject:sd.stop];
	
	MapViewController *mapView = [[MapViewController alloc] initWithStopList:stops];
	[stops release];
	
	[mapView setListViewType:TTMapViewListTypeNearbyList];
	UISegmentedControl *segment = (UISegmentedControl *)self.navigationItem.titleView;
	[segment removeTarget:self action:@selector(flipToMapView) forControlEvents:UIControlEventValueChanged];
	[segment addTarget:mapView action:@selector(flipToListView) forControlEvents:UIControlEventValueChanged];
	[mapView.navigationItem setTitleView:segment];
	
	// flip over to the map view
	//[UIView beginAnimations:@"FlipToMapView" context:nil];
	[[self.navigationController.view.subviews objectAtIndex:0] setBackgroundColor:[UIColor blackColor]];
	//[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:[self.navigationController.view.subviews objectAtIndex:0] cache:YES];
	//[UIView setAnimationDuration:1];
	
	UINavigationController *nav = self.navigationController;
	
	// reset the view controllers array
	if ([[nav.viewControllers objectAtIndex:0] isEqual:[self.tabBarController.moreNavigationController.viewControllers objectAtIndex:0]])
		[nav setViewControllers:[NSArray arrayWithObjects:[nav.viewControllers objectAtIndex:0], mapView, nil]];
	else
		[nav setViewControllers:[NSArray arrayWithObject:mapView] animated:NO];
	
	//[UIView commitAnimations];
	
	[mapView release];
} */

//
// Support for pushing a PID with the nearby mode enabled
//
- (void)pushPIDWithNearbyAnimated:(BOOL)animated
{
	if (self.stopList == nil)
		return;
	// do we have a pid instance yet?
	PIDViewController *PID = nil;
    if ([[UIScreen mainScreen] bounds].size.height == 568) {
        PID = [[PIDViewController alloc] initWithNibName:@"PIDView-iPhone5" bundle:[NSBundle mainBundle]];
    }
    else{
        PID = [[PIDViewController alloc] initWithNibName:@"PIDView" bundle:[NSBundle mainBundle]];
    }
    
	StopDistance *stop = [self.stopList objectAtIndex:0];
	[PID startWithStop:stop.stop];
	
	// push it onto the stack
	[self.navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
	[self.navigationController pushViewController:PID animated:animated];
	[PID release];
}
- (void)pushPIDWithNearbyAnimated
{
	[self pushPIDWithNearbyAnimated:YES];
}

//
// The location Service has told us there was an error
//
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
	// if they've clicked "no" and we're not permitted to update, fail over to showing the default stop
	[manager stopUpdatingLocation];
	
	if (self.nearbyViewController != nil)
		[self.nearbyViewController setListViewRefreshing:NO];

	// hide the animation
	[self.locatingImage stopAnimating];
	[self.locatingImage setHidden:YES];
	
	//NSLog(@"%@", error);

	UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"nearby-error-cannotlocate", @"Cannot Locate")
															 delegate:self
													cancelButtonTitle:nil
											   destructiveButtonTitle:NSLocalizedString(@"nearby-error-okbutton", @"OK")
													otherButtonTitles:nil];
	[actionSheet showFromTabBar:self.nearbyViewController.navigationController.tabBarController.tabBar];
	[actionSheet release];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
	// has the location actually changed?
	//NSLog(@"Received location update.");
	

	// the location manager might have a more recent location
	if ([manager.location.timestamp isEqualToDate:[manager.location.timestamp laterDate:newLocation.timestamp]])
		newLocation = manager.location;

	// is it more than 2 minutes old?
	if (fabs([newLocation.timestamp timeIntervalSinceNow]) > 120)
	{
		//NSLog(@"Discarding location more than 2 minutes old");
		return;
	}
	
	// What sort of accuracy do we need for here?
	NSTimeInterval secondsPassed = [[NSDate date] timeIntervalSinceDate:self.locatingStarted];
	NSInteger requiredAccracy = secondsPassed > NEARBY_THRESHOLD_500M ? 500 : (secondsPassed > NEARBY_THRESHOLD_200M ? 200 : 50);

	// check this location for accuracy
	if (newLocation.horizontalAccuracy <= requiredAccracy)
	{
		//NSLog(@"Required accuracy of %dm has been met (%.2fm). Using location %@", requiredAccracy, newLocation.horizontalAccuracy, newLocation);

		// display the stops
		[self displayStopsWithLocation:newLocation];
		[self.locationManager stopUpdatingLocation];

		// finished locating
		if (self.nearbyViewController != nil)
			[self.nearbyViewController setListViewRefreshing:NO];

	} else
	{
		//NSLog(@"Ignoring location %@ because accuracy %.2fm does not meet required accuracy of %dm", newLocation, newLocation.horizontalAccuracy, requiredAccracy);
	}
}
- (void)displayStopsWithLocation:(CLLocation *)location
{
	// stopped refreshing
	if (self.nearbyViewController != nil)
		[self.nearbyViewController setListViewRefreshing:NO];

	[self.nearestButton setEnabled:YES];
	[self.nearestButton setHidden:NO];
	[self.nearestLabel setHidden:NO];
	
	// hide the animation
	[self.locatingImage stopAnimating];
	[self.locatingImage setHidden:YES];
	

	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSNumber *stopCount = [defaults objectForKey:@"nearbyStopCount"];
	if (!stopCount) stopCount = [NSNumber numberWithInt:10];
	
	// find the nearest stops
	NSArray *nearestStops = [[StopList sharedManager] getNearestStopsToLocation:location count:[stopCount intValue]];
	[self setStopList:nearestStops];
	
	// set the accracy label
	if ([self.locationManager.location horizontalAccuracy] == 0)
		[self.accuracyLabel setHidden:YES];
	else
	{
		[self.accuracyLabel setHidden:NO];
		[self.accuracyLabel setText:[NSString stringWithFormat:NSLocalizedString(@"nearby-location-accuracy", @"Accuracy to within"), [location horizontalAccuracy]]];
	}
}
- (void)displayStopsFromTimer:(NSTimer *)aTimer
{
	// get the current location
	if (self.locationManager == nil || self.locationManager.location == nil || [self.locationManager stopped] || ![self.locationManager locationServicesEnabled])
	{
		// some error handling
		if (self.nearbyViewController != nil)
			[self.nearbyViewController setListViewRefreshing:NO];

		return;
	}

	// have we reached a threshold?
	// What sort of accuracy do we need for here?
	NSTimeInterval secondsPassed = [[NSDate date] timeIntervalSinceDate:self.locatingStarted];
	NSInteger requiredAccracy = secondsPassed > NEARBY_THRESHOLD_500M ? 500 : (secondsPassed > NEARBY_THRESHOLD_200M ? 200 : 50);
	
	if (secondsPassed >= NEARBY_THRESHOLD_MAX || self.locationManager.location.horizontalAccuracy <= requiredAccracy)
	{
		//NSLog(@"Timeout has been reached, using location %@", self.locationManager.location);
		[self displayStopsWithLocation:self.locationManager.location];
		[self.locationManager stopUpdatingLocation];
	}
}

- (void)dealloc {
	// release properties
	[locationManager release];
	[stopList release];
	[locatingStarted release];
	
    [super dealloc];
}


@end

