//
//  StopDeleteOperation.h
//  tramTRACKER
//
//  Created by Robert Amos on 7/08/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>


@class Stop;
@class BackgroundSynchroniser;

/**
 * A delete operation for a stop
 *
 * @ingroup Sync
**/
@interface StopDeleteOperation : NSOperation {

	/**
	 * The stop to be deleted
	**/
	Stop *stop;

	/**
	 * The BackgroundSynchroniser object that is managing this update operation
	 **/
	BackgroundSynchroniser *syncManager;
	
	/**
	 * Status - YES if the operation is executing, NO otherwise
	 **/
	BOOL executing;
	
	/**
	 * Status - YES if the operation was finished (or cancelled), NO otherwise
	 **/
	BOOL finished;
}

@property (nonatomic, readonly) Stop *stop;
@property (nonatomic, assign) BackgroundSynchroniser *syncManager;

/**
 * Initialises a stop delete operation for the specified stop.
 *
 * @param	aStop			The stop to be deleted.
 * @return					An initialised StopDeleteOperation object
**/
- (id)initWithStop:(Stop *)aStop;

/**
 * Required by the NSOperationQueue, whether to execute this operation concurrently.
 **/
- (BOOL)isConcurrent;

/**
 * NSOperationQueue wrapper for the executing ivar.
 **/
- (BOOL)isExecuting;

/**
 * NSOperationQueue wrapper for the finished ivar.
 **/
- (BOOL)isFinished;

/**
 * Starts the Stop Delete Operation
 **/
- (void)start;

/**
 * Finishes the Update operation and handles notifications
 **/
- (void)finish;

/**
 * Deletes the stop from the current context
**/
- (void)deleteStop;

@end

