//
//  UpdatesController.h
//  tramTRACKER
//
//  Created by Robert Amos on 3/08/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BackgroundSynchroniserDelegate.h"

@class BackgroundSynchroniser;


@interface UpdatesController : UITableViewController <BackgroundSynchroniserDelegate> {
	BOOL hasLoaded;
	BOOL hasAppeared;
	BOOL isCheckingForUpdates;
	BOOL isSyncing;
	BOOL hasFinished;
	BOOL hasTicketOutletUpdates;
	NSNumber *ticketOutletFileSize;
	NSNumber *numberOfStopUpdates;
	NSNumber *numberOfRouteUpdates;
	NSArray *updates;
	NSString *errorMessage;
	BackgroundSynchroniser *syncManager;
}

- (id)initUpdatesController;
- (BOOL)doesHaveUpdates;
- (UIView *)tableFooterView;
- (UIView *)tableHeaderView;
- (BOOL)hasFinished;
- (BOOL)hasAppeared;

- (void)synchronise;
- (void)checkForUpdates;
- (void)cancel;
- (BOOL)isActivityInProgress;
- (void)showLoadingIndicator;

- (void)checkForUpdatesDidStart;
- (void)checkForUpdatesDidFinish:(NSDictionary *)updatesFound;
- (void)syncDidStart;
- (void)syncDidFinishAtTime:(NSDate *)finishTime;
- (void)showHistory;

@end
