//
//  NSDictionary+SafeAccess.m
//  tramTRACKER
//
//  Created by Tom King on 13/11/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "NSDictionary+SafeAccess.h"

@implementation NSDictionary (SafeAccess)

- (id)valueForKey:(NSString *)key
         ifKindOf:(Class)class
     defaultValue:(id)defaultValue
{
    id obj = [self objectForKey:key];
    return [obj isKindOfClass:class] && obj ? obj : defaultValue;
}

@end
