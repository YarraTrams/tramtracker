//
//  TTTools.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 21/11/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "TTTools.h"
#import "Constants.h"
#import "UIDevice-Hardware.h"

@implementation TTTools

+ (NSString *)voiceOverMinutesUntilArrivalTime:(NSDate *)arrivalTime
{
	NSTimeInterval secondsUntilArrivalTime = [arrivalTime timeIntervalSinceDate:[NSDate date]];
	
	// Work out the number of minutes to go
	int minutes = floor(secondsUntilArrivalTime / 60);
	
	// If we're less than a minute (but not more than a minute past) return now
	if (minutes >= -1 && minutes < 1)
		return NSLocalizedString(@"pid-now", "now");
	
	else if (minutes == 1)
		return [NSString stringWithFormat:@"in %d minute", minutes];
    
	// Or if less than 60 minutes from now return the number
	else if (minutes > 1 && minutes < 61)
		return [NSString stringWithFormat:@"in %d minutes", minutes];
	
	// Have we gone more than 60 seconds past this one?
	else if (minutes < -1)
		return NSLocalizedString(@"pid-tramnotarriving", "--");
	
	// More than 60 seconds mean we switch over to using the exact arrival time
	else
	{
		// Find the date
		NSDateFormatter     * formatter = [[NSDateFormatter alloc] init];
		
		// Use the current calendar to match days
		NSCalendar          * cal = [NSCalendar currentCalendar];
		NSDate              * today = [NSDate date];
		NSDateComponents    * todayComponents = [cal components:(NSCalendarUnitDay |
                                                                 NSCalendarUnitWeekday |
                                                                 NSCalendarUnitYear)
                                                       fromDate:today];

		NSDateComponents    * arrivalComponents = [cal components:(NSCalendarUnitDay |
                                                                   NSCalendarUnitWeekday |
                                                                   NSCalendarUnitYear)
                                                         fromDate:arrivalTime];

		NSString            * formattedArrivalTime;
        
		// If its in the year 9999 then it means there is an error, display dashes
		if (arrivalComponents.year >= 9999)
		{
			return NSLocalizedString(@"pid-tramnotarriving", "--");
		}

		// Check the weekday, if its arriving on a different day then we add the day name.
		if (todayComponents.weekday != arrivalComponents.weekday)
		{
			[formatter setDateFormat:@"'on' cccc 'at' hh:mm a"];
			formattedArrivalTime = [formatter stringFromDate:arrivalTime];
		}
        else
		{
			[formatter setTimeStyle:NSDateFormatterShortStyle];
			[formatter setDateStyle:NSDateFormatterNoStyle];
			formattedArrivalTime = [NSString stringWithFormat:@"at %@", [formatter stringFromDate:arrivalTime]];
		}
		return formattedArrivalTime;
	}
}

+ (NSInteger)requiredAccuracy:(NSTimeInterval)secondsPassed
{
    if (secondsPassed > kNearby_Threshold_500M)
        return 500;
    else if (secondsPassed > kNearby_Threshold_200M)
        return 200;
    
    return 50;
}

+ (NSInteger)maxStopsCount
{
    UIDevicePlatform platform = (UIDevicePlatform)[[UIDevice currentDevice] platformType];
    switch (platform)
    {
        case UIDevice1GiPhone:
        case UIDevice1GiPod:
        case UIDevice2GiPod:
        case UIDevice3GiPhone:
        case UIDevice3GSiPhone:
            return 20;
        default:
            return 100;
    }
}

@end
