//
//  UIView+EBFrame.h
//  EBFoundationKit_Example
//
//  Created by Eike Bartels on 20.06.13.
//  Copyright (c) 2013 Eike T. Bartels. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (EBFrame)

@property (nonatomic,assign) int x;
@property (nonatomic,assign) int y;
@property (nonatomic,assign) int width;
@property (nonatomic,assign) int height;
@property (nonatomic,assign) CGSize size;
@property (nonatomic,assign) CGPoint position;

@end
