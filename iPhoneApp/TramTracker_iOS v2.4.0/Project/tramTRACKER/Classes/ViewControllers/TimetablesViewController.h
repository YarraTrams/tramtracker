//
//  TimetablesViewController.h
//  tramTRACKER
//
//  Created by Raji on 24/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomIOSAlertView.h"

@interface DeparturesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel* label;
@property (weak, nonatomic) IBOutlet UITextField* textField;
@end

@interface TimetablesViewController : UIViewController<UIAlertViewDelegate>

- (void)setCurrentRoute:(Route *)aRoute;
- (void)setStop:(Stop *)aStop direction:(BOOL)upDirection isTerminus:(BOOL)isTerminus;

@end
