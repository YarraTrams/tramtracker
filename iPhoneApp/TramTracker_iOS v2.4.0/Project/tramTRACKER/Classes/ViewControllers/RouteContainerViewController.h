//
//  RouteContainerViewController.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 11/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RouteContainerViewController : UIViewController

- (void)setRoute:(Route *)aRoute direction:(BOOL)upDirection isTimeTables:(BOOL)isTimetables;

@end
