//
//  NeabyRightViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 21/11/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "StopRightFilterViewController.h"
#import "SectionHeaderView.h"
#import "RightMenuDelegate.h"
#import "SWRevealViewController.h"
#import "Constants.h"
#import "Analytics.h"

#import "NearbyViewController.h"
#import "RouteContainerViewController.h"

@interface StopRightFilterViewController()<UITableViewDataSource, UITableViewDelegate>

typedef enum
{
    StopRightFilterViewNearby,
    StopRightFilterViewRoutes
}   StopRightFilterViewType;

@property NSUInteger    selectedIndex;

@property (assign, nonatomic) BOOL hasClicked;

@property (weak, nonatomic) SWRevealViewController  * revealViewController;
@property StopRightFilterViewType       type;

@end

@implementation StopRightFilterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.revealViewController = (id)self.tabBarController.parentViewController.parentViewController;
    
    if (self.type == StopRightFilterViewRoutes)
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didReceiveNotificationReset)
                                                     name:kNotificationRoutesResetRight
                                                   object:nil];
    
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    self.tableView.delegate   = nil;
    self.tableView.dataSource = nil;
}

- (void)didReceiveNotificationReset
{
    self.selectedIndex = 0;
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                animated:NO
                          scrollPosition:UITableViewScrollPositionNone];
}

- (void)viewWillAppear:(BOOL)animated
{
    
self.hasClicked = NO;
    
    [super viewWillAppear:animated];
    
    if (![self.tableView indexPathForSelectedRow])
    {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedIndex inSection:0]
                                    animated:NO
                              scrollPosition:UITableViewScrollPositionNone];
    }
}

#pragma mark - UITableViewDataSource

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString            * title = !section ? @"Filters" : @"Actions";
    return [SectionHeaderView rightSectionHeaderViewWithTitle:title];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 29.0f;
}

+ (NSString *)sectionHeaderTitleForType:(StopRightMenuType)filterType
{
    switch (filterType)
    {
        case StopRightMenuShelter:
            return @"Stops with shelter";
            
        case StopRightMenuAccess:
            return @"Easy access stops";
            
        case StopRightMenuOutlets:
            return @"Ticket outlets";
            
        default:
            return @"All stops";
    }
}

+ (NSArray *)filterDistanceArray:(NSArray *)items filterType:(StopRightMenuType)filterType string:(NSString *)filterString
{
    NSPredicate *predicate = nil;
    
    if (filterString.length)
    {
        for (NSString * filterWord in [filterString componentsSeparatedByString:@" "])
        {
            if ([filterWord isEqualToString:@""])
                continue;
            
            if (filterType == StopRightMenuOutlets)
                predicate = [NSPredicate predicateWithFormat:@"(retailer.name CONTAINS[c] %@) OR (retailer.address CONTAINS[x] %@) OR (retailer.suburb CONTAINS %@)", filterString, filterString, filterString];
            else
                predicate = [NSPredicate predicateWithFormat:@"(stop.name CONTAINS[c] %@) OR (stop.number LIKE %@)", filterWord, filterWord];
        }
    }
    return predicate ? [items filteredArrayUsingPredicate:predicate] : items;
}

+ (NSArray *)filterArray:(NSArray *)items filterType:(StopRightMenuType)filterType string:(NSString *)filterString
{
    NSMutableArray  * compoundPredicate = [NSMutableArray new];
    
    /* Add predicate part for filterType */
    switch (filterType) {
        case StopRightMenuShelter:
            [compoundPredicate addObject:[NSPredicate predicateWithFormat:@"(isShelterStop = YES) OR (isYtShelterStop = YES)"]];
            break;
            
        case StopRightMenuAccess:
            [compoundPredicate addObject:[NSPredicate predicateWithFormat:@"isEasyAccessStop = YES"]];
            break;
            
        default:
            break;
    }
    
    /* Add predicate part for filterString */
    if (filterString.length)
    {
        for (NSString * filterWord in [filterString componentsSeparatedByString:@" "])
        {
            if ([filterWord isEqualToString:@""])
                continue;
            
            if (filterType == StopRightMenuOutlets)
                [compoundPredicate addObject:[NSPredicate predicateWithFormat:@"(name CONTAINS[c] %@) OR (address CONTAINS[x] %@) OR (suburb CONTAINS %@)", filterString, filterString, filterString]];
            else
                [compoundPredicate addObject:[NSPredicate predicateWithFormat:@"(name CONTAINS[c] %@) OR (number LIKE %@)", filterWord, filterWord]];
        }
    }
    
    return compoundPredicate.count ? [items filteredArrayUsingPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:compoundPredicate]] : items;
}

- (void)saveAnalytics:(id<StopRightMenuDelegate>)vc
{
    AnalyticsFeature    feature = AnalyticsFeatureNearbyList;
    UITabBarController  * tabbar = ((UIViewController *)vc).childViewControllers.firstObject;
    
    if ([vc isKindOfClass:[RouteContainerViewController class]])
    {
        if (!tabbar.selectedIndex)
            feature = AnalyticsFeatureRoutesDetailedList;
        else
            feature = AnalyticsFeatureRoutesDetailedMap;
    }
    else if ([vc isKindOfClass:[NearbyViewController class]])
    {
        if (!tabbar.selectedIndex)
            feature = AnalyticsFeatureNearbyList;
        else
            feature = AnalyticsFeatureNearbyMap;
    }
    
    AnalyticsFilter filter = AnalyticsFilterAll;
    
    if (self.selectedIndex == StopRightMenuShelter)
        filter = AnalyticsFilterShelter;
    else if (self.selectedIndex == StopRightMenuAccess)
        filter = AnalyticsFilterEasyAccess;
    else if (self.selectedIndex == StopRightMenuOutlets)
        filter = AnalyticsFilterOutlets;
    
    [[Analytics sharedInstance] featureAccessed:feature withFilter:filter];
}

#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.hasClicked)
        return;
    
    UITabBarController              * tabbarController = (id)self.revealViewController.frontViewController;
    UINavigationController          * navigationController = (id)tabbarController.selectedViewController;
    id<StopRightMenuDelegate>       nearbyInitialViewController = (id)navigationController.topViewController;
    NSInteger                       row = indexPath.row;
    
    if (!indexPath.section)
    {
        [tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedIndex inSection:0] animated:YES];
        self.selectedIndex = indexPath.row;
        [self saveAnalytics:nearbyInitialViewController];
    }
    else
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        row += 4;
    }
    
    if ([nearbyInitialViewController respondsToSelector:@selector(didSelectStopAction:)])
    {
        self.hasClicked = YES;
        double delayInSeconds = 0.2f;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [nearbyInitialViewController didSelectStopAction:(StopRightMenuType)row];
        });
    }
    
}

@end
