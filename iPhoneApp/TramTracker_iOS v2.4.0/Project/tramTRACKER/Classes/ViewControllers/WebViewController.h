//
//  WebViewController.h
//  tramTRACKER
//
//  Created by Alex Louey on 17/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController

@property(nonatomic,strong) NSString * urlStr;

@end
