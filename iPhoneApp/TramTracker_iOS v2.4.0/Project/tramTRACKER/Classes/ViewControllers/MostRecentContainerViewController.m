//
//  MostRecentContainerViewController.m
//  tramTRACKER
//
//  Created by Raji on 7/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import "MostRecentContainerViewController.h"
#import "MostRecentViewController.h"
#import "MapViewController.h"
#import "PIDViewController.h"
#import "Constants.h"
#import "TTTabBarController.h"
#import "StopList.h"

@interface MostRecentContainerViewController ()

@property (strong, nonatomic) UITabBarController        * childTabBar;
@property (strong, nonatomic) MostRecentViewController  * mostRecentViewController;
@property (strong, nonatomic) MapViewController         * mapViewController;
@property (strong, nonatomic) NSArray                   * stopList;

@end

@implementation MostRecentContainerViewController

#pragma mark - Inits & Loads

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.childTabBar = self.childViewControllers[0];
    self.mostRecentViewController = self.childTabBar.childViewControllers.firstObject;
    self.mapViewController = self.childTabBar.childViewControllers.lastObject;
    self.navigationItem.rightBarButtonItem = nil;
}

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - User Actions on Nav Bar

- (IBAction)toggleListMapAction:(UIBarButtonItem *)sender
{
    TTTabBarController  * tabbarController = self.childViewControllers.firstObject;
    [tabbarController toggleListMapAction:sender isRight:NO];
}

#pragma mark - Segue Methods

- (void)pushToPID:(Stop *)stop
{
    UIStoryboard        * sb = [UIStoryboard storyboardWithName:kMainStoryboard bundle:nil];
    PIDViewController   * vc = [sb instantiateViewControllerWithIdentifier:kScreenPID];

    [vc setCurrentStop:stop];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
