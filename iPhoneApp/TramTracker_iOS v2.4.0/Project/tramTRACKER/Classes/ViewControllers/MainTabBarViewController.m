//
//  MainTabBarViewController.m
//  tramTracker
//
//  Created by Hugo Cuvillier on 24/03/2014.
//  Copyright (c) 2014 AppsCore. All rights reserved.
//

#import "MainTabBarViewController.h"
#import "FavouriteContainerViewController.h"

@interface MainTabBarViewController ()<UITabBarControllerDelegate>

@end

@implementation MainTabBarViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.delegate = self;
    }
    return self;
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {

    // Trying to reselect an already selected tab
    if (viewController == tabBarController.selectedViewController) {

        // In favourites
        if (![tabBarController.viewControllers indexOfObject:viewController]) {

            UINavigationController  * navigationController = (id)viewController;

            if (navigationController.viewControllers.count == 1) {

                FavouriteContainerViewController * favouritesTopVC = (id)navigationController.topViewController;
                [favouritesTopVC locateAndPushNearestFavourite];
                return NO;
            }
        }
    }
    return YES;
}

@end
