//
//  MoreViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 10/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "MoreViewController.h"
#import "Constants.h"
#import "BackgroundSynchroniser.h"
#import "WebViewController.h"
#import "Analytics.h"
#import "tramTRACKERAppDelegate.h"
#import "RoutesListViewController.h"
#import "FTZOverlay.h"

#import "OLRichInboxTileCardViewController.h"

typedef enum : NSUInteger {
    MoreViewControllerTrackerID,
    MoreViewControllerSearch,
    MoreViewControllerSettings,
    MoreViewControllerTicketOutlets,
    MoreViewControllerServiceUpdates,
    MoreViewControllerTwitter,
    MoreViewControllerPTV,
    MoreViewControllerTimetables,
    MoreViewControllerFeedback,
    MoreViewControllerHelp,
    MoreViewControllerFTZ,
    MoreViewControllerMostRecent,
    MoreViewControllerPrivacyPolicy,
    MoreViewControllerSpecialOffers = 9999,
} MoreViewControllerCellType;

@interface MoreViewController () <UIAlertViewDelegate>

@property (nonatomic) BOOL areUpdatesAvailable;

@property (strong, nonatomic) NSMutableArray        * observers;
@property (strong, nonatomic) NSURL                 * currentURL;

@property (nonatomic, weak) IBOutlet UITableView* tableView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint* tableToBottomConstraint;

@property (readonly, nonatomic) NSArray* cellIdentifiers;

@end

@implementation MoreViewController

- (NSArray *)cellIdentifiers
{
    if([AdManager adsEnabled])
    {
        return @[
            @(MoreViewControllerTrackerID),
            @(MoreViewControllerSearch),
            @(MoreViewControllerSettings),
            @(MoreViewControllerSpecialOffers),
            @(MoreViewControllerTicketOutlets),
            @(MoreViewControllerServiceUpdates),
            @(MoreViewControllerTwitter),
            @(MoreViewControllerPTV),
            @(MoreViewControllerTimetables),
            @(MoreViewControllerFeedback),
            @(MoreViewControllerHelp),
            @(MoreViewControllerFTZ),
            @(MoreViewControllerMostRecent),
            @(MoreViewControllerPrivacyPolicy),
        ];
    }
    else
    {
        return @[
            @(MoreViewControllerTrackerID),
            @(MoreViewControllerSearch),
            @(MoreViewControllerSettings),
            @(MoreViewControllerTicketOutlets),
            @(MoreViewControllerServiceUpdates),
            @(MoreViewControllerTwitter),
            @(MoreViewControllerPTV),
            @(MoreViewControllerTimetables),
            @(MoreViewControllerFeedback),
            @(MoreViewControllerHelp),
            @(MoreViewControllerFTZ),
            @(MoreViewControllerMostRecent),
            @(MoreViewControllerPrivacyPolicy),
        ];
    }
}

- (NSLayoutConstraint *)adSpacingConstraint
{ return self.tableToBottomConstraint; }

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureMore];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self showAds];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(respondToKillSwitchUpdate:) name:kNotificationKillSwitchUpdate object:nil];
}

- (void)dealloc
{
    self.tableView.dataSource = nil;
    self.tableView.delegate = nil;
    [self removeAds:NO];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (NSString*)cellIdentifierForType:(MoreViewControllerCellType)cellType
{
    switch(cellType)
    {
        case MoreViewControllerTrackerID      : return @"TrackerID";
        case MoreViewControllerSearch         : return @"Search";
        case MoreViewControllerFTZ            : return @"FTZ";
        case MoreViewControllerTicketOutlets  : return @"TicketOutlets";
        case MoreViewControllerServiceUpdates : return @"ServiceUpdates";
        case MoreViewControllerTwitter        : return @"Twitter";
        case MoreViewControllerPTV            : return @"PTV";
        case MoreViewControllerTimetables     : return @"Timetables";
        case MoreViewControllerFeedback       : return @"Feedback";
        case MoreViewControllerHelp           : return @"Help";
        case MoreViewControllerSettings       : return @"Settings";
        case MoreViewControllerMostRecent     : return @"MostRecent";
        case MoreViewControllerPrivacyPolicy  : return @"PrivacyPolicy";
        case MoreViewControllerSpecialOffers  : return @"SpecialOffers";
    }
    return @"";
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{ return 1; }

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{ return self.cellIdentifiers.count; }

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* cellIdentifier = [self cellIdentifierForType:[self.cellIdentifiers[indexPath.row] integerValue]];
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MoreViewControllerCellType type = [self.cellIdentifiers[indexPath.row] integerValue];
    switch (type)
    {
        case MoreViewControllerTwitter:
        {
            [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureFollowOnTwitter];
            
            self.currentURL = [NSURL URLWithString:@"twitter:///user?screen_name=yarratrams"];//@"twitter://"];

            NSString * title = @"Open in Twitter?";
            NSString * alertText = @"Would you like to close the tramTracker® app and launch Twitter?";
            
            if (![[UIApplication sharedApplication] canOpenURL:self.currentURL])
            {
                title = @"Open in Safari?";
                alertText = @"Would you like to close the tramTracker® app and launch Safari?";
                self.currentURL = [NSURL URLWithString:@"http://twitter.com/yarratrams"];
            }
            
            [[[UIAlertView alloc] initWithTitle:title
                                        message:alertText
                                       delegate:self
                              cancelButtonTitle:@"Cancel"
                              otherButtonTitles:@"OK", nil]
             show];
            break;
        }
        case MoreViewControllerPTV:
        {
            [[Analytics sharedInstance] featureAccessed:AnalyticsFeaturePTV];
            
            WebViewController * webView = [[WebViewController alloc] init];
            [webView setUrlStr:@"http://ptv.vic.gov.au"];
            [webView setTitle:@"PTV Journey Planner"];
            
            [self.navigationController pushViewController:webView animated:YES];
            break;
            
        }
//        case MoreViewControllerFTZ:
//        {
//            [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureFTZ];
//            [self.view.window addSubview:[FTZOverlay overlay]];
//            
////            WebViewController * webView = [[WebViewController alloc] init];
////            [webView setUrlStr:@"http://tramtracker.com/ftz"];
////            [webView setTitle:@"Free Tram Zone"];
////            
////            [self.navigationController pushViewController:webView animated:YES];
//            break;
//        }
        case MoreViewControllerSpecialOffers:
        {
            OLRichInboxTileCardViewController* controller = [OLRichInboxTileCardViewController new];
            controller.title = @"Special Offers";
            [self.navigationController pushViewController:controller animated:YES];
//            controller.navigationItem.hidesBackButton = YES;
//            [self presentViewController:controller animated:YES completion:nil];
           break;
        }
        case MoreViewControllerPrivacyPolicy:
        {
            WebViewController * webView = [[WebViewController alloc] init];
            [webView setUrlStr:@"http://yarratrams.com.au/general/privacy-policy/"];
            [webView setTitle:@"Privacy Policy"];
            
            [self.navigationController pushViewController:webView animated:YES];
            break;
        }
        case MoreViewControllerTimetables:
        {
            UIStoryboard                * sb = [UIStoryboard storyboardWithName:kMainStoryboard bundle:nil];
            RoutesListViewController    * vc = (id)[sb instantiateViewControllerWithIdentifier:kScreenRoutesList];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        default:
            break;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Alertview Delegate Methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
        [[UIApplication sharedApplication] openURL:self.currentURL];
}

@end
