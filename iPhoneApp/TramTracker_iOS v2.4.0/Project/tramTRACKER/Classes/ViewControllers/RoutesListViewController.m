//
//  RouteListViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 9/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "RoutesListViewController.h"
#import "SectionHeaderView.h"
#import "RouteCell.h"
#import "myTramListViewController.h"
#import "Constants.h"
#import "RouteList.h"
#import "Analytics.h"
#import "Route.h"
#import "RouteContainerViewController.h"

#import "WebViewController.h"

@interface RoutesListViewController () <UIAlertViewDelegate>

typedef enum
{
    RouteListViewControllerUp,
    RouteListViewControllerDown,
    RouteListViewControllerStatus
} RouteListViewControllerRow;

@property (strong, nonatomic) NSArray               * routes;
@property (strong, nonatomic) NSMutableArray        * routesStates;
@property (strong, nonatomic) NSMutableDictionary   * specialFooter;
@property (weak, nonatomic) IBOutlet UITableView        * tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint* tableToBottomConstraint;

@property (nonatomic, assign) int indexOfRoute35;

typedef enum
{
    RoutesListViewControllerRoutes,
    RoutesListViewControllerTimetables
}   RoutesListViewControllerType;

@property (nonatomic) RoutesListViewControllerType type;

@end

@implementation RoutesListViewController

NSString * const kRoute35 = @"35";
NSString * const kRoute35SectionText = @"35 - City Circle (Free Tourist Tram)";
NSString * const kRoute35CellText = @"More Information";

- (NSLayoutConstraint *)adSpacingConstraint
{ return self.tableToBottomConstraint; }

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self showAds];
    
    [self loadData];

    [[NSNotificationCenter defaultCenter] addObserver:self.navigationController selector:@selector(popToRootViewControllerAnimated:) name:kNotificationSyncFinished object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadData) name:kNotificationSyncFinished object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(respondToKillSwitchUpdate:) name:kNotificationKillSwitchUpdate object:nil];
}

- (void)loadData
{
    NSMutableArray *arrTemp = [NSMutableArray arrayWithArray:[[RouteList sharedManager] routeList]];
    self.indexOfRoute35 = 0;
    for (int i = 0; i < arrTemp.count; i++)
    {
        Route *route = arrTemp[i];
        //Find the 1st item > 35
        if ([route.number integerValue] > 35)
        {
            self.indexOfRoute35 = i;
            [arrTemp insertObject:kRoute35 atIndex:self.indexOfRoute35];
            break;
        }
    }
    
    self.routes = [NSArray arrayWithArray:arrTemp];

    self.routesStates = [NSMutableArray new];
    self.specialFooter = [NSMutableDictionary new];

    NSDictionary     * specialRoutes = @{@"3" :  NSLocalizedString(@"browse-footers-3", "Route 3 message"),
                                         @"3a" : NSLocalizedString(@"browse-footers-3a", "Route 3a message")};

    [self.routes enumerateObjectsUsingBlock:^(Route * curRoute, NSUInteger idx, BOOL *stop)
     {
         NSString    *value = kRoute35;
         if (idx != self.indexOfRoute35)
             value = specialRoutes[curRoute.number];
         
         if (value)
             self.specialFooter[@(idx)] = value;
         [self.routesStates addObject:@YES];
     }];
    
    [self.tableView reloadData];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    self.tableView.delegate   = nil;
    self.tableView.dataSource = nil;
    
    [self removeAds:NO];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.type = self.navigationController.viewControllers.count == 2 ? RoutesListViewControllerTimetables : RoutesListViewControllerRoutes;

    if (self.type == RoutesListViewControllerTimetables)
    {
        self.navigationItem.title = @"Timetables";
        [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureTimeTables];
    }
    else
    {
        self.navigationItem.title = @"Routes";
        self.navigationItem.leftBarButtonItem = nil;
        [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureRoutes];
    }
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"networks-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(showNetworkMaps:)];
}

#pragma mark - User Nav Bar Actions

- (IBAction)backAction:(id)sender
{ [self.navigationController popViewControllerAnimated:YES]; }

- (IBAction)showNetworkMaps:(UIButton*)sender
{ [self performSegueWithIdentifier:@"NetworkMaps" sender:self]; }

#pragma mark - Segue Methods

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kSegueShowRouteInfo])
    { [segue.destinationViewController setRoute:sender direction:[[self.tableView indexPathForSelectedRow] row] == RouteListViewControllerUp isTimeTables:self.type == RoutesListViewControllerTimetables]; }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{ return self.routes.count; }

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.routesStates[section] boolValue])
    { return [self noOfRowsForSection:section]; }
    return 0;
}

- (NSInteger)noOfRowsForSection:(NSInteger)section
{
    if (section == self.indexOfRoute35)
    { return 1; }
    
    NSString    * footer = self.specialFooter[@(section)];
    Route       * route = self.routes[section];
    NSInteger   noOfRows = 0;
    
    if (footer)
        noOfRows++;

    if (route.upDestination)
        noOfRows++;

    if (route.downDestination)
        noOfRows++;

    return noOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RouteCell       * cell;

    if (indexPath.row == RouteListViewControllerStatus)
    {
        static NSString * CellIdentifierDesc = @"RouteCellDesc";
        cell = (id)[tableView dequeueReusableCellWithIdentifier:CellIdentifierDesc forIndexPath:indexPath];
        [cell configureWithText:self.specialFooter[@(indexPath.section)]];
    }
    else
    {
        NSString        * text = @"";
        static NSString * CellIdentifier = @"RouteCell";

        if ([self.routes[indexPath.section] isKindOfClass:[NSString class]])
            text = kRoute35CellText;
        else
        {
            Route           * route = self.routes[indexPath.section];

            text = (indexPath.row == RouteListViewControllerUp && route.upDestination) ? route.upDestination : route.downDestination;
        }
        cell = (id)[tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        [cell configureWithText:text];
    }
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *title = @"";
    if ([self.routes[section] isKindOfClass:[NSString class]])
        title = kRoute35SectionText;
    else
    {
        Route * route = self.routes[section];
        title = route.formattedRouteName;
    }
    SectionHeaderView   * view = [SectionHeaderView greyExpandableSectionHeaderViewWithTitle:title
                                                                                     section:section
                                                                                      target:self
                                                                                      action:@selector(didSelectAccessoryForSectionHeader:)];
    [view setExpandedState:[self.routesStates[section] boolValue]];
    return view;
}

- (void)didSelectAccessoryForSectionHeader:(SectionHeaderView *)sectionHeader
{
    NSInteger       section = sectionHeader.section;

    self.routesStates[section] = @(![self.routesStates[section] boolValue]);

    NSMutableArray  * indexexPathsToModify = [NSMutableArray new];
    
    for (NSInteger i = 0; i < [self noOfRowsForSection:section]; ++i)
         [indexexPathsToModify addObject:[NSIndexPath indexPathForRow:i inSection:section]];

    if (![self.routesStates[section] boolValue])
        [self.tableView deleteRowsAtIndexPaths:indexexPathsToModify withRowAnimation:UITableViewRowAnimationMiddle];
    else
        [self.tableView insertRowsAtIndexPaths:indexexPathsToModify withRowAnimation:UITableViewRowAnimationMiddle];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 36.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath.row != RouteListViewControllerStatus ? 45.0f : 30.0f;
}

#pragma mark - TableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row != RouteListViewControllerStatus)
    {
        if ([self.routes[indexPath.section] isKindOfClass:[NSString class]])
        {
            WebViewController * webView = [[WebViewController alloc] init];
            [webView setUrlStr:@"http://www.yarratrams.com.au/using-trams/visitors-new-users/city-circle-tram/"];
            [webView setTitle:@"35 - City Circle (Free Tourist Tram)"];
            [self.navigationController pushViewController:webView animated:YES];
            
            
        }
        else
        { [self performSegueWithIdentifier:kSegueShowRouteInfo sender:self.routes[indexPath.section]]; }
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

@end
