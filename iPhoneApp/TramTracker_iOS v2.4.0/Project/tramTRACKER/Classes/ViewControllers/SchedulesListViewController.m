//
//  SchedulesListViewController.m
//  tramTracker
//
//  Created by Jonathan Head on 27/04/2015.
//  Copyright (c) 2015 AppsCore. All rights reserved.
//

#import "SchedulesListViewController.h"
#import "myTramCell.h"
#import "Prediction.h"
#import "SectionHeaderView.h"
#import "TTActivityIndicatorView.h"

@interface SchedulesListViewController ()
@property (weak, nonatomic) IBOutlet UITableView* disruptionsTableView;
@property (weak, nonatomic) IBOutlet UITableView* tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint* tableToBottomConstraint;

@property (strong, nonatomic) NSArray* suburbStopList;
@property (strong, nonatomic) NSArray* data;

@property (strong, nonatomic) NSMutableArray* dataStates;
@property (strong, nonatomic) NSMutableArray* ftzStates;

@property (strong, nonatomic) NSMutableArray* selectedIndexPaths;

@end

@implementation SchedulesListViewController

- (NSLayoutConstraint *)adSpacingConstraint
{ return self.tableToBottomConstraint; }

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self showAds];

    self.tableView.hidden = YES;
}

- (void)dealloc
{
    self.tableView.delegate   = nil;
    self.tableView.dataSource = nil;
}

- (void)scrollToStop:(Stop *)stop
{
    NSIndexPath *indexPath = [self indexPathOfStop:stop];
    
    if (indexPath == nil)
    { return; }
    
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (NSIndexPath *)indexPathOfStop:(Stop *)stop
{
    for(NSInteger section = 0; section < self.data.count; section++)
    {
        NSArray* suburbData = self.data[section];
        for(NSInteger row = 0; row < suburbData.count; row++)
        {
            Prediction* prediction = suburbData[row];
            if(prediction.stop == self.stop)
            { return [NSIndexPath indexPathForRow:row inSection:section+1]; }
        }
    }
    return nil;
}

#pragma mark - TableView Delegate/Data Source

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{ return NO; }

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // one section for the legend, and one for each suburb
    return self.data.count + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0)
    { return 1; }
    else
    {
        BOOL isExpanded = [self.dataStates[section - 1] boolValue];
        return isExpanded ? [self.data[section - 1] count] : 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{ return section == 0 ? 0 : 36; }

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{ return indexPath.section == 0 ? 85 : 65; }

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section == 0)
    { return nil; }
    
    section--;
    
    Prediction* prediction = self.data[section][0];
    SectionHeaderView* view = [SectionHeaderView expandableSectionHeaderViewWithTitle:[prediction.stop suburb] section:section target:self action:@selector(didSelectAccessoryForSectionHeader:)];
    [view setExpandedState:[self.dataStates[section] boolValue]];
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    myTramCell* cell;
    
    if(tableView == self.tableView)
    {
        if(indexPath.section == 0)
        {
            static NSString     * const LegendCellIdentifier = @"myTramLegendCell";
            cell = [tableView dequeueReusableCellWithIdentifier:LegendCellIdentifier];
        }
        else
        {
            NSArray* suburbStops = self.data[indexPath.section-1];
            Prediction* prediction = suburbStops[indexPath.row];
            Stop* stop = prediction.stop;
            NSString* routeImage;
            NSInteger stopIndex = [self.ftzStates[indexPath.section - 1][indexPath.row] integerValue];
            
            NSString* location;
            if((indexPath.section == 1) && (indexPath.row == 0))
            { location = @"start"; }
            else if((indexPath.section == self.data.count) && (indexPath.row == [self.data.lastObject count] - 1))
            { location = @"end"; }
            else
            { location = @"middle"; }
            routeImage = [NSString stringWithFormat:@"icn_route_%@_%@", self.route ? self.route.colour : @"yellow", location];
            
            static NSString* const DataCellIdentifier = @"myTramCell";
            cell = [tableView dequeueReusableCellWithIdentifier:DataCellIdentifier];
            [cell configureWithStop:stop routeImage:routeImage stopIdx:stopIndex];
            
            NSDateFormatter* formatter = [NSDateFormatter new];
            formatter.dateStyle = NSDateFormatterNoStyle;
            formatter.timeStyle = NSDateFormatterShortStyle;
            cell.estimatedTime.text = [formatter stringFromDate:prediction.predictedArrivalDateTime];
            if([stop.number isEqualToString:@"1"])
            {
                NSLog(@"Hidden!");
            }
        }
    }
    
    return cell;
}

#pragma mark - Setup data methods

- (void)setPredictionsList:(NSArray *)predictionsList
{
    _predictionsList = predictionsList;
    [self setupSuburbList];
    [self.tableView reloadData];
    self.tableView.hidden = NO;
    [self scrollToStop:self.stop];
}

- (void)setupSuburbList
{
    self.dataStates = [NSMutableArray new];
    NSMutableArray  * data = [NSMutableArray new];
    NSString        * lastSuburb = nil;
    
    for (Prediction* prediction  in self.predictionsList)
    {
        Stop* stop = prediction.stop;
        if (!data.count || ![lastSuburb isEqualToString:stop.suburbName])
        {
            [data addObject:[NSMutableArray new]];
            [self.dataStates addObject:@YES];
        }
        lastSuburb = stop.suburbName;
        [data.lastObject addObject:prediction];
    }
    self.suburbStopList = [data copy];
    self.data = self.suburbStopList;
    [self setupFTZ];
}

- (void)setupFTZ
{
    NSMutableArray * ftzRanges = [NSMutableArray new];
    // Array of Array of NSValue containing a NSRange. NSRange is just a tuple of Integers with (Location, Length).
    
    void (^addRange)(NSInteger location, NSInteger length, NSMutableArray * inArray) = ^(NSInteger location, NSInteger length, NSMutableArray * inArray) {
        
        if (location != -1 && length >= 3) {
            NSInteger occurences = (length + 1) / 4;
            NSInteger requiredLength = (occurences * 4) - 1;
            NSInteger start = (length - requiredLength) / 2;
            
            [inArray addObject:[NSValue valueWithRange:NSMakeRange(location + start, (occurences * 4) - 1)]];
        }
    };
    
    // self.suburbStopList is and Array of Array of Stops, grouped by suburb.
    
    [self.data enumerateObjectsUsingBlock:^(NSArray *stops, NSUInteger suburbIdx, BOOL *suburbStop) {
        __block NSMutableArray * ftzSuburbStates = [NSMutableArray new];
        __block NSInteger location = -1;
        __block NSInteger length = 0;
        
        [stops enumerateObjectsUsingBlock:^(Prediction* currentPrediction, NSUInteger stopIdx, BOOL *stopStop) {
            Stop* currentStop = currentPrediction.stop;
            if (currentStop.isFTZStop) {
                if (location == -1) {
                    location = stopIdx;
                }
                ++length;
            } else {
                addRange(location, length, ftzSuburbStates);
                location = -1;
                length = 0;
            }
        }];
        addRange(location, length, ftzSuburbStates);
        [ftzRanges addObject:ftzSuburbStates];
    }];
    
    self.ftzStates = [NSMutableArray new];
    
    [self.data enumerateObjectsUsingBlock:^(NSArray * suburb, NSUInteger suburbIdx, BOOL *suburbStop) {
        
        NSMutableArray * ftzFinalState = [NSMutableArray new];
        __block NSValue * value = [ftzRanges[suburbIdx] firstObject];
        __block NSInteger count = 0;
        
        if (value) {
            [ftzRanges[suburbIdx] removeObjectAtIndex:0];
        }
        
        [suburb enumerateObjectsUsingBlock:^(Prediction * prediction, NSUInteger stopIdx, BOOL *stopStop) {
            if (!value) {
                // -1 Corresponds to Nothing
                [ftzFinalState addObject:@-1];
            } else {
                NSRange range = value.rangeValue;
                
                if (stopIdx < range.location) {
                    // -1 Corresponds to Nothing
                    [ftzFinalState addObject:@-1];
                } else if (stopIdx >= range.location + range.length) {
                    // -1 Corresponds to Nothing
                    [ftzFinalState addObject:@-1];
                    if ((value = [ftzRanges[suburbIdx] firstObject])) {
                        [ftzRanges[suburbIdx] removeObjectAtIndex:0];
                        count = 0;
                    }
                } else {
                    // 0: Free
                    // 1: Tram
                    // 2: Zone
                    // 3: EMPTY
                    [ftzFinalState addObject:@(count)];
                    count = (count + 1) % 4;
                }
            }
        }];
        [self.ftzStates addObject:ftzFinalState];
    }];
}

- (void)didSelectAccessoryForSectionHeader:(SectionHeaderView *)sectionHeader
{
    self.dataStates[sectionHeader.section] = @(![self.dataStates[sectionHeader.section] boolValue]);
    
    NSMutableArray  * indexPaths = [NSMutableArray new];
    for (NSInteger i = 0; i < [self.data[sectionHeader.section] count]; ++i)
        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:sectionHeader.section+1]];
    
    if (![self.dataStates[sectionHeader.section] boolValue])
    { [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationMiddle]; }
    else
    { [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationMiddle]; }
}

@end
