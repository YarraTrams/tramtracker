//
//  SelectRouteViewController.m
//  tramTRACKER
//
//  Created by Raji on 24/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "SelectRouteViewController.h"
#import "Stop.h"
#import "RouteFilterViewController.h"

@interface SelectRouteViewController ()

@property (strong, nonatomic) Stop * currentStop;
@property (strong, nonatomic) NSArray * allRoutesThroughThisStopToDisplay;
@property (nonatomic, assign) NSInteger selectedIndex;
@property (nonatomic, assign) NSInteger initialSelectedIndex;
@property (nonatomic, assign) BOOL didSelectionChange;
@property (nonatomic, assign) BOOL upDirection;

@end

@implementation SelectRouteViewController

static viewControllerType currentMode;

#pragma mark - Class Methods

+ (void)setCurrentMode:(viewControllerType)mode
{
    currentMode = mode;
}

#pragma mark - Utility Methods

- (void)setStop:(Stop *)aStop direction:(BOOL)upDirection
{
    self.currentStop = aStop;
    self.upDirection = upDirection;
}

#pragma mark - Inits & Loads

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSMutableArray *arrTemp = [NSMutableArray array];
    for (NSString *routeNo in [self.currentStop routesThroughStop])
    {
        Route       * route = [Route routeWithNumber:routeNo];
        NSString *stringToDisplay = [route routeNameWithNumberAndDestinationUp:[route.upStops containsObject:self.currentStop.trackerID]];
        [arrTemp addObject:stringToDisplay];
    }
    
    if (currentMode == FromRouteFilter)
    {
        self.navigationItem.title = @"Select Route";
        [arrTemp insertObject:@"All Routes" atIndex:0];
    }
    else
    {
       self.navigationItem.title = @"Route Filter";
    }
    self.allRoutesThroughThisStopToDisplay = [[NSArray alloc] initWithArray:arrTemp];
    
    self.initialSelectedIndex = self.selectedIndex;
    
    self.didSelectionChange = NO;
}

- (void)dealloc
{
    self.tableView.delegate = nil;
    self.tableView.dataSource = nil;
}

#pragma mark - User Actions

- (IBAction)backAction:(id)sender
{
    if (self.didSelectionChange)
    {
        NSInteger   currentViewController = self.navigationController.childViewControllers.count - 1;
        id          previousViewController = nil;
        
        if (currentViewController > 0)
        {
            previousViewController = self.navigationController.childViewControllers[currentViewController - 1];
            [previousViewController updateSelectedRoute:self.selectedIndex];
        }
    }
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.allRoutesThroughThisStopToDisplay.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

    BOOL isSelected = (indexPath.row == self.selectedIndex);
    if (isSelected)
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Tick.png"]];
    else
        cell.accessoryView = nil;
    cell.textLabel.text = self.allRoutesThroughThisStopToDisplay[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.didSelectionChange = YES;
    
    UITableViewCell  * cell;
    if (self.initialSelectedIndex != -1)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.initialSelectedIndex inSection:0];
        cell = (id)[tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryView = nil;
        self.initialSelectedIndex = -1;
    }
    
    cell = (id)[tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Tick.png"]];
    self.selectedIndex = indexPath.row;
    [self backAction:nil];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell  * cell = (id)[tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryView = nil;
}

@end
