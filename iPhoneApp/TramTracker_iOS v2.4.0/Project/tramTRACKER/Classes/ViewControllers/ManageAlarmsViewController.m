//
//  ManageAlarmsViewController.m
//  tramTracker
//
//  Created by Jonathan Head on 16/07/2015.
//  Copyright (c) 2015 AppsCore. All rights reserved.
//

#import "ManageAlarmsViewController.h"
#import "FavouriteCell.h"
#import "AlarmSelectionPopup.h"
#import "MBProgressHUD.h"
#import "TutorialManager.h"

typedef enum : NSUInteger {
    AlarmTypeStops,
    AlarmTypeTrams,
} AlarmType;

NSString* const NotificationLocalNotificationAdded = @"LocalNotificationAdded";

@implementation AlarmCell
@end

@interface ManageAlarmsViewController ()
@property (strong, nonatomic) NSArray* onTramAlarms; // On tram, approaching stop
@property (strong, nonatomic) NSArray* atStopAlarms; // At stop, waiting for tram

@property (weak, nonatomic) IBOutlet UITableView* tableView;
@property (weak, nonatomic) IBOutlet UILabel* emptyLabel;
@property (strong, nonatomic) NSDateFormatter* dateFormatter;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint* tableToBottomConstraint;

- (IBAction)onBack;
- (IBAction)manageAlarms;
@end

@implementation ManageAlarmsViewController

- (void)onBack
{ [self.navigationController popViewControllerAnimated:YES]; }

- (void)manageAlarms
{ [self.tableView setEditing:!self.tableView.editing animated:YES]; }

+ (BOOL)addNotificationForStop:(Stop*)stop tram:(Tram*)tram date:(NSDate*)date
{
    if([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
    { [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeSound categories:nil]]; }
    
    UILocalNotification* notif = [UILocalNotification new];
    notif.fireDate = [date dateByAddingTimeInterval:-120]; // fire date - 2 minutes before the stop arrival time
    notif.alertBody = [NSString stringWithFormat:@"Your stop is approaching soon. Please check tramTRACKER for real-time info."];
    notif.soundName = @"a.mp3";
    notif.userInfo = @{
        @"type":@"onTram",
        @"tramId":tram.number,
        @"trackerId":stop.trackerID,
        @"route":tram.routeNumber,
        @"baseDate":date
    };
    
    for(UILocalNotification* notification in [UIApplication sharedApplication].scheduledLocalNotifications)
    {
        NSString* type = notification.userInfo[@"type"];
        NSDate*   baseDate = notification.userInfo[@"baseDate"];
        NSNumber* tramID = notification.userInfo[@"tramId"];
        NSString* routeNumber = notification.userInfo[@"route"];
        
        if(type && baseDate && tramID)
        {
            if([type isEqualToString:@"onTram"] && [ManageAlarmsViewController isDate:notification.fireDate sameAsDate:notif.fireDate] && [tramID isEqual:notif.userInfo[@"tramId"]] && [routeNumber isEqual:notif.userInfo[@"route"]])
            { return NO; }
        }
    }
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notif];
    [[NSNotificationCenter defaultCenter] postNotificationName:NotificationLocalNotificationAdded object:nil userInfo:nil];
    return YES;
}

+ (BOOL)isDate:(NSDate*)firstDate sameAsDate:(NSDate*)secondDate
{
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* firstComponents = [calendar components:NSCalendarUnitMinute|NSCalendarUnitHour|NSCalendarUnitDay|NSCalendarUnitMonth fromDate:firstDate];
    NSDateComponents* secondComponents = [calendar components:NSCalendarUnitMinute|NSCalendarUnitHour|NSCalendarUnitDay|NSCalendarUnitMonth fromDate:secondDate];
    return ((firstComponents.minute == secondComponents.minute) &&
            (firstComponents.hour   == secondComponents.hour)   &&
            (firstComponents.day    == secondComponents.day)    &&
            (firstComponents.month  == secondComponents.month));
}

+ (BOOL)addNotificationForTram:(Stop*)stop prediction:(Prediction*)prediction timeOffset:(NSTimeInterval)offset
{
    if([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
    { [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeSound categories:nil]]; }
    
    NSDate* date = prediction.predictedArrivalDateTime;
    TramType tramType = [prediction.tram classType];
    
    UILocalNotification* notif = [UILocalNotification new];
    notif.fireDate = [date dateByAddingTimeInterval:-offset];
    notif.alertBody = [NSString stringWithFormat:@"Your tram is approaching soon. Please check tramTRACKER for real-time info."];
    switch(tramType)
    {
        case TramTypeW:
            notif.soundName = @"w.mp3"; break;
        case TramTypeC:
        case TramTypeC2:
        case TramTypeD1:
        case TramTypeD2:
        case TramTypeE:
            notif.soundName = @"citadis.mp3"; break;
        default:
            notif.soundName = @"a.mp3";
    }
    notif.userInfo = @{
        @"type":@"atStop",
        @"trackerId":stop.trackerID,
        @"route":prediction.headboardRouteNumber,
        @"baseDate":date
    };
    
    for(UILocalNotification* notification in [UIApplication sharedApplication].scheduledLocalNotifications)
    {
        NSString* type = notification.userInfo[@"type"];
        NSNumber* trackerId = notification.userInfo[@"trackerId"];
//        NSDate*   baseDate  = notification.userInfo[@"baseDate"];
        NSString* routeNumber = notification.userInfo[@"route"];
        if([type isEqualToString:@"atStop"] && [trackerId isEqual:notif.userInfo[@"trackerId"]] && [ManageAlarmsViewController isDate:notification.fireDate sameAsDate:notif.fireDate] && [routeNumber isEqual:notif.userInfo[@"route"]])
        { return NO; }
    }
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notif];
    [[NSNotificationCenter defaultCenter] postNotificationName:NotificationLocalNotificationAdded object:nil userInfo:nil];
    return YES;
}

+ (CustomIOSAlertView *)popupForView:(UIView*)view stop:(Stop*)stop prediction:(Prediction*)prediction times:(NSArray*)times completion:(CompletionBlock)completion isEstimate:(BOOL)isEstimate
{
    CustomIOSAlertView* alert = [CustomIOSAlertView new];
    alert.buttonTitles = @[ @"Cancel", @"OK" ];
    [alert setContainerView:[AlarmSelectionPopup popupForPrediction:prediction stop:stop timeOffsets:times isEstimate:isEstimate]];
    [alert setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        if(buttonIndex == 1)
        {
            AlarmSelectionPopup* popup = (AlarmSelectionPopup*)alertView.containerView;
            NSDate* fireDate = [prediction.predictedArrivalDateTime dateByAddingTimeInterval:-popup.secondsOffset];
            if(fireDate.timeIntervalSinceNow <= 60)
            {
                [[[UIAlertView alloc] initWithTitle:@"Error" message:@"You cannot set an alarm for a past time." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                completion(NO);
            }
            else if([ManageAlarmsViewController addNotificationForTram:stop prediction:prediction timeOffset:popup.secondsOffset])
            {
                MBProgressHUD* loadIcon = [MBProgressHUD showHUDAddedTo:view animated:YES];
                loadIcon.mode = MBProgressHUDModeCustomView;
                UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_tick_popup"]];
                loadIcon.customView = imageView;
                NSDateFormatter* formatter = [NSDateFormatter new];
                formatter.dateFormat = @"hh:mm a";
                
                NSDate* reminderDate = [NSDate dateWithTimeIntervalSince1970:prediction.predictedArrivalDateTime.timeIntervalSince1970-popup.secondsOffset];
                loadIcon.detailsLabelText = [NSString stringWithFormat:@"You will be alerted at %@. Please tap on the notification to get updated real-time information.", [formatter stringFromDate:reminderDate]];
                [loadIcon hide:YES afterDelay:ToastMessageDelay];
                [alertView close];
                completion(YES);
            }
            else
            {
                [[[UIAlertView alloc] initWithTitle:@"Error" message:@"You have already set an alarm for this tram." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                completion(NO);
            }
        }
        else
        { [alertView close]; }
    }];
    return alert;
}

+ (void)addNotificationForTram:(Stop*)stop date:(NSDate*)date
{
    UILocalNotification* notif = [UILocalNotification new];
    notif.fireDate = [date dateByAddingTimeInterval:-120];
    notif.alertBody = [NSString stringWithFormat:@""];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self showAds];
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    
    [self.dateFormatter setDateFormat:@"HH:mm"]; // -- use built in style to allow for use of 24 hour time setting
    self.dateFormatter.dateFormat = NSDateFormatterNoStyle;
    self.dateFormatter.timeStyle  = NSDateFormatterShortStyle;
    
    [self updateNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateNotifications) name:NotificationLocalNotificationAdded object:nil];
}

- (NSLayoutConstraint *)adSpacingConstraint
{ return self.tableToBottomConstraint; }

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    TutorialManager* instance = [TutorialManager instance];
    if(instance.currentTutorialSettings == TutorialSettingsManageAlarms)
    {
        if(self.tableView.visibleCells.count > 0)
        {
            UITableViewCell* cell = self.tableView.visibleCells[0];
            instance.currentTutorialSettings = TutorialSettingsDeleteAlarm;
            [instance showTutorial:TutorialSettingsDeleteAlarm container:self.tableView alertDelegate:nil frame:cell.frame dismiss:^{
                instance.currentTutorialSettings = TutorialSettingsBackFromAlarms;
                [instance showTutorial:TutorialSettingsBackFromAlarms container:self.view alertDelegate:nil frame:CGRectMake(10, 0, 70, 1) dismiss:nil];
            }];
        }
        else
        {
            instance.currentTutorialSettings = TutorialSettingsBackFromAlarms;
            [instance showTutorial:TutorialSettingsBackFromAlarms container:self.view alertDelegate:nil frame:CGRectMake(10, 0, 70, 1) dismiss:nil];
        }
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NotificationLocalNotificationAdded object:nil];
    [self removeAds:NO];
}

- (void)updateNotifications
{
    NSMutableArray* mutableStopAlarms = [NSMutableArray array];
    NSMutableArray* mutableTramAlarms = [NSMutableArray array];
    
    for(UILocalNotification* notif in [UIApplication sharedApplication].scheduledLocalNotifications)
    {
        if([notif.userInfo[@"type"] isEqualToString:@"atStop"])
        { [mutableStopAlarms addObject:notif]; }
        else if([notif.userInfo[@"type"] isEqualToString:@"onTram"])
        { [mutableTramAlarms addObject:notif]; }
    }
    self.atStopAlarms = mutableStopAlarms;
    self.onTramAlarms = mutableTramAlarms;
    
    self.tableView.hidden  = self.atStopAlarms.count + self.onTramAlarms.count == 0;
    self.emptyLabel.hidden = self.atStopAlarms.count + self.onTramAlarms.count >  0;
    [self.tableView reloadData];
    self.navigationItem.rightBarButtonItem.enabled = (self.atStopAlarms.count + self.onTramAlarms.count > 0);
}

- (AlarmType)typeForSection:(NSInteger)section
{
    if(section == 0)
    {
        if(self.onTramAlarms.count > 0)
        { return AlarmTypeTrams; }
        else
        { return AlarmTypeStops; }
    }
    else
    { return AlarmTypeStops; }
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    AlarmType type = [self typeForSection:section];
    switch(type)
    {
        case AlarmTypeTrams: return @"On tram, approaching stop";
        case AlarmTypeStops: return @"At stop, waiting for tram";
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(self.atStopAlarms.count == 0)
    {
        if(self.onTramAlarms.count == 0)
        { return 0; }
        else
        { return 1; }
    }
    else if(self.onTramAlarms.count == 0)
    { return 1; }
    else
    { return 2; }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    AlarmType type = [self typeForSection:section];
    switch(type)
    {
        case AlarmTypeStops: return self.atStopAlarms.count;
        case AlarmTypeTrams: return self.onTramAlarms.count;
        default: return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AlarmCell* cell;
    UILocalNotification* notif;
    AlarmType type = [self typeForSection:indexPath.section];
    if(type == AlarmTypeStops)
    { notif = self.atStopAlarms[indexPath.row]; }
    else if(type == AlarmTypeTrams)
    { notif = self.onTramAlarms[indexPath.row]; }
    
    Stop* stop = [Stop stopForTrackerID:notif.userInfo[@"trackerId"]];
    if(stop.isFTZStop)
    { cell = [tableView dequeueReusableCellWithIdentifier:@"AlarmFTZ" forIndexPath:indexPath]; }
    else
    { cell = [tableView dequeueReusableCellWithIdentifier:@"Alarm" forIndexPath:indexPath]; }
    
    cell.labelTitle.text = stop.formattedName;
    cell.labelSubtitle.text = [NSString stringWithFormat:@"Route %@ - %@", notif.userInfo[@"route"], stop.displayedCityDirection];
    cell.labelTime.text = [self.dateFormatter stringFromDate:notif.fireDate];
    
    return cell;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{ return UITableViewCellEditingStyleDelete; }

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([TutorialManager instance].currentTutorialSettings == TutorialSettingsDeleteAlarm)
    { [[TutorialManager instance] hideTutorial:TutorialScreenSettings]; }
    
    AlarmType type = [self typeForSection:indexPath.section];
    NSMutableArray* alarms;
    UILocalNotification* notif;
    if(type == AlarmTypeStops)
    {
        alarms = [self.atStopAlarms mutableCopy];
        notif = alarms[indexPath.row];
        [alarms removeObject:notif];
        self.atStopAlarms = [alarms copy];
    }
    else if(type == AlarmTypeTrams)
    {
        alarms = [self.onTramAlarms mutableCopy];
        notif = alarms[indexPath.row];
        [alarms removeObject:notif];
        self.onTramAlarms = [alarms copy];
    }
    
    if(notif)
    { [[UIApplication sharedApplication] cancelLocalNotification:notif]; }
    
    if(alarms.count == 0)
    { [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationLeft]; }
    else
    { [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft]; }
    
    if((self.onTramAlarms.count == 0) && (self.atStopAlarms.count == 0))
    {
        self.tableView.hidden = YES;
        self.emptyLabel.hidden = NO;
    }
    self.navigationItem.rightBarButtonItem.enabled = (self.atStopAlarms.count + self.onTramAlarms.count > 0);
}

@end
