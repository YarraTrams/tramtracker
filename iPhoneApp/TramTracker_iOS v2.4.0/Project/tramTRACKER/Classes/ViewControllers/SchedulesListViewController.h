//
//  SchedulesListViewController.h
//  tramTracker
//
//  Created by Jonathan Head on 27/04/2015.
//  Copyright (c) 2015 AppsCore. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SchedulesListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) NSArray* predictionsList;
@property (strong, nonatomic) Route* route;
@property (strong, nonatomic) Stop* stop;
@end
