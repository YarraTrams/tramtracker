//
//  myTramContainerViewController.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 29/11/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Journey.h"
#import "MapViewController.h"

@interface myTramViewController : UIViewController

@property (strong, nonatomic) JourneyStub * journey;
@property (nonatomic, assign) BOOL hasButtonBeenPressed;

@property (nonatomic, assign, getter = isAtLayover) BOOL atLayover;
@property (nonatomic, assign, getter = isStopped) BOOL stopped;
@property (nonatomic, strong) NSDate * startedLocating;
@property (nonatomic, assign) NSInteger accuracyLevel;

- (BOOL)hasTramChangedRoute:(Tram *)tram;
- (MapViewController *)map;
- (BOOL)isSelectedCell:(Stop *)stop withJourneyStop:(JourneyStop *)jstop;
- (void)setTramNumberString:(NSString *)aTramNo;

@end
