//
//  SearchResultContainerViewController.m
//  tramTracker
//
//  Created by Hugo Cuvillier on 21/02/2014.
//  Copyright (c) 2014 AppsCore. All rights reserved.
//

#import "SearchResultContainerViewController.h"
#import "TTTabBarController.h"
#import "MapViewController.h"
#import "POIViewController.h"
#import "Constants.h"

@interface SearchResultContainerViewController ()

@property (nonatomic, assign) BOOL              easyAccess;
@property (nonatomic, assign) BOOL              shelter;
@property (nonatomic, strong) NSArray           * lists;
@property (weak, nonatomic) TTTabBarController  * subTabBar;

@end

@implementation SearchResultContainerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.subTabBar = self.childViewControllers.firstObject;

    NSMutableArray  * flatList = [NSMutableArray new];

    for (NSDictionary *curDic in self.lists)
        [flatList addObjectsFromArray:curDic[@"List"]];
    
    
    /* Setting List Data */
    [self.subTabBar.childViewControllers.firstObject setLists:self.lists];
    [self.subTabBar.childViewControllers.firstObject setBooleanForEasyAccessStops:self.easyAccess andShelter:self.shelter];

    /* Setting Map Data */
    [self.subTabBar.childViewControllers.lastObject setStopList:flatList];
}

- (void)setBooleanForEasyAccessStops:(BOOL)hasEasyAccess andShelter:(BOOL)hasShelter
{
    self.shelter = hasShelter;
    self.easyAccess = hasEasyAccess;
}

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)toggleMapAction:(id)sender
{
    [self.subTabBar toggleListMapAction:sender isRight:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kSegueShowPOI])
        [segue.destinationViewController setPoi:sender];
}

@end
