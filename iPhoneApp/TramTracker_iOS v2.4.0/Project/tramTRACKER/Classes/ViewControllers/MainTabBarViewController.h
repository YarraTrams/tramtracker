//
//  MainTabBarViewController.h
//  tramTracker
//
//  Created by Hugo Cuvillier on 24/03/2014.
//  Copyright (c) 2014 AppsCore. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainTabBarViewController : UITabBarController

@end
