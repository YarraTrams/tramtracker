//
//  ManageDisruptionsViewController.m
//  tramTracker
//
//  Created by Jonathan Head on 6/08/2015.
//  Copyright (c) 2015 AppsCore. All rights reserved.
//

#import "ManageDisruptionsViewController.h"
#import "RouteList.h"
#import "Constants.h"
#import "MBProgressHUD.h"
#import "TutorialManager.h"

typedef enum : NSUInteger {
    TravelTimeMorningWeekdayPeak = 1,
    TravelTimeEveningWeekdayPeak = 2,
    TravelTimeAllWeekday = 3,
    TravelTimeWeekend = 4
} TravelTime;

static NSString* kSelectedRoutesNotifications = @"SelectedRoutesNotifications";
static NSString* kSelectedTimesNotifications  = @"SelectedTimesNotifications";

@interface DisruptionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel* labelTitle;
@property (weak, nonatomic) IBOutlet UILabel* labelSubtitle;
@property (weak, nonatomic) IBOutlet UIImageView* imageCheck;
@end

@implementation DisruptionCell
@end

@interface ManageDisruptionsViewController ()
@property (strong, nonatomic) NSArray* allRoutes;
@property (strong, nonatomic) NSMutableArray* selectedRoutes;
@property (strong, nonatomic) NSMutableArray* selectedTimes;
@property (weak, nonatomic) IBOutlet UITableView* tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint* tableToBottomConstraint;
- (IBAction)saveSelections:(id)sender;
@end

@interface ManageDisruptionsViewController ()
/// This tracks if any changes were made to the schedule so we can prompt the user if they try and go back without saving.
@property (nonatomic) BOOL wasEdited;
@end

@implementation ManageDisruptionsViewController

#pragma mark Init Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showAds];
    self.wasEdited = NO;
    self.allRoutes = [[RouteList sharedManager] routeList];
    self.selectedRoutes = [[[NSUserDefaults standardUserDefaults] arrayForKey:kSelectedRoutesNotifications] mutableCopy];
    self.selectedTimes  = [[[NSUserDefaults standardUserDefaults] arrayForKey:kSelectedTimesNotifications] mutableCopy];
    
    if(self.selectedRoutes == nil)
    { self.selectedRoutes  = [NSMutableArray array]; }
    
    if(self.selectedTimes == nil)
    { self.selectedTimes  = [NSMutableArray array]; }
    
    [self.tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if([TutorialManager instance].currentTutorialSettings == TutorialSettingsGotoDisruptions)
    {
        [TutorialManager instance].currentTutorialSettings = TutorialSettingsSelectDisruptionTime;
        UITableViewCell* timeCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
        [[TutorialManager instance] showTutorial:TutorialSettingsSelectDisruptionTime container:self.tableView alertDelegate:self frame:[self.tableView convertRect:timeCell.contentView.bounds fromView:timeCell.contentView] dismiss:^{
            UITableViewCell* routeCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
            [TutorialManager instance].currentTutorialSettings = TutorialSettingsSelectDisruptionRoute;
            [[TutorialManager instance] showTutorial:TutorialSettingsSelectDisruptionRoute container:self.tableView alertDelegate:self frame:[self.tableView convertRect:routeCell.contentView.bounds fromView:routeCell.contentView] dismiss:^{
                [TutorialManager instance].currentTutorialSettings = TutorialSettingsSaveDisruptions;
                [[TutorialManager instance] showTutorial:TutorialSettingsSaveDisruptions container:self.view alertDelegate:self frame:CGRectMake(self.view.frame.size.width-90, 0, 70, 1) dismiss:nil];
            }];
        }];
    }
}

- (void)dealloc
{ [self removeAds:NO]; }

- (NSLayoutConstraint *)adSpacingConstraint
{ return self.tableToBottomConstraint; }

#pragma mark User Action Methods

- (IBAction)saveSelections:(id)sender
{
    if(self.wasEdited)
    {
        PidsService* service = [PidsService new];

        NSArray* routes = [self getSelectedRoutes];
        NSArray* times  = [self getSelectedTimes];
        
        if(times.count == 0)
        { [[[UIAlertView alloc] initWithTitle:@"Warning" message:@"Please select travel times for notifications" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show]; }
        else if(routes.count == 0)
        { [[[UIAlertView alloc] initWithTitle:@"Warning" message:@"Please select a route for notifications" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];}
        else
        {
            MBProgressHUD* loadIcon = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            loadIcon.labelText = @"Saving...";
            [service submitNotificationUpdateRequestWithToken:service.guid routes:routes times:times completion:^(BOOL successful, NSError *error) {
                [loadIcon hide:YES];
                if(successful)
                {
                    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setObject:self.selectedRoutes forKey:kSelectedRoutesNotifications];
                    [defaults setObject:self.selectedTimes  forKey:kSelectedTimesNotifications];
                    [defaults synchronize];
                    [self.navigationController popViewControllerAnimated:YES];
                    [[[UIAlertView alloc] initWithTitle:@"" message:@"Disruption notification updated successfully" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                }
                else
                { [[[UIAlertView alloc] initWithTitle:@"Warning" message:@"Your changes could not be saved. Would you still like to go back?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Go Back", nil] show]; }
            }];
        }
    }
    else
    { [self.navigationController popViewControllerAnimated:YES]; }
}

- (IBAction)onBack:(id)sender
{
    if(self.wasEdited)
    { [[[UIAlertView alloc] initWithTitle:@"Warning" message:@"You haven't saved your changes. Would you still like to go back?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Go Back", nil] show]; }
    else
    { [self.navigationController popViewControllerAnimated:YES]; }
}

#pragma mark Utility / Helper Methods

- (NSArray*)getSelectedTimes
{ return self.selectedTimes; }

- (NSArray*)getSelectedRoutes
{
    NSMutableArray* routes = [NSMutableArray array];
    NSNumberFormatter* formatter = [NSNumberFormatter new];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    for(NSString* string in self.selectedRoutes)
    {
        NSNumber* num = [formatter numberFromString:string];
        if(num)
        { [routes addObject:num]; }
        else
        {
            if([string isEqualToString:@"3a"])
            { [routes addObject:@4]; }
        }
    }
    return routes;
}

- (BOOL)allRoutesSelected
{
    for(Route* route in self.allRoutes)
    {
        if(![self.selectedRoutes containsObject:route.number])
        { return NO; }
    }
    return YES;
}

#pragma mark - TableView Delegate / Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{ return 2; }

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch(section)
    {
        case 0: return @"Travel Times";
        case 1: return @"Routes";
        default: return @"";
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch(section) {
        case 0:  return 4;
        case 1:  return self.allRoutes.count; // +1 for the 'All Routes' option
        default: return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.wasEdited = YES;
    
    DisruptionCell* cell;
    switch(indexPath.section)
    {
        case 0:
        {
            switch(indexPath.row)
            {
                case 0:
                    cell = [tableView dequeueReusableCellWithIdentifier:@"CellWithSubtitle"];
                    cell.labelTitle.text = @"Morning Weekday Peak";
                    cell.labelSubtitle.text = @"(6am-9am)";
                    cell.imageCheck.hidden = ![self.selectedTimes containsObject:@(TravelTimeMorningWeekdayPeak)];
                    break;
                case 1:
                    cell = [tableView dequeueReusableCellWithIdentifier:@"CellWithSubtitle"];
                    cell.labelTitle.text = @"Evening Weekday Peak";
                    cell.labelSubtitle.text = @"(4pm-7pm)";
                    cell.imageCheck.hidden = ![self.selectedTimes containsObject:@(TravelTimeEveningWeekdayPeak)];
                    break;
                case 2:
                    cell = [tableView dequeueReusableCellWithIdentifier:@"CellNoSubtitle"];
                    cell.labelTitle.text = @"Weekday All Day";
                    cell.imageCheck.hidden = ![self.selectedTimes containsObject:@(TravelTimeAllWeekday)];
                    break;
                case 3:
                    cell = [tableView dequeueReusableCellWithIdentifier:@"CellNoSubtitle"];
                    cell.labelTitle.text = @"Weekend";
                    cell.imageCheck.hidden = ![self.selectedTimes containsObject:@(TravelTimeWeekend)];
                    break;
            }
            [cell correctContentSize];
            
            return cell;
        }
        case 1:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"CellNoSubtitle"];
            Route* route = self.allRoutes[indexPath.row];
            cell.labelTitle.text   = [NSString stringWithFormat:@"%@ - %@", route.number, route.name];
            cell.imageCheck.hidden = ![self.selectedRoutes containsObject:route.number];
            [cell correctContentSize];
            
            return cell;
        }
        default:
        { @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:[NSString stringWithFormat:@"Attempted to get cell for unknown section %@", @(indexPath.section)] userInfo:nil]; }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch(indexPath.section)
    {
        case 0:
        {
            NSNumber* time = @(indexPath.row + 1);
            if([self.selectedTimes containsObject:time])
            { [self.selectedTimes removeObject:time]; }
            else
            { [self.selectedTimes addObject:time]; }
            [tableView reloadRowsAtIndexPaths:@[ indexPath ] withRowAnimation:UITableViewRowAnimationFade];
            if([TutorialManager instance].currentTutorialSettings == TutorialSettingsSelectDisruptionTime)
            { [[TutorialManager instance] hideTutorial:TutorialScreenSettings]; }
            break;
        }
        case 1:
        {
            Route* route = self.allRoutes[indexPath.row];
            if([self.selectedRoutes containsObject:route.number])
            { [self.selectedRoutes removeObject:route.number]; }
            else
            { [self.selectedRoutes addObject:route.number]; }
            [tableView reloadRowsAtIndexPaths:@[ indexPath ] withRowAnimation:UITableViewRowAnimationFade];
            if([TutorialManager instance].currentTutorialSettings == TutorialSettingsSelectDisruptionRoute)
            { [[TutorialManager instance] hideTutorial:TutorialScreenSettings]; }
            break;
        }
        default: @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:[NSString stringWithFormat:@"Attempted to tap row for unknown section %@", @(indexPath.section)] userInfo:nil];
    }
}

#pragma mark AlertView Delegate

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(buttonIndex != alertView.cancelButtonIndex)
    { [self.navigationController popViewControllerAnimated:YES]; }
}

@end
