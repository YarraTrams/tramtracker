//
//  PidsService.h
//  tramTRACKER
//
//  Created by Robert Amos on 18/02/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>

/// This type of block is used when all this needed to know is whether a request succeeded, such as a login. Note that the API call may still provide data (as with a login), but unless this data needs to be provided to the calling method, this block should be used instead.
typedef void(^NetworkCompletionBlock)(BOOL successful, NSError* error);
typedef void(^NetworkCompletionBlockWithData)(NSArray* data, NSError* error);

extern NSString * const PIDClientType;
extern NSString * const PIDServiceExpectedVersion;
extern NSString * const PIDServiceURI;
extern NSString * const PIDServiceNamespace;
extern NSInteger const PIDServiceErrorNotReachable;
extern NSInteger const PIDServiceValidationError;
extern NSInteger const PIDServiceValidationErrorStopNotFound;
extern NSInteger const PIDServiceValidationErrorOutletNotFound;
extern NSInteger const PIDServiceValidationErrorRouteNotFound;
extern NSInteger const PIDServiceErrorOnboardNotReachable;
extern NSInteger const PIDServiceErrorTimeoutReached;
extern NSString * const kAppID;

/**
 * A type of action as returned to the delegate in -setUpdates:
**/
typedef NSInteger PIDServiceActionType;

/**
 * An action requiring updating data
**/
extern PIDServiceActionType const PIDServiceActionTypeUpdate;

/**
 * An action requiring deleting data
**/
extern PIDServiceActionType const PIDServiceActionTypeDelete;

@class Route;
@class PidsServiceDelegate;
@class Stop;

/**
 * @defgroup Data Data Retrieval
**/

/**
 * @ingroup Data
**/

/**
 * Methods for calling the PIDS Service. Calls are asynchronous so you need to supply a delegate to receive the results.
**/
@interface PidsService : NSObject
/**
 * The delegate that will receive the results from the service
 **/
@property (nonatomic, strong) id                  delegate;

// And the guid
@property (nonatomic, strong) NSString *guid;
@property (nonatomic, strong) NSNumber *trackerID;
@property (nonatomic, strong) NSNumber *poiID;
@property (nonatomic, strong) NSString *routeNumber;
@property (nonatomic, strong) NSNumber *upDirection;
@property (nonatomic, strong) NSNumber *retailerID;

/**
 * Initiate the Service with its delegate
**/
- (instancetype)initWithDelegate:(id)aDelegate;

/**
 * Clear the delegate that receives the results from the service
 * (Setting is taken care of with @synthesize), use -setDelegate
**/
- (void)clearDelegate;

/**
 * Get the information for a stop
 *
 * Your delegate must implement -setInformation:(Stop *)stop forStop:(NSNumber *)trackerID;
 *
 * @param	stopID		The Tracker ID of a stop
**/
- (void)getInformationForStop:(int)stopID;

- (void)getInformationForRetailer:(int)retailerID;

- (void)getInformationForPOI:(int)poiID;

/**
 * Get a set of predictions for a stop
 *
 * Your delegate must implement -setPredictions:(NSArray *)predictions forStop:(NSNumber *)trackerID;
 *
 * @param	stopID		The Tracker ID of a stop
**/
- (void)getPredictionsForStop:(int)stopID;

/**
 * Get a set of predictions with a specific filter
 *
 * Your delegate must implement -setPredictions:(NSArray *)predictions forStop:(NSNumber *)trackerID;
 *
 * @param	stopID			The tracker ID of a stop
 * @param	route			A Route object to filter against or nil for all routes
 * @param	lowFloorOnly	YES to only show low floor trams, NO to show all trams
**/
- (void)getPredictionsForStop:(int)stopID route:(Route *)route lowFloorOnly:(BOOL)lowFloorOnly;

/**
 * Get a set of scheduled departures for a stop
 *
 * Your delegate must implement -setDepartures:(NSArray *)departures forStop:(NSNumber *)trackerID;
**/
- (void)getScheduledDeparturesForStop:(int)stopID routeNumber:(NSString *)routeNumber atDateTime:(NSDate *)date withLowFloorOnly:(BOOL)lowFloorOnly;


/**
 * Get a set of stops for a scheduled departure
 *
 * Your delegate must implement -setStopsForTrip:(NSArray*)stops;
**/
- (void)getScheduledStopsForTrip:(NSInteger)tripID scheduledDateTime:(NSDate*)date;

- (void)POIsThroughStop:(Stop *)aStop;

/**
 * Get a list of the Routes and the destinations heading in either direction
 *
 * Your delegate must implement -setRouteList:(NSArray *)routeList;
**/
- (void)getRouteList;

/**
 * Get the destination endpoints for a route
 *
 * Your delegate must implement -setDestinations:(NSDictionary *)destinations forRouteNumber:(NSString *)routeNumber
 *
 * @param	route		A Route object to get results for.
**/
- (void)destinationsForRoute:(Route *)route;

/**
 * Get a list of stops for a route and direction
 *
 * your delegate must implement -setStopList:(NSArray *)stops forRoute:(Route *)route;
 *
 * @param	route			A Route object to get results for
 * @param	upDirection		The direction of travel relative to the up direction (YES for up, NO for down)
**/
- (void)getStopListForRoute:(Route *)route upDirection:(BOOL)upDirection;

/**
 * Get journey information for a tram number
 *
 * Your delegate must implement -setJourney:(Journey *)journey forTramNumber:(NSNumber *)tramNumber;
 *
**/
- (void)getJourneyForTramNumber:(NSNumber *)tramNumber;

/**
 * Get the routes through a stop
 *
 * Your delegate must implement -setRoutes:(NSArray *)routes throughStop:(NSNumber *)trackerID
 *
 * @param	stop			A Stop object
**/
- (void)routesThroughStop:(Stop *)stop;

/**
 * Get a List of changed stops and routes since a specified date and time
 *
 * Your delegate must implement -setUpdates:(NSArray *)updates atDate:(NSDate *)atDate;
 *
 * @param	date			A NSDate time to check for updates since
**/
- (void)updatesSinceDate:(NSDate *)date;

/**
 * Send a failure message to the delegate
**/
- (void)failWithError:(NSError *)error;

/**
 * Check to make sure that we have access to the internet
**/
- (BOOL)hasAccessToService;

/**
 * Format a NSDate to how the web service expects it
 *
 * @param	date				The NSDate to format
**/
- (NSString *)formatDateForService:(NSDate *)date;

- (void)cancel;

+ (NSString *)baseURL:(BOOL)webAPI;
- (void)submitNotificationRegistrationRequest:(NSString*)deviceToken UUID:(NSString*)uuid completion:(NetworkCompletionBlock)completion;
- (void)submitNotificationUpdateRequestWithToken:(NSString*)deviceToken routes:(NSArray*)routes times:(NSArray*)times completion:(NetworkCompletionBlock)completion;
- (void)submitNotificationRegistrationDeleteRequestForUUID:(NSString*)uuid completion:(NetworkCompletionBlock)completion;
- (void)submitInfoPageRequest:(NetworkCompletionBlockWithData)completion;
- (void)submitKillSwitchInfoRequest:(NetworkCompletionBlock)completion;
@end
