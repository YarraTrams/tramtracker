//
//  BackgroundSynchroniser.h
//  tramTRACKER
//
//  Created by Robert Amos on 27/07/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BackgroundSynchroniserDelegate.h"

@class PidsService;
@class StopUpdate;
@class RouteUpdate;
@class Stop;
@class Route;
@class TicketOutletService;

extern NSString * const TTSyncKeyStops;
extern NSString * const TTSyncKeyRoutes;
extern NSString * const TTSyncKeyUpdates;
extern NSString * const TTSyncKeyTickets;
extern NSString * const TTSyncKeyPOIs;
extern NSString * const TTSyncKeyMaps;

extern NSInteger TTSyncTypeAutomatic;
extern NSInteger TTSyncTypeAutoCheckManualSync;
extern NSInteger TTSyncTypeManual;


/**
 * The BackgroundSynchroniser class manages the synchronisation process.
 * 
 * @ingroup Sync
 **/
@interface BackgroundSynchroniser : NSObject {

    BOOL synchronisationInProgress;
    
	/**
	 * A reference to the persistent Core Data store coordinator. So that changes can be saved to the database.
	**/
	NSPersistentStoreCoordinator *persistentStoreCoordinator;

	/**
	 * The last successful synchronisation date.
	**/
	NSDate *synchronisationDate;

	/**
	 * An instance of the PidsService class. Used to retrieve updates from the service.
	**/
	PidsService *service;

	/**
	 * A copy of the NSManagedObjectContext used to interface with the database.
	**/
	NSManagedObjectContext *managedObjectContext;

	/**
	 * The Update queue. Update Operations are added to this queue for processing and are removed once complete.
	 * The update queue is processed before the delete queue.
	**/
	NSOperationQueue *queue;

	/**
	 * The Delete queue. Delete operations are added to this queue for processing and are removed once complete.
	 * The delete queue is processed after the completion of the update queue.
	**/
	NSOperationQueue *deleteQueue;
	
	NSOperationQueue *ticketQueue;

	/**
	 * Whether the synchronisation that is occuring is an Automated background sync. YES means it is, NO means that
	 * the synchronisation progress is being presented by a UI.
	**/
	BOOL isAutomatedSync;

	/**
	 * The object that acts as the delegate of the synchronisation process.
	 * This must adopt the BackgroundSynchroniserDelegate protocol.
	**/
	NSObject <BackgroundSynchroniserDelegate> *__weak delegate;

	/**
	 * Whether the synchronisation process has been cancalled. This property is monitored by the process
	 * and synchronisation is cancelled if it is set to NO at any time.
	**/
	BOOL isCancelled;
    BOOL wasAborted;
    
    /* Status variable for updates {POIs, Retailers, Routes & Stops} */

	BOOL    poiChecked;
    NSArray * temporaryPOIUpdateStorage;

	BOOL ticketOutletsChecked;
    NSArray * temporaryOutletsUpdateStorage;

//    BOOL networkMapChecked;
//    NSArray * temporaryNetworkMapUpdates;

	NSArray *temporaryUpdateStorage;
    NSDate *serverTimeOfUpdates;
}

@property (nonatomic, weak) NSObject <BackgroundSynchroniserDelegate> *delegate;
@property (nonatomic, readonly) NSMutableArray *stopsToBeUpdated;
@property (nonatomic, readonly) NSMutableArray *stopsToBeDeleted;
@property (nonatomic, readonly) NSMutableArray *routesThroughStopsToBeUpdated;
@property (nonatomic, readonly) NSMutableArray *routesToBeUpdated;
@property (nonatomic, readonly) NSMutableArray *routesToBeDeleted;
@property (nonatomic, readonly) NSMutableArray *poisThroughStopsToBeUpdated;
@property (nonatomic, readonly) BOOL synchronisationInProgress;
@property (nonatomic) BOOL shouldKeepThreadAlive;

/**
 * Initialises and returns a Background Synchroniser object with a last synchronisation date and a Core Data persistent store coordinator.
 *
 * @param	date				A NSDate indicating the last successful synchronisation date. Is provided to the service to find all updates since that date.
 * @param	storeCoordinator	A Core Data store coordinator, so changes can be saved to the database.
 * @return						Returns an initialised BackgroundSynchroniser object or nil if not successfully initialised.
**/
- (id)initWithLastSynchronisationDate:(NSDate *)date persistentStoreCoordinator:(NSPersistentStoreCoordinator *)storeCoordinator;

/**
 * Starts synchronising in a background thread.
**/
- (void)synchroniseInBackgroundThread;

/**
 * Starts synchronising in a background thread using cached updates from a previous check.
 *
 * @param	params				The NSDictionary as passed to the delegate in the -checkForUpdatesDidFinish message
**/
- (void)synchroniseInBackgroundThread:(NSDictionary *)params;

/**
 * Starts checking for updates in a background thread.
 *
 * @param	params				Unused. To be removed.
 * @todo						Refactor to remove params param.
**/
- (void)checkForUpdatesInBackgroundThread:(NSDictionary *)params;


/**
 * Checks with the PidsService for a list of updates since the last synchronisation date.
**/
- (void)synchronise;

/**
 * Stops the synchronisation process and releases the delegate.
**/
- (void)stop;

/**
 * Cancels the synchronisation process. Tells all active and pending operations about the cancellation.
 * Active operations may not be cancelled immediately if they're busy but will cancel before committing any data.
 * The -syncWasCancelled message will be sent to the delegate once the cancellation has been completed.
**/
- (void)cancel;
- (void)didReceiveMemoryWarning:(NSNotification *)note;

/**
 * Called by the PidsService when it has retrieved a list of available updates. This will process
 * the updates and send through a -checkForUpdatesDidFinish if it was an update-only operation.
 *
 * If it is a synchronisation process it will sort the updates and prepare them for the creation
 * of appropriate operations.
**/
- (void)setUpdates:(NSArray *)updates atDate:(NSDate *)atDate;

/**
 * Data Retrieval - Finds the Stop object for a specified tracker ID.
 * It is done in the thread's context in order to support linking for new objects.
 *
 * @param	aTrackerID			A valid tramTRACKER Tracker ID
 * @return						A Stop object with the matching tracker id, or nil if none found.
**/
- (Stop *)stopForTrackerID:(NSNumber *)aTrackerID;

/**
 * Returns the Stop objects for a specified list of tracker IDs. The Tracker ID will be the key in the NSDictionary.
 * It is done in the thread's context to support linking for new objects.
 *
 * @param	aTrackerIDArray		A NSArray of valid tramTRACKER Tracker IDs
 * @return						A NSDictionary with all matching Stop objects. The Tracker IDs are the keys.
**/
- (NSDictionary *)stopsForTrackerIDs:(NSArray *)aTrackerIDArray;

/**
 * Retrieves the route with the specified internal route number.
 * It is done in the thread's context to support linking for new objects.
 *
 * @param	internalRouteNumber	A string representation of an internal route number.
 * @return						The corresponding Route object, or nil if none found.
**/
- (Route *)routeForInternalRouteNumber:(NSString *)internalRouteNumber;

/**
 * Returns the Route objects for a specified list of internal route numbers. The Internal Route Number will be the key in the NSDictionary.
 * It is done in the thread's context to support linking for new objects.
 *
 * @param	anInternalRouteNumberArray	A NSArray of valid internal route number strings.
 * @return						A NSDictionary with all matching Route objects. The keys are the internal route numbers.
**/
- (NSDictionary *)routesForInternalRouteNumbers:(NSArray *)anInternalRouteNumberArray;

/**
 * Retrieves the Turn object for a specific route, direction and stop combination.
 * This is done in the thread's context to support linking for new objects.
 *
 * @param	aRoute				A Route Object
 * @param	isUpDirection		The direction of travel, YES for up, NO for down
 * @param	aStop				The stop that the turn happens after.
 * @return						A Turn object, or nil if not found
**/
- (Turn *)turnForRoute:(Route *)aRoute upDirection:(BOOL)isUpDirection stop:(Stop *)aStop;

/**
 * Retrieves the Turn objects for a specific route and direction.
 * This is done in the thread's ontext to support linking for new objects.
 *
 * @param	aRoute				A Route object
 * @param	isUpDirection		The direction of travel, YES for up, NO for down
 * @return						An array of Turn objects
**/
- (NSArray *)turnsForRoute:(Route *)aRoute upDirection:(BOOL)isUpDirection;

- (NSManagedObjectContext *)managedObjectContext;

- (BOOL)updateAllContent:(NSString*)jsonString;

/**
 * Iterates over each map's data structure and checks to make sure that that map file is present on the system. If not, it attempts to download it.
 *
**/
- (void)fetchMissingMaps;

@end


