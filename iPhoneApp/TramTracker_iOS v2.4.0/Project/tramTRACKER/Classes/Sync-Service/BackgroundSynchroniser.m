

//
//  BackgroundSynchroniser.m
//  tramTRACKER
//
//  Created by Robert Amos on 27/07/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "BackgroundSynchroniser.h"
#import "PidsService.h"
#import "Stop.h"
#import "Route.h"
#import "StopList.h"
#import "Turn.h"
#import "tramTRACKERAppDelegate.h"
#import "Constants.h"
#import "PointOfInterest.h"
#import "TicketRetailer.h"
#import "PidsServiceDelegate.h"
#import "AFNetworking.h"

NSString * const TTSyncKeyStops = @"stops";
NSString * const TTSyncKeyRoutes = @"routes";
NSString * const TTSyncKeyTickets = @"tickets";
NSString * const TTSyncKeyPOIs = @"pois";
NSString * const TTSyncKeyUpdates = @"updates";
NSString * const TTSyncKeyServerTime = @"serverTime";

NSInteger TTSyncTypeAutomatic = 1;
NSInteger TTSyncTypeAutoCheckManualSync = 2;
NSInteger TTSyncTypeManual = 3;

@implementation BackgroundSynchroniser

@synthesize delegate, stopsToBeUpdated, stopsToBeDeleted, routesToBeUpdated, routesToBeDeleted, routesThroughStopsToBeUpdated, synchronisationInProgress, poisThroughStopsToBeUpdated;

- (id)initWithLastSynchronisationDate:(NSDate *)date persistentStoreCoordinator:(NSPersistentStoreCoordinator *)storeCoordinator
{
	if (self = [super init])
	{
		// retain our objects
		persistentStoreCoordinator = storeCoordinator;
        
		synchronisationDate = date;
		service = [[PidsService alloc] initWithDelegate:self];
        
		queue = [NSOperationQueue new];
		[queue setMaxConcurrentOperationCount:1];
		[queue setSuspended:YES];
		
		deleteQueue = [NSOperationQueue new];
		[deleteQueue setMaxConcurrentOperationCount:1];
		[deleteQueue setSuspended:YES];
        
		ticketQueue = [NSOperationQueue new];
		[ticketQueue setMaxConcurrentOperationCount:1];
		[ticketQueue setSuspended:YES];
		
		// we need to keep a list of those stops and routes that are affected, to make sure
		// we don't update them more than once
		stopsToBeUpdated = [NSMutableArray new];
		stopsToBeDeleted = [NSMutableArray new];
		routesThroughStopsToBeUpdated = [NSMutableArray new];
		routesToBeUpdated = [NSMutableArray new];
		routesToBeDeleted = [NSMutableArray new];
        
        // register for notifications as long as we're around
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveMemoryWarning:) name:UIApplicationDidReceiveMemoryWarningNotification object:nil];
	}
	return self;
}

- (void)stop
{
	[service setDelegate:nil];
}

- (void)dealloc
{
    [service setDelegate:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)synchroniseInBackgroundThread
{
	[self synchroniseInBackgroundThread:nil];
}

- (void)synchroniseInBackgroundThread:(NSDictionary *)params
{
    self.shouldKeepThreadAlive = YES;
    
	// create a pool and run loop
	NSRunLoop *runLoop = [NSRunLoop currentRunLoop];
	isAutomatedSync = YES;
    
	// let everyone know we're starting
	if (!isCancelled && [self.delegate respondsToSelector:@selector(syncDidStart)])
		[self.delegate performSelectorOnMainThread:@selector(syncDidStart) withObject:nil waitUntilDone:NO];
    
    synchronisationInProgress = YES;
    
	// start the synchronisation!
	if (!isCancelled)
	{ [self synchronise]; }
    
    // start the run loop, run as long as there are operations in the queue (and the queue has been setup)
    while (!isCancelled && [queue operationCount] > 0)
    { [runLoop runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]]; }
    
    // then we move onto the delete queue
    
    [deleteQueue setSuspended:NO];
    while (!isCancelled && [deleteQueue operationCount] > 0)
    { [runLoop runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]]; }
    
    // now that that is all finished, update the tickets if necessary
    // start the run loop, run as long as there are operations in the queue (and the queue has been setup)
    [ticketQueue setSuspended:NO];
    while (!isCancelled && [ticketQueue operationCount] > 0)
    { [runLoop runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]]; }
	
	// save all the core data stuff back into place
	if (!isCancelled)
	{
		NSError *error;
		NSManagedObjectContext *context = [self managedObjectContext];
        
        [[context parentContext] performBlockAndWait:^{
            [context.parentContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
        }];
        
        if ([self.delegate respondsToSelector:@selector(syncDidStartSaving)])
        { [self.delegate performSelectorOnMainThread:@selector(syncDidStartSaving) withObject:nil waitUntilDone:NO]; }
		
		if ([context save:&error])
		{
			NSLog([context hasChanges] ? @"Not really saved." : @"Saved successfully.");
            
			if (!isCancelled && [self.delegate respondsToSelector:@selector(syncDidFinishAtTime:)])
            { [self.delegate performSelectorOnMainThread:@selector(syncDidFinishAtTime:) withObject:serverTimeOfUpdates waitUntilDone:NO]; }
        }
        else
        {
			// is it a merging error?
			if ([error code] == NSManagedObjectMergeError)
            { NSLog(@"A Merge Error occured while saving: %@", [[error userInfo] objectForKey:@"conflictList"]); }
			else
            { NSLog(@"An error occured while saving: %@, %@", error, [[error userInfo] objectForKey:NSDetailedErrorsKey]); }
            
			[context rollback];
            
			// send a failure message to the delegate
			if ([self.delegate respondsToSelector:@selector(syncDidFailWithError:)])
            { [self.delegate performSelectorOnMainThread:@selector(syncDidFailWithError:) withObject:error waitUntilDone:NO]; }
		}
	}
    
    
    synchronisationInProgress = NO;
	
	// send out a stop update message
	[[NSNotificationCenter defaultCenter] postNotificationName:TTStopsAndRoutesHaveBeenUpdatedNotice object:nil];
//	
//	NSLog(@"Stopping background thread");
//	CFRunLoopStop([runLoop getCFRunLoop]);
}

- (void)checkForUpdatesInBackgroundThread:(NSDictionary *)params
{
	NSRunLoop *runLoop = [NSRunLoop currentRunLoop];
    
	isAutomatedSync = NO;
    
	// let everyone know we're starting
	if (!isCancelled && [self.delegate respondsToSelector:@selector(checkForUpdatesDidStart)])
		[self.delegate performSelectorOnMainThread:@selector(checkForUpdatesDidStart) withObject:nil waitUntilDone:NO];
	
    synchronisationInProgress = YES;
    
	// start the checking!
	if (!isCancelled)
        [self synchronise];
    
    // start the run loop, run as long as there are operations in the queue (and the queue has been setup)
    while (!isCancelled && [queue operationCount] > 0)
        [runLoop runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    
    synchronisationInProgress = NO;
	
	CFRunLoopStop([runLoop getCFRunLoop]);
}

- (void)synchronise
{
	// check with the PidsService for a list of things that have changed since last time
	[service updatesSinceDate:synchronisationDate];
}

- (void)processUpdateString:(NSString*)jsonString
{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:jsonString forKey:@"updateString"];
    [userDefaults setObject:[NSDate date] forKey:kLastSynchronisationDate];
    [userDefaults synchronize];
}

- (BOOL)updateAllContent:(NSString*)jsonString
{
    NSDictionary* allUpdates = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
    
    NSArray* updatedStops = allUpdates[@"updatedStops"];
    NSArray* deletedStops = allUpdates[@"deletedStops"];
    NSArray* updatedRoutes = allUpdates[@"updatedRoutes"];
    NSArray* deletedRoutes = allUpdates[@"deletedRoutes"];
    NSArray* updatedOutlets = allUpdates[@"updatedTicketOutlets"];
    NSArray* deletedOutlets = allUpdates[@"deletedTicketOutlets"];
    NSArray* updatedPointsOfInterest = allUpdates[@"updatedPoi"];
    NSArray* deletedPointsOfInterest = allUpdates[@"deletedPoi"];
    NSArray* networkMaps = allUpdates[@"addedNetworkMaps"];
    
    BOOL hasNetworkMapUpdates = (networkMaps.count > 0);
    BOOL hasGeneralUpdates = (updatedRoutes.count > 0) || (deletedRoutes.count > 0) || (updatedStops.count > 0) || (deletedStops.count > 0) || (updatedOutlets.count > 0) || (deletedOutlets.count > 0) || (updatedPointsOfInterest.count > 0) || (deletedPointsOfInterest.count > 0);
    
//    NSMutableDictionary* routes = [[self routesForInternalRouteNumbers:@[@"109"]] mutableCopy];
    
    if(hasGeneralUpdates || hasNetworkMapUpdates)
    {
        NSManagedObjectContext* context = [self managedObjectContext];
        if(hasGeneralUpdates)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowUpdateProgress" object:nil];
            NSLog(@"\nUPDATE SUMMARY\n--------------\nRoutes: %d updated, %d deleted\nStops: %d updated, %d deleted\nOutlets: %d updated, %d deleted\nPOIs: %d updated, %d deleted\n",
                (int)updatedRoutes.count,           (int)deletedRoutes.count,
                (int)updatedStops.count,            (int)deletedStops.count,
                (int)updatedOutlets.count,          (int)deletedOutlets.count,
                (int)updatedPointsOfInterest.count, (int)deletedPointsOfInterest.count);
    
            [context performBlockAndWait:^{
                [self updateStops:updatedStops           andDeleteStops:deletedStops];
                [self updateRoutes:updatedRoutes         andDeleteRoutes:deletedRoutes];
                [self updateOutlets:updatedOutlets       andDeleteOutlets:deletedOutlets];
                [self updatePOIs:updatedPointsOfInterest andDeletePOIs:deletedPointsOfInterest];
                [self updateNetworkMaps:networkMaps];
            }];
        }
        else
        {
            [context performBlockAndWait:^{
                [self updateNetworkMaps:networkMaps];
            }];
        }
        
        [[context parentContext] performBlockAndWait:^{
            [context.parentContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
        }];
        
        NSError* error;
        [context save:&error];
        if(error)
        {
            NSLog(@"Error = %@", error.localizedDescription);
            return NO;
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"updateString"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        tramTRACKERAppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
        [appDelegate saveContext];
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSyncFinished object:nil];
    }
    else
    {
        NSLog(@"\nUPDATE SUMMARY\nNo updates to be installed.");
        if(networkMaps.count > 0)
        { [self updateNetworkMaps:networkMaps]; }
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"updateString"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    return YES;
}

- (void)cancel
{
	// Cancel any active operations
	if (queue != nil && [[queue operations] count] > 0)
		[queue cancelAllOperations];
	
	if (deleteQueue != nil && [[deleteQueue operations] count] > 0)
		[deleteQueue cancelAllOperations];
    
	if (ticketQueue != nil && [[ticketQueue operations] count] > 0)
		[ticketQueue cancelAllOperations];
	
	if ([self.delegate respondsToSelector:@selector(syncWasCancelled)])
		[self.delegate performSelectorOnMainThread:@selector(syncWasCancelled) withObject:nil waitUntilDone:NO];
	
    synchronisationInProgress = NO;
	isCancelled = YES;
}

- (void)didReceiveMemoryWarning:(NSNotification *)note
{
    if (wasAborted)
        return;
    
	// Cancel any active operations
	if (queue != nil && [[queue operations] count] > 0)
		[queue cancelAllOperations];
	
	if (deleteQueue != nil && [[deleteQueue operations] count] > 0)
		[deleteQueue cancelAllOperations];
    
	if (ticketQueue != nil && [[ticketQueue operations] count] > 0)
		[ticketQueue cancelAllOperations];
	
	if ([self.delegate respondsToSelector:@selector(syncWasAbortedDueToMemoryWarning)])
		[self.delegate performSelectorOnMainThread:@selector(syncWasAbortedDueToMemoryWarning) withObject:nil waitUntilDone:NO];
	
	synchronisationInProgress = NO;
    isCancelled = YES;
    wasAborted = YES;
}

// Indicates a failure of some sort.
- (void)pidsServiceDidFailWithError:(NSError *)error
{
	// send a failure message to the delegate
	if ([self.delegate respondsToSelector:@selector(syncDidFailWithError:)])
    { [self.delegate performSelectorOnMainThread:@selector(syncDidFailWithError:) withObject:error waitUntilDone:NO]; }
}


#pragma mark -
#pragma mark Data Retrieval

- (Stop *)stopForTrackerID:(NSNumber *)aTrackerID
{
	NSManagedObjectContext *context = [self managedObjectContext];
	NSFetchRequest *fetchRequest = [NSFetchRequest new];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Stop" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"trackerID == %@", aTrackerID];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (!fetchedObjects.count)
		return nil;
    
	return fetchedObjects .firstObject;
}

- (NSDictionary *)stopsForTrackerIDs:(NSArray *)aTrackerIDArray
{
	// Grab our managed object context and create a fetch request
	NSManagedObjectContext *context = [self managedObjectContext];
	NSFetchRequest *fetchRequest = [NSFetchRequest new];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Stop" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"trackerID IN %@", aTrackerIDArray];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (!fetchedObjects.count)
		return nil;
	
	// return the object
	
	NSMutableDictionary *list = [NSMutableDictionary new];
    
	for (Stop *s in fetchedObjects)
		[list setObject:s forKey:s.trackerID];
	return list;
}

- (NSDictionary *)poisForTrackerIDs:(NSArray *)aTrackerIDArray
{
	// Grab our managed object context and create a fetch request
	NSManagedObjectContext *context = [self managedObjectContext];
	NSFetchRequest *fetchRequest = [NSFetchRequest new];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"PointOfInterest" inManagedObjectContext:context];
    
	[fetchRequest setEntity:entity];
    
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"poiID IN %@", aTrackerIDArray];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (!fetchedObjects.count)
		return nil;
	
	// return the object
	
	NSMutableDictionary *list = [NSMutableDictionary new];
	for (PointOfInterest *s in fetchedObjects)
		[list setObject:s forKey:s.poiID];
	return list;
}

- (NSDictionary *)retailersForIDs:(NSArray *)aTrackerIDArray
{
	// Grab our managed object context and create a fetch request
	NSManagedObjectContext *context = [self managedObjectContext];
	NSFetchRequest *fetchRequest = [NSFetchRequest new];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"TicketRetailer" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
    
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"retailerID IN %@", aTrackerIDArray];
	[fetchRequest setPredicate:predicate];
    
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (!fetchedObjects.count)
		return nil;
	
	// return the object
	
	NSMutableDictionary *list = [NSMutableDictionary new];
	for (TicketRetailer *s in fetchedObjects)
		[list setObject:s forKey:s.retailerID];
	return list;
}

- (Route *)routeForInternalRouteNumber:(NSString *)internalRouteNumber
{
	NSManagedObjectContext *context = [self managedObjectContext];
	NSFetchRequest *fetchRequest = [NSFetchRequest new];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Route" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"internalNumber == %@", internalRouteNumber];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (!fetchedObjects.count)
    { return nil; }
	
	// return the object
	return fetchedObjects.firstObject;
}

- (NSDictionary *)routesForInternalRouteNumbers:(NSArray *)anInternalRouteNumberArray
{
	// Grab our managed object context and create a fetch request
	NSManagedObjectContext *context = [self managedObjectContext];
	NSFetchRequest *fetchRequest = [NSFetchRequest new];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Route" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"internalNumber IN %@", anInternalRouteNumberArray];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (!fetchedObjects.count)
    { return nil; }
    
	// return the object
	NSMutableDictionary *list = [NSMutableDictionary new];
	for (Route *r in fetchedObjects)
    {
        if(list[r.internalNumber] == nil)
        { list[r.internalNumber] = r; }
        else
        {
            Route* existingDuplicate = list[r.internalNumber];
            [[self managedObjectContext] deleteObject:existingDuplicate];
            [self deleteTurnsForRoute:existingDuplicate];
            [self deleteStopsForRoute:existingDuplicate];
            
            [[self managedObjectContext] deleteObject:r];
            [self deleteTurnsForRoute:r];
            [self deleteStopsForRoute:r];
            list[r.internalNumber] = nil;
        }
    }
    
	return list;
}

- (Turn *)turnForRoute:(Route *)aRoute upDirection:(BOOL)isUpDirection stop:(Stop *)aStop
{
	// and build the predicate
	Stop *xStop = [self stopForTrackerID:aStop.trackerID];
	Route *xRoute = [self routeForInternalRouteNumber:aRoute.internalNumber];
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"upDirection == %@ AND stop == %@", [NSNumber numberWithBool:isUpDirection], xStop];
	return [[xRoute.turns filteredSetUsingPredicate:predicate] anyObject];
}

- (NSArray *)turnsForRoute:(Route *)aRoute upDirection:(BOOL)isUpDirection
{
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"upDirection == %@", @(isUpDirection)];
	return [[aRoute.turns filteredSetUsingPredicate:predicate] allObjects];
}

- (NSManagedObjectContext *)managedObjectContext
{
    if (managedObjectContext != nil)
        return managedObjectContext;
	
    NSPersistentStoreCoordinator *coordinator = persistentStoreCoordinator;
    if (coordinator != nil)
    {
        tramTRACKERAppDelegate * d = [[UIApplication sharedApplication] delegate];
        
        managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        //        [managedObjectContext setPersistentStoreCoordinator: coordinator];
        managedObjectContext.parentContext = d.managedObjectContext;
    }
    return managedObjectContext;
}

#pragma mark Routes

- (void)updateRoutes:(NSArray*)updateList andDeleteRoutes:(NSArray*)deleteList
{
    NSManagedObjectContext* context = [self managedObjectContext];
    NSMutableArray* changedIDs = [NSMutableArray new];
    for(NSDictionary* update in updateList)
//    { [changedIDs addObject:update[@"routeNo"]]; } // Requesting by number sometimes fails to properly request the item from the database
    { [changedIDs addObject:[update[@"routeNo"] stringValue]]; }
    for(NSDictionary* delete in deleteList)
//    { [changedIDs addObject:delete[@"routeNo"]]; } // Requesting by number sometimes fails to properly request the item from the database
    { [changedIDs addObject:[delete[@"routeNo"] stringValue]]; }
    NSMutableDictionary* routes = [[self routesForInternalRouteNumbers:changedIDs] mutableCopy];
    
    for(NSDictionary* update in updateList)
    {
        Route* route = routes[[update[@"routeNo"] stringValue]];
        if(route == nil)
        {
            route = [NSEntityDescription insertNewObjectForEntityForName:@"Route" inManagedObjectContext:context];
            route.number = [update[@"routeNo"] stringValue];
            route.internalNumber = [update[@"routeNo"] stringValue];
        }
        
        route.headboardNumber = update[@"headboardNo"];
        route.upDestination = update[@"upDestination"];
        route.downDestination = update[@"downDestination"];
        route.subRoute = ![update[@"isMainRoute"] boolValue];
        route.name = [NSString stringWithFormat:@"%@ to %@", route.upDestination, route.downDestination];
        if(![update[@"colour"] isKindOfClass:[NSNull class]])
        { route.colour = update[@"colour"]; }
        
        NSMutableArray* allStops = [NSMutableArray array];
        NSArray* upStopsJson = update[@"upStops"];
        NSArray* downStopsJson = update[@"downStops"];
        for(NSDictionary* stopData in upStopsJson)
        { [allStops addObject:stopData[@"trackerId"]]; }
        for(NSDictionary* stopData in downStopsJson)
        { [allStops addObject:stopData[@"trackerId"]]; }
        
        NSDictionary* stops = [self stopsForTrackerIDs:allStops];
        NSMutableArray* upStops = [NSMutableArray array];
        NSMutableArray* downStops = [NSMutableArray array];
        for(NSDictionary* stopData in upStopsJson)
        {
            Stop* stop = stops[stopData[@"trackerId"]];
            if(stop == nil)
            { stop = [self createStopAndAddToDatabase:stopData forRoute:route.number]; }
            [upStops addObject:stop.trackerID];
            
            if(!([stop.routesThroughStop containsObject:route.number]))
            {
                NSMutableArray* routesThroughStopMutable = [stop.routesThroughStop mutableCopy];
                [routesThroughStopMutable addObject:route.number];
                stop.routesThroughStop = routesThroughStopMutable;
            }
        }
        for(NSDictionary* stopData in downStopsJson)
        {
            Stop* stop = stops[stopData[@"trackerId"]];
            if(stop == nil)
            { stop = [self createStopAndAddToDatabase:stopData forRoute:route.number]; }
            [downStops addObject:stop.trackerID];
            
            if(!([stop.routesThroughStop containsObject:route.number]))
            {
                NSMutableArray* routesThroughStopMutable = [stop.routesThroughStop mutableCopy];
                [routesThroughStopMutable addObject:route.number];
                stop.routesThroughStop = routesThroughStopMutable;
            }
        }
        
        route.upStops = upStops;
        route.downStops = downStops;
    }
    
    for(NSDictionary* delete in deleteList)
    {
        NSString* routeNumber = [NSString stringWithFormat:@"%@", delete[@"routeNo"]];
        Route* route = routes[routeNumber];
        if(route != nil)
        {
            [[self managedObjectContext] deleteObject:route];
            [self deleteTurnsForRoute:route];
            [self deleteStopsForRoute:route];
        }
        else if([self.delegate respondsToSelector:@selector(syncDidRemoveOperation)])
        { [self.delegate performSelectorOnMainThread:@selector(syncDidRemoveOperation) withObject:nil waitUntilDone:NO]; }
    }
}

- (void)updateStopsForRoute:(Route*)route json:(NSDictionary*)json
{
    NSArray* upStopsJson = json[@"upStops"];
    NSArray* downStopsJson = json[@"downStops"];
    
    NSMutableArray* stopIDs   = [NSMutableArray new];
    
    for(NSDictionary* stop in upStopsJson)
    { [stopIDs addObject:stop[@"trackerId"]]; }
    for(NSDictionary* stop in downStopsJson)
    { [stopIDs addObject:stop[@"trackerId"]]; }
    
    NSDictionary* existingStops = [self stopsForTrackerIDs:stopIDs];
    
    NSMutableArray* completeUpStopList   = [NSMutableArray array];
    NSMutableArray* completeDownStopList = [NSMutableArray array];
    
    for(NSDictionary* stopJson in upStopsJson)
    {
        Stop* stop = existingStops[stopJson[@"trackerId"]];
        [completeUpStopList addObject:stop];
    }
    
    for(NSDictionary* stopJson in downStopsJson)
    {
        Stop* stop = existingStops[stopJson[@"trackerId"]];
        [completeDownStopList addObject:stop];
    }
    
    route.upStops = completeUpStopList;
    route.downStops = completeDownStopList;
}

- (void)updateTurnsForRoute:(Route*)route json:(NSDictionary*)json
{
    NSManagedObjectContext* context = [self managedObjectContext];
    [self deleteTurnsForRoute:route]; // when updating a route, we don't know which turns no longer apply, so delete all the current ones. We can can re-create the requisite ones as required.
    
    NSArray* upStopsJson   = json[@"upStops"];
    NSArray* downStopsJson = json[@"downStops"];
    
    NSMutableArray* stopIDs = [NSMutableArray new];
    
    for(NSDictionary* stopJson in upStopsJson)
    { [stopIDs addObject:stopJson[@"trackerId"]]; }
    for(NSDictionary* stopJson in downStopsJson)
    { [stopIDs addObject:stopJson[@"trackerId"]]; }
    
    NSDictionary* stops = [self stopsForTrackerIDs:stopIDs];
    
    for(NSDictionary* stopJson in upStopsJson)
    {
        if([stopJson[@"turnType"] isKindOfClass:[NSNull class]] || [stopJson[@"turnMessage"] isKindOfClass:[NSNull class]])
        { continue; }
        
        Turn* turn = [NSEntityDescription insertNewObjectForEntityForName:@"Turn" inManagedObjectContext:context];
        turn.route = route;
        turn.stop  = stops[@([stopJson[@"trackerId"] integerValue])];
        [turn setTypeFromString:stopJson[@"turnType"]];
        turn.message = stopJson[@"turnMessage"];
        turn.upDirection = true;
    }
    
    for(NSDictionary* stopJson in downStopsJson)
    {
        if([stopJson[@"turnType"] isKindOfClass:[NSNull class]] || [stopJson[@"turnMessage"] isKindOfClass:[NSNull class]])
        { continue; }
        
        Turn* turn = [NSEntityDescription insertNewObjectForEntityForName:@"Turn" inManagedObjectContext:context];
        turn.route = route;
        turn.stop  = stops[@([stopJson[@"trackerId"] integerValue])];
        [turn setTypeFromString:stopJson[@"turnType"]];
        turn.message = stopJson[@"turnMessage"];
        turn.upDirection = false;
    }
}

- (void)deleteStopsForRoute:(Route*)route
{
    NSMutableArray* changedIDs = [NSMutableArray array];
    
    for(NSNumber* stopId in route.upStops)
    { [changedIDs addObject:stopId]; }
    for(NSNumber* stopId in route.downStops)
    { [changedIDs addObject:stopId]; }
    
    NSDictionary* stops = [self stopsForTrackerIDs:changedIDs];
    
    for(NSNumber* stopId in route.upStops)
    {
        Stop* stop = stops[stopId];
        NSMutableArray* routesThroughStop = [stop.routesThroughStop mutableCopy];
        [routesThroughStop removeObject:route.internalNumber];
        if(routesThroughStop == nil)
        { NSLog(@"Attempted to get routes through downwards stop %@ (trackerID: %@), none found", stop.name, stop.trackerID); }
        stop.routesThroughStop = [routesThroughStop copy];
    }
    for(NSNumber* stopId in route.downStops)
    {
        Stop* stop = stops[stopId];
        NSMutableArray* routesThroughStop = [stop.routesThroughStop mutableCopy];
        [routesThroughStop removeObject:route.internalNumber];
        if(routesThroughStop == nil)
        { NSLog(@"Attepted to get routes through upwards stop %@ (trackerID: %@), none found", stop.name, stop.trackerID); }
        stop.routesThroughStop = [routesThroughStop copy];
    }
}

- (void)deleteTurnsForRoute:(Route*)route
{
    NSManagedObjectContext* context = [self managedObjectContext];
    for(Turn* turn in route.turns)
    { [context deleteObject:turn]; }
}

#pragma mark Stops

- (void)updateStops:(NSArray*)updateList andDeleteStops:(NSArray*)deleteList
{
    /// Fetch all the stops that need to be changed from the database
    NSManagedObjectContext* context = [self managedObjectContext];
    NSMutableArray* changedIDs = [NSMutableArray new];
    for(NSDictionary* update in updateList)
    { [changedIDs addObject:update[@"trackerId"]]; }
    for(NSDictionary* delete in deleteList)
    { [changedIDs addObject:delete[@"trackerId"]]; }
    NSDictionary* stops = [self stopsForTrackerIDs:changedIDs];
    
    for(NSDictionary* update in updateList)
    {
        Stop* stop = stops[update[@"trackerId"]];
        
        if(!stop)
        {
            stop = [NSEntityDescription insertNewObjectForEntityForName:@"Stop" inManagedObjectContext:context];
            stop.trackerID = update[@"trackerId"];
            [stop setTurns:nil];
            [stop setRoutesThroughStop:@[]];
            [stop setPoisThroughStop:@[]];
        }
        
        stop.cityDirection = update[@"direction"];
        stop.cityStop      = update[@"isCityStop"];
        stop.easyAccess    = update[@"isEasyAccess"];
        stop.ftzStop       = update[@"isInFreeZone"];
        stop.shelter       = update[@"isShelter"];
        stop.latitude      = update[@"latitude"];
        stop.longitude     = update[@"longitude"];
        stop.name          = update[@"stopName"];
        stop.length        = @0;
        stop.platformStop  = @NO;
        
        stop.connectingBuses      = @(![update[@"connectingBuses"] isKindOfClass:[NSNull class]]);
        stop.connectingBusesList  = [stop.connectingBuses isEqualToNumber:@(YES)] ? update[@"connectingBuses"] : nil;
        stop.connectingTrains     = @(![update[@"connectingTrains"] isKindOfClass:[NSNull class]]);
        stop.connectingTrainsList = [stop.connectingTrains isEqualToNumber:@(YES)] ? update[@"connectingTrains"] : nil;
        stop.connectingTrams      = @(![update[@"connectingTrams"] isKindOfClass:[NSNull class]]);
        stop.connectingTramsList  = [stop.connectingTrams isEqualToNumber:@(YES)] ? update[@"connectingTrams"] : nil;
        
        stop.suburbName = update[@"suburbName"];
        stop.number = update[@"flagStopNo"];
        
        if([update[@"zones"] isKindOfClass:[NSString class]])
        {
            NSMutableArray* zones = [NSMutableArray array];
            NSArray* zonesList = [update[@"zones"] componentsSeparatedByString:@","];
            for (NSString* zone in zonesList)
            { [zones addObject:@([zone integerValue])]; }
            [stop setMetcardZones:zones];
        }
    }
    
    /// Delete the stops that are flagged as needing to be removed.
    for(NSDictionary* delete in deleteList)
    {
        Stop* stop = stops[delete[@"trackerId"]];
        if(stop != nil)
        {
//            [self deleteStopFromRoutes:stop]; // This one should not be required. If a stop is removed, the route update should handle it being erased.
            [self deleteStopFromRetailers:stop];
            [self deleteStopFromPOIs:stop];
            [self deleteTurnsForStop:stop];
            [self deleteStopFromFavourites:stop];
            [[self managedObjectContext] deleteObject:stop];
        }
        else if([self.delegate respondsToSelector:@selector(syncDidRemoveOperation)])
        { [self.delegate performSelectorOnMainThread:@selector(syncDidRemoveOperation) withObject:nil waitUntilDone:NO]; }
    }
}

- (void)deleteStopFromFavourites:(Stop*)stop
{
    BOOL didChange = NO;
    NSMutableArray* favouriteStopList = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"favouriteStopsList"] mutableCopy];
    
    NSNumberFormatter* formatter = [NSNumberFormatter new];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    for(int i = 0; i < favouriteStopList.count; i++)
    {
        NSDictionary* favouriteStop = favouriteStopList[i];
        NSNumber* trackerId = favouriteStop[@"trackerid"];
        if([trackerId isEqualToNumber:stop.trackerID])
        {
            didChange = YES;
            [favouriteStopList removeObjectAtIndex:i];
            i--;
        }
    }
    
    if(didChange)
    {
        [[NSUserDefaults standardUserDefaults] setObject:favouriteStopList forKey:@"favouriteStopsList"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
}

- (void)deleteStopFromRetailers:(Stop*)stop
{
    for(TicketRetailer* retailer in stop.nearestRetailers)
    {
        NSMutableOrderedSet* nearestStops = [retailer.nearestStops mutableCopy];
        [nearestStops removeObject:stop];
        retailer.nearestStops = nearestStops;
    }
}

- (void)deleteStopFromPOIs:(Stop*)stop
{
    NSDictionary* POIs = [self poisForTrackerIDs:stop.poisThroughStop];
    
    for(PointOfInterest* poi in POIs.allValues)
    {
        NSMutableArray* stopsThroughPOI = [poi.stopsThroughPOI mutableCopy];
        [stopsThroughPOI removeObject:[stop.trackerID stringValue]];
        poi.stopsThroughPOI = stopsThroughPOI;
    }
}

- (void)deleteTurnsForStop:(Stop*)stop
{
    NSManagedObjectContext* context = [self managedObjectContext];
    for(Turn* turn in stop.turns)
    { [context deleteObject:turn]; } // because each turn consists of a route and a stop, any turn referring to this stop should be erased in it's entirety.
}

- (Stop*)createStopAndAddToDatabase:(NSDictionary*)stopData forRoute:(NSString*)routeNumber
{
    Stop* stop = [NSEntityDescription insertNewObjectForEntityForName:@"Stop" inManagedObjectContext:[self managedObjectContext]];
    stop.trackerID = stopData[@"trackerId"];
    [stop setTurns:nil];
    [stop setRoutesThroughStop:@[routeNumber]];
    [stop setPoisThroughStop:@[]];
    
    stop.cityDirection = stopData[@"direction"];
    stop.cityStop      = stopData[@"isCityStop"];
    stop.easyAccess    = stopData[@"isEasyAccess"];
    stop.ftzStop       = stopData[@"isInFreeZone"];
    stop.shelter       = stopData[@"isShelter"];
    stop.latitude      = stopData[@"latitude"];
    stop.longitude     = stopData[@"longitude"];
    stop.name          = stopData[@"stopName"];
    stop.length        = @0;
    stop.platformStop  = @NO;
    
    stop.connectingBuses      = @(![stopData[@"connectingBuses"] isKindOfClass:[NSNull class]]);
    stop.connectingBusesList  = [stop.connectingBuses isEqualToNumber:@(YES)] ? stopData[@"connectingBuses"] : nil;
    stop.connectingTrains     = @(![stopData[@"connectingTrains"] isKindOfClass:[NSNull class]]);
    stop.connectingTrainsList = [stop.connectingTrains isEqualToNumber:@(YES)] ? stopData[@"connectingTrains"] : nil;
    stop.connectingTrams      = @(![stopData[@"connectingTrams"] isKindOfClass:[NSNull class]]);
    stop.connectingTramsList  = [stop.connectingTrams isEqualToNumber:@(YES)] ? stopData[@"connectingTrams"] : nil;
    
    stop.suburbName = stopData[@"suburbName"];
    stop.number = stopData[@"flagStopNo"];
    
    if([stopData[@"zones"] isKindOfClass:[NSString class]])
    {
        NSMutableArray* zones = [NSMutableArray array];
        NSArray* zonesList = [stopData[@"zones"] componentsSeparatedByString:@","];
        for (NSString* zone in zonesList)
        { [zones addObject:@([zone integerValue])]; }
        [stop setMetcardZones:zones];
    }
    return stop;
}

#pragma mark POIs

- (void)updatePOIs:(NSArray*)updateList andDeletePOIs:(NSArray*)deleteList
{
    NSManagedObjectContext* context = [self managedObjectContext];
    NSMutableArray* changedIDs = [NSMutableArray new];
    for(NSDictionary* update in updateList)
    { [changedIDs addObject:update[@"poiId"]]; }
    for(NSDictionary* delete in deleteList)
    { [changedIDs addObject:delete[@"id"]]; }
    NSDictionary* pointsOfInterest = [self poisForTrackerIDs:changedIDs];
    
    for(NSDictionary* update in updateList)
    {
        PointOfInterest* poi = pointsOfInterest[update[@"poiId"]];
        if(poi == nil)
        {
            poi = [NSEntityDescription insertNewObjectForEntityForName:@"PointOfInterest" inManagedObjectContext:context];
            poi.poiID = update[@"poiId"];
        }
        
        poi.categoryName = update[@"categoryName"];
        poi.disabledAccess = update[@"disabledAccess"];
        if(![update[@"emailAddress"] isKindOfClass:[NSNull class]])
        { poi.emailAddress = update[@"emailAddress"]; }
        poi.hasEntryFee = update[@"hasEntryFee"];
        poi.hasToilets = update[@"hasToilets"];
        poi.latitude = update[@"latitude"];
        poi.longitude = update[@"longitude"];
        if(![update[@"moreInfo"] isKindOfClass:[NSNull class]])
        { poi.moreInfo = update[@"moreInfo"]; }
        if(![update[@"name"] isKindOfClass:[NSNull class]])
        { poi.name = update[@"name"]; }
        if(![update[@"openingHours"] isKindOfClass:[NSNull class]])
        { poi.openingHours = update[@"openingHours"]; }
        if(![update[@"poiDescription"] isKindOfClass:[NSNull class]])
        { poi.poiDescription = update[@"poiDescription"]; }
        if(![update[@"phoneNumber"] isKindOfClass:[NSNull class]])
        { poi.phoneNumber = update[@"phoneNumber"]; }
        if(![update[@"streetAddress"] isKindOfClass:[NSNull class]])
        { poi.streetAddress = update[@"streetAddress"]; }
        if(![update[@"suburb"] isKindOfClass:[NSNull class]])
        { poi.suburb = update[@"suburb"]; }
        if(![update[@"webAddress"] isKindOfClass:[NSNull class]])
        { poi.webAddress = update[@"webAddress"]; }
        
        NSMutableArray* stopsList = [NSMutableArray array];
        for(NSDictionary* stopJson in update[@"stops"])
        { [stopsList addObject:stopJson[@"stopNo"]]; }
        poi.stopsThroughPOI = stopsList;
        NSDictionary* stops = [self stopsForTrackerIDs:stopsList];
        for(Stop* stop in stops.allValues)
        {
            NSMutableArray* poisThroughStop;
            if(stop.poisThroughStop == nil)
            { poisThroughStop = [NSMutableArray array]; }
            else
            { poisThroughStop = [stop.poisThroughStop mutableCopy]; }
            if(![poisThroughStop containsObject:poi.poiID])
            { [poisThroughStop addObject:poi.poiID]; }
            stop.poisThroughStop = [poisThroughStop copy];
        }
    }
    
    for(NSDictionary* delete in deleteList)
    {
        PointOfInterest* poi = pointsOfInterest[delete[@"id"]];
        if(poi != nil)
        {
            [[self managedObjectContext] deleteObject:poi];
            [self deletePOIFromStops:(PointOfInterest*)poi];
        }
        else if([self.delegate respondsToSelector:@selector(syncDidRemoveOperation)])
        { [self.delegate performSelectorOnMainThread:@selector(syncDidRemoveOperation) withObject:nil waitUntilDone:NO]; }
    }
}

- (void)deletePOIFromStops:(PointOfInterest*)poi
{
    NSDictionary* stops = [self stopsForTrackerIDs:poi.stopsThroughPOI];
    for(Stop* stop in stops.allValues)
    {
        NSMutableArray* poisThroughStop = [stop.poisThroughStop mutableCopy];
        [poisThroughStop removeObject:[poi.poiID stringValue]];
        stop.poisThroughStop = poisThroughStop;
    }
}

#pragma mark Outlets

- (void)updateOutlets:(NSArray*)updateList andDeleteOutlets:(NSArray*)deleteList
{
    NSManagedObjectContext* context = [self managedObjectContext];
    NSMutableArray* changedIDs = [NSMutableArray new];
    for(NSDictionary* update in updateList)
    { [changedIDs addObject:update[@"id"]]; }
    for(NSDictionary* delete in deleteList)
    { [changedIDs addObject:delete[@"id"]]; }
    NSDictionary* outlets = [self retailersForIDs:changedIDs];
    
    for(NSDictionary* update in updateList)
    {
        TicketRetailer* outlet = outlets[update[@"id"]];
        if(!outlet)
        {
            outlet = [NSEntityDescription insertNewObjectForEntityForName:@"TicketRetailer" inManagedObjectContext:context];
            outlet.retailerID = update[@"id"];
        }
        
        outlet.address      = update[@"address"];
        outlet.sellsMyki    = update[@"hasMyki"];
        outlet.hasMykiTopUp = update[@"hasMykiTopUp"];
        outlet.open24Hour   = update[@"is24Hour"];
        outlet.latitude     = update[@"latitude"];
        outlet.longitude    = update[@"longitude"];
        outlet.name         = update[@"name"];
        outlet.suburb       = update[@"suburb"];
    }
    
    for(NSDictionary* delete in deleteList)
    {
        TicketRetailer* outlet = outlets[delete[@"id"]];
        if(outlet != nil)
        {
            [self deleteRetailerFromStops:outlet];
            [[self managedObjectContext] deleteObject:outlet];
        }
        else if([self.delegate respondsToSelector:@selector(syncDidRemoveOperation)])
        { [self.delegate performSelectorOnMainThread:@selector(syncDidRemoveOperation) withObject:nil waitUntilDone:NO]; }
    }
}

- (void)deleteRetailerFromStops:(TicketRetailer*)retailer
{
    for(Stop* stop in retailer.nearestStops)
    {
        NSMutableOrderedSet* retailersForStop = [stop.nearestRetailers mutableCopy];
        [retailersForStop removeObject:retailer];
        stop.nearestRetailers = retailersForStop;
    }
}

#pragma mark SetUpdates

- (void)setTicketOutletsUpdates:(NSArray *)ticketOutletUpdates
{
    ticketOutletsChecked = YES;
    temporaryOutletsUpdateStorage = ticketOutletUpdates;
    
    if (poiChecked && temporaryUpdateStorage)
        [self setUpdates:temporaryUpdateStorage atDate:serverTimeOfUpdates];
    else
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

- (void)fetchMissingMaps
{
    NSArray* existingMaps = [[NSUserDefaults standardUserDefaults] objectForKey:@"maps"];
    if(!existingMaps)
    { return; }
    
    for(NSDictionary* mapData in existingMaps)
    {
        NSString* filename;
        if(mapData[@"FileName"])
        { filename = mapData[@"FileName"]; }
        else if(mapData[@"fileName"])
        { filename = mapData[@"fileName"]; }
        
        if(filename)
        {
            BOOL existsAtPath = [[NSFileManager defaultManager] fileExistsAtPath:filename];
            BOOL existsAtExtendedPath = [[NSFileManager defaultManager] fileExistsAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingFormat:@"/%@", filename]];
            
            if(!existsAtPath && !existsAtExtendedPath)
            {
                // map is not on the system. It needs to be downloaded.
                [self downloadAndSaveMap:mapData];
            }
        }
    }
}

- (void)updateNetworkMaps:(NSArray*)networkMaps
{
//    
//    networkMapUpdates = @[@{@"ActiveDate" : @"/Date(1420936409000+1100)/",
//                            @"FileName" : @"hey.png",
//                            @"Url" : @"http://extranetdev.yarratrams.com.au/TTMetaContent/assets/network_map_201412190344.png"
//                            }];
    
    if(networkMaps.count == 0)
    { return; }
    
    NSMutableArray* existingMaps = [[[NSUserDefaults standardUserDefaults] objectForKey:@"maps"] mutableCopy];
    if(existingMaps == nil)
    { existingMaps = [NSMutableArray array]; }
    NSMutableArray* filenamesToDelete = [NSMutableArray array];
    if(existingMaps)
    {
        for(NSDictionary* existingMapData in existingMaps)
        {
            NSString* filename;
            if(existingMapData[@"FileName"])
            { filename = existingMapData[@"FileName"]; }
            else if(existingMapData[@"fileName"])
            { filename = existingMapData[@"fileName"]; }
            
            if(filename)
            {
                BOOL existsAtPath = [[NSFileManager defaultManager] fileExistsAtPath:filename];
                BOOL existsAtExtendedPath = [[NSFileManager defaultManager] fileExistsAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingFormat:@"/%@", filename]];
                
                if(existsAtPath || existsAtExtendedPath)
                {
                    BOOL existsInUpdates = NO;
                    for(NSDictionary* newMapData in networkMaps)
                    {
                        if([newMapData[@"fileName"] isEqualToString:filename])
                        {
                            existsInUpdates = YES;
                            break;
                        }
                    }
                    if(!existsInUpdates)
                    {
                        // file exists on system, but not in the newly returned network maps. Delete it.
                        [filenamesToDelete addObject:filename];
                    }
                }
            }
        }
    }
    
    
    
    NSMutableArray * newMapsUpdates = [NSMutableArray new];
    
    for (NSDictionary *dic in networkMaps) {
        
        NSString* mapFile = [[tramTRACKERAppDelegate applicationDocumentsDirectory] stringByAppendingPathComponent:dic[@"fileName"]];
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:mapFile] &&
            ![[NSFileManager defaultManager] fileExistsAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingFormat:@"/%@", dic[@"FileName"]]]) {
            
            [newMapsUpdates addObject:dic];
        }
    }
    
    if (newMapsUpdates.count == 0)
    { return; }
    
    for(NSDictionary* filenameToDelete in filenamesToDelete)
    {
        NSLog(@"Should be deleting a file here: %@", filenameToDelete);
    }
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        for (NSDictionary * map in newMapsUpdates) {
            NSDictionary* newData = [self downloadAndSaveMap:map];
            if(newData)
//            { [newMapsUD addObject:newData]; }
            {
                [existingMaps addObject:newData];
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [[NSUserDefaults standardUserDefaults] setObject:[existingMaps copy] forKey:@"maps"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        });
        
    });
    

}



- (NSDictionary*)downloadAndSaveMap:(NSDictionary*)map
{
    NSString * url = map[@"url"];
    NSLog(@"Downloading: %@", url);
    NSURLResponse* response = nil;
    NSURLRequest * request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
//    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.google.com/lajhsdjkfhakjlsdf"]];
    NSError *error = nil;
    NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if([response isKindOfClass:[NSHTTPURLResponse class]])
    {
        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
        if(httpResponse.statusCode == 404)
        { error = [NSError errorWithDomain:@"General" code:404 userInfo:nil]; }
    }
    
    if (!error && data) {
        NSLog(@"Downloaded: %@", url);
        NSString * filename = map[@"FileName"];
        if(filename == nil)
        { filename = map[@"fileName"]; }
        
        NSString * path = [[tramTRACKERAppDelegate applicationDocumentsDirectory] stringByAppendingPathComponent:filename];
        
        NSError* writeError;
        [data writeToFile:path options:NSDataWritingAtomic error:&writeError];
        
        if(writeError)
        { NSLog(@"There was a problem writing the map to the system memory. %@", writeError); }
        
        NSMutableDictionary * newData = [map mutableCopy];
        id activeDate = map[@"activeDate"];
        if(activeDate == nil)
        { activeDate = map[@"ActiveDate"]; }
        newData[@"ActiveDate"] = activeDate ? [self getDateFromString:activeDate] : @"";
        //                newData[@"ActiveDate"] = [PidsServiceDelegate parseDateFromDotNetJSONString:map[@"activeDate"]];
        return newData;

    } else {
        NSLog(@"Not Downloaded: %@", url);
        
        NSMutableDictionary* newData = [map mutableCopy];
        id activeDate = map[@"activeDate"];
        if(activeDate == nil)
        { activeDate = map[@"ActiveDate"]; }
        newData[@"ActiveDate"] = activeDate ? [self getDateFromString:activeDate] : @"";
        return newData;
    }
}

- (NSDate*)getDateFromString:(NSString*)dateStr
{
    if([dateStr isKindOfClass:[NSDate class]])
    { return (NSDate*)dateStr; }
    
    NSArray* components = [dateStr componentsSeparatedByString:@"T"];
    NSDateFormatter* dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSString* assembledString = [NSString stringWithFormat:@"%@ %@", components[0], components[1]];
    
    return [dateFormatter dateFromString:assembledString];
}

- (void)setPOIUpdates:(NSArray *)poiUpdates
{
    poiChecked = YES;
    temporaryPOIUpdateStorage = poiUpdates;
    
    if (ticketOutletsChecked && temporaryUpdateStorage)
        [self setUpdates:temporaryUpdateStorage atDate:serverTimeOfUpdates];
    else
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

- (void)setUpdates:(NSArray *)updates atDate:(NSDate *)atDate
{
    if (atDate != nil)
        serverTimeOfUpdates = atDate;
    
    if (updates)
        temporaryUpdateStorage = updates;
    
    // if we've run before the ticket outlets have been updated, wait for it to finish
    if (!ticketOutletsChecked || !poiChecked)// || !networkMapChecked)
	{
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
		return;
	}
    
	// Now that we've received a list, is there anything in it?
	if (!temporaryUpdateStorage.count &&
        !temporaryPOIUpdateStorage.count &&
        !temporaryOutletsUpdateStorage.count)// &&
        //        !temporaryNetworkMapUpdates.count)
	{
		// send a notification just in case
		if (isAutomatedSync == NO)
		{
			if ([self.delegate respondsToSelector:@selector(checkForUpdatesDidFinish:)])
				[self.delegate performSelectorOnMainThread:@selector(checkForUpdatesDidFinish:) withObject:nil waitUntilDone:NO];
            synchronisationInProgress = NO;
		}
		return;
	}
    
    NSArray * routeUpdates = [temporaryUpdateStorage filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"class == %@", [RouteUpdate class]]];
    NSArray * stopUpdates = [temporaryUpdateStorage filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"class == %@", [StopUpdate class]]];
    
    if (!serverTimeOfUpdates) {
        serverTimeOfUpdates = [NSDate date];
    }
    
    // we return a very basic one with zeros
    NSDictionary *params = nil;
    
    if (stopUpdates.count > 0 || routeUpdates.count > 0 || temporaryPOIUpdateStorage.count > 0 || temporaryOutletsUpdateStorage.count > 0) {
        params = @{
                   TTSyncKeyStops         : stopUpdates,
                   TTSyncKeyRoutes        : routeUpdates,
                   TTSyncKeyPOIs          : temporaryPOIUpdateStorage,
                   TTSyncKeyTickets       : temporaryOutletsUpdateStorage,
                   TTSyncKeyServerTime    : serverTimeOfUpdates};
    }
    
	// are we just checking for updates?
	if (isAutomatedSync == NO)
	{
        
		// post the notification
		if ([self.delegate respondsToSelector:@selector(checkForUpdatesDidFinish:)])
			[self.delegate performSelectorOnMainThread:@selector(checkForUpdatesDidFinish:) withObject:params waitUntilDone:NO];
		
		// let everything die
        synchronisationInProgress = NO;
		return;
	}
	
	//[self startUpdatingWithUpdates:params];
    [self synchronise];
}

@end
