//
//  PidsServiceDelegate.h
//  tramTRACKER
//
//  Created by Robert Amos on 18/02/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Stop.h"
#import "Prediction.h"
#import "PidsService.h"
#import "Route.h"
#import "StopList.h"
#import "Journey.h"
#import "Tram.h"
#import "RouteList.h"
#import "StopList.h"

/**
 * @ingroup Data
**/

/**
 * A delegate created by the PidsService to parse the results of a call
**/
@interface PidsServiceDelegate : NSObject

//
// The delegate that will handle the results
//

@property (nonatomic, weak) id delegate;
@property (nonatomic, strong) NSNumber *trackerID;
@property (nonatomic, strong) NSString *routeNumber;
@property (nonatomic, strong) NSNumber *upDirection;
@property (nonatomic, strong) NSDate *requestTime;
@property (nonatomic, strong) NSMutableString *responseText;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSTimer * timeoutTimer;

- (void)cancel;

/**
 * Custom Initialiser
 *
 * Initialises ourselves with the delegate passed to us by the PidsService
**/
- (PidsServiceDelegate *)initWithDelegate:(id)newDelegate;

+ (NSDate *)parseDateFromDotNetJSONString:(NSString *)string;

/**
 * A NSURLConnectionDelegate method. Called when the connection encountered an error.
**/
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error;
- (void)manageTimer;

/**
 * A NSURLConnectionDelegate method. Called when we did receive response headers from the PIDS Service.
 *
 * Prepares to receive data from the server
**/
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response;

/**
 * A NSURLConnectionDelegate method. Called when we have received a chunk of data from the service.
**/
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data;

/**
 * A NSURLConnectionDelegate method. Called when we have received the entire response from the server.
 * Calls -parseResponse to begin parsing the PIDS Service's response.
**/
- (void)connectionDidFinishLoading:(NSURLConnection *)connection;

/**
 * Parses a Date and Time string returned by the PIDS Service into a NSDate object
 *
 * @param	dateString		A date string as returned by the PIDS Service
 * @returns					An NSDate object
**/
- (NSDate *)parseDateTime:(NSString *)dateString;

/**
 * Find the predicted arrival time relative to the local clock by using the server's predicted arrival time and a comparison
 * of the difference between the two clocks
 *
 * @param	serverPredictedArrival			The predicated arrival time relative to the server's clock
 * @param	serverRequestTime				The time that this prediction was requested relative to the server's clock
 * @param	localRequestTime				The time that this prediction was requested relative to the local clock
 *
 * @returns									The predicted arrival time relative to the local clock
**/
- (NSDate *)calculateLocalPredictedArrivalTimeFromServerTime:(NSDate *)serverPredictedArrival withServerRequestTime:(NSDate *)serverRequestTime withLocalRequestTime:(NSDate *)localRequestTime;
- (NSDate *)calculateLocalScheduledDepartureTimeFromServerTime:(NSDate *)serverPredictedArrival withServerRequestTime:(NSDate *)serverRequestTime withLocalRequestTime:(NSDate *)localRequestTime;

/**
 * Send a failure message to the delegate
 **/
- (void)failWithError:(NSError *)error;
- (void)timeout:(NSTimer *)timer;

/**
 * Data Retrieval
**/
- (NSManagedObjectContext *)managedObjectContext;
- (StopStub *)stopForTrackerID:(NSNumber *)aTrackerID;
- (RouteStub *)routeWithNumber:(NSString *)aRouteNumber;
- (RouteStub *)routeWithNumber:(NSString *)aRouteNumber headboardRouteNumber:(NSString *)aHeadboardRouteNumber;

@end
