//
//  AdManager.h
//  tramTracker
//
//  Created by Jonathan Head on 18/05/2015.
//  Copyright (c) 2015 AppsCore. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIViewController (TramTrackerAds)
- (NSLayoutConstraint*)adSpacingConstraint;
- (void)showAds;
- (void)removeAds:(BOOL)updateLayout;
- (void)respondToKillSwitchUpdate:(NSNotification*)notification;
@end

@interface AdManager : NSObject
/** This property contains the ad controllers for each of the displayed view controllers. This needs to be a map table 
 rather than a dictionary because it needs to hold a weak reference to each of the displayed view controllers - otherwise
 they will never be deallocated (or removed from the dictionary). */
@property (strong, nonatomic) NSMapTable* adControllers;
+ (AdManager*)instance;
+ (BOOL)adsEnabled;
- (void)createAdsForViewController:(UIViewController*)controller;
- (void)removeAdsForViewController:(UIViewController*)controller;
@end
