// 
//  HistoricalUpdate.m
//  tramTRACKER
//
//  Created by Robert Amos on 5/08/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "HistoricalUpdate.h"
#import "tramTRACKERAppDelegate.h"

NSString * const HistoricalUpdateFileName = @"updates.plist";

@implementation HistoricalUpdate 

@synthesize name, message, updatedDate;

+ (instancetype)historicalUpdateWithName:(NSString *)aName message:(NSString *)aMessage
{
    HistoricalUpdate    * update = [HistoricalUpdate new];
    
    [update setMessage:aMessage];
    [update setUpdatedDate:[NSDate date]];
    [update setName:aName];
    return update;
}

- (id)initWithCoder:(NSCoder *)coder
{
	[self setName:[coder decodeObjectForKey:@"HUName"]];
	[self setMessage:[coder decodeObjectForKey:@"HUMessage"]];
	[self setUpdatedDate:[coder decodeObjectForKey:@"HUDate"]];
	return self;
}
- (void)encodeWithCoder:(NSCoder *)coder
{
	[coder encodeObject:self.name forKey:@"HUName"];
	[coder encodeObject:self.message forKey:@"HUMessage"];
	[coder encodeObject:self.updatedDate forKey:@"HUDate"];
}

+ (NSArray *)getHistoricalUpdatesList
{
	// check if the file exists
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSString *docDir = [tramTRACKERAppDelegate applicationDocumentsDirectory];
	NSString *file = [docDir stringByAppendingPathComponent:HistoricalUpdateFileName];

	// no file? return an empty array
	if (![fileManager fileExistsAtPath:file])
		return [NSArray array];
	
	// read the file in
	NSArray *updates = [NSKeyedUnarchiver unarchiveObjectWithFile:file];
	if (updates == nil)
		return [NSArray array];

	// return the array
	return updates;
}

+ (void)addHistoricalUpdatesList:(NSArray *)updateList
{
    NSMutableArray * currentList = [[HistoricalUpdate getHistoricalUpdatesList] mutableCopy];

    if (!currentList)
        currentList = [NSMutableArray array];
    
    [currentList addObject:updateList];
    
    [HistoricalUpdate saveHistoricalUpdatesList:currentList];
}

+ (void)saveHistoricalUpdatesList:(NSArray *)updateList
{
	// write the array to the file
	NSString *docDir = [tramTRACKERAppDelegate applicationDocumentsDirectory];
	NSString *file = [docDir stringByAppendingPathComponent:HistoricalUpdateFileName];
	
	[NSKeyedArchiver archiveRootObject:updateList toFile:file];
}

+ (void)clearHistoricalUpdatesList
{
	NSString *docDir = [tramTRACKERAppDelegate applicationDocumentsDirectory];
	NSString *file = [docDir stringByAppendingPathComponent:HistoricalUpdateFileName];
	NSFileManager *manager = [NSFileManager defaultManager];
	[manager removeItemAtPath:file error:NULL];
}

+ (BOOL)hasHistoricalUpdates
{
	// does the file exist?
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSString *docDir = [tramTRACKERAppDelegate applicationDocumentsDirectory];
	NSString *file = [docDir stringByAppendingPathComponent:HistoricalUpdateFileName];
	
	// check to see if our file exists and is readable
	return [fileManager fileExistsAtPath:file] && [fileManager isReadableFileAtPath:file];
}

- (id)initHistoricalUpdate
{
	if (self = [super init])
	{
		// customisation?
	}
	return self;
}

@end
