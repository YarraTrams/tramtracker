//
//  Tram.m
//  tramTRACKER
//
//  Created by Robert Amos on 19/06/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "Tram.h"
#import "Route.h"

@implementation Tram

@synthesize number, route, headboardRouteNumber, upDirection, atLayover, available, disrupted, specialEvent, routeNumber;


+ (Tram *)tramForStub:(TramStub *)stub
{
    Tram *tram = [stub copy];
    
    // Pass it through with a route set.
    [tram setRoute:[Route routeForStub:stub.routeStub]];

    tram.specialEvent = stub.specialEvent;
    tram.specialMessage = stub.specialMessage;
    // return it
    return tram;
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"<Tram: %p> Tram Number %@ travelling on %@ (%@)", self, number, route, headboardRouteNumber];
}

// return the trams name, if it has one
- (NSString *)name
{
	NSInteger tramNumber = [self.number integerValue];
	switch (tramNumber)
	{
		case 5123:
			return NSLocalizedString(@"tram-name-5123", @"Tram 5123");
			
		case 5113:
			return NSLocalizedString(@"tram-name-5113", @"Tram 5113");
			
		case 5103:
			return NSLocalizedString(@"tram-name-5103", @"Tram 5103");
			
		case 5106:
			return NSLocalizedString(@"tram-name-5106", @"Tram 5106");
			
		case 5111:
			return NSLocalizedString(@"tram-name-5111", @"Tram 5111");
			
		default:
			return [NSString stringWithFormat:NSLocalizedString(@"tram-name", @"Tram x"), self.number];
	}
}

- (BOOL)isSameRoute:(Tram *)tram
{
    /* if the route itself has changed, then yes */
    if (![tram.route isEqual:self.route])
        return YES;
    
    /* if the headboard details have changed, then yes */
    if (![tram.headboardRouteNumber isEqualToString:self.headboardRouteNumber])
        return YES;
    
    /* If the direction has changed, then yes */
    if (tram.upDirection != self.upDirection)
        return YES;

    return NO;
}

- (TramType)classType
{
    NSInteger       tramNumber = [self.number integerValue];
    
    if (tramNumber <= 0)
        return TramTypeUnknown;

	// Z1 class trams
	if (tramNumber <= 100)
		return TramTypeZ1;

	// Z2 class trams
	else if (tramNumber <= 115)
		return TramTypeZ2;
	
	// Z3 class trams
	else if (tramNumber <= 230)
		return TramTypeZ3;
	
	// A1 class trams
	else if (tramNumber <= 258)
		return TramTypeA1;

	// A2 class trams
	else if (tramNumber <= 300)
		return TramTypeA2;
	
	// W Class trams
	else if (tramNumber >= 681 && tramNumber <= 1040)
		return TramTypeW;
	
	// B1 class trams
	else if (tramNumber == 2001 || tramNumber == 2002)
		return TramTypeB1;
	
	// B2 Class Trams
	else if (tramNumber >= 2003 && tramNumber <= 2132)
		return TramTypeB2;
	
	// C Class Trams
	else if (tramNumber >= 3001 && tramNumber <= 3036)
		return TramTypeC;
	
	// D1 Class trams
	else if (tramNumber >= 3501 && tramNumber <= 3600)
		return TramTypeD1;
	
	// D2 Class trams
	else if (tramNumber >= 5001 && tramNumber <= 5100)
		return TramTypeD2;
	
	// Bumblebee trams
	else if (tramNumber >= 5101 && tramNumber <= 5200)
		return TramTypeC2;
    
    // E Class trams
	else if (tramNumber >= 6001 && tramNumber <= 6050)
		return TramTypeE;
	
	// bleh, return the default
	return TramTypeUnknown;
}

- (UIImage *)image
{
    switch ([self classType])
    {
        case TramTypeB1:
        case TramTypeB2:
            return [UIImage imageNamed:@"tram-b"];
            
        case TramTypeC:
            return [UIImage imageNamed:@"tram-c"];
            
        case TramTypeC2:
            return [UIImage imageNamed:@"tram-c2"];
            
        case TramTypeD1:
            return [UIImage imageNamed:@"tram-d1"];
        case TramTypeD2:
            return [UIImage imageNamed:@"tram-d2"];
            
        case TramTypeE:
            return [UIImage imageNamed:@"tram-e"];
            
        case TramTypeW:
            return [UIImage imageNamed:@"tram-w"];
            
        case TramTypeZ1:
        case TramTypeZ2:
            return [UIImage imageNamed:@"tram-z1"];

        case TramTypeA1:
        case TramTypeA2:
            return [UIImage imageNamed:@"tram-a"];

        case TramTypeZ3:
            return [UIImage imageNamed:@"tram-z3"];

        default:
            return nil;
    }
}

- (BOOL)isRouteZero
{
	return [self.headboardRouteNumber isEqualToString:@"0"];
}
- (BOOL)isUnknownRoute
{
	return self.route == nil;
}
- (BOOL)isCityCircle
{
	return [self.headboardRouteNumber isEqualToString:@"35"];
}

// do we have a pull cord?
- (BOOL)hasPullCord
{
    switch ([self classType])
    {
        case TramTypeZ1:
        case TramTypeZ2:
        case TramTypeZ3:
        case TramTypeA1:
        case TramTypeA2:
        case TramTypeW:
        case TramTypeB1:
        case TramTypeB2:
            return YES;
            
        default:
            return NO;
    }
}

- (NSString *)destination
{
	return upDirection ? route.upDestination : route.downDestination;
}

- (BOOL)isEqualToTram:(Tram *)otherTram
{
	// same tram number?
	return [self.number isEqualToNumber:otherTram.number];
}

- (id)copyWithZone:(NSZone *)zone
{
	Tram *copy = [[Tram alloc] init];
	[copy setAtLayover:self.atLayover];
	[copy setAvailable:self.available];
	[copy setDisrupted:self.disrupted];
	[copy setHeadboardRouteNumber:[self.headboardRouteNumber copy]];
	[copy setNumber:[self.number copy]];
	[copy setRoute:self.route];
	[copy setRouteNumber:[self.routeNumber copy]];
	[copy setSpecialEvent:self.specialEvent];
	[copy setUpDirection:self.upDirection];
	return copy;
}

@end

@implementation TramStub

@synthesize routeStub;

@end
