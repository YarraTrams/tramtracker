//
//  FavouriteStop.h
//  tramTracker
//
//  Created by Hugo Cuvillier on 8/05/2014.
//  Copyright (c) 2014 AppsCore. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Stop.h"
#import "Filter.h"

@interface FavouriteStop : NSObject

@property (strong, nonatomic) Stop * stop;
@property (strong, nonatomic) Filter * filter;
@property (strong, nonatomic) NSString * name;
@property (strong, nonatomic) NSIndexPath * indexPath;

- (instancetype)initWithStop:(Stop *)stop filter:(Filter *)filter indexPath:(NSIndexPath *)indexPath name:(NSString *)name;
- (NSComparisonResult)compareWithFavouriteStop:(FavouriteStop *)otherStop;

@end
