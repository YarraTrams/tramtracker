//
//  Turn.h
//  tramTRACKER
//
//  Created by Robert Amos on 17/06/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>

@class Stop;
@class Route;

typedef NSInteger TurnType;
extern const TurnType TTTurnLeft;
extern const TurnType TTTurnRight;
extern const TurnType TTTurnStraight;
extern const TurnType TTTurnSRight;
extern const TurnType TTTurnVeerLeft;
extern const TurnType TTTurnVeerRight;

/**
 * Describes a turn that a tram will take when following a specific route.
 * Only turns that separate one route from another route usually have Turns.
**/
@interface Turn : NSManagedObject {
}

/**
 * The Route that the turn is on.
**/
@property (nonatomic, strong) Route *route;

/**
 * The direction of travel that the turn affects
**/
@property (nonatomic) BOOL upDirection;

/**
 * The last stop prior to the turn. Ie the turn comes AFTER This stop.
**/
@property (nonatomic, strong) Stop *stop;

/**
 * The type of turn. Must be an NSNumber with one of the TurnType constants.
**/
@property (nonatomic, strong) NSNumber *type;

/**
 * The message to display on the Onboard screen for this turn
**/
@property (nonatomic, strong) NSString *message;

/**
 * An image for use in the Onboard screen that describes this turn
**/
- (UIImage *)image;

/**
 * Finds a matching turn for a specific route direction and stop.
 *
 * @param		aRoute			A Route
 * @param		isUpDirection	YES for the up direction, NO for down
 * @param		aStop			The last stop before the turn
 * @return						A matching Turn object, or nil if none found.
**/
+ (Turn *)turnForRoute:(Route *)aRoute upDirection:(BOOL)isUpDirection stop:(Stop *)aStop;

/**
 * The type of turn
**/
- (TurnType)typeOfTurn;

- (void)setTypeFromString:(NSString*)typeString;

@end
