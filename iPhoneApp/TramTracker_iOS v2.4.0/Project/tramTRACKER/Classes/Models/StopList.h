//
//  StopList.h
//  tramTRACKER
//
//  Created by Robert Amos on 23/02/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "Stop.h"
#import "Filter.h"
#import "RouteList.h"

@class FavouriteStop;

extern int const TTNearbyStopCount;

/**
 * @ingroup Data
**/

/**
 * A singleton that provides information relating to lists of stops.
**/
@interface StopList : NSObject {

	/**
	 * The singleton instance of ourselves
	**/
	StopList *sharedStopList;
	
	NSArray *internalFavouriteStopList;

	/**
	 * A cached copy of the suburb list, this list is thrown away when memory is low and recreated as needed
	**/
	NSArray *suburbList;
}

@property (nonatomic, strong) NSArray *suburbList;

/**
 * Singleton manager. Call [StopList sharedManager] to get a copy of the singleton
**/
+ (StopList *)sharedManager;

/**
 * Search for a stop by name or suburb
 *
 * @param	searchString		The string to search for
 * @returns						A NSArray of matching stops
**/
- (NSArray *)stopsBySearchingNameOrSuburb:(NSString *)searchString;

/**
 * Get the stops nearest to a location
 *
 * @param	location		The location with which to sort the stop list
 * @param	numberOfStops	The number of nearest stops to return
 * @returns					A NSArray of the nearest stops and the distances to those stops
**/
- (NSArray *)getNearestStopsToLocation:(CLLocation *)location count:(NSUInteger)numberOfStops;
- (NSArray *)getNearestStopsWithoutDistancesToLocation:(CLLocation *)location
												 count:(NSUInteger)numberOfStops
                                              lowFloor:(BOOL)isLowFloor
                                               shelter:(BOOL)isShelter;

- (NSArray *)getNearestStopsToLocation:(CLLocation *)location count:(NSUInteger)numberOfStops shelter:(BOOL)isShelter lowFloor:(BOOL)isLowFloow;

- (NSArray *)getNearestStopsWithoutDistancesToLocation:(CLLocation *)location count:(NSUInteger)numberOfStops;
- (NSArray *)getStopsInRegion:(MKCoordinateRegion)region;

/**
 * Get the single stop nearest to a location
 *
 * @param	location		The location with which to sort the stop list
 * @returns					A stop distance object containing information about the nearest stop and the distance to that stop
**/
- (StopDistance *)getNearestStopToLocation:(CLLocation *)location;

/**
 * Get the single stop from the list of favourite stops that is nearest to the location
 *
 * @param	location		The location with which to sort the favourites stop list
 * @returns					A favourite stop object containing information about the nearest favourite
**/
//- (FavouriteStop *)getNearestFavouriteToLocation:(CLLocation *)location;

/**
 * Get the single nearest stop from the provided list of stops that is nearest to that location
 *
 * @param	location		The location with which to sort the stop list
 * @param	stopList		The list of stops to search for the nearest one from
 * @returns					A stop distance object containing information about the nearest stop
**/
- (StopDistance *)getNearestStopToLocation:(CLLocation *)location withStopList:(NSArray *)stopList;

/// Save the favourites list into the shared NSUserDefaults for the app group the widget can access.
+ (void)saveWidgetFavourites;

/**
 * Get a list of the favourite stops
**/
- (NSArray *)getFavouriteStopList;
- (NSArray *)getFavouriteTrackerIDs;
- (FavouriteStop *)favouriteStopAtIndex:(NSIndexPath *)indexPath;
- (NSIndexPath *)indexPathForFavouriteStop:(Stop *)stop;

/**
 * Whether the user has a favourite stops list
**/
- (BOOL)hasFavouriteStopList;

/**
 * Get a Stop object for a specific tracker ID
 *
 * @param	trackerID		The tracker ID to get a stop for
**/
- (Stop *)getStopForTrackerID:(NSNumber *)trackerID;

/**
 * Get stop objects for an array of tracker IDs
 *
 * @param	trackerIDs		An array of TrackerIDs
**/
- (NSArray *)getStopListForTrackerIDs:(NSArray *)trackerIDs;


/**
 * Add a tracker ID to the favourites stop list
 *
 * @param	trackerID		The tracker ID to add to the list
**/
//- (FavouriteStop *)addFavouriteStopID:(NSNumber *)trackerID inSection:(NSInteger)section;

/**
 * Deletes a favorite stop from the favorites list
 *
 * @param	trackerID		The tracker ID to add to the list
 **/
- (void)removeFavouriteByStopID:(NSNumber *)trackerID;

/**
 * Whether the specified tracker ID is on the favourite stop list
 *
 * @param	trackerID		The tracker ID to check if its on the favourite stop list
**/
- (BOOL)isFavouriteStop:(NSNumber *)trackerID;

/**
 * Remove the favourite at the specified index. Called when the user has edited the table of favourites and removed one.
 *
 * @param	index			The index of the favourite to remove.
 * @returns A boolean indicating if the favourite could be removed. If it could, YES is returned. Otherwise, NO is returned.
**/
- (BOOL)removeFavouriteAtIndexPath:(NSIndexPath *)indexPath;

/**
 * Move a favourite at the specified index to the specified index.
 *
 * Called when the user edits the list of favourites and re-arranges them
 *
 * @param	fromIndex		The index of the favourite to be moved
 * @param	toIndex			The index where the favourite should be re-added at
**/
- (void)moveFavouriteAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath;

- (Filter *)filterForFavouriteAtIndexPath:(NSIndexPath *)indexPath;
- (void)setFilter:(Filter *)filter forFavouriteAtIndexPath:(NSIndexPath *)indexPath;

- (NSArray *)sectionNamesForFavourites;
- (void)removeFavouriteSectionAtIndex:(NSUInteger)index;
- (void)moveFavouriteSectionAtIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex;
- (void)addFavouriteSection:(NSString *)sectionName;
- (void)changeFavouriteSectionNameAtIndex:(NSUInteger)index toName:(NSString *)newName;
- (FavouriteStop *)addFavouriteStopID:(NSNumber *)trackerID inSection:(NSInteger)section withName:(NSString *)name;
- (void)setName:(NSString *)name forFavouriteAtIndexPath:(NSIndexPath *)indexPath;

- (void)upgradeFavouritesTo11;
- (BOOL)hasUpgradedFavouritesTo11;
- (void)upgradeFavouritesTo12;
- (BOOL)hasUpgradedFavouritesTo12;
- (NSInteger)countOfFavouritesInSection:(NSInteger)section;
- (FavouriteStop *)closestFavourite:(CLLocation *)aLocation;

- (NSMutableArray *)_sortedMutableFavourites;
- (NSArray *)_flattenedArrayOfRenumberedFavourites:(NSArray *)favouriteStops;

- (BOOL)hasUpgradedFavouritesTo20;
- (void)upgradeFavouritesTo20;

/**
 * Get a list of unique suburb names that contain stops
**/
- (NSArray *)listOfSuburbs;

/**
 * Get the number of unique suburbs names that contain stops
**/
- (NSInteger)numberOfSuburbs;

/**
 * Get all of the stops in a specified suburb
 *
 * @param	suburb			The suburb to get all the stops in
**/
- (NSArray *)stopsInSuburb:(NSString *)suburb;

/**
 * Get an array of the most recent stops. The number of recent stops returned is user-configurable. Up to 100 are stored.
**/
- (NSArray *)mostRecentStops;

/**
 * Add a Tracker ID to the list of Most Recent stops. A tracker ID can only appear once.
**/
- (void)addMostRecentStopID:(NSNumber *)trackerID;

/**
 * Whether the user has stops added to their most recent list.
**/
- (BOOL)hasMostRecentStops;

/**
 * Gets the override of the direction text for a particular stop
**/
- (NSString *)directionTextForStopID:(NSNumber *)trackerID;
- (NSInteger)indexOfDefaultSection;

/**
 * Save direction text for a particular stop
**/
//- (void)setDirectionText:(NSString *)directionText forStopID:(NSNumber *)trackerID;

@end
