////
////  FTZOverlay.m
////  tramTracker
////
////  Created by Hugo Cuvillier on 8/12/2014.
////  Copyright (c) 2014 AppsCore. All rights reserved.
////
//
//#import "FTZOverlay.h"
//
//@interface FTZOverlay()
//
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewHeight;
//@property (weak, nonatomic) IBOutlet UITextView *firstTV;
//@property (weak, nonatomic) IBOutlet UITextView *secondTV;
//@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ftzTopSpace;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint* textTopSpace;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint* ftzLogoTopSpace;
//@property BOOL isLink;
//
//@end
//
//@implementation FTZOverlay
//
//+ (FTZOverlay *)overlay {
//    FTZOverlay * view = nil;
//    if (IS_IPHONE_5)
//    { view = [[NSBundle mainBundle] loadNibNamed:@"FTZOverlay4" owner:self options:nil][0]; }
//    else
//    { view = [[NSBundle mainBundle] loadNibNamed:@"FTZOverlay" owner:self options:nil][0]; }
//    
//    view.frame = [UIScreen mainScreen].bounds;
//    
//    return view;
//}
//
//- (void)awakeFromNib {
//    [super awakeFromNib];
//    self.scrollView.scrollEnabled = NO;
//    
//    if(floor(NSFoundationVersionNumber) < NSFoundationVersionNumber_iOS_7_1)
//    {
//        self.textTopSpace.constant = 15;
//        self.ftzLogoTopSpace.constant = -100;
//    }
//}
//
//- (IBAction)didPressContinueButton:(id)sender {
//    [self removeFromSuperview];
//}
//
//- (IBAction)didTapLogo:(id)sender
//{
//    self.isLink = NO;
//    [[[UIAlertView alloc] initWithTitle:@"Open in Safari?" message:@"Would you like to close the tramTracker® app and launch Safari?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil] show];
//}
//
//- (IBAction)didTapLink:(id)sender
//{
//    self.isLink = YES;
//    [[[UIAlertView alloc] initWithTitle:@"Open in Safari?" message:@"Would you like to close the tramTracker® app and launch Safari?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil] show];
//}
//
//#pragma mark - Alertview Delegate Methods
//
//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    if (buttonIndex == 1)
//    {
//        if (self.isLink)
//        { [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://yarratrams.com.au"]]; }
//        else
//        { [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://ptv.vic.gov.au"]]; }
//    }
//}
//
//@end
