//
//  myTramCell.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 27/11/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "myTramCell.h"
#import "Journey.h"
#import "Prediction.h"
#import "TicketRetailer.h"
#import "PointOfInterest.h"

@interface myTramCell()

@property (weak, nonatomic) IBOutlet UILabel        * title;
@property (weak, nonatomic) IBOutlet UILabel        * subtitle;
@property (weak, nonatomic) IBOutlet UIImageView    * ftzLogo;

@property (weak, nonatomic) IBOutlet UIImageView    * zoneImage;
@property (weak, nonatomic) IBOutlet UIImageView    * routeImage;
@property (weak, nonatomic) IBOutlet UIImageView    * ftzImage;

@property (weak, nonatomic) IBOutlet UIImageView    * platformImage;
@property (weak, nonatomic) IBOutlet UIImageView    * connectingTrainIcon;
@property (weak, nonatomic) IBOutlet UIImageView    * connectingTramIcon;
@property (weak, nonatomic) IBOutlet UIImageView    * connectingBusIcon;

@property (weak, nonatomic) IBOutlet UIImageView    * turnImage;

@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *legendTopSpaces;
@end

@implementation myTramCell

- (void)configureWithJourneyStop:(JourneyStop *)journeyStop isSelected:(BOOL)isSelected routeImage:(NSString *)routeImage stopIdx:(NSInteger)stopIdx
{
    [self setStop:journeyStop.stop routeImage:routeImage isSelected:isSelected stopIdx:stopIdx];

    self.estimatedTime.hidden = NO;
    self.accessoryType = UITableViewCellAccessoryNone;
}

- (void)configureWithStop:(Stop *)stop routeImage:(NSString *)routeImage stopIdx:(NSInteger)stopIdx
{
    [self setStop:stop routeImage:routeImage isSelected:NO stopIdx:stopIdx];
    self.estimatedTime.text = @"--:--";
    self.platformImage.hidden = !stop.isPlatformStop;
    self.estimatedTime.hidden = NO;
    self.accessoryType = UITableViewCellAccessoryNone;
}

- (void)updatePOIFromStop:(Stop *)stop
{
    NSArray         * pois = [PointOfInterest poisForIDs:stop.poisThroughStop];
    NSMutableString * poiStrings = [NSMutableString string];
    
    for (PointOfInterest *poi in pois)
    {
        if (poiStrings.length > 0)
            [poiStrings appendFormat:@", %@", poi.name];
        else
            [poiStrings appendFormat:@"%@", poi.name];
    }
    if (poiStrings.length)
    {
        self.subtitle.text = poiStrings;
        for (NSLayoutConstraint *curConstrain in self.legendTopSpaces)
            curConstrain.constant = 45.0f;
    }
    else
    {
        self.subtitle.text = @"";
        for (NSLayoutConstraint *curConstrain in self.legendTopSpaces)
            curConstrain.constant = 27.0f;
    }
    [self layoutIfNeeded];
}

- (void)configureFromRouteWithStop:(Stop *)stop routeImage:(NSString *)routeImage stopIdx:(NSInteger)stopIdx
{
    if (stop.trackerID.integerValue == 2352) {
        
    }
    [self setStop:stop routeImage:routeImage isSelected:NO stopIdx:stopIdx];
    self.platformImage.hidden = !stop.isPlatformStop;
    self.estimatedTime.hidden = YES;
    self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
}

- (void)configureWithJourney:(Journey *)journey
{
    if (journey.tram.name)
        self.title.text = [NSString stringWithFormat:NSLocalizedString(@"onboard-message-atlayover", @"At Layover"), [journey.tram name]];
}

- (void)configureWithRetailer:(TicketRetailer *)retailer
{
    self.title.text = retailer.name;
    self.subtitle.text = retailer.address;
}

- (void)setStop:(Stop *)stop routeImage:(NSString *)routeImage isSelected:(BOOL)isSelected stopIdx:(NSInteger)stopIdx
{
    self.title.text = stop.formattedName;
    self.platformImage.hidden = !stop.isPlatformStop;

    [self updatePOIFromStop:stop];

    self.routeImage.image = self.routeImage.highlightedImage = [UIImage imageNamed:routeImage];
    
    self.zoneImage.hidden = NO;
    self.connectingBusIcon.hidden = ![stop.connectingBuses boolValue];
    self.connectingTrainIcon.hidden = ![stop.connectingTrains boolValue];
    self.connectingTramIcon.hidden = ![stop.connectingTrams boolValue];
    self.ftzImage.image = nil;
    self.ftzLogo.hidden = !stop.isFTZStop;
    
    if (stop.isFTZStop) {
        self.zoneImage.image = self.zoneImage.highlightedImage = [UIImage imageNamed:@"mytram_Left_FTZ"];
        if (stopIdx == 0) {
            self.ftzImage.image = [UIImage imageNamed:@"Zone"];
        } else if (stopIdx == 1) {
            self.ftzImage.image = [UIImage imageNamed:@"Tram"];
        } else if (stopIdx == 2) {
            self.ftzImage.image = [UIImage imageNamed:@"Free"];
        }
    }
    else if (stop.isZone2)
    {
        if (!isSelected)
            self.zoneImage.image = self.zoneImage.highlightedImage = [UIImage imageNamed:@"myTRAM_Left_Stripped_Grey"];
        else
            self.zoneImage.image = self.zoneImage.highlightedImage = [UIImage imageNamed:@"myTRAM_Left_Stripped_Red"];
    }
    else
        self.zoneImage.hidden = YES;
    
    if (isSelected)
    {
        self.backgroundView = [UIView new];
        self.backgroundView.backgroundColor = [UIColor colorWithRed:0.988235294f green:0.890196078f blue:0.878431373f alpha:1.0f];
        self.backgroundColor = [UIColor colorWithRed:0.988235294f green:0.890196078f blue:0.878431373f alpha:1.0f];
    }
    else
    {
        self.backgroundView = [UIView new];
        self.backgroundColor = [UIColor whiteColor];
        self.backgroundView.backgroundColor = [UIColor whiteColor];
    }
}

- (void)setTurnIndicatorImage:(UIImage *)aImage
{
    self.turnImage.image = aImage;
}

@end
