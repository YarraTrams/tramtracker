//
//  MapCallout.h
//  tramTracker
//
//  Created by Hugo Cuvillier on 4/12/2014.
//  Copyright (c) 2014 AppsCore. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapCallout : UIView

@property (weak, nonatomic) IBOutlet UILabel *calloutTitle;
@property (weak, nonatomic) IBOutlet UILabel *calloutSubtitle;
@property (weak, nonatomic) IBOutlet UIButton *timetablesButton;
@property (weak, nonatomic) IBOutlet UIButton *pidButton;

@end
