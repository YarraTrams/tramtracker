//
//  ActivityIndicator.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 6/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityIndicator : UIImageView

typedef enum
{
  ActivityIndicatorTracking,
    ActivityIndicatorTramming
} ActivityIndicatorType;

- (void)loadImages:(ActivityIndicatorType)type;

@end
