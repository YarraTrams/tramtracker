//

//  AccordionButton.m
//  tramTRACKER
//
//  Created by Alex Louey on 15/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import <FrameAccessor/FrameAccessor.h>

#import "AccordionButton.h"
#import <QuartzCore/QuartzCore.h>

@interface AccordionButton()
{
    UILabel * textLb;
    CGFloat baseHeight;
}

@end

@implementation AccordionButton

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [[self layer] setBorderWidth:1.0f];
        [[self layer] setBorderColor:[UIColor lightGrayColor].CGColor];
        self.layer.cornerRadius = 5;
        self.clipsToBounds = YES;
    }
    return self;
}

- (void)setText:(NSString *)text
{
    _text = text;
    baseHeight = self.height;
    textLb = [[UILabel alloc] initWithFrame:CGRectMake(10, baseHeight, self.width -20, 30)];
//    textLb = [[UILabel alloc] initWithFrame:CGRectZero];
    [textLb setBackgroundColor:self.backgroundColor];
    [textLb setFont:[UIFont fontWithName:self.titleLabel.font.familyName size:13]];
    textLb.textColor = self.titleLabel.textColor;
    [textLb setTextAlignment:NSTextAlignmentCenter];
    textLb.numberOfLines = 0;
    textLb.text = text;
    
    [self addSubview:textLb];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    textLb.frame = CGRectMake(10, baseHeight, self.width-20, 30);
    [textLb sizeToFit];
}

- (void)setupLayout
{
//    [self addConstraints:@[
//        [NSLayoutConstraint constraintWithItem:textLb attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0],
//        [NSLayoutConstraint constraintWithItem:textLb attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1.0 constant:0],
//        [NSLayoutConstraint constraintWithItem:textLb attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:self.height],
//    ]];
//    [textLb addConstraint:[NSLayoutConstraint constraintWithItem:textLb attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:10]];
//    [self layoutIfNeeded];
}

@end
