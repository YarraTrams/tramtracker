//
//  FavouriteGroupCell.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 16/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "FavouriteGroupCell.h"

@interface FavouriteGroupCell()

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIImageView *image;

@end

@implementation FavouriteGroupCell

- (void)configureWithString:(NSString *)string isSelected:(BOOL)isSelected
{
    self.name.text = string;
    [self setToggleState:isSelected];
}

- (void)setToggleState:(BOOL)selected;
{
    if (selected)
        [self.image setImage:[UIImage imageNamed:@"Toggle-On"]];
    else
        [self.image setImage:[UIImage imageNamed:@"Toggle-Off"]];
}

@end
