//
//  PIDCell.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 21/11/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "MCSwipeTableViewCell.h"

@class PredictionStub;

@interface PIDCell : MCSwipeTableViewCell
- (void)configureWithPrediction:(PredictionStub *)prediction target:(id)target action:(SEL)action;
- (void)manageExpandCollapseArrows:(BOOL)isExpanded index:(NSInteger)index size:(NSInteger)size isExpandable:(BOOL)isExpandable;
@end
