//
//  CustomMapView.h
//  tramTracker
//
//  Created by Hugo Cuvillier on 4/12/2014.
//  Copyright (c) 2014 AppsCore. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "SMCalloutView.h"

@interface CustomMapView : MKMapView

@property (nonatomic, strong) SMCalloutView *calloutView;

@end
