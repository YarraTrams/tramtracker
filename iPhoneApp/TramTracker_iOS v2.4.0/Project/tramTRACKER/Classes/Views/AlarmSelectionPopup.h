//
//  AlarmSelectionPopup.h
//  tramTracker
//
//  Created by Jonathan Head on 22/07/2015.
//  Copyright (c) 2015 AppsCore. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Prediction.h"

@interface AlarmSelectionPopup : UIView
@property (nonatomic) NSInteger secondsOffset;
+ (AlarmSelectionPopup *)popupForPrediction:(Prediction*)prediction stop:(Stop*)stop timeOffsets:(NSArray*)timeOffsets isEstimate:(BOOL)isEstimate;
@end
