//
//  StopAnnotationView.m
//  tramTRACKER
//
//  Created by Robert Amos on 19/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "StopAnnotationView.h"
#import "Constants.h"
#import "MapCallout.h"

@interface StopAnnotationView()
{
    UIButton    * p_realTimeDisclosureButton;
    UIButton    * p_scheduledDisclosureButton;
    BOOL        _showCustomCallout;
}

@property (strong, nonatomic) MapCallout  * calloutView;

@end

@implementation StopAnnotationView

@synthesize purplePin, shouldShowRealTimeAccessory, shouldShowScheduledAccessory;

///**
// * Override the highlighting stuff
//**/
//- (void)setSelected:(BOOL)selected animated:(BOOL)animated
//{
//    self.image = self.image;
//}

/**
 * Return a real-time button
 **/
- (UIView *)rightCalloutAccessoryView
{
	// show a real time button?
	if ([self shouldShowRealTimeAccessory])
	{
        if (!p_realTimeDisclosureButton)
        {
            // make a button that calls stuff
            UIButton *realTimeDisclosureButton = [UIButton buttonWithType:UIButtonTypeCustom];
            UIImage *pidIcon = [UIImage imageNamed:@"PIDIcon.png"];
            [realTimeDisclosureButton setImage:pidIcon forState:UIControlStateNormal];
            [realTimeDisclosureButton setFrame:CGRectMake(0, 0, pidIcon.size.width + 10, pidIcon.size.height + 10)];
            
            // configure the target to call ourselves
            [realTimeDisclosureButton addTarget:self action:@selector(didTouchRealTimeCalloutAccessory:) forControlEvents:UIControlEventTouchUpInside];
            
            p_realTimeDisclosureButton = realTimeDisclosureButton;
        }
		return p_realTimeDisclosureButton;
	}
	
	// nope
	return nil;
}

/**
 * Return a scheduled
**/
- (UIView *)leftCalloutAccessoryView
{
	// show a scheduled button?
	if ([self shouldShowScheduledAccessory])
	{
        
        if (!p_scheduledDisclosureButton)
        {
            // make a button that calls stuff
            UIButton *scheduledDisclosureButton = [UIButton buttonWithType:UIButtonTypeCustom];
            UIImage *scheduledIcon = [UIImage imageNamed:@"ScheduledIcon.png"];
            [scheduledDisclosureButton setImage:scheduledIcon forState:UIControlStateNormal];
            [scheduledDisclosureButton setFrame:CGRectMake(0, 0, scheduledIcon.size.width, scheduledIcon.size.height)];
            
            // configure the target to call ourselves
            [scheduledDisclosureButton addTarget:self action:@selector(didTouchScheduledCalloutAccessory:) forControlEvents:UIControlEventTouchUpInside];
            
            p_scheduledDisclosureButton = scheduledDisclosureButton;
        }
		return p_scheduledDisclosureButton;
	}
	return nil;
}

/**
 * Called when the user touches our callout stuff
**/
- (void)didTouchRealTimeCalloutAccessory:(id)sender
{
//	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
//	
//	// send a notification that we clicked the stop
//	[nc postNotificationName:kNotificationPIDTouchedInPinOnMap object:self.annotation];
}
- (void)didTouchScheduledCalloutAccessory:(id)sender
{
//	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
//	
//	// send a notification that we clicked the stop
//	[nc postNotificationName:kNotificationScheduleTouchedInPinOnMap object:self.annotation];
}

- (void)setAnnotationType:(StopRightMenuType)type
{
    switch (type)
    {
        case StopRightMenuAccess:
            self.image = [UIImage imageNamed:@"Low-Level-Access"];
            break;

        case StopRightMenuShelter:
            self.image = [UIImage imageNamed:@"Shelters-Pin"];
            break;

        case StopRightMenuCurrent:
            self.image = [UIImage imageNamed:@"pin-red2"];
            break;

        default:
            self.image = [UIImage imageNamed:@"pin"];
            break;
    }

    self.canShowCallout = YES;
}

- (BOOL)canShowCallout {
    return NO;
}

@end
