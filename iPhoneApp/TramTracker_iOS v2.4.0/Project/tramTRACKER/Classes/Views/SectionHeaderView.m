//
//  NearbyHeaderView.m
//  tramTRACKER
//
//  Created by Tom King on 1/11/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "SectionHeaderView.h"
#import <QuartzCore/QuartzCore.h>

@interface SectionHeaderView()

@property (weak, nonatomic) id target;
@property SEL action;
@property (getter = isExpanded) BOOL expanded;
@property (weak, nonatomic) IBOutlet UIImageView *topSection;
@property (weak, nonatomic) IBOutlet UIImageView *bottomSection;

@property (weak, nonatomic) IBOutlet UIImageView *accessoryImage;

@end

@implementation SectionHeaderView

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        self.expanded = YES;
    }
    return self;
}

+ (SectionHeaderView *)nearbySectionHeaderWithTitle:(NSString *)aTitle
{
    static NSString     * const NearbySectionHeaderXIB = @"NearbySectionHeader";
    SectionHeaderView   * view = [SectionHeaderView viewWithXIBName:NearbySectionHeaderXIB];
    
	view.headerText.text = aTitle;
    return view;
}

+ (SectionHeaderView *)staticSectionHeaderWithTitle:(NSString *)aTitle
{
    static NSString     * const NearbySectionHeaderXIB = @"NearbySectionHeader";
    SectionHeaderView   * view = [SectionHeaderView viewWithXIBName:NearbySectionHeaderXIB];
    
    view.topSection.hidden = view.bottomSection.hidden = YES;
	view.headerText.text = aTitle;
    return view;
}

+ (SectionHeaderView *)PIDSectionHeaderView
{
    static NSString * const PIDSectionHeaderXIB = @"PIDSectionHeader";
    return [SectionHeaderView viewWithXIBName:PIDSectionHeaderXIB];
}

+ (SectionHeaderView *)expandableSectionHeaderViewWithTitle:(NSString *)aTitle
                                                    section:(NSInteger)aSection
                                                     target:(id)aTarget
                                                     action:(SEL)aAction
{
    static NSString     * const ExpandableSectionHeaderXIB = @"GreyExpandableSectionHeader";
    SectionHeaderView   * view = [SectionHeaderView viewWithXIBName:ExpandableSectionHeaderXIB];
    
    view.headerText.text = aTitle;
    view.section = aSection;
    view.target = aTarget;
    view.action = aAction;
    
    [view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:view action:@selector(expandCollapseAction)]];
    
    return view;
}

+ (SectionHeaderView *)greyExpandableSectionHeaderViewWithTitle:(NSString *)aTitle
                                                    section:(NSInteger)aSection
                                                     target:(id)aTarget
                                                     action:(SEL)aAction
{
    static NSString     * const ExpandableSectionHeaderXIB = @"GreyExpandableSectionHeader";
    SectionHeaderView   * view = [SectionHeaderView viewWithXIBName:ExpandableSectionHeaderXIB];
    
    view.headerText.text = aTitle;
    view.section = aSection;
    view.target = aTarget;
    view.action = aAction;
    
    [view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:view action:@selector(expandCollapseAction)]];
    
    return view;
}

+ (SectionHeaderView *)viewWithXIBName:(NSString *)aName
{
	SectionHeaderView * sectionHeaderView = [[NSBundle mainBundle] loadNibNamed:aName owner:self options:nil][0];
    return sectionHeaderView;
}

+ (SectionHeaderView *)rightSectionHeaderViewWithTitle:(NSString *)aTitle
{
    static NSString     * const RightSectionHeaderXIB = @"RightSectionHeader";
    SectionHeaderView   * view = [SectionHeaderView viewWithXIBName:RightSectionHeaderXIB];
    
    view.headerText.text = aTitle;
    return view;
}

- (void)setExpandedState:(BOOL)state
{
    UIImage * nextImage = [UIImage imageNamed:state ? @"List-Collapse" : @"List-Expand"];
    
    self.accessoryImage.image = self.accessoryImage.highlightedImage = nextImage;
    self.expanded = state;
}

- (void)expandCollapseAction
{
    [self setExpandedState:!self.isExpanded];
    [self.target performSelector:self.action withObject:self afterDelay:0];
}

@end
