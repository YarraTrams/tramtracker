//
//  DynamicSplashView.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 24/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import "DynamicSplashView.h"
#import "ActivityIndicator.h"
#import "BackgroundSynchroniser.h"
#import "FeaturePopup.h"

@interface DynamicSplashView ()

@property (weak, nonatomic) IBOutlet ActivityIndicator* indicator;
@property (weak, nonatomic) IBOutlet UIImageView*                bg;
@property (weak, nonatomic) IBOutlet UILabel*                    updatesLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView*    updatesProgress;

@property (nonatomic, strong) UIDynamicAnimator         * animator;
@property (nonatomic, strong) UICollisionBehavior       * collision;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint* topSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint* topSpaceActivity;

@end

@implementation DynamicSplashView

+ (DynamicSplashView *)splashWithFrame:(CGRect)frame
{
    static NSString * const aName = @"Splash";
    
    DynamicSplashView * v = [[NSBundle mainBundle] loadNibNamed:aName owner:self options:nil].firstObject;
    [v.indicator loadImages:ActivityIndicatorTramming];
    [v.indicator startAnimating];
    v.frame = frame;
    [[NSNotificationCenter defaultCenter] addObserver:v selector:@selector(showUpdateProgress:) name:@"ShowUpdateProgress" object:nil];
    return v;
}

- (void)dealloc
{ [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ShowUpdateProgress" object:nil]; }

- (void)runUpdatesAndDisappear
{
    PidsService* service = [PidsService new];
    [service submitKillSwitchInfoRequest:^(BOOL successful, NSError *error) {
        [service submitInfoPageRequest:^(NSArray* features, NSError *error) {
            // Regardless of this operation's success, the updates process must be run as usual.
            tramTRACKERAppDelegate* delegate = [[UIApplication sharedApplication] delegate];
            
            NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
            NSString* jsonString = [userDefaults objectForKey:@"updateString"];
            if(jsonString)
            { [delegate installUpdates:jsonString]; }
            else
            { [delegate downloadMissingMaps]; }
            
            [delegate checkForUpdates];
            [self transitionToApp:features];
        }];
    }];
}

- (void)transitionToApp:(NSArray*)features
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if(features)
        {
            if([FeaturePopup shouldShowFeaturePopup:[features mutableCopy]])
            {
                // Display the feature popup. If the call couldn't get which fields to display (or all of the features have already been seen), display none of them.
                tramTRACKERAppDelegate* delegate = (tramTRACKERAppDelegate*)[UIApplication sharedApplication].delegate;
                FeaturePopup* featurePopup = [FeaturePopup createAndShowPopupOverView:delegate.window.rootViewController.view];
                if(featurePopup)
                { featurePopup.features = features; }
            }
        }
        
        tramTRACKERAppDelegate* delegate = [UIApplication sharedApplication].delegate;
        [delegate autoOpenPID];
        [UIView animateWithDuration:0.5f animations:^{
            self.alpha = 0.0f;
        } completion:^(BOOL finished) {
            if (finished)
            { [self removeFromSuperview]; }
        }];
    });
}

- (void)showUpdateProgress:(NSNotification*)notification
{
    self.updatesLabel.hidden = NO;
    self.updatesProgress.hidden = NO;
    [self.updatesProgress startAnimating];
}

@end
