//
//  AutomaticSynchroniserDataLogger.h
//  tramTRACKER
//
//  Created by Robert Amos on 6/08/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BackgroundSynchroniserDelegate.h"

@class Stop;
@class Route;

/**
 * Creates an appends updates messages to the log during a background synchronisation. This
 * implemented the BackgroundSynchroniserDelete protocol to watch for sync messages.
 *
 * @ingroup Sync
 **/
@interface AutomaticSynchroniserDataLogger : NSObject <BackgroundSynchroniserDelegate> {

	/**
	 * A mutable copy of all stop changes that have occured so far
	**/
	NSMutableDictionary *stopChanges;

	/**
	 * A mutable copy of all route changes that have occured so far.
	**/
	NSMutableDictionary *routeChanges;

	/**
	 * The existing update list.
	**/
	NSMutableArray *updateList;
}

/**
 * Initialises the Auromatic Synchroniser Data Logger
 *
 * @return				An initialised AutomaticSynchroniserDataLogger object
**/
- (id)initAutomaticSynchroniserDataLogger;

- (void)syncDidStart;
- (void)syncDidFinishAtTime:(NSDate *)finishTime;
- (void)syncDidStartStop:(Stop *)stop;
- (void)syncDidFinishStop:(Stop *)stop;
- (void)syncDidStartRoute:(Route *)route;
- (void)syncDidFinishRoute:(Route *)route;
- (void)syncDidAddOperation:(NSOperation *)operation;


@end
