//
//  TicketRetailerViewController.h
//  tramTRACKER
//
//  Created by Robert Amos on 26/07/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TicketRetailer;
@class TicketRetailerHeaderView;

@interface TicketRetailerViewController : UITableViewController {

	TicketRetailer *retailer;
}

@property (nonatomic, strong) TicketRetailer *retailer;
@property (weak, nonatomic, readonly) TicketRetailerHeaderView *headerView;

- (id)initWithTicketRetailer:(TicketRetailer *)aRetailer;
- (void)refreshRetailer:(NSNotification *)note;

@end
