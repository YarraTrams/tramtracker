//
//  AutomaticSynchroniserDataLogger.m
//  tramTRACKER
//
//  Created by Robert Amos on 6/08/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "AutomaticSynchroniserDataLogger.h"
#import "Stop.h"
#import "Route.h"
#import "BackgroundSynchroniser.h"
#import "HistoricalUpdate.h"
#import "PidsService.h"

@implementation AutomaticSynchroniserDataLogger

- (id)initAutomaticSynchroniserDataLogger
{
	if (self = [super init])
	{
		// we own ourselves so add a retain here, we release ourselves once we're told the sync has finished
	}
	return self;
}


#pragma mark -
#pragma mark Notifications from the Background Synchroniser

- (void)syncDidStart
{
	// grab the historical updates list so we can append to it
	NSArray *list = [HistoricalUpdate getHistoricalUpdatesList];
	updateList = [list mutableCopy];
}

- (void)syncDidFinishAtTime:(NSDate *)finishTime
{
	// save changes to the history
	NSArray *copyOfUpdateList = [updateList copy];
	[HistoricalUpdate saveHistoricalUpdatesList:copyOfUpdateList];
	 copyOfUpdateList = nil;
	
	// tell the app delegate to save this as our last synchronisation time
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	[d setLastSyncDate:finishTime];
	
	// we're finished, so we can release ourselves
}

- (void)syncDidStartStop:(Stop *)stop
{
    // if we're in trouble then don't do anything here
    if (stop == nil)
        return;

	// store some information about the stop for later comparison
	NSString *name = stop.name;
	NSNumber *isPlatformStop = [NSNumber numberWithBool:[stop isPlatformStop]];
	NSDictionary *stopInfo = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:(name == nil ? @"" : name), isPlatformStop, nil]
														 forKeys:[NSArray arrayWithObjects:@"name", @"platform", nil]];
	[stopChanges setObject:stopInfo forKey:stop.trackerID];
}

- (void)syncDidFinishStop:(Stop *)stop
{
	// set the default message
	NSString *name = [NSString stringWithFormat:NSLocalizedString(@"stop-name-mixed-list", nil), stop.number, stop.name];
	NSString *message = [NSString stringWithFormat:NSLocalizedString(@"updates-stop-updated-template", nil), [stop displayedCityDirection]];
	NSDate *date = [NSDate date];
	
	// get the old stop info
	NSDictionary *oldStopInfo = [stopChanges objectForKey:stop.trackerID];
	if (oldStopInfo != nil)
	{
		// do detecting - new stop
		if ([oldStopInfo objectForKey:@"name"] == nil)
			message = [NSString stringWithFormat:NSLocalizedString(@"updates-stop-added-template", nil), [stop displayedCityDirection]];
		// upgraded to a platform stop
		else if (![[oldStopInfo objectForKey:@"platform"] boolValue] && [stop isPlatformStop])
			message = [NSString stringWithFormat:NSLocalizedString(@"updates-stop-platform-template", nil), [stop displayedCityDirection]];
	}
	
	// create a history item
	HistoricalUpdate *update = [[HistoricalUpdate alloc] initHistoricalUpdate];
	[update setName:name];
	[update setMessage:message];
	[update setUpdatedDate:date];
	[updateList insertObject:update atIndex:0];
	
}
- (void)syncDidStartRoute:(Route *)route
{
	if (route.name != nil)
	{
		NSString *name = route.name;
		NSString *upDestination = (route.upDestination != nil ? route.upDestination : @"");
		NSString *downDestination = (route.downDestination != nil ? route.downDestination : @"");
		NSNumber *isSubRoute = [NSNumber numberWithBool:[route isSubRoute]];
		NSDictionary *routeInfo = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:name, upDestination, downDestination, isSubRoute, nil]
															  forKeys:[NSArray arrayWithObjects:@"name", @"up", @"down", @"subroute", nil]];
		[routeChanges setObject:routeInfo forKey:route.internalNumber];
	}
}
- (void)syncDidFinishRoute:(Route *)route
{
	// set the default message
	NSString *name = [NSString stringWithFormat:NSLocalizedString(([route isSubRoute] ? @"route-name-subroute" : @"route-name"), nil), route.number, route.name];
	NSString *message = NSLocalizedString(@"updates-route-updated-template", nil);
	NSDate *date = [NSDate date];
	
	// get the old stop info
	NSDictionary *oldRouteInfo = [routeChanges objectForKey:route.internalNumber];
	if (oldRouteInfo == nil)
	{
		// do detecting - new route
		message = NSLocalizedString(@"updates-route-added-template", nil);
		
	} else
	{
		// has a destination changed?
		if (route.upDestination != nil && route.downDestination != nil && (![route.upDestination isEqualToString:[oldRouteInfo objectForKey:@"up"]] || ![route.downDestination isEqualToString:[oldRouteInfo objectForKey:@"down"]]))
			message = [NSString stringWithFormat:NSLocalizedString(@"updates-route-destination-template", nil), route.upDestination, route.downDestination];
	}
	
	// create a history item
	HistoricalUpdate *update = [[HistoricalUpdate alloc] initHistoricalUpdate];
	[update setName:name];
	[update setMessage:message];
	[update setUpdatedDate:date];
	[updateList insertObject:update atIndex:0];
}
- (void)syncDidAddOperation:(NSOperation *)operation
{
}

- (void)syncDidUpdateTicketOutlets
{
	//NSLog(@"Ticket Outlets updated");
	
	// create a history item
	HistoricalUpdate *update = [[HistoricalUpdate alloc] initHistoricalUpdate];
	[update setName:NSLocalizedString(@"updates-log-tickets-name", nil)];
	[update setMessage:NSLocalizedString(@"updates-log-tickets-message", nil)];
	[update setUpdatedDate:[NSDate date]];
	[updateList insertObject:update atIndex:0];
	
}


- (void)syncDidFailWithError:(NSError *)error
{
	// if its merely a connection error then we don't bother
	if ([error code] != PIDServiceErrorNotReachable)
	{
		// sync failures
		//NSLog(@"Synchronisation Failed: %@", error);
		tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
//		[d syncDidFailWithMessage:NSLocalizedString(@"updates-sync-alert-message", nil)];
	}
}

- (void)syncWasAbortedDueToMemoryWarning
{
    //NSLog(@"Synchronisation was aborted due to low memory warning");

	// create a history item
	HistoricalUpdate *update = [[HistoricalUpdate alloc] initHistoricalUpdate];
	[update setName:NSLocalizedString(@"updates-log-memoryabort-name", nil)];
	[update setMessage:[NSString stringWithFormat:NSLocalizedString(@"updates-log-memoryabort-message", nil), [[UIDevice currentDevice] name]]];
	[update setUpdatedDate:[NSDate date]];
	[updateList insertObject:update atIndex:0];

	// save changes to the history
	NSArray *copyOfUpdateList = [updateList copy];
	[HistoricalUpdate saveHistoricalUpdatesList:copyOfUpdateList];
	 copyOfUpdateList = nil;

    // release ourselves
}

@end
