//
//  ServiceChangeViewController.h
//  tramTRACKER
//
//  Created by Robert Amos on 21/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServiceChange.h"

@interface ServiceChangeViewController : UIViewController {
	ServiceChange *change;
}

@property (nonatomic, strong) ServiceChange *change;

- (id)initWithServiceChange:(ServiceChange *)aChange;

@end
