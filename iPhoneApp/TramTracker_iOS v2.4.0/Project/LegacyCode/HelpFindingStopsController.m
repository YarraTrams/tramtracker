//
//  HelpFindingStopsController.m
//  tramTRACKER
//
//  Created by Robert Amos on 4/05/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "HelpFindingStopsController.h"

const NSString *HelpURLSubdirFind = @"finding_stops";

#define HELP_FINDINGSTOP_BROWSE 0
#define HELP_FINDINGSTOP_NEARBY 1
#define HELP_FINDINGSTOP_SEARCH 2
#define HELP_FINDINGSTOP_MOSTRECENT 3
#define HELP_FINDINGSTOP_ENTERID 4

@implementation HelpFindingStopsController

- (id)initHelpFindingStopsController
{
	if (self = [super initWithStyle:UITableViewStylePlain])
	{
		[self setTitle:NSLocalizedString(@"help-finding-stop", @"Help")];
		[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
	}
	return self;
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}


#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    SimpleCell *cell = (SimpleCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[SimpleCell alloc] initWithCellStyle:TTSimpleCellStyleDefault reuseIdentifier:CellIdentifier];
		[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
		
		GradientBackgroundCell *background = [[GradientBackgroundCell alloc] initWithFrame:CGRectMake(0, 0, 320, 44) cellStyle:OnboardCellStyleNone];
		[cell setBackgroundView:background];
	}
    
	switch (indexPath.row)
	{
		case HELP_FINDINGSTOP_BROWSE:
			[cell setTextLabelText:NSLocalizedString(@"help-findingstops-browse", @"Browse")];
			break;
		case HELP_FINDINGSTOP_NEARBY:
			[cell setTextLabelText:NSLocalizedString(@"help-findingstops-nearby", @"Nearby")];
			break;
		case HELP_FINDINGSTOP_SEARCH:
			[cell setTextLabelText:NSLocalizedString(@"help-findingstops-search", @"Search")];
			break;
		case HELP_FINDINGSTOP_MOSTRECENT:
			[cell setTextLabelText:NSLocalizedString(@"help-findingstops-mostrecent", @"Most Recent")];
			break;
		case HELP_FINDINGSTOP_ENTERID:
			[cell setTextLabelText:NSLocalizedString(@"help-findingstops-enterid", @"Enter ID")];
			break;
	}
	
    return cell;	
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

	// we create a UIWebView that we push onto the stack, start by working out our URL
	NSString *fileName = nil;
	
	switch (indexPath.row)
	{
		case HELP_FINDINGSTOP_BROWSE:
			fileName = @"browse15.html";
			break;
			
		case HELP_FINDINGSTOP_NEARBY:
			fileName = @"nearby15.html";
			break;
			
		case HELP_FINDINGSTOP_SEARCH:
			fileName = @"search15.html";
			break;
			
		case HELP_FINDINGSTOP_MOSTRECENT:
			fileName = @"most_recent15.html";
			break;
			
		case HELP_FINDINGSTOP_ENTERID:
			fileName = @"enter_id15.html";
			break;
	}
	
	// nothing found?
	if (fileName == nil)
		return;
	
	// create the web view
	HelpWebController *helpWeb = [[HelpWebController alloc] initWithHelpFile:[NSString stringWithFormat:@"%@/%@", HelpURLSubdirFind, fileName]];
	SimpleCell *cell = (SimpleCell *)[self.tableView cellForRowAtIndexPath:indexPath];
	[helpWeb setTitle:cell.textLabel.text];
	[self.navigationController pushViewController:helpWeb animated:YES];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/




@end

