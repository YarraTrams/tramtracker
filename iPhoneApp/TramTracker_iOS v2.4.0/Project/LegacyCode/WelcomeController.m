//
//  WelcomeController.m
//  tramTRACKER
//
//  Created by Robert Amos on 2/04/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "WelcomeController.h"


@implementation WelcomeController

 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {

		WelcomeView *welcome = (WelcomeView *)self.view;
		[welcome setContentSize:CGSizeMake(320, 800)];
	}
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
	NSDictionary *bundle = [[NSBundle mainBundle] infoDictionary];
	[self setTitle:[NSString stringWithFormat:@"tramTRACKER %@", [bundle objectForKey:@"CFBundleVersion"]]];
	[self.navigationController.navigationBar setTintColor:[UIColor colorWithRed:TTNavigationBarTintColorRed green:TTNavigationBarTintColorGreen blue:TTNavigationBarTintColorBlue alpha:0.8]];
}

- (IBAction)didClickButton:(id)sender
{
    [self.parentViewController dismissViewControllerAnimated:YES completion:NULL];
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}






@end


@implementation WelcomeView

@end
