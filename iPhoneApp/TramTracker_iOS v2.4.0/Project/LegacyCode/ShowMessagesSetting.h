//
//  ShowMessagesSetting.h
//  tramTRACKER
//
//  Created by Robert Amos on 30/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SimpleCell.h"

@class PredictionListView;


@interface ShowMessagesSetting : UITableViewController {
	NSInteger showMessages;
	id target;
	SEL action;
}

@property (nonatomic) NSInteger showMessages;

// custom init
- (id)initWithShowMessages:(NSInteger)messages;
- (id)initWithShowMessages:(NSInteger)messages target:(id)aTarget action:(SEL)aAction;

@end
