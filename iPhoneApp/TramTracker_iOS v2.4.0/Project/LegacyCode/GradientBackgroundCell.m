//
//  GradientBackgroundCell.m
//  tramTRACKER
//
//  Created by Robert Amos on 13/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "GradientBackgroundCell.h"

@implementation GradientBackgroundCell

@synthesize highlightLayer, destinationHighlightLayer, style;

- (id)initWithFrame:(CGRect)frame cellStyle:(NSInteger)cellStyle
{
    if (self = [super initWithFrame:frame]) {
        // Initialization code
		[self setStyle:cellStyle];
		
		// add the highlight layer
		HighlightedBackgroundCell *i = [[HighlightedBackgroundCell alloc] initWithFrame:frame cellStyle:cellStyle];
		[self setHighlightLayer:i];
		[self addSubview:i];
		[i setHidden:YES];
		
		// and the destination highlight layer
		DestinationHighlightedBackgroundCell *d = [[DestinationHighlightedBackgroundCell alloc] initWithFrame:frame];
		[self setDestinationHighlightLayer:d];
		[self addSubview:d];
		[d setHidden:YES];
    }
    return self;
}


- (void)drawRect:(CGRect)rect {

	// draw a gradient background
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGFloat minWidth = 60;
	
	// colour components and locations
	CGFloat locations[3] = { 0.0, 0.6, 1.0 };
	CGFloat components[12] = 
	{
		1.0, 1.0, 1.0, 1.0,							// white
		0.95, 0.95, 0.95, 1.0,						// gray
		0.98, 0.98, 0.98, 1.0						// dark grey
	};
	
	// make our gradient
	CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB();
	CGGradientRef gradient = CGGradientCreateWithColorComponents(space,	components, locations, 3);
	
	// draw our gradient in our view
	CGContextDrawLinearGradient(context, gradient, CGPointMake(0, 0), CGPointMake(0, rect.size.height), 0);

	CGContextSetRGBFillColor(context, 0.87, 0.87, 0.87, 1.0);
	CGContextFillRect(context, CGRectMake(0, 43, 320, 1));

	if (self.style == OnboardCellStyleMinutesOnRight)
	{

		// draw a coloured gradient in right hand side
		CGFloat greenComponents[12] =
		{
			0.0, 0.5, 0.0, 1.0,					// green
			0.0, 0.40, 0.0, 1.0,				// darker green
			0.0, 0.45, 0.0, 1.0					// in-between green
		};
		CGGradientRef greenGradient = CGGradientCreateWithColorComponents(space, greenComponents, locations, 3);

		CGContextSaveGState(context);
		CGContextClipToRect(context, CGRectMake(rect.size.width-(rect.size.height < minWidth ? minWidth : rect.size.height), 0, (rect.size.height < minWidth ? minWidth : rect.size.height), rect.size.height-1));
		CGContextDrawLinearGradient(context, greenGradient, CGPointMake(0, 0), CGPointMake(0, rect.size.height), 0);
		CGGradientRelease(greenGradient);
	} else if (self.style == OnboardCellStyleMinutesOnRightDisabled)
	{
		// draw a coloured gradient in right hand side
		CGFloat grayComponents[12] =
		{
			0.5, 0.5, 0.5, 1.0,					// green
			0.40, 0.40, 0.40, 1.0,				// darker green
			0.45, 0.45, 0.45, 1.0					// in-between green
		};
		CGGradientRef grayGradient = CGGradientCreateWithColorComponents(space, grayComponents, locations, 3);
		
		CGContextSaveGState(context);
		CGContextClipToRect(context, CGRectMake(rect.size.width-(rect.size.height < minWidth ? minWidth : rect.size.height), 0, (rect.size.height < minWidth ? minWidth : rect.size.height), rect.size.height-1));
		CGContextDrawLinearGradient(context, grayGradient, CGPointMake(0, 0), CGPointMake(0, rect.size.height), 0);
		CGGradientRelease(grayGradient);

	} else if (self.style == OnboardCellStyleMinutesOnRightLowAccuracy)
	{
		// draw a coloured gradient in right hand side
		CGFloat redComponents[12] =
		{
			0.8, 0.0, 0.0, 1.0,							// red
			0.70, 0.0, 0.0, 1.0,						// darker red
			0.75, 0.0, 0.0, 1.0						// in between red
		};
		CGGradientRef redGradient = CGGradientCreateWithColorComponents(space, redComponents, locations, 3);
		
		CGContextSaveGState(context);
		CGContextClipToRect(context, CGRectMake(rect.size.width-(rect.size.height < minWidth ? minWidth : rect.size.height), 0, (rect.size.height < minWidth ? minWidth : rect.size.height), rect.size.height-1));
		CGContextDrawLinearGradient(context, redGradient, CGPointMake(0, 0), CGPointMake(0, rect.size.height), 0);
		CGGradientRelease(redGradient);
	}

	// release things
	/*CGGradientRelease(gradient);
	CGColorSpaceRelease(space);
	CGContextRelease(context); */
	CGGradientRelease(gradient);
	CGColorSpaceRelease(space);
}




@end

@implementation HighlightedBackgroundCell

- (id)initWithFrame:(CGRect)frame cellStyle:(NSInteger)aStyle {
    if (self = [super initWithFrame:frame]) {
		style = aStyle;
    }
    return self;
}


- (void)drawRect:(CGRect)rect {
	
	// draw a gradient background
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB();
	CGGradientRef gradient;
	
	// colour components and locations
	CGFloat locations[3] = { 0.0, 0.6, 1.0 };
	
	if (style == OnboardCellStyleMinutesOnRightLowAccuracy)
	{
		CGFloat components[12] = {
			1.0, 0.8, 0.8, 1.0,							// light red
			0.90, 0.70, 0.70, 1.0,						// darker red
			0.95, 0.75, 0.75, 1.0						// in between red
		};
		gradient = CGGradientCreateWithColorComponents(space, components, locations, 3);

	} else if (style == OnboardCellStyleMinutesOnRightDisabled)
	{
		CGFloat components[12] = {
			0.8, 0.8, 0.8, 1.0,							// light red
			0.70, 0.70, 0.70, 1.0,						// darker red
			0.75, 0.75, 0.75, 1.0						// in between red
		};
		gradient = CGGradientCreateWithColorComponents(space, components, locations, 3);
			
	} else
	{
		CGFloat components[12] = {
			0.8, 1.0, 0.8, 1.0,							// light green
			0.70, 0.90, 0.70, 1.0,						// darker green
			0.75, 0.95, 0.75, 1.0						// in between green
		};
		gradient = CGGradientCreateWithColorComponents(space, components, locations, 3);
	}	
	
	// draw our gradient in our view
	CGContextDrawLinearGradient(context, gradient, CGPointMake(0, 0), CGPointMake(0, rect.size.height), 0);
	
	// release things
	/*CGGradientRelease(gradient);
	 CGColorSpaceRelease(space);
	 CGContextRelease(context); */
	CGGradientRelease(gradient);
	CGColorSpaceRelease(space);
}




@end

@implementation DestinationHighlightedBackgroundCell

- (void)drawRect:(CGRect)rect
{
	// draw a gradient background
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB();
	CGGradientRef gradient;
	
	// colour components and locations
	CGFloat locations[3] = { 0.0, 0.6, 1.0 };
	CGFloat components[12] = {
		1.0, 1.0, 0.8, 1.0,							// light yellow
		0.90, 0.90, 0.70, 1.0,						// darker yellow
		0.95, 0.95, 0.75, 1.0						// in between yellow
	};
	gradient = CGGradientCreateWithColorComponents(space, components, locations, 3);
	// draw our gradient in our view
	CGContextDrawLinearGradient(context, gradient, CGPointMake(0, 0), CGPointMake(0, rect.size.height), 0);
	
	// release things
	/*CGGradientRelease(gradient);
	 CGColorSpaceRelease(space);
	 CGContextRelease(context); */
	CGGradientRelease(gradient);
	CGColorSpaceRelease(space);
}


@end

