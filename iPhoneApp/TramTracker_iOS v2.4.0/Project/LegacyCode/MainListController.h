//
//  MainListController.h
//  tramTRACKER
//
//  Created by Robert Amos on 26/02/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StopList.h"
#import "RouteListController.h"
#import "PidsService.h"
#import "PagedPIDViewController.h"
#import "PIDViewController.h"
#import "FavouriteCell.h"
#import <objc/runtime.h>
#import "UnderBackgroundView.h"
#import "Filter.h"
#import "Stop.h"

@class MapViewControllerOld;
//@class FavouriteStop;

/**
 * @ingroup Lists
**/

/**
 * A custom UITableViewController that displays and allows for editing of a list of favourite stops
**/
@interface MainListController : UITableViewController <UINavigationControllerDelegate> {
	NSDictionary *stops;
	NSArray *sections;
	NSArray *favouriteStops;
}

@property (weak, readonly) UIBarButtonItem *rightButtonItem;

/**
 * Create and Push a Paged PID onto the stack when the user selects a favourite
 *
 * @param	favouriteIndex	The index in the table of the selected favourite stop
**/
- (void)pushPIDWithFavourite:(NSInteger)favouriteIndex;
- (void)pushPIDWithFavourite:(NSInteger)favouriteIndex animated:(BOOL)animated;

/**
 * Creates and Pushes a Paged PID which loads the nearest stop.
 *
 * @param	animated		Whether the Push should be animated
**/
- (void)pushPIDWithNearbyFavouritesAnimated:(BOOL)animated;
- (void)pushPIDWithNearbyFavouritesAnimated;

/**
 * Reloads the favourites if they have been updated while we're open.
 *
 * @param	note	The NSNotification object that triggered this message.
**/
- (void)refreshFavourites:(NSNotification *)note;
- (void)reloadFavourites;

//- (FavouriteStop *)favouriteAtIndexPath:(NSIndexPath *)indexPath;
- (NSInteger)indexAtIndexPath:(NSIndexPath *)indexPath;

/**
 * Flips over to the Map View
 **/
- (void)flipToMapView;

/**
 * Creates the table Footer view
**/
- (UIView *)tableFooterView;

- (void)toggleEditing;
- (UIBarButtonItem *)editButtonItem;
- (UIBarButtonItem *)rightButtonItem;
- (void)presentGroupEditingDialog;

@end
