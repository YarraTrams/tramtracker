//
//  DepartureTimePicker.h
//  tramTRACKER
//
//  Created by Robert Amos on 6/04/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DepartureTimePicker : UIViewController {
	UIButton *departureDate;
	UIButton *departureTime;
	NSDate *scheduledDepartureTime;
	UIDatePicker *picker;
	id target;
	SEL action;
}

@property (nonatomic, strong) IBOutlet UIButton *departureDate;
@property (nonatomic, strong) IBOutlet UIButton *departureTime;
@property (nonatomic, strong) NSDate *scheduledDepartureTime;
@property (nonatomic, strong) IBOutlet UIDatePicker *picker;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil scheduledDepartureTime:(NSDate *)date target:(id)target action:(SEL)selector;
- (IBAction)pickerDidChangeValue:(id)sender;
- (void)updateDateAndTimeButtons;

@end
