//
//  HelpAboutController.h
//  tramTRACKER
//
//  Created by Robert Amos on 6/05/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface HelpAboutController : UIViewController {
	UILabel *version;
	NSDate *fT;
	NSMutableString	*x;
	CGPoint	cTP;
	UIImageView *logo;
	UILabel *bodyLabel1;
	UILabel *bodyLabel2;
}

@property (nonatomic, strong) IBOutlet UILabel *version;
@property (nonatomic, strong) IBOutlet UIImageView *logo;
@property (nonatomic, strong) IBOutlet UILabel *bodyLabel1;
@property (nonatomic, strong) IBOutlet UILabel *bodyLabel2;

- (id)initHelpAboutController;

@end
