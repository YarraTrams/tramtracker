//
//  SearchListController.h
//  tramTRACKER
//
//  Created by Robert Amos on 6/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PIDViewController.h"
#import "MainListController.h"
#import "SearchResultCell.h"
#import "UnderBackgroundView.h"

/**
 * @ingroup Lists
**/

/**
 * A custom UITableViewController that manages a search box and results list
**/
@interface SearchListController : UITableViewController <UINavigationControllerDelegate, UISearchBarDelegate> {
	
	/**
	 * A cached list of the stops used to generate and maintain the results list. Is updated by the search functions
	**/
	NSArray *stopList;
}

@property (nonatomic, strong) NSArray *stopList;

/**
 * Table Footer View
**/
- (UIView *)tableFooterView;

@end
