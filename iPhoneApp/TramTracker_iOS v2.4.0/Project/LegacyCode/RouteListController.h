//
//  RouteListController.h
//  tramTRACKER
//
//  Created by Robert Amos on 27/02/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Route.h"
#import "PidsService.h"
#import "StopListController.h"
#import "MainListController.h"
#import "OnboardListView.h"
#import "MapViewController.h"
#import "SimpleCell.h"
#import "RouteList.h"

/**
 * @defgroup Lists Lists and List Controllers
**/

/**
 * @ingroup Lists
**/

/**
 * A custom UITableViewController that retrieves and displays a list of Routes
**/
@interface RouteListController : UITableViewController <UINavigationControllerDelegate> {
	
	/**
	 * The list of Route objects to display. The Routes are cached here after being loaded.
	**/
	NSArray *routeList;

	/**
	 * An indexed list of the routeNumbers listed. This is used to generate and manage the right-hand shortcut index
	**/
	NSArray *routeNumbers;

	/**
	 * The sub-menu (list of stops) made when selecting a route + direction
	**/
	StopListController *stopList;
	
	/**
	 * The onboard view if created
	**/
	OnboardListView *onboardList;

	/**
	 * A loading indicator that sits in the middle of an empty table while we fetch the list of routes
	**/
	UIView *loadingIndicator;
	
	/**
	 * Whether we're pushing to an Onboard Screen or a standard stop list
	**/
	BOOL pushToOnboardScreen;
	
	/**
	 * Whether we're pushing (eventually) to a Scheduled Departures Screen
	**/
	BOOL pushToScheduledDeparturesScreen;
}

@property (nonatomic, strong) NSArray *routeList;
@property (nonatomic, strong) NSArray *routeNumbers;
@property (nonatomic, strong) StopListController *stopList;
@property (nonatomic, strong) UIView *loadingIndicator;
@property (nonatomic) BOOL pushToOnboardScreen;
@property (nonatomic, strong) OnboardListView *onboardList;
@property (nonatomic) BOOL pushToScheduledDeparturesScreen;

/**
 * Shows the loading indicator
**/
- (void)showLoading;

/**
 * Hides the loading indicator
**/
- (void)hideLoading;

/**
 * Indicates whether the loading indicator is currently visible
 *
 * @returns		YES if the loading indicator is visible, NO otherwise.
**/
- (BOOL)isLoadingVisible;

- (UIView *)tableFooterView;

@end
