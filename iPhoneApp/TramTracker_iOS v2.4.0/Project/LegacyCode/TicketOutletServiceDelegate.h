////
////  TicketOutletServiceDelegate.h
////  tramTRACKER
////
////  Created by Robert Amos on 5/08/10.
////  Copyright 2010 Metro Trains Melbourne. All rights reserved.
////
//
//#import <Foundation/Foundation.h>
//
//typedef enum
//{
//    TicketOutletServiceDelegateCheck,
//    TicketOutletServiceDelegateUpdate
//}   TTTicketOutletServiceType;
//
//@interface TicketOutletServiceDelegate : NSObject
//
//@property (weak, nonatomic, readonly) NSObject  * delegate;
//@property (nonatomic, strong) NSDate            * updatesSinceDate;
//@property (nonatomic) TTTicketOutletServiceType serviceType;
//
//- (id)initWithDelegate:(NSObject *)aDelegate;
//- (void)failWithError:(NSError *)error;
//
//- (void)doesTicketOutletsHaveUpdatesUsingResponse:(NSHTTPURLResponse *)response;
//- (void)updateTicketOutletsWithData:(NSData *)data;
//
//@end
