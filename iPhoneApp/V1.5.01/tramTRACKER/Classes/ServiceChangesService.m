//
//  ServiceChangesService.m
//  tramTRACKER
//
//  Created by Robert Amos on 20/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "ServiceChangesService.h"
#import "ServiceChangesDelegate.h"
#import <SystemConfiguration/SCNetworkReachability.h>

NSString * const ServiceChangesURI = @"http://www.yarratrams.com.au/servicechanges.rss";
const NSInteger ServiceChangesServiceErrorNotReachable = 2001;

@implementation ServiceChangesService

@synthesize delegate, action;

- (id)initWithDelegate:(id)aDelegate action:(SEL)anAction
{
	if (self = [super init])
	{
		// set the target and the action
		delegate = aDelegate;
		action = anAction;
	}
	return self;
}

// get the service changes
- (void)getServiceChanges
{
	// initialise the URL Request
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	// Make sure that we have access to the interwebs
	if (![self hasAccessToService])
	{
		[pool drain];
		return;
	}
	
	NSRunLoop *runLoop = [NSRunLoop currentRunLoop];
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:ServiceChangesURI]];
	
	// Create the ServiceChangesDelegate, it will handle the connection and parsing of the response
	ServiceChangesDelegate *changesDelegate = (ServiceChangesDelegate *)[[ServiceChangesDelegate alloc] initWithDelegate:delegate action:action];
	
	// Start the connection
	//NSLog(@"Requesting service changes");
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	[[[NSURLConnection alloc] initWithRequest:request delegate:changesDelegate startImmediately:YES] autorelease];
	[runLoop runUntilDate:[NSDate dateWithTimeIntervalSinceNow:15]];
	CFRunLoopStop([runLoop getCFRunLoop]);
	[changesDelegate release];
	[pool drain];
	
}

- (void)getServiceChangesInBackgroundThread
{
	[self performSelectorInBackground:@selector(getServiceChanges) withObject:nil];
}

#pragma mark -
#pragma mark Checking service availablility and error handling

- (void)failWithError:(NSError *)error
{
	// check the delegate to see if we can report the error
	if (delegate != nil && [delegate respondsToSelector:@selector(serviceChangesServiceDidFailWithError:)])
	{
		[delegate performSelectorOnMainThread:@selector(serviceChangesServiceDidFailWithError:) withObject:error waitUntilDone:NO];
	}
}

// Check for the availability of the service
- (BOOL)hasAccessToService
{
	NSURL *host = [NSURL URLWithString:ServiceChangesURI];
	SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(NULL, [[host host] UTF8String]);
	SCNetworkReachabilityFlags flags;
	SCNetworkReachabilityGetFlags(reachability, &flags);
	
	BOOL isReachable = flags & kSCNetworkReachabilityFlagsReachable;
	//BOOL noConnectionRequired = !(flags & kSCNetworkReachabilityFlagsConnectionRequired);
	
	// close the reachability ref
	CFRelease(reachability);
	
	if (isReachable)// || noConnectionRequired)
		return YES;

	// is the service not reachable and a connection is required
	NSString *errorMessage = [NSString stringWithFormat:NSLocalizedString(@"errors-nonetwork-service-changes", @"No internet error"), [[UIDevice currentDevice] model]];
	NSString *recoverySuggestion = [NSString stringWithFormat:NSLocalizedString(@"errors-nonetwork-suggestion", @"No internet suggestion"), [[UIDevice currentDevice] model]];
	NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] initWithCapacity:0];
	[userInfo setObject:errorMessage forKey:NSLocalizedDescriptionKey];
	[userInfo setObject:recoverySuggestion forKey:NSLocalizedRecoverySuggestionErrorKey];
	NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier] code:ServiceChangesServiceErrorNotReachable userInfo:[NSDictionary dictionaryWithDictionary:userInfo]];
	[self failWithError:error];
	[userInfo release];
	return NO;
}

@end
