//
//  SpecialMessage.h
//  tramTRACKER
//
//  Created by Robert Amos on 16/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSInteger const SpecialMessageTypeSpecialEvent;
extern NSInteger const SpecialMessageTypeDisruption;

/**
 * Represents a special message as displayed by the PID.
**/
@interface SpecialMessage : NSObject {

	/**
	 * The message body.
	**/
	NSString *message;

	/**
	 * The type of message, either a SpecialMessageTypeSpecialEvent or a SpecialMessageTypeDisruption.
	**/
	NSInteger type;
}

@property (nonatomic, retain) NSString *message;
@property (nonatomic) NSInteger type;

@end
