//
//  StartupOptionSelector.h
//  tramTRACKER
//
//  Created by Robert Amos on 27/04/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SimpleCell.h"


@interface StartupOptionSelector : UITableViewController {
	NSInteger startupOption;
	id target;
	SEL action;
	NSArray *startupOptions;
}

- (id)initWithStartupOption:(NSInteger)aStartupOption target:(id)aTarget action:(SEL)anAction;

@end
