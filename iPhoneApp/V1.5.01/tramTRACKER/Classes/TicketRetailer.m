// 
//  TicketRetailer.m
//  tramTRACKER
//
//  Created by Robert Amos on 22/07/10.
//  Copyright 2010 Yarra Trams. All rights reserved.
//

#import "TicketRetailer.h"


@implementation TicketRetailer 

@dynamic open24Hour;
@dynamic name;
@dynamic address;
@dynamic sellsMyki;
@dynamic latitude;
@dynamic sellsMetcards;
@dynamic longitude;
@dynamic suburb;


/**
 * Returns all Ticket Retailers
**/
+ (NSArray *)allTicketRetailers
{
	// Grab our managed object context and create a fetch request
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [d managedObjectContext];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	// set the entity that we're looking for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"TicketRetailer" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// fetch it.
	NSError *outError;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&outError];
	
	// cant find any?
	if (fetchedObjects == nil || [fetchedObjects count] == 0)
	{
		[fetchRequest release];
		return nil;
	}
	
	// return them sorted
	[fetchRequest release];
	return fetchedObjects;	
}

/**
 * Returns all ticket retailers in a region
**/
+ (NSArray *)ticketRetailersInRegion:(MKCoordinateRegion)region
{
	CLLocationDegrees northernBoundary;
	CLLocationDegrees southernBoundary;
	CLLocationDegrees easternBoundary;
	CLLocationDegrees westernBoundary;
	
	// simple
	northernBoundary = region.center.latitude + region.span.latitudeDelta;
	southernBoundary = region.center.latitude - region.span.latitudeDelta;
	easternBoundary = region.center.longitude + region.span.longitudeDelta;
	westernBoundary = region.center.longitude - region.span.longitudeDelta;
	
	// make sure we haven't reversed this
	if (southernBoundary > northernBoundary)
	{
		CLLocationDegrees tmpBoundary = northernBoundary;
		northernBoundary = southernBoundary;
		southernBoundary = tmpBoundary;
	}
	if (westernBoundary > easternBoundary)
	{
		CLLocationDegrees tmpBoundary = easternBoundary;
		easternBoundary = westernBoundary;
		westernBoundary = tmpBoundary;
	}
	
	// Grab our managed object context and create a fetch request
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [d managedObjectContext];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"TicketRetailer" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"latitude BETWEEN { %@, %@ } AND longitude BETWEEN { %@, %@ }",
							  [NSNumber numberWithDouble:southernBoundary], [NSNumber numberWithDouble:northernBoundary],
							  [NSNumber numberWithDouble:westernBoundary], [NSNumber numberWithDouble:easternBoundary]];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (fetchedObjects == nil || [fetchedObjects count] == 0)
	{
		[fetchRequest release];
		return nil;
	}
	
	// return the object
	[fetchRequest release];
	return fetchedObjects;
}

+ (NSArray *)nearestTicketRetailersToLocation:(CLLocation *)location count:(NSUInteger)numberOfRetailers
{
	// make sure that we have a stop list
	NSArray *listOfRetailers = nil;
	
	// start with a decent sized region and go from there
	MKCoordinateRegion region;
	region.center.latitude = location.coordinate.latitude;
	region.center.longitude = location.coordinate.longitude;
	region.span.latitudeDelta = 0.01796550;
	region.span.longitudeDelta = 0.01982689;
	
	// loop through until we have enough stops, cap at 1000
	if (numberOfRetailers > 1000) numberOfRetailers = 1000;
	listOfRetailers = [TicketRetailer ticketRetailersInRegion:region];
	if ([listOfRetailers count] < numberOfRetailers)
	{
		while ([listOfRetailers count] < numberOfRetailers)
		{
			region.span.latitudeDelta = region.span.latitudeDelta * 2;
			region.span.longitudeDelta = region.span.longitudeDelta * 2;
			listOfRetailers = [TicketRetailer ticketRetailersInRegion:region];
		}
	}
	
	// create an array of ticket retailer distances
	NSMutableArray *distances = [[NSMutableArray alloc] initWithCapacity:0];
	
	// loop over the current retailers and work out the distance to the location
	for (TicketRetailer *retailer in listOfRetailers)
	{
		// create a new distance object
		TicketRetailerDistance *distance = [[TicketRetailerDistance alloc] init];
		
		// set the reference stop
		[distance setRetailer:retailer];
		
		// set the distance
		[distance setDistance:[LocationManager distanceFromLocation:location toLocation:[retailer location]]];
		
		// add the retailer to the list
		[distances addObject:distance];
		
		// we're finished with the distance object - the array will take it from here
		[distance release];
		distance = nil;
	}
	
	// sort the distance object
	[distances sortUsingSelector:@selector(compareDistance:)];
	
	// grab the nearest and release the whole list
	NSArray *nearestTicketRetailerDistances;
	if ([distances count] < numberOfRetailers)
		nearestTicketRetailerDistances = distances;
	else
		nearestTicketRetailerDistances = [distances subarrayWithRange:NSMakeRange(0, numberOfRetailers)];
	
	[distances release];
	distances = nil;
	
	// return the list
	return nearestTicketRetailerDistances;
}

+ (NSArray *)nearestTicketRetailersWithoutDistancesToLocation:(CLLocation *)location count:(NSUInteger)numberOfRetailers
{
	NSArray *retailers = [self nearestTicketRetailersToLocation:location count:numberOfRetailers];
	NSMutableArray *newRetailersArray = [[NSMutableArray alloc] initWithCapacity:0];
	for (TicketRetailerDistance *s in retailers)
		[newRetailersArray addObject:s.retailer];
	return [newRetailersArray autorelease];
}


- (BOOL)isOpen24Hours
{
	return [self.open24Hour boolValue];
}
- (BOOL)doesSellMetcard
{
	return [self.sellsMetcards boolValue];
}
- (BOOL)doesSellMyki
{
	return [self.sellsMyki boolValue];
}


- (CLLocation *)location
{
	NSAssert([self latitude] && [self longitude], @"Latitude or Longitude not set");
	
	return [[[CLLocation alloc]
			 initWithLatitude:[self.latitude doubleValue]
			 longitude:[self.longitude doubleValue]] autorelease];
}
- (CLLocationCoordinate2D)coordinate
{
	return [[self location] coordinate];
}

- (NSString *)title
{
	return self.name;
}
- (NSString *)subtitle
{
	return self.address;
}


- (void)launchGoogleMapsWithDirectionsToHere
{
	// launches the Google Maps application with the ticket retailer's location pinpointed, this will not return, tramTRACKER will close
	NSString *stringURL = [NSString stringWithFormat:@"http://maps.google.com/maps?daddr=%@", self.address];
	NSString *escapedStringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
	NSURL *url = [NSURL URLWithString:escapedStringURL];
	[[UIApplication sharedApplication] openURL:url];
}

- (void)launchGoogleMapsWithDirectionsFromHere
{
	// launches the Google Maps application with the ticket retailer's location pinpointed, this will not return, tramTRACKER will close
	NSString *stringURL = [NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%@", self.address];
	NSString *escapedStringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
	NSURL *url = [NSURL URLWithString:escapedStringURL];
	[[UIApplication sharedApplication] openURL:url];
}

@end


@implementation TicketRetailerDistance

@synthesize retailer;
@synthesize distance;

//
// Compare our distance from the comparisonLocation with the distance of another retailer
// 
- (NSComparisonResult)compareDistance:(TicketRetailerDistance *)otherTicketRetailerDistance
{
	return [[NSNumber numberWithDouble:[self distance]] compare:[NSNumber numberWithDouble:[otherTicketRetailerDistance distance]]];
}


// get the description about the ticket retailer distance
- (NSString *)description
{
	return [NSString stringWithFormat:@"%@ at distance %@m", retailer, [NSNumber numberWithDouble:distance]];
}

// return a formatted distance
- (NSString *)formattedDistance
{
	if (self.distance > 10000)
		return @"10 km+";
	if (self.distance == 10000)
		return @"10 km";
	if (self.distance >= 1000)
		return [NSString stringWithFormat:@"%.2f km", self.distance/1000];
	return [NSString stringWithFormat:@"%.0f m", self.distance];
}

- (void)dealloc
{
	// release the stop
	[retailer release];
	
	[super dealloc];
}

@end
