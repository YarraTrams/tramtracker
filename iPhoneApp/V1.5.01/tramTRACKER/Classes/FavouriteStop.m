//
//  FavouriteStop.m
//  tramTRACKER
//
//  Created by Robert Amos on 21/07/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "FavouriteStop.h"
#import "Stop.h"
#import "Filter.h"

@implementation FavouriteStop

@synthesize stop, filter, indexPath;

- (id)initWithStop:(Stop *)aStop filter:(Filter *)aFilter indexPath:(NSIndexPath *)anIndexPath
{
	if (self = [super init])
	{
		stop = [aStop retain];
		filter = [aFilter retain];
		indexPath = [anIndexPath retain];
	}
	return self;
}

- (NSComparisonResult)compareWithFavouriteStop:(FavouriteStop *)otherStop
{
	return [self.indexPath compare:otherStop.indexPath];
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"%@ with filter %@ at %@", stop, filter, indexPath];
}

- (void)dealloc
{
	[stop release];
	[filter release];
	[indexPath release];
	[super dealloc];
}

@end
