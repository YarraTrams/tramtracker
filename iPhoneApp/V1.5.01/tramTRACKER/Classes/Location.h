//
//  Location.h
//  tramTRACKER
//
//  Created by Robert Amos on 20/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>


@interface LocationManager : CLLocationManager {

#if TARGET_IPHONE_SIMULATOR
	NSTimer *refreshTimer;
	CLLocation *__oldLocation;
	CLLocation *location;
#endif
	BOOL stopped;
}


// we override a stack of functions should we be using the iPhone Simulator
@property (readonly, nonatomic) BOOL stopped;
#if TARGET_IPHONE_SIMULATOR

@property (nonatomic, retain) NSTimer *refreshTimer;
@property (nonatomic, retain) CLLocation *__oldLocation;
@property (readonly, nonatomic) CLLocation *location;

- (void)startUpdatingLocation;
- (void)stopUpdatingLocation;
- (void)updateLocation:(NSTimer *)aTimer;

#endif

- (BOOL)locationServicesEnabled;
+ (CLLocationDistance)distanceFromLocation:(CLLocation *)from toLocation:(CLLocation *)to;

@end
