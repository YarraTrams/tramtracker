//
//  FavouriteGroupEditCell.h
//  tramTRACKER
//
//  Created by Robert Amos on 21/07/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FavouriteGroupEditCell : UITableViewCell <UITextFieldDelegate> {
	UITextField *textField;
}

@property (readonly) UITextField *textField;

@end
