//
//  Journey.m
//  tramTRACKER
//
//  Created by Robert Amos on 9/04/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "Journey.h"


@implementation Journey

@synthesize tram, stops;

+ (Journey *)journeyForStub:(JourneyStub *)stub
{
    // create a new Journey
    Journey *journey = [[Journey alloc] init];
    
    // Set the tram
    if (stub.tramStub != nil)
        [journey setTram:[Tram tramForStub:stub.tramStub]];
    
    // Set the stops
    if (stub.stubStops != nil)
    {
        NSMutableArray *stops = [[NSMutableArray alloc] initWithCapacity:0];
        for (JourneyStopStub *s in stub.stubStops)
        {
            // add the stop
            [stops addObject:[JourneyStop journeyStopForStub:s]];
        }
        [journey setStops:stops];
        [stops release]; stops = nil;
    }
    NSLog (@"Journey Stub: %@, Journey: %@", stub, journey);
    
    // return the journey
    return [journey autorelease];
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"Journey of %@ arriving at %@", tram, stops];
}

- (JourneyStop *)journeyStopForStop:(Stop *)stop
{
	if (stop == nil || self.stops == nil || [self.stops count] == 0)
		return nil;
	
	// loop over the stops and find our stop
	for (JourneyStop *s in self.stops)
	{
		if ([stop isEqual:s.stop])
			return s;
	}
	return nil;
}

- (void)dealloc
{
	[tram release];
	[stops release];
	[super dealloc];
}

- (id)copyWithZone:(NSZone *)zone
{
	Journey *copy = [[Journey alloc] init];
	[copy setTram:[[self.tram copy] autorelease]];
	[copy setStops:[[self.stops copy] autorelease]];
	return copy;
}

@end

@implementation JourneyStop

@synthesize stop, predicatedArrivalDateTime, originalPredictedArrivalDateTime;

+ (JourneyStop *)journeyStopForStub:(JourneyStopStub *)stub
{
    JourneyStop *journeyStop = [stub copy];

    // Set the stop
    [journeyStop setStop:[Stop stopForStub:stub.stopStub]];
    
    return [journeyStop autorelease];
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"Stopping at %@ at %@", stop, predicatedArrivalDateTime];
}

- (NSTimeInterval)secondsUntilArrival
{
	NSTimeInterval secondsUntilArrivalTime = [self.predicatedArrivalDateTime timeIntervalSinceDate:[NSDate date]];
	return secondsUntilArrivalTime;
}

- (NSString *)minutesUntilArrival
{
	
	// work out the number of minutes to go
	int minutes = floor([self secondsUntilArrival] / 60);
	
	// if we're less than a minute (but not more than a minute past) return now
	if (minutes < 1)
		return NSLocalizedString(@"onboard-now", @"next");
	
	else
		return [NSString stringWithFormat:@"%d", minutes];
}

- (id)copyWithZone:(NSZone *)zone
{
	JourneyStop *copy = [[JourneyStop alloc] init];
	[copy setStop:stop];
	[copy setPredicatedArrivalDateTime:[[predicatedArrivalDateTime copy] autorelease]];
	[copy setOriginalPredictedArrivalDateTime:[[originalPredictedArrivalDateTime copy] autorelease]];
	return copy;
}

- (void)dealloc
{
	[stop release];
	[predicatedArrivalDateTime release];
	[super dealloc];
}

@end

@implementation JourneyStub

@synthesize tramStub, stubStops;

- (void)dealloc
{
    [tramStub release]; tramStub = nil;
    [stubStops release]; stubStops = nil;
    [super dealloc];
}
@end

@implementation JourneyStopStub

@synthesize stopStub;

- (void)dealloc
{
    [stopStub release]; stopStub = nil;
    [super dealloc];
}

@end
