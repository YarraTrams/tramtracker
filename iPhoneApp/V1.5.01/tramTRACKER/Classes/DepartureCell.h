//
//  DepartureCell.h
//  tramTRACKER
//
//  Created by Robert Amos on 6/04/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GradientBackgroundCell.h"
#import "OnboardListCell.h"


@interface DepartureCell : UITableViewCell {
	/**
	 * The label for the time to arrival
	 **/
	UILabel *arrival;
	
	/**
	 * The label for the stop's name
	 **/
	UILabel *name;
	
	/**
	 * The label for the route's description
	**/
	UILabel *routeDescription;
}

@property (nonatomic, retain) UILabel *arrival;
@property (nonatomic, retain) UILabel *name;
@property (nonatomic, retain) UILabel *routeDescription;

- (UILabel *)newLabelForArrival;
- (UILabel *)newLabelForName;
- (UILabel *)newLabelForRouteDescription;

@end
