//
//  HelpController.h
//  tramTRACKER
//
//  Created by Robert Amos on 4/05/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SimpleCell.h"
#import "GradientBackgroundCell.h"
#import "HelpBaseController.h"

@interface HelpController : HelpBaseController {

}

- (id)initHelpController;

@end
