//
//  HelpDataUsageController.m
//  tramTRACKER
//
//  Created by Robert Amos on 13/05/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "HelpDataUsageController.h"

const NSString *HelpURLSubdirDataUsage = @"data_usage";

#define HELP_DATAUSAGE_PID 0
#define HELP_DATAUSAGE_SCHEDULES 1
#define HELP_DATAUSAGE_ONBOARD 2
#define HELP_DATAUSAGE_SYNC 3

@implementation HelpDataUsageController

- (id)initHelpDataUsageController
{
	if (self = [super initWithStyle:UITableViewStylePlain])
	{
		[self setTitle:NSLocalizedString(@"help-data-usage", @"Data Usage")];
		[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
	}
	return self;
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    SimpleCell *cell = (SimpleCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[SimpleCell alloc] initWithCellStyle:TTSimpleCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
		
		GradientBackgroundCell *background = [[GradientBackgroundCell alloc] initWithFrame:CGRectMake(0, 0, 320, 44) cellStyle:OnboardCellStyleNone];
		[cell setBackgroundView:background];
		[background release];
	}
    
	switch (indexPath.row)
	{
		case HELP_DATAUSAGE_PID:
			[cell setTextLabelText:NSLocalizedString(@"help-datausage-pid", @"PID")];
			break;
		case HELP_DATAUSAGE_SCHEDULES:
			[cell setTextLabelText:NSLocalizedString(@"help-datausage-schedules", @"Scheduled Departures")];
			break;
		case HELP_DATAUSAGE_ONBOARD:
			[cell setTextLabelText:NSLocalizedString(@"help-datausage-onboard", @"Onboard")];
			break;
		case HELP_DATAUSAGE_SYNC:
			[cell setTextLabelText:NSLocalizedString(@"help-datausage-sync", @"Data Updates")];
			break;
	}
	
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	// we create a UIWebView that we push onto the stack, start by working out our URL
	NSString *fileName = nil;
	
	switch (indexPath.row)
	{
		case HELP_DATAUSAGE_PID:
			fileName = @"pid.html";
			break;
			
		case HELP_DATAUSAGE_SCHEDULES:
			fileName = @"schedules.html";
			break;
			
		case HELP_DATAUSAGE_ONBOARD:
			fileName = @"onboard.html";
			break;
			
		case HELP_DATAUSAGE_SYNC:
			fileName = @"sync.html";
			break;
	}
	
	// nothing found?
	if (fileName == nil)
		return;
	
	// create the web view
	HelpWebController *helpWeb = [[HelpWebController alloc] initWithHelpFile:[NSString stringWithFormat:@"%@/%@", HelpURLSubdirDataUsage, fileName]];
	SimpleCell *cell = (SimpleCell *)[self.tableView cellForRowAtIndexPath:indexPath];
	[helpWeb setTitle:cell.textLabel.text];
	[self.navigationController pushViewController:helpWeb animated:YES];
	[helpWeb release];	
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


- (void)dealloc {
    [super dealloc];
}


@end

