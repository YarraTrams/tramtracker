//
//  Route.m
//  tramTRACKER
//
//  Created by Robert Amos on 10/02/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "Route.h"

NSString * const RouteColourYellow = @"yellow";
NSString * const RouteColourOrange = @"orange";
NSString * const RouteColourPink = @"pink";
NSString * const RouteColourCyan = @"cyan";
NSString * const RouteColourTeal = @"teal";
NSString * const RouteColourGreen = @"green";
NSString * const RouteColourCityCircle = @"citycircle";
NSString * const RouteColourDisabled = @"disabled";

@implementation Route

@dynamic number, name, upDestination, downDestination, upStops, downStops, subRoute, headboardNumber, internalNumber, turns, colour;

+ (NSArray *)allRoutes
{
	// Grab our managed object context and create a fetch request
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [d managedObjectContext];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	// set the entity that we're looking for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Route" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// fetch it.
	NSError *outError;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&outError];
	
	// cant find any?
	if (fetchedObjects == nil || [fetchedObjects count] == 0)
	{
		[fetchRequest release];
		return nil;
	}
	
	// return them sorted
	[fetchRequest release];
	return fetchedObjects;
}

+ (NSArray *)allPublicRoutes
{
	// Grab our managed object context and create a fetch request
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [d managedObjectContext];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Route" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"subRoute == NO"];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (fetchedObjects == nil || [fetchedObjects count] == 0)
	{
		[fetchRequest release];
		return nil;
	}
	
	// return the object
	[fetchRequest release];
	return fetchedObjects;
}

- (NSString *)destinationForTrackerID:(NSNumber *)trackerID
{
	// is it in the up direction?
	if ([self.upStops indexOfObject:trackerID] != NSNotFound)
		return self.upDestination;
	
	// is it in the down direction?
	if ([self.downStops indexOfObject:trackerID] != NSNotFound)
		return self.downDestination;
	
	return nil;
}

+ (Route *)routeWithNumber:(NSString *)aRouteNumber
{
	// Grab our managed object context and create a fetch request
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [d managedObjectContext];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Route" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"number == %@ AND subRoute == NO", aRouteNumber];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (fetchedObjects == nil || [fetchedObjects count] == 0)
	{
		[fetchRequest release];
		return nil;
	}
	
	// return the object
	[fetchRequest release];
	return [fetchedObjects objectAtIndex:0];
}

+ (Route *)routeWithNumber:(NSString *)aRouteNumber headboardRouteNumber:(NSString *)aHeadboardRouteNumber
{
	// Grab our managed object context and create a fetch request
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [d managedObjectContext];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Route" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"number == %@ AND headboardNumber = %@", aRouteNumber, aHeadboardRouteNumber];
	[fetchRequest setPredicate:predicate];
	
	// sort by whether its a sub-route or not
	NSSortDescriptor *sortBySubRoute = [[NSSortDescriptor alloc] initWithKey:@"subRoute" ascending:YES];
	NSArray *sortDescriptors = [NSArray arrayWithObject:sortBySubRoute];
	[fetchRequest setSortDescriptors:sortDescriptors];
	[sortBySubRoute release];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (fetchedObjects == nil || [fetchedObjects count] == 0)
	{
		[fetchRequest release];
		return nil;
	}
	
	// return the object
	[fetchRequest release];
	return [fetchedObjects objectAtIndex:0];	
}

+ (Route *)routeForStub:(RouteStub *)stub
{
    // do we have a headboard number and a regular route number (could be a subroute)
    if (stub.number != nil && stub.headboardNumber != nil)
        return [Route routeWithNumber:stub.number headboardRouteNumber:stub.headboardNumber];

    // do we have just a route number (usually a main route)
    if (stub.number != nil)
        return [Route routeWithNumber:stub.number];

    // nothing? bummer.
    return nil;
}

/**
 * Get information about a turn from a route
 **/
- (Turn *)turnForStop:(Stop *)stop upDirection:(BOOL)isUpDirection
{
	return [Turn turnForRoute:self upDirection:isUpDirection stop:stop];
}


//
// Accessors for subroute
//
- (void)setSubRoute:(BOOL)subRoute
{
	[self willChangeValueForKey:@"subRoute"];
	[self setPrimitiveValue:[NSNumber numberWithBool:subRoute] forKey:@"subRoute"];
	[self didChangeValueForKey:@"subRoute"];
}
- (BOOL)isSubRoute
{
	[self willAccessValueForKey:@"subRoute"];
	BOOL sub = [[self primitiveValueForKey:@"subRoute"] boolValue];
	[self didAccessValueForKey:@"subRoute"];
	return sub;
}

//
// Set the route name to "UpDestination to DownDestination"
//
- (void)updateName
{
	[self setName:[NSString stringWithFormat:@"%@ to %@", self.upDestination, self.downDestination]];
}

//
// Compare this route against another
//
- (NSComparisonResult)compareRoute:(Route *)otherRoute
{
	// do it using the numeric values of the route number
	NSNumber *routeNumber = [[NSNumber alloc] initWithInt:[self.number intValue]];
	NSNumber *otherRouteNumber = [[NSNumber alloc] initWithInt:[otherRoute.number intValue]];
	
	// are they equal? if so its probably an alphanumeric route number, sort it by its string value
	if ([routeNumber isEqualToNumber:otherRouteNumber])
	{
		[routeNumber release];
		[otherRouteNumber release];
		return [self.number caseInsensitiveCompare:otherRoute.number];
	}
	
	// otherwise compare by the numeric value
	NSComparisonResult result = [routeNumber compare:otherRouteNumber];
	[routeNumber release];
	[otherRouteNumber release];
	
	return result;
}

//
// The description of the route
//
- (NSString *)description
{
	return [NSString stringWithFormat:@"Route %@ - %@", self.internalNumber, self.name];
}

//
// Format a route number including the number
//
- (NSString *)formattedRouteName
{
	return [NSString stringWithFormat:NSLocalizedString(@"route-name", @"Route Name"), self.number, self.name];
}


-(NSString *)formattedDestinationDepartingFromStop:(Stop *)stop upDirection:(BOOL)isUpDirection
{
	NSString *dest = isUpDirection ? self.upDestination : self.downDestination;
	if ([[stop cityDirection] rangeOfString:@"towards City"].location != NSNotFound && [dest rangeOfString:@"City"].location == NSNotFound)
		return [NSString stringWithFormat:NSLocalizedString(@"route-destination-viacity", @"Route x via city"), dest];
	return [NSString stringWithFormat:NSLocalizedString(@"route-destination", @"Route x via city"), dest];
}

// override the colour option
- (NSString *)colour
{
	[self willAccessValueForKey:@"colour"];
	NSString *colour = (NSString *)[self primitiveValueForKey:@"colour"];
	[self didAccessValueForKey:@"colour"];
    
    // if its a valid colour, return it
    if ([colour isEqualToString:RouteColourCityCircle] ||
        [colour isEqualToString:RouteColourCyan] ||
        [colour isEqualToString:RouteColourGreen] ||
        [colour isEqualToString:RouteColourOrange] ||
        [colour isEqualToString:RouteColourPink] ||
        [colour isEqualToString:RouteColourTeal] ||
        [colour isEqualToString:RouteColourYellow])
        return colour;
    
    // otherwise default to yellow
    return RouteColourYellow;
}


- (void)dealloc
{
	[super dealloc];
}

@end


@implementation RoutePart

@synthesize number, isUpPart, destination;

- (void)dealloc
{
	// release the properties
	[number release];
	[destination release];
	
	[super dealloc];
}

@end



@implementation RouteUpdate

@synthesize routeNumber, actionType, internalRouteNumber, headboardRouteNumber, subRoute, numbersHaveBeenCompleted, destinationsHaveBeenCompleted;
@synthesize upStopsHaveBeenCompleted, downStopsHaveBeenCompleted, colour;

- (NSString *)description
{
	NSString *typeDescription = @"unknown";
	if (actionType == PIDServiceActionTypeUpdate)
		typeDescription = @"updated";
	else if (actionType == PIDServiceActionTypeDelete)
		typeDescription = @"deleted";
	
	return [NSString stringWithFormat:@"Route %@ %@ %@", routeNumber, (numbersHaveBeenCompleted && destinationsHaveBeenCompleted && upStopsHaveBeenCompleted && downStopsHaveBeenCompleted ? @"has been" : @"needs to be"), typeDescription];
}

- (void)dealloc
{
	[internalRouteNumber release];
	[routeNumber release];
	[headboardRouteNumber release];
    [colour release];
	[super dealloc];
}

@end

@implementation RouteStub

@synthesize number, headboardNumber;

- (void)dealloc
{
    [number release]; number = nil;
    [headboardNumber release]; headboardNumber = nil;
    [super dealloc];
}

@end