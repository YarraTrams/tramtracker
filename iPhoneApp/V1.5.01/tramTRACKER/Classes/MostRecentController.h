//
//  MostRecentController.h
//  tramTRACKER
//
//  Created by Robert Amos on 10/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Stop.h"
#import "StopList.h"
#import "PIDViewController.h"
#import "MainListController.h"
#import "MostRecentCell.h"

@class MapViewController;

/**
 * @ingroup Lists
**/

/**
 * A custom UITableViewController that displays a list of the most recently opened stops.
**/
@interface MostRecentController : UITableViewController <UINavigationControllerDelegate> {
}

/**
 * Flips over to the Map View
**/
- (void)flipToMapView;

/**
 * Table footer view
**/
- (UIView *)tableFooterView;

@end
