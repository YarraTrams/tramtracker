//
//  TicketRetailerHeaderView.h
//  tramTRACKER
//
//  Created by Robert Amos on 26/07/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TicketRetailer;

@interface TicketRetailerHeaderView : UIView {

	TicketRetailer *retailer;
	UILabel *retailerName;
}

@property (nonatomic, retain) TicketRetailer *retailer;
@property (nonatomic, readonly) UILabel *retailerName;


@end
