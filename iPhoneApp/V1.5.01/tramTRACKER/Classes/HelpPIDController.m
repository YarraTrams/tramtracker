//
//  HelpPIDController.m
//  tramTRACKER
//
//  Created by Robert Amos on 4/05/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "HelpPIDController.h"

const NSString *HelpURLSubdirPID = @"pid";

#define HELP_PID_UNDERSTANDING 0
#define HELP_PID_MESSAGES 1
#define HELP_PID_ACTIONS 2
#define HELP_PID_FILTERING 3
#define HELP_PID_CUSTOMISATION 4

@implementation HelpPIDController

- (id)initHelpPIDController
{
	if (self = [super initWithStyle:UITableViewStylePlain])
	{
		[self setTitle:NSLocalizedString(@"help-pid", @"Help")];
		[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
	}
	return self;
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (UIView *)tableFooterView
{
	UIView *v = [super tableFooterView];
	
	// create a message to go in there
	UILabel *message = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 60)];
	[message setText:NSLocalizedString(@"help-pid-whatis", @"What is a PID?")];
	[message setNumberOfLines:0];
	[message setTextAlignment:UITextAlignmentCenter];
	[message setTextColor:[UIColor blackColor]];
	[message setBackgroundColor:[UIColor clearColor]];
	[message setFont:[UIFont systemFontOfSize:14]];

	// increase the size of the view to hold our message
	CGRect viewRect = v.frame;
	viewRect.size.height += message.frame.size.height;
	[v setFrame:viewRect];
	
	// add the message to the view
	[v addSubview:message];
	[message release];
	return v;
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    SimpleCell *cell = (SimpleCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[SimpleCell alloc] initWithCellStyle:TTSimpleCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
		
		GradientBackgroundCell *background = [[GradientBackgroundCell alloc] initWithFrame:CGRectMake(0, 0, 320, 44) cellStyle:OnboardCellStyleNone];
		[cell setBackgroundView:background];
		[background release];
	}
    
	switch (indexPath.row)
	{
		case HELP_PID_UNDERSTANDING:
			[cell setTextLabelText:NSLocalizedString(@"help-pid-understanding", @"Understanding")];
			break;
		case HELP_PID_ACTIONS:
			[cell setTextLabelText:NSLocalizedString(@"help-pid-actions", @"Actions")];
			break;
		case HELP_PID_FILTERING:
			[cell setTextLabelText:NSLocalizedString(@"help-pid-filtering", @"Filtering")];
			break;
		case HELP_PID_CUSTOMISATION:
			[cell setTextLabelText:NSLocalizedString(@"help-pid-customising", @"Customisation")];
			break;
		case HELP_PID_MESSAGES:
			[cell setTextLabelText:NSLocalizedString(@"help-pid-messages", @"Disruptions and Special Events")];
			break;
	}
	
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	// we create a UIWebView that we push onto the stack, start by working out our URL
	NSString *fileName = nil;
	
	switch (indexPath.row)
	{
		case HELP_PID_UNDERSTANDING:
			fileName = @"understanding13.html";
			break;
		case HELP_PID_ACTIONS:
			fileName = @"menu14.html";
			break;
		case HELP_PID_FILTERING:
			fileName = @"filter.html";
			break;
		case HELP_PID_CUSTOMISATION:
			fileName = @"customising.html";
			break;
		case HELP_PID_MESSAGES:
			fileName = @"messages15.html";
			break;
	}
	
	// nothing found?
	if (fileName == nil)
		return;

	// create the web view
	HelpWebController *helpWeb = [[HelpWebController alloc] initWithHelpFile:[NSString stringWithFormat:@"%@/%@", HelpURLSubdirPID, fileName]];
	SimpleCell *cell = (SimpleCell *)[self.tableView cellForRowAtIndexPath:indexPath];
	[helpWeb setTitle:cell.textLabel.text];
	[self.navigationController pushViewController:helpWeb animated:YES];
	[helpWeb release];
}

- (void)dealloc {
    [super dealloc];
}


@end

