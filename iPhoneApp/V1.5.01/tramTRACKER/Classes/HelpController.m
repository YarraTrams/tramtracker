//
//  HelpController.m
//  tramTRACKER
//
//  Created by Robert Amos on 4/05/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "HelpController.h"
#import "HelpPIDController.h"
#import "HelpFindingStopsController.h"
#import "HelpFavouritesController.h"
#import "HelpOnboardController.h"
#import "HelpSchedulesController.h"
#import "HelpSettingsController.h"
#import "HelpAboutController.h"
#import "HelpMapsController.h"
#import "HelpDataUsageController.h"
#import "FeedbackController.h"
#import "HelpSyncController.h"
#import "HelpTicketOutletsController.h"

#define HELPABOUT 0
#define HELPFEEDBACK 1
#define HELPPID 2
#define HELPFINDINGSTOP 3
#define HELPFAVOURITES 4
#define HELPONBOARD 5
#define HELPMAPS 6
#define HELPSCHEDULEDDEPARTURES 7
#define HELPTICKETS 8
#define HELPSYNC 9
#define HELPSETTINGS 10
#define HELPDATAUSAGE 11

@implementation HelpController

- (id)initHelpController
{
	if (self = [super initWithStyle:UITableViewStylePlain])
	{
		[self setTitle:NSLocalizedString(@"title-help", @"Help")];
		[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
	}
	return self;
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 12;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    SimpleCell *cell = (SimpleCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[SimpleCell alloc] initWithCellStyle:TTSimpleCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];

		GradientBackgroundCell *background = [[GradientBackgroundCell alloc] initWithFrame:CGRectMake(0, 0, 320, 44) cellStyle:OnboardCellStyleNone];
		[cell setBackgroundView:background];
		[background release];
	}
    
    // Set up the cell...
	switch (indexPath.row)
	{
		case HELPABOUT:
			[cell setTextLabelText:NSLocalizedString(@"help-about", @"About")];
			break;
		case HELPFEEDBACK:
			[cell setTextLabelText:NSLocalizedString(@"title-feedback", @"Feedback")];
			break;
		case HELPPID:
			[cell setTextLabelText:NSLocalizedString(@"help-pid", @"PID")];
			break;
		case HELPFINDINGSTOP:
			[cell setTextLabelText:NSLocalizedString(@"help-finding-stop", @"Finding Stops")];
			break;
		case HELPONBOARD:
			[cell setTextLabelText:NSLocalizedString(@"help-onboard", @"Onboard")];
			break;
		case HELPSCHEDULEDDEPARTURES:
			[cell setTextLabelText:NSLocalizedString(@"help-scheduled-departures", @"Scheduled Departures")];
			break;
		case HELPTICKETS:
			[cell setTextLabelText:NSLocalizedString(@"help-tickets", @"Ticket Outlets")];
			break;
		case HELPSETTINGS:
			[cell setTextLabelText:NSLocalizedString(@"help-settings", @"Settings")];
			break;
		case HELPDATAUSAGE:
			[cell setTextLabelText:NSLocalizedString(@"help-data-usage", @"Data Usage")];
			break;
		case HELPMAPS:
			[cell setTextLabelText:NSLocalizedString(@"help-maps", @"Maps")];
			break;
		case HELPFAVOURITES:
			[cell setTextLabelText:NSLocalizedString(@"help-favourites", @"Favourites")];
			break;
		case HELPSYNC:
			[cell setTextLabelText:NSLocalizedString(@"help-sync", @"Sync")];
			break;
	}
	
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	switch (indexPath.row)
	{
		case HELPABOUT:
		{
			HelpAboutController *helpAbout = [[HelpAboutController alloc] initHelpAboutController];
			[self.navigationController pushViewController:helpAbout animated:YES];
			[helpAbout release];
			break;
		}
			
		case HELPFEEDBACK:
		{
			FeedbackController *feedback = [[FeedbackController alloc] initWithNibName:@"FeedbackController" bundle:[NSBundle mainBundle]];
			[feedback setTitle:NSLocalizedString(@"title-feedback", @"Feedback")];
			[self.navigationController pushViewController:feedback animated:YES];
			[feedback release];
			break;
		}
			
		case HELPPID:
		{
			HelpPIDController *helpPID = [[HelpPIDController alloc] initHelpPIDController];
			[self.navigationController pushViewController:helpPID animated:YES];
			[helpPID release];
			break;
		}

		case HELPFINDINGSTOP:
		{
			HelpFindingStopsController *helpFindingStops = [[HelpFindingStopsController alloc] initHelpFindingStopsController];
			[self.navigationController pushViewController:helpFindingStops animated:YES];
			[helpFindingStops release];
			break;
		}
			
		case HELPFAVOURITES:
		{
			HelpFavouritesController *helpFavourites = [[HelpFavouritesController alloc] initHelpFavouritesController];
			[self.navigationController pushViewController:helpFavourites animated:YES];
			[helpFavourites release];
			break;
		}
			
		case HELPONBOARD:
		{
			HelpOnboardController *helpOnboard = [[HelpOnboardController alloc] initHelpOnboardController];
			[self.navigationController pushViewController:helpOnboard animated:YES];
			[helpOnboard release];
			break;
		}
		case HELPSCHEDULEDDEPARTURES:
		{
			HelpSchedulesController *helpSchedules = [[HelpSchedulesController alloc] initHelpSchedulesController];
			[self.navigationController pushViewController:helpSchedules animated:YES];
			[helpSchedules release];
			break;	
		}
		case HELPTICKETS:
		{
			HelpTicketOutletsController *helpTickets = [[HelpTicketOutletsController alloc] initHelpTicketOutletsController];
			[self.navigationController pushViewController:helpTickets animated:YES];
			[helpTickets release];
			break;	
		}
		case HELPSETTINGS:
		{
			HelpSettingsController *helpSettings = [[HelpSettingsController alloc] initHelpSettingsController];
			[self.navigationController pushViewController:helpSettings animated:YES];
			[helpSettings release];
			break;
		}
			
		case HELPSYNC:
		{
			HelpSyncController *helpSync = [[HelpSyncController alloc] initHelpSyncController];
			[self.navigationController pushViewController:helpSync animated:YES];
			[helpSync release];
			break;
		}

		case HELPMAPS:
		{
			HelpMapsController *helpMaps = [[HelpMapsController alloc] initHelpMapsController];
			[self.navigationController pushViewController:helpMaps animated:YES];
			[helpMaps release];
			break;
		}
			
		case HELPDATAUSAGE:
		{
			HelpDataUsageController *helpDataUsage = [[HelpDataUsageController alloc] initHelpDataUsageController];
			[self.navigationController pushViewController:helpDataUsage animated:YES];
			[helpDataUsage release];
			break;
		}
	}
}


- (void)dealloc {
    [super dealloc];
}


@end

