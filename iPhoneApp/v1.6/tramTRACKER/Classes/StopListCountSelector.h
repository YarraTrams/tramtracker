//
//  StopListCountSelector.h
//  tramTRACKER
//
//  Created by Robert Amos on 27/04/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SimpleCell.h"


@interface StopListCountSelector : UITableViewController {
	id target;
	SEL action;
	NSInteger stopCount;
	NSArray *stopCountList;
}

- (id)initWithStopCount:(NSInteger)aStopCount target:(id)aTarget action:(SEL)anAction;

@end
