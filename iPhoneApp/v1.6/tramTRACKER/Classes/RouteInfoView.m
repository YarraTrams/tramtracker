//
//  RouteInfoView.m
//  tramTRACKER
//
//  Created by Robert Amos on 21/04/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "RouteInfoView.h"


@implementation RouteInfoView

@synthesize routeNumber, destination, message, image, badConnection;

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
		
		// add an image view
		tramIconView = [[TramImage alloc] initWithImage:[UIImage imageNamed:@"tramicon.png"] target:self action:@selector(playGong)];
		[tramIconView setFrame:CGRectMake(10, 10, tramIconView.frame.size.width, tramIconView.frame.size.height)];
		[self addSubview:tramIconView];
		isPlaying = NO;
		
		// add our route number label
		routeNumberLabel = [self newRouteNumberLabel];
		[self addSubview:routeNumberLabel];
		
		// and the destination label
		destinationLabel = [self newDestinationLabel];
		[self addSubview:destinationLabel];
		
		// and the bad connection indicator
		badConnectionImage = [self newBadConnectionImage];
		[self addSubview:badConnectionImage];
    }
    return self;
}

// when you set the route number, update the label
- (void)setRouteNumber:(NSString *)newRouteNumber
{
	[newRouteNumber retain];
	[routeNumber release];
	routeNumber = newRouteNumber;
	
	[routeNumberLabel setText:[NSString stringWithFormat:NSLocalizedString(@"onboard-routeinfo-routenumber", @"Route Number"), routeNumber]];
}
- (void)setRouteNumberName:(NSString *)newRouteNumberName
{
	// sometimes the 'route number' has a name. Well - only the City Circle route.
	[newRouteNumberName retain];
	[routeNumber release];
	routeNumber = newRouteNumberName;
	
	[routeNumberLabel setText:newRouteNumberName];
}

// when you set the destination, update the label
- (void)setDestination:(NSString *)newDestination
{
	[destination release];
	destination = [[newDestination stringByReplacingOccurrencesOfString:@"  " withString:@", "] retain];
	
	[destinationLabel setText:[NSString stringWithFormat:NSLocalizedString(@"onboard-routeinfo-destination", @"Destination"), destination]];
}
- (void)setDirection:(NSString *)newDirection
{
	// for the city circle the destination actually described the direction the tram travels, so the "to" prefix is not appropriate
	[newDirection retain];
	[destination release];
	destination = newDirection;
	
	[destinationLabel setText:newDirection];
}

// When you set the picture, update it
- (void)setImage:(UIImage *)newImage
{
	// xmas fun
	if ([self isChristmas])
		newImage = [UIImage imageNamed:@"Xmas.png"];

	[newImage retain];
	[image release];
	image = newImage;
	
	// update the picture
	[tramIconView setImage:image];
}

// set the message
- (void)setMessage:(NSString *)newMessage
{
	[newMessage retain];
	[message release];
	message = newMessage;

	// does the message label exist?
	if (messageLabel == nil)
	{
		messageLabel = [self newMessageLabel];
		[messageLabel retain];
		[self addSubview:messageLabel];
	}
	
	[messageLabel setText:message];
}


// set the gong sound
- (void)setURLOfGongSound:(NSURL *)aURL
{
	// does it exist?
	if (aURL == nil)
		return;
	
	// set the audio file object
	AudioServicesCreateSystemSoundID((CFURLRef)aURL, &soundID);
}

- (void)setBadConnection:(BOOL)newBadConnection
{
	// dont do stuff if its the same
	if (newBadConnection == badConnection)
		return;

	badConnection = newBadConnection;

	// show or hide the image based on the results
	[badConnectionImage setHidden:!badConnection];

	// resize the labels as appropriate
	CGRect routeRect = routeNumberLabel.frame;
	CGRect destinationRect = destinationLabel.frame;
	
	if (badConnection)
	{
		routeRect.size.width -= badConnectionImage.frame.size.width;
		destinationRect.size.width -= badConnectionImage.frame.size.width;
	} else
	{
		routeRect.size.width += badConnectionImage.frame.size.width;
		destinationRect.size.width += badConnectionImage.frame.size.width;
	}
	[routeNumberLabel setFrame:routeRect];
	[destinationLabel setFrame:destinationRect];
}

- (void)drawRect:(CGRect)rect {
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	// colour components and locations
	CGFloat locations[3] = { 0.0, 0.8, 1.0 };
	CGFloat components[12] = 
	{
		0.0, 0.0, 0.0, 0.0,
		0.0, 0.0, 0.0, 0.4,
		0.0, 0.0, 0.0, 0.6
	};
	
	// make our gradient
	CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB();
	CGGradientRef gradient = CGGradientCreateWithColorComponents(space,	components, locations, 3);
	
	// draw our gradient in our view
	CGContextSaveGState(context);
	//	CGContextClipToRect(context, CGRectMake(0, 0, rect.size.width, 10));
	CGContextDrawLinearGradient(context, gradient, CGPointMake(0, self.frame.size.height-10), CGPointMake(0, self.frame.size.height), 0);
	CGGradientRelease(gradient);
	CGColorSpaceRelease(space);
}

- (UILabel *)newRouteNumberLabel
{
	// position it
	CGRect frame = CGRectMake(66, 10, 240, 30);
	UILabel *newLabel = [[UILabel alloc] initWithFrame:frame];
	
	// make some happy happy changes
	[newLabel setBackgroundColor:[UIColor clearColor]];					// background colour
	[newLabel setTextColor:[UIColor blackColor]];						// text colour
	[newLabel setOpaque:NO];											// no need for transparency here
	[newLabel setFont:[UIFont boldSystemFontOfSize:18]];				// and a 16pt font
	[newLabel setTextAlignment:UITextAlignmentLeft];					// right align the text (so it sits against the right side)
	[newLabel setLineBreakMode:UILineBreakModeTailTruncation];
	
	return newLabel;
}

- (UILabel *)newDestinationLabel
{
	// position it
	CGRect frame = CGRectMake(66, 35, 240, 20);
	UILabel *newLabel = [[UILabel alloc] initWithFrame:frame];
	
	// make some happy happy changes
	[newLabel setBackgroundColor:[UIColor clearColor]];					// background colour
	[newLabel setTextColor:[UIColor blackColor]];						// text colour
	[newLabel setOpaque:NO];											// no need for transparency here
	[newLabel setFont:[UIFont boldSystemFontOfSize:14]];				// and a 16pt font
	[newLabel setTextAlignment:UITextAlignmentLeft];					// right align the text (so it sits against the right side)
	[newLabel setLineBreakMode:UILineBreakModeTailTruncation];
	
	return newLabel;
}

- (UILabel *)newMessageLabel
{
	// position it
	CGRect frame = CGRectMake(10, 61, 300, 20);
	UILabel *newLabel = [[UILabel alloc] initWithFrame:frame];
	
	// make some happy happy changes
	[newLabel setBackgroundColor:[UIColor clearColor]];					// background colour
	[newLabel setTextColor:[UIColor blackColor]];						// text colour
	[newLabel setOpaque:NO];											// no need for transparency here
	[newLabel setFont:[UIFont systemFontOfSize:13]];					// and a 16pt font
	[newLabel setTextAlignment:UITextAlignmentCenter];					// right align the text (so it sits against the right side)
	[newLabel setLineBreakMode:UILineBreakModeTailTruncation];
	[newLabel setAdjustsFontSizeToFitWidth:YES];
	//[newLabel setMinimumFontSize:10];
	
	return newLabel;
}

- (UIImageView *)newBadConnectionImage
{
	UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"badconnection.png"]];
	CGRect frame = CGRectMake(self.frame.size.width-10-imageView.frame.size.width, 15, imageView.frame.size.width, imageView.frame.size.height);
	[imageView setFrame:frame];
	[imageView setHidden:YES];
	return imageView;
}

- (BOOL)isChristmas
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"MM-dd"];
	NSString *formattedDate = [formatter stringFromDate:[NSDate date]];
	if ([formattedDate isEqualToString:@"12-25"] || [formattedDate isEqualToString:@"12-24"])
	{
		[formatter release];
		return YES;
	}
	[formatter release];
	return NO;
}

- (void)playGong
{
	AudioServicesPlaySystemSound(soundID);
}

- (void)dealloc {
	
	[routeNumber release];
	[routeNumberLabel release];
	[destination release];
	[destinationLabel release];
	[message release];
	[messageLabel release];

    [super dealloc];
}


@end
