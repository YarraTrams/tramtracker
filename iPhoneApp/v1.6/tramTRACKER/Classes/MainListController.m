//
//  MainListController.m
//  tramTRACKER
//
//  Created by Robert Amos on 26/02/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "MainListController.h"
#import "OnboardListCell.h"
#import "FavouriteGroupsController.h"
#import "FavouriteStop.h"


@implementation MainListController

//
// When the view loads we need to set stuff like the title.
//
- (void)viewDidLoad {
    [super viewDidLoad];

	[self setTitle:NSLocalizedString(@"title-favourites", @"Favourites")];
	
	if (favouriteStops == nil)
		[self reloadFavourites];
	
	// Set the edit and map buttons
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
	self.navigationItem.rightBarButtonItem = self.rightButtonItem;
	
	// and the delegate for the table view to ourselves
	[self.tableView setDelegate:self];

	// Reset our background colour
	[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
    

	
	// Register so that when the favourites update we can reload the table (when you add a favourite)

	[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshFavourites:)
                                                 name:TTFavouritesUpdated
                                               object:nil];
    
    // register for route list updates so the routesThroughStop property will be updated
	[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshFavourites:)
                                                 name:TTStopsAndRoutesHaveBeenUpdatedNotice
                                               object:nil];

	// set the table footer
	[self.tableView setTableFooterView:[self tableFooterView]];
}

- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];

	// release the pidsservice because we're about to die
	
}


//
// Create the table footer
//
- (UIView *)tableFooterView
{
	CGRect frame = CGRectMake(0, 0, self.tableView.frame.size.width, 100);
	UnderBackgroundView *ub = [[UnderBackgroundView alloc] initWithFrame:frame];

	// are we editing?
	if (self.tableView.editing)
	{
		UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 20, 280, 44)];
		[label setText:NSLocalizedString(@"favourites-editing-message", nil)];
		[label setTextColor:[UIColor darkGrayColor]];
		[label setFont:[UIFont systemFontOfSize:14]];
		[label setTextAlignment:UITextAlignmentCenter];
		[label setNumberOfLines:0];
		[label setOpaque:NO];
		[label setBackgroundColor:[UIColor clearColor]];
		[ub addSubview:label];
		[label release];

	// no favourites? do nothing
	} else if ([[StopList sharedManager] hasFavouriteStopList])
	{
		UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 20, 280, 44)];
		[label setText:NSLocalizedString(@"favourites-nearest-message", @"Nearest Favourite")];
		[label setTextColor:[UIColor darkGrayColor]];
		[label setFont:[UIFont systemFontOfSize:14]];
		[label setTextAlignment:UITextAlignmentCenter];
		[label setNumberOfLines:0];
		[label setOpaque:NO];
		[label setBackgroundColor:[UIColor clearColor]];
		[ub addSubview:label];
		[label release];
	} else
	{
		UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 20, 280, 44)];
		[label setText:NSLocalizedString(@"favourites-none", @"No favourites")];
		[label setTextColor:[UIColor darkGrayColor]];
		[label setFont:[UIFont systemFontOfSize:14]];
		[label setTextAlignment:UITextAlignmentCenter];
		[label setNumberOfLines:0];
		[label setOpaque:NO];
		[label setBackgroundColor:[UIColor clearColor]];
		[ub addSubview:label];
		[label release];
	}

	return [ub autorelease];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}

- (void)toggleEditing
{
	[self.tableView setEditing:!self.tableView.editing animated:YES];
	[self.navigationItem setLeftBarButtonItem:[self editButtonItem] animated:YES];
	[self.navigationItem setRightBarButtonItem:[self rightButtonItem] animated:YES];
	[self.tableView setTableFooterView:[self tableFooterView]];

	// if we only have a single section and that section is named the default then resize it
	if ([self.tableView numberOfSections] == 1)
	{
		NSString *sectionName = [sections objectAtIndex:0];
		if ([sectionName isEqualToString:NSLocalizedString(@"favourites-default-section", nil)])
		{
			[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
		}
	}
}

- (UIBarButtonItem *)editButtonItem
{
	// no favourite stops?
	if (![[StopList sharedManager] hasFavouriteStopList] &&
		(
		 (sections == nil || [sections count] == 1) ||
		 ([sections count] == 1 && [[sections objectAtIndex:0] isEqualToString:NSLocalizedString(@"favourites-default-section", nil)])
		))
		return nil;

	UIBarButtonItem *button = nil;
	
	if (!self.tableView.editing)
	{
		button = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(toggleEditing)];
	} else
	{
		button = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(toggleEditing)];
	}
	return [button autorelease];
}

- (UIBarButtonItem *)rightButtonItem
{
	UIBarButtonItem *button = nil;
	
	// return the Edit Groups button
	if (self.tableView.editing)
	{
		button = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"favourites-groups-button", nil)
												  style:UIBarButtonItemStylePlain
												 target:self
												 action:@selector(presentGroupEditingDialog)];
		
	// return the map button
	} else if ([[StopList sharedManager] hasFavouriteStopList])
	{
		button = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"button-map", @"Map")
												  style:UIBarButtonItemStylePlain
												 target:self
												 action:@selector(flipToMapView)];
	}
	
	return [button autorelease];
}


#pragma mark Table view methods


//
// Tell the Table View the number of sections to show
//
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return [sections count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	if ([self tableView:tableView titleForHeaderInSection:section] == nil)
		return 0;
    return 22;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	// get the title
	NSString *sectionName = [self tableView:tableView titleForHeaderInSection:section];
	if (sectionName == nil)
		return nil;

	// create a view
	OnboardListSectionHeaderView *view = [[OnboardListSectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 22)];
	[view.title setText:sectionName];
	return [view autorelease];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	// no title for single section with the "Ungrouped" name
	if (sections == nil || [sections count] == 0)
		return nil;
	
	// return the section name
	NSString *sectionName = [sections objectAtIndex:section];

	// only return 0 if the table is not editing and we have the default section
	if (!tableView.editing && [sectionName isEqualToString:NSLocalizedString(@"favourites-default-section", @"Ungrouped")])
		return nil;
	
	return sectionName;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	NSInteger count = 0;
	for (FavouriteStop *favourite in favouriteStops)
	{
		if (favourite.indexPath.section == section)
			count++;
	}
	
	return count;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
	static NSString *StopCellIdentifier = @"Favourite Cell";
	
	// is this our filler row?
	FavouriteStop *favourite = [self favouriteAtIndexPath:indexPath];
	
	// Otherwise for < 2 we use a custom cell
	FavouriteCell *cell = (FavouriteCell *)[tableView dequeueReusableCellWithIdentifier:StopCellIdentifier];
	if (cell == nil)
	{
		// create a new cell using the stop views
        cell = [[[FavouriteCell alloc] initWithStyle:UITableViewCellStyleDefault
                                     reuseIdentifier:StopCellIdentifier] autorelease];
		[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
	}

	// set the stop name
	[cell.name setText:[favourite.stop formattedName]];
	
	// and the route description
	[cell.routeDescription setText:[favourite.stop formattedRouteDescriptionForFavouriteAtIndexPath:indexPath]];
	
	// and the low floor filter (if its there)
	[cell setLowFloorOnly:(favourite.filter != nil && favourite.filter.lowFloor)];

	return cell;

}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

	// clicking on a favourite, spawn a pid at the stop
	FavouriteStop *favourite = [self favouriteAtIndexPath:indexPath];
	[self pushPIDWithFavourite:[favouriteStops indexOfObject:favourite]];
}

//
// This is called when the user hits the edit button and allows us to make the fixed menu non-editable
//
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	return YES;
}

//
// When the table view goes into edit mode, it calls this to find out which rows can be re-arranged
//
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
	return YES;
}

//
// And when the row has been moved
//
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
	// move the stop in the favourites list
	[[StopList sharedManager] moveFavouriteAtIndexPath:fromIndexPath toIndexPath:toIndexPath];

	NSArray *newFavourites = [[StopList sharedManager] getFavouriteStopList];
	[newFavourites retain];
	[favouriteStops release];
	favouriteStops = newFavourites;
}

//
// Finished editing a row (most likely it got deleted)
//
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
	// if the row is deleted, remove it
	if (editingStyle == UITableViewCellEditingStyleDelete)
	{
		// remove the favourite
		[[StopList sharedManager] removeFavouriteAtIndexPath:indexPath];
		
		NSArray *newFavourites = [[StopList sharedManager] getFavouriteStopList];
		[newFavourites retain];
		[favouriteStops release];
		favouriteStops = newFavourites;

		// now remove it from the table
		[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationTop];

		// hide the button if we no longer have any favourite stops
		/*if (![[StopList sharedManager] hasFavouriteStopList])
		{
			//[self.navigationItem setRightBarButtonItem:nil animated:YES];
			[self.tableView setTableFooterView:[self tableFooterView]];
		}*/
	}
}

- (FavouriteStop *)favouriteAtIndexPath:(NSIndexPath *)indexPath
{
	if (favouriteStops == nil)
		[self reloadFavourites];
	for (FavouriteStop *stop in favouriteStops)
	{
		if ([stop.indexPath compare:indexPath] == NSOrderedSame)
			return stop;
	}
	return nil;
}

- (NSInteger)indexAtIndexPath:(NSIndexPath *)indexPath
{
	FavouriteStop *stop = [self favouriteAtIndexPath:indexPath];

	if (stop == nil) return -1;
	
	return [favouriteStops indexOfObject:stop];
}

#pragma mark -
#pragma mark Group Editing

- (void)presentGroupEditingDialog
{
	// create a favourites group editor inside a navigation controller
	FavouriteGroupsController *groupsController = [[FavouriteGroupsController alloc] initWithGroups:sections];
	UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:groupsController];
	[nav.navigationBar setTintColor:self.navigationController.navigationBar.tintColor];
	[groupsController release];
    [self.navigationController presentViewController:nav animated:YES completion:NULL];
	[nav release];
}


#pragma mark -
#pragma mark Load a PID

//
// Support for loading a PID with a specific stop
//
- (void)pushPIDWithFavourite:(NSInteger)favouriteIndex
{
	[self pushPIDWithFavourite:favouriteIndex animated:YES];
}
- (void)pushPIDWithFavourite:(NSInteger)favouriteIndex animated:(BOOL)animated
{
	// create a paged PID and pop it onto the stack
	PagedPIDViewController *pagedPID = [[PagedPIDViewController alloc] initWithFavourites:[[StopList sharedManager] getFavouriteStopList] withStartingIndex:favouriteIndex];
	[self.navigationController pushViewController:pagedPID animated:animated];
	[pagedPID release];
}


//
// Push a PID with the nearest favourites on there
//
- (void)pushPIDWithNearbyFavouritesAnimated:(BOOL)animated
{
	// create a paged PID and pop it onto the stack
	PagedPIDViewController *pagedPID = [[PagedPIDViewController alloc] initNearestWithFavourites:[[StopList sharedManager] getFavouriteStopList]];
	[self.navigationController pushViewController:pagedPID animated:animated];
	[pagedPID release];
}
- (void)pushPIDWithNearbyFavouritesAnimated
{
	[self pushPIDWithNearbyFavouritesAnimated:YES];
}

// Flip over to show a map view
- (void)flipToMapView
{
	// build a stop list based on ourselves
	NSMutableArray *stopListForMaps = [[NSMutableArray alloc] initWithCapacity:0];
	StopList *s = [StopList sharedManager];
	for (FavouriteStop *favourite in [s getFavouriteStopList])
		[stopListForMaps addObject:favourite.stop];
	
	MapViewController *mapView = [[MapViewController alloc] initWithStopList:stopListForMaps];
	[stopListForMaps release];

	[mapView setListViewType:TTMapViewListTypeFavouriteList];
	[mapView setTitle:self.title];
	
	// flip over to the map view
	[UIView beginAnimations:@"FlipToMapView" context:nil];
	[[self.navigationController.view.subviews objectAtIndex:0] setBackgroundColor:[UIColor blackColor]];
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:[self.navigationController.view.subviews objectAtIndex:0] cache:YES];
	[UIView setAnimationDuration:1];
	
	UINavigationController *nav = self.navigationController;
	
	// reset the view controllers array
	if ([[nav.viewControllers objectAtIndex:0] isEqual:[self.tabBarController.moreNavigationController.viewControllers objectAtIndex:0]])
		[nav setViewControllers:[NSArray arrayWithObjects:[nav.viewControllers objectAtIndex:0], mapView, nil]];
	else
		[nav setViewControllers:[NSArray arrayWithObject:mapView] animated:NO];
	
	[UIView commitAnimations];
	
	[mapView release];
}

- (void)reloadFavourites
{
	// refresh the favourite stops
	NSArray *newFavouriteStops = [[StopList sharedManager] getFavouriteStopListFromCoreData];
	[newFavouriteStops retain];
	[favouriteStops	release];
	favouriteStops = newFavouriteStops;
	
	// refresh the sections
	NSArray *newSections = [[StopList sharedManager] sectionNamesForFavourites];
	[newSections retain];
	[sections release];
	sections = newSections;
}

//
// Refresh the table when something is added to the favourites list
//
- (void)refreshFavourites:(NSNotification *)note
{
	[self reloadFavourites];

	[self.navigationItem setLeftBarButtonItem:[self editButtonItem] animated:YES];
	[self.navigationItem setRightBarButtonItem:[self rightButtonItem] animated:YES];

	// refresh the table
	[self.tableView reloadData];
}


- (void)dealloc {
	[stops release];
	[sections release];
    [super dealloc];
}


@end

