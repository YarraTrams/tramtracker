//
//  DepartureListController.h
//  tramTRACKER
//
//  Created by Robert Amos on 6/04/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SimpleCell.h"
#import "GradientBackgroundCell.h"
#import "OnboardListCell.h"
#import "Stop.h"
#import "Prediction.h"
#import "DepartureCell.h"
#import "PidsService.h"
#import "DepartureTimePicker.h"
#import "RouteList.h"
#import "RouteSelectionController.h"
#import "SimpleCell.h"

@class Route;

@interface DepartureListHeaderController : UIViewController
{
	UIButton *routeFilterButton;
	UIButton *departureTimeButton;
}

@property (nonatomic, retain) IBOutlet UIButton *routeFilterButton;
@property (nonatomic, retain) IBOutlet UIButton *departureTimeButton;

@end

@interface DepartureListController : UITableViewController {
	BOOL realtime;
	BOOL relativeTime;
	Stop *currentStop;
	NSTimer *timerRefresh;
	NSArray *currentPredictions;
	PidsService *service;
	NSDate *scheduledDepartureTime;
	DepartureListHeaderController *tableHeader;
	NSString *selectedRoute;
	Route *tripRoute;
	NSError *currentError;
}

@property (nonatomic) BOOL realtime;
@property (nonatomic, retain) Stop *currentStop;
@property (nonatomic, retain) NSTimer *timerRefresh;
@property (nonatomic, retain) NSArray *currentPredictions;
@property (nonatomic, retain) PidsService *service;
@property (nonatomic) BOOL relativeTime;
@property (nonatomic, retain) NSDate *scheduledDepartureTime;
@property (nonatomic, retain) DepartureListHeaderController *tableHeader;
@property (nonatomic, retain) NSString *selectedRoute;
@property (nonatomic, retain) NSError *currentError;
@property (nonatomic, retain) Route *tripRoute;

//initialisers
- (id)initWithListForStop:(Stop *)stop;						// Generic
- (id)initWithRealtimeListForStop:(Stop *)stop;				// Realtime, refreshes, no departure time
- (id)initWithScheduledListForStop:(Stop *)stop;			// Scheduled, no refresh, changable departure time

// custom table footer
- (UIView *)tableFooterView;
- (UIView *)tableHeaderView;

- (void)setPredictions:(NSArray *)predictions;
- (void)toggleRelativeTime;
- (UIBarButtonItem *)absoluteTimeButton;
- (UIBarButtonItem *)relativeTimeButton;
- (NSString *)absoluteArrivalTime:(NSDate *)arrivalTime;
- (NSString *)formattedScheduledDepartureTime;
- (void)setScheduledDepartureTimeFromPicker:(NSDate *)date;
- (void)setSelectedRouteFromPicker:(NSString *)routeIndex;
- (void)pushTimePicker;
- (void)pushRoutePicker;
- (void)setRouteFilterButtonTitle;
- (void)refreshScheduledDepartures;
- (NSString *)specialEventMessageForPrediction:(Prediction *)prediction;
- (void)pidsServiceDidFailWithError:(NSError *)error;

@end


