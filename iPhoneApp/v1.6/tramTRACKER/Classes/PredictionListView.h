//
//  PredictionListView.h
//  tramTRACKER
//
//  Created by Robert Amos on 11/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Stop.h"
#import "StopList.h"
#import "SimpleCell.h"
#import "ShowMessagesSetting.h"
#import "DepartureListController.h"
#import "Route.h"
#import "RouteSelectionController.h"
#import "RouteList.h"

@class PIDViewController;
@class PagedPIDViewController;
@class Filter;

/**
 * @ingroup PIDS
**/

/**
 * The PredictionListView is the back side of a PID and controls customisation/filtering of what the PID is showing.
**/
@interface PredictionListView : UITableViewController <UITextFieldDelegate> {
	
	/**
	 * The Stop that the PID View we're customising is showing.
	**/
	Stop *currentStop;
	
	/**
	 * Whether the PID is currently filtering to only showing low floor trams or specific routes
	**/
	Filter *filter;
	
	/**
	 * The PID View Controller that called us (and will be handed the customisation results back)
	**/
	PIDViewController *delegate;
	
	/**
	 * The index path of the cell that we're editing
	**/
	NSIndexPath *cellBeingEdited;
	
	/**
	 * The type of show messages argument
	**/
	NSInteger showMessages;

}

@property (nonatomic, retain) Stop *currentStop;
@property (nonatomic, retain) Filter *filter;
@property (nonatomic, retain) NSIndexPath *cellBeingEdited;
@property (nonatomic) NSInteger showMessages;

/**
 * Custom initialiser
 *
 * @param	style		The UITableViewStyle of this custom UITableView
 * @param	stop		The Stop that is currently being displayed on the PID
 * @param	withOnlyLowFloorTrams	Whether the PID is being filtered to only show low floor trams.
 * @param	fromPagedPID	Whether we were spawned from a Paged PID.
**/
- (id)initWithStyle:(UITableViewStyle)style stop:(Stop *)stop filter:(Filter *)aFilter showMessagesType:(NSInteger)messages delegate:(PIDViewController *)theDelegate;

/**
 * Toggle the filtering by low floor trams
**/
- (void)toggleLowFloorTramFilter:(id)sender;

/**
 * Flip back to the PID View
 **/
- (void)flipToPIDView;

/**
 * Edit the direction text for this stop
**/
- (void)editTextAtIndexPath:(NSIndexPath *)indexPath;

/**
 * Push to change the show messages type
**/
- (void)pushChangeShowMessages;

/**
 * Push to view a scheduled departure list
**/
- (void)pushScheduledList;

/**
 * Push the route selection screen
**/
- (void)pushRoutePicker;

/**
 * Sets the selected route from the picker
**/
- (void)setSelectedRouteFromPicker:(NSString *)routeIndex;

@end
