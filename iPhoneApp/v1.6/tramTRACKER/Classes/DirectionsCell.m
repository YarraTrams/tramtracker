//
//  DirectionsCell.m
//  tramTRACKER
//
//  Created by Robert Amos on 26/07/10.
//  Copyright 2010 Metro Trains Melbourne. All rights reserved.
//

#import "DirectionsCell.h"


@implementation DirectionsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

// override the drawing code for Layout Subviews
- (void)layoutSubviews
{
	// let it lay them out as normal
	[super layoutSubviews];

	// hide the detail label
	[self.detailTextLabel setHidden:YES];
	
	// expand the text label to full cell and centre align it
	CGRect frame = self.textLabel.frame;
	frame.size.width = 300;
	frame.origin.x = 0;
	[self.textLabel setFrame:frame];
	[self.textLabel setTextAlignment:UITextAlignmentCenter];
	[self.textLabel setBackgroundColor:[UIColor clearColor]];
}


- (void)dealloc {
    [super dealloc];
}


@end
