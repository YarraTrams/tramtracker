//
//  ServiceChanges.m
//  tramTRACKER
//
//  Created by Robert Amos on 20/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "ServiceChangesController.h"
#import "UnderBackgroundView.h"
#import "SimpleCell.h"
#import "ServiceChange.h"
#import "GradientBackgroundCell.h"
#import "ServiceChangeViewController.h"

@implementation ServiceChangesController

@synthesize service, serviceChanges;

- (id)initWithStyle:(UITableViewStyle)style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    if (self = [super initWithStyle:style]) {
		
		[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
		[self.tableView setTableFooterView:[self tableFooterView]];
    }
    return self;
}

- (UIView *)tableFooterView
{
	CGRect frame = CGRectMake(0, 0, self.tableView.frame.size.width, 120);
	UnderBackgroundView *container = [[UnderBackgroundView alloc] initWithFrame:frame];

	// add the twirly thing
	loadingIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	CGRect loadingFrame = loadingIndicator.frame;
	loadingFrame.origin.y = (container.frame.size.height / 2) - (loadingFrame.size.height / 2);
	loadingFrame.origin.x = (container.frame.size.width / 2) - (loadingFrame.size.width / 2) - 50;
	[loadingIndicator setFrame:loadingFrame];
	[loadingIndicator startAnimating];
	[container addSubview:loadingIndicator];
	
	// add the loading text
	loadingText = [[UILabel alloc] initWithFrame:CGRectMake(130, loadingFrame.origin.y+1, 100, 20)];
	[loadingText setText:NSLocalizedString(@"service-changes-loading", @"Loading...")];
	[loadingText setBackgroundColor:[UIColor clearColor]];
	[loadingText setOpaque:NO];
	[loadingText setTextColor:[UIColor grayColor]];
	[loadingText setFont:[UIFont boldSystemFontOfSize:14]];
	[container addSubview:loadingText];
	
	return [container autorelease];
}

/*
- (void)viewDidLoad {
    [super viewDidLoad];

    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
*/


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

	// fire off the request to the service if we don't already have a cached set
	if (self.serviceChanges == nil)
	{
		ServiceChangesService *s = [[ServiceChangesService alloc] initWithDelegate:self action:@selector(setServiceChanges:)];
		[self setService:s];
		[s release];
		
		[self.service getServiceChangesInBackgroundThread];
	}
}

- (void)setServiceChanges:(NSArray *)newServiceChanges
{
	[newServiceChanges retain];
	[serviceChanges release];
	serviceChanges = newServiceChanges;

	// hide the loading stuff
	[loadingText setHidden:YES];
	[loadingIndicator stopAnimating];
	[loadingIndicator setHidden:YES];
	
	// and reload the table
	[self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

#pragma mark -
#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.serviceChanges != nil ? [self.serviceChanges count] : 0;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
	static NSString *CellWithDateIdentifier = @"DateCell";

	ServiceChange *change = [self.serviceChanges objectAtIndex:indexPath.row];
	BOOL hasDateString = change.dateString != nil && [change.dateString length] > 0;
	
	TTSimpleCellStyle styleToUse = (hasDateString ? TTSimpleCellStyleSubtitle : TTSimpleCellStyleDefault);
	NSString *identifier = (hasDateString ? CellIdentifier : CellWithDateIdentifier);
	
    SimpleCell *cell = (SimpleCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[[SimpleCell alloc] initWithCellStyle:styleToUse reuseIdentifier:identifier] autorelease];
		[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];

		GradientBackgroundCell *background = [[GradientBackgroundCell alloc] initWithFrame:CGRectMake(0, 0, 320, 44) cellStyle:OnboardCellStyleNone];
		[cell setBackgroundView:background];
		[background release];
    }
    
    // Set up the cell...
	[cell setTextLabelText:change.title];
	if (hasDateString)
		[cell setDetailTextLabelText:change.dateString];
	
    return cell;
}

- (void)serviceChangesServiceDidFailWithError:(NSError *)error
{
	// hide the loading text and error text
	[loadingIndicator stopAnimating];
	[loadingIndicator setHidden:YES];
	[loadingText setHidden:YES];
	
	// show the error text
	errorText = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 100)];
	[errorText setTextColor:[UIColor blackColor]];
	[errorText setBackgroundColor:[UIColor clearColor]];
	[errorText setOpaque:NO];
	[errorText setFont:[UIFont boldSystemFontOfSize:14]];
	[errorText setTextAlignment:UITextAlignmentCenter];
	[errorText setNumberOfLines:0];
	[errorText setLineBreakMode:UILineBreakModeWordWrap];
	[errorText setText:([error code] == ServiceChangesServiceErrorNotReachable ? [error localizedDescription] : NSLocalizedString(@"service-changes-error", nil))];
	[self.tableView.tableFooterView addSubview:errorText];
	
	
	// create a retry button
	UIBarButtonItem *retry = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"service-changes-retry", nil)
															  style:UIBarButtonItemStylePlain
															 target:self
															 action:@selector(retry)];
	[self.navigationItem setRightBarButtonItem:retry animated:YES];
	[retry release];
}

- (void)retry
{
	// remove the error text
	[errorText removeFromSuperview];
	[errorText release];
	errorText = nil;
	
	// re-show the loading
	[loadingText setHidden:NO];
	[loadingIndicator setHidden:NO];
	[loadingIndicator startAnimating];
	
	// attempt to redownload
	[self.service getServiceChangesInBackgroundThread];
	[self.navigationItem setRightBarButtonItem:nil animated:YES];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

	ServiceChange *change = [self.serviceChanges objectAtIndex:indexPath.row];

	ServiceChangeViewController *v = [[ServiceChangeViewController alloc] initWithServiceChange:change];
	[self.navigationController pushViewController:v animated:YES];
	[v release];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


- (void)dealloc {
    [super dealloc];
}


@end

