//
//  TramImage.h
//  tramTRACKER
//
//  Created by Robert Amos on 11/05/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TramImage : UIImageView {
	id target;
	SEL action;
}

- (id)initWithImage:(UIImage *)anImage target:(id)aTarget action:(SEL)anAction;

@end
