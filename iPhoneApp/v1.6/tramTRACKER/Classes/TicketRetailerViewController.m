//
//  TicketRetailerViewController.m
//  tramTRACKER
//
//  Created by Robert Amos on 26/07/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "TicketRetailerViewController.h"
#import "TicketRetailerHeaderView.h"
#import "DirectionsCell.h"

@implementation TicketRetailerViewController

@synthesize retailer;

#pragma mark -
#pragma mark Initialization

- (id)initWithTicketRetailer:(TicketRetailer *)aRetailer
{
	if ((self = [super initWithStyle:UITableViewStyleGrouped]))
	{
		// set our background colour
		[self setRetailer:aRetailer	];
	}
	return self;
}
/*
- (id)initWithStyle:(UITableViewStyle)style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    if ((self = [super initWithStyle:style])) {
    }
    return self;
}
*/


#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];

	[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];

	// set the header view
	[self.tableView setTableHeaderView:[self headerView]];
	
	// Make sure our title is in place
	[self.navigationItem setTitle:NSLocalizedString(@"ticket-outlet-view-title", @"Ticket Outlet")];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    // while ever we're alive, listen for updates
    [[NSNotificationCenter defaultCenter] addObserver:self.tableView.tableHeaderView selector:@selector(showNeedsUpdate:) name:TTTicketOutletsUpdateRequired object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self.tableView.tableHeaderView selector:@selector(hideNeedsUpdate:) name:TTTicketOutletsUpdateNotRequired object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshRetailer:) name:TTTicketOutletsHaveBeenUpdatedNotice object:nil];
}

/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    [[NSNotificationCenter defaultCenter] removeObserver:self.tableView.tableHeaderView];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

#pragma mark -
#pragma mark Table Header

- (TicketRetailerHeaderView *)headerView
{
	// Create a new header view.
	CGRect frame = CGRectMake(0, 0, self.tableView.frame.size.width, 50);
	TicketRetailerHeaderView *header = [[TicketRetailerHeaderView alloc] initWithFrame:frame];
	
	[header setRetailer:self.retailer];
	
	return [header autorelease];
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 4;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return section >= 2 ? 2 : 1;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell = nil;
    
    // Configure the cell...
	if (indexPath.section == 0 && indexPath.row == 0)
	{
		static NSString *CellIdentifier = @"AddressCell";
		
		cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		if (cell == nil) {
			cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:CellIdentifier] autorelease];
			[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
		}

		[cell.textLabel setText:NSLocalizedString(@"ticket-outlet-label-address", @"Address")];
		[cell.detailTextLabel setNumberOfLines:0];
		[cell.detailTextLabel setText:[self.retailer.address stringByReplacingOccurrencesOfString:@", " withString:@"\n"]];
	}
	
	// is open 24 hours?
    else if (indexPath.section == 1 && indexPath.row == 0)
	{
		static NSString *CellIdentifier = @"DetailCell";
		
		cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		if (cell == nil) {
			cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:CellIdentifier] autorelease];
			[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
		}
		
		[cell.textLabel setText:NSLocalizedString(@"ticket-outlet-24hour", @"Open 24 Hours")];
		[cell.detailTextLabel setText:([retailer isOpen24Hours] ? NSLocalizedString(@"ticket-outlet-24hour-yes", @"Yes") : NSLocalizedString(@"ticket-outlet-24hour-no", @"No"))];
	}
	
	// sells Myki?
	else if (indexPath.section == 2 && indexPath.row == 0)
	{
		static NSString *CellIdentifier = @"DetailCell";
		
		cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		if (cell == nil) {
			cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:CellIdentifier] autorelease];
			[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
		}
		
		[cell.textLabel setText:NSLocalizedString(@"ticket-outlet-myki", @"Sells myki")];
		[cell.detailTextLabel setText:([retailer doesSellMyki] ? NSLocalizedString(@"ticket-outlet-myki-yes", @"Yes") : NSLocalizedString(@"ticket-outlet-myki-no", @"No"))];
	}
	
	// sells Metcard
	else if (indexPath.section == 2 && indexPath.row == 1)
	{
		static NSString *CellIdentifier = @"DetailCell";
		
		cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		if (cell == nil) {
			cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:CellIdentifier] autorelease];
			[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
		}
		
		[cell.textLabel setText:NSLocalizedString(@"ticket-outlet-metcard", @"Sells Metcard")];
		[cell.detailTextLabel setText:([retailer doesSellMetcard] ? NSLocalizedString(@"ticket-outlet-metcard-yes", @"Yes") : NSLocalizedString(@"ticket-outlet-metcard-no", @"No"))];
	}
	
	// Directions To Here
	else if (indexPath.section == 3)
	{
		static NSString *CellIdentifier = @"DirectionsCell";

		DirectionsCell *directionsCell = (DirectionsCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		if (directionsCell == nil) {
			directionsCell = [[[DirectionsCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:CellIdentifier] autorelease];

			// hide the detail text label
			[directionsCell.detailTextLabel setHidden:YES];
		}
		
		// To or From?
		if (indexPath.row == 0)
			[directionsCell.textLabel setText:NSLocalizedString(@"ticket-outlet-directions-to", @"Directions To Here")];
		else
			[directionsCell.textLabel setText:NSLocalizedString(@"ticket-outlet-directions-from", @"Directions From Here")];

		return directionsCell;
	}
	
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 0 && indexPath.row == 0)
	{
		NSString *newString = [self.retailer.address stringByReplacingOccurrencesOfString:@", " withString:@"\n"];
		CGSize size = [newString sizeWithFont:[UIFont systemFontOfSize:16] constrainedToSize:CGSizeMake(200, 2009) lineBreakMode:UILineBreakModeWordWrap];
		
		CGFloat newHeight = size.height;
		newHeight += 20;
		
		// but only if its bigger than 44px
		if (newHeight < 44)
			newHeight = 44;
		
		return newHeight;
	}
	
	return 44;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	if (section == 2)
		return NSLocalizedString(@"ticket-outlet-sectionheader-sells", @"Sells");
	return nil;
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    if (section == 1)
        return NSLocalizedString(@"ticket-outlet-needs-update", nil);
    return nil;
}

- (void)refreshRetailer:(NSNotification *)note 
{
    [self.tableView reloadData];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
	// we only support touching the direction cells
	if (indexPath.section != 3)
		return;
	
	// showing directions to or from?
	if (indexPath.row == 0)
		[self.retailer launchGoogleMapsWithDirectionsToHere];
	else
		[self.retailer launchGoogleMapsWithDirectionsFromHere];
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {

	[retailer release]; retailer = nil;
	[super dealloc];
}


@end

