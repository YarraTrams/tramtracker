//
//  WelcomeController.h
//  tramTRACKER
//
//  Created by Robert Amos on 2/04/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface WelcomeController : UIViewController {
}

- (IBAction)didClickButton:(id)sender;

@end

@interface WelcomeView : UIScrollView
{
}

@end

