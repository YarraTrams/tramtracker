//
//  RouteDeleteOperation.h
//  tramTRACKER
//
//  Created by Robert Amos on 7/08/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Route;
@class BackgroundSynchroniser;

/**
 * A delete operation for a specific route
 *
 * @ingroup Sync
**/
@interface RouteDeleteOperation : NSOperation {

	/**
	 * The route that will be deleted
	**/
	Route *route;

	/**
	 * The BackgroundSynchroniser object that is managing this update operation
	 **/
	BackgroundSynchroniser *syncManager;
	
	/**
	 * Status - YES if the operation is executing, NO otherwise
	 **/
	BOOL executing;
	
	/**
	 * Status - YES if the operation was finished (or cancelled), NO otherwise
	 **/
	BOOL finished;
}

@property (nonatomic, readonly) Route *route;
@property (nonatomic, assign) BackgroundSynchroniser *syncManager;

/**
 * Initialises the route delete operation.
 *
 * @param	aRoute			The route to be deleted
 * @return					An initialised RouteDeleteOperation object
**/
- (id)initWithRoute:(Route *)aRoute;

/**
 * Required by the NSOperationQueue, whether to execute this operation concurrently.
 **/
- (BOOL)isConcurrent;

/**
 * NSOperationQueue wrapper for the executing ivar.
 **/
- (BOOL)isExecuting;

/**
 * NSOperationQueue wrapper for the finished ivar.
 **/
- (BOOL)isFinished;

/**
 * Starts the Stop Delete Operation
 **/
- (void)start;

/**
 * Finishes the Update operation and handles notifications
 **/
- (void)finish;

/**
 * Deletes the route
**/
- (void)deleteRoute;

@end
