//
//  Tram.m
//  tramTRACKER
//
//  Created by Robert Amos on 19/06/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "Tram.h"


@implementation Tram

@synthesize number, route, headboardRouteNumber, upDirection, atLayover, available, disrupted, specialEvent, routeNumber;


+ (Tram *)tramForStub:(TramStub *)stub
{
    Tram *tram = [stub copy];
    
    // Pass it through with a route set.
    [tram setRoute:[Route routeForStub:stub.routeStub]];

    // return it
    return [tram autorelease];
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"Tram Number %@ travelling on %@ (%@)", number, route, headboardRouteNumber];
}

// return the trams name, if it has one
- (NSString *)name
{
	NSInteger tramNumber = [self.number integerValue];
	switch (tramNumber)
	{
		case 5123:
			return NSLocalizedString(@"tram-name-5123", @"Tram 5123");
			
		case 5113:
			return NSLocalizedString(@"tram-name-5113", @"Tram 5113");
			
		case 5103:
			return NSLocalizedString(@"tram-name-5103", @"Tram 5103");
			
		case 5106:
			return NSLocalizedString(@"tram-name-5106", @"Tram 5106");
			
		case 5111:
			return NSLocalizedString(@"tram-name-5111", @"Tram 5111");
			
		default:
			return [NSString stringWithFormat:NSLocalizedString(@"tram-name", @"Tram x"), self.number];
	}
}

- (NSString *)className
{
	NSInteger tramNumber = [self.number integerValue];
    
	// zeros are unknown
	if (tramNumber <= 0)
		return nil;

	// Z1 class trams
	if (tramNumber >= 1 && tramNumber <= 100)
		return @"Z1";

	// Z2 class trams
	else if (tramNumber <= 115)
		return @"Z2";
	
	// Z3 class trams
	else if (tramNumber <= 230)
		return @"Z3";
	
	// A1 class trams
	else if (tramNumber <= 258)
		return @"A1";

	// A2 class trams
	else if (tramNumber <= 300)
		return @"A2";
	
	// W Class trams
	else if (tramNumber >= 681 && tramNumber <= 1040)
		return @"W";
	
	// B1 class trams
	else if (tramNumber == 2001 || tramNumber == 2002)
		return @"B1";
	
	// B2 Class Trams
	else if (tramNumber >= 2003 && tramNumber <= 2132)
		return @"B2";
	
	// C Class Trams
	else if (tramNumber >= 3001 && tramNumber <= 3036)
		return @"C";
	
	// D1 Class trams
	else if (tramNumber >= 3501 && tramNumber <= 3600)
		return @"D1";
	
	// D2 Class trams
	else if (tramNumber >= 5001 && tramNumber <= 5100)
		return @"D2";
	
	// Bumblebee trams
	else if (tramNumber >= 5101 && tramNumber <= 5200)
		return @"C2";
    
    // E Class trams
	else if (tramNumber >= 6001 && tramNumber <= 6050)
		return @"E";
    
    
	
	// bleh, return the default
	return nil;
}
- (UIImage *)image
{
	NSString *c = [self className];
	
	// Z1, Z2, Z3 class trams
	if ([c isEqualToString:@"Z1"] || [c isEqualToString:@"Z2"] || [c isEqualToString:@"Z3"])
		return [UIImage imageNamed:@"TramIconZClass.png"];
	
	// A1, A2 class trams
	else if ([c isEqualToString:@"A1"] || [c isEqualToString:@"A2"])
		return [UIImage imageNamed:@"TramIconAClass.png"];
	
	// W Class trams
	else if ([c isEqualToString:@"W"])
		return [UIImage imageNamed:@"TramIconWClass.png"];
	
	// B Class Trams
	else if ([c isEqualToString:@"B1"] || [c isEqualToString:@"B2"])
		return [UIImage imageNamed:@"TramIconBClass.png"];
	
	// C1 Class Trams
	else if ([c isEqualToString:@"C"])
		return [UIImage imageNamed:@"TramIconCClass.png"];
	
	// D1 Class trams
	else if ([c isEqualToString:@"D1"] || [c isEqualToString:@"D2"])
		return [UIImage imageNamed:@"TramIconDClass.png"];
	
	// Bumblebee trams
	else if ([c isEqualToString:@"C2"])
		return [UIImage imageNamed:@"TramIconC2Class.png"];
    
    // E Class trams
	else if ([c isEqualToString:@"E"])
		return [UIImage imageNamed:@"TramIconEClass.png"];
	
	// bleh, return the default
	return [UIImage imageNamed:@"tramicon.png"];
}

- (UIImage *)silhouette
{
	NSString *c = [self className];
	
	if (c == nil)
		return nil;
	
	// Z1, Z2 class trams
	if ([c isEqualToString:@"Z1"] || [c isEqualToString:@"Z2"])
		return [UIImage imageNamed:@"z1.png"];
	
	// Z3 class trams
	else if ([c isEqualToString:@"Z3"])
		return [UIImage imageNamed:@"z3.png"];
	
	// A1, A2 class trams
	else if ([c isEqualToString:@"A1"] || [c isEqualToString:@"A2"])
		return [UIImage imageNamed:@"a.png"];
	
	// W Class trams
	else if ([c isEqualToString:@"W"])
		return [UIImage imageNamed:@"w.png"];
	
	// B Class Trams
	else if ([c isEqualToString:@"B1"] || [c isEqualToString:@"B2"])
		return [UIImage imageNamed:@"b.png"];
	
	// C1 Class Trams
	else if ([c isEqualToString:@"C"])
		return [UIImage imageNamed:@"c.png"];
	
	// D1 Class trams
	else if ([c isEqualToString:@"D1"])
		return [UIImage imageNamed:@"d1.png"];

	// D2 class trams
	else if ([c isEqualToString:@"D2"])
		return [UIImage imageNamed:@"d2.png"];
	
	// Bumblebee trams
	else if ([c isEqualToString:@"C2"])
		return [UIImage imageNamed:@"c2.png"];
    
    // E Class trams
	else if ([c isEqualToString:@"E"])
		return [UIImage imageNamed:@"e.png"];
	
	// bleh, return the default
	return nil;	
}

/**
 * A clipped silhouette
**/

/* No longer using a clipped silhouette
- (UIImage *)clippedSilhouette
{
	NSString *c = [self className];
	
	if (c == nil)
		return nil;
	
	// Only the C2 and E class are big enough to requrie a clipped silhouette.
	if ([c isEqualToString:@"C2"])
		return [UIImage imageNamed:@"c2-clipped.png"];

	else if ([c isEqualToString:@"E"])
		return [UIImage imageNamed:@"e-clipped.png"];
	
	// Use the default for all other classes
	return [self silhouette];
}
 */

- (NSURL *)URLOfGongSound
{
	NSString *nameOfGongSound = [Tram nameOfGongSoundForClass:[self className]];
	if (nameOfGongSound != nil)
		return [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:nameOfGongSound ofType:nil]];
	
	// not supported
	return nil;
}

+ (NSString *)nameOfGongSoundForClass:(NSString *)c
{
	// easy peasy, only three supported
	if ([c isEqualToString:@"C"] || [c isEqualToString:@"C2"] || [c isEqualToString:@"D1"]
        || [c isEqualToString:@"D2"]|| [c isEqualToString:@"E"])
	{
		// These classes use the citadis gong
		return @"citadis.caf";
		
	} else if ([c isEqualToString:@"W"])
	{
		return @"w.caf";
	}
		
	return @"a.caf";
}

- (BOOL)isRouteZero
{
	return [self.headboardRouteNumber isEqualToString:@"0"];
}
- (BOOL)isUnknownRoute
{
	return self.route == nil;
}
- (BOOL)isCityCircle
{
	return [self.headboardRouteNumber isEqualToString:@"35"];
}

// do we have a pull cord?
- (BOOL)hasPullCord
{
	// this is class dependent
	NSString *c = [self className];

	// these are a yes
	if ([c isEqualToString:@"Z1"] || [c isEqualToString:@"Z2"] || [c isEqualToString:@"Z3"] || [c isEqualToString:@"A1"] ||
		[c isEqualToString:@"A2"] || [c isEqualToString:@"W"] || [c isEqualToString:@"B1"] || [c isEqualToString:@"B2"])
		return YES;

	// nada
	return NO;
}

- (NSString *)destination
{
	return upDirection ? route.upDestination : route.downDestination;
}

- (BOOL)isEqualToTram:(Tram *)otherTram
{
	// same tram number?
	return [self.number isEqualToNumber:otherTram.number];
}

- (id)copyWithZone:(NSZone *)zone
{
	Tram *copy = [[Tram alloc] init];
	[copy setAtLayover:self.atLayover];
	[copy setAvailable:self.available];
	[copy setDisrupted:self.disrupted];
	[copy setHeadboardRouteNumber:[[self.headboardRouteNumber copy] autorelease]];
	[copy setNumber:[[self.number copy] autorelease]];
	[copy setRoute:self.route];
	[copy setRouteNumber:[[self.routeNumber copy] autorelease]];
	[copy setSpecialEvent:self.specialEvent];
	[copy setUpDirection:self.upDirection];
	return copy;
}


- (void)dealloc
{
	[number release];
	[route release];
	[headboardRouteNumber release];
	[routeNumber release];
	[super dealloc];
}

@end

@implementation TramStub

@synthesize routeStub;

- (void)dealloc
{
    [routeStub release]; routeStub = nil;
    [super dealloc];
}

@end
