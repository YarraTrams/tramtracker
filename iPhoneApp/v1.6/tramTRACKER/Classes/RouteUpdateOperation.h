//
//  RouteUpdateOperation.h
//  tramTRACKER
//
//  Created by Robert Amos on 29/07/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Route;
@class PidsService;
@class BackgroundSynchroniser;

/**
 * An Update operation to a Route
 *
 * @ingroup Sync
 **/
@interface RouteUpdateOperation : NSOperation {

	/**
	 * The Route to be updated
	**/
	Route *route;

	/**
	 * An instance of the PidsService to retrieve route information from.
	**/
	PidsService *service;

	/**
	 * The BackgroundSynchroniser object that is managing this update operation
	**/
	BackgroundSynchroniser *syncManager;

	/**
	 * Status - YES if the operation is executing, NO otherwise
	**/
	BOOL executing;

	/**
	 * Status - YES if the operation was finished (or cancelled), NO otherwise
	**/
	BOOL finished;
	
	/**
	 * Status - Whether the route's destinations have been updated.
	**/
	BOOL destinationsUpdated;

	/**
	 * Status - Whether the list of up stops has been updated.
	**/
	BOOL upStopsUpdated;
    BOOL upTurnsUpdated;

	/**
	 * Status - Whether the list of down stops has been updated.
	**/
	BOOL downStopsUpdated;
    BOOL downTurnsUpdated;
}

@property (nonatomic, readonly) Route *route;
@property (nonatomic, assign) BackgroundSynchroniser *syncManager;

/**
 * Initialises a Route Update Operation for the specified Route
 *
 * @param	aRoute				The Route to be updated.
 * @return						An initialised RouteUpdateOperation
**/
- (id)initWithRoute:(Route *)aRoute;

/**
 * Required by the NSOperationQueue, whether to execute this operation concurrently.
**/
- (BOOL)isConcurrent;

/**
 * NSOperationQueue wrapper for the executing ivar.
**/
- (BOOL)isExecuting;

/**
 * NSOperationQueue wrapper for the finished ivar.
**/
- (BOOL)isFinished;

/**
 * Starts the Route Update Operation
**/
- (void)start;

/**
 * Updates the route's destination information
**/
- (void)updateDestinations;

/**
 * Updates the route's up and down stop lists.
**/
- (void)updateStopLists;

/**
 * Finishes the Update operation and handles notifications
**/
- (void)finish;


@end
