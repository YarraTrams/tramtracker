//
//  HelpSettingsController.m
//  tramTRACKER
//
//  Created by Robert Amos on 5/05/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "HelpSettingsController.h"

const NSString *HelpURLSubdirSettings = @"settings";

#define HELP_SETTINGS_ACCESS 0
#define HELP_SETTINGS_STARTUP 1
#define HELP_SETTINGS_SYNC 2
#define HELP_SETTINGS_DISPLAY 3
#define HELP_SETTINGS_BROWSE 4


@implementation HelpSettingsController

- (id)initHelpSettingsController
{
	if (self = [super initWithStyle:UITableViewStylePlain])
	{
		[self setTitle:NSLocalizedString(@"help-settings", @"Help")];
		[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
	}
	return self;
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    SimpleCell *cell = (SimpleCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[SimpleCell alloc] initWithCellStyle:TTSimpleCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
		
		GradientBackgroundCell *background = [[GradientBackgroundCell alloc] initWithFrame:CGRectMake(0, 0, 320, 44) cellStyle:OnboardCellStyleNone];
		[cell setBackgroundView:background];
		[background release];
	}
    
	switch (indexPath.row)
	{
		case HELP_SETTINGS_ACCESS:
			[cell setTextLabelText:NSLocalizedString(@"help-settings-access", @"Accessing Settings")];
			break;
		case HELP_SETTINGS_STARTUP:
			[cell setTextLabelText:NSLocalizedString(@"help-settings-startup", @"Startup")];
			break;
		case HELP_SETTINGS_DISPLAY:
			[cell setTextLabelText:NSLocalizedString(@"help-settings-display", @"Display Options")];
			break;
		case HELP_SETTINGS_BROWSE:
			[cell setTextLabelText:NSLocalizedString(@"help-settings-browse", @"Browsing Stop Lists")];
			break;
		case HELP_SETTINGS_SYNC:
			[cell setTextLabelText:NSLocalizedString(@"help-settings-sync", @"Synchronisation Settings")];
			break;
	}
	
    return cell;	
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

	// we create a UIWebView that we push onto the stack, start by working out our URL
	NSString *fileName = nil;
	
	switch (indexPath.row)
	{
		case HELP_SETTINGS_ACCESS:
			fileName = @"accessing15.html";
			break;
			
		case HELP_SETTINGS_STARTUP:
			fileName = @"startup12.html";
			break;
			
		case HELP_SETTINGS_DISPLAY:
			fileName = @"display_options.html";
			break;
			
		case HELP_SETTINGS_BROWSE:
			fileName = @"stop_lists.html";
			break;
			
		case HELP_SETTINGS_SYNC:
			fileName = @"sync15.html";
			break;
	}
	
	// nothing found?
	if (fileName == nil)
		return;
	
	// create the web view
	HelpWebController *helpWeb = [[HelpWebController alloc] initWithHelpFile:[NSString stringWithFormat:@"%@/%@", HelpURLSubdirSettings, fileName]];
	SimpleCell *cell = (SimpleCell *)[self.tableView cellForRowAtIndexPath:indexPath];
	[helpWeb setTitle:cell.textLabel.text];
	[self.navigationController pushViewController:helpWeb animated:YES];
	[helpWeb release];	
}



- (void)dealloc {
    [super dealloc];
}


@end

