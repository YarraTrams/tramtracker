//
//  tramTRACKERAppDelegate.m
//  tramTRACKER
//
//  Created by Robert Amos on 17/02/09.
//  Copyright Yarra Trams 2009. All rights reserved.
//

#import "tramTRACKERAppDelegate.h"
#import "BackgroundSynchroniser.h"
#import "AutomaticSynchroniserDataLogger.h"
#import "Tram.h"

CGFloat const TTBackgroundColorRed = 0.8745;
CGFloat const TTBackgroundColorGreen = 0.9529;
CGFloat const TTBackgroundColorBlue = 0.9059;
CGFloat const TTNavigationBarTintColorRed = 0.0;
CGFloat const TTNavigationBarTintColorGreen = 0.5;
CGFloat const TTNavigationBarTintColorBlue = 0.0;
CGFloat const TTSearchBarTintColorRed = 0.62;
CGFloat const TTSearchBarTintColorGreen = 0.9;
CGFloat const TTSearchBarTintColorBlue = 0.62;

NSInteger const PredictionListCellHeight = 70;

NSInteger const PIDShowMessagesNone = 1;
NSInteger const PIDShowMessagesOnce = 2;
NSInteger const PIDShowMessagesTwice = 3;
NSInteger const PIDShowMessagesAlways = 4;

NSString * const AlertSoundNone = @"none";
NSString * const AlertSoundSystemDefault = @"system";
NSString * const AlertSoundTramAClass = @"a.caf";
NSString * const AlertSoundTramCClass = @"citadis.caf";
NSString * const AlertSoundTramWClass = @"w.caf";

NSString * const TTTabKeyFavourites = @"favourites";
NSString * const TTTabKeyNearby = @"nearby";
NSString * const TTTabKeyBrowse = @"browse";
NSString * const TTTabKeyOnboard = @"onboard";
NSString * const TTTabKeySearch = @"search";
NSString * const TTTabKeyEnterID = @"enterid";
NSString * const TTTabKeySchedules = @"schedules";
NSString * const TTTabKeyServiceChanges = @"servicechanges";
NSString * const TTTabKeyMostRecent = @"mostrecent";
NSString * const TTTabKeySettings = @"settings";
NSString * const TTTabKeyHelp = @"help";
NSString * const TTTabKeyUpdates = @"updates";
NSString * const TTTabKeyTickets = @"tickets";

NSString * const TTTicketOutletsUpdateRequired = @"TTTicketOutletsUpdateRequired";
NSString * const TTTicketOutletsUpdateNotRequired = @"TTTicketOutletsUpdateNotRequired";

NSString * const TTTicketOutletsHaveBeenUpdatedNotice = @"TTTicketOutletsHaveBeenUpdatedNotice";
NSString * const TTStopsAndRoutesHaveBeenUpdatedNotice = @"TTStopsAndRoutesHaveBeenUpdatedNotice";

//NSString * const TTTicketOutletUpdateURL = @"http://extranetdev.yarratrams.com.au/MetlinkRetailOutletInfo/TicketRetailers.plist";
NSString * const TTTicketOutletUpdateURL = @"http://ws.tramtracker.com.au/MetlinkRetailOutletInfo/TicketRetailers.plist";

// The modification date of the tickets file and database file, as a unix timestamp.
/*
 // Timestamps for previous version 2012-05-20 00:00
NSTimeInterval const TTTicketsFileModicationDate = 1335830400; 
NSTimeInterval const TTDatabaseModificationDate = 1335830400; 
 Timestamps for current version
 
 
 //timestamp from version 6.1
 NSTimeInterval const TTTicketsFileModicationDate = 1372809600;//2013-07-03 00:00
 */

NSTimeInterval const TTTicketsFileModicationDate = 1372809600;//2013-07-03 00:00
NSTimeInterval const TTDatabaseModificationDate = 1378684800;//2013-09-09 00:00

@implementation tramTRACKERAppDelegate

@synthesize window, favouritesNavigationController, favouritesList, tabBar, browseNavigationController,
			searchController, enterIDController, nearbyController, recentController, serviceChangesController, updatesController, updatesNavigationController,
			settingsController, onboardDisplaysController, recent, aboutController, scheduledController, tramDetector, tramNumberController,
			ticketsNavigationController, lastTapTime;



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	// Register applications defaults
	[self registerDefaults];
	
	// Start with the Tab View
	[self initTabBar];
	
	// upgrade stops as needed
	StopList *stopList = [StopList sharedManager];
	if (![stopList hasUpgradedFavouritesTo11])
		[stopList upgradeFavouritesTo11];
	if (![stopList hasUpgradedFavouritesTo12])
		[stopList upgradeFavouritesTo12];
	
	// Start a background synchronisation
	[self synchroniseData];
	
	// check to see how we are starting up
	NSURL *launchURL = [launchOptions objectForKey:UIApplicationLaunchOptionsURLKey];
	if (launchOptions != nil && launchURL != nil)
	{
		// startup with a URL
		if (![self startupWithURL:launchURL allowRestore:NO])
		{
			// if that fails just use the preferences
			[self lookForTramsInRange];
			[self startupWithPreferencesAndAllowRestore:NO];
		}

	} else
	{
		// startup based on the users preferences
		[self lookForTramsInRange];
		[self startupWithPreferencesAndAllowRestore:NO];
	}
    
    [window addSubview:[tabBar view]];
	[window makeKeyAndVisible];
	
	return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
	return [self startupWithURL:url allowRestore:NO];
}
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
	return [self startupWithURL:url allowRestore:NO];
}

//
// Register the application defaults
//
- (void)registerDefaults
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSMutableDictionary *appDefaults = [[NSMutableDictionary alloc] initWithCapacity:0];
	
	// turn indicators are enabled
	[appDefaults setObject:@"YES" forKey:@"showTurnIndicators"];
	
	// show tram profiles
	[appDefaults setObject:@"YES" forKey:@"showTramProfiles"];
	
	// gps on board is enabled
	[appDefaults setObject:@"YES" forKey:@"onboardGPSEnabled"];
	
	// the default for the nearby tab
	[appDefaults setObject:@"list" forKey:@"nearbyTabDefaultSegment"];
	
	// jump to the stop by default
	[appDefaults setObject:@"NO" forKey:@"jumpToNearestStopInStopList"];
	[appDefaults setObject:[NSDate dateWithTimeIntervalSince1970:TTTicketsFileModicationDate] forKey:@"ticketLastSynchronisationDate"];

	[appDefaults setObject:[NSDate dateWithTimeIntervalSince1970:TTDatabaseModificationDate] forKey:@"lastSynchronisationNSDate"];

	// background location support
	/**
     * ARRIVAL ALARM SUPPORT
     *
     *
    [appDefaults setObject:@"YES" forKey:@"backgroundEnabled"];
	[appDefaults setObject:@"YES" forKey:@"backgroundDisplayBadge"];
	[appDefaults setObject:@"citadis.caf" forKey:@"backgroundAlertSoundName"];*/
	
	// background synchronisation
	[appDefaults setObject:[NSNumber numberWithInteger:TTSyncTypeAutoCheckManualSync] forKey:@"synchronisationType"];
	
	// show messages twice
	[appDefaults setObject:[NSNumber numberWithInteger:PIDShowMessagesTwice] forKey:@"showMessagesOnPID"];
	
	// nearest favourite and smart restore
	[appDefaults setObject:[NSNumber numberWithInt:1] forKey:@"startup"];
	[appDefaults setObject:@"YES" forKey:@"smartRestore"];
	
	// the default starting region for the Nearby Map
	NSDictionary *defaultNearbyMapRegion = [NSDictionary dictionaryWithObjectsAndKeys:
											[NSNumber numberWithDouble:-37.81403046749801], @"latitude",
											[NSNumber numberWithDouble:144.9632048606873], @"longitude",
											[NSNumber numberWithDouble:0.006221261474024686], @"latitudeDelta",
											[NSNumber numberWithDouble:0.006866455078125], @"longitudeDelta",
											nil];
	[appDefaults setObject:defaultNearbyMapRegion forKey:@"nearbyMapLastRegion"];
	
	// register those
	[defaults registerDefaults:appDefaults];
	[appDefaults release];
}

//
// Setup the main tab controller
//
- (void)initTabBar
{
	// set the default tab order
	defaultTabOrder = [[NSArray arrayWithObjects:TTTabKeyFavourites, TTTabKeyNearby, TTTabKeyBrowse, TTTabKeyOnboard, TTTabKeySearch, TTTabKeyEnterID, TTTabKeySchedules, TTTabKeyTickets, TTTabKeyServiceChanges, TTTabKeyMostRecent, TTTabKeyUpdates, TTTabKeySettings, TTTabKeyHelp, nil] retain];
	
	// create the tab bar
	[self setTabBar:[[[UITabBarController alloc] initWithNibName:nil bundle:nil] autorelease]];
	[self.tabBar setDelegate:self];
	
	// init the favourites list for the first tab
	[self initFavourites];
	
	// and the browse by route for the second
	[self initBrowse];
	
	// And the Search one
	[self initSearch];
	
	// Nearby
	[self initNearby];
	
	// Enter Tracker ID
	[self initEnterTrackerID];
	
	// Most Recent Stops
	[self initMostRecent];
	
	// Onboard Displays
	[self initOnboard];
	
	// About / Help
	[self initAbout];
	
	// Scheduled
	[self initScheduled];
	
	// Settings
	[self initSettings];
	
	// Service Changes
	[self initServiceChanges];
	
	// Updates
	[self initUpdates];
	
	// Ticket Retailers
	[self initTicketRetailers];
	
	// set the view controllers
	tabStorage = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:favouritesNavigationController, nearbyController, browseNavigationController, onboardDisplaysController, searchController, enterIDController, scheduledController, ticketsNavigationController, serviceChangesController, recentController, updatesNavigationController, settingsController, aboutController, nil]
											   forKeys:defaultTabOrder];
	[self.tabBar setViewControllers:[self orderedTabs]];
	
	// if we have enough view controllers we should have a More item now, tint that bar too if it exists
	if (self.tabBar.moreNavigationController != nil)
	{
		[[self.tabBar.moreNavigationController.view.subviews objectAtIndex:0] setBackgroundColor:[UIColor blackColor]];
		[self.tabBar.moreNavigationController.navigationBar setTintColor:[UIColor colorWithRed:TTNavigationBarTintColorRed green:TTNavigationBarTintColorGreen blue:TTNavigationBarTintColorBlue alpha:0.8]];
	}
}

- (NSArray *)orderedTabs
{
	// Check the defaults to see if we have a better list
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSArray *tabOrdering = [defaults arrayForKey:@"tabOrder"];
	
	// if theres no custom ordering set a default
	if (tabOrdering == nil || [tabOrdering count] == 0)
		tabOrdering = defaultTabOrder;

	
	// so now we loop over and make a mutable array of tabs in the correct order, we also mark them off as we go
	NSMutableArray *tabs = [[NSMutableArray alloc] initWithCapacity:0];
	NSMutableArray *fullTabList = [defaultTabOrder mutableCopy];

	for (NSString *tabKey in tabOrdering)
	{
		// is the tab in the list? mark it off
		if ([tabStorage objectForKey:tabKey] != nil)
			[tabs addObject:[tabStorage objectForKey:tabKey]];
		[fullTabList removeObject:tabKey];
	}
	
	// are there any tabs left over? add them to the end
	if ([fullTabList count] > 0)
	{
		for (NSString *tabKey in fullTabList)
		{
			if ([tabStorage objectForKey:tabKey] != nil)
				[tabs addObject:[tabStorage objectForKey:tabKey]];
		}
	}
	
	// return the tab list
	[fullTabList release];
	return [tabs autorelease];
}

//
// Setup the main navigation controller
//
- (void)initFavourites
{
	favouritesList = [[MainListController alloc] initWithStyle:UITableViewStylePlain];
	favouritesNavigationController = [[UINavigationController alloc] initWithRootViewController:favouritesList];
	
	[favouritesNavigationController.view setBackgroundColor:[UIColor blackColor]];
	
	// change the bar tint colour
	[favouritesNavigationController.navigationBar setTintColor:[UIColor colorWithRed:TTNavigationBarTintColorRed green:TTNavigationBarTintColorGreen blue:TTNavigationBarTintColorBlue alpha:0.8]];

	// set the title and the tab image
	[favouritesNavigationController setTabBarItem:[favouritesNavigationController.tabBarItem initWithTabBarSystemItem:UITabBarSystemItemFavorites tag:1]];
}

//
// Load the browse by route view
//
- (void)initBrowse
{
	// Get an instance of the PidsService
	RouteListController *routeList = [[RouteListController alloc] initWithStyle:UITableViewStylePlain];
	[self setBrowseNavigationController:[[UINavigationController alloc] initWithRootViewController:routeList]];
	[routeList setTitle:NSLocalizedString(@"title-browse", @"Browse")];

	// change the bar tint colour
	[self.browseNavigationController.navigationBar setTintColor:[UIColor colorWithRed:TTNavigationBarTintColorRed green:TTNavigationBarTintColorGreen blue:TTNavigationBarTintColorBlue alpha:0.8]];
	[self.browseNavigationController.view setBackgroundColor:[UIColor blackColor]];
	
	// set the image and tab bar title
	[self.browseNavigationController setTabBarItem:[self.browseNavigationController.tabBarItem initWithTitle:NSLocalizedString(@"title-browse", @"Browse")
																									   image:[UIImage imageNamed:@"listtabicon.png"]
																										 tag:2]];
	[routeList release];
}

//
// Load the search page
//
- (void)initSearch
{
	// Get an instance of the PidsService
	SearchListController *searchList= [[SearchListController alloc] initWithStyle:UITableViewStylePlain];
	[searchList setTitle:NSLocalizedString(@"title-search", @"Search")];
	[self setSearchController:[[UINavigationController alloc] initWithRootViewController:searchList]];
	
	// change the bar tint colour
	[self.searchController.navigationBar setTintColor:[UIColor colorWithRed:TTNavigationBarTintColorRed green:TTNavigationBarTintColorGreen blue:TTNavigationBarTintColorBlue alpha:0.8]];
	[self.searchController.view setBackgroundColor:[UIColor blackColor]];
	
	// set the image and tab bar title
	[self.searchController setTabBarItem:[self.searchController.tabBarItem initWithTabBarSystemItem:UITabBarSystemItemSearch tag:3]];
	[searchList release];
}

//
// Load the nearby page
//
- (void)initNearby
{
	NearbyViewController *nearbyViewController = [[NearbyViewController alloc] initNearbyViewController];
	[self setNearbyController:[[UINavigationController alloc] initWithRootViewController:nearbyViewController]];
	
	// change the bar tint colour
	[self.nearbyController.navigationBar setTintColor:[UIColor colorWithRed:TTNavigationBarTintColorRed green:TTNavigationBarTintColorGreen blue:TTNavigationBarTintColorBlue alpha:0.8]];
	[self.nearbyController.view setBackgroundColor:[UIColor blackColor]];
	
	// set the image and tab bar title
	[self.nearbyController setTabBarItem:[self.nearbyController.tabBarItem initWithTitle:NSLocalizedString(@"title-nearby", @"Nearby") image:[UIImage imageNamed:@"nearbytabicon.png"] tag:4]];
	[nearbyViewController release];
}

//
// Load the Enter Tracker ID page
//
- (void)initEnterTrackerID
{
	// Get an instance of the PidsService
	EnterTrackerIDController *enterIDView = [[EnterTrackerIDController alloc] initWithNibName:nil bundle:nil];
	[enterIDView setTitle:NSLocalizedString(@"title-enter-tramtracker-id", @"Enter ID")];
	[self setEnterIDController:[[UINavigationController alloc] initWithRootViewController:enterIDView]];
	
	// change the bar tint colour
	[self.enterIDController.navigationBar setTintColor:[UIColor colorWithRed:TTNavigationBarTintColorRed green:TTNavigationBarTintColorGreen blue:TTNavigationBarTintColorBlue alpha:0.8]];
	[self.enterIDController.view setBackgroundColor:[UIColor blackColor]];
	
	// set the image and tab bar title
	[self.enterIDController setTabBarItem:[self.enterIDController.tabBarItem initWithTitle:NSLocalizedString(@"title-enter-tramtracker-id", @"Enter ID") image:[UIImage imageNamed:@"enteridtabicon.png"] tag:5]];
	[enterIDView release];
}

//
// Load the most recent stops
//
- (void)initMostRecent
{
	// Get an instance of the PidsService
	recent = [[MostRecentController alloc] initWithStyle:UITableViewStylePlain];
	[recent setTitle:NSLocalizedString(@"title-mostrecent", @"Most Recent")];
	[self setRecentController:[[UINavigationController alloc] initWithRootViewController:recent]];
	
	// change the bar tint colour
	[self.recentController.navigationBar setTintColor:[UIColor colorWithRed:TTNavigationBarTintColorRed green:TTNavigationBarTintColorGreen blue:TTNavigationBarTintColorBlue alpha:0.8]];
	[self.recentController.view setBackgroundColor:[UIColor blackColor]];
	
	// set the image and tab bar title
	[self.recentController setTabBarItem:[self.recentController.tabBarItem initWithTabBarSystemItem:UITabBarSystemItemMostRecent tag:6]];
}

//
// Load the settings page
//
- (void)initSettings
{
	// Get an instance of the PidsService
	SettingsListView *settings = [[SettingsListView alloc] initWithStyle:UITableViewStyleGrouped];
	[settings setTitle:NSLocalizedString(@"title-settings", @"Settings")];
	[self setSettingsController:[[UINavigationController alloc] initWithRootViewController:settings]];
	
	// change the bar tint colour
	[self.settingsController.navigationBar setTintColor:[UIColor colorWithRed:TTNavigationBarTintColorRed green:TTNavigationBarTintColorGreen blue:TTNavigationBarTintColorBlue alpha:0.8]];
	
	// set the image and tab bar title
	[self.settingsController setTabBarItem:[self.settingsController.tabBarItem initWithTitle:NSLocalizedString(@"title-settings", @"Settings")
 																					   image:[UIImage imageNamed:@"SettingsTabIcon.png"]
																						 tag:7]];
	[settings release];
}

//
// Load the Onboard Displays
//
- (void)initOnboard
{
	EnterTramNumberController *enterTramNumber = nil;
    
    if ([[UIScreen mainScreen] bounds].size.height == 568) {
        enterTramNumber = [[EnterTramNumberController alloc] initWithNibName:@"EnterTramNumber-iPhone5" bundle:nil];
    }
    else{
        enterTramNumber = [[EnterTramNumberController alloc] initWithNibName:@"EnterTramNumber" bundle:nil];
    }

	[self setOnboardDisplaysController:[[UINavigationController alloc] initWithRootViewController:enterTramNumber]];
	[enterTramNumber setTitle:NSLocalizedString(@"title-onboard", @"Onboard")];
	
	// change the bar tint colour
	[self.onboardDisplaysController.navigationBar setTintColor:[UIColor colorWithRed:TTNavigationBarTintColorRed green:TTNavigationBarTintColorGreen blue:TTNavigationBarTintColorBlue alpha:0.8]];
	[self.onboardDisplaysController.view setBackgroundColor:[UIColor whiteColor]];
	
	// set the image and tab bar title
	[self.onboardDisplaysController setTabBarItem:[self.onboardDisplaysController.tabBarItem initWithTitle:NSLocalizedString(@"title-onboard", @"Onboard")
													image:[UIImage imageNamed:@"onboardview.png"]
													tag:8]];

	[self setTramNumberController:enterTramNumber];
	[enterTramNumber release];
}

//
// Load the scheduled departures screen
//
- (void)initScheduled
{
	// Get an instance of the PidsService
	RouteListController *routeList = [[RouteListController alloc] initWithStyle:UITableViewStylePlain];
	[self setScheduledController:[[UINavigationController alloc] initWithRootViewController:routeList]];
	[routeList setTitle:NSLocalizedString(@"title-scheduled", @"Scheduled Departures")];
	[routeList setPushToScheduledDeparturesScreen:YES];
	
	// change the bar tint colour
	[self.scheduledController.navigationBar setTintColor:[UIColor colorWithRed:TTNavigationBarTintColorRed green:TTNavigationBarTintColorGreen blue:TTNavigationBarTintColorBlue alpha:0.8]];
	[self.scheduledController.view setBackgroundColor:[UIColor blackColor]];
	
	// set the image and tab bar title
	[self.scheduledController setTabBarItem:[self.scheduledController.tabBarItem initWithTitle:NSLocalizedString(@"title-scheduled", @"Scheduled Departures")
											 image:[UIImage imageNamed:@"ScheduledTabBarItem.png"]
											 tag:10]];
	
	[routeList release];
}

//
// Load the help/about screen
//
- (void)initAbout
{
	// Go go
	HelpController *help = [[HelpController alloc] initHelpController];
	[self setAboutController:[[UINavigationController alloc] initWithRootViewController:help]];
	
	[self.aboutController.navigationBar setTintColor:[UIColor colorWithRed:TTNavigationBarTintColorRed green:TTNavigationBarTintColorGreen blue:TTNavigationBarTintColorBlue alpha:0.8]];
	UITabBarItem *tab = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"title-help", @"About / Help") image:[UIImage imageNamed:@"AboutTabIcon.png"] tag:9];
	[self.aboutController setTabBarItem:tab];
	[tab release];
	[help release];
}

//
// Load the Service Changes
//
- (void)initServiceChanges
{
	ServiceChangesController *changesController = [[ServiceChangesController alloc] initWithStyle:UITableViewStylePlain];
	[self setServiceChangesController:[[UINavigationController alloc] initWithRootViewController:changesController]];
	[changesController setTitle:NSLocalizedString(@"title-service-changes", @"Service Changes")];
	
	// change the bar tint colour
	[self.serviceChangesController.navigationBar setTintColor:[UIColor colorWithRed:TTNavigationBarTintColorRed green:TTNavigationBarTintColorGreen blue:TTNavigationBarTintColorBlue alpha:0.8]];
	[self.serviceChangesController.view setBackgroundColor:[UIColor whiteColor]];
	
	// set the image and tab bar title
	[self.serviceChangesController setTabBarItem:[self.serviceChangesController.tabBarItem
                                                  initWithTitle:NSLocalizedString(@"title-service-changes", @"Service Changes")
												   image:[UIImage imageNamed:@"ServiceChangesTabIcon.png"]
												   tag:12]];
	[changesController release];
}

//
// Load the updates controller
//
- (void)initUpdates
{
	UpdatesController *updates = [[UpdatesController alloc] initUpdatesController];
	UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:updates];
	[self setUpdatesController:updates];
	[updates release];
	
	// change the bar tint colour
	[nav.navigationBar setTintColor:[UIColor colorWithRed:TTNavigationBarTintColorRed green:TTNavigationBarTintColorGreen blue:TTNavigationBarTintColorBlue alpha:0.8]];

	// change the tab bar item
	UITabBarItem *tab = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"title-updates", nil) image:[UIImage imageNamed:@"UpdatesTabIcon.png"] tag:11];
	[nav setTabBarItem:tab];
	[tab release];
	
	// save it
	[self setUpdatesNavigationController:nav];
	[nav release];
}

//
// Load the ticket retailer controller
//
- (void)initTicketRetailers
{
	TicketRetailerList *tickets = [[TicketRetailerList alloc] initTicketRetailerList];
	UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:tickets];
	[tickets release];
	
	// change the bar tint colour
	[nav.navigationBar setTintColor:[UIColor colorWithRed:TTNavigationBarTintColorRed green:TTNavigationBarTintColorGreen blue:TTNavigationBarTintColorBlue alpha:0.8]];
	
	// change the tab bar item
	UITabBarItem *tab = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"title-tickets", nil) image:[UIImage imageNamed:@"TicketTabIcon.png"] tag:12];
	[nav setTabBarItem:tab];
	[tab release];
	
	
	// save it
	[self setTicketsNavigationController:nav];
	[nav release];
}



- (void)presentWelcomeScreen
{
	// Create a temporary view controller
	UIViewController *tmpController = [[UIViewController alloc] initWithNibName:nil bundle:nil];
	[tmpController setTitle:NSLocalizedString(@"title-getting-started", nil)];
	
	// load the nib containing our help contents
	NSObject *obj = [[NSObject alloc] init];
	NSString *nibName = [[NSDate dateWithTimeIntervalSince1970:1259425800] timeIntervalSinceNow] < 0 ? @"Welcome2" : @"Welcome";
	NSArray *nibItems = [[NSBundle mainBundle] loadNibNamed:nibName owner:obj options:nil];
	UIView *viewContents;
	
	viewContents = [nibItems objectAtIndex:0];
	
	if (viewContents == nil)
	{
		[tmpController release];
		[obj release];
		return;
	}
	
	// if its a scroll view then resize the view
	if ([viewContents isKindOfClass:[UIScrollView class]])
	{
		UIScrollView *scrollView = (UIScrollView *)viewContents;
		CGRect scrollFrame = scrollView.frame;
		[scrollView setContentSize:scrollFrame.size];
		scrollFrame.size.height = 460;
		[scrollView setFrame:scrollFrame];
		[tmpController setView:scrollView];
	}
	else
	{
		[tmpController setView:viewContents];
	}
	
	[self.tabBar.moreNavigationController pushViewController:tmpController animated:NO];
	[tmpController release];
	[obj release];	
}

// Startup with a url
- (BOOL)startupWithURL:(NSURL *)url allowRestore:(BOOL)canRestore
{
	// invalid url?
	if (url == nil)
		return NO;
	
	// lets parse our URL
	NSString *query = [url query];
	if (query == nil)
		return NO;
	
	// split the URL based on the parameters
	NSDictionary *params = [self dictionaryFromQueryString:query];
	[params retain];
	
	// check for a date parameter, if we have that then its a selectively smart startup
	if ([params objectForKey:@"date"] != nil)
	{
		//NSLog(@"Found timestamped restore url: %@", params);
		NSDate *closeDate = [NSDate dateWithTimeIntervalSince1970:[[params objectForKey:@"date"] doubleValue]];
		//NSLog(@"Timestamp: %@", closeDate);
		if ([closeDate timeIntervalSinceNow] > -180)
		{
			//NSLog(@"Within restore timeframe");
			// yep, we're within the time needed to restore this url, but are we allowed?
			NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
			if (![defaults boolForKey:@"smartRestore"])
			{
				// no not allowed
				//NSLog(@"Not allowed to restore");
				[params release];
				return NO;
			}
		} else
		{
			// this URL is date specific and we're outside the time frame, don't load it
			//NSLog(@"Outside restore timeframe");
			[params release];
			return NO;
		}
	}
	
	// are we restoring from the background? we basically do nothing then! but only if we support multitasking!
	if ([[params objectForKey:@"restore"] boolValue] && canRestore && [[UIDevice currentDevice] respondsToSelector:@selector(isMultitaskingSupported)] && [[UIDevice currentDevice] isMultitaskingSupported])
    {
        [params release];
		return YES;
    }

	// do we have a valid trackerid?
	if ([params objectForKey:@"trackerid"] != nil)
	{
		NSNumber *trackerID = [NSNumber numberWithInteger:[[params objectForKey:@"trackerid"] integerValue]];
		if (trackerID != nil)
		{
			// we have a valid number, lets see if its a valid trackerid
			Stop *stop = [[StopList sharedManager] getStopForTrackerID:trackerID];
			if (stop != nil)
			{
				// do we have a filter?
				Filter *f = [[Filter alloc] init];
				if ([params objectForKey:@"route"] != nil)
				{
					Route *r = [[RouteList sharedManager] routeForRouteNumber:[params objectForKey:@"route"]];
					if (r != nil)
						[f setRoute:r];
				}
				if ([params objectForKey:@"lowfloor"] != nil)
					[f setLowFloor:([[params objectForKey:@"lowfloor"] isEqualToString:@"YES"])];
				
				// yep its valid, switch to the nearby tab and launch the PID
				[self.tabBar setSelectedViewController:nearbyController];

				// tell the list/map nearby view to show the PID
				NearbyViewController *nearby = (NearbyViewController *)[[nearbyController viewControllers] objectAtIndex:0];
				[nearby pushPIDForStop:stop withFilter:f animated:NO];
				[f release];
				[params release];
				return YES;
			}
		}
	}
	
	// do we have a valid tram number?
	if ([params objectForKey:@"tram"] != nil)
	{
		NSNumber *tramNumber = [NSNumber numberWithInteger:[[params objectForKey:@"tram"] integerValue]];
		if (tramNumber != nil)
		{
			// can't validate this, change to the tram tab, enter the number and let it check itself
			[self.onboardDisplaysController popToRootViewControllerAnimated:NO];
			[self.tabBar setSelectedViewController:self.onboardDisplaysController];
			EnterTramNumberController *t = (EnterTramNumberController *)[self.onboardDisplaysController topViewController];
			[t setStoredTramNumber:tramNumber];
			[params release];
			
			return YES;
		}
	}
	
	// do we have a favourite?
	if ([params objectForKey:@"favourite"] != nil)
	{
		NSArray *splitFavourite = [[params objectForKey:@"favourite"] componentsSeparatedByString:@":"];
		if (splitFavourite == nil || [splitFavourite count] != 2)
		{
			[params release];
			return NO;
		}

		// valid?
		NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[[splitFavourite objectAtIndex:1] integerValue] inSection:[[splitFavourite objectAtIndex:0] integerValue]];
		if (indexPath == nil)
		{
			[params release];
			return NO;
		}
		
		// make sure its not beyond the end of our array
		FavouriteStop *fav = [[StopList sharedManager] favouriteStopAtIndex:indexPath];
		if (fav != nil)
		{
			NSArray *favourites = [[StopList sharedManager] getFavouriteStopList];
			[self.tabBar setSelectedViewController:self.favouritesNavigationController];
			[favouritesList pushPIDWithFavourite:[favourites indexOfObject:fav]];
			[params release];
			
			return YES;
		}
	}
	
	[params release];
	return NO;
}

// make a NSDictionary from the query string
- (NSDictionary *)dictionaryFromQueryString:(NSString *)queryString
{
	// blanket check
	if (queryString == nil)
		return nil;

	// create our dictionary
	NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithCapacity:0];
	
	// split our query string by the param separator (&)
	for (NSString *paramString in [queryString componentsSeparatedByString:@"&"])
	{
		// split them again by the equals
		NSArray *paramComponents = [paramString componentsSeparatedByString:@"="];
		if ([paramComponents count] != 2)
			continue;

		[params setObject:[paramComponents objectAtIndex:1] forKey:[paramComponents objectAtIndex:0]];
	}
	
	// all done?
	return [params autorelease];
}

- (void)netServiceDidResolveAddress:(NSNetService *)sender
{
	// found a tram
	Tram *tram = [[Tram alloc] init];
	[tram setNumber:[NSNumber numberWithInteger:[sender port]]];
	//NSLog(@"Tram detected: %@", tram);
	
	// create the alert
	EnterTramNumberController *onboard = (EnterTramNumberController *)[[self.onboardDisplaysController viewControllers] objectAtIndex:0];
	[onboard detectedTram:tram];
	[tram release];
	
	[sender stop];
}

- (void)netService:(NSNetService *)sender didNotResolve:(NSDictionary *)errorDict
{
	//NSLog(@"Could not detect tram");
}

- (void)lookForTramsInRange
{
	//NSLog(@"Attempting tram detection...");
	NSNetService *ns = [[NSNetService alloc] initWithDomain:@"local" type:@"_tramtracker._tcp." name:@"tramTRACKER"];
	[ns setDelegate:self];
	[ns resolveWithTimeout:30];
	[self setTramDetector:ns];
	[ns release];
}



- (NSDate *)lastSyncDate
{
    // work around to a potential race issue. Make sure that we attempt to access the database
    // before grabbing the date. Otherwise potential upgrade operations may not have occured yet.
    NSPersistentStoreCoordinator *store = [[self persistentStoreCoordinator] retain];
    [store release];
    
	// Get the date from the user defaults
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSDate *date = [defaults objectForKey:@"lastSynchronisationNSDate"];

	return date;
}

- (NSDate *)lastTicketSyncDate
{
	NSString *file = [self ticketOutletsFile];
	NSFileManager *manager = [NSFileManager defaultManager];
	NSDictionary *fileAttributes = [manager attributesOfItemAtPath:file error:NULL];
	
	// return the last modified date
	if (!fileAttributes) return nil;
	return [fileAttributes objectForKey:NSFileModificationDate];
}

- (void)setLastSyncDate:(NSDate *)lastSyncDate
{
	// default to today's date and time
	if (lastSyncDate == nil)
		lastSyncDate = [NSDate date];

	// Set the date in the user defaults
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

	// set the date
	//NSLog(@"Setting last sync date: %@", lastSyncDate);
	[defaults setObject:lastSyncDate forKey:@"lastSynchronisationNSDate"];
	
	// save and go
	[defaults synchronize];
}

//
// Synchronise any data changes
//
- (void)synchroniseData
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSNumber *syncType = [defaults objectForKey:@"synchronisationType"];
    
    // do we already have a synchronisation or check in progress?
    if (synchroniser != nil)
    {
        // check to see if there is a sync already in progress
        if ([synchroniser synchronisationInProgress])
        {
            //NSLog(@"[DEL] Synchronisation already in progress, not running again.");
            return;
        }
        
        // a synchronisation has obviously finished, lets release the current one
        [synchroniser release];
        synchroniser = nil;
    }
	
	// get the last synchronisation date and make sure that we haven't already checked today
	/*NSDate *lastSyncDate = [self lastSyncDate];
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"yyyy-MM-dd"];
	NSDate *today = ([NSDate instancesRespondToSelector:@selector(dateByAddingTimeInterval:)] ? [[formatter dateFromString:[formatter stringFromDate:[NSDate date]]] dateByAddingTimeInterval:43200] : [[formatter dateFromString:[formatter stringFromDate:[NSDate date]]] addTimeInterval:43200]);
	[formatter release];
	
	// are they the same?
	//NSLog(@"Today: %@, Last Sync: %@", today, lastSyncDate);
	if (![today isEqualToDate:lastSyncDate])
	{*/
		/*if ([syncType integerValue] == TTSyncTypeAutomatic)
		{
			synchroniser = [[BackgroundSynchroniser alloc] initWithLastSynchronisationDate:[self lastSyncDate]
																			ticketFileDate:[self lastTicketSyncDate]
																persistentStoreCoordinator:[self persistentStoreCoordinator]];
			
			// create the data logger
			AutomaticSynchroniserDataLogger *logger = [[AutomaticSynchroniserDataLogger alloc] initAutomaticSynchroniserDataLogger];
			[synchroniser setDelegate:logger];
			[logger release];

			// perform background synchronisation
			[synchroniser performSelectorInBackground:@selector(synchroniseInBackgroundThread:) withObject:nil]; 
		} else */
        if ([syncType integerValue] == TTSyncTypeAutoCheckManualSync || [syncType integerValue] == TTSyncTypeAutomatic)
		{
			synchroniser = [[BackgroundSynchroniser alloc] initWithLastSynchronisationDate:[self lastSyncDate]
																			ticketFileDate:[self lastTicketSyncDate]
																persistentStoreCoordinator:[self persistentStoreCoordinator]];
			[synchroniser setDelegate:self.updatesController];
			[synchroniser performSelectorInBackground:@selector(checkForUpdatesInBackgroundThread:) withObject:nil]; 
		}
	//}
}

- (void)syncDidFailWithMessage:(NSString *)message
{
	// configure an alert view
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"updates-sync-alert-title", nil)
													message:message
												   delegate:nil
										  cancelButtonTitle:NSLocalizedString(@"updates-sync-alert-button", nil)
										  otherButtonTitles:nil];
	[alert show];
	[alert release];
	alert = nil;
}

//
// Depending on what the user has set for their preferences, we load that on the application
//
- (void)startupWithPreferencesAndAllowRestore:(BOOL)canRestore
{
	// get the preferences
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	// do we have a URL to restore from?
	NSString *urlToRestoreFrom = [defaults objectForKey:@"urlToRestore"];
	if (urlToRestoreFrom != nil)
	{
		// clear it out first
		[defaults removeObjectForKey:@"urlToRestore"];
		
		// now run it through the url launching mechanism
		if ([self startupWithURL:[NSURL URLWithString:urlToRestoreFrom] allowRestore:canRestore])
			return;

		// otherwise it has failed, continue with normal startup
	}
	
	
	// do we need to upgrade our favourites storage?
	StopList *stopList = [StopList sharedManager];
	
	// Is this the first load?
	BOOL isFirstLoad = ![defaults boolForKey:@"firstLoad"] && ![stopList hasFavouriteStopList];
	if (isFirstLoad)
	{
		[self.tabBar setSelectedViewController:aboutController];
		[self presentWelcomeScreen];

		[defaults setBool:YES forKey:@"firstLoad"];
		[defaults synchronize];
		return;
	}
	
	
	// find out what they've chosen at this point
	NSNumber *startupOption = [defaults objectForKey:@"startup"];

	// if its empty default to 1
	if (startupOption == nil) startupOption = [NSNumber numberWithInt:1];
	
	// lets pop stuff onto places depending on what they've chosen
	if ([startupOption isEqualToNumber:[NSNumber numberWithInt:1]])
	{
		// starting with nearest favourite, pop that onto the controller
		if ([[StopList sharedManager] hasFavouriteStopList])
		{
			[self.tabBar setSelectedViewController:favouritesNavigationController];
			[favouritesList pushPIDWithNearbyFavouritesAnimated:NO];
		} else
		{
			[self.tabBar setSelectedViewController:nearbyController];
			//[[self.nearbyController.viewControllers objectAtIndex:0] pushPIDWithNearbyAnimated:NO];
		}
	}

	// favourites list
	else if ([startupOption isEqualToNumber:[NSNumber numberWithInt:2]])
	{
		[self.tabBar setSelectedViewController:favouritesNavigationController];
	}

	// otherwise, just the nearest stop in general
	else if ([startupOption isEqualToNumber:[NSNumber numberWithInt:3]])
	{
		[self.tabBar setSelectedViewController:nearbyController];
	}
	
	// Nearby list, change tab appropriately
	else if ([startupOption isEqualToNumber:[NSNumber numberWithInt:4]])
	{
		[self.tabBar setSelectedViewController:nearbyController];
	}
	
	// Browse list
	else if ([startupOption isEqualToNumber:[NSNumber numberWithInt:5]])
	{
		[self.tabBar setSelectedViewController:browseNavigationController];
	} 
	
	// otherwise, do nothing (which is the favourites list)
}

// set last opened stop
- (void)setLastOpenedPID:(NSDictionary *)pidInfo
{
	// Save the last opened stop to the defaults
	//NSLog(@"Setting last opened PID: %@", pidInfo);
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	if (pidInfo == nil)
		[defaults removeObjectForKey:@"lastOpenedPID"];
	else
		[defaults setObject:pidInfo forKey:@"lastOpenedPID"];
	[defaults synchronize];
}

- (NSDictionary *)lastOpenedPID
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	return [defaults objectForKey:@"lastOpenedPID"];
}

#pragma mark -
#pragma mark UITabBarControllerDelegate methods

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{

	// we do this one the first two
	NSUInteger oldSelectedTabIndex = tabBarController.selectedIndex;
	NSUInteger newSelectedTabIndex = [tabBarController.viewControllers indexOfObject:viewController];

	// if its the same tab and its the first one - push the nearest favourite onto the stack
	if (oldSelectedTabIndex == newSelectedTabIndex && newSelectedTabIndex == 0 && [self.favouritesNavigationController.viewControllers count] == 1)
	{
		// only if we have favourite stops!
		if (![[StopList sharedManager] hasFavouriteStopList])
			return NO;

		// make sure that location services are enabled first
		//LocationManager *locManager = [[LocationManager alloc] init];
		if (![CLLocationManager locationServicesEnabled])
		{
		//	[locManager release];
			return NO;
		}
		//[locManager release];
		[self.favouritesList pushPIDWithNearbyFavouritesAnimated:YES];
		return NO;
	}

	// if its the same tab and its the second one - push the nearest stop onto the stack
	else if (oldSelectedTabIndex == newSelectedTabIndex && newSelectedTabIndex == 1 && [self.nearbyController.viewControllers count] == 1)
	{
		NearbyViewController *n = (NearbyViewController *)[self.nearbyController.viewControllers objectAtIndex:0];
		
		// make sure that we have stops in the list
		[n refreshLocation];
		return NO;
	}
	
	// if its the same tab and its the Enter ID view controller..
	else if ([viewController isEqual:self.onboardDisplaysController])
	{
		// do we have a last tap?
		if (lastTapTime == nil || [[NSDate date] timeIntervalSinceDate:lastTapTime] > 2)
		{
			[self setLastTapTime:[NSDate date]];
			return YES;
		}
		
		//NSLog(@"[APPDELEGATE] Double-tapped onboard tab");
        /**
         * ARRIVAL ALARM SUPPORT
         *
         * When double tapping the Onboard tab, this checks for an active arrival alarm and re-opens that tram into the
         * Onboard tab.
		if (self.backgroundLocationService != nil && [self.backgroundLocationService isActive])
		{
			//NSLog(@"[APPDELEGATE] Service is active, re-opening");
			[self setLastTapTime:nil];

			// are we already active and tracking this tram? don't do something silly
			if ([self.onboardDisplaysController.topViewController isKindOfClass:[OnboardViewController class]])
			{
				OnboardViewController *v = (OnboardViewController *)self.onboardDisplaysController.topViewController;
				if (v.journey != nil && [v.journey.tram isEqualToTram:self.backgroundLocationService.journey.tram])
					return NO;
			}
			
			// is it visible?
			if (![tabBarController.selectedViewController isEqual:self.onboardDisplaysController])
				[tabBarController setSelectedViewController:self.onboardDisplaysController];

			// pop it back to the root
			if (![self.onboardDisplaysController.topViewController isEqual:self.tramNumberController])
				[self.onboardDisplaysController popToRootViewControllerAnimated:YES];
			
			// set the stored tram number
			[self.tramNumberController setStoredTramNumber:self.backgroundLocationService.journey.tram.number];
			[self.tramNumberController locateTram];
			return NO;
		} **/

	}
	
	return YES;
}

- (void)tabBarController:(UITabBarController *)tabBarCtl willBeginCustomizingViewControllers:(NSArray *)viewControllers
{
	// we want to tint the nav bar to keep it consistent
    UIView *editView = [tabBarCtl.view.subviews objectAtIndex:1];
    UINavigationBar *editNavBar = [editView.subviews objectAtIndex:0];
	[editNavBar setTintColor:[UIColor colorWithRed:TTNavigationBarTintColorRed green:TTNavigationBarTintColorGreen blue:TTNavigationBarTintColorBlue alpha:0.8]];
}

- (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed
{
	// if the tabs have changed generate the order and save it
	if (changed)
	{
		NSMutableArray *tabKeys = [[NSMutableArray alloc] initWithCapacity:0];
		for (UIViewController *controller in viewControllers)
		{
			// find the key
			NSArray *keys = [tabStorage allKeysForObject:controller];
			if ([keys count] > 0)
				[tabKeys addObject:[keys objectAtIndex:0]];
		}
		
		// save that ordering to the defaults
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		[defaults setObject:tabKeys forKey:@"tabOrder"];
		[tabKeys release];
	}
}

#pragma mark -
#pragma mark Core Data stack

/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *) managedObjectContext {
	
    if (managedObjectContext != nil) {
        return managedObjectContext;
    }
	
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        if ([NSManagedObjectContext instancesRespondToSelector:@selector(initWithConcurrencyType:)])
            managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSConfinementConcurrencyType];
        else
            managedObjectContext = [[NSManagedObjectContext alloc] init];
        [managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
	//[managedObjectContext save:nil];
	
    return managedObjectContext;
}


/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created by merging all of the models found in the application bundle.
 */
- (NSManagedObjectModel *)managedObjectModel {
	
    if (managedObjectModel != nil) {
        return managedObjectModel;
    }
    managedObjectModel = [[NSManagedObjectModel mergedModelFromBundles:nil] retain];    
    return managedObjectModel;
}


/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
	
    if (persistentStoreCoordinator != nil) {
        return persistentStoreCoordinator;
    }
	
	// we need to copy this into the data directory, only if it hasn't already been done
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSString *docDir = [self applicationDocumentsDirectory];

	NSString *destinationFileName = [docDir stringByAppendingPathComponent:@"tramTRACKER.db"];
	NSString *sourceFileName = [[NSBundle mainBundle] pathForResource:@"tramTRACKER" ofType:@"db"];

	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	if (![fileManager fileExistsAtPath:destinationFileName])
	{
		//NSLog(@"Copying because it doesn't exist yet..");
		/**
         *
         * UNCOMMENT AND RUN THIS CODE WHENEVER YOU UPGRADE THE SCHEMA!
         * Make sure its run against a fresh install in the simulator.
         *
         * This will allow the CoreData database to automatically map itself into the 
         * new schema, but is a performance hit so don't leave this bit in production.
         *
        **/
		/*NSURL *source = [NSURL fileURLWithPath:sourceFileName];
		NSURL *dest = [NSURL fileURLWithPath:destinationFileName];
		NSError *error = nil;
		NSPersistentStoreCoordinator *c = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];

		NSDictionary *o = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
		[c addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:source options:o error:&error];
		//NSLog(@"%@", error);
		NSPersistentStore *sourceStore = [c persistentStoreForURL:source];
		NSPersistentStore *destStore = [c migratePersistentStore:sourceStore toURL:dest options:nil withType:NSSQLiteStoreType error:&error];
		[destStore release];*/

        
        // END UPGRADE CODE.
        
        [fileManager copyItemAtPath:sourceFileName toPath:destinationFileName error:NULL];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:TTDatabaseModificationDate];
		[self setLastSyncDate:date];

		[defaults setObject:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] forKey:@"databaseVersion"];

		//NSLog(@"Completed.");
	} else
	{
		// double check to make sure that the file we have isn't more recent
		NSDictionary *bundle = [[NSBundle mainBundle] infoDictionary];
        //NSLog(@"[DELEGATE] %@", [defaults objectForKey:@"databaseVersion"]);
		// has the database version changed??
		if ([defaults objectForKey:@"databaseVersion"] == nil || ![[defaults objectForKey:@"databaseVersion"] isEqualToString:[bundle objectForKey:@"CFBundleVersion"]])
		{
			//NSLog(@"Copying because version has changed..");
			[fileManager removeItemAtPath:destinationFileName error:NULL];
			[fileManager copyItemAtPath:sourceFileName toPath:destinationFileName error:NULL];

			NSDate *date = [NSDate dateWithTimeIntervalSince1970:TTDatabaseModificationDate];
			[self setLastSyncDate:date];
			[defaults setObject:[bundle objectForKey:@"CFBundleVersion"] forKey:@"databaseVersion"];
			//NSLog(@"Completed.");
		}
	}

	
	NSURL *storeUrl = [NSURL fileURLWithPath:sourceFileName];
	if ([fileManager fileExistsAtPath:destinationFileName])
		storeUrl = [NSURL fileURLWithPath:destinationFileName];
	
	NSError *error;
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: [self managedObjectModel]];
    if (![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:nil error:&error]) {
        // Handle error
    }   
	
    return persistentStoreCoordinator;
}

#pragma mark -
#pragma mark File Locations

/**
 Returns the path to the application's documents directory.
 */
- (NSString *)applicationDocumentsDirectory {
	
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

/**
 * Returns the NSString to the Ticket Outlet File
**/
- (NSString *)ticketOutletsFile
{
	// Get the "last modified" date
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:TTTicketsFileModicationDate];

	// we need to copy this into the data directory, only if it hasn't already been done
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSString *docDir = [self applicationDocumentsDirectory];
	
	NSString *destinationFileName = [docDir stringByAppendingPathComponent:@"TicketRetailers.plist"];
	NSString *sourceFileName = [[NSBundle mainBundle] pathForResource:@"TicketRetailers" ofType:@"plist"];
	
	if (![fileManager fileExistsAtPath:destinationFileName])
	{
		//NSLog(@"[DEL] Copying ticket retailers because it doesn't exist yet..");
		[fileManager copyItemAtPath:sourceFileName toPath:destinationFileName error:NULL];
		//NSLog(@"[DEL] Completed.");

		// Update the modification time on the new file from its source
		NSError *e = nil;
		[fileManager setAttributes:[NSDictionary dictionaryWithObject:date forKey:NSFileModificationDate]
					  ofItemAtPath:destinationFileName
							 error:&e];
		//NSLog(@"[DEL] Error copying: %@", e);
	} else
	{
		// check to see which is more recent
		NSDictionary *destinationAttributes = [fileManager attributesOfItemAtPath:destinationFileName error:NULL];
		
		// if the source modification time is more recent (greater than) the destination, copy it
		NSDate *destinationModDate = (NSDate *)[destinationAttributes objectForKey:NSFileModificationDate];
		if ([date compare:destinationModDate] == NSOrderedDescending)
		{
			//NSLog(@"[DEL] Copying ticket retailers because it is more recent..");
			[fileManager copyItemAtPath:sourceFileName toPath:destinationFileName error:NULL];
			//NSLog(@"[DEL] Completed.");

			// Update the modification time on the new file from its source
            NSError *e = nil;
			[fileManager setAttributes:[NSDictionary dictionaryWithObject:date forKey:NSFileModificationDate]
						  ofItemAtPath:destinationFileName
								 error:&e];
            //NSLog(@"[DEL] Error copying: %@", e);
		}
	}

		
	return [self.applicationDocumentsDirectory stringByAppendingPathComponent:@"TicketRetailers.plist"];
}


/**
 * ARRIVAL ALARM SUPPORT
 *
 * This could be re-used and changed to the push notification delegate instead of a local notif
 
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
	//NSLog(@"[DELEGATE] Did receive local notif!");
	// are we still active, if we're not then we've received an arrival alarm and are re-opening the onboard tab
	if (application.applicationState == UIApplicationStateInactive && ![self.tabBar.selectedViewController isEqual:self.onboardDisplaysController])
	{
		// start it up again
		NSString *url = [NSString stringWithFormat:@"tramtracker://?tram=%@", [notification.userInfo objectForKey:@"tramNumber"]];
		[self startupWithURL:[NSURL URLWithString:url] allowRestore:YES];
	}
}
 **/

#pragma mark -
#pragma mark Methods for handling changing application state

// Get the URL needed to restore the currently active session
- (NSString *)restoreURL
{
	NSString *url = nil;
	
	// find the currently open tab
	if (self.tabBar == nil)
		return nil;
	
	// make sure theres a visible view controller there
	UINavigationController *nav = (UINavigationController *)[self.tabBar selectedViewController];
	if (nav == nil)
		return nil;
	
	// so the view controller should actually be a navigation controller with another view controller on top
	UIViewController *top = [nav topViewController];
	if (top == nil)
		return nil;
	
	// is there a tram on top?
	if ([top isKindOfClass:[OnboardViewController class]])
	{
		// get the tram number
		OnboardViewController *o = (OnboardViewController *)top;
		Tram *t = o.journey.tram;
		
		if (t != nil)
			url = [NSString stringWithFormat:@"tramtracker://?tram=%@", t.number];

		
	// is it a PID on top?
	} else if ([top isKindOfClass:[PIDViewController class]])
	{
		PIDViewController *p = (PIDViewController *)top;
		Stop *s = p.currentStop;
		Filter *f = p.filter;
		//NSLog(@"Found PID for stop %@", s);
		
		if (s != nil)
		{
			// is there no filter? just save the stop
			if (f == nil || [f isEmpty])
			{
				url = [NSString stringWithFormat:@"tramtracker://?trackerid=%@", s.trackerID];
				
			// save the filter too
			} else
			{
				url = [NSString stringWithFormat:@"tramtracker://?trackerid=%@&route=%@&lowfloor=%@", s.trackerID, (f.route == nil ? @"0" : f.route.number), (f.lowFloor ? @"YES" : @"NO")];
			}
		}
		
		// is it the favourite PID?
	} else if ([top isKindOfClass:[PagedPIDViewController class]])
	{
		// grab the index of the open favourite
		PagedPIDViewController *p = (PagedPIDViewController *)top;
		url = [NSString stringWithFormat:@"tramtracker://?favourite=%d:%d", p.currentPID.favouriteStopIndexPath.section, p.currentPID.favouriteStopIndexPath.row];
	}
	
	return url;
}

// When the application starts to resign
- (void)applicationWillResignActive:(UIApplication *)application
{
	// find the currently open tab
	if (self.tabBar == nil)
		return;
	
	// make sure theres a visible view controller there
	UINavigationController *nav = (UINavigationController *)[self.tabBar selectedViewController];
	if (nav == nil)
		return;
	
	// so the view controller should actually be a navigation controller with another view controller on top
	UIViewController *top = [nav topViewController];
	if (top == nil)
		return;
	
	// yep, does this have a pause message?
	if ([top respondsToSelector:@selector(applicationWillResignActive)])
	{
		[top performSelector:@selector(applicationWillResignActive)];
	}
	
	// Get the URL needed to restore this session
	urlToRestore = [self restoreURL];
	if (urlToRestore != nil)
		[urlToRestore retain];
}

// When the application has come back
- (void)applicationDidBecomeActive:(UIApplication *)application
{
    //NSLog(@"[DEL] Application did become active");
	// clear any saved restoration url
	if (urlToRestore != nil)
	{
		[urlToRestore release];
		urlToRestore = nil;
	}
	

	// find the currently open tab
	if (self.tabBar == nil)
		return;
	
	// make sure theres a visible view controller there
	UINavigationController *nav = (UINavigationController *)[self.tabBar selectedViewController];
	if (nav == nil)
		return;
	
	// so the view controller should actually be a navigation controller with another view controller on top
	UIViewController *top = [nav topViewController];
	if (top == nil)
		return;
	
	// yep, does this have an unpause message?
	if ([top respondsToSelector:@selector(applicationDidBecomeActive)])
	{
		[top performSelector:@selector(applicationDidBecomeActive)];
	}

	// check for updates
	[self synchroniseData];
	
	// look for trams in range
	[self lookForTramsInRange];
}


- (void)applicationWillTerminate:(UIApplication *)application
{
	//NSLog(@"Will Terminate");
	
    if (managedObjectContext != nil && [managedObjectContext hasChanges]) {
		[managedObjectContext rollback];
    }

	// save the URL - taking a phone call
	if (urlToRestore != nil)
	{
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		[defaults setObject:urlToRestore forKey:@"urlToRestore"];
		[defaults synchronize];
		
		[urlToRestore release];
		urlToRestore = nil;
		
	} else
	{
		// If we have a smart restore option selected, save the necessary items
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		BOOL smartRestore = [defaults boolForKey:@"smartRestore"];
		if (smartRestore)
		{
			NSString *url = [self restoreURL];
			
			// append the current time to the URL
			url = [url stringByAppendingFormat:@"&date=%f", [[NSDate date] timeIntervalSince1970]];
			
			// save it
			if (url != nil)
			{
				//NSLog(@"Setting URL to restore: %@", url);
				NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
				[defaults setObject:url forKey:@"urlToRestore"];
				[defaults synchronize];
			}
		}
	}	
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	// find the currently open tab
	if (self.tabBar == nil)
		return;
	
	// make sure theres a visible view controller there
	UINavigationController *nav = (UINavigationController *)[self.tabBar selectedViewController];
	if (nav == nil)
		return;
	
	// so the view controller should actually be a navigation controller with another view controller on top
	UIViewController *top = [nav topViewController];
	if (top == nil)
		return;
	
	// yep, does this have a pause message?
	if ([top respondsToSelector:@selector(applicationDidEnterBackground)])
		[top performSelector:@selector(applicationDidEnterBackground)];
	
	// if we have a tracking service configured, lets get it started!
	//if (([[UIDevice currentDevice] respondsToSelector:@selector(isMultitaskingSupported)]) && [[UIDevice currentDevice] isMultitaskingSupported] && backgroundLocationService != nil & backgroundLocationService.destination != nil)
	//	[backgroundLocationService startTracking];
	
	// If we have a smart restore option selected, save the necessary items
	if ([[UIDevice currentDevice] respondsToSelector:@selector(isMultitaskingSupported)] && [[UIDevice currentDevice] isMultitaskingSupported])
	{
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		BOOL smartRestore = [defaults boolForKey:@"smartRestore"];
		NSString *currentRestoreURL = [defaults objectForKey:@"urlToRestore"];
		if (smartRestore && currentRestoreURL == nil)
		{
			currentRestoreURL = [self restoreURL];
			NSString *url = [currentRestoreURL stringByAppendingString:@"&restore=1"];
			
			// append the current time to the URL
			url = [url stringByAppendingFormat:@"&date=%f", [[NSDate date] timeIntervalSince1970]];
			
			// save it
			if (url != nil)
			{
				//NSLog(@"Setting URL to restore: %@", url);
				NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
				[defaults setObject:url forKey:@"urlToRestore"];
				[defaults synchronize];
			}
		}
	}
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
	// find the currently open tab
	if (self.tabBar == nil)
		return;
	
	// make sure theres a visible view controller there
	UINavigationController *nav = (UINavigationController *)[self.tabBar selectedViewController];
	if (nav == nil)
		return;
	
	// so the view controller should actually be a navigation controller with another view controller on top
	UIViewController *top = [nav topViewController];
	if (top == nil)
		return;
	
	// yep, does this have a pause message?
	if ([top respondsToSelector:@selector(applicationWillEnterForeground)])
	{
		[top performSelector:@selector(applicationWillEnterForeground)];
	}


	// run the normal startup
	//[self startupWithPreferencesAndAllowRestore:YES];
	
	// two things we need to do even if re-entering.
}
	

//
// When the application starts to run low on memory we can try freeing some things
//
- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
	//NSLog(@"Application did receive memory warning! Uhohs!");
	// try freeing up the stop list
	//[[StopList sharedManager] clearStopList];
	
	// and the route list
	//[[RouteList sharedManager] clearRouteList];
}


- (void)dealloc {
    [window release];
	[favouritesNavigationController release];
	[favouritesList release];
	[tabBar release];
	[browseNavigationController release];
	[searchController release];
	[nearbyController release];
	[enterIDController release];
	[recentController release];
	[settingsController release];
	[aboutController release];
	[tramDetector release];
	[tabStorage release];
	[defaultTabOrder release];
	[synchroniser release];
	[tramNumberController release];

	[managedObjectContext release];
    [managedObjectModel release];
    [persistentStoreCoordinator release];
	[lastTapTime release]; lastTapTime = nil;
	
	[super dealloc];
}

@end
