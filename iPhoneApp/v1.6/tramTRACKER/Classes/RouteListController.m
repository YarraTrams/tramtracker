//
//  RouteListController.m
//  tramTRACKER
//
//  Created by Robert Amos on 27/02/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "RouteListController.h"


@implementation RouteListController

@synthesize routeList, routeNumbers, stopList, loadingIndicator, pushToOnboardScreen, onboardList, pushToScheduledDeparturesScreen;



- (void)viewDidLoad {
    [super viewDidLoad];

	// change the background colour of the table
	[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
	[self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];

	if (!self.routeList)
		[self setRouteList:[[RouteList sharedManager] getRouteList]];
	
	// empty footer so we don't show non-blank rows
	[self.tableView setTableFooterView:[self tableFooterView]];
	
    // register for route list updates
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshRoutes:) name:TTStopsAndRoutesHaveBeenUpdatedNotice object:nil];
}

- (void)refreshRoutes:(NSNotification *)note
{
	[self setRouteList:nil];
	[self setRouteList:[[RouteList sharedManager] getRouteList]];
	[self.tableView reloadData];
}

//
// Create the table footer
//
- (UIView *)tableFooterView
{
	CGRect frame = CGRectMake(0, 0, self.tableView.frame.size.width, 10);
	UnderBackgroundView *ub = [[UnderBackgroundView alloc] initWithFrame:frame];
	return [ub autorelease];
}



- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}

#pragma mark Table view methods

//
// Return the number of sections in the table view, one section per route
//
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

	// no routes yet?
	if (!self.routeList)
		return 1;

	// base it on the number of Routes
	return [self.routeList count];
}

//
// Custom Headers
//
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	// no route list yet?
	if (!self.routeList)
	{
		return [[[UIView alloc] initWithFrame:CGRectZero] autorelease];
	}
	
	// grab a OnboardListSection thingy
	OnboardListSectionHeaderView *view = [[OnboardListSectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 22)];
	
	// set the title
	[view.title setText:[self tableView:tableView titleForHeaderInSection:section]];
	[view.title setLineBreakMode:UILineBreakModeMiddleTruncation];
	
	return [view autorelease];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
	Route *route = [self.routeList objectAtIndex:section];
	if ([route.number isEqualToString:@"3"] || [route.number isEqualToString:@"3a"])
	{
		UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
		[label setText:[self tableView:tableView titleForFooterInSection:section]];
		[label setFont:[UIFont systemFontOfSize:14]];
		[label setTextAlignment:UITextAlignmentCenter];
		[label setLineBreakMode:UILineBreakModeTailTruncation];
		[label setBackgroundColor:[UIColor whiteColor]];
		[label setTextColor:[UIColor blackColor]];
		return [label autorelease];
	}
	return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (!self.routeList)
        return 0;
    return 22;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	Route *route = [self.routeList objectAtIndex:section];
	if ([route.number isEqualToString:@"3"] || [route.number isEqualToString:@"3a"])
        return 18;
    return 0;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	Route *route = [self.routeList objectAtIndex:section];
	return [NSString stringWithFormat:@"%@ - %@", route.number, route.name];
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
	Route *route = [self.routeList objectAtIndex:section];

	if ([route.number isEqualToString:@"3"])
		return NSLocalizedString(@"browse-footers-3", "Route 3 message");
	else if ([route.number isEqualToString:@"3a"])
		return NSLocalizedString(@"browse-footers-3a", "Route 3a message");
	return nil;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	// no routes yet?
	if (!self.routeList)
		return 0;

	// check for number of directions
	NSInteger directions = 0;
	Route *r = [self.routeList objectAtIndex:section];
	if (r.upDestination != nil && ![r.upDestination isEqualToString:@""]) directions++;
	if (r.downDestination != nil && ![r.downDestination isEqualToString:@""]) directions++;
    return directions;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    SimpleCell *cell = (SimpleCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[SimpleCell alloc] initWithCellStyle:TTSimpleCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		
		// set disclosure
		[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];

		GradientBackgroundCell *background = [[GradientBackgroundCell alloc] initWithFrame:CGRectMake(0, 0, 320, 44) cellStyle:OnboardCellStyleNone];
		[cell setBackgroundView:background];
		[background release];
	}
    
	// Simple text cell
	Route *r = [self.routeList objectAtIndex:indexPath.section];
	if (indexPath.row == 0)
	{
		// First row is usually up, unless up is not set (ie it only has one direction)
		[cell.textLabel setText:[NSString stringWithFormat:NSLocalizedString(@"browse-direction-template", "To..."), (r.upDestination != nil && ![r.upDestination isEqualToString:@""] ? r.upDestination : r.downDestination)]];
			
	} else if (indexPath.row == 1)
		[cell.textLabel setText:[NSString stringWithFormat:NSLocalizedString(@"browse-direction-template", "To..."), r.downDestination]];
	
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

	// Push to a standard Stop List
	StopListController *stopListCont = [[StopListController alloc] initWithStyle:UITableViewStylePlain];
	

	// set the title
	Route *r = [self.routeList objectAtIndex:indexPath.section];
	BOOL isUpDirection = (indexPath.row == 0 && r.upDestination != nil && ![r.upDestination isEqualToString:@""]);

	[stopListCont setTitle:[NSString stringWithFormat:NSLocalizedString(@"browse-stoplist-title", "<Num>: To..."), r.number, isUpDirection ? r.upDestination : r.downDestination]];
	[stopListCont setRoute:r];

	// set the stops
	[stopListCont setStopList:[[StopList sharedManager] getStopListForTrackerIDs:(isUpDirection ? r.upStops : r.downStops)]];
	
	// pushing to a scheduled departure list
	[stopListCont setPushToScheduledDeparturesScreen:self.pushToScheduledDeparturesScreen];

	//[self setStopList:stopListCont];
	[self.navigationController pushViewController:stopListCont animated:YES];
	
	// release it, and the pids service we're holding on to
	[stopListCont release];
}


//
// Set the route list and push it onto the view
//
- (void)setRouteList:(NSArray *)routes
{
	[routes retain];
	[routeList release];
	
	routeList = routes;
	
	[self.tableView reloadData];

	// do we have a loading indicator? get rid of it if we do
	if ([self isLoadingVisible])
		[self hideLoading];
}


#pragma mark Loading methods

//
// Show the loading view
//
- (void)showLoading
{
	// create a nice semi-transparent overlay
	UIView *loading = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
	[loading setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.2]];
	
	
	// now we need to place a nice centered Loading indicator
	UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(110, 173.5, 20, 20)];
	[activity setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
	[activity startAnimating];
	[loading addSubview:activity];
	[activity release];
	
	// add the loading text
	UILabel *loadingText = [[UILabel alloc] initWithFrame:CGRectMake(135, 173.5, 70, 20)];
	[loadingText setBackgroundColor:[UIColor clearColor]];
	[loadingText setTextColor:[UIColor grayColor]];
	[loadingText setText:@"Loading..."];
	[loadingText setFont:[UIFont boldSystemFontOfSize:14]];
	[loading addSubview:loadingText];
	[loadingText release];
	
	// add it to the current view
	[self.view addSubview:loading];

	// save it so we can find it later
	[self setLoadingIndicator:loading];
	[loading release];
}

//
// Hide the loading view
//
- (void)hideLoading
{
	if ([self isLoadingVisible])
	{
		[self.loadingIndicator removeFromSuperview];
		[self setLoadingIndicator:nil];
	}
}

//
// Is the loading view visible
//
- (BOOL)isLoadingVisible
{
	return self.loadingIndicator != nil && ![self.loadingIndicator isHidden];
}


- (void)dealloc {
	
	// release our properties
	[routeList release];
	[routeNumbers release];
	[loadingIndicator release];
	
    [super dealloc];
}


@end

