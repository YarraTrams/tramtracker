//
//  Prediction.m
//  tramTRACKER
//
//  Created by Robert Amos on 10/02/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "Prediction.h"


@implementation Prediction

@synthesize routeNo;
@synthesize destination;
@synthesize headboardRouteNumber;
@synthesize lowFloor;
@synthesize airConditioned;
@synthesize displayAirConditioning;
@synthesize predictedArrivalDateTime;
@synthesize requestDateTime;
@synthesize specialEventMessage;
@synthesize disrupted;
@synthesize tramTrackerAvailable;
@synthesize distance;
@synthesize hasSpecialEvent;
@synthesize stop;
@synthesize tram;
@synthesize tripID;

+ (Prediction *)predictionForStub:(PredictionStub *)stub
{
    Prediction *prediction = [stub copy];

    // Look up the stop
    [prediction setStop:[Stop stopForStub:stub.stopStub]];

    // Look up the route
    [prediction setTram:[Tram tramForStub:stub.tramStub]];

    // return it
    return [prediction autorelease];
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"Prediction: Arrival at %@ at %@ for%@ Tram on Route %@ (%@)%@%@", stop, predictedArrivalDateTime, lowFloor ? @" Low Floor" : @"", headboardRouteNumber, routeNo, disrupted ? @" - Disrupted" : @"", specialEventMessage ? specialEventMessage : @""];
}

- (NSString *)voiceOver
{
	return [NSString stringWithFormat:@"%@ tram on route %@ going to %@ is %@ due to arrive ",
			([self lowFloor] ? @"Low Floor" : ([self displayAirConditioning] ? @"Air Conditioned" : @"")),
			[self headboardRouteNumber],
			[self destination],
			([self disrupted] ? @"affected by a disruption and is" : ([self specialEventMessage] != nil && [self.specialEventMessage length] > 0 ? @"affected by a special event and is" : @""))];
}

- (NSString *)formattedPredictedArrivalDateTime
{
	return [Prediction formattedPredictedArrivalDateTime:self.predictedArrivalDateTime];
}
+ (NSString *)formattedPredictedArrivalDateTime:(NSDate *)arrival
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	//[formatter setDateFormat:@"h:mm a"]; -- use built in style to allow for use of 24 hour time setting
	[formatter setTimeStyle:NSDateFormatterShortStyle];
	[formatter setDateStyle:NSDateFormatterNoStyle];
	NSString *formattedArrivalTime = [formatter stringFromDate:arrival];
	[formatter release];
	return formattedArrivalTime;
}
+ (NSString *)voiceOverPredictedArrivalDateTime:(NSDate *)arrival
{
	return [NSString stringWithFormat:@"at %@", [self formattedPredictedArrivalDateTime:arrival]];
}

// custom setter for destination to enforce line breaks in long destinations
- (void)setDestination:(NSString *)dest
{
	[dest retain];
	[destination release];
	
	NSString *newDestination = [dest stringByReplacingOccurrencesOfString:@"  " withString:@"\n"];
	[newDestination retain];
	[dest release];
	destination = newDestination;
}

//
// Route description when departing a particular stop
//
- (NSString *)formattedRouteDescriptionFromStop:(Stop *)aStop
{
	if ([[aStop cityDirection] rangeOfString:@"towards City"].location != NSNotFound)
		return [NSString stringWithFormat:NSLocalizedString(@"prediction-route-viacity", @"Route x via city"), self.routeNo];
	return [NSString stringWithFormat:@"Route %@", self.routeNo];
}

//
// Compare this prediction against another
//
- (NSComparisonResult)comparePredictionByArrivalTime:(Prediction *)otherPrediction
{
	return [self.predictedArrivalDateTime compare:otherPrediction.predictedArrivalDateTime];
}

- (NSString *)minutesUntilArrivalTime
{
	return [Prediction minutesUntilArrivalTime:self.predictedArrivalDateTime];
}
//
// Calculate the number of minutes until the arrival time
// 
+ (NSString *)minutesUntilArrivalTime:(NSDate *)arrivalTime
{
	NSTimeInterval secondsUntilArrivalTime = [arrivalTime timeIntervalSinceDate:[NSDate date]];
	
	// work out the number of minutes to go
	int minutes = floor(secondsUntilArrivalTime / 60);
	
	// if we're less than a minute (but not more than a minute past) return now
	if (minutes >= -1 && minutes < 1)
		return NSLocalizedString(@"pid-now", @"now");
	
	// or if less than 60 minutes from now return the number
	else if (minutes > 0 && minutes < 61)
		return [NSString stringWithFormat:@"%d", minutes];
	
	// have we gone more than 60 seconds past this one?
	else if (minutes < -1)
		return NSLocalizedString(@"pid-tramnotarriving", @"--");
	
	// more than 60 seconds mean we switch over to using the exact arrival time
	else
	{
		// find the date
		NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
		
		// use the current calendar to match days
		NSCalendar *cal = [NSCalendar currentCalendar];
		
		NSDate *today = [NSDate date];
		NSDateComponents *todayComponents = [cal components:(NSDayCalendarUnit | NSWeekdayCalendarUnit | NSYearCalendarUnit) fromDate:today];
		NSDateComponents *arrivalComponents = [cal components:(NSDayCalendarUnit | NSWeekdayCalendarUnit | NSYearCalendarUnit) fromDate:arrivalTime];
		
		// if its in the year 9999 then it means there is an error, display dashes
		if ([arrivalComponents year] >= 9999)
		{
			[formatter release];
			return NSLocalizedString(@"pid-tramnotarriving", @"--");
		}
		
		// check the weekday, if its arriving on a different day then we add the day name.
		if ([todayComponents weekday] != [arrivalComponents weekday])
			[formatter setDateFormat:@"ccc H:mm"];
		else
		{
			//[formatter setDateFormat:@"h:mm a"]; -- use built in style to allow for use of 24 hour time setting
			[formatter setTimeStyle:NSDateFormatterShortStyle];
			[formatter setDateStyle:NSDateFormatterNoStyle];
		}
		
		NSString *formattedArrivalTime = [formatter stringFromDate:arrivalTime];
		[formatter release];
		return formattedArrivalTime;
	}
}

- (id)copyWithZone:(NSZone *)zone
{
    Prediction *copy = [[Prediction alloc] init];
    [copy setDestination:[[self.destination copy] autorelease]];
    [copy setRouteNo:[[self.routeNo copy] autorelease]];
    [copy setHeadboardRouteNumber:[[self.headboardRouteNumber copy] autorelease]];
    [copy setTram:[[self.tram copy] autorelease]];
    [copy setLowFloor:self.lowFloor];
    [copy setAirConditioned:self.airConditioned];
    [copy setDisplayAirConditioning:self.displayAirConditioning];
    [copy setPredictedArrivalDateTime:[[self.predictedArrivalDateTime copy] autorelease]];
    [copy setRequestDateTime:[[self.requestDateTime copy] autorelease]];
    [copy setSpecialEventMessage:[[self.specialEventMessage copy] autorelease]];
    [copy setTripID:[[self.tripID copy] autorelease]];
    [copy setDisrupted:self.disrupted];
    [copy setTramTrackerAvailable:self.tramTrackerAvailable];
    [copy setDistance:self.distance];
    [copy setHasSpecialEvent:self.hasSpecialEvent];
    [copy setStop:[[self.stop copy] autorelease]];
    return copy;
}


// dealloc
- (void)dealloc
{
	[destination release];
	[routeNo release];
	[headboardRouteNumber release];
	[predictedArrivalDateTime release];
	[requestDateTime release];
	[specialEventMessage release];
	[tripID release];

	[super dealloc];
}

@end


@implementation PredictionStub

@synthesize tramStub, stopStub;

- (void)dealloc
{
    [tramStub release]; tramStub = nil;
    [stopStub release]; stopStub = nil;
    [super dealloc];
}

@end

