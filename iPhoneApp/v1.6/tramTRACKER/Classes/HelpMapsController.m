//
//  HelpMapsController.m
//  tramTRACKER
//
//  Created by Robert Amos on 13/05/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "HelpMapsController.h"

const NSString *HelpURLSubdirMaps = @"maps";

#define HELP_MAPS_SWITCHING		0
#define HELP_MAPS_USING			1
#define HELP_MAPS_DIRECTIONS	2
#define HELP_MAPS_PANNABLE		3
#define HELP_MAPS_TICKETOUTLETS	4

@implementation HelpMapsController

- (id)initHelpMapsController
{
	if (self = [super initWithStyle:UITableViewStylePlain])
	{
		[self setTitle:NSLocalizedString(@"help-maps", @"Maps")];
		[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
	}
	return self;
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    SimpleCell *cell = (SimpleCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[SimpleCell alloc] initWithCellStyle:TTSimpleCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
		
		GradientBackgroundCell *background = [[GradientBackgroundCell alloc] initWithFrame:CGRectMake(0, 0, 320, 44) cellStyle:OnboardCellStyleNone];
		[cell setBackgroundView:background];
		[background release];
	}
    
	switch (indexPath.row)
	{
		case HELP_MAPS_SWITCHING:
			[cell setTextLabelText:NSLocalizedString(@"help-maps-switching", @"Switching to Maps")];
			break;
		case HELP_MAPS_USING:
			[cell setTextLabelText:NSLocalizedString(@"help-maps-using", @"Using Maps")];
			break;
		case HELP_MAPS_DIRECTIONS:
			[cell setTextLabelText:NSLocalizedString(@"help-maps-directions", @"Finding Directions")];
			break;
		case HELP_MAPS_PANNABLE:
			[cell setTextLabelText:NSLocalizedString(@"help-maps-pannable", @"Pannable Maps")];
			break;
		case HELP_MAPS_TICKETOUTLETS:
			[cell setTextLabelText:NSLocalizedString(@"help-maps-ticketoutlets", @"Ticket Outlets")];
			break;
	}
	
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	// we create a UIWebView that we push onto the stack, start by working out our URL
	NSString *fileName = nil;
	
	switch (indexPath.row)
	{
		case HELP_MAPS_SWITCHING:
			fileName = @"switching14.html";
			break;
			
		case HELP_MAPS_USING:
			fileName = @"using15.html";
			break;
			
		case HELP_MAPS_DIRECTIONS:
			fileName = @"directions.html";
			break;
			
		case HELP_MAPS_PANNABLE:
			fileName = @"pannable14.html";
			break;
			
		case HELP_MAPS_TICKETOUTLETS:
			fileName = @"ticketoutlets15.html";
			break;
	}
	
	// nothing found?
	if (fileName == nil)
		return;
	
	// create the web view
	HelpWebController *helpWeb = [[HelpWebController alloc] initWithHelpFile:[NSString stringWithFormat:@"%@/%@", HelpURLSubdirMaps, fileName]];
	SimpleCell *cell = (SimpleCell *)[self.tableView cellForRowAtIndexPath:indexPath];
	[helpWeb setTitle:cell.textLabel.text];
	[self.navigationController pushViewController:helpWeb animated:YES];
	[helpWeb release];	
}


- (void)dealloc {
    [super dealloc];
}


@end

