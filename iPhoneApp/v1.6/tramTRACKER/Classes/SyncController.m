//
//  SyncController.m
//  tramTRACKER
//
//  Created by Robert Amos on 3/08/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "SyncController.h"
#import "BackgroundSynchroniser.h"
#import "HistoricalUpdate.h"

@implementation SyncController

@synthesize progressCounter, progressView, textView, parentUpdatesController;

- (id)initWithNumberOfStops:(NSNumber *)aStopCount numberOfRoutes:(NSNumber *)aRouteCount shouldUpdateTicketOutlets:(BOOL)updateTicketOutlets updateCache:(NSArray *)updates
{
	if (self = [super initWithStyle:UITableViewStyleGrouped])
	{
		NSInteger stops = (aStopCount != nil ? [aStopCount integerValue] : 0);
		NSInteger routes = (aRouteCount != nil ? [aRouteCount integerValue] : 0);
		
		NSInteger u = (stops + routes);
		if (updateTicketOutlets)
			u++;

		totalUpdates = [[NSNumber alloc] initWithInteger:u];
		completedUpdates = [[NSNumber alloc] initWithInteger:0];
		detectedOperations = 0;
		
		isSyncing = NO;
		isFinished = NO;
		isSaving = NO;
        wasAborted = NO;
		
		// set the title
		[self setTitle:NSLocalizedString(@"title-syncing", nil)];
		
		// set the background colour
		[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
		
		// set the table footer view
		[self.tableView setTableFooterView:[self tableFooterView]];
		[self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
		
		tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
		syncManager = [[BackgroundSynchroniser alloc] initWithLastSynchronisationDate:[d lastSyncDate] ticketFileDate:[d lastTicketSyncDate] persistentStoreCoordinator:[d persistentStoreCoordinator]];
		[syncManager setDelegate:self];
		
		stopChanges = [[NSMutableDictionary alloc] initWithCapacity:0];
		routeChanges = [[NSMutableDictionary alloc] initWithCapacity:0];
		
		params = [[NSDictionary dictionaryWithObjectsAndKeys:updates, TTSyncKeyUpdates, [NSNumber numberWithBool:updateTicketOutlets], TTSyncKeyTickets, nil] retain];
	}
	return self;
}

- (void)viewWillAppear:(BOOL)animated
{
	// start the synchronisation
	[syncManager performSelectorInBackground:@selector(synchroniseInBackgroundThread:) withObject:params];
}

- (UIView *)tableFooterView
{
	// our footer view is just a button
	UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
	UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
	
	// button label depends on how far into it we are
	if (isFinished)
	{
		[button setTitle:NSLocalizedString(@"updates-close", nil) forState:UIControlStateNormal];
		[button addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
		[button setBackgroundImage:[UIImage imageNamed:@"GreenButton.png"] forState:UIControlStateNormal];
	} else 
	{
		[button setTitle:NSLocalizedString(@"updates-cancel", nil) forState:UIControlStateNormal];
		[button addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
		[button setBackgroundImage:[UIImage imageNamed:@"RedButton.png"] forState:UIControlStateNormal];
	}
	
	[button.titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
	[button.titleLabel setShadowOffset:CGSizeMake(0, -1.0)];
	[button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[button setTitleShadowColor:[UIColor blackColor] forState:UIControlStateNormal];
	[button setFrame:CGRectMake(10, 10, 300, 44)];
	[footer addSubview:button];
	return [footer autorelease];
}

- (UIView *)tableHeaderView
{
	if (errorMessage == nil)
		return nil;

	// find the size of the message
	CGSize sizeOfMessage = [errorMessage sizeWithFont:[UIFont systemFontOfSize:16] constrainedToSize:CGSizeMake(300, 400) lineBreakMode:UILineBreakModeWordWrap];
	
	// create the header and its label
	UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, sizeOfMessage.height+20)];
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, sizeOfMessage.height)];
	[label setText:errorMessage];
	[label setBackgroundColor:[UIColor clearColor]];
	[label setOpaque:NO];
	[label setTextColor:[UIColor blackColor]];
	[label setFont:[UIFont systemFontOfSize:16]];
	[label setNumberOfLines:0];
	[label setLineBreakMode:UILineBreakModeWordWrap];
	[header addSubview:label];
	[label release];

	return [header autorelease];
}

- (void)cancel
{
	if (syncManager != nil)
		[syncManager cancel];
}

- (void)close
{
    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
}

- (BOOL)isActivityInProgress
{
	return isSyncing;
}

- (void)showLoadingIndicator
{
	if ([self isActivityInProgress])
	{
		// set a loading thing
		UIActivityIndicatorView *loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
		[loading startAnimating];
		UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithCustomView:loading];
		[loading release];
		[self.navigationItem setRightBarButtonItem:button animated:YES];
		[button release];
	} else
	{
		[self.navigationItem setRightBarButtonItem:nil animated:YES];
	}
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (void)updateProgress
{
	[self.tableView reloadData];
}

- (void)resetTotalUpdates:(NSNumber *)newNumber
{
	totalUpdates = [newNumber retain];
	[self updateProgress];
}

#pragma mark -
#pragma mark Notifications from the Background Synchroniser

- (void)syncDidStart
{
	isSyncing = YES;
	isFinished = NO;
	isSaving = NO;
	[self showLoadingIndicator];
	
	// grab the historical updates list so we can append to it
	NSArray *list = [HistoricalUpdate getHistoricalUpdatesList];
	updateList = [list mutableCopy];
}

- (void)syncDidFinishAtTime:(NSDate *)finishTime
{
	isSyncing = NO;
	isFinished = YES;
	isSaving = NO;
	[self showLoadingIndicator];
	[self updateProgress];
	
	// alter the cancel button to a close button.
	[self.tableView setTableFooterView:[self tableFooterView]];

	// Reset the title of the view
	[self setTitle:NSLocalizedString(@"updates-modal-complete", nil)];
	
	// tell our parent
	if (self.parentUpdatesController != nil)
		[self.parentUpdatesController syncDidFinishAtTime:finishTime];

	// save changes to the history
	NSArray *copyOfUpdateList = [updateList copy];
	[HistoricalUpdate saveHistoricalUpdatesList:copyOfUpdateList];
	[updateList release];
	updateList = nil;
	[copyOfUpdateList release];
	
	// tell the app delegate to save this as our last synchronisation time
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	[d setLastSyncDate:finishTime];
}

- (void)syncDidStartSaving
{
	//NSLog(@"Starting saving");
	isSaving = YES;
	[self updateProgress];
}

- (void)syncWasCancelled
{
	isSyncing = NO;
	isFinished = YES;
	isSaving = NO;
	[self showLoadingIndicator];
	[self updateProgress];
	
	// Alter the cancel button to a close button
	[self.tableView setTableFooterView:[self tableFooterView]];
	[self.tableView setTableHeaderView:[self tableHeaderView]];

	// Reset the title of the view
	[self setTitle:NSLocalizedString(@"updates-modal-cancel", nil)];
	
	// clear changes to the history
	[updateList release];
}

- (void)syncWasAbortedDueToMemoryWarning
{
	isSyncing = NO;
	isFinished = YES;
	isSaving = NO;
    wasAborted = YES;
	[self showLoadingIndicator];
	[self updateProgress];
	
	// Alter the cancel button to a close button
    errorMessage = [[NSString stringWithFormat:NSLocalizedString(@"updates-message-memoryabort", nil), [[UIDevice currentDevice] name]] retain];
	[self.tableView setTableFooterView:[self tableFooterView]];
	[self.tableView setTableHeaderView:[self tableHeaderView]];
    
	// Reset the title of the view
	[self setTitle:NSLocalizedString(@"updates-modal-abort", nil)];
    [self.tableView reloadData];
	
	// clear changes to the history
	[updateList release];
}

- (void)syncDidStartStop:(Stop *)stop
{
	// store some information about the stop for later comparison
	NSString *name = stop.name != nil ? stop.name : @"";
	NSNumber *isPlatformStop = [NSNumber numberWithBool:[stop isPlatformStop]];
	NSDictionary *stopInfo = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:name, isPlatformStop, nil]
														 forKeys:[NSArray arrayWithObjects:@"name", @"platform", nil]];
	NSNumber *trackerID = [NSNumber numberWithInteger:[stop.trackerID integerValue]];
	[stopChanges setObject:stopInfo forKey:trackerID];
}

- (void)syncDidFinishStop:(Stop *)stop
{
	// finished that one? increment our counter
	completedUpdates = [[NSNumber numberWithInteger:([completedUpdates integerValue] + 1)] retain];
	[self updateProgress];

	[self updateMessageForStop:stop];
}
- (void)deleteDidFinishStop:(Stop *)stop
{
	// finished that one? increment our counter
	completedUpdates = [[NSNumber numberWithInteger:([completedUpdates integerValue] + 1)] retain];
	[self updateProgress];
	//NSLog(@"%@ was deleted", stop);
}

- (void)syncDidStartRoute:(Route *)route
{
	// if its a new route we wont have any of this information yet
	if (route.name != nil)
	{
		NSString *name = route.name;
		NSString *upDestination = (route.upDestination != nil ? route.upDestination : @"");
		NSString *downDestination = (route.downDestination != nil ? route.downDestination : @"");
		NSNumber *isSubRoute = [NSNumber numberWithBool:[route isSubRoute]];
		NSDictionary *routeInfo = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:name, upDestination, downDestination, isSubRoute, nil]
															  forKeys:[NSArray arrayWithObjects:@"name", @"up", @"down", @"subroute", nil]];
		NSString *routeNumber = [NSString stringWithString:route.internalNumber];
		[routeChanges setObject:routeInfo forKey:routeNumber];
	}
}
- (void)syncDidFinishRoute:(Route *)route
{
	// finished that one? increment our counter
	completedUpdates = [[NSNumber numberWithInteger:([completedUpdates integerValue] + 1)] retain];
	[self updateProgress];

	[self updateMessageForRoute:route];
	//NSLog(@"%@ was updated with turns: %@", route, route.turns);
}
- (void)deleteDidFinishRoute:(Route *)route
{
	// finished that one? increment our counter
	completedUpdates = [[NSNumber numberWithInteger:([completedUpdates integerValue] + 1)] retain];
	[self updateProgress];
	//NSLog(@"%@ was deleted", route);
}
- (void)syncDidAddOperation:(NSOperation *)operation
{
	detectedOperations++;

	if (detectedOperations > [totalUpdates integerValue])
	{
		totalUpdates = [[NSNumber numberWithInteger:detectedOperations] retain];
		[self updateProgress];
	}
}

- (void)syncDidRemoveOperation
{
	totalUpdates = [[NSNumber numberWithInteger:[totalUpdates integerValue]-1] retain];
	[self updateProgress];
}


- (void)syncDidFinishRoutesThroughStop:(Stop *)stop
{
	// finished that one? increment our counter
	completedUpdates = [[NSNumber numberWithInteger:([completedUpdates integerValue] + 1)] retain];

	[self updateProgress];
}

- (void)syncDidUpdateTicketOutlets
{
	completedUpdates = [[NSNumber numberWithInteger:([completedUpdates integerValue] + 1)] retain];
	[self updateProgress];

	// create a history item
	HistoricalUpdate *update = [[HistoricalUpdate alloc] initHistoricalUpdate];
	[update setName:NSLocalizedString(@"updates-log-tickets-name", nil)];
	[update setMessage:NSLocalizedString(@"updates-log-tickets-message", nil)];
	[update setUpdatedDate:[NSDate date]];
	[updateList insertObject:update atIndex:0];
	[update release];
	
}


- (void)syncDidFailWithError:(NSError *)error
{
	// if its a connection error display the message
	if ([error code] == PIDServiceErrorNotReachable)
		errorMessage = [error localizedDescription];
	else
		errorMessage = NSLocalizedString(@"updates-unknown-error-sync", nil);
	
	[errorMessage retain];
	[self syncWasCancelled];
}


- (void)updateMessageForStop:(Stop *)stop
{
	// This will be nil if the process aborted
	if (updateList == nil)
		return;

	// set the default message
	NSString *name = [NSString stringWithFormat:NSLocalizedString(@"stop-name-mixed-list", nil), stop.number, stop.name];
	NSString *message = [NSString stringWithFormat:NSLocalizedString(@"updates-stop-updated-template", nil), [stop displayedCityDirection]];
	NSDate *date = [NSDate date];
	
	// have we been deleted?
	if ([stop isDeleted])
	{
		message = [NSString stringWithFormat:NSLocalizedString(@"updates-stop-deleted-template", nil), [stop displayedCityDirection]];
	} else
	{
		// get the old stop info
		NSNumber *trackerID = [NSNumber numberWithInteger:[stop.trackerID integerValue]];
		NSDictionary *oldStopInfo = [stopChanges objectForKey:trackerID];
		if (oldStopInfo != nil)
		{
			// do detecting - new stop
			if ([[oldStopInfo objectForKey:@"name"] isEqualToString:@""])
				message = [NSString stringWithFormat:NSLocalizedString(@"updates-stop-added-template", nil), [stop displayedCityDirection]];

			// upgraded to a platform stop
			else if (![[oldStopInfo objectForKey:@"platform"] boolValue] && [stop isPlatformStop])
				message = [NSString stringWithFormat:NSLocalizedString(@"updates-stop-platform-template", nil), [stop displayedCityDirection]];
		}
	}

	// create a history item
	HistoricalUpdate *update = [[HistoricalUpdate alloc] initHistoricalUpdate];
	[update setName:name];
	[update setMessage:message];
	[update setUpdatedDate:date];
	[updateList insertObject:update atIndex:0];
	[update release];
}

- (void)updateMessageForRoute:(Route *)route
{
	// This will be nil if the process aborted
	if (updateList == nil)
		return;

	// set the default message
	NSString *name = [NSString stringWithFormat:NSLocalizedString(([route isSubRoute] ? @"route-name-subroute" : @"route-name"), nil), route.number, route.name];
	NSString *message = NSLocalizedString(@"updates-route-updated-template", nil);
	NSDate *date = [NSDate date];
	
	// has it been deleted?
	if ([route isDeleted])
	{
		message = NSLocalizedString(@"updates-route-deleted-template", nil);

	} else
	{
		
		// get the old stop info
		NSString *routeNumber = [NSString stringWithString:route.internalNumber];
		NSDictionary *oldRouteInfo = [routeChanges objectForKey:routeNumber];
		if (oldRouteInfo == nil)
		{
			// do detecting - new route
			message = NSLocalizedString(@"updates-route-added-template", nil);
		}
		else
		{
			// has a destination changed?
			if (route.upDestination != nil && route.downDestination != nil && (![route.upDestination isEqualToString:[oldRouteInfo objectForKey:@"up"]] || ![route.downDestination isEqualToString:[oldRouteInfo objectForKey:@"down"]]))
				message = [NSString stringWithFormat:NSLocalizedString(@"updates-route-destination-template", nil), route.upDestination, route.downDestination];
		}
	}
	
	// create a history item
	HistoricalUpdate *update = [[HistoricalUpdate alloc] initHistoricalUpdate];
	[update setName:name];
	[update setMessage:message];
	[update setUpdatedDate:date];
	[updateList insertObject:update atIndex:0];
	[update release];
}

#pragma mark -
#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return wasAborted ? 0 : 1;
}


// Customize the number of rows in the table view. 
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
	static NSString *ProgressCell = @"ProgressCell";
	BOOL isProgressCell = (indexPath.row == 1);
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:(isProgressCell ? ProgressCell : CellIdentifier)];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:(isProgressCell ? ProgressCell : CellIdentifier)] autorelease];

		if (isProgressCell)
		{
			UIProgressView *progress = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
			[progress setFrame:CGRectMake(10, 18, 280, 10)];
			[progress setProgress:0];
			[cell.contentView addSubview:progress];
			[self setProgressView:progress];
			[progress release];
		} else
		{
			[cell.textLabel setText:NSLocalizedString(@"updates-modal-syncing", nil)];
		}
    }
	
	if (!isProgressCell)
	{
		[cell.detailTextLabel setText:[NSString stringWithFormat:NSLocalizedString(@"updates-modal-syncing-count", nil), completedUpdates, totalUpdates]];

		if (isFinished)
			[cell.textLabel setText:NSLocalizedString(@"updates-modal-complete", nil)];
		else if (isSaving)
			[cell.textLabel setText:NSLocalizedString(@"updates-modal-saving", nil)];
	} else
	{
		CGFloat progress = [completedUpdates doubleValue] / [totalUpdates doubleValue];
		[self.progressView setProgress:progress];
	}

    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
	// AnotherViewController *anotherViewController = [[AnotherViewController alloc] initWithNibName:@"AnotherView" bundle:nil];
	// [self.navigationController pushViewController:anotherViewController];
	// [anotherViewController release];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


- (void)dealloc {
	[textView release];
	[progressCounter release];
	[progressView release];
	[totalUpdates release];
	[completedUpdates release];
	[syncManager release];
	[stopChanges release];
	[routeChanges release];
	[parentUpdatesController release];
	[params release];
	[errorMessage release];
    [super dealloc];
}


@end

