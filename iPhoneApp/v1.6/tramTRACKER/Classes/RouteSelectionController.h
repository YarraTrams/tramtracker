//
//  RouteSelectionController.h
//  tramTRACKER
//
//  Created by Robert Amos on 7/04/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SimpleCell.h"

@interface RouteSelectionController : UITableViewController {
	NSArray *selectedRoutes;
	BOOL direction;
	NSNumber *trackerID;
	NSInteger selectedRouteIndex;
	BOOL displayAllRoutesOption;
	id target;
	SEL action;
}

@property (nonatomic, retain) NSArray *selectedRoutes;
@property (nonatomic) BOOL direction;
@property (nonatomic, retain) NSNumber *trackerID;
@property (nonatomic) NSInteger selectedRouteIndex;
@property (nonatomic) BOOL displayAllRoutesOption;

- (id)initWithAllRoutesTarget:(id)aTarget action:(SEL)selector;
- (id)initWithRoutes:(NSArray *)routes target:(id)aTarget action:(SEL)selector;
- (id)initWithRoutes:(NSArray *)routes direction:(BOOL)isUpDirection target:(id)aTarget action:(SEL)selector;
- (id)initWithRoutes:(NSArray *)routes trackerID:(NSNumber *)trackerID target:(id)aTarget action:(SEL)selector;

@end
