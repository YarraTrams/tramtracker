//
//  ScheduledTripController.m
//  tramTRACKER
//
//  Created by Robert Amos on 31/07/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "ScheduledTripController.h"
#import "OnboardListCell.h"
#import "Journey.h"
#import "Route.h"
#import "Stop.h"
#import "StopList.h"
#import "RouteList.h"
#import "DepartureListController.h"
#import "UnderBackgroundView.h"
#import "OnboardUnderCell.h"


@implementation ScheduledTripController

@synthesize indexPathToSelectedStop, suburbList, stopListSplitBySuburbs;

- (id)initWithJourney:(Journey *)aJourney onRoute:(Route *)aRoute fromStop:(Stop *)aStop
{
	if (self = [super initWithStyle:UITableViewStylePlain])
	{
		journey = aJourney;
		[journey retain];
		
		stop = aStop;
		[stop retain];
		
		route = aRoute;
		[route retain];
		
		// Reset our background colour
		[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
		[self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
		
		BOOL isViaCity = [stop.cityDirection rangeOfString:@"towards City"].location != NSNotFound;
		[self setTitle:[NSString stringWithFormat:NSLocalizedString(isViaCity ? @"route-destination-viacity" : @"route-destination", nil), [route destinationForTrackerID:stop.trackerID]]];

		[self.tableView setTableFooterView:[self tableFooterView]];
	}
	return self;
}

- (UIView *)tableFooterView
{
	CGRect frame = CGRectMake(0, 0, self.tableView.frame.size.width, 10);
	UnderBackgroundView *ub = [[UnderBackgroundView alloc] initWithFrame:frame];
	return [ub autorelease];
}

- (void)viewDidAppear:(BOOL)animated
{
    // find the stop in the split list
    NSUInteger section = 0;
    NSUInteger row = 0;
    
    for (NSArray *stops in stopListSplitBySuburbs)
    {
        for (Stop *s in stops)
        {
            if ([s isEqualToStop:stop])
            {
                NSIndexPath *pathToStop = [NSIndexPath indexPathForRow:row inSection:section];
                [self.tableView scrollToRowAtIndexPath:pathToStop atScrollPosition:UITableViewScrollPositionTop animated:YES];
                return;
            }
            row++;
        }
        section++;
        row = 0;
    }
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.suburbList == nil || [self.suburbList count] == 0)
        [self setupSuburbList];
    return [self.suburbList count];
}


//
// Get the header title
//
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	return [self.suburbList objectAtIndex:section];
}

//
// Custom Headers
//
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	// grab a OnboardListSection thingy
	OnboardListSectionHeaderView *view = [[OnboardListSectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 22)];
	
	// if its not the first section then add the track image from the last stop passed
	if (section != 0)
	{
		NSArray *s = [self.stopListSplitBySuburbs objectAtIndex:section];
		if ([s count] != 0)
		{
			Stop *st = [s objectAtIndex:0];
			[view.imageView setImage:[st imageForEmptyTrackAtStopOfRoute:route]];
		}
	}
	
	// set the title
	[view.title setText:[self tableView:tableView titleForHeaderInSection:section]];
	
	return [view autorelease];
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	// get the number of stops that match a suburb name
	return [[self.stopListSplitBySuburbs objectAtIndex:section] count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"TripCell";
	
    Stop *s = [[self.stopListSplitBySuburbs objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
	
	// showing the "under side" of the cell.
	if (indexPathToSelectedStop != nil && [indexPathToSelectedStop compare:indexPath] == NSOrderedSame)
	{
		OnboardUnderCell *cell = (OnboardUnderCell *)[tableView dequeueReusableCellWithIdentifier:@"UnderCell"];
		if (cell == nil)
		{
			cell = [[[OnboardUnderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"UnderCell" allowsAlarm:NO] retain];
		}
		[cell setDelegate:self];
		[cell setStop:s];
		return cell;
	}
	
    OnboardListCell *cell = (OnboardListCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[OnboardListCell alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width-60, 44)
									   reuseIdentifier:CellIdentifier
											 cellStyle:OnboardCellStyleMinutesOnRightDisabled] autorelease];
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	}
	
	JourneyStop *jstop = [journey journeyStopForStop:s];
    
	// Set up the cell...
	[cell setTrackSegmentView:[s viewForTrackOnRoute:route isDisabled:NO]];

	// add the stop name
	[cell.name setText:[s formattedName]];
	
	if (jstop != nil)
	{
		NSString *predictedArrivalString = [self absoluteArrivalTime:jstop.predicatedArrivalDateTime];
		[cell.arrival setText:predictedArrivalString];
	} else
	{
		[cell.arrival setText:@"--"];
	}
	
	// show/hide the connection indicators
	[cell.connectingTrain setHidden:!([s hasConnectingTrains])];
	[cell.connectingTram setHidden:!([s hasConnectingTrams])];
	[cell.connectingBus setHidden:!([s hasConnectingBuses])];
	
	// is this the departing stop? highlight it if so.
	[cell setHighlightedStop:[s isEqual:stop]];

    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

	if (indexPathToSelectedStop != nil)
	{
		NSIndexPath *oldIndexPath = [indexPathToSelectedStop retain];
		[indexPathToSelectedStop release]; indexPathToSelectedStop = nil;
		[self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:oldIndexPath] withRowAnimation:UITableViewRowAnimationFade];
		
		if ([oldIndexPath compare:indexPath] == NSOrderedSame)
		{
			[oldIndexPath release]; oldIndexPath = nil;
			return;
		}
		[oldIndexPath release]; oldIndexPath = nil;
	}
	
	indexPathToSelectedStop = [indexPath retain];
	[self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPathToSelectedStop] withRowAnimation:UITableViewRowAnimationFade];
	
	
	/*[UIView beginAnimations:nil context:nil];
	 [UIView setAnimationDuration:1];
	 [UIView setAnimationDelegate:self];
	 [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:)];
	 [cell.layer setTransform:CATransform3DRotate(cell.layer.transform, (90 * M_PI / 180), cell.frame.size.height/2, 0, 0)];
	 [UIView commitAnimations];*/
	
}

- (NSString *)absoluteArrivalTime:(NSDate *)arrivalTime
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	
	// use the current calendar to match days
	NSCalendar *cal = [NSCalendar currentCalendar];
	
	//NSDate *today = [NSDate date];
	//NSDateComponents *todayComponents = [cal components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:today];
	NSDateComponents *arrivalComponents = [cal components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:arrivalTime];
	
	// if its in the year 9999 then it means there is an error, display dashes
	if ([arrivalComponents year] >= 9999)
	{
		[formatter release];
		return @"--";
	}
	
	// check the weekday, if its arriving on a different day then we add the day name.
	//if ([todayComponents day] == [arrivalComponents day] && [todayComponents month] == [arrivalComponents month] && [todayComponents year] == [arrivalComponents year])
		//[formatter setDateFormat:@"h:mm a"]; -- use built in style to allow for use of 24 hour time setting
	[formatter setTimeStyle:NSDateFormatterShortStyle];
	[formatter setDateStyle:NSDateFormatterNoStyle];
	/*else
		[formatter setDateFormat:@"dd/MM/yy h:mm a"];*/
	
	NSString *formattedArrivalTime = [formatter stringFromDate:arrivalTime];
	[formatter release];
	return formattedArrivalTime;
}

- (void)showConnectionsForStop:(Stop *)s
{
	//NSLog(@"Showing connections for stop: %@", s);
	
	// find the 10 nearest stops to our central stop
	NSArray *nearbyStops = [[StopList sharedManager] getNearestStopsWithoutDistancesToLocation:s.location count:10];

	NSMutableArray *stopList = [[NSMutableArray alloc] initWithCapacity:0];
	for (JourneyStop *jstop in journey.stops)
		[stopList addObject:jstop.stop];
	
	MapViewController *mapView = [[MapViewController alloc] initWithStopList:nearbyStops];
	[mapView setListViewType:TTMapViewListTypeConnectionList];
	[mapView setPurpleStops:stopList];
	[mapView setSelectedStop:s];
	[mapView setArrivalDate:[[journey journeyStopForStop:s] predicatedArrivalDateTime]];
	[mapView setTitle:NSLocalizedString(@"onboard-button-connections", nil)];
	[mapView setPushToScheduledDepartures:YES];
	[self.navigationController pushViewController:mapView animated:YES];
	[mapView release];
	[stopList release];
}

- (void)setupSuburbList
{
	// find the number of suburbs in the list
	[self setSuburbList:[NSMutableArray arrayWithCapacity:0]];
	[self setStopListSplitBySuburbs:[NSMutableArray arrayWithCapacity:0]];

	NSArray *stopList = [route.upStops containsObject:stop.trackerID] ? route.upStops : route.downStops;
    //NSLog(@"%@", stopList);
    for (NSNumber *trackerID in stopList)
	{
        Stop *s = [[StopList sharedManager] getStopForTrackerID:trackerID];
		if ([self.suburbList count] == 0 || ![[self.suburbList objectAtIndex:[self.suburbList count]-1] isEqual:s.suburbName])
		{
			[self.suburbList addObject:[s suburbName]];
			[self.stopListSplitBySuburbs addObject:[NSMutableArray arrayWithCapacity:0]];
		}
		[[self.stopListSplitBySuburbs objectAtIndex:[self.stopListSplitBySuburbs count]-1] addObject:s];
	}
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


- (void)dealloc {
	[journey release];
	[stop release];
	[indexPathToSelectedStop release];
    [suburbList release]; suburbList = nil;
    [stopListSplitBySuburbs release]; stopListSplitBySuburbs = nil;
    [super dealloc];
}


@end

