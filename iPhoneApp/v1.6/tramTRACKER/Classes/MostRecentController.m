//
//  MostRecentController.m
//  tramTRACKER
//
//  Created by Robert Amos on 10/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "MostRecentController.h"


@implementation MostRecentController

- (void)viewDidLoad
{
	// Reset our background colour
	[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];

	// add the table footer
	[self.tableView setTableFooterView:[self tableFooterView]];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

	// display the map button
	if ([[StopList sharedManager] hasMostRecentStops])
	{
		UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"button-map", @"Map")
																   style:UIBarButtonItemStylePlain
																  target:self
																  action:@selector(flipToMapView)];
		[self.navigationItem setRightBarButtonItem:button animated:NO];
		[button release];
	}
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}

//
// Create the table footer
//
- (UIView *)tableFooterView
{
	CGRect frame = CGRectMake(0, 0, self.tableView.frame.size.width, 10);
	UnderBackgroundView *ub = [[UnderBackgroundView alloc] initWithFrame:frame];
	return [ub autorelease];
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	// find out the number from the stop list
	if (![[StopList sharedManager] hasMostRecentStops])
		return 0;
	return [[[StopList sharedManager] mostRecentStops] count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"MostRecentCell";
    
    MostRecentCell *cell = (MostRecentCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        cell = [[[MostRecentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		
		[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    }
    
    // Set up the cell...
	NSArray *mostRecentStops = [[StopList sharedManager] mostRecentStops];
	Stop *stop = [[StopList sharedManager] getStopForTrackerID:[mostRecentStops objectAtIndex:[indexPath indexAtPosition:1]]];
	[cell.name setText:[stop formattedName]];
	[cell.routeDescription setText:[stop formattedRouteDescription]];
	
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	// Get the stop
	NSNumber *trackerID = [[[StopList sharedManager] mostRecentStops] objectAtIndex:[indexPath indexAtPosition:1]];
	Stop *stop = [[StopList sharedManager] getStopForTrackerID:trackerID];
	
	// Start the pid view
	PIDViewController *PID = nil;
    
    if ([[UIScreen mainScreen] bounds].size.height == 568) {
        PID = [[PIDViewController alloc] initWithNibName:@"PIDView-iPhone5" bundle:[NSBundle mainBundle]];
    }
    else{
        PID = [[PIDViewController alloc] initWithNibName:@"PIDView" bundle:[NSBundle mainBundle]];
    }
	
	// get the stop info
	[PID startWithStop:stop];
	
	// push it onto the stack
	[self.navigationController pushViewController:PID animated:YES];
	[PID release];
}


// Flip over to show a map view
- (void)flipToMapView
{
	// build a stop list based on ourselves
	NSMutableArray *stops = [[NSMutableArray alloc] initWithCapacity:0];
	StopList *s = [StopList sharedManager];
	for (NSNumber *trackerID in [s mostRecentStops])
		[stops addObject:[s getStopForTrackerID:trackerID]];
	
	MapViewController *mapView = [[MapViewController alloc] initWithStopList:stops];
	[stops release];
	
	[mapView setListViewType:TTMapViewListTypeMostRecent];
	[mapView setTitle:self.title];
	
	// flip over to the map view
	[UIView beginAnimations:@"FlipToMapView" context:nil];
	[[self.navigationController.view.subviews objectAtIndex:0] setBackgroundColor:[UIColor blackColor]];
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:[self.navigationController.view.subviews objectAtIndex:0] cache:YES];
	[UIView setAnimationDuration:1];
	
	UINavigationController *nav = self.navigationController;

	// reset the view controllers array
	if ([[nav.viewControllers objectAtIndex:0] isEqual:[self.tabBarController.moreNavigationController.viewControllers objectAtIndex:0]])
		[nav setViewControllers:[NSArray arrayWithObjects:[nav.viewControllers objectAtIndex:0], mapView, nil]];
	else
		[nav setViewControllers:[NSArray arrayWithObject:mapView] animated:NO];
	
	[UIView commitAnimations];
	
	[mapView release];
}


- (void)dealloc {

	
	[super dealloc];
}


@end

