//
//  VoiceOverButton.m
//  tramTRACKER
//
//  Created by Robert Amos on 13/10/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "VoiceOverButton.h"


@implementation VoiceOverButton

@synthesize prediction;

- (void)dealloc {
	[prediction release];
    [super dealloc];
}


@end
