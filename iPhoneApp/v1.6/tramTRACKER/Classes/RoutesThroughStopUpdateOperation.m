//
//  RoutesThroughStopUpdateOperation.m
//  tramTRACKER
//
//  Created by Robert Amos on 31/07/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "RoutesThroughStopUpdateOperation.h"
#import "Stop.h"
#import "PidsService.h"
#import "BackgroundSynchroniser.h"

@implementation RoutesThroughStopUpdateOperation

@synthesize syncManager, stop;

- (id)initWithStop:(Stop *)aStop
{
	if (self = [super init])
	{
		stop = aStop;
		[stop retain];
		
		service = [[PidsService alloc] init];
		[service setDelegate:self];
		
		[self willChangeValueForKey:@"isExecuting"];
		executing = NO;
		[self didChangeValueForKey:@"isExecuting"];
		
		[self willChangeValueForKey:@"isFinished"];
		finished = NO;
		[self didChangeValueForKey:@"isFinished"];
		
		// set our priority
		[self setQueuePriority:NSOperationQueuePriorityNormal];
	}
	return self;
}

- (BOOL)isConcurrent
{
	return NO;
}

- (BOOL)isExecuting
{
	return executing;
}

- (BOOL)isFinished
{
	return finished;
}

- (void)cancel
{
	[super cancel];
	//NSLog(@"Operation %@ cancelled.", self);
}


/**
 * Actually start processing the update
 **/
- (void)main
{
	[self start];
}
- (void)start
{
	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}
	
	// we start by getting the destinations for this route
	[self willChangeValueForKey:@"isExecuting"];
	executing = YES;
	[self didChangeValueForKey:@"isExecuting"];

	// send a notification
	if (self.syncManager != nil && self.syncManager.delegate != nil && [self.syncManager.delegate respondsToSelector:@selector(syncDidStartRoutesThroughStop:)])
		[self.syncManager.delegate performSelectorOnMainThread:@selector(syncDidStartRoutesThroughStop:) withObject:stop waitUntilDone:NO];	
	
	[self updateRoutesThroughStop];

	// sigh, poll until its finished
	while (![self isFinished])
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
}

- (void)updateRoutesThroughStop
{
	[service routesThroughStop:stop];
}

- (void)setRoutes:(NSArray *)routes throughStop:(NSNumber *)trackerID
{
	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}
	[stop setRoutesThroughStop:routes];

	[self finish];
}

// Indicates a failure of some sort.
- (void)pidsServiceDidFailWithError:(NSError *)error
{
	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}
	
	static NSInteger previousFailureCount = 0;

	// if the stop has been deleted we can silently fail over to syncing that change
	if ([error code] == PIDServiceValidationErrorStopNotFound)
	{
		// this error can occur for a valid stop with no routes "through" it (terminus)
		//[self.stop setRoutesThroughStop:[NSArray array]];
		
		// update the stop to be sure, if the update triggers the same error it will get deleted from there
		//if (self.syncManager != nil)
		//	[self.syncManager addStopUpdateToQueue:self.stop];

		[self finish];
		
		// otherwise some other error has occured, abort
	}
	else if ([error code] == PIDServiceErrorTimeoutReached && previousFailureCount == 0)
	{
		// try it again
		//NSLog(@"Timed out, trying again.");
		[self updateRoutesThroughStop];
		previousFailureCount = 1;

	} else
	{
		[self cancel];
		[self finish];

		// send a failure message to the delegate
		if (self.syncManager != nil && self.syncManager.delegate != nil && ![self isCancelled] && [self.syncManager.delegate respondsToSelector:@selector(syncDidFailWithError:)])
			[self.syncManager.delegate performSelectorOnMainThread:@selector(syncDidFailWithError:) withObject:error waitUntilDone:NO];
	}
}

- (void)finish
{
	// send a notification if we werent cancelled
	if (self.syncManager != nil && self.syncManager.delegate != nil && ![self isCancelled] && [self.syncManager.delegate respondsToSelector:@selector(syncDidFinishRoutesThroughStop:)])
		[self.syncManager.delegate performSelectorOnMainThread:@selector(syncDidFinishRoutesThroughStop:) withObject:stop waitUntilDone:NO];

	[self willChangeValueForKey:@"isExecuting"];
	executing = NO;
	[self didChangeValueForKey:@"isExecuting"];
	
	[self willChangeValueForKey:@"isFinished"];
	finished = YES;
	[self didChangeValueForKey:@"isFinished"];
	
	// release ourselves from the delegate
	[service setDelegate:self];

	//NSLog(@"Routes through %@ were updated", stop);
}

- (void)dealloc
{
	[stop release];
	[service release];
	[super dealloc];
}

@end
