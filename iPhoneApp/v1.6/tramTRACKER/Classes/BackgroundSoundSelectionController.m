//
//  BackgroundSoundSelectionController.m
//  tramTRACKER
//
//  Created by Robert Amos on 17/08/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "BackgroundSoundSelectionController.h"
#import "SimpleCell.h"

@implementation BackgroundSoundSelectionController


#pragma mark -
#pragma mark Initialization


- (id)initWithSoundType:(NSString *)aSoundType target:(id)aTarget action:(SEL)anAction
{
	if ((self = [super initWithStyle:UITableViewStyleGrouped]))
	{
		[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
		soundType = aSoundType;
		target = aTarget;
		action = anAction;
		
		if ([[UIDevice currentDevice] respondsToSelector:@selector(isMultitaskingSupported)] && [[UIDevice currentDevice] isMultitaskingSupported])
			sounds = [[NSDictionary alloc] initWithObjectsAndKeys:NSLocalizedString(@"settings-background-nosound", @"None"), AlertSoundNone,
					  NSLocalizedString(@"settings-background-sounddefault", @"System Default Sound"), AlertSoundSystemDefault,
					  NSLocalizedString(@"settings-background-soundaclass", @"A/B/Z Class Tram"), AlertSoundTramAClass,
					  NSLocalizedString(@"settings-background-soundcclass", @"Citadis Tram"), AlertSoundTramCClass,
					  NSLocalizedString(@"settings-background-soundwclass", @"W Class Tram"), AlertSoundTramWClass, nil];
		else
			sounds = [[NSDictionary alloc] initWithObjectsAndKeys:NSLocalizedString(@"settings-background-nosound", @"None"), AlertSoundNone,
					  NSLocalizedString(@"settings-background-soundaclass", @"A/B/Z Class Tram"), AlertSoundTramAClass,
					  NSLocalizedString(@"settings-background-soundcclass", @"Citadis Tram"), AlertSoundTramCClass,
					  NSLocalizedString(@"settings-background-soundwclass", @"W Class Tram"), AlertSoundTramWClass, nil];
	}
	return self;
}


#pragma mark -
#pragma mark View lifecycle



- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
	
	if (target != nil && [target respondsToSelector:action])
	{
		[target performSelector:action withObject:soundType];
	}
}


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [sounds count];
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
	if ([[UIDevice currentDevice] respondsToSelector:@selector(isMultitaskingSupported)] && [[UIDevice currentDevice] isMultitaskingSupported])
		return [NSString stringWithFormat:NSLocalizedString(@"settings-background-soundfooter", nil), NSLocalizedString(@"settings-background-sounddefault", nil)];
	return nil;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    SimpleCell *cell = (SimpleCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[SimpleCell alloc] initWithCellStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
	[cell setTextLabelText:[[sounds allValues] objectAtIndex:indexPath.row]];
	
	// is it checked?
	[cell setAccessoryType:UITableViewCellAccessoryNone];

	if ([[[sounds allKeys] objectAtIndex:indexPath.row] isEqual:soundType])
		[cell setAccessoryType:UITableViewCellAccessoryCheckmark];
		
    return cell;
}




#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

	// save the thing
	soundType = [[sounds allKeys] objectAtIndex:indexPath.row];

	[self.tableView reloadData];
	
	[self playSelectedSound];
}

- (void)playSelectedSound
{
	// play the selected sound
	if (soundType != AlertSoundSystemDefault && soundType != AlertSoundNone)
	{
		// grab the name of the file
		NSString *soundName = @"citadis.caf";
		
		if (soundType == AlertSoundTramAClass)
			soundName = @"a.caf";
		else if (soundType == AlertSoundTramCClass)
			soundName = @"citadis.caf";
		else if (soundType == AlertSoundTramWClass)
			soundName = @"w.caf";

		// play it
		SystemSoundID soundID;
		AudioServicesCreateSystemSoundID((CFURLRef)[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:soundName ofType:nil]], &soundID);
		AudioServicesPlaySystemSound(soundID);
	}
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
	[sounds release]; sounds = nil;
    [super dealloc];
}


@end

