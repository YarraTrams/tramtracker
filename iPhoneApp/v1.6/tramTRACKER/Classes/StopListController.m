//
//  StopListController.m
//  tramTRACKER
//
//  Created by Robert Amos on 27/02/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "StopListController.h"


@implementation StopListController

@synthesize stopList, suburbList, stopListSplitBySuburbs, locationManager, currentStop, pushToScheduledDeparturesScreen, route;

- (void)viewDidLoad
{
	// go go!
	LocationManager *l = [[LocationManager alloc] init];
	if ([CLLocationManager locationServicesEnabled])
	{
		[l setDelegate:self];
		
		[self setLocationManager:l];
		locationResponseCount = 0;
	}
	[l release];
	
	// Reset our background colour
	[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
	[self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];

	// set the table footer
	[self.tableView setTableFooterView:[self tableFooterView]];
	
}

// Custom startup
- (void)viewDidAppear:(BOOL)animated
{
	// set the map view button if we can show a map
	UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithTitle:@"Map"
															   style:UIBarButtonItemStylePlain
															  target:self
															  action:@selector(flipToMapView)];
	[self.navigationItem setRightBarButtonItem:button animated:NO];
	[button release];
	
	// if they've requested it find the nearest stop in this list
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	BOOL jumpToNearestStop = [defaults boolForKey:@"jumpToNearestStopInStopList"];
	if (jumpToNearestStop)
	{
		// start locating
		[self startTracking];
	} else
	{
		[self stopTracking];
	}
}

// Stop the location manager
- (void)viewWillDisappear:(BOOL)animated
{
	if (self.locationManager)
		[self.locationManager stopUpdatingLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}

- (void)setupSuburbList
{
	// make sure that we have a stop list
	NSAssert(self.stopList, @"Cannot build suburb list as no stops have been set.");

	// find the number of suburbs in the list
	[self setSuburbList:[NSMutableArray arrayWithCapacity:0]];
	[self setStopListSplitBySuburbs:[NSMutableArray arrayWithCapacity:0]];
	for (Stop *stop in self.stopList)
	{
		if ([self.suburbList count] == 0 || ![[self.suburbList objectAtIndex:[self.suburbList count]-1] isEqual:stop.suburbName])
		{
			[self.suburbList addObject:[stop suburbName]];
			[self.stopListSplitBySuburbs addObject:[NSMutableArray arrayWithCapacity:0]];
		}
		[[self.stopListSplitBySuburbs objectAtIndex:[self.stopListSplitBySuburbs count]-1] addObject:stop];
	}
}


//
// Create the table footer
//
- (UIView *)tableFooterView
{
	CGRect frame = CGRectMake(0, 0, self.tableView.frame.size.width, 126);
	UnderBackgroundView *ub = [[UnderBackgroundView alloc] initWithFrame:frame];
	
	// put the key in there
	UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stoplistkey.png"]];
	CGRect imageFrame = imageView.frame;
	imageFrame.origin.y = 20;
    imageFrame.origin.x = 20;
	[imageView setFrame:imageFrame];
	[ub addSubview:imageView];
	[imageView release];
	
	return [ub autorelease];
}

// Flip over to show a map view
- (void)flipToMapView
{
	MapViewController *mapView = [[MapViewController alloc] initWithStopList:self.stopList];
	[mapView setListViewType:TTMapViewListTypeBrowseList];
	[mapView setTitle:self.title];
	[mapView setRoute:self.route];
	[mapView setPushToScheduledDepartures:self.pushToScheduledDeparturesScreen];

	// flip over to the map view
	[UIView beginAnimations:@"FlipToMapView" context:nil];
	[[self.navigationController.view.subviews objectAtIndex:0] setBackgroundColor:[UIColor blackColor]];
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:[self.navigationController.view.subviews objectAtIndex:0] cache:YES];
	[UIView setAnimationDuration:1];
	
	UINavigationController *nav = self.navigationController;
	
	// reset the view controllers array
	[nav popViewControllerAnimated:NO];
	[nav pushViewController:mapView animated:NO];
	
	[UIView commitAnimations];
	
	[mapView release];
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	if (!self.suburbList)
	{
		[self setupSuburbList];
	}
	return [self.suburbList count];
}

//
// Get the header title
//
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	return [self.suburbList objectAtIndex:section];
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	// get the number of stops that match a suburb name
	return [[self.stopListSplitBySuburbs objectAtIndex:section] count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    OnboardListCell *cell = (OnboardListCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[OnboardListCell alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 44) reuseIdentifier:CellIdentifier cellStyle:OnboardCellStyleNone] autorelease];
    }
    
	// find the stop
	Stop *stop = [[self.stopListSplitBySuburbs objectAtIndex:[indexPath indexAtPosition:0]] objectAtIndex:[indexPath indexAtPosition:1]];
	
	// all stops but the last on the route are clickable
	if ([stop.trackerID integerValue] >= 8000)
	{
		[cell setAccessoryType:UITableViewCellAccessoryNone];
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	}
	else
	{
		[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
		[cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
	}
	
	// Set up the cell...
	[cell setTrackSegmentView:[stop viewForTrackOnRoute:self.route isDisabled:NO]];
	
	// add the stop name
	[cell.name setText:[stop formattedName]];
	
	// show/hide the connection indicators
	[cell.connectingTrain setHidden:!([stop hasConnectingTrains])];
	[cell.connectingTram setHidden:!([stop hasConnectingTrams])];
	[cell.connectingBus setHidden:!([stop hasConnectingBuses])];
	
	// is this cell highlighted?
	[cell setHighlightedStop:[indexPath isEqual:self.currentStop]];
	
	return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	// set the text to the stop number: name
	Stop *stop = [[self.stopListSplitBySuburbs objectAtIndex:[indexPath indexAtPosition:0]] objectAtIndex:[indexPath indexAtPosition:1]];
	if ([stop.trackerID integerValue] >= 8000)
		return;

	// pushing to a scheduled departures list?
	if (self.pushToScheduledDeparturesScreen)
	{
		DepartureListController *departures = [[DepartureListController alloc] initWithScheduledListForStop:stop];
		[departures setSelectedRoute:self.route.number];
		[self.navigationController pushViewController:departures animated:YES];
		[departures release];
		
	} else
	{
		// Start the pid view
		PIDViewController *PID = nil;
        
        if ([[UIScreen mainScreen] bounds].size.height == 568) {
            PID = [[PIDViewController alloc] initWithNibName:@"PIDView-iPhone5" bundle:[NSBundle mainBundle]];
        }
        else{
            PID = [[PIDViewController alloc] initWithNibName:@"PIDView" bundle:[NSBundle mainBundle]];
        }

		
		// get the stop info
		[PID startWithStop:stop];

		// push it onto the stack
		[self.navigationController pushViewController:PID animated:YES];

		[PID release];
	}
}

//
// Custom Headers
//
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	// grab a OnboardListSection thingy
	OnboardListSectionHeaderView *view = [[OnboardListSectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 22)];
	
	// if its not the first section then add the track image from the last stop passed
	if (section != 0)
	{
		NSArray *s = [self.stopListSplitBySuburbs objectAtIndex:section];
		if ([s count] != 0)
		{
			Stop *stop = [s objectAtIndex:0];
			[view.imageView setImage:[stop imageForEmptyTrackAtStopOfRoute:self.route]];
		}
	}
	
	// set the title
	[view.title setText:[self tableView:tableView titleForHeaderInSection:section]];
	
	return [view autorelease];
}

#pragma mark -
#pragma mark Tracking

- (void)startTracking
{
	// set the right bar button item to be tracking
	stopTooFar = 0;
	[self.locationManager startUpdatingLocation];
	//NSLog(@"Starting tracking");
}

- (void)stopTracking
{
	// set the right bar button item
	[self.locationManager stopUpdatingLocation];
	//NSLog(@"Stopping tracking");
}


#pragma mark CLLocationManagerDelegate methods

//
// The location Service has told us there was an error
//
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
	// if they've clicked "no" and we're not permitted to update, dont do anything
	if ([error code] == kCLErrorDenied)
	{
		[self stopTracking];
	}
}


//
// We got a new location
//
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
	// has the location actually changed?
	//NSLog(@"Received location update.");

	// the location manager might have a more recent location
	if ([manager.location.timestamp isEqualToDate:[manager.location.timestamp laterDate:newLocation.timestamp]])
		newLocation = manager.location;
	
	// too far?
	if (stopTooFar > 2)
	{
		[self stopTracking];
		return;
	}

	if (oldLocation == nil || [LocationManager distanceFromLocation:newLocation toLocation:oldLocation] > 0)
	{
		//NSLog(@"Found new location %@", newLocation);
		
		// find out which stop on our list is nearest
		StopDistance *nearestStop = [[StopList sharedManager] getNearestStopToLocation:newLocation withStopList:self.stopList];
		
		// is it within say 500m?
		if (nearestStop.distance <= 500)
		{
			// so we know now, scroll to it
			NSInteger section = 0;
			NSInteger row = -1;
			for (NSArray *list in self.stopListSplitBySuburbs)
			{
				// check out the list of stops in that suburb to find our row count
				row = [list indexOfObject:nearestStop.stop];
				if (row != NSNotFound)
				{
					//NSLog(@"Scrolling to %d, %d", section, row);
					NSUInteger indexPath[2] = { section, row };
					[self setCurrentStop:[NSIndexPath indexPathWithIndexes:indexPath length:2]];
					
					// scroll to it
					[self.tableView reloadData];
					if (row < 1)
						[self.tableView scrollToRowAtIndexPath:self.currentStop atScrollPosition:UITableViewScrollPositionTop animated:YES];
					else
					{
						NSUInteger scrollIndexPath[2];
						if (row == 0)
						{
							scrollIndexPath[0] = section - 1;
							scrollIndexPath[1] = [[self.stopListSplitBySuburbs objectAtIndex:(section - 1)] count] - 1;
						} else
						{
							scrollIndexPath[0] = section;
							scrollIndexPath[1] = row - 1;
						}
						[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathWithIndexes:scrollIndexPath length:2] atScrollPosition:UITableViewScrollPositionTop animated:YES];
						stopTooFar = 0;
					}
					
					break;
				}
				section++;
			}
		} else
		{
			stopTooFar++;
		}
	} else
	{
		// if we've got the same location and the last location was too far increment it here too
		stopTooFar++;
	}
}


- (void)dealloc {
	//NSLog(@"byes from %@", self);
	// release our properties
	[stopList release];
	[suburbList release];
	[stopListSplitBySuburbs release];
	[locationManager release];
	[route release];

    [super dealloc];
}


@end

