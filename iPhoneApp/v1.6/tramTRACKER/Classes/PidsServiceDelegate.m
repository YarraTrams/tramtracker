//
//  PidsServiceDelegate.m
//  tramTRACKER
//
//  Created by Robert Amos on 18/02/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "PidsServiceDelegate.h"
#import "Turn.h"
#import "ISO8601DateFormatter.h"

@implementation PidsServiceDelegate

@synthesize delegate, responseText, currentProperty, currentStop, currentPrediction, currentPredictions, trackerID, validationResult, routesThroughStop,
			requestTime, routeList, currentRoute, stopList, scheduledPrediction, currentTram, currentJourney, currentJourneyStop,
			updateList, currentStopUpdate, currentRouteUpdate, routeNumber, currentRouteDestinations, upDirection, turnList, currentTurn,
			webServiceMethod, updateSinceDate, ticketOutletUpdateCheck, ticketOutletDownload, serverTime;

//
// Initialize with the delegate that will receive the results
//
- (PidsServiceDelegate *)initWithDelegate:(id)newDelegate isWebServiceMethod:(BOOL)isWebService
{
	// Make sure usual init stuff still happens
    self = [self init];
	if (self)
	{
		// set the delegate
		[self setDelegate:newDelegate];
		webServiceMethod = isWebService;
		
		//NSLog(@"PIDServiceDelegate created");
		
		// create a timeout timer for 15 seconds from now
		//timeoutTimer = [[NSTimer timerWithTimeInterval:15 target:self selector:@selector(timeout:) userInfo:nil repeats:NO] retain];
		//[[NSRunLoop currentRunLoop] addTimer:timeoutTimer forMode:NSDefaultRunLoopMode];
		timeoutTimer = nil;
	}
	return self;
}


#pragma mark -
#pragma mark NSURLConnection Delegate methods

// Connection failed with error
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	// For now, just log the error and silently fail
	//NSLog(@"Error with NSURLConnection %@ - %@", connection, error);
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	
	[self failWithError:error];
	
	// let the delegate know
	if ([self.delegate respondsToSelector:@selector(connection:didFailWithError:)])
		[self.delegate performSelector:@selector(connection:didFailWithError:) withObject:connection withObject:error];
	
	// and make sure we abort and clean up
	[timeoutTimer invalidate];
}

// We received a response from the server
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	// Start a new empty responseText that we can append to
	[self setResponseText:[NSMutableString string]];
	
	////NSLog(@"Received response to connection %@, text encoding is %@", connection, [response textEncodingName]);
	
	// if the delegate implements it, let it know we received a response
	if ([self.delegate respondsToSelector:@selector(connection:didReceiveResponse:)])
		[self.delegate performSelector:@selector(connection:didReceiveResponse:) withObject:connection withObject:response];

	[timeoutTimer setFireDate:[NSDate dateWithTimeIntervalSinceNow:15]];
}


// We received a chunk of data from the server
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	// append the data to our responseText string
	NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
	[[self responseText] appendString:str];
	[str release];
	str = nil;
	
	// if the delegate implements it, let it know we received data
	if ([self.delegate respondsToSelector:@selector(connection:didReceiveData:)])
		[self.delegate performSelector:@selector(connection:didReceiveData:) withObject:connection withObject:data];

	[timeoutTimer setFireDate:[NSDate dateWithTimeIntervalSinceNow:15]];
}

// The connection has finished loading, start parsing
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

	// let the delegate know
	if ([self.delegate respondsToSelector:@selector(connectionDidFinishLoading:WithResponseText:)])
		[self.delegate performSelector:@selector(connectionDidFinishLoading:WithResponseText:) withObject:connection withObject:self.responseText];

	//NSLog(@"[PSD] Received response: %@", self.responseText);
	
	// parse the response
	if ([self isWebServiceMethod])
		[self parseResponse:[self responseText]];
	
	// clear the response
	[self setResponseText:nil];

	[timeoutTimer setFireDate:[NSDate dateWithTimeIntervalSinceNow:15]];
}



#pragma mark -
#pragma mark NSXMLParser Delegate methods

//
// Start parsing the response
//
- (void)parseResponse:(NSString *)text
{
	// if we have no delegate there is no need to parse the results
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	if (!delegate)
	{
		//NSLog(@"No delegate set, not parsing response.");
		[pool drain];
		return;
	}
	[timeoutTimer setFireDate:[NSDate dateWithTimeIntervalSinceNow:15]];

	// Initialise the parser
	NSXMLParser *parser = [[NSXMLParser alloc] initWithData:[text dataUsingEncoding:NSUTF8StringEncoding]];
	
	// was there a parsing error?
	if (!parser)
	{
		//NSLog(@"Parsing SOAP Response failed for text: %@", text);
		[pool drain];
		return;
	}
	
	// Hold on to the parser and set ourselves as the delegate
	[parser setDelegate:self];
	
	// We don't need these features of the parser so disable them
	[parser setShouldProcessNamespaces:NO];
	[parser setShouldReportNamespacePrefixes:NO];
	[parser setShouldResolveExternalEntities:NO];

	// start the parsing
	[parser parse];
	[parser release];
	
	// is there a validation message hanging around?
	if (self.validationResult != nil)
	{
		[self sendValidationResult];
	}
	
	[pool drain];
}

//
// NSXMLParser Delegate - called when we encounter the start of an element. eg: <element>
//
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributes
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

	[timeoutTimer setFireDate:[NSDate dateWithTimeIntervalSinceNow:15]];

	// use the qualified name if we have one
	if (qualifiedName) elementName = qualifiedName;

	// this element indicates the start of a new block of stop information
	if ([elementName isEqualToString:@"StopInformation"])
	{
		// start a new stop and pass the tracker id to it
		NSMutableDictionary *s = [[NSMutableDictionary alloc] initWithCapacity:0];
		[self setCurrentStop:s];
		[[self currentStop] setObject:self.trackerID forKey:@"trackerID"];
		[s release];
	}

	
	// This element indicates the start of a new block of predictions or departures
	else if ([elementName isEqualToString:@"GetNextPredictedRoutesCollectionResult"])
	{
		// start the new predictions array
		NSMutableArray *p = [[NSMutableArray alloc] initWithCapacity:0];
		[self setCurrentPredictions:p];
		[self setScheduledPrediction:NO];
		[p release];
	}
	
	// This element indicates the start of a new block of scheduled departures
	else if ([elementName isEqualToString:@"GetSchedulesCollectionResult"])
	{
		// start the new predictions array'
		NSMutableArray *p = [[NSMutableArray alloc] initWithCapacity:0];
		[self setCurrentPredictions:p];
		[self setScheduledPrediction:YES];
		[p release];
	}
	
	// This element indicates the start of a new journey
	else if ([elementName isEqualToString:@"GetNextPredictedArrivalTimeAtStopsForTramNoResponse"] || [elementName isEqualToString:@"GetSchedulesForTripResult"])
	{
		// start a new journey
		JourneyStub *j = [[JourneyStub alloc] init];
		[self setCurrentJourney:j];
		[j release];

		// start the list of stops
		NSMutableArray *s = [[NSMutableArray alloc] initWithCapacity:0];
		[self setStopList:s];
		[s release];
	}
	
	// This element indicates the start of a tram on a journey
	else if ([elementName isEqualToString:@"TramNoRunDetailsTable"])
	{
		// start a new tram
		TramStub *t = [[TramStub alloc] init];
		[self setCurrentTram:t];
		[t release];
	}
	
	// This element indicates the start of a stop that a tram on a journey will make
	else if ([elementName isEqualToString:@"NextPredictedStopsDetailsTable"] || [elementName isEqualToString:@"Table"])
	{
		// start one
		JourneyStopStub *s = [[JourneyStopStub alloc] init];
		[self setCurrentJourneyStop:s];
		[s release];
	}


	// This element indicates the start of a new prediction or scheduled departure
	else if ([elementName isEqualToString:@"ToReturn"] || [elementName isEqualToString:@"SchedulesResultsTable"])
	{
		// start the new prediction element
		PredictionStub *p = [[PredictionStub alloc] init];
		[self setCurrentPrediction:p];
		[p release];
		
		// start a new tram element
		TramStub *t = [[TramStub alloc] init];
		[self setCurrentTram:t];
		[t release];
	}
	
	// This element indicates the start of a new bit of a route
	else if ([elementName isEqualToString:@"ListOfDestinationsForAllRoutes"])
	{
		// just mark that we're working on a route destination
		RoutePart *r = [[RoutePart alloc] init];
		[self setCurrentRoute:r];
		[r release];
	}
	
	// The element means we're looking for a list of routes
	else if ([elementName isEqualToString:@"GetDestinationsForAllRoutesResult"])
	{
		// and make sure that we have a list of routes open
		NSMutableDictionary *d = [[NSMutableDictionary alloc] initWithCapacity:0];
		[self setRouteList:d];
		[d release];
	}
	
	// The element means we're looking for a list of stops
	else if ([elementName isEqualToString:@"GetListOfStopsByRouteNoAndDirectionResult"])
	{
		// start the list
		NSMutableArray *s = [[NSMutableArray alloc] initWithCapacity:0];
		[self setStopList:s];
		[s release];
		
		// start a turn list too
		NSMutableArray *t = [[NSMutableArray alloc] initWithCapacity:0];
		[self setTurnList:t];
		[t release];
	}
	
	// This element means we're looking at a route list for a specific stop
	else if ([elementName isEqualToString:@"GetMainRoutesForStopResult"])
	{
		// start the list
		NSMutableArray *r = [[NSMutableArray alloc] initWithCapacity:0];
		[self setRoutesThroughStop:r];
		[r release];
	}
	
	// A current Stop
	else if ([elementName isEqualToString:@"S"])
	{
		NSMutableDictionary *s = [[NSMutableDictionary alloc] initWithCapacity:0];
		[self setCurrentStop:s];
		[s release];
		
		NSMutableDictionary *t = [[NSMutableDictionary alloc] initWithCapacity:0];
		[self setCurrentTurn:t];
		[t release];
	}
	
	// This element indicates the start of a list of updates
	else if ([elementName isEqualToString:@"GetStopsAndRoutesUpdatesSinceResult"])
	{
		// Start the list
		//NSLog(@"[PSD] Found update element start");
		NSMutableArray *list = [[NSMutableArray alloc] initWithCapacity:0];
		[self setUpdateList:list];
		[list release];
	}
	
	// This element indicates the start of a stop update
	else if ([elementName isEqualToString:@"dtStopsChanges"])
	{
		// Start the stop update
		StopUpdate *u = [[StopUpdate alloc] init];
		[self setCurrentStopUpdate:u];
		[u release];
	}
	
	// This element indicates the start of a route update
	else if ([elementName isEqualToString:@"dtRoutesChanges"])
	{
		// Start the route update
		RouteUpdate *u = [[RouteUpdate alloc] init];
		[self setCurrentRouteUpdate:u];
		[u release];
	}
	
	// This element indicates the start of route destinations
	else if ([elementName isEqualToString:@"GetDestinationsForRouteResult"])
	{
		// start the destinations list
		NSMutableDictionary *d = [[NSMutableDictionary alloc] initWithCapacity:0];
		[self setCurrentRouteDestinations:d];
		[d release];
	}
	
	
	// These elements contain just text that is used to compile results
	else if ([elementName isEqualToString:@"validationResult"] || [elementName isEqualToString:@"GetNewClientGuidResult"] ||
			 [elementName isEqualToString:@"RouteNo"] || [elementName isEqualToString:@"HeadboardRouteNo"] ||
			 [elementName isEqualToString:@"Destination"] || [elementName isEqualToString:@"HasDisruption"] ||
			 [elementName isEqualToString:@"IsTTAvailable"] || [elementName isEqualToString:@"IsLowFloorTram"] ||
			 [elementName isEqualToString:@"SpecialEventMessage"] || [elementName isEqualToString:@"PredictedArrivalDateTime"] ||
			 [elementName isEqualToString:@"AirConditioned"] || [elementName isEqualToString:@"DisplayAC"] ||
			 [elementName isEqualToString:@"RequestDateTime"] || [elementName isEqualToString:@"FlagStopNo"] ||
			 [elementName isEqualToString:@"StopName"] || [elementName isEqualToString:@"CityDirection"] ||
			 [elementName isEqualToString:@"Latitude"] || [elementName isEqualToString:@"Longitude"] ||
			 [elementName isEqualToString:@"SuburbName"] || [elementName isEqualToString:@"UpStop"] ||
			 [elementName isEqualToString:@"TID"] || [elementName isEqualToString:@"VehicleNo"] ||
			 [elementName isEqualToString:@"Up"] || [elementName isEqualToString:@"StopNo"] ||
			 [elementName isEqualToString:@"HeadBoardRouteNo"] || [elementName isEqualToString:@"Distance"] ||
			 [elementName isEqualToString:@"AtLayover"] || [elementName isEqualToString:@"Down"] ||
			 [elementName isEqualToString:@"Available"] || [elementName isEqualToString:@"HasSpecialEvent"] ||
			 [elementName isEqualToString:@"StopSeq"] || [elementName isEqualToString:@"Action"] ||
			 [elementName isEqualToString:@"IsCityStop"] || [elementName isEqualToString:@"HasConnectingBuses"] ||
			 [elementName isEqualToString:@"HasConnectingTrains"] || [elementName isEqualToString:@"HasConnectingTrams"] ||
			 [elementName isEqualToString:@"StopLength"] || [elementName isEqualToString:@"IsPlatformStop"] ||
			 [elementName isEqualToString:@"Zones"] || [elementName isEqualToString:@"ID"] ||
			 [elementName isEqualToString:@"IsMainRoute"] || [elementName isEqualToString:@"TripID"] ||
			 [elementName isEqualToString:@"UpDestination"] || [elementName isEqualToString:@"DownDestination"] ||
			 [elementName isEqualToString:@"TurnType"] || [elementName isEqualToString:@"TurnMessage"] ||
			 [elementName isEqualToString:@"ScheduledArrivalDateTime"] || [elementName isEqualToString:@"Colour"] ||
             [elementName isEqualToString:@"ServerTime"])
	{
		NSString *s = [[NSString alloc] init];
		[self setCurrentProperty:s];
		[s release];
	}
		 
	// Make sure that we're not capturing random data into the property by disabling it when not in use
	else
	{
		[self setCurrentProperty:nil];
	}
    
    [pool drain]; pool = nil;
}

//
// NSXMLParser Delegate - called when we reach the end of an element. eg: </element>
//
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

	[timeoutTimer setFireDate:[NSDate dateWithTimeIntervalSinceNow:15]];

	// use the qualified name if specified
	if (qualifiedName) elementName = qualifiedName;
	
	// if we hit a validation result we've made an error somewhere
	if ([elementName isEqualToString:@"validationResult"])
	{
		// its not unusual for it to be blank and have no error
		if ([[self currentProperty] length] > 0)
		{
			// log the error and abort everything
			//NSLog(@"An error occured: %@", [self currentProperty]);
			[self setValidationResult:self.currentProperty];
			[self setCurrentProperty:nil];
		}
	}
	
	// we've received a Guid result from the service, pass it to the delegate
	else if ([elementName isEqualToString:@"GetNewClientGuidResult"])
	{
		// check that our delegate respondes to the selector
		if ([[self delegate] respondsToSelector:@selector(setNewClientGuid:)])
		{
			// pass it to the delegate
			[[self delegate] performSelector:@selector(setNewClientGuid:) withObject:[self currentProperty]];

		} else
		{
			// Nope, log it
			//NSLog(@"Found Guid %@, but delegate %@ does not implement selector setNewClientGuid:", [self currentProperty], [self delegate]);
		}
		[timeoutTimer invalidate];
		[self setCurrentProperty:nil];
	}
	
	
	// we've recieved a set of predictions, pass it to the delegate
	else if ([elementName isEqualToString:@"GetNextPredictedRoutesCollectionResult"])
	{
		// check that our delegate is setup to receive the predictions
		if ([[self delegate] respondsToSelector:@selector(setPredictions:)])
		{
			// pass it to the delegate
			[[self delegate] performSelectorOnMainThread:@selector(setPredictions:) withObject:self.currentPredictions waitUntilDone:NO];
			//[[self delegate] performSelector:@selector(setPredictions:forStop:) withObject:[self currentPredictions] withObject:[self trackerID]];
		} else
		{
			// Nope, log it
			//NSLog(@"Found Predictions %@, but delegate %@ does not implement selector setPredictions:", [self currentPredictions], [self delegate]);
		}
		[timeoutTimer invalidate];
		[self setCurrentPredictions:nil];
	}

	// we've recieved a set of departures, pass it to the delegate
	else if ([elementName isEqualToString:@"GetSchedulesCollectionResult"])
	{
		// check that our delegate is setup to receive the predictions
		if ([[self delegate] respondsToSelector:@selector(setDepartures:)])
		{
			// pass it to the delegate
			[[self delegate] performSelectorOnMainThread:@selector(setDepartures:) withObject:self.currentPredictions waitUntilDone:NO];
			//[[self delegate] performSelector:@selector(setDepartures:) withObject:[self currentPredictions] withObject:[self trackerID]];
		} else
		{
			// Nope, log it
			//NSLog(@"Found Departures %@, but delegate %@ does not implement selector setDepartures:", [self currentPredictions], [self delegate]);
		}
		[timeoutTimer invalidate];
		[self setCurrentPredictions:nil];
	}
	
	// we've received a journey, pass it to the delegate
	else if ([elementName isEqualToString:@"GetNextPredictedArrivalTimeAtStopsForTramNoResponse"])
	{
		// grab the stop list into the journey
		[self.currentJourney setStubStops:self.stopList];
		
		// if there is a validation error then clear out the results (nil result send to delegate)
		if (self.validationResult != nil && [self.validationResult length] > 0)
			[self setCurrentJourney:nil];
		
		// check that our  delegate is setup to receive the journey
		if ([[self delegate] respondsToSelector:@selector(setJourney:)])
		{
			// pass it to the delegate
			[[self delegate] performSelectorOnMainThread:@selector(setJourney:) withObject:self.currentJourney waitUntilDone:NO];
			//[[self delegate] performSelector:@selector(setJourney:forTramNumber:) withObject:self.currentJourney withObject:self.currentJourney.tram.number];
		} else
		{
			// Nope, log it
			//NSLog(@"Found Journey %@, but delegate %@ does not implement selector setJourney:", [self currentJourney], [self delegate]);
		}
		[timeoutTimer invalidate];
		[self setCurrentJourney:nil];
		[self setStopList:nil];
	}
	
	// we've received a scheduled journey, pass it to the delegate
	else if ([elementName isEqualToString:@"GetSchedulesForTripResult"])
	{
		[self.currentJourney setStubStops:self.stopList];
		[self.currentJourney setTramStub:nil];
		
		// check that our delegate is setup to receive the journey
		if ([self.delegate respondsToSelector:@selector(setScheduledJourney:)])
		{
			[self.delegate performSelectorOnMainThread:@selector(setScheduledJourney:) withObject:self.currentJourney waitUntilDone:NO];
		} else
		{
			// Nope, log it
			//NSLog(@"Found scheduled journey %@, but delegate %@ does not implement selector setScheduledJourney:", self.currentJourney, self.delegate);
		}
		[timeoutTimer invalidate];
		[self setCurrentJourney:nil];
		[self setStopList:nil];
	}
	
	// we've received some stop information, pass it to the delegate
	else if ([elementName isEqualToString:@"StopInformation"])
	{
		// check that our delegate is setup to receive the information
		if ([[self delegate] respondsToSelector:@selector(setInformation:)])
		{
			// pass it to the delegate
			[[self delegate] performSelector:@selector(setInformation:) withObject:self.currentStop];
			//[[self delegate] performSelector:@selector(setInformation:forStop:) withObject:[self currentStop] withObject:[self trackerID]];
		} else
		{
			// Nope, log it
			//NSLog(@"Found Stop Information %@, but delegate %@ does not implement selector setInformation:", [self currentStop], [self delegate]);
		}
		[timeoutTimer invalidate];
		[self setCurrentStop:nil];
	}
	
	//
	// If we have a Prediction open check to see if its one of the prediction elements
	//
	else if ([self currentPrediction])
	{
		// is this prediction finished? add it to the list if it is
		if ([elementName isEqualToString:@"ToReturn"] || [elementName isEqualToString:@"SchedulesResultsTable"])
		{
			// offset the predictions by comparing the server's request time and ours.
			if ([[self currentPrediction] requestDateTime] && [[self currentPrediction] predictedArrivalDateTime])
			{
				// offset the predicted arrival based on the difference between their time and ours
				if (![self isScheduledPrediction])
				{
					[[self currentPrediction] setPredictedArrivalDateTime:[self
																		   calculateLocalPredictedArrivalTimeFromServerTime:[[self currentPrediction] predictedArrivalDateTime]
																									  withServerRequestTime:[[self currentPrediction] requestDateTime]
																									   withLocalRequestTime:[self requestTime]]];
				} else
				{
					[[self currentPrediction] setPredictedArrivalDateTime:[self
																		   calculateLocalScheduledDepartureTimeFromServerTime:[[self currentPrediction] predictedArrivalDateTime]
																									    withServerRequestTime:[[self currentPrediction] requestDateTime]
																										 withLocalRequestTime:[self requestTime]]];
				}

			}
			// copy the tracker ID into the results
			if (self.trackerID != nil)
				[self.currentPrediction setStopStub:[self stopForTrackerID:self.trackerID]];
			
			// set the tram on the prediction
			[self.currentPrediction setTramStub:self.currentTram];
			[self setCurrentTram:nil];
			[[self currentPredictions] addObject:[self currentPrediction]];
			[self setCurrentPrediction:nil];
		}
		
		// otherwise, we if we have a text element currently open
		else if ([self currentProperty])
		{
			
			// The route number that the tram is operating on
			if ([elementName isEqualToString:@"RouteNo"])
			{
				[[self currentPrediction] setRouteNo:[self currentProperty]];
				[self.currentTram setRouteStub:[self routeWithNumber:[self currentProperty]]];
				[self setCurrentProperty:nil];
			}
			
			// The Route Number on the headboard of the tram
			else if ([elementName isEqualToString:@"HeadboardRouteNo"])
			{
				[[self currentPrediction] setHeadboardRouteNumber:[self currentProperty]];
				[self.currentTram setHeadboardRouteNumber:self.currentProperty];
				[self setCurrentProperty:nil];
			}
			
			// The Destination
			else if ([elementName isEqualToString:@"Destination"])
			{
				[[self currentPrediction] setDestination:[self currentProperty]];
				[self setCurrentProperty:nil];
			}
			
			// Whether the service is disrupted
			else if ([elementName isEqualToString:@"HasDisruption"])
			{
				[[self currentPrediction] setDisrupted:[[self currentProperty] isEqualToString:@"true"]];
				[self setCurrentProperty:nil];
			}

			// Whether the service is a low floor tram
			else if ([elementName isEqualToString:@"IsLowFloorTram"])
			{
				[[self currentPrediction] setLowFloor:[[self currentProperty] isEqualToString:@"true"]];
				[self setCurrentProperty:nil];
			}
			
			// Whether tramtracker is available at that stop (if it is then the disruption isn't disabling)
			else if ([elementName isEqualToString:@"IsTTAvailable"])
			{
				[[self currentPrediction] setTramTrackerAvailable:[[self currentProperty] isEqualToString:@"true"]];
				[self setCurrentProperty:nil];
			}
			
			// Whether the tram has air conditioning installed
			else if ([elementName isEqualToString:@"AirConditioned"])
			{
				[[self currentPrediction] setAirConditioned:[[self currentProperty] isEqualToString:@"true"]];
				[self setCurrentProperty:nil];
			}
			
			// Whether the tram is likely to have the air conditioning turned on
			else if ([elementName isEqualToString:@"DisplayAC"])
			{
				[[self currentPrediction] setDisplayAirConditioning:[[self currentProperty] isEqualToString:@"true"]];
				[self setCurrentProperty:nil];
			}
			
			// The special event message, if there is one
			else if ([elementName isEqualToString:@"SpecialEventMessage"])
			{
				[[self currentPrediction] setSpecialEventMessage:[self currentProperty]];
				[self setCurrentProperty:nil];
			}
			
			// Whether this prediction's special event message is actually relevant to itself
			else if ([elementName isEqualToString:@"HasSpecialEvent"])
			{
				[[self currentPrediction] setHasSpecialEvent:[self.currentProperty isEqualToString:@"true"]];
				[self setCurrentProperty:nil];
			}
			
			// The Predicted Arrival Time
			else if ([elementName isEqualToString:@"PredictedArrivalDateTime"])
			{
				[[self currentPrediction] setPredictedArrivalDateTime:[self parseDateTime:[self currentProperty]]];
				[self setCurrentProperty:nil];
			}
			
			// The date and time of the request
			else if ([elementName isEqualToString:@"RequestDateTime"])
			{
				[[self currentPrediction] setRequestDateTime:[self parseDateTime:[self currentProperty]]];
				[self setCurrentProperty:nil];
			}
			
			// The tram's vehicle number
			else if ([elementName isEqualToString:@"VehicleNo"])
			{
				[self.currentTram setNumber:[NSNumber numberWithInteger:[self.currentProperty integerValue]]];
				[self setCurrentProperty:nil];
			}
			
			// The distance the tram is along
			else if ([elementName isEqualToString:@"Distance"])
			{
				[[self currentPrediction] setDistance:[self.currentProperty doubleValue]];
				[self setCurrentProperty:nil];
			}
			
			// The trip id
			else if ([elementName isEqualToString:@"TripID"])
			{
				[self.currentPrediction setTripID:[NSNumber numberWithInteger:[self.currentProperty integerValue]]];
				[self setCurrentProperty:nil];
			}
		}
	}

	// working on a journey
	else if ([self currentJourney])
	{
		// found the end of a tram result? assign it to the journey
		if ([elementName isEqualToString:@"TramNoRunDetailsTable"])
		{
			// check to see if the headboard route number is different to the regular one, it might mean a subroute
			[self.currentTram setRouteStub:[self routeWithNumber:self.currentTram.routeNumber headboardRouteNumber:self.currentTram.headboardRouteNumber]];

			[self.currentJourney setTramStub:self.currentTram];
			[self setCurrentTram:nil];
		}
		
		// found the end of a journey stop? assign it to the journey
		else if ([elementName isEqualToString:@"NextPredictedStopsDetailsTable"] || [elementName isEqualToString:@"Table"])
		{
			if (self.currentJourneyStop != nil)
			{
				[self.stopList addObject:self.currentJourneyStop];
				[self setCurrentJourneyStop:nil];
			}
		}
		
		// run details
		else if ([self currentTram])
		{
			// vehicle number
			if ([elementName isEqualToString:@"VehicleNo"])
			{
				[self.currentTram setNumber:[NSNumber numberWithInteger:[self.currentProperty integerValue]]];
				[self setCurrentProperty:nil];
			}
			
			// route number
			else if ([elementName isEqualToString:@"RouteNo"])
			{
				[self.currentTram setRouteNumber:self.currentProperty];
				[self setCurrentProperty:nil];
			}
			
			// headboard route number
			else if ([elementName isEqualToString:@"HeadBoardRouteNo"])
			{
				[self.currentTram setHeadboardRouteNumber:self.currentProperty];
				[self setCurrentProperty:nil];
			}
			
			// direction
			else if ([elementName isEqualToString:@"Up"])
			{
				[self.currentTram setUpDirection:[self.currentProperty isEqualToString:@"true"]];
				[self setCurrentProperty:nil];
			}
			else if ([elementName isEqualToString:@"Down"])
			{
				[self.currentTram setUpDirection:[self.currentProperty isEqualToString:@"false"]];
				[self setCurrentProperty:nil];
			}
			
			// at layover?
			else if ([elementName isEqualToString:@"AtLayover"])
			{
				[self.currentTram setAtLayover:[self.currentProperty isEqualToString:@"true"]];
				[self setCurrentProperty:nil];
			}
			
			// is it a non-public trip?
			else if ([elementName isEqualToString:@"Available"])
			{
				[self.currentTram setAvailable:[self.currentProperty isEqualToString:@"true"]];
				[self setCurrentProperty:nil];

			// is it affected by a special event?
			} else if ([elementName isEqualToString:@"HasSpecialEvent"])
			{
				[self.currentTram setSpecialEvent:[self.currentProperty isEqualToString:@"true"]];
				[self setCurrentProperty:nil];
			}
			
			// is it affected by a disruption?
			else if ([elementName isEqualToString:@"HasDisruption"])
			{
				[self.currentTram setDisrupted:[self.currentProperty isEqualToString:@"true"]];
				[self setCurrentProperty:nil];
			}
		}
		
		// stop details
		else if ([self currentJourneyStop])
		{
			// stop number
			if ([elementName isEqualToString:@"StopNo"])
			{
				// the final stop on the route will have a zero trackerid - find eet!
				if ([self.currentProperty isEqualToString:@"0"])
				{
					TramStub *tram = self.currentJourney.tramStub;
                    Route *r = [Route routeForStub:tram.routeStub];
					NSArray *stops = tram.upDirection ? r.upStops : r.downStops;
					[self.currentJourneyStop setStopStub:[self stopForTrackerID:[stops objectAtIndex:[stops count]-1]]];
				} else
				{
					[self.currentJourneyStop setStopStub:[self stopForTrackerID:[NSNumber numberWithInteger:[self.currentProperty integerValue]]]];
				}
				
				[self setCurrentProperty:nil];
			}
			
			// predicted arrival date time
			else if ([elementName isEqualToString:@"PredictedArrivalDateTime"] || [elementName isEqualToString:@"ScheduledArrivalDateTime"])
			{
				NSDate *d = [self parseDateTime:self.currentProperty];
				[self.currentJourneyStop setPredicatedArrivalDateTime:d];
				[self.currentJourneyStop setOriginalPredictedArrivalDateTime:d];
				[self setCurrentProperty:nil];
			}
			
			// stop sequence - this is here purely to remove the 'previous stops'
			else if ([elementName isEqualToString:@"StopSeq"])
			{
				if ([self.currentProperty isEqualToString:@"Prev"])
				{
					// it is, kill off this stop
					[self setCurrentJourneyStop:nil];
					[self setCurrentProperty:nil];
				} else
				{
					[self setCurrentProperty:nil];
				}
			}
		}
	}
	
	//
	// Working on a stop list!
	//
	else if ([self stopList])
	{
		// found a stop, add it to the list
		if ([elementName isEqualToString:@"S"])
		{
			[self.stopList addObject:[self.currentStop objectForKey:@"trackerid"]];
			if ([self.currentTurn count] > 0) [self.turnList addObject:self.currentTurn];
			[self setCurrentStop:nil];
			[self setCurrentTurn:nil];

		} else if (self.currentStop != nil)
		{
			// tracker id
			if ([elementName isEqualToString:@"TID"])
			{
				[self.currentStop setObject:[NSNumber numberWithInteger:[self.currentProperty integerValue]] forKey:@"trackerid"];
				if ([self.currentTurn count] > 0)
					[self.currentTurn setObject:[NSNumber numberWithInteger:[self.currentProperty integerValue]] forKey:@"trackerid"];

				[self setCurrentProperty:nil];
			}
			
			else if ([elementName isEqualToString:@"TurnType"])
			{
				if ([self.currentTurn objectForKey:@"trackerid"] == nil && [self.currentStop objectForKey:@"trackerid"] != nil)
					[self.currentTurn setObject:[self.currentStop objectForKey:@"trackerid"] forKey:@"trackerid"];

				NSNumber *turnType = 0;
				if ([self.currentProperty isEqualToString:@"left"])
					turnType = [NSNumber numberWithInteger:TTTurnLeft];
				else if ([self.currentProperty isEqualToString:@"right"])
					turnType = [NSNumber numberWithInteger:TTTurnRight];
				else if ([self.currentProperty isEqualToString:@"straight"])
					turnType = [NSNumber numberWithInteger:TTTurnStraight];
				else if ([self.currentProperty isEqualToString:@"s_to_right"])
					turnType = [NSNumber numberWithInteger:TTTurnSRight];
				else if ([self.currentProperty isEqualToString:@"veer_left"])
					turnType = [NSNumber numberWithInteger:TTTurnVeerLeft];
				else if ([self.currentProperty isEqualToString:@"veer_right"])
					turnType = [NSNumber numberWithInteger:TTTurnVeerRight];
				[self.currentTurn setObject:turnType forKey:@"type"];
				[self setCurrentProperty:nil];

			
			} else if ([elementName isEqualToString:@"TurnMessage"])
			{
				if ([self.currentTurn objectForKey:@"trackerid"] == nil && [self.currentStop objectForKey:@"trackerid"] != nil)
					[self.currentTurn setObject:[self.currentStop objectForKey:@"trackerid"] forKey:@"trackerid"];

				[self.currentTurn setObject:self.currentProperty forKey:@"message"];
				[self setCurrentProperty:nil];
			}
		}
		
		// finished the list, close it off
		else if ([elementName isEqualToString:@"GetListOfStopsByRouteNoAndDirectionResult"])
		{
			NSNumber *up = [upDirection copy];
			NSString *r = [routeNumber copy];
			
			// check that our delegate is setup to receive the predictions
			SEL selector = @selector(setStopList:forRouteNumber:upDirection:);
			if ([self.delegate respondsToSelector:selector])
			{
				NSMethodSignature *sig = [[self.delegate class] instanceMethodSignatureForSelector:selector];
				NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:sig];
				[invocation setSelector:selector];
				[invocation setArgument:&stopList atIndex:2];
				[invocation setArgument:&r atIndex:3];
				[invocation setArgument:&up atIndex:4];
				[invocation invokeWithTarget:self.delegate];
			} else
			{
				// Nope, log it
				//NSLog(@"Found Stop List %@, but delegate %@ does not implement selector setStopList:forRoute:", self.stopList, [self delegate]);
			}
			[timeoutTimer invalidate];
			[self setStopList:nil];

			// check that our delegate is setup to receive the turns
			selector = @selector(setTurnList:forRouteNumber:upDirection:);
			if ([self.delegate respondsToSelector:selector])
			{
				NSMethodSignature *sig = [[self.delegate class] instanceMethodSignatureForSelector:selector];
				NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:sig];
				[invocation setSelector:selector];
				[invocation setArgument:&turnList atIndex:2];
				[invocation setArgument:&r atIndex:3];
				[invocation setArgument:&up atIndex:4];
				[invocation invokeWithTarget:self.delegate];
			} else
			{
				// Nope, log it
				//NSLog(@"Found Stop List %@, but delegate %@ does not implement selector setStopList:forRoute:", self.stopList, [self delegate]);
			}
			[timeoutTimer invalidate];
			[self setTurnList:nil];
			[self setUpDirection:nil];
			[r release];
			[up release];
		}
	}
	
	// if we have a routes through stop list open
	else if ([self routesThroughStop])
	{
		// Add a route number to it
		if ([elementName isEqualToString:@"RouteNo"])
		{
			[self.routesThroughStop addObject:self.currentProperty];
			[self setCurrentProperty:nil];
		}
		
		
		// finished the list? send it to the delegate
		else if ([elementName isEqualToString:@"GetMainRoutesForStopResult"])
		{
			SEL selector = @selector(setRoutes:throughStop:);
			if ([self.delegate respondsToSelector:selector])
			{
                NSArray *stopsArray = [self.routesThroughStop copy];
				[self.delegate performSelector:selector
                                    withObject:stopsArray
                                    withObject:self.trackerID];
                
                //[self.delegate performSelector:selector
                //                    withObject:self.routesThroughStop
                //                    withObject:self.trackerID];
			} else
			{
				//NSLog(@"Found routes %@ through stop %@, but delegate %@ does not implement selector setRoutes:throughStop:", self.routesThroughStop, self.trackerID, self.delegate);
			}
			[timeoutTimer invalidate];
			[self setRoutesThroughStop:nil];
		}
	}

	//
	// If we have a current stop open check for its child elements
	//
	else if ([self currentStop])
	{
		// the stop number as listed on the flag
		if ([elementName isEqualToString:@"FlagStopNo"])
		{
			[self.currentStop setObject:self.currentProperty forKey:@"number"];
			[self setCurrentProperty:nil];
		}

		// the name of the stop
		else if ([elementName isEqualToString:@"StopName"])
		{
			[self.currentStop setObject:self.currentProperty forKey:@"name"];
			[self setCurrentProperty:nil];
		}

		// The direction the tram would head relative to the city
		else if ([elementName isEqualToString:@"CityDirection"])
		{
			[self.currentStop setObject:self.currentProperty forKey:@"cityDirection"];
			[self setCurrentProperty:nil];
		}
		
		// The Latitude of the stop
		else if ([elementName isEqualToString:@"Latitude"])
		{
			[self.currentStop setObject:[NSDecimalNumber decimalNumberWithString:self.currentProperty] forKey:@"latitude"];
			[self setCurrentProperty:nil];
		}
		
		// The Longitude of the stop
		else if ([elementName isEqualToString:@"Longitude"])
		{
			[self.currentStop setObject:[NSDecimalNumber decimalNumberWithString:self.currentProperty] forKey:@"longitude"];
			[self setCurrentProperty:nil];
		}

		// The suburb that this stop is located in
		else if ([elementName isEqualToString:@"SuburbName"])
		{
			[self.currentStop setObject:self.currentProperty forKey:@"suburbName"];
			[self setCurrentProperty:nil];
		}
		
		// Whether or not this is a city stop
		else if ([elementName isEqualToString:@"IsCityStop"])
		{
			[self.currentStop setObject:[NSNumber numberWithBool:([self.currentProperty isEqualToString:@"true"])] forKey:@"cityStop"];
			[self setCurrentProperty:nil];
		}

		// Whether or not this is a platform
		else if ([elementName isEqualToString:@"IsPlatformStop"])
		{
			[self.currentStop setObject:[NSNumber numberWithBool:([self.currentProperty isEqualToString:@"true"])] forKey:@"platformStop"];
			[self setCurrentProperty:nil];
		}

		// Whether or not this has connecting trains
		else if ([elementName isEqualToString:@"HasConnectingTrains"])
		{
			[self.currentStop setObject:[NSNumber numberWithBool:([self.currentProperty isEqualToString:@"true"])] forKey:@"connectingTrains"];
			[self setCurrentProperty:nil];
		}

		// Whether or not this has connecting trams
		else if ([elementName isEqualToString:@"HasConnectingTrams"])
		{
			[self.currentStop setObject:[NSNumber numberWithBool:([self.currentProperty isEqualToString:@"true"])] forKey:@"connectingTrams"];
			[self setCurrentProperty:nil];
		}

		// Whether or not this has connecting buses
		else if ([elementName isEqualToString:@"HasConnectingBuses"])
		{
			[self.currentStop setObject:[NSNumber numberWithBool:([self.currentProperty isEqualToString:@"true"])] forKey:@"connectingBuses"];
			[self setCurrentProperty:nil];
		}

		// Length of the stop
		else if ([elementName isEqualToString:@"StopLength"])
		{
			[self.currentStop setObject:[NSNumber numberWithInteger:[self.currentProperty integerValue]] forKey:@"length"];
			[self setCurrentProperty:nil];
		}
		
		// Zones
		else if ([elementName isEqualToString:@"Zones"])
		{
			[self.currentStop setObject:self.currentProperty forKey:@"zones"];
			[self setCurrentProperty:nil];
		}
	}
	
	//
	// Are we working on a route
	//
	else if ([self currentRoute])
	{
		// The end of the current route, merge it back into the route list
		if ([elementName isEqualToString:@"ListOfDestinationsForAllRoutes"])
		{
			// does this route number exist already in the list?
			if ([self.routeList objectForKey:[self.currentRoute number]] == nil)
				[self.routeList setObject:[[[Route alloc] init] autorelease] forKey:[self.currentRoute number]];

			// set update the properties on the route
			[[self.routeList objectForKey:[self.currentRoute number]] setNumber:[self.currentRoute number]];
			
			// set the destination depending on the direction
			if ([self.currentRoute isUpPart])
				[[self.routeList objectForKey:[self.currentRoute number]] setUpDestination:[self.currentRoute destination]];
			else
				[[self.routeList objectForKey:[self.currentRoute number]] setDownDestination:[self.currentRoute destination]];

			// get rid of the route part
			[self setCurrentRoute:nil];
		}
		
		// The Route Number
		else if ([elementName isEqualToString:@"RouteNo"])
		{
			// set the route number on the object
			[self.currentRoute setNumber:[self currentProperty]];
			[self setCurrentProperty:nil];
		}
		
		// Whether this Route Part is for heading up or down
		else if ([elementName isEqualToString:@"UpStop"])
		{
			[self.currentRoute setIsUpPart:[self.currentProperty isEqualToString:@"true"]];
			[self setCurrentProperty:nil];
		}
		
		// The destination for this route part
		else if ([elementName isEqualToString:@"Destination"])
		{
			[self.currentRoute setDestination:[self currentProperty]];
			[self setCurrentProperty:nil];
		}
	}
	
	//
	// Are we working on a route list, pass it to the delegate
	//
	else if ([self routeList] && [elementName isEqualToString:@"GetDestinationsForAllRoutesResult"])
	{
		// Loop through the route list and make sure that its updated
		for (id key in [self routeList])
			[[self.routeList objectForKey:key] updateName];

		// order the route list
		NSArray *routeArray = [[[self.routeList allValues] sortedArrayUsingSelector:@selector(compareRoute:)] retain];
		[self setRouteList:nil];
		
		// check that our delegate is setup to receive the predictions
		if ([[self delegate] respondsToSelector:@selector(setRouteList:)])
		{
			// pass it to the delegate
			[[self delegate] performSelector:@selector(setRouteList:) withObject:routeArray];
		} else
		{
			// Nope, log it
			//NSLog(@"Found Route List %@, but delegate %@ does not implement selector setRouteList:", routeArray, [self delegate]);
		}
		[timeoutTimer invalidate];
		[routeArray release];
	}
	
	
	
	//
	// Working on an updates List
	//
	else if ([self updateList])
	{
		// Found the end of our update list?
		if ([elementName isEqualToString:@"GetStopsAndRoutesUpdatesSinceResult"])
		{
			//NSLog(@"[PSD] Found update element end");
			// check that our delegate is setup to receive the list
			if ([self.delegate respondsToSelector:@selector(setUpdates:atDate:)])
			{
				// pass it to the delegate
				[self.delegate performSelector:@selector(setUpdates:atDate:) withObject:self.updateList withObject:self.serverTime];
			} else
			{
				// Nope, log it
				//NSLog(@"Found Update List %@ at %@, but delegate %@ does not implement selector setUpdates:withDate:", self.updateList, self.serverTime, self.delegate);
			}
			[timeoutTimer invalidate];
			[self setUpdateList:nil];
		}
        
        // is it the server time?
        else if ([elementName isEqualToString:@"ServerTime"])
        {
            [self setServerTime:[self parseDateTime:self.currentProperty]];
            [self setCurrentProperty:nil];
        }
		
		
		// Working on a route update
		else if ([self currentRouteUpdate])
		{
			// found a route change, add it to the list
			if ([elementName isEqualToString:@"dtRoutesChanges"])
			{
				[self.updateList addObject:self.currentRouteUpdate];
				[self setCurrentRouteUpdate:nil];
			}
			
			// Found the route number
			else if ([elementName isEqualToString:@"RouteNo"])
			{
				[self.currentRouteUpdate setRouteNumber:self.currentProperty];
				[self setCurrentProperty:nil];
			}
			
			// Found the Route ID
			else if ([elementName isEqualToString:@"ID"])
			{
				[self.currentRouteUpdate setInternalRouteNumber:self.currentProperty];
				[self setCurrentProperty:nil];
			}
			
			// Found the Headboard Route Number
			else if ([elementName isEqualToString:@"HeadboardRouteNo"])
			{
				[self.currentRouteUpdate setHeadboardRouteNumber:self.currentProperty];
				[self setCurrentProperty:nil];
			}
			
			// Found whether its a sub route or not
			else if ([elementName isEqualToString:@"IsMainRoute"])
			{
				[self.currentRouteUpdate setSubRoute:[self.currentProperty isEqualToString:@"false"]];
				[self setCurrentProperty:nil];
			}

            // Found the colour of the route
			else if ([elementName isEqualToString:@"Colour"])
			{
				[self.currentRouteUpdate setColour:self.currentProperty];
				[self setCurrentProperty:nil];
			}

			// Found the action type
			else if ([elementName isEqualToString:@"Action"])
			{
				PIDServiceActionType type = 0;
				if ([self.currentProperty isEqualToString:@"UPDATE"])
					type = PIDServiceActionTypeUpdate;
				else if ([self.currentProperty isEqualToString:@"DELETE"])
					type = PIDServiceActionTypeDelete;
				[self.currentRouteUpdate setActionType:type];
				[self setCurrentProperty:nil];
			}

		// Working on a stop update
		} else if ([self currentStopUpdate])
		{
			// found a stop change, add it to the list
			if ([elementName isEqualToString:@"dtStopsChanges"])
			{
				[self.updateList addObject:self.currentStopUpdate];
				[self setCurrentStopUpdate:nil];
			}
			
			// found the tracker id
			else if ([elementName isEqualToString:@"StopNo"])
			{
				[self.currentStopUpdate setTrackerID:[NSNumber numberWithInteger:[self.currentProperty integerValue]]];
				[self setCurrentProperty:nil];
			}
			
			// found the action type
			else if ([elementName isEqualToString:@"Action"])
			{
				PIDServiceActionType type = 0;
				if ([self.currentProperty isEqualToString:@"UPDATE"])
					type = PIDServiceActionTypeUpdate;
				else if ([self.currentProperty isEqualToString:@"DELETE"])
					type = PIDServiceActionTypeDelete;
				[self.currentStopUpdate setActionType:type];
				[self setCurrentProperty:nil];
			}
		}		
	}
	
	//
	// Working on a Destination List
	//
	else if ([self currentRouteDestinations])
	{
		// finished up, send it back to the delegate
		if ([elementName isEqualToString:@"GetDestinationsForRouteResult"])
		{
			// Check that the delegate responds
			if ([self.delegate respondsToSelector:@selector(setDestinations:forRouteNumber:)])
			{
				[self.delegate performSelector:@selector(setDestinations:forRouteNumber:) withObject:self.currentRouteDestinations withObject:self.routeNumber];
			} else
			{
				//NSLog(@"Found destinations %@, but delegate %@ does not respond to selector setDestinations:forRouteNumber:", self.currentRouteDestinations, self.delegate);
			}
			[self setCurrentRouteDestinations:nil];
		}
		
		// Found the up destination
		else if ([elementName isEqualToString:@"UpDestination"])
		{
			[self.currentRouteDestinations setObject:self.currentProperty forKey:@"up"];
			[self setCurrentProperty:nil];
		}
		
		// found the down destination
		else if ([elementName isEqualToString:@"DownDestination"])
		{
			[self.currentRouteDestinations setObject:self.currentProperty forKey:@"down"];
			[self setCurrentProperty:nil];
		}
	}
    
    [pool drain]; pool = nil;
}

//
// NSXMLParser Delegate - Called when we encounter a set of characters outside of an element
//
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
	// If we have a property open for reading add it to it
	if ([self currentProperty])
		[self setCurrentProperty:[self.currentProperty stringByAppendingString:string]];
}

//
// Abort parsing the XML string and cleanup
//
- (void)abortParsing:(NSXMLParser *)parser
{
	// aborting the parsing
	[parser abortParsing];
	[timeoutTimer invalidate];
}

#pragma mark -
#pragma mark Local methods

//
// Parse the ISO8601 date sent by the server
//
- (NSDate *)parseDateTime:(NSString *)dateString
{
    // The service will return dates in the format: yyyy-MM-ddThh:mm:ss[.SSSSSSS]+zz:zz (the milliseconds are optional)
    // Use the ISO8601 parser by Peter Hosey on 2009-04-11.
    
    // Check ISO8601DateFormatter.h for the license
    ISO8601DateFormatter *formatter = [[ISO8601DateFormatter alloc] init];
    NSDate *newDate = [formatter dateFromString:dateString];
    
    //if (newDate == nil)
        //NSLog(@"[PSD] Could not parse date string: %@", dateString);
	
    /*
	// Start by making a mutable string so we can wrangle the string to something we can parse
	NSMutableString *date = [NSMutableString stringWithString:dateString];

	// if it has milliseconds specified (the .SSSSSSS bit) throw those out - we can't parse them
	if ([[date substringWithRange:NSMakeRange(19, 1)] isEqualToString:@"."])
	{
		NSRange indexOfTZ = [date rangeOfString:@"+"];
		[date deleteCharactersInRange:NSMakeRange(19, indexOfTZ.location-19)];
	}

	// also toss out the colon in the timezone, we can't parse that either
	[date replaceOccurrencesOfString:@":"
						  withString:@""
							 options:NSLiteralSearch
							   range:NSMakeRange(20, [date length]-20)];

	// initialize the date formatter to read the input string
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZ"];
	
	// parse the date
	NSDate *newDate = [formatter dateFromString:date]; */
	[formatter release];
	
	// return the new date
	return newDate;
}


//
// The local time and server time is usually different, calculate the predicted arrival time against the local clock
// using the difference between the local request time and the server request time (should be accurate to a second or two)
//
- (NSDate *)calculateLocalPredictedArrivalTimeFromServerTime:(NSDate *)serverPredictedArrival withServerRequestTime:(NSDate *)serverRequestTime withLocalRequestTime:(NSDate *)localRequestTime
{
	// the timeDelta is the difference between our clock and the server clock (approximate)
	//NSLog(@"Server Request: %@, Local Request: %@, Server Arrival: %@", serverRequestTime, localRequestTime, serverPredictedArrival);
	NSTimeInterval timeDelta = [localRequestTime timeIntervalSinceDate:serverRequestTime];
	//NSLog(@"Time Delta: %f", timeDelta);
	
	// add the delta to the prediction to get the predicted arrival time according to our own clock
	NSDate *localPredictedArrival = ([serverPredictedArrival respondsToSelector:@selector(dateByAddingTimeInterval:)] ? [serverPredictedArrival dateByAddingTimeInterval:timeDelta] : [serverPredictedArrival dateByAddingTimeInterval:timeDelta]);
	//NSLog(@"Local Arrival: %@", localPredictedArrival);
	return localPredictedArrival;
}

- (NSDate *)calculateLocalScheduledDepartureTimeFromServerTime:(NSDate *)serverPredictedArrival withServerRequestTime:(NSDate *)serverRequestTime withLocalRequestTime:(NSDate *)localRequestTime
{
	// we only change it when the time zones are different between the two
	NSDateFormatter *timezoneFormatter = [[NSDateFormatter alloc] init];
	[timezoneFormatter setDateFormat:@"ZZZZ"];
	if ([[timezoneFormatter stringFromDate:localRequestTime] isEqualToString:[timezoneFormatter stringFromDate:serverRequestTime]])
	{
		[timezoneFormatter release];
		return serverPredictedArrival;
	}
	[timezoneFormatter release];

	// the timeDelta is the difference between our clock and the server clock (approximate)
	NSTimeInterval timeDelta = [serverRequestTime timeIntervalSinceDate:localRequestTime];
	
	// add the delta to the prediction to get the predicted arrival time according to our own clock
	NSDate *localPredictedArrival = ([serverPredictedArrival respondsToSelector:@selector(dateByAddingTimeInterval:)] ? [serverPredictedArrival dateByAddingTimeInterval:timeDelta] : [serverPredictedArrival dateByAddingTimeInterval:timeDelta]);
	return localPredictedArrival;
}

#pragma mark -
#pragma mark Core Data Retrieval Methods

- (NSManagedObjectContext *) managedObjectContext
{
    if (managedObjectContext != nil) {
        return managedObjectContext;
    }
	
    NSPersistentStoreCoordinator *coordinator = [(tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate] persistentStoreCoordinator];
    if (coordinator != nil) {
        if ([NSManagedObjectContext instancesRespondToSelector:@selector(initWithConcurrencyType:)])
            managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSConfinementConcurrencyType];
        else
            managedObjectContext = [[NSManagedObjectContext alloc] init];
        [managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return managedObjectContext;
}


- (StopStub *)stopForTrackerID:(NSNumber *)aTrackerID
{
    StopStub *stub = [[StopStub alloc] init];
    [stub setTrackerID:aTrackerID];
    return [stub autorelease];
}

- (RouteStub *)routeWithNumber:(NSString *)aRouteNumber
{
    RouteStub *stub = [[RouteStub alloc] init];
    [stub setNumber:aRouteNumber];
    return [stub autorelease];
}


- (RouteStub *)routeWithNumber:(NSString *)aRouteNumber headboardRouteNumber:(NSString *)aHeadboardRouteNumber
{
    RouteStub *stub = [[RouteStub alloc] init];
    [stub setNumber:aRouteNumber];
    [stub setHeadboardNumber:aHeadboardRouteNumber];
    return [stub autorelease];
}



#pragma mark -
#pragma mark Error Handling

// send a validation result error
- (void)sendValidationResult
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	// remove the validation stuff from the error message
	NSString *errorMessage;
	NSInteger validationErrorCode = PIDServiceValidationError;
	if ([self.validationResult rangeOfString:@"[VAL"].location != NSNotFound)
	{
		errorMessage = [self.validationResult substringFromIndex:[self.validationResult rangeOfString:@"]"].location+1];

		// see if we have a matching error code
		if ([self.validationResult rangeOfString:@"[VAL:006]"].location != NSNotFound)
			validationErrorCode = PIDServiceValidationErrorStopNotFound;
		else if ([self.validationResult rangeOfString:@"[VAL:013]"].location != NSNotFound)
			validationErrorCode = PIDServiceValidationErrorRouteNotFound;
	}
	else if ([self.validationResult rangeOfString:@"[ERR"].location != NSNotFound)
	{
		errorMessage = [self.validationResult substringFromIndex:[self.validationResult rangeOfString:@"]"].location+1];
		
		// see if we have a matching error code
		if ([self.validationResult rangeOfString:@"[ERR:003]"].location != NSNotFound)
			validationErrorCode = PIDServiceErrorNotReachable;
		else if ([self.validationResult rangeOfString:@"[ERR:005"].location != NSNotFound)
			validationErrorCode = PIDServiceErrorOnboardNotReachable;
	}
	else
		errorMessage = self.validationResult;

	// create a new error
	NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
										 code:validationErrorCode
									 userInfo:[NSDictionary dictionaryWithObject:errorMessage forKey:NSLocalizedDescriptionKey]];
	[self failWithError:error];
    [pool drain]; pool = nil;
}	

- (void)timeout:(NSTimer *)timer
{
	//NSLog(@"[PSD] Timeout reached.");
	[timer invalidate];
	// fail with an error
	[self failWithError:[NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
											code:PIDServiceErrorTimeoutReached
										userInfo:nil]];
}


- (void)failWithError:(NSError *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	// check the delegate to see if we can report the error
	if (self.delegate != nil && [self.delegate respondsToSelector:@selector(pidsServiceDidFailWithError:)])
	{
		[self.delegate performSelectorOnMainThread:@selector(pidsServiceDidFailWithError:) withObject:error waitUntilDone:NO];
	}
	[timeoutTimer invalidate];
}

- (void)dealloc
{
	//NSLog(@"PIDServiceDelegate dealloc'd: %@", self);

	[delegate release];
	[responseText release];
	[currentProperty release];
	[currentStop release];
	[currentPredictions release];
	[currentPrediction release];
	[trackerID release];
	[requestTime release];
	[updateSinceDate release];
	[routeList release];
	[currentRoute release];
	[stopList release];
	[turnList release];
	[currentTurn release];
	[currentJourney release];
	[currentTram release];
	[currentJourneyStop release];
	[updateList release];
	[currentStopUpdate release];
	[currentRouteUpdate release];
	[routeNumber release];
	[currentRouteDestinations release];
	[upDirection release];
	[routesThroughStop release];
	[timeoutTimer invalidate];
	[timeoutTimer release];
    [serverTime release]; serverTime = nil;
    [managedObjectContext release]; managedObjectContext = nil;
	
	[super dealloc];
}


@end
