//
//  RouteInfoView.h
//  tramTRACKER
//
//  Created by Robert Amos on 21/04/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import "TramImage.h"

@interface RouteInfoView : UIView {
	UILabel *routeNumberLabel;
	NSString *routeNumber;
	UILabel *destinationLabel;
	NSString *destination;
	UILabel *messageLabel;
	NSString *message;
	TramImage *tramIconView;
	UIImage *image;
	SystemSoundID soundID;
	BOOL isPlaying;
	BOOL badConnection;
	UIImageView *badConnectionImage;
}

@property (nonatomic, retain) NSString *routeNumber;
@property (nonatomic, retain) NSString *destination;
@property (nonatomic, retain) NSString *message;
@property (nonatomic, retain) UIImage *image;
@property (nonatomic, getter=hasBadConnection) BOOL badConnection;

- (UILabel *)newRouteNumberLabel;
- (UILabel *)newDestinationLabel;
- (UILabel *)newMessageLabel;
- (UIImageView *)newBadConnectionImage;
- (void)setRouteNumberName:(NSString *)newRouteNumberName;
- (void)setDirection:(NSString *)newDirection;
- (BOOL)isChristmas;
- (void)playGong;
- (void)setURLOfGongSound:(NSURL *)aURL;

@end
