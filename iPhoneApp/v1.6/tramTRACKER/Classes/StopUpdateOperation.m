//
//  StopUpdateOperation.m
//  tramTRACKER
//
//  Created by Robert Amos on 29/07/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "StopUpdateOperation.h"
#import "Stop.h"
#import "PidsService.h"
#import "BackgroundSynchroniser.h"

@implementation StopUpdateOperation

@synthesize syncManager, stop;

- (id)initWithStop:(Stop *)aStop
{
	if (self = [super init])
	{
		stop = aStop;
		[stop retain];
		
		service = [[PidsService alloc] init];
		[service setDelegate:self];
		
		[self willChangeValueForKey:@"isExecuting"];
		executing = NO;
		[self didChangeValueForKey:@"isExecuting"];
		
		[self willChangeValueForKey:@"isFinished"];
		finished = NO;
		[self didChangeValueForKey:@"isFinished"];
		
		// set our priority
		[self setQueuePriority:NSOperationQueuePriorityHigh];
	}
	return self;
}

- (BOOL)isConcurrent
{
	return NO;
}

- (BOOL)isExecuting
{
	return executing;
}

- (BOOL)isFinished
{
	return finished;
}

- (void)cancel
{
	[super cancel];
	//NSLog(@"Operation %@ cancelled.", self);
}

/**
 * Actually start processing the update
 **/
- (void)main
{
	[self start];
}
- (void)start
{
	//NSLog(@"[STOPUPDATE] Retrieiving stop info for stop %@", stop);
	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}

	// we start by getting the destinations for this route
	[self willChangeValueForKey:@"isExecuting"];
	executing = YES;
	[self didChangeValueForKey:@"isExecuting"];

	// send a notification
	if (self.syncManager != nil && self.syncManager.delegate != nil && [self.syncManager.delegate respondsToSelector:@selector(syncDidStartStop:)])
		[self.syncManager.delegate performSelectorOnMainThread:@selector(syncDidStartStop:) withObject:stop waitUntilDone:NO];
	
	[self updateInformation];

	// sigh, poll until its finished
	while (![self isFinished])
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
}

- (void)updateInformation
{
	[service getInformationForStop:[stop.trackerID intValue]];
}

- (void)setInformation:(NSDictionary *)info
{
	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

	// start updating
	[stop setCityDirection:[info objectForKey:@"cityDirection"]];
	[stop setCityStop:[info objectForKey:@"cityStop"]];
	[stop setConnectingBuses:[info objectForKey:@"connectingBuses"]];
	[stop setConnectingTrains:[info objectForKey:@"connectingTrains"]];
	[stop setConnectingTrams:[info objectForKey:@"connectingTrams"]];
	[stop setLatitude:[info objectForKey:@"latitude"]];
	[stop setLongitude:[info objectForKey:@"longitude"]];
	[stop setLength:[info objectForKey:@"length"]];
	[stop setName:[info objectForKey:@"name"]];
	[stop setNumber:[info objectForKey:@"number"]];
	[stop setPlatformStop:[info objectForKey:@"platformStop"]];
	[stop setSuburbName:[info objectForKey:@"suburbName"]];
	
	NSMutableArray *zones = [[NSMutableArray alloc] initWithCapacity:0];
	for (NSString *z in [[info objectForKey:@"zones"] componentsSeparatedByString:@","])
		[zones addObject:[NSNumber numberWithInteger:[z integerValue]]];
	[stop setMetcardZones:zones];
	[zones release];

	// now that we're done..
	[self finish];
    [pool drain]; pool = nil;
}

// Indicates a failure of some sort.
- (void)pidsServiceDidFailWithError:(NSError *)error
{
	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}

	static NSInteger previousFailureCount = 0;

	// if the stop has been deleted we can silently fail over to syncing that change
	if ([error code] == PIDServiceValidationErrorStopNotFound)
	{
		//NSLog(@"Triggered delete: %@", self.stop);
		//if (self.syncManager != nil)
		//	[self.syncManager addStopDeleteToQueue:self.stop];
		[self finish];

	} else if ([error code] == PIDServiceErrorTimeoutReached && previousFailureCount == 0)
	{
		// try it again
		[self start];
		previousFailureCount = 1;
		
	} else
	{
		[self cancel];
		[self finish];
		
		// send a failure message to the delegate
		if (self.syncManager != nil && self.syncManager.delegate != nil && ![self isCancelled] && [self.syncManager.delegate respondsToSelector:@selector(syncDidFailWithError:)])
			[self.syncManager.delegate performSelectorOnMainThread:@selector(syncDidFailWithError:) withObject:error waitUntilDone:NO];
	}
}

- (void)finish
{
	// send a notification if we werent cancelled
	if (self.syncManager != nil && self.syncManager.delegate != nil && ![self isCancelled] && [self.syncManager.delegate respondsToSelector:@selector(syncDidFinishStop:)])
		[self.syncManager.delegate performSelectorOnMainThread:@selector(syncDidFinishStop:) withObject:stop waitUntilDone:NO];
	
	[self willChangeValueForKey:@"isExecuting"];
	executing = NO;
	[self didChangeValueForKey:@"isExecuting"];
	
	[self willChangeValueForKey:@"isFinished"];
	finished = YES;
	[self didChangeValueForKey:@"isFinished"];

	// release ourselves from the delegate
	[service setDelegate:self];

	//NSLog(@"%@ was updated", stop);
}

- (void)dealloc
{
	[stop release];
	[service release];
	[super dealloc];
}

@end
