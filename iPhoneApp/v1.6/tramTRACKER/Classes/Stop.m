//
//  Stop.m
//  tramTRACKER
//
//  Created by Robert Amos on 10/02/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "Stop.h"

TTMetcardZone const TTMetcardZoneCitySaver = 0;
TTMetcardZone const TTMetcardZone1 = 1;
TTMetcardZone const TTMetcardZone2 = 2;

@implementation Stop

@dynamic number, name, cityDirection, latitude, longitude, suburbName, trackerID, routesThroughStop, platformStop, connectingTrains, connectingTrams, connectingBuses, metcardZones, length, cityStop, turns;

- (NSString *)description
{
	return [NSString stringWithFormat:@"Stop: Stop %@ %@ - %@ in %@ (%@)", self.number, self.name, self.cityDirection, self.suburbName, self.trackerID];
}


+ (NSArray *)allStops
{
	// Grab our managed object context and create a fetch request
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [d managedObjectContext];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	// set the entity that we're looking for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Stop" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// fetch it.
	NSError *outError;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&outError];
	
	// cant find any?
	if (fetchedObjects == nil || [fetchedObjects count] == 0)
	{
		[fetchRequest release];
		return nil;
	}
	
	// return them sorted
	[fetchRequest release];
	return fetchedObjects;
}

+ (Stop *)stopForTrackerID:(NSNumber *)aTrackerID
{
	// Grab our managed object context and create a fetch request
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [d managedObjectContext];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Stop" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"trackerID == %@", aTrackerID];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (fetchedObjects == nil || [fetchedObjects count] == 0)
	{
		[fetchRequest release];
		return nil;
	}
	
	// return the object
	[fetchRequest release];
	return [fetchedObjects objectAtIndex:0];
}

+ (NSArray *)stopsForTrackerIDs:(NSArray *)aTrackerIDArray
{
	// make sure this is actually an NSArray, and that it has items
	if (aTrackerIDArray == nil || [aTrackerIDArray count] == 0)
		return nil;
	
	// Grab our managed object context and create a fetch request
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [d managedObjectContext];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Stop" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"trackerID IN %@", aTrackerIDArray];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (fetchedObjects == nil || [fetchedObjects count] == 0)
	{
		[fetchRequest release];
		return nil;
	}
	
	// return the object
	[fetchRequest release];
	
	NSMutableArray *list = [[NSMutableArray alloc] initWithCapacity:0];
	for (NSNumber *trackerID in aTrackerIDArray)
	{
		for (Stop *s in fetchedObjects)
		{
			if ([trackerID isEqualToNumber:s.trackerID])
			{
				[list addObject:s];
				break;
			}
		}
	}
	return [list autorelease];
}

+ (NSDictionary *)dictionaryOfStopsForTrackerIDs:(NSArray *)aTrackerIDArray
{
	// Grab our managed object context and create a fetch request
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [d managedObjectContext];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Stop" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"trackerID IN %@", aTrackerIDArray];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (fetchedObjects == nil || [fetchedObjects count] == 0)
	{
		[fetchRequest release];
		return nil;
	}
	
	// return the object
	[fetchRequest release];
	
	NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:0];
	for (Stop *s in fetchedObjects)
		[dict setObject:s forKey:s.trackerID];

	return [dict autorelease];
	
}

+ (NSArray *)stopsInSuburb:(NSString *)aSuburb
{
	// Grab our managed object context and create a fetch request
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [d managedObjectContext];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Stop" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"suburbName == %@", aSuburb];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (fetchedObjects == nil || [fetchedObjects count] == 0)
	{
		[fetchRequest release];
		return nil;
	}
	
	// return the object
	[fetchRequest release];
	return fetchedObjects;
}

+ (NSArray *)stopsBySearchingNameOrSuburb:(NSString *)search
{
	// Grab our managed object context and create a fetch request
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [d managedObjectContext];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Stop" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@ OR suburbName CONTAINS[cd] %@", search, search];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (fetchedObjects == nil || [fetchedObjects count] == 0)
	{
		[fetchRequest release];
		return nil;
	}
	
	// return the object
	[fetchRequest release];
	return fetchedObjects;
}

+ (NSArray *)stopsInRegion:(MKCoordinateRegion)region
{
	CLLocationDegrees northernBoundary;
	CLLocationDegrees southernBoundary;
	CLLocationDegrees easternBoundary;
	CLLocationDegrees westernBoundary;

	// simple
	northernBoundary = region.center.latitude + region.span.latitudeDelta;
	southernBoundary = region.center.latitude - region.span.latitudeDelta;
	easternBoundary = region.center.longitude + region.span.longitudeDelta;
	westernBoundary = region.center.longitude - region.span.longitudeDelta;

	// make sure we haven't reversed this
	if (southernBoundary > northernBoundary)
	{
		CLLocationDegrees tmpBoundary = northernBoundary;
		northernBoundary = southernBoundary;
		southernBoundary = tmpBoundary;
	}
	if (westernBoundary > easternBoundary)
	{
		CLLocationDegrees tmpBoundary = easternBoundary;
		easternBoundary = westernBoundary;
		westernBoundary = tmpBoundary;
	}

	// Grab our managed object context and create a fetch request
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [d managedObjectContext];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Stop" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"latitude BETWEEN { %@, %@ } AND longitude BETWEEN { %@, %@ } AND trackerID < 8000",
							  [NSNumber numberWithDouble:southernBoundary], [NSNumber numberWithDouble:northernBoundary],
							  [NSNumber numberWithDouble:westernBoundary], [NSNumber numberWithDouble:easternBoundary]];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (fetchedObjects == nil || [fetchedObjects count] == 0)
	{
		[fetchRequest release];
		return nil;
	}
	
	// return the object
	[fetchRequest release];
	return fetchedObjects;
}

- (CLLocation *)location
{
	NSAssert([self latitude] && [self longitude], @"Latitude or Longitude not set");
	
	return [[[CLLocation alloc]
						initWithLatitude:[self.latitude doubleValue]
							   longitude:[self.longitude doubleValue]] autorelease];
}
- (CLLocationCoordinate2D)coordinate
{
	return [[self location] coordinate];
}


// Nicely formatted name
- (NSString *)formattedName
{
	return [NSString stringWithFormat:NSLocalizedString(@"stop-name", "Stop Name"), self.number, self.name];
}

// Nicely formatted route description
- (NSString *)formattedRouteDescription
{
	// more than one route through here?
	if ([self.routesThroughStop count] >= 4)
		return [NSString stringWithFormat:NSLocalizedString(@"stop-routedescription-multiple", @"Routes .."), [self shortenedFormattedRouteList], [self displayedCityDirection]];
	else if ([self.routesThroughStop count] > 1)
		return [NSString stringWithFormat:NSLocalizedString(@"stop-routedescription-multiple", @"Routes .."), [self.routesThroughStop componentsJoinedByString:@", "], [self displayedCityDirection]];
	else
		return [NSString stringWithFormat:NSLocalizedString(@"stop-routedescription-single", @"Route x.."), [self.routesThroughStop objectAtIndex:0], [self displayedCityDirection]];
}
- (NSString *)formattedRouteDescriptionForFavouriteAtIndexPath:(NSIndexPath *)indexPath
{
	Filter *filter = [[StopList sharedManager] filterForFavouriteAtIndexPath:indexPath];
	
	if (filter != nil && filter.route != nil)
		return [NSString stringWithFormat:NSLocalizedString(@"stop-routedescription-filtered", @"Route x only..."), filter.route.number, [self displayedCityDirection]];
	else
		return [self formattedRouteDescription];
}

- (NSString *)shortenedFormattedRouteList
{
	// get a list of all the routes
	NSArray *routes = [[RouteList sharedManager] getRouteList];
	NSMutableArray *routeNumbers = [[NSMutableArray alloc] initWithCapacity:0];
	for (Route *r in routes)
		[routeNumbers addObject:r.number];
    
	// now build up an index based on the routes that pass this stop
	NSMutableArray *routeIndex = [[NSMutableArray alloc] initWithCapacity:0];
	for (NSString *routeNumber in self.routesThroughStop)
		[routeIndex addObject:[NSNumber numberWithInt:[routeNumbers indexOfObject:routeNumber]]];

	NSMutableArray *condensedRouteIndex = [[NSMutableArray alloc] initWithCapacity:0];
	for (NSNumber *i in routeIndex)
	{
		NSUInteger index = [routeIndex indexOfObject:i];
		if (index > 0 && index < [routeIndex count]-1)
		{
			NSNumber *before = [routeIndex objectAtIndex:index-1];
			NSNumber *after = [routeIndex objectAtIndex:index+1];
			NSInteger myself = [i integerValue];
			
			if (myself - 1 != [before integerValue] || myself + 1 != [after integerValue])
				[condensedRouteIndex addObject:i];
		} else
		{
			[condensedRouteIndex addObject:i];
		}
	}
	
	NSMutableString *shortenedRouteList = [[NSMutableString alloc] initWithCapacity:0];
	for (NSNumber *i in condensedRouteIndex)
	{
		// first one? just add it
		if ([condensedRouteIndex indexOfObject:i] == 0)
		{
			[shortenedRouteList appendString:[routeNumbers objectAtIndex:[i integerValue]]];
			continue;
		}
		
		// now in our full index list, if our index - 1 existed there then theres been stuff cut, prepend it with a dash
		if ([routeIndex indexOfObject:[NSNumber numberWithInteger:[i integerValue]-1]] != NSNotFound &&
			[routeIndex indexOfObject:[NSNumber numberWithInteger:[i integerValue]-2]] != NSNotFound)
			[shortenedRouteList appendFormat:@"-%@", [routeNumbers objectAtIndex:[i integerValue]]];
		else
			[shortenedRouteList appendFormat:@", %@", [routeNumbers objectAtIndex:[i integerValue]]];
	}
	
	[routeNumbers release];
	[routeIndex release];
	[condensedRouteIndex release];
	
	NSString *shortenedFormattedRouteList = [NSString stringWithString:shortenedRouteList];
	[shortenedRouteList release];
	return shortenedFormattedRouteList;
}

// Short name
- (NSString *)shortName
{
	if ([self.name rangeOfString:@"&"].location != NSNotFound)
	{
		// split it up around the ampersand
		NSArray *pieces = [self.name componentsSeparatedByString:@"&"];
		
		// take the first piece for the title
		return [[pieces objectAtIndex:0]
				 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
	}
	return self.name;
}

// Override the retrieval of the city direction
- (NSString *)displayedCityDirection
{
	// check the stop list service to see if we've been overriden
	StopList *s = [StopList sharedManager];
	NSString *c = [s directionTextForStopID:self.trackerID];
	
	// does the override not exist? return default
	if (c == nil || [c isEqual:@""])
		return self.cityDirection;

	// return the override
	return c;
}

- (BOOL)isPlatformStop
{
	return [self.platformStop boolValue];
}
- (BOOL)isCityStop
{
	return [self.cityStop boolValue];
}

- (BOOL)hasConnectingTrains
{
	return [self.connectingTrains boolValue];
}
- (BOOL)hasConnectingTrams
{
	return [self.connectingTrams boolValue];
}
- (BOOL)hasConnectingBuses
{
	return [self.connectingBuses boolValue];
}


//
// Compare Stop
//
- (NSComparisonResult)compareStop:(Stop *)otherStop
{
	if ([self number] == [otherStop number])
		return [self.name compare:otherStop.name];
	return [self.number compare:otherStop.number];
}

- (BOOL)isEqualToStop:(Stop *)otherStop
{
	return [self.trackerID isEqualToNumber:otherStop.trackerID];
}

//
// Search functions
//
- (BOOL)nameContains:(NSString *)searchString
{
	// if the tracker ID is zero its excluded from all search functions
	if ([self.trackerID integerValue] >= 8000)
		return NO;

	if (searchString == nil)
		return NO;
	return [self.name rangeOfString:searchString options:NSCaseInsensitiveSearch].location != NSNotFound;
}

- (BOOL)suburbNameContains:(NSString *)searchString
{
	// if the tracker ID is zero its excluded from all search functions
	if ([self.trackerID integerValue] >= 8000)
		return NO;

	if (searchString == nil)
		return NO;
	return [self.suburbName rangeOfString:searchString options:NSCaseInsensitiveSearch].location != NSNotFound;
}

// Map View Pin's title
- (NSString *)title
{
	return [self formattedName];
}

// Map View Pin's subtitle
- (NSString *)subtitle
{
	return [self formattedRouteDescription];
}

- (BOOL)isZone1
{
    for (NSNumber *zone in self.metcardZones)
        if ([zone integerValue] == 1)
            return YES;
    return NO;
}

- (BOOL)isZone2
{
    for (NSNumber *zone in self.metcardZones)
        if ([zone integerValue] == 2)
            return YES;
    return NO;
}

// Return an image based on its position in a route
- (UIView *)viewForTrackOnRoute:(Route *)route isDisabled:(BOOL)isDisabled
{
    UIImage *segmentImage = nil;
    UIImage *stopImage = nil;
    UIImage *platformImage = nil;
    
    // are we the first stop in either direction? (trackerids can only appear in one direction for a route)
	if (([route.upStops count] > 0 && [[route.upStops objectAtIndex:0] isEqualToNumber:self.trackerID]) ||
		([route.downStops count] > 0 && [[route.downStops objectAtIndex:0] isEqualToNumber:self.trackerID]))
    {
		segmentImage = (isDisabled ? [self imageForDisabledStopAsStartTerminus] : [self imageForStopAsStartTerminusOfRoute:route]);
        stopImage = [UIImage imageNamed:@"track_start_stop.png"];
        if ([self isPlatformStop])
            platformImage = [UIImage imageNamed:@"track_start_platform.png"];
    }
	
	// are we the last stop in either direction?
	else if (([route.upStops count] > 0 && [[route.upStops objectAtIndex:[route.upStops count]-1] isEqualToNumber:self.trackerID]) ||
		([route.downStops count] > 0 && [[route.downStops objectAtIndex:[route.downStops count]-1] isEqualToNumber:self.trackerID]))
    {
		segmentImage = (isDisabled ? [self imageForDisabledStopAsEndTerminus] : [self imageForStopAsEndTerminusOfRoute:route]);
        stopImage = [UIImage imageNamed:@"track_end_stop.png"];
        if ([self isPlatformStop])
            platformImage = [UIImage imageNamed:@"track_end_platform.png"];
    }
	
	// must be a mid-route stop
    else
    {
        segmentImage = (isDisabled ? [self imageForDisabledStopAsMidRoute] : [self imageForStopAsMidRouteOfRoute:route]);
        stopImage = [UIImage imageNamed:@"track_middle_stop.png"];
        if ([self isPlatformStop])
            platformImage = [UIImage imageNamed:@"track_middle_platform.png"];
    }

    // create the image view for the segment, and our overall view!
    UIImageView *segmentView = [[UIImageView alloc] initWithImage:segmentImage];
    UIView *view = [[UIView alloc] initWithFrame:segmentView.frame];
    
    // so now we have the appropriate track segment, are we in the zone 1/2 overlap? If yes then we add the zone
    // image to the bottom of the view stack
    if ([self isZone2])
    {
        [view addSubview:[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"zone_1or2.png"]] autorelease]];
    }

    // now add the segment
    [view addSubview:segmentView];
    [segmentView release]; segmentView = nil;
    
    // and the stop
    UIImageView *stopView = [[UIImageView alloc] initWithImage:stopImage];
    [view addSubview:stopView];
    [stopView release]; stopView = nil;
    
    // and the platform, if it is one
    if (platformImage != nil)
    {
        UIImageView *platformView = [[UIImageView alloc] initWithImage:platformImage];
        [view addSubview:platformView];
        [platformView release]; platformView = nil;
    }
    
    // return that view!
    return [view autorelease];
}

// Image for stop as a start terminus
- (UIImage *)imageForStopAsStartTerminusOfRoute:(Route *)route;
{
	return [UIImage imageNamed:[NSString stringWithFormat:@"track_start_%@.png", ([route colour] == nil ? @"yellow" : [route colour])]];
}
- (UIImage *)imageForDisabledStopAsStartTerminus
{
	return [UIImage imageNamed:@"track_start_disabled.png"];
}

// Image for stop as an end terminus
- (UIImage *)imageForStopAsEndTerminusOfRoute:(Route *)route;
{
	return [UIImage imageNamed:[NSString stringWithFormat:@"track_end_%@.png", ([route colour] == nil ? @"yellow" : [route colour])]];
}
- (UIImage *)imageForDisabledStopAsEndTerminus
{
	return [UIImage imageNamed:@"track_end_disabled.png"];
}

// Image for stop as a regular midroute stop
- (UIImage *)imageForStopAsMidRouteOfRoute:(Route *)route;
{
	return [UIImage imageNamed:[NSString stringWithFormat:@"track_middle_%@.png", ([route colour] == nil ? @"yellow" : [route colour])]];
}
- (UIImage *)imageForDisabledStopAsMidRoute
{
	return [UIImage imageNamed:@"track_middle_disabled.png"];
}

// Image for the empty track that runs after this stop
- (UIImage *)imageForEmptyTrackAtStopOfRoute:(Route *)route;
{
	return [UIImage imageNamed:[NSString stringWithFormat:@"track_middle_%@.png", ([route colour] == nil ? @"yellow" : [route colour])]];
}
- (UIImage *)imageForDisabledEmptyTrackAtStop
{
	return [UIImage imageNamed:@"track_middle_disabled.png"];
}

// find the distance travelled between two stops
- (TTDistanceTravelled)distanceTravelledToStop:(Stop *)otherStop atLocation:(CLLocation *)loc
{
	// work out the distance between the two stops
	TTDistanceTravelled distance;
	distance.totalDistance = [LocationManager distanceFromLocation:self.location toLocation:otherStop.location];
	
	// lets check to see whether this point falls between the two stops if we're not then set the distance travelled to -1
	if (![self location:loc betweenSelfAndStop:otherStop])
	{
		distance.travelledDistance = -1;
		return distance;
	}
	
	// this is definitely not the most elegant way to do this, but it should work
	CGFloat bearingToStop = [self bearingToLocation:otherStop.location];
	CGFloat bearingToLoc = [self bearingToLocation:loc];
	
	// work out the angle between the two bearings
	CGFloat angle = fabs(bearingToLoc - bearingToStop);
	
	// and the distance to the location
	CGFloat distanceToLoc = [LocationManager distanceFromLocation:loc toLocation:self.location];
	
	// so if we project the point onto our line between the two stops we end up with a right-angled triangle
	// use trig to find the length of the projection
	CGFloat lengthOfProjection = sin(angle) * distanceToLoc;
	
	// And then pythagoras to find the distance we're looking for!
	distance.travelledDistance = sqrt(pow(distanceToLoc, 2) - pow(lengthOfProjection, 2));
	
	return distance;
}

- (BOOL)location:(CLLocation *)loc betweenSelfAndStop:(Stop *)otherStop
{
	// shortcut, if our location is the same as one of the other two then YES
	if ([LocationManager distanceFromLocation:loc toLocation:self.location] == 0 || [LocationManager distanceFromLocation:loc toLocation:otherStop.location] == 0)
		return YES;
	
	// calculate the bearings between ourselves and the other two locations
	CGFloat bearingToStop = [self bearingToLocation:otherStop.location];
	CGFloat bearingToLoc = [self bearingToLocation:loc];
	CGFloat angle = fabs(bearingToLoc - bearingToStop);
	
	// if the angle between those two bearings is more than 90 degrees then the stop is behind us
	if (angle > 90)
		return NO;
	
	// so the stop is in front of us, work out the bearings from the remote end
	CGFloat bearingFromStopToSelf = [otherStop bearingToLocation:self.location];
	CGFloat bearingFromStopToLoc = [otherStop bearingToLocation:loc];
	CGFloat remoteAngle = fabs(bearingFromStopToLoc - bearingFromStopToSelf);
	
	// if its more than 90 degrees then it is beyond the remote stop
	if (remoteAngle > 90)
		return NO;
	
	// its between us!
	return YES;
}

- (CGFloat)bearingToLocation:(CLLocation *)loc
{
	// Convert these to radians
	double lat1 = (self.location.coordinate.latitude * M_PI / 180);
	double lat2 = (loc.coordinate.latitude * M_PI / 180);
	double dLon = (loc.coordinate.longitude - self.location.coordinate.longitude) * M_PI / 180;

	// Calculate and return the bearing
	double y = sin(dLon) * cos(lat2);
	double x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon);
	return (atan2(y, x) * 180 / M_PI) + 360 % 360;
}

- (void)launchGoogleMapsWithLocation
{
	// launches the Google Maps application with the stop's location pinpointed, this will not return, tramTRACKER will close
	NSString *stringURL = [NSString stringWithFormat:@"http://maps.google.com/maps?q=%.8f,%.8f (%@)", self.location.coordinate.latitude, self.location.coordinate.longitude, [self formattedNameForURL]];
	NSString *escapedStringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
	NSURL *url = [NSURL URLWithString:escapedStringURL];
	[[UIApplication sharedApplication] openURL:url];
}

- (void)launchGoogleMapsWithDirections
{
	// launches the Google Maps application with the stop's location pinpointed, this will not return, tramTRACKER will close
	NSString *stringURL = [NSString stringWithFormat:@"http://maps.google.com/maps?daddr=%.8f,%.8f (%@)", self.location.coordinate.latitude, self.location.coordinate.longitude, [self formattedNameForURL]];
	NSString *escapedStringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
	NSURL *url = [NSURL URLWithString:escapedStringURL];
	[[UIApplication sharedApplication] openURL:url];
}

- (void)launchGoogleMapsWithDirectionsFromLocation:(CLLocation *)loc
{
	// launches the Google Maps application with the stop's location pinpointed, this will not return, tramTRACKER will close
	NSString *stringURL = [NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%.8f,%.8f&daddr=%.8f,%.8f (%@)", loc.coordinate.latitude, loc.coordinate.longitude, self.location.coordinate.latitude, self.location.coordinate.longitude, [self formattedNameForURL]];
	NSString *escapedStringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
	NSURL *url = [NSURL URLWithString:escapedStringURL];
	[[UIApplication sharedApplication] openURL:url];
}

- (NSString *)formattedNameForURL
{
	return [NSString stringWithFormat:@"%@: %@", self.number, [self.name stringByReplacingOccurrencesOfString:@"&" withString:@"and"]];
}

#pragma mark -
#pragma mark Stubs

+ (Stop *)stopForStub:(StopStub *)stub
{
    // tracker id set?
    if (stub.trackerID != nil)
        return [Stop stopForTrackerID:stub.trackerID];
    
    // nope. bummer
    return nil;
}


- (void)dealloc
{
	
	[super dealloc];
}
	
@end

@implementation StopDistance

@synthesize stop, favourite;
@synthesize distance;

//
// Compare our distance from the comparisonLocation with the distance of another stop
// 
- (NSComparisonResult)compareDistance:(StopDistance *)otherStopDistance
{
	return [[NSNumber numberWithDouble:[self distance]] compare:[NSNumber numberWithDouble:[otherStopDistance distance]]];
}


// get the description about the stopdistance
- (NSString *)description
{
	return [NSString stringWithFormat:@"%@ at distance %@m", stop, [NSNumber numberWithDouble:distance]];
}

// return a formatted distance
- (NSString *)formattedDistance
{
	if (self.distance > 10000)
		return @"10 km+";
	if (self.distance == 10000)
		return @"10 km";
	if (self.distance >= 1000)
		return [NSString stringWithFormat:@"%.2f km", self.distance/1000];
	return [NSString stringWithFormat:@"%.0f m", self.distance];
}

- (void)dealloc
{
	// release the stop
	[stop release];
	[favourite release];
	
	[super dealloc];
}

@end



@implementation StopUpdate

@synthesize trackerID, actionType, hasBeenCompleted;

- (NSString *)description
{
	NSString *typeDescription = @"unknown";
	if (actionType == PIDServiceActionTypeUpdate)
		typeDescription = @"updated";
	else if (actionType == PIDServiceActionTypeDelete)
		typeDescription = @"deleted";

	return [NSString stringWithFormat:@"Stop %@ %@ %@", trackerID, (hasBeenCompleted ? @"has been" : @"needs to be"), typeDescription];
}

- (void)dealloc
{
	[trackerID release];
	[super dealloc];
}

@end

@implementation StopStub

@synthesize trackerID;

- (void)dealloc
{
    [trackerID release]; trackerID = nil;
    [super dealloc];
}

@end
