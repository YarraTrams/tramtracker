//
//  OnboardListView.h
//  tramTRACKER
//
//  Created by Robert Amos on 18/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Location.h"
#import "OnboardListCell.h"
#import "Journey.h"
#import "Prediction.h"
#import "Turn.h"
#import <QuartzCore/QuartzCore.h>

@class OnboardViewController;

/**
 * @ingroup Lists
**/

/**
 * Provides an auto-updating view that acts as a live-view of the tram that you're on
**/
@interface OnboardListView : UITableViewController <CLLocationManagerDelegate, UIAlertViewDelegate> {
	
	/**
	 * A cached copy of the journey
	**/
	Journey *journey;
	
	/**
	 * The stop list for the route
	**/
	NSArray *stopList;

	/**
	 * A list of Suburb names used to generate and maintain the section headers
	 **/
	NSMutableArray *suburbList;
	
	/**
	 * A convenience array that has the stops split into arrays per each suburb. The index of this array matches the index of the suburbList
	 **/
	NSMutableArray *stopListSplitBySuburbs;
	
	/**
	 * The location manager
	**/
	LocationManager *locationManager;
	
	/**
	 * The next stop
	**/
	Stop *nextStop;
	Stop *destination;
	
	/**
	 * The offset from the scheduled time (calculated by a moving tram)
	**/
	NSTimeInterval offset;
	
	BOOL hasAppeared;
	OnboardViewController *__weak parent;
	Stop *atStop;
	NSIndexPath	*indexPathToOpenTurnIndicator;
	BOOL shouldShowTurnIndicators;
	NSMutableArray *previouslyHighlightedStops;
	NSIndexPath *indexPathToSelectedStop;
}

@property (nonatomic, strong) Journey *journey;
@property (nonatomic, strong) NSArray *stopList;
@property (nonatomic, strong) NSMutableArray *suburbList;
@property (nonatomic, strong) NSMutableArray *stopListSplitBySuburbs;
@property (nonatomic, strong) LocationManager *locationManager;
@property (nonatomic, strong) Stop *nextStop;
@property (nonatomic) NSTimeInterval offset;
@property (weak, nonatomic, readonly) OnboardViewController *parent;
@property (nonatomic, strong) Stop *atStop;
@property (nonatomic, strong) NSIndexPath *indexPathToOpenTurnIndicator;

/**
 * Creates the suburbList and stopListSplitBySuburbs arrays using the stopList array.
 **/
- (void)setupSuburbList;

/**
 * Scrolls to the first stop in a journey
**/
- (void)scrollToStop:(Stop *)stop;
- (NSIndexPath *)indexPathOfStop:(Stop *)stop;

- (void)moveToStop:(Stop *)stop;
- (Stop *)nextStopWithLocation:(CLLocation *)location;
- (NSTimeInterval)secondsToNextStopWithLocation:(CLLocation *)location;
- (UIView *)tableFooterView;
- (id)initWithParentViewController:(OnboardViewController *)parent;
- (void)moveOnFromNextStop:(NSTimer *)aTimer;
- (BOOL)isGPSEnabled;

- (void)showTurnIndicatorOnlyForCell:(OnboardListCell *)cell;
- (void)hideTurnIndicatorForCell:(OnboardListCell *)cell;
- (void)setShouldShowTurnIndicators;

- (void)showConnectionsForStop:(Stop *)stop;

// arrival alarm
//- (void)setArrivalAlarmAtStop:(Stop *)stop;
//- (void)cancelArrivalAlarm;

@end

