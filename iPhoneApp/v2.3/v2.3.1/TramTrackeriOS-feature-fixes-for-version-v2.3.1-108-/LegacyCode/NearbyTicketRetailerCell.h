//
//  NearbyTicketRetailerCell.h
//  tramTRACKER
//
//  Created by Robert Amos on 26/07/10.
//  Copyright 2010 Metro Trains Melbourne. All rights reserved.
//

#import <UIKit/UIKit.h>


/**
 * @defgroup CustomTableCells Custom Table Cells
 **/

/**
 * @ingroup CustomTableCells
 **/

/**
 * A custom UITableViewCell that includes labels for the ticket retailer name, address and distance to the retailer.
 **/
@interface NearbyTicketRetailerCell : UITableViewCell {
	/**
	 * The label for the distance to the retailer from a specific location.
	 **/
	UILabel *distance;
	
	/**
	 * The label for the retailer's name
	 **/
	UILabel *name;
	
	/**
	 * The label for the address
	 **/
	UILabel *address;
}

@property (nonatomic, strong) UILabel *distance;
@property (nonatomic, strong) UILabel *name;
@property (nonatomic, strong) UILabel *address;

/**
 * Creates and positions the distance label
 * @private
 **/
- (UILabel *)newLabelForDistance;

/**
 * Creates and positions the name label
 * @private
 **/
- (UILabel *)newLabelForName;

/**
 * Creates and positions the address label
 * @private
 **/
- (UILabel *)newLabelForAddress;

@end
