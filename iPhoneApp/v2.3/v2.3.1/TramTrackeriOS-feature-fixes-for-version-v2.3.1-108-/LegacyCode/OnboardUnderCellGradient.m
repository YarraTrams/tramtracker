//
//  OnboardUnderCellGradient.m
//  tramTRACKER
//
//  Created by Robert Amos on 7/10/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "OnboardUnderCellGradient.h"


@implementation OnboardUnderCellGradient


- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        // Initialization code
		[self setBackgroundColor:[UIColor colorWithWhite:0.2 alpha:1.0]];
    }
    return self;
}


- (void)drawRect:(CGRect)rect {
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	// colour components and locations
	CGFloat locations[3] = { 0.0, 0.2, 1.0 };
	CGFloat components[16] = 
	{
		0.0, 0.0, 0.0, 0.6,
		0.0, 0.0, 0.0, 0.0,
		0.0, 0.0, 0.0, 0.0
	};
	
	// make our gradient
	CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB();
	CGGradientRef gradient = CGGradientCreateWithColorComponents(space,	components, locations, 3);
	
	// draw our gradient in our view
	CGContextSaveGState(context);
	//	CGContextClipToRect(context, CGRectMake(0, 0, rect.size.width, 10));
	CGContextDrawLinearGradient(context, gradient, CGPointMake(0, 0), CGPointMake(0, 44), 0);
	CGGradientRelease(gradient);
	CGColorSpaceRelease(space);
}




@end
