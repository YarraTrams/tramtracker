//
//  HelpPIDController.h
//  tramTRACKER
//
//  Created by Robert Amos on 4/05/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SimpleCell.h"
#import "GradientBackgroundCell.h"
#import "HelpBaseController.h"
#import "HelpWebController.h"

extern const NSString *HelpURLSubdirPID;

@interface HelpPIDController : HelpBaseController {

}

- (id)initHelpPIDController;

@end
