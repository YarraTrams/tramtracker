//
//  FavouriteGroupsController.m
//  tramTRACKER
//
//  Created by Robert Amos on 21/07/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "FavouriteGroupsController.h"
#import "FavouriteGroupsEditController.h"
#import "SimpleCell.h"
#import "UnderBackgroundView.h"
#import "GradientBackgroundCell.h"
#import "OnboardListCell.h"


@implementation FavouriteGroupsController

- (id)initWithGroups:(NSArray *)newGroups
{
	if (self = [super initWithStyle:UITableViewStylePlain])
	{
		groups = newGroups;
		[self setTitle:NSLocalizedString(@"favourites-groups-title", nil)];
		[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
	}
	return self;
}

- (void)viewDidLoad
{
	[self.tableView setTableFooterView:[self tableFooterView]];
	[self.tableView setEditing:YES animated:NO];
	[self.tableView setAllowsSelectionDuringEditing:YES];

	// Set the done item button
	UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(saveGroups)];
	[self.navigationItem setRightBarButtonItem:doneButton animated:NO];
}

- (UIView *)tableFooterView
{
	CGRect frame = CGRectMake(0, 0, self.tableView.frame.size.width, 10);
	UnderBackgroundView *ub = [[UnderBackgroundView alloc] initWithFrame:frame];
	return ub;
}

- (void)saveGroups
{
    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
}



- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}


#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [groups count] + 1;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    SimpleCell *cell = (SimpleCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[SimpleCell alloc] initWithCellStyle:TTSimpleCellStyleDefault reuseIdentifier:CellIdentifier];
		[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
		[cell setEditingAccessoryType:UITableViewCellAccessoryDisclosureIndicator];

		GradientBackgroundCell *background = [[GradientBackgroundCell alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 44) cellStyle:OnboardCellStyleNone];
		[cell setBackgroundView:background];
    }
    
    // Set up the cell...
	if (indexPath.row >= [groups count])
		[cell setTextLabelText:NSLocalizedString(@"favourites-groups-add", nil)];
	else
		[cell setTextLabelText:[groups objectAtIndex:indexPath.row]];
	
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	// pop a edit/delete thing
	FavouriteGroupsEditController *editController;
	
	if (indexPath.row >= [groups count])
	{
		editController = [[FavouriteGroupsEditController alloc] initWithTarget:self action:@selector(addGroup:)];
		[editController setTitle:NSLocalizedString(@"favourites-groups-add", nil)];
	} else
	{
		editController = [[FavouriteGroupsEditController alloc] initWithGroupName:[groups objectAtIndex:indexPath.row] target:self action:@selector(changeGroupName:toName:)];
		[editController setTitle:NSLocalizedString(@"favourites-groups-edit", nil)];
	}

	[self.navigationController pushViewController:editController animated:YES];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
	// is it the one and only group?
	if (groups == nil || ([groups count] <= 1 && indexPath.row == 0))
		return UITableViewCellEditingStyleNone;
	
	// is it past the end?
	if (indexPath.row >= [groups count])
		return UITableViewCellEditingStyleInsert;

	return UITableViewCellEditingStyleDelete;
}

//
// This is called when the user hits the edit button and allows us to make the fixed menu non-editable
//
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // a single group cannot be moved
	if (groups == nil || ([groups count] <= 1 && indexPath.row == 0))
		return NO;
    
	return YES;
}

//
// When the table view goes into edit mode, it calls this to find out which rows can be re-arranged
//
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
	// a single group cannot be moved
	if (groups == nil || ([groups count] <= 1 && indexPath.row == 0))
		return NO;
	
	// the add new group control cannot be moved
	if (indexPath.row >= [groups count])
		return NO;
	
	return YES;
}

//
// And when the row has been moved
//
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
	// move the stop in the favourites list
	[[StopList sharedManager] moveFavouriteSectionAtIndex:fromIndexPath.row toIndex:toIndexPath.row];
	NSArray *newGroups = [[StopList sharedManager] sectionNamesForFavourites];
	groups = newGroups;
}

//
// Finished editing a row (most likely it got deleted)
//
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
	// if the row is deleted, remove it
	if (editingStyle == UITableViewCellEditingStyleDelete)
	{
		// remove the favourite
		[[StopList sharedManager] removeFavouriteSectionAtIndex:[indexPath indexAtPosition:1]];
		NSArray *newGroups = [[StopList sharedManager] sectionNamesForFavourites];
		groups = newGroups;
		
		// now remove it from the table
		[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationTop];
	}
}

#pragma mark -
#pragma mark Adding/Editing Groups

- (void)addGroup:(NSString *)newGroupName
{
	[[StopList sharedManager] addFavouriteSection:newGroupName];

	NSArray *newGroups = [[StopList sharedManager] sectionNamesForFavourites];
	groups = newGroups;
	[self.tableView reloadData];
}

- (void)changeGroupName:(NSString *)existingGroupName toName:(NSString *)newGroupName
{
	// make sure we can find this group in the sections list
	NSUInteger index = [groups indexOfObject:existingGroupName];
	if (index == NSNotFound)
		return;

	[[StopList sharedManager] changeFavouriteSectionNameAtIndex:index toName:newGroupName];

	NSArray *newGroups = [[StopList sharedManager] sectionNamesForFavourites];
	groups = newGroups;
	[self.tableView reloadData];
}



@end

