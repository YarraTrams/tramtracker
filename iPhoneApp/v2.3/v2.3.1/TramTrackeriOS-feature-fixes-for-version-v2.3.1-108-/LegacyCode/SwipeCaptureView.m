//
//  SwipeCaptureView.m
//  tramTRACKER
//
//  Created by Robert Amos on 6/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "SwipeCaptureView.h"


@implementation SwipeCaptureView

@synthesize delegate;

//
// When the user starts touching the screen
//
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	// does our delegate handle touches?
	BOOL cancelledByDelegate = NO;
	if (self.delegate)
		cancelledByDelegate = [self.delegate touchesDidBegin:touches withEvent:event];

	//touchStarted = [[touches anyObject] locationInView:self];
	
	if (!cancelledByDelegate)
		[super touchesBegan:touches withEvent:event];
}

//
// When the user drags their finger
//
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	// check to see if our delegate handles this
	BOOL cancelledByDelegate = NO;
	if (self.delegate)
		cancelledByDelegate = [self.delegate touchesDidMove:touches withEvent:event];
	
	// did the delegate take care of it?
	if (!cancelledByDelegate)
		[super touchesMoved:touches withEvent:event];
}

//
// When a touch is cancelled
//
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
	// check to see if our delegate handles this
	BOOL cancelledByDelegate = NO;
	if (self.delegate)
		cancelledByDelegate = [self.delegate touchesWereCancelled:touches withEvent:event];

	// did the delegate take care of it?
	if (!cancelledByDelegate)
		[super touchesCancelled:touches withEvent:event];
}

//
// When the user stops touching the screen
//
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	// pass it up to the delegate
	BOOL cancelledByDelegate = NO;
	if (self.delegate)
		cancelledByDelegate = [self.delegate touchesDidEnd:touches withEvent:event];
	
	//touchEnded = [[touches anyObject] locationInView:self];


	// we're swiping if we've at least moved half the width of the screen
	/*if (abs(touchStarted.x - touchEnded.x) > 100)
	{
		// we're swiping, fire notification
		NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
		
		// left or right?
		if (touchStarted.x > touchEnded.x)
			[nc postNotificationName:@"PIDSwipeLeft" object:nil];
		else
			[nc postNotificationName:@"PIDSwipeRight" object:nil];

		// don't worry about going up the tree, we've handled it.
		return;
	} */
	
	if (!cancelledByDelegate)
		[super touchesEnded:touches withEvent:event];
}



@end

