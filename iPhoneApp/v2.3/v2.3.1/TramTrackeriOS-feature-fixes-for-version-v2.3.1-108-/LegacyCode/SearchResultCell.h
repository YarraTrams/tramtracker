//
//  SearchResultCell.h
//  tramTRACKER
//
//  Created by Robert Amos on 6/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GradientBackgroundCell.h"

/**
 * @ingroup CustomTableCells
**/

/**
 * A custom UITableViewCell that displays stops returned as part of search results
**/
@interface SearchResultCell : UITableViewCell {
	
	/**
	 * The label for the stop's name
	**/
	UILabel *name;

	/**
	 * The label for the description of the routes travelling through the stop
	**/
	UILabel *routeDescription;
}

@property (nonatomic, strong) UILabel *name;
@property (nonatomic, strong) UILabel *routeDescription;

/**
 * Creates and positions the name label
**/
- (UILabel *)newLabelForName;

/**
 * Creates and positions the route description label
**/
- (UILabel *)newLabelForRouteDescription;

@end
