//
//  NearbyListController.h
//  tramTRACKER
//
//  Created by Robert Amos on 4/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Location.h"
#import "Stop.h"
#import "StopList.h"
#import "NearbyCell.h"
#import "PIDViewController.h"
#import "MainListController.h"
#import "WelcomeController.h"
#import <QuartzCore/QuartzCore.h>
#import "NearbyViewControllerOld.h"

@class MapViewControllerOld;

/**
 * @ingroup Lists
**/

/**
 * A custom UITableViewController that displays an item for the Nearby PID and stops that are nearby to the user
**/
@interface NearbyListController : UITableViewController <CLLocationManagerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate> {
	
	/**
	 * A cached array that is used to display and maintain the stopList. Is updated by the locationManager
	 * as the device moves around.
	**/
	NSArray *stopList;

	/**
	 * The location manager used to find a list of the nearby stops.
	**/
	LocationManager *locationManager;
	
	/**
	 * The number of times we get the same stop result from the location manager
	**/
	NSInteger sameStopResult;
	
	/**
	 * The refresh button painted in the footer
	**/
	UILabel *accuracyLabel;
	NSDate *locatingStarted;
	UIButton *nearestButton;
	UILabel *nearestLabel;
	UIImageView *locatingImage;
	NearbyViewControllerOld *__weak nearbyViewController;
	
}

@property (nonatomic, strong) NSArray *stopList;
@property (nonatomic, strong) LocationManager *locationManager;
@property (nonatomic, strong) UILabel *accuracyLabel;
@property (nonatomic, strong) NSDate *locatingStarted;
@property (nonatomic, strong) UIButton *nearestButton;
@property (nonatomic, strong) UIImageView *locatingImage;
@property (nonatomic, strong) UILabel *nearestLabel;
@property (weak, readonly, nonatomic) NearbyViewControllerOld *nearbyViewController;


- (id)initNearbyListWithNearbyViewController:(NearbyViewControllerOld *)pVC;

/**
 * Creates and pushes the "Nearby PID" onto the Navigation Controller's stack.
 *
 * @param	animated		Whether the push should be animated
**/
- (void)pushPIDWithNearbyAnimated:(BOOL)animated;
- (void)pushPIDWithNearbyAnimated;

- (void)pushPIDForStop:(Stop *)stop;
- (void)pushPIDForStop:(Stop *)stop withFilter:(Filter *)filter;
- (void)pushPIDForStop:(Stop *)stop withFilter:(Filter *)filter animated:(BOOL)animated;


/**
 * A CLLocationManagerDelegate method. This is called whenever the device encountered an error while attempting to locate the user
 * (generally the user refused permission to try).
 *
 * Stops the location manager from attempting to try again.
**/
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error;

/**
 * Flips over to the Map View
 **/
//- (void)flipToMapView;

/**
 * Creates the table Footer view
 **/
- (UIView *)tableFooterView;

- (void)refreshLocation;
- (void)displayStopsWithLocation:(CLLocation *)location;
- (void)displayStopsFromTimer:(NSTimer *)aTimer;

@end
