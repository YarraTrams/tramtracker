//
//  NearbyViewController.h
//  tramTRACKER
//
//  Created by Robert Amos on 25/07/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Stop.h"
#import "Filter.h"
#import "MapViewControllerOld.h"
#import "SWRevealViewController.h"
#import "NearbyActionMenuViewController.h"
#import "RightMenuDelegate.h"

@interface NearbyViewControllerOld : UIViewController <SWRevealViewControllerDelegate> {
	NearbyListController *list;
	MapViewControllerOld *map;
//	BOOL listViewRefreshing;
//	UIBarButtonItem *refreshButton;
}

@property (readonly, nonatomic, strong) NearbyListController *list;
@property (readonly, nonatomic, strong) MapViewControllerOld *map;
//@property (nonatomic, getter=isListViewRefreshing) BOOL listViewRefreshing;
//@property (nonatomic, strong) UIBarButtonItem *refreshButton;

- (id)initNearbyViewController;

- (void)toggleSideMenu;

- (void)showListView;
- (void)hideListView;

- (void)showMapView;
- (void)hideMapView;
- (void)toggleTicketRetailers;

- (void)pushPIDForStop:(Stop *)stop withFilter:(Filter *)f animated:(BOOL)animated;
- (void)refreshLocation;

- (void)applicationDidBecomeActive;

@end
