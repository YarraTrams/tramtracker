//
//  OnboardListCell.h
//  tramTRACKER
//
//  Created by Robert Amos on 18/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GradientBackgroundCell.h"

/**
 * @ingroup CustomTableCells
 **/

extern const NSInteger OnboardCellStyleNone;
extern const NSInteger OnboardCellStyleMinutesOnRight;
extern const NSInteger OnboardCellStyleMinutesOnRightLowAccuracy;
extern const NSInteger OnboardCellStyleMinutesOnRightDisabled;


/**
 * A highlightable UITableViewCell that displays a track segment, stop name and minutes until arrival (green square)
 **/
@interface OnboardListCell : UITableViewCell {

	/**
	 * The label for the time to arrival
	 **/
	UILabel *arrival;
	
	/**
	 * The label for the stop's name
	 **/
	UILabel *name;
	
	/**
	 * The view for the track segment
	**/
	UIView *trackSegmentView;
	
	/**
	 * Connecting Train icon
	**/
	UIImageView *connectingTrain;
	
	/**
	 * Connecting Tram icon
	**/
	UIImageView *connectingTram;
	
	/**
	 * Connecting Bus icon
	**/
	UIImageView *connectingBus;
	
	/**
	 * Alarm Indicator
	**/
	UIImageView *alarmIndicator;
	
	/**
	 * The turn indicator
	**/
	UIButton *turnIndicator;
	
	/**
	 * The turn message
	**/
	UIImageView *turnMessage;
	UILabel *turnMessageLabel;
	
	id __weak delegate;
	
	/**
	 * Whether this cell is highlighted
	**/
	BOOL highlightedStop;
	BOOL destinationStop;
	
	/**
	 * The cell style
	**/
	NSInteger style;
	
}

@property (nonatomic, strong) UILabel *arrival;
@property (nonatomic, strong) UILabel *name;
@property (nonatomic, strong) UIView *trackSegmentView;
@property (nonatomic, strong) UIImageView *connectingTrain;
@property (nonatomic, strong) UIImageView *connectingTram;
@property (nonatomic, strong) UIImageView *connectingBus;
@property (nonatomic, strong) UIImageView *alarmIndicator;
@property (nonatomic, strong) UIButton *turnIndicator;
@property (nonatomic, strong) UIImageView *turnMessage;
@property (nonatomic, strong) UILabel *turnMessageLabel;
@property (nonatomic) BOOL highlightedStop;
@property (nonatomic) BOOL destinationStop;
@property (nonatomic) NSInteger style;
@property (nonatomic, weak) id delegate;

/**
 * Create a new label for the arrival time
**/
- (UILabel *)newLabelForArrival;

/**
 * Create a new label for the stop name
**/
- (UILabel *)newLabelForName;

/**
 * New turn indicator placeholder
**/
- (UIButton *)newTurnIndicator;
- (void)setTurnIndicatorImage:(UIImage *)indicator withMessage:(NSString *)message;
- (UIImageView *)newTurnMessage;
- (UILabel *)newTurnMessageLabel;
- (void)showTurnMessage;
- (void)hideTurnMessage;
- (void)toggleTurnMessage;

/**
 * Draw the connection icons (hidden) on the right hand side before the arrival box
**/
- (void)drawConnectionIcons;
- (void)drawAlarmIndicator;

/**
 * Overridden initialiser
**/
- (id)initWithFrame:(CGRect)frame reuseIdentifier:(NSString *)identifier cellStyle:(NSInteger)cellStyle;

@end


/**
 * Provides a view for the section header
 **/
@interface OnboardListSectionHeaderView : UIView
{
	UIImageView *imageView;
	UILabel *title;
}

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *title;

- (UILabel *)newLabelForTitleWithFrame:(CGRect)frame;

@end

@interface OnboardListSectionHeaderMask : UIView
{
}
@end
