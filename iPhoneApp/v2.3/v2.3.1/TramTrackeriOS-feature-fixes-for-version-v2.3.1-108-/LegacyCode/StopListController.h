//
//  StopListController.h
//  tramTRACKER
//
//  Created by Robert Amos on 27/02/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Stop.h"
#import "PIDViewController.h"
#import "Location.h"
#import "OnboardListCell.h"
#import "DepartureListController.h"
#import "ActivityBarButtonController.h"
#import "UnderBackgroundView.h"

/**
 * @ingroup Lists
**/

/**
 * A custom UITableViewController that displays and maintains a list of stops.
**/
@interface StopListController : UITableViewController <CLLocationManagerDelegate> {
	
	/**
	 * The cached copy of the Stop List that we are displaying from
	**/
	NSArray *stopList;

	/**
	 * A list of Suburb names used to generate and maintain the section headers
	**/
	NSMutableArray *suburbList;

	/**
	 * A convenience array that has the stops split into arrays per each suburb. The index of this array matches the index of the suburbList
	**/
	NSMutableArray *stopListSplitBySuburbs;

	/**
	 * The location manager
	 **/
	LocationManager *locationManager;
	
	/**
	 * The number of times we've attempted a lookup
	**/
	NSInteger locationResponseCount;
	
	/**
	 * The index path to our current stop
	**/
	NSIndexPath *currentStop;
	
	NSInteger stopTooFar;
	BOOL pushToScheduledDeparturesScreen;
	
	Route *route;
}

@property (nonatomic, strong) NSArray *stopList;
@property (nonatomic, strong) NSMutableArray *suburbList;
@property (nonatomic, strong) NSMutableArray *stopListSplitBySuburbs;
@property (nonatomic, strong) LocationManager *locationManager;
@property (nonatomic, strong) NSIndexPath *currentStop;
@property (nonatomic) BOOL pushToScheduledDeparturesScreen;
@property (nonatomic, strong) Route *route;

/**
 * Creates the suburbList and stopListSplitBySuburbs arrays using the stopList array.
**/
- (void)setupSuburbList;

/**
 * Flips over to the Map View
**/
- (void)flipToMapView;

/**
 * Starts tracking the user
**/
- (void)startTracking;

/**
 * Stops tracking the user
**/
- (void)stopTracking;
- (UIView *)tableFooterView;



@end
