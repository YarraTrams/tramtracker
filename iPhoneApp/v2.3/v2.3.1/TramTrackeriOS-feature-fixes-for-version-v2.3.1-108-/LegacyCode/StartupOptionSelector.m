//
//  StartupOptionSelector.m
//  tramTRACKER
//
//  Created by Robert Amos on 27/04/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "StartupOptionSelector.h"


@implementation StartupOptionSelector

- (id)initWithStartupOption:(NSInteger)aStartupOption target:(id)aTarget action:(SEL)anAction
{
	if (self = [super initWithStyle:UITableViewStyleGrouped])
	{
		[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
		startupOption = aStartupOption;
		target = aTarget;
		action = anAction;
		
		startupOptions = [[NSArray alloc] initWithObjects:NSLocalizedString(@"settings-startup-nearestfavourite", @"Nearest Favourite"),
						  NSLocalizedString(@"settings-startup-favouriteslist", @"Favourites List"),
						  NSLocalizedString(@"settings-startup-nearbylist", @"Nearby List"), nil];
	}
	return self;
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}




#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [startupOptions count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    SimpleCell *cell = (SimpleCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[SimpleCell alloc] initWithCellStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
	[cell setTextLabelText:[startupOptions objectAtIndex:indexPath.row]];
	
	if ((indexPath.row == 0 && startupOption == 1) ||
		(indexPath.row == 1 && startupOption == 2) ||
		(indexPath.row == 2 && startupOption == 3))
		[cell setAccessoryType:UITableViewCellAccessoryCheckmark];
	else
		[cell setAccessoryType:UITableViewCellAccessoryNone];
	
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (target != nil && [target respondsToSelector:action])
	{
		NSInteger newStartupOption = indexPath.row+1;
		SuppressPerformSelectorLeakWarning([target performSelector:action withObject:[NSNumber numberWithInteger:newStartupOption]]);
	}
	[self.navigationController popViewControllerAnimated:YES];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/




@end

