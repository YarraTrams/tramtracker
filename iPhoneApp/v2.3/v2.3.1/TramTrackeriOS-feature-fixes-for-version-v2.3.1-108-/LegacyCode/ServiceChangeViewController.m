//
//  ServiceChangeViewController.m
//  tramTRACKER
//
//  Created by Robert Amos on 21/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "ServiceChangeViewController.h"


@implementation ServiceChangeViewController

@synthesize change;

- (id)initWithServiceChange:(ServiceChange *)aChange
{
	if (self = [super initWithNibName:nil bundle:nil])
	{
		[self setChange:aChange];
		[self setTitle:self.change.title];
	}
	return self;
}

- (void)loadView
{
	// create a UIWebView for this service change
	UIWebView *web = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 367)];

	// load up the template
	NSError *error;
	NSString *template = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Service Change Template" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
	[web loadHTMLString:[NSString stringWithFormat:template, self.change.content] baseURL:[NSURL URLWithString:@"http://www.yarratrams.com.au/"]];
	[self setView:web];
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}






@end
