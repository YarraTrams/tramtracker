//
//  ScheduledTripController.h
//  tramTRACKER
//
//  Created by Robert Amos on 31/07/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Journey;
@class Stop;
@class Route;

@interface ScheduledTripController : UITableViewController {
	Journey *journey;
	Stop *stop;
	Route *route;
	NSIndexPath *indexPathToSelectedStop;

	/**
	 * A list of Suburb names used to generate and maintain the section headers
     **/
	NSMutableArray *suburbList;
    
	/**
	 * A convenience array that has the stops split into arrays per each suburb. The index of this array matches the index of the suburbList
     **/
	NSMutableArray *stopListSplitBySuburbs;

}

@property (nonatomic, strong) NSIndexPath *indexPathToSelectedStop;
@property (nonatomic, strong) NSMutableArray *suburbList;
@property (nonatomic, strong) NSMutableArray *stopListSplitBySuburbs;

- (id)initWithJourney:(Journey *)aJourney onRoute:(Route *)aRoute fromStop:(Stop *)aStop;
- (NSString *)absoluteArrivalTime:(NSDate *)arrivalTime;
- (UIView *)tableFooterView;
- (void)showConnectionsForStop:(Stop *)stop;

/**
 * Creates the suburbList and stopListSplitBySuburbs arrays using the stopList array.
 **/
- (void)setupSuburbList;

@end
