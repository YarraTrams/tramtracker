//
//  GradientBackgroundCell.h
//  tramTRACKER
//
//  Created by Robert Amos on 13/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HighlightedBackgroundCell;
@class DestinationHighlightedBackgroundCell;

/**
 * @ingroup CustomTableCells
**/

/**
 * A nicely gradiented background cell for table rows
**/
@interface GradientBackgroundCell : UIView {

	/**
	 * The highlight layer
	**/
	HighlightedBackgroundCell *highlightLayer;
	DestinationHighlightedBackgroundCell *destinationHighlightLayer;
	
	/**
	 * The style of this cell
	**/
	NSInteger style;
}

@property (nonatomic, strong) HighlightedBackgroundCell *highlightLayer;
@property (nonatomic, strong) DestinationHighlightedBackgroundCell *destinationHighlightLayer;
@property (nonatomic) NSInteger style;

- (id)initWithFrame:(CGRect)frame cellStyle:(NSInteger)style;

@end


/**
 * A light green gradiented highlight view that sits just above the background
**/
@interface HighlightedBackgroundCell : UIView
{
	NSInteger style;
}

- (id)initWithFrame:(CGRect)frame cellStyle:(NSInteger)style;
@end

/**
 * A light yellow gradient highlight view that sits just above the background
**/
@interface DestinationHighlightedBackgroundCell : UIView
{
}
@end;