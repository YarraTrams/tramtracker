//
//  SyncOptionSelector.m
//  tramTRACKER
//
//  Created by Robert Amos on 14/09/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "SyncOptionSelector.h"
#import "BackgroundSynchroniser.h"


@implementation SyncOptionSelector

- (id)initWithSyncOption:(NSInteger)aSyncOption target:(id)aTarget action:(SEL)anAction
{
	if (self = [super initWithStyle:UITableViewStyleGrouped])
	{
		[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
		syncOption = aSyncOption;
		target = aTarget;
		action = anAction;
		
		//syncOptions = [[NSArray alloc] initWithObjects:NSLocalizedString(@"settings-sync-type-auto-full", @"Automatic"),
		//				  NSLocalizedString(@"settings-sync-type-semi-auto-full", @"Semi Auto"),
		//				  NSLocalizedString(@"settings-sync-type-manual-full", @"Manual"), nil];
		syncOptions = [[NSArray alloc] initWithObjects:NSLocalizedString(@"settings-sync-type-semi-auto-full", @"Semi Auto"),
                       NSLocalizedString(@"settings-sync-type-manual-full", @"Manual"), nil];
	}
	return self;
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}




#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [syncOptions count];
}

// Footer for each section
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
	return NSLocalizedString(@"settings-sync-type-footer", @"Footer");
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    SimpleCell *cell = (SimpleCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[SimpleCell alloc] initWithCellStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
	[cell setTextLabelText:[syncOptions objectAtIndex:indexPath.row]];
	
	if ((indexPath.row == 0 && (syncOption == TTSyncTypeAutomatic || syncOption == TTSyncTypeAutoCheckManualSync)) ||
        (indexPath.row == 1 && syncOption == TTSyncTypeManual))
		[cell setAccessoryType:UITableViewCellAccessoryCheckmark];
	else
		[cell setAccessoryType:UITableViewCellAccessoryNone];
	
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (target != nil && [target respondsToSelector:action])
	{
		SuppressPerformSelectorLeakWarning([target performSelector:action withObject:[NSNumber numberWithInteger:(indexPath.row==0 ? TTSyncTypeAutoCheckManualSync : TTSyncTypeManual)]]);
	}
	[self.navigationController popViewControllerAnimated:YES];
}




@end

