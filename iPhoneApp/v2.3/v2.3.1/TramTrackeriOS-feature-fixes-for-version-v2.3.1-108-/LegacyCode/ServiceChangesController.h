//
//  ServiceChanges.h
//  tramTRACKER
//
//  Created by Robert Amos on 20/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServiceChangesService.h"


@interface ServiceChangesController : UITableViewController {
	UIActivityIndicatorView *loadingIndicator;
	UILabel *loadingText;
	UILabel *errorText;
	NSArray *serviceChanges;
	ServiceChangesService *service;
}

@property (nonatomic, strong) NSArray *serviceChanges;
@property (nonatomic, strong) ServiceChangesService *service;

/**
 * Creates the table Footer view
 **/
- (UIView *)tableFooterView;

- (void)setServiceChanges:(NSArray *)newServiceChanges;
- (void)serviceChangesServiceDidFailWithError:(NSError *)error;
- (void)retry;

@end
