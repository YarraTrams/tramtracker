//
//  OnboardListView.m
//  tramTRACKER
//
//  Created by Robert Amos on 18/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "OnboardListView.h"
#import "OnboardUnderCell.h"
#import "MapViewController.h"

@implementation OnboardListView

@synthesize journey, suburbList, locationManager, stopListSplitBySuburbs, nextStop, stopList, offset, parent, atStop, indexPathToOpenTurnIndicator;

- (id)initWithParentViewController:(OnboardViewController *)aParent
{
	if (self = [super initWithStyle:UITableViewStylePlain])
	{
		parent = aParent;
		previouslyHighlightedStops = [[NSMutableArray alloc] initWithCapacity:0];
		[self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	}
	return self;
}


- (void)viewDidAppear:(BOOL)animated
{
	if (self.nextStop != nil)
		[self scrollToStop:self.nextStop];
	hasAppeared = YES;
}

- (void)viewDidDisappear:(BOOL)animated
{
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc removeObserver:self];
}

//
// Create the table footer
//
- (UIView *)tableFooterView
{
	CGRect frame = CGRectMake(0, 0, self.tableView.frame.size.width, 126);
	UnderBackgroundView *ub = [[UnderBackgroundView alloc] initWithFrame:frame];
	
	// put the key in there
	UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stoplistkey.png"]];
	CGRect imageFrame = imageView.frame;
	imageFrame.origin.y = 20;
    imageFrame.origin.x = 20;
	[imageView setFrame:imageFrame];
	[ub addSubview:imageView];
	
	return ub;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}

- (BOOL)isGPSEnabled
{
	// GPS is on in the preferences
	if ([[NSUserDefaults standardUserDefaults] boolForKey:@"onboardGPSEnabled"])
	{
		// make sure we have a journey.
		if (self.journey == nil || [self.journey.stops count] == 0)
			return NO;

		// check to make sure that we're not in the CBD.
		JourneyStop *firstStop = [self.journey.stops objectAtIndex:0];
		if ([firstStop.stop isCityStop])
			return NO;
		
		// otherwise GPS should be on.
		return YES;
	}
	
	// GPS is switched off in the preferences
	return NO;
}

- (void)setShouldShowTurnIndicators
{
	shouldShowTurnIndicators = [[NSUserDefaults standardUserDefaults] boolForKey:@"showTurnIndicators"];
}

- (void)setJourney:(Journey *)newJourney
{
	// set the journey
	
	// is this a new journey that we've not seen before?
	if (journey == nil || [self.parent hasTramChangedRoute:newJourney.tram])
	{
		// setup the stop lists
		[self setStopList:[[StopList sharedManager] getStopListForTrackerIDs:(newJourney.tram.upDirection ? newJourney.tram.route.upStops : newJourney.tram.route.downStops)]];

		// bail out if we have no stops
		if (self.stopList == nil)
			return;
		[self setupSuburbList];

		// clear the next stop
		[self setNextStop:nil];
		
		// clear the highlighting stuff
		[previouslyHighlightedStops removeAllObjects];
		
		// if we have changed route scroll to the top
		if (journey != nil && [self.parent hasTramChangedRoute:newJourney.tram])
			[self scrollToStop:[self.stopList objectAtIndex:0]];
	}
	
	// do we show turn indicators? We check this once every 15 seconds, instead of live. It's rare it will change
	[self setShouldShowTurnIndicators];

	
	// if we're at layover but we're moving then force us off layover (if the number of stops in the new journey has shrunk)
	if (journey != nil && ![self.parent hasTramChangedRoute:newJourney.tram] &&
		[self.parent isAtLayover] && [newJourney.stops count] > 0 && [newJourney.stops count] < [journey.stops count])
	{
		// double check to make sure that the first stop is no longer the same
		Stop *firstStop = [self.stopList objectAtIndex:0];
		JourneyStop *firstJStop = [newJourney.stops objectAtIndex:0];
		JourneyStop *oldFirstJStop = [journey.stops objectAtIndex:0];
		if (![firstJStop.stop isEqual:firstStop] && ![firstJStop.stop isEqual:oldFirstJStop.stop])
		{
			// we're not on layover anymore
			[self.parent setAtLayover:NO];
			
			// set a timer for the pinpoint message
			[self.parent setStartedLocating:[NSDate date]];
		}
	}
	
	if ([newJourney.stops count] > 0 && ![self.parent isAtLayover])
	{
		// so we know what the next stop apparently is now
		JourneyStop *nextJStop = [newJourney.stops objectAtIndex:0];
		Stop *next = nextJStop.stop;

		// did we have no data previously?
		if (self.nextStop == nil)
		{
			journey = newJourney;
			[self moveToStop:next];
		} else
		{
			
			// find the indexes of those two stops
			NSInteger indexOfNewNextStop = [self.stopList indexOfObject:next];
			NSInteger indexOfOldNextStop = [self.stopList indexOfObject:self.nextStop];
			
			journey = newJourney;

			// reset the offset
			[self setOffset:0];
			
			// move to whichever is further along
			if (indexOfNewNextStop > indexOfOldNextStop)
				[self moveToStop:next];
			else {
				// nope, we need to discard yet more stops off the front
				NSInteger indexOfOldNextStopInJourney = [journey.stops indexOfObject:[journey journeyStopForStop:self.nextStop]];
				if (indexOfNewNextStop != NSNotFound)
					[journey setStops:[journey.stops subarrayWithRange:NSMakeRange(indexOfOldNextStopInJourney, [journey.stops count]-indexOfOldNextStopInJourney)]];
			}
		}

	} else
	{
		// update the data but no moving
		journey = newJourney;
	}

	// start the GPS going
	if ([CLLocationManager locationServicesEnabled] && [self isGPSEnabled])
		[self.locationManager startUpdatingLocation];
	else
	{
		[self.parent setAccuracyLevel:5];
		[self.locationManager stopUpdatingLocation];
	}
	
	[self.tableView reloadData];
}

- (void)setupSuburbList
{
	// make sure that we have a stop list
	NSAssert(self.stopList, @"Cannot build suburb list as no stops have been set.");
	
	// find the number of suburbs in the list
	[self setSuburbList:[NSMutableArray arrayWithCapacity:0]];
	[self setStopListSplitBySuburbs:[NSMutableArray arrayWithCapacity:0]];
	for (Stop *stop in self.stopList)
	{
		if ([self.suburbList count] == 0 || ![[self.suburbList objectAtIndex:[self.suburbList count]-1] isEqual:stop.suburbName])
		{
			[self.suburbList addObject:stop.suburbName];
			[self.stopListSplitBySuburbs addObject:[NSMutableArray arrayWithCapacity:0]];
		}
		[[self.stopListSplitBySuburbs objectAtIndex:[self.stopListSplitBySuburbs count]-1] addObject:stop];
	}
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return self.stopList == nil ? 1 : [self.suburbList count];
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	// get the number of stops that match a suburb name
	return [[self.stopListSplitBySuburbs objectAtIndex:section] count];
}

//
// Get the header title
//
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	return [self.suburbList objectAtIndex:section];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
	static NSString *LowAccuracyCellIdentifier = @"LowAccuracyCell";
	static NSString *DisabledCellIdentifier = @"DisabledCell";
	static NSString *NoArrivalTimeCell = @"NoArrivalTimeCell";

	// find the stop
	Stop *stop = [[self.stopListSplitBySuburbs objectAtIndex:[indexPath indexAtPosition:0]] objectAtIndex:[indexPath indexAtPosition:1]];
	
	// find the predicted arrival date and time
	JourneyStop *jstop = [self.journey journeyStopForStop:stop];
	BOOL isDisabledStop = jstop == nil || [self.parent isAtLayover];
	BOOL isCBDStop = ([self.journey.stops count] > 0 && [[(JourneyStop *)[self.journey.stops objectAtIndex:0] stop] isCityStop]);
	BOOL isLowAccuracyStop =  isCBDStop || self.parent.accuracyLevel > 3;
	//BOOL isLowAccuracyStop = self.parent.accuracyLevel > 3;
	BOOL isAvailable = [self.journey.tram isAvailable];
	
	// find out if its a highlightable stop
	BOOL isHighlightableStop = NO;

	if ([self.parent isStopped] || isDisabledStop)
		isHighlightableStop = NO;
	else if (![stop isCityStop] && self.parent.accuracyLevel <= 3)
		isHighlightableStop = [stop isEqual:self.nextStop];
	else if (isLowAccuracyStop && self.nextStop != nil)
	{
		// if we're experiencing a bad connection we don't show anything
		if ([self.parent hasBadConnection])
		{
			isHighlightableStop = NO;
		
		} else
		{
			// in the city (or with bad GPS) we highlight stops with a predicted arrival <= 1.5 minutes
			isHighlightableStop = ([stop isEqual:self.nextStop] || [jstop.predicatedArrivalDateTime timeIntervalSinceNow] <= (isCBDStop ? 150 : 90));
			
			// are we more than 2 away from the next stop?
			if (isHighlightableStop)
			{
				NSInteger indexOfNextStop = self.nextStop == nil ? 0 : [self.stopList indexOfObject:self.nextStop];
				NSInteger indexOfCurrentStop = [self.stopList indexOfObject:stop];
				
				if (indexOfNextStop != NSNotFound && indexOfCurrentStop != NSNotFound && indexOfNextStop+2 <= indexOfCurrentStop)
					isHighlightableStop = NO;
			}
			
			// make sure that the previously highlighted stops list doens't disagree
			if ([previouslyHighlightedStops indexOfObject:stop.trackerID] == NSNotFound)
			{
				// not found in the list. if its to be highlighted add it to the list
				if (isHighlightableStop)
					[previouslyHighlightedStops addObject:stop.trackerID];

			} else {
				// it is in the previously highlighted list, for consistency we dont change our mind about highlights
				// (if its highlighted, it *stays* highlighted)
				if (!isHighlightableStop)
					isHighlightableStop = YES;
			}
		}
	}
	
	if (indexPathToSelectedStop != nil && [indexPathToSelectedStop compare:indexPath] == NSOrderedSame)
	{
		//NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		BOOL arrivalAlarmEnabled = NO; // ARRIVAL ALARM SUPPORT. Fixed at NO. Change back to get the icon back. [defaults boolForKey:@"backgroundEnabled"];
		NSString *identifier = (isDisabledStop ? @"UnderCellDisabled" : (arrivalAlarmEnabled ? @"UnderCellEnabledWithAlarm" : @"UnderCellEnabledNoAlarm"));
		OnboardUnderCell *cell = (OnboardUnderCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
		if (cell == nil)
		{
			cell = [[OnboardUnderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier allowsAlarm:(arrivalAlarmEnabled && !isDisabledStop)];
		}
		[cell setDelegate:self];
		[cell setStop:stop];


		if (cell.alarmButton != nil)
		{
			[cell.alarmButton removeTarget:cell action:NULL forControlEvents:UIControlEventTouchUpInside];
            
            /**
             * ARRIVAL ALARM SUPPORT
             *
             * Change the if statement to some way of detecting whether the alarm is set for this stop. Toggles the
             * cancel alarm button
             *
			if (d.backgroundLocationService != nil && [d.backgroundLocationService.destination isEqualToStop:stop])
			{
				[cell.alarmButton setTitle:NSLocalizedString(@"onboard-button-alarm-cancel", nil) forState:UIControlStateNormal];
				[cell.alarmButton addTarget:cell action:@selector(cancelArrivalAlarm) forControlEvents:UIControlEventTouchUpInside];
			} else
			{
				[cell.alarmButton setTitle:NSLocalizedString(@"onboard-button-alarm-set", nil) forState:UIControlStateNormal];
				[cell.alarmButton addTarget:cell action:@selector(setArrivalAlarm) forControlEvents:UIControlEventTouchUpInside];
			} **/
		}
		
		
		return cell;
	}
	
	NSInteger stopStyle = isLowAccuracyStop && isHighlightableStop ? OnboardCellStyleMinutesOnRightLowAccuracy : OnboardCellStyleMinutesOnRight;
	NSString *useThisCellIdenfitier = (!isAvailable ? NoArrivalTimeCell : (isDisabledStop ? DisabledCellIdentifier : (isLowAccuracyStop && isHighlightableStop ? LowAccuracyCellIdentifier : CellIdentifier)));
	NSInteger useThisCellStyle = (!isAvailable ? OnboardCellStyleNone : (isDisabledStop ? OnboardCellStyleMinutesOnRightDisabled : stopStyle));
	NSInteger cellWidth = (useThisCellStyle == OnboardCellStyleNone ? self.tableView.frame.size.width : self.tableView.frame.size.width-60);
	
	OnboardListCell *cell = (OnboardListCell *)[tableView dequeueReusableCellWithIdentifier:useThisCellIdenfitier];
    if (cell == nil) {
        cell = [[OnboardListCell alloc] initWithFrame:CGRectMake(0, 0, cellWidth, 44)
									   reuseIdentifier:useThisCellIdenfitier
											 cellStyle:useThisCellStyle];

		// not selectable
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
		
		// pass ourselves as a delegate
		[cell setDelegate:self];
	}

	// find out if this stop has a turn, show the button if so.
	if (!isDisabledStop && shouldShowTurnIndicators)
	{
		Turn *turn = [self.journey.tram.route turnForStop:stop upDirection:[self.journey.tram upDirection]];
		[cell setTurnIndicatorImage:(turn == nil ? nil : [turn image]) withMessage:turn.message];
		if (self.indexPathToOpenTurnIndicator != nil && [self.indexPathToOpenTurnIndicator compare:indexPath] == NSOrderedSame)
			[cell showTurnMessage];
		else
			[cell hideTurnMessage];
	} else {
		[cell hideTurnMessage];
		[cell setTurnIndicatorImage:nil withMessage:nil];
	}
	
	// are we the arrival cell?
	[cell.alarmIndicator setHidden:YES];
	[cell setDestinationStop:NO];

    /**
     * ARRIVAL ALARM SUPPORT
     *
     * This controls the small yellow alarm icon and whether the stop is highlighted yellow.
     *
     * Change the IF statement to something that checks for an active alarm for the current tram number and stop
	if (d.backgroundLocationService != nil && [d.backgroundLocationService.journey.tram isEqualToTram:self.journey.tram] &&
		[d.backgroundLocationService.destination isEqualToStop:stop])
	{
		[cell.alarmIndicator setHidden:NO];
		if (!isHighlightableStop)
			[cell setDestinationStop:YES];
	} **/
	
	// Set up the cell...
	[cell setTrackSegmentView:[stop viewForTrackOnRoute:self.journey.tram.route isDisabled:isDisabledStop && isAvailable]];
	
	// add the stop name
	[cell.name setText:[stop formattedName]];
	NSString *voiceOverString;
	
	// and the time to arrival
	if (jstop != nil && isAvailable)
	{
		NSDate *offsetPredictedArrivalDate = ([jstop.predicatedArrivalDateTime respondsToSelector:@selector(dateByAddingTimeInterval:)] ? [jstop.predicatedArrivalDateTime dateByAddingTimeInterval:self.offset] : [jstop.predicatedArrivalDateTime dateByAddingTimeInterval:self.offset]);
		NSString *predictedArrivalString = [Prediction formattedPredictedArrivalDateTime:offsetPredictedArrivalDate];
		NSString *predictedArrivalVoiceOverString = [Prediction voiceOverPredictedArrivalDateTime:offsetPredictedArrivalDate];
		voiceOverString = [NSString stringWithFormat:@"%@ is due to arrive at stop %@ %@", [self.journey.tram name], [jstop.stop number], [jstop.stop name]];

		// if this is the first stop in the journey, and we're closer than 2 minutes then update it
		if (![self.parent isStopped] && self.parent.accuracyLevel <= 3 && ![stop isCityStop] && [stop isEqual:self.nextStop] && [offsetPredictedArrivalDate timeIntervalSinceNow] <= 120)
		{
			// are we pretty damn close?
			if (self.atStop != nil && [self.atStop isEqual:stop])
			{
				predictedArrivalString = NSLocalizedString(@"onboard-now", @"now");
				predictedArrivalVoiceOverString = predictedArrivalString;
			}
			else
			{
				predictedArrivalString = NSLocalizedString(@"onboard-next", @"next");
				predictedArrivalVoiceOverString = predictedArrivalString;
			}
		}

		[cell.arrival setText:predictedArrivalString];
		[cell setAccessibilityLabel:[NSString stringWithFormat:@"%@ %@", voiceOverString, predictedArrivalVoiceOverString]];

	} else if (isAvailable)
	{
		[cell.arrival setText:NSLocalizedString(@"onboard-disabled", @"--")];
		[cell setAccessibilityLabel:[NSString stringWithFormat:@"%@ is not stopping at stop %@ %@", [self.journey.tram name], [jstop.stop number], [jstop.stop name]]];
	}
	
	// show/hide the connection indicators
	[cell.connectingTrain setHidden:!([stop hasConnectingTrains])];
	[cell.connectingTram setHidden:!([stop hasConnectingTrams])];
	[cell.connectingBus setHidden:!([stop hasConnectingBuses])];
	
	// is this cell highlighted?
	[cell setHighlightedStop:isHighlightableStop];

    return cell;
}


/*- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // We don't want users selecting cells on this view yet
	return nil;
}*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPathToSelectedStop != nil)
	{
		NSIndexPath *oldIndexPath = indexPathToSelectedStop;
		 indexPathToSelectedStop = nil;
		[self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:oldIndexPath] withRowAnimation:UITableViewRowAnimationFade];

		if ([oldIndexPath compare:indexPath] == NSOrderedSame)
		{
			 oldIndexPath = nil;
			return;
		}
		 oldIndexPath = nil;
	}

	indexPathToSelectedStop = indexPath;
	[self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPathToSelectedStop] withRowAnimation:UITableViewRowAnimationFade];

	
	/*[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:1];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:)];
	[cell.layer setTransform:CATransform3DRotate(cell.layer.transform, (90 * M_PI / 180), cell.frame.size.height/2, 0, 0)];
	[UIView commitAnimations];*/
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
	// trigger a refresh of the table row
}

- (void)showConnectionsForStop:(Stop *)stop
{
	//NSLog(@"Showing connections for stop: %@", stop);

	// find the 10 nearest stops to our central stop
	NSArray *nearbyStops = [[StopList sharedManager] getNearestStopsWithoutDistancesToLocation:stop.location count:10];
	
	

#warning MAPVIEW_INIT
	MapViewControllerOld *mapView;// = [[MapViewController alloc] initWithStopList:nearbyStops];
	[mapView setListViewType:MapViewControllerTypeOldConnectionList];
	[mapView setPurpleStops:self.stopList];
	[mapView setSelectedStop:stop];
	[mapView setTitle:NSLocalizedString(@"onboard-button-connections", nil)];
	[self.parent.navigationController pushViewController:mapView animated:YES];
	
}

- (void)showTurnIndicatorOnlyForCell:(OnboardListCell *)cell
{
	// get a list of visible cells
	NSMutableArray *cells = [[NSMutableArray alloc] initWithCapacity:0];
	[cells setArray:[self.tableView visibleCells]];
	
	// remove our turn indicator cell from it
	[cells removeObject:cell];
	
	// loop over the cells
	[cells makeObjectsPerformSelector:@selector(hideTurnMessage)];
	
	// and we're done
	
	// save the index path of the cell so that we keep it shown through reloads
	[self setIndexPathToOpenTurnIndicator:[self.tableView indexPathForCell:cell]];
}

- (void)hideTurnIndicatorForCell:(OnboardListCell *)cell
{
	NSIndexPath *p = [self.tableView indexPathForCell:cell];
	if (indexPathToOpenTurnIndicator != nil && [indexPathToOpenTurnIndicator compare:p] == NSOrderedSame)
	{
		[self setIndexPathToOpenTurnIndicator:nil];
	}
}

//
// Custom Headers
//
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	// grab a OnboardListSection thingy
	OnboardListSectionHeaderView *view = [[OnboardListSectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 22)];
	
	// if its not the first section then add the image
	if (section != 0)
	{
		// find out if this stop is disabled
		NSArray *s = [self.stopListSplitBySuburbs objectAtIndex:section];
		if ([s count] != 0)
		{
			Stop *stop = [s objectAtIndex:0];
			BOOL isDisabledStop = [self.parent isAtLayover] || ([self.journey.tram isAvailable] && [self.journey journeyStopForStop:stop] == nil);
			[view.imageView setImage:(isDisabledStop ? [stop imageForDisabledEmptyTrackAtStop] : [stop imageForEmptyTrackAtStopOfRoute:self.journey.tram.route])];
		}
	}
	
	// set the title
	[view.title setText:[self tableView:tableView titleForHeaderInSection:section]];

	return view;
}

#pragma mark CLLocationManagerDelegate methods

//
// The location Service has told us there was an error
//
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
	// if they've clicked "no" and we're not permitted to update, dont do anything
	if ([error code] == kCLErrorDenied)
	{
		[manager stopUpdatingLocation];
	}
}


//
// We got a new location
//
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
	// has the location actually changed?
	//NSLog(@"Received location update.");
	CLLocation *newLocation = [locations lastObject];
	
	// the location manager might have a more recent location
	if ([manager.location.timestamp isEqualToDate:[manager.location.timestamp laterDate:newLocation.timestamp]])
		newLocation = manager.location;

	// temporarily update the view controller
	/*OnboardViewController *v = (OnboardViewController *)self.parentViewController;
	[v setHasMessage:YES animated:YES];
	[v.routeInfo setMessage:[newLocation description]];*/
	
	// first thing we do is discard the first result, the cell tower location just isn't accurate enough
//	if (oldLocation == nil)
//	{
//		//NSLog(@"Discarding first location");
//		return;
//	}
	
	// is it more than 2 minutes old?
	if (fabs([newLocation.timestamp timeIntervalSinceNow]) > 120)
	{
		//NSLog(@"Discarding location more than 2 minutes old");
		return;
	}
	
	// have we moved at all? are we now more than 50m from the terminus
	if ([self.parent isAtLayover] && [self.stopList count] > 1)
	{
		Stop *firstStop = [self.stopList objectAtIndex:0];
		Stop *secondStop = [self.stopList objectAtIndex:1];
		if ([self.parent isAtLayover] && [LocationManager distanceFromLocation:newLocation toLocation:firstStop.location] > ([firstStop.length integerValue] + 50) &&
			[firstStop location:newLocation betweenSelfAndStop:secondStop])
		{
			[self.parent setAtLayover:NO];
		}
	}
	
	// update the accuracy indicator
	NSInteger accuracyLevel;
	if (newLocation.horizontalAccuracy <= 50)
		accuracyLevel = 1;
	else if (newLocation.horizontalAccuracy <= 150)
		accuracyLevel = 2;
	else if (newLocation.horizontalAccuracy <= 250)
		accuracyLevel = 3;
	else if (newLocation.horizontalAccuracy <= 500)
		accuracyLevel = 4;
	else
		accuracyLevel = 5;
	
	// has the accuracy level changed? If so reload the table, the next/now may have been disabled.
//	if (self.parent.accuracyLevel != accuracyLevel)
//		[self.tableView reloadData];
	
	// save the new accuracy level
	[self.parent setAccuracyLevel:accuracyLevel];
			
	// if we're still at layover then discard this result
	if ([self.parent isAtLayover])
	{
		//NSLog(@"Discarding location due to being at layover");
		[self.parent updateHeadboardMessage:YES];
		return;
	}
	
	// throw out anything thats too far away
	if (newLocation.horizontalAccuracy >= 500)
	{
		//NSLog(@"Discarding location because the accuracy is too bad");
		[self.parent updateHeadboardMessage:YES];
		return;
	}
	
	// if this is a non-public trip then we don't have scheduled times (and we're in free-flow mode)
	if (![self.journey.tram isAvailable])
	{
		// Find out where the next stop is
		Stop *next = [self nextStopWithLocation:newLocation];
		if (next == nil)
		{
			//NSLog(@"Could not find the next stop value");
			[self.parent updateHeadboardMessage:YES];
			return;
		}
		[self setNextStop:next];
		[self scrollToStop:next];
		[self.tableView reloadData];
		[self.parent updateHeadboardMessage:YES];
		return;
	}
	
	// if we don't hae a full data set we discard this again
	if ([self.journey.stops count] == 0)
	{
		//NSLog(@"Discarding location due to insufficient setup");
		[self.parent updateHeadboardMessage:YES];
		return;
	}
	
	// are we in the CBD?
	if ([[(JourneyStop *)[journey.stops objectAtIndex:0] stop] isCityStop])
	{
		//NSLog(@"Discarding GPS location because we're in the CBD.");
		[self.parent setAccuracyLevel:5];
		[self.parent updateHeadboardMessage:YES];
		return;
	}
	
	// if we're more than 500m from what is considered the current stop then discard this result
	if (self.nextStop != nil && [LocationManager distanceFromLocation:newLocation toLocation:self.nextStop.location] > 500)
	{
		//NSLog(@"Discarding location because its more than 500m from the \"next stop\"");
		[self.parent setAccuracyLevel:5];
		[self.parent updateHeadboardMessage:YES];
		return;
	}

	// Find out where the next stop is
	Stop *next = [self nextStopWithLocation:newLocation];
	if (next == nil)
	{
		//NSLog(@"Could not find the next stop value");
		[self.parent updateHeadboardMessage:YES];
		return;
	}
	
	// Find it in the journey
	JourneyStop *jstop = [self.journey journeyStopForStop:next];
	if (jstop == nil)
	{
		//NSLog(@"Could not find the stop in the journey");
		[self.parent updateHeadboardMessage:YES];
		return;
	}
	
	// if the predicted arrival time at that stop is more than 60 seconds away then something isn't working, unless its the next stop
	NSDate *offsetPredictedArrivalDate = ([jstop.predicatedArrivalDateTime respondsToSelector:@selector(dateByAddingTimeInterval:)] ? [jstop.predicatedArrivalDateTime dateByAddingTimeInterval:self.offset] : [jstop.predicatedArrivalDateTime dateByAddingTimeInterval:self.offset]);
	//NSLog(@"Predicted Arrival Time: %@, With Offset: %@, Travel Time: %.2f", jstop.predicatedArrivalDateTime, offsetPredictedArrivalDate, [offsetPredictedArrivalDate timeIntervalSinceNow]);
	if (fabs([offsetPredictedArrivalDate timeIntervalSinceNow]) > 60 && [self.journey.stops indexOfObject:jstop] > 1)
	{
		//NSLog(@"Discarding location due to impossible travel times.");
		[self.parent updateHeadboardMessage:YES];
		return;
	}
	
	// update the offset
	NSTimeInterval secondsToNextStop = [self secondsToNextStopWithLocation:newLocation];
	NSTimeInterval secondsPastPreviousStop = [jstop.predicatedArrivalDateTime timeIntervalSinceNow] - secondsToNextStop;
	//NSLog(@"%.2f - %.2f", secondsToNextStop, secondsPastPreviousStop);
	[self setOffset:secondsPastPreviousStop*-1];
	
	// calculate the "now" value
	if (self.parent.accuracyLevel <= 3 && ![next isCityStop])
	{
		NSUInteger distanceForNow = [next.length integerValue] > 20 ? [next.length integerValue] : 20;
		if ([offsetPredictedArrivalDate timeIntervalSinceNow] <= 120 &&								// closer than 2 minutes
			[LocationManager distanceFromLocation:next.location toLocation:newLocation] <= distanceForNow &&						// distance is close enough
			newLocation.speed < 40)																	// tram isn't hurtling past the stop
		{
			if (![self.atStop isEqual:next])
			{
				[self setAtStop:next];
				
				// start a timer to kick us on from "now", assuming it hasn't already moved
				[NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(moveOnFromNextStop:) userInfo:nil repeats:NO];
			}
		}
		// are we at the stop but no longer close enough for "next" ?
		else if (self.atStop != nil && [self.atStop isEqual:next])
		{
			// yep, clear the at stop and move to the next stop - we can never go from now->next on the same stop
			[self setAtStop:nil];
			NSInteger indexOfNextStop = [self.stopList indexOfObject:next];
			if (indexOfNextStop + 1 < [self.stopList count])
			{
				// we can move on to the next stop, reset the offset
				[self setOffset:0];
				next = [self.stopList objectAtIndex:indexOfNextStop+1];
			}
		}
	}
	
	// move to that stop
	[self moveToStop:next];
	[self.parent updateHeadboardMessage:YES];
}

#pragma mark -
#pragma mark Stop Calculations and Movement Methods

- (void)moveOnFromNextStop:(NSTimer *)aTimer
{
	// if our at stop is still set to the first stop on the journey then move us onward
	if (self.atStop != nil && [self.atStop isEqual:self.nextStop])
	{
		// yes it is, can we move to the next stop?
		[self setAtStop:nil];
		
		// move it and reset the office
		NSInteger indexOfNextStop = [self.stopList indexOfObject:self.nextStop];
		if (indexOfNextStop + 1 < [self.stopList count])
		{
			// we can move on to the next stop, reset the offset
			[self setOffset:0];
			[self moveToStop:[self.stopList objectAtIndex:indexOfNextStop+1]];
		}
	}
}

- (void)moveToStop:(Stop *)stop
{
	if (stop == nil)
		return;

	// Find it in the journey
	JourneyStop *jstop = [self.journey journeyStopForStop:stop];
	if (jstop == nil)
		return;
	
	// if it is more than the first stop, make it the first stop
	NSInteger indexOfJourneyStop = [self.journey.stops indexOfObject:jstop];
	if (indexOfJourneyStop > 0)
	{
		[self.journey setStops:[self.journey.stops subarrayWithRange:NSMakeRange(indexOfJourneyStop, [self.journey.stops count]-indexOfJourneyStop)]];
	}
	
	// scroll to it
	if (hasAppeared && ![stop isEqual:self.nextStop]) [self scrollToStop:stop];
	[self setNextStop:stop];
	
	// and refresh the table
	[self.tableView reloadData];
}

- (NSIndexPath *)indexPathOfStop:(Stop *)stop
{
	
	NSInteger section = 0;
	NSInteger row = -1;
	for (NSArray *list in self.stopListSplitBySuburbs)
	{
		// check out the list of stops in that suburb to find our row count
		row = [list indexOfObject:stop];
		if (row != NSNotFound)
		{
			NSUInteger indexPath[2] = { section, row };

			return [NSIndexPath indexPathWithIndexes:indexPath length:2];
		}
		section++;
	}
	return nil;
}

- (void)scrollToStop:(Stop *)stop
{
	NSIndexPath *indexPath = [self indexPathOfStop:stop];
	if (indexPath == nil)
		return;

	// scroll to it
	if (indexPath.row < 1)
		[self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
	else
	{
		NSUInteger scrollIndexPath[2];
		if (indexPath.row == 0)
		{
			scrollIndexPath[0] = indexPath.section - 1;
			scrollIndexPath[1] = [[self.stopListSplitBySuburbs objectAtIndex:(indexPath.section - 1)] count] - 1;
		} else
		{
			scrollIndexPath[0] = indexPath.section;
			scrollIndexPath[1] = indexPath.row - 1;
		}
		[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathWithIndexes:scrollIndexPath length:2] atScrollPosition:UITableViewScrollPositionTop animated:YES];
	}
}

/**
 * Find the next stop in the stop list according to the provided location
**/
- (Stop *)nextStopWithLocation:(CLLocation *)location
{
	// journey times are available
	if ([self.journey.tram isAvailable])
	{
		// we start at the start of the journey - can never go backwards
		if (self.journey.stops == nil || [self.journey.stops count] == 0)
			return nil;
		
		// start counting the location
		NSInteger locationInJourney = 0;
		while (locationInJourney <= 2)
		{
			// gone beyond the end of the journey?
			if ([self.journey.stops count] <= locationInJourney)
				return nil;

			JourneyStop *jNext = [self.journey.stops objectAtIndex:locationInJourney];
			NSInteger locationInStopList = [self.stopList indexOfObject:jNext.stop];

			// if this is the very first stop in the list then skip it and check the next two
			// we default back to the first anyway if we can't find it elsewhere
			if (locationInStopList == NSNotFound || locationInStopList == 0)
			{
				locationInJourney++;
				continue;
			}

			// the "previous" stop according to the stop list
			Stop *previous = [self.stopList objectAtIndex:locationInStopList-1];
			
			// are we between these two stops?
			if ([previous location:location betweenSelfAndStop:jNext.stop])
			{
				// this is our next stop then. Are we so-close to the previous stop that we're likely to be parked at it?
				if ([LocationManager distanceFromLocation:location toLocation:previous.location] <= 20)
					return previous;
				
				// yep, this is our next stop
				return jNext.stop;
			}

			// nope, try more
			locationInJourney++;
		}
		
		// nothing, let it go to its default
		return nil;
	} else
	{
		// journey times are not available
		if (self.stopList == nil || [self.stopList count] == 0)
			return nil;

		// if we don't have a next stop use the location to find the nearest and use that as our reference
		Stop *next = self.nextStop;
		NSInteger nextIndex = NSNotFound;
		if (next == nil)
		{
			// find the closest
			next = [(StopDistance *)[[StopList sharedManager] getNearestStopToLocation:location withStopList:self.stopList] stop];
			
			// if we have found one use find its index
			if (next != nil)
				nextIndex = [self.stopList indexOfObject:next];
		} else
		{
			// find the index
			nextIndex = [self.stopList indexOfObject:next];
		}
		
		// any good?
		if (next == nil || nextIndex == NSNotFound)
			return nil;

		// ok start working forward as normal
		NSInteger locationInStopList = nextIndex;
		while (locationInStopList+1 < [self.stopList count])
		{
			// if this is the very first stop in the list then skip it and check the next two
			// we default back to the first anyway if we can't find it elsewhere
			if (locationInStopList == NSNotFound || locationInStopList == 0)
			{
				locationInStopList++;
				continue;
			}
			
			// the "previous" stop according to the stop list
			Stop *previous = [self.stopList objectAtIndex:locationInStopList-1];

			// are we between these two stops?
			if ([previous location:location betweenSelfAndStop:next])
			{
				// this is our next stop then. Are we so-close to the previous stop that we're likely to be parked at it?
				if ([LocationManager distanceFromLocation:location toLocation:previous.location] <= 20)
					return previous;
				
				// yep, this is our next stop
				return next;
			}
			
			// nope, try more
			locationInStopList++;
		}
		
		// nothing, let it go to its default
		return nil;
	}
}

/**
 * Find out how far away we are from the next stop using the distance to it
**/
- (NSTimeInterval)secondsToNextStopWithLocation:(CLLocation *)location
{
	// find the next stop
	Stop *next = [self nextStopWithLocation:location];
	if (next == nil)
		return 0;
	
	// find the previous stop in the list
	NSInteger locationInStopList = [self.stopList indexOfObject:next];
	if (locationInStopList == 0 || locationInStopList == NSNotFound)
		return 0;
	Stop *previous = [self.stopList objectAtIndex:locationInStopList];
	
	// find the distance that we've travelled
	TTDistanceTravelled distance = [previous distanceTravelledToStop:next atLocation:location];
	if (distance.totalDistance == -1 || distance.totalDistance == 0)
		return 0;
	
	// so how much is left to go?
	CGFloat percentageLeftToNextStop = (distance.totalDistance - distance.travelledDistance) / distance.totalDistance;

	// work that out
	JourneyStop *jstop = [self.journey journeyStopForStop:next];
	if (jstop == nil)
		return 0;

	// so this is the interval until we reach that next stop
	NSTimeInterval secondsToGo = [jstop.predicatedArrivalDateTime timeIntervalSinceNow] * percentageLeftToNextStop;
	
	// all done
	return secondsToGo;
}

#pragma mark -
#pragma mark Arrival Alarm

/**
 * ARRIVAL ALARM SUPPORT
 *
 * Controls the set or cancellation of an arrival alarm. The touching the button will call these.
 *
- (void)setArrivalAlarmAtStop:(Stop *)stop
{
	
	// about to do the arrival alarm, check first to see if we need to popup an alert
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSInteger seenAlertCount = [defaults integerForKey:@"seenArrivalAlarmAlertCount"];
	if (seenAlertCount > 1 || destination != nil)
	{

		// simply really
		tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
		[d startBackgroundLocationServiceForJourney:self.journey arrivingAt:stop];
		
		// stop the selection of the stop
		[indexPathToSelectedStop release]; indexPathToSelectedStop = nil;
		[self.tableView reloadData];

		// are we already at the stop?
		if ([destination isEqualToStop:self.nextStop])
		{
			[d.backgroundLocationService alertUser];
			[d cancelBackgroundLocationService];
		}
		
		// update the badge value immediately
		JourneyStop *jstop = [self.journey journeyStopForStop:stop];
		NSTimeInterval timeToArrival = [jstop.predicatedArrivalDateTime timeIntervalSinceNow];
		[d.backgroundLocationService updateBadgeValue:timeToArrival];
		
		[destination release]; destination = nil;
		

	} else
	{
		NSString *message = [NSString stringWithFormat:NSLocalizedString(@"background-location-confirmation", nil), [stop formattedName], [[UIDevice currentDevice] name]];
		if (![[UIDevice currentDevice] respondsToSelector:@selector(isMultitaskingSupported)] || ![[UIDevice currentDevice] isMultitaskingSupported])
			message = [NSString stringWithFormat:NSLocalizedString(@"background-location-confirm3g", nil), message, [[UIDevice currentDevice] name]];
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
														message:message
													   delegate:self
											  cancelButtonTitle:NSLocalizedString(@"background-location-confirmcancel", nil)
											  otherButtonTitles:NSLocalizedString(@"background-location-confirmok", nil), nil];
		[alert show];
		
		[defaults setInteger:seenAlertCount+1 forKey:@"seenArrivalAlarmAlertCount"];
		[defaults synchronize];
		[alert release];

		[stop retain];
		[destination release];
		destination = stop;
	}
}

- (void)cancelArrivalAlarm
{
	// simply really
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	[d cancelBackgroundLocationService];

	// stop the selection of the stop
	[indexPathToSelectedStop release]; indexPathToSelectedStop = nil;
	
	// and redraw
	[self.tableView reloadData];
}
 

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == -1 || buttonIndex == alertView.cancelButtonIndex)
	{
		[destination release]; destination = nil;
	} else
	{
		[self setArrivalAlarmAtStop:destination];
	}
} **/

#pragma mark -
#pragma mark Cleanup



@end


