//
//  HelpTicketOutletsController.m
//  tramTRACKER
//
//  Created by Robert Amos on 8/08/10.
//  Copyright 2010 Yarra Trams. All rights reserved.
//

#import "HelpTicketOutletsController.h"
#import "HelpWebController.h"

const NSString *HelpURLSubdirTicketOutlets = @"tickets";

#define HELP_TICKETS_BROWSE 0
#define HELP_TICKETS_MAPS 1
#define	HELP_TICKETS_NEARSTOPS 2
#define HELP_TICKETS_DETAILS 3

@implementation HelpTicketOutletsController

- (id)initHelpTicketOutletsController
{
	if (self = [super initWithStyle:UITableViewStylePlain])
	{
		[self setTitle:NSLocalizedString(@"help-tickets", @"Help")];
		[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
	}
	return self;
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}




#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    SimpleCell *cell = (SimpleCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[SimpleCell alloc] initWithCellStyle:TTSimpleCellStyleDefault reuseIdentifier:CellIdentifier];
		[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
		
		GradientBackgroundCell *background = [[GradientBackgroundCell alloc] initWithFrame:CGRectMake(0, 0, 320, 44) cellStyle:OnboardCellStyleNone];
		[cell setBackgroundView:background];
	}
    
	switch (indexPath.row)
	{
		case HELP_TICKETS_BROWSE:
			[cell setTextLabelText:NSLocalizedString(@"help-tickets-browse", @"Browsing and Searching")];
			break;
		case HELP_TICKETS_MAPS:
			[cell setTextLabelText:NSLocalizedString(@"help-tickets-maps", @"Maps")];
			break;
		case HELP_TICKETS_DETAILS:
			[cell setTextLabelText:NSLocalizedString(@"help-tickets-details", @"Outlet Details")];
			break;
		case HELP_TICKETS_NEARSTOPS:
			[cell setTextLabelText:NSLocalizedString(@"help-tickets-nearstops", @"Outlets Near Stops")];
			break;
	}
	
    return cell;	
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	// we create a UIWebView that we push onto the stack, start by working out our URL
	NSString *fileName = nil;
	
	switch (indexPath.row)
	{
		case HELP_TICKETS_BROWSE:
			fileName = @"browse.html";
			break;
			
		case HELP_TICKETS_MAPS:
			fileName = @"maps15.html";
			break;
		
		case HELP_TICKETS_DETAILS:
			fileName = @"details.html";
			break;
			
		case HELP_TICKETS_NEARSTOPS:
			fileName = @"nearstops.html";
			break;
	}
	
	// nothing found?
	if (fileName == nil)
		return;
	
	// create the web view
	HelpWebController *helpWeb = [[HelpWebController alloc] initWithHelpFile:[NSString stringWithFormat:@"%@/%@", HelpURLSubdirTicketOutlets, fileName]];
	SimpleCell *cell = (SimpleCell *)[self.tableView cellForRowAtIndexPath:indexPath];
	[helpWeb setTitle:cell.textLabel.text];
	[self.navigationController pushViewController:helpWeb animated:YES];
}





@end

