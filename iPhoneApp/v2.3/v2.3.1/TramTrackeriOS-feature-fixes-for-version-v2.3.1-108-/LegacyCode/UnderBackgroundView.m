//
//  UnderBackgroundView.m
//  tramTRACKER
//
//  Created by Robert Amos on 2/04/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "UnderBackgroundView.h"


@implementation UnderBackgroundView


- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
		[self setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
    }
    return self;
}


- (void)drawRect:(CGRect)rect {
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	// colour components and locations
	CGFloat locations[3] = { 0.0, 0.2, 1.0 };
	CGFloat components[12] = 
	{
		0.0, 0.0, 0.0, 0.6,
		0.0, 0.0, 0.0, 0.4,
		0.0, 0.0, 0.0, 0.0
	};
	
	// make our gradient
	CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB();
	CGGradientRef gradient = CGGradientCreateWithColorComponents(space,	components, locations, 3);
	
	// draw our gradient in our view
	CGContextSaveGState(context);
//	CGContextClipToRect(context, CGRectMake(0, 0, rect.size.width, 10));
	CGContextDrawLinearGradient(context, gradient, CGPointMake(0, 0), CGPointMake(0, 10), 0);
	CGGradientRelease(gradient);
	CGColorSpaceRelease(space);
}




@end
