//
//  NearbyListController.m
//  tramTRACKER
//
//  Created by Robert Amos on 4/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "NearbyListController.h"
#import <objc/runtime.h>
#import "SimpleCell.h"
#import "SectionHeaderView.h"
#import "Constants.h"


@interface NearbyListController()

@property (nonatomic, strong) SectionHeaderView *headerView;

@end

@implementation NearbyListController

@synthesize stopList, locationManager, accuracyLabel, locatingStarted, nearestButton, locatingImage, nearestLabel, nearbyViewController;

- (id)initNearbyListWithNearbyViewController:(NearbyViewControllerOld *)pVC
{
	if ((self = [self initWithStyle:UITableViewStylePlain]))
	{
		// set the parent view controller, we deliberately don't retain this.
		nearbyViewController = pVC;

		sameStopResult = 0;
		
		// the view has loaded, fire up the location manager to find our nearest stop
		LocationManager *manager = [[LocationManager alloc] init];
		[self setLocationManager:manager];
		
		[self.locationManager setDelegate:self];
		[self.locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
	}
	return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];

	[self.tableView setBackgroundColor:[UIColor whiteColor]];
	
	if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
	{
		[self.tableView setSeparatorInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
	}

	//[self.tableView setTableFooterView:[self tableFooterView]];
	
	//Add the pull to refresh control
	UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
	[refreshControl addTarget:self action:@selector(refreshLocation) forControlEvents:UIControlEventValueChanged];
	[self setRefreshControl:refreshControl];
	
    self.headerView = [SectionHeaderView nearbySectionHeaderWithTitle:NSLocalizedString(@"nearby-header-all-stops", @"All Stops")];
}

// Control the location manager based on when we can be seen
- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
		
	if (self.stopList == nil || [self.stopList count] == 0)
	{
		[self refreshLocation];
	}
}
- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
	[self.locationManager stopUpdatingLocation];
}

//
// Create the table footer
//
- (UIView *)tableFooterView
{
	CGRect frame = CGRectMake(0, 0, self.tableView.frame.size.width, 150);
	UnderBackgroundView *ub = [[UnderBackgroundView alloc] initWithFrame:frame];
	
	// if there are no location services just display an error.
	if (![CLLocationManager locationServicesEnabled])
	{
		// Text to say you can touch the tab again
		UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 70, 280, 44)];
		[label setText:NSLocalizedString(@"nearby-corelocation-required", @"Core Location Required")];
		[label setTextColor:[UIColor darkGrayColor]];
		[label setFont:[UIFont systemFontOfSize:14]];
		[label setTextAlignment:NSTextAlignmentCenter];
		[label setNumberOfLines:0];
		[label setOpaque:NO];
		[label setBackgroundColor:[UIColor clearColor]];
		[ub addSubview:label];
	} else
	{
		// locating image
//		UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(130, 75, 60, 60)];
//        
//        if ([[UIScreen mainScreen] bounds].size.height == 568) {
//            image.center = CGPointMake(self.tableView.bounds.size.width / 2 , 220);
//        }else{
//            image.center = CGPointMake(self.tableView.bounds.size.width / 2 , 170);
//        }
//        
//		[image setAnimationImages:[NSArray arrayWithObjects:[UIImage imageNamed:@"radar-frame1.png"], [UIImage imageNamed:@"radar-frame2.png"],
//								   [UIImage imageNamed:@"radar-frame3.png"], [UIImage imageNamed:@"radar-frame4.png"],
//								   [UIImage imageNamed:@"radar-frame5.png"], [UIImage imageNamed:@"radar-frame6.png"],
//								   [UIImage imageNamed:@"radar-frame7.png"], [UIImage imageNamed:@"radar-frame8.png"],
//								   [UIImage imageNamed:@"radar-frame9.png"], [UIImage imageNamed:@"radar-frame10.png"],
//								   [UIImage imageNamed:@"radar-frame11.png"], [UIImage imageNamed:@"radar-frame12.png"],
//								   [UIImage imageNamed:@"radar-frame13.png"], [UIImage imageNamed:@"radar-frame14.png"],
//								   [UIImage imageNamed:@"radar-frame15.png"], [UIImage imageNamed:@"radar-frame16.png"],
//								   [UIImage imageNamed:@"radar-frame17.png"], [UIImage imageNamed:@"radar-frame18.png"],
//								   [UIImage imageNamed:@"radar-frame19.png"], [UIImage imageNamed:@"radar-frame20.png"],
//								   [UIImage imageNamed:@"radar-frame21.png"], [UIImage imageNamed:@"radar-frame22.png"],
//								   [UIImage imageNamed:@"radar-frame23.png"], [UIImage imageNamed:@"radar-frame24.png"],
//								   [UIImage imageNamed:@"radar-frame25.png"], [UIImage imageNamed:@"radar-frame26.png"],
//								   [UIImage imageNamed:@"radar-frame27.png"], [UIImage imageNamed:@"radar-frame28.png"],
//								   [UIImage imageNamed:@"radar-frame29.png"], [UIImage imageNamed:@"radar-frame30.png"],
//								   nil]];
//		[image setAnimationDuration:2];
//		[ub addSubview:image];
//		[self setLocatingImage:image];
		
		UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 5, 280, 40)];
		[label setHidden:YES];
		[label setTextColor:[UIColor darkGrayColor]];
		[label setFont:[UIFont systemFontOfSize:14]];
		[label setTextAlignment:NSTextAlignmentCenter];
		[label setNumberOfLines:0];
		[label setOpaque:NO];
		[label setBackgroundColor:[UIColor clearColor]];
		[ub addSubview:label];
		[self setAccuracyLabel:label];
	}
	return ub;
}

//- (UIView *)tableHeaderView;

- (void)refreshLocation
{
	[self setStopList:nil];

	// if we're allowed
	if ([CLLocationManager locationServicesEnabled])
	{
		[self.locationManager startUpdatingLocation];

		if ([self.refreshControl isRefreshing] == NO)
		{
			//NSLog(@"Beginning refresh");
			[self.refreshControl beginRefreshing];
			//The refresh control sometimes doesn't update the contentOffset, so we manually do it
			if (self.tableView.contentOffset.y == 0.0)
			{
				[self.tableView setContentOffset:CGPointMake(0.0, -self.refreshControl.frame.size.height) animated:NO];
			}
		}

		[self.nearestButton setHidden:YES];
		[self.nearestLabel setHidden:YES];
		[self.accuracyLabel setHidden:YES];
		[self setLocatingStarted:[NSDate date]];

		// show the animation
		[self.locatingImage startAnimating];
		[self.locatingImage setHidden:NO];

		// set a timer to display results
		[NSTimer scheduledTimerWithTimeInterval:kNearby_Threshold_200M target:self selector:@selector(displayStopsFromTimer:) userInfo:nil repeats:NO];
		[NSTimer scheduledTimerWithTimeInterval:kNearby_Threshold_500M target:self selector:@selector(displayStopsFromTimer:) userInfo:nil repeats:NO];
		[NSTimer scheduledTimerWithTimeInterval:kNearby_Threshold_MAX target:self selector:@selector(displayStopsFromTimer:) userInfo:nil repeats:NO];

		// fade the nearest stop button out
		if (self.nearestButton != nil)
			[self.nearestButton setEnabled:NO];
	}
}
#pragma mark Table view methods

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	return self.headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	CGFloat height = 0.0f;
	if (section == 0 && [self.refreshControl isRefreshing] == NO && self.stopList != nil)
	{
		height = self.headerView.bounds.size.height;
	}
	else
	{
		height = 0.0f;
	}
	
	return height;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (!self.stopList)
		return 0;
	return [self.stopList count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
	static NSString *cellIdentifier = @"NearbyCell";

	NearbyCell *cell = (NearbyCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
//	if (cell == nil) {
//        cell = [NearbyCell viewWithDefaultNib];		
//	}
	
	// Set up the cell...
	StopDistance *stop = [self.stopList objectAtIndex:[indexPath indexAtPosition:1]];

	// set the distance to the stop
	[cell.distance setText:[stop formattedDistance]];

	// and the stop name
	[cell.name setText:[stop.stop formattedName]];
	
	// and the route description
	[cell.routeDescription setText:[stop.stop formattedRouteDescription]];

	return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 80.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	StopDistance *stop = [stopList objectAtIndex:[indexPath indexAtPosition:1]];
	[self pushPIDForStop:stop.stop];
}

- (void)pushPIDForStop:(Stop *)stop 
{
	// Start the pid view
	PIDViewControllerOld *PID = nil;
    if ([[UIScreen mainScreen] bounds].size.height == 568) {
        PID = [[PIDViewControllerOld alloc] initWithNibName:@"PIDView-iPhone5" bundle:[NSBundle mainBundle]];
    }
    else{
        PID = [[PIDViewControllerOld alloc] initWithNibName:@"PIDView" bundle:[NSBundle mainBundle]];
    }
	
	// get the stop info
	[PID startWithStop:stop];
	
	// push it onto the stack
	if (self.nearbyViewController != nil)
	{
		[self.nearbyViewController.navigationController pushViewController:PID animated:YES];
	} else
	{
		[self.navigationController pushViewController:PID animated:YES];
	}
}

- (void)pushPIDForStop:(Stop *)stop withFilter:(Filter *)filter animated:(BOOL)animated
{
	// Start the pid view
	PIDViewControllerOld *PID = nil;
    if ([[UIScreen mainScreen] bounds].size.height == 568) {
        PID = [[PIDViewControllerOld alloc] initWithNibName:@"PIDView-iPhone5" bundle:[NSBundle mainBundle]];
    }
    else{
        PID = [[PIDViewControllerOld alloc] initWithNibName:@"PIDView" bundle:[NSBundle mainBundle]];
    }
	
	// get the stop info
	[PID setFilter:filter];
	[PID startWithStop:stop];
	
	// push it onto the stack
	if (self.nearbyViewController != nil)
	{
		[self.nearbyViewController.navigationController pushViewController:PID animated:animated];
	} else
	{
		[self.navigationController pushViewController:PID animated:animated];
	}
}

- (void)pushPIDForStop:(Stop *)stop withFilter:(Filter *)filter
{
	[self pushPIDForStop:stop withFilter:filter animated:YES];
}


- (void)setStopList:(NSArray *)list
{
	stopList = list;
	
	[self.tableView reloadData];
}

// Flip over to show a map view
/*- (void)flipToMapView
{
	// build a stop list based on ourselves
	NSMutableArray *stops = [[NSMutableArray alloc] initWithCapacity:0];
	for (StopDistance *sd in self.stopList)
		[stops addObject:sd.stop];
	
	MapViewController *mapView = [[MapViewController alloc] initWithStopList:stops];
	[stops release];
	
	[mapView setListViewType:TTMapViewListTypeNearbyList];
	UISegmentedControl *segment = (UISegmentedControl *)self.navigationItem.titleView;
	[segment removeTarget:self action:@selector(flipToMapView) forControlEvents:UIControlEventValueChanged];
	[segment addTarget:mapView action:@selector(flipToListView) forControlEvents:UIControlEventValueChanged];
	[mapView.navigationItem setTitleView:segment];
	
	// flip over to the map view
	//[UIView beginAnimations:@"FlipToMapView" context:nil];
	[[self.navigationController.view.subviews objectAtIndex:0] setBackgroundColor:[UIColor blackColor]];
	//[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:[self.navigationController.view.subviews objectAtIndex:0] cache:YES];
	//[UIView setAnimationDuration:1];
	
	UINavigationController *nav = self.navigationController;
	
	// reset the view controllers array
	if ([[nav.viewControllers objectAtIndex:0] isEqual:[self.tabBarController.moreNavigationController.viewControllers objectAtIndex:0]])
		[nav setViewControllers:[NSArray arrayWithObjects:[nav.viewControllers objectAtIndex:0], mapView, nil]];
	else
		[nav setViewControllers:[NSArray arrayWithObject:mapView] animated:NO];
	
	//[UIView commitAnimations];
	
	[mapView release];
} */

//
// Support for pushing a PID with the nearby mode enabled
//
- (void)pushPIDWithNearbyAnimated:(BOOL)animated
{
	if (self.stopList == nil)
		return;
    
	// do we have a pid instance yet?
	PIDViewControllerOld *PID = nil;
    if ([[UIScreen mainScreen] bounds].size.height == 568) {
        PID = [[PIDViewControllerOld alloc] initWithNibName:@"PIDView-iPhone5" bundle:[NSBundle mainBundle]];
    }
    else{
        PID = [[PIDViewControllerOld alloc] initWithNibName:@"PIDView" bundle:[NSBundle mainBundle]];
    }
    
	StopDistance *stop = [self.stopList objectAtIndex:0];
	[PID startWithStop:stop.stop];
	
	// push it onto the stack
	[self.navigationController pushViewController:PID animated:animated];
}
- (void)pushPIDWithNearbyAnimated
{
	[self pushPIDWithNearbyAnimated:YES];
}

//
// The location Service has told us there was an error
//
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
	// if they've clicked "no" and we're not permitted to update, fail over to showing the default stop
	[manager stopUpdatingLocation];
	
	if ([self.refreshControl isRefreshing] == YES) 
	{
		//NSLog(@"Ending refresh");
		[self.refreshControl endRefreshing];
	}
	
	// hide the animation
	[self.locatingImage stopAnimating];
	[self.locatingImage setHidden:YES];
	
	//NSLog(@"%@", error);

	UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"nearby-error-cannotlocate", @"Cannot Locate")
															 delegate:self
													cancelButtonTitle:nil
											   destructiveButtonTitle:NSLocalizedString(@"nearby-error-okbutton", @"OK")
													otherButtonTitles:nil];
	[actionSheet showFromTabBar:self.nearbyViewController.navigationController.tabBarController.tabBar];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
	// has the location actually changed?
	//NSLog(@"Received location update.");
	CLLocation *newLocation = [locations lastObject];

	// the location manager might have a more recent location
	if ([manager.location.timestamp isEqualToDate:[manager.location.timestamp laterDate:newLocation.timestamp]])
		newLocation = manager.location;

	// is it more than 2 minutes old?
	if (fabs([newLocation.timestamp timeIntervalSinceNow]) > 120)
	{
		//NSLog(@"Discarding location more than 2 minutes old");
		return;
	}
	
	// What sort of accuracy do we need for here?
	NSTimeInterval secondsPassed = [[NSDate date] timeIntervalSinceDate:self.locatingStarted];
	NSInteger requiredAccracy = secondsPassed > kNearby_Threshold_500M ? 500 : (secondsPassed > kNearby_Threshold_200M ? 200 : 50);

	// check this location for accuracy
	if (newLocation.horizontalAccuracy <= requiredAccracy)
	{
		//NSLog(@"Required accuracy of %dm has been met (%.2fm). Using location %@", requiredAccracy, newLocation.horizontalAccuracy, newLocation);

		// display the stops
		[self displayStopsWithLocation:newLocation];
		[self.locationManager stopUpdatingLocation];
	} 
	else
	{
		//NSLog(@"Ignoring location %@ because accuracy %.2fm does not meet required accuracy of %dm", newLocation, newLocation.horizontalAccuracy, requiredAccracy);
	}
}
- (void)displayStopsWithLocation:(CLLocation *)location
{
	// stopped refreshing
	if ([self.refreshControl isRefreshing] == YES)
	{
		//NSLog(@"Ending refresh");
		[self.refreshControl endRefreshing];
	}
	[self.nearestButton setEnabled:YES];
	[self.nearestButton setHidden:NO];
	[self.nearestLabel setHidden:NO];
	
	// hide the animation
	[self.locatingImage stopAnimating];
	[self.locatingImage setHidden:YES];
	

	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSNumber *stopCount = [defaults objectForKey:@"nearbyStopCount"];
	if (!stopCount) stopCount = [NSNumber numberWithInt:10];
	
	// find the nearest stops
	NSArray *nearestStops = [[StopList sharedManager] getNearestStopsToLocation:location count:[stopCount intValue]];
	[self setStopList:nearestStops];
	
	// set the accracy label
	if ([self.locationManager.location horizontalAccuracy] == 0)
		[self.accuracyLabel setHidden:YES];
	else
	{
		[self.accuracyLabel setHidden:NO];
		[self.accuracyLabel setText:[NSString stringWithFormat:NSLocalizedString(@"nearby-location-accuracy", @"Accuracy to within"), [location horizontalAccuracy]]];
	}
}
- (void)displayStopsFromTimer:(NSTimer *)aTimer
{
	// get the current location
	if (self.locationManager == nil || self.locationManager.location == nil || [self.locationManager stopped] || ![CLLocationManager locationServicesEnabled])
	{
		// some error handling
		if ([self.refreshControl isRefreshing] == YES)
		{
			//NSLog(@"Ending refresh");		
			[self.refreshControl endRefreshing];
		}

		return;
	}

	// have we reached a threshold?
	// What sort of accuracy do we need for here?
	NSTimeInterval secondsPassed = [[NSDate date] timeIntervalSinceDate:self.locatingStarted];
	NSInteger requiredAccracy = secondsPassed > kNearby_Threshold_500M ? 500 : (secondsPassed > kNearby_Threshold_200M ? 200 : 50);
	
	if (secondsPassed >= kNearby_Threshold_MAX || self.locationManager.location.horizontalAccuracy <= requiredAccracy)
	{
		//NSLog(@"Timeout has been reached, using location %@", self.locationManager.location);
		[self displayStopsWithLocation:self.locationManager.location];
		[self.locationManager stopUpdatingLocation];
	}
}


@end

