//
//  tramTRACKERAppDelegate.m
//  tramTRACKER
//
//  Created by Robert Amos on 17/02/09.
//  Copyright Yarra Trams 2009. All rights reserved.
//

#import "tramTRACKERAppDelegate.h"

#import "OtherLevels.h"
#import "Constants.h"
#import "Filter.h"
#import "BackgroundSynchroniser.h"
#import "AutomaticSynchroniserDataLogger.h"
#import "Tram.h"
#import "SWRevealViewController.h"
#import "DynamicSplashView.h"
#import "TicketRetailer.h"
#import "RouteList.h"
#import "Settings.h"
#import "Analytics.h"
#import "UIAlertView+Blocks.h"
#import "StopList.h"
#import "FTZOverlay.h"
#import "PointOfInterest.h"
#import "PIDViewController.h"
#import "myTramViewController.h"
#import "BCMicroLocationManager.h"
#import "ServiceUpdatesListViewController.h"
#import "Constants.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "BlueCatsSDK.h"

static NSString* OtherLevelsAppKeyDev = @"145f4a66027be2a7fb5530c1ba80ede7";
static NSString* OtherLevelsAppKeyProd = @"736ac1cf8384b4b9cd3fe87ef4dbe2a9";

#define OtherLevelsAppKey OtherLevelsAppKeyDev

@interface tramTRACKERAppDelegate()<BackgroundSynchroniserDelegate, BCMicroLocationManagerDelegate>

@property (strong, nonatomic) BackgroundSynchroniser * syncManager;
@property (strong, nonatomic) BCMicroLocationManager * beaconManager;

/// These fields are used in order to store the information necessary to jump to PID from the splash (possibly now unused?)
@property (strong, nonatomic) Stop* startingStop;
@property (strong, nonatomic) Filter* startingFilter;
@property (nonatomic) BOOL startingLowFloor;
@property (nonatomic) BOOL autoJumpToPID;

@end

@implementation tramTRACKERAppDelegate

//static NSTimeInterval const TTDatabaseModificationDate = 1407478360; // V2.0
//static NSTimeInterval const TTDatabaseModificationDate = 1246492800; // 2 July 09
//static NSTimeInterval const TTDatabaseModificationDate = 1419897600; // NEW (30th Dec 2014)
//static NSTimeInterval const TTDatabaseModificationDate = 1421289742; // NEW (15th Jan 2015)
//static NSTimeInterval const TTDatabaseModificationDate = 1447313493; // NEW (12th Nov 2015)
//static NSTimeInterval const TTDatabaseModificationDate = 1449447456; // NEW (7th Dec 2015)
//static NSTimeInterval const TTDatabaseModificationDate = 1449642285; // NEW (9th Dec 2015)
static NSTimeInterval const TTDatabaseModificationDate = 1450086711; // NEW (14th Dec 2015)

- (BOOL)installUpdates:(NSString*)jsonString
{
    [self createSyncManager];
    return [self.syncManager updateAllContent:jsonString];
}

- (void)downloadMissingMaps
{
    [self createSyncManager];
    [self.syncManager performSelectorOnMainThread:@selector(fetchMissingMaps) withObject:nil waitUntilDone:YES];
}

- (void)checkForUpdates
{
    [self createSyncManager];
    [self.syncManager performSelectorInBackground:@selector(synchroniseInBackgroundThread:) withObject:nil];
}

- (void)createSyncManager
{
    if(self.syncManager)
    { return; }
    
    NSDate* lastSyncDate = [[NSUserDefaults standardUserDefaults] objectForKey:kLastSynchronisationDate];
    if (!lastSyncDate)
    { lastSyncDate = [NSDate dateWithTimeIntervalSince1970:TTDatabaseModificationDate]; }
    
    self.syncManager = [[BackgroundSynchroniser alloc] initWithLastSynchronisationDate:lastSyncDate persistentStoreCoordinator:self.persistentStoreCoordinator];
    self.syncManager.delegate = self;
}

- (void)askForAnalytics
{
    static NSString * const kAnalyticsAskAnalyticsKey = @"askForAnalytics";
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:kAnalyticsAskAnalyticsKey])
    {
        [UIAlertView showWithTitle:@"The tramTRACKER team would like to capture data analytics"
                           message:@"To improve your tramTRACKER experience, we would like to anonymously understand how you navigate through the app. Access can be turned on/off in the app Settings."
                             style:UIAlertViewStyleDefault
                 cancelButtonTitle:@"Don't Allow"
                 otherButtonTitles:@[@"OK"]
                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex)
         {
             [[NSUserDefaults standardUserDefaults] setBool:buttonIndex forKey:kAnalyticsAllowAnalyticsKey];
         }];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kAnalyticsAskAnalyticsKey];
    }
}

- (void)cancelUpdate
{
    [self.syncManager cancel];
    self.syncManager = nil;
}

- (void)checkForUpdatesDidStart
{ self.isUpdating = YES; }

- (void)syncDidFailWithError:(NSError *)error
{ [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationHasFailedUpdating object:error]; }

- (void)checkForUpdatesDidFinish:(NSDictionary *)updatesFound
{
    self.updatesFound = updatesFound;
    
    self.syncManager = nil;
    self.isUpdating = NO;

    NSUInteger          numberOfStopsToBeUpdated = [updatesFound[TTSyncKeyStops] count];
    NSUInteger          numberOfRoutesToBeUpdated = [updatesFound[TTSyncKeyRoutes] count];
    NSUInteger          numberOfTicketsToBeUpdated = [updatesFound[TTSyncKeyTickets] count];
    NSUInteger          numberOfPOIsToBeUpdated = [updatesFound[TTSyncKeyPOIs] count];
    
    NSNumber            * numberOfUpdates = @(numberOfStopsToBeUpdated + numberOfRoutesToBeUpdated + numberOfTicketsToBeUpdated + numberOfPOIsToBeUpdated);
    
    NSString            * numberOfUpdatesString = [numberOfUpdates integerValue] ? [numberOfUpdates stringValue] : nil;

    UITabBarController  * mainTabBarController = (id)[(SWRevealViewController *)self.window.rootViewController frontViewController];

    [[mainTabBarController.viewControllers.lastObject tabBarItem] setBadgeValue:numberOfUpdatesString];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationHasFinishedUpdating object:numberOfUpdates];
}

- (void)microLocationManager:(BCMicroLocationManager *)microLocationManager didEnterSite:(BCSite *)site
{ [self.beaconManager startRangingBeaconsInSite:site]; }

- (void)microLocationManager:(BCMicroLocationManager *)microLocationManager didExitSite:(BCSite *)site
{ [self.beaconManager stopRangingBeaconsInSite:site]; }

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    [OtherLevels setTrackingID:@"@OL@c08fb4b34861a5a87fa4c609c749"];
    
    [OtherLevels startSessionWithAppKey:OtherLevelsAppKey launchOptions:launchOptions andLocationUpdates:NO withAppOpenInterstitial:nil];
    [BlueCatsSDK setOptions:@{ BCOptionShowBluetoothPowerWarningMessage:@NO } ];

    [Settings convert];

    [Fabric with:@[CrashlyticsKit]];
    
    if (![[StopList sharedManager] hasUpgradedFavouritesTo20]) {
        [[StopList sharedManager] upgradeFavouritesTo20];
    }
    
    [self askForAnalytics];
    [[Analytics sharedInstance] appHasLaunched];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
	[self registerDefaults];
    [self configureAppearance];
    [self persistentStoreCoordinator];

    [StopList saveWidgetFavourites];
    
    UILocalNotification* notif = launchOptions[UIApplicationLaunchOptionsLocalNotificationKey];
    NSDictionary* remoteNotif  = launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey];
    
    if(remoteNotif)
    {
        NSNumber* messageType = remoteNotif[@"messageType"];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self jumpToScreenForRemoteNotification:messageType];
        });
    }
    else
    {
        if(notif == nil)
        {
            DynamicSplashView   * splashView = [DynamicSplashView splashWithFrame:self.window.frame];

            [self.window.rootViewController.view addSubview:splashView];
            [self.window.rootViewController.view bringSubviewToFront:splashView];
            [splashView runUpdatesAndDisappear];
        }
        else
        {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self jumpToScreenForLocalNotification:notif];
            });
        }
    }
    
    return YES;
}

- (void)createBeaconManager
{
    if(self.beaconManager == nil)
    {
        self.beaconManager = [BCMicroLocationManager new];
        self.beaconManager.delegate = self;
    }
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    NSLog(@"Local notification received: %@", notification.userInfo);
    if(application.applicationState == UIApplicationStateInactive)
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self jumpToScreenForLocalNotification:notification];
        });
    }
    else if(application.applicationState == UIApplicationStateActive)
    {
        if([notification.userInfo[@"type"] isEqualToString:@"onTram"])
        { [[[UIAlertView alloc] initWithTitle:@"" message:@"Your stop is approaching soon." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show]; }
        else
        { [[[UIAlertView alloc] initWithTitle:@"" message:@"Your tram is approaching soon." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show]; }
    }
}

- (void)jumpToScreenForRemoteNotification:(NSNumber*)messageType
{
    if(messageType == nil)
    { return; }
    
    SWRevealViewController* controller = (SWRevealViewController*)self.window.rootViewController;
    UITabBarController* tabController = (UITabBarController*)controller.frontViewController;
    
    if([messageType isEqualToNumber:@2])
    {
        tabController.selectedIndex = 4;
        UINavigationController* nav = (UINavigationController*)tabController.selectedViewController;
        ServiceUpdatesListViewController* updatesController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:kScreenServiceUpdates];
        [nav pushViewController:updatesController animated:NO];
    }
}

- (void)jumpToScreenForLocalNotification:(UILocalNotification*)notif
{
    // We need to build in a brief delay, in order to give the view controllers time to be set. Otherwise, they will still be nil at the time this code is run.
    SWRevealViewController* controller = (SWRevealViewController*)self.window.rootViewController;
    UITabBarController* tabController  = (UITabBarController*)controller.frontViewController;
    
    NSDictionary* userInfo = notif.userInfo;
    if([userInfo[@"type"] isEqualToString:@"onTram"])
    {
        tabController.selectedIndex = 3;
        UINavigationController* nav = (UINavigationController*)tabController.selectedViewController;
        myTramViewController* myTramController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:kScreenMyTram];
        
        NSNumber* tramId = userInfo[@"tramId"];
        [myTramController setTramNumberString:[tramId stringValue]];
        [nav pushViewController:myTramController animated:NO];
    }
    else if([userInfo[@"type"] isEqualToString:@"atStop"])
    {
        NSNumberFormatter* formatter = [NSNumberFormatter new];
        formatter.numberStyle = NSNumberFormatterDecimalStyle;
        Stop* stop = [Stop stopForTrackerID:userInfo[@"trackerId"]];
        tabController.selectedIndex = 1;
        UINavigationController* nav = (UINavigationController*)tabController.selectedViewController;
        PIDViewController* pidController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:kScreenPID];
        [pidController setCurrentStop:stop];
        [nav pushViewController:pidController animated:NO];
    }
}

- (void)configureAppearance
{
    UIColor * greenColor = [UIColor colorWithRed:105.0f/255.0f green:190.0f/255.0f blue:40.0f/255.0f alpha:1.0f];
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        /// Navigation bar
        [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
        [[UINavigationBar appearance] setBackgroundColor:greenColor];
        
        [[UINavigationBar appearance] setTintColor:[UIColor redColor]];
        [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
        [[UIBarButtonItem appearance] setBackgroundImage:[[UIImage alloc] init] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        
        /// tabbar
        
        UIColor * darkColor = [UIColor colorWithRed:32.0f/256.0f green:33.0f/256.0f blue:31.0f/256.0f alpha:1.0f];
        [[UITabBar appearance] setBackgroundColor:darkColor];
        [[UITabBar appearance] setBackgroundImage:[[UIImage alloc] init]];
        [[UITabBar appearance] setSelectionIndicatorImage:[[UIImage alloc] init]];
        [[UITabBar appearance] setShadowImage:[[UIImage alloc] init]];
        [[UITabBar appearance] setTintColor:[UIColor grayColor]];
        
        /// SearchBar
        [[UISearchBar appearance] setTintColor:[UIColor lightGrayColor]];
    }
    [[UITabBar appearance] setSelectedImageTintColor:greenColor];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    NSLog(@"Url received: %@", url);
    
    SWRevealViewController* controller = (SWRevealViewController*)self.window.rootViewController;
    UITabBarController* tabController  = (UITabBarController*)controller.frontViewController;
    
    if([url.host isEqualToString:@"home"])
    { return YES; }
    else if([url.host isEqualToString:@"nearby"])
    {
        // Jump to Nearby tab
        tabController.selectedIndex = 1;
        UINavigationController* nav = (UINavigationController*)tabController.selectedViewController;
        [nav popToRootViewControllerAnimated:YES];
        return YES;
    }
    else if([url.host isEqualToString:@"favourites"])
    {
        // Jump to favourites tab
        tabController.selectedIndex = 0;
        UINavigationController* nav = (UINavigationController*)tabController.selectedViewController;
        [nav popToRootViewControllerAnimated:YES];
        return YES;
    }
    else if([url.host isEqualToString:@"pid"]) // This needs to be adjusted to include the parameters
    {
        // Push PIDViewController and assign stop
        tabController.selectedIndex = 2;
        UINavigationController* nav = (UINavigationController*)tabController.selectedViewController;
        [nav popToRootViewControllerAnimated:NO];
        
//        UIStoryboard* storyboard         = [UIStoryboard storyboardWithName:kMainStoryboard bundle:nil];
//        PIDViewController* pidController = [storyboard instantiateViewControllerWithIdentifier:kScreenPID];
        NSNumberFormatter* formatter = [NSNumberFormatter new];
        formatter.numberStyle = NSNumberFormatterDecimalStyle;
        
        if(url.query.length > 0)
        {
            NSNumber* trackerId;
            BOOL      lowFloor;
            NSString* routes;
            
            NSArray* paramComponents = [url.query componentsSeparatedByString:@"&"];
            for(NSString* param in paramComponents)
            {
                NSArray* subcomponents = [param componentsSeparatedByString:@"="];
                NSString* name = subcomponents[0];
                NSString* value = subcomponents[1];
                
                if([name isEqualToString:@"trackerId"])
                { trackerId = [formatter numberFromString:value]; }
                else if([name isEqualToString:@"lowFloor"])
                {
                    if([value isEqualToString:@"true"])
                    { lowFloor = YES; }
                    else
                    { lowFloor = NO; }
                }
                else if([name isEqualToString:@"routes"])
                { routes = value; }
            }
        
            Stop* stop = [Stop stopForTrackerID:trackerId];
            if(stop)
            {
                self.startingStop = stop;
                self.startingFilter = [Filter filterForRoute:[Route routeWithNumber:routes] lowFloor:lowFloor];
                self.autoJumpToPID = YES;
                
                [self autoOpenPID];
            }
        }
        return YES;
    }
    
    return NO;
}

- (void)autoOpenPID
{
    if(self.autoJumpToPID)
    {
        SWRevealViewController* controller = (SWRevealViewController*)self.window.rootViewController;
        UITabBarController* tabController  = (UITabBarController*)controller.frontViewController;
        tabController.selectedIndex = 1;
        UINavigationController* nav = (UINavigationController*)tabController.selectedViewController;
        [nav popToRootViewControllerAnimated:NO];
        
        UIStoryboard* storyboard         = [UIStoryboard storyboardWithName:kMainStoryboard bundle:nil];
        PIDViewController* pidController = [storyboard instantiateViewControllerWithIdentifier:kScreenPID];
        NSNumberFormatter* formatter = [NSNumberFormatter new];
        formatter.numberStyle = NSNumberFormatterDecimalStyle;
        
        pidController.currentStop = self.startingStop;
        [pidController updateFilter:self.startingFilter];
        [nav pushViewController:pidController animated:NO];
    }
    self.autoJumpToPID = NO;
}

- (UITabBarController*)tabBarController {
    SWRevealViewController* controller = (SWRevealViewController*)self.window.rootViewController;
    if([controller.frontViewController isKindOfClass:[UITabBarController class]])
    { return (UITabBarController*)controller.frontViewController; }
    return nil;
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{ NSLog(@"Notification received: %@", userInfo); }

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    NSLog(@"App received device token %@", token);
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    PidsService* service = [PidsService new];
    NSUUID* uuid = [UIDevice currentDevice].identifierForVendor;
    [service submitNotificationRegistrationRequest:token UUID:[uuid UUIDString] completion:^(BOOL successful, NSError *error) {
        if(successful)
        { [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationDeviceTokenAvailable object:nil]; }
    }];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{ NSLog(@"Unable to register for notifications: %@", error.localizedDescription); }

// Register the application defaults
- (void)registerDefaults
{
    if (![[NSUserDefaults standardUserDefaults] objectForKey:kLastSynchronisationDate])
    { [[NSUserDefaults standardUserDefaults] setObject:[NSDate dateWithTimeIntervalSince1970:TTDatabaseModificationDate] forKey:kLastSynchronisationDate]; }
}

#pragma mark -
#pragma mark Core Data stack

/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
	
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        if ([NSManagedObjectContext instancesRespondToSelector:@selector(initWithConcurrencyType:)])
            _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        else
            _managedObjectContext = [NSManagedObjectContext new];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}


/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created by merging all of the models found in the application bundle.
 */
- (NSManagedObjectModel *)managedObjectModel {
	
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"tramTRACKER2" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
    return _managedObjectModel;
}

NSString * const kDefaultsDatabaseVersion = @"databaseVersion";

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
	
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
	/* We need to copy this into the data directory, only if it hasn't already been done */
	NSFileManager   * fileManager = [NSFileManager defaultManager];
	NSString        * docDir = [tramTRACKERAppDelegate applicationDocumentsDirectory];
	NSString        * destinationFileName = [docDir stringByAppendingPathComponent:@"tramTRACKER.db"];
	NSString        * destinationWalFileName = [destinationFileName stringByAppendingString:@"-wal"];
	NSString        * sourceFileName = [[NSBundle mainBundle] pathForResource:@"tramTRACKER" ofType:@"db"];
	NSString        * sourceWalFileName = [sourceFileName stringByAppendingString:@"-wal"];
    NSDictionary    * bundle = [[NSBundle mainBundle] infoDictionary];
    NSUserDefaults  * defaults = [NSUserDefaults standardUserDefaults];

	if ((![fileManager fileExistsAtPath:destinationFileName] ||
         ![fileManager fileExistsAtPath:destinationWalFileName] ||
         [defaults objectForKey:kDefaultsDatabaseVersion] == nil ||
         ![[defaults objectForKey:kDefaultsDatabaseVersion] isEqualToString:[bundle objectForKey:@"CFBundleVersion"]])
        && sourceFileName)
	{
        /* Removing previous files */
        [fileManager removeItemAtPath:destinationFileName error:NULL];
        [fileManager removeItemAtPath:destinationWalFileName error:NULL];
        
        /* Copying database files */
        [fileManager copyItemAtPath:sourceFileName    toPath:destinationFileName    error:nil];
        [fileManager copyItemAtPath:sourceWalFileName toPath:destinationWalFileName error:nil];

        /* Setting the lastSyncDate to original date */
        [defaults setObject:[NSDate dateWithTimeIntervalSince1970:TTDatabaseModificationDate] forKey:kLastSynchronisationDate];

        /* Setting the database version */
        [defaults setObject:[bundle objectForKey:@"CFBundleVersion"] forKey:kDefaultsDatabaseVersion];
    }

	NSURL   * storeUrl = [NSURL fileURLWithPath:destinationFileName];
	NSError * error = nil;

    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];

    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                   configuration:nil
                                                             URL:storeUrl
                                                         options:nil
                                                           error:&error] || error)
    {

    }
    return _persistentStoreCoordinator;
}

#pragma mark -
#pragma mark File Locations

/**
 Returns the path to the application's documents directory.
 */
+ (NSString *)applicationDocumentsDirectory {
	
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

#pragma mark -
#pragma mark Methods for handling changing application state

- (void)applicationWillResignActive:(UIApplication *)application
{ }

- (void)applicationDidBecomeActive:(UIApplication *)Mainapplication
{ }


- (void)applicationWillTerminate:(UIApplication *)application
{ }

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [[Analytics sharedInstance] appHasClosed];
    NSLog(@"App minimised");
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [[Analytics sharedInstance] appHasLaunched];
    NSLog(@"App restored");
}

- (void)saveContext
{
    tramTRACKERAppDelegate  * d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext  * context = [d managedObjectContext];

    NSError *error = nil;

    if (![context save:&error] || error)
    {
        NSLog(@"Error while saving, rollingback");
        [context rollback];
    }
}

@end
