//
//  PidsServiceDelegate.m
//  tramTRACKER
//
//  Created by Robert Amos on 18/02/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <objc/message.h>

#import "PidsServiceDelegate.h"
#import "Turn.h"
#import "ISO8601DateFormatter.h"
#import "NSDictionary+SafeAccess.h"
#import "PointOfInterest.h"
#import "TicketRetailer.h"
#import "SpecialMessage.h"
#import "tramTRACKERAppDelegate.h"

@interface PidsServiceDelegate()

@property (nonatomic, assign, getter = isCancelled) BOOL cancelled;

@end

@implementation PidsServiceDelegate

//
// Initialize with the delegate that will receive the results
//
- (PidsServiceDelegate *)initWithDelegate:(id)newDelegate
{
	// Make sure usual init stuff still happens
	if (self = [self init])
	{
		[self setDelegate:newDelegate];
	}
	return self;
}

- (void)timeout:(NSTimer *)timer {
    [self failWithError:[NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier] code:PIDServiceErrorTimeoutReached userInfo:nil]];
    [self cancel];
}

- (void)manageTimer {
    [self.timeoutTimer invalidate];
    self.timeoutTimer = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(timeout:) userInfo:nil repeats:NO];
}

#pragma mark -
#pragma mark NSURLConnection Delegate methods

// Connection failed with error
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	// For now, just log the error and silently fail
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	[self failWithError:error];
    [self cancel];
}

// We received a response from the server
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	// Start a new empty responseText that we can append to
	[self setResponseText:[NSMutableString string]];
	
	// if the delegate implements it, let it know we received a response
	if ([self.delegate respondsToSelector:@selector(connection:didReceiveResponse:)])
		[self.delegate performSelector:@selector(connection:didReceiveResponse:) withObject:connection withObject:response];
}


// We received a chunk of data from the server
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	// append the data to our responseText string
	NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    if(![str isKindOfClass:[NSString class]])
    { return; } // if the returned data couldn't be processed into a string, simply ignore it (Fabric issue #672)
    
	[[self responseText] appendString:str];
	str = nil;
	
	// if the delegate implements it, let it know we received data
	if ([self.delegate respondsToSelector:@selector(connection:didReceiveData:)])
		[self.delegate performSelector:@selector(connection:didReceiveData:) withObject:connection withObject:data];
}

// The connection has finished loading, start parsing
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
	// let the delegate know
	if ([self.delegate respondsToSelector:@selector(connectionDidFinishLoading:WithResponseText:)])
		[self.delegate performSelector:@selector(connectionDidFinishLoading:WithResponseText:) withObject:connection withObject:self.responseText];
    
//	NSLog(@"[PSD] Received response: %@", self.responseText);
	
    //Only REST uses http get
    [self parseJSONResponse:self.responseText];
	
	// clear the response
	[self setResponseText:nil];
}

#pragma mark -
#pragma mark NSXMLParser Delegate methods

- (void)parseJSONResponse:(NSString *)text
{
    [self.timeoutTimer invalidate];
    
    if (self.isCancelled)
        return;
    
	NSError *error = nil;
	NSDictionary *response = [NSJSONSerialization JSONObjectWithData:[text dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO] options:0 error:&error];
	NSString *errorMessage = [response valueForKey:@"errorMessage" ifKindOf:[NSString class] defaultValue:nil];
	
	if (!errorMessage)
	{
		NSString *calledMethod = [response valueForKey:@"webMethodCalled" ifKindOf:[NSString class] defaultValue:nil];
        
		if ([calledMethod isEqualToString:@"GetStopsAndRoutesUpdatesSince"])
		{
			NSDictionary *responseObject = [response valueForKey:@"responseObject" ifKindOf:[NSDictionary class] defaultValue:nil];
			NSMutableArray *pendingUpdateList = [[NSMutableArray alloc] init];
			
			NSArray *routeChanges = [responseObject valueForKey:@"RouteChanges" ifKindOf:[NSArray class] defaultValue:nil];
			
			if (routeChanges != nil)
			{
				for (NSDictionary *routeChange in routeChanges)
				{
					RouteUpdate *routeUpdate = [[RouteUpdate alloc] init];
					
					[routeUpdate setRouteNumber:[routeChange valueForKey:@"RouteNo" ifKindOf:[NSString class] defaultValue:nil]];
					[routeUpdate setInternalRouteNumber:[[routeChange valueForKey:@"ID" ifKindOf:[NSNumber class] defaultValue:nil] stringValue]];
					[routeUpdate setHeadboardRouteNumber:[routeChange valueForKey:@"HeadboardRouteNo" ifKindOf:[NSString class] defaultValue:nil]];
					[routeUpdate setSubRoute:![[routeChange valueForKey:@"IsMainRoute" ifKindOf:[NSNumber class] defaultValue:@NO] boolValue]];
					[routeUpdate setColour:[routeChange valueForKey:@"Colour" ifKindOf:[NSString class] defaultValue:nil]];
					
					NSString *action = [routeChange valueForKey:@"Action" ifKindOf:[NSString class] defaultValue:nil];
					PIDServiceActionType type = 0;
					if ([action isEqualToString:@"UPDATE"])
						type = PIDServiceActionTypeUpdate;
					else if ([action isEqualToString:@"DELETE"])
						type = PIDServiceActionTypeDelete;
					[routeUpdate setActionType:type];
					
					[pendingUpdateList addObject:routeUpdate];
				}
			}
			
			NSArray *stopChanges = [responseObject valueForKey:@"StopChanges" ifKindOf:[NSArray class] defaultValue:nil];
			
			if (stopChanges != nil)
			{
				for (NSDictionary *stopChange in stopChanges)
				{
					StopUpdate *stopUpdate = [[StopUpdate alloc] init];
					
					[stopUpdate setTrackerID:[stopChange valueForKey:@"StopNo" ifKindOf:[NSNumber class] defaultValue:nil]];
					
					NSString *action = [stopChange valueForKey:@"Action" ifKindOf:[NSString class] defaultValue:nil];
					PIDServiceActionType type = 0;
					if ([action isEqualToString:@"UPDATE"])
						type = PIDServiceActionTypeUpdate;
					else if ([action isEqualToString:@"DELETE"])
						type = PIDServiceActionTypeDelete;
					[stopUpdate setActionType:type];
					
					////////////////////////////////////////////
					////// COMMENT THIS (THE BELOW LINE) OUT WHEN SCRAPING THE API
					[pendingUpdateList addObject:stopUpdate];
				}
			}
            
			NSDate *parsedServerTime;
            if (responseObject)
            {
                //lol
                NSString *serverTimeText = [[[responseObject valueForKey:@"ServerTime" ifKindOf:[NSArray class] defaultValue:nil] firstObject] valueForKey:@"ServerTime" ifKindOf:[NSString class] defaultValue:nil];
                
                parsedServerTime = [PidsServiceDelegate parseDateFromDotNetJSONString:serverTimeText];
            }
			
            
			
			if ([self.delegate respondsToSelector:@selector(setUpdates:atDate:)])
			{
				// pass it to the delegate
				[self.delegate performSelector:@selector(setUpdates:atDate:) withObject:[NSArray arrayWithArray:pendingUpdateList] withObject:parsedServerTime];
			} else
			{
				// Nope, log it
				NSLog(@"Found Update List %@ at %@, but delegate %@ does not implement selector setUpdates:withDate:", pendingUpdateList, parsedServerTime, self.delegate);
				[self failWithError:error];
			}
		}
        else if ([calledMethod isEqualToString:@"GetPointsOfInterestChangesSince"])
        {
            NSArray             * responseObject = [response valueForKey:@"responseObject" ifKindOf:[NSArray class] defaultValue:nil];
            NSMutableArray      * pois = [NSMutableArray new];
            
            for (NSDictionary * poiDic in responseObject)
            {
                NSNumber                * poiID = [poiDic valueForKey:@"POIId"
                                                             ifKindOf:[NSNumber class]
                                                         defaultValue:nil];
                
                NSString                * poiAction = [poiDic valueForKey:@"Action"
                                                                 ifKindOf:[NSString class]
                                                             defaultValue:nil];
                
                PointOfInterestUpdate   * poi = [PointOfInterestUpdate pointOfInterestUpdateWithID:poiID
                                                                               andActionTypeString:poiAction] ;
                [pois addObject:poi];
                
            }
            
			if ([self.delegate respondsToSelector:@selector(setPOIUpdates:)])
				[self.delegate performSelector:@selector(setPOIUpdates:) withObject:pois];
            else
				[self failWithError:error];
        }
        else if ([calledMethod isEqualToString:@"GetNextPredictedArrivalTimeAtStopsForTramNo"])
        {
            NSDictionary        * responseObject = [response valueForKey:@"responseObject" ifKindOf:[NSDictionary class] defaultValue:nil];
            NSArray             * predictions = [responseObject valueForKey:@"NextPredictedStopsDetails" ifKindOf:[NSArray class] defaultValue:nil];
            
            JourneyStub * journeyStub = nil;
            
            if (responseObject && predictions)
            {
                journeyStub = [JourneyStub new];
                
                journeyStub.tramStub = [TramStub new];
                journeyStub.tramStub.headboardRouteNumber = [responseObject valueForKey:@"HeadBoardRouteNo" ifKindOf:[NSString class] defaultValue:@""];
                journeyStub.tramStub.specialEvent = [[responseObject valueForKey:@"HasSpecialEvent" ifKindOf:[NSNumber class] defaultValue:@NO] boolValue];
                journeyStub.tramStub.disrupted = [[responseObject valueForKey:@"HasDisruption" ifKindOf:[NSNumber class] defaultValue:@NO] boolValue];
                journeyStub.tramStub.available = [[responseObject valueForKey:@"Available" ifKindOf:[NSNumber class] defaultValue:@NO] boolValue];
                journeyStub.tramStub.atLayover = [[responseObject valueForKey:@"AtLayover" ifKindOf:[NSNumber class] defaultValue:@NO] boolValue];
                journeyStub.tramStub.routeNumber = [responseObject valueForKey:@"RouteNo" ifKindOf:[NSString class] defaultValue:@""];
                journeyStub.tramStub.upDirection = [[responseObject valueForKey:@"Up" ifKindOf:[NSNumber class] defaultValue:@NO] boolValue];
                journeyStub.tramStub.number = [responseObject valueForKey:@"VehicleNo" ifKindOf:[NSNumber class] defaultValue:@0];
                
                NSDictionary    * disruptionMessage = [responseObject valueForKey:@"DisruptionMessage" ifKindOf:[NSDictionary class] defaultValue:[NSDictionary dictionary]];
                
                journeyStub.tramStub.disruptionMessage = [disruptionMessage valueForKey:@"Message" ifKindOf:[NSString class] defaultValue:@""];
                journeyStub.tramStub.specialMessage = [responseObject valueForKey:@"SpecialEventMessage" ifKindOf:[NSString class] defaultValue:@""];
                journeyStub.tramStub.routeStub = [RouteStub new];
                journeyStub.tramStub.routeStub.number = journeyStub.tramStub.routeNumber;
                journeyStub.tramStub.routeStub.headboardNumber = journeyStub.tramStub.headboardRouteNumber;
                
                NSMutableArray * stubStops = [NSMutableArray new];
                
                for (NSDictionary *curStop in predictions)
                {
                    JourneyStopStub  * s = [JourneyStopStub new];
                    
                    s.stopStub = [StopStub new];
                    s.stopStub.trackerID = [curStop valueForKey:@"StopNo" ifKindOf:[NSNumber class] defaultValue:@0];
                    s.originalPredictedArrivalDateTime = [PidsServiceDelegate parseDateFromDotNetJSONString:[curStop valueForKey:@"PredictedArrivalDateTime" ifKindOf:[NSString class] defaultValue:@""]];
                    s.predicatedArrivalDateTime = s.originalPredictedArrivalDateTime;
                    
                    [stubStops addObject:s];
                }
                journeyStub.stubStops = stubStops;
            }
            
            if ([self.delegate respondsToSelector:@selector(setJourney:)])
                [self.delegate performSelectorOnMainThread:@selector(setJourney:) withObject:journeyStub waitUntilDone:NO];
            else
                [self failWithError:error];
        }
        else if ([calledMethod isEqualToString:@"GetTicketOutletChangesSince"])
        {
            NSArray             * responseObject = [response valueForKey:@"responseObject" ifKindOf:[NSArray class] defaultValue:nil];
            NSMutableArray      * retailers = [NSMutableArray new];
            
            for (NSDictionary * outletDIC in responseObject)
            {
                NSNumber                * aID = [outletDIC valueForKey:@"ID"
                                                              ifKindOf:[NSNumber class]
                                                          defaultValue:nil];
                
                NSString                * aAction = [outletDIC valueForKey:@"Action"
                                                                  ifKindOf:[NSString class]
                                                              defaultValue:nil];
                
                TicketRetailerUpdate   * ticket = [TicketRetailerUpdate ticketRetailerUpdateWithID:aID
                                                                               andActionTypeString:aAction];
                [retailers addObject:ticket];
            }
            
			if ([self.delegate respondsToSelector:@selector(setTicketOutletsUpdates:)])
				[self.delegate performSelector:@selector(setTicketOutletsUpdates:) withObject:retailers];
            else
				[self failWithError:error];
        }
        else if ([calledMethod isEqualToString:@"GetLatestNetworkMaps"])
        {
            NSArray             * responseObject = [response valueForKey:@"responseObject" ifKindOf:[NSArray class] defaultValue:nil];

			if ([self.delegate respondsToSelector:@selector(setNetworkMapUpdates:)])
				[self.delegate performSelector:@selector(setNetworkMapUpdates:) withObject:responseObject];
            else
				[self failWithError:error];
        }
		else if ([calledMethod isEqualToString:@"GetListOfStopsByRouteNoAndDirection"])
		{
			NSArray *responseObject = [response valueForKey:@"responseObject" ifKindOf:[NSArray class] defaultValue:nil];
            
			NSMutableArray *stops = [[NSMutableArray alloc] init];
			NSMutableArray *turns = [[NSMutableArray alloc] init];
			
			if (responseObject != nil && [responseObject count] > 0)
			{
				for (NSDictionary *stop in responseObject)
				{
					NSNumber *stopTrackerID = [stop valueForKey:@"StopNo" ifKindOf:[NSNumber class] defaultValue:nil];
					if (stopTrackerID != nil)
					{
						[stops addObject:stopTrackerID];
						
						NSString *turnTypeString = [stop valueForKey:@"TurnType" ifKindOf:[NSString class] defaultValue:nil];
						
						if (turnTypeString != nil)
						{
							NSMutableDictionary *turn = [[NSMutableDictionary alloc] init];
							[turn setObject:stopTrackerID forKey:@"trackerid"];
							
							NSString *message = [stop valueForKey:@"TurnMessage" ifKindOf:[NSString class] defaultValue:@""];
							[turn setObject:message forKey:@"message"];
							
							NSNumber *turnType = @0;
							if ([turnTypeString isEqualToString:@"left"])
								turnType = [NSNumber numberWithInteger:TTTurnLeft];
							else if ([turnTypeString isEqualToString:@"right"])
								turnType = [NSNumber numberWithInteger:TTTurnRight];
							else if ([turnTypeString isEqualToString:@"straight"])
								turnType = [NSNumber numberWithInteger:TTTurnStraight];
							else if ([turnTypeString isEqualToString:@"s_to_right"])
								turnType = [NSNumber numberWithInteger:TTTurnSRight];
							else if ([turnTypeString isEqualToString:@"veer_left"])
								turnType = [NSNumber numberWithInteger:TTTurnVeerLeft];
							else if ([turnTypeString isEqualToString:@"veer_right"])
								turnType = [NSNumber numberWithInteger:TTTurnVeerRight];
                            
							[turn setObject:turnType forKey:@"type"];
							
							[turns addObject:[NSDictionary dictionaryWithDictionary:turn]];
						}
					}
				}
                
                if ([self.delegate respondsToSelector:@selector(setSuburbNames:)])
                    [self.delegate performSelector:@selector(setSuburbNames:) withObject:responseObject];
                /*
                // check that our delegate is setup to receive the predictions
                if ([self.delegate respondsToSelector:@selector(setStopList:forRouteNumber:upDirection:)])
                { [self.delegate setStopList:[NSArray arrayWithArray:stops] forRouteNumber:r upDirection:up]; }
                
                if ([self.delegate respondsToSelector:@selector(setTurnList:forRouteNumber:upDirection:)])
                { [self.delegate setTurnList:[NSArray arrayWithArray:turns] forRouteNumber:r upDirection:up]; }
                */
			}
            else
                [self failWithError:[NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                        code:PIDServiceValidationErrorRouteNotFound
                                                    userInfo:nil]];
		}
        else if ([calledMethod isEqualToString:@"GetTicketOutletById"] ||
                 [calledMethod isEqualToString:@"GetPointOfInterestById"])
        {
            NSDictionary *responseObject = [[response valueForKey:@"responseObject" ifKindOf:[NSArray class] defaultValue:nil] firstObject];
            
            if ([[self delegate] respondsToSelector:@selector(setInformation:)])
                [self.delegate performSelector:@selector(setInformation:) withObject:responseObject];
        }
		else if ([calledMethod isEqualToString:@"GetStopInformation"])
		{
			NSMutableDictionary *stop = [NSMutableDictionary new];
            
			[stop setObject:self.trackerID forKey:@"trackerID"];
           
			NSDictionary *responseObject = [[response valueForKey:@"responseObject" ifKindOf:[NSArray class] defaultValue:nil] firstObject];
            
			if (responseObject != nil)
			{
				[stop setObject:[responseObject valueForKey:@"IsInFreeZone" ifKindOf:[NSNumber class] defaultValue:@0] forKey:@"ftzStop"];

				[stop setObject:[responseObject valueForKey:@"CityDirection" ifKindOf:[NSString class] defaultValue:@""] forKey:@"cityDirection"];
				[stop setObject:[responseObject valueForKey:@"ConnectingBus" ifKindOf:[NSString class] defaultValue:@""] forKey:@"connectingBusesList"];
				[stop setObject:[responseObject valueForKey:@"ConnectingTrains" ifKindOf:[NSString class] defaultValue:@""] forKey:@"connectingTrainsList"];
				[stop setObject:[responseObject valueForKey:@"ConnectingTrams" ifKindOf:[NSString class] defaultValue:@""] forKey:@"connectingTramsList"];
				[stop setObject:[responseObject valueForKey:@"FlagStopNo" ifKindOf:[NSString class] defaultValue:@""] forKey:@"number"];
				[stop setObject:[responseObject valueForKey:@"HasConnectingBuses" ifKindOf:[NSNumber class] defaultValue:@0] forKey:@"hasConnectingBuses"];
				[stop setObject:[responseObject valueForKey:@"HasConnectingTrains" ifKindOf:[NSNumber class] defaultValue:@0] forKey:@"hasConnectingTrains"];
				[stop setObject:[responseObject valueForKey:@"HasConnectingTrams" ifKindOf:[NSNumber class] defaultValue:@0] forKey:@"hasConnectingTrams"];
				[stop setObject:[responseObject valueForKey:@"IsCityStop" ifKindOf:[NSNumber class] defaultValue:@0] forKey:@"cityStop"];
				[stop setObject:[responseObject valueForKey:@"IsEasyAccess" ifKindOf:[NSNumber class] defaultValue:@0] forKey:@"easyAccess"];
				[stop setObject:[responseObject valueForKey:@"IsPlatformStop" ifKindOf:[NSNumber class] defaultValue:@0] forKey:@"platformStop"];
				[stop setObject:[responseObject valueForKey:@"IsShelter" ifKindOf:[NSNumber class] defaultValue:@0] forKey:@"shelter"];
				[stop setObject:[responseObject valueForKey:@"IsYTShelter" ifKindOf:[NSNumber class] defaultValue:@0] forKey:@"ytShelter"];
				[stop setObject:[responseObject valueForKey:@"Latitude" ifKindOf:[NSNumber class] defaultValue:@0] forKey:@"latitude"];
				[stop setObject:[responseObject valueForKey:@"Longitude" ifKindOf:[NSNumber class] defaultValue:@0] forKey:@"longitude"];
				[stop setObject:[responseObject valueForKey:@"StopLength" ifKindOf:[NSNumber class] defaultValue:@0] forKey:@"length"];
				[stop setObject:[responseObject valueForKey:@"StopName" ifKindOf:[NSString class] defaultValue:@""] forKey:@"name"];
				[stop setObject:[responseObject valueForKey:@"Zones" ifKindOf:[NSString class] defaultValue:@""] forKey:@"zones"];
			}
            
			// Check that our delegate is setup to receive the information
			if ([[self delegate] respondsToSelector:@selector(setInformation:)])
			{
				// Pass it to the delegate
				[[self delegate] performSelector:@selector(setInformation:) withObject:stop];
			}
            else
                [self failWithError:nil];
		}
		else if ([calledMethod isEqualToString:@"GetMainRoutesForStop"])
		{
			NSMutableArray *routes = [[NSMutableArray alloc] init];
			
			NSArray *responseObject = [response valueForKey:@"responseObject" ifKindOf:[NSArray class] defaultValue:nil];
			
			if (responseObject != nil)
			{
				for (NSDictionary *routeDict in responseObject)
				{
					NSString *routeNo = [routeDict valueForKey:@"RouteNo" ifKindOf:[NSString class] defaultValue:nil];
					[routes addObject:routeNo];
				}
			}
			
			if ([self.delegate respondsToSelector:@selector(setRoutes:throughStop:)])
			{
				[self.delegate performSelector:@selector(setRoutes:throughStop:)
                                    withObject:[NSArray arrayWithArray:routes]
                                    withObject:self.trackerID];
			}
		}
        else if ([calledMethod isEqualToString:@"GetDeviceToken"])
        {
            if ([self.delegate respondsToSelector:@selector(setNewClientGuid:)])
                [self.delegate performSelector:@selector(setNewClientGuid:)
                                    withObject:[[response valueForKey:@"responseObject" ifKindOf:[NSString class] defaultValue:@""]
                                                valueForKey:@"DeviceToken" ifKindOf:[NSString class] defaultValue:@""]];
        }
		else if ([calledMethod isEqualToString:@"GetDestinationsForRoute"])
		{
			NSMutableDictionary *destinations = [[NSMutableDictionary alloc] init];
			
			NSArray *responseObject = [response valueForKey:@"responseObject" ifKindOf:[NSArray class] defaultValue:nil];
            
			if (responseObject != nil)
			{
				NSDictionary *destinationsResponseDict = [responseObject firstObject];
				NSString *upDestination = [destinationsResponseDict valueForKey:@"UpDestination"
                                                                       ifKindOf:[NSString class]
                                                                   defaultValue:nil];
                
				NSString *downDestination = [destinationsResponseDict valueForKey:@"DownDestination"
                                                                         ifKindOf:[NSString class]
                                                                     defaultValue:nil];
                
				if (upDestination != nil)
					[destinations setObject:upDestination forKey:@"up"];
                
				if (downDestination != nil)
					[destinations setObject:downDestination forKey:@"down"];
                if ([self.delegate respondsToSelector:@selector(setDestinations:forRouteNumber:)])
                    [self.delegate performSelector:@selector(setDestinations:forRouteNumber:)
                                        withObject:destinations
                                        withObject:self.routeNumber];
                else
                    [self failWithError:error];
			}
            else
                [self failWithError:error];
		}
        else if ([calledMethod isEqualToString:@"GetSchedulesCollection"])
        {
			NSArray                 * responseObject = [response valueForKey:@"responseObject" ifKindOf:[NSArray class] defaultValue:nil];
            __block NSMutableArray  * predictions = [NSMutableArray new];
            
			if (responseObject != nil)
			{
                [responseObject enumerateObjectsUsingBlock:^(NSDictionary *curObject, NSUInteger idx, BOOL *stop)
                 {
                     PredictionStub  * predictionStub = [PredictionStub new];
                     
                     predictionStub.stopStub = [StopStub new];
                     predictionStub.tramStub = [TramStub new];
                     predictionStub.tramStub.routeStub = [RouteStub new];
                     
                     predictionStub.stopStub.trackerID = self.trackerID;
                     predictionStub.tramStub.routeStub.number = predictionStub.routeNo = predictionStub.tramStub.routeNumber = [curObject valueForKey:@"RouteNo" ifKindOf:[NSString class] defaultValue:@""];
                     predictionStub.destination = [curObject valueForKey:@"Destination" ifKindOf:[NSString class] defaultValue:@""];
                     predictionStub.airConditioned = [[curObject valueForKey:@"AirConditioned" ifKindOf:[NSNumber class] defaultValue:@NO] boolValue];
                     predictionStub.displayAirConditioning = [[curObject valueForKey:@"DisplayAC" ifKindOf:[NSNumber class] defaultValue:@NO] boolValue];
                     predictionStub.hasSpecialEvent = predictionStub.tramStub.specialEvent = [[curObject valueForKey:@"HasSpecialEvent" ifKindOf:[NSNumber class] defaultValue:@NO] boolValue];
                     predictionStub.disrupted = predictionStub.tramStub.disrupted = [[curObject valueForKey:@"HasDisruption" ifKindOf:[NSNumber class] defaultValue:@NO] boolValue];
                     predictionStub.headboardRouteNumber = predictionStub.tramStub.routeStub.headboardNumber = predictionStub.tramStub.headboardRouteNumber = [curObject valueForKey:@"HeadBoardRouteNo" ifKindOf:[NSString class] defaultValue:@""];
                     
                     predictionStub.lowFloor = [[curObject valueForKey:@"IsLowFloorTram" ifKindOf:[NSNumber class] defaultValue:@NO] boolValue];
                     predictionStub.tramTrackerAvailable = [[curObject valueForKey:@"IsTTAvailable" ifKindOf:[NSNumber class] defaultValue:@NO] boolValue];
                     predictionStub.tripID = [curObject valueForKey:@"TripID" ifKindOf:[NSNumber class] defaultValue:@0];
                     predictionStub.tramStub.number = [curObject valueForKey:@"VehicleNo" ifKindOf:[NSNumber class] defaultValue:@0];
                     predictionStub.requestDateTime = [PidsServiceDelegate parseDateFromDotNetJSONString:[response valueForKey:@"timeRequested" ifKindOf:[NSString class] defaultValue:@""]];
                     
                     predictionStub.predictedArrivalDateTime = [PidsServiceDelegate parseDateFromDotNetJSONString:[curObject valueForKey:@"PredictedArrivalDateTime" ifKindOf:[NSString class] defaultValue:@""]];
                     predictionStub.predictedArrivalDateTime = [self calculateLocalPredictedArrivalTimeFromServerTime:predictionStub.predictedArrivalDateTime
                                                                                                withServerRequestTime:predictionStub.requestDateTime
                                                                                                 withLocalRequestTime:[self requestTime]];
                     
                     if (![predictionStub.routeNo isEqualToString:@"0"])
                         [predictions addObject:predictionStub];
                 }];
                
                // check that our delegate is setup to receive the predictions
                if ([[self delegate] respondsToSelector:@selector(setSchedules:)])
                {
                    // pass it to the delegate
                    [[self delegate] performSelectorOnMainThread:@selector(setSchedules:)
                                                      withObject:predictions
                                                   waitUntilDone:NO];
                }
            }
        }
        else if ([calledMethod isEqualToString:@"GetSchedulesForTrip"])
        {
            NSArray* responseObject = [response valueForKey:@"responseObject" ifKindOf:[NSArray class] defaultValue:nil];
            NSMutableArray* stops = [NSMutableArray new];
            
            if(responseObject != nil)
            {
                NSDate* responseTime   = [PidsServiceDelegate parseDateFromDotNetJSONString:response[@"timeRequested"]];
                for(NSDictionary* stopData in responseObject)
                {
                    PredictionStub* prediction = [PredictionStub new];
                    
                    NSDate* predictionTime = [PidsServiceDelegate parseDateFromDotNetJSONString:stopData[@"ScheduledArrivalDateTime"]];
                    prediction.predictedArrivalDateTime = [self calculateLocalPredictedArrivalTimeFromServerTime:predictionTime withServerRequestTime:responseTime withLocalRequestTime:[self requestTime]];
                    
                    prediction.stop = [Stop stopForTrackerID:stopData[@"StopNo"]];
                    
                    [stops addObject:prediction];
                }
            }
            
            if([self.delegate respondsToSelector:@selector(setStopList:)])
            { [self.delegate performSelectorOnMainThread:@selector(setStopList:) withObject:stops waitUntilDone:NO]; }
        }
        else if ([calledMethod isEqualToString:@"GetStopsByPointsOfInterestId"])
        {
			NSArray                 * responseObject = [response valueForKey:@"responseObject" ifKindOf:[NSArray class] defaultValue:nil];
            __block NSMutableArray  * stops = [NSMutableArray new];
            
			if (responseObject != nil)
			{
                [responseObject enumerateObjectsUsingBlock:^(NSDictionary *curObject, NSUInteger idx, BOOL *stop)
                 {
                     NSNumber *number = curObject[@"StopNo"];
                     
                     if (number)
                         [stops addObject:number];
                 }];
            }
            // check that our delegate is setup to receive the predictions
			if ([[self delegate] respondsToSelector:@selector(setStopsForPOI:)])
				[[self delegate] performSelectorOnMainThread:@selector(setStopsForPOI:)
                                                  withObject:stops
                                               waitUntilDone:NO];
        }
        else if ([calledMethod isEqualToString:@"GetPointsOfInterestByStopNo"])
        {
            
			NSArray                 * responseObject = [response valueForKey:@"responseObject" ifKindOf:[NSArray class] defaultValue:nil];
            __block NSMutableArray  * pois = [NSMutableArray new];
            
			if (responseObject != nil)
			{
                [responseObject enumerateObjectsUsingBlock:^(NSDictionary *curObject, NSUInteger idx, BOOL *stop)
                 {
                     NSNumber *number = curObject[@"POIId"];
                     
                     if (number)
                         [pois addObject:number];
                 }];
            }
            // check that our delegate is setup to receive the predictions
			if ([[self delegate] respondsToSelector:@selector(setPOIs:)])
				[[self delegate] performSelectorOnMainThread:@selector(setPOIs:)
                                                  withObject:pois
                                               waitUntilDone:NO];
        }
		else if ([calledMethod isEqualToString:@"GetNextPredictedRoutesCollection"])
		{
			NSArray                 * responseObject = [response valueForKey:@"responseObject" ifKindOf:[NSArray class] defaultValue:nil];
            
            NSMutableArray          * finalPredictions = nil;
            NSMutableArray          * messageList = nil;
            BOOL                    ttAvailable = YES;
            
            if (responseObject != nil)
            {
                __block NSMutableArray  * predictions = [NSMutableArray new];
                
                messageList = [NSMutableArray new];
                
                __block NSMutableArray      * trams = nil;
                __block NSMutableDictionary * disruptions = [NSMutableDictionary new];
                __block NSMutableSet        * disruptionsRouteNo = [NSMutableSet new];
                __block NSMutableArray      * specialEvents = [NSMutableArray new];
                __block PredictionStub      * lastPrediction = nil;
                
                
                [responseObject enumerateObjectsUsingBlock:^(NSDictionary *curObject, NSUInteger idx, BOOL *stop)
                 {
                     if (![[curObject valueForKey:@"RouteNo" ifKindOf:[NSString class] defaultValue:@""] isEqualToString:@"35"])
                     {
                         
                         PredictionStub  * predictionStub = [PredictionStub new];
                         
                         predictionStub.stopStub = [StopStub new];
                         predictionStub.tramStub = [TramStub new];
                         predictionStub.tramStub.routeStub = [RouteStub new];
                         
                         predictionStub.stopStub.trackerID = self.trackerID;
                         predictionStub.tramStub.routeStub.number = predictionStub.routeNo = predictionStub.tramStub.routeNumber = [curObject valueForKey:@"RouteNo" ifKindOf:[NSString class] defaultValue:@""];
                         predictionStub.destination = [curObject valueForKey:@"Destination" ifKindOf:[NSString class] defaultValue:@""];
                         predictionStub.airConditioned = [[curObject valueForKey:@"AirConditioned" ifKindOf:[NSNumber class] defaultValue:@NO] boolValue];
                         predictionStub.displayAirConditioning = [[curObject valueForKey:@"DisplayAC" ifKindOf:[NSNumber class] defaultValue:@NO] boolValue];
                         
                         predictionStub.hasSpecialEvent = predictionStub.tramStub.specialEvent = [[curObject valueForKey:@"HasSpecialEvent" ifKindOf:[NSNumber class] defaultValue:@NO] boolValue];
                         
                         
                         predictionStub.disrupted = predictionStub.tramStub.disrupted = [[curObject valueForKey:@"HasDisruption" ifKindOf:[NSNumber class] defaultValue:@NO] boolValue];
                         predictionStub.headboardRouteNumber = predictionStub.tramStub.routeStub.headboardNumber = predictionStub.tramStub.headboardRouteNumber = [curObject valueForKey:@"HeadBoardRouteNo" ifKindOf:[NSString class] defaultValue:@""];
                         
                         predictionStub.lowFloor = [[curObject valueForKey:@"IsLowFloorTram" ifKindOf:[NSNumber class] defaultValue:@NO] boolValue];
                         predictionStub.tramTrackerAvailable = [[curObject valueForKey:@"IsTTAvailable" ifKindOf:[NSNumber class] defaultValue:@NO] boolValue];
                         predictionStub.tripID = [curObject valueForKey:@"TripID" ifKindOf:[NSNumber class] defaultValue:@0];
                         predictionStub.tramStub.number = [curObject valueForKey:@"VehicleNo" ifKindOf:[NSNumber class] defaultValue:@0];
                         predictionStub.requestDateTime = [PidsServiceDelegate parseDateFromDotNetJSONString:[response valueForKey:@"timeRequested" ifKindOf:[NSString class] defaultValue:@""]];
                         
                         predictionStub.predictedArrivalDateTime = [PidsServiceDelegate parseDateFromDotNetJSONString:[curObject valueForKey:@"PredictedArrivalDateTime" ifKindOf:[NSString class] defaultValue:@""]];
                         predictionStub.predictedArrivalDateTime = [self calculateLocalPredictedArrivalTimeFromServerTime:predictionStub.predictedArrivalDateTime
                                                                                                    withServerRequestTime:predictionStub.requestDateTime
                                                                                                     withLocalRequestTime:[self requestTime]];
                         
                         if ((!lastPrediction ||
                              ([lastPrediction.routeNo integerValue] != [predictionStub.routeNo integerValue] &&
                               ![lastPrediction.destination isEqualToString:predictionStub.destination])) || trams.count >= 3)
                         {
                             if (trams)
                                 [predictions addObject:trams];
                             trams = [NSMutableArray new];
                         }
                         
                         NSArray    *customDisruption = curObject[@"DisruptionMessage"][@"Messages"];
                         
                         if (customDisruption.count > 0 || predictionStub.disrupted)
                         {
                             if (customDisruption.count > 0)
                             {
                                 for (NSDictionary *curError in customDisruption)
                                     if (curError[@"MessageId"])
                                         disruptions[curError[@"MessageId"]] = curError[@"Message"];
                             }
                             else
                                 [disruptionsRouteNo addObject:predictionStub.routeNo];
                         }
                         
                         NSString * specialMessage = curObject[@"SpecialEventMessage"];
                         
                         if (specialMessage.length > 0 && [specialEvents indexOfObject:specialMessage] == NSNotFound)
                             [specialEvents addObject:specialMessage];
                         
                         [trams addObject:predictionStub];
                         lastPrediction = predictionStub;
                     }
                 }];
                
                /* Create disruptions */
                NSMutableString * message = [NSMutableString new];
                
                if (trams.count)
                    [predictions addObject:trams];
                
                for (NSString *curKey in disruptions.allKeys)
                {
                    if (message.length)
                        [message appendString:[NSString stringWithFormat:@", %@", disruptions[curKey]]];
                    else
                        [message appendString:disruptions[curKey]];
                }
                
                if (disruptionsRouteNo.count)
                {
                    NSString    * newMessage = nil;
                    
                    if ([disruptionsRouteNo count] == 1)
                        newMessage = [NSString stringWithFormat:NSLocalizedString(@"pid-message-disruption-single", @"Single route disruption"), [disruptionsRouteNo allObjects].firstObject];
                    else
                        newMessage = [NSString stringWithFormat:NSLocalizedString(@"pid-message-disruption-multiple", @"Multiple route disruption"), [[disruptionsRouteNo allObjects] componentsJoinedByString:@", "]];
                    
                    if (message.length)
                        [message appendString:[NSString stringWithFormat:@", %@", newMessage]];
                    else
                        [message appendString:newMessage];
                }
                
                if (message.length)
                    [messageList addObject:[SpecialMessage specialMessageWithMessage:message type:SpecialMessageTypeDisruption]];
                
                /* Special Event, create one special message with all special events message */
                
                NSMutableString     * specialMessage = [NSMutableString new];
                
                for (NSString * curSpecialMessage in specialEvents)
                {
                    if (!curSpecialMessage.length)
                        continue;
                    else if (specialMessage.length > 0)
                        [specialMessage appendFormat:@", %@", curSpecialMessage];
                    else
                        [specialMessage appendString:curSpecialMessage];
                }
                
                if (specialMessage.length)
                    [messageList addObject:[SpecialMessage specialMessageWithMessage:specialMessage type:SpecialMessageTypeSpecialEvent]];
                
                finalPredictions = [NSMutableArray new];
                
                for (NSArray *subPredictions in predictions)
                {
                    NSArray     * subPredictionsFiltered = [subPredictions filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"tramTrackerAvailable = true && routeNo !=[c] %@", @"0"]];
                    
                    if (subPredictionsFiltered.count != subPredictions.count)
                        ttAvailable = NO;
                    
                    if (subPredictionsFiltered.count)
                        [finalPredictions addObject:subPredictionsFiltered];
                }
            }
            
            // check that our delegate is setup to receive the predictions
			if ([[self delegate] respondsToSelector:@selector(setPredictions:)])
			{
                NSDictionary    * result = nil;
                
                if (finalPredictions)
                    result = @{@"predictions" : finalPredictions, @"disruptions" : messageList, @"ttAvailable" : @(ttAvailable)};
                
				// pass it to the delegate
				[[self delegate] performSelectorOnMainThread:@selector(setPredictions:) withObject:result waitUntilDone:NO];
			}
		}
        else if(error == nil)
        {
            if(response[@"deletedStops"] && response[@"deletedRoutes"] && response[@"updatedRoutes"] && response[@"updatedStops"]) // [calledMethod isEqualToString:@"Updates"] (at present)
            {
                if([self.delegate respondsToSelector:@selector(processUpdateString:)])
                { [[self delegate] performSelectorOnMainThread:@selector(processUpdateString:) withObject:text waitUntilDone:NO]; }
            }
        }
		else if (error)
		{
			//Unrecognized method
			NSLog(@"Failed to parse JSON response with error: %@, and userInfo: %@", error, error.userInfo);
			[self failWithError:error];
		}
	}
	else
	{
		if (errorMessage != nil && error == nil)
		{
			error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                        code:PIDServiceValidationError
                                    userInfo:[NSDictionary dictionaryWithObject:errorMessage forKey:NSLocalizedDescriptionKey]];
		}
        
		NSLog(@"Failed to parse JSON response with error: %@, and userInfo: %@", error, error.userInfo);
		[self failWithError:error];
	}
}

#pragma mark -
#pragma mark Local methods

+ (NSDate *)parseDateFromDotNetJSONString:(NSString *)string {
    static NSRegularExpression *dateRegEx = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateRegEx = [[NSRegularExpression alloc] initWithPattern:@"^\\/date\\((-?\\d++)(?:([+-])(\\d{2})(\\d{2}))?\\)\\/$" options:NSRegularExpressionCaseInsensitive error:nil];
    });
    NSTextCheckingResult *regexResult = [dateRegEx firstMatchInString:string options:0 range:NSMakeRange(0, [string length])];
	
    if (regexResult)
    {
        // milliseconds
        NSTimeInterval seconds = [[string substringWithRange:[regexResult rangeAtIndex:1]] doubleValue] / 1000.0;
        return [NSDate dateWithTimeIntervalSince1970:seconds];
    }
    return nil;
}

//
// Parse the ISO8601 date sent by the server
//
- (NSDate *)parseDateTime:(NSString *)dateString
{
    ISO8601DateFormatter *formatter = [[ISO8601DateFormatter alloc] init];
    NSDate *newDate = [formatter dateFromString:dateString];
    
	return newDate;
}

//
// The local time and server time is usually different, calculate the predicted arrival time against the local clock
// using the difference between the local request time and the server request time (should be accurate to a second or two)
//
- (NSDate *)calculateLocalPredictedArrivalTimeFromServerTime:(NSDate *)serverPredictedArrival withServerRequestTime:(NSDate *)serverRequestTime withLocalRequestTime:(NSDate *)localRequestTime
{
	// the timeDelta is the difference between our clock and the server clock (approximate)
	//NSLog(@"Server Request: %@, Local Request: %@, Server Arrival: %@", serverRequestTime, localRequestTime, serverPredictedArrival);
	NSTimeInterval timeDelta = [localRequestTime timeIntervalSinceDate:serverRequestTime];
	//NSLog(@"Time Delta: %f", timeDelta);
	
	// add the delta to the prediction to get the predicted arrival time according to our own clock
	NSDate *localPredictedArrival = ([serverPredictedArrival respondsToSelector:@selector(dateByAddingTimeInterval:)] ? [serverPredictedArrival dateByAddingTimeInterval:timeDelta] : [serverPredictedArrival dateByAddingTimeInterval:timeDelta]);
	//NSLog(@"Local Arrival: %@", localPredictedArrival);
	return localPredictedArrival;
}

- (NSDate *)calculateLocalScheduledDepartureTimeFromServerTime:(NSDate *)serverPredictedArrival withServerRequestTime:(NSDate *)serverRequestTime withLocalRequestTime:(NSDate *)localRequestTime
{
	// we only change it when the time zones are different between the two
	NSDateFormatter *timezoneFormatter = [[NSDateFormatter alloc] init];
	[timezoneFormatter setDateFormat:@"ZZZZ"];
	if ([[timezoneFormatter stringFromDate:localRequestTime] isEqualToString:[timezoneFormatter stringFromDate:serverRequestTime]])
	{
		return serverPredictedArrival;
	}
    
	// the timeDelta is the difference between our clock and the server clock (approximate)
	NSTimeInterval timeDelta = [serverRequestTime timeIntervalSinceDate:localRequestTime];
	
	// add the delta to the prediction to get the predicted arrival time according to our own clock
	NSDate *localPredictedArrival = ([serverPredictedArrival respondsToSelector:@selector(dateByAddingTimeInterval:)] ? [serverPredictedArrival dateByAddingTimeInterval:timeDelta] : [serverPredictedArrival dateByAddingTimeInterval:timeDelta]);
	return localPredictedArrival;
}

#pragma mark -
#pragma mark Core Data Retrieval Methods

- (NSManagedObjectContext *) managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
	
    NSPersistentStoreCoordinator *coordinator = [(tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate] persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSConfinementConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}


- (StopStub *)stopForTrackerID:(NSNumber *)aTrackerID
{
    StopStub *stub = [[StopStub alloc] init];
    [stub setTrackerID:aTrackerID];
    return stub;
}

- (RouteStub *)routeWithNumber:(NSString *)aRouteNumber
{
    RouteStub *stub = [[RouteStub alloc] init];
    [stub setNumber:aRouteNumber];
    return stub;
}


- (RouteStub *)routeWithNumber:(NSString *)aRouteNumber headboardRouteNumber:(NSString *)aHeadboardRouteNumber
{
    RouteStub *stub = [[RouteStub alloc] init];
    [stub setNumber:aRouteNumber];
    [stub setHeadboardNumber:aHeadboardRouteNumber];
    return stub;
}

#pragma mark -
#pragma mark Error Handling

- (void)failWithError:(NSError *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
	if (!self.isCancelled && [self.delegate respondsToSelector:@selector(pidsServiceDidFailWithError:)])
		[self.delegate performSelectorOnMainThread:@selector(pidsServiceDidFailWithError:) withObject:error waitUntilDone:NO];
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

- (void)cancel
{
    self.delegate = nil;
    self.cancelled = YES;
    [self.timeoutTimer invalidate];
    self.timeoutTimer = nil;
}

- (void)dealloc
{
//    NSLog(@"PidsServiceDelegate dealloc'ed");
    [self cancel];
}

@end
