//
//  Constants.h
//  
//
//  Created by Hugo Cuvillier on 21/11/2013.
//
//

#import <Foundation/Foundation.h>

@interface CLLocationManager (TramTrackerLocation)
+ (BOOL)isAuthorized;
- (void)requestPermissionAndStartUpdatingLocation;
@end

@interface UITableViewCell (TramTrackerCell)
/// This method resolves an issue in Xcode 6.x whereby the cell is created with the incorrect content size on iOS7, causing constraints to be broken and the app to subsequently crash. Refer http://stackoverflow.com/questions/19132908/auto-layout-constraints-issue-on-ios7-in-uitableviewcell
- (void)correctContentSize;
@end

@interface Constants : NSObject

extern NSString* const BlueCatsKeyAppscoreTest;
extern NSString* const BlueCatsKeyYarraTrams;

/*
 * Segues
 */

extern NSString* const kSegueShowPID;
extern NSString* const kSegueShowUpdatesDetailed;
extern NSString* const kSegueShowMyTramDetailed;
extern NSString* const kSegueShowStopInformation;
extern NSString* const kSegueShowRouteFilter;
extern NSString* const kSegueShowRouteInfo;
extern NSString* const kSegueShowTicketRetailer;
extern NSString* const kSegueShowTicketRetailers;
extern NSString* const kSegueShowTimetables;
extern NSString* const kSegueUnwindFromServiceUpdates;
extern NSString* const kSegueUnwindToMore;
extern NSString* const kSegueSynchronise;
extern NSString* const kSegueShowNearbyStops;
extern NSString* const kSegueShowMostRecent;
extern NSString* const kSegueManageDisruptions;
extern NSString* const kSegueManageAlarms;
extern NSString* const kSegueTimetablesToSelectRoute;
extern NSString* const kSegueRouteFilterToSelectRoute;
extern NSString* const kSegueEditFavourites;
extern NSString* const kSegueTimetablesToStopInfo;
extern NSString* const kSegueScheduleScreen;
extern NSString* const kSeguePIDToTimetables;
extern NSString* const kSegueEditGroups;
extern NSString* const kSeguePIDToStopMap;
extern NSString* const kSegueShowRoute35Info;
extern NSString* const kSeguePIDToMyTram;
extern NSString* const kSegueShowSearchResults;
extern NSString* const kSegueShowPOI;

/*
 * Notifications
 */

extern NSString* const kNotificationSyncFinished;
extern NSString* const kNotificationPIDResetRight;
extern NSString* const kNotificationKillSwitchUpdate;
extern NSString* const kNotificationRoutesResetRight;
//extern NSString * const kNotificationScheduleTouchedInPinOnMap;
//extern NSString * const kNotificationPIDTouchedInPinOnMap;
extern NSString* const kNotificationTicketOutletInfoTouchedInPinOnMap;
extern NSString* const kNotificationRouteFilterApplied;
extern NSString* const kNotificationHasFinishedUpdating;
extern NSString* const TTStopsAndRoutesHaveBeenUpdatedNotice;
extern NSString* const TTTicketOutletsHaveBeenUpdatedNotice;
extern NSString* const kNotificationHasFailedUpdating;
extern NSString* const kNotificationMostRecent;
extern NSString* const kNotificationDeviceTokenAvailable;

/*
 * Deltas
 */

extern NSInteger const  kNearby_Threshold_200M;
extern NSInteger const  kNearby_Threshold_500M;
extern NSInteger const  kNearby_Threshold_MAX;

extern CGFloat const   kDefaultLatDelta;
extern CGFloat const   kDefaultLonDelta;

/*
 * View Names
 */

extern NSString* const kScreenSearch;
extern NSString* const kScreenTicketDetailed;
extern NSString* const kScreenTrackerID;
extern NSString* const kScreenTicketOutlet;
extern NSString* const kScreenRoutesList;
extern NSString* const kScreenTimetables;
extern NSString* const kScreenPID;
extern NSString* const kScreenMyTram;
extern NSString* const kScreenAddFavourites;
extern NSString* const kScreenServiceUpdates;

/*
 * SB Name
 */

extern NSString* const kMainStoryboard;

/*
 * Tab Bar Controller Indices
 */

extern NSInteger const kIndexmyTram;

/*
 * UserDefaults
 */

extern NSString* const kLastSynchronisationDate;
extern NSString* const kKillSwitchShowAdvertising;
extern NSString* const kKillSwitchUseBluecats;
extern NSString* const kSettingsBluecatsDisabled;

/*
 * Search Screen Sections
 */

extern NSString* const kStops;
extern NSString* const kTicketOutlets;
extern NSString* const kPointsOfInterest;

/*
 * Tab Bar Controller Animations
 */

extern NSInteger const kTransitionToMap;
extern NSInteger const kTransitionToList;

@end
