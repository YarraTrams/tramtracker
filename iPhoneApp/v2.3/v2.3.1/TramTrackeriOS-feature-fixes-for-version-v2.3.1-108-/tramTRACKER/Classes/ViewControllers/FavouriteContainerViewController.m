//
//  FavouriteContainerViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 16/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "FavouriteContainerViewController.h"
#import "FavouritesViewController.h"
#import "MapViewController.h"
#import "Constants.h"
#import "FavouriteAddViewController.h"
#import "Filter.h"
#import "PIDViewController.h"
#import "TTTabBarController.h"
#import "Settings.h"
#import "StopList.h"

@interface FavouriteContainerViewController () <CLLocationManagerDelegate>

@property (strong, nonatomic) UITabBarController        * childTabBar;
@property (strong, nonatomic) FavouritesViewController  * favouritesViewController;
@property (strong, nonatomic) MapViewController         * mapViewController;
@property (strong, nonatomic) CLLocationManager         * locationManager;
@property (strong, nonatomic) NSDate                    * dateLocating;

@end

@implementation FavouriteContainerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.childTabBar = self.childViewControllers[0];
    self.favouritesViewController = self.childTabBar.childViewControllers.firstObject;
    self.mapViewController = self.childTabBar.childViewControllers.lastObject;
    
    self.childTabBar.selectedViewController = self.favouritesViewController;
    [self.navigationItem.rightBarButtonItem setImage:[UIImage imageNamed:@"Nav-Map-Right"]];
    
    NSInteger   favCount = [[StopList sharedManager] getFavouriteStopList].count;

    if ([[Settings openWith] isEqualToString:@"Nearest Favourite"] && favCount)
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{ [self locateAndPushNearestFavourite]; });
    }
    else if ([[Settings openWith] isEqualToString:@"Favourite Map"] && favCount)
    {
        self.childTabBar.selectedViewController = self.mapViewController;
        [self.navigationItem.rightBarButtonItem setImage:[UIImage imageNamed:@"Nav-List-Right"]];
        [self.navigationItem.rightBarButtonItem setAccessibilityLabel:NSLocalizedString(@"button-list", nil)];
        
        self.navigationItem.leftBarButtonItem.enabled = NO;
    }
    else if ((![[Settings openWith] isEqualToString:@"Favourite Map"] && ![[Settings openWith] isEqualToString:@"Favourite List"]) ||
             (!favCount))
        self.parentViewController.tabBarController.selectedIndex = 1;
    
    [[NSNotificationCenter defaultCenter] addObserver:self.navigationController selector:@selector(popToRootViewControllerAnimated:) name:kNotificationSyncFinished object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.locationManager.delegate = nil;
}

- (void)locateAndPushNearestFavourite
{
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.dateLocating = [NSDate date];
    [self.locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    if (!self.locationManager)
        return;

    if (locations.lastObject && [[NSDate date] timeIntervalSinceDate:self.dateLocating] <= 2)
    {
        FavouriteStop  * closestFavourite = [[StopList sharedManager] closestFavourite:locations.lastObject];

        if (closestFavourite)
        {
            [self pushToPID:closestFavourite];
            [self.locationManager stopUpdatingLocation];
            self.locationManager = nil;
        }
    }
    else
    {
        [self.locationManager stopUpdatingLocation];
        self.locationManager = nil;
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [self.locationManager stopUpdatingLocation];
    self.locationManager = nil;
}

- (IBAction)editAction:(id)sender
{
    [self.favouritesViewController editAction:nil];
}

- (IBAction)toggleListMapAction:(UIBarButtonItem *)sender
{
    if ([self.favouritesViewController mapAction:nil])
    {
        TTTabBarController  * tabbarController = (TTTabBarController *)self.childTabBar;
        [tabbarController toggleListMapAction:sender isRight:YES];
        
        if (self.childTabBar.selectedViewController == self.favouritesViewController) {
            self.navigationItem.leftBarButtonItem.enabled = NO;
        }
        else
        {
            self.navigationItem.leftBarButtonItem.enabled = YES;
            [self.navigationItem.leftBarButtonItem setAccessibilityLabel:@"Manage Favourites"];
            
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"manage-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(editAction:)];
            self.navigationItem.leftBarButtonItem.enabled = [[StopList sharedManager] getFavouriteStopList].count > 0;
        }
    }
}

#pragma mark - Segue Methods

- (void)pushToPID:(FavouriteStop *)favouriteStop
{
    UIStoryboard        * sb = [UIStoryboard storyboardWithName:kMainStoryboard bundle:nil];
    PIDViewController   * vc = [sb instantiateViewControllerWithIdentifier:kScreenPID];
    
    [vc setFilter:[Filter filterForRoute:favouriteStop.filter.route lowFloor:favouriteStop.filter.lowFloor]];
    [vc setCurrentStop:favouriteStop.stop];
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kSegueShowPID])
    {
        FavouriteStop  * favouriteStop = sender;

        [segue.destinationViewController setFilter:favouriteStop.filter];
        [segue.destinationViewController setCurrentStop:[favouriteStop stop]];
    }
    else if ([segue.identifier isEqualToString:kSegueEditFavourites])
        [segue.destinationViewController setFavouriteToEdit:sender upDirection:YES];
}

@end
