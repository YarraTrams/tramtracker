//
//  NearbyViewController.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 20/11/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RightMenuDelegate.h"

@class Stop;

@interface NearbyViewController : UIViewController

- (void)pushToPID:(Stop *)stop;

@property (weak, nonatomic) IBOutlet UIBarButtonItem    * filtersButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem    * mapButton;
@property (weak, nonatomic) IBOutlet UISearchBar        * searchField;

@end

