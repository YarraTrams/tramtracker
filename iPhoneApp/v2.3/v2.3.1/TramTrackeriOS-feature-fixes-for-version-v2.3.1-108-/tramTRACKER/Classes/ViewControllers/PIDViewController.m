//
//  PIDViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 21/11/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <FrameAccessor/FrameAccessor.h>

#import "PIDViewController.h"
#import "PIDCell.h"
#import "Filter.h"
#import "PidsService.h"
#import "DisruptionUpdateCell.h"
#import "SectionHeaderView.h"
#import "Constants.h"
#import "Prediction.h"
#import "SpecialMessage.h"
#import "FavouriteAddViewController.h"
#import "RouteFilterViewController.h"
#import "TicketOutletsViewController.h"
#import "myTramSearchViewController.h"
#import "SWRevealViewController.h"
#import "Journey.h"
#import "TTActivityIndicatorView.h"
#import "RightMenuDelegate.h"
#import "Analytics.h"
#import "UIAlertView+Blocks.h"
#import "StopList.h"
#import "myTramViewController.h"
#import "ManageAlarmsViewController.h"
#import "AlarmSelectionPopup.h"
#import "TimetablesViewController.h"
#import "TutorialManager.h"

#import <QuartzCore/QuartzCore.h>

static const NSInteger SectionsBeforePrediction = 2;

typedef enum : BOOL {
    /// Sort the groups by the route number
    PIDSortModeRouteNum = YES,
    /// Sort the groups by the ETA of the first tram arriving in that group
    PIDSortModeMinutes = NO,
} PIDSortMode;

@interface PIDTopBar : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel            * stopTitle;
@property (nonatomic, weak) IBOutlet UILabel            * stopSubTitle;
@property (nonatomic, weak) IBOutlet UIImageView        * ftzLogo;
- (void)updateDisplayedStopInformation:(Stop*)stop;
@end

@implementation PIDTopBar

- (void)updateDisplayedStopInformation:(Stop*)stop
{
    // Don't try to update it if we have no stop information
    NSAssert(stop, @"No stop information stored");
    
    // We have to rebuild the title and subtitle information
    //
    // By default, the stop name looks like: Spring St & Bourke St
    // We move the second street name into the subtitle with the direction
    
    NSString *title = stop.name;
    NSString *subtitle = stop.displayedCityDirection;
    
    // If it has an ampersand (&) then it has a secondary street name
    if ([title rangeOfString:@"&"].location != NSNotFound)
    {
        // split it up around the ampersand
        NSArray *pieces = [title componentsSeparatedByString:@"&"];
        
        // take the first piece for the title
        title = [[pieces objectAtIndex:0] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        subtitle = [NSString stringWithFormat:@"%@ - %@", [pieces[1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]], stop.displayedCityDirection];
    }
    
    self.stopTitle.text = title;
    self.stopSubTitle.text = subtitle;
    self.ftzLogo.hidden = !stop.isFTZStop;
}

@end

@interface PIDRoutesHeader : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel*  routesLabel;
@property (nonatomic, weak) IBOutlet UILabel*  minutesLabel;
@property (nonatomic, weak) IBOutlet UIButton* routesButton;
@property (nonatomic, weak) IBOutlet UIButton* minutesButton;
- (void)setSortMode:(PIDSortMode)mode;
@end

@implementation PIDRoutesHeader
- (void)setSortMode:(PIDSortMode)mode
{
    self.routesLabel.layer.borderWidth   = 1.0f;
    self.minutesLabel.layer.borderWidth  = 1.0f;
    self.routesLabel.layer.cornerRadius  = 4.0f;
    self.minutesLabel.layer.cornerRadius = 4.0f;
    
    BOOL selectedRoutes  = (mode == PIDSortModeRouteNum);
    BOOL selectedMinutes = (mode == PIDSortModeMinutes);
    self.routesLabel.layer.backgroundColor  = selectedRoutes  ? [UIColor colorWithWhite:0.9023 alpha:1.0].CGColor : [UIColor clearColor].CGColor;
    self.routesLabel.layer.borderColor      = selectedRoutes  ? [UIColor colorWithWhite:0.8341 alpha:1.0].CGColor : [UIColor clearColor].CGColor;
    self.minutesLabel.layer.backgroundColor = selectedMinutes ? [UIColor colorWithWhite:0.9023 alpha:1.0].CGColor : [UIColor clearColor].CGColor;
    self.minutesLabel.layer.borderColor     = selectedMinutes ? [UIColor colorWithWhite:0.8341 alpha:1.0].CGColor : [UIColor clearColor].CGColor;
    if(selectedRoutes)
    {
        self.routesButton.accessibilityTraits = UIAccessibilityTraitButton | UIAccessibilityTraitSelected;
        self.minutesButton.accessibilityTraits = UIAccessibilityTraitButton;
    }
    if(selectedMinutes)
    {
        self.routesButton.accessibilityTraits = UIAccessibilityTraitButton;
        self.minutesButton.accessibilityTraits = UIAccessibilityTraitButton | UIAccessibilityTraitSelected;
    }
}
@end

@interface PIDViewController ()<UITableViewDataSource, UITableViewDelegate, PIDRightMenuDelegate>

@property (getter = isFirstExpanded) BOOL               firstExpanded;
@property (getter = isSecondExpanded) BOOL              secondExpanded;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ftzRightSpace;

@property (nonatomic, strong) PidsService               * PIDService;
@property (nonatomic, strong) NSTimer                   * timerRefresh;
@property (nonatomic, strong) NSArray                   * currentDisruptions;

@property (nonatomic, strong) NSArray                   * currentPredictions;
@property (nonatomic, strong) NSMutableArray            * currentPredictionsStates;
@property (nonatomic, strong) NSMutableArray            * currentPredictionsExpandable;

@property (nonatomic, strong) NSArray                 * sortedPredictions;
@property (nonatomic, strong) NSMutableArray          * sortedPredictionsExpandable;
@property (nonatomic)           PIDSortMode               sortMode;

@property (nonatomic, strong) UIRefreshControl          * refreshControl;

@property (nonatomic, strong) DisruptionUpdateCell      * updatesCell;
@property (nonatomic, strong) DisruptionUpdateCell      * disruptionsCell;
@property (nonatomic, strong) TTActivityIndicatorView   * activity;

@property (nonatomic, weak) IBOutlet UILabel            * footerTitle;
@property (nonatomic, weak) IBOutlet UITableView        * tableView;
@property (nonatomic, weak) IBOutlet UIView             * tableViewContainer;
@property (nonatomic, assign, getter = isTTAvailable) BOOL ttAvailable;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *footerTitleHeight;
@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (nonatomic, weak) IBOutlet UILabel* stopTrackerIDHeader;
@property (nonatomic, weak) IBOutlet UILabel* stopTrackerID;
@property (nonatomic, weak) IBOutlet UILabel* stopIDHeader;
@property (nonatomic, weak) IBOutlet UILabel* stopID;
@property (nonatomic, weak) IBOutlet UILabel* routesLabel;
@property (nonatomic, weak) IBOutlet UILabel* minutesLabel;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint* tableToBottomConstraint;

@property (nonatomic, weak) IBOutlet UIBarButtonItem    * filtersButton;

@property (nonatomic) BOOL upDirection;
@property (nonatomic) BOOL shownTutorial;

@end

@implementation PIDViewController

- (NSLayoutConstraint *)adSpacingConstraint
{ return self.tableToBottomConstraint; }

#pragma mark Utility methods

- (void)dismissReveal:(id)sender
{
    //push the reveal controller if its there
    if (self.parentViewController.revealViewController.frontViewPosition == FrontViewPositionLeftSide)
    { [self.parentViewController.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES]; }
}

- (NSIndexPath*)indexPathForDisruption:(NSIndexPath*)baseIndexPath
{ return [NSIndexPath indexPathForRow:baseIndexPath.row-1 inSection:baseIndexPath.section]; }

- (NSIndexPath*)indexPathForPredictions:(NSIndexPath*)baseIndexPath
{ return [NSIndexPath indexPathForRow:baseIndexPath.row inSection:baseIndexPath.section-SectionsBeforePrediction]; }

#pragma mark - Inits & Loads

- (PidsService *)PIDService
{
    if (!_PIDService)
    { _PIDService = [[PidsService alloc] initWithDelegate:self]; }
    return _PIDService;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.shownTutorial = NO;
    self.sortMode = PIDSortModeRouteNum;
    
    [self showAds];
    
    if (self.currentStop)
    { [[StopList sharedManager] addMostRecentStopID:self.currentStop.trackerID]; }
    
    self.stopTrackerID.text = [@" " stringByAppendingString:self.currentStop.trackerID.stringValue];
    self.stopID.text = [@" " stringByAppendingString:self.currentStop.number];
    self.stopTrackerID.accessibilityLabel = [NSString stringWithFormat:@"Tracker Stop ID: %@", self.currentStop.trackerID.stringValue];
    self.stopTrackerIDHeader.accessibilityLabel = [NSString stringWithFormat:@"Tracker Stop ID: %@", self.currentStop.trackerID.stringValue];
    self.stopID.accessibilityLabel = [NSString stringWithFormat:@"Stop: %@", self.currentStop.number];
    self.stopIDHeader.accessibilityLabel = [NSString stringWithFormat:@"Stop: %@", self.currentStop.number];

    self.upDirection = [self.currentStop.cityDirection rangeOfString:@"towards City"].location != NSNotFound;
    
    [self.tableView registerNib:[UINib nibWithNibName:kCellDisruptionFull     bundle:[NSBundle mainBundle]] forCellReuseIdentifier:kCellDisruptionFull];
    [self.tableView registerNib:[UINib nibWithNibName:kCellDisruptionMinimal  bundle:[NSBundle mainBundle]] forCellReuseIdentifier:kCellDisruptionMinimal];
    [self.tableView registerNib:[UINib nibWithNibName:kCellUpdateFull         bundle:[NSBundle mainBundle]] forCellReuseIdentifier:kCellUpdateFull];
    [self.tableView registerNib:[UINib nibWithNibName:kCellUpdateMinimal      bundle:[NSBundle mainBundle]] forCellReuseIdentifier:kCellUpdateMinimal];
    
    self.refreshControl = [UIRefreshControl new];
    
    UITableViewController *tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = self.tableView;
    tableViewController.refreshControl = self.refreshControl;
    
    [self.refreshControl addTarget:self action:@selector(refreshPredictions) forControlEvents:UIControlEventValueChanged];
    
    self.filtersButton.target = self.navigationController.tabBarController.revealViewController;
    self.filtersButton.action = @selector(rightRevealToggle:);
    
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self.parentViewController.revealViewController action:@selector(rightRevealToggle:)];
    swipeGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeGesture];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissReveal:)];
    [tap setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:tap];
    
    // Enabling iOS 7 screen-edge-pan-gesture for pop action
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    self.navigationController.interactivePopGestureRecognizer.delegate = nil;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshPredictionsWithDelay) name:UIApplicationDidBecomeActiveNotification object:nil];
    NSLog(@"Pid Screen opened, registered for refresh");
}

- (void)dealloc
{
    NSLog(@"Pid Screen closed, deregistered for refresh");
    [self removeAds:NO];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPIDResetRight object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    self.tableView.delegate   = nil;
    self.tableView.dataSource = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.currentStop)
    {
        self.timerRefresh = [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(refreshPredictions) userInfo:nil repeats:YES];
        [self.timerRefresh fire];
    }
    
    if (!self.currentPredictions && !self.activity)
    { self.activity = [TTActivityIndicatorView activityIndicatorForView:self.tableViewContainer animated:NO]; }

    [[Analytics sharedInstance] featureAccessed:AnalyticsFeaturePID withFilter:self.filter.lowFloor ? AnalyticsFilterLowFloor : AnalyticsFilterAll];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self pause];
}

#pragma mark - Notification Handlers

- (void)updateFilter:(Filter *)aFilter
{
    [self.activity startAnimatingAnimated:NO];
    self.filter = aFilter;
    self.currentPredictions = nil;
    [self.tableView reloadData];
    [self.timerRefresh fire];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationRouteFilterApplied object:aFilter];
}

#pragma mark - User Actions

- (IBAction)backAction:(id)sender
{ [self.navigationController popViewControllerAnimated:YES]; }

- (IBAction)changeModeToRoute:(UIButton*)sender
{
    self.sortMode = PIDSortModeRouteNum;
    if([TutorialManager instance].currentTutorialPID == TutorialPIDSortByRoutes)
    { [[TutorialManager instance] hideTutorial:TutorialScreenPID]; }
}

- (IBAction)changeModeToMinutes:(UIButton*)sender
{
    self.sortMode = PIDSortModeMinutes;
    if([TutorialManager instance].currentTutorialPID == TutorialPIDSortByMinutes)
    { [[TutorialManager instance] hideTutorial:TutorialScreenPID]; }
}

- (void)setSortMode:(PIDSortMode)sortMode
{
    _sortMode = sortMode;
    [self regenerateSortedPredictions];
    for(int i = 0; i < self.currentPredictionsStates.count; i++)
    {
        if([self.sortedPredictionsExpandable[i] isEqual:@YES])
        { self.currentPredictionsStates[i] = @NO; }
        else
        { self.currentPredictionsStates[i] = @YES; }
    }
    
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(2, self.currentPredictions.count)] withRowAnimation:UITableViewRowAnimationFade];
}

#pragma mark - Logic

- (void)pause
{
    [self.timerRefresh invalidate];
    self.timerRefresh = nil;
    [self.PIDService cancel];
    self.PIDService = nil;
}

- (void)refreshPredictionsWithDelay
{
    tramTRACKERAppDelegate* delegate = (tramTRACKERAppDelegate*)[UIApplication sharedApplication].delegate;
    UITabBarController* tabController = [delegate tabBarController];
    if((self.navigationController.visibleViewController == self) && (self.navigationController == tabController.selectedViewController))
    {
        if(!self.activity)
        { self.activity = [TTActivityIndicatorView activityIndicatorForView:self.tableViewContainer animated:false]; }
        else
        { [self.activity startAnimatingAnimated:false]; }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{ [self refreshPredictions]; });
    }
}

- (void)refreshPredictions
{
    if([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
    {
        NSAssert(self.currentStop.trackerID, @"Tracker ID Must be set");

        if (self.filter.route != nil || self.filter.lowFloor)
        { [self.PIDService getPredictionsForStop:self.currentStop.trackerID.floatValue route:self.filter.route lowFloorOnly:self.filter.lowFloor]; }
        else
        { [self.PIDService getPredictionsForStop:self.currentStop.trackerID.floatValue]; }
    }
//    else
//    { NSLog(@"Aborted refreshPredictions PID call - app not active"); }
}

- (void)reloadDisruptionsTableViewState:(BOOL)newState indexPath:(NSIndexPath *)indexPath
{
    BOOL reloadAll;
    if(indexPath)
    {
        NSInteger numRows = self.currentDisruptions.count;
        if((indexPath.row < numRows) && (numRows > 0))
        { reloadAll = NO; }
        else
        { reloadAll = YES; }
    }
    else
    { reloadAll = YES; }
    
    if(reloadAll)
    {
        if(indexPath == nil)
        { [self.tableView reloadData]; }
        else
        { [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade]; }
    }
    else
    { [self.tableView reloadRowsAtIndexPaths:@[ [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section] ] withRowAnimation:UITableViewRowAnimationFade]; }
}

#pragma mark - Segue Methods

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kSegueShowStopInformation] || [segue.identifier isEqualToString:kSeguePIDToStopMap])
    { [segue.destinationViewController setStop:sender]; }
    else if ([segue.identifier isEqualToString:kSegueShowRouteFilter])
    {
        [segue.destinationViewController setStop:sender direction:self.upDirection];
        [segue.destinationViewController setFilter:self.filter];
    }
    else if ([segue.identifier isEqualToString:kSeguePIDToTimetables])
    { [(TimetablesViewController*)segue.destinationViewController setStop:sender direction:self.upDirection isTerminus:[(Stop*)sender routesThroughStop].count == 0]; }
    else if ([segue.identifier isEqualToString:kSeguePIDToMyTram])
    { [segue.destinationViewController setTramNumberString:sender]; }
}

#pragma mark - UITableView Data Source

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 2)
    { return 36.0f; }
    else
    { return 0.0f; }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section == 2)
    {
        PIDRoutesHeader* cell = [tableView dequeueReusableCellWithIdentifier:@"RoutesHeader"];
        [cell setSortMode:self.sortMode];
        
        // Code for disabling the gesture recogniser, to prevent a known issue whereby long pressing on the bar causes a crash.
        NSArray* recognisers = [cell.contentView.gestureRecognizers copy];
        for(UIGestureRecognizer* recogniser in recognisers) { [cell.contentView removeGestureRecognizer:recogniser]; }
        
        self.routesLabel = cell.routesLabel;
        self.minutesLabel = cell.minutesLabel;
        return cell.contentView;
    }
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2 + self.currentPredictionsStates.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0) // Top bar height
    { return 85; }
    else if(indexPath.section == 1)
    {
        if(indexPath.row == 0)
        { return self.isFirstExpanded ? 98.0f : 30.0f; }
        else if(indexPath.row == 1)
        { return self.isSecondExpanded ? 98.0f : 30.0f; }
        else
        { return 30.0f; }
    }
    else
    {
        indexPath = [self indexPathForPredictions:indexPath]; // [NSIndexPath indexPathForRow:indexPath.row - 2 inSection:indexPath.section];
        return [self isEndCell:indexPath] ? 85.0f : 80.0f;
    }
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0)
    { return 1; }
    else if(section == 1)
    { return self.currentDisruptions.count; }
    else
    {
        section -= SectionsBeforePrediction; // offset to original index path
        if(section < self.currentPredictionsStates.count)
        {
            NSNumber* value = self.currentPredictionsStates[section];
            if(section < self.currentPredictions.count)
            { return value.boolValue ? [self.currentPredictions[section] count] : 1; }
        }
        return 1;
    }
}

- (BOOL)isEndCell:(NSIndexPath *)indexPath
{
//    if(indexPath.section <= self.currentPredictions.count)
//    { return NO; }
    
    BOOL        isExpanded = [self.currentPredictionsStates[indexPath.section] boolValue];
    NSInteger   noOfRowsInSection = [self.currentPredictions[indexPath.section] count];
    NSInteger   noOfSections = self.currentPredictions.count;
    
    if(isExpanded)
    {
        return (indexPath.row == noOfRowsInSection - 1);
    }
    else
    { return YES; }
    
    return (!isExpanded && !indexPath.row && indexPath.section != noOfSections - 1) ||
    ((isExpanded && indexPath.row == noOfRowsInSection - 1) && (indexPath.section != noOfSections - 1));
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        PIDTopBar* cell = [tableView dequeueReusableCellWithIdentifier:@"TopBar"];
        [cell updateDisplayedStopInformation:self.currentStop];
        return cell;
    }
    else if(indexPath.section == 1)
    {
        DisruptionUpdateCell    * cell;
        BOOL                    isExpanded = !indexPath.row ? self.isFirstExpanded : self.isSecondExpanded;
        SpecialMessage          * message = self.currentDisruptions[indexPath.row];
        
        if (message.type == SpecialMessageTypeDisruption)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:isExpanded ? kCellDisruptionFull : kCellDisruptionMinimal];
            self.disruptionsCell = cell;
        }
        else
        {
            cell = [tableView dequeueReusableCellWithIdentifier:isExpanded ? kCellUpdateFull : kCellUpdateMinimal];
            self.updatesCell = cell;
        }
        
        [cell configureWithMessage:message.message];
        return cell;
    }
    else // PID cell
    {
        indexPath = [self indexPathForPredictions:indexPath];
    
        static NSString * CellIdentifierEnd = @"PIDCellEnd";
        static NSString * CellIdentifier = @"PIDCell";
    
        PIDCell         * cell = [tableView dequeueReusableCellWithIdentifier:[self isEndCell:indexPath] ? CellIdentifierEnd : CellIdentifier];
        PredictionStub  * currentPrediction = self.sortedPredictions[indexPath.section][indexPath.row];

        __weak PIDViewController* weakSelf = self;
        Stop* currentStop = weakSelf.currentStop;
        
        UIImageView* imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 70, cell.contentView.frame.size.height)];
        imageView.image = [UIImage imageNamed:@"icon_alarm"];
        imageView.contentMode = UIViewContentModeCenter;
        [cell setSwipeGestureWithView:imageView color:[UIColor colorWithRed:0.4723 green:0.7533 blue:0.0 alpha:1.0] mode:MCSwipeTableViewCellModeSwitch state:MCSwipeTableViewCellState3 completionBlock:^(MCSwipeTableViewCell *cell, MCSwipeTableViewCellState stat , MCSwipeTableViewCellMode mode)
        {
            NSTimeInterval timeDifference = [currentPrediction.predictedArrivalDateTime timeIntervalSinceDate:[NSDate date]];
            if(timeDifference >= 180)
            {
                CustomIOSAlertView* alert = [ManageAlarmsViewController popupForView:weakSelf.view stop:currentStop prediction:currentPrediction times:@[ @2, @5, @10 ] completion:^(BOOL successful) {
                    if(successful && ([TutorialManager instance].currentTutorialPID == TutorialPIDTramArrivalAlarm))
                    {
                        [[TutorialManager instance] hideTutorial:TutorialScreenPID];
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(ToastMessageDelay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            if([TutorialManager instance].currentTutorialPID == TutorialPIDTramArrivalAlarm)
                            {
                                [TutorialManager instance].currentTutorialPID = TutorialNone;
                                UIAlertView* alert = [[TutorialManager instance] alertForTutorialEnd:TutorialScreenPID];
                                alert.delegate = weakSelf;
                                [alert show];
                            }
                        });
 
                    }
                } isEstimate:YES];
                [alert show];
            }
            else
            {
                NSInteger numberMinutes = (int)timeDifference/60;
                NSString* message;
                if(numberMinutes <= 0)
                { message = @"now"; }
                else if(numberMinutes == 1)
                { message = @"within 1 minute"; }
                else
                { message = [NSString stringWithFormat:@"within %lu minutes", (long)numberMinutes]; }
                 
                [[[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"You can't set this alarm as your tram is due to arrive %@.", message] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
        }];
        cell.firstTrigger = 0.2;
    
        [cell configureWithPrediction:currentPrediction target:self action:@selector(didSelectAccessoryWithPrediction:)];
        [cell manageExpandCollapseArrows:[self.currentPredictionsStates[indexPath.section] boolValue] index:indexPath.row size:[self.sortedPredictions[indexPath.section] count] isExpandable:[self.currentPredictionsExpandable[indexPath.section] boolValue]];
        return cell;
    }
}

- (NSArray *)indexPathArraysForSection:(NSInteger)section
{
    __block NSMutableArray  * array = [NSMutableArray new];
    
    [self.sortedPredictions[section] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if (idx)
        { [array addObject:[NSIndexPath indexPathForRow:idx inSection:section + SectionsBeforePrediction]]; }
    }];
    return [array copy];
}

#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Section = %lu, Row = %lu", (long)indexPath.section, (long)indexPath.row);
    if(indexPath.section == 1)
    {
        BOOL newState;
        
        if (!indexPath.row)
        { newState = self.firstExpanded = !self.isFirstExpanded; }
        else
        { newState = self.secondExpanded = !self.isSecondExpanded; }
        
        [self reloadDisruptionsTableViewState:newState indexPath:indexPath];
    }
    else if(indexPath.section > 1)
    {
        NSIndexPath* predictionIndexPath = [self indexPathForPredictions:indexPath];
        NSLog(@"Prediction: (%lu, %lu)", (long)predictionIndexPath.section, (long)predictionIndexPath.row);
        if ([self.currentPredictionsExpandable[predictionIndexPath.section] boolValue])
        {
            NSNumber    * expanded = self.currentPredictionsStates[predictionIndexPath.section];
            PIDCell     *topCell = (id)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:indexPath.section]];
            
            self.currentPredictionsStates[predictionIndexPath.section] = @(!expanded.boolValue);
            
            [tableView beginUpdates];
            if (expanded.boolValue)
            {
                [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:0 inSection:indexPath.section]] withRowAnimation:UITableViewRowAnimationFade];
                [tableView deleteRowsAtIndexPaths:[self indexPathArraysForSection:predictionIndexPath.section] withRowAnimation:UITableViewRowAnimationMiddle];
                [tableView endUpdates];
            }
            else
            {
                [tableView insertRowsAtIndexPaths:[self indexPathArraysForSection:predictionIndexPath.section] withRowAnimation:UITableViewRowAnimationMiddle];
                [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:indexPath.section]] withRowAnimation:UITableViewRowAnimationNone];
                [tableView endUpdates];
                [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:indexPath.section] atScrollPosition:UITableViewScrollPositionTop animated:YES];
            }
            
            [topCell manageExpandCollapseArrows:[self.currentPredictionsStates[predictionIndexPath.section] boolValue] index:0 size:[self.currentPredictions[predictionIndexPath.section] count] isExpandable:YES];
        }
    }
}

- (void)didSelectAccessoryWithPrediction:(PredictionStub *)prediction
{
    if (prediction.tramStub.number.integerValue > 0)
    { [self performSegueWithIdentifier:kSeguePIDToMyTram sender:[prediction.tramStub.number stringValue]]; }
}

#pragma mark - UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == TutorialAlertStart)
    {
        if(buttonIndex != alertView.cancelButtonIndex)
        {
            __weak TutorialManager* instance = [TutorialManager instance];
            [TutorialManager instance].currentTutorialPID = TutorialPIDSortByMinutes;
            [instance showTutorial:TutorialPIDSortByMinutes container:self.tableView alertDelegate:self frame:[self.tableView convertRect:self.minutesLabel.bounds fromView:self.minutesLabel] dismiss:^{
                [TutorialManager instance].currentTutorialPID = TutorialPIDSortByRoutes;
                [instance showTutorial:TutorialPIDSortByRoutes container:self.tableView alertDelegate:self frame:[self.tableView convertRect:self.routesLabel.bounds fromView:self.routesLabel] dismiss:^{
                    UITableViewCell* firstCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:SectionsBeforePrediction]];
                    [TutorialManager instance].currentTutorialPID = TutorialPIDTramArrivalAlarm;
                    if(firstCell)
                    { [instance showTutorial:TutorialPIDTramArrivalAlarm container:self.tableView alertDelegate:self frame:[self.tableView convertRect:firstCell.contentView.bounds fromView:firstCell.contentView] dismiss:nil]; }
                }];
            }];
        }
    }
    else if(alertView.tag == TutorialAlertEnd)
    {
        [[TutorialManager instance] setTutorialShown:TutorialScreenPID wasSeen:YES];
    }
}

#pragma mark - PIDsDelegate

- (void)setPredictions:(NSDictionary *)predictionsObject
{
    NSArray * predictions = predictionsObject[@"predictions"];
    NSArray * disruptions = predictionsObject[@"disruptions"];

    self.ttAvailable = [predictionsObject[@"ttAvailable"] boolValue];
    
    if (self.refreshControl.isRefreshing)
    { [self.refreshControl endRefreshing]; }
    
    if (predictionsObject && !self.currentPredictions)
    {
        self.firstExpanded = disruptions.count >= 1;
        self.currentPredictionsStates = [NSMutableArray new];
        self.currentPredictionsExpandable = [NSMutableArray new];
    }
    
    if (predictions.count)
    {
        while (self.currentPredictionsStates.count > predictions.count)
        {
            [self.currentPredictionsStates removeLastObject];
            [self.currentPredictionsExpandable removeLastObject];
        }
        
        for (NSArray * currentPredictions in predictions)
        {
            if (self.currentPredictionsStates.count < predictions.count)
            {
                NSArray * predictionsWithFirstDestination = [currentPredictions filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"destination = %@", [currentPredictions.firstObject destination]]];
                NSArray * predictionsWithFirstRouteNo     = [currentPredictions filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"routeNo = %@",     [currentPredictions.firstObject routeNo]]];
                
                if (predictionsWithFirstDestination.count == predictionsWithFirstRouteNo.count && predictions.count != 1)
                {
                    [self.currentPredictionsStates addObject:@NO];
                    [self.currentPredictionsExpandable addObject:@YES];
                }
                else
                {
                    [self.currentPredictionsStates addObject:@YES];
                    [self.currentPredictionsExpandable addObject:@NO];
                }
            }
        }
    }
    else if (self.filter.lowFloor || self.filter.route)
    { [self pidsServiceDidFailWithError:nil]; }

    self.currentPredictions = predictions;
    self.currentDisruptions = disruptions;
    [self regenerateSortedPredictions];

    [self manageFooterTitleAndIcon];
    [self.tableView reloadData];
    [self reloadDisruptionsTableViewState:NO indexPath:nil];
    
    __weak PIDViewController    * weakSelf = self;
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5f * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        [weakSelf.activity stopAnimatingAnimated:YES];
        weakSelf.activity = nil;
        
        if((self.currentPredictions.count > 0) && (self.shownTutorial == NO) && ([[TutorialManager instance] shouldShowTutorial:TutorialScreenPID]))
        {
            self.shownTutorial = YES;
            UIAlertView* alert = [[TutorialManager instance] alertForTutorialStart:TutorialScreenPID];
            alert.delegate = weakSelf;
            [alert show];
        }
    });
}

- (void)regenerateSortedPredictions
{
    NSMutableArray* sortedExpandable = [NSMutableArray arrayWithCapacity:self.currentPredictions.count];
    if(self.sortMode == PIDSortModeRouteNum)
    {
        self.sortedPredictions = self.currentPredictions;
    }
    else
    {
        self.sortedPredictions = [self.currentPredictions sortedArrayUsingComparator:^NSComparisonResult(NSArray* obj1, NSArray* obj2) {
            NSDate* lowestValue1 = [NSDate distantFuture];
            NSDate* lowestValue2 = [NSDate distantFuture];
            
            for(Prediction* prediction in obj1)
            {
                if(prediction.predictedArrivalDateTime.timeIntervalSince1970 < lowestValue1.timeIntervalSince1970)
                { lowestValue1 = prediction.predictedArrivalDateTime; }
            }
            for(Prediction* prediction in obj2)
            {
                if(prediction.predictedArrivalDateTime.timeIntervalSince1970 < lowestValue2.timeIntervalSince1970)
                { lowestValue2 = prediction.predictedArrivalDateTime; }
            }
            
            return [lowestValue1 compare:lowestValue2];
        }];
    }
    
    for (NSArray * currentPredictions in self.sortedPredictions)
    {
        if (sortedExpandable.count < self.sortedPredictions.count)
        {
            NSArray * predictionsWithFirstDestination = [currentPredictions filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"destination = %@", [currentPredictions.firstObject destination]]];
            NSArray * predictionsWithFirstRouteNo     = [currentPredictions filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"routeNo = %@",     [currentPredictions.firstObject routeNo]]];
            
            if (predictionsWithFirstDestination.count == predictionsWithFirstRouteNo.count && self.sortedPredictions.count != 1)
            { [sortedExpandable addObject:@YES]; }
            else
            { [sortedExpandable addObject:@NO]; }
        }
    }
    
    self.sortedPredictionsExpandable = sortedExpandable;
}

- (void)manageFooterTitleAndIcon
{
    if (!self.currentPredictions.count)
    {
        if (self.filter.lowFloor && self.filter.route)
        { self.footerTitle.text = [NSString stringWithFormat:@"There may be no low floor trams travelling on route %@.", self.filter.route.number]; }
        else if (self.filter.lowFloor)
        { self.footerTitle.text = @"There may be no low floor trams on the selected route."; }
        else if (self.filter.route)
        { self.footerTitle.text = [NSString stringWithFormat:@"There may be no trams travelling on route %@.", self.filter.route.number]; }
        else if (self.currentDisruptions.count && !self.isTTAvailable)
        { self.footerTitle.text = @"Service changes affect trams at this stop. Please consider alternative travel options."; }
        else
        { self.footerTitle.text = @"tramTRACKER had a problem fetching information."; }
    }
    else
    {
        if (self.filter.route && self.filter.lowFloor)
        { self.footerTitle.text = [NSString stringWithFormat:@"Showing low floor trams on route %@.", self.filter.route.number]; }
        else if (self.filter.lowFloor)
        { self.footerTitle.text = @"Showing low floor trams only."; }
        else if (self.filter.route != nil)
        { self.footerTitle.text = [NSString stringWithFormat:@"Showing trams on route %@.", self.filter.route.number]; }
        else if (self.filter.route == nil)
        { self.footerTitle.text = @"Showing all trams on all routes."; }
    }

    CGFloat height = [self.footerTitle sizeThatFits:CGSizeMake(self.footerTitle.frame.size.width, 1000)].height;
    
    self.footerTitleHeight.constant = height;
    [self.view layoutIfNeeded];
    self.tableView.tableFooterView.height = height + 40;
    self.tableView.tableFooterView = self.tableView.tableFooterView;
}

#pragma mark - PIDRightFilterDelegate

- (void)didSelectPIDAction:(PIDRightMenuType)filterType
{
    if (self.filter == nil)
    {
        self.filter = [[Filter alloc] init];
        self.filter.route = nil;
        self.filter.lowFloor = NO;
    }
    
    switch (filterType)
    {
        case PIDRightMenuAll:
            [self.activity startAnimatingAnimated:YES];
            self.filter.lowFloor = NO;
            self.filter.route = nil;
            self.currentPredictions = nil;
            [self.PIDService cancel];
            [self.timerRefresh fire];
            break;
            
        case PIDRightMenuLowFloorOnly:
            [self.activity startAnimatingAnimated:YES];
            self.filter.route = nil;
            self.filter.lowFloor = YES;
            self.currentPredictions = nil;
            [self.tableView reloadData];
            [self.PIDService cancel];
            [self.timerRefresh fire];
            break;
            
        case PIDRightMenuAddToFavourite:
        {
            if (![self.currentStop routesThroughStop].count)
            {
                [[[UIAlertView alloc] initWithTitle:@"Unable to add Stop to Favourites" message:@"This Stop cannot be added to your Favourites as there are no routes connected." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                return;
            }
            
            UIStoryboard                * sb = [UIStoryboard storyboardWithName:kMainStoryboard bundle:nil];
            FavouriteAddViewController  * vc = [sb instantiateViewControllerWithIdentifier:kScreenAddFavourites];
            
            [vc setStop:self.currentStop direction:self.upDirection];
            [vc setFilter:self.filter];
            [self.navigationController pushViewController:vc animated:NO];
            break;
        }
        case PIDRightMenuStopInformation:
            [self performSegueWithIdentifier:kSegueShowStopInformation sender:self.currentStop];
            break;
            
        case PIDRightMenuRouteFilter:
        {
            [self performSegueWithIdentifier:kSegueShowRouteFilter sender:self.currentStop];
            break;
        }
        case PIDRightMenuScheduledDepartures:
        {
            [self performSegueWithIdentifier:kSeguePIDToTimetables sender:self.currentStop];
            break;
        }
        case PIDRightMenuTicketOutlets:
        {
            UIStoryboard                * sb = [UIStoryboard storyboardWithName:kMainStoryboard bundle:nil];
            TicketOutletsViewController * vc = [sb instantiateViewControllerWithIdentifier:kScreenTicketOutlet];
            
            [vc setStop:self.currentStop];
            [self.navigationController pushViewController:vc animated:NO];
            break;
        }
            
        case PIDRightMenuDirections:
        {
            __weak PIDViewController    * weakSelf = self;
            
            [[UIAlertView showWithTitle:@"Open in Maps?" message:@"Would you like to close the tramTracker® app and launch Maps?" cancelButtonTitle:@"Cancel" otherButtonTitles:@[@"Launch Maps"] tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                if (buttonIndex)
                {
                    [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureDirections];
                    MKPlacemark * placemark = [[MKPlacemark alloc] initWithCoordinate:weakSelf.currentStop.coordinate addressDictionary:nil];
                    MKMapItem   * item = [[MKMapItem alloc] initWithPlacemark:placemark];
 
                    item.name = weakSelf.currentStop.name;
                    [item openInMapsWithLaunchOptions:nil];
                }
            }] show];
            break;
        }
        case PIDRightMenuStopMap:
            [self performSegueWithIdentifier:kSeguePIDToStopMap sender:self.currentStop];
            break;
    }

    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2f * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self.parentViewController.revealViewController rightRevealToggle:nil];
    });
}

- (void)pidsServiceDidFailWithError:(NSError *)error
{
    if (!error.code && (self.filter.lowFloor || self.filter.route))
    {
        self.currentPredictions = nil;
        self.currentDisruptions = nil;
        self.currentPredictionsExpandable = nil;
        self.currentPredictionsStates = nil;
        
        [self reloadDisruptionsTableViewState:NO indexPath:nil];
        [self.tableView reloadData];
        
        [self manageFooterTitleAndIcon];
        [self.activity stopAnimatingAnimated:YES];
        self.activity = nil;
    }
    else if (error.code >= 1000 || error.code <= -1000)
    {
        self.currentPredictions = nil;
        self.currentDisruptions = nil;
        self.currentPredictionsExpandable = nil;
        self.currentPredictionsStates = nil;
        
        [self.tableView reloadData];

        if (self.refreshControl.isRefreshing)
        { [self.refreshControl endRefreshing]; }
        self.footerTitle.text = @"Unable to contact the tramTRACKER service. Please make sure that your device is connected to the internet and try again.";

        CGFloat height = [self.footerTitle sizeThatFits:CGSizeMake(self.footerTitle.frame.size.width, 1000)].height;

        self.footerTitleHeight.constant = height;
        [self.view layoutIfNeeded];
        self.tableView.tableFooterView.height = height + 10;
        self.tableView.tableFooterView = self.tableView.tableFooterView;
        [self.activity stopAnimatingAnimated:YES];
        self.activity = nil;
    }
    
    if (self.currentPredictions.count)
    {
        NSInteger lastSection = self.currentPredictions.count - 1;
        BOOL isSectionShown = [self.currentPredictionsStates[lastSection] boolValue];
        NSInteger row = isSectionShown ? [self.currentPredictions[lastSection] count] - 1 : 0;

        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:lastSection] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

@end
