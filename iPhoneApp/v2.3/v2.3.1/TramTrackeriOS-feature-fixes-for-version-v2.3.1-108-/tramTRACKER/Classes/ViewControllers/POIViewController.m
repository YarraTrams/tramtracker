//
//  POIViewController.m
//  tramTRACKER
//
//  Created by Raji on 8/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import <FrameAccessor/FrameAccessor.h>

#import "POIViewController.h"
#import "PointOfInterest.h"
#import "Analytics.h"
#import "Stop.h"

@interface POIViewController ()

@property (weak, nonatomic) IBOutlet UILabel        * poiTitle;
@property (weak, nonatomic) IBOutlet UILabel        * poiDescription;
@property (weak, nonatomic) IBOutlet UILabel        * connectingTrains;
@property (weak, nonatomic) IBOutlet UILabel        * connectingTrams;
@property (weak, nonatomic) IBOutlet UILabel        * connectingBuses;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *detailsHeight;
@property (weak, nonatomic) IBOutlet UIImageView *descriptionBG;
@property (weak, nonatomic) IBOutlet UIView *lastSquare;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint* scrollToBottomConstraint;

@end

@implementation POIViewController

#pragma mark - Inits & Loads

- (NSLayoutConstraint *)adSpacingConstraint
{ return self.scrollToBottomConstraint; }

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self showAds];
	// Do any additional setup after loading the view.
    
    self.poiTitle.text = self.poi.name;
    self.poiDescription.text = self.poi.poiDescription;

    [self updateConnectingServices];
    
    CGFloat newSize = [self.poi heightInCell] - 40;
    
    if (newSize >= self.detailsHeight.constant) {
        self.detailsHeight.constant = newSize;
        [self.view layoutIfNeeded];
//        self.scrollViewHeight.constant = self.lastSquare.y + self.lastSquare.height + 50;
//        [self.view layoutIfNeeded];

        UIEdgeInsets insets = UIEdgeInsetsMake(5, 5, 5, 5);
        self.descriptionBG.image = [[UIImage imageNamed:@"Stop_InformationCell"] resizableImageWithCapInsets:insets];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [[Analytics sharedInstance] featureAccessed:AnalyticsFeaturePOIInfo];
}

- (void)dealloc
{ [self removeAds:NO]; }

- (IBAction)directionsAction:(id)sender
{
    [[[UIAlertView alloc] initWithTitle:@"Open in Maps?"
                                message:@"Would you like to close the tramTracker® app and launch Maps?"
                               delegate:self
                      cancelButtonTitle:@"Cancel"
                      otherButtonTitles:@"Launch Maps", nil]
     show];
}

- (void)updateConnectingServices
{
    NSMutableSet    * trams = [NSMutableSet new];
    NSMutableSet    * buses = [NSMutableSet new];
    NSMutableSet    * trains = [NSMutableSet new];

    for (NSNumber *curStopNumber in self.poi.stopsThroughPOI)
    {
        Stop    *curStop = [Stop stopForTrackerID:curStopNumber];

        if (curStop.connectingTramsList.length)
            [trams addObjectsFromArray:[curStop.connectingTramsList componentsSeparatedByString:@", "]];

        if (curStop.connectingTrainsList.length)
            [trains addObjectsFromArray:[curStop.connectingTrainsList componentsSeparatedByString:@", "]];

        if (curStop.connectingBusesList.length)
            [buses addObjectsFromArray:[curStop.connectingBusesList componentsSeparatedByString:@", "]];
    }
    self.connectingBuses.text = buses.count ? [[buses allObjects] componentsJoinedByString:@", "] : @"None";
    self.connectingTrains.text = trains.count ? [[trains allObjects] componentsJoinedByString:@", "] : @"None";
    self.connectingTrams.text = trams.count ? [[trams allObjects] componentsJoinedByString:@", "] : @"None";
}

#pragma mark - User Actions on Nav Bar

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Utility Methods

- (NSString *)textForConnection:(NSString *)textForConnection
{
    return !textForConnection || [textForConnection isEqualToString:@""] ? @"None" : textForConnection;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex)
    {
        [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureDirections];

        CLLocationDegrees latitude = [self.poi.latitude doubleValue];
        CLLocationDegrees longitude = [self.poi.longitude doubleValue];

        MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:CLLocationCoordinate2DMake(latitude, longitude) addressDictionary:nil];
        MKMapItem *item = [[MKMapItem alloc] initWithPlacemark:placemark];

        item.name = self.poi.name;
        [item openInMapsWithLaunchOptions:nil];
    }
}


@end
