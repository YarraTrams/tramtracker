//
//  NearbyViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 20/11/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "NearbyViewController.h"
#import "SWRevealViewController.h"
#import "PIDViewController.h"
#import "Constants.h"
#import "TTTabBarController.h"
#import "StopRightFilterViewController.h"
#import "Settings.h"

@interface NearbyViewController ()<CLLocationManagerDelegate, SWRevealViewControllerDelegate, StopRightMenuDelegate>

@property (strong, nonatomic) IBOutlet UIView           * containerView;
@property (strong, nonatomic) UITapGestureRecognizer    * dismissGesture;
@property (nonatomic, assign) StopRightMenuType         listType;

@property (strong, nonatomic) UISwipeGestureRecognizer  * revealSwipeGesture;
@property (strong, nonatomic) UISwipeGestureRecognizer  * tableViewSwipeDismissKeyboard;
@property (strong, nonatomic) UITapGestureRecognizer    * tableViewDismissKeyboard;
@property (strong, nonatomic) UITapGestureRecognizer    * navbarDismissKeyboard;

@property (assign, nonatomic) BOOL shouldDismissReveal;

@end

@implementation NearbyViewController

@synthesize filterType = _filterType;

#pragma mark Utility methods

- (void)dismissReveal:(id)sender
{
    //push the reveal controller if its there
    if (self.parentViewController.revealViewController.frontViewPosition == FrontViewPositionLeftSide)
    { [self.parentViewController.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES]; }
}

#pragma mark - Inits & Loads

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.parentViewController.revealViewController.delegate = self;
    
    self.shouldDismissReveal = YES;
    
    UITabBarController * childTabBar = self.childViewControllers.firstObject;
    childTabBar.selectedViewController = childTabBar.childViewControllers.firstObject;

    [self.navigationItem.leftBarButtonItem setImage:[UIImage imageNamed:@"Nav-Map-Left"]];
    [self.navigationItem.leftBarButtonItem setAccessibilityLabel:@"Map"];

    if ([[Settings openWith] isEqualToString:NSLocalizedString(@"settings-startup-nearbymap", nil)])
    {
        childTabBar.selectedViewController = childTabBar.childViewControllers.lastObject;
        [self.navigationItem.leftBarButtonItem setImage:[UIImage imageNamed:@"Nav-List"]];
        [self.navigationItem.leftBarButtonItem setAccessibilityLabel:@"List"];
        
        self.navigationItem.leftBarButtonItem.enabled = self.navigationItem.rightBarButtonItem.enabled = YES;
    }

    self.navigationItem.leftBarButtonItem.enabled = NO;
    self.navigationItem.rightBarButtonItem.enabled = NO;

    self.filtersButton.target = self.parentViewController.revealViewController;
    self.filtersButton.action = @selector(rightRevealToggle:);

    self.revealSwipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipeLeftRightOnContainerView:)];

    self.revealSwipeGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:self.revealSwipeGesture];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissReveal:)];
    [tap setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:tap];

    [[NSNotificationCenter defaultCenter] addObserver:self.navigationController selector:@selector(popToRootViewControllerAnimated:) name:kNotificationSyncFinished object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.searchField.delegate = nil;
}

- (void)didSwipeLeftRightOnContainerView:(UISwipeGestureRecognizer *)aRecogniser
{
    if (!self.searchField.isFirstResponder)
        [self.parentViewController.revealViewController rightRevealToggle:nil];
    else
        [self searchBarSearchButtonClicked:nil];
}
     
- (IBAction)toggleListMapAction:(UIBarButtonItem *)sender
{
    TTTabBarController  * tabbarController = self.childViewControllers.firstObject;

    id<StopRightMenuDelegate> otherViewController;
    
    if (tabbarController.selectedViewController == tabbarController.childViewControllers.firstObject)
        otherViewController = (id)tabbarController.childViewControllers.lastObject;
    else
        otherViewController = (id)tabbarController.childViewControllers.firstObject;

    [otherViewController didSelectStopAction:self.listType];
    [tabbarController toggleListMapAction:sender isRight:NO];
}

- (SWRevealViewController *)revealViewController
{
    return (id)self.parentViewController.parentViewController;
}

- (void)pushToPID:(Stop *)stop
{
    UIStoryboard        * sb = [UIStoryboard storyboardWithName:kMainStoryboard bundle:nil];
    PIDViewController   * vc = [sb instantiateViewControllerWithIdentifier:kScreenPID];
    
    [vc setCurrentStop:stop];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - NearbyRightFilterDelegate

- (void)didSelectStopAction:(StopRightMenuType)filterType
{
    if (filterType == StopRightMenuOutlets)
        self.searchField.placeholder = @"Search by Outlet name or address";
    else
        self.searchField.placeholder = @"Search by Stop name or address";

    switch (filterType)
    {
        case StopRightMenuTrackerID:
            [self pushViewControllerWithID:kScreenTrackerID];
            break ;

        case StopRightMenuSearch:
            [self pushViewControllerWithID:kScreenSearch];
            break;

        default:
        {
            self.listType = filterType;

            UITabBarController          * subTabBar = self.childViewControllers.firstObject;
            id<StopRightMenuDelegate>   curViewController = (id)subTabBar.selectedViewController;
            [curViewController didSelectStopAction:filterType];
            break;
        }
    }

    if (self.shouldDismissReveal)
    { [self.tabBarController.revealViewController rightRevealToggle:nil]; }
    self.shouldDismissReveal = YES;
}

- (void)pushViewControllerWithID:(NSString *)viewControllerID
{
    UIStoryboard        * sb = [UIStoryboard storyboardWithName:kMainStoryboard bundle:nil];
    UIViewController    * vc = [sb instantiateViewControllerWithIdentifier:viewControllerID];

    [self.navigationController pushViewController:vc animated:NO];
}

#pragma mark - SearchBar Delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    self.shouldDismissReveal = NO;
    [self didSelectStopAction:self.listType];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [self searchBarSearchButtonClicked:searchBar];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchField resignFirstResponder];

    /* Remove gesture recognisers */
    [self.containerView removeGestureRecognizer:self.tableViewDismissKeyboard];
    [self.navigationController.navigationBar removeGestureRecognizer:self.navbarDismissKeyboard];
    [self.containerView removeGestureRecognizer:self.tableViewSwipeDismissKeyboard];

    self.revealSwipeGesture.direction = UISwipeGestureRecognizerDirectionLeft;

    /* Enable navbar buttons */
    self.navigationItem.leftBarButtonItem.enabled = YES;
    self.navigationItem.rightBarButtonItem.enabled = YES;

    /* Enable user interaction */
    for (UIView *subView in self.containerView.subviews)
        subView.userInteractionEnabled = YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    /* Hide Keyboard on tableview swipe up/down */
    self.tableViewSwipeDismissKeyboard = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(searchBarSearchButtonClicked:)];
    self.tableViewSwipeDismissKeyboard.direction = UISwipeGestureRecognizerDirectionDown | UISwipeGestureRecognizerDirectionUp;
    [self.containerView addGestureRecognizer:self.tableViewSwipeDismissKeyboard];
    
    /* Hide Keyboard on tableview tap */
    self.tableViewDismissKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchBarSearchButtonClicked:)];
    [self.containerView addGestureRecognizer:self.tableViewDismissKeyboard];

    self.revealSwipeGesture.direction = UISwipeGestureRecognizerDirectionLeft | UISwipeGestureRecognizerDirectionRight;

    /* Hide Keyboard on nav tap */
    self.navbarDismissKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchBarSearchButtonClicked:)];
    [self.navigationController.navigationBar addGestureRecognizer:self.navbarDismissKeyboard];

    /* Disable navbar buttons */
    self.navigationItem.leftBarButtonItem.enabled = NO;
    self.navigationItem.rightBarButtonItem.enabled = NO;

    for (UIView *subView in self.containerView.subviews)
        subView.userInteractionEnabled = NO;
}


@end
