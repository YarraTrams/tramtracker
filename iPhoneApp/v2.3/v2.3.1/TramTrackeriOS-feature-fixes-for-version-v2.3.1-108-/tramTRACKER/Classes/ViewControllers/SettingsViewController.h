//
//  SettingsViewController.h
//  tramTRACKER
//
//  Created by Raji on 19/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BCEventManager.h"
#import "BCMicroLocationManager.h"

@interface BlueCatsCell: UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel*     beaconName;
@property (weak, nonatomic) IBOutlet UILabel*     beaconID;
@property (weak, nonatomic) IBOutlet UILabel*     beaconRange;
@property (weak, nonatomic) IBOutlet UIImageView* beaconIndicator;
@end

@interface SettingsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel* title;
@property (weak, nonatomic) IBOutlet UILabel* detail;
@property (weak, nonatomic) IBOutlet UISwitch* toggle;
@end

@interface SettingsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, BCEventManagerDelegate, BCMicroLocationManagerDelegate>

@end
