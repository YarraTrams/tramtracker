//
//  SearchViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 10/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <FrameAccessor/FrameAccessor.h>

#import "SearchViewController.h"
#import "Constants.h"
#import "StopList.h"
#import "TicketRetailer.h"
#import "PointOfInterest.h"
#import "SearchResultsViewController.h"
#import "Analytics.h"


@interface SearchViewController ()<UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIButton           * tramsFilter;
@property (weak, nonatomic) IBOutlet UIButton           * stopsFilter;
@property (weak, nonatomic) IBOutlet UIButton           * easyAccessFilter;
@property (weak, nonatomic) IBOutlet UIButton           * ticketFilter;
@property (weak, nonatomic) IBOutlet UIButton           * poiFilter;
@property (weak, nonatomic) IBOutlet UIBarButtonItem    * goButton;

@property (weak, nonatomic) IBOutlet UISearchBar        * searchField;
@property (weak, nonatomic) IBOutlet UIScrollView       * scrollView;


@property (nonatomic, assign, readonly) NSInteger       limit;

@property (weak, nonatomic) IBOutlet UIView *subView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint* tableHeightFromBottom;

@end

@implementation SearchViewController

- (NSLayoutConstraint *)adSpacingConstraint
{ return self.tableHeightFromBottom; }

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
        _limit = 100;
    return self;
}

#pragma mark - SearchBar Delegate Methods

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self performSearch];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [self.searchField resignFirstResponder];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (void)didTapScreenWhileSearching
{
    [self.searchField resignFirstResponder];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    self.goButton.enabled = self.searchField.text.length > 2;
}

#pragma mark - User Actions on Nav Bar

-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Utility Methods

- (void)performSearch
{
    [self.searchField resignFirstResponder];
    NSString * searchString = self.searchField.text;
    
    NSInteger       count = 0;
    
    if (![searchString isEqualToString:@""])
    {
        NSMutableArray *data = [[NSMutableArray alloc] init];
        
        if ([self.tramsFilter isSelected])
        {
            NSArray * stops;

            if ([searchString isEqualToString:[@([searchString integerValue]) stringValue]]) {
                id stop = [Stop stopForTrackerID:@([searchString integerValue])];
                if (stop)
                    stops = @[stop];
            }
            if (!stops.count)
                stops = [[StopList sharedManager] stopsBySearchingNameOrSuburb:self.searchField.text];

            if (self.stopsFilter.isSelected) {
                stops = [stops filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"shelter = YES || ytShelter = YES "]];
            }

            if (self.easyAccessFilter.isSelected) {
                stops = [stops filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"easyAccess = YES"]];
            }

            if (stops.count > self.limit)
                stops = [stops subarrayWithRange:NSMakeRange(0, self.limit)];
            if (stops.count)
            {
                stops = [stops sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"stopNumber" ascending:YES],
                                                             [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]]];
                [data addObject:@{@"Name" : kStops, @"List" : stops}];
                count = stops.count;
            }
        }

        if ([self.ticketFilter isSelected] && count < self.limit)
        {
            NSArray     * tickets = [TicketRetailer ticketRetailerBySearchingNameOrAddress:self.searchField.text];
         
            if (tickets.count + count > self.limit)
                tickets = [tickets subarrayWithRange:NSMakeRange(0, self.limit - count)];

            if (tickets.count)
            {
                tickets = [tickets sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]]];
                [data addObject:@{@"Name" : kTicketOutlets, @"List" : tickets}];
                count += tickets.count;
            }
        }

        if ([self.poiFilter isSelected] && count < self.limit)
        {
            NSArray     * pois = [PointOfInterest poisBySearchNameOrAddress:self.searchField.text];
            
            if (pois.count + count > self.limit)
                pois = [pois subarrayWithRange:NSMakeRange(0, self.limit - count)];
            

            
            if (pois.count) {
                pois = [pois sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]]];
                [data addObject:@{@"Name" : kPointsOfInterest, @"List" : pois}];
            }
        }
        
        if (data.count)
            [self performSegueWithIdentifier:kSegueShowSearchResults sender:[NSArray arrayWithArray:data]];
        else
        {
            [[[UIAlertView alloc] initWithTitle:@"No results found"
                                        message:@"Unable to match any results."
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil, nil]
             show];
            
            [self.searchField becomeFirstResponder];
            return;
        }
    }
}

#pragma mark - Inits & Loads

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self showAds];
    
    self.goButton.enabled = NO;
    [self.tramsFilter setSelected:YES];
    [self.poiFilter setSelected:YES];
    [self.ticketFilter setSelected:YES];
    
    UITapGestureRecognizer * tapGesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(didTapScreenWhileSearching)];
    
    tapGesture.delegate = self;
    
    [self.subView addGestureRecognizer:tapGesture];
    [self.subView setUserInteractionEnabled:YES];
    
    [self.scrollView setCanCancelContentTouches:YES];
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardWillHideNotification object:nil];

    [self updateButtonsAccesibility];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    self.searchField.delegate = nil;
    self.scrollView.delegate = nil;
    
    [self removeAds:NO];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureSearch];

    [self.scrollView setScrollEnabled:YES];
    self.scrollView.contentSize = CGSizeMake(320, self.scrollView.frame.size.height);
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.searchField resignFirstResponder];
}

- (void)keyboardShow:(NSNotification *)notification
{
    CGFloat keyboardHeight = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    self.tableHeightFromBottom.constant = keyboardHeight;
    
    [UIView animateWithDuration:1.0f animations:^{
        [self.view layoutIfNeeded];
        self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 380, 0);
    }];
}

- (void)keyboardHide:(NSNotification*)notification
{
    self.tableHeightFromBottom.constant = 0;
    [UIView animateWithDuration:[notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue] animations:^{
        [self.view layoutIfNeeded];
        self.scrollView.contentInset = UIEdgeInsetsZero;
    }];
}


#pragma mark - User Actions

- (IBAction)buttonAction:(UIButton *)sender
{
    UIImage *image =  [UIImage imageNamed: [sender isSelected] ? @"Toggle-Off" : @"Toggle-On"];
    
    [sender setImage:image forState:[sender isSelected] ? UIControlStateNormal : UIControlStateSelected];
    [sender setSelected:![sender isSelected]];
    
    if ((sender == self.easyAccessFilter || sender == self.stopsFilter) && [sender isSelected])
    {
        [self.tramsFilter setImage:image forState:UIControlStateSelected];
        [self.tramsFilter setSelected:[sender isSelected]];
    }
    else if (sender == self.tramsFilter && ![self.tramsFilter isSelected])
    {
        [self.stopsFilter setImage:image forState:UIControlStateNormal];
        [self.stopsFilter setSelected:[sender isSelected]];
        [self.easyAccessFilter setImage:image forState:UIControlStateNormal];
        [self.easyAccessFilter setSelected:[sender isSelected]];
    }
    [self updateButtonsAccesibility];
}

- (void)updateButtonsAccesibility
{
    [self.poiFilter setAccessibilityLabel:
     [NSString stringWithFormat:@"Points Of Interest %@", self.poiFilter.isSelected ? @"On" : @"Off"]];
    [self.tramsFilter setAccessibilityLabel:
     [NSString stringWithFormat:@"Tram Stops %@", self.tramsFilter.isSelected ? @"On" : @"Off"]];
    [self.stopsFilter setAccessibilityLabel:
     [NSString stringWithFormat:@"Stops with shelter %@", self.stopsFilter.isSelected ? @"On" : @"Off"]];
    [self.easyAccessFilter setAccessibilityLabel:
     [NSString stringWithFormat:@"Easy Access Stops %@", self.easyAccessFilter.isSelected ? @"On" : @"Off"]];
    [self.ticketFilter setAccessibilityLabel:
     [NSString stringWithFormat:@"Ticket Outlets %@", self.ticketFilter.isSelected ? @"On" : @"Off"]];
}

- (IBAction)searchAction:(id)sender
{
    [self performSearch];
}

#pragma mark - Segue Methods

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kSegueShowSearchResults])
    {
        SearchResultsViewController * vc = segue.destinationViewController;
        vc.navigationItem.title = [NSString stringWithFormat:@"Results for: %@", self.searchField.text];
        
        [segue.destinationViewController setLists:sender];
        [segue.destinationViewController setBooleanForEasyAccessStops:[self.easyAccessFilter isSelected]
                                                           andShelter:[self.stopsFilter isSelected]];
    }
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    return ![touch.view.class isSubclassOfClass:[UIButton class]];
}

@end
