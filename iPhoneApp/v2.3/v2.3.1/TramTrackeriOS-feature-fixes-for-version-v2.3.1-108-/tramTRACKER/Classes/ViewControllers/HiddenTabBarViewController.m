//
//  HiddenTabBarViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 28/01/2014.
//  Copyright (c) 2014 AppsCore. All rights reserved.
//

#import "HiddenTabBarViewController.h"

@interface HiddenTabBarViewController ()

@end

@implementation HiddenTabBarViewController

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
        {
            for (UIView *view in self.view.subviews)
            {
                if (![view isKindOfClass:[UITabBar class]])
                    view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, view.frame.size.height + 49);
            }
        }
    }
    return self;
}

@end
