//
//  MapViewController.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 4/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RightMenuDelegate.h"

@class Journey;
@class Route;
@class JourneyStop;

@interface MapViewController : UIViewController

typedef enum
{
    MapViewControllerTypeNone,
    MapViewControllerTypeStopList,
    MapViewControllerTypeNearbyList,
    MapViewControllerTypeMyTram,
    MapViewControllerTypeFavouriteList,
    MapViewControllerTypeMostRecent,
    MapViewControllerTypeBrowseList,
    MapViewControllerTypeConnectionList,
    MapViewControllerTypeStopMap,
    MapViewControllerTypeTicketOutlets,
    MapViewControllerTypeTimetables,
    MapViewControllerTypeTicketOutletsMore,
    MapViewControllerTypeSearchResults
}   MapViewControllerType;

@property (strong, nonatomic) Stop* highlightedStop;
- (void)setRoute:(Route *)aRoute direction:(BOOL)upDirection isTimeTables:(BOOL)isTimeTables;
- (void)setFinalJourney:(Journey *)journey;
- (void)didSelectStopAction:(StopRightMenuType)filterType;
- (void)setStopList:(NSArray *)stopList;

- (void)setNextStop:(Stop *)nextStop;
- (void)setAtStop:(Stop *)atStop;

@end
