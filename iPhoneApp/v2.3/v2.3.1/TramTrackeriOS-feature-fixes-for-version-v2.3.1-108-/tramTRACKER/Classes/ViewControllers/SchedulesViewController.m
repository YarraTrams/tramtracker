//
//  SchedulesViewController.m
//  tramTracker
//
//  Created by Jonathan Head on 27/04/2015.
//  Copyright (c) 2015 AppsCore. All rights reserved.
//

#import "SchedulesViewController.h"
#import "PidsService.h"
#import "Journey.h"
#import "MapViewController.h"
#import "SchedulesListViewController.h"
#import "TTTabBarController.h"
#import "Prediction.h"
#import "Constants.h"
#import "TTActivityIndicatorView.h"

@interface SchedulesViewController()
@property (strong, nonatomic) NSTimer*     refreshTimer;
@property (strong, nonatomic) PidsService* pidService;
@property (strong, nonatomic) NSArray*     predictions;

@property (weak, nonatomic) IBOutlet UILabel*                 titleLabel;
@property (weak, nonatomic) IBOutlet UILabel*                 subtitleLabel;
@property (weak, nonatomic) IBOutlet UIButton*                refreshButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView* refreshIndicator;
@property (weak, nonatomic) IBOutlet UIView*                  containerView;
@property (weak, nonatomic) IBOutlet UITableView*             tableView;
@property (weak, nonatomic) TTActivityIndicatorView*          activityView;
@property (nonatomic)       BOOL                              firstAppearance;
@end

@implementation SchedulesViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.pidService = [[PidsService alloc] initWithDelegate:self];
    
    if(self.firstAppearance == NO)
    {
        if(!self.activityView)
        { self.activityView = [TTActivityIndicatorView activityIndicatorForView:self.view animated:NO]; }
        else
        { [self.activityView startAnimatingAnimated:NO]; }
    
        [self downloadStops];
        self.firstAppearance = YES;
    }
}

- (IBAction)onBack:(id)sender
{ [self.navigationController popViewControllerAnimated:YES]; }

- (void)dealloc
{
    [self.pidService cancel];
    [self.pidService setDelegate:nil];
}

- (void)downloadStops
{
    if(self.predictions == nil)
    { [self.pidService getScheduledStopsForTrip:self.tripID scheduledDateTime:self.tripDate]; }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{ return 1; }

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{ return self.predictions.count; }

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    return cell;
}

- (void)setStopList:(NSArray*)stops
{
    self.predictions = stops;
    
    NSMutableArray* stopList = [NSMutableArray array];
    for(Prediction* prediction in self.predictions)
    { [stopList addObject:prediction.stop]; }
    [[self map] setNextStop:self.stop];
    [[self map] setStopList:stopList];
    [[self map] setHighlightedStop:self.stop];
    [[self list] setStop:self.stop];
    [[self list] setPredictionsList:stops];
    [[self list] setRoute:self.route];
    
    Prediction* lastPrediction = self.predictions.lastObject;
    Stop* lastStop = lastPrediction.stop;
    BOOL isUp = [self.route.upStops containsObject:lastStop.trackerID];
    self.titleLabel.text = [self.route routeNameWithNumberAndDestinationUp:isUp];
    //self.titleLabel.text = [NSString stringWithFormat:@"Route %@ To %@", route.nu, jstop.stop.name];

    self.navigationItem.rightBarButtonItem.enabled = YES;
    [self.activityView stopAnimatingAnimated:YES];
}

- (IBAction)toggleListMapAction:(UIBarButtonItem *)sender
{
    TTTabBarController  * tabbarController = self.childViewControllers.firstObject;
    id                  newViewController;
    
    if (tabbarController.selectedViewController == tabbarController.childViewControllers.firstObject)
    { newViewController = tabbarController.childViewControllers.lastObject; }
    else
    { newViewController = tabbarController.childViewControllers.firstObject; }
    
    [tabbarController toggleListMapAction:sender isRight:YES];
}

- (MapViewController*)map
{
    TTTabBarController * tab = self.childViewControllers.lastObject;
    return tab.childViewControllers.lastObject;
}

- (SchedulesListViewController*)list
{
    TTTabBarController * tab = self.childViewControllers.lastObject;
    return tab.childViewControllers.firstObject;
}

@end
