//
//  PIDRightFilterViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 5/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "PIDRightFilterViewController.h"
#import "RightMenuDelegate.h"
#import "SectionHeaderView.h"
#import "SWRevealViewController.h"
#import "Constants.h"
#import "Filter.h"
#import "Analytics.h"

@interface PIDRightFilterViewController ()

@property (nonatomic) NSInteger     selectedIndex;

@property (weak, nonatomic) SWRevealViewController  * revealViewController;

@end

@implementation PIDRightFilterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.revealViewController = (id)self.tabBarController.parentViewController.parentViewController;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotificationPIDResetRight) name:kNotificationPIDResetRight object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveFilterChangeNotification:) name:kNotificationRouteFilterApplied object:nil];
}

- (void)didReceiveFilterChangeNotification:(NSNotification *)notification
{
    Filter * filter = notification.object;
    
    if (filter.route)
        self.selectedIndex = 2;
    else if (filter.lowFloor && !filter.route)
        self.selectedIndex = 1;
    else
        self.selectedIndex = 0;

    [self.tableView reloadData];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.tableView.delegate   = nil;
    self.tableView.dataSource = nil;
}

- (void)didReceiveNotificationPIDResetRight
{
    self.selectedIndex = 0;
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                animated:NO scrollPosition:UITableViewScrollPositionNone];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.view.userInteractionEnabled = YES;
    if (![self.tableView indexPathForSelectedRow])
    {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedIndex inSection:0]
    animated:NO
                              scrollPosition:UITableViewScrollPositionNone];
    }
}

#pragma mark - UITableViewDataSource

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString            * title = !section ? @"Filters" : @"Actions";
    return [SectionHeaderView rightSectionHeaderViewWithTitle:title];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 29.0f;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITabBarController              * tabbarController = (id)self.revealViewController.frontViewController;
    UINavigationController          * navigationController = (id)tabbarController.selectedViewController;
    id<PIDRightMenuDelegate>        PIDInitialViewController = (id)navigationController.topViewController;
    NSInteger                       row = indexPath.row;

    if (!indexPath.section)
    {
        if (row != 2)
        {
            [tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedIndex inSection:0] animated:YES];
            self.selectedIndex = indexPath.row;
        }
        
        AnalyticsFilter filter = AnalyticsFilterAll;
        
        if (row == 1)
            filter = AnalyticsFilterLowFloor;
        else if (row == 2)
            filter = AnalyticsFilterRoute;
        [[Analytics sharedInstance] featureAccessed:AnalyticsFeaturePID withFilter:filter];
    }
    else
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        row += 3;
    }

    if ([PIDInitialViewController respondsToSelector:@selector(didSelectPIDAction:)])
    {
        [PIDInitialViewController didSelectPIDAction:(PIDRightMenuType)row];
        self.view.userInteractionEnabled = NO;
    }
}

@end
