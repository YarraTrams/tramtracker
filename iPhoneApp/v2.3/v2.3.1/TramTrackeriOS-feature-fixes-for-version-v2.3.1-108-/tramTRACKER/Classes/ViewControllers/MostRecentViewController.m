//
//  MostRecentViewController.m
//  tramTRACKER
//
//  Created by Raji on 23/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//


#import "MostRecentViewController.h"
#import "MostRecentContainerViewController.h"
#import "Constants.h"
#import "FavouriteCell.h"
#import "tramTRACKERAppDelegate.h"
#import "Analytics.h"
#import "StopList.h"

@interface MostRecentViewController ()

@property (strong, nonatomic) tramTRACKERAppDelegate    * d;
@property (strong, nonatomic) NSArray                   * stopList;

@property (weak, nonatomic) IBOutlet UITableView* tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint* tableToBottomConstraint;

@end

@implementation MostRecentViewController

#pragma mark - Inits & Loads

- (NSLayoutConstraint *)adSpacingConstraint
{ return self.tableToBottomConstraint; }

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showAds];

    self.stopList = [[StopList sharedManager] mostRecentStops];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveStopUpdateNotification) name:kNotificationMostRecent object:nil];
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
}

- (void)didReceiveStopUpdateNotification {
    self.stopList = [[StopList sharedManager] mostRecentStops];
    [self.tableView reloadData];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.tableView.dataSource = nil;
    self.tableView.delegate   = nil;
    [self removeAds:NO];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];

    [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureMostRecentList];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.stopList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Stop            * stop    = self.stopList[indexPath.row];
    NSString * CellIdentifier = stop.isFTZStop ? @"Favourite Cell FTZ" : @"Favourite Cell";
    FavouriteCell   * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

    cell.labelName.text = [NSString stringWithFormat:@"%@", [stop formattedName]];
    cell.labelRouteDescription.text = [stop formattedRouteDescription];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Stop* stop = self.stopList[indexPath.row];
    return stop.isFTZStop ? tableView.rowHeight + 14 : tableView.rowHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MostRecentContainerViewController   * parent = (id)self.parentViewController.parentViewController;

    [parent pushToPID:self.stopList[indexPath.row]];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
