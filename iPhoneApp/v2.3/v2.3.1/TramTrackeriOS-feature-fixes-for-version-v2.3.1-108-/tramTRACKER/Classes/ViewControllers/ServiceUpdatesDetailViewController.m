//
//  ServiceUpdatesDetailViewController.m
//  tramTRACKER
//
//  Created by Raji on 20/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "ServiceUpdatesDetailViewController.h"
#import "ServiceChange.h"
#import "Analytics.h"

@interface ServiceUpdatesDetailViewController ()

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint* webViewToBottomConstraint;

@end

@implementation ServiceUpdatesDetailViewController

- (NSLayoutConstraint *)adSpacingConstraint
{ return self.webViewToBottomConstraint; }

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self showAds];
    
    self.webView.scalesPageToFit = YES;

	NSError * error = nil;
    NSString *template = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Service Change Template" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
	[self.webView loadHTMLString:[NSString stringWithFormat:template, self.serviceChange.content] baseURL:[NSURL URLWithString:@"http://www.yarratrams.com.au/"]];
    self.title = self.serviceChange.title;
}

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureServiceUpdatesDetailed];
}

- (void)dealloc
{
    self.webView.delegate = nil;
    [self removeAds:NO];
}

@end
