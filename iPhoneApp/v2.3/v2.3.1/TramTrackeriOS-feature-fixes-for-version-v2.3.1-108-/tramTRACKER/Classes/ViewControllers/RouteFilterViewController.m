//
//  RouteFilterViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 13/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "RouteFilterViewController.h"
#import "SelectRouteViewController.h"
#import "Constants.h"
#import "Filter.h"
#import "PIDViewController.h"
#import "Analytics.h"
#import "PIDViewController.h"

@interface RouteFilterViewController ()

@property (strong, nonatomic) Stop          * stop;
@property (strong, nonatomic) Filter        * filter;
@property (assign, nonatomic) NSInteger     selectedRouteIndex;
@property (strong, nonatomic) NSArray       * allRoutesThroughThisStopToDisplay;
@property (nonatomic, assign) BOOL          didSelectionChange;
@property (nonatomic, assign) BOOL          upDirection;

@property (strong, nonatomic)   IBOutlet UILabel            * lblRoute;
@property (strong, nonatomic)   IBOutlet UISwitch           * lowFloorOnlySwitch;
@property (strong, nonatomic)   IBOutlet UITableViewCell    * cellLowFloorOnly;
@property (strong, nonatomic)   IBOutlet UITableViewCell    * cellShow;
@property (weak, nonatomic)     IBOutlet UITableView        * myTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightConstrain;

@end

@implementation RouteFilterViewController

#pragma mark - User Actions on Nav Bar

-(IBAction)backAction:(id)sender
{
    if (self.didSelectionChange)
    {
        NSInteger   currentViewController = self.navigationController.childViewControllers.count - 1;
        id          previousViewController = nil;

        if (currentViewController > 0)
        {
            previousViewController = self.navigationController.childViewControllers[currentViewController - 1];
            [previousViewController updateFilter:self.filter];
        }
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.myTableView.delegate   = nil;
    self.myTableView.dataSource = nil;
    
    self.tableView.delegate     = nil;
    self.tableView.dataSource   = nil;
}

#pragma mark - Utility Methods

- (void)setStop:(Stop *)aStop direction:(BOOL)upDirection
{
    self.stop = aStop;
    self.upDirection = upDirection;
}

#pragma mark - Inits & Loads

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.lowFloorOnlySwitch setOn:self.filter.lowFloor];//self.filter.lowFloorOnly.boolValue

    self.selectedRouteIndex = 0;

    NSMutableArray *arrTemp = [NSMutableArray array];

    [arrTemp insertObject:@"All Routes" atIndex:0];

    for (NSString *routeNo in [self.stop routesThroughStop])
    {
        Route       * route = [Route routeWithNumber:routeNo];
        NSString    * stringToDisplay = [route routeNameWithNumberAndDestinationUp:self.upDirection];

        if (!self.selectedRouteIndex && self.filter.route && [self.filter.route.number isEqualToString:routeNo])
        {
            self.selectedRouteIndex = arrTemp.count;
            self.lblRoute.text = stringToDisplay;
        }
        [arrTemp addObject:stringToDisplay];
    }

    self.allRoutesThroughThisStopToDisplay = [[NSArray alloc] initWithArray:arrTemp];
    self.didSelectionChange = NO;
    
    
    if (SYSTEM_VERSION_LESS_THAN(@"7")) {
        self.rightConstrain.constant = 37;
        [self.view layoutIfNeeded];
    }
    
    self.title = @"Route Filter";
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureRouteFilter];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Segue Methods

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:kSegueRouteFilterToSelectRoute])
    {
        [SelectRouteViewController setCurrentMode:FromRouteFilter];

        [[segue destinationViewController] setSelectedIndex:self.selectedRouteIndex];
        [segue.destinationViewController setStop:self.stop direction:self.upDirection];
    }
}

#pragma mark - Notification Handlers

- (void)updateSelectedRoute:(NSInteger)aSelectedRouteIndex
{
    self.selectedRouteIndex = aSelectedRouteIndex;
    self.lblRoute.text = self.allRoutesThroughThisStopToDisplay[self.selectedRouteIndex];
    
    if (self.selectedRouteIndex)
        self.filter.route = [Route routeWithNumber:self.stop.routesThroughStop[self.selectedRouteIndex - 1]];
    else
        self.filter.route = nil;
    self.didSelectionChange = YES;
}

#pragma mark - User Actions

- (IBAction)lowFloorOnlySwitchChanged:(id)sender
{
    self.filter.lowFloor = !self.filter.lowFloor;
    self.didSelectionChange = YES;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        self.lowFloorOnlySwitch.frame = CGRectMake(205, self.lowFloorOnlySwitch.frame.origin.y, self.lowFloorOnlySwitch.frame.size.width, self.lowFloorOnlySwitch.frame.size.height);
        self.lblRoute.frame = CGRectMake(55, self.lblRoute.frame.origin.y, self.lblRoute.frame.size.width, self.lblRoute.frame.size.height);
    }
    if (indexPath.row)
    { return self.cellShow; }
    
    if(self.cellLowFloorOnly == nil)
    { self.cellLowFloorOnly = [tableView dequeueReusableCellWithIdentifier:@"LowFloorCell"]; }
    return self.cellLowFloorOnly;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        return 30;
    }
    return UITableViewAutomaticDimension;
}

@end
