//
//  HelpViewController.h
//  tramTRACKER
//
//  Created by Raji on 23/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property(nonatomic,strong) NSString * controller;

@end
