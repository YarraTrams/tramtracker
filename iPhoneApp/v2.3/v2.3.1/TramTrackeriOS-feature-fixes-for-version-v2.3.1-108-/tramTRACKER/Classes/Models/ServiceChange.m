//
//  ServiceChange.m
//  tramTRACKER
//
//  Created by Robert Amos on 20/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "ServiceChange.h"


@implementation ServiceChange

@synthesize title, content, dateString;

- (NSString *)description
{
	return [NSString stringWithFormat:@"Service Change %@. Body: %@", title, content];
}

@end
