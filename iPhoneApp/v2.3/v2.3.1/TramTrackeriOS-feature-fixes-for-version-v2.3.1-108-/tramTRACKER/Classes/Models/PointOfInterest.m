//
//  PointOfInterest.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 9/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import "PointOfInterest.h"
#import "Stop.h"
#import "tramTRACKERAppDelegate.h"

@implementation PointOfInterest

@dynamic categoryName;
@dynamic disabledAccess;
@dynamic emailAddress;
@dynamic hasEntryFee;
@dynamic hasToilets;
@dynamic latitude;
@dynamic longitude;
@dynamic moreInfo;
@dynamic name;
@dynamic openingHours;
@dynamic phoneNumber;
@dynamic postcode;
@dynamic streetAddress;
@dynamic poiID;
@dynamic suburb;
@dynamic webAddress;
@dynamic poiDescription;
@dynamic stopsThroughPOI;


- (NSString *)title
{
    return self.name;
}

- (NSString *)subtitle
{
    return self.streetAddress;
}

- (CLLocationCoordinate2D)coordinate
{
    return CLLocationCoordinate2DMake(self.latitude.doubleValue, self.longitude.doubleValue);
}

- (CLLocation *)location
{
	NSAssert([self latitude] && [self longitude], @"Latitude or Longitude not set");

	return [[CLLocation alloc]
            initWithLatitude:[self.latitude doubleValue]
            longitude:[self.longitude doubleValue]];
}

+ (NSArray *)poisForIDs:(NSArray *)poids
{
	// Grab our managed object context and create a fetch request
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [d managedObjectContext];
	NSFetchRequest *fetchRequest = [NSFetchRequest new];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"PointOfInterest" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];

    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"poiID IN %@", poids]];
    [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"poiID" ascending:YES]]];

    // fetch it!
	NSError *error = nil;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (!fetchedObjects.count)
		return nil;
	
	// return the object
	return fetchedObjects;
}

+ (NSArray *)allPOIs
{
	// Grab our managed object context and create a fetch request
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [d managedObjectContext];
	NSFetchRequest *fetchRequest = [NSFetchRequest new];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"PointOfInterest" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// fetch it!
	NSError *error = nil;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (!fetchedObjects.count)
		return nil;
	
	// return the object
	return fetchedObjects;
}

- (CGFloat)heightInCell {
    CGSize size = [self.poiDescription sizeWithFont:[UIFont fontWithName:@"HelveticaNeue" size:12] constrainedToSize:CGSizeMake([UIScreen mainScreen].bounds.size.width-35, 100)];
    return size.height + 60;
}

+ (NSArray *)poisBySearchNameOrAddress:(NSString *)aString
{
	// Grab our managed object context and create a fetch request
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [d managedObjectContext];
	NSFetchRequest *fetchRequest = [NSFetchRequest new];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"PointOfInterest" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@ OR streetAddress CONTAINS[cd] %@", aString, aString];

	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error = nil;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (!fetchedObjects.count)
		return nil;
	
	// return the object
	return fetchedObjects;
}

@end

@implementation PointOfInterestUpdate

+ (instancetype)pointOfInterestUpdateWithID:(NSNumber *)poidID andActionTypeString:(NSString *)actionTypeString
{
    PointOfInterestUpdate * result = [PointOfInterestUpdate new];
    
    result.ID = [poidID integerValue];
    if ([actionTypeString isEqualToString:@"UPDATE"])
        result.actionType = PIDServiceActionTypeUpdate;
    else if ([actionTypeString isEqualToString:@"DELETE"])
        result.actionType = PIDServiceActionTypeDelete;
    return result;
}

@end
