//
//  TicketRetailer.m
//  tramTRACKER
//
//  Created by Robert Amos on 22/07/10.
//  Copyright 2010 Yarra Trams. All rights reserved.
//

#import "TicketRetailer.h"
#import "Stop.h"

@implementation TicketRetailer

@dynamic open24Hour;
@dynamic name;
@dynamic address;
@dynamic sellsMyki;
@dynamic latitude;
@dynamic retailerID;
@dynamic hasMykiTopUp;
@dynamic longitude;
@dynamic nearestStops;
@dynamic suburb;

+ (NSArray *)suburbOutletsFromSuburbStops:(NSArray *)stopList
{
    NSArray         * curSuburbOutlet = nil;
    NSMutableArray  * retailerList = [NSMutableArray new];
    NSString        * previousSuburb = nil;
    
    for (NSArray * curSuburb in stopList)
    {
        curSuburbOutlet = [self outletsFromSuburbStops:curSuburb];

        if (curSuburbOutlet.count)
        {
            NSString        * curSuburb = [curSuburbOutlet.firstObject suburb];
            
            if (!previousSuburb || ![curSuburb isEqualToString:previousSuburb])
                [retailerList addObject:curSuburbOutlet];
            else
            {
                NSArray     * lastSuburb = retailerList.lastObject;
                NSPredicate * predicate = [NSPredicate predicateWithFormat:@"NOT SELF IN %@", lastSuburb];

                [retailerList.lastObject addObjectsFromArray:[curSuburbOutlet filteredArrayUsingPredicate:predicate]];
            }
            previousSuburb = [curSuburbOutlet.firstObject suburb];
        }
    }

    NSMutableArray      * finalRetailers = [NSMutableArray new];
    
    for (NSArray *currentSuburb in retailerList)
        [finalRetailers addObject:[[NSOrderedSet orderedSetWithArray:currentSuburb] array]];

    return finalRetailers;
}

+ (NSArray *)outletsFromSuburbStops:(NSArray *)stopList
{
    TicketRetailer  * lastRetailer = nil;
    NSMutableArray  * outletList = [NSMutableArray new];

    for (Stop * curStop in stopList)
    {
        TicketRetailer * ticketRetailer = [[TicketRetailer nearestTicketRetailersWithoutDistancesToLocation:curStop.location count:1] firstObject];
        
        if (lastRetailer != ticketRetailer && ticketRetailer)
            [outletList addObject:ticketRetailer];
        lastRetailer = ticketRetailer;
    }
    return outletList;
}

+ (NSArray *)allTicketRetailersGroupedBySuburbs
{
    NSArray         * ticketRetailers = [TicketRetailer allTicketRetailers];
    NSMutableArray  * newResults = [NSMutableArray new];
    NSMutableArray  * currentSuburb;
    NSString        * prevSuburb;
    
    for (TicketRetailer *curTicket in ticketRetailers)
    {
        if (!prevSuburb || ![prevSuburb isEqualToString:curTicket.suburb])
        {
            prevSuburb = curTicket.suburb;
            
            if (currentSuburb)
                [newResults addObject:[currentSuburb sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]]]];
            
            currentSuburb = [NSMutableArray new];
        }
        [currentSuburb addObject:curTicket];
    }
    if (currentSuburb.count)
        [newResults addObject:[currentSuburb sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]]]];
    return newResults;
}

/**
 * Returns all Ticket Retailers
 **/

+ (NSArray *)allTicketRetailers
{
	// Grab our managed object context and create a fetch request
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [d managedObjectContext];
	NSFetchRequest *fetchRequest = [NSFetchRequest new];
	
	// set the entity that we're looking for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"TicketRetailer" inManagedObjectContext:context];
    
	NSSortDescriptor    * sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"suburb" ascending:YES];
    
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    [fetchRequest setEntity:entity];
    
	// fetch it.
	NSError *outError;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&outError];
	
	// cant find any?
	if (fetchedObjects == nil || [fetchedObjects count] == 0)
		return nil;
    
	// return them sorted
	return fetchedObjects;
}

/**
 * Returns all ticket retailers that match the search string
 **/
+ (NSArray *)ticketRetailerBySearchingNameOrAddress:(NSString *)search
{
	// Grab our managed object context and create a fetch request
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [d managedObjectContext];
	NSFetchRequest *fetchRequest = [NSFetchRequest new];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"TicketRetailer" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@ OR address CONTAINS[cd] %@", search, search];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (fetchedObjects == nil || [fetchedObjects count] == 0)
	{
		return nil;
	}
	
	// return the object
	return fetchedObjects;
}

/**
 * Returns all ticket retailers in a region
 **/
+ (NSArray *)ticketRetailersInRegion:(MKCoordinateRegion)region
{
	CLLocationDegrees northernBoundary;
	CLLocationDegrees southernBoundary;
	CLLocationDegrees easternBoundary;
	CLLocationDegrees westernBoundary;
	
	// simple
	northernBoundary = region.center.latitude + region.span.latitudeDelta;
	southernBoundary = region.center.latitude - region.span.latitudeDelta;
	easternBoundary = region.center.longitude + region.span.longitudeDelta;
	westernBoundary = region.center.longitude - region.span.longitudeDelta;
	
	// make sure we haven't reversed this
	if (southernBoundary > northernBoundary)
	{
		CLLocationDegrees tmpBoundary = northernBoundary;
		northernBoundary = southernBoundary;
		southernBoundary = tmpBoundary;
	}
	if (westernBoundary > easternBoundary)
	{
		CLLocationDegrees tmpBoundary = easternBoundary;
		easternBoundary = westernBoundary;
		westernBoundary = tmpBoundary;
	}
	
	// Grab our managed object context and create a fetch request
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [d managedObjectContext];
	NSFetchRequest *fetchRequest = [NSFetchRequest new];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"TicketRetailer" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"latitude BETWEEN { %@, %@ } AND longitude BETWEEN { %@, %@ }",
							  [NSNumber numberWithDouble:southernBoundary], [NSNumber numberWithDouble:northernBoundary],
							  [NSNumber numberWithDouble:westernBoundary], [NSNumber numberWithDouble:easternBoundary]];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (fetchedObjects == nil || [fetchedObjects count] == 0)
	{
		return nil;
	}

	// return the object
	return fetchedObjects;
}

+ (NSArray *)nearestTicketRetailersToLocation:(CLLocation *)location count:(NSUInteger)numberOfRetailers
{
	/* Make sure that we have a stop list */
	NSArray *listOfRetailers = nil;
    
	/* Start with a decent sized region and go from there */
	MKCoordinateRegion region;
    
	region.center.latitude = location.coordinate.latitude;
	region.center.longitude = location.coordinate.longitude;
	region.span.latitudeDelta = 0.01796550;
	region.span.longitudeDelta = 0.01982689;
    
	numberOfRetailers = numberOfRetailers > 1000 ? 1000 : numberOfRetailers;
    listOfRetailers = [TicketRetailer ticketRetailersInRegion:region];
    
    for (NSInteger noMaxOfLoop = 300;
         noMaxOfLoop && listOfRetailers.count < numberOfRetailers;
         --noMaxOfLoop)
    {
        region.span.latitudeDelta = region.span.latitudeDelta * 2;
        region.span.longitudeDelta = region.span.longitudeDelta * 2;
        listOfRetailers = [TicketRetailer ticketRetailersInRegion:region];
    }
    
	/* Create an array of ticket retailer distances */
	NSMutableArray *distances = [NSMutableArray new];
    
	/* Loop over the current retailers and work out the distance to the location */
	for (TicketRetailer *retailer in listOfRetailers)
	{
		// create a new distance object
		TicketRetailerDistance *distance = [TicketRetailerDistance new];
        
		// set the reference stop
		[distance setRetailer:retailer];
		
		// set the distance
		[distance setDistance:[LocationManager distanceFromLocation:location toLocation:[retailer location]]];
		
		// add the retailer to the list
		[distances addObject:distance];
		
		// we're finished with the distance object - the array will take it from here
		distance = nil;
	}
	
	// sort the distance object
	[distances sortUsingSelector:@selector(compareDistance:)];
	
	// grab the nearest and release the whole list
	NSArray *nearestTicketRetailerDistances;
	if ([distances count] < numberOfRetailers)
		nearestTicketRetailerDistances = distances;
	else
		nearestTicketRetailerDistances = [distances subarrayWithRange:NSMakeRange(0, numberOfRetailers)];
	
	distances = nil;
	
	// return the list
	return nearestTicketRetailerDistances;
}

+ (NSArray *)nearestTicketRetailersWithoutDistancesToLocation:(CLLocation *)location count:(NSUInteger)numberOfRetailers
{
	NSArray *retailers = [self nearestTicketRetailersToLocation:location count:numberOfRetailers];
	NSMutableArray *newRetailersArray = [NSMutableArray new];
	for (TicketRetailerDistance *s in retailers)
		[newRetailersArray addObject:s.retailer];
	return newRetailersArray;
}


- (BOOL)isOpen24Hours
{
	return [self.open24Hour boolValue];
}
- (BOOL)doesSellMetcard
{
	return NO;
}
- (BOOL)doesSellMyki
{
	return [self.sellsMyki boolValue];
}


- (CLLocation *)location
{
	NSAssert([self latitude] && [self longitude], @"Latitude or Longitude not set");
	
	return [[CLLocation alloc]
            initWithLatitude:[self.latitude doubleValue]
            longitude:[self.longitude doubleValue]];
}

- (CLLocationCoordinate2D)coordinate
{
	return [[self location] coordinate];
}

- (NSString *)title
{
	return self.name;
}
- (NSString *)subtitle
{
	return self.address;
}


- (void)launchGoogleMapsWithDirectionsToHere
{
	// launches the Google Maps application with the ticket retailer's location pinpointed, this will not return, tramTRACKER will close
	NSString *stringURL = [NSString stringWithFormat:@"http://maps.google.com/maps?daddr=%@", self.address];
	NSString *escapedStringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
	NSURL *url = [NSURL URLWithString:escapedStringURL];
	[[UIApplication sharedApplication] openURL:url];
}

- (void)launchGoogleMapsWithDirectionsFromHere
{
	// launches the Google Maps application with the ticket retailer's location pinpointed, this will not return, tramTRACKER will close
	NSString *stringURL = [NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%@", self.address];
	NSString *escapedStringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
	NSURL *url = [NSURL URLWithString:escapedStringURL];
	[[UIApplication sharedApplication] openURL:url];
}

@end


@implementation TicketRetailerDistance

@synthesize retailer;
@synthesize distance;

//
// Compare our distance from the comparisonLocation with the distance of another retailer
//
- (NSComparisonResult)compareDistance:(TicketRetailerDistance *)otherTicketRetailerDistance
{
	return [[NSNumber numberWithDouble:[self distance]] compare:[NSNumber numberWithDouble:[otherTicketRetailerDistance distance]]];
}


// get the description about the ticket retailer distance
- (NSString *)description
{
	return [NSString stringWithFormat:@"%@ at distance %@m", retailer, [NSNumber numberWithDouble:distance]];
}

// return a formatted distance
- (NSString *)formattedDistance
{
	if (self.distance > 10000)
		return @"10 km+";
	if (self.distance == 10000)
		return @"10 km";
	if (self.distance >= 1000)
		return [NSString stringWithFormat:@"%.2f km", self.distance/1000];
	return [NSString stringWithFormat:@"%.0f m", self.distance];
}

@end

@implementation TicketRetailerUpdate

+ (instancetype)ticketRetailerUpdateWithID:(NSNumber *)aID
                       andActionTypeString:(NSString *)aActionTypeString
{
    TicketRetailerUpdate * result = [TicketRetailerUpdate new];
    
    result.ID = [aID integerValue];
    if ([aActionTypeString isEqualToString:@"UPDATE"])
        result.actionType = PIDServiceActionTypeUpdate;
    else if ([aActionTypeString isEqualToString:@"DELETE"])
        result.actionType = PIDServiceActionTypeDelete;
    return result;
}

@end
