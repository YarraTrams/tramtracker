//
//  TutorialManager.m
//  tramTracker
//
//  Created by Jonathan Head on 25/08/2015.
//  Copyright (c) 2015 AppsCore. All rights reserved.
//

#import "TutorialManager.h"
#import "AMPopTip.h"

#define USER_DEFAULT_KEY_TUTORIAL_SHOW_FIRST_TIME @"USER_DEFAULT_KEY_TUTORIAL_SHOW_FIRST_TIME"

static TutorialManager* instance;

@implementation TutorialTableView

- (AMPopTip*)getTutorialView
{
    for(UIView* subview in self.subviews)
    {
        if([subview isKindOfClass:[AMPopTip class]])
        { return (AMPopTip*)subview; }
    }
    return nil;
}

- (void)layoutSubviews
{
    AMPopTip* tutorial = [self getTutorialView];
    if(tutorial)
    { [self bringSubviewToFront:tutorial]; }
    
    [super layoutSubviews];
}

@end

@interface TutorialManager()
@property (strong, nonatomic) AMPopTip* popTipPID;
@property (strong, nonatomic) AMPopTip* popTipSettings;
@property (strong, nonatomic) AMPopTip* popTipMyTram;
@property (strong, nonatomic) AMPopTip* popTipScheduled;
@end

@implementation TutorialManager

+ (TutorialManager *)instance
{
    static dispatch_once_t createToken;
    dispatch_once(&createToken, ^{
        instance = [TutorialManager new];
        
        instance.popTipMyTram    = [AMPopTip popTip];
        instance.popTipPID       = [AMPopTip popTip];
        instance.popTipScheduled = [AMPopTip popTip];
        instance.popTipSettings  = [AMPopTip popTip];
        
        [instance configurePopTip:instance.popTipMyTram];
        [instance configurePopTip:instance.popTipPID];
        [instance configurePopTip:instance.popTipScheduled];
        [instance configurePopTip:instance.popTipSettings];
        
        instance.currentTutorialMyTram    = TutorialNone;
        instance.currentTutorialPID       = TutorialNone;
        instance.currentTutorialScheduled = TutorialNone;
        instance.currentTutorialSettings  = TutorialNone;
    });
    return instance;
}

- (void)configurePopTip:(AMPopTip*)popTip
{
    popTip.borderColor = [UIColor grayColor];
    popTip.borderWidth = 1.0f;
    
#define TEST_TYPE 0
#if TEST_TYPE == 0
    popTip.popoverColor = [UIColor whiteColor];
    popTip.textColor    = [UIColor blackColor];
#elif TEST_TYPE == 1
    popTip.popoverColor = [UIColor colorWithRed: 1.0 green: 1.0 blue: 1.0 alpha: 1.0];
    popTip.textColor    = [UIColor colorWithRed: 0.0312 green: 0.0374 blue: 0.0254 alpha: 1.0];
#elif TEST_TYPE == 2
    popTip.popoverColor = [UIColor colorWithRed: 0.9405 green: 0.9405 blue: 0.9405 alpha: 1.0];
    popTip.textColor    = [UIColor colorWithRed: 0.0312 green: 0.0374 blue: 0.0254 alpha: 1.0];
#elif TEST_TYPE == 3
    popTip.popoverColor = [UIColor colorWithRed: 0.9999 green: 0.9986 blue: 0.9561 alpha: 1.0];
    popTip.textColor    = [UIColor colorWithRed: 0.0312 green: 0.0374 blue: 0.0254 alpha: 1.0];
#elif TEST_TYPE == 4
    popTip.popoverColor = [UIColor colorWithRed: 0.9813 green: 0.9813 blue: 0.9813 alpha: 1.0];
    popTip.textColor    = [UIColor colorWithRed: 0.0312 green: 0.0374 blue: 0.0254 alpha: 1.0];
#elif TEST_TYPE == 5
    popTip.popoverColor = [UIColor colorWithRed: 0.9499 green: 0.9496 blue: 0.9373 alpha: 1.0];
    popTip.textColor    = [UIColor colorWithRed: 0.0312 green: 0.0374 blue: 0.0254 alpha: 1.0];
#elif TEST_TYPE == 6
    popTip.popoverColor = [UIColor colorWithRed: 0.1931 green: 0.1878 blue: 0.1777 alpha: 1.0];
    popTip.textColor    = [UIColor colorWithRed: 1.0 green: 1.0 blue: 1.0 alpha: 1.0];
#endif
    popTip.actionAnimation = AMPopTipActionAnimationPulse;
    popTip.actionPulseOffset = 0.95;
    popTip.actionFloatOffset = 2;
    popTip.edgeMargin = 5;
}

- (BOOL)tutorialShouldShowFirstTime
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:USER_DEFAULT_KEY_TUTORIAL_SHOW_FIRST_TIME];
}

- (void)updateTutorialShouldShowFirstTime:(BOOL)bValue
{
    [[NSUserDefaults standardUserDefaults] setBool:bValue forKey:USER_DEFAULT_KEY_TUTORIAL_SHOW_FIRST_TIME];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString*)keyForTutorialType:(TutorialScreen)type
{
    switch(type)
    {
        case TutorialScreenMyTram:    return @"tutorial_mytram";
        case TutorialScreenPID:       return @"tutorial_pid";
        case TutorialScreenTimetables: return @"tutorial_scheduled";
        case TutorialScreenSettings:  return @"tutorial_settings";
    }
}

- (void)setTutorialShown:(TutorialScreen)type wasSeen:(BOOL)wasSeen
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:wasSeen forKey:[self keyForTutorialType:type]];
    [defaults synchronize];
}

- (BOOL)shouldShowTutorial:(TutorialScreen)type
{
    BOOL bShouldShowFirstTime = [self tutorialShouldShowFirstTime];
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    return ![defaults boolForKey:[self keyForTutorialType:type]] && bShouldShowFirstTime;
}

- (BOOL)allTutorialsShown
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    BOOL shownPid       = [defaults boolForKey:[self keyForTutorialType:TutorialScreenPID]];
    BOOL shownMyTram    = [defaults boolForKey:[self keyForTutorialType:TutorialScreenMyTram]];
    BOOL shownScheduled = [defaults boolForKey:[self keyForTutorialType:TutorialScreenTimetables]];
    BOOL shownSettings  = [defaults boolForKey:[self keyForTutorialType:TutorialScreenSettings]];
    return shownPid && shownMyTram && shownScheduled && shownSettings;
}

- (void)setAllTutorialsShown:(BOOL)shown
{
    [self setTutorialShown:TutorialScreenMyTram    wasSeen:shown];
    [self setTutorialShown:TutorialScreenPID       wasSeen:shown];
    [self setTutorialShown:TutorialScreenTimetables wasSeen:shown];
    [self setTutorialShown:TutorialScreenSettings  wasSeen:shown];
}

- (NSAttributedString*)messageForTutorial:(TutorialMessage)type
{
    NSMutableAttributedString* message;
    NSDictionary* fontAttribute = @{ NSForegroundColorAttributeName:self.popTipPID.textColor, NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue" size:15]};
    UIFont* boldFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
    switch(type)
    {
        case TutorialPIDSortByMinutes:
            message = [[NSMutableAttributedString alloc] initWithString:@"Tap Minutes to order by arrival time." attributes:fontAttribute];
            [message addAttribute:NSFontAttributeName value:boldFont range:NSMakeRange(0, 3)];
            break;
        case TutorialPIDSortByRoutes:
            message = [[NSMutableAttributedString alloc] initWithString:@"Tap Route to sort by route number." attributes:fontAttribute];
            [message addAttribute:NSFontAttributeName value:boldFont range:NSMakeRange(0, 3)];
            break;
        case TutorialPIDTramArrivalAlarm:
            message = [[NSMutableAttributedString alloc] initWithString:@"Swipe left here to receive an alarm when your tram is due to arrive at this stop, or tap here to dismiss this tooltip." attributes:fontAttribute];
            [message addAttribute:NSFontAttributeName value:boldFont range:NSMakeRange(0, 10)];
            break;
        case TutorialSettingsActivateDisruptions:
            message = [[NSMutableAttributedString alloc] initWithString:@"To get disruption notifications, tap this switch." attributes:fontAttribute];
            [message addAttribute:NSFontAttributeName value:boldFont range:NSMakeRange(33, 3)];
            break;
        case TutorialSettingsGotoDisruptions:
            message = [[NSMutableAttributedString alloc] initWithString:@"Tap Manage Notifications to select your travel times and routes." attributes:fontAttribute];
            [message addAttribute:NSFontAttributeName value:boldFont range:NSMakeRange(0, 3)];
            break;
        case TutorialSettingsSelectDisruptionTime:
            message = [[NSMutableAttributedString alloc] initWithString:@"To receive notifications, select at least one travel time..." attributes:fontAttribute];
            [message addAttribute:NSFontAttributeName value:boldFont range:NSMakeRange(26, 6)];
            break;
        case TutorialSettingsSelectDisruptionRoute:
            message = [[NSMutableAttributedString alloc] initWithString:@"...and select at least one route." attributes:fontAttribute];
            [message addAttribute:NSFontAttributeName value:boldFont range:NSMakeRange(7, 6)];
            break;
        case TutorialSettingsSaveDisruptions:
            message = [[NSMutableAttributedString alloc] initWithString:@"Tap Done to finish." attributes:fontAttribute];
            [message addAttribute:NSFontAttributeName value:boldFont range:NSMakeRange(4, 4)];
            break;
        case TutorialSettingsManageAlarms:
            message = [[NSMutableAttributedString alloc] initWithString:@"Tap here to manage your alarms." attributes:fontAttribute];
            [message addAttribute:NSFontAttributeName value:boldFont range:NSMakeRange(0, 3)];
            break;
        case TutorialSettingsDeleteAlarm:
            message = [[NSMutableAttributedString alloc] initWithString:@"To delete an alarm, swipe left here and confirm the action. Press here to continue or swipe left." attributes:fontAttribute];
            [message addAttribute:NSFontAttributeName value:boldFont range:NSMakeRange(60, 5)];
            break;
        case TutorialSettingsBackFromAlarms:
            message = [[NSMutableAttributedString alloc] initWithString:@"Tap here to go back." attributes:fontAttribute];
            [message addAttribute:NSFontAttributeName value:boldFont range:NSMakeRange(0, 3)];
            break;
        case TutorialMyTram:
            message = [[NSMutableAttributedString alloc] initWithString:@"Swipe left here to receive an alarm when you are due to arrive at your destination, or tap here to dismiss this tooltip." attributes:fontAttribute];
            [message addAttribute:NSFontAttributeName value:boldFont range:NSMakeRange(0, 10)];
            break;
        case TutorialMyTramNoEntry:
            message = [[NSMutableAttributedString alloc] initWithString:@"Unable to set an alarm. Your tram is set to arrive at the route destination shortly." attributes:fontAttribute];
            break;
        case TutorialTimetables:
            message = [[NSMutableAttributedString alloc] initWithString:@"Swipe left here to receive an alarm when your tram is due to arrive at this stop, or tap here to dismiss this tooltip." attributes:fontAttribute];
            [message addAttribute:NSFontAttributeName value:boldFont range:NSMakeRange(0, 10)];
            break;
        case TutorialNone: return nil;
    }
    return message;
}

/// Displays the tutorial, appropriately configured, for the provided message type. The frame must be for the supplied container view, and the view will ideally be the top-level view of the view controller.
- (void)showTutorial:(TutorialMessage)type container:(UIView*)container alertDelegate:(id<UIAlertViewDelegate>)alertDelegate frame:(CGRect)frame dismiss:(void(^)(void))dismissHandler
{
    NSAttributedString* message = [self messageForTutorial:type];
    AMPopTipDirection direction = AMPopTipDirectionNone;
    NSInteger maxWidth = 250;
    AMPopTip* popup;
    
    switch(type)
    {
        case TutorialPIDSortByMinutes:
        {
            popup = self.popTipPID;
            direction = AMPopTipDirectionDown;
            popup.shouldDismissOnTap = NO;
            popup.shouldDismissOnTapOutside = NO;
            popup.shouldDismissOnSwipeOutside = NO;
            break;
        }
        case TutorialPIDSortByRoutes:
        {
            popup = self.popTipPID;
            direction = AMPopTipDirectionDown;
            maxWidth = 180;
            popup.shouldDismissOnTap = NO;
            popup.shouldDismissOnTapOutside = NO;
            popup.shouldDismissOnSwipeOutside = NO;
            break;
        }
        case TutorialPIDTramArrivalAlarm:
        {
            popup = self.popTipPID;
            direction = AMPopTipDirectionUp;
            popup.shouldDismissOnTap = YES;
            __weak AMPopTip* weakPopup = popup;
            popup.tapHandler = ^{
                self.currentTutorialPID = TutorialNone;
                UIAlertView* alert = [self alertForTutorialEnd:TutorialScreenPID];
                alert.delegate = alertDelegate;
                [alert show];
                weakPopup.tapHandler = nil;
            };
            break;
        }
        case TutorialTimetables:
        {
            popup = self.popTipScheduled;
            direction = AMPopTipDirectionUp;
            popup.shouldDismissOnTap = YES;
            popup.shouldDismissOnTapOutside = NO;
            __weak AMPopTip* weakPopup = popup;
            popup.tapHandler = ^{
                self.currentTutorialScheduled = TutorialNone;
                UIAlertView* alert = [self alertForTutorialEnd:TutorialScreenTimetables];
                alert.delegate = alertDelegate;
                [alert show];
                weakPopup.tapHandler = nil;
            };
            break;
        }
        case TutorialMyTram:
        {
            popup = self.popTipMyTram;
            direction = AMPopTipDirectionUp;
            popup.shouldDismissOnTap = YES;
            popup.shouldDismissOnTapOutside = NO;
            break;
        }
        case TutorialMyTramNoEntry:
        {
            popup = self.popTipMyTram;
            direction = AMPopTipDirectionNone;
            popup.shouldDismissOnTap = YES;
            popup.shouldDismissOnTapOutside = NO;
            break;
        }
        case TutorialSettingsActivateDisruptions:
        {
            popup = self.popTipSettings;
            direction = AMPopTipDirectionUp;
            // This tutorial should only be dismissable by the user attempting to activate push notifications.
            popup.shouldDismissOnTap          = NO;
            popup.shouldDismissOnTapOutside   = NO;
            popup.shouldDismissOnSwipeOutside = NO;
            break;
        }
        case TutorialSettingsGotoDisruptions:
        {
            popup = self.popTipSettings;
            direction = AMPopTipDirectionUp;
            popup.shouldDismissOnTap          = NO;
            popup.shouldDismissOnTapOutside   = NO;
            popup.shouldDismissOnSwipeOutside = NO;
            break;
        }
        case TutorialSettingsSelectDisruptionTime:
        {
            popup = self.popTipSettings;
            direction = AMPopTipDirectionDown;
            break;
        }
        case TutorialSettingsSelectDisruptionRoute:
        {
            popup = self.popTipSettings;
            direction = AMPopTipDirectionUp;
            break;
        }
        case TutorialSettingsSaveDisruptions:
        {
            popup = self.popTipSettings;
            direction = AMPopTipDirectionDown;
            maxWidth = 200;
            break;
        }
        case TutorialSettingsManageAlarms:
        {
            popup = self.popTipSettings;
            direction = AMPopTipDirectionUp;
            break;
        }
        case TutorialSettingsDeleteAlarm:
        {
            popup = self.popTipSettings;
            direction = AMPopTipDirectionDown;
            popup.shouldDismissOnTap          = YES;
            popup.shouldDismissOnTapOutside   = NO;
            popup.shouldDismissOnSwipeOutside = NO;
            break;
        }
        case TutorialSettingsBackFromAlarms:
        {
            popup = self.popTipSettings;
            direction = AMPopTipDirectionDown;
            popup.shouldDismissOnTap          = NO;
            popup.shouldDismissOnTapOutside   = NO;
            popup.shouldDismissOnSwipeOutside = NO;
            break;
        }
            
        default:
            NSLog(@"Warning: Attempted to show tutorial of unknown type");
            break;
    }
    popup.dismissHandler = dismissHandler;
//    popup.tapHandler = ^{ };
    [popup showAttributedText:message direction:direction maxWidth:maxWidth inView:container fromFrame:frame];
}

- (void)hideTutorial:(TutorialScreen)type
{
    AMPopTip* popup;
    switch(type)
    {
        case TutorialScreenPID:        popup = self.popTipPID;       break;
        case TutorialScreenSettings:   popup = self.popTipSettings;  break;
        case TutorialScreenMyTram:     popup = self.popTipMyTram;    break;
        case TutorialScreenTimetables: popup = self.popTipScheduled; break;
    }
    [popup hide];
}

- (NSString *)alertMessageForScreen:(TutorialScreen)screen
{
    return @"To improve your tramTRACKER experience, there are quick tutorials to guide you through the new features.\n\nTo turn off this reminder go to More > my tramTRACKER > Interactive Tutorials.";
//    return @"To improve your tramTRACKER experience, there are quick tutorials to guide you through the new features.";
//    switch(screen)
//    {
//        case TutorialScreenMyTram:    return @"On a tram and want to be notified when you should prepare to alight? Tap 'View Now' to learn how.\n\nIf you would like to skip the tutorial and view it next time, please select the 'Remind Me Later' option below.";
//        case TutorialScreenPID:       return @"To make your experience with tramTRACKER better, we have introduced an interactive tutorial to help guide you through the new features on the tram arrival screen.\n\nIf you would like to skip the tutorial and view it next time, please select the 'Remind Me Later' option below.";
//        case TutorialScreenScheduled: return @"At your stop and want to be notified when your tram is approaching? Tap 'View Now' to learn how.\n\nIf you would like to skip the tutorial and view it next time, please select the 'Remind Me Later' option below.";
//        case TutorialScreenSettings:  return @"You can now subscribe to disruption notifications and be notified when there is a disruption on your tram route. There is also a section for you to manage your tram and stop arrival alarms.\n\nIf you would like to skip the tutorial and view it next time, please select the 'Remind Me Later' option below.";
//    }
}

- (NSString *)completionAlertMessageForScreen:(TutorialScreen)screen
{
    switch(screen)
    {
        case TutorialScreenSettings:   return @"Thank you for completing this tutorial. Get more tutorials on the tram arrival screen, myTRAM and Timetables.";
        case TutorialScreenTimetables: return @"Thank you for completing this tutorial. Get more tutorials in the tram arrival screen, myTRAM and my tramTRACKER.";
        case TutorialScreenPID:        return @"Thank you for completing this tutorial. Get more tutorials in my tramTRACKER, myTRAM and Timetables.";
        case TutorialScreenMyTram:     return @"Thank you for completing this tutorial. Get more tutorials in the tram arrival screen, Timetables and my tramTRACKER.";
    }
}

- (UIAlertView *)alertForTutorialStart:(TutorialScreen)type
{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"" message:[self alertMessageForScreen:type] delegate:nil cancelButtonTitle:@"Remind Me Later" otherButtonTitles:@"View Now", nil];
    alert.tag = TutorialAlertStart;
    return alert;
}

- (UIAlertView *)alertForTutorialEnd:(TutorialScreen)type
{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"" message:[self completionAlertMessageForScreen:type] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    alert.tag = TutorialAlertEnd;
    return alert;
}

@end
