//
//  Turn.m
//  tramTRACKER
//
//  Created by Robert Amos on 17/06/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "Turn.h"
#import "tramTRACKERAppDelegate.h"

const TurnType TTTurnLeft = 1;
const TurnType TTTurnRight = 2;
const TurnType TTTurnStraight = 3;
const TurnType TTTurnSRight = 4;
const TurnType TTTurnVeerLeft = 5;
const TurnType TTTurnVeerRight = 6;


@implementation Turn

@dynamic type, message, stop, upDirection, route;

+ (Turn *)turnForRoute:(Route *)aRoute upDirection:(BOOL)isUpDirection stop:(Stop *)aStop
{
#ifdef WIDGET_EXTENSION
    return nil;
#else
	// Grab our managed object context and create a fetch request
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [d managedObjectContext];
	NSFetchRequest *fetchRequest = [NSFetchRequest new];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Turn" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"route == %@ AND upDirection == %@ AND stop == %@", aRoute, [NSNumber numberWithBool:isUpDirection], aStop];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (fetchedObjects == nil || [fetchedObjects count] == 0)
	{
		return nil;
	}
	
	// return the object
	return [fetchedObjects objectAtIndex:0];
#endif
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"<Turn: %p> Turning after %@ on %@. Type: %@, Message: %@", self, self.stop, self.route, self.type, self.message];
}

- (BOOL)upDirection
{
	[self willAccessValueForKey:@"upDirection"];
	BOOL up = [[self primitiveValueForKey:@"upDirection"] boolValue];
	[self didAccessValueForKey:@"upDirection"];
	return up;
}

- (void)setUpDirection:(BOOL)isUpDirection
{
	[self willChangeValueForKey:@"upDirection"];
	[self setPrimitiveValue:[NSNumber numberWithBool:isUpDirection] forKey:@"upDirection"];
	[self didChangeValueForKey:@"upDirection"];
}

// Return the image used for this turn
- (UIImage *)image
{
	TurnType t = [self typeOfTurn];
	if (t == TTTurnLeft)
		return [UIImage imageNamed:@"Left.png"];
	else if (t == TTTurnRight)
		return [UIImage imageNamed:@"Right.png"];
	else if (t == TTTurnStraight)
		return [UIImage imageNamed:@"Straight.png"];
	else if (t == TTTurnSRight)
		return [UIImage imageNamed:@"SRight.png"];
	else if (t == TTTurnVeerLeft)
		return [UIImage imageNamed:@"VeerLeft.png"];
	else if (t == TTTurnVeerRight)
		return [UIImage imageNamed:@"VeerRight.png"];
	return nil;
}

- (TurnType)typeOfTurn
{
	return (TurnType)[self.type integerValue];
}

- (void)setTypeFromString:(NSString *)typeString
{
    NSNumber *turnType = @0;
    if ([typeString isEqualToString:@"left"])
    { turnType = [NSNumber numberWithInteger:TTTurnLeft]; }
    else if ([typeString isEqualToString:@"right"])
    { turnType = [NSNumber numberWithInteger:TTTurnRight]; }
    else if ([typeString isEqualToString:@"straight"])
    { turnType = [NSNumber numberWithInteger:TTTurnStraight]; }
    else if ([typeString isEqualToString:@"s_to_right"])
    { turnType = [NSNumber numberWithInteger:TTTurnSRight]; }
    else if ([typeString isEqualToString:@"veer_left"])
    { turnType = [NSNumber numberWithInteger:TTTurnVeerLeft]; }
    else if ([typeString isEqualToString:@"veer_right"])
    { turnType = [NSNumber numberWithInteger:TTTurnVeerRight]; }
    
    self.type = turnType;
}


@end
