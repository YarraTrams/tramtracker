//
//  TimetableCell.h
//  tramTRACKER
//
//  Created by Raji on 24/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCSwipeTableViewCell.h"
#import "Constants.h"

@interface TimetableCell : MCSwipeTableViewCell

- (void)configureCellWithName:(NSString *)name andRouteNumber:(NSString *)routeNumber andTime:(NSString *)timeString andDay:(NSString *)aDay;

@end
