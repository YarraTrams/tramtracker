//
//  HistoricalUpdate.h
//  tramTRACKER
//
//  Created by Robert Amos on 5/08/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <CoreData/CoreData.h>

extern NSString * const HistoricalUpdateFileName;

/**
 * Represents an update that has occured to the local database.
**/
@interface HistoricalUpdate :  NSObject <NSCoding>  

/**
 * Whether the application has historical updates
**/
+ (BOOL)hasHistoricalUpdates;

/**
 * Returns an array of all historical updates
**/
+ (NSArray *)getHistoricalUpdatesList;

/**
 * Saves a new historical updates list to the storage
**/
+ (void)saveHistoricalUpdatesList:(NSArray *)updateList;

+ (void)addHistoricalUpdatesList:(NSArray *)updateList;

/**
 * Clears the historical updates list from storage
**/
+ (void)clearHistoricalUpdatesList;

- (id)initHistoricalUpdate;

- (id)initWithCoder:(NSCoder *)coder;
- (void)encodeWithCoder:(NSCoder *)coder;

@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * message;
@property (nonatomic, strong) NSDate * updatedDate;

+ (instancetype)historicalUpdateWithName:(NSString *)aName message:(NSString *)aMessage;

@end



