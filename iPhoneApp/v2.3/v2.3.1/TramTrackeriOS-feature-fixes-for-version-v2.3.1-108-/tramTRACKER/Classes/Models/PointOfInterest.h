//
//  PointOfInterest.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 9/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <MapKit/MapKit.h>

@class Stop;

@interface PointOfInterestUpdate : NSObject

@property (nonatomic, assign) NSInteger             ID;
@property (nonatomic, assign) PIDServiceActionType  actionType;

+ (instancetype)pointOfInterestUpdateWithID:(NSNumber *)poidID andActionTypeString:(NSString *)actionTypeString;

@end

@interface PointOfInterest : NSManagedObject <MKAnnotation>

+ (NSArray *)poisBySearchNameOrAddress:(NSString *)aString;

@property (nonatomic, retain) NSString          * categoryName;
@property (nonatomic, retain) NSNumber          * disabledAccess;
@property (nonatomic, retain) NSString          * emailAddress;
@property (nonatomic, retain) NSString          * poiDescription;
@property (nonatomic, retain) NSNumber          * hasEntryFee;
@property (nonatomic, retain) NSNumber          * hasToilets;
@property (nonatomic, retain) NSNumber          * poiID;
@property (nonatomic, retain) NSDecimalNumber   * latitude;
@property (nonatomic, retain) NSDecimalNumber   * longitude;
@property (nonatomic, retain) NSString          * moreInfo;
@property (nonatomic, retain) NSString          * name;
@property (nonatomic, retain) NSString          * openingHours;
@property (nonatomic, retain) NSString          * phoneNumber;
@property (nonatomic, retain) NSNumber          * postcode;
@property (nonatomic, retain) NSString          * streetAddress;
@property (nonatomic, retain) NSString          * suburb;
@property (nonatomic, retain) NSString          * webAddress;
@property (nonatomic, strong) NSArray           * stopsThroughPOI;

@end

@interface PointOfInterest (CoreDataGeneratedAccessors)

+ (NSArray *)allPOIs;
+ (NSArray *)poisForIDs:(NSArray *)poids;
- (CGFloat)heightInCell;

@end
