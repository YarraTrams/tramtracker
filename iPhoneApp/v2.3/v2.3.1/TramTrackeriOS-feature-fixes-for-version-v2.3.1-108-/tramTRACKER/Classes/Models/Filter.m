//
//  Filter.m
//  tramTRACKER
//
//  Created by Robert Amos on 24/04/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "Filter.h"
#import "RouteList.h"

@implementation Filter

@synthesize lowFloor, route;

+ (Filter *)filterForRoute:(Route *)aRoute lowFloor:(BOOL)lowFloorOnly
{
	Filter *f = [[Filter alloc] init];
	[f setRoute:aRoute];
	[f setLowFloor:lowFloorOnly];
	return f;
}

+ (Filter *)filterFromDictionary:(NSDictionary *)favourite
{
	Filter *f = [[Filter alloc] init];
	
	// is the route not-zero?
	NSString *route = (NSString *)[favourite objectForKey:@"route"];
	if (![route isEqualToString:@"0"])
		[f setRoute:[[RouteList sharedManager] routeForRouteNumber:route]];

	// set the low floor
	NSString *lowfloor = (NSString *)[favourite objectForKey:@"lowfloor"];
	[f setLowFloor:(lowfloor != nil && [lowfloor isEqualToString:@"YES"])];
	
	// done, return
	return f;
}

+ (Filter *)emptyFilter
{
	Filter *f = [[Filter alloc] init];
	[f setRoute:nil];
	[f setLowFloor:NO];
	return f;
}

- (NSDictionary *)dictionary
{
	NSArray *objects = [NSArray arrayWithObjects:(self.route == nil ? @"0" : self.route.number),
						(self.lowFloor ? @"YES" : @"NO"),
						nil];
	NSArray *keys = [NSArray arrayWithObjects:@"route", @"lowfloor", nil];
	return [NSDictionary dictionaryWithObjects:objects forKeys:keys];
}


- (BOOL)isEqualToFilter:(Filter *)otherFilter
{
	// obvious check
	if (otherFilter == nil)
		return NO;
	
	// check that the lowfloor flag is different
	if (self.lowFloor != otherFilter.lowFloor)
		return NO;
	
	// check that the route is different
	if (![self.route isEqual:otherFilter.route])
		return NO;

	// must be the same then
	return YES;
}

- (BOOL)isEmpty
{
	return !self.lowFloor && self.route == nil;
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"Filter showing %@%@",
			(self.lowFloor ? @"only low floor trams" : @"all trams"),
			(self.route != nil ? [NSString stringWithFormat:@" on %@", self.route] : @"")];
}

@end
