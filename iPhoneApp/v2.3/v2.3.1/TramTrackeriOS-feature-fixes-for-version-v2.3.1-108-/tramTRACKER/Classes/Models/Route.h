//
//  Route.h
//  tramTRACKER
//
//  Created by Robert Amos on 10/02/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Turn.h"
#import "PidsService.h"

extern NSString * const RouteColourYellow;
extern NSString * const RouteColourOrange;
extern NSString * const RouteColourPink;
extern NSString * const RouteColourCyan;
extern NSString * const RouteColourTeal;
extern NSString * const RouteColourGreen;
extern NSString * const RouteColourCityCircle;
extern NSString * const RouteColourDisabled;

@class Stop;
@class RouteStub;

/**
 * Represents a route as travelled by a tram
**/
@interface Route : NSManagedObject

/**
 * The number of the route. Can be alphanumberic. Eg 3 or 3a
**/
@property (nonatomic, strong) NSString *number;

/**
 * The name of the route, generally includes both destinations
**/
@property (nonatomic, strong) NSString *name;

/**
 * The Headboard Route Number. This is the route number that should be displayed on the headboard of the tram
**/
@property (nonatomic, strong) NSString *headboardNumber;

/**
 * The internal route number, as it is known by AVM
**/
@property (nonatomic, strong) NSString *internalNumber;

/**
 * The destination when travelling in the up direction
**/
@property (nonatomic, strong) NSString *upDestination;

/**
 * The destination when travelling in the down direction
**/
@property (nonatomic, strong) NSString *downDestination;

/**
 * An array of stops in the order travelled in the Up direction
**/
@property (nonatomic, strong) NSArray *upStops;

/**
 * An array of stops in the order travelled in the Down direction
**/
@property (nonatomic, strong) NSArray *downStops;

/**
 * The assigned colour of this route
**/
@property (nonatomic, strong) NSString *colour;

/**
 * Whether this route is a sub-route (or short) of another route
**/
@property (nonatomic, getter=isSubRoute) BOOL subRoute;

/**
 * A collection of turns that occur on this route
**/
@property (nonatomic, strong) NSSet* turns;

/**
 * Retrieves an array of all available Routes
**/
+ (NSArray *)allRoutes;

/**
 * Retrieves an array of all public routes (routes that are not sub-routes)
**/
+ (NSArray *)allPublicRoutes;

/**
 * Finds the matching public route for a given route number. This does not support sub-routes.
 *
 * @param	aRouteNumber		A route number. Alphanumeric.
 * @return						A Route object that matches the supplied route number. Or nil if no match found.
**/
+ (Route *)routeWithNumber:(NSString *)aRouteNumber;

/**
 * Finds the matching route for a given route number and headboard route number combination.
 *
 * For example, Route Number 1 with Headboard Route Number 8 will return matching internal route number 22
 *
 * @param	aRouteNumber			A route number. Alphanumeric.
 * @param	aHeadboardRouteNumber	A headboard route number. Alphanumeric.
 * @return							A matching Route object, or nil if no match found.
**/
+ (Route *)routeWithNumber:(NSString *)aRouteNumber headboardRouteNumber:(NSString *)aHeadboardRouteNumber;

/**
 * Updates the stop name to "UpDestination to DownDestination". Is called by the PidsService when
 * the route does not have a specific name.
**/
-(void)updateName;

/**
 * Compare a route with another route using natural sorting. Compares on the route number using its integer value.
 *
 * Allows you to use -sortedArrayUsingSelector:\@selector(compareRoute:) on a NSArray of Routes.
 *
 * @param	otherRoute		The route to compare this route against
**/
-(NSComparisonResult)compareRoute:(Route *)otherRoute;

/**
 * Get a nicely formatted Route Name. Includes the Route Number in the output.
**/
-(NSString *)formattedRouteName;

/**
 * Returns in format :- Route <number> To <destination>
**/
- (NSString *)routeNameWithNumberAndDestinationUp:(BOOL)upDirection;

/**
 * Get a nicely formatted destination when departing a specific stop.
 *
 * The destination message may include a via where appropriate.
 *
 * @param	stop			A Stop object that is on the route
 * @param	upDirection		Whether the departing tram would be travelling in the up direction
 * @return					A formatted destination with via information
**/
-(NSString *)formattedDestinationDepartingFromStop:(Stop *)stop upDirection:(BOOL)isUpDirection;

/**
 * Get information about a turn from a route
**/
- (Turn *)turnForStop:(Stop *)stop upDirection:(BOOL)isUpDirection;

/**
 * Get the route's destination after passing a particular tracker ID
 *
 * @param	trackerID		A tramTRACKER Tracker ID
 * @return					The destination
 * @todo					Deprecate this method. Fold it into -formattedDestinationDepartingFromStop:upDirection;
**/
- (NSString *)destinationForTrackerID:(NSNumber *)trackerID;

/**
 * Converts a RouteStub into a full route
**/
+ (Route *)routeForStub:(RouteStub *)stub;


/**
 * RouteName depending on direction
 **/
- (NSString *)nameWithDirection:(BOOL)upDirection;

@end

@interface Route (CoreDataGeneratedAccessors)
- (void)addTurnsObject:(Turn *)value;
- (void)removeTurnsObject:(Turn *)value;
- (void)addTurns:(NSSet *)value;
- (void)removeTurns:(NSSet *)value;

@end


/**
 * Represents part of a route. This is used by the PidsService when compiling routing results.
 * Generally indicates travel for a specific route number in a specific direction. Two RouteParts
 * are combined into a proper Route.
**/
@interface RoutePart : NSObject
{
	/**
	 * The number of the route. Can be alphanumeric.
	**/
	NSString *number;

	/**
	 * The direction of travel. YES to indicate the part is for the up direction, NO to indicate down.
	**/
	BOOL isUpPart;

	/**
	 * The destination for this direction of travel
	**/
	NSString *destination;
}

@property (nonatomic, strong) NSString *number;
@property (nonatomic) BOOL isUpPart;
@property (nonatomic, strong) NSString *destination;

@end

/**
 * A RouteUpdate object is a temporary object used by the synchronisation system to
 * describe a route that needs to be updated and to track its progress
**/
@interface RouteUpdate : NSObject
{
	/**
	 * The Route Number
	**/
	NSString *routeNumber;
	
	/**
	 * The internal Route number
	**/
	NSString *internalRouteNumber;

	/**
	 * The headboard route number
	**/
	NSString *headboardRouteNumber;

	/**
	 * The type of action to be performed on the route. Could be PIDServiceActionTypeUpdate or PIDServiceActionTypeDelete.
	**/
	PIDServiceActionType actionType;
	
	/**
	 * Status - Whether the route numbers have been updated.
	**/
	BOOL numbersHaveBeenCompleted;
	
	/**
	 * Status - Whether the destination names have been updated
	**/
	BOOL destinationsHaveBeenCompleted;

	/**
	 * Status - Whether the up stop lists have been updated
	**/
	BOOL upStopsHaveBeenCompleted;

	/**
	 * Status - Whether the down stop lists have been updated
	**/
	BOOL downStopsHaveBeenCompleted;

	/**
	 * Whether the route is a sub-route
	**/
	BOOL subRoute;
    
    /**
     * The colour of the route
    **/
    NSString *colour;
}

@property (nonatomic, strong) NSString *routeNumber;
@property (nonatomic, strong) NSString *internalRouteNumber;
@property (nonatomic, strong) NSString *headboardRouteNumber;
@property (nonatomic, getter=isSubRoute) BOOL subRoute;
@property (nonatomic) PIDServiceActionType actionType;
@property (nonatomic) BOOL numbersHaveBeenCompleted;
@property (nonatomic) BOOL destinationsHaveBeenCompleted;
@property (nonatomic) BOOL upStopsHaveBeenCompleted;
@property (nonatomic) BOOL downStopsHaveBeenCompleted;
@property (nonatomic, strong) NSString *colour;

@end

/**
 * A Route Stub is a partial route that is used by the PIDS Service to return
 * results from the background thread.
 *
 * Pass it through +[Route routeForStub:] to convert it to a full route.
**/
@interface RouteStub : NSObject
{
    NSString *number;
    NSString *headboardNumber;
}

@property (nonatomic, strong) NSString *number;
@property (nonatomic, strong) NSString *headboardNumber;

@end