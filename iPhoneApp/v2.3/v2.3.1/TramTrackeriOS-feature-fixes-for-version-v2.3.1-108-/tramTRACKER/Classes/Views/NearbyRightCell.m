//
//  NearbyRightCell.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 21/11/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "NearbyRightCell.h"

@implementation NearbyRightCell

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        UIView *    selectedBackgroundView = [UIView new];
        
        selectedBackgroundView.backgroundColor = [UIColor clearColor];
        
        self.selectedBackgroundView = selectedBackgroundView;
    }
    return self;
}

@end
