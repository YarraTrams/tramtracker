//
//  NearbyHeaderView.h
//  tramTRACKER
//
//  Created by Tom King on 1/11/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SectionHeaderView : UIView

typedef enum
{
    SectionHeaderViewNearby,
    SectionHeaderViewNearbyRight,
}       SectionHeaderViewType;

+ (SectionHeaderView *)staticSectionHeaderWithTitle:(NSString *)aTitle;
+ (SectionHeaderView *)nearbySectionHeaderWithTitle:(NSString *)aTitle;
+ (SectionHeaderView *)expandableSectionHeaderViewWithTitle:(NSString *)aTitle
                                                    section:(NSInteger)aSection
                                                     target:(id)target
                                                     action:(SEL)action;

+ (SectionHeaderView *)greyExpandableSectionHeaderViewWithTitle:(NSString *)aTitle
                                                        section:(NSInteger)aSection
                                                         target:(id)aTarget
                                                         action:(SEL)aAction;

+ (SectionHeaderView *)PIDSectionHeaderView;
+ (SectionHeaderView *)rightSectionHeaderViewWithTitle:(NSString *)aTitle;

- (void)setExpandedState:(BOOL)state;

@property (nonatomic, weak) IBOutlet UILabel        * headerText;
@property NSInteger         section;

@end
