//
//  AlarmSelectionPopup.m
//  tramTracker
//
//  Created by Jonathan Head on 22/07/2015.
//  Copyright (c) 2015 AppsCore. All rights reserved.
//

#import "AlarmSelectionPopup.h"

@interface AlarmSelectionPopup()
@property (weak, nonatomic) IBOutlet UIImageView* firstCheckboxImage;
@property (weak, nonatomic) IBOutlet UIImageView* secondCheckboxImage;
@property (weak, nonatomic) IBOutlet UIImageView* thirdCheckboxImage;
@property (weak, nonatomic) IBOutlet UIButton* firstCheckbox;
@property (weak, nonatomic) IBOutlet UIButton* secondCheckbox;
@property (weak, nonatomic) IBOutlet UIButton* thirdCheckbox;
@property (weak, nonatomic) IBOutlet UILabel* firstTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel* secondTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel* thirdTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel* descriptionLabel;
@property (strong, nonatomic) NSArray* timeOffsets;
- (IBAction)onSelectCheckbox:(UIButton*)sender;
@end

@implementation AlarmSelectionPopup

+ (AlarmSelectionPopup *)popupForPrediction:(Prediction*)prediction stop:(Stop*)stop timeOffsets:(NSArray*)timeOffsets isEstimate:(BOOL)isEstimate
{
    AlarmSelectionPopup* popup = [[NSBundle mainBundle] loadNibNamed:@"AlarmPopup" owner:self options:nil][0];
    popup.timeOffsets = timeOffsets;
    NSDateFormatter* dateFormatter = [NSDateFormatter new];
    dateFormatter.dateStyle = NSDateFormatterNoStyle;
    dateFormatter.timeStyle = NSDateFormatterShortStyle;
    popup.descriptionLabel.text = [NSString stringWithFormat:@"Your tram is predicted to arrive at Stop %@ %@ at %@.\n\nHow long before the %@ arrival time would you like to be alerted?\n\nIf there are disruptions on your route, please check and rely on real-time information.", stop.stopNumber, stop.name, [dateFormatter stringFromDate:prediction.predictedArrivalDateTime], isEstimate ? @"estimated" : @"scheduled"];
//    popup.descriptionLabel.text = [NSString stringWithFormat:@"How long before the tram is scheduled to arrive at Stop %@ %@ would you like to be alerted?", isEstimate ? "estimated" : "scheduled" stop.stopNumber, stop.name];
    [popup layoutIfNeeded];
    return popup;
}

- (void)awakeFromNib
{ [self onSelectCheckbox:self.firstCheckbox]; }

- (void)setTimeOffsets:(NSArray *)timeOffsets
{
    if(timeOffsets.count != 3)
    { return; }
    
    _timeOffsets = timeOffsets;
    self.firstTimeLabel.text  = [NSString stringWithFormat:@"%@ minutes", timeOffsets[0]];
    self.secondTimeLabel.text = [NSString stringWithFormat:@"%@ minutes", timeOffsets[1]];
    self.thirdTimeLabel.text  = [NSString stringWithFormat:@"%@ minutes", timeOffsets[2]];
    
    self.secondsOffset = [self.timeOffsets[0] integerValue] * 60;
}

- (void)onSelectCheckbox:(UIButton *)sender
{
    self.firstCheckboxImage.image  = [UIImage imageNamed:sender == self.firstCheckbox  ? @"icon_radio_active" : @"icon_radio_idle"];
    self.secondCheckboxImage.image = [UIImage imageNamed:sender == self.secondCheckbox ? @"icon_radio_active" : @"icon_radio_idle"];
    self.thirdCheckboxImage.image  = [UIImage imageNamed:sender == self.thirdCheckbox  ? @"icon_radio_active" : @"icon_radio_idle"];
    
    if(sender == self.firstCheckbox)
    { self.secondsOffset = [self.timeOffsets[0] integerValue] * 60; }
    else if(sender == self.secondCheckbox)
    { self.secondsOffset = [self.timeOffsets[1] integerValue] * 60; }
    else if(sender == self.thirdCheckbox)
    { self.secondsOffset = [self.timeOffsets[2] integerValue] * 60; }
}

@end
