//
//  StopInformationCell.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 4/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "StopInformationPOICell.h"
#import "PointOfInterest.h"

@interface StopInformationPOICell()

@property (weak, nonatomic) IBOutlet UILabel    * name;
@property (weak, nonatomic) IBOutlet UILabel    * descriptionLabel;
@property (weak, nonatomic) UIImageView         * backgroundImageView;

@end

@implementation StopInformationPOICell

- (void)configureWithPointOfInterest:(PointOfInterest *)aPOI
{
    UIEdgeInsets insets = UIEdgeInsetsMake(5, 5, 5, 5);
    self.backgroundImageView.image = [[UIImage imageNamed:@"Stop_InformationCell"] resizableImageWithCapInsets:insets];

    self.backgroundView = nil;
    self.backgroundColor = [UIColor colorWithWhite:0.960784314f alpha:1.0f];

    self.name.text = aPOI.name;
    self.descriptionLabel.text = aPOI.poiDescription;
}

@end
