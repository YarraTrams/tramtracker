//
//  FeaturePopup.h
//  tramTracker
//
//  Created by Jonathan Head on 21/08/2015.
//  Copyright (c) 2015 AppsCore. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeaturePopup : UIView<UIScrollViewDelegate, UIWebViewDelegate>
/// The list of feature displays. Each entry should be an NSDictionary containing a unique 'id' field, a 'sortOrder', and a 'url' field. The array is automatically sorted when set into this property.
@property (strong, nonatomic) NSArray* features;
+ (FeaturePopup*)createAndShowPopupOverView:(UIView*)view;
+ (BOOL)shouldShowFeaturePopup:(NSMutableArray*)featureList;
@end
