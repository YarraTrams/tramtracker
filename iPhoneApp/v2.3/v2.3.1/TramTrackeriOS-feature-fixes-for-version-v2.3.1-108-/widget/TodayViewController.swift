//
//  TodayViewController.swift
//  widget
//
//  Created by Jonathan Head on 30/06/2015.
//  Copyright (c) 2015 AppsCore. All rights reserved.
//

import UIKit
import NotificationCenter
import Fabric
import Crashlytics

enum TramTrackerLaunchMode {
    case Home
    case Nearby
    case Favourites
    case PID
}

extension NSDate {
    
    class func dateFromString(dateStr: String) -> NSDate
    {
        let dateFormatter: NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        return dateFormatter.dateFromString(dateStr)!
    }
}

class TodayViewController: UIViewController, NSURLConnectionDataDelegate, NCWidgetProviding {
    @IBOutlet weak var stopName:              UILabel?
    @IBOutlet weak var stopDescription:       UILabel?
    @IBOutlet weak var stopFreeTramZone:      UIImageView?
    
    @IBOutlet weak var firstRouteNumber:         UILabel?
    @IBOutlet weak var firstRouteName:           UILabel?
    @IBOutlet weak var firstRoutePrediction:     UILabel?
    @IBOutlet weak var firstRouteDisruption:     UIImageView?
    @IBOutlet weak var firstRouteServiceUpdate:  UIImageView?
    @IBOutlet weak var firstRouteAircon:         UIImageView?
    @IBOutlet weak var firstRouteLowFloor:       UIImageView?
    
    @IBOutlet weak var secondRouteNumber:        UILabel?
    @IBOutlet weak var secondRouteName:          UILabel?
    @IBOutlet weak var secondRoutePrediction:    UILabel?
    @IBOutlet weak var secondRouteDisruption:    UIImageView?
    @IBOutlet weak var secondRouteServiceUpdate: UIImageView?
    @IBOutlet weak var secondRouteAircon:        UIImageView?
    @IBOutlet weak var secondRouteLowFloor:      UIImageView?
    
    @IBOutlet weak var thirdRouteNumber:         UILabel?
    @IBOutlet weak var thirdRouteName:           UILabel?
    @IBOutlet weak var thirdRoutePrediction:     UILabel?
    @IBOutlet weak var thirdRouteDisruption:     UIImageView?
    @IBOutlet weak var thirdRouteServiceUpdate:  UIImageView?
    @IBOutlet weak var thirdRouteAircon:         UIImageView?
    @IBOutlet weak var thirdRouteLowFloor:       UIImageView?
    @IBOutlet weak var thirdRouteDivider:        UIView?
    
    @IBOutlet weak var routesShown:        UILabel?
    @IBOutlet weak var lastUpdate:         UILabel?
    
    @IBOutlet weak var contentContainer:               UIView?
    @IBOutlet weak var subContentContainer:            UIView?
    @IBOutlet weak var subContentContainerTransparent: UIView?
    
    @IBOutlet weak var errorMessage:        UILabel?
    @IBOutlet weak var loadingContainer:    UIView?
    @IBOutlet var contentConstraintHeight:  NSLayoutConstraint?
    @IBOutlet var contentConstraint:        NSLayoutConstraint?
    @IBOutlet var errorConstraint:          NSLayoutConstraint?
    @IBOutlet var loadingConstraint:        NSLayoutConstraint?
    @IBOutlet var errorTopWidgetConstraint: NSLayoutConstraint?
    @IBOutlet var errorTopTitleConstraint:  NSLayoutConstraint?
    
    var favouritesList: [AnyObject!]?
    let locationManager: CLLocationManager = CLLocationManager()
    var closestTrackerId:  NSNumber?
    var closestLowFloor:   String?
    var closestRoutes:     String?
    
    var isUpdating: Bool = false;
    var launchMode: TramTrackerLaunchMode = .Home
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        Crashlytics.startWithAPIKey("1c523be2ad73a1438832cbb47a77cebdef92e66e")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    deinit
    {
        locationManager.stopUpdatingLocation()
    }
    
    func widgetPerformUpdateWithCompletionHandler(completionHandler: ((NCUpdateResult) -> Void))
    {
        if(isUpdating)
        {
            completionHandler(NCUpdateResult.NoData)
        }
        else
        {
            isUpdating = true
            getFavourites(completionHandler)
        }
    }
    
    func getFavourites(completionHandler: ((NCUpdateResult) -> Void)!)
    {
        let appId:     String   = "TTIOSJSON"
        let defaults = NSUserDefaults(suiteName: "group.au.com.yarratrams.tramtracker")!
        
        let lastUpdateTime: Double = defaults.doubleForKey("lastUpdate")
        
        if lastUpdateTime == 0
        { self.setDisplayLastUpdate(lastUpdateTime) }
        
        if let favouritesData = defaults.arrayForKey("favouriteStopsList")
        {
            if(favouritesData.count <= 0)
            {
                self.setDisplayErrorNoFavourites();
                completionHandler(NCUpdateResult.NewData)
                return
            }
            
            self.favouritesList = []
            var closestFavourite: NSDictionary?
            let currentLocation: CLLocation? = locationManager.location
            var closestDistance: Double = 2000.0
            
            if let location = currentLocation
            {
                for favourite in favouritesData
                {
                    let latitude: NSNumber  = favourite["latitude"]  as! NSNumber
                    let longitude: NSNumber = favourite["longitude"] as! NSNumber
                    let distance = location.distanceFromLocation(CLLocation(latitude: latitude.doubleValue, longitude: longitude.doubleValue))
                    if(distance < closestDistance)
                    {
                        closestDistance = distance
                        closestFavourite = favourite as? NSDictionary
                    }
                }
            
                if let favourite = closestFavourite
                {
                    let trackerId:    NSNumber = favourite["trackerid"]     as! NSNumber
                    let routeNum:     String   = favourite["route"]         as! String
                    let isLowFloor:   Bool     = ((favourite["lowfloor"]    as! Int) != 0)
                    let lowFloor:     String   = (isLowFloor ? "true" : "false")
                    let freeTramZone: Bool     = (favourite["freetramzone"] as! Int) != 0
                    let routeName:    String   = favourite["name"]          as! String
                    let routeSubName: String   = favourite["subname"]       as! String
                    let guid: String
                    if let vendorID = UIDevice.currentDevice().identifierForVendor
                    { guid = vendorID.UUIDString }
                    else
                    { guid = "00000000-0000-0000-0000-000000000000" }
                    self.closestTrackerId = trackerId
                    self.closestLowFloor  = lowFloor
                    self.closestRoutes    = routeNum
                    self.stopName?.text = routeName
                    self.stopDescription?.text = routeSubName
                    self.stopFreeTramZone?.hidden = !freeTramZone
            
                    self.setDisplayShowLoading()
//                    let url: String = "http://qa-json.tramtracker.com.au/TramTrackerWebAPI/api/predictions/widget/\(trackerId)"
                    let url: String = "http://ws3.tramtracker.com.au/TramTrackerWebAPI/api/predictions/widget/\(trackerId)"
                    request(Method.GET, url, parameters: ["authId":appId, "token":guid, "routeNo":routeNum, "lowFloorOnly":lowFloor], encoding: ParameterEncoding.URL)
                        .responseJSON { (data) in
                            
                            if let json = data.result.value as? NSArray
                            {
                                print("json: \(json)")

                                if json.count <= 0
                                {
                                    self.setDisplayErrorNoResults([ "1" ], lowFloor: isLowFloor)
                                    completionHandler(NCUpdateResult.Failed)
                                    return
                                }
                                
                                let currDate: NSDate = NSDate()
                                var routes = Set<String>()
                                
                                if let firstEntry = json[0] as? NSDictionary
                                {
                                    let routeNo = firstEntry["routeNo"] as? NSNumber
                                    self.firstRouteNumber!.text = "\(routeNo!)"
                                    self.firstRouteName!.text   = firstEntry["destination"]   as? String
                                    self.firstRoutePrediction!.text      = self.getMinutesRemainingFromPrediction(firstEntry["arrivalTime"] as! String)
                                    self.firstRouteAircon!.hidden        = (firstEntry["hasAirCon"]       as? Int) == 0
                                    self.firstRouteLowFloor!.hidden      = (firstEntry["isLowFloor"]      as? Int) == 0
                                    self.firstRouteDisruption!.hidden    = (firstEntry["hasDisruption"]   as? Int) == 0
                                    self.firstRouteServiceUpdate!.hidden = (firstEntry["hasSpecialEvent"] as? Int) == 0
                                    routes.insert(routeNo!.stringValue)
                                }
                            
                                if let secondEntry = json[1] as? NSDictionary
                                {
                                    let routeNo = secondEntry["routeNo"] as? NSNumber
                                    self.secondRouteNumber!.text = "\(routeNo!)"
                                    self.secondRouteName!.text   = secondEntry["destination"] as? String
                                    self.secondRoutePrediction!.text      = self.getMinutesRemainingFromPrediction(secondEntry["arrivalTime"] as! String)
                                    self.secondRouteAircon!.hidden        = (secondEntry["hasAirCon"]       as? Int) == 0
                                    self.secondRouteLowFloor!.hidden      = (secondEntry["isLowFloor"]      as? Int) == 0
                                    self.secondRouteDisruption!.hidden    = (secondEntry["hasDisruption"]   as? Int) == 0
                                    self.secondRouteServiceUpdate!.hidden = (secondEntry["hasSpecialEvent"] as? Int) == 0
                                    routes.insert(routeNo!.stringValue)
                                }
                            
                                if json.count > 2
                                {
                                    if let thirdEntry = json[2] as? NSDictionary
                                    {
                                        let routeNo = thirdEntry["routeNo"] as? NSNumber
                                        self.thirdRouteNumber!.text = "\(routeNo!)"
                                        self.thirdRouteName!.text   = thirdEntry["destination"] as? String
                                        self.thirdRoutePrediction!.text      = self.getMinutesRemainingFromPrediction(thirdEntry["arrivalTime"] as! String)
                                        self.thirdRouteAircon!.hidden        = (thirdEntry["hasAirCon"]       as? Int) == 0
                                        self.thirdRouteLowFloor!.hidden      = (thirdEntry["isLowFloor"]      as? Int) == 0
                                        self.thirdRouteDisruption!.hidden    = (thirdEntry["hasDisruption"]   as? Int) == 0
                                        self.thirdRouteServiceUpdate!.hidden = (thirdEntry["hasSpecialEvent"] as? Int) == 0
                                        routes.insert(routeNo!.stringValue)
                                    }
                                }
                                else // only two JSON fields were returned. Hide the third, and resize the widget as required.
                                {
                                    self.thirdRouteNumber!.text          = "-"
                                    self.thirdRouteName!.text            = "-"
                                    self.thirdRoutePrediction!.text      = "-"
                                    self.thirdRouteAircon!.hidden        = true
                                    self.thirdRouteLowFloor!.hidden      = true
                                    self.thirdRouteDisruption!.hidden    = true
                                    self.thirdRouteServiceUpdate!.hidden = true
                                    
                                }
                                
                                self.setDisplayShowFavourites(json.count > 2)
                                
                                self.setDisplayRoutes(routes, lowFloor: isLowFloor)
                                self.setDisplayLastUpdate(NSDate().timeIntervalSince1970)
                                defaults.setDouble(NSDate().timeIntervalSince1970, forKey: "lastUpdate")
                                defaults.synchronize()
                                completionHandler(NCUpdateResult.NewData)
                                return
                            }
                            else
                            {
                                self.setDisplayErrorNoDataConnection()
                                completionHandler(NCUpdateResult.Failed)
                                return
                            }
                    }
                    completionHandler(NCUpdateResult.NewData)
                    return
                }
                else
                {
                    self.setDisplayErrorNearbyFavourites()
                    completionHandler(NCUpdateResult.NewData)
                    return
                }
            }
            else
            {
                self.setDisplayErrorNoLocation()
                completionHandler(NCUpdateResult.NewData)
                return
            }
        }
        else
        { self.setDisplayErrorNoFavourites() }
        completionHandler(NCUpdateResult.Failed)
        return
    }
    
    @IBAction func gotoApp(sender: UIButton) {
        let url: NSURL
        switch self.launchMode {
        case .Home:
            url = NSURL(string: "tramtracker://home")!
        case .Nearby:
            url = NSURL(string: "tramtracker://nearby")!
        case .Favourites:
            url = NSURL(string: "tramtracker://favourites")!
        case .PID:
            url = NSURL(string: "tramtracker://pid?trackerId=\(self.closestTrackerId!)&lowFloor=\(self.closestLowFloor!)&routes=\(self.closestRoutes!)")!
        }
        self.extensionContext?.openURL(url, completionHandler: nil)
    }
    
    func getMinutesRemainingFromPrediction(predictionTimeStr: String) -> String {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let predictionTime: NSTimeInterval = formatter.dateFromString(predictionTimeStr)!.timeIntervalSince1970
        
        let currentTime: NSTimeInterval = NSDate().timeIntervalSince1970
        let timeDelta = predictionTime - currentTime;
        if(timeDelta < -60)
        { return "departed" }
        if(timeDelta < 60)
        { return "now" }
        
        if(predictionTime - currentTime < 3600) // 60 minutes
        {
            let minutes: Int = Int((predictionTime - currentTime) / 60)
            return "\(minutes)"
        }
        else
        {
            let dateFormatter: NSDateFormatter = NSDateFormatter()
            dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
            return dateFormatter.stringFromDate(NSDate(timeIntervalSince1970: predictionTime))
        }
    }
    
    func setDisplayLastUpdate(lastUpdateDate: NSTimeInterval) {
        let dateFormatter: NSDateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
        dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
        
        let date: NSDate = NSDate(timeIntervalSince1970: lastUpdateDate)
        let dateString: String = dateFormatter.stringFromDate(date)
        self.lastUpdate!.text = "Last update: \(dateString)"
    }
    
    func setDisplayRoutes(routes: Set<String>, lowFloor: Bool) {
        let tramType: String = lowFloor ? "low floor" : "all"
        
        
        let routeArray = Array(routes).sort( { (firstObj: String, secondObj: String) -> Bool in
            return firstObj.localizedStandardCompare(secondObj) == NSComparisonResult.OrderedAscending
        })
        
        if(routes.count == 1)
        { self.routesShown!.text = "Showing \(tramType) trams on route \(routes.first!)" }
        else if(routes.count > 1)
        {
            var message = "Showing \(tramType) trams on routes "
            for str in routeArray
            { message += "\(str), " }
            self.routesShown!.text = message.substringToIndex(message.endIndex.predecessor().predecessor())
        }
    }
    
    func setDisplayErrorNoResults(routes: Set<String>, lowFloor: Bool) {
        let tramType: String = lowFloor ? "low floor " : ""

        self.setDisplayErrorWithStopDetails("There may be no \(tramType)trams on your route")
        self.launchMode = .PID
    }
    
    func setDisplayErrorNoDataConnection() {
        self.setDisplayError("tramTRACKER was unable to get predictions.\n\nPlease check your data connection.")
        self.launchMode = .Home
    }
    
    func setDisplayErrorNoLocation() {
        self.setDisplayError("tramTRACKER was unable to get your location.\n\nTap here to open tramTRACKER.")
        self.launchMode = .Home
    }
    
    func setDisplayErrorNearbyFavourites() {
        self.setDisplayError("You have no favourites within 15 minutes walking distance.\n\nTap here to open tramTRACKER to view nearby stops.")
        self.launchMode = .Nearby
    }
    
    func setDisplayErrorNoFavourites() {
        self.setDisplayError("You currently have no favourites saved.\n\nTap here to open tramTRACKER to set up your favourites.")
        self.launchMode = .Nearby
    }
    
    func setDisplayErrorWithStopDetails(message: String) {
        self.zeroConstraints()
        self.errorMessage!.text = message
        
        self.contentConstraint!.active        = false
        self.loadingConstraint!.active        = false
        self.errorConstraint!.active          = true
        self.errorTopWidgetConstraint!.active = false
        self.errorTopTitleConstraint!.active  = true;
        UIView.animateWithDuration(0.7, animations: { () -> Void in
            self.contentContainer!.alpha               = 1.0
            self.subContentContainer!.alpha            = 0.0
            self.subContentContainerTransparent!.alpha = 0.0
            self.loadingContainer!.alpha               = 0.0
            self.errorMessage!.alpha                   = 1.0
        })
    }
    
    func setDisplayError(message: String) {
        self.zeroConstraints()
        self.errorMessage!.text = message
        
        self.contentConstraint!.active        = false
        self.loadingConstraint!.active        = false
        self.errorConstraint!.active          = true
        self.errorTopWidgetConstraint!.active = true
        self.errorTopTitleConstraint!.active  = false
        UIView.animateWithDuration(0.7, animations: { () -> Void in
            self.contentContainer!.alpha               = 0.0
            self.subContentContainer!.alpha            = 0.0
            self.subContentContainerTransparent!.alpha = 0.0
            self.loadingContainer!.alpha               = 0.0
            self.errorMessage!.alpha                   = 1.0
            
            self.view.layoutIfNeeded()
        })
    }
    
    func setDisplayShowLoading() {
        self.zeroConstraints()
        self.contentConstraint!.active = false
        self.loadingConstraint!.active = true
        self.errorConstraint!.active   = false
        self.errorTopWidgetConstraint!.active = true
        self.errorTopTitleConstraint!.active = false
        self.launchMode = .Home
        
        UIView.animateWithDuration(0.7, animations: { () -> Void in
            self.contentContainer!.alpha               = 0.0
            self.subContentContainer!.alpha            = 0.0
            self.subContentContainerTransparent!.alpha = 0.0
            self.loadingContainer!.alpha               = 1.0
            self.errorMessage!.alpha                   = 0.0
            
            self.view.layoutIfNeeded()
        })
    }
    
    func setDisplayShowFavourites(showAllWidgets: Bool) {
        self.zeroConstraints()
        self.contentConstraint!.active = true
        self.loadingConstraint!.active = false
        self.errorConstraint!.active   = false
        self.errorTopWidgetConstraint!.active = true
        self.errorTopTitleConstraint!.active = false
        self.contentConstraintHeight!.constant = showAllWidgets ? 265 : 212
        self.launchMode = .PID
        
        UIView.animateWithDuration(0.7, animations: { () -> Void in
            self.contentContainer!.alpha               = 1.0
            self.subContentContainer!.alpha            = 1.0
            self.subContentContainerTransparent!.alpha = 1.0
            self.loadingContainer!.alpha               = 0.0
            self.errorMessage!.alpha                   = 0.0
            
            self.thirdRouteName!.alpha       = showAllWidgets ? 1.0 : 0.0
            self.thirdRouteNumber!.alpha     = showAllWidgets ? 1.0 : 0.0
            self.thirdRoutePrediction!.alpha = showAllWidgets ? 1.0 : 0.0
            self.thirdRouteDivider!.alpha    = showAllWidgets ? 1.0 : 0.0
            
            self.view.layoutIfNeeded()
        })
    }
    
    func zeroConstraints() {
        self.contentConstraint!.constant = 0
        self.loadingConstraint!.constant = 0
        self.errorConstraint!.constant   = 0
        self.errorTopTitleConstraint!.constant = -10
        self.errorTopWidgetConstraint!.constant = 0
    }

    func widgetMarginInsetsForProposedMarginInsets(defaultMarginInsets: UIEdgeInsets) -> (UIEdgeInsets)
    { return UIEdgeInsets(top: 0, left: defaultMarginInsets.left, bottom: 0, right: 0); }
    
}
