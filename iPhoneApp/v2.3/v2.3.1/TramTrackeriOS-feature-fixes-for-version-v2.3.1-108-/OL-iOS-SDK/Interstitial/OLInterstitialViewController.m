//
//  OLInterstitialViewController.m
//  OtherLevels
//
//  Copyright (c) 2015 OtherLevels. All rights reserved.
//

#import "OLInterstitialViewController.h"

@interface OLInterstitialViewController()

@property (nonatomic, assign) IBOutlet UIWebView *webView;

- (IBAction)dismiss:(id)sender;

@end

@implementation OLInterstitialViewController

+ (void)getInterstitial:(NSString *)placement {
    [OtherLevels getInterstitialForPlacement:placement
                         presentInterstitial:[[OLInterstitialViewController alloc] init]
                                    animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.webView.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self hideViewUntilFinishedLoading];
    
    self.isInterstitialLinkClicked = NO;
    
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:path];

    self.webView.scrollView.scrollEnabled = NO;
    [self.webView loadHTMLString:self.htmlContent baseURL:baseURL];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void)hideViewUntilFinishedLoading {
    self.isInterstitialContentLoaded = NO;
}

- (IBAction)dismiss:(id)sender {
    [[UIApplication sharedApplication] setStatusBarHidden:NO];

    if (self.parentViewController) {
        [self willMoveToParentViewController:nil];
        [self viewWillDisappear:YES];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
        [self viewDidDisappear:YES];
        self.view = nil;
    } else {
        __block __weak OLInterstitialViewController *weakSelf = self;
        [self dismissViewControllerAnimated:YES completion:^{
            weakSelf.view = nil;
        }];
    }
}

#pragma mark - UIWebView Delegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    BOOL isShouldStartLoad = YES;
    
    if ([request.URL.absoluteString isEqualToString:@"otherlevels://in-app.purchase"]) {
        self.isInterstitialLinkClicked = YES;
        isShouldStartLoad = NO;
//        [self performSegueWithIdentifier:@"showInAppPurchaseScreen" sender:self];
        
    } else if (UIWebViewNavigationTypeLinkClicked==navigationType) {
        
        [OtherLevels registerEvent:@"Click" label:request.URL.absoluteString phash:self.pHash];
        
        if (!self.isInterstitialLinkClicked) {
            self.isInterstitialLinkClicked = YES;
            [self dismiss:self];
        }
    }
    
    return isShouldStartLoad;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self interstitialFinishedLoading];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    NSLog(@"Interstitial content failed to load: %@", error.description);
    
    [self interstitialFinishedLoading];
}

- (void)interstitialFinishedLoading {
    self.isInterstitialContentLoaded = YES;
}

@end
