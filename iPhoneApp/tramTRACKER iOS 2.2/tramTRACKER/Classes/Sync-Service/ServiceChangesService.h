//
//  ServiceChangesService.h
//  tramTRACKER
//
//  Created by Robert Amos on 20/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const ServiceChangesURI;
extern NSInteger const ServiceChangesServiceErrorNotReachable;

@interface ServiceChangesService : NSObject {
	id __weak delegate;
	SEL action;
}

@property (nonatomic, weak) id delegate;
@property (nonatomic, assign) SEL action;

- (id)initWithDelegate:(id)aDelegate action:(SEL)anAction;

// Will call the delegate + action with the service list
- (void)getServiceChanges;
- (void)getServiceChangesInBackgroundThread;

- (void)failWithError:(NSError *)error;
- (BOOL)hasAccessToService;

@end
