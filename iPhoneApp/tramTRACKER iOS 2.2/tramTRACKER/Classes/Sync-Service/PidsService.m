
//
//  PidsService.m
//  tramTRACKER
//
//  Created by Robert Amos on 18/02/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "PidsService.h"
#import <SystemConfiguration/SCNetworkReachability.h>
#import "Route.h"
#import "PidsServiceDelegate.h"
#import "Stop.h"
#import "UIDevice-Hardware.h"

NSInteger const PIDServiceErrorNotReachable = 1001;
NSInteger const PIDServiceValidationError = 1002;
NSInteger const PIDServiceValidationErrorStopNotFound = 1003;
NSInteger const PIDServiceValidationErrorRouteNotFound = 1004;
NSInteger const PIDServiceValidationErrorOutletNotFound = 1004;
NSInteger const PIDServiceErrorOnboardNotReachable = 1005;
NSInteger const PIDServiceErrorTimeoutReached = 1006;

PIDServiceActionType const PIDServiceActionTypeUpdate = 1;
PIDServiceActionType const PIDServiceActionTypeDelete = 2;

NSString * const kAppID = @"TTIOSJSON";

//Returns information for a stop - /GetStopsAndRoutesUpdatesSince/{lastUpdateDate}/?aid={aid}&tkn={tkn}
NSString * const kGetStopsAndRoutesUpdatesSinceURLFormat = @"GetStopsAndRoutesUpdatesSince/%@/?aid=%@&tkn=%@";

//Returns information for a POI - /GetPointsOfInterestChangesSince/{lastUpdateDate}/?aid={aid}&tkn={tkn}
NSString * const kGetPOIUpdatesSinceURLFormat = @"GetPointsOfInterestChangesSince/%@/?aid=%@&tkn=%@";

//Returns information for a POI - /GetTicketOutletChangesSince/{lastUpdateDate}/?aid={aid}&tkn={tkn}
NSString * const kGetOutletsUpdatesSinceURLFormat = @"GetTicketOutletChangesSince/%@/?aid=%@&tkn=%@";

NSString * const kGetMapsUpdates = @"GetLatestNetworkMaps/?aid=%@&tkn=%@";

//Returns a list of stops for a given route number and direction
// /GetListOfStopsByRouteNoAndDirection/{routeNo}/{isUpDirection}/?aid={aid}&tkn={tkn}
NSString * const kGetListOfStopsByRouteNoAndDirectionURLFormat = @"GetListOfStopsByRouteNoAndDirection/%@/%@/?aid=%@&tkn=%@";

//Return the main (all) routes for a stop - /GetMainRoutesForStop/{stopNo}/?aid={aid}&tkn={tkn}
NSString * const kGetMainRoutesForStopURLFormat = @"GetMainRoutesForStop/%@/?aid=%@&tkn=%@";

NSString * const kGetMainPOIsForStopURLFormat = @"GetPointsOfInterestByStopNo/%@/?aid=%@&tkn=%@";

//Return the destinations for a route - /GetDestinationsForRoute/{routeId}/?aid={aid}&tkn={tkn}
NSString * const kGetDestinationsForRouteURLFormat = @"GetDestinationsForRoute/%@/?aid=%@&tkn=%@";

//Return the information for a stop - /GetStopInformation/{stopNo}/?aid={aid}&tkn={tkn}
NSString * const kGetStopInformationURLFormat = @"GetStopInformation/%@/?aid=%@&tkn=%@";

//Return the information for a stop - /GetStopInformation/{stopNo}/?aid={aid}&tkn={tkn}
NSString * const kGetRetailerInformationURLFormat = @"GetTicketOutletById/%@/?aid=%@&tkn=%@";

NSString * const kGetPOIInformationURLFormat = @"GetPointOfInterestById/%@/?aid=%@&tkn=%@";

NSString * const kGetStopsForPOIURLFormat = @"GetStopsByPointOfInterestId/%@/?aid=%@&tkn=%@";

// Returns predictions for a stop and route - /GetNextPredictedRoutesCollection/{stopNo}/{routeNo}/{lowFloor}/?aid={aid}&cid={cid}&tkn={tkn}
NSString * const kGetNextPredictedRoutesCollectionURLFormat = @"GetNextPredictedRoutesCollection/%@/%@/%@/?aid=%@&cid=2&tkn=%@";

NSString * const kGetNextPredictedArrivalTmeAtStopsForTramNoURLFormat = @"GetNextPredictedArrivalTimeAtStopsForTramNo/%ld/?aid=%@&tkn=%@";

NSString * const kGetDeviceTokenURLFormat = @"%@/GetDeviceToken/?aid=%@&devInfo=%@";

@interface PidsService()

@property (nonatomic, assign) BOOL backgroundedSelf;
@property (nonatomic, assign, getter = isRealtimeRequest) BOOL realtimeRequest;
//@property (strong, nonatomic) PidsServiceDelegate * serviceDelegate;
@property (strong, nonatomic) NSURLConnection * connection;

@property (strong, nonatomic) NSMutableArray * connections;
@property (strong, nonatomic) NSMutableArray * delegates;

@end

@implementation PidsService

//
// Get or set the Guid
//

+ (NSString *)baseURL
{
//    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"devAPI"]) {
//        return @"http://extranetdev.yarratrams.com.au/PIDSServiceWCF/restservice/";
//    } else {
        return @"http://ws3.tramtracker.com.au/TramTracker/restservice/";
//    }
}

- (id)init
{
    if (self = [super init])
    {
        self.backgroundedSelf = NO;
        
        self.delegates = [NSMutableArray new];
        self.connections = [NSMutableArray new];

        NSUUID * vendorID = [[UIDevice currentDevice] identifierForVendor];
        
        if (vendorID)
            [self setGuid:[vendorID UUIDString]];
        else
            [self setGuid:@"00000000-0000-0000-0000-000000000000"];
    }
	return self;
}

- (instancetype)initWithDelegate:(id)aDelegate
{
    if (self = [self init])
    {
        self.delegate = aDelegate;
    }
    return self;
}

//
// Clear the delegate. Set the delegate with -setDelegate (as part of the @synthesize)
// The selector you need to implement depends on the method you're calling
//
- (void)clearDelegate
{
	[self setDelegate:nil];
}

//
// Get Information about a Stop
// Your delegate must implement -setInformation:(Stop *)stop forStop:(NSNumber *)trackerID;
//
- (void)getInformationForStop:(int)stopID
{
	// make sure that we have a Guid set.
	NSAssert([self guid], @"Unable to get information for stop as no GUID is set.");
	
	// Keep the tracker ID around for later
	[self setTrackerID:[NSNumber numberWithInt:stopID]];
	
	NSMutableString *url = [NSMutableString stringWithString:[PidsService baseURL]];
	
	//Return the information for a stop - /GetStopInformation/{stopNo}/?aid={aid}&tkn={tkn}
	[url appendFormat:kGetStopInformationURLFormat, [[NSNumber numberWithInt:stopID] stringValue], kAppID, self.guid];
	
	[self callRESTServiceWithURL:[NSString stringWithString:url]];
}

- (void)getInformationForRetailer:(int)retailerID
{
    self.retailerID = @(retailerID);
    
	NSMutableString *url = [NSMutableString stringWithString:[PidsService baseURL]];
	[url appendFormat:kGetRetailerInformationURLFormat, [self.retailerID stringValue], kAppID, self.guid];
	[self callRESTServiceWithURL:[NSString stringWithString:url]];
}

- (void)getInformationForPOI:(int)poiID
{
    self.poiID = @(poiID);

	NSMutableString *url = [NSMutableString stringWithString:[PidsService baseURL]];
    
	[url appendFormat:kGetPOIInformationURLFormat, [self.poiID stringValue], kAppID, self.guid];
	[self callRESTServiceWithURL:[NSString stringWithString:url]];
    
    
    url = [NSMutableString stringWithString:[PidsService baseURL]];
    
    [url appendFormat:kGetStopsForPOIURLFormat, @(poiID).stringValue, kAppID, self.guid];
    
    [self callRESTServiceWithURL:url];
}

//
// Get Predicted Arrivals for a Stop
// Your delegate must implement -setPredictions:(NSArray *)predictions forStop:(NSNumber *)trackerID;
//
- (void)getPredictionsForStop:(int)stopID route:(Route *)route lowFloorOnly:(BOOL)lowFloorOnly
{
	NSAssert([self guid], @"Unable to get predictions for stop as no GUID is set.");
	self.realtimeRequest = YES;

	[self setTrackerID:[NSNumber numberWithInt:stopID]];

	NSMutableString *url = [NSMutableString stringWithString:[PidsService baseURL]];
	
	[url appendFormat:kGetNextPredictedRoutesCollectionURLFormat, [[NSNumber numberWithInt:stopID] stringValue], route == nil ? @"0" : [@(route.internalNumber.integerValue) stringValue], lowFloorOnly ? @"true" : @"false", kAppID, self.guid];

	[self callRESTServiceWithURL:[NSString stringWithString:url]];
}

- (void)getPredictionsForStop:(int)stopID
{
	[self getPredictionsForStop:stopID route:nil lowFloorOnly:NO];
}

//
// Get scheduled departures for a stop
// Your delegate must implement -setDepartures:(NSArray *)departures forStop:(NSNumber *)trackerID;
// 
- (void)getScheduledDeparturesForStop:(int)stopID routeNumber:(NSString *)aRouteNumber atDateTime:(NSDate *)date withLowFloorOnly:(BOOL)lowFloorOnly
{
	// make sure that we have a Guid set.
	NSAssert([self guid], @"Unable to get scheduled departures for stop as no Guid is set.");
	
    [self setTrackerID:@(stopID)];

    NSMutableString * url = [[PidsService baseURL] mutableCopy];

    [url appendFormat:@"GetSchedulesCollection/%@/%@/%@/%@/?aid=%@&tkn=%@", [@(stopID) stringValue], aRouteNumber, lowFloorOnly ? @"true" : @"false", [self formatDateForService:date], kAppID, self.guid];

	// call the web service
	[self callRESTServiceWithURL:url];
}

//
// Get the Route List
//
- (void)getRouteList
{
	// make sure that we have a Guid set.
	NSAssert([self guid], @"Unable to get route list as no GUID is set.");
	
    NSAssert(NO, @"getRouteList not defined");

	// No parameters needed
//	[self callWebServiceMethod:@"GetDestinationsForAllRoutes"];
}

//
// Get a list of destinations for a specific route
//
- (void)destinationsForRoute:(Route *)route
{
	// make sure that we have a Guid set.
	NSAssert([self guid], @"Unable to get stop list as no GUID is set.");
	
	[self setRouteNumber:route.internalNumber];
	
	NSMutableString *url = [NSMutableString stringWithString:[PidsService baseURL]];
	
	//Return the destinations for a route - /GetDestinationsForRoute/{routeId}/?aid={aid}&tkn={tkn}
	[url appendFormat:kGetDestinationsForRouteURLFormat, route.internalNumber, kAppID, self.guid];
	
	[self callRESTServiceWithURL:[NSString stringWithString:url]];
}

//
// Get the list of stops for a specific route and direction
// Your delegate should implement -setStopList:(NSArray *)stops forRouteNumber:(NSString *)routeNumber;
//
- (void)getStopListForRoute:(Route *)route upDirection:(BOOL)isUpDirection
{
	// make sure that we have a Guid set.
	NSAssert([self guid], @"Unable to get stop list as no GUID is set.");

	NSMutableString *url = [NSMutableString stringWithString:[PidsService baseURL]];
	
	[url appendFormat:kGetListOfStopsByRouteNoAndDirectionURLFormat, route.internalNumber, isUpDirection ? @"true" : @"false", kAppID, self.guid];
	
	[self setRouteNumber:route.internalNumber];
	[self setUpDirection:[NSNumber numberWithBool:isUpDirection]];
	
	[self callRESTServiceWithURL:[NSString stringWithString:url]];
}

//
// Get info on a journey for a tram number
//
- (void)getJourneyForTramNumber:(NSNumber *)tramNumber
{
	NSAssert([self guid], @"Unable to get journey as no Guid is set.");
	self.realtimeRequest = YES;

//	NSDictionary *params = [NSDictionary dictionaryWithObject:tramNumber forKey:@"tramNo"];
//	[self callWebServiceMethod:@"GetNextPredictedArrivalTimeAtStopsForTramNo" withParameters:params];

    NSMutableString * url = [NSMutableString stringWithString:[PidsService baseURL]];

    [url appendFormat:kGetNextPredictedArrivalTmeAtStopsForTramNoURLFormat, (long)tramNumber.integerValue, kAppID, self.guid];
    [self callRESTServiceWithURL:url];
}

//
// Get a list of updates since date and time
//
- (void)updatesSinceDate:(NSDate *)date
{
	NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];

	[dateFormatter setDateFormat:@"YYYY-MM-dd"];
	NSString        * dateString = [dateFormatter stringFromDate:date];
	NSMutableString * url = [NSMutableString stringWithString:[PidsService baseURL]];

	//Returns information for a stop - /GetStopsAndRoutesUpdatesSince/{lastUpdateDate}/?aid={aid}&tkn={tkn}

	[url appendFormat:kGetStopsAndRoutesUpdatesSinceURLFormat, dateString, kAppID, self.guid];

    NSMutableString * poiUrl = [[PidsService baseURL] mutableCopy];
    [poiUrl appendFormat:kGetPOIUpdatesSinceURLFormat, dateString, kAppID, self.guid];
    
    NSMutableString * outletsUrl = [[PidsService baseURL] mutableCopy];
    
    [outletsUrl appendFormat:kGetOutletsUpdatesSinceURLFormat, dateString, kAppID, self.guid];

    
    NSMutableString * mapsUrl = [[PidsService baseURL] mutableCopy];
    
    [mapsUrl appendFormat:kGetMapsUpdates, kAppID, self.guid];

	[self callRESTServiceWithURL:url];
    [self callRESTServiceWithURL:poiUrl];
    [self callRESTServiceWithURL:outletsUrl];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self callRESTServiceWithURL:mapsUrl];
    });
}

//
// Get the number of routes through a stop
//
- (void)routesThroughStop:(Stop *)stop
{
	// if it is a stop that is a terminus (end terminus), dont bother the server won't return anything
	if ([stop.trackerID integerValue] >= 8000)
	{
		[self failWithError:[NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
												code:PIDServiceValidationErrorStopNotFound
											userInfo:nil]];
		return;
	}

	// Create the parameters
//	NSDictionary *params = [NSDictionary dictionaryWithObject:stop.trackerID forKey:@"stopNo"];
//	[self setTrackerID:stop.trackerID];
//	
//	// call the thingy
//	[self callWebServiceMethod:@"GetMainRoutesForStop" withParameters:params];
	
	[self setTrackerID:stop.trackerID];
	
	NSMutableString *url = [NSMutableString stringWithString:[PidsService baseURL]];
	
	[url appendFormat:kGetMainRoutesForStopURLFormat, [stop.trackerID stringValue], kAppID, self.guid];
	
	[self callRESTServiceWithURL:[NSString stringWithString:url]];
}

- (void)POIsThroughStop:(Stop *)aStop
{
    NSMutableString * url = [NSMutableString stringWithString:[PidsService baseURL]];
    
    [url appendFormat:kGetMainPOIsForStopURLFormat, aStop.trackerID.stringValue, kAppID, self.guid];

    [self callRESTServiceWithURL:url];
}



- (void)callRESTServiceWithURL:(NSString *)url
{
	// Make sure all requests aren't performed on the main thread
	if ([NSThread isMainThread])
	{
		self.backgroundedSelf = YES;

		[self performSelectorInBackground:@selector(callRESTServiceWithURL:) withObject:url];
		return;
	}

    NSLog(@"Calling: %@", url);
    
	//Autorelease pool because we're in the background and apple non-arc code can use autorelease,
	//and without an autorelease pool they will leak
	@autoreleasepool {
		// Make sure that we have access to the interwebs
		if (![self hasAccessToService])
			return;

		NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];

        request.timeoutInterval = 60;

		// We POST the data
		[request setHTTPMethod:@"GET"];

		//NSLog(@"[PIDSSERVICE] Calling REST API with URL: %@", url);

        PidsServiceDelegate * delegate = [[PidsServiceDelegate alloc] initWithDelegate:[self delegate]];
        
		[self.delegates addObject:delegate];

		// if we have a trackerID pass it through to the delegate
		if (self.trackerID != nil)
			[delegate setTrackerID:self.trackerID];
		if (self.routeNumber != nil)
			[delegate setRouteNumber:self.routeNumber];
		if (self.upDirection != nil)
			[delegate setUpDirection:self.upDirection];

        [delegate manageTimer];

		// Set the request date and time
		[delegate setRequestTime:[NSDate date]];

		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

		[self.connections addObject:[NSURLConnection connectionWithRequest:request delegate:delegate]];

		//What is this for? it blocks for 15 seconds
		if (self.backgroundedSelf)
		{
			NSRunLoop *runLoop = [NSRunLoop currentRunLoop];

			[runLoop runUntilDate:[NSDate dateWithTimeIntervalSinceNow:60]];
			CFRunLoopStop([runLoop getCFRunLoop]);
		}
	}
}

- (void)cancel
{
    for (NSURLConnection * connection in self.connections) {
        [connection cancel];
    }
    
    for (PidsServiceDelegate * delegate in self.delegates) {
        [delegate cancel];
    }
    
    self.connections = [NSMutableArray new];
    self.delegates = [NSMutableArray new];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

- (void)dealloc
{
    [self cancel];
}

//
// Format a date for the service
//
- (NSString *)formatDateForService:(NSDate *)date
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:00ZZZ"];
	NSMutableString *dateString = [NSMutableString stringWithString:[formatter stringFromDate:date]];
	[dateString insertString:@":" atIndex:22];
	return dateString;
}

#pragma mark -
#pragma mark Checking service availablility and error handling

- (void)failWithError:(NSError *)error
{
	// check the delegate to see if we can report the error
	if ([self.delegate respondsToSelector:@selector(pidsServiceDidFailWithError:)])
	{
		[self.delegate performSelectorOnMainThread:@selector(pidsServiceDidFailWithError:) withObject:error waitUntilDone:NO];
	}
}

// Check for the availability of the service
- (BOOL)hasAccessToService
{
	NSURL *host = [NSURL URLWithString:[[self class] baseURL]];
	SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(NULL, [[host host] UTF8String]);
	SCNetworkReachabilityFlags flags;
	SCNetworkReachabilityGetFlags(reachability, &flags);

	BOOL isReachable = flags & kSCNetworkReachabilityFlagsReachable;
	
	// close the reachability ref
	CFRelease(reachability);
	
	if (isReachable)
		return YES;

	// is the service not reachable and a connection is required
	NSString *errorMessage = [NSString stringWithFormat:NSLocalizedString(@"errors-nonetwork", @"No internet error"), (self.isRealtimeRequest ? NSLocalizedString(@"errors-snippet-realtime", @"Real Time") : NSLocalizedString(@"errors-snippet-scheduled", @"Scheduled")), [[UIDevice currentDevice] model]];
	NSString *recoverySuggestion = [NSString stringWithFormat:NSLocalizedString(@"errors-nonetwork-suggestion", @"No internet suggestion"), [[UIDevice currentDevice] model]];
	NSMutableDictionary *userInfo = [NSMutableDictionary new];
	[userInfo setObject:errorMessage forKey:NSLocalizedDescriptionKey];
	[userInfo setObject:recoverySuggestion forKey:NSLocalizedRecoverySuggestionErrorKey];
	NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier] code:PIDServiceErrorNotReachable userInfo:[NSDictionary dictionaryWithDictionary:userInfo]];
	[self failWithError:error];
	return NO;
}

//
// Clean Up
//
@end
