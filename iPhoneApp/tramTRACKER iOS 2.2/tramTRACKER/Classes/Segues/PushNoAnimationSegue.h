//
//  PushNoAnimationSegue.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 6/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PushNoAnimationSegue : UIStoryboardSegue

@end
