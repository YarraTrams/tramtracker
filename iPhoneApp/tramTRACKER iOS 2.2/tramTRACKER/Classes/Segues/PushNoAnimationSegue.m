//
//  PushNoAnimationSegue.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 6/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "PushNoAnimationSegue.h"

@implementation PushNoAnimationSegue

- (void)perform
{
    [[[self sourceViewController] navigationController] pushViewController:[self destinationViewController] animated:NO];
}

@end