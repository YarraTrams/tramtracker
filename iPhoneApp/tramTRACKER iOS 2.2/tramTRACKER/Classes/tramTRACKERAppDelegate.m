//
//  tramTRACKERAppDelegate.m
//  tramTRACKER
//
//  Created by Robert Amos on 17/02/09.
//  Copyright Yarra Trams 2009. All rights reserved.
//

#import "tramTRACKERAppDelegate.h"

#import "Constants.h"
#import "Filter.h"
#import "BackgroundSynchroniser.h"
#import "AutomaticSynchroniserDataLogger.h"
#import "Tram.h"
#import "SWRevealViewController.h"
#import "DynamicSplashView.h"
#import "TicketRetailer.h"
#import <Crashlytics/Crashlytics.h>
#import "RouteList.h"
#import "Settings.h"
#import "Analytics.h"
#import "UIAlertView+Blocks.h"
#import "StopList.h"
#import "FTZOverlay.h"
#import "PointOfInterest.h"

@interface tramTRACKERAppDelegate()<BackgroundSynchroniserDelegate>

@property (strong, nonatomic) BackgroundSynchroniser * syncManager;

@end

@implementation tramTRACKERAppDelegate

//static NSTimeInterval const TTDatabaseModificationDate = 1407478360; // V2.0
//static NSTimeInterval const TTDatabaseModificationDate = 1246492800; // 2 July 09
static NSTimeInterval const TTDatabaseModificationDate = 1421289742; // NEW

- (void)checkForUpdates
{
    NSDate  * lastSyncDate = [[NSUserDefaults standardUserDefaults] objectForKey:kLastSynchronisationDate];
    
    if (!lastSyncDate)
        lastSyncDate = [NSDate dateWithTimeIntervalSince1970:TTDatabaseModificationDate];

    self.syncManager = [[BackgroundSynchroniser alloc] initWithLastSynchronisationDate:lastSyncDate
                                                            persistentStoreCoordinator:self.persistentStoreCoordinator];

    self.syncManager.delegate = self;
	[self.syncManager performSelectorInBackground:@selector(checkForUpdatesInBackgroundThread:) withObject:nil];
}

- (void)askForAnalytics
{
    static NSString * const kAnalyticsAskAnalyticsKey = @"askForAnalytics";
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:kAnalyticsAskAnalyticsKey])
    {
        [UIAlertView showWithTitle:@"The tramTRACKER team would like to capture data analytics"
                           message:@"To improve your tramTRACKER experience, we would like to anonymously understand how you navigate through the app. Access can be turned on/off in the app Settings."
                             style:UIAlertViewStyleDefault
                 cancelButtonTitle:@"Don't Allow"
                 otherButtonTitles:@[@"OK"]
                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex)
         {
             [[NSUserDefaults standardUserDefaults] setBool:buttonIndex forKey:kAnalyticsAllowAnalyticsKey];
         }];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kAnalyticsAskAnalyticsKey];
    }
}

- (void)cancelUpdate
{
    [self.syncManager cancel];
    self.syncManager = nil;
}

- (void)checkForUpdatesDidStart
{
    self.isUpdating = YES;
}

- (void)syncDidFailWithError:(NSError *)error
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationHasFailedUpdating object:error];
}

- (void)checkForUpdatesDidFinish:(NSDictionary *)updatesFound
{
    self.updatesFound = updatesFound;
    
    self.syncManager = nil;
    self.isUpdating = NO;

    NSUInteger          numberOfStopsToBeUpdated = [updatesFound[TTSyncKeyStops] count];
    NSUInteger          numberOfRoutesToBeUpdated = [updatesFound[TTSyncKeyRoutes] count];
    NSUInteger          numberOfTicketsToBeUpdated = [updatesFound[TTSyncKeyTickets] count];
    NSUInteger          numberOfPOIsToBeUpdated = [updatesFound[TTSyncKeyPOIs] count];
    
    NSNumber            * numberOfUpdates = @(numberOfStopsToBeUpdated + numberOfRoutesToBeUpdated + numberOfTicketsToBeUpdated + numberOfPOIsToBeUpdated);
    
    NSString            * numberOfUpdatesString = [numberOfUpdates integerValue] ? [numberOfUpdates stringValue] : nil;

    UITabBarController  * mainTabBarController = (id)[(SWRevealViewController *)self.window.rootViewController frontViewController];

    [[mainTabBarController.viewControllers.lastObject tabBarItem] setBadgeValue:numberOfUpdatesString];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationHasFinishedUpdating object:numberOfUpdates];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Settings convert];
    [Crashlytics startWithAPIKey:@"1c523be2ad73a1438832cbb47a77cebdef92e66e"];
    if (![[StopList sharedManager] hasUpgradedFavouritesTo20]) {
        [[StopList sharedManager] upgradeFavouritesTo20];
    }
    
    [self askForAnalytics];
    [[Analytics sharedInstance] appHasLaunched];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
	[self registerDefaults];
    [self configureAppearance];
    [self persistentStoreCoordinator];
    if ([Settings synchronise])
        [self checkForUpdates];
    
    // Adding FTZ Overlay

    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"FTZ"]) {
        [self.window.rootViewController.view addSubview:[FTZOverlay overlay]];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"FTZ"];
    }

    DynamicSplashView   * splashView = [DynamicSplashView splashWithFrame:self.window.frame];
    
    [self.window.rootViewController.view addSubview:splashView];
    
    [self.window.rootViewController.view bringSubviewToFront:splashView];
    [UIView animateWithDuration:0.5f
                          delay:2.0f
                        options:UIViewAnimationOptionTransitionNone
                     animations:^{
                         splashView.alpha = .0f;
                     } completion:^(BOOL finished) {
                         if (finished) {
                             [splashView removeFromSuperview];
                         }
                     }];
    return YES;
}

- (void)configureAppearance
{
    UIColor * greenColor = [UIColor colorWithRed:105.0f/255.0f green:190.0f/255.0f blue:40.0f/255.0f alpha:1.0f];
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        /// Navigation barD
        [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
        [[UINavigationBar appearance] setBackgroundColor:greenColor];
        
        [[UINavigationBar appearance] setTintColor:[UIColor redColor]];
        [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
        [[UIBarButtonItem appearance] setBackgroundImage:[[UIImage alloc] init] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        
        /// tabbar
        
        UIColor * darkColor = [UIColor colorWithRed:32.0f/256.0f green:33.0f/256.0f blue:31.0f/256.0f alpha:1.0f];
        [[UITabBar appearance] setBackgroundColor:darkColor];
        [[UITabBar appearance] setBackgroundImage:[[UIImage alloc] init]];
        [[UITabBar appearance] setSelectionIndicatorImage:[[UIImage alloc] init]];
        [[UITabBar appearance] setShadowImage:[[UIImage alloc] init]];
        [[UITabBar appearance] setTintColor:[UIColor grayColor]];
        
        /// SearchBar
        [[UISearchBar appearance] setTintColor:[UIColor lightGrayColor]];
    }
    [[UITabBar appearance] setSelectedImageTintColor:greenColor];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return NO;
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    return NO;
}

//
// Register the application defaults
//
- (void)registerDefaults
{
    if (![[NSUserDefaults standardUserDefaults] objectForKey:kLastSynchronisationDate])
    {
        [[NSUserDefaults standardUserDefaults] setObject:[NSDate dateWithTimeIntervalSince1970:TTDatabaseModificationDate]
                                                  forKey:kLastSynchronisationDate];
    }
}

#pragma mark -
#pragma mark Core Data stack

/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
	
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        if ([NSManagedObjectContext instancesRespondToSelector:@selector(initWithConcurrencyType:)])
            _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        else
            _managedObjectContext = [NSManagedObjectContext new];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}


/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created by merging all of the models found in the application bundle.
 */
- (NSManagedObjectModel *)managedObjectModel {
	
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"tramTRACKER2" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
    return _managedObjectModel;
}

NSString * const kDefaultsDatabaseVersion = @"databaseVersion";

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
	
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
	/* We need to copy this into the data directory, only if it hasn't already been done */
	NSFileManager   * fileManager = [NSFileManager defaultManager];
	NSString        * docDir = [tramTRACKERAppDelegate applicationDocumentsDirectory];
	NSString        * destinationFileName = [docDir stringByAppendingPathComponent:@"tramTRACKER.db"];
	NSString        * destinationWalFileName = [destinationFileName stringByAppendingString:@"-wal"];
	NSString        * sourceFileName = [[NSBundle mainBundle] pathForResource:@"tramTRACKER" ofType:@"db"];
	NSString        * sourceWalFileName = [sourceFileName stringByAppendingString:@"-wal"];
    NSDictionary    * bundle = [[NSBundle mainBundle] infoDictionary];
    NSUserDefaults  * defaults = [NSUserDefaults standardUserDefaults];

	if ((![fileManager fileExistsAtPath:destinationFileName] ||
         ![fileManager fileExistsAtPath:destinationWalFileName] ||
         [defaults objectForKey:kDefaultsDatabaseVersion] == nil ||
         ![[defaults objectForKey:kDefaultsDatabaseVersion] isEqualToString:[bundle objectForKey:@"CFBundleVersion"]])
        && sourceFileName)
	{
        /* Removing previous files */
        [fileManager removeItemAtPath:destinationFileName error:NULL];
        [fileManager removeItemAtPath:destinationWalFileName error:NULL];
        
        /* Copying database files */
        [fileManager copyItemAtPath:sourceFileName toPath:destinationFileName error:nil];
        [fileManager copyItemAtPath:sourceWalFileName
                             toPath:destinationWalFileName
                              error:nil];

        /* Setting the lastSyncDate to original date */
        [defaults setObject:[NSDate dateWithTimeIntervalSince1970:TTDatabaseModificationDate] forKey:kLastSynchronisationDate];

        /* Setting the database version */
        [defaults setObject:[bundle objectForKey:@"CFBundleVersion"] forKey:kDefaultsDatabaseVersion];
    }

	NSURL   * storeUrl = [NSURL fileURLWithPath:destinationFileName];
	NSError * error = nil;

    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];

    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                   configuration:nil
                                                             URL:storeUrl
                                                         options:nil
                                                           error:&error] || error)
    {

    }
    return _persistentStoreCoordinator;
}

#pragma mark -
#pragma mark File Locations

/**
 Returns the path to the application's documents directory.
 */
+ (NSString *)applicationDocumentsDirectory {
	
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

#pragma mark -
#pragma mark Methods for handling changing application state

- (void)applicationWillResignActive:(UIApplication *)application
{ }

- (void)applicationDidBecomeActive:(UIApplication *)Mainapplication
{ }


- (void)applicationWillTerminate:(UIApplication *)application
{ }

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [[Analytics sharedInstance] appHasClosed];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [[Analytics sharedInstance] appHasLaunched];
}

- (void)saveContext
{
    tramTRACKERAppDelegate  * d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext  * context = [d managedObjectContext];

    NSError *error = nil;

    if (![context save:&error] || error)
    {
        NSLog(@"Error while saving, rollingback");
        [context rollback];
    }
}

@end
