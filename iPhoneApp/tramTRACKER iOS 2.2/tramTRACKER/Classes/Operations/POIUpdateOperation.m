//
//  POIUpdateOperation.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 10/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import "POIUpdateOperation.h"
#import "BackgroundSynchroniser.h"
#import "PidsService.h"
#import "NSDictionary+SafeAccess.h"
#import "PointOfInterest.h"

@interface POIUpdateOperation()

@property (nonatomic, strong) PidsService           * service;
@property (nonatomic, weak) PointOfInterest         * poi;

@property (nonatomic, assign, getter = isExecuting) BOOL executing;
@property (nonatomic, assign, getter = isFinished) BOOL finished;

@property (nonatomic, assign, getter = hasPoiBeenUpdated) BOOL poiBeenUpdated;
@property (nonatomic, assign, getter = hasStopsBeenUpdated) BOOL stopsBeenUpdated;

@end

@implementation POIUpdateOperation

- (id)initWithPOI:(PointOfInterest *)aPOI
{
	if (self = [super init])
	{
        self.poi = aPOI;
        
		self.service = [[PidsService alloc] init];
		[self.service setDelegate:self];
        
		self.executing = NO;
		self.finished = NO;
        
		// set our priority
		[self setQueuePriority:NSOperationQueuePriorityHigh];
	}
	return self;
}

- (void)dealloc
{
    [self.service setDelegate:nil];
}

- (BOOL)isConcurrent
{
	return NO;
}

/**
 * Actually start processing the update
 **/

- (void)main
{
	[self start];
}

- (void)start
{
	if ([self isCancelled])
	{
		[self finish];
		return;
	}
    
	// we start by getting the destinations for this route
	self.executing = YES;
    
	// send a notification
    
    if (self.syncManager != nil && self.syncManager.delegate != nil && [self.syncManager.delegate respondsToSelector:@selector(syncDidStartPOI:)])
        [self.syncManager.delegate performSelectorOnMainThread:@selector(syncDidStartPOI:) withObject:self.poi waitUntilDone:NO];

	[self updateInformation];
    
	// sigh, poll until its finished
	while (![self isFinished])
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
}

- (void)updateInformation
{
	[self.service getInformationForPOI:[[self.poi poiID] floatValue]];
}

- (void)setStopsForPOI:(NSArray *)stops
{
    self.poi.stopsThroughPOI = stops;
    
    if (self.hasPoiBeenUpdated)
        [self finish];
    else
        self.stopsBeenUpdated = YES;
}

- (void)setInformation:(NSDictionary *)info
{
	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}
    
    PointOfInterest * poi = self.poi;
    
    poi.categoryName = [info valueForKey:@"CategoryName" ifKindOf:[NSString class] defaultValue:@""];
    poi.disabledAccess = [info valueForKey:@"DisabledAccess" ifKindOf:[NSNumber class] defaultValue:@NO];
    poi.emailAddress = [info valueForKey:@"emailAddress" ifKindOf:[NSString class] defaultValue:@""];
    poi.hasEntryFee = [info valueForKey:@"HasEntryFee" ifKindOf:[NSNumber class] defaultValue:@NO];
    poi.hasToilets = [info valueForKey:@"HasToilets" ifKindOf:[NSNumber class] defaultValue:@NO];
    poi.latitude = [info valueForKey:@"Latitude" ifKindOf:[NSNumber class] defaultValue:@0];
    poi.longitude = [info valueForKey:@"Longitude" ifKindOf:[NSNumber class] defaultValue:@0];
    poi.moreInfo = [info valueForKey:@"MoreInfo" ifKindOf:[NSString class] defaultValue:@""];
    poi.name = [info valueForKey:@"Name" ifKindOf:[NSString class] defaultValue:@""];
    poi.openingHours = [info valueForKey:@"OpeningHours" ifKindOf:[NSString class] defaultValue:@""];
    poi.poiDescription = [info valueForKey:@"POIDescription" ifKindOf:[NSString class] defaultValue:@""];
    poi.phoneNumber = [info valueForKey:@"PhoneNumber" ifKindOf:[NSString class] defaultValue:@""];
    poi.postcode = [info valueForKey:@"Postcode" ifKindOf:[NSNumber class] defaultValue:@0];
    poi.streetAddress = [info valueForKey:@"StreetAddress" ifKindOf:[NSString class] defaultValue:@""];
    poi.suburb = [info valueForKey:@"Suburb" ifKindOf:[NSString class] defaultValue:@""];
    poi.webAddress = [info valueForKey:@"WebAddress" ifKindOf:[NSString class] defaultValue:@""];
    
    if (self.hasStopsBeenUpdated)
        [self finish];
    else
        self.poiBeenUpdated = YES;
}

// Indicates a failure of some sort.
- (void)pidsServiceDidFailWithError:(NSError *)error
{
	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}
    
	static NSInteger previousFailureCount = 0;
    
	// if the stop has been deleted we can silently fail over to syncing that change
	if ([error code] == PIDServiceValidationErrorOutletNotFound)
	{
		//NSLog(@"Triggered delete: %@", self.stop);
		//if (self.syncManager != nil)
		//	[self.syncManager addStopDeleteToQueue:self.stop];
		[self finish];
	}
    else if ([error code] == PIDServiceErrorTimeoutReached && previousFailureCount == 0)
	{
		// try it again
		[self start];
		previousFailureCount = 1;
	}
    else
	{
		[self cancel];
		[self finish];
		
		// send a failure message to the delegate
		if (self.syncManager != nil && self.syncManager.delegate != nil && ![self isCancelled] && [self.syncManager.delegate respondsToSelector:@selector(syncDidFailWithError:)])
			[self.syncManager.delegate performSelectorOnMainThread:@selector(syncDidFailWithError:) withObject:error waitUntilDone:NO];
	}
}

- (void)finish
{
	// send a notification if we werent cancelled
	if (self.syncManager != nil && self.syncManager.delegate != nil && ![self isCancelled] && [self.syncManager.delegate respondsToSelector:@selector(syncDidFinishPOI:)])
		[self.syncManager.delegate performSelectorOnMainThread:@selector(syncDidFinishPOI:) withObject:self.poi waitUntilDone:NO];
    
	self.executing = NO;
	self.finished = YES;
    
	// release ourselves from the delegate
	[self.service setDelegate:self];
}

@end
