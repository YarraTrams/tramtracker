//
//  POIDeleteOperation.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 10/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PointOfInterest;

@interface POIDeleteOperation : NSOperation

- (id)initWithPOI:(PointOfInterest *)aPOI;

@property (nonatomic, weak) BackgroundSynchroniser  * syncManager;

@end
