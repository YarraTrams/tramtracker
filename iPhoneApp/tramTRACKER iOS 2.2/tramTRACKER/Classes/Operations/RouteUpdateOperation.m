
//  RouteUpdateOperation.m
//  tramTRACKER
//
//  Created by Robert Amos on 29/07/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "RouteUpdateOperation.h"
#import "Route.h"
#import "PidsService.h"
#import "BackgroundSynchroniser.h"
#import "Turn.h"
#import "NSDictionary+SafeAccess.h"

@implementation RouteUpdateOperation

@synthesize syncManager, route;

- (id)initWithRoute:(Route *)aRoute
{
	if (self = [super init])
	{
		route = aRoute;
		
		service = [[PidsService alloc] init];
		[service setDelegate:self];
		
		[self willChangeValueForKey:@"isExecuting"];
		executing = NO;
		[self didChangeValueForKey:@"isExecuting"];
		
		[self willChangeValueForKey:@"isFinished"];
		finished = NO;
		[self didChangeValueForKey:@"isFinished"];

		destinationsUpdated = NO;
		upStopsUpdated = NO;
		downStopsUpdated = NO;
        upTurnsUpdated = NO;
        downTurnsUpdated = NO;
		
		// set our priority
		[self setQueuePriority:NSOperationQueuePriorityLow];
	}
	return self;
}

- (void)dealloc
{
    [service setDelegate:nil];
}

- (BOOL)isConcurrent
{
	return NO;
}

- (BOOL)isExecuting
{
	return executing;
}

- (BOOL)isFinished
{
	return finished;
}

- (void)cancel
{
	[super cancel];
	//NSLog(@"Operation %@ cancelled.", self);
}

/**
 * Actually start processing the update
**/
- (void)main
{
	[self start];
}

- (void)start
{
	//NSLog(@"[ROUTEUPDATE] Updating route %@ - %@", route.internalNumber, [NSThread currentThread]);

	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}

	// we start by getting the destinations for this route
	[self willChangeValueForKey:@"isExecuting"];
	executing = YES;
	[self didChangeValueForKey:@"isExecuting"];

	// send a notification
	if (self.syncManager != nil && self.syncManager.delegate != nil && [self.syncManager.delegate respondsToSelector:@selector(syncDidStartRoute:)])
		[self.syncManager.delegate performSelectorOnMainThread:@selector(syncDidStartRoute:) withObject:route waitUntilDone:NO];
	
	[self updateDestinations];

	// sigh, poll until its finished
	while (![self isFinished] && ![self isCancelled])
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
}

- (void)updateDestinations
{
	[service destinationsForRoute:route];

}

- (void)updateStopLists
{
	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}
	
	// if the up direction has no destination
	if (route.upDestination == nil)
	{
		// does it have existing stops in that direction? if so they will need to be cleared and updated
		if (route.upStops != nil)
		{
			if (syncManager != nil)
			{
				for (NSNumber *trackerID in route.upStops)
					[syncManager addRoutesThroughTrackerIDToQueue:trackerID];

				// If there are any turns for that direction delete them
				NSArray *turns = [syncManager turnsForRoute:route upDirection:YES];
				if (turns != nil && [turns count] > 0)
					for (Turn *turn in turns)
						[self.syncManager addTurnDeleteToQueue:turn];
			}
			[route setUpStops:nil];
		}
		
		upStopsUpdated = YES;
        upTurnsUpdated = YES;
		
	} else
	{
		// update the list of stops from the service
		[service getStopListForRoute:route upDirection:YES];
	}

	// if the down direction has no destination
	if (route.downDestination == nil)
	{
		// does it have existing stops going down? if so they will need to be cleared and updated
		if (route.downStops != nil)
		{
			if (syncManager != nil)
			{
				for (NSNumber *trackerID in route.downStops)
					[syncManager addRoutesThroughTrackerIDToQueue:trackerID];

				// If there are any turns for that direction delete them
				NSArray *turns = [syncManager turnsForRoute:route upDirection:NO];
				if (turns != nil && [turns count] > 0)
					for (Turn *turn in turns)
						[self.syncManager addTurnDeleteToQueue:turn];
			}
			[route setDownStops:nil];
		}
		
		downStopsUpdated = YES;
        downTurnsUpdated = YES;
		
	} else
	{
		// update the list of stops from the service
		[service getStopListForRoute:route upDirection:NO];
	}
    
    // were both of those nil? if so we need to exit
    if (route.upDestination == nil && route.downDestination == nil)
        [self finish];    
}

/**
 * Received the destinations from the service
**/
- (void)setDestinations:(NSDictionary *)destinations forRouteNumber:(NSString *)internalRouteNumber
{
	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}

	BOOL destinationsHaveChanged = NO;
	
	// update the destinations
	NSString *upDestination = [destinations objectForKey:@"up"];
	if (upDestination == nil || [upDestination isEqualToString:@""])
	{
		if (route.upDestination != nil)
			destinationsHaveChanged = YES;
		[route setUpDestination:nil];
	}
	else
	{
		if (![route.upDestination isEqualToString:upDestination])
			destinationsHaveChanged = YES;
		[route setUpDestination:upDestination];
	}
	
	NSString *downDestination = [destinations objectForKey:@"down"];
	if (downDestination == nil || [downDestination isEqualToString:@""])
	{
		if (route.downDestination != nil)
			destinationsHaveChanged = YES;
		[route setDownDestination:nil];
	}
	else
	{
		if (![route.downDestination isEqualToString:downDestination])
			destinationsHaveChanged = YES;
		[route setDownDestination:downDestination];
	}

	if (destinationsHaveChanged)
		[route updateName];
	
	// now that we have the destinations, update the route definitions
	destinationsUpdated = YES;
	
	[self updateStopLists];	    
}

- (void)setSuburbNames:(NSArray *)items
{
    for (NSDictionary * curObject in items)
    {
        NSNumber    * stopNo = [curObject valueForKey:@"StopNo" ifKindOf:[NSNumber class] defaultValue:nil];
        NSString    * suburb = [curObject valueForKey:@"SuburbName" ifKindOf:[NSString class] defaultValue:nil];
        
        if (stopNo && suburb.length > 0)
        {
            Stop *stop = [syncManager stopForTrackerID:stopNo];

            if ([stop.trackerID.stringValue isEqualToString:@"4236"])
            {
                int i = 0;
                ++i;
            }
            
            stop.suburbName = suburb;
        }
    }
    suburbsUpdated = YES;
	if (destinationsUpdated && upStopsUpdated && downStopsUpdated && upTurnsUpdated && downTurnsUpdated && suburbsUpdated)
		[self finish];
}

/**
 * Received turns
**/
- (void)setTurnList:(NSArray *)turnList forRouteNumber:(NSString *)internalRouteNumber upDirection:(NSNumber *)upDirection
{

	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}

	BOOL up = [upDirection boolValue];

	if (syncManager != nil)
	{
		// Find any existing turns
		NSManagedObjectContext *context = [syncManager managedObjectContext];
		NSEntityDescription *turnEntity = [NSEntityDescription entityForName:@"Turn" inManagedObjectContext:context];
		NSMutableArray *turns = [[syncManager turnsForRoute:route upDirection:up] mutableCopy];

		// loop through and create/update any turns necessary
		for (NSDictionary *turn in turnList)
		{
			NSNumber *trackerID = [turn objectForKey:@"trackerid"];
			if (trackerID != nil)
			{
				// does this turn already exist?
				Stop *stop = [syncManager stopForTrackerID:trackerID];
				Turn *existingTurn = [syncManager turnForRoute:route upDirection:up stop:stop];
                if (stop) {
                    if (existingTurn == nil)
                    {
                        // turn does not exist, create it
                        existingTurn = [[Turn alloc] initWithEntity:turnEntity insertIntoManagedObjectContext:context];
                        [existingTurn setRoute:[syncManager routeForInternalRouteNumber:route.internalNumber]];
                        [existingTurn setStop:stop];
                        [existingTurn setUpDirection:up];
                        [existingTurn setType:[turn objectForKey:@"type"]];
                        [existingTurn setMessage:[turn objectForKey:@"message"]];
                    } else
                    {
                        // remove this turn from the array
                        [turns removeObject:existingTurn];
                        [existingTurn setType:[turn objectForKey:@"type"]];
                        [existingTurn setMessage:[turn objectForKey:@"message"]];
                    }
                    
                }
			}
		}
		
		// if there are any turns left in the turns array then delete them
		for (Turn *turn in turns)
			[self.syncManager addTurnDeleteToQueue:turn];

	}
    
    // which direction did we just finish?
    if (up)
        upTurnsUpdated = YES;
    else
        downTurnsUpdated = YES;

    // are we the last to complete?
	if (destinationsUpdated && upStopsUpdated && downStopsUpdated && upTurnsUpdated && downTurnsUpdated && suburbsUpdated)
		[self finish];
}

/**
 * Received stop lists
**/
- (void)setStopList:(NSArray *)stopList forRouteNumber:(NSString *)internalRouteNumber upDirection:(NSNumber *)upDirection
{
	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}
	
	BOOL up = [upDirection boolValue];
	
	// find the existing stop list
	NSArray *existingStopList = up ? route.upStops : route.downStops;

	// if the existing stop list is empty, make sure that we double check all these stops
	if (existingStopList == nil || [existingStopList count] == 0)
	{
		if (syncManager != nil)
		{
			for (NSNumber *trackerID in stopList)
				[syncManager addRoutesThroughTrackerIDToQueue:trackerID];
		}
	} else
	{
		// check for stops that no longer exist in the new stop list
		for (NSNumber *trackerID in existingStopList)
		{
			if (![stopList containsObject:trackerID] && syncManager != nil)
				[syncManager addRoutesThroughTrackerIDToQueue:trackerID];
		}
		
		// check for stops that are in the new list but not the old one
		for (NSNumber *trackerID in stopList)
		{
			if (![existingStopList containsObject:trackerID] && syncManager != nil)
				[syncManager addRoutesThroughTrackerIDToQueue:trackerID];
		}
	}
	
	// save the new stop list
	if (up)
	{
		upStopsUpdated = YES;
		[route setUpStops:stopList];
	} else
	{
		downStopsUpdated = YES;
		[route setDownStops:stopList];
	}
	
	// are we the last to complete?
	if (destinationsUpdated && upStopsUpdated && downStopsUpdated && upTurnsUpdated && downTurnsUpdated && suburbsUpdated)
		[self finish];
}

// Indicates a failure of some sort.
- (void)pidsServiceDidFailWithError:(NSError *)error
{
	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}

	static NSInteger previousFailureCount = 0;

	// if the route has been deleted we can silently fail over to syncing that change
	if ([error code] == PIDServiceValidationErrorRouteNotFound || error.code == PIDServiceValidationError)
	{
		//NSLog(@"Triggered delete: %@", self.route);
		if (self.syncManager != nil)
			[self.syncManager addRouteDeleteToQueue:self.route];
		[self finish];
	} else if ([error code] == PIDServiceErrorTimeoutReached && previousFailureCount == 0)
	{
		// try it again
		[self start];
		previousFailureCount = 1;
		
	} else
		// otherwise some other error has occured, abort
	{
		[self cancel];
		[self finish];

		// send a failure message to the delegate
		if (self.syncManager != nil && self.syncManager.delegate != nil && ![self isCancelled] && [self.syncManager.delegate respondsToSelector:@selector(syncDidFailWithError:)])
			[self.syncManager.delegate performSelectorOnMainThread:@selector(syncDidFailWithError:) withObject:error waitUntilDone:NO];
	}
}

- (void)finish
{
	//NSLog(@"[ROUTEUPDATE] Finished route %@ - %@", route.internalNumber, [NSThread currentThread]);
	// send a notification if we werent cancelled
	if (self.syncManager != nil && self.syncManager.delegate != nil && ![self isCancelled] && [self.syncManager.delegate respondsToSelector:@selector(syncDidFinishRoute:)])
		[self.syncManager.delegate performSelectorOnMainThread:@selector(syncDidFinishRoute:) withObject:route waitUntilDone:NO];

	[self willChangeValueForKey:@"isExecuting"];
	executing = NO;
	[self didChangeValueForKey:@"isExecuting"];

	[self willChangeValueForKey:@"isFinished"];
	finished = YES;
	[self didChangeValueForKey:@"isFinished"];

	[service setDelegate:nil];
}


@end
