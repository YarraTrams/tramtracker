//
//  RouteDeleteOperation.h
//  tramTRACKER
//
//  Created by Robert Amos on 7/08/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Route;
@class BackgroundSynchroniser;

/**
 * A delete operation for a specific route
 *
 * @ingroup Sync
**/
@interface RouteDeleteOperation : NSOperation

@property (nonatomic, weak) BackgroundSynchroniser *syncManager;
@property (nonatomic, assign, getter = isExecuting) BOOL executing;
@property (nonatomic, assign, getter = isFinished) BOOL finished;


/**
 * Initialises the route delete operation.
 *
 * @param	aRoute			The route to be deleted
 * @return					An initialised RouteDeleteOperation object
**/
- (id)initWithRoute:(Route *)aRoute;

/**
 * Required by the NSOperationQueue, whether to execute this operation concurrently.
 **/
- (BOOL)isConcurrent;

/**
 * Starts the Stop Delete Operation
 **/
- (void)start;

/**
 * Finishes the Update operation and handles notifications
 **/
- (void)finish;

@end
