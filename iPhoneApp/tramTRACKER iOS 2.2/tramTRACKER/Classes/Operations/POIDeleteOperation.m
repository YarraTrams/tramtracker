//
//  POIDeleteOperation.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 10/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import "POIDeleteOperation.h"
#import "BackgroundSynchroniser.h"
#import "PidsService.h"
#import "NSDictionary+SafeAccess.h"
#import "PointOfInterest.h"

@interface POIDeleteOperation()

@property (nonatomic, strong) PidsService           * service;
@property (nonatomic, weak) PointOfInterest         * poi;

@property (nonatomic, assign, getter = isExecuting) BOOL executing;
@property (nonatomic, assign, getter = isFinished) BOOL finished;

@end

@implementation POIDeleteOperation

- (id)initWithPOI:(PointOfInterest *)aPOI
{
	if (self = [super init])
	{
        self.poi = aPOI;

		self.service = [[PidsService alloc] init];
		[self.service setDelegate:self];

		self.executing = NO;
		self.finished = NO;

		// set our priority
		[self setQueuePriority:NSOperationQueuePriorityHigh];
	}
	return self;
}

- (void)dealloc
{
    [self.service setDelegate:nil];
}

- (BOOL)isConcurrent
{
	return NO;
}

/**
 * Actually start processing the update
 **/

- (void)main
{
	[self start];
}

- (void)start
{
	if ([self isCancelled])
	{
		[self finish];
		return;
	}
    
	// we start by getting the destinations for this route
	self.executing = YES;

	// send a notification
	if (self.syncManager != nil && self.syncManager.delegate != nil && [self.syncManager.delegate respondsToSelector:@selector(deleteDidStartPOI:)])
		[self.syncManager.delegate performSelectorOnMainThread:@selector(deleteDidStartPOI:) withObject:self.poi waitUntilDone:NO];

    [self deletePOI];

	// sigh, poll until its finished
	while (![self isFinished])
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
}

- (void)deletePOI
{
	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}
	
	// so we're at the stage where we can delete this route now
	if (self.syncManager != nil)
	{
		NSManagedObjectContext *context = [self.syncManager managedObjectContext];
		[context deleteObject:self.poi];
	}

	[self finish];
}

// Indicates a failure of some sort.
- (void)pidsServiceDidFailWithError:(NSError *)error
{
	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}
    
	static NSInteger previousFailureCount = 0;
    
	// if the stop has been deleted we can silently fail over to syncing that change
	if ([error code] == PIDServiceValidationErrorOutletNotFound)
	{
		//NSLog(@"Triggered delete: %@", self.stop);
		//if (self.syncManager != nil)
		//	[self.syncManager addStopDeleteToQueue:self.stop];
		[self finish];
	}
    else if ([error code] == PIDServiceErrorTimeoutReached && previousFailureCount == 0)
	{
		// try it again
		[self start];
		previousFailureCount = 1;
	}
    else
	{
		[self cancel];
		[self finish];
		
		// send a failure message to the delegate
		if (self.syncManager != nil && self.syncManager.delegate != nil && ![self isCancelled] && [self.syncManager.delegate respondsToSelector:@selector(syncDidFailWithError:)])
			[self.syncManager.delegate performSelectorOnMainThread:@selector(syncDidFailWithError:) withObject:error waitUntilDone:NO];
	}
}

- (void)finish
{
	// send a notification if we werent cancelled
	if (self.syncManager != nil && self.syncManager.delegate != nil && ![self isCancelled] && [self.syncManager.delegate respondsToSelector:@selector(syncDidFinishPOI:)])
		[self.syncManager.delegate performSelectorOnMainThread:@selector(syncDidFinishPOI:) withObject:self.poi waitUntilDone:NO];

	self.executing = NO;
	self.finished = YES;

	// release ourselves from the delegate
	[self.service setDelegate:self];
}

@end
