//
//  StopUpdateOperation.m
//  tramTRACKER
//
//  Created by Robert Amos on 29/07/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "StopUpdateOperation.h"
#import "Stop.h"
#import "PidsService.h"
#import "BackgroundSynchroniser.h"
#import "NSDictionary+SafeAccess.h"

@implementation StopUpdateOperation

@synthesize syncManager, stop;

- (id)initWithStop:(Stop *)aStop
{
	if (self = [super init])
	{
		stop = aStop;
		
		service = [[PidsService alloc] init];
		[service setDelegate:self];
		
		[self willChangeValueForKey:@"isExecuting"];
		executing = NO;
		[self didChangeValueForKey:@"isExecuting"];
		
		[self willChangeValueForKey:@"isFinished"];
		finished = NO;
		[self didChangeValueForKey:@"isFinished"];
		
		// set our priority
		[self setQueuePriority:NSOperationQueuePriorityHigh];
	}
	return self;
}

- (void)dealloc
{
    [service setDelegate:nil];
}

- (BOOL)isConcurrent
{
	return NO;
}

- (BOOL)isExecuting
{
	return executing;
}

- (BOOL)isFinished
{
	return finished;
}

- (void)cancel
{
	[super cancel];
	//NSLog(@"Operation %@ cancelled.", self);
}

/**
 * Actually start processing the update
 **/
- (void)main
{
	[self start];
}
- (void)start
{
	//NSLog(@"[STOPUPDATE] Retrieiving stop info for stop %@", stop);
	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}
    
	// we start by getting the destinations for this route
	[self willChangeValueForKey:@"isExecuting"];
	executing = YES;
	[self didChangeValueForKey:@"isExecuting"];
    
	// send a notification
	if (self.syncManager != nil && self.syncManager.delegate != nil && [self.syncManager.delegate respondsToSelector:@selector(syncDidStartStop:)])
		[self.syncManager.delegate performSelectorOnMainThread:@selector(syncDidStartStop:) withObject:stop waitUntilDone:NO];
	
	[self updateInformation];
    
	// sigh, poll until its finished
	while (![self isFinished])
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
}

- (void)updateInformation
{
	[service getInformationForStop:[stop.trackerID intValue]];
}

- (void)setInformation:(NSDictionary *)info
{
	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}

    
	// start updating
	[stop setCityDirection:[info valueForKey:@"cityDirection" ifKindOf:[NSString class] defaultValue:nil]];
	[stop setCityStop:[info valueForKey:@"cityStop" ifKindOf:[NSNumber class] defaultValue:@NO]];
	[stop setConnectingBuses:[info valueForKey:@"hasConnectingBuses" ifKindOf:[NSNumber class] defaultValue:@NO]];
	[stop setConnectingTrains:[info valueForKey:@"hasConnectingTrains" ifKindOf:[NSNumber class] defaultValue:@NO]];
	[stop setConnectingTrams:[info valueForKey:@"hasConnectingTrams" ifKindOf:[NSNumber class] defaultValue:@NO]];
	[stop setLatitude:[info valueForKey:@"latitude" ifKindOf:[NSNumber class] defaultValue:@0]];

	[stop setFtzStop:[info valueForKey:@"ftzStop" ifKindOf:[NSNumber class] defaultValue:@0]];
	
    [stop setLongitude:[info valueForKey:@"longitude" ifKindOf:[NSNumber class] defaultValue:@0]];
	[stop setLength:[info valueForKey:@"length" ifKindOf:[NSNumber class] defaultValue:@0]];
	[stop setName:[info valueForKey:@"name" ifKindOf:[NSString class] defaultValue:nil]];
	[stop setNumber:[info valueForKey:@"number" ifKindOf:[NSString class] defaultValue:nil]];
	[stop setPlatformStop:[info valueForKey:@"platformStop" ifKindOf:[NSNumber class] defaultValue:@NO]];

	NSString    *suburb = [info valueForKey:@"suburbName" ifKindOf:[NSString class] defaultValue:nil];
    
    if (suburb.length)
        [stop setSuburbName:suburb];
	[stop setConnectingBusesList:[info valueForKey:@"connectingBusesList" ifKindOf:[NSString class] defaultValue:nil]];
	[stop setConnectingTrainsList:[info valueForKey:@"connectingTrainsList" ifKindOf:[NSString class] defaultValue:nil]];
	[stop setConnectingTramsList:[info valueForKey:@"connectingTramsList" ifKindOf:[NSString class] defaultValue:nil]];
	[stop setEasyAccess:[info valueForKey:@"easyAccess" ifKindOf:[NSNumber class] defaultValue:@NO]];
	[stop setShelter:[info valueForKey:@"shelter" ifKindOf:[NSNumber class] defaultValue:@NO]];
	[stop setYtShelter:[info valueForKey:@"ytShelter" ifKindOf:[NSNumber class] defaultValue:@NO]];
	
	NSMutableArray *zones = [NSMutableArray new];
	for (NSString *z in [[info valueForKey:@"zones" ifKindOf:[NSString class] defaultValue:nil] componentsSeparatedByString:@","])
		[zones addObject:[NSNumber numberWithInteger:[z integerValue]]];
	[stop setMetcardZones:zones];
    
	// now that we're done..
	[self finish];
}

// Indicates a failure of some sort.
- (void)pidsServiceDidFailWithError:(NSError *)error
{
	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}
    
	static NSInteger previousFailureCount = 0;
    
	// if the stop has been deleted we can silently fail over to syncing that change
	if ([error code] == PIDServiceValidationErrorStopNotFound)
	{
		//NSLog(@"Triggered delete: %@", self.stop);
		//if (self.syncManager != nil)
		//	[self.syncManager addStopDeleteToQueue:self.stop];
		[self finish];
        
	} else if ([error code] == PIDServiceErrorTimeoutReached && previousFailureCount == 0)
	{
		// try it again
		[self start];
		previousFailureCount = 1;
		
	} else
	{
		[self cancel];
		[self finish];
		
		// send a failure message to the delegate
		if (self.syncManager != nil && self.syncManager.delegate != nil && ![self isCancelled] && [self.syncManager.delegate respondsToSelector:@selector(syncDidFailWithError:)])
			[self.syncManager.delegate performSelectorOnMainThread:@selector(syncDidFailWithError:) withObject:error waitUntilDone:NO];
	}
}

- (void)finish
{
	// send a notification if we werent cancelled
	if (self.syncManager != nil && self.syncManager.delegate != nil && ![self isCancelled] && [self.syncManager.delegate respondsToSelector:@selector(syncDidFinishStop:)])
		[self.syncManager.delegate performSelectorOnMainThread:@selector(syncDidFinishStop:) withObject:stop waitUntilDone:NO];
	
	[self willChangeValueForKey:@"isExecuting"];
	executing = NO;
	[self didChangeValueForKey:@"isExecuting"];
	
	[self willChangeValueForKey:@"isFinished"];
	finished = YES;
	[self didChangeValueForKey:@"isFinished"];
    
	// release ourselves from the delegate
	[service setDelegate:nil];

	//NSLog(@"%@ was updated", stop);
}


@end
