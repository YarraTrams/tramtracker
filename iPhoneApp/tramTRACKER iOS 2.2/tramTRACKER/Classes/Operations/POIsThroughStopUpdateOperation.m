//
//  POIsThroughStopUpdateOperation.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 23/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import "POIsThroughStopUpdateOperation.h"
#import "Stop.h"
#import "BackgroundSynchroniser.h"
#import "PidsService.h"

@interface POIsThroughStopUpdateOperation()

@property (nonatomic, weak) Stop * stop;
@property (nonatomic, strong) PidsService * service;

@property (nonatomic, assign, getter = isExecuting) BOOL executing;
@property (nonatomic, assign, getter = isFinished) BOOL finished;

@end

@implementation POIsThroughStopUpdateOperation

- (id)initWithStop:(Stop *)aStop
{
	if (self = [super init])
	{
		self.stop = aStop;
		
		self.service = [[PidsService alloc] init];
		[self.service setDelegate:self];

		self.executing = NO;
		
		self.finished = NO;

		// set our priority
		[self setQueuePriority:NSOperationQueuePriorityNormal];
	}
	return self;
}

- (void)dealloc
{
    [self.service setDelegate:nil];
}

- (BOOL)isConcurrent
{
	return NO;
}

/**
 * Actually start processing the update
 **/
- (void)main
{
	[self start];
}

- (void)start
{
	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}
	
	self.executing = YES;
    
	// send a notification
	if ([self.syncManager.delegate respondsToSelector:@selector(syncDidStartRoutesThroughStop:)])
		[self.syncManager.delegate performSelectorOnMainThread:@selector(syncDidStartRoutesThroughStop:) withObject:self.stop waitUntilDone:NO];

	[self updatePOIsThroughStop];
    
	// sigh, poll until its finished
	while (![self isFinished])
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
}

- (void)updatePOIsThroughStop
{
	[self.service POIsThroughStop:self.stop];
}

- (void)setPOIs:(NSArray *)pois
{
	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}
	[self.stop setPoisThroughStop:pois];
	[self finish];
}

// Indicates a failure of some sort.
- (void)pidsServiceDidFailWithError:(NSError *)error
{
	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}
	
	static NSInteger previousFailureCount = 0;
    
	// if the stop has been deleted we can silently fail over to syncing that change
	if ([error code] == PIDServiceValidationErrorStopNotFound)
	{
		// this error can occur for a valid stop with no routes "through" it (terminus)
		//[self.stop setRoutesThroughStop:[NSArray array]];
		
		// update the stop to be sure, if the update triggers the same error it will get deleted from there
		//if (self.syncManager != nil)
		//	[self.syncManager addStopUpdateToQueue:self.stop];
        
		[self finish];
		
		// otherwise some other error has occured, abort
	}
	else if ([error code] == PIDServiceErrorTimeoutReached && previousFailureCount == 0)
	{
		// try it again
		//NSLog(@"Timed out, trying again.");
		[self updatePOIsThroughStop];
		previousFailureCount = 1;
        
	} else
	{
		[self cancel];
		[self finish];
        
		// send a failure message to the delegate
		if ([self.syncManager.delegate respondsToSelector:@selector(syncDidFailWithError:)])
			[self.syncManager.delegate performSelectorOnMainThread:@selector(syncDidFailWithError:) withObject:error waitUntilDone:NO];
	}
}

- (void)finish
{
	// send a notification if we werent cancelled
	if (![self isCancelled] && [self.syncManager.delegate respondsToSelector:@selector(syncDidFinishPOIsThroughStop:)])
		[self.syncManager.delegate performSelectorOnMainThread:@selector(syncDidFinishPOIsThroughStop:) withObject:self.stop waitUntilDone:NO];

	self.executing = NO;

	self.finished = YES;

	[self.service setDelegate:self];
}


@end
