//
//  NSDictionary+SafeAccess.h
//  tramTRACKER
//
//  Created by Tom King on 13/11/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (SafeAccess)

- (id)valueForKey:(NSString *)key
         ifKindOf:(Class)class
defaultValue:(id)defaultValue;

@end
