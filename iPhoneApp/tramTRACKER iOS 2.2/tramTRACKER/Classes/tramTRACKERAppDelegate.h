//
//  tramTRACKERAppDelegate.h
//  tramTRACKER
//
//  Created by Robert Amos on 17/02/09.
//  Copyright Yarra Trams 2009. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Stop.h"

@class PIDViewControllerOld;
@class BackgroundSynchroniser;

@interface tramTRACKERAppDelegate : NSObject <UIApplicationDelegate>

@property (nonatomic, strong) IBOutlet UIWindow             * window;
@property (nonatomic, strong) NSManagedObjectModel          * managedObjectModel;
@property (nonatomic, strong) NSManagedObjectContext        * managedObjectContext;
@property (nonatomic, strong) NSPersistentStoreCoordinator  * persistentStoreCoordinator;

@property (strong, nonatomic) NSDictionary                  * updatesFound;

@property (strong, nonatomic) NSArray                       * stopList;

@property (nonatomic) BOOL                                  favouriteStopCountChanged;

@property (nonatomic) BOOL                                  isUpdating;

- (void)saveContext;
- (void)checkForUpdates;
- (void)cancelUpdate;
+ (NSString *)applicationDocumentsDirectory;

@end
