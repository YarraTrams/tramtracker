//
//  Stop.m
//  tramTRACKER
//
//  Created by Robert Amos on 10/02/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "Stop.h"
#import "StopList.h"
#import "tramTRACKERAppDelegate.h"
#import "Route.h"
#import "FavouriteStop.h"

TTMetcardZone const TTMetcardZoneCitySaver = 0;
TTMetcardZone const TTMetcardZone1 = 1;
TTMetcardZone const TTMetcardZone2 = 2;

@implementation Stop

@dynamic number, name, cityDirection, latitude, longitude, suburbName, trackerID, routesThroughStop, poisThroughStop, platformStop, connectingTrains, connectingTrams, connectingBuses, metcardZones, length, cityStop, turns, connectingBusesList, connectingTrainsList, connectingTramsList, easyAccess, shelter, ytShelter, nearestRetailers, ftzStop;

@synthesize predictedTimeString;

- (void)addNearestRetailersObject:(TicketRetailer *)ticketRetailer
{
    NSMutableOrderedSet * tempSet = [NSMutableOrderedSet orderedSetWithOrderedSet:self.nearestRetailers];
    
    [tempSet addObject:ticketRetailer];
    self.nearestRetailers = tempSet;
}

- (BOOL)isFTZStop
{
    return self.ftzStop.boolValue;
}

- (NSString *)suburb
{
    return self.suburbName;
}

- (NSNumber *)stopNumber {
    return @([self.number integerValue]);
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"<Stop: %p>Stop %@ %@ - %@ in %@ (%@)", self, self.number, self.name, self.cityDirection, self.suburbName, self.trackerID];
}

- (NSString *)connectingTramsList
{
    [self willAccessValueForKey:@"self.connectingTramsList"];
    NSString *myName = [self primitiveValueForKey:@"connectingTramsList"];
    [self didAccessValueForKey:@"self.connectingTramsList"];
    return [myName stringByReplacingOccurrencesOfString:@"," withString:@", "];
}

- (NSString *)connectingBusesList
{
    [self willAccessValueForKey:@"self.connectingBusesList"];
    NSString *myName = [self primitiveValueForKey:@"connectingBusesList"];
    [self didAccessValueForKey:@"self.connectingBusesList"];
    return [myName stringByReplacingOccurrencesOfString:@"," withString:@", "];
}

- (NSString *)connectingTrainsList
{
    [self willAccessValueForKey:@"self.connectingTrainsList"];
    NSString *myName = [self primitiveValueForKey:@"connectingTrainsList"];
    [self didAccessValueForKey:@"self.connectingTrainsList"];
    return [myName stringByReplacingOccurrencesOfString:@"," withString:@", "];
}

+ (NSArray *)allStops
{
	// Grab our managed object context and create a fetch request
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [d managedObjectContext];
	NSFetchRequest *fetchRequest = [NSFetchRequest new];
	
	// set the entity that we're looking for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Stop" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// fetch it.
	NSError *outError;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&outError];
	
	// cant find any?
	if (fetchedObjects == nil || [fetchedObjects count] == 0)
	{
		return nil;
	}

	// return them sorted
	return fetchedObjects;
}

+ (Stop *)stopForTrackerID:(NSNumber *)aTrackerID
{
	// Grab our managed object context and create a fetch request
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [d managedObjectContext];
	NSFetchRequest *fetchRequest = [NSFetchRequest new];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Stop" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"trackerID == %@", aTrackerID];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (fetchedObjects == nil || [fetchedObjects count] == 0)
	{
		return nil;
	}
	
	// return the object
	return [fetchedObjects objectAtIndex:0];
}

+ (NSArray *)stopsForTrackerIDs:(NSArray *)aTrackerIDArray
{
	// make sure this is actually an NSArray, and that it has items
	if (aTrackerIDArray == nil || [aTrackerIDArray count] == 0)
		return nil;
	
	// Grab our managed object context and create a fetch request
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [d managedObjectContext];
	NSFetchRequest *fetchRequest = [NSFetchRequest new];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Stop" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"trackerID IN %@", aTrackerIDArray];

	[fetchRequest setPredicate:predicate];

	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (fetchedObjects == nil || [fetchedObjects count] == 0)
	{
		return nil;
	}
	
	// return the object
	
	NSMutableArray *list = [NSMutableArray new];
	for (NSNumber *trackerID in aTrackerIDArray)
	{
		for (Stop *s in fetchedObjects)
		{
			if ([trackerID isEqualToNumber:s.trackerID])
			{
				[list addObject:s];
				break;
			}
		}
	}
	return list;
}

+ (NSDictionary *)dictionaryOfStopsForTrackerIDs:(NSArray *)aTrackerIDArray
{
	// Grab our managed object context and create a fetch request
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [d managedObjectContext];
	NSFetchRequest *fetchRequest = [NSFetchRequest new];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Stop" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"trackerID IN %@", aTrackerIDArray];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (fetchedObjects == nil || [fetchedObjects count] == 0)
	{
		return nil;
	}
	
	// return the object
	
	NSMutableDictionary *dict = [NSMutableDictionary new];
	for (Stop *s in fetchedObjects)
		[dict setObject:s forKey:s.trackerID];
	return dict;
}

+ (NSArray *)stopsInSuburb:(NSString *)aSuburb
{
	// Grab our managed object context and create a fetch request
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [d managedObjectContext];
	NSFetchRequest *fetchRequest = [NSFetchRequest new];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Stop" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"suburbName == %@", aSuburb];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (fetchedObjects == nil || [fetchedObjects count] == 0)
	{
		return nil;
	}
	
	// return the object
	return fetchedObjects;
}

+ (NSArray *)stopsBySearchingNameOrSuburb:(NSString *)search
{
	// Grab our managed object context and create a fetch request
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [d managedObjectContext];
	NSFetchRequest *fetchRequest = [NSFetchRequest new];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Stop" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@ OR suburbName CONTAINS[cd] %@", search, search];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (fetchedObjects == nil || [fetchedObjects count] == 0)
	{
		return nil;
	}
	
	// return the object
	return fetchedObjects;
}

+ (NSArray *)stopsInRegion:(MKCoordinateRegion)region
{
    return [Stop stopsInRegion:region isLowFloor:NO shelter:NO];
}

+ (NSArray *)stopsInRegion:(MKCoordinateRegion)region isLowFloor:(BOOL)isLowFloor shelter:(BOOL)isShelter
{
	CLLocationDegrees northernBoundary;
	CLLocationDegrees southernBoundary;
	CLLocationDegrees easternBoundary;
	CLLocationDegrees westernBoundary;

	// simple
	northernBoundary = region.center.latitude + region.span.latitudeDelta;
	southernBoundary = region.center.latitude - region.span.latitudeDelta;
	easternBoundary = region.center.longitude + region.span.longitudeDelta;
	westernBoundary = region.center.longitude - region.span.longitudeDelta;

	// make sure we haven't reversed this
	if (southernBoundary > northernBoundary)
	{
		CLLocationDegrees tmpBoundary = northernBoundary;
		northernBoundary = southernBoundary;
		southernBoundary = tmpBoundary;
	}
	if (westernBoundary > easternBoundary)
	{
		CLLocationDegrees tmpBoundary = easternBoundary;
		easternBoundary = westernBoundary;
		westernBoundary = tmpBoundary;
	}

	// Grab our managed object context and create a fetch request
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [d managedObjectContext];
	NSFetchRequest *fetchRequest = [NSFetchRequest new];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Stop" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];

//	// and build the predicate
    NSMutableArray  * compoundPredicate = [NSMutableArray new];

	
    [compoundPredicate addObject:[NSPredicate predicateWithFormat:@"latitude BETWEEN { %@, %@ } AND longitude BETWEEN { %@, %@ } AND trackerID < 8000",
							  [NSNumber numberWithDouble:southernBoundary], [NSNumber numberWithDouble:northernBoundary],
							  [NSNumber numberWithDouble:westernBoundary], [NSNumber numberWithDouble:easternBoundary]]];
    
    if (isLowFloor)
        [compoundPredicate addObject:[NSPredicate predicateWithFormat:@"easyAccess = YES"]];
    else if (isShelter)
        [compoundPredicate addObject:[NSPredicate predicateWithFormat:@"shelter = YES OR ytShelter = YES"]];

    NSPredicate * finalPred = [NSCompoundPredicate andPredicateWithSubpredicates:compoundPredicate];
    
	[fetchRequest setPredicate:finalPred];

	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (fetchedObjects == nil || [fetchedObjects count] == 0)
	{
		return nil;
	}
	
	// return the object
	return fetchedObjects;
}

- (CLLocation *)location
{
	NSAssert([self latitude] && [self longitude], @"Latitude or Longitude not set");
	
	return [[CLLocation alloc]
						initWithLatitude:[self.latitude doubleValue]
							   longitude:[self.longitude doubleValue]];
}
- (CLLocationCoordinate2D)coordinate
{
	return [[self location] coordinate];
}


// Nicely formatted name
- (NSString *)formattedName
{
	return [NSString stringWithFormat:NSLocalizedString(@"stop-name", "Stop Name"), self.number, self.name];
}

// Nicely formatted route description
- (NSString *)formattedRouteDescription
{
	// more than one route through here?
	if ([self.routesThroughStop count] >= 4)
		return [NSString stringWithFormat:NSLocalizedString(@"stop-routedescription-multiple", @"Routes .."), [self shortenedFormattedRouteList], [self displayedCityDirection]];
	else if ([self.routesThroughStop count] > 1)
		return [NSString stringWithFormat:NSLocalizedString(@"stop-routedescription-multiple", @"Routes .."), [self.routesThroughStop componentsJoinedByString:@", "], [self displayedCityDirection]];
	else if ([self.routesThroughStop count] == 1)
		return [NSString stringWithFormat:NSLocalizedString(@"stop-routedescription-single", @"Route x.."), [self.routesThroughStop objectAtIndex:0], [self displayedCityDirection]];
    else
        return @"";
}

- (NSString *)formattedRouteDescriptionForFavouriteStop:(FavouriteStop *)aModel
{
	if (aModel.filter != nil && aModel.filter.route != nil)
		return [NSString stringWithFormat:NSLocalizedString(@"stop-routedescription-filtered", @"Route x only..."), aModel.filter.route.number, [self displayedCityDirection]];
	else
		return [self formattedRouteDescription];
}

- (NSString *)shortenedFormattedRouteList
{
	// get a list of all the routes
	NSArray *routes = [[RouteList sharedManager] routeList];
	NSMutableArray *routeNumbers = [NSMutableArray new];
	for (Route *r in routes)
		[routeNumbers addObject:r.number];
    
	// now build up an index based on the routes that pass this stop
	NSMutableArray *routeIndex = [NSMutableArray new];
	for (NSString *routeNumber in self.routesThroughStop)
		[routeIndex addObject:@([routeNumbers indexOfObject:routeNumber])];

	NSMutableArray *condensedRouteIndex = [NSMutableArray new];
	for (NSNumber *i in routeIndex)
	{
		NSUInteger index = [routeIndex indexOfObject:i];
		if (index > 0 && index < [routeIndex count]-1)
		{
			NSNumber *before = [routeIndex objectAtIndex:index-1];
			NSNumber *after = [routeIndex objectAtIndex:index+1];
			NSInteger myself = [i integerValue];
			
			if (myself - 1 != [before integerValue] || myself + 1 != [after integerValue])
				[condensedRouteIndex addObject:i];
		} else
		{
			[condensedRouteIndex addObject:i];
		}
	}
	
	NSMutableString *shortenedRouteList = [[NSMutableString alloc] initWithCapacity:0];
	for (NSNumber *i in condensedRouteIndex)
	{
		// first one? just add it
		if ([condensedRouteIndex indexOfObject:i] == 0)
		{
			[shortenedRouteList appendString:[routeNumbers objectAtIndex:[i integerValue]]];
			continue;
		}
		
		// now in our full index list, if our index - 1 existed there then theres been stuff cut, prepend it with a dash
		if ([routeIndex indexOfObject:[NSNumber numberWithInteger:[i integerValue]-1]] != NSNotFound &&
			[routeIndex indexOfObject:[NSNumber numberWithInteger:[i integerValue]-2]] != NSNotFound)
			[shortenedRouteList appendFormat:@"-%@", [routeNumbers objectAtIndex:[i integerValue]]];
		else
			[shortenedRouteList appendFormat:@", %@", [routeNumbers objectAtIndex:[i integerValue]]];
	}
	
	
	NSString *shortenedFormattedRouteList = [NSString stringWithString:shortenedRouteList];
	return shortenedFormattedRouteList;
}

// Short name
- (NSString *)shortName
{
	if ([self.name rangeOfString:@"&"].location != NSNotFound)
	{
		// split it up around the ampersand
		NSArray *pieces = [self.name componentsSeparatedByString:@"&"];
		
		// take the first piece for the title
		return [[pieces objectAtIndex:0]
				 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
	}
	return self.name;
}

// Override the retrieval of the city direction
- (NSString *)displayedCityDirection
{
	// check the stop list service to see if we've been overriden
	StopList *s = [StopList sharedManager];
	NSString *c = [s directionTextForStopID:self.trackerID];
	
	// does the override not exist? return default
	if (c == nil || [c isEqual:@""])
		return self.cityDirection;

	// return the override
	return c;
}

- (BOOL)isPlatformStop
{
	return [self.platformStop boolValue];
}
- (BOOL)isCityStop
{
	return [self.cityStop boolValue];
}

- (BOOL)hasConnectingTrains
{
	return [self.connectingTrains boolValue];
}
- (BOOL)hasConnectingTrams
{
	return [self.connectingTrams boolValue];
}
- (BOOL)hasConnectingBuses
{
	return [self.connectingBuses boolValue];
}
- (BOOL)isEasyAccessStop
{
	return [self.easyAccess boolValue];
}
- (BOOL)isShelterStop
{
	return [self.shelter boolValue];
}
- (BOOL)isYtShelterStop
{
	return [self.ytShelter boolValue];
}


//
// Compare Stop
//
- (NSComparisonResult)compareStop:(Stop *)otherStop
{
	if ([self number] == [otherStop number])
		return [self.name compare:otherStop.name];
	return [self.number compare:otherStop.number];
}

- (BOOL)isEqualToStop:(Stop *)otherStop
{
	return [self.trackerID isEqualToNumber:otherStop.trackerID];
}

//
// Search functions
//
- (BOOL)nameContains:(NSString *)searchString
{
	// if the tracker ID is zero its excluded from all search functions
	if ([self.trackerID integerValue] >= 8000)
		return NO;

	if (searchString == nil)
		return NO;
	return [self.name rangeOfString:searchString options:NSCaseInsensitiveSearch].location != NSNotFound;
}

- (BOOL)suburbNameContains:(NSString *)searchString
{
	// if the tracker ID is zero its excluded from all search functions
	if ([self.trackerID integerValue] >= 8000)
		return NO;

	if (searchString == nil)
		return NO;
	return [self.suburbName rangeOfString:searchString options:NSCaseInsensitiveSearch].location != NSNotFound;
}

// Map View Pin's title
- (NSString *)title
{
	return [self formattedName];
}

// Map View Pin's subtitle
- (NSString *)subtitle
{
    if (self.predictedTimeString == nil || !self.predictedTimeString.length)
        return [self formattedRouteDescription];
    return [NSString stringWithFormat:@"%@ - %@", self.predictedTimeString, self.formattedRouteDescription];
}

- (BOOL)isZone1
{
    for (NSNumber *zone in self.metcardZones)
        if ([zone integerValue] == 1)
            return YES;
    return NO;
}

- (BOOL)isZone2
{
    for (NSNumber *zone in self.metcardZones)
        if ([zone integerValue] == 2)
            return YES;
    return NO;
}

// Return an image based on its position in a route
- (UIView *)viewForTrackOnRoute:(Route *)route isDisabled:(BOOL)isDisabled
{
    UIImage *segmentImage = nil;
    UIImage *stopImage = nil;
    UIImage *platformImage = nil;
    
    // are we the first stop in either direction? (trackerids can only appear in one direction for a route)
	if (([route.upStops count] > 0 && [[route.upStops objectAtIndex:0] isEqualToNumber:self.trackerID]) ||
		([route.downStops count] > 0 && [[route.downStops objectAtIndex:0] isEqualToNumber:self.trackerID]))
    {
		segmentImage = (isDisabled ? [self imageForDisabledStopAsStartTerminus] : [self imageForStopAsStartTerminusOfRoute:route]);
        stopImage = [UIImage imageNamed:@"track_start_stop.png"];
        if ([self isPlatformStop])
            platformImage = [UIImage imageNamed:@"track_start_platform.png"];
    }
	
	// are we the last stop in either direction?
	else if (([route.upStops count] > 0 && [[route.upStops objectAtIndex:[route.upStops count]-1] isEqualToNumber:self.trackerID]) ||
		([route.downStops count] > 0 && [[route.downStops objectAtIndex:[route.downStops count]-1] isEqualToNumber:self.trackerID]))
    {
		segmentImage = (isDisabled ? [self imageForDisabledStopAsEndTerminus] : [self imageForStopAsEndTerminusOfRoute:route]);
        stopImage = [UIImage imageNamed:@"track_end_stop.png"];
        if ([self isPlatformStop])
            platformImage = [UIImage imageNamed:@"track_end_platform.png"];
    }
	
	// must be a mid-route stop
    else
    {
        segmentImage = (isDisabled ? [self imageForDisabledStopAsMidRoute] : [self imageForStopAsMidRouteOfRoute:route]);
        stopImage = [UIImage imageNamed:@"track_middle_stop.png"];
        if ([self isPlatformStop])
            platformImage = [UIImage imageNamed:@"track_middle_platform.png"];
    }

    // create the image view for the segment, and our overall view!
    UIImageView *segmentView = [[UIImageView alloc] initWithImage:segmentImage];
    UIView *view = [[UIView alloc] initWithFrame:segmentView.frame];
    
    // so now we have the appropriate track segment, are we in the zone 1/2 overlap? If yes then we add the zone
    // image to the bottom of the view stack
    if ([self isZone2])
    {
        [view addSubview:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"zone_1or2.png"]]];
    }

    // now add the segment
    [view addSubview:segmentView];
     segmentView = nil;
    
    // and the stop
    UIImageView *stopView = [[UIImageView alloc] initWithImage:stopImage];
    [view addSubview:stopView];
     stopView = nil;
    
    // and the platform, if it is one
    if (platformImage != nil)
    {
        UIImageView *platformView = [[UIImageView alloc] initWithImage:platformImage];
        [view addSubview:platformView];
         platformView = nil;
    }
    
    // return that view!
    return view;
}

// Image for stop as a start terminus
- (UIImage *)imageForStopAsStartTerminusOfRoute:(Route *)route;
{
	return [UIImage imageNamed:[NSString stringWithFormat:@"track_start_%@.png", ([route colour] == nil ? @"yellow" : [route colour])]];
}
- (UIImage *)imageForDisabledStopAsStartTerminus
{
	return [UIImage imageNamed:@"track_start_disabled.png"];
}

// Image for stop as an end terminus
- (UIImage *)imageForStopAsEndTerminusOfRoute:(Route *)route;
{
	return [UIImage imageNamed:[NSString stringWithFormat:@"track_end_%@.png", ([route colour] == nil ? @"yellow" : [route colour])]];
}
- (UIImage *)imageForDisabledStopAsEndTerminus
{
	return [UIImage imageNamed:@"track_end_disabled.png"];
}

// Image for stop as a regular midroute stop
- (UIImage *)imageForStopAsMidRouteOfRoute:(Route *)route;
{
	return [UIImage imageNamed:[NSString stringWithFormat:@"track_middle_%@.png", ([route colour] == nil ? @"yellow" : [route colour])]];
}
- (UIImage *)imageForDisabledStopAsMidRoute
{
	return [UIImage imageNamed:@"track_middle_disabled.png"];
}

// Image for the empty track that runs after this stop
- (UIImage *)imageForEmptyTrackAtStopOfRoute:(Route *)route;
{
	return [UIImage imageNamed:[NSString stringWithFormat:@"track_middle_%@.png", ([route colour] == nil ? @"yellow" : [route colour])]];
}
- (UIImage *)imageForDisabledEmptyTrackAtStop
{
	return [UIImage imageNamed:@"track_middle_disabled.png"];
}

// find the distance travelled between two stops
- (TTDistanceTravelled)distanceTravelledToStop:(Stop *)otherStop atLocation:(CLLocation *)loc
{
	// work out the distance between the two stops
	TTDistanceTravelled distance;
	distance.totalDistance = [LocationManager distanceFromLocation:self.location toLocation:otherStop.location];
	
	// lets check to see whether this point falls between the two stops if we're not then set the distance travelled to -1
	if (![self location:loc betweenSelfAndStop:otherStop])
	{
		distance.travelledDistance = -1;
		return distance;
	}
	
	// this is definitely not the most elegant way to do this, but it should work
	CGFloat bearingToStop = [self bearingToLocation:otherStop.location];
	CGFloat bearingToLoc = [self bearingToLocation:loc];
	
	// work out the angle between the two bearings
	CGFloat angle = fabs(bearingToLoc - bearingToStop);
	
	// and the distance to the location
	CGFloat distanceToLoc = [LocationManager distanceFromLocation:loc toLocation:self.location];
	
	// so if we project the point onto our line between the two stops we end up with a right-angled triangle
	// use trig to find the length of the projection
	CGFloat lengthOfProjection = sin(angle) * distanceToLoc;
	
	// And then pythagoras to find the distance we're looking for!
	distance.travelledDistance = sqrt(pow(distanceToLoc, 2) - pow(lengthOfProjection, 2));
	
	return distance;
}

- (BOOL)location:(CLLocation *)loc betweenSelfAndStop:(Stop *)otherStop
{
	// shortcut, if our location is the same as one of the other two then YES
	if ([LocationManager distanceFromLocation:loc toLocation:self.location] == 0 || [LocationManager distanceFromLocation:loc toLocation:otherStop.location] == 0)
		return YES;
	
	// calculate the bearings between ourselves and the other two locations
	CGFloat bearingToStop = [self bearingToLocation:otherStop.location];
	CGFloat bearingToLoc = [self bearingToLocation:loc];
	CGFloat angle = fabs(bearingToLoc - bearingToStop);
	
	// if the angle between those two bearings is more than 90 degrees then the stop is behind us
	if (angle > 90)
		return NO;
	
	// so the stop is in front of us, work out the bearings from the remote end
	CGFloat bearingFromStopToSelf = [otherStop bearingToLocation:self.location];
	CGFloat bearingFromStopToLoc = [otherStop bearingToLocation:loc];
	CGFloat remoteAngle = fabs(bearingFromStopToLoc - bearingFromStopToSelf);
	
	// if its more than 90 degrees then it is beyond the remote stop
	if (remoteAngle > 90)
		return NO;
	
	// its between us!
	return YES;
}

- (CGFloat)bearingToLocation:(CLLocation *)loc
{
	// Convert these to radians
	double lat1 = (self.location.coordinate.latitude * M_PI / 180);
	double lat2 = (loc.coordinate.latitude * M_PI / 180);
	double dLon = (loc.coordinate.longitude - self.location.coordinate.longitude) * M_PI / 180;

	// Calculate and return the bearing
	double y = sin(dLon) * cos(lat2);
	double x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon);
	return (atan2(y, x) * 180 / M_PI) + 360 % 360;
}

- (void)launchGoogleMapsWithLocation
{
	// launches the Google Maps application with the stop's location pinpointed, this will not return, tramTRACKER will close
	NSString *stringURL = [NSString stringWithFormat:@"http://maps.google.com/maps?q=%.8f,%.8f (%@)", self.location.coordinate.latitude, self.location.coordinate.longitude, [self formattedNameForURL]];
	NSString *escapedStringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
	NSURL *url = [NSURL URLWithString:escapedStringURL];
	[[UIApplication sharedApplication] openURL:url];
}

- (void)launchGoogleMapsWithDirections
{
	// launches the Google Maps application with the stop's location pinpointed, this will not return, tramTRACKER will close
	NSString *stringURL = [NSString stringWithFormat:@"http://maps.google.com/maps?daddr=%.8f,%.8f (%@)", self.location.coordinate.latitude, self.location.coordinate.longitude, [self formattedNameForURL]];
	NSString *escapedStringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
	NSURL *url = [NSURL URLWithString:escapedStringURL];
	[[UIApplication sharedApplication] openURL:url];
}

- (void)launchGoogleMapsWithDirectionsFromLocation:(CLLocation *)loc
{
	// launches the Google Maps application with the stop's location pinpointed, this will not return, tramTRACKER will close
	NSString *stringURL = [NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%.8f,%.8f&daddr=%.8f,%.8f (%@)", loc.coordinate.latitude, loc.coordinate.longitude, self.location.coordinate.latitude, self.location.coordinate.longitude, [self formattedNameForURL]];
	NSString *escapedStringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
	NSURL *url = [NSURL URLWithString:escapedStringURL];
	[[UIApplication sharedApplication] openURL:url];
}

- (NSString *)formattedNameForURL
{
	return [NSString stringWithFormat:@"%@: %@", self.number, [self.name stringByReplacingOccurrencesOfString:@"&" withString:@"and"]];
}

#pragma mark -
#pragma mark Stubs

+ (Stop *)stopForStub:(StopStub *)stub
{
    // tracker id set?
    if (stub.trackerID != nil)
        return [Stop stopForTrackerID:stub.trackerID];
    
    // nope. bummer
    return nil;
}


	
@end

@implementation StopDistance

@synthesize stop;//, favourite;
@synthesize distance;

//
// Compare our distance from the comparisonLocation with the distance of another stop
// 
- (NSComparisonResult)compareDistance:(StopDistance *)otherStopDistance
{
	return [[NSNumber numberWithDouble:[self distance]] compare:[NSNumber numberWithDouble:[otherStopDistance distance]]];
}

// get the description about the stopdistance
- (NSString *)description
{
	return [NSString stringWithFormat:@"%@ at distance %@m", stop, [NSNumber numberWithDouble:distance]];
}

// return a formatted distance
- (NSString *)formattedDistance
{
    return [StopDistance formattedDistanceForDistance:self.distance];
}

+ (NSString *)formattedDistanceForDistance:(CLLocationDistance)distance
{
	if (distance > 10000)
		return @"10 km+";
	else if (distance == 10000)
		return @"10 km";
	else if (distance >= 1000)
		return [NSString stringWithFormat:@"%.2f km", distance / 1000];
	return [NSString stringWithFormat:@"%.0f m", distance];
}

@end

@implementation StopUpdate

@synthesize trackerID, actionType, hasBeenCompleted;

- (NSString *)description
{
	NSString *typeDescription = @"unknown";
	if (actionType == PIDServiceActionTypeUpdate)
		typeDescription = @"updated";
	else if (actionType == PIDServiceActionTypeDelete)
		typeDescription = @"deleted";
	return [NSString stringWithFormat:@"Stop %@ %@ %@", trackerID, (hasBeenCompleted ? @"has been" : @"needs to be"), typeDescription];
}

@end

@implementation StopStub

@synthesize trackerID;

+ (instancetype)stopStubForStopNo:(NSNumber *)stopNo
{
    StopStub    * result = [StopStub new];
    
    result.trackerID = stopNo;
    return result;
}

@end
