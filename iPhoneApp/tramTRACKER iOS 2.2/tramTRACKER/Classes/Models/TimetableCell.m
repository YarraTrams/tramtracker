//
//  TimetableCell.m
//  tramTRACKER
//
//  Created by Raji on 24/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "TimetableCell.h"

@interface TimetableCell ()

@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblRouteNumber;

@property (strong, nonatomic) IBOutlet UILabel *lblToday;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblTimeRight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblTodayRight;

@end

@implementation TimetableCell

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        if (!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        {
            self.lblTimeRight.constant = self.lblTodayRight.constant = 40.0f;
            [self layoutIfNeeded];
            
//            self.lblTime.frame = CGRectMake(200, self.lblTime.frame.origin.y, self.lblTime.frame.size.width, self.lblTime.frame.size.height);
//            self.lblToday.frame = CGRectMake(200, self.lblToday.frame.origin.y, self.lblToday.frame.size.width, self.lblToday.frame.size.height);
        }
    }
    return self;
}

- (void)configureCellWithName:(NSString *)name andRouteNumber:(NSString *)routeNumber andTime:(NSString *)timeString andDay:(NSString *)aDay
{
    self.lblName.text = name;
    self.lblRouteNumber.text = [NSString stringWithFormat:@"Route %@", routeNumber];
    self.lblToday.text = aDay;
    self.lblTime.text = timeString;
}

@end
