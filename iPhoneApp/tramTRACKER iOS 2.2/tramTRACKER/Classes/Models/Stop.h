//
//  Stop.h
//  tramTRACKER
//
//  Created by Robert Amos on 10/02/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Location.h"
#import <MapKit/MapKit.h>
#import "Route.h"
#import "PidsService.h"

@class FavouriteStop;
@class JourneyStop;

/**
 * Represents a Metcard Fare Zone
**/
typedef NSInteger TTMetcardZone;

/**
 * The City Saver Metcard Fare Zone
**/
extern TTMetcardZone const TTMetcardZoneCitySaver;

/**
 * Metcard Fare Zone 1
**/
extern TTMetcardZone const TTMetcardZone1;

/**
 * Metcard Fare Zone 2
**/
extern TTMetcardZone const TTMetcardZone2;

/**
 * The distance a tram has travelled between two stops
**/
struct TTDistanceTravelled {
	CGFloat totalDistance;
	CGFloat travelledDistance;
};
typedef struct TTDistanceTravelled TTDistanceTravelled;

@class TicketRetailer;
@class StopStub;

/**
 * The Stop class represents the location of a physical stop in the network and contains
 * location and direction information.
**/
@interface Stop : NSManagedObject <MKAnnotation>

/**
 * Whether or not a stop is considered to have connecting trams available.
**/
@property (nonatomic, strong) NSNumber * connectingTrams;

/**
 * A list of the Routes that travel through a stop
**/

@property (nonatomic, strong) NSArray * routesThroughStop;

@property (nonatomic, strong) NSArray * poisThroughStop;

/**
 * Represents the stop number as printed on the flag. Can contain alphanumeric characters.
**/
@property (nonatomic, strong) NSString * number;

- (NSNumber *)stopNumber;

/**
 * Text that describes the direction the tram will be travelling in after leaving the station.
**/
@property (nonatomic, strong) NSString * cityDirection;

/**
 * The latitude of the stop.
**/
@property (nonatomic, strong) NSDecimalNumber * latitude;

/**
 * The stop's four-digit unique tracker ID.
**/
@property (nonatomic, strong) NSNumber * trackerID;

/**
 * The name of the Suburb that the stop is in.
**/
@property (nonatomic, strong) NSString * suburbName;

/* Easy Accessor */
- (NSString *)suburb;

@property (nonatomic, strong) NSString * predictedTimeString;

/**
 * The name of the stop, generally as printed on the flag. For stop names describing an intersection
 * both the cross-street and street the stop is on are listed.
**/
@property (nonatomic, strong) NSString * name;

/**
 * Whether or not a stop is considered to have connecting trains available.
 **/
@property (nonatomic, strong) NSNumber * connectingTrains;

/**
 * The Metcard fare zones that this stop is a part of.
**/
@property (nonatomic, strong) NSArray * metcardZones;

/**
 * The length of the stop in metres if known.
**/
@property (nonatomic, strong) NSNumber * length;

/**
 * Whether or not a stop is considered to have connecting buses available.
 **/
@property (nonatomic, strong) NSNumber * connectingBuses;

/**
 * Whether this stop is considered to be a part of the CBD region.
 **/
@property (nonatomic, strong) NSNumber * cityStop;

/**
 * Whether this stop is considered to be a part of the FTZ region.
 **/
@property (nonatomic, strong) NSNumber * ftzStop;

/**
 * The longitude of the stop.
**/
@property (nonatomic, strong) NSDecimalNumber * longitude;

/**
 * Whether this stop is considered to be a raised platform stop.
**/
@property (nonatomic, strong) NSNumber * platformStop;

/**
 * A set of turns that a tram may take after departing this stop.
**/
@property (nonatomic, strong) NSSet* turns;

/**
 * Nearest Ticket Retailer
 **/
@property (nonatomic, strong) NSOrderedSet * nearestRetailers;

/**
 * A string of comma delimited bus routes connecting to this stop
**/
@property (nonatomic, strong) NSString *connectingBusesList;

/**
 * A string of comma delimited train routes connecting to this stop
 **/
@property (nonatomic, strong) NSString *connectingTrainsList;

/**
 * A string of comma delimited tram routes connecting to this stop
 **/
@property (nonatomic, strong) NSString *connectingTramsList;

/**
 * Is this stop an easy access stop?
 **/
@property (nonatomic, strong) NSNumber *easyAccess;

/**
 * Is this stop a non-yarra-trams sheltered stop?
 **/
@property (nonatomic, strong) NSNumber *shelter;

/**
 * Is this stop a yarra-trams sheltered stop?
 **/
@property (nonatomic, strong) NSNumber *ytShelter;


/**
 * Returns a NSArray of every Stop object available
 *
 * @return	A NSArray of Stop objects for every stop in the system
**/
+ (NSArray *)allStops;

/**
 * Finds a stop for a specific Tracker ID
 *
 * @param	aTrackerID		A Tracker ID
 * @return An auto-released Stop object with the information about the requested stop.
**/
+ (Stop *)stopForTrackerID:(NSNumber *)aTrackerID;

/**
 * Find a stop based on its suburb
 *
 * @param	aSuburb			The name of a suburb.
 * @return	A NSArray of all Stop objects for stops that are located in that suburb. Exact suburb name match only.
**/
+ (NSArray *)stopsInSuburb:(NSString *)aSuburb;

/**
 * Find stops by searching based on the stop name or suburb.
 *
 * @param	search			A search string suitable for usage with the CONTAINS NSPredicate operator. Eg. Matched stops will contain the search string
 * @return	A NSArray of all Stop objects that contain that search string in their name or suburb.
**/
+ (NSArray *)stopsBySearchingNameOrSuburb:(NSString *)search;

/**
 * Retrieve multiple stops based on an array of Tracker IDs
 *
 * @param	aTrackerIDArray			A NSArray of Tracker IDs (NSNumbers)
 * @return	A NSArray of Stop objects for each Tracker ID in aTrackerIDArray
**/
+ (NSArray *)stopsForTrackerIDs:(NSArray *)aTrackerIDArray;


/**
 * Retrieve multiple stops in a NSDictionary
 *
 * @param	aTrackerIDArray			A NSArray of Tracker IDs (NSNumbers)
 * @return	A NSDictionary object with the key set to the supplied Tracker ID and the object the matching Stop.
**/
+ (NSDictionary *)dictionaryOfStopsForTrackerIDs:(NSArray *)aTrackerIDArray;

/**
 * Find all stops in a specific geographic region
 *
 * @param	region					A geographic region as defined by MKMapKit
 * @return	A NSArray of all Stop objects located in that region.
**/
+ (NSArray *)stopsInRegion:(MKCoordinateRegion)region;

+ (NSArray *)stopsInRegion:(MKCoordinateRegion)region isLowFloor:(BOOL)isLowFloor shelter:(BOOL)isShelter;


/**
 * The Location of the Stop
 *
 * @return	A Location object with the coordinates of the Stop
**/
- (CLLocation *)location;

/**
 * The 2D Coordinates of a stop as required by MKAnnotation
 *
 * @return	A CLLocationCoordinate2D struct containing the latitude and longitude of the stop, as per the MKAnnotation protocol.
**/
- (CLLocationCoordinate2D)coordinate;

/**
 * Whether a stop is considered to be a raised platform stop.
 *
 * @return	Boolean YES if the stop is a raised platform stop, otherwise NO.
**/
- (BOOL)isPlatformStop;

/**
 * Whether a stop is considered to be part of the CBD area. This is mainly used to indicate areas of poor GPS reception.
 *
 * @return	Boolean YES if the stop is considered part of the CBD area, otherwise NO.
 **/
- (BOOL)isCityStop;

/**
 * Whether a stop is considered to be part of the FTZ area.
 *
 * @return	Boolean YES if the stop is considered part of the FTZ area, otherwise NO.
 **/
- (BOOL)isFTZStop;

/**
 * Whether a stop is considered to have connecting/nearby train stations.
 *
 * @return	Boolean YES if the stop is considered to have connecting trains available, otherwise NO.
**/
- (BOOL)hasConnectingTrains;

/**
 * Whether a stop is considered to have connecting/nearby tram stops.
 *
 * @return	Boolean YES if the stop is considered to have connecting trams available, otherwise NO.
 **/
- (BOOL)hasConnectingTrams;

/**
 * Whether a stop is considered to have connecting/nearby bus stops.
 *
 * @return	Boolean YES if the stop is considered to have connecting buses available, otherwise NO.
 **/
- (BOOL)hasConnectingBuses;

/**
 * Whether a stop is an easy access stop
 *
 * @return	Boolean YES if the stop is an easy access stop, otherwise NO.
 **/
- (BOOL)isEasyAccessStop;

/**
 * Whether a stop is an non-yarra-trams shelter stop
 *
 * @return	Boolean YES if the stop is a non-yarra-trams shelter stop, otherwise NO.
 **/
- (BOOL)isShelterStop;

/**
 * Whether a stop is a yarra-trams shelter stop
 *
 * @return	Boolean YES if the stop is a yarra-trams shelter stop, otherwise NO.
 **/
- (BOOL)isYtShelterStop;

/**
 * Compares one stop with another stop and returns a comparison result. Used for natural sorting of
 * stop results in a search. Compares using the stop number and name.
 *
 * Allows you to use -sortedArrayUsingSelector:\@selector(compareStop:) on an NSArray of Stops.
 *
 * @param otherStop		The stop to compare this stop with.
 * @return An NSComparisonResult for returning to a sort method.		
**/
- (NSComparisonResult)compareStop:(Stop *)otherStop;
- (BOOL)isEqualToStop:(Stop *)otherStop;

/**
 * A nicely formatted stop name used for display purposes.
**/
- (NSString *)formattedName;

/**
 * A nicely formatted description of the routes that run through this stop.
 * Includes a list of the routes and the direction of travel.
**/
- (NSString *)formattedRouteDescription;

/**
 * A nicely formatted description of the routes that run through this stop.
 * Includes a list of the routes and the direction of travel after applying the filter
 * that is saved with the specified favourite.
 *
 * @param	indexPath			The index path to the position this stop occupies in the favourites list
**/
- (NSString *)formattedRouteDescriptionForFavouriteStop:(FavouriteStop *)aModel;

/**
 * A shortened route list by truncating consequtive route numbers.
**/
- (NSString *)shortenedFormattedRouteList;

/**
 * A shorter version of the stop's name. This is accomplished by removing the cross street
 * if its specified. For example "Dorcas St & Eastern Rd" would become "Dorcas St"
**/
- (NSString *)shortName;

/**
 * Check to see whether the stop's name contains searchString. Used by the search screens.
 *
 * @param	searchString		A string of text to check for
 * @return	YES if the name contains the searchString, NO otherwise
**/
- (BOOL)nameContains:(NSString *)searchString;

/**
 * Check to see whether the stop's suburb contains the search string. Used by the search screens.
 *
 * @param	searchString		A string of text to check for
 * @returns	YES if the suburb name contains the searchString, NO otherwise.
**/
- (BOOL)suburbNameContains:(NSString *)searchString;

/**
 * Get the stop's title. This is just a convience method for -formattedName to allow us to plot this stop on the map
**/
- (NSString *)title;

/**
 * Get the stop's subtitle. this is just a convenience method for -formattedRouteDescription to allow us to plot this stop on the map
*/
- (NSString *)subtitle;

/**
 * The overridden city direction
**/
- (NSString *)displayedCityDirection;

/**
 * Returns a UIImage with the track segment for this stop based on the route number
**/
- (UIView *)viewForTrackOnRoute:(Route *)route isDisabled:(BOOL)isDisabled;

/**
 * Returns a UIImage with the track segment for this stop based on its position in a list
**/
- (UIImage *)imageForStopAsStartTerminusOfRoute:(Route *)route;
- (UIImage *)imageForStopAsEndTerminusOfRoute:(Route *)route;
- (UIImage *)imageForStopAsMidRouteOfRoute:(Route *)route;
- (UIImage *)imageForEmptyTrackAtStopOfRoute:(Route *)route;
- (UIImage *)imageForDisabledStopAsStartTerminus;
- (UIImage *)imageForDisabledStopAsEndTerminus;
- (UIImage *)imageForDisabledStopAsMidRoute;
- (UIImage *)imageForDisabledEmptyTrackAtStop;

/**
 * Whether a geographical location is considered to be between ourselves and another stop
 *
 * Works by considering whether the location is "within" two lines projected from each stop
 * perpendicular to the other stop.
 *
 * @param	otherStop	Another instance of the Stop ID.
 * @return				YES if the location is considered between the stops, NO otherwise
**/
- (BOOL)location:(CLLocation *)loc betweenSelfAndStop:(Stop *)otherStop;

/**
 * The distance that a tram has travelled between ourselves and the otherStop as measured at the
 * specified location.
 *
 * @param	otherStop		The other stop that the tram is travelling towards
 * @param	location		A Location object that indicates the estimated position of the tram
 * @return					The estimated distance (in metres) that the tram has travelled towards otherStop
**/
- (TTDistanceTravelled)distanceTravelledToStop:(Stop *)otherStop atLocation:(CLLocation *)location;

/**
 * Finds the bearing (in degrees) from ourselves to the location specified.
 *
 * @param	loc				A Location object
 * @return					The bearing in degrees from our location to loc.
**/
- (CGFloat)bearingToLocation:(CLLocation *)loc;

/**
 * Causes the Google Maps app (or website if no app) to be opened at our location
 *
 * The tramTRACKER App will be closed after calling this method
**/
- (void)launchGoogleMapsWithLocation;

/**
 * Causes the Google Maps app (or website if no app) to be opened with our location filled in as
 * the destination in the directions dialog.
 *
 * The tramTRACKER App will be closed after calling this method
 **/
- (void)launchGoogleMapsWithDirections;

/**
 * Causes the Google Maps app (or website if no app) to be opened with our location filled in as the
 * desetination in the directions dialog. The supplied location will be filled in as the source
 * and the directions dialog will be automatically submitted and the directions provided.
 *
 * The tramTRACKER App will be closed after calling this method
 **/
- (void)launchGoogleMapsWithDirectionsFromLocation:(CLLocation *)loc;

/**
 * Returns the stop name in a format suitable for use by the Google Maps app.
 *
 * @return				Stop name with specific entities changed to be acceptable to Google.
**/
- (NSString *)formattedNameForURL;

- (BOOL)isZone1;
- (BOOL)isZone2;

/**
 * Stop Stub
**/
+ (Stop *)stopForStub:(StopStub *)stub;

@end


@interface Stop (CoreDataGeneratedAccessors)

- (void)insertObject:(TicketRetailer *)value inNearestRetailersAtIndex:(NSUInteger)idx;
- (void)removeObjectFromNearestRetailersAtIndex:(NSUInteger)idx;
- (void)insertNearestRetailers:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeNearestRetailersAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInNearestRetailersAtIndex:(NSUInteger)idx withObject:(TicketRetailer *)value;
- (void)replaceNearestRetailersAtIndexes:(NSIndexSet *)indexes withNearestRetailers:(NSArray *)values;
- (void)addNearestRetailersObject:(TicketRetailer *)value;
- (void)removeNearestRetailersObject:(TicketRetailer *)value;
- (void)addNearestRetailers:(NSOrderedSet *)values;
- (void)removeNearestRetailers:(NSOrderedSet *)values;
- (void)addTurnsObject:(Turn *)value;
- (void)removeTurnsObject:(Turn *)value;
- (void)addTurns:(NSSet *)values;
- (void)removeTurns:(NSSet *)values;

@end

/**
 * A StopDistance instance contains information about a particular stop and its distance from a reference point.
 *
 * These are generated and returned by the StopList class.
**/
@interface StopDistance : NSObject
{
	/**
	 * The stop that this StopDistance refers to. A stop or favourite stop might be used.
	**/
	Stop *stop;
	
	/**
	 * The favourite stop that this StopDistance refers to. A favourite stop or stop might be used.
	**/
//	FavouriteStop *favourite;

	/**
	 * The distance from the stop to the reference point, in metres.
	**/
	CLLocationDistance distance;
}

@property (nonatomic, strong) Stop *stop;
@property (nonatomic) CLLocationDistance distance;

/**
 * Compare the distances between two StopDistance objects.
 *
 * Allows you to use -sortedArrayUsingSelector:\@selector(compareDistance:) on an NSArray of StopDistance objects.
 *
 * @param	otherStopDistance		The other StopDistance object to compare against
**/
- (NSComparisonResult)compareDistance:(StopDistance *)otherStopDistance;

/**
 * A formatted distance string that can be used for debugging or nicely formatted human-readable output.
**/
- (NSString *)formattedDistance;

+ (NSString *)formattedDistanceForDistance:(CLLocationDistance)distance;


@end


/**
 * A StopUpdate object is a temporary object used in the Synchronisation process to describe
 * an update that needs to happen to a stop object. It also tracks the updates progress.
**/
@interface StopUpdate : NSObject
{
	/**
	 * The Stop's tracker ID that needs to be updated
	**/
	NSNumber *trackerID;

	/**
	 * The type of action to occur on this stop. This can be either PIDServiceActionTypeUpdate or PIDServiceActionTypeDelete
	**/
	PIDServiceActionType actionType;

	/**
	 * Whether this stop update operation has been completed.
	**/
	BOOL hasBeenCompleted;
}

@property (nonatomic, strong) NSNumber *trackerID;
@property (nonatomic) PIDServiceActionType actionType;
@property (nonatomic) BOOL hasBeenCompleted;

@end

/**
 * A stop stub has only the tracker id
 *
 * Pass it through +[Stop stopForStub:stopStub]; to get the full Stop.
 **/
@interface StopStub : NSObject

@property (nonatomic, strong) NSNumber *trackerID;

+ (instancetype)stopStubForStopNo:(NSNumber *)stopNo;

@end
