//
//  Settings.m
//  tramTRACKER
//
//  Created by Raji on 23/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "Settings.h"

@implementation Settings

@dynamic openWith;
@dynamic smartRestore;
@dynamic synchronise;
@dynamic showNearbyStops;
@dynamic showMostRecent;

+ (void)convert {
    
    if ([self showMostRecentStops] == 0) {
        NSURL                           * modelURL = [[NSBundle mainBundle] URLForResource:@"tramTRACKER2.momd/tramTRACKER2" withExtension:@"mom"];
        NSManagedObjectModel            * model = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
        NSString                        * docDir = [tramTRACKERAppDelegate applicationDocumentsDirectory];
        NSString                        * destinationFileName = [docDir stringByAppendingPathComponent:@"tramTRACKER.db"];
        NSURL                           * storeUrl = [NSURL fileURLWithPath:destinationFileName];
        NSError                         * error = nil;
        NSPersistentStoreCoordinator    * store = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:model];
        
        if (![store addPersistentStoreWithType:NSSQLiteStoreType
                                 configuration:nil
                                           URL:storeUrl
                                       options:nil
                                         error:&error] || error)
        {
            
        }
        
        NSManagedObjectContext * context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        
        [context setPersistentStoreCoordinator:store];
        
        NSFetchRequest          * fetchRequest = [NSFetchRequest new];
        
        NSEntityDescription     * entity = [NSEntityDescription entityForName:@"Settings" inManagedObjectContext:context];
        NSError *outError;
        
        [fetchRequest setEntity:entity];
        
        NSArray * fetchedObjects = [context executeFetchRequest:fetchRequest error:&outError];
        Settings * s;
        
        if (!fetchedObjects.count)
        {
            s = [NSEntityDescription
                 insertNewObjectForEntityForName:@"Settings"
                 inManagedObjectContext:context];
            
            s.openWith = @"Nearby List";
            s.smartRestore = @YES;
            s.synchronise = @YES;
            s.showNearbyStops = @(30);
            s.showMostRecent = @(30);
        } else {
            s = fetchedObjects.firstObject;
        }
        
        [self setSync:s.synchronise.boolValue];
        [self setOpenWith:s.openWith];
        [self setShowMostRecentStops:s.showMostRecent.integerValue];
        [self setShowNearbyStops:s.showNearbyStops.integerValue];
    }
}

+ (void)setSync:(BOOL)value {
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:@"isSync"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (BOOL)synchronise {
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"isSync"];
}

+ (void)setShowMostRecentStops:(NSInteger)count {
    [[NSUserDefaults standardUserDefaults] setInteger:count forKey:@"showMostRecentStops"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSInteger)showMostRecentStops {
    return [[NSUserDefaults standardUserDefaults] integerForKey:@"showMostRecentStops"];
}

+ (void)setShowNearbyStops:(NSInteger)count {
    [[NSUserDefaults standardUserDefaults] setInteger:count forKey:@"showNearbyStops"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSInteger)showNearbyStops {
    return [[NSUserDefaults standardUserDefaults] integerForKey:@"showNearbyStops"];
}

+ (NSString *)openWith {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"openWith"];
}

+ (void)setOpenWith:(NSString *)openWith {
    [[NSUserDefaults standardUserDefaults] setObject:openWith forKey:@"openWith"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
