//
//  FavouriteGroupsViewController.h
//  tramTRACKER
//
//  Created by Raji on 4/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FavouriteSection;

@interface FavouriteGroupsViewController : UITableViewController

- (void)setCurrentSection:(NSInteger)section;

@end
