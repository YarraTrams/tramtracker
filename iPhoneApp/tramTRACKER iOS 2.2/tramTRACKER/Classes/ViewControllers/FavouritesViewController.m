//
//  FavouriteGroupsTableViewController.m
//  tramTRACKER
//
//  Created by Raji on 3/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "FavouritesViewController.h"
#import "StopList.h"
#import "SectionHeaderView.h"
#import "FavouriteCell.h"
#import "Constants.h"
#import "FavouriteAddViewController.h"
#import "FavouriteContainerViewController.h"
#import "Analytics.h"
#import "tramTRACKERAppDelegate.h"
#import "UIAlertView+Blocks.h"
#import "FavouriteContainerViewController.h"
#import "FavouriteStop.h"


@interface FavouritesViewController ()

@property (strong, nonatomic) IBOutlet UIView               * noFavouritesMessage;
@property (weak, nonatomic) IBOutlet UILabel                * bottomLabel;
@property (assign, nonatomic) FavouritesViewControllerType  currentMode;

@property (weak, nonatomic) IBOutlet UITableView            * tableView;

@property (strong, nonatomic) NSMutableArray * expandedStates;
@property (strong, nonatomic) UIView    *footerBackup;
@property (strong, nonatomic) NSString * currentSectionName;

@end

@implementation FavouritesViewController

#pragma mark - Inits and Loads

- (void)manageEmptyViewAndNavigationButtons
{
    BOOL hasItems = [[StopList sharedManager] getFavouriteStopList].count > 0;
    BOOL hasSections = [[StopList sharedManager] sectionNamesForFavourites].count > 0;
    
    self.tabBarController.parentViewController.navigationItem.leftBarButtonItem.enabled = hasSections;
    self.tabBarController.parentViewController.navigationItem.rightBarButtonItem.enabled = hasItems;
    self.noFavouritesMessage.hidden = hasSections;
    self.tableView.bounces = hasSections;
    self.bottomLabel.hidden = hasSections;
    
    if (!self.footerBackup)
        self.footerBackup = self.tableView.tableFooterView;
    self.tableView.tableFooterView = hasSections ? nil : self.footerBackup;
}

- (void)manageExpandedStates {
    
    NSInteger sectionCount = [[StopList sharedManager] sectionNamesForFavourites].count;
    
    if (!self.expandedStates)
        self.expandedStates = [NSMutableArray new];
    
    while (self.expandedStates.count < sectionCount)
        [self.expandedStates addObject:@YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.currentMode = FavouritesViewControllerFavourites;
    
    [self manageExpandedStates];
    
    // Notification sent from cell (FavouriteCell) to edit a favourite by performing a segue to AddToFavouritesController and passing the favourite group to edit
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotificationEditFavourite:) name:kNotificationEditFavourite object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadData) name:kNotificationReloadFavourited  object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadData) name:kNotificationSyncFinished object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotificationEditGroupName:) name:kNotificationEditGroupName object:nil];
    
    [self.bottomLabel setAccessibilityLabel:@"You currently have no favourites saved. To save a stop as a favourite, tap the Add To Favourite button from the options menu at a stop. You can browse stops from the Routes tab."];
}

- (void)reloadData
{
    [self manageExpandedStates];
    [self.tableView reloadData];
    [self manageEmptyViewAndNavigationButtons];
}

//- (void)viewWillAppear:(BOOL)animated:(BOOL)animated
//{
//    [super viewWillDisappear:animated];
//}

- (void)viewWillAppear:(BOOL)animated
{
    self.currentMode = FavouritesViewControllerFavourites;
    [super viewWillAppear:animated];
    [self reloadData];
    [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureFavouritesList];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Notification Handlers

- (void)didReceiveNotificationEditFavourite:(NSNotification *)notification
{
    [self.parentViewController.parentViewController performSegueWithIdentifier:kSegueEditFavourites sender:notification.object];
}

- (void)didReceiveNotificationEditGroupName:(NSNotification *)notification
{
    self.currentSectionName = notification.object;
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Edit Group Name"
                                                     message:@"Please enter a new group name."
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"Ok", nil];
    
    [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [[alert textFieldAtIndex:0] setText:self.currentSectionName];
    [alert show];
}

#pragma mark - Utility Methods

- (void)didSelectAccessoryForSectionHeader:(SectionHeaderView *)sectionHeader
{
    NSInteger           section = sectionHeader.section;
    NSMutableArray      * indexPaths = [NSMutableArray new];
    BOOL                isExpanded = [self.expandedStates[sectionHeader.section] boolValue];
    
    self.expandedStates[sectionHeader.section] = @(!isExpanded);
    
    NSInteger rowCount = [[StopList sharedManager] countOfFavouritesInSection:sectionHeader.section];
    
    for (NSInteger i = 0; i < rowCount; ++i)
        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:section]];
    
    if (isExpanded)
        [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationMiddle];
    else {
        [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationMiddle];
        [self.tableView scrollToRowAtIndexPath:indexPaths.firstObject atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

- (void)setCurrentMode:(FavouritesViewControllerType)currentMode
{
    NSDictionary    * titleDictionary =
    
    @{@(FavouritesViewControllerManageGroups) : @[@"Groups", @"stops-pencil-icon", @"Edit Stops", @"Nav-Done", @"Done"],
      @(FavouritesViewControllerFavourites) : @[@"Favourites", @"manage-icon", @"Manage Favourites", @"Nav-Map-Right", @"Map"],
      @(FavouritesViewControllerManageFavourites) : @[@"Favourites", @"Nav-Groups", @"Edit Groups", @"Nav-Done", @"Done"]};
    
    enum
    {
        FavouriteModeTitle,
        FavouriteModeLeftNavIcon,
        FavouriteModeLeftNavAccessibilityLabel,
        FavouriteModeRightNavIcon,
        FavouriteModeRightNavAccessibilityLabel
    };
    
    NSArray     * currentArray = titleDictionary[@(currentMode)];
    
    UINavigationItem    * navigationItem = self.tabBarController.parentViewController.navigationItem;
    
    navigationItem.title = currentArray[FavouriteModeTitle];
    
    [navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:currentArray[FavouriteModeLeftNavIcon]]
                                                                          style:UIBarButtonItemStyleDone
                                                                         target:self.tabBarController.parentViewController action:@selector(editAction:)]
                                animated:YES];
    [navigationItem.leftBarButtonItem setAccessibilityLabel:currentArray[FavouriteModeLeftNavAccessibilityLabel]];
    
    [navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:currentArray[FavouriteModeRightNavIcon]]
                                                                           style:UIBarButtonItemStyleDone
                                                                          target:self.tabBarController.parentViewController action:@selector(toggleListMapAction:)]
                                 animated:YES];
    [navigationItem.rightBarButtonItem setAccessibilityLabel:currentArray[FavouriteModeRightNavAccessibilityLabel]];
    
    BOOL                            shouldBeEditing = currentMode == FavouritesViewControllerManageGroups || currentMode == FavouritesViewControllerManageFavourites;
    FavouritesViewControllerType    previousMode = _currentMode;
    
    _currentMode = currentMode;
    
    [self.tableView setEditing:shouldBeEditing animated:YES];
    
    if (currentMode == FavouritesViewControllerManageGroups || previousMode == FavouritesViewControllerManageGroups)
        [self.tableView reloadData];
    
    if (currentMode == FavouritesViewControllerFavourites)
        [self manageEmptyViewAndNavigationButtons];
}

#pragma mark - User Actions in Nav Bar

- (void)editAction:(id)sender
{
    //do nothing if user swiped left on favourite cell and entered editing mode
    if (self.currentMode == FavouritesViewControllerFavourites && [self.tableView isEditing])
        return;
    if (self.currentMode == FavouritesViewControllerFavourites ||
        self.currentMode == FavouritesViewControllerManageGroups)
        self.currentMode = FavouritesViewControllerManageFavourites;
    else if (self.currentMode == FavouritesViewControllerManageFavourites)
        self.currentMode = FavouritesViewControllerManageGroups;
}

- (BOOL)mapAction:(id)sender
{
    if (self.currentMode == FavouritesViewControllerFavourites)
        return YES;
    else
    {
        [(tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
        self.currentMode = FavouritesViewControllerFavourites;
        return NO;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.currentMode == FavouritesViewControllerFavourites || self.currentMode == FavouritesViewControllerManageFavourites) {
        
        return [[StopList sharedManager] sectionNamesForFavourites].count;
        //        NSInteger noOfSections = [[StopList sharedManager] sectionNamesForFavourites].count;
        //        NSInteger noOfFavourites = [[StopList sharedManager] getFavouriteStopList].count;
        //        return noOfFavourites > 0 ? noOfSections : 0;
    } else
        return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (self.currentMode == FavouritesViewControllerFavourites || self.currentMode == FavouritesViewControllerManageFavourites)
    {
        NSString            * title = [StopList sharedManager].sectionNamesForFavourites[section];
        SectionHeaderView   * view = [SectionHeaderView expandableSectionHeaderViewWithTitle:title
                                                                                     section:section
                                                                                      target:self
                                                                                      action:@selector(didSelectAccessoryForSectionHeader:)];
        
        [view setBackgroundColor:[UIColor colorWithRed:0.941 green:0.941 blue:0.941 alpha:1.0]];
        [view setExpandedState:[self.expandedStates[section] boolValue]];
        return view;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (self.currentMode == FavouritesViewControllerFavourites || self.currentMode == FavouritesViewControllerManageFavourites)
        return 36.0f;
    else
        return 0.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.currentMode == FavouritesViewControllerFavourites || self.currentMode == FavouritesViewControllerManageFavourites)
    {
        if (section < self.expandedStates.count)
            return ![self.expandedStates[section] boolValue] ? 0 : [[StopList sharedManager] countOfFavouritesInSection:section];
        return 0;
    }
    else
    {
        return [[StopList sharedManager] sectionNamesForFavourites].count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FavouriteCell   * cell = nil;
    
    if (self.currentMode == FavouritesViewControllerManageGroups)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"Group Cell" forIndexPath:indexPath];
        NSString * title = [StopList sharedManager].sectionNamesForFavourites[indexPath.row];
        [cell configureWithFavouriteSectionName:title];
    }
    else
    {
        FavouriteStop  * favStop       = [[StopList sharedManager] favouriteStopAtIndex:indexPath];
        NSString * cellIdentifier = favStop.stop.isFTZStop ? @"FavouriteFTZ" : (favStop.name.length ? @"Favourite Cell" : @"Favourite CellTwo");
        
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        //        cell = [tableView dequeueReusableCellWithIdentifier:@"Favourite Cell" forIndexPath:indexPath];
        [cell configureWithStop:favStop mode:self.currentMode];
    }
    return cell;
}

//disable Manage button if user swiped left on favourite cell and entered editing mode
- (void)tableView:(UITableView *)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.currentMode == FavouritesViewControllerFavourites)
        self.tabBarController.parentViewController.navigationItem.leftBarButtonItem.enabled = NO;
}

//enable Manage button if user swiped right on favourite cell and exited editing mode
- (void)tableView:(UITableView*)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (self.currentMode == FavouritesViewControllerFavourites)
        self.tabBarController.parentViewController.navigationItem.leftBarButtonItem.enabled = YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        if (self.currentMode == FavouritesViewControllerManageGroups) {
            [[StopList sharedManager] removeFavouriteSectionAtIndex:indexPath.row];
            
            if (![[StopList sharedManager] sectionNamesForFavourites].count)
                self.tabBarController.parentViewController.navigationItem.title = @"Favourites";
        }
        else
        {
            [[StopList sharedManager] removeFavouriteAtIndexPath:indexPath];
        }
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        if ([[StopList sharedManager] sectionNamesForFavourites].count > 0)
            self.currentMode = self.currentMode;
        else
            self.currentMode = FavouritesViewControllerFavourites;
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationReloadFavouritedMap object:nil];
    }
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    if (self.currentMode == FavouritesViewControllerManageFavourites)
    {
        [[StopList sharedManager] moveFavouriteAtIndexPath:fromIndexPath toIndexPath:toIndexPath];
    }
    else if (self.currentMode == FavouritesViewControllerManageGroups)
    {
        [[StopList sharedManager] moveFavouriteSectionAtIndex:fromIndexPath.row toIndex:toIndexPath.row];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    FavouriteStop                  * currentStop = [[StopList sharedManager] favouriteStopAtIndex:indexPath];
    FavouriteContainerViewController    * vc = (id)self.parentViewController.parentViewController;
    
    [vc pushToPID:currentStop];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    NSString *inputText = [[alertView textFieldAtIndex:0] text];
    return [inputText length] > 0;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex)
    {
        NSInteger i = [[[StopList sharedManager] sectionNamesForFavourites] indexOfObject:self.currentSectionName];
        
        if (i != NSNotFound) {
            [[StopList sharedManager] changeFavouriteSectionNameAtIndex:i toName:[alertView textFieldAtIndex:0].text];
        }
        
        [self.tableView reloadData];
    }
}

@end
