//
//  NearbyViewController.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 20/11/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RightMenuDelegate.h"

@interface NearbyListViewController : UIViewController<StopRightMenuDelegate>

@end
