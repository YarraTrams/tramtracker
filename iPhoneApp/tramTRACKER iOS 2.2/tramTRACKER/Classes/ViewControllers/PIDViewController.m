//
//  PIDViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 21/11/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <FrameAccessor/FrameAccessor.h>

#import "PIDViewController.h"
#import "PIDCell.h"
#import "Filter.h"
#import "PidsService.h"
#import "DisruptionUpdateCell.h"
#import "SectionHeaderView.h"
#import "Constants.h"
#import "Prediction.h"
#import "SpecialMessage.h"
#import "FavouriteAddViewController.h"
#import "RouteFilterViewController.h"
#import "TicketOutletsViewController.h"
#import "myTramSearchViewController.h"
#import "SWRevealViewController.h"
#import "Journey.h"
#import "TTActivityIndicatorView.h"
#import "RightMenuDelegate.h"
#import "Analytics.h"
#import "UIAlertView+Blocks.h"
#import "StopList.h"
#import "myTramViewController.h"

@interface PIDViewController ()<UITableViewDataSource, UITableViewDelegate, PIDRightMenuDelegate>

@property (getter = isFirstExpanded) BOOL               firstExpanded;
@property (getter = isSecondExpanded) BOOL              secondExpanded;
@property (weak, nonatomic) IBOutlet UIImageView        * ftzLogo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ftzRightSpace;

@property (nonatomic, strong) PidsService               * PIDService;
@property (nonatomic, strong) NSTimer                   * timerRefresh;
@property (nonatomic, strong) NSArray                   * currentPredictions;
@property (nonatomic, strong) NSArray                   * currentDisruptions;
@property (nonatomic, strong) NSMutableArray            * currentPredictionsStates;
@property (nonatomic, strong) NSMutableArray            * currentPredictionsExpandable;
@property (nonatomic, strong) UIRefreshControl          * refreshControl;

@property (nonatomic, strong) DisruptionUpdateCell      * updatesCell;
@property (nonatomic, strong) DisruptionUpdateCell      * disruptionsCell;
@property (nonatomic, strong) TTActivityIndicatorView   * activity;

@property (nonatomic, weak) IBOutlet UILabel            * footerTitle;
@property (nonatomic, weak) IBOutlet UILabel            * stopTitle;
@property (nonatomic, weak) IBOutlet UILabel            * stopSubTitle;
@property (nonatomic, weak) IBOutlet UILabel            * stopTrackerID;
@property (nonatomic, weak) IBOutlet UILabel            * stopID;
@property (nonatomic, weak) IBOutlet UITableView        * tableView;
@property (nonatomic, weak) IBOutlet UITableView        * disruptionsTableView;
@property (nonatomic, weak) IBOutlet UIView             * tableViewContainer;
@property (nonatomic, assign, getter = isTTAvailable) BOOL ttAvailable;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *footerTitleHeight;
@property (weak, nonatomic) IBOutlet UIView *footerView;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint * disruptionsUpdatesTableviewHeight;

@property (nonatomic, weak) IBOutlet UIBarButtonItem    * filtersButton;

@property (nonatomic) BOOL upDirection;

@end

@implementation PIDViewController

#pragma mark Utility methods

- (void)dismissReveal:(id)sender
{
    //push the reveal controller if its there
    if (self.parentViewController.revealViewController.frontViewPosition == FrontViewPositionLeftSide)
    {
        [self.parentViewController.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];
    }
}

#pragma mark - Inits & Loads

- (PidsService *)PIDService
{
    if (!_PIDService)
        _PIDService = [[PidsService alloc] initWithDelegate:self];
    return _PIDService;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.currentStop) {
        [[StopList sharedManager] addMostRecentStopID:self.currentStop.trackerID];
        if (!self.currentStop.isFTZStop) {
            self.ftzRightSpace.constant = -(self.ftzLogo.frame.size.width);
            [self.view layoutIfNeeded];
        }
    }

    self.upDirection = [self.currentStop.cityDirection rangeOfString:@"towards City"].location != NSNotFound;
    
    [self.disruptionsTableView registerNib:[UINib nibWithNibName:kCellDisruptionFull
                                                          bundle:[NSBundle mainBundle]]
     
                    forCellReuseIdentifier:kCellDisruptionFull];
    [self.disruptionsTableView registerNib:[UINib nibWithNibName:kCellDisruptionMinimal
                                                          bundle:[NSBundle mainBundle]]
                    forCellReuseIdentifier:kCellDisruptionMinimal];
    
    [self.disruptionsTableView registerNib:[UINib nibWithNibName:kCellUpdateFull
                                                          bundle:[NSBundle mainBundle]]
                    forCellReuseIdentifier:kCellUpdateFull];
    
    [self.disruptionsTableView registerNib:[UINib nibWithNibName:kCellUpdateMinimal
                                                          bundle:[NSBundle mainBundle]]
                    forCellReuseIdentifier:kCellUpdateMinimal];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    
    UITableViewController *tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = self.tableView;
    tableViewController.refreshControl = self.refreshControl;
    
    [self.refreshControl addTarget:self action:@selector(refreshPredictions) forControlEvents:UIControlEventValueChanged];
    
    
    self.filtersButton.target = self.navigationController.tabBarController.revealViewController;
    self.filtersButton.action = @selector(rightRevealToggle:);
    
    UISwipeGestureRecognizer *swipeGesture =
    [[UISwipeGestureRecognizer alloc] initWithTarget:self.parentViewController.revealViewController
                                              action:@selector(rightRevealToggle:)];
    
    swipeGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeGesture];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissReveal:)];
    [tap setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:tap];
    
    // Enabling iOS 7 screen-edge-pan-gesture for pop action
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPIDResetRight object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.currentStop)
    {
        [self updateDisplayedStopInformation];
        self.timerRefresh = [NSTimer scheduledTimerWithTimeInterval:15
                                                             target:self
                                                           selector:@selector(refreshPredictions)
                                                           userInfo:nil
                                                            repeats:YES];
        [self.timerRefresh fire];
    }
    
    if (!self.currentPredictions && !self.activity)
        self.activity = [TTActivityIndicatorView activityIndicatorForView:self.tableViewContainer animated:NO];

    [[Analytics sharedInstance] featureAccessed:AnalyticsFeaturePID withFilter:self.filter.lowFloor ? AnalyticsFilterLowFloor : AnalyticsFilterAll];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self pause];
}

#pragma mark - Notification Handlers

- (void)updateFilter:(Filter *)aFilter
{
    [self.activity startAnimatingAnimated:NO];
    self.filter = aFilter;
    self.currentPredictions = nil;
    [self.tableView reloadData];
    [self.timerRefresh fire];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationRouteFilterApplied object:aFilter];
}

#pragma mark - User Actions on Nav Bar

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Logic

- (void)updateDisplayedStopInformation
{
	// Don't try to update it if we have no stop information
	NSAssert([self currentStop], @"No stop information stored");

	// We have to rebuild the title and subtitle information
	//
	// By default, the stop name looks like: Spring St & Bourke St
	// We move the second street name into the subtitle with the direction
    
	NSString *title = [NSString stringWithString:[[self currentStop] name]];
	NSString *subtitle = [NSString stringWithString:[[self currentStop] displayedCityDirection]];
	
	// If it has an ampersand (&) then it has a secondary street name
	if ([title rangeOfString:@"&"].location != NSNotFound)
	{
		// split it up around the ampersand
		NSArray *pieces = [title componentsSeparatedByString:@"&"];
		
		// take the first piece for the title
		title = [[pieces objectAtIndex:0]
				 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
		subtitle = [NSString stringWithFormat:@"%@ - %@",
					[[pieces objectAtIndex:1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]],
					[[self currentStop] displayedCityDirection]];
	}
    
	//
	// Update the stop information
	//
    
    self.stopTitle.text = title;
    self.stopSubTitle.text = subtitle;
    self.stopTrackerID.text = [[@" " mutableCopy] stringByAppendingString:self.currentStop.trackerID.stringValue];
    self.stopID.text = [[@" " mutableCopy] stringByAppendingString:self.currentStop.number];
}

- (void)pause
{
    [self.timerRefresh invalidate];
    self.timerRefresh = nil;
    [self.PIDService cancel];
    self.PIDService = nil;
}

- (void)refreshPredictions
{
	NSAssert(self.currentStop.trackerID, @"Tracker ID Must be set");

    if (self.filter.route != nil || self.filter.lowFloor)
    {
		[self.PIDService getPredictionsForStop:self.currentStop.trackerID.floatValue
                                         route:self.filter.route
                                  lowFloorOnly:self.filter.lowFloor];
    }
	else
		[self.PIDService getPredictionsForStop:self.currentStop.trackerID.floatValue];
}

- (void)reloadDisruptionsTableViewState:(BOOL)newState indexPath:(NSIndexPath *)indexPath
{
    CGFloat     tableViewHeight = 0.0f;
    
    if (indexPath && indexPath.row < [self tableView:self.tableView numberOfRowsInSection:indexPath.section])
        [self.disruptionsTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    else
        [self.disruptionsTableView reloadData];

    // Calculate new height of updates/disruption tableview
    if (self.currentDisruptions.count > 0)
    {
        tableViewHeight += self.isFirstExpanded ? 98.0f : 30.0f;
        if (self.currentDisruptions.count > 1)
            tableViewHeight += self.isSecondExpanded ? 98.0f : 30.0f;
    }
    
    // Set the height
    self.disruptionsUpdatesTableviewHeight.constant = tableViewHeight;
    [UIView animateWithDuration:0.3f
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}

#pragma mark - Segue Methods

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kSegueShowStopInformation] ||
        [segue.identifier isEqualToString:kSeguePIDToStopMap])
        [segue.destinationViewController setStop:sender];
    else if ([segue.identifier isEqualToString:kSegueShowRouteFilter])
    {
        [segue.destinationViewController setStop:sender direction:self.upDirection];
        [segue.destinationViewController setFilter:self.filter];
    }
    else if ([segue.identifier isEqualToString:kSeguePIDToTimetables])
        [segue.destinationViewController setStop:sender direction:self.upDirection];
    else if ([segue.identifier isEqualToString:kSeguePIDToMyTram])
        [segue.destinationViewController setTramNumberString:sender];
}

#pragma mark - UITableView Data Source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.tableView)
        return [self isEndCell:indexPath] ? 85.0f : 80.0f;
    else
        return (self.isFirstExpanded && !indexPath.row) || (self.isSecondExpanded && indexPath.row) ? 98.0f : 30.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return tableView == self.tableView ? self.currentPredictions.count : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.tableView)
    {
        NSNumber    * value = self.currentPredictionsStates[section];
        
        return value.boolValue ? [self.currentPredictions[section] count] : 1;
    }
    return self.currentDisruptions.count;
}

- (BOOL)isEndCell:(NSIndexPath *)indexPath
{
    BOOL        isExpanded = [self.currentPredictionsStates[indexPath.section] boolValue];
    NSInteger   noOfRowsInSection = [self.currentPredictions[indexPath.section] count];
    NSInteger   noOfSections = self.currentPredictions.count;
    
    return (!isExpanded && !indexPath.row && indexPath.section != noOfSections - 1) ||
    ((isExpanded && indexPath.row == noOfRowsInSection - 1) && (indexPath.section != noOfSections - 1));
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.tableView)
    {
        static NSString * CellIdentifierEnd = @"PIDCellEnd";
        static NSString * CellIdentifier = @"PIDCell";
        
        PIDCell         * cell = [tableView dequeueReusableCellWithIdentifier:[self isEndCell:indexPath] ? CellIdentifierEnd : CellIdentifier];
        PredictionStub  * currentPrediction = self.currentPredictions[indexPath.section][indexPath.row];
        
        [cell configureWithPrediction:currentPrediction target:self action:@selector(didSelectAccessoryWithPrediction:)];
        [cell manageExpandCollapseArrows:[self.currentPredictionsStates[indexPath.section] boolValue]
                                   index:indexPath.row
                                    size:[self.currentPredictions[indexPath.section] count]
                            isExpandable:[self.currentPredictionsExpandable[indexPath.section] boolValue]];
        return cell;
    }
    else
    {
        DisruptionUpdateCell    * cell;
        BOOL                    isExpanded = !indexPath.row ? self.isFirstExpanded : self.isSecondExpanded;
        SpecialMessage          * message = self.currentDisruptions[indexPath.row];
        
        if (message.type == SpecialMessageTypeDisruption)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:isExpanded ? kCellDisruptionFull : kCellDisruptionMinimal];
            self.disruptionsCell = cell;
        }
        else
        {
            cell = [tableView dequeueReusableCellWithIdentifier:isExpanded ? kCellUpdateFull : kCellUpdateMinimal];
            self.updatesCell = cell;
        }
        
        [cell configureWithMessage:message.message];
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0f;
}

- (NSArray *)indexPathArraysForSection:(NSInteger)section
{
    __block NSMutableArray  * array = [NSMutableArray new];
    
    [self.currentPredictions[section] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
     {
         if (idx)
             [array addObject:[NSIndexPath indexPathForRow:idx inSection:section]];
     }];
    return [array copy];
}

#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.tableView)
    {
        if ([self.currentPredictionsExpandable[indexPath.section] boolValue])
        {
            NSNumber    * expanded = self.currentPredictionsStates[indexPath.section];
            PIDCell     *topCell = (id)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:indexPath.section]];
            
            self.currentPredictionsStates[indexPath.section] = @(!expanded.boolValue);
            
            if (expanded.boolValue)
            {
                [tableView deleteRowsAtIndexPaths:[self indexPathArraysForSection:indexPath.section]
                                 withRowAnimation:UITableViewRowAnimationMiddle];
            }
            else
            {
                [tableView insertRowsAtIndexPaths:[self indexPathArraysForSection:indexPath.section]
                                 withRowAnimation:UITableViewRowAnimationMiddle];
                [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:indexPath.section]] withRowAnimation:UITableViewRowAnimationNone];
                [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:indexPath.section]
                                 atScrollPosition:UITableViewScrollPositionTop animated:YES];
            }
            [topCell manageExpandCollapseArrows:[self.currentPredictionsStates[indexPath.section] boolValue]
                                          index:0
                                           size:[self.currentPredictions[indexPath.section] count]
                                   isExpandable:YES];
        }
    }
    else
    {
        BOOL newState;
        
        if (!indexPath.row)
            newState = self.firstExpanded = !self.isFirstExpanded;
        else
            newState = self.secondExpanded = !self.isSecondExpanded;
        
        [self reloadDisruptionsTableViewState:newState indexPath:indexPath];
    }
}

- (void)didSelectAccessoryWithPrediction:(PredictionStub *)prediction
{
    if (prediction.tramStub.number.integerValue > 0) {
        [self performSegueWithIdentifier:kSeguePIDToMyTram sender:[prediction.tramStub.number stringValue]];
    }
}

#pragma mark - PIDsDelegate

- (void)setPredictions:(NSDictionary *)predictionsObject
{
//    [self scheduleUpdateIn15s];

    NSArray * predictions = predictionsObject[@"predictions"];
    NSArray * disruptions = predictionsObject[@"disruptions"];

    self.ttAvailable = [predictionsObject[@"ttAvailable"] boolValue];
    
    if (self.refreshControl.isRefreshing)
        [self.refreshControl endRefreshing];
    
    if (predictionsObject && !self.currentPredictions)
    {
        self.firstExpanded = disruptions.count >= 1;
        self.currentPredictionsStates = [NSMutableArray new];
        self.currentPredictionsExpandable = [NSMutableArray new];
    }
    
    if (predictions.count)
    {
        while (self.currentPredictionsStates.count > predictions.count)
        {
            [self.currentPredictionsStates removeLastObject];
            [self.currentPredictionsExpandable removeLastObject];
        }
        
        for (NSArray * currentPredictions in predictions)
        {
            if (self.currentPredictionsStates.count < predictions.count)
            {
                NSArray * predictionsWithFirstDestination = [currentPredictions filteredArrayUsingPredicate:
                                                             [NSPredicate predicateWithFormat:@"destination = %@", [currentPredictions.firstObject destination]]];
                NSArray * predictionsWithFirstRouteNo = [currentPredictions filteredArrayUsingPredicate:
                                                         [NSPredicate predicateWithFormat:@"routeNo = %@", [currentPredictions.firstObject routeNo]]];
                
                if (predictionsWithFirstDestination.count == predictionsWithFirstRouteNo.count && predictions.count != 1)
                {
                    [self.currentPredictionsStates addObject:@NO];
                    [self.currentPredictionsExpandable addObject:@YES];
                }
                else
                {
                    [self.currentPredictionsStates addObject:@YES];
                    [self.currentPredictionsExpandable addObject:@NO];
                }
            }
        }
    }
    else if (self.filter.lowFloor || self.filter.route)
        [self pidsServiceDidFailWithError:nil];
    
    self.currentPredictions = predictions;
    self.currentDisruptions = disruptions;
    
    [self manageFooterTitleAndIcon];
    [self.tableView reloadData];
    [self reloadDisruptionsTableViewState:NO indexPath:nil];
    
    __weak PIDViewController    * weakSelf = self;
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5f * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [weakSelf.activity stopAnimatingAnimated:YES];
    });
}

- (void)manageFooterTitleAndIcon
{
    if (!self.currentPredictions.count)
    {
        if (self.filter.lowFloor && self.filter.route)
            self.footerTitle.text = [NSString stringWithFormat:@"There may be no low floor trams travelling on route %@.", self.filter.route.number];
        else if (self.filter.lowFloor)
            self.footerTitle.text = @"There may be no low floor trams on the selected route.";
        else if (self.filter.route)
            self.footerTitle.text = [NSString stringWithFormat:@"There may be no trams travelling on route %@.", self.filter.route.number];
        else if (self.currentDisruptions.count && !self.isTTAvailable)
            self.footerTitle.text = @"tramTRACKER real time tram arrival information is unavailable.";
        else
            self.footerTitle.text = @"tramTRACKER had a problem fetching information.";
    }
    else
    {
        if (self.filter.route && self.filter.lowFloor)
            self.footerTitle.text = [NSString stringWithFormat:@"Showing low floor trams on route %@.", self.filter.route.number];
        else if (self.filter.lowFloor)
            self.footerTitle.text = @"Showing low floor trams only.";
        else if (self.filter.route != nil)
            self.footerTitle.text = [NSString stringWithFormat:@"Showing trams on route %@.", self.filter.route.number];
        else if (self.filter.route == nil)
            self.footerTitle.text = @"Showing all trams on all routes.";
    }

    CGFloat height = [self.footerTitle sizeThatFits:CGSizeMake(self.footerTitle.frame.size.width, 1000)].height;
    
    self.footerTitleHeight.constant = height;
    [self.view layoutIfNeeded];
    self.tableView.tableFooterView.height = height + 10;
    self.tableView.tableFooterView = self.tableView.tableFooterView;
}

#pragma mark - PIDRightFilterDelegate

- (void)didSelectPIDAction:(PIDRightMenuType)filterType
{
    if (self.filter == nil)
    {
        self.filter = [[Filter alloc] init];
        self.filter.route = nil;
        self.filter.lowFloor = NO;
    }
    
    switch (filterType)
    {
        case PIDRightMenuAll:
            [self.activity startAnimatingAnimated:YES];
            self.filter.lowFloor = NO;
            self.filter.route = nil;
            self.currentPredictions = nil;
            [self.PIDService cancel];
            [self.timerRefresh fire];
            break;
            
        case PIDRightMenuLowFloorOnly:
            [self.activity startAnimatingAnimated:YES];
            self.filter.route = nil;
            self.filter.lowFloor = YES;
            self.currentPredictions = nil;
            [self.tableView reloadData];
            [self.PIDService cancel];
            [self.timerRefresh fire];
            break;
            
        case PIDRightMenuAddToFavourite:
        {
            if (![self.currentStop routesThroughStop].count)
            {
                [[[UIAlertView alloc] initWithTitle:@"Unable to add Stop to Favourites"
                                            message:@"This Stop cannot be added to your Favourites as there are no routes connected."
                                           delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil]
                 show];
                return;
            }
            
            UIStoryboard                * sb = [UIStoryboard storyboardWithName:kMainStoryboard bundle:nil];
            FavouriteAddViewController  * vc = [sb instantiateViewControllerWithIdentifier:kScreenAddFavourites];
            
            [vc setStop:self.currentStop direction:self.upDirection];
            [vc setFilter:self.filter];
            [self.navigationController pushViewController:vc animated:NO];
            break;
        }
        case PIDRightMenuStopInformation:
            [self performSegueWithIdentifier:kSegueShowStopInformation sender:self.currentStop];
            break;
            
        case PIDRightMenuRouteFilter:
        {
            [self performSegueWithIdentifier:kSegueShowRouteFilter sender:self.currentStop];
            break;
        }
        case PIDRightMenuScheduledDepartures:
        {
            [self performSegueWithIdentifier:kSeguePIDToTimetables sender:self.currentStop];
            break;
        }
        case PIDRightMenuTicketOutlets:
        {
            UIStoryboard                * sb = [UIStoryboard storyboardWithName:kMainStoryboard bundle:nil];
            TicketOutletsViewController * vc = [sb instantiateViewControllerWithIdentifier:kScreenTicketOutlet];
            
            [vc setStop:self.currentStop];
            [self.navigationController pushViewController:vc animated:NO];
            break;
        }
            
        case PIDRightMenuDirections:
        {
            __weak PIDViewController    * weakSelf = self;
            
            [[UIAlertView showWithTitle:@"Open in Maps?"
                                message:@"Would you like to close the tramTracker® app and launch Maps?"
                      cancelButtonTitle:@"Cancel"
                      otherButtonTitles:@[@"Launch Maps"]
                               tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                   if (buttonIndex)
                                   {
                                       [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureDirections];
                                       MKPlacemark * placemark = [[MKPlacemark alloc] initWithCoordinate:weakSelf.currentStop.coordinate addressDictionary:nil];
                                       MKMapItem   * item = [[MKMapItem alloc] initWithPlacemark:placemark];
                                       
                                       item.name = weakSelf.currentStop.name;
                                       [item openInMapsWithLaunchOptions:nil];
                                   }
                               }]
             show];
            break;
        }
        case PIDRightMenuStopMap:
            [self performSegueWithIdentifier:kSeguePIDToStopMap sender:self.currentStop];
            break;
    }

    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2f * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self.parentViewController.revealViewController rightRevealToggle:nil];
    });
}

- (void)pidsServiceDidFailWithError:(NSError *)error
{
    if (!error.code && (self.filter.lowFloor || self.filter.route))
    {
        self.currentPredictions = nil;
        self.currentDisruptions = nil;
        self.currentPredictionsExpandable = nil;
        self.currentPredictionsStates = nil;
        
        [self reloadDisruptionsTableViewState:NO indexPath:nil];
        [self.tableView reloadData];
        
        [self manageFooterTitleAndIcon];
        [self.activity stopAnimatingAnimated:YES];
    }
    else if (error.code >= 1000 || error.code <= -1000)
    {
        self.currentPredictions = nil;
        self.currentDisruptions = nil;
        self.currentPredictionsExpandable = nil;
        self.currentPredictionsStates = nil;
        
        [self.tableView reloadData];

        if (self.refreshControl.isRefreshing)
            [self.refreshControl endRefreshing];
        self.footerTitle.text = @"Unable to contact the tramTRACKER service. Please make sure that your device is connected to the internet and try again.";

        CGFloat height = [self.footerTitle sizeThatFits:CGSizeMake(self.footerTitle.frame.size.width, 1000)].height;

        self.footerTitleHeight.constant = height;
        [self.view layoutIfNeeded];
        self.tableView.tableFooterView.height = height + 10;
        self.tableView.tableFooterView = self.tableView.tableFooterView;
        [self.activity stopAnimatingAnimated:YES];
    }
    
    
    if (self.currentPredictions.count) {
        NSInteger lastSection = self.currentPredictions.count - 1;
        BOOL isSectionShown = [self.currentPredictionsStates[lastSection] boolValue];
        NSInteger row = isSectionShown ? [self.currentPredictions[lastSection] count] - 1 : 0;

        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:lastSection] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

@end
