//
//  FTZViewController.m
//  tramTracker
//
//  Created by Hugo Cuvillier on 10/12/2014.
//  Copyright (c) 2014 AppsCore. All rights reserved.
//

#import "FTZViewController.h"

@interface FTZViewController ()
@property (weak, nonatomic) IBOutlet UITextView *firstTV;
@property (weak, nonatomic) IBOutlet UITextView *secondTV;
@property (unsafe_unretained, nonatomic) IBOutlet NSLayoutConstraint *ftzTopSpace;
@property BOOL isLink;

@end

@implementation FTZViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Enabling iOS 7 screen-edge-pan-gesture for pop action
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        self.ftzTopSpace.constant += 8;
    }
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)ftzAction:(id)sender {
    self.isLink = NO;
    [[[UIAlertView alloc] initWithTitle:@"Open in Safari?"
                                message:@"Would you like to close the tramTracker® app and launch Safari?"
                               delegate:self
                      cancelButtonTitle:@"Cancel"
                      otherButtonTitles:@"OK", nil]
     show];
    
}

- (IBAction)linkAction:(id)sender {
    self.isLink = YES;
    [[[UIAlertView alloc] initWithTitle:@"Open in Safari?"
                                message:@"Would you like to close the tramTracker® app and launch Safari?"
                               delegate:self
                      cancelButtonTitle:@"Cancel"
                      otherButtonTitles:@"OK", nil]
     show];
    
}

#pragma mark - Alertview Delegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        if (self.isLink) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://yarratrams.com.au"]];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://ptv.vic.gov.au"]];
        }
    }
}

@end
