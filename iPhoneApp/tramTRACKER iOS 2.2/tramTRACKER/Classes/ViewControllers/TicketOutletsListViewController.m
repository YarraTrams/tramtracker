//
//  TicketOutletsListViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 12/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "TicketOutletsListViewController.h"
#import "SectionHeaderView.h"
#import "NearbyCell.h"
#import "Constants.h"
#import "TicketRetailer.h"
#import "Stop.h"
#import "Analytics.h"
#import "Settings.h"

@interface TicketOutletsListViewController ()

@property (strong, nonatomic) NSArray           * data;
@property (strong, nonatomic) NSMutableArray    * dataStates;
@property (strong, nonatomic) Stop              * stop;

@end

@implementation TicketOutletsListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    if (!self.stop)
        self.data = [TicketRetailer allTicketRetailersGroupedBySuburbs];
    else
        self.data = @[[TicketRetailer nearestTicketRetailersToLocation:self.stop.location count:[Settings showNearbyStops]]];

    self.dataStates = [NSMutableArray new];
    for (NSInteger i = 0; i < self.data.count; ++i)
        [self.dataStates addObject:@NO];
    [self.tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.stop)
        [[Analytics sharedInstance] featureAccessed:AnalyticsFeaturePIDNearbyTicketOutlet];
    else
        [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureOutletList];
}

- (void)didSelectAccessoryForSectionHeader:(SectionHeaderView *)sectionHeader
{
    NSInteger       dataSection = sectionHeader.section;
    
    self.dataStates[dataSection] = @(![self.dataStates[dataSection] boolValue]);
    
    NSMutableArray  * indexPaths = [NSMutableArray new];
    for (NSInteger i = 0; i < [self.data[dataSection] count]; ++i)
        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:dataSection]];
    
    if (![self.dataStates[dataSection] boolValue])
        [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationMiddle];
    else
        [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationMiddle];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.data.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [self.dataStates[section] boolValue] || self.stop ? [self.data[section] count] : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * CellIdentifier = @"ticketCell";
    NearbyCell      * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    TicketRetailer  * ticketRetailer;
    NSString        * distance;
    
    if (self.stop)
    {
        TicketRetailerDistance  * retailerDistance = self.data[0][indexPath.row];
        
        ticketRetailer = retailerDistance.retailer;
        distance = [retailerDistance formattedDistance];
    }
    else
        ticketRetailer = self.data[indexPath.section][indexPath.row];

    [cell configureWithTicketRetailer:ticketRetailer distance:distance];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 36.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SectionHeaderView * view;
    
    if (self.stop)
        view = [SectionHeaderView nearbySectionHeaderWithTitle:@"Ticket Outlets"];
    else
    {
        view = [SectionHeaderView expandableSectionHeaderViewWithTitle:[self.data[section][0] suburb]
                                                          section:section
                                                           target:self
                                                           action:@selector(didSelectAccessoryForSectionHeader:)];
        [view setExpandedState:[self.dataStates[section] boolValue]];
    }
    return view;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (!self.stop)
        [self.tabBarController.parentViewController performSegueWithIdentifier:kSegueShowTicketRetailer sender:self.data[indexPath.section][indexPath.row]];
    else
        [self.tabBarController.parentViewController performSegueWithIdentifier:kSegueShowTicketRetailer sender:[self.data[indexPath.section][indexPath.row] retailer]];
}

@end
