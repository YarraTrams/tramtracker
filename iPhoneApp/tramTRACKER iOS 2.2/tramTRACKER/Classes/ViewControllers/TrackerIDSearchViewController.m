//
//  TrackerIDSearchViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 10/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "TrackerIDSearchViewController.h"
#import "StopList.h"
#import "Constants.h"
#import "PIDViewController.h"
#import "Analytics.h"

@interface TrackerIDSearchViewController ()

@property (weak, nonatomic) IBOutlet UISearchBar        * searchField;
@property (weak, nonatomic) IBOutlet UIView             * bottomView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem    * goButton;

@end

@implementation TrackerIDSearchViewController

#pragma mark - User Actions on Nav Bar

-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    self.goButton.enabled = self.searchField.text.length > 0;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    UITapGestureRecognizer  * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self.searchField action:@selector(resignFirstResponder)];

    [self.bottomView addGestureRecognizer:tapGesture];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
    }
    
    self.goButton.enabled = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureEnterTrackerID];
}

- (IBAction)goAction:(UIBarButtonItem *)barButtonItem
{
    [self.searchField resignFirstResponder];

    if (!self.searchField.text.length)
        return;
    
    Stop *stop = [Stop stopForTrackerID:@([self.searchField.text intValue])];

    if (stop == nil || [self.searchField.text integerValue] >= 8000)
    {
        /* Alert to say the stop doesn't exist */

        NSString *strMessage =[[NSString alloc] initWithFormat:NSLocalizedString(@"entertrackerid-stopnotfound", "Stop Not Found Message"), self.searchField.text];
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:strMessage delegate:nil
                                               cancelButtonTitle:NSLocalizedString(@"entertrackerid-error-okbutton", "OK") otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else
    {
        UIStoryboard        * sb = [UIStoryboard storyboardWithName:kMainStoryboard bundle:nil];
        PIDViewController   * vc = [sb instantiateViewControllerWithIdentifier:kScreenPID];

        [vc setCurrentStop:stop];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - SearchBar Delegate 

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [self.searchField resignFirstResponder];
}

@end
