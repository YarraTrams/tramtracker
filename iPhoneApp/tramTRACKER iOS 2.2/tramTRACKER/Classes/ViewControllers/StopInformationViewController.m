//
//  StopInformationViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 4/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "StopInformationViewController.h"
#import "StopInformationPOICell.h"
#import "PointOfInterest.h"
#import "Analytics.h"

@interface StopInformationViewController ()<UITableViewDataSource, UITableViewDelegate>

typedef enum
{
    StopInformationViewControllerSectionInfo,
    StopInformationViewControllerSectionConnections
}   StopInformationViewControllerSection;

typedef enum
{
    StopInformationViewControllerSectionConnectionsTrain,
    StopInformationViewControllerSectionConnectionsTram,
    StopInformationViewControllerSectionConnectionsBus
}   StopInformationViewControllerSectionConnectionsType;

@property (strong, nonatomic) IBOutlet UIScrollView * scrollView;
@property (weak, nonatomic) IBOutlet UITableView    * tableView;
@property (weak, nonatomic) IBOutlet UILabel        * stopTitle;
@property (weak, nonatomic) IBOutlet UILabel        * connectingTrains;
@property (weak, nonatomic) IBOutlet UILabel        * connectingTrams;
@property (weak, nonatomic) IBOutlet UILabel        * connectingBuses;
@property (nonatomic, strong) NSArray * pois;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;
@property (weak, nonatomic) IBOutlet UILabel *lblPOI;

@end

@implementation StopInformationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (self.navigationController.childViewControllers.count > 1)
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Nav-Back"]
                                                                                 style:UIBarButtonItemStyleBordered
                                                                                target:self
                                                                                action:@selector(backAction:)];

    self.stopTitle.text = self.stop.formattedName;
    self.connectingBuses.text = [self textForConnection:self.stop.connectingBusesList];
    self.connectingTrains.text = [self textForConnection:self.stop.connectingTrainsList];
    self.connectingTrams.text = [self textForConnection:self.stop.connectingTramsList];

    self.lblPOI.hidden = self.tableView.hidden = !self.pois.count;
    self.tableView.scrollsToTop = YES;
}

- (void)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSArray *)pois
{
    if (!_pois)
        _pois = [PointOfInterest poisForIDs:self.stop.poisThroughStop];
    return _pois;
}

- (IBAction)launchDirections:(id)sender
{
    [[[UIAlertView alloc] initWithTitle:@"Open in Maps?"
                                message:@"Would you like to close the tramTracker® app and launch Maps?"
                               delegate:self
                      cancelButtonTitle:@"Cancel"
                      otherButtonTitles:@"Launch Maps", nil]
     show];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex)
    {
        [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureDirections];
        CLLocationDegrees latitude = [self.stop.latitude doubleValue];
        CLLocationDegrees longitude = [self.stop.longitude doubleValue];

        MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:CLLocationCoordinate2DMake(latitude, longitude) addressDictionary:nil];
        MKMapItem *item = [[MKMapItem alloc] initWithPlacemark:placemark];

        item.name = self.stop.name;
        [item openInMapsWithLaunchOptions:nil];
    }
}

- (NSString *)textForConnection:(NSString *)textForConnection
{
    return !textForConnection || [textForConnection isEqualToString:@""] ? @"None" : textForConnection;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSInteger tableViewHeight = 0;
    
    for (PointOfInterest *curPoi in self.pois) {
        tableViewHeight += [curPoi heightInCell];
    }

    self.tableViewHeight.constant = tableViewHeight + 15;
    [self.view layoutIfNeeded];

    [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureStopInformation];
}

#pragma mark - User Actions

- (IBAction)makeCall:(id)sender
{
    if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt:1800800007"]])
    {
        [[[UIAlertView alloc] initWithTitle:@"Unable to Connect Call"
                                    message:@"Please make sure that your device is configured to make calls or check that your SIM card is inserted correctly."
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil, nil]
         show];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.pois.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString     * CellIdentifier     = @"POICell";
    StopInformationPOICell * cell = (id)[tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

    [cell configureWithPointOfInterest:self.pois[indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self.pois[indexPath.row] heightInCell];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
