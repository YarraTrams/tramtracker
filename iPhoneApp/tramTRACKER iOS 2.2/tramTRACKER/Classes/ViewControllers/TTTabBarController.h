//
//  TTTabBarController.h
//  tramTRACKER
//
//  Created by Apscore Mac Mini 3 on 9/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTTabBarController : UITabBarController

enum
{
    kIndexForMap,
    kIndexForList
};

- (void)toggleListMapAction:(UIBarButtonItem *)sender isRight:(BOOL)isRight;
- (void)setSelectedIndex:(NSUInteger)selectedIndex animated:(BOOL)value;

@end
