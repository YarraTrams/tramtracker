//
//  OpenWithViewController.h
//  tramTRACKER
//
//  Created by Raji on 20/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OpenWithViewController : UITableViewController

typedef enum
{
    OpenWithMode,
    SynchroniseMode,
    ShowNearbyStopsMode,
    ShowMostRecentMode
}SettingsOptionSelectionMode;

-(void) setCurrentMode:(SettingsOptionSelectionMode)currentMode andSelectedString:(NSString*)selectedString;

@end
