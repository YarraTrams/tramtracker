//
//  TimetablesViewController.h
//  tramTRACKER
//
//  Created by Raji on 24/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimetablesViewController : UITableViewController

- (void)setCurrentRoute:(Route *)aRoute;
- (void)setStop:(Stop *)aStop direction:(BOOL)upDirection;

@end
