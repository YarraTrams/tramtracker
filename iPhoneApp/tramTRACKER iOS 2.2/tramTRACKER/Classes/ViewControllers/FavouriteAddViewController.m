//
//  AddToFavouritesViewController.m
//  tramTRACKER
//
//  Created by Raji on 4/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "FavouriteAddViewController.h"
#import "FavouriteGroupsViewController.h"
#import "FavouriteGroupCell.h"
#import "FavouriteStop.h"
#import "Stop.h"
#import "Filter.h"//This is needed when we pass current filter from PID when adding favourite
#import "Constants.h"
#import "StopList.h"
#import "Analytics.h"
#import "tramTRACKERAppDelegate.h"

@interface FavouriteAddViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField        * name;
@property (weak, nonatomic) IBOutlet UILabel            * group;

@property (strong, nonatomic) Stop                      * stop;
@property (strong, nonatomic) Route                     * selectedRoute;

@property (strong, nonatomic) NSArray                   * allRoutesThroughThisStop;
@property (strong, nonatomic) FavouriteStop             * favouriteToEdit;
@property (assign, nonatomic) NSInteger                 section;

@property (nonatomic) BOOL  isAllRoutesSelected;
@property (nonatomic) BOOL  isLowFloorOnly;
@property (nonatomic) BOOL  upDirection;

@property (weak, nonatomic) IBOutlet UITableView        * tableView;
@property (weak, nonatomic) IBOutlet UIView             * headerView;
@property (weak, nonatomic) IBOutlet UIScrollView       * scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * tableViewHeight;
@property (weak, nonatomic) IBOutlet UIView *topHeader;

@property (weak, nonatomic) IBOutlet UILabel *trackerID;
@property (weak, nonatomic) IBOutlet UILabel *stopNumber;
@property (weak, nonatomic) IBOutlet UILabel *subTitle;
@property (weak, nonatomic) IBOutlet UILabel *stopTitle;

typedef enum
{
    FavouriteAddLowFloor,
    FavouriteAddAllRoutes,
    FavouriteAddRoutes
    
}  FavouriteAddRows;

@end

@implementation FavouriteAddViewController

NSString * const kNotificationReloadFavourited = @"reloadFavourites";
NSString * const kNotificationReloadFavouritedMap = @"reloadFavouritesMap";
NSString * const kFavouriteRouteCellIdentifier = @"FavouriteRouteCell";

#pragma mark - Segue Methods

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [self.name resignFirstResponder];

    if ([segue.identifier isEqualToString:kSegueEditGroups])
        [segue.destinationViewController setCurrentSection:self.favouriteToEdit ? self.favouriteToEdit.indexPath.section : self.section];
}

#pragma mark - Utility Methods

- (void)setStop:(Stop *)aStop direction:(BOOL)upDirection
{
    self.stop = aStop;
    self.upDirection = upDirection;
}

#pragma mark - Inits & Loads

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureAddToFavourite];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    if (self.favouriteToEdit)   //Edit favourite stop
    {
        self.name.text              = self.favouriteToEdit.name;
        self.group.text             = [[StopList sharedManager] sectionNamesForFavourites][self.favouriteToEdit.indexPath.section];
        self.section                = self.favouriteToEdit.indexPath.section;
        self.stop                   = self.favouriteToEdit.stop;
        self.selectedRoute          = self.favouriteToEdit.filter.route;
        self.isAllRoutesSelected    = !self.favouriteToEdit.filter.route;
        self.isLowFloorOnly         = self.favouriteToEdit.filter.lowFloor;
        self.title                  = @"Edit Favourite";
    }
    else                        //add stop to favourites
    {
        self.section = [[StopList sharedManager] indexOfDefaultSection];
        self.isLowFloorOnly         = self.filter.lowFloor;
        self.selectedRoute          = self.filter.route;
        self.isAllRoutesSelected    = !self.filter.route;
    }

	// We have to rebuild the title and subtitle information
	//
	// By default, the stop name looks like: Spring St & Bourke St
	// We move the second street name into the subtitle with the direction

	NSString *title = [NSString stringWithString:[[self stop] name]];
	NSString *subtitle = [NSString stringWithString:[[self stop] displayedCityDirection]];

	// If it has an ampersand (&) then it has a secondary street name
	if ([title rangeOfString:@"&"].location != NSNotFound)
	{
		// split it up around the ampersand
		NSArray *pieces = [title componentsSeparatedByString:@"&"];
		
		// take the first piece for the title
		title = [[pieces objectAtIndex:0]
				 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
		subtitle = [NSString stringWithFormat:@"%@ - %@",
					[[pieces objectAtIndex:1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]],
					[[self stop] displayedCityDirection]];
	}
    
	//
	// Update the stop information
	//
    
    self.stopTitle.text = title;
    self.subTitle.text = subtitle;
    
    self.trackerID.text = [[@" " mutableCopy] stringByAppendingString:self.stop.trackerID.stringValue];
    self.stopNumber.text = [[@" " mutableCopy] stringByAppendingString:self.stop.number];
    
    self.allRoutesThroughThisStop = [self.stop routesThroughStop];
    self.tableViewHeight.constant = (self.allRoutesThroughThisStop.count + 2) * 44;
    
    [self.headerView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self.name action:@selector(resignFirstResponder)]];
    [self.topHeader addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self.name action:@selector(resignFirstResponder)]];

    //Notification sent from FavouriteGroups, when returning from Favourite Groups, to update the group selected
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveNotificationGroupSelected:)
                                                 name:kNotificationGroupSelected
                                               object:nil];
   
    //Notification to change scrollView contentsize when keyboard is shown
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    //Notification to change scrollView contentsize when keyboard is gone
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    
    

}

- (void)setFavouriteToEdit:(FavouriteStop *)favouriteToEdit upDirection:(BOOL)upDirection
{
    _favouriteToEdit = favouriteToEdit;
    
    NSNumber * trackerID = favouriteToEdit.stop.trackerID;
    
    
    NSInteger up = 0;
    NSInteger down = 0;
    
    for (NSString *routeNo in favouriteToEdit.stop.routesThroughStop) {
        
        Route * route = [Route routeWithNumber:routeNo];
        
        if ([route.upStops containsObject:trackerID]) {
            up++;
        } else {
            down++;
        }
    }

    self.upDirection = up >= down;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Notification Handlers

- (void)didReceiveNotificationGroupSelected:(NSNotification *)notification
{
    self.section = [notification.object integerValue];

    NSArray * sections = [[StopList sharedManager] sectionNamesForFavourites];
    
    if (self.section >= 0 && self.section < sections.count)
        self.group.text = sections[self.section];
    else
        self.group.text = @"Add to Favourites Group";
}

- (void)keyboardWillShow:(NSNotification*)notification
{
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameRect = [keyboardFrameBegin CGRectValue];
    
    CGRect rec = self.scrollView.frame;
    rec.size.height = rec.size.height - keyboardFrameRect.size.height + 40;
    
    [UIView animateWithDuration:0.25f animations:^{
        self.scrollView.frame = rec;
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameRect = [keyboardFrameBegin CGRectValue];
    
    CGRect rec = self.scrollView.frame;
    rec.size.height = rec.size.height + keyboardFrameRect.size.height -40;
    
    [UIView animateWithDuration:0.25f animations:^{
        self.scrollView.frame = rec;
    }];
}

#pragma mark - User Actions in Nav Bar

-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)doneAction:(id)sender
{
    if (self.selectedRoute == nil && self.isAllRoutesSelected == NO)
    {
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Unable to add Stop to Favourites"
                                                         message:@"Please select at least one route to add the Stop to your Favourites."
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if (self.favouriteToEdit)
    {
        Filter * f = [Filter filterForRoute:self.selectedRoute lowFloor:self.isLowFloorOnly];
        
        if (self.section < 0) {
            self.section = [[StopList sharedManager] indexOfDefaultSection];
        }
        
        NSIndexPath * toIndexPath = self.favouriteToEdit.indexPath;
        
        if (self.section != self.favouriteToEdit.indexPath.section) {
            toIndexPath = [NSIndexPath indexPathForRow:[[StopList sharedManager] countOfFavouritesInSection:self.section]
                                                           inSection:self.section];
            [[StopList sharedManager] moveFavouriteAtIndexPath:self.favouriteToEdit.indexPath
                                                   toIndexPath:toIndexPath];
        }

        [[StopList sharedManager] setFilter:f forFavouriteAtIndexPath:toIndexPath];
        [[StopList sharedManager] setName:self.name.text forFavouriteAtIndexPath:toIndexPath];
    }
    else
    {
        FavouriteStop * favouriteStop = [[StopList sharedManager] addFavouriteStopID:self.stop.trackerID inSection:self.section < 0 ? NSNotFound : self.section withName:self.name.text];

        [favouriteStop setFilter:[Filter filterForRoute:self.selectedRoute lowFloor:self.isLowFloorOnly]];
        [[StopList sharedManager] setFilter:favouriteStop.filter forFavouriteAtIndexPath:favouriteStop.indexPath];
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationReloadFavourited object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationReloadFavouritedMap object:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Text Field Delegate Methods

- (IBAction)textFieldDidEndOnExit:(id)sender
{
    [self.name resignFirstResponder];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.allRoutesThroughThisStop.count + 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FavouriteGroupCell  * cell;
    NSString            * cellText;
    BOOL                isSelected = self.isAllRoutesSelected;
    
    switch (indexPath.row)
    {
        case FavouriteAddLowFloor:
            cellText = @"Low floor only";
            isSelected = self.isLowFloorOnly;
            break;
            
        case FavouriteAddAllRoutes:
            cellText = @"All routes";
            break;
            
        default:
        {
            NSString    * routeNo = self.allRoutesThroughThisStop[indexPath.row - 2];
            Route       * route = [Route routeWithNumber:routeNo];

            cellText = [route routeNameWithNumberAndDestinationUp:[route.upStops containsObject:self.stop.trackerID]];
            isSelected = [self.selectedRoute.number isEqualToString:routeNo] || self.isAllRoutesSelected;
        }
    }

    cell = (id)[tableView dequeueReusableCellWithIdentifier:kFavouriteRouteCellIdentifier];
    [cell configureWithString:cellText isSelected:isSelected];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.name resignFirstResponder];
    
    switch (indexPath.row) {
        case FavouriteAddLowFloor:
            self.isLowFloorOnly = !self.isLowFloorOnly;
            break;
        case FavouriteAddAllRoutes:
            self.isAllRoutesSelected = !self.isAllRoutesSelected;
            self.selectedRoute = nil;
            break;
        default:
        {
            Route * newRoute = [Route routeWithNumber:self.allRoutesThroughThisStop[indexPath.row - 2]];
            
            self.selectedRoute = newRoute == self.selectedRoute ? nil : newRoute;
            self.isAllRoutesSelected = NO;
        }
    }
    [self.tableView reloadData];
    
}

@end
