//
//  HiddenTabBarViewController.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 28/01/2014.
//  Copyright (c) 2014 AppsCore. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HiddenTabBarViewController : UITabBarController

@end
