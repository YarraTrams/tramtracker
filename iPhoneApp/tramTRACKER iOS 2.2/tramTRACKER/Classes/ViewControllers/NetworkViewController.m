//
//  NetworkViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 17/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "NetworkViewController.h"
#import "tramTRACKERAppDelegate.h"
#import "Analytics.h"

@interface NetworkViewController ()<UIWebViewDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) UIImage *image;

@end

@implementation NetworkViewController

static NSString    * kUserKeyTimeMap = @"timeMapDownloaded";

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.webView.delegate = self;
    
    self.webView.scalesPageToFit = YES;
    
    NSString    *fileName = [[NSBundle mainBundle] pathForResource:@"network_map_201501010000" ofType:@"png"];
    NSArray     * maps = [[NSUserDefaults standardUserDefaults] objectForKey:@"maps"];
    
    for (NSDictionary * map in [maps reverseObjectEnumerator]) {
        if (map) {
            NSDate      * currentDate = map[@"ActiveDate"];
            
            NSString * currentPath = [[tramTRACKERAppDelegate applicationDocumentsDirectory] stringByAppendingPathComponent:map[@"FileName"]];
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:currentPath] &&
                [currentDate compare:[NSDate date]] == NSOrderedAscending) {
                fileName = currentPath;
            }
        }
    }
    
    NSString * header = [NSString stringWithFormat:@"<meta name='viewport' content='width=%d; initial-scale=0.1; maximum-scale=1.0; user-scalable=1;'/>", (NSInteger)self.webView.frame.size.width];
    NSString * html = [NSString stringWithFormat:@"<html>%@<img src='file://%@'/></html>", header, fileName];
    
    [self.webView loadHTMLString:html baseURL:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureNetwork];
}

#pragma mark - User Actions on Nav Bar

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
