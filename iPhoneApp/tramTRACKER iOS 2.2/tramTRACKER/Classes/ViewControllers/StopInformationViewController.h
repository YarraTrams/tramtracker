//
//  StopInformationViewController.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 4/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Stop.h"

@interface StopInformationViewController : UIViewController

@property (strong, nonatomic) Stop * stop;

@end
