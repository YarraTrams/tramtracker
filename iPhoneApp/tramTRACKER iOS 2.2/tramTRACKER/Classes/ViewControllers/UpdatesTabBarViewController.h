//
//  UpdatesTabBarViewController.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 3/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTTabBarController.h"

@interface UpdatesTabBarViewController : TTTabBarController

@end
