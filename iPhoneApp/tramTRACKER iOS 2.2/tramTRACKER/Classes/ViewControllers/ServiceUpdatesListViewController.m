//
//  ServiceUpdatesListViewController.m
//  tramTRACKER
//
//  Created by Raji on 20/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "ServiceUpdatesListViewController.h"
#import "ServiceUpdatesDetailViewController.h"
#import "Constants.h"
#import "ServiceChangesService.h"
#import "ServiceChange.h"
#import "TTActivityIndicatorView.h"
#import "UIAlertView+Blocks.h"
#import "Analytics.h"

@interface ServiceUpdatesListViewController ()

@property (nonatomic, strong) NSArray                   * serviceUpdatesList;
@property (nonatomic, strong) ServiceChangesService     * service;
@property (nonatomic, strong) TTActivityIndicatorView   * activity;

@end

@implementation ServiceUpdatesListViewController

#define kTitle          @"title"
#define kDate           @"date"
#define kDescription    @"description"

#pragma mark - Segue Methods

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(ServiceChange *)sender
{
    if (![[segue identifier] isEqualToString:kSegueUnwindFromServiceUpdates])
        [segue.destinationViewController setServiceChange:sender];
}

#pragma mark - Inits & Loads

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.service = [[ServiceChangesService alloc] initWithDelegate:self action:@selector(setUpdates:)];
    [self.service getServiceChangesInBackgroundThread];
    
    self.activity = [TTActivityIndicatorView activityIndicatorForView:self.view animated:NO];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureServiceUpdatesList];
}

- (void)serviceChangesServiceDidFailWithError:(NSError *)error
{
    __weak ServiceUpdatesListViewController * weakSelf = self;
    
    [[UIAlertView showWithTitle:@"Unable to load content"
                       message:@"Please make sure that your device is connected to the internet and try again."
             cancelButtonTitle:@"OK"
             otherButtonTitles:nil tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                 [weakSelf.navigationController popViewControllerAnimated:YES];
             }]
     show];
}

- (void)setUpdates:(NSArray *)updates
{
    [self.activity stopAnimatingAnimated:YES];
    self.serviceUpdatesList = updates;
    [self.tableView reloadData];
}

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.serviceUpdatesList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * CellIdentifier = @"Cell";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    ServiceChange   * serviceChange = self.serviceUpdatesList[indexPath.row];

    cell.textLabel.text = serviceChange.title;
    cell.detailTextLabel.text = serviceChange.dateString;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:kSegueShowUpdatesDetailed sender:self.serviceUpdatesList[indexPath.row]];
}

@end
