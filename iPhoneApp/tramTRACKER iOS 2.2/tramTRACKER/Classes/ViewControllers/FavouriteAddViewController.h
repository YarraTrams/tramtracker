//
//  AddToFavouritesViewController.h
//  tramTRACKER
//
//  Created by Raji on 4/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Filter;
@class FavouriteStop;

@interface FavouriteAddViewController : UIViewController

extern NSString * const kNotificationGroupSelected;
extern NSString * const kNotificationReloadFavourited;
extern NSString * const kNotificationReloadFavouritedMap;

@property (strong, nonatomic) Filter    * filter;

// Call this method to Edit a Favourite Stop
- (void)setFavouriteToEdit:(FavouriteStop *)favouriteToEdit upDirection:(BOOL)upDirection;

// Call this method to Add Stop to Favourites list
- (void)setStop:(Stop *)aStop direction:(BOOL)upDirection;

@end
