//
//  WebViewController.m
//  tramTRACKER
//
//  Created by Alex Louey on 17/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import <FrameAccessor/FrameAccessor.h>

#import "WebViewController.h"

@interface WebViewController ()<UIWebViewDelegate>{

    UIBarButtonItem * historyBack;
    UIBarButtonItem * historyForward;
    UIBarButtonItem * refresh;
    
    UIActivityIndicatorView * indicator;
    
    

}

@property(nonatomic,strong) UIToolbar * toolsView;
@property(nonatomic,strong) UIWebView * webView;

@end

@implementation WebViewController

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    
    self.webView.delegate = self;
    
    self.webView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.webView.scalesPageToFit = YES;

    [self.view addSubview:self.webView];
    
    if (self.urlStr) {
        self.urlStr = self.urlStr;
    }

    
    self.toolsView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.webView.height -40, 320, 40)];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        [self.toolsView setBarTintColor:[UIColor colorWithRed:198/256.0 green:198/256.0 blue:198/256.0 alpha:1.0]];
    [self.toolsView setTintColor:[UIColor colorWithRed:79/256.0 green:79/256.0 blue:79/256.0 alpha:1.0]];
    [self.view addSubview:self.toolsView];
    
    self.toolsView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    
    
    historyBack = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"historyBack"] style:UIBarButtonItemStylePlain target:self action:@selector(historyBack:)];
    
    UIBarButtonItem *fix = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:self action:nil];
    [fix setWidth:50 ];
    
    historyForward = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"historyForward"] style:UIBarButtonItemStylePlain target:self action:@selector(historyForward:)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    refresh = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btn_refresh"] style:UIBarButtonItemStylePlain target:self action:@selector(refresh:)];
  

    [self.toolsView setItems:@[fix, historyBack,fix, historyForward, flex, refresh, fix]];

    UIBarButtonItem * back = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Nav-Back"]
                                                              style:UIBarButtonItemStylePlain
                                                             target:self
                                                             action:@selector(backAction:)];
    
    [self.navigationItem setLeftBarButtonItem:back];
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button action 
- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)historyBack:(id)sender{
    if ([self.webView canGoBack]) {
        [self.webView goBack];
    }
}

-(IBAction)historyForward:(id)sender{
    if ([self.webView canGoForward]) {
        [self.webView goForward];
    }
}

-(IBAction)refresh:(id)sender{
    [self.webView reload];
}

#pragma mark - Setter

-(void)setUrlStr:(NSString *)urlStr{
    _urlStr = urlStr;
    
    NSURLRequest * request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [self.webView loadRequest:request];
}

#pragma mark - UIWebView Delegate

- (void)webViewDidStartLoad:(UIWebView *)webView{
    historyBack.enabled = [self.webView canGoBack];
    historyForward.enabled = [self.webView canGoForward];
    
    indicator= [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [indicator startAnimating];
    
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:indicator]];

}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    historyBack.enabled = [self.webView canGoBack];
    historyForward.enabled = [self.webView canGoForward];

    [indicator stopAnimating];
    [indicator removeFromSuperview];
}

@end
