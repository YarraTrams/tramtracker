//
//  TicketOutletsViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 12/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "TicketOutletsViewController.h"
#import "TicketOutletDetailedViewController.h"
#import "Constants.h"
#import "TTTabBarController.h"
#import "Analytics.h"

@interface TicketOutletsViewController ()

@end

@implementation TicketOutletsViewController

#pragma mark - User Actions on Nav Bar

-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)toggleListMapAction:(UIBarButtonItem *)sender
{
    TTTabBarController  * tabbarController = self.childViewControllers.firstObject;
    [tabbarController toggleListMapAction:sender isRight:YES];
}

#pragma mark - Inits & Loads

- (void)viewDidLoad
{
    [super viewDidLoad];

    if (self.stop)
    {
        UITabBarController  * tabbarViewController = self.childViewControllers.firstObject;
        
        for (id curViewController in tabbarViewController.childViewControllers)
            [curViewController setStop:self.stop];
    }
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

#pragma mark - Segue Methods

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kSegueShowTicketRetailer])
        [segue.destinationViewController setCurRetailer:sender];
}

@end
