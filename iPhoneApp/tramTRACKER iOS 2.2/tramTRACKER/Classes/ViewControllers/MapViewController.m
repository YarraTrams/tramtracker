
//
//  MapViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 4/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "MapViewController.h"
#import "StopAnnotationView.h"
#import "TicketRetailerAnnotationView.h"
#import "Constants.h"
#import "StopRightFilterViewController.h"
#import "PIDViewController.h"
#import "Journey.h"
#import "TTTools.h"
#import "TimetablesViewController.h"
#import "TicketOutletDetailedViewController.h"
#import "StopList.h"
#import "Settings.h"
#import "TTActivityIndicatorView.h"
#import "Constants.h"
#import "NearbyViewController.h"
#import "PointOfInterest.h"
#import "FavouriteAddViewController.h"
#import "Analytics.h"
#import "MoreViewController.h"
#import "FavouriteStop.h"
#import "myTramViewController.h"
#import "UIAlertView+Blocks.h"
#import "StopRightFilterViewController.h"
#import "MapCallout.h"
#import "SMCalloutView.h"
#import "CustomMapView.h"

@interface MapViewController ()<StopRightMenuDelegate, SMCalloutViewDelegate>

@property (nonatomic, assign) MapViewControllerType listViewType;
@property (nonatomic, assign) StopRightMenuType     previousFilterType;
@property (nonatomic, assign) BOOL                  trackUserLocation;
@property (nonatomic, assign) BOOL                  upDirection;
@property (strong, nonatomic) NSArray               * stopList;
@property (strong, nonatomic) NSArray               * outletList;
@property (strong, nonatomic) Stop                  * stop;
@property (strong, nonatomic) Journey               * journey;
@property (strong, nonatomic) Route                 * route;
@property (weak, nonatomic) NearbyViewController    * nearbyContainer;

@property (strong, nonatomic) NSMutableArray    * selectedPins;
@property (nonatomic, assign, getter = isFirstLaunch) BOOL  firstLaunch;
@property (nonatomic, assign) BOOL isTimetables;
@property (nonatomic, strong) SMCalloutView *calloutView;

@property (nonatomic, strong) MapCallout * ftzCallout;
@property (nonatomic, strong) MapCallout * regCallout;

@property (strong, nonatomic) id selectedStop;

/*
 * IBOutlet
 */

@property (weak, nonatomic) IBOutlet CustomMapView  * mapView;
@property (weak, nonatomic) IBOutlet UIButton   * locateMeButton;
@property (strong, nonatomic) NSDate            * locatingStarted;

@property (nonatomic, assign, getter = isNotFirst) BOOL notFirst;

@property (nonatomic, assign) NSInteger     numberOfRegionChanges;


@property (strong, nonatomic) Stop * nextStop;
@property (strong, nonatomic) Stop * atStop;

@end

@implementation MapViewController

@synthesize filterType = _filterType;

#pragma mark - Inits & Loads

- (void)viewDidLoad
{
	[super viewDidLoad];
    self.mapView.showsUserLocation = YES;
    
    if (self.listViewType == MapViewControllerTypeNearbyList)
        self.nearbyContainer = (id)self.parentViewController.parentViewController;

    if (self.listViewType == MapViewControllerTypeTicketOutlets &&
        [[[[[[self tabBarController] tabBarController] selectedViewController] childViewControllers] firstObject] class] == [MoreViewController class])
        self.listViewType = MapViewControllerTypeTicketOutletsMore;
    

//    //Notification to display Timetables for current stop pin in map
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(didReceiveNotificationScheduleTouchedInPinOnMap:)
//                                                 name:kNotificationScheduleTouchedInPinOnMap object:nil];
//    //Notification to display PID for current stop pin in map
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(didReceiveNotificationPIDTouchedInPinOnMap:)
//                                                 name:kNotificationPIDTouchedInPinOnMap object:nil];
    //Notification to display Ticket Detail for current ticket retailer pin in map
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveNotificationTicketOutletInfoTouchedInPinOnMap:)
                                                 name:kNotificationTicketOutletInfoTouchedInPinOnMap object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshMapData) name:kNotificationSyncFinished object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadFavourites) name:kNotificationReloadFavouritedMap object:nil];
    [self refreshMapData];
    
    
    
    self.calloutView = [SMCalloutView new];
    self.calloutView.delegate = self;
    
    // tell our custom map view about the callout so it can send it touches
    self.mapView.calloutView = self.calloutView;
}

- (void)reloadFavourites
{
    if (self.listViewType == MapViewControllerTypeFavouriteList)
    {
        [self removeAnnotations:self.mapView.annotations];
        
        NSArray * favourites = [[StopList sharedManager] getFavouriteStopList];
        NSMutableArray * stopList = [NSMutableArray new];
        
        for (FavouriteStop * fstop in favourites) {
            [stopList addObject:fstop.stop];
        }

        self.stopList = stopList;

        [self.mapView addAnnotations:self.stopList];
        [self resetToDefault];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.nearbyContainer)
    {
        self.nearbyContainer.navigationItem.leftBarButtonItem.enabled = YES;
        self.nearbyContainer.navigationItem.rightBarButtonItem.enabled = YES;
    }
    [self logAnalytics];
}

- (void)refreshMapData
{
    /* Center on location */
    self.trackUserLocation = self.listViewType == MapViewControllerTypeNearbyList || self.listViewType == MapViewControllerTypeTicketOutletsMore;
    
    /* Fetch Data */
    if (self.listViewType == MapViewControllerTypeFavouriteList)
        [self reloadFavourites];
    
    /* Load Data */
    if (self.listViewType == MapViewControllerTypeMyTram ||
        self.listViewType == MapViewControllerTypeFavouriteList ||
        self.listViewType == MapViewControllerTypeSearchResults)
        [self.mapView addAnnotations:self.stopList];
    
    /* Center Map */
    if (self.listViewType == MapViewControllerTypeStopMap ||
        self.listViewType == MapViewControllerTypeTicketOutlets ||
        self.listViewType == MapViewControllerTypeMyTram ||
        self.listViewType == MapViewControllerTypeFavouriteList ||
        self.listViewType == MapViewControllerTypeSearchResults ||
        self.listViewType == MapViewControllerTypeTicketOutletsMore)
        [self resetToDefault];
    self.mapView.showsUserLocation = YES;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.mapView.delegate = nil;
}

#pragma mark - Notification Handlers

//- (void)didReceiveNotificationScheduleTouchedInPinOnMap:(NSNotification *)notification
//{
//    if (self.isViewLoaded && self.view.window)
//    {
//        Stop * stop = notification.object;
//        
//        if ([stop isKindOfClass:[JourneyStop class]])
//             stop = [(JourneyStop *)stop stop];
//        
//        if ([stop routesThroughStop].count)
//        {
//            UIStoryboard                * sb = [UIStoryboard storyboardWithName:kMainStoryboard bundle:nil];
//            TimetablesViewController    * vc = [sb instantiateViewControllerWithIdentifier:kScreenTimetables];
//            
//            [vc setStop:stop direction:self.upDirection];
//            [self.navigationController pushViewController:vc animated:YES];
//        }
//        else
//        {
//            [[[UIAlertView alloc] initWithTitle:@""
//                                        message:@"There are no routes through this stop"
//                                       delegate:nil
//                              cancelButtonTitle:@"OK"
//                              otherButtonTitles:nil, nil]
//             show];
//        }
//    }
//}
//
//- (void)didReceiveNotificationPIDTouchedInPinOnMap:(NSNotification *)notification
//{
//    if (self.isViewLoaded && self.view.window)
//    {
//        Stop                * stop = notification.object;
//
//        if ([stop isKindOfClass:[JourneyStop class]])
//            stop = [(JourneyStop *)stop stop];
//
//        UIStoryboard        * sb = [UIStoryboard storyboardWithName:kMainStoryboard bundle:nil];
//        PIDViewController   * vc = [sb instantiateViewControllerWithIdentifier:kScreenPID];
//        
//        if (self.listViewType == MapViewControllerTypeFavouriteList) {
//            NSIndexPath * indexPath = [[StopList sharedManager] indexPathForFavouriteStop:stop];
//            Filter * filter = [[StopList sharedManager] filterForFavouriteAtIndexPath:indexPath];
//            
//            [vc setFilter:filter];
//        }
//        [vc setCurrentStop:stop];
//        [self.navigationController pushViewController:vc animated:YES];
//    }
//}

- (void)didReceiveNotificationTicketOutletInfoTouchedInPinOnMap:(NSNotification *)notification
{
    if (self.isViewLoaded && self.view.window)
    {
        if ([notification.object isKindOfClass:[TicketRetailer class]])
        {
            TicketRetailer                      * ticketRetailer = notification.object;
            UIStoryboard                        * sb = [UIStoryboard storyboardWithName:kMainStoryboard bundle:nil];
            TicketOutletDetailedViewController  * vc = [sb instantiateViewControllerWithIdentifier:kScreenTicketDetailed];
            
            [vc setCurRetailer:ticketRetailer];
            [self.navigationController pushViewController:vc animated:YES];
        }
        else
            [self.tabBarController.parentViewController performSegueWithIdentifier:kSegueShowPOI
                                                                            sender:notification.object];
    }
}

#pragma mark - User Actions

- (IBAction)refreshMap:(id)sender
{
    [self resetToDefault];
}

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - MKMapViewDelegate

//
// Return a pin to place on the map for a stop
//
- (MKAnnotationView *)mapView:(MKMapView *)map viewForAnnotation:(id<MKAnnotation>)annotation
{
	if ([annotation isKindOfClass:[Stop class]] || [annotation isKindOfClass:[JourneyStop class]])
	{
		/* Create a stop pin */
		static NSString *annotationIdentifier = @"StopPin";
		StopAnnotationView *pin = (StopAnnotationView *)[map dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];
        
		if (!pin)
			pin = [[StopAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationIdentifier];
        
        [pin setAnnotationType:self.filterType];
        pin.shouldShowScheduledAccessory = self.listViewType != MapViewControllerTypeStopMap;
        pin.shouldShowRealTimeAccessory = self.listViewType != MapViewControllerTypeStopMap;
        
        if (self.stop)
        {
            if ([self.stop.formattedRouteDescription isEqualToString:((Stop *)annotation).formattedRouteDescription] &&
                [self.stop.name isEqualToString:((Stop *)annotation).name])
                [pin setAnnotationType:StopRightMenuCurrent];//For purple pin
        }
        else if (self.journey)
        {
            JourneyStop * jstop = nil;
            Stop        * stop = nil;
            
            if ([annotation isKindOfClass:[JourneyStop class]])
            {
                jstop = (id)annotation;
                stop = jstop.stop;
            }
            else
            {
                jstop = [self.journey journeyStopForStop:annotation];
                stop = annotation;
            }

            [pin setAnnotationType:jstop && [self.parent isSelectedCell:stop withJourneyStop:jstop] ? StopRightMenuCurrent : StopRightMenuAll];
        }
		return pin;
	}
    else if ([annotation isKindOfClass:[TicketRetailer class]] ||
             [annotation isKindOfClass:[PointOfInterest class]])
	{
		/* Create a ticket retailer pin */
		static NSString *annotationIdentifier = @"TicketRetailerPin";

		TicketRetailerAnnotationView *pin = (TicketRetailerAnnotationView *)[map dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];

		if (!pin)
			pin = [[TicketRetailerAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationIdentifier];
		else
            pin.annotation = annotation;
		return pin;
	}
	return nil;
}

- (void)mapView:(MKMapView *)aMapView regionDidChangeAnimated:(BOOL)animated
{
    if (self.listViewType == MapViewControllerTypeNearbyList || self.stop || self.listViewType == MapViewControllerTypeTicketOutletsMore)
        [self findAndAddAnnotationsForCurrentRegion];
    self.previousFilterType = self.filterType;
    self.numberOfRegionChanges++;
}

- (void)findAndAddAnnotationsForCurrentRegion
{
    CLLocation  * location = [[CLLocation alloc] initWithLatitude:self.mapView.centerCoordinate.latitude
                                                        longitude:self.mapView.centerCoordinate.longitude];
    
    NSArray     * itemsInRegion;
    
    /* If filterType has changed since last refresh, remove all previous annotations */
    if (self.filterType != self.previousFilterType) {
        [self removeAnnotations:self.mapView.annotations];
    }
    
    /* Performance: if its smaller than where 100 stops would fit in then go with the region direct */
    if (self.filterType == StopRightMenuOutlets || self.listViewType == MapViewControllerTypeTicketOutlets || self.listViewType == MapViewControllerTypeTicketOutletsMore)
        itemsInRegion = [TicketRetailer nearestTicketRetailersWithoutDistancesToLocation:location
                                                                                   count:[Settings showNearbyStops]];
    else
        itemsInRegion = [[StopList sharedManager] getNearestStopsWithoutDistancesToLocation:location
                                                                                      count:[Settings showNearbyStops] * 2];
    
    /* Filter with current filter (easy access, shelter, none)*/
    itemsInRegion = [StopRightFilterViewController filterArray:itemsInRegion filterType:self.filterType string:self.nearbyContainer.searchField.text];

    if (self.stop)
        itemsInRegion = [itemsInRegion arrayByAddingObject:self.stop];
    
    if (self.filterType == self.previousFilterType)
    {
        /* Predicates: Add only new Annotations that weren't present already and remove only the not present in the new array */
        NSPredicate * relativeComplementPredicateAdd = [NSPredicate predicateWithFormat:@"NOT SELF IN %@", self.mapView.annotations];
        NSPredicate * relativeComplementPredicateDel = [NSPredicate predicateWithFormat:@"NOT SELF IN %@", itemsInRegion];
        
        /* Add new stops */
        [self.mapView addAnnotations:[itemsInRegion filteredArrayUsingPredicate:relativeComplementPredicateAdd]];

        /* Remove out of region stops */
        [self removeAnnotations:[self.mapView.annotations filteredArrayUsingPredicate:relativeComplementPredicateDel]];
    }
    else
        [self.mapView addAnnotations:itemsInRegion];

    /* Set the new data */
    if (self.filterType == StopRightMenuOutlets)
        self.outletList = itemsInRegion;
    else
        self.stopList = itemsInRegion;
}

- (void)resetToDefault
{
    MKCoordinateRegion region;

    region.span = MKCoordinateSpanMake(0.01, 0.01);
    region.center = CLLocationCoordinate2DMake(-37.815319f, 144.960748f);

    self.numberOfRegionChanges = 0;

    if (self.listViewType == MapViewControllerTypeNearbyList ||
        self.listViewType == MapViewControllerTypeTicketOutletsMore)
    {
		NSArray                 * nearestStops = nil;
        BOOL                    useMyLocation = self.mapView.userLocation.location.coordinate.longitude != 0 && self.mapView.userLocation.location.coordinate.latitude != 0;

        if (useMyLocation)
        {
            if (self.filterType == StopRightMenuOutlets)
                nearestStops = [TicketRetailer nearestTicketRetailersWithoutDistancesToLocation:self.mapView.userLocation.location
                                                                                          count:5];
            else
                nearestStops = [[StopList sharedManager] getNearestStopsWithoutDistancesToLocation:self.mapView.userLocation.location
                                                                                             count:5
                                                                                          lowFloor:self.filterType == StopRightMenuAccess
                                                                                           shelter:self.filterType == StopRightMenuShelter];
            
            region = [self.mapView regionThatFits:[self regionThatFitsStops:[nearestStops arrayByAddingObject:self.mapView.userLocation]]];
        }
        else
            region.center = CLLocationCoordinate2DMake(-37.815319f, 144.960748f);
        self.trackUserLocation = !useMyLocation;
    }
    else if (self.stop) { // StopMap, TicketOutlets around a map
        region.center = self.stop.coordinate;
        self.trackUserLocation = NO;
    } else if (self.listViewType == MapViewControllerTypeMyTram ||
             self.listViewType == MapViewControllerTypeFavouriteList ||
             self.listViewType == MapViewControllerTypeSearchResults) {
        region = [self regionThatFitsStops:self.stopList];
        self.trackUserLocation = NO;
    }

    [self.mapView setRegion:region animated:self.isNotFirst];
    self.notFirst = YES;
}

- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    if (self.trackUserLocation &&
        (self.listViewType == MapViewControllerTypeNearbyList ||
         self.listViewType == MapViewControllerTypeTicketOutletsMore))
        [self resetToDefault];
}

- (void)mapView:(MKMapView *)aMapView didFailToLocateUserWithError:(NSError *)error
{
    if (self.listViewType == MapViewControllerTypeNearbyList ||
        self.listViewType == MapViewControllerTypeTicketOutletsMore)
        [self resetToDefault];
}

- (void)mapViewWillStartLoadingMap:(MKMapView *)aMapView
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

- (void)mapViewDidFinishLoadingMap:(MKMapView *)aMapView
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

- (void)removeAnnotations:(NSArray *)annotations {
    NSArray * userLocation = [annotations filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self isMemberOfClass: %@", [MKUserLocation class]]];

    if (userLocation.count > 0) {
        NSMutableArray  * annotationsCopy = [annotations mutableCopy];
        
        [annotationsCopy removeObject:userLocation.firstObject];
        annotations = annotationsCopy;
    }
    [self.mapView removeAnnotations:annotations];
}

#pragma mark - NearbyRightFilterDelegate

- (void)didSelectStopAction:(StopRightMenuType)filterType
{
    self.previousFilterType = self.filterType;
    self.filterType = filterType;

    if (self.listViewType == MapViewControllerTypeNearbyList ||
        self.listViewType == MapViewControllerTypeTicketOutletsMore)
        [self findAndAddAnnotationsForCurrentRegion];
    else if (self.listViewType == MapViewControllerTypeMyTram &&
             (self.filterType != self.previousFilterType || !self.mapView.annotations.count))
    {
        [self removeAnnotations:self.mapView.annotations];

        if (self.journey)
            self.stopList = [[StopList sharedManager] getStopListForTrackerIDs:(self.upDirection ?
                                                                                self.journey.tram.route.upStops :
                                                                                self.journey.tram.route.downStops)];
        else
            self.stopList = [[StopList sharedManager] getStopListForTrackerIDs:(self.upDirection ?
                                                                                self.route.upStops :
                                                                                self.route.downStops)];

        if (self.filterType == StopRightMenuOutlets)
            self.stopList = [TicketRetailer outletsFromSuburbStops:self.stopList];
        else
            self.stopList = [StopRightFilterViewController filterArray:self.stopList filterType:self.filterType string:nil];

        [self.mapView addAnnotations:self.stopList];
    }
    
    if (self.numberOfRegionChanges == 1)
        [self resetToDefault];
}

#pragma mark - SetCurrentJourneyDelegate

- (void)setFinalJourney:(Journey *)journey
{
    self.upDirection = journey.tram.upDirection;
    self.stopList = [[StopList sharedManager] getStopListForTrackerIDs:(journey.tram.upDirection ?
                                                                        journey.tram.route.upStops :
                                                                        journey.tram.route.downStops)];

    if (self.stopList == nil)
    {
        NSMutableArray * stopList = [NSMutableArray new];

        for (JourneyStop * curJourneyStop in journey.stops)
            [stopList addObject:curJourneyStop.stop];

        if (!stopList.count)
            return;
        self.stopList = [stopList copy];
    }
    
    NSMutableArray * stopList = [self.stopList mutableCopy];
    
    for (JourneyStop * jstop in journey.stops)
        [stopList removeObject:jstop.stop];

    [stopList addObjectsFromArray:journey.stops];
    self.stopList = [stopList copy];
    self.journey = journey;
}

- (void)setRoute:(Route *)aRoute direction:(BOOL)upDirection isTimeTables:(BOOL)isTimeTables
{
    self.isTimetables = isTimeTables;
    self.upDirection = upDirection;
    self.route = aRoute;
    self.stopList = [[StopList sharedManager] getStopListForTrackerIDs:(upDirection ?
                                                                        aRoute.upStops :
                                                                        aRoute.downStops)];
}

- (void)logAnalytics
{
    AnalyticsFilter filter = AnalyticsFilterNone;
    
    if (self.filterType == StopRightMenuShelter)
        filter = AnalyticsFilterShelter;
    else if (self.filterType == StopRightMenuOutlets)
        filter = AnalyticsFilterOutlets;
    else if (self.filterType == StopRightMenuAccess)
        filter = AnalyticsFilterEasyAccess;
    
    AnalyticsFeature feature = AnalyticsFeatureNearbyMap;
    
    if (self.listViewType == MapViewControllerTypeStopMap)
    {
        feature = AnalyticsFeatureStopMap;
        filter = AnalyticsFilterNone;
    }
    else if (self.listViewType == MapViewControllerTypeTicketOutletsMore)
    {
        feature = AnalyticsFeatureOutletMap;
        filter = AnalyticsFilterNone;
    }
    else if (self.listViewType == MapViewControllerTypeTicketOutlets)
    {
        feature = AnalyticsFeaturePIDNearbyTicketOutletMap;
        filter = AnalyticsFilterNone;
    }
    else if (self.listViewType == MapViewControllerTypeFavouriteList)
    {
        feature = AnalyticsFeatureFavouritesMap;
        filter = AnalyticsFilterNone;
    }
    else if (self.listViewType == MapViewControllerTypeMyTram)
    {
        if (self.isTimetables) {
            feature = AnalyticsFeatureTimeTablesMap;
            filter = AnalyticsFilterNone;
        } else if (self.journey) {
            feature = AnalyticsFeatureMyTramDetailedMap;
            filter = AnalyticsFilterNone;
        }
        else
            feature = AnalyticsFeatureRoutesDetailedMap;
    }
    else if (self.listViewType == MapViewControllerTypeSearchResults)
        feature = AnalyticsFeatureSearchResultMap;

    [[Analytics sharedInstance] featureAccessed:feature withFilter:filter];
}

/*
 * Find a coordinate region that fits all of our stops
 */

- (MKCoordinateRegion)regionThatFitsStops:(NSArray *)stops
{
	Stop                    * stop = stops.firstObject;
    
    
    if ([stop isKindOfClass:[JourneyStop class]])
         stop = [(JourneyStop *)stop stop];

	MKCoordinateSpan  	      span;
	CLLocationCoordinate2D  center;
	
	/* Single Stop */
	if (stops.count == 1)
	{
		/* Center on the selected stop */
		center.latitude = stop.location.coordinate.latitude;
		center.longitude = stop.location.coordinate.longitude;
		
		/* And the default span */
		span.latitudeDelta = kDefaultLatDelta;
		span.longitudeDelta = kDefaultLonDelta;
        
	}
    else
	{
        /* Multiple stops, find the region that will fit them all */
		CLLocationDegrees northernBoundary, southernBoundary, easternBoundary, westernBoundary;
        
		// set the initial boundaries to the first stop
		northernBoundary = stop.location.coordinate.latitude;
		southernBoundary = northernBoundary;
		westernBoundary = stop.location.coordinate.longitude;
		easternBoundary = westernBoundary;
		
		/* Loop over our stops and find the boundaries */
		for (Stop *curStop in stops)
		{
            Stop * stop = curStop;
            
            if ([stop isKindOfClass:[JourneyStop class]])
                stop = [(JourneyStop *)stop stop];

			/* Use latitude to find north/south boundaries */
			if (stop.location.coordinate.latitude > northernBoundary)
				northernBoundary = stop.location.coordinate.latitude;
			else if (stop.location.coordinate.latitude < southernBoundary)
				southernBoundary = stop.location.coordinate.latitude;
			
			/* And longitude for east/west */
			if (stop.location.coordinate.longitude > westernBoundary)
				westernBoundary = stop.location.coordinate.longitude;
			else if (stop.location.coordinate.longitude < easternBoundary)
				easternBoundary = stop.location.coordinate.longitude;
		}
        northernBoundary += 0.004;
        southernBoundary -= 0.004;
        westernBoundary += 0.004;
        easternBoundary -= 0.004;

        /* So we should know our span now between the boundaries */
		span.latitudeDelta = (northernBoundary - southernBoundary);
		span.longitudeDelta = (westernBoundary - easternBoundary);

		/* so now we find the center by using the lower boundary and adding half the span */
		center.latitude = southernBoundary + (span.latitudeDelta / 2);
		center.longitude = easternBoundary + (span.longitudeDelta / 2);
	}
	return MKCoordinateRegionMake(center, span);
}

- (void)setNextStop:(Stop *)nextStop
{
    if (nextStop != _nextStop)
    {
        Stop *previousStop = _nextStop;
        
        _nextStop = nextStop;
        
        if (previousStop)
        {
            [self removeStopAnnotation:previousStop];
            [self addStopAnnotation:previousStop];
        }

        if (nextStop)
        {
            [self removeStopAnnotation:nextStop];
            [self addStopAnnotation:nextStop];
        }
    }
}

- (myTramViewController *)parent
{
    return (myTramViewController *)self.parentViewController.parentViewController;
}

- (void)setAtStop:(Stop *)atStop
{
    if (atStop != _atStop)
    {
        Stop *previousStop = _nextStop;

        _atStop = atStop;

        if (previousStop)
        {
            [self removeStopAnnotation:previousStop];
            [self addStopAnnotation:previousStop];
        }

        if (atStop)
        {
            [self removeStopAnnotation:atStop];
            [self addStopAnnotation:atStop];
        }
    }
}

- (void)removeStopAnnotation:(Stop *)stop
{
    JourneyStop * j = [self.journey journeyStopForStop:stop];
    
    if (j)
        [self.mapView removeAnnotation:j];
    [self.mapView removeAnnotation:stop];
}

- (void)addStopAnnotation:(Stop *)stop
{
    JourneyStop * j = [self.journey journeyStopForStop:stop];

    if (j)
        [self.mapView addAnnotation:j];
    else
        [self.mapView addAnnotation:stop];
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    if ([view.annotation isKindOfClass:[MKUserLocation class]] || [view isKindOfClass:[TicketRetailerAnnotationView class]]) {
        return;
    }

    Stop * stop = view.annotation;
    
    if ([stop isKindOfClass:[JourneyStop class]]) {
        stop = [(JourneyStop *)stop stop];
    }

    if (stop.isFTZStop) {
        self.calloutView.contentView = self.ftzCallout;
    } else {
        self.calloutView.contentView = self.regCallout;
    }
    
    MapCallout * callout = (id)self.calloutView.contentView;
    
    callout.calloutTitle.text = [view.annotation title];
    callout.calloutSubtitle.text = [view.annotation subtitle];

    // Apply the MKAnnotationView's desired calloutOffset (from the top-middle of the view)
    self.calloutView.calloutOffset = view.calloutOffset;

    // iOS 7 only: Apply our view controller's edge insets to the allowable area in which the callout can be displayed.
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
        self.calloutView.constrainedInsets = UIEdgeInsetsMake(self.topLayoutGuide.length, 0, self.bottomLayoutGuide.length, 0);

    // This does all the magic.
    [self.calloutView presentCalloutFromRect:view.bounds inView:view constrainedToView:self.view animated:YES];
    self.selectedStop = view.annotation;
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
    if ([view.annotation isKindOfClass:[MKUserLocation class]] || [view isKindOfClass:[TicketRetailerAnnotationView class]]) {
        return;
    }
    [self.calloutView dismissCalloutAnimated:YES];
}

//
// SMCalloutView delegate methods
//

- (NSTimeInterval)calloutView:(SMCalloutView *)calloutView delayForRepositionWithSize:(CGSize)offset {
    
    // When the callout is being asked to present in a way where it or its target will be partially offscreen, it asks us
    // if we'd like to reposition our surface first so the callout is completely visible. Here we scroll the map into view,
    // but it takes some math because we have to deal in lon/lat instead of the given offset in pixels.
    
    CLLocationCoordinate2D coordinate = self.mapView.centerCoordinate;
    
    // where's the center coordinate in terms of our view?
    CGPoint center = [self.mapView convertCoordinate:coordinate toPointToView:self.view];
    
    // move it by the requested offset
    center.x -= offset.width;
    center.y -= offset.height;
    
    // and translate it back into map coordinates
    coordinate = [self.mapView convertPoint:center toCoordinateFromView:self.view];
    
    // move the map!
    [self.mapView setCenterCoordinate:coordinate animated:YES];
    
    // tell the callout to wait for a while while we scroll (we assume the scroll delay for MKMapView matches UIScrollView)
    return kSMCalloutViewRepositionDelayForUIScrollView;
}

- (void)didClickTimetables {
    if (self.isViewLoaded && self.view.window)
    {
        Stop * stop = self.selectedStop;
        
        if ([stop isKindOfClass:[JourneyStop class]])
            stop = [(JourneyStop *)stop stop];
        
        if ([stop routesThroughStop].count)
        {
            UIStoryboard                * sb = [UIStoryboard storyboardWithName:kMainStoryboard bundle:nil];
            TimetablesViewController    * vc = [sb instantiateViewControllerWithIdentifier:kScreenTimetables];
            
            [vc setStop:stop direction:self.upDirection];
            [self.navigationController pushViewController:vc animated:YES];
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:@""
                                        message:@"There are no routes through this stop"
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil, nil]
             show];
        }
    }
}

- (void)didClickPID {
    if (self.isViewLoaded && self.view.window)
    {
        Stop                * stop = self.selectedStop;
        
        if ([stop isKindOfClass:[JourneyStop class]])
            stop = [(JourneyStop *)stop stop];
        
        UIStoryboard        * sb = [UIStoryboard storyboardWithName:kMainStoryboard bundle:nil];
        PIDViewController   * vc = [sb instantiateViewControllerWithIdentifier:kScreenPID];
        
        if (self.listViewType == MapViewControllerTypeFavouriteList) {
            NSIndexPath * indexPath = [[StopList sharedManager] indexPathForFavouriteStop:stop];
            Filter * filter = [[StopList sharedManager] filterForFavouriteAtIndexPath:indexPath];
            
            [vc setFilter:filter];
        }
        [vc setCurrentStop:stop];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (MapCallout *)regCallout {
    if (!_regCallout) {
        _regCallout = [[NSBundle mainBundle] loadNibNamed:@"MapCalloutOriginal" owner:self options:nil][0];
        [_regCallout.pidButton addTarget:self action:@selector(didClickPID) forControlEvents:UIControlEventTouchUpInside];
        [_regCallout.timetablesButton addTarget:self action:@selector(didClickTimetables) forControlEvents:UIControlEventTouchUpInside];
    }
    return _regCallout;
}

- (MapCallout *)ftzCallout {
    if (!_ftzCallout) {
        _ftzCallout = [[NSBundle mainBundle] loadNibNamed:@"MapCallout" owner:self options:nil][0];
        [_ftzCallout.pidButton addTarget:self action:@selector(didClickPID) forControlEvents:UIControlEventTouchUpInside];
        [_ftzCallout.timetablesButton addTarget:self action:@selector(didClickTimetables) forControlEvents:UIControlEventTouchUpInside];
    }
    return _ftzCallout;
}

@end
