//
//  RouteContainerViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 11/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "RouteContainerViewController.h"
#import "TimetablesViewController.h"
#import "Constants.h"
#import "RightMenuDelegate.h"
#import "PIDViewController.h"
#import "SWRevealViewController.h"
#import "TTTabBarController.h"
#import "TrackerIDSearchViewController.h"
#import "SearchViewController.h"

@interface RouteContainerViewController ()

@property (strong, nonatomic) Route                     * route;
@property (nonatomic, assign) StopRightMenuType         filterType;
@property (weak, nonatomic) IBOutlet UIButton           * toggleButton;
@property (weak, nonatomic) IBOutlet UIButton           * filtersButton;
@property (strong, nonatomic) IBOutlet UILabel          * routeTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *optionLeadingSpace;

@property (nonatomic, assign) BOOL  upDirection;
@property (nonatomic, assign) BOOL  isTimetables;

typedef enum
{
    FromRoutes,
    FromTimetables
}   viewControllerType;

@property (nonatomic) viewControllerType type;

@end

@implementation RouteContainerViewController

#pragma mark - User Actions on Nav Bar

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark Utility methods

- (void)dismissReveal:(id)sender
{
    //push the reveal controller if its there
    if (self.parentViewController.revealViewController.frontViewPosition == FrontViewPositionLeftSide)
    {
        [self.parentViewController.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];
    }
}

#pragma mark - Inits & Loads

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    /* Custom back button */
    
    [self.filtersButton addTarget:self.tabBarController.revealViewController action:@selector(rightRevealToggle:)
                 forControlEvents:UIControlEventTouchUpInside];
    
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self.parentViewController.revealViewController action:@selector(rightRevealToggle:)];
    swipeGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeGesture];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissReveal:)];
    [tap setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:tap];
    
    if (self.route)
    {
        UITabBarController      * childTabBar = self.childViewControllers.firstObject;
        
        for (id curViewController in childTabBar.childViewControllers)
            [curViewController setRoute:self.route direction:self.upDirection isTimeTables:self.isTimetables];
        
        self.routeTitle.text = [self.route routeNameWithNumberAndDestinationUp:self.upDirection];
    }
    
    self.type = self.navigationController.viewControllers.count == 3 ? FromTimetables : FromRoutes;
    if (self.type == FromTimetables)
    {
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Nav-Map-Right"]
                                                                                   style:UIBarButtonItemStyleDone
                                                                                   target:self
                                                                                   action:@selector(toggleListMapAction:)]
                                          animated:NO];
    }
    else if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        self.optionLeadingSpace.constant = 0;
        [self.navigationController.navigationBar layoutIfNeeded];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationRoutesResetRight object:nil];
}

#pragma mark - Segue Methods

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kSegueShowPID])
        [segue.destinationViewController setCurrentStop:sender];
    if ([segue.identifier isEqualToString:kSegueShowTimetables])
        [segue.destinationViewController setStop:sender direction:self.upDirection];
}

#pragma mark - Utility Methods

- (void)setRoute:(Route *)aRoute direction:(BOOL)upDirection isTimeTables:(BOOL)isTimetables
{
    self.route = aRoute;
    self.upDirection = upDirection;
    self.isTimetables = isTimetables;

    UITabBarController  * tabbarController = self.childViewControllers.firstObject;
    id curViewController = tabbarController.childViewControllers.lastObject;
    [curViewController setRoute:self.route direction:self.upDirection isTimeTables:isTimetables];
}

- (IBAction)toggleListMapAction:(UIBarButtonItem *)sender
{
    TTTabBarController                      * tabbarController = self.childViewControllers.firstObject;
    id<StopRightMenuDelegate>               newViewController;
    UIImage                                 * image;
    
    if (tabbarController.selectedViewController == tabbarController.childViewControllers.firstObject)
    {
        newViewController = tabbarController.childViewControllers.lastObject;
        [tabbarController setSelectedIndex:kIndexForList animated:YES];
        image = [UIImage imageNamed:@"Nav-List-Right"];
        [sender setAccessibilityLabel:NSLocalizedString(@"button-list", nil)];
    }
    else
    {
        newViewController = tabbarController.childViewControllers.firstObject;
        [tabbarController setSelectedIndex:kIndexForMap animated:YES];
        image = [UIImage imageNamed:@"Nav-Map-Right"];
        [sender setAccessibilityLabel:NSLocalizedString(@"button-map", nil)];
    }
    
    if ([sender isKindOfClass:[UIBarButtonItem class]])
        [sender setImage:image];
    else
        [(UIButton *)sender setImage:image forState:UIControlStateNormal];
    
    [newViewController didSelectStopAction:self.filterType];
}

#pragma mark - StopRightMenuDelegate

- (void)didSelectStopAction:(StopRightMenuType)filterType
{
    switch (filterType)
    {
        case StopRightMenuTrackerID:
        {
            UIStoryboard        * sb = [UIStoryboard storyboardWithName:kMainStoryboard bundle:nil];
            [self.navigationController pushViewController:[sb instantiateViewControllerWithIdentifier:kScreenTrackerID] animated:NO];
            break ;
        }
        case StopRightMenuSearch:
        {
            UIStoryboard        * sb = [UIStoryboard storyboardWithName:kMainStoryboard bundle:nil];
            [self.navigationController pushViewController:[sb instantiateViewControllerWithIdentifier:kScreenSearch] animated:NO];
            break;
        }
        default:
        {
            UITabBarController          * childNavigationController = self.childViewControllers.firstObject;
            id<StopRightMenuDelegate>   curViewController = (id)childNavigationController.selectedViewController;
            
            [curViewController didSelectStopAction:filterType];
            break;
        }
    }
    [self.tabBarController.revealViewController rightRevealToggle:nil];
    self.filterType = filterType;
}


@end
