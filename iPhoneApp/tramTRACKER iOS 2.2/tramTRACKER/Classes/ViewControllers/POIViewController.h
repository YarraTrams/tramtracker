//
//  POIViewController.h
//  tramTRACKER
//
//  Created by Raji on 8/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PointOfInterest;

@interface POIViewController : UIViewController

@property (strong, nonatomic) PointOfInterest       * poi;

@end
