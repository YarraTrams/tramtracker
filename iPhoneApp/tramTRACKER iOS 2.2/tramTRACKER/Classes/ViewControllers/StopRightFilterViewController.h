//
//  NeabyRightViewController.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 21/11/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RightMenuDelegate.h"

@interface StopRightFilterViewController : UITableViewController

+ (NSString *)sectionHeaderTitleForType:(StopRightMenuType)filterType;
+ (NSArray *)filterDistanceArray:(NSArray *)items filterType:(StopRightMenuType)filterType string:(NSString *)filterString;
+ (NSArray *)filterArray:(NSArray *)items filterType:(StopRightMenuType)filterType string:(NSString *)filterString;

@end
