//
//  LogViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 27/01/2014.
//  Copyright (c) 2014 AppsCore. All rights reserved.
//

#import "LogViewController.h"
#import "HistoricalUpdate.h"

@interface LogViewController ()

@property (nonatomic, strong) NSArray   * data;

@end

@implementation LogViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.data = [HistoricalUpdate getHistoricalUpdatesList];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    HistoricalUpdate    * update = self.data[indexPath.row];

    cell.textLabel.text = update.name;
    cell.detailTextLabel.text = update.message;
    return cell;
}

@end
