//
//  AccordionButton.h
//  tramTRACKER
//
//  Created by Alex Louey on 15/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccordionButton : UIButton

@property (nonatomic,strong) NSString *text;
@property (nonatomic,assign,readonly) BOOL isOpen;

@property (nonatomic,copy) void (^heightChange)(int newHeight, float duration,float usingSpringWithDamping,float initialSpringVelocity);


@end
