//
//  FTZOverlay.h
//  tramTracker
//
//  Created by Hugo Cuvillier on 8/12/2014.
//  Copyright (c) 2014 AppsCore. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FTZOverlay : UIView

+ (FTZOverlay *)overlay;

@end
