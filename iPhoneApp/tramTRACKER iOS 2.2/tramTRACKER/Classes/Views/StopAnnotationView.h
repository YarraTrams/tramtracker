//
//  StopAnnotationView.h
//  tramTRACKER
//
//  Created by Robert Amos on 19/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "RightMenuDelegate.h"

@interface StopAnnotationView : MKPinAnnotationView {
	BOOL purplePin;
	BOOL shouldShowRealTimeAccessory;
	BOOL shouldShowScheduledAccessory;
}

@property (nonatomic,getter=isPurplePin) BOOL purplePin;
@property (nonatomic) BOOL shouldShowRealTimeAccessory;
@property (nonatomic) BOOL shouldShowScheduledAccessory;


/**
 * Called when the user touches our detail disclosure button in the callout bubble
**/

- (void)didTouchRealTimeCalloutAccessory:(id)sender;
- (void)didTouchScheduledCalloutAccessory:(id)sender;
- (void)setAnnotationType:(StopRightMenuType)type;

@end
