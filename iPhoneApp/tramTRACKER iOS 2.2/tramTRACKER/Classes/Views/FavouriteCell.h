//
//  FavouritesCell.h
//  tramTRACKER
//
//  Created by Raji on 4/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FavouritesViewController.h"

@class FavouriteStop;

@interface FavouriteCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel          * labelName;
@property (nonatomic, strong) IBOutlet UILabel          * labelRouteDescription;

- (void)configureWithStop:(FavouriteStop *)faveStop mode:(FavouritesViewControllerType)mode;
- (void)configureWithFavouriteSectionName:(NSString *)sectionName;

extern NSString * const kNotificationEditFavourite;
extern NSString * const kNotificationEditGroupName;

@end
