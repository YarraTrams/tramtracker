//
//  PIDUpdatesCell.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 21/11/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 * Cell Identifiers
 */

extern NSString * const kCellDisruptionMinimal;
extern NSString * const kCellDisruptionFull;
extern NSString * const kCellUpdateMinimal;
extern NSString * const kCellUpdateFull;

@interface DisruptionUpdateCell : UITableViewCell

- (void)configureWithMessage:(NSString *)message;


@end
