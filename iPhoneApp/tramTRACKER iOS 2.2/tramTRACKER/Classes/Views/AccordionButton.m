//

//  AccordionButton.m
//  tramTRACKER
//
//  Created by Alex Louey on 15/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import <FrameAccessor/FrameAccessor.h>

#import "AccordionButton.h"
#import <QuartzCore/QuartzCore.h>

@interface AccordionButton()
{
        UILabel * textLb;
}

@end

@implementation AccordionButton

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [[self layer] setBorderWidth:1.0f];
        [[self layer] setBorderColor:[UIColor lightGrayColor].CGColor];
        self.layer.cornerRadius = 5;
        self.clipsToBounds = YES;
    }
    return self;
}

- (void)setText:(NSString *)text
{
    _text = text;
    textLb = [[UILabel alloc] initWithFrame:CGRectMake(10, self.height, self.width -20, 10)];
    [textLb setBackgroundColor:self.backgroundColor];
    [textLb setFont:[UIFont fontWithName:self.titleLabel.font.familyName size:13]];
    textLb.textColor = self.titleLabel.textColor;
    [textLb setTextAlignment:NSTextAlignmentCenter];
    textLb.numberOfLines = 0;
    textLb.text = text;
    [textLb sizeToFit];
    
    [self addSubview:textLb];

}

@end
