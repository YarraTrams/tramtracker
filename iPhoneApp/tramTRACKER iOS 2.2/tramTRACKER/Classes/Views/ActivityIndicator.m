//
//  ActivityIndicator.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 6/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "ActivityIndicator.h"

@implementation ActivityIndicator

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
        [self loadImages:ActivityIndicatorTracking];
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        [self loadImages:ActivityIndicatorTracking];
    }
    return self;
}

- (void)loadImages:(ActivityIndicatorType)type
{
    static NSInteger  const kNoOfAnimationImages = 27;
    static NSString * const kAnimationImagePrefix = @"Tracking-Animation";
    static NSString * const kWhiteAnimationImagePrefix = @"w_Tracking-Animation";

    NSString        * imagePrefix = type == ActivityIndicatorTracking ? kAnimationImagePrefix : kWhiteAnimationImagePrefix;

    NSMutableArray  * animationImages = [NSMutableArray new];

    for (NSInteger i = 1; i < kNoOfAnimationImages; ++i)
    {
        NSString    * imageName = [NSString stringWithFormat:@"%@%02ld", imagePrefix, (unsigned long)i];
        UIImage     * image = [UIImage imageNamed:imageName];

        [animationImages addObject:image];
    }
    self.animationImages = [animationImages copy];
    self.animationDuration = 2.0f;
}

@end
