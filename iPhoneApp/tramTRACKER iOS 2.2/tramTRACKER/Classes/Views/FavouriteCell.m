//
//  FavouritesCell.m
//  tramTRACKER
//
//  Created by Raji on 4/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "FavouriteCell.h"
#import "FavouriteStop.h"
#import "Stop.h"

@interface FavouriteCell ()

@property (nonatomic, strong) IBOutlet UILabel          * labelFavouriteName;

@property (nonatomic, strong) IBOutlet UIButton         * btnEdit;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * leftSpaceConstrain;

@property (strong, nonatomic) FavouriteStop             * favouriteStop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint * rightSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * StopNameTopSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * StopDescriptionTopSpace;
@property (weak, nonatomic) IBOutlet UIImageView        * lowFloor;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * routesHeight;

@end

@implementation FavouriteCell

NSString * const kNotificationEditFavourite = @"notificationEditFavourite";
NSString * const kNotificationEditGroupName = @"notificationEditGroupName";

- (void)willTransitionToState:(UITableViewCellStateMask)state
{
    [super willTransitionToState:state];

    if ((state & UITableViewCellStateEditingMask) == UITableViewCellStateEditingMask)
        [self configureCellInMode:FavouritesViewControllerManageFavourites];
    else if ((state & UITableViewCellStateDefaultMask) == UITableViewCellStateDefaultMask)
        [self configureCellInMode:FavouritesViewControllerFavourites];
    [self updateRouteDescription];
}

- (void)configureCellInMode:(FavouritesViewControllerType)mode
{
    self.leftSpaceConstrain.constant = mode == FavouritesViewControllerFavourites ? 10.0f : 70.0f;
    self.rightSpace.constant = mode == FavouritesViewControllerFavourites ? 0.0f : 10.0f;
    self.btnEdit.hidden = mode == FavouritesViewControllerFavourites;
    [self layoutIfNeeded];
    
    if (self.favouriteStop.filter && mode == FavouritesViewControllerFavourites)
        self.lowFloor.hidden = !self.favouriteStop.filter.lowFloor;
    else
        self.lowFloor.hidden = YES;
}

- (void)updateRouteDescription {
    NSString * routeDescription = [self.favouriteStop.stop formattedRouteDescriptionForFavouriteStop:self.favouriteStop];
    CGSize  size = [routeDescription sizeWithFont:self.labelRouteDescription.font];
    
    if (size.width > 249 && !self.favouriteStop.stop.isFTZStop) {
        self.routesHeight.constant = 34;
    } else {
        self.routesHeight.constant = 17;
    }

    [self layoutIfNeeded];
    self.labelRouteDescription.text = routeDescription;
}

- (void)configureWithStop:(FavouriteStop *)faveStop mode:(FavouritesViewControllerType)mode
{
    self.favouriteStop = faveStop;
    [self configureCellInMode:mode];

    if (faveStop.stop.isFTZStop && faveStop.name.length) {
        self.labelName.text = faveStop.name;
        self.labelName.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
    } else {
        self.labelFavouriteName.text = faveStop.name;
        self.labelName.text = [faveStop.stop formattedName];
        self.labelName.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:16];
    }

    if (faveStop.filter && mode == FavouritesViewControllerFavourites)
        self.lowFloor.hidden = !faveStop.filter.lowFloor;
    else
        self.lowFloor.hidden = YES;
    [self updateRouteDescription];
}

- (void)configureWithFavouriteSectionName:(NSString *)sectionName
{
    self.favouriteStop = nil;
    [self configureCellInMode:FavouritesViewControllerManageGroups];
    self.labelFavouriteName.text = sectionName;
}

- (IBAction)editCurrentCell:(id)sender
{
    if (!self.favouriteStop)
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationEditGroupName object:self.labelFavouriteName.text];
    else
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationEditFavourite object:self.favouriteStop];
}

@end
