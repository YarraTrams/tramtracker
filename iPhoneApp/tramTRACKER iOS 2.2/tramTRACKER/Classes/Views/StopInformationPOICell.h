//
//  StopInformationCell.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 4/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PointOfInterest;

@interface StopInformationPOICell : UITableViewCell

- (void)configureWithPointOfInterest:(PointOfInterest *)aPOI;

@end

