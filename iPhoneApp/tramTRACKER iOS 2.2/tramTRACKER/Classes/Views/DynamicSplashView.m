//
//  DynamicSplashView.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 24/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import "DynamicSplashView.h"
#import "ActivityIndicator.h"

@interface DynamicSplashView ()

@property (nonatomic, weak) IBOutlet ActivityIndicator  * indicator;

@property (weak, nonatomic) IBOutlet UIImageView *      bg;

@property (nonatomic, strong) UIDynamicAnimator         * animator;
@property (nonatomic, strong) UICollisionBehavior       * collision;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpaceActivity;

@end

@implementation DynamicSplashView

+ (DynamicSplashView *)splashWithFrame:(CGRect)frame
{
    static NSString * const aName = @"Splash";
    
    DynamicSplashView * v = [[NSBundle mainBundle] loadNibNamed:aName owner:self options:nil].firstObject;

    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        if (IS_IPHONE_5)
        {
            v.bg.image = [UIImage imageNamed:@"splash_iphone5_ios6"];
            v.topSpaceActivity.constant = 171;
        }
        else
        {
            v.bg.image = [UIImage imageNamed:@"splash_iphone4_ios6"];
            v.topSpaceActivity.constant = 83;
        }
    }
    else if (IS_IPHONE_5)
    {
        v.bg.image = [UIImage imageNamed:@"splash_iphone5"];
        v.topSpaceActivity.constant = 191;
    }

    [v layoutIfNeeded];

    [v.indicator loadImages:ActivityIndicatorTramming];
    [v.indicator startAnimating];
    v.frame = frame;
    return v;
}

@end
