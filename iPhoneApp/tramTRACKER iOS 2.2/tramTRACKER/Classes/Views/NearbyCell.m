//
//  NearbyCell.m
//  tramTRACKER
//
//  Created by Robert Amos on 4/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "NearbyCell.h"
#import "Stop.h"
#import "TicketRetailer.h"
#import "PointOfInterest.h"

@interface NearbyCell()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *routesHeight;

@end

@implementation NearbyCell

- (void)configureWithStopDistance:(StopDistance *)aStopDistance
{
    self.distance.text = aStopDistance.formattedDistance;
    self.name.text = aStopDistance.stop.formattedName;
    [self setRouteDescriptions:aStopDistance.stop.formattedRouteDescription];
}

- (void)configureWithTicketRetailerDistance:(TicketRetailerDistance *)aTicketRetailerDistance
{
    [self configureWithTicketRetailer:aTicketRetailerDistance.retailer distance:aTicketRetailerDistance.formattedDistance];
    self.distance.text = aTicketRetailerDistance.formattedDistance;
}

- (void)configureWithTicketRetailer:(TicketRetailer *)aTicketRetailer distance:(NSString *)distance
{
    self.name.text = aTicketRetailer.name;
    [self setRouteDescriptions:aTicketRetailer.address];
    self.distance.text = distance;
}

- (void)setRouteDescriptions:(NSString *)aRouteDescription
{
    CGSize  size = [aRouteDescription sizeWithFont:self.routeDescription.font];

    if (size.width > self.routeDescription.frame.size.width) {
        self.routesHeight.constant = 34;
    } else {
        self.routesHeight.constant = 17;
    }

    [self layoutIfNeeded];
    self.routeDescription.text = aRouteDescription;
}

- (void)configureWithStop:(Stop *)aStop andEasyAccess:(BOOL)hasEasyAccess andShelter:(BOOL)hasShelter
{
    NSString *imageName = @"S_Green-PIN";
   
    if (aStop.easyAccess && hasEasyAccess)
        imageName = @"S_Lowlevel-PIN";
    else if (aStop.shelter && hasShelter)
        imageName = @"S_Shelter-PIN";
    
   [self.image setImage:[UIImage imageNamed:imageName]];
    self.name.text = aStop.formattedName;
    [self setRouteDescriptions:aStop.formattedRouteDescription];
}

- (void)configureWithTR:(TicketRetailer *)aTicketRetailer
{
    [self.image setImage:[UIImage imageNamed:@"S_Ticket-PIN"]];
    self.name.text = aTicketRetailer.name;
    [self setRouteDescriptions:aTicketRetailer.address];
}

- (void)configureWithPOI:(PointOfInterest *)aPOI
{
    [self.image setImage:[UIImage imageNamed:@"S_POI-PIN"]];
    self.name.text = aPOI.name;
    [self setRouteDescriptions:aPOI.poiDescription];
}

@end
