//
//  PagedPIDViewController.h
//  tramTRACKER
//
//  Created by Robert Amos on 2/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreLocation/CoreLocation.h>
#import "PIDViewControllerOld.h"
#import "PidsService.h"
#import "Stop.h"

@class Route;

/**
 * @ingroup PIDS
**/

/**
 * A custom UIViewController that creates a view container that can hold 'pages' of PIDS.
 *
 * A single PID is created and the Page Control and Swiping moves fake PIDS (just the background image)
 * to the front of the device before drawing a new PID over the top
**/
@interface PagedPIDViewController : UIViewController <CLLocationManagerDelegate> {
	
	/**
	 * A cached list of the stops that are managed as pages.
	**/
	NSArray *favourites;

	/**
	 * An integer that represents the currentPage. Refers to an index of stopList.
	**/
	NSUInteger currentPage;

	/**
	 * The dotted page control mechanism at the top of the PID in the black bar.
	**/
	UIPageControl *pageControl;

	/**
	 * The PID that we are displaying as the current page.
	**/
	PIDViewControllerOld *currentPID;

	/**
	 * A container view that holds the PID and two fake pids to either side of the real PID. Used for swiping animations
	**/
	UIView *viewContainer;

	/**
	 * Whether this is a nearby favourite PID
	**/
	BOOL isFavouritePIDs;

	/**
	 * The Location Manager used to find our nearest favourite stop if needed
	**/
	LocationManager *locationManager;

	/**
	 * A counter that watches to see how many times the location manager has returned the same favourite stop.
	 * After 2-3 times we stop the location manager.
	**/
	NSUInteger sameStopResult;

	/**
	 * When a touch is started inside the SwipeCaptureView we store it here for later calculations in the swiping animation.
	**/
	CGPoint touchStarted;

	/**
	 * When a touch is ended inside the SwipeCaptureView we store it here for later calculations in the swiping animation.
	**/
	CGPoint touchEnded;

	/**
	 * A copy of the last touched location, used for calculating the swipe animations
	**/
	CGPoint lastTouch;

	/**
	 * The time that the user started touching inside the SwipeCaptureView. Used to determine swipe animation speed.
	**/
	NSTimeInterval touchStartedTime;

	/**
	 * The Speed at which the user swiped their finger across the screen in pixels per second. Used to calculate the animation duration
	**/
	CGFloat touchAnimationSpeed;
	
	/**
	 * A fake PID sitting offscreen to the left of the real PID. When the user drags to the right this fake PID is moved into view.
	**/
	UIImageView *leftFakePID;

	/**
	 * A fake PID sitting offscreen to the right of the real PID. When the user drags to the left this fake PID is moved into view.
	**/
	UIImageView *rightFakePID;
	
	UINavigationController *oldNavigationController;
	
	Filter *filter;
	NSInteger showMessages;
	BOOL pagingAvailable;
	BOOL editing;
	NSDate *locationUpdateStarted;
}

@property (nonatomic, strong) NSArray *favourites;
@property (nonatomic, strong) IBOutlet UIPageControl *pageControl;
@property (nonatomic, strong) PIDViewControllerOld *currentPID;
@property (nonatomic, strong) IBOutlet UIView *viewContainer;
@property (nonatomic) NSUInteger currentPage;
@property (nonatomic) BOOL isFavouritePIDs;
@property (nonatomic, strong) LocationManager *locationManager;
@property (nonatomic, strong) UIImageView *leftFakePID;
@property (nonatomic, strong) UIImageView *rightFakePID;
@property (nonatomic, strong) Filter *filter;
@property (nonatomic) NSInteger showMessages;
@property (nonatomic, getter=isPagingAvailable) BOOL pagingAvailable;
@property (nonatomic) BOOL editing;

/**
 * Custom Initialiser - Specific Stop
 *
 * Calls -displayInitialPID
 *
 * @param	favouriteStops	An array of FavouriteStop objects to manage as pages
 * @param	startingIndex	The first trackerID to display (the page to start on)
**/
- (id)initWithFavourites:(NSArray *)favouriteStops withStartingIndex:(NSUInteger)startingIndex;

/**
 * Custom Initialiser - Nearest Favourite Stop
 *
 * Calls -displayNearbyFavouritePID
 *
 * @param	favouriteStops		An array of FavouriteStop objects to manage as pages
**/
- (id)initNearestWithFavourites:(NSArray *)favouriteStops;

/**
 * The user has used the Page Control to change to a different page. Start the animation to slide the
 * appropriate fake PID into place.
**/
- (IBAction)changePage:(id)sender;

/**
 * A UIViewAnimationDelegate method.
 *
 * Is called when the swiping animation has finished. This will remove the old (now hidden) PID, draw a 
 * new PID on top of the fake PID and then move the fake PIDS back to their location off the sides of the screen.
**/
- (void)changeToNewPIDAfterAnimation:(NSString *)animationID finished:(BOOL)finished context:(NSString *)context;

/**
 * A CLLocationManagerDelegate method. Called when the location manager has found a new location for us.
 *
 * Checks to see if our nearest favourite has changed and changes to that "page" if so. Increments the sameStopResult counter if not
**/
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations;

/**
 * A CLLocationManagerDelegate method. Called when the location manager received an error (generally "permission denied").
 *
 * Display the first page in the PID if one has not yet been displayed.
**/
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error;

/**
 * Touch event received from the SwipeCaptureView - the user has touched the screen.
 *
 * Stores the touch location and time for later calculations
**/
- (BOOL)touchesDidBegin:(NSSet *)touches withEvent:(UIEvent *)event;

/**
 * Touch event received from the SwipeCaptureView - the user has moved their finger.
 *
 * Calculates the offset from the most recent touch event and moves the real/fake pids
 * accordingly. If they have been moved more than 100px in either direction it triggers
 * the touchesEnded event.
**/
- (BOOL)touchesDidMove:(NSSet *)touches withEvent:(UIEvent *)event;

/**
 * Touch event received from the SwipeCaptureView - the user has cancelled the touch.
 *
 * Triggers a touchesEnded event.
**/
- (BOOL)touchesWereCancelled:(NSSet *)touches withEvent:(UIEvent *)event;

/**
 * Touch event received from the SwipeCaptureView - the user has finished touching the screen.
 *
 * If the PIDs have been moved by 100px or more in either direction it starts the swipe animation.
 * Otherwise it animates snapping the PIDs back into the appropriate places.
**/
- (BOOL)touchesDidEnd:(NSSet *)touches withEvent:(UIEvent *)event;

/**
 * Flip the Paged PID over to show the PredictedListView
**/
- (void)flipToListView;

/**
 * Stops the current PID's timers.
**/
- (void)stop;

- (void)displayStopFromTimer:(NSTimer *)aTimer;
- (void)displayStopWithLocation:(CLLocation *)location;


@end