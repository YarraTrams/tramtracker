//
//  OnboardViewController.h
//  tramTRACKER
//
//  Created by Robert Amos on 21/04/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "Journey.h"
#import "OnboardListView.h"
#import "PidsService.h"
#import "RouteInfoView.h"
#import "Location.h"
#import "ActivityBarButtonController.h"

extern const NSInteger RouteLabelHeight;
extern const NSInteger RouteLabelHeightWithMessage;
extern const NSInteger OnboardRefreshInterval;

@interface OnboardViewController : UIViewController {
	OnboardListView *listViewController;
	RouteInfoView *routeInfo;
	Journey *journey;
	PidsService *service;
	BOOL hasMessage;
	LocationManager *locationManager;
	NSTimer *timerRefresh;
	UIBarButtonItem *locateButton;
	UIBarButtonItem *stopButton;
	ActivityBarButtonController *activityController;
	NSInteger accuracyLevel;

	/**
	 * Whether we've stopped
	 **/
	BOOL isStopped;
	BOOL atLayover;
	NSInteger nilJourneyCount;
	NSDate *startedLocating;
	NSInteger updateRequestCount;
	NSInteger seenGPSTipCount;
	NSDate *tipFirstShownDate;
	NSInteger seenMapTipCount;
	NSDate *mapTipFirstShownDate;
}

@property (nonatomic, strong) OnboardListView *listViewController;
@property (nonatomic, strong) PidsService *service;
@property (nonatomic, strong) RouteInfoView *routeInfo;
@property (nonatomic) BOOL hasMessage;
@property (nonatomic, strong) LocationManager *locationManager;
@property (nonatomic, strong) NSTimer *timerRefresh;
@property (nonatomic, strong) UIBarButtonItem *locateButton;
@property (nonatomic, strong) UIBarButtonItem *stopButton;
@property (nonatomic, strong) ActivityBarButtonController *activityController;
@property (nonatomic, getter=isAtLayover) BOOL atLayover;
@property (nonatomic) BOOL isStopped;
@property (nonatomic, strong) NSDate *startedLocating;
@property (nonatomic) NSInteger accuracyLevel;


- (void)toggleRouteInfo;
- (void)toggleRouteInfoStopped:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context;
- (void)setHasMessage:(BOOL)newHasMessage animated:(BOOL)animated;

/**
 * Start updating the predictions
 **/
- (void)startUpdating;
- (void)startUpdatingImmediately;
- (void)stopUpdating;
- (void)refreshPredictions:(NSTimer *)aTimer;
- (void)toggleLocating:(NSNumber *)isLocating;

- (void)applicationDidBecomeActive;
- (void)applicationDidEnterBackground;
- (void)setMessage:(NSString *)message;
- (NSString *)message;

- (BOOL)hasTramChangedRoute:(Tram *)tram;
- (void)updateHeadboardMessage:(BOOL)animated;
- (BOOL)hasBadConnection;
- (Journey *)journey;
- (void)setJourney:(JourneyStub *)aJourneyStub;

@end
