//
//  HelpFavouritesController.m
//  tramTRACKER
//
//  Created by Robert Amos on 5/05/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "HelpFavouritesController.h"

const NSString *HelpURLSubdirFavourites = @"favourites";

#define HELP_FAVOURITES_ADD 0
#define HELP_FAVOURITES_VIEW 1
#define HELP_FAVOURITES_NEAREST 2
#define HELP_FAVOURITES_FILTERS 3
#define HELP_FAVOURITES_EDIT 4
#define HELP_FAVOURITES_GROUPS 5

@implementation HelpFavouritesController


- (id)initHelpFavouritesController
{
	if (self = [super initWithStyle:UITableViewStylePlain])
	{
		[self setTitle:NSLocalizedString(@"help-favourites", @"Help")];
		[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
	}
	return self;
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}


#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 6;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    SimpleCell *cell = (SimpleCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[SimpleCell alloc] initWithCellStyle:TTSimpleCellStyleDefault reuseIdentifier:CellIdentifier];
		[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
		
		GradientBackgroundCell *background = [[GradientBackgroundCell alloc] initWithFrame:CGRectMake(0, 0, 320, 44) cellStyle:OnboardCellStyleNone];
		[cell setBackgroundView:background];
	}
    
	switch (indexPath.row)
	{
		case HELP_FAVOURITES_ADD:
			[cell setTextLabelText:NSLocalizedString(@"help-favourites-add", @"Add To Favourites")];
			break;
		case HELP_FAVOURITES_VIEW:
			[cell setTextLabelText:NSLocalizedString(@"help-favourites-view", @"Viewing Favourites")];
			break;
		case HELP_FAVOURITES_NEAREST:
			[cell setTextLabelText:NSLocalizedString(@"help-favourites-nearest", @"Nearest Favourites")];
			break;
		case HELP_FAVOURITES_FILTERS:
			[cell setTextLabelText:NSLocalizedString(@"help-favourites-filters", @"Favourites and Filters")];
			break;
		case HELP_FAVOURITES_EDIT:
			[cell setTextLabelText:NSLocalizedString(@"help-favourites-edit", @"Editing Favourites")];
			break;
		case HELP_FAVOURITES_GROUPS:
			[cell setTextLabelText:NSLocalizedString(@"help-favourites-groups", @"Favourite Groups")];
			break;
	}
	
    return cell;	
	
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

	// we create a UIWebView that we push onto the stack, start by working out our URL
	NSString *fileName = nil;
	
	switch (indexPath.row)
	{
		case HELP_FAVOURITES_ADD:
			fileName = @"adding15.html";
			break;
			
		case HELP_FAVOURITES_VIEW:
			fileName = @"viewing15.html";
			break;
			
		case HELP_FAVOURITES_NEAREST:
			fileName = @"nearest.html";
			break;
			
		case HELP_FAVOURITES_FILTERS:
			fileName = @"filters.html";
			break;
			
		case HELP_FAVOURITES_EDIT:
			fileName = @"editing12.html";
			break;
			
		case HELP_FAVOURITES_GROUPS:
			fileName = @"groups.html";
			break;
	}
	
	// nothing found?
	if (fileName == nil)
		return;
	
	// create the web view
	HelpWebController *helpWeb = [[HelpWebController alloc] initWithHelpFile:[NSString stringWithFormat:@"%@/%@", HelpURLSubdirFavourites, fileName]];
	SimpleCell *cell = (SimpleCell *)[self.tableView cellForRowAtIndexPath:indexPath];
	[helpWeb setTitle:cell.textLabel.text];
	[self.navigationController pushViewController:helpWeb animated:YES];
}




@end

