//
//  HelpAboutController.m
//  tramTRACKER
//
//  Created by Robert Amos on 6/05/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "HelpAboutController.h"
#import "NSData+Base64.h"


@implementation HelpAboutController

@synthesize version, logo, bodyLabel1, bodyLabel2;

- (id)initHelpAboutController
{
	NSString *nibName = [[NSDate dateWithTimeIntervalSince1970:1259425800] timeIntervalSinceNow] < 0 ? @"HelpAbout2" : @"HelpAbout";
	if (self = [super initWithNibName:nibName bundle:[NSBundle mainBundle]])
	{
		[self setTitle:NSLocalizedString(@"help-about", @"About tramTRACKER")];
	}
	return self;
}

- (void)viewDidLoad
{
	[self.version setText:[NSString stringWithFormat:@"%@ %@", [self.version text], [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]]];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	if (fT==nil||[fT timeIntervalSinceNow]<-10){fT=nil;fT=[NSDate date];x=nil;x=[[NSMutableString alloc] initWithCapacity:0];}cTP=[[touches anyObject] locationInView:self.view];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	CGFloat xDiff,yDiff;CGPoint point = [[touches anyObject] locationInView:self.view];xDiff=point.x-cTP.x;yDiff=point.y-cTP.y;
	if(yDiff>100)[x appendString:@"d"];else if(yDiff<-100)[x appendString:@"u"];else if(xDiff>100)[x appendString:@"r"];else if (xDiff<-100)[x appendString:@"l"];
	else if([[touches anyObject] tapCount]==2)[x appendString:@"b"];else if(point.x<self.logo.frame.origin.x+self.logo.frame.size.width&&point.x>self.logo.frame.origin.x&&point.y<self.logo.frame.origin.y+self.logo.frame.size.height&&point.y>self.logo.frame.origin.y)[x appendString:@"a"];
	if([x isEqualToString:@"uuddlrlrba"]){x=nil;NSString*s=[[NSString alloc]initWithData:[NSData dataFromBase64String:@"Q3JlZGl0cw=="] encoding:NSUTF8StringEncoding];
	NSString*d=[[NSString alloc] initWithData:[NSData dataFromBase64String:[NSString stringWithFormat:@"%@%@%@%@%@",@"QnVpbHQgYnkgdGhlIGRldmVsb3BtZW50IHRlYW0gYXQgWWFycm",@"EgVHJhbXMgaW4gMjAwOS4KCk1hbmFnZXI6ICAgICAgICAgIEFuZHJlIFNvY2hhClRlYW",@"0gTGVhZGVyOiAgIE1hcnNlbGluYSBXaWhhcnRvCldlYiBTZXJ2aWN",@"lOiAgICBUZXJlbmNlIFRoYW0KaVBob25lIEFwcDogI",@"CAgICBSb2JlcnQgQW1vcw=="]] encoding:NSUTF8StringEncoding];
	[self.version setText:s];[self.bodyLabel1 setText:d];[self.bodyLabel2 setText:@""];s=nil;d=nil;}
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}




@end
