////
////  TicketOutletServiceDelegate.m
////  tramTRACKER
////
////  Created by Robert Amos on 5/08/10.
////  Copyright 2010 Metro Trains Melbourne. All rights reserved.
////
//
//#import "TicketOutletServiceDelegate.h"
//#import "NSDate+NSDateRFC1123.h"
//
//@interface TicketOutletServiceDelegate()
//
//@property (weak, nonatomic) NSObject *delegate;
//@property (strong, nonatomic) NSMutableString * responseText;
//
//@end
//
//@implementation TicketOutletServiceDelegate
//
//- (id)initWithDelegate:(NSObject *)aDelegate
//{
//	if (self = [super init])
//		self.delegate = aDelegate;
//	return self;
//}
//
//#pragma mark -
//#pragma mark NSURLConnection delegate methods
//
//// Connection failed with error
//- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
//{
//	// For now, just log the error and silently fail
//	//NSLog(@"[TOS] Error with NSURLConnection %@ - $@", connection, error);
//	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
//	
//	[self failWithError:error];
//	
//	// let the delegate know
//	if ([self.delegate respondsToSelector:@selector(connection:didFailWithError:)])
//		[self.delegate performSelector:@selector(connection:didFailWithError:) withObject:connection withObject:error];
//}
//
//// We received a response from the server
//- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
//{
//	// Start a new empty responseText that we can append to
//	self.responseText = [NSMutableString string];
//	
//	//NSLog(@"Received response to connection %@, text encoding is %@", connection, [response textEncodingName]);
//	
//	// if the delegate implements it, let it know we received a response
//	if ([self.delegate respondsToSelector:@selector(connection:didReceiveResponse:)])
//		[self.delegate performSelector:@selector(connection:didReceiveResponse:) withObject:connection withObject:response];
//
//	// for the check method, this is all we need
//	if ([self serviceType] == TicketOutletServiceDelegateCheck)
//		[self doesTicketOutletsHaveUpdatesUsingResponse:(NSHTTPURLResponse *)response];
//}
//
//
//// We received a chunk of data from the server
//- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
//{
//	// append the data to our responseText string
//	NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//	[self.responseText appendString:str];
//	str = nil;
//	
//	// if the delegate implements it, let it know we received data
//	if ([self.delegate respondsToSelector:@selector(connection:didReceiveData:)])
//		[self.delegate performSelector:@selector(connection:didReceiveData:) withObject:connection withObject:data];
//}
//
//// The connection has finished loading, start parsing
//- (void)connectionDidFinishLoading:(NSURLConnection *)connection
//{
//	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
//	
//	// let the delegate know
//	if ([self.delegate respondsToSelector:@selector(connectionDidFinishLoading:WithResponseText:)])
//		[self.delegate performSelector:@selector(connectionDidFinishLoading:WithResponseText:) withObject:connection withObject:self.responseText];
//
//	// clear the response
//	 self.responseText = nil;
//}
//
//- (void)failWithError:(NSError *)error
//{
//	// check the delegate to see if we can report the error
//	if ([self.delegate respondsToSelector:@selector(ticketOutletServiceDidFailWithError:)])
//	{
//		[self.delegate performSelectorOnMainThread:@selector(ticketOutletServiceDidFailWithError:) withObject:error waitUntilDone:NO];
//	}
//}
//
//#pragma mark -
//#pragma mark Methods for dealing with the return result
//
//- (void)doesTicketOutletsHaveUpdatesUsingResponse:(NSHTTPURLResponse *)response
//{
//	// check our response for the last modified time
//	// Look in the headers for a Last-Modified date.
//	//NSLog(@"[TOS] received HEAD response");
//	NSDictionary *allHeaders = [response allHeaderFields];
//	if (allHeaders == nil)
//		return;
//	
//	NSString *lastModified = [allHeaders objectForKey:@"Last-Modified"];
//	if (lastModified == nil)
//		return;
//	
//	// parse it
//    NSDate *lastModifiedDate = [NSDate dateFromRFC1123:lastModified];
//	//NSLog(@"[TOS] Last Updated Date: %@ (%@)", lastModified, lastModifiedDate);
//    
//	/*NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//	[formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
//	[formatter setDateFormat:@"EEE',' dd MMM yyyy HH':'mm':'ss 'GMT'"];
//	[formatter setLocale:[[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] autorelease]];
//	NSDate *lastModifiedDate = [formatter dateFromString:lastModified];
//	[formatter release];*/
//    
//    
//	
//	// now that we have the date, compare it with the original update
//	BOOL hasUpdates = [lastModifiedDate compare:[self updatesSinceDate]] == NSOrderedDescending;
//	//NSLog(@"has updates %@ - last modified: %@, update since: %@", hasUpdates ? @"YES" : @"NO", lastModifiedDate, self.updatesSinceDate);
//	
//	// grab the file size in bytes too
//	NSNumber *fileSize = [NSNumber numberWithDouble:[[allHeaders objectForKey:@"Content-Length"] doubleValue]];
//	
//	// tell the delegate
//	if ([self.delegate respondsToSelector:@selector(setHasTicketOutletUpdates:withFileSize:)])
//		[self.delegate performSelector:@selector(setHasTicketOutletUpdates:withFileSize:) withObject:[NSNumber numberWithBool:hasUpdates] withObject:fileSize];
//}
//
//- (void)updateTicketOutletsWithData:(NSData *)data
//{
//	// save this text to the file
//	NSString *file = [NSTemporaryDirectory() stringByAppendingPathComponent:@"TicketRetailers.plist"];
//	
//	// write it to the file
//	NSError *error = nil;
//	BOOL didWriteToFile = [data writeToFile:file atomically:YES];
//
//	// tell the delegate if it failed
//	if (!didWriteToFile && [self.delegate respondsToSelector:@selector(ticketServicedidFailWithError:)])
//		[self.delegate performSelector:@selector(ticketServicedidFailWithError:) withObject:error];
//
//	// all done, try to open the file
//	NSArray *ticketOutlets = [NSArray arrayWithContentsOfFile:file];
//	
//	// remove the file
//	NSFileManager *fileManager = [NSFileManager defaultManager];
//	[fileManager removeItemAtPath:file error:NULL];
//	
//
//	// tell the delegate that we got it
//	if ([self.delegate respondsToSelector:@selector(setTicketOutlets:)])
//		[self.delegate performSelector:@selector(setTicketOutlets:) withObject:ticketOutlets];
//}
//
//
//- (void)dealloc
//{
//	 self.responseText = nil;
//}
//
//@end
