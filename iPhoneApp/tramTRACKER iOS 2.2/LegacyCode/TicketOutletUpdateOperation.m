////
////  TicketOutletUpdateOperation.m
////  tramTRACKER
////
////  Created by Robert Amos on 5/08/10.
////  Copyright 2010 __MyCompanyName__. All rights reserved.
////
//
//#import "TicketOutletUpdateOperation.h"
//#import "BackgroundSynchroniser.h"
//#import "TicketOutletService.h"
//#import "TicketRetailer.h"
//#import "Constants.h"
//
//@implementation TicketOutletUpdateOperation
//
//@synthesize syncManager;
//
//- (id)init
//{
//	if (self = [super init])
//	{
//		service = [[TicketOutletService alloc] initWithDelegate:self];
//
//		[self willChangeValueForKey:@"isExecuting"];
//		executing = NO;
//		[self didChangeValueForKey:@"isExecuting"];
//		
//		[self willChangeValueForKey:@"isFinished"];
//		finished = NO;
//		[self didChangeValueForKey:@"isFinished"];
//		
//		// set our priority
//		[self setQueuePriority:NSOperationQueuePriorityNormal];
//	}
//	return self;
//}
//
//- (BOOL)isConcurrent
//{
//	return NO;
//}
//
//- (BOOL)isExecuting
//{
//	return executing;
//}
//
//- (BOOL)isFinished
//{
//	return finished;
//}
//
///**
// * Actually start processing the update
// **/
//- (void)main
//{
//	[self start];
//}
//- (void)start
//{
//    if ([self isCancelled])
//    {
//        [self finish];
//        return;
//    }
//    
//	// we start by getting the destinations for this route
//	[self willChangeValueForKey:@"isExecuting"];
//	executing = YES;
//	[self didChangeValueForKey:@"isExecuting"];
//	
//	// send a notification
//	if (self.syncManager != nil && self.syncManager.delegate != nil && [self.syncManager.delegate respondsToSelector:@selector(didStartTicketOutlets)])
//		[self.syncManager.delegate performSelectorOnMainThread:@selector(didStartTicketOutlets) withObject:nil waitUntilDone:NO];
//	
//	// this is easy, call the PidsService to grab it
//	[self updateTicketOutlets];
//
//	// sigh, poll until its finished
//	while (![self isFinished])
//		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
//}
//
//- (void)updateTicketOutlets
//{
//	[service getTicketOutletUpdates];
//}
//
//- (void)setTicketOutlets:(NSArray *)ticketOutlets
//{
//	// make sure we're not cancelled
//	if ([self isCancelled])
//	{
//		[self finish];
//		return;
//	}
//	
//	// nothing?
//	if (ticketOutlets == nil)
//	{
//		[self finish];
//		return;
//	}
//
//	// basically, as long as we just got a new batch of ticket outlets, update the file
//	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
////	BOOL didUpdateTicketOutlets = [ticketOutlets writeToFile:d.ticketOutletsFile atomically:NO];
////	if (didUpdateTicketOutlets)
////	{
////		// manually update the file's modification date
////		//NSFileManager *fileManager = [NSFileManager defaultManager];
////		//[fileManager setAttributes:[NSDictionary dictionaryWithObject:[NSDate date] forKey:NSFileModificationDate] ofItemAtPath:d.ticketOutletsFile error:NULL];
////	
////		
////		// Kick off a new import
////		[self updateCoreDataTicketOutletsFromFile:d.ticketOutletsFile];
////	}
//	
//	// all done
//	[self finish];
//}
//
//- (void)pidsServiceDidFailWithError:(NSError *)error
//{
//	static NSInteger previousFailureCount = 0;
//	
//	if ([error code] == PIDServiceErrorTimeoutReached && previousFailureCount == 0)
//	{
//		// try it again
//		[self start];
//		previousFailureCount = 1;
//		
//	} else
//	{
//		[self cancel];
//		[self finish];
//		
//		// send a failure message to the delegate
//		if (self.syncManager != nil && self.syncManager.delegate != nil && ![self isCancelled] && [self.syncManager.delegate respondsToSelector:@selector(syncDidFailWithError:)])
//			[self.syncManager.delegate performSelectorOnMainThread:@selector(syncDidFailWithError:) withObject:error waitUntilDone:NO];
//	}
//}
//
//
//- (void)updateCoreDataTicketOutletsFromFile:(NSString *)file
//{
//	// Step 1, open the plist
//	NSArray *list = [NSArray arrayWithContentsOfFile:file];
//	
//	// Step 2, get our context
//	NSManagedObjectContext *importContext = [self.syncManager managedObjectContext];
//    [importContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
//	
//	// Step 3, clear out all existing ticket retailers (if any)
//	NSFetchRequest *fetchRequest = [NSFetchRequest new];
//	
//	// set the entity that we're looking for
//	NSEntityDescription *entity = [NSEntityDescription entityForName:@"TicketRetailer" inManagedObjectContext:importContext];
//	[fetchRequest setEntity:entity];
//	
//	// fetch it.
//	NSError *outError = nil;
//	NSArray *fetchedObjects = [importContext executeFetchRequest:fetchRequest error:&outError];
//	 fetchRequest = nil;
//	//NSLog(@"Fetched existing ticket outlets. Error? %@", outError);
//	
//	// find any?
//	if (fetchedObjects != nil && [fetchedObjects count] > 0)
//	{
//		// remove all of them
//		for (TicketRetailer *t in fetchedObjects)
//		{
//			[importContext deleteObject:t];
//		}
//		// save that
//		[importContext save:&outError];
//	}
//	
//	//NSLog(@"[TICKETUPDATES] All objects removed. Error? %@", outError);
//	
//	// now we go adding new ones!
//	for (NSDictionary *nt in list)
//	{
//		// create the new TicketRetailer
//		TicketRetailer *newTicketRetailer = (TicketRetailer *)[NSEntityDescription insertNewObjectForEntityForName:@"TicketRetailer"
//																							 inManagedObjectContext:importContext];
//		
//		// set all the bits
//		[newTicketRetailer setName:[[nt objectForKey:@"name"] stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"]];
//		[newTicketRetailer setAddress:[[nt objectForKey:@"address"] stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"]];
//		[newTicketRetailer setOpen24Hour:[nt objectForKey:@"24hour"]];
//		[newTicketRetailer setSellsMyki:[nt objectForKey:@"myki"]];
//		[newTicketRetailer setLatitude:[NSDecimalNumber decimalNumberWithString:[nt objectForKey:@"latitude"]]];
//		[newTicketRetailer setLongitude:[NSDecimalNumber decimalNumberWithString:[nt objectForKey:@"longitude"]]];
//		[newTicketRetailer setSuburb:[nt objectForKey:@"suburb"]];
//	}
//	[importContext save:&outError];
//	//NSLog(@"[TICKETUPDATES] All new objects added. Error? %@", outError);
//}
//
//- (void)finish
//{
//	[self willChangeValueForKey:@"isExecuting"];
//	executing = NO;
//	[self didChangeValueForKey:@"isExecuting"];
//	
//	[self willChangeValueForKey:@"isFinished"];
//	finished = YES;
//	[self didChangeValueForKey:@"isFinished"];
//	
//	// send a notification
//	if (self.syncManager != nil && self.syncManager.delegate != nil && ![self isCancelled] && [self.syncManager.delegate respondsToSelector:@selector(syncDidUpdateTicketOutlets)])
//		[self.syncManager.delegate performSelectorOnMainThread:@selector(syncDidUpdateTicketOutlets) withObject:nil waitUntilDone:NO];
//
//	// tell the world
//	[[NSNotificationCenter defaultCenter] postNotificationName:TTTicketOutletsHaveBeenUpdatedNotice object:nil];
//
//	//NSLog(@"Ticket Outlets updated");
//}
//
//
//@end
