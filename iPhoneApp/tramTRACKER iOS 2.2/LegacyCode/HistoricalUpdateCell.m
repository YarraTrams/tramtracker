//
//  HistoricalUpdateCell.m
//  tramTRACKER
//
//  Created by Robert Amos on 5/08/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "HistoricalUpdateCell.h"
#import "SimpleCell.h"
#import "GradientBackgroundCell.h"

@implementation HistoricalUpdateCell

- (id)initWithCellStyle:(TTSimpleCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
	if (self = [super initWithCellStyle:style reuseIdentifier:reuseIdentifier])
	{
		GradientBackgroundCell *background = [[GradientBackgroundCell alloc] initWithFrame:CGRectMake(0, 0, 320, 44) cellStyle:OnboardCellStyleNone];
		[self setBackgroundView:background];
	}
	return self;
}



@end
