//
//  ActivityBarButtonController.h
//  tramTRACKER
//
//  Created by Robert Amos on 7/04/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ActivityBarButtonController : UIViewController

@property (nonatomic, assign) NSInteger accuracyLevel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil target:(id)clickTarget action:(SEL)clickAction;
- (void)touchDown;
- (void)touchUpInside;
- (void)touchUpOutside;

@end
