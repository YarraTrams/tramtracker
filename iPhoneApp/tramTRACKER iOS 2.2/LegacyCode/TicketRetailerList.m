//
//  TicketRetailerList.m
//  tramTRACKER
//
//  Created by Robert Amos on 24/07/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "TicketRetailerList.h"
#import "TicketRetailerViewController.h"


@implementation TicketRetailerList

@synthesize searchBar;

#pragma mark -
#pragma mark Initialization

- (id)initTicketRetailerList
{
	if (self = [self initWithStyle:UITableViewStylePlain])
	{
		[self.navigationItem setTitle:NSLocalizedString(@"title-tickets", @"Ticket Outlets")];
		
	}
	return self;
}

- (id)initWithStyle:(UITableViewStyle)style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    if ((self = [super initWithStyle:style])) {
    }
    return self;
}

#pragma mark -
#pragma mark Core Data Methods

- (NSFetchedResultsController *)resultsController
{
	// has our fetched results controller been created?
	if (resultsController == nil)
	{
		tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
		NSManagedObjectContext *context = [d managedObjectContext];
		
		//Configure the sort descriptor
		NSSortDescriptor *sortDescriptorSuburb = [[NSSortDescriptor alloc] initWithKey:@"suburb" ascending:YES];
		NSSortDescriptor *sortDescriptorName = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
		NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorSuburb, sortDescriptorName, nil];
		
		// Create the fetch request
		NSFetchRequest *request = [[NSFetchRequest alloc] init];
		[request setEntity:[NSEntityDescription entityForName:@"TicketRetailer" inManagedObjectContext:context]];
		
		[request setSortDescriptors:sortDescriptors];
		
		// Create our fetched results controller, finally
		resultsController = [[NSFetchedResultsController alloc]
							 initWithFetchRequest:request
							 managedObjectContext:context
							 sectionNameKeyPath:@"suburb"
							 cacheName:@"tickets"];
		
		// Perform the fetch
		NSError *error;
		if (![resultsController performFetch:&error])
		{
			//NSLog(@"error: %@", error);
		}
	}
	return resultsController;
}

- (void)refreshResults:(NSNotification *)note
{
	[self refreshResults];
}
- (void)refreshResults
{
	// clear off the existing results controller
	 resultsController = nil;

	// next time someone requests it it will be recreated.
}
 
- (NSFetchedResultsController *)searchResultsController
{
	// has our fetched results controller been created?
	if (searchResultsController == nil)
	{
		tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
		NSManagedObjectContext *context = [d managedObjectContext];
		
		//Configure the sort descriptor
		NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
		NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
		
		// Create the fetch request
		NSFetchRequest *request = [[NSFetchRequest alloc] init];
		[request setEntity:[NSEntityDescription entityForName:@"TicketRetailer" inManagedObjectContext:context]];
		
		[request setSortDescriptors:sortDescriptors];
		
		// Create our fetched results controller, finally
		searchResultsController = [[NSFetchedResultsController alloc]
							 initWithFetchRequest:request
							 managedObjectContext:context
							 sectionNameKeyPath:nil
							 cacheName:@"ticketSearch"];
	}
	
	// now we set the predicate
	return searchResultsController;
}

- (void)setSearchResultsControllerSearchString:(NSString *)searchString
{
	NSFetchedResultsController *searchResults = [self searchResultsController];

	// less than 3 characters? clear the cache and return something that will definitely find no results.
	if ([searchString length] < 3)
	{
		NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"name = %@", @"DOESNOTEXIST!!!"];
		[searchResults.fetchRequest setPredicate:searchPredicate];

		[NSFetchedResultsController deleteCacheWithName:@"ticketSearch"];
		
		// perform the fetch again
		NSError *error = nil;
		if (![searchResults performFetch:&error])
		{
			//NSLog(@"Search error: %@, search controller: %@", error, searchResults);
		}
		
	} else
	{
		// Create a new predicate based on the search string
		NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@ || address CONTAINS[cd] %@", searchString, [NSString stringWithFormat:@"%@", searchString]];
		[searchResults.fetchRequest setPredicate:searchPredicate];
		
		// delete the cache
		[NSFetchedResultsController deleteCacheWithName:@"ticketSearch"];
		
		// perform the fetch again
		NSError *error = nil;
		if (![searchResults performFetch:&error])
		{
			//NSLog(@"Search error: %@, search controller: %@", error, searchResults);
		}
	}
	
}

#pragma mark -
#pragma mark View lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];

	// Reset our background colour
	[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
	
	// add the table footer
	[self.tableView setTableFooterView:[self tableFooterView]];

	// Create the search bar
	searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 320, 45)];
	[searchBar setPlaceholder:NSLocalizedString(@"ticket-outlet-search-placeholder", @"Search Ticket Outlets")];
	[searchBar setTintColor:[UIColor colorWithRed:TTSearchBarTintColorRed green:TTSearchBarTintColorGreen blue:TTSearchBarTintColorBlue alpha:1.0]];
	
	// Add the search bar to the view
	[self.tableView setTableHeaderView:searchBar];
	[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
	
	
	// we also need to create the searchdisplaycontroller
	UISearchDisplayController *displayController = [[UISearchDisplayController alloc] initWithSearchBar:searchBar contentsController:self];
	[displayController setDelegate:self];
	[displayController setSearchResultsDelegate:self];
	[displayController setSearchResultsDataSource:self];

	// register ourselves for notifications
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshResults:) name:TTTicketOutletsHaveBeenUpdatedNotice object:nil];
}



/*
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
*/

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];


	// display import button
	/*UIBarButtonItem *import = [[UIBarButtonItem alloc] initWithTitle:@"Import" style:UIBarButtonItemStylePlain target:self action:@selector(importTicketRetailers)];
	[self.navigationItem setRightBarButtonItem:import];
	[import release]; */
}

//
// Create the table footer
//
- (UIView *)tableFooterView
{
	CGRect frame = CGRectMake(0, 0, self.tableView.frame.size.width, 10);
	UnderBackgroundView *ub = [[UnderBackgroundView alloc] initWithFrame:frame];
	return ub;
}

- (void)importTicketRetailers
{
	
	// Step 1, open the plist
	NSString *file = [[NSBundle mainBundle] pathForResource:@"TicketRetailers" ofType:@"plist"];
	NSArray *list = [NSArray arrayWithContentsOfFile:file];

	// Step 2, get our context
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *importContext = nil;
    if ([NSManagedObjectContext instancesRespondToSelector:@selector(initWithConcurrencyType:)])
        importContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSConfinementConcurrencyType];
    else
        importContext = [[NSManagedObjectContext alloc] init];
	[importContext setPersistentStoreCoordinator:[d persistentStoreCoordinator]];
	[importContext setUndoManager:nil];
	
	// Step 3, clear out all existing ticket retailers (if any)
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	// set the entity that we're looking for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"TicketRetailer" inManagedObjectContext:importContext];
	[fetchRequest setEntity:entity];
	
	// fetch it.
	NSError *outError;
	NSArray *fetchedObjects = [importContext executeFetchRequest:fetchRequest error:&outError];
	
	// find any?
	if (fetchedObjects != nil && [fetchedObjects count] > 0)
	{
		// remove all of them
		for (TicketRetailer *t in fetchedObjects)
		{
			[importContext deleteObject:t];
		}
		// save that
		[importContext save:&outError];
	}
	
	// now we go adding new ones!
	for (NSDictionary *nt in list)
	{
		// create the new TicketRetailer
		TicketRetailer *newTicketRetailer = (TicketRetailer *)[NSEntityDescription insertNewObjectForEntityForName:@"TicketRetailer"
																										   inManagedObjectContext:importContext];
		
		// set all the bits
		[newTicketRetailer setName:[[nt objectForKey:@"name"] stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"]];
		[newTicketRetailer setAddress:[[nt objectForKey:@"address"] stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"]];
		[newTicketRetailer setOpen24Hour:[nt objectForKey:@"24hour"]];
		[newTicketRetailer setSellsMyki:[nt objectForKey:@"myki"]];
		[newTicketRetailer setSellsMetcards:[nt objectForKey:@"metcard"]];
		[newTicketRetailer setLatitude:[NSDecimalNumber decimalNumberWithString:[nt objectForKey:@"latitude"]]];
		[newTicketRetailer setLongitude:[NSDecimalNumber decimalNumberWithString:[nt objectForKey:@"longitude"]]];
		[newTicketRetailer setSuburb:[nt objectForKey:@"suburb"]];
	}
	// all done
	[importContext save:&outError];
	 importContext = nil;
}

/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
	if (tableView == self.searchDisplayController.searchResultsTableView)
		return 1;
	
    return [[self.resultsController sections] count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	if (tableView == self.searchDisplayController.searchResultsTableView)
		return nil;

	// grab a OnboardListSection thingy
	OnboardListSectionHeaderView *view = [[OnboardListSectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 22)];
	
	// set the title
	[view.title setText:[self tableView:tableView titleForHeaderInSection:section]];
	[view.title setLineBreakMode:NSLineBreakByTruncatingMiddle];
	[view.title setTextAlignment:NSTextAlignmentLeft];
	
	return view;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	if (tableView == self.searchDisplayController.searchResultsTableView)
		return nil;

	id <NSFetchedResultsSectionInfo> sectionInfo = [[self.resultsController sections] objectAtIndex:section];
	return [sectionInfo name];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	if (tableView == self.searchDisplayController.searchResultsTableView)
		return 0;
    return 22;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // if its the search result, check our search results controller
	if (tableView == self.searchDisplayController.searchResultsTableView)
	{
		id <NSFetchedResultsSectionInfo> sectionInfo = [[self.searchResultsController sections] objectAtIndex:section];
		return [sectionInfo numberOfObjects];
	}
	// Return the number of rows in the section.
	id <NSFetchedResultsSectionInfo> sectionInfo = [[self.resultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}



// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    TicketRetailerCell *cell = (TicketRetailerCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
		cell = [[TicketRetailerCell alloc] initWithCellStyle:TTSimpleCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
	TicketRetailer *retailer = nil;
	
	// are we getting it from the normal list, or the search list?
	if (tableView == self.searchDisplayController.searchResultsTableView)
		retailer = (TicketRetailer *)[self.searchResultsController objectAtIndexPath:indexPath];
	else
		retailer = (TicketRetailer *)[self.resultsController objectAtIndexPath:indexPath];
    
	[cell.textLabel setText:retailer.name];
	[cell.detailTextLabel setText:retailer.address];
	[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

	// get the ticket retailer, create a view for it and throw it on the stack
	TicketRetailer *retailer = nil;
	
	// are we getting it from the normal list, or the search list?
	if (tableView == self.searchDisplayController.searchResultsTableView)
		retailer = (TicketRetailer *)[self.searchResultsController objectAtIndexPath:indexPath];
	else
		retailer = (TicketRetailer *)[self.resultsController objectAtIndexPath:indexPath];
	
	// Create the view
	TicketRetailerViewController *retailerViewController = [[TicketRetailerViewController alloc] initWithTicketRetailer:retailer];
	[self.navigationController pushViewController:retailerViewController animated:YES];
	 retailerViewController = nil;
}

#pragma mark -
#pragma mark Search Display Controller Delegate

- (void)searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller
{
	// change the tint on the search bar
	[controller.searchBar setTintColor:[UIColor colorWithRed:TTNavigationBarTintColorRed green:TTNavigationBarTintColorGreen blue:TTNavigationBarTintColorBlue alpha:1.0]];

	// since the results view is recreated, redo the background/footer
	[controller.searchResultsTableView setTableFooterView:[self tableFooterView]];
	[controller.searchResultsTableView setBackgroundColor:[self.tableView backgroundColor]];

}

- (void)searchDisplayControllerWillEndSearch:(UISearchDisplayController *)controller
{
	// change the tint on the search bar
	[controller.searchBar setTintColor:[UIColor colorWithRed:TTSearchBarTintColorRed green:TTSearchBarTintColorGreen blue:TTSearchBarTintColorBlue alpha:1.0]];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)searchController shouldReloadTableForSearchString:(NSString *)searchString
{
	// re-prime the search results controller
	[self setSearchResultsControllerSearchString:searchString];
	
	// yep go ahead and reload
	return YES;
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}


- (void)dealloc {

	[[NSNotificationCenter defaultCenter] removeObserver:self];

	// Release
	 resultsController = nil;
	 searchResultsController = nil;
}


@end

