    //
//  NearbyViewController.m
//  tramTRACKER
//
//  Created by Robert Amos on 25/07/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "NearbyViewControllerOld.h"
#import "NearbyListController.h"
#import "SWRevealViewController.h"

@interface NearbyViewControllerOld()

@property (nonatomic, strong) UISearchDisplayController *searchController;
@property (nonatomic, strong) UISearchBar *searchBar;


@end

@implementation NearbyViewControllerOld

@synthesize list, map;

- (id)initNearbyViewController
{
    self = [super initWithNibName:nil bundle:nil];
	if (self)
	{		
		[self.navigationItem setTitle:NSLocalizedString(@"title-nearby", @"Nearby")];
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	UIImage *searchBarBackground = [UIImage imageNamed:@"Nearby-Search-Bar-.png"];
	[self setSearchBar:[[UISearchBar alloc] initWithFrame:CGRectIntegral(CGRectMake(0.0f, 0.0f, searchBarBackground.size.width, searchBarBackground.size.height))]];
	[self.searchBar setBackgroundImage:searchBarBackground];
	[self.searchBar setTranslucent:NO];
	[self.searchBar setPlaceholder:NSLocalizedString(@"nearby-search-placeholder", @"Enter Stop Name, Number or Route Address")];
	
	[self.view addSubview:self.searchBar];
	 
	[self setSearchController:[[UISearchDisplayController alloc] initWithSearchBar:self.searchBar contentsController:self]];
	
	
	// we're ready to show the view, add the appropriate view to the stack
	NSString *defaultView = [self retrieveDefaultView];
	
	if (defaultView == nil || [defaultView isEqualToString:@"list"])
	{
		[self showListView];
	} else
	{
		// Nearby Map
		[self showMapView];
	}
}

- (void)viewDidLayoutSubviews
{
	[super viewDidLayoutSubviews];
	
	UIView *view = nil;
	
	if (self.list != nil)
	{
		view = self.list.view;
	}
	else if (self.map != nil)
	{
		view = self.map.view;
	}
	
	[self.searchBar setFrame:CGRectIntegral(CGRectMake(0.0f, 0.0f, self.searchBar.backgroundImage.size.width, self.searchBar.backgroundImage.size.height - 1))];
	
	CGFloat searchBarHeight = self.searchBar.bounds.size.height;
	
	// make sure we're the right size because something seems to derp up the views
	if ([[UIScreen mainScreen] bounds].size.height == 568)
	{
		[view setFrame:CGRectMake(0, searchBarHeight, 320, 455 - searchBarHeight)];
	}
	else
	{
		[view setFrame:CGRectMake(0, searchBarHeight, 320, 367 - searchBarHeight)];
	}
	
	if (self.map != nil)
	{
		[self.map.mapView setFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
	}
}

- (void)applicationDidBecomeActive
{
	if (list != nil)
	{
		[self.list refreshLocation];
	}
	else if (map != nil)
	{
//		[map startTracking];
	}
}

#pragma mark -

- (void)saveDefaultView:(NSString *)defaultView
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject:defaultView forKey:@"nearbyTabDefaultView"];
	[defaults synchronize];
}

- (NSString *)retrieveDefaultView
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	return [defaults objectForKey:@"nearbyTabDefaultView"];
}

#pragma mark - SWRevealViewController delegate methods

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
	//FrontViewPositionLeft on init
	//FrontViewPositionLeftSide on click
	
	//Need to disable user interaction during slide out, then re-enable it only for the navigation bar
	
	[revealController.frontViewController.view setUserInteractionEnabled:NO];
	
	//will show the menu
	if (position == FrontViewPositionLeft)
	{
		//Starting to hide the menu
		[revealController.rightViewController.view setUserInteractionEnabled:NO];
	}
	else
	{
		//Starting to show the menu
		if (self.list != nil)
		{
			[self.list.view setUserInteractionEnabled:NO];
		}
		else if (self.map != nil)
		{
			[self.map.view setUserInteractionEnabled:NO];
		}
	}
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
	//Need to disable user interaction during slide out, then re-enable it only for the navigation bar
	
	[revealController.frontViewController.view setUserInteractionEnabled:YES];
	
	if (position == FrontViewPositionLeft)
	{
		//Finished hiding menu
		//Starting to show the menu
		if (self.list != nil)
		{
			[self.list.view setUserInteractionEnabled:YES];
		}
		else if (self.map != nil)
		{
			[self.map.view setUserInteractionEnabled:YES];
		}	
	}
	else
	{
		//Finished showing menu
		[revealController.rightViewController.view setUserInteractionEnabled:YES];
	}
}

- (void)toggleSideMenu
{	
	[self.revealViewController rightRevealToggleAnimated:YES];
}

#pragma mark -
#pragma mark List View Methods

- (void)showListView
{
	[self saveDefaultView:@"list"];
	
	if (map != nil)
	{
		[self hideMapView];
	}
	
	// Nearby List
	list = [[NearbyListController alloc] initNearbyListWithNearbyViewController:self];
	
	// reset frame because we're not worried about the status bar
	CGRect newFrame = list.view.frame;
	newFrame.origin.y = 0;
	list.view.frame = newFrame;
	
	// add the view to the stack and tell the list we've done so
//	[list viewWillAppear:NO];
	[self addChildViewController:list];
	[self.view addSubview:list.view];
	[list didMoveToParentViewController:self];
//	[list viewDidAppear:NO];

	UIBarButtonItem *showMapButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"title-nearby-map", @"Map") style:(UIBarButtonItemStylePlain) target:self action:@selector(showMapView)];
	[self.navigationItem setLeftBarButtonItem:showMapButton];
}

- (void)hideListView
{	
	// remove the view from the stack
//	[list viewWillDisappear:NO];
	[list willMoveToParentViewController:nil];
	[list.view removeFromSuperview];
	[list removeFromParentViewController];
//	[list viewDidDisappear:NO];
	list = nil;
}

- (void)refreshLocation
{
	if (self.list != nil)
	{
		[self.list refreshLocation];
	}
}

- (void)pushPIDForStop:(Stop *)stop withFilter:(Filter *)f animated:(BOOL)animated
{
	if (self.list != nil)
		[self.list pushPIDForStop:stop withFilter:f animated:animated];
    else if (self.map != nil)
        [self.map pushPIDForStop:stop];
}

#pragma mark -
#pragma mark Map View Methods

- (void)showMapView
{
	[self saveDefaultView:@"map"];
	
	if (list != nil)
	{
		[self hideListView];
	}
	
	// Nearby Map
#warning MAPVIEW_INIT
//    map = [[MapViewController alloc] initNearbyMapWithNearbyViewController:self];
	
	// reset frame because we're not worried about the status bar
	CGRect newFrame = map.view.frame;
	newFrame.origin.y = 0;
	map.view.frame = newFrame;

	// Add the view to the stack
//	[map viewWillAppear:NO];
	[self addChildViewController:map];
	[self.view addSubview:map.view];
	[map didMoveToParentViewController:self];
//	[map viewDidAppear:NO];
	
	UIBarButtonItem *showListButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"title-nearby-list", @"List") style:(UIBarButtonItemStylePlain) target:self action:@selector(showListView)];
	[self.navigationItem setLeftBarButtonItem:showListButton];
}

- (void)hideMapView
{
	// remove the map view from the stack
//	[map viewWillDisappear:NO];
	[map willMoveToParentViewController:nil];
	[map.view removeFromSuperview];
	[map removeFromParentViewController];
//	[map viewDidDisappear:NO];
	
	// kill the map view controller
	map = nil;
}

- (void)toggleTicketRetailers
{
	// we show or hide tickets based on the map view and what it currently thinks
	if (map != nil)
	{
		// show or hide?
		if (map.showTicketRetailers)
		{
			// Currently showing ticket retailers, lets hide them
			[map setShowTicketRetailers:NO];
		} else
		{
			// Currently not showing ticket retailers, show them
			[map setShowTicketRetailers:YES];
		}
	}
}

#pragma Mark - NearbyActionMenuDelegate methods

- (void)didSelectAllStops
{
	
}

- (void)didSelectStopsWithShelter
{
	
}

- (void)didSelectEasyAccessStops
{
	
}

- (void)didSelectTicketOutlets
{
	[self toggleSideMenu];
}

- (void)didSelectEnterTrackerID
{
	[self toggleSideMenu];
	[self.navigationController pushViewController:[[EnterTrackerIDController alloc] init] animated:NO];
}

- (void)didSelectSearch;
{
	[self toggleSideMenu];
	[self.navigationController pushViewController:[[SearchListController alloc] init] animated:NO];
}


@end
