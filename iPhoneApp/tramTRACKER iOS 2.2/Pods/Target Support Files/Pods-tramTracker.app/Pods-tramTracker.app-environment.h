
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// CrashlyticsFramework
#define COCOAPODS_POD_AVAILABLE_CrashlyticsFramework
#define COCOAPODS_VERSION_MAJOR_CrashlyticsFramework 2
#define COCOAPODS_VERSION_MINOR_CrashlyticsFramework 1
#define COCOAPODS_VERSION_PATCH_CrashlyticsFramework 7

// FrameAccessor
#define COCOAPODS_POD_AVAILABLE_FrameAccessor
#define COCOAPODS_VERSION_MAJOR_FrameAccessor 1
#define COCOAPODS_VERSION_MINOR_FrameAccessor 3
#define COCOAPODS_VERSION_PATCH_FrameAccessor 2

// SMCalloutView
#define COCOAPODS_POD_AVAILABLE_SMCalloutView
#define COCOAPODS_VERSION_MAJOR_SMCalloutView 2
#define COCOAPODS_VERSION_MINOR_SMCalloutView 0
#define COCOAPODS_VERSION_PATCH_SMCalloutView 3

// UIAlertView+Blocks
#define COCOAPODS_POD_AVAILABLE_UIAlertView_Blocks
#define COCOAPODS_VERSION_MAJOR_UIAlertView_Blocks 0
#define COCOAPODS_VERSION_MINOR_UIAlertView_Blocks 8
#define COCOAPODS_VERSION_PATCH_UIAlertView_Blocks 1

// Debug build configuration
#ifdef DEBUG

  // Reveal-iOS-SDK
  #define COCOAPODS_POD_AVAILABLE_Reveal_iOS_SDK
  #define COCOAPODS_VERSION_MAJOR_Reveal_iOS_SDK 1
  #define COCOAPODS_VERSION_MINOR_Reveal_iOS_SDK 0
  #define COCOAPODS_VERSION_PATCH_Reveal_iOS_SDK 6

#endif
