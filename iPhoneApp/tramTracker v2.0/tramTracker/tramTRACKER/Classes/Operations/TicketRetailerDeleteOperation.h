//
//  TicketRetailersDeleteOperation.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 13/01/2014
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>


@class TicketRetailer;
@class BackgroundSynchroniser;

/**
 * A delete operation for a stop
 *
 * @ingroup Sync
**/
@interface TicketRetailerDeleteOperation : NSOperation

@property (weak, nonatomic, readonly) TicketRetailer *retailer;

@property (nonatomic, weak) BackgroundSynchroniser *syncManager;
@property (nonatomic, assign) BOOL finished;
@property (nonatomic, assign) BOOL executing;

- (id)initWithRetailer:(TicketRetailer *)aTicketRetailer;

/**
 * Required by the NSOperationQueue, whether to execute this operation concurrently.
 **/
- (BOOL)isConcurrent;

/**
 * NSOperationQueue wrapper for the executing ivar.
 **/
- (BOOL)isExecuting;

/**
 * NSOperationQueue wrapper for the finished ivar.
 **/
- (BOOL)isFinished;

/**
 * Starts the Stop Delete Operation
 **/
- (void)start;

/**
 * Finishes the Update operation and handles notifications
 **/
- (void)finish;

/**
 * Deletes the stop from the current context
**/
- (void)deleteRetailer;

@end

