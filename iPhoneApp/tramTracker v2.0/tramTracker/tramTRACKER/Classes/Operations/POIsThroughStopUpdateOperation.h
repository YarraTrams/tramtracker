//
//  POIsThroughStopUpdateOperation.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 23/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Stop;
@class BackgroundSynchroniser;

@interface POIsThroughStopUpdateOperation : NSOperation

@property (nonatomic, weak) BackgroundSynchroniser * syncManager;

- (id)initWithStop:(Stop *)aStop;

@end
