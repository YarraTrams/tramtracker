//
//  RoutesThroughStopUpdateOperation.h
//  tramTRACKER
//
//  Created by Robert Amos on 31/07/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Stop;
@class PidsService;
@class BackgroundSynchroniser;

/**
 * An update operation to a list of routes through a stop
 *
 * @ingroup Sync
 **/
@interface RoutesThroughStopUpdateOperation : NSOperation {

	/**
	 * The stop that will have its list of routes updated.
	**/
	Stop *__weak stop;

	/**
	 * An instance of the PidsService to retrieve route information from.
	 **/
	PidsService *service;
	
	/**
	 * The BackgroundSynchroniser object that is managing this update operation
	 **/
	BackgroundSynchroniser *__weak syncManager;
	
	/**
	 * Status - YES if the operation is executing, NO otherwise
	 **/
	BOOL executing;
	
	/**
	 * Status - YES if the operation was finished (or cancelled), NO otherwise
	 **/
	BOOL finished;
    
    NSManagedObjectContext *managedObjectContext;
}

@property (weak, nonatomic, readonly) Stop *stop;
@property (nonatomic, weak) BackgroundSynchroniser *syncManager;

/**
 * Initialises an update to the list of routes through the specified stop
 *
 * @param	aStop				The stop to have its list of routes updated.
 * @return						An initialised RoutesThroughStopUpdateOperation
**/
- (id)initWithStop:(Stop *)aStop;

/**
 * Required by the NSOperationQueue, whether to execute this operation concurrently.
 **/
- (BOOL)isConcurrent;

/**
 * NSOperationQueue wrapper for the executing ivar.
 **/
- (BOOL)isExecuting;

/**
 * NSOperationQueue wrapper for the finished ivar.
 **/
- (BOOL)isFinished;

/**
 * Asks the PidsService for a list of the routes through the stop
**/
- (void)updateRoutesThroughStop;

/**
 * Starts the routes through stop update operation
 **/
- (void)start;

/**
 * Finishes the Update operation and handles notifications
 **/
- (void)finish;


@end
