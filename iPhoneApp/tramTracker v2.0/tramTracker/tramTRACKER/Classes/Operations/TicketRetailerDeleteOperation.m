//
//  TicketRetailersDeleteOperation.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 13/01/2014
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "TicketRetailerDeleteOperation.h"
#import "TicketRetailer.h"
#import "BackgroundSynchroniser.h"

@interface TicketRetailerDeleteOperation()

@property (nonatomic, weak) TicketRetailer *retailer;

@end

@implementation TicketRetailerDeleteOperation

- (id)initWithRetailer:(TicketRetailer *)aTicketRetailer;
{
	if (self = [super init])
	{
		self.retailer = aTicketRetailer;
		
		self.executing = NO;
		self.finished = NO;

		// set our priority
		[self setQueuePriority:NSOperationQueuePriorityNormal];
	}
	return self;
}

- (BOOL)isConcurrent
{
	return NO;
}

- (BOOL)isExecuting
{
	return self.executing;
}

- (BOOL)isFinished
{
	return self.finished;
}

/**
 * Actually start processing the update
 **/
- (void)main
{
	[self start];
}
- (void)start
{
	if ([self isCancelled])
	{
		[self finish];
		return;
	}

	self.executing = YES;
	
	// send a notification
	if (self.syncManager != nil && self.syncManager.delegate != nil && ![self isCancelled] && [self.syncManager.delegate respondsToSelector:@selector(deleteDidStartRetailer:)])
		[self.syncManager.delegate performSelectorOnMainThread:@selector(deleteDidStartRetailer:) withObject:self.retailer waitUntilDone:NO];

	[self deleteRetailer];

	while (![self isFinished])
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
}

- (void)deleteRetailer
{
	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return ;
	}
	
	if (self.syncManager != nil)
	{
		NSManagedObjectContext *context = [self.syncManager managedObjectContext];

		[context deleteObject:self.retailer];
	}
	[self finish];
}

- (void)finish
{
	self.executing = NO;
	self.finished = YES;

	// send a notification
	if (self.syncManager != nil && self.syncManager.delegate != nil && ![self isCancelled] && [self.syncManager.delegate respondsToSelector:@selector(deleteDidFinishRetailer:)])
		[self.syncManager.delegate performSelectorOnMainThread:@selector(deleteDidFinishRetailer:)
                                                    withObject:self.retailer
                                                 waitUntilDone:NO];
}

@end
