//
//  TurnDeleteOperation.m
//  tramTRACKER
//
//  Created by Robert Amos on 18/12/2010.
//  Copyright 2010 Yarra Trams. All rights reserved.
//

#import "TurnDeleteOperation.h"
#import "Turn.h"
#import "BackgroundSynchroniser.h"

@implementation TurnDeleteOperation

@synthesize turn, syncManager;

- (id)initWithTurn:(Turn *)aTurn
{
	if (self = [super init])
	{
		turn = aTurn;
		
		[self willChangeValueForKey:@"isExecuting"];
		executing = NO;
		[self didChangeValueForKey:@"isExecuting"];
		
		[self willChangeValueForKey:@"isFinished"];
		finished = NO;
		[self didChangeValueForKey:@"isFinished"];
		
		// set our priority
		[self setQueuePriority:NSOperationQueuePriorityNormal];
	}
	return self;
}

- (BOOL)isConcurrent
{
	return NO;
}

- (BOOL)isExecuting
{
	return executing;
}

- (BOOL)isFinished
{
	return finished;
}

/**
 * Actually start processing the update
 **/
- (void)main
{
	[self start];
}
- (void)start
{
	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}
	
	//NSLog(@"[TURNDELETE] Deleting Turn %@", turn);
	[self willChangeValueForKey:@"isExecuting"];
	executing = YES;
	[self didChangeValueForKey:@"isExecuting"];
	
	// send a notification
	if (self.syncManager != nil && self.syncManager.delegate != nil && [self.syncManager.delegate respondsToSelector:@selector(deleteDidStartTurn:)])
		[self.syncManager.delegate performSelectorOnMainThread:@selector(deleteDidStartTurn:) withObject:turn waitUntilDone:NO];
	
	// delete the turn
	[self deleteTurn];
	
	// sigh, poll until its finished
	while (![self isFinished])
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
}

- (void)deleteTurn
{
	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}
	
	if (self.syncManager != nil)
	{
		// have the stop removed from the context, it will take care of itself
		NSManagedObjectContext *context = [self.syncManager managedObjectContext];
		
		// but first re-fetch the turn in the current context
		Turn *turnInContext = [self.syncManager turnForRoute:turn.route upDirection:turn.upDirection stop:turn.stop];
		if (turnInContext != nil)
			[context deleteObject:turnInContext];
	}
	
	[self finish];
}

- (void)finish
{
	[self willChangeValueForKey:@"isExecuting"];
	executing = NO;
	[self didChangeValueForKey:@"isExecuting"];
	
	[self willChangeValueForKey:@"isFinished"];
	finished = YES;
	[self didChangeValueForKey:@"isFinished"];
	
	// send a notification
	if (self.syncManager != nil && self.syncManager.delegate != nil && ![self isCancelled] && [self.syncManager.delegate respondsToSelector:@selector(deleteDidFinishTurn:)])
		[self.syncManager.delegate performSelectorOnMainThread:@selector(deleteDidFinishTurn:) withObject:turn waitUntilDone:NO];
}


@end
