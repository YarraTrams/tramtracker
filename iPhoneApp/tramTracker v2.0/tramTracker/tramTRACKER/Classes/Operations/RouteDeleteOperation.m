//
//  RouteDeleteOperation.m
//  tramTRACKER
//
//  Created by Robert Amos on 7/08/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "RouteDeleteOperation.h"
#import "Route.h"
#import "BackgroundSynchroniser.h"

@interface RouteDeleteOperation()

@property (weak, nonatomic) Route *route;

@end

@implementation RouteDeleteOperation

@synthesize syncManager, route;

- (id)initWithRoute:(Route *)aRoute
{
	if (self = [super init])
	{
		self.route = aRoute;

		self.executing = NO;
		self.finished = NO;

		// set our priority
		[self setQueuePriority:NSOperationQueuePriorityNormal];
	}
	return self;
}

- (BOOL)isConcurrent
{
	return NO;
}

/**
 * Actually start processing the update
 **/
- (void)main
{
	[self start];
}
- (void)start
{
	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}
	
	self.executing = YES;
	
	// send a notification
	if (self.syncManager != nil && self.syncManager.delegate != nil && [self.syncManager.delegate respondsToSelector:@selector(deleteDidStartRoute:)])
		[self.syncManager.delegate performSelectorOnMainThread:@selector(deleteDidStartRoute:) withObject:route waitUntilDone:NO];
	
	// delete the route
	[self deleteRoute];
	
	// sigh, poll until its finished
	while (![self isFinished])
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
}

// start deleting the route
- (void)deleteRoute
{
	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}
	
	// so we're at the stage where we can delete this route now
	if (self.syncManager != nil)
	{
		NSManagedObjectContext *context = [self.syncManager managedObjectContext];
		[context deleteObject:route];
	}
	[self finish];
}



- (void)finish
{
	self.executing = NO;
	self.finished = YES;

	// send a notification
	if (self.syncManager != nil && self.syncManager.delegate != nil && ![self isCancelled] && [self.syncManager.delegate respondsToSelector:@selector(deleteDidFinishRoute:)])
		[self.syncManager.delegate performSelectorOnMainThread:@selector(deleteDidFinishRoute:) withObject:route waitUntilDone:NO];

	//NSLog(@"Route deleted: %@", route);
}


@end
