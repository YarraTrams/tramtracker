//
//  TicketRetailersUpdateOperation.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 10/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import "TicketRetailerUpdateOperation.h"
#import "BackgroundSynchroniser.h"
#import "PidsService.h"
#import "NSDictionary+SafeAccess.h"
#import "TicketRetailer.h"

@interface TicketRetailerUpdateOperation()

@property (nonatomic, strong) PidsService           * service;
@property (nonatomic, weak) TicketRetailer          * retailer;

@property (nonatomic, assign, getter = isExecuting) BOOL executing;
@property (nonatomic, assign, getter = isFinished) BOOL finished;

@end

@implementation TicketRetailerUpdateOperation

- (id)initWithRetailer:(TicketRetailer *)aRetailer
{
	if (self = [super init])
	{
        self.retailer = aRetailer;

		self.service = [[PidsService alloc] init];
		[self.service setDelegate:self];

		[self willChangeValueForKey:@"isExecuting"];

		self.executing = NO;

		[self didChangeValueForKey:@"isExecuting"];
		
		[self willChangeValueForKey:@"isFinished"];
		self.finished = NO;
		[self didChangeValueForKey:@"isFinished"];

		// set our priority
		[self setQueuePriority:NSOperationQueuePriorityHigh];
	}
	return self;
}

- (void)dealloc
{
    [self.service setDelegate:nil];
}

- (BOOL)isConcurrent
{
	return NO;
}

/**
 * Actually start processing the update
 **/
- (void)main
{
	[self start];
}

- (void)start
{
	if ([self isCancelled])
	{
		[self finish];
		return;
	}
    
	// we start by getting the destinations for this route
	[self willChangeValueForKey:@"isExecuting"];
	self.executing = YES;
	[self didChangeValueForKey:@"isExecuting"];

	// send a notification
	if (self.syncManager != nil && self.syncManager.delegate != nil && [self.syncManager.delegate respondsToSelector:@selector(syncDidStartRetailer:)])
		[self.syncManager.delegate performSelectorOnMainThread:@selector(syncDidStartRetailer:) withObject:self.retailer waitUntilDone:NO];

	[self updateInformation];

	// sigh, poll until its finished
	while (![self isFinished])
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
}

- (void)updateInformation
{
	[self.service getInformationForRetailer:[[self.retailer retailerID] floatValue]];
}

- (void)setInformation:(NSDictionary *)info
{
	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}
    
    TicketRetailer * retailer = self.retailer;

    retailer.name = [info valueForKey:@"Name" ifKindOf:[NSString class] defaultValue:@""];
    retailer.open24Hour = [info valueForKey:@"Is24Hour" ifKindOf:[NSNumber class] defaultValue:@NO];
    retailer.latitude = [info valueForKey:@"Latitude" ifKindOf:[NSNumber class] defaultValue:@0];
    retailer.longitude = [info valueForKey:@"Longitude" ifKindOf:[NSNumber class] defaultValue:@0];
    retailer.address = [info valueForKey:@"Address" ifKindOf:[NSString class] defaultValue:@""];
    retailer.hasMykiTopUp = [info valueForKey:@"HasMykiTopUp" ifKindOf:[NSNumber class] defaultValue:@NO];
    retailer.sellsMyki = [info valueForKey:@"HasMyki" ifKindOf:[NSNumber class] defaultValue:@NO];
    retailer.suburb = [info valueForKey:@"Suburb" ifKindOf:[NSString class] defaultValue:@""];

	// now that we're done..
	[self finish];
}

// Indicates a failure of some sort.
- (void)pidsServiceDidFailWithError:(NSError *)error
{
	// make sure we're not cancelled
	if ([self isCancelled])
	{
		[self finish];
		return;
	}
    
	static NSInteger previousFailureCount = 0;
    
	// if the stop has been deleted we can silently fail over to syncing that change
	if ([error code] == PIDServiceValidationErrorOutletNotFound)
	{
		//NSLog(@"Triggered delete: %@", self.stop);
		//if (self.syncManager != nil)
		//	[self.syncManager addStopDeleteToQueue:self.stop];
		[self finish];
	}
    else if ([error code] == PIDServiceErrorTimeoutReached && previousFailureCount == 0)
	{
		// try it again
		[self start];
		previousFailureCount = 1;
	}
    else
	{
		[self cancel];
		[self finish];
		
		// send a failure message to the delegate
		if (self.syncManager != nil && self.syncManager.delegate != nil && ![self isCancelled] && [self.syncManager.delegate respondsToSelector:@selector(syncDidFailWithError:)])
			[self.syncManager.delegate performSelectorOnMainThread:@selector(syncDidFailWithError:) withObject:error waitUntilDone:NO];
	}
}

- (void)finish
{
	// send a notification if we werent cancelled
	if (self.syncManager != nil && self.syncManager.delegate != nil && ![self isCancelled] && [self.syncManager.delegate respondsToSelector:@selector(syncDidFinishRetailer:)])
		[self.syncManager.delegate performSelectorOnMainThread:@selector(syncDidFinishRetailer:) withObject:self.retailer waitUntilDone:NO];

	[self willChangeValueForKey:@"isExecuting"];
	self.executing = NO;
	[self didChangeValueForKey:@"isExecuting"];
	
	[self willChangeValueForKey:@"isFinished"];
	self.finished = YES;
	[self didChangeValueForKey:@"isFinished"];

	[self.service setDelegate:self];
}

@end
