//
//  Location.h
//  tramTRACKER
//
//  Created by Robert Amos on 20/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationManager : NSObject

+ (CLLocationDistance)distanceFromLocation:(CLLocation *)from toLocation:(CLLocation *)to;

@end
