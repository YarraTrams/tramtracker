//
//  Settings.m
//  tramTRACKER
//
//  Created by Raji on 23/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "Settings.h"

@implementation Settings

@dynamic openWith;
@dynamic smartRestore;
@dynamic synchronise;
@dynamic showNearbyStops;
@dynamic showMostRecent;

+ (Settings *)settings
{
	tramTRACKERAppDelegate  * d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext  * context = [d managedObjectContext];
	NSFetchRequest          * fetchRequest = [NSFetchRequest new];

	NSEntityDescription     * entity = [NSEntityDescription entityForName:@"Settings" inManagedObjectContext:context];
   	NSError *outError;
	[fetchRequest setEntity:entity];

    NSArray * fetchedObjects = [context executeFetchRequest:fetchRequest error:&outError];

    if (!fetchedObjects.count)
    {
        Settings    * mainObject = [NSEntityDescription
                                      insertNewObjectForEntityForName:@"Settings"
                                      inManagedObjectContext:context];

        mainObject.openWith = @"Nearby List";
        mainObject.smartRestore = @YES;
        mainObject.synchronise = @YES;
        mainObject.showNearbyStops = @(30);
        mainObject.showMostRecent = @(30);

        [(tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
        return mainObject;
    }
    NSAssert(!outError, @"Error while fetching Settings");
    return fetchedObjects.firstObject;
}

@end
