//
//  Journey.m
//  tramTRACKER
//
//  Created by Robert Amos on 9/04/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "Journey.h"


@implementation Journey

@synthesize tram, stops;

+ (Journey *)journeyForStub:(JourneyStub *)stub
{
    // create a new Journey
    Journey *journey = [[Journey alloc] init];
    
    // Set the tram
    if (stub.tramStub != nil)
        [journey setTram:[Tram tramForStub:stub.tramStub]];
    
    // Set the stops
    if (stub.stubStops != nil)
    {
        NSMutableArray *stops = [NSMutableArray new];
        for (JourneyStopStub *s in stub.stubStops)
            [stops addObject:[JourneyStop journeyStopForStub:s]];
        journey.stops = stops;
    }
    journey.tram.disruptionMessage = stub.tramStub.disruptionMessage;
    return journey;
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"Journey of %@ arriving at %@", tram, stops];
}

- (JourneyStop *)journeyStopForStop:(Stop *)stop
{
	if (stop == nil || self.stops == nil || [self.stops count] == 0)
		return nil;
	
	// loop over the stops and find our stop
	for (JourneyStop *s in self.stops)
	{
		if ([stop isEqual:s.stop])
			return s;
	}
	return nil;
}


- (id)copyWithZone:(NSZone *)zone
{
	Journey *copy = [[Journey alloc] init];
	[copy setTram:[self.tram copy]];
	[copy setStops:[self.stops copy]];
	return copy;
}

@end

@implementation JourneyStop

@synthesize stop, predicatedArrivalDateTime, originalPredictedArrivalDateTime;

- (NSString *)title
{
    return self.stop.formattedName;
}

- (NSString *)subtitle
{
    return [NSString stringWithFormat:@"%@ - %@", self.preditedTime, stop.subtitle];
}

+ (JourneyStop *)journeyStopForStub:(JourneyStopStub *)stub
{
    JourneyStop *journeyStop = [stub copy];

    // Set the stop
    [journeyStop setStop:[Stop stopForStub:stub.stopStub]];
    
    return journeyStop;
}

- (CLLocationCoordinate2D)coordinate
{
    return self.stop.coordinate;
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"Stopping at %@ at %@", stop, predicatedArrivalDateTime];
}

- (NSTimeInterval)secondsUntilArrival
{
	NSTimeInterval secondsUntilArrivalTime = [self.predicatedArrivalDateTime timeIntervalSinceDate:[NSDate date]];
	return secondsUntilArrivalTime;
}

- (NSString *)minutesUntilArrival
{
	
	// work out the number of minutes to go
	int minutes = floor([self secondsUntilArrival] / 60);
	
	// if we're less than a minute (but not more than a minute past) return now
	if (minutes < 1)
		return NSLocalizedString(@"onboard-now", @"next");
	
	else
		return [NSString stringWithFormat:@"%d", minutes];
}

- (id)copyWithZone:(NSZone *)zone
{
	JourneyStop *copy = [[JourneyStop alloc] init];
	[copy setStop:stop];
	[copy setPredicatedArrivalDateTime:[predicatedArrivalDateTime copy]];
	[copy setOriginalPredictedArrivalDateTime:[originalPredictedArrivalDateTime copy]];
	return copy;
}

- (NSString *)preditedTime
{

    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    [df setDateFormat:@"HH:mm"];
    return [df stringFromDate:self.predicatedArrivalDateTime];
    
}


@end

@implementation JourneyStub

@synthesize tramStub, stubStops;

@end

@implementation JourneyStopStub

@synthesize stopStub;


@end
