//
//  Settings.h
//  tramTRACKER
//
//  Created by Raji on 23/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

//@interface Settings : NSObject
//
///*
// * Open With Section
// */
//@property (nonatomic, retain) NSString * openWith;
//
///*
// * Smart Restore ON/OFF Switch
// */
//@property (nonatomic, retain) NSNumber * smartRestore;
//
///*
// * Synchronise YES = Check Automatically; NO = Manually
// */
//@property (nonatomic, retain) NSNumber * synchronise;
//
///*
// * Number of Nearby Stops to show
// */
//@property (nonatomic, retain) NSNumber * showNearbyStops;
//
///*
// * Number of Most Recent Stops to show
// */
//@property (nonatomic, retain) NSNumber * showMostRecent;
//
//+ (Settings *)settings;
//+(void)saveSettings:(Settings *)settings;
//
//@end

@interface Settings : NSManagedObject

/*
 * Open With Section
 */
@property (nonatomic, retain) NSString * openWith;

/*
 * Smart Restore ON/OFF Switch
 */
@property (nonatomic, retain) NSNumber * smartRestore;

/*
 * Synchronise YES = Check Automatically; NO = Manually
 */
@property (nonatomic, retain) NSNumber * synchronise;

/*
 * Number of Nearby Stops to show
 */
@property (nonatomic, retain) NSNumber * showNearbyStops;

/*

 * Number of Most Recent Stops to show
 */
@property (nonatomic, retain) NSNumber * showMostRecent;

+ (Settings *)settings;

@end
