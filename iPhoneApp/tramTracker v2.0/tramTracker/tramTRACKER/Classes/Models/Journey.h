//
//  Journey.h
//  tramTRACKER
//
//  Created by Robert Amos on 9/04/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Stop.h"
#import "Route.h"
#import "Tram.h"

@class JourneyStub;
@class Journey;

//@protocol SetCurrentJourneyDelegate <NSObject>
//
//- (void)setJourney:(Journey *)aJourney;
//
//@optional
//- (void)setJourneyStub:(JourneyStub *)aJourneyStub;
//
//@end

@class JourneyStopStub;

/**
 * A stop on a journey
**/
@interface JourneyStop : NSObject <NSCopying, MKAnnotation>

/**
 * The Stop that the tram will be stopping at.
 **/
@property (nonatomic, strong) Stop      * stop;

/**
 * The predicted arrival date and time of that tram at the stop.
 * This can be recalculated at any time due according to location information (GPS)
 **/
@property (nonatomic, strong) NSDate    * predicatedArrivalDateTime;

/**
 * The original predicted arrival date and time as specified by the service.
 **/
@property (nonatomic, strong) NSDate    * originalPredictedArrivalDateTime;

/**
 * The minutes from now until arrival at this JourneyStop
**/
- (NSString *)minutesUntilArrival;
- (NSTimeInterval)secondsUntilArrival;

+ (JourneyStop *)journeyStopForStub:(JourneyStopStub *)stub;

@end

@class JourneyStub;

/**
 * Represents a Journey as made by a tram.
**/
@interface Journey : NSObject <NSCopying>

/**
 * The Tram making the journey
 **/
@property (nonatomic, strong) Tram *tram;

/**
 * An array of stops that this tram has left to make on its journey.
 **/
@property (nonatomic, strong) NSArray *stops;

/**
 * Find the journey stop that corresponds to a particular stop.
 *
 * @param	stop		A Stop
 * @return				A JourneyStop for the specified stop. Will be nil if the specified stop will not be passed on this journey
**/
- (JourneyStop *)journeyStopForStop:(Stop *)stop;

+ (Journey *)journeyForStub:(JourneyStub *)stub;

@end


/**
 * A Journey Stub
 *
 * A stub that allows the PIDS Service to return data from the background thread
 * Pass through +[Journey journeyForStub:] to get the full journey data
**/

@interface JourneyStub : NSObject

@property (nonatomic, strong) TramStub *tramStub;
@property (nonatomic, strong) NSArray *stubStops;

@end

/**
 * A JourneyStop Stub
 *
 * A stub that allows the PIDS Service to return data from the background thread
 * Pass through +[JourneyStop journeyStopForStub:] to get the full journey data
**/
@interface JourneyStopStub : JourneyStop {
@private
    StopStub *stopStub;
}

@property (nonatomic, strong) StopStub *stopStub;

@end