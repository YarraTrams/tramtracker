//
//  Analytics.m
//  tramTracker
//
//  Created by Hugo Cuvillier on 12/02/2014.
//  Copyright (c) 2014 AppsCore. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

#import "Analytics.h"
#import "PidsService.h"
#import "Constants.h"

@interface Analytics()<CLLocationManagerDelegate>
{
    NSMutableDictionary     *_currentLaunch;
    CLLocationManager       *_locationManager;
    CLLocationCoordinate2D  _coordinate;
    
    AnalyticsFeature        _lastFeature;
    AnalyticsFilter         _lastFilter;
    BOOL                    _lastAccess;
}

@end

@implementation Analytics

static NSString * const kAnalyticsUDKey                 = @"analytics";
static NSString * const kAnalyticsDateUDKey             = @"analyticsDate";
static NSString * const kAnalyticsDeclineSentUDKey      = @"analyticsDeclineSent";
static NSString * const kAnalyticsiOSVersionKey         = @"iOSVersion";
static NSString * const kAnalyticstramTrackerVersionKey = @"tramTrackerVersion";
static NSString * const kAnalyticsDeviceIDKey           = @"DeviceID";
static NSString * const kAnalyticsLaunchesKey           = @"Launches";
static NSString * const kAnalyticsTimestampStartKey     = @"TimestampStart";
static NSString * const kAnalyticsTimestampEndKey       = @"TimestampEnd";
static NSString * const kAnalyticsLatKey                = @"Latitude";
static NSString * const kAnalyticsLonKey                = @"Longitude";
static NSString * const kAnalyticsFeaturesKey           = @"Features";

static NSString * const kAnalyticsNameKey               = @"Name";
static NSString * const kAnalyticsFilterKey             = @"Filter";
static NSString * const kAnalyticsTimeStampKey          = @"Timestamp";

NSString * const kAnalyticsAllowAnalyticsKey            = @"AllowsAnalytics";

+ (BOOL)date:(NSDate *)aDate1 isEqualToDateIgnoringTime:(NSDate *)aDate2
{
	NSDateComponents * components1 = [[NSCalendar currentCalendar] components:(NSYearCalendarUnit|
                                                                               NSMonthCalendarUnit |
                                                                               NSDayCalendarUnit |
                                                                               NSWeekCalendarUnit |
                                                                               NSHourCalendarUnit |
                                                                               NSMinuteCalendarUnit |
                                                                               NSSecondCalendarUnit |
                                                                               NSWeekdayCalendarUnit |
                                                                               NSWeekdayOrdinalCalendarUnit)
                                                                     fromDate:aDate1];
    
	NSDateComponents * components2 = [[NSCalendar currentCalendar] components:(NSYearCalendarUnit|
                                                                               NSMonthCalendarUnit |
                                                                               NSDayCalendarUnit |
                                                                               NSWeekCalendarUnit |
                                                                               NSHourCalendarUnit |
                                                                               NSMinuteCalendarUnit |
                                                                               NSSecondCalendarUnit |
                                                                               NSWeekdayCalendarUnit |
                                                                               NSWeekdayOrdinalCalendarUnit)
                                                                     fromDate:aDate2];
    
	return ((components1.year == components2.year) &&
			(components1.month == components2.month) &&
			(components1.day == components2.day));
}

+ (NSMutableDictionary *)analytics
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:kAnalyticsUDKey] mutableCopy];
}

+ (void)setAnalytics:(NSDictionary *)analytics
{
    [[NSUserDefaults standardUserDefaults] setObject:analytics forKey:kAnalyticsUDKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (BOOL)isAnalyticsAllowed { return [[NSUserDefaults standardUserDefaults] boolForKey:kAnalyticsAllowAnalyticsKey]; }

+ (void)addFeature:(NSDictionary *)aFeature
{
    Analytics           * analytics = [Analytics sharedInstance];
    NSMutableArray      * features = analytics->_currentLaunch[kAnalyticsFeaturesKey];
    
    if (!features)
        features = [NSMutableArray array];
    
    [features addObject:aFeature];
    analytics->_currentLaunch[kAnalyticsFeaturesKey] = features;
}

- (instancetype)init
{
    if (self = [super init])
    {
        NSMutableDictionary * analytics = [Analytics analytics];
        
        _lastAccess = NO;
        if (!analytics)
            analytics = [NSMutableDictionary dictionary];
        
        analytics[kAnalyticsiOSVersionKey]          = [UIDevice currentDevice].systemVersion;
        analytics[kAnalyticstramTrackerVersionKey]  = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        analytics[kAnalyticsDeviceIDKey]            = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        analytics[kAnalyticsAllowAnalyticsKey]      = [Analytics isAnalyticsAllowed] ? @"Yes" : @"No";
        
        [Analytics setAnalytics:analytics];
        
        if ([CLLocationManager locationServicesEnabled])
        {
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kAnalyticsDeclineSentUDKey];
            
            _locationManager = [CLLocationManager new];
            _locationManager.delegate = self;
            _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            [_locationManager startUpdatingLocation];
        }
    }
    return self;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = locations.lastObject;
    
    if (location)
    {
        _coordinate = location.coordinate;
        [_locationManager stopUpdatingLocation];
        _locationManager = nil;
    }
}

+ (instancetype)sharedInstance
{
    static dispatch_once_t  onceToken;
    static id               instance;
    
    dispatch_once(&onceToken, ^{
        instance = [[Analytics alloc] init];
    });
    return instance;
}

- (void)appHasLaunched
{
    NSDate  * currentDate   = [NSDate date];
    
    _currentLaunch = [NSMutableDictionary dictionary];
    _currentLaunch[kAnalyticsTimestampStartKey] = @((NSUInteger)[currentDate timeIntervalSince1970]);
    
    if (![Analytics isAnalyticsAllowed])
    {
        if (![[NSUserDefaults standardUserDefaults] boolForKey:kAnalyticsDeclineSentUDKey])
        {
            [self sendDictionaryToAPI:[Analytics analytics]];
        }
        return ;
    }
    
    NSDate  * lastDateSent  = [[NSUserDefaults standardUserDefaults] objectForKey:kAnalyticsDateUDKey];

    if (!lastDateSent)
    {
        [[NSUserDefaults standardUserDefaults] setObject:currentDate forKey:kAnalyticsDateUDKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        lastDateSent = currentDate;
    }

    if (_lastAccess)
        [self featureAccessed:_lastFeature withFilter:_lastFilter];

    if (![Analytics date:lastDateSent isEqualToDateIgnoringTime:currentDate])
        [self sendDictionaryToAPI:[Analytics analytics]];
}

- (void)appHasClosed
{
    if (![Analytics isAnalyticsAllowed])
        return ;
    
    NSMutableDictionary     * analytics = [Analytics analytics];
    NSMutableArray          * launches = [analytics[kAnalyticsLaunchesKey] mutableCopy];
    
    if (!launches)
        launches = [NSMutableArray array];
    
    analytics[kAnalyticsAllowAnalyticsKey] = @"Yes";
    
    _currentLaunch[kAnalyticsTimestampEndKey] = @((NSUInteger)[[NSDate date] timeIntervalSince1970]);
    _currentLaunch[kAnalyticsLonKey] = @(_coordinate.longitude);
    _currentLaunch[kAnalyticsLatKey] = @(_coordinate.latitude);
    
    if ([_currentLaunch[kAnalyticsFeaturesKey] count])
    {
        [launches addObject:_currentLaunch];
        _currentLaunch = nil;
    }
    
    analytics[kAnalyticsLaunchesKey] = launches;
    [Analytics setAnalytics:analytics];
}

- (void)featureAccessed:(AnalyticsFeature)aFeature
{
    [self featureAccessed:aFeature withFilter:AnalyticsFilterNone];
}

- (void)featureAccessed:(AnalyticsFeature)aFeature withFilter:(AnalyticsFilter)aFilter
{
    if (![Analytics isAnalyticsAllowed])
        return ;
    
    NSDictionary    * const featuresDic =   @{
                                              @(AnalyticsFeatureFavouritesList)             : @"FavouriteList",
                                              @(AnalyticsFeatureFavouritesMap)              : @"FavouriteMap",
                                              @(AnalyticsFeatureNearbyList)                 : @"NearbyList",
                                              @(AnalyticsFeatureNearbyMap)                  : @"NearbyMap",
                                              @(AnalyticsFeaturePID)                        : @"PID",
                                              @(AnalyticsFeatureAddToFavourite)             : @"AddToFavourites",
                                              @(AnalyticsFeatureStopInformation)            : @"StopInformation",
                                              @(AnalyticsFeatureRouteFilter)                : @"RouteFilter",
                                              @(AnalyticsFeatureSchedulesDeparture)         : @"SchedulesDeparture",
                                              @(AnalyticsFeatureDirections)                 : @"Directions",
                                              @(AnalyticsFeatureStopMap)                    : @"StopMap",
                                              @(AnalyticsFeatureRoutes)                     : @"Routes",
                                              @(AnalyticsFeatureNetwork)                    : @"Network",
                                              @(AnalyticsFeatureRoutesDetailedList)         : @"RoutesDetailedList",
                                              @(AnalyticsFeatureRoutesDetailedMap)          : @"RoutesDetailedMap",
                                              @(AnalyticsFeatureMyTram)                     : @"myTram",
                                              @(AnalyticsFeatureMyTramDetailedList)         : @"myTramDetailedList",
                                              @(AnalyticsFeatureMyTramDetailedMap)          : @"myTramDetailedMap",
                                              @(AnalyticsFeatureMore)                       : @"More",
                                              @(AnalyticsFeatureEnterTrackerID)             : @"EnterTrackerID",
                                              @(AnalyticsFeatureSearch)                     : @"Search",
                                              @(AnalyticsFeatureSearchResultList)           : @"SearchResultList",
                                              @(AnalyticsFeatureSearchResultMap)            : @"SearchResultMap",
                                              @(AnalyticsFeaturePOIInfo)                    : @"POIInformation",
                                              @(AnalyticsFeatureOutletList)                 : @"TicketOutletsList",
                                              @(AnalyticsFeatureOutletMap)                  : @"TicketOutletsMap",
                                              @(AnalyticsFeatureOutletDetailed)             : @"TicketOutletsDetailed",
                                              @(AnalyticsFeaturePIDNearbyTicketOutletMap)   : @"PIDNearbyTicketOutletsMap",
                                              @(AnalyticsFeaturePIDNearbyTicketOutlet)      : @"PIDNearbyTicketOutlets",
                                              @(AnalyticsFeatureServiceUpdatesList)         : @"ServiceUpdatesList",
                                              @(AnalyticsFeatureServiceUpdatesDetailed)     : @"ServiceUpdatesDetailed",
                                              @(AnalyticsFeatureFollowOnTwitter)            : @"FollowOnTwitter",
                                              @(AnalyticsFeaturePTV)                        : @"PTV",
                                              @(AnalyticsFeatureTimeTables)                 : @"TimeTables",
                                              @(AnalyticsFeatureTimeTablesMap)              : @"TimeTablesMap",
                                              @(AnalyticsFeatureTimeTablesList)             : @"TimeTablesList",
                                              @(AnalyticsFeatureFeedback)                   : @"Feedback",
                                              @(AnalyticsFeatureHelp)                       : @"Help",
                                              @(AnalyticsFeatureSettings)                   : @"Settings",
                                              @(AnalyticsFeatureMostRecentList)             : @"MostRecentList",
                                              @(AnalyticsFeatureUpdates)                    : @"Updates"
                                              };
    
    NSDictionary    * const filterDic =     @{
                                              @(AnalyticsFilterNone)          : @"",
                                              @(AnalyticsFilterAll)           : @"All",
                                              @(AnalyticsFilterShelter)       : @"Shelter",
                                              @(AnalyticsFilterEasyAccess)    : @"EasyAccess",
                                              @(AnalyticsFilterOutlets)       : @"Outlets",
                                              @(AnalyticsFilterLowFloor)      : @"LowFloor",
                                              @(AnalyticsFilterRoute)         : @"Route"
                                              };

    if (aFilter != AnalyticsFilterNone)
        NSLog(@"\n\n\t\t\tFeature: %@ Filter: %@\n\n", featuresDic[@(aFeature)], filterDic[@(aFilter)]);
    else
        NSLog(@"\n\n\t\t\tFeature: %@\n\n", featuresDic[@(aFeature)]);

    NSUInteger currentTime = [[NSDate date] timeIntervalSince1970];

    if (aFilter != AnalyticsFilterNone)
        [Analytics addFeature:@{kAnalyticsNameKey       : [featuresDic[@(aFeature)] stringByAppendingString:filterDic[@(aFilter)]],
                                kAnalyticsTimeStampKey  : @(currentTime)}];
    else
        [Analytics addFeature:@{kAnalyticsNameKey       : featuresDic[@(aFeature)],
                                kAnalyticsTimeStampKey  : @(currentTime)}];

    _lastFeature = aFeature;
    _lastFilter = aFilter;
    _lastAccess = YES;
}

- (void)sendDictionaryToAPI:(NSDictionary *)aDictionary
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSError*    error = nil;
        NSData*     toSendData = [NSJSONSerialization dataWithJSONObject:aDictionary options:kNilOptions error:&error];
        
        if (!error)
        {
            NSMutableURLRequest * request = [NSMutableURLRequest new];
            NSString            * postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[toSendData length]];
            NSURLResponse       * response = nil;

            [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@AddDeviceUsage/?aid=%@&tkn=%@", [PidsService baseURL], aDictionary[kAnalyticsDeviceIDKey], kAppID]]];

            [request setHTTPMethod:@"POST"];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:toSendData];

            (void)[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];

            if (!error && [(NSHTTPURLResponse *)response statusCode] == 200)
            {
                /* Clear launches */
                NSMutableDictionary * analytics = [Analytics analytics];
                
                if ([analytics[kAnalyticsLaunchesKey] count])
                {
                    /* Clear launches */
                    analytics[kAnalyticsLaunchesKey] = [NSMutableArray array];
                    [Analytics setAnalytics:analytics];
                }
                else
                {
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kAnalyticsDeclineSentUDKey];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
                [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:kAnalyticsDateUDKey];
            }
        }
    });
}

@end
