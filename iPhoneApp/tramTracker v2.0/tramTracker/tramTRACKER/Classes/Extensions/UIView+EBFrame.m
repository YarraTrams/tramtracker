//
//  UIView+EBFrame.m
//  EBFoundationKit_Example
//
//  Created by Eike Bartels on 20.06.13.
//  Copyright (c) 2013 Eike T. Bartels. All rights reserved.
//

#import "UIView+EBFrame.h"

@implementation UIView (EBFrame)

@dynamic x,y,width,height,size,position;



-(int)x{
    return self.frame.origin.x;
}

-(void)setX:(int)x{
    [self setFrame:CGRectMake(x, self.frame.origin.y, self.frame.size.width, self.frame.size.height)];
}

-(int)y{
    return self.frame.origin.y;
}

-(void)setY:(int)y{
    [self setFrame:CGRectMake(self.frame.origin.x, y, self.frame.size.width, self.frame.size.height)];
}

-(int)width{
    return self.frame.size.width;
}

-(void)setWidth:(int)width{
    [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, width, self.frame.size.height)];
}

-(int)height{
    return self.frame.size.height;
}
-(void)setHeight:(int)height{
    [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, height)];
}

-(CGSize)getSize{
    return CGSizeMake(self.frame.size.width, self.frame.size.height);
}
-(void)setSize:(CGSize)size{
    [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, size.width,size.height)];
    
}

-(CGPoint)getPosition{
    return CGPointMake(self.frame.origin.x, self.frame.origin.y);
}
-(void)setPosition:(CGPoint)position{
    [self setFrame:CGRectMake(position.x, position.y, self.frame.size.width, self.frame.size.height)];
    
}



@end
