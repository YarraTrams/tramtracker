//
//  TTTools.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 21/11/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TTTools : NSObject

+ (NSString *)voiceOverMinutesUntilArrivalTime:(NSDate *)arrivalTime;
+ (NSInteger)requiredAccuracy:(NSTimeInterval)secondsPassed;
+ (NSInteger)maxStopsCount;

@end
