//
//  Constants.m
//  
//
//  Created by Hugo Cuvillier on 21/11/2013.
//
//

#import "Constants.h"

@implementation Constants

/*
 * Segues
 */

NSString * const kSegueShowPID                  = @"showPIDSegue";
NSString * const kSegueShowMyTramDetailed       = @"showMyTramDetails";
NSString * const kSegueShowStopInformation      = @"showStopInformation";
NSString * const kSegueShowRouteFilter          = @"showRouteFilter";
NSString * const kSegueShowRouteInfo            = @"showRouteInfo";
NSString * const kSegueShowTicketRetailer       = @"showTicketRetailerDetailed";
NSString * const kSegueShowTicketRetailers      = @"showTicketOutlets";
NSString * const kSegueShowTimetables           = @"showTimetables";
NSString * const kSegueShowUpdatesDetailed      = @"showUpdatesDetailed";

NSString * const kSegueUnwindFromServiceUpdates = @"UnwindFromServiceUpdates";
NSString * const kSegueUnwindToMore             = @"UnwindToMore";
NSString * const kSegueSynchronise              = @"Synchronise";
NSString * const kSegueShowNearbyStops          = @"ShowNearbyStops";
NSString * const kSegueShowMostRecent           = @"ShowMostRecent";
NSString * const kSegueTimetablesToSelectRoute  = @"TimetablesToSelectRoute";
NSString * const kSegueRouteFilterToSelectRoute = @"RouteFilterToSelectRoute";
NSString * const kSegueEditFavourites           = @"EditFavourites";
NSString * const kSegueTimetablesToStopInfo     = @"TimetablesToStopInfo";
NSString * const kSeguePIDToTimetables          = @"PIDToTimetables";
NSString * const kSegueEditGroups               = @"EditGroups";
NSString * const kSeguePIDToStopMap             = @"PIDToStopMap";
NSString * const kSegueShowRoute35Info          = @"showRoute35Info";
NSString * const kSeguePIDToMyTram              = @"PIDToMyTram";
NSString * const kSegueShowSearchResults        = @"showSearchResults";
NSString * const kSegueShowPOI                  = @"showPOI";

/*
 * Notifications
 */

NSString * const kNotificationPIDResetRight             = @"resetPIDFilters";
NSString * const kNotificationSyncFinished              = @"syncFinished";
NSString * const kNotificationRoutesResetRight          = @"resetRouteFilters";
NSString * const kNotificationScheduleTouchedInPinOnMap = @"TTDidTouchScheduledCalloutAccessory";
NSString * const kNotificationPIDTouchedInPinOnMap      = @"TTDidTouchRealTimeCalloutAccessory";
NSString * const kNotificationTicketOutletInfoTouchedInPinOnMap = @"TTDidTouchTicketCalloutAccessory";
NSString * const kNotificationRouteFilterApplied        = @"routeFilterApplied";

NSString * const kNotificationHasFinishedUpdating       = @"hasFinishedUpdating";
NSString * const kNotificationHasFailedUpdating         = @"hasFailedUpdating";

NSString * const TTStopsAndRoutesHaveBeenUpdatedNotice = @"TTStopsAndRoutesHaveBeenUpdatedNotice";
NSString * const TTTicketOutletsHaveBeenUpdatedNotice = @"TTTicketOutletsHaveBeenUpdatedNotice";

/*
 * Deltas
 */

NSInteger const kNearby_Threshold_200M  = 7;
NSInteger const kNearby_Threshold_500M  = 14;
NSInteger const kNearby_Threshold_MAX   = 18;

CGFloat const   kDefaultLatDelta = 0.00634111;
CGFloat const   kDefaultLonDelta = 0.00699520;

/*
 * View Names
 */

NSString * const kScreenSearch          = @"Search";
NSString * const kScreenTicketDetailed  = @"Ticket Detailed";
NSString * const kScreenRoutesList      = @"Routes List";
NSString * const kScreenTrackerID       = @"TrackerID";
NSString * const kScreenTicketOutlet    = @"Ticket Outlets";
NSString * const kScreenTimetables      = @"Timetables";
NSString * const kScreenPID             = @"PID";
NSString * const kScreenAddFavourites   = @"Add Favourite";

NSString * const kNotificationMostRecent = @"mostRecentUpdatedNotification";

/*
 * SB Name
 */

NSString * const kMainStoryboard = @"Main";


/*
 * Tab Bar Controller Indices
 */

NSInteger const kIndexmyTram = 3;

/*
 * UserDefaults
 */

NSString * const kFavouriteStopCountChanged = @"FavouriteStopCountChanged";
NSString * const kMostRecentStopCountChanged = @"MostRecentStopCountChanged";
NSString * const kLastSynchronisationDate = @"klastSynchronisationNSDate";

/*
 * Search Screen Sections
 */

NSString * const kStops             = @"Stops";
NSString * const kTicketOutlets     = @"Ticket outlets";
NSString * const kPointsOfInterest  = @"Points of interest";

/*
 * Tab Bar Controller Animations
 */

NSInteger const kTransitionToMap  = UIViewAnimationOptionTransitionCurlUp;
NSInteger const kTransitionToList = UIViewAnimationOptionTransitionCurlDown;

@end
