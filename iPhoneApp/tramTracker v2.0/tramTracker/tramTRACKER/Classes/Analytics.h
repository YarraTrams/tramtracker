//
//  Analytics.h
//  tramTracker
//
//  Created by Hugo Cuvillier on 12/02/2014.
//  Copyright (c) 2014 AppsCore. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Analytics : NSObject


typedef enum
{
    AnalyticsFeatureFavouritesList,
    AnalyticsFeatureFavouritesMap,
    AnalyticsFeatureNearbyList,
    AnalyticsFeatureNearbyMap,
    AnalyticsFeaturePID,
    AnalyticsFeatureAddToFavourite,
    AnalyticsFeatureStopInformation,
    AnalyticsFeatureRouteFilter,
    AnalyticsFeatureSchedulesDeparture,
    AnalyticsFeaturePIDNearbyTicketOutlet,
    AnalyticsFeaturePIDNearbyTicketOutletMap,
    AnalyticsFeatureDirections,
    AnalyticsFeatureStopMap,
    AnalyticsFeatureRoutes,
    AnalyticsFeatureNetwork,
    AnalyticsFeatureRoutesDetailedList,
    AnalyticsFeatureRoutesDetailedMap,
    AnalyticsFeatureMyTram,
    AnalyticsFeatureMyTramDetailedList,
    AnalyticsFeatureMyTramDetailedMap,
    AnalyticsFeatureMore,
    AnalyticsFeatureEnterTrackerID,
    AnalyticsFeatureSearch,
    AnalyticsFeatureSearchResultList,
    AnalyticsFeatureSearchResultMap,
    AnalyticsFeaturePOIInfo,
    AnalyticsFeatureOutletList,
    AnalyticsFeatureOutletMap,
    AnalyticsFeatureOutletDetailed,
    AnalyticsFeatureServiceUpdatesList,
    AnalyticsFeatureServiceUpdatesDetailed,
    AnalyticsFeatureFollowOnTwitter,
    AnalyticsFeaturePTV,
    AnalyticsFeatureTimeTables,
    AnalyticsFeatureTimeTablesList,
    AnalyticsFeatureTimeTablesMap,
    AnalyticsFeatureFeedback,
    AnalyticsFeatureHelp,
    AnalyticsFeatureSettings,
    AnalyticsFeatureMostRecentList,
    AnalyticsFeatureUpdates
}   AnalyticsFeature;

typedef enum
{
    AnalyticsFilterNone,
    AnalyticsFilterAll,
    AnalyticsFilterShelter,
    AnalyticsFilterEasyAccess,
    AnalyticsFilterOutlets,
    AnalyticsFilterLowFloor,
    AnalyticsFilterRoute
}   AnalyticsFilter;

extern NSString * const kAnalyticsAllowAnalyticsKey;

+ (instancetype)sharedInstance;

- (void)appHasLaunched;
- (void)appHasClosed;
- (void)featureAccessed:(AnalyticsFeature)aFeature;
- (void)featureAccessed:(AnalyticsFeature)aFeature withFilter:(AnalyticsFilter)aFilter;

@end
