//
//  RightMenuDelegate.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 21/11/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    StopRightMenuAll = 0,
    StopRightMenuShelter,
    StopRightMenuAccess,
    StopRightMenuOutlets,
    StopRightMenuTrackerID,
    StopRightMenuSearch,
    StopRightMenuCurrent
}   StopRightMenuType;

@protocol StopRightMenuDelegate <NSObject>

- (void)didSelectStopAction:(StopRightMenuType)filterType;

@property (nonatomic, assign) StopRightMenuType filterType;

@end

typedef enum
{
    PIDRightMenuAll,
    PIDRightMenuLowFloorOnly,
    PIDRightMenuRouteFilter,
    PIDRightMenuAddToFavourite,
    PIDRightMenuStopInformation,
    PIDRightMenuScheduledDepartures,
    PIDRightMenuTicketOutlets,
    PIDRightMenuDirections,
    PIDRightMenuStopMap
}   PIDRightMenuType;

@protocol PIDRightMenuDelegate <NSObject>

- (void)didSelectPIDAction:(PIDRightMenuType)filterType;

@end