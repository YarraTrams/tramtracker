

//
//  BackgroundSynchroniser.m
//  tramTRACKER
//
//  Created by Robert Amos on 27/07/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "BackgroundSynchroniser.h"
#import "PidsService.h"
#import "Stop.h"
#import "Route.h"
#import "StopList.h"
#import "RouteUpdateOperation.h"
#import "StopUpdateOperation.h"
#import "RoutesThroughStopUpdateOperation.h"
#import "Turn.h"
#import "StopDeleteOperation.h"
#import "RouteDeleteOperation.h"
#import "TurnDeleteOperation.h"
#import "TicketRetailerUpdateOperation.h"
#import "tramTRACKERAppDelegate.h"
#import "TicketRetailerDeleteOperation.h"
#import "POIsThroughStopUpdateOperation.h"
#import "POIUpdateOperation.h"
#import "Constants.h"
#import "PointOfInterest.h"
#import "POIDeleteOperation.h"
#import "TicketRetailer.h"

NSString * const TTSyncKeyStops = @"stops";
NSString * const TTSyncKeyRoutes = @"routes";
NSString * const TTSyncKeyTickets = @"tickets";
NSString * const TTSyncKeyPOIs = @"pois";
NSString * const TTSyncKeyUpdates = @"updates";
NSString * const TTSyncKeyServerTime = @"serverTime";

NSInteger TTSyncTypeAutomatic = 1;
NSInteger TTSyncTypeAutoCheckManualSync = 2;
NSInteger TTSyncTypeManual = 3;

@implementation BackgroundSynchroniser

@synthesize delegate, stopsToBeUpdated, stopsToBeDeleted, routesToBeUpdated, routesToBeDeleted, routesThroughStopsToBeUpdated, synchronisationInProgress, poisThroughStopsToBeUpdated;

- (id)initWithLastSynchronisationDate:(NSDate *)date persistentStoreCoordinator:(NSPersistentStoreCoordinator *)storeCoordinator
{
	if (self = [super init])
	{
		// retain our objects
		persistentStoreCoordinator = storeCoordinator;

		synchronisationDate = date;
		service = [[PidsService alloc] initWithDelegate:self];

		shouldKeepRunning = YES;

		queue = [NSOperationQueue new];
		[queue setMaxConcurrentOperationCount:1];
		[queue setSuspended:YES];
		
		deleteQueue = [NSOperationQueue new];
		[deleteQueue setMaxConcurrentOperationCount:1];
		[deleteQueue setSuspended:YES];

		ticketQueue = [NSOperationQueue new];
		[ticketQueue setMaxConcurrentOperationCount:1];
		[ticketQueue setSuspended:YES];
		
		// we need to keep a list of those stops and routes that are affected, to make sure
		// we don't update them more than once
		stopsToBeUpdated = [NSMutableArray new];
		stopsToBeDeleted = [NSMutableArray new];
		routesThroughStopsToBeUpdated = [NSMutableArray new];
		routesToBeUpdated = [NSMutableArray new];
		routesToBeDeleted = [NSMutableArray new];

        // register for notifications as long as we're around
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveMemoryWarning:) name:UIApplicationDidReceiveMemoryWarningNotification object:nil];
	}
	return self;
}

- (void)stop
{
	[service setDelegate:nil];
}

- (void)dealloc
{
    [service setDelegate:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)synchroniseInBackgroundThread
{
	[self synchroniseInBackgroundThread:nil];
}

- (void)synchroniseInBackgroundThread:(NSDictionary *)params
{
	// create a pool and run loop
	NSRunLoop *runLoop = [NSRunLoop currentRunLoop];
	//NSLog(@"Starting background synchronisation");
	isAutomatedSync = YES;

	// let everyone know we're starting
	if (!isCancelled && [self.delegate respondsToSelector:@selector(syncDidStart)])
		[self.delegate performSelectorOnMainThread:@selector(syncDidStart) withObject:nil waitUntilDone:NO];

    synchronisationInProgress = YES;
    
	// start the synchronisation!
	if (!isCancelled)
	{
		if (params)
            [self startUpdatingWithUpdates:params];
        else
			[self synchronise];
	}
    
    // start the run loop, run as long as there are operations in the queue (and the queue has been setup)
    while (!isCancelled && (shouldKeepRunning || [queue operationCount] > 0))
        [runLoop runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];

    // then we move onto the delete queue

    [deleteQueue setSuspended:NO];
    while (!isCancelled && [deleteQueue operationCount] > 0)
        [runLoop runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    
    // now that that is all finished, update the tickets if necessary
    // start the run loop, run as long as there are operations in the queue (and the queue has been setup)
    [ticketQueue setSuspended:NO];
    while (!isCancelled && (shouldKeepRunning || [ticketQueue operationCount] > 0))
    {
        [runLoop runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        //if (!runLoopResult)
        //	break;
    }
	
	// save all the core data stuff back into place
	if (!isCancelled)
	{
		NSError *error;
		NSManagedObjectContext *context = [self managedObjectContext];
//        [context setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
        
        [[context parentContext] performBlockAndWait:^{
            [context.parentContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
        }];

        if ([self.delegate respondsToSelector:@selector(syncDidStartSaving)])
			[self.delegate performSelectorOnMainThread:@selector(syncDidStartSaving) withObject:nil waitUntilDone:NO];
		
		if ([context save:&error])
		{
			NSLog([context hasChanges] ? @"Not really saved." : @"Saved successfully.");

			if (!isCancelled && [self.delegate respondsToSelector:@selector(syncDidFinishAtTime:)])
				[self.delegate performSelectorOnMainThread:@selector(syncDidFinishAtTime:)
                                                withObject:serverTimeOfUpdates
                                             waitUntilDone:NO];

		} else
		{
			
			// is it a merging error?
			if ([error code] == NSManagedObjectMergeError)
				NSLog(@"A Merge Error occured while saving: %@", [[error userInfo] objectForKey:@"conflictList"]);
			else
                NSLog(@"An error occured while saving: %@, %@", error, [[error userInfo] objectForKey:NSDetailedErrorsKey]);

			[context rollback];

			// send a failure message to the delegate
			if ([self.delegate respondsToSelector:@selector(syncDidFailWithError:)])
				[self.delegate performSelectorOnMainThread:@selector(syncDidFailWithError:) withObject:error waitUntilDone:NO];
		}
	}
    
    
    synchronisationInProgress = NO;
	
	// send out a stop update message
	[[NSNotificationCenter defaultCenter] postNotificationName:TTStopsAndRoutesHaveBeenUpdatedNotice object:nil];
	
	//NSLog(@"Stopping background thread");
	CFRunLoopStop([runLoop getCFRunLoop]);
}

- (void)checkForUpdatesInBackgroundThread:(NSDictionary *)params
{
	NSRunLoop *runLoop = [NSRunLoop currentRunLoop];

	isAutomatedSync = NO;

	// let everyone know we're starting
	if (!isCancelled && [self.delegate respondsToSelector:@selector(checkForUpdatesDidStart)])
		[self.delegate performSelectorOnMainThread:@selector(checkForUpdatesDidStart) withObject:nil waitUntilDone:NO];
	
    synchronisationInProgress = YES;

	// start the checking!
	if (!isCancelled)
        [self synchronise];

    // start the run loop, run as long as there are operations in the queue (and the queue has been setup)
    while (!isCancelled && (shouldKeepRunning || [queue operationCount] > 0))
        [runLoop runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    
    synchronisationInProgress = NO;
	
	CFRunLoopStop([runLoop getCFRunLoop]);
}

- (void)synchronise
{
	// check with the PidsService for a list of things that have changed since last time
	[service updatesSinceDate:synchronisationDate];
}

- (void)cancel
{
	// Cancel any active operations
	if (queue != nil && [[queue operations] count] > 0)
		[queue cancelAllOperations];
	
	if (deleteQueue != nil && [[deleteQueue operations] count] > 0)
		[deleteQueue cancelAllOperations];

	if (ticketQueue != nil && [[ticketQueue operations] count] > 0)
		[ticketQueue cancelAllOperations];
	
	if ([self.delegate respondsToSelector:@selector(syncWasCancelled)])
		[self.delegate performSelectorOnMainThread:@selector(syncWasCancelled) withObject:nil waitUntilDone:NO];
	
    synchronisationInProgress = NO;
	isCancelled = YES;
}

- (void)didReceiveMemoryWarning:(NSNotification *)note 
{
     if (wasAborted)
        return;

	// Cancel any active operations
	if (queue != nil && [[queue operations] count] > 0)
		[queue cancelAllOperations];
	
	if (deleteQueue != nil && [[deleteQueue operations] count] > 0)
		[deleteQueue cancelAllOperations];
    
	if (ticketQueue != nil && [[ticketQueue operations] count] > 0)
		[ticketQueue cancelAllOperations];
	
	if ([self.delegate respondsToSelector:@selector(syncWasAbortedDueToMemoryWarning)])
		[self.delegate performSelectorOnMainThread:@selector(syncWasAbortedDueToMemoryWarning) withObject:nil waitUntilDone:NO];
	
	synchronisationInProgress = NO;
    isCancelled = YES;
    wasAborted = YES;
}


- (void)startUpdatingWithUpdates:(NSDictionary *)updates
{
    synchronisationInProgress = YES;

	// Group them
	if (updates != nil && [updates count] > 0)
	{
		NSMutableArray * stopUpdates = updates[TTSyncKeyStops];
		NSMutableArray * routeUpdates = updates[TTSyncKeyRoutes];
		NSMutableArray * poiUpdates = updates[TTSyncKeyPOIs];
        NSMutableArray * outletUpdates = updates[TTSyncKeyTickets];

		if ([routeUpdates count] > 0)
			[self updateRoutes:routeUpdates];
		if ([stopUpdates count] > 0)
			[self updateStops:stopUpdates];
        if ([poiUpdates count] > 0)
            [self updatePOIs:poiUpdates];
        if ([outletUpdates count] > 0)
            [self updateOutlets:outletUpdates];
	}

	// at this point, reset the total on the progress count to be the detected operations
	if ([self.delegate respondsToSelector:@selector(resetTotalUpdates:)])
	{
		// add the queue count
		NSNumber *queueCount = @(queue.operations.count + deleteQueue.operations.count + ticketQueue.operations.count);
		[self.delegate performSelectorOnMainThread:@selector(resetTotalUpdates:) withObject:queueCount waitUntilDone:NO];
	}

	// unsuspend the queue - let it run
	[queue setSuspended:NO];

	// tell the run loop that we're not holding it open anymore, once queue operations have finished it can close
    synchronisationInProgress = NO;
	shouldKeepRunning = NO;
}

// Indicates a failure of some sort.
- (void)pidsServiceDidFailWithError:(NSError *)error
{
	// send a failure message to the delegate
	if ([self.delegate respondsToSelector:@selector(syncDidFailWithError:)])
		[self.delegate performSelectorOnMainThread:@selector(syncDidFailWithError:) withObject:error waitUntilDone:NO];

    shouldKeepRunning = NO;
}

- (void)addTurnDeleteToQueue:(Turn *)turn
{
	// double check to make sure that we have a valid turn
	if (!turn)
		return;

	// Add the operation to the delete queue
	TurnDeleteOperation *operation = [[TurnDeleteOperation alloc] initWithTurn:turn];

	[operation setSyncManager:self];
	[deleteQueue addOperation:operation];
}


#pragma mark -
#pragma mark Data Retrieval

- (Stop *)stopForTrackerID:(NSNumber *)aTrackerID
{
	NSManagedObjectContext *context = [self managedObjectContext];
	NSFetchRequest *fetchRequest = [NSFetchRequest new];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Stop" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"trackerID == %@", aTrackerID];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (!fetchedObjects.count)
		return nil;

	return fetchedObjects .firstObject;
}

- (NSDictionary *)stopsForTrackerIDs:(NSArray *)aTrackerIDArray
{
	// Grab our managed object context and create a fetch request
	NSManagedObjectContext *context = [self managedObjectContext];
	NSFetchRequest *fetchRequest = [NSFetchRequest new];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Stop" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"trackerID IN %@", aTrackerIDArray];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (!fetchedObjects.count)
		return nil;
	
	// return the object
	
	NSMutableDictionary *list = [NSMutableDictionary new];
    
	for (Stop *s in fetchedObjects)
		[list setObject:s forKey:s.trackerID];
	return list;
}

- (NSDictionary *)poisForTrackerIDs:(NSArray *)aTrackerIDArray
{
	// Grab our managed object context and create a fetch request
	NSManagedObjectContext *context = [self managedObjectContext];
	NSFetchRequest *fetchRequest = [NSFetchRequest new];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"PointOfInterest" inManagedObjectContext:context];

	[fetchRequest setEntity:entity];
    
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"poiID IN %@", aTrackerIDArray];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (!fetchedObjects.count)
		return nil;
	
	// return the object
	
	NSMutableDictionary *list = [NSMutableDictionary new];
	for (PointOfInterest *s in fetchedObjects)
		[list setObject:s forKey:s.poiID];
	return list;
}

- (NSDictionary *)retailersForIDs:(NSArray *)aTrackerIDArray
{
	// Grab our managed object context and create a fetch request
	NSManagedObjectContext *context = [self managedObjectContext];
	NSFetchRequest *fetchRequest = [NSFetchRequest new];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"TicketRetailer" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
    
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"retailerID IN %@", aTrackerIDArray];
	[fetchRequest setPredicate:predicate];

	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (!fetchedObjects.count)
		return nil;
	
	// return the object
	
	NSMutableDictionary *list = [NSMutableDictionary new];
	for (TicketRetailer *s in fetchedObjects)
		[list setObject:s forKey:s.retailerID];
	return list;
}

- (Route *)routeForInternalRouteNumber:(NSString *)internalRouteNumber
{
	NSManagedObjectContext *context = [self managedObjectContext];
	NSFetchRequest *fetchRequest = [NSFetchRequest new];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Route" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"internalNumber == %@", internalRouteNumber];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (!fetchedObjects.count)
		return nil;
	
	// return the object
	return fetchedObjects.firstObject;
}

- (NSDictionary *)routesForInternalRouteNumbers:(NSArray *)anInternalRouteNumberArray
{
	// Grab our managed object context and create a fetch request
	NSManagedObjectContext *context = [self managedObjectContext];
	NSFetchRequest *fetchRequest = [NSFetchRequest new];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Route" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"internalNumber IN %@", anInternalRouteNumberArray];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (!fetchedObjects.count)
		return nil;

	// return the object
	NSMutableDictionary *list = [NSMutableDictionary new];
	for (Route *r in fetchedObjects)
        list[r.internalNumber] = r;

	return list;
}

- (Turn *)turnForRoute:(Route *)aRoute upDirection:(BOOL)isUpDirection stop:(Stop *)aStop
{
	// and build the predicate
	Stop *xStop = [self stopForTrackerID:aStop.trackerID];
	Route *xRoute = [self routeForInternalRouteNumber:aRoute.internalNumber];
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"upDirection == %@ AND stop == %@", [NSNumber numberWithBool:isUpDirection], xStop];
	return [[xRoute.turns filteredSetUsingPredicate:predicate] anyObject];
}

- (NSArray *)turnsForRoute:(Route *)aRoute upDirection:(BOOL)isUpDirection
{
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"upDirection == %@", @(isUpDirection)];
	return [[aRoute.turns filteredSetUsingPredicate:predicate] allObjects];
}


- (NSManagedObjectContext *)managedObjectContext
{
    if (managedObjectContext != nil)
        return managedObjectContext;
	
    NSPersistentStoreCoordinator *coordinator = persistentStoreCoordinator;
    if (coordinator != nil)
    {
        tramTRACKERAppDelegate * d = [[UIApplication sharedApplication] delegate];
        
        managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
//        [managedObjectContext setPersistentStoreCoordinator: coordinator];
        managedObjectContext.parentContext = d.managedObjectContext;
    }
    return managedObjectContext;
}

#pragma mark Routes

- (void)updateRoutes:(NSArray *)updates
{
	NSManagedObjectContext *context = [self managedObjectContext];
    
	// get all affected routes
	NSMutableArray *internalRouteNumbers = [NSMutableArray new];
	for (RouteUpdate *update in updates)
		[internalRouteNumbers addObject:update.internalRouteNumber];
	NSDictionary *routes = [self routesForInternalRouteNumbers:internalRouteNumbers];
    internalRouteNumbers = nil;
    
	for (RouteUpdate *routeU in updates)
	{
		Route *route = [routes objectForKey:routeU.internalRouteNumber];
        
		if (routeU.actionType == PIDServiceActionTypeUpdate)
        {
            if (!route)
				route = [NSEntityDescription insertNewObjectForEntityForName:@"Route" inManagedObjectContext:context];
            
            [route setInternalNumber:routeU.internalRouteNumber];
            [route setNumber:routeU.routeNumber];
            [route setHeadboardNumber:routeU.headboardRouteNumber];
            [route setSubRoute:[routeU isSubRoute]];
            
            // dont attempt to overwrite the colour if it is not supplied
            if (routeU.colour != nil)
                [route setColour:routeU.colour];
            
            [self addRouteUpdateToQueue:route];
            //NSLog(@"Adding route with internal route number: %@", update.internalRouteNumber);
        }
        else
        {
            if (route)
				[self addRouteDeleteToQueue:route];
            else if ([self.delegate respondsToSelector:@selector(syncDidRemoveOperation)])
                [self.delegate performSelectorOnMainThread:@selector(syncDidRemoveOperation) withObject:nil waitUntilDone:NO];
        }
	}
}

- (void)addRouteUpdateToQueue:(Route *)route
{
	// double check to make sure that we have a valid route
	if (!route.internalNumber)
		return;
    
	// check to see if this one is in the list already
	if ([self.routesToBeUpdated containsObject:route.internalNumber] || [self.routesToBeDeleted containsObject:route.internalNumber])
		return;
	
	// create the queue object
	RouteUpdateOperation *operation = [[RouteUpdateOperation alloc] initWithRoute:route];
	[operation setSyncManager:self];
	[queue addOperation:operation];
    
	// send a notification
	if ([self.delegate respondsToSelector:@selector(syncDidAddOperation:)])
		[self.delegate performSelectorOnMainThread:@selector(syncDidAddOperation:) withObject:operation waitUntilDone:NO];
    
	// add it to the update list
	NSString *routeNumber = [NSString stringWithString:route.internalNumber];
	[self.routesToBeUpdated addObject:routeNumber];
}

- (void)addRouteDeleteToQueue:(Route *)route
{
	// double check to make sure that we have a valid route
	if (!route.internalNumber)
		return;
	
	// check to see if this one is in the list already
	if ([self.routesToBeDeleted containsObject:route.internalNumber])
		return;
    
	// if we have stops in either direction make sure that we update the routes through those stops
	// since this route will obviously no longer be in those lists
    for (NSNumber *trackerID in [route.upStops arrayByAddingObjectsFromArray:route.downStops])
        [self addRoutesThroughTrackerIDToQueue:trackerID];
    
	// create the queue object
	RouteDeleteOperation *operation = [[RouteDeleteOperation alloc] initWithRoute:route];
    
	[operation setSyncManager:self];
	[deleteQueue addOperation:operation];
	
	// send a notification
	if ([self.delegate respondsToSelector:@selector(syncDidAddOperation:)])
		[self.delegate performSelectorOnMainThread:@selector(syncDidAddOperation:) withObject:operation waitUntilDone:NO];
	
	// add it to the update list
	NSString *routeNumber = [NSString stringWithString:route.internalNumber];
	[self.routesToBeDeleted addObject:routeNumber];
}

- (void)addRoutesThroughTrackerIDToQueue:(NSNumber *)trackerID
{
	[self addRoutesThroughTrackerIDToQueue:trackerID fromOperation:nil];
}

- (void)addRoutesThroughTrackerIDToQueue:(NSNumber *)trackerID fromOperation:(NSOperation *)dependantOperation
{
    if ([trackerID isEqualToNumber:@0])
        return;

    Stop *stop = [self stopForTrackerID:trackerID];

	if (stop)
		[self addRoutesThroughStopToQueue:stop fromOperation:dependantOperation];
}

- (void)addRoutesThroughStopToQueue:(Stop *)stop
{
	[self addRoutesThroughStopToQueue:stop fromOperation:nil];
}

- (void)addPoisThroughStopToQueue:(Stop *)stop
{
    if (!stop.trackerID)
        return;
    
    
    if ([self.stopsToBeDeleted containsObject:stop.trackerID] || [self.poisThroughStopsToBeUpdated containsObject:stop.trackerID])
        return;
    
    POIsThroughStopUpdateOperation * operation = [[POIsThroughStopUpdateOperation alloc] initWithStop:stop];
    
    [operation setSyncManager:self];
    
    [queue addOperation:operation];
    
    if ([self.delegate respondsToSelector:@selector(syncDidAddOperation:)])
        [self.delegate performSelectorOnMainThread:@selector(syncDidAddOperation:) withObject:operation waitUntilDone:NO];
    [self.poisThroughStopsToBeUpdated addObject:stop.trackerID];
}

- (void)addRoutesThroughStopToQueue:(Stop *)stop fromOperation:(NSOperation *)dependantOperation
{
	// double check to make sure that we have a valid stop
	if (!stop.trackerID)
		return;
    
	// check to see if its in the stop delete queue or routes through stop update queue
	if ([self.stopsToBeDeleted containsObject:stop.trackerID] || [self.routesThroughStopsToBeUpdated containsObject:stop.trackerID])
		return;
	
	// it's not, add it to the queue
	RoutesThroughStopUpdateOperation *operation = [[RoutesThroughStopUpdateOperation alloc] initWithStop:stop];
	[operation setSyncManager:self];
    
	// if we have a dependency add it here
	if (dependantOperation)
		[operation addDependency:dependantOperation];
	
	// add the operation to the queue
	[queue addOperation:operation];
    
	// send a notification
	if ([self.delegate respondsToSelector:@selector(syncDidAddOperation:)])
		[self.delegate performSelectorOnMainThread:@selector(syncDidAddOperation:) withObject:operation waitUntilDone:NO];

	[self.routesThroughStopsToBeUpdated addObject:stop.trackerID];
}

#pragma mark Stops

- (void)updateStops:(NSArray *)updates
{
	NSManagedObjectContext *context = [self managedObjectContext];
	
	NSMutableArray  * trackerIDs = [NSMutableArray new];
    
	for (StopUpdate *update in updates)
		[trackerIDs addObject:update.trackerID];

	NSDictionary    * stops = [self stopsForTrackerIDs:trackerIDs];
	
	// loop over the stops and do what we need to do
	for (StopUpdate *update in updates)
	{
        Stop *stop = [stops objectForKey:update.trackerID];
		if (!stop)
		{
			if (update.actionType == PIDServiceActionTypeUpdate)
			{
				stop = [NSEntityDescription insertNewObjectForEntityForName:@"Stop" inManagedObjectContext:context];
				[stop setTrackerID:update.trackerID];
				[stop setTurns:nil];
				[stop setRoutesThroughStop:@[]];
                [stop setPoisThroughStop:@[]];

                [self addRoutesThroughStopToQueue:stop];
                [self addPoisThroughStopToQueue:stop];
                [self addStopUpdateToQueue:stop];
			}
		}
        else
		{
			if (update.actionType == PIDServiceActionTypeDelete)
			{
				if (stop != nil)
					[self addStopDeleteToQueue:stop];
				else if ([self.delegate respondsToSelector:@selector(syncDidRemoveOperation)])
                    [self.delegate performSelectorOnMainThread:@selector(syncDidRemoveOperation) withObject:nil waitUntilDone:NO];
			}
            else
				[self addStopUpdateToQueue:stop];
		}
        stop = nil;
	}
}

- (void)addStopUpdateToQueue:(Stop *)stop
{
	// double check to make sure that we have a valid stop
	if (!stop.trackerID)
		return;
    
	// check to see if this stop's tracker id exists in the list already
	if ([self.stopsToBeDeleted containsObject:stop.trackerID] || [self.stopsToBeUpdated containsObject:stop.trackerID])
		return;
	
	StopUpdateOperation *operation = [[StopUpdateOperation alloc] initWithStop:stop];
	[operation setSyncManager:self];
	[queue addOperation:operation];
    
	// send a notification
	if ([self.delegate respondsToSelector:@selector(syncDidAddOperation:)])
		[self.delegate performSelectorOnMainThread:@selector(syncDidAddOperation:) withObject:operation waitUntilDone:NO];
    
	// add the stop to the list
	[self.stopsToBeUpdated addObject:stop.trackerID];
}

- (void)addStopDeleteToQueue:(Stop *)stop
{
	// double check to make sure that we have a valid stop
	if (!stop.trackerID)
		return;
    
	// check to see if this stop's tracker id exists in the list already
	if ([self.stopsToBeDeleted containsObject:stop.trackerID])
		return;
	
	// loop through all routes and check to see if they contain this stop. Make sure it gets updated if they do
	NSArray *routes = [Route allRoutes];
	for (Route *r in routes)
		if ([r.upStops containsObject:stop.trackerID] || [r.downStops containsObject:stop.trackerID])
            [self addRouteUpdateToQueue:r];
    
	// Add the operation to the delete queue
	StopDeleteOperation *operation = [[StopDeleteOperation alloc] initWithStop:stop];
	[operation setSyncManager:self];
	[deleteQueue addOperation:operation];
    
	// send a notification
	if ([self.delegate respondsToSelector:@selector(syncDidAddOperation:)])
		[self.delegate performSelectorOnMainThread:@selector(syncDidAddOperation:) withObject:operation waitUntilDone:NO];
    
	// add the stop to the list
	NSNumber *trackerID = [NSNumber numberWithInteger:[stop.trackerID integerValue]];
	[self.stopsToBeDeleted addObject:trackerID];
}

#pragma mark POIs

- (void)updatePOIs:(NSArray *)updates
{
	NSManagedObjectContext *context = [self managedObjectContext];
	
	// get all the stops
	NSMutableArray *poiIDS = [NSMutableArray new];
    
	for (PointOfInterestUpdate *update in updates)
		[poiIDS addObject:@(update.ID)];
    
    NSDictionary *pois = [self poisForTrackerIDs:poiIDS];
    
    for (PointOfInterestUpdate *poiU in updates)
    {
        PointOfInterest * poi = [pois objectForKey:@(poiU.ID)];
        
        if (poiU.actionType == PIDServiceActionTypeUpdate)
        {
            if (!poi)
            {
                poi = [NSEntityDescription insertNewObjectForEntityForName:@"PointOfInterest"
                                                    inManagedObjectContext:context];
                poi.poiID = @(poiU.ID);
            }
            [self addPointOfInterestUpdateToQueue:poi];
        }
        else if (poiU.actionType == PIDServiceActionTypeDelete)
        {
            if (poi)
                [self addPointOfInterestDeleteToQueue:poi];
            else if ([self.delegate respondsToSelector:@selector(syncDidRemoveOperation)])
                [self.delegate performSelectorOnMainThread:@selector(syncDidRemoveOperation) withObject:nil waitUntilDone:NO];
        }
    }
}

- (void)addPointOfInterestUpdateToQueue:(PointOfInterest *)poi
{
	// double check to make sure that we have a valid stop
	if (!poi.poiID)
		return;
    
	// check to see if this stop's tracker id exists in the list already
	if ([poiToBeDeleted containsObject:poi.poiID] || [poiToBeUpdated containsObject:poi.poiID])
		return;
    
	POIUpdateOperation *operation = [[POIUpdateOperation alloc] initWithPOI:poi];
    
	[operation setSyncManager:self];
	[queue addOperation:operation];
    
	// send a notification
	if ([self.delegate respondsToSelector:@selector(syncDidAddOperation:)])
		[self.delegate performSelectorOnMainThread:@selector(syncDidAddOperation:) withObject:operation waitUntilDone:NO];
    
	// add the stop to the list
	[poiToBeUpdated addObject:poi.poiID];
}

- (void)addPointOfInterestDeleteToQueue:(PointOfInterest *)poi
{
	// double check to make sure that we have a valid stop
	if (!poi.poiID)
		return;
    
	// check to see if this stop's tracker id exists in the list already
	if ([poiToBeDeleted containsObject:poi.poiID])
		return;
    
	POIDeleteOperation *operation = [[POIDeleteOperation alloc] initWithPOI:poi];
    
	[operation setSyncManager:self];
	[queue addOperation:operation];
    
	// send a notification
	if ([self.delegate respondsToSelector:@selector(syncDidAddOperation:)])
		[self.delegate performSelectorOnMainThread:@selector(syncDidAddOperation:) withObject:operation waitUntilDone:NO];
    
	// add the stop to the list
	[poiToBeDeleted addObject:poi.poiID];
}

#pragma mark Outlets

- (void)updateOutlets:(NSArray *)updates
{
	NSManagedObjectContext *context = [self managedObjectContext];
	
	// get all the stops
	NSMutableArray *ids = [NSMutableArray new];
    
	for (TicketRetailerUpdate *update in updates)
		[ids addObject:@(update.ID)];
    
    NSDictionary *outlets = [self retailersForIDs:ids];
    
    for (TicketRetailerUpdate *retailerU in updates)
    {
        TicketRetailer * retailer = [outlets objectForKey:@(retailerU.ID)];
        
        if (retailerU.actionType == PIDServiceActionTypeUpdate)
        {
            if (!retailer)
            {
                retailer = [NSEntityDescription insertNewObjectForEntityForName:@"TicketRetailer"
                                                         inManagedObjectContext:context];
                
                retailer.retailerID = @(retailerU.ID);
            }

            [self addRetailerUpdateToQueue:retailer];
        }
        else if (retailerU.actionType == PIDServiceActionTypeDelete)
        {
            if (retailer)
                [self addRetailerDeleteToQueue:retailer];
            else if ([self.delegate respondsToSelector:@selector(syncDidRemoveOperation)])
                [self.delegate performSelectorOnMainThread:@selector(syncDidRemoveOperation) withObject:nil waitUntilDone:NO];
        }
    }
}

- (void)addRetailerUpdateToQueue:(TicketRetailer *)retailer
{
	// double check to make sure that we have a valid stop
	if (!retailer.retailerID)
		return;
    
	// check to see if this stop's tracker id exists in the list already
	if ([retailersToBeDeleted containsObject:retailer.retailerID] || [retailersToBeUpdated containsObject:retailer.retailerID])
		return;
    
	TicketRetailerUpdateOperation *operation = [[TicketRetailerUpdateOperation alloc] initWithRetailer:retailer];
    
	[operation setSyncManager:self];
	[queue addOperation:operation];
    
	// send a notification
	if ([self.delegate respondsToSelector:@selector(syncDidAddOperation:)])
		[self.delegate performSelectorOnMainThread:@selector(syncDidAddOperation:) withObject:operation waitUntilDone:NO];
    
	// add the stop to the list
	[retailersToBeUpdated addObject:retailer.retailerID];
}

- (void)addRetailerDeleteToQueue:(TicketRetailer *)retailer
{
	// double check to make sure that we have a valid stop
	if (!retailer.retailerID)
		return;
    
	// check to see if this stop's tracker id exists in the list already
	if ([retailersToBeDeleted containsObject:retailer.retailerID])
		return;
    
	TicketRetailerDeleteOperation *operation = [[TicketRetailerDeleteOperation alloc] initWithRetailer:retailer];
    
	[operation setSyncManager:self];
	[queue addOperation:operation];
    
	// send a notification
	if ([self.delegate respondsToSelector:@selector(syncDidAddOperation:)])
		[self.delegate performSelectorOnMainThread:@selector(syncDidAddOperation:) withObject:operation waitUntilDone:NO];
    
	// add the stop to the list
	[retailersToBeUpdated addObject:retailer.retailerID];
}

#pragma mark SetUpdates

- (void)setTicketOutletsUpdates:(NSArray *)ticketOutletUpdates
{
    ticketOutletsChecked = YES;
    temporaryOutletsUpdateStorage = ticketOutletUpdates;
    
    if (poiChecked && temporaryUpdateStorage)
        [self setUpdates:temporaryUpdateStorage atDate:serverTimeOfUpdates];
    else
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

- (void)setPOIUpdates:(NSArray *)poiUpdates
{
    poiChecked = YES;
    temporaryPOIUpdateStorage = poiUpdates;
    
    if (ticketOutletsChecked && temporaryUpdateStorage)
        [self setUpdates:temporaryUpdateStorage atDate:serverTimeOfUpdates];
    else
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

- (void)setUpdates:(NSArray *)updates atDate:(NSDate *)atDate
{
    if (atDate != nil)
        serverTimeOfUpdates = atDate;
    
    if (updates)
        temporaryUpdateStorage = updates;

    // if we've run before the ticket outlets have been updated, wait for it to finish
    if (!ticketOutletsChecked || !poiChecked)
	{
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
		return;
	}
    
	// Now that we've received a list, is there anything in it?
	if (!temporaryUpdateStorage.count && !temporaryPOIUpdateStorage.count && !temporaryOutletsUpdateStorage.count)
	{
		shouldKeepRunning = NO;
		
		// send a notification just in case
		if (isAutomatedSync == NO)
		{
			if ([self.delegate respondsToSelector:@selector(checkForUpdatesDidFinish:)])
				[self.delegate performSelectorOnMainThread:@selector(checkForUpdatesDidFinish:) withObject:nil waitUntilDone:NO];
            synchronisationInProgress = NO;
		}
		return;
	}
    
    NSArray * routeUpdates = [temporaryUpdateStorage filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"class == %@", [RouteUpdate class]]];
    NSArray * stopUpdates = [temporaryUpdateStorage filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"class == %@", [StopUpdate class]]];

    // we return a very basic one with zeros
    NSDictionary *params = @{
                             TTSyncKeyStops         : stopUpdates,
                             TTSyncKeyRoutes        : routeUpdates,
                             TTSyncKeyPOIs          : temporaryPOIUpdateStorage,
                             TTSyncKeyTickets       : temporaryOutletsUpdateStorage,
                             TTSyncKeyServerTime    : serverTimeOfUpdates};
    
	// are we just checking for updates?
	if (isAutomatedSync == NO)
	{
        
		// post the notification
		if ([self.delegate respondsToSelector:@selector(checkForUpdatesDidFinish:)])
			[self.delegate performSelectorOnMainThread:@selector(checkForUpdatesDidFinish:) withObject:params waitUntilDone:NO];
		
		// let everything die
		shouldKeepRunning = NO;
        synchronisationInProgress = NO;
		return;
	}
	
	[self startUpdatingWithUpdates:params];
}

@end
