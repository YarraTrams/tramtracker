//
//  BackgroundSynchroniser.h
//  tramTRACKER
//
//  Created by Robert Amos on 27/07/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BackgroundSynchroniserDelegate.h"

@class PidsService;
@class StopUpdate;
@class RouteUpdate;
@class Stop;
@class Route;
@class TicketOutletService;

extern NSString * const TTSyncKeyStops;
extern NSString * const TTSyncKeyRoutes;
extern NSString * const TTSyncKeyUpdates;
extern NSString * const TTSyncKeyTickets;
extern NSString * const TTSyncKeyPOIs;

extern NSInteger TTSyncTypeAutomatic;
extern NSInteger TTSyncTypeAutoCheckManualSync;
extern NSInteger TTSyncTypeManual;


/**
 * The BackgroundSynchroniser class manages the synchronisation process.
 * 
 * @ingroup Sync
 **/
@interface BackgroundSynchroniser : NSObject {

    BOOL synchronisationInProgress;
    
	/**
	 * A reference to the persistent Core Data store coordinator. So that changes can be saved to the database.
	**/
	NSPersistentStoreCoordinator *persistentStoreCoordinator;

	/**
	 * The last successful synchronisation date.
	**/
	NSDate *synchronisationDate;

	/**
	 * An instance of the PidsService class. Used to retrieve updates from the service.
	**/
	PidsService *service;

	/**
	 * Property that is watched by the NSRunLoop to see whether it should continue.
	 * Once synchronisation has been completed or aborted this is set to NO.
	**/
	BOOL shouldKeepRunning;

	/**
	 * A copy of the NSManagedObjectContext used to interface with the database.
	**/
	NSManagedObjectContext *managedObjectContext;

	/**
	 * The Update queue. Update Operations are added to this queue for processing and are removed once complete.
	 * The update queue is processed before the delete queue.
	**/
	NSOperationQueue *queue;

	/**
	 * The Delete queue. Delete operations are added to this queue for processing and are removed once complete.
	 * The delete queue is processed after the completion of the update queue.
	**/
	NSOperationQueue *deleteQueue;
	
	NSOperationQueue *ticketQueue;

	/**
	 * Whether the synchronisation that is occuring is an Automated background sync. YES means it is, NO means that
	 * the synchronisation progress is being presented by a UI.
	**/
	BOOL isAutomatedSync;

	/**
	 * The object that acts as the delegate of the synchronisation process.
	 * This must adopt the BackgroundSynchroniserDelegate protocol.
	**/
	NSObject <BackgroundSynchroniserDelegate> *__weak delegate;

	/**
	 * Whether the synchronisation process has been cancalled. This property is monitored by the process
	 * and synchronisation is cancelled if it is set to NO at any time.
	**/
	BOOL isCancelled;
    BOOL wasAborted;
	
	/**
	 * A Mutable array of stops that need updating. This is used to assemble the list from the service and to create
	 * StopUpdateOperation objects to be added to the queue.
	**/
	NSMutableArray *stopsToBeUpdated;

	/**
	 * A Mutable array of stops that need deleting. This is used to assemble the list from the service and to create
	 * StopDeleteOperation objects to be added to the delete queue.
	 **/
	NSMutableArray *stopsToBeDeleted;

	/**
	 * A Mutable array of stops that need their route lists updated. This is used to assemble the list from the service and to create
	 * RoutesThroughStopUpdateOperation objects to be added to the queue.
	 **/
	NSMutableArray *routesThroughStopsToBeUpdated;

    /**
	 * A Mutable array of stops that need their pois lists updated. This is used to assemble the list from the service and to create
	 * POIsThroughStopUpdateOperation objects to be added to the queue.
	 **/
	NSMutableArray *poisThroughStopsToBeUpdated;

	/**
	 * A Mutable array of routes that need updating. This is used to assemble the list from the service and to create
	 * RouteUpdateOperation objects to be added to the queue.
	 **/
	NSMutableArray *routesToBeUpdated;

	/**
	 * A Mutable array of routes that need updating. This is used to assemble the list from the service and to create
	 * RouteDeleteOperation objects to be added to the delete queue.
	 **/
	NSMutableArray *routesToBeDeleted;
    
	/**
	 * A Mutable array of poi that need updating. This is used to assemble the list from the service and to create
	 * PoiUpdateOperation objects to be added to the queue.
     **/
	NSMutableArray *poiToBeUpdated;
    
    /**
	 * A Mutable array of poi that need deleting. This is used to assemble the list from the service and to create
	 * PoiDeleteOperation objects to be added to the queue.
     **/
	NSMutableArray *poiToBeDeleted;
    
    
	/**
	 * A Mutable array of TicketRetailers that need updating. This is used to assemble the list from the service and to create
	 * TicketRetailersUpdateOperation objects to be added to the queue.
     **/
	NSMutableArray *retailersToBeUpdated;
    
    /**
	 * A Mutable array of TicketRetailers that need deleting. This is used to assemble the list from the service and to create
	 * TicketRetailersDeleteOperation objects to be added to the queue.
     **/
	NSMutableArray *retailersToBeDeleted;
    
    /* Status variable for updates {POIs, Retailers, Routes & Stops} */

	BOOL    poiChecked;
    NSArray * temporaryPOIUpdateStorage;

	BOOL ticketOutletsChecked;
    NSArray * temporaryOutletsUpdateStorage;

	NSArray *temporaryUpdateStorage;
    NSDate *serverTimeOfUpdates;
}

@property (nonatomic, weak) NSObject <BackgroundSynchroniserDelegate> *delegate;
@property (nonatomic, readonly) NSMutableArray *stopsToBeUpdated;
@property (nonatomic, readonly) NSMutableArray *stopsToBeDeleted;
@property (nonatomic, readonly) NSMutableArray *routesThroughStopsToBeUpdated;
@property (nonatomic, readonly) NSMutableArray *routesToBeUpdated;
@property (nonatomic, readonly) NSMutableArray *routesToBeDeleted;
@property (nonatomic, readonly) NSMutableArray *poisThroughStopsToBeUpdated;
@property (nonatomic, readonly) BOOL synchronisationInProgress;

/**
 * Initialises and returns a Background Synchroniser object with a last synchronisation date and a Core Data persistent store coordinator.
 *
 * @param	date				A NSDate indicating the last successful synchronisation date. Is provided to the service to find all updates since that date.
 * @param	storeCoordinator	A Core Data store coordinator, so changes can be saved to the database.
 * @return						Returns an initialised BackgroundSynchroniser object or nil if not successfully initialised.
**/
- (id)initWithLastSynchronisationDate:(NSDate *)date persistentStoreCoordinator:(NSPersistentStoreCoordinator *)storeCoordinator;

/**
 * Starts synchronising in a background thread.
**/
- (void)synchroniseInBackgroundThread;

/**
 * Starts synchronising in a background thread using cached updates from a previous check.
 *
 * @param	params				The NSDictionary as passed to the delegate in the -checkForUpdatesDidFinish message
**/
- (void)synchroniseInBackgroundThread:(NSDictionary *)params;

/**
 * Starts checking for updates in a background thread.
 *
 * @param	params				Unused. To be removed.
 * @todo						Refactor to remove params param.
**/
- (void)checkForUpdatesInBackgroundThread:(NSDictionary *)params;


/**
 * Checks with the PidsService for a list of updates since the last synchronisation date.
**/
- (void)synchronise;

/**
 * Stops the synchronisation process and releases the delegate.
**/
- (void)stop;

/**
 * Cancels the synchronisation process. Tells all active and pending operations about the cancellation.
 * Active operations may not be cancelled immediately if they're busy but will cancel before committing any data.
 * The -syncWasCancelled message will be sent to the delegate once the cancellation has been completed.
**/
- (void)cancel;
- (void)didReceiveMemoryWarning:(NSNotification *)note;

/**
 * Creates a RouteUpdateOperation for the specified route and adds it to the queue.
 *
 * @param	route			The Route to be updated.
**/
- (void)addRouteUpdateToQueue:(Route *)route;

/**
 * Creates a StopUpdateOperation for the specified stop and adds it to the queue.
 *
 * @param	stop			The Stop to be updated.
**/
- (void)addStopUpdateToQueue:(Stop *)stop;

/**
 * Creates a RoutesThroughStopUpdateOperation to update the routes through the specified stop and adds it to the queue.
 *
 @param		stop			The Stop to have the list of routes updated.
**/
- (void)addRoutesThroughStopToQueue:(Stop *)stop;

/**
 * Creates a RoutesThroughStopUpdateOperation to update the routes through the specified stop and adds it to the queue.
 * The new RoutesThroughStopUpdateOperation will be made dependant on the dependantOperation finishing first.
 *
 * @param	stop				The Stop to have the list of routes updated.
 * @param	dependantOperation	A dependant operation. If this stop is being updated the StopUpdateOperation should be
 *								the dependant operation so no conflicts occur.
**/
- (void)addRoutesThroughStopToQueue:(Stop *)stop fromOperation:(NSOperation *)dependantOperation;

/**
 * Creates a new RoutesThroughStopUpdateOperation to update the routes through the stop with the supplied tracker id and adds it to the queue.
 *
 * @param	trackerID			A valid tramTRACKER Tracker ID.
**/
- (void)addRoutesThroughTrackerIDToQueue:(NSNumber *)trackerID;

/**
 * Creates a new RoutesThroughStopUpdateOperation to update the routes through the stop with the supplied tracker id and adds it to the queue.
 * The new RoutesThroughStopUpdateOperation will be made dependant on the dependantOperation finishing first.
 *
 * @param	stop				A valid tramTRACKER Tracker ID.
 * @param	dependantOperation	A dependant operation. If this stop is being updated the StopUpdateOperation should be
 *								the dependant operation so no conflicts occur.
**/
 - (void)addRoutesThroughTrackerIDToQueue:(NSNumber *)trackerID fromOperation:(NSOperation *)dependantOperation;

/**
 * Creates a new StopDeleteOperation to delete the specified stop and adds it to the delete queue.
 * This will also trigger updates to any routes that have this stop's tracker ID in their upStops or downStops lists.
 *
 * @param	stop				The stop to be deleted.
**/
- (void)addStopDeleteToQueue:(Stop *)stop;

/**
 * Creates a new RouteDeleteOperation to delete the specified route and adds it to the delete queue.
 *
 * This will trigger an update to the list of routes through the stops for all stops that are on this route.
 *
 * @param	route				The route to be deleted.
**/
- (void)addRouteDeleteToQueue:(Route *)route;

- (void)addTurnDeleteToQueue:(Turn *)turn;

/**
 * Called by the PidsService when it has retrieved a list of available updates. This will process
 * the updates and send through a -checkForUpdatesDidFinish if it was an update-only operation.
 *
 * If it is a synchronisation process it will sort the updates and prepare them for the creation
 * of appropriate operations.
**/
- (void)setUpdates:(NSArray *)updates atDate:(NSDate *)atDate;
- (void)startUpdatingWithUpdates:(NSDictionary *)updates;

/**
 * Loops through the supplied list of StopUpdate templates and commences the update proceedure. It will locate the
 * existing stop (or create a new one if it is being updated and doesn't already exist) and call for the
 * appropriate type of operation to be created. It keeps the stop operations unique to prevent duplicate
 * updates being performed.
 *
 * @param	updates				A NSArray of StopUpdate objects.
**/
- (void)updateStops:(NSArray *)updates;

/**
 * Loops through the supplied list of RouteUpdate templates and commences the update proceedure. It will locate the
 * existing route (or create a new one if it is being updated and doesn't already exist) and call for the
 * appropriate type of operation to be created. It keeps the route operations unique to prevent duplicate
 * updates being performed.
 *
 * @param	updates				A NSArray of RouteUpdate objects.
**/
- (void)updateRoutes:(NSArray *)updates;

/**
 * Data Retrieval - Finds the Stop object for a specified tracker ID.
 * It is done in the thread's context in order to support linking for new objects.
 *
 * @param	aTrackerID			A valid tramTRACKER Tracker ID
 * @return						A Stop object with the matching tracker id, or nil if none found.
**/
- (Stop *)stopForTrackerID:(NSNumber *)aTrackerID;

/**
 * Returns the Stop objects for a specified list of tracker IDs. The Tracker ID will be the key in the NSDictionary.
 * It is done in the thread's context to support linking for new objects.
 *
 * @param	aTrackerIDArray		A NSArray of valid tramTRACKER Tracker IDs
 * @return						A NSDictionary with all matching Stop objects. The Tracker IDs are the keys.
**/
- (NSDictionary *)stopsForTrackerIDs:(NSArray *)aTrackerIDArray;

/**
 * Retrieves the route with the specified internal route number.
 * It is done in the thread's context to support linking for new objects.
 *
 * @param	internalRouteNumber	A string representation of an internal route number.
 * @return						The corresponding Route object, or nil if none found.
**/
- (Route *)routeForInternalRouteNumber:(NSString *)internalRouteNumber;

/**
 * Returns the Route objects for a specified list of internal route numbers. The Internal Route Number will be the key in the NSDictionary.
 * It is done in the thread's context to support linking for new objects.
 *
 * @param	anInternalRouteNumberArray	A NSArray of valid internal route number strings.
 * @return						A NSDictionary with all matching Route objects. The keys are the internal route numbers.
**/
- (NSDictionary *)routesForInternalRouteNumbers:(NSArray *)anInternalRouteNumberArray;

/**
 * Retrieves the Turn object for a specific route, direction and stop combination.
 * This is done in the thread's context to support linking for new objects.
 *
 * @param	aRoute				A Route Object
 * @param	isUpDirection		The direction of travel, YES for up, NO for down
 * @param	aStop				The stop that the turn happens after.
 * @return						A Turn object, or nil if not found
**/
- (Turn *)turnForRoute:(Route *)aRoute upDirection:(BOOL)isUpDirection stop:(Stop *)aStop;

/**
 * Retrieves the Turn objects for a specific route and direction.
 * This is done in the thread's ontext to support linking for new objects.
 *
 * @param	aRoute				A Route object
 * @param	isUpDirection		The direction of travel, YES for up, NO for down
 * @return						An array of Turn objects
**/
- (NSArray *)turnsForRoute:(Route *)aRoute upDirection:(BOOL)isUpDirection;

- (NSManagedObjectContext *)managedObjectContext;

//- (void)updateTicketOutlets;

@end


