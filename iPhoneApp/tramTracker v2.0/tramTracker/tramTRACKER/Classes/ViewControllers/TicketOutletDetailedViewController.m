//
//  TicketOutletDetailedViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 12/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "TicketOutletDetailedViewController.h"
#import "Analytics.h"

@interface TicketOutletDetailedViewController () <UIAlertViewDelegate>

@property (strong, nonatomic) TicketRetailer            * curRetailer;
@property (weak, nonatomic) IBOutlet UILabel            * retailerTitle;
@property (weak, nonatomic) IBOutlet UILabel            * address;
@property (weak, nonatomic) IBOutlet UIImageView        * buyMyKi;
@property (weak, nonatomic) IBOutlet UIImageView        * twentyFourHours;
@property (weak, nonatomic) IBOutlet UIImageView        * topUp;

@end

@implementation TicketOutletDetailedViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.retailerTitle.text = self.curRetailer.name;
    self.address.text = self.curRetailer.address;
    self.twentyFourHours.hidden = !self.curRetailer.isOpen24Hours;
    self.buyMyKi.hidden = ![self.curRetailer.sellsMyki boolValue];
    self.topUp.hidden = ![self.curRetailer.hasMykiTopUp boolValue];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureOutletDetailed];
}

#pragma mark - User Actions on Nav Bar

-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)directionsAction:(id)sender
{
    //Open Apple Maps app
    [[[UIAlertView alloc]
      initWithTitle:@"Open in Maps?"
      message:@"Would you like to close the tramTracker® app and launch Maps?"
      delegate:self
      cancelButtonTitle:@"Cancel"
      otherButtonTitles:@"Launch Maps", nil]
     show];
}

#pragma mark - UIAlertView Delegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex)
    {
        [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureDirections];
        CLLocationDegrees latitude = [self.curRetailer.latitude doubleValue];
        CLLocationDegrees longitude = [self.curRetailer.longitude doubleValue];
        
        MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:CLLocationCoordinate2DMake(latitude, longitude) addressDictionary:nil];
        MKMapItem *item = [[MKMapItem alloc] initWithPlacemark:placemark];
        item.name = self.curRetailer.name;
        [item openInMapsWithLaunchOptions:nil];
    }
}

@end
