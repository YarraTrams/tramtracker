//
//  myTramViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 25/11/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "myTramSearchViewController.h"
#import "Constants.h"
#import "Journey.h"
#import "PidsService.h"
#import "TTActivityIndicatorView.h"
#import "StopList.h"
#import "myTramViewController.h"

#import "Analytics.h"

@interface myTramSearchViewController ()<UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UIBarButtonItem    * goButton;
@property (weak, nonatomic) IBOutlet UISearchBar        * searchBar;
@property (weak, nonatomic) IBOutlet UIScrollView       * myScrollView;

@end

@implementation myTramSearchViewController

#pragma mark - Inits & Loads

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupGestureRecogniser];
    self.goButton.enabled = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self.navigationController selector:@selector(popToRootViewControllerAnimated:) name:kNotificationSyncFinished object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureMyTram];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setupGestureRecogniser
{
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc]
                                     initWithTarget:self.searchBar
                                     action:@selector(resignFirstResponder)]];
    
    
    [self.navigationController.navigationBar addGestureRecognizer:[[UITapGestureRecognizer alloc]
                                                                   initWithTarget:self.searchBar
                                                                   action:@selector(resignFirstResponder)]];
}

#pragma mark - SearchBar delegate 

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    self.goButton.enabled = self.searchBar.text.length > 0;
}

#pragma mark - Segue Methods

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kSegueShowMyTramDetailed]) {
//        [segue.destinationViewController setJourney:sender];
        [segue.destinationViewController setTramNumberString:sender];
    }
}

#pragma mark - Logic

//- (void)callPIDServiceForJourneyForTramNumber:(NSNumber *) tramNumber
//{
//    // Create the service if its not there already
//	if (!self.pidService)
//		self.pidService = [[PidsService alloc] initWithDelegate:self];
//    
//    self.tramNumber = tramNumber;
//	
//    [self.pidService getJourneyForTramNumber:self.tramNumber];
//    
//    self.navigationItem.rightBarButtonItem.enabled = NO;
//    self.timeoutTimer = [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(timeout:) userInfo:nil repeats:NO];
//}

- (IBAction)locateTram:(UIBarButtonItem *)sender
{
	if (!self.searchBar.text.length)
        return ;

    [self.searchBar resignFirstResponder];
    [self performSegueWithIdentifier:kSegueShowMyTramDetailed sender:self.searchBar.text];
}

#pragma mark - PIDsServiceDelegate

//- (void)setJourney:(JourneyStub *)aJourneyStub
//{
//    self.goButton.enabled = YES;
//    
//    /* Clear the timeout timer */
//    [self.timeoutTimer invalidate];
//    self.timeoutTimer = nil;
//    
//    Journey *journey = [Journey journeyForStub:aJourneyStub];
//    
//    // is this a valid result?
//    
//    NSString    * tramNumber = self.searchBar.text;
//    
//    if (!journey.tram)
//    {
//        // alert to say the tram doesn't exist
//        
//        [self.activityView stopAnimating];
//        NSString        * message = [[NSString alloc] initWithFormat:NSLocalizedString(@"onboard-tramnotfound", "Tram Not Found message"), tramNumber];
//        
//        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:nil
//                                               cancelButtonTitle:NSLocalizedString(@"onboard-error-okbutton", @"OK") otherButtonTitles:nil, nil];
//        [alert show];
//        
//        return;
//    }
//    else if ([journey.tram isRouteZero])
//    {
//        [self.activityView stopAnimating];
//
//        // alert to say the tram doesn't exist
//        NSString        * message = [[NSString alloc] initWithFormat:NSLocalizedString(@"onboard-tramreturntodepot", "Tram returning to the depot"), [NSString stringWithFormat:@"Tram %@", tramNumber]];
//        
//        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:nil
//                                               cancelButtonTitle:NSLocalizedString(@"onboard-error-okbutton", @"OK") otherButtonTitles:nil, nil];
//        [alert show];
//        return;
//    }
//
//    // Is this for the wrong tram?
//    if (self.tramNumber && ![journey.tram.number isEqualToNumber:self.tramNumber])
//        return;
//
//    [self.searchBar resignFirstResponder];
//    [self performSegueWithIdentifier:kSegueShowMyTramDetailed sender:aJourneyStub];
//}
//
@end
