//
//  RouteFilterViewController.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 13/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RouteFilterViewController : UITableViewController

- (void)setStop:(Stop *)aStop direction:(BOOL)upDirection;
- (void)updateSelectedRoute:(NSInteger)aSelectedRouteIndex;

@end
