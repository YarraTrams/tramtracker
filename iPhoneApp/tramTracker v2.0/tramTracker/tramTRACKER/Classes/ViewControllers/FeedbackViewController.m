//
//  FeedbackViewController.m
//  tramTRACKER
//
//  Created by Raji on 20/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>
#import "FeedbackViewController.h"
#import "AccordionButton.h"
#import "Analytics.h"

@interface FeedbackViewController () <MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) IBOutlet AccordionButton  * btnHowToTakeScreenshot;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * buttonHeight;

@end

@implementation FeedbackViewController

#pragma mark - Inits & Loads

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.btnHowToTakeScreenshot.text = @"To take a screenshot with your iPhone, just press the Home and Sleep buttons at the same time.\n\nIf you nail the timing, the screen will flash and you'll hear the same camera shutter sound that the Camera app makes.\n\nTo access your screenshots go to Photos and open the Camera Roll.";
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureFeedback];
}

#pragma mark - User Actions

- (IBAction)screenshotClicked:(id)sender
{
    self.buttonHeight.constant = self.buttonHeight.constant == 47.0f ? 220.0f : 47.0f;
    
    [UIView animateWithDuration:0.5f animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (IBAction)phoneClicked:(id)sender
{
    if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://1800800007"]])
        [[[UIAlertView alloc] initWithTitle:@"Unable to Connect Call"
                                    message:@"Please make sure that your device is configured to make calls or check that your SIM card is inserted correctly."
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil]
         show];
}

- (IBAction)emailClicked:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController* mailController = [[MFMailComposeViewController alloc] init];
        mailController.mailComposeDelegate = self;
        [mailController setSubject:@"Re: Feedback on tramTracker® iOS app"];
        [mailController setMessageBody:@"Hello,\n\nMy comments for the tramTracker® iOS app are as follows:\n" isHTML:NO];
        [mailController setToRecipients:[NSArray arrayWithObject:@"feedback@yarratrams.com.au"]];
        
        if (mailController)
            [self presentViewController:mailController animated:YES completion:NULL];
    }
    else    //The device can not send email.
    {
        NSLog(@"Error :- The device is not configured for sending email !");
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Unable to Send Email"
                                                         message:@"Please make sure that your device is configured to send emails."
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark Mail Controller Delegate Methods

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}


@end
