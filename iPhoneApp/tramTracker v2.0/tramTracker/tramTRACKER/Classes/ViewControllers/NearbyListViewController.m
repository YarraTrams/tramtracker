//
//  NearbyViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 20/11/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "NearbyListViewController.h"
#import "SectionHeaderView.h"
#import "NearbyCell.h"
#import "Constants.h"
#import "TTTools.h"
#import "TicketOutletDetailedViewController.h"
#import "NearbyViewController.h"
#import "TTActivityIndicatorView.h"
#import "StopRightFilterViewController.h"
#import "StopList.h"
#import "Settings.h"
#import "Analytics.h"

@interface NearbyListViewController()<CLLocationManagerDelegate>

/*
 * Data
 */
@property (strong, nonatomic) NSArray                   * stopList;
@property (strong, nonatomic) NSArray                   * ticketRetailersList;
@property (strong, nonatomic) NSArray                   * data;
@property (strong, nonatomic) NSArray                   * filterData;

@property (strong, nonatomic) UIRefreshControl          * refreshControl;

@property (nonatomic, weak) NearbyViewController        * nearbyContainer;

/*
 * IBOutlets
 */

@property (weak, nonatomic) IBOutlet UITableView        * tableView;
@property (strong, nonatomic) SectionHeaderView         * sectionHeaderView;

@property (strong, nonatomic) CLLocationManager         * locationManager;
@property (strong, nonatomic) NSDate                    * locatingStarted;
@property (nonatomic, strong) TTActivityIndicatorView   * activity;
@property (nonatomic,strong) NSTimer                    * timer;
@property (strong, nonatomic) NSDate                    * lastSyncDate;
@property (weak, nonatomic) IBOutlet UIView             * headerView;
@property (weak, nonatomic) IBOutlet UILabel            *  headerText;
@property (weak, nonatomic) IBOutlet UIImageView        * headerImage;

@end

@implementation NearbyListViewController

@synthesize filterType = _filterType;

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        self.refreshControl = [UIRefreshControl new];
    }
    return self;
}

#pragma mark - NearbyRightFilterDelegate

- (void)didSelectStopAction:(StopRightMenuType)filterType
{
    BOOL        hasFilterChanged = self.filterType != filterType;
    
    self.sectionHeaderView.headerText.text = [StopRightFilterViewController sectionHeaderTitleForType:filterType];

    self.filterType = filterType;

    if (hasFilterChanged)
        [self refreshLocation];
    else
    {
        self.data = [StopRightFilterViewController filterDistanceArray:self.filterType == StopRightMenuOutlets ? self.ticketRetailersList : self.stopList filterType:self.filterType string:self.nearbyContainer.searchField.text];

        self.headerImage.image = [UIImage imageNamed:@"EmptyResults"];
        self.headerView.hidden = self.data.count > 0;
        self.tableView.hidden = self.data.count == 0;
        [self.tableView reloadData];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.nearbyContainer = (id)self.parentViewController.parentViewController;

    // Refresh control
    UITableViewController *tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = self.tableView;

    tableViewController.refreshControl = self.refreshControl;
    
    [self.refreshControl addTarget:self action:@selector(refreshLocation) forControlEvents:UIControlEventValueChanged];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshLocation) name:kNotificationSyncFinished object:nil];
    
    self.headerText.accessibilityLabel = @"To use the Nearby feature, tramTracker must be able to access your device GPS location. For tramTracker to determine your location, please enable Location Services in your device Settings. To do this, go to Settings, then Privacy and Location Services.";
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self refreshLocation];

    if (self.filterType == StopRightMenuOutlets)
        [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureNearbyList withFilter:AnalyticsFilterOutlets];
    else if (self.filterType == StopRightMenuAll)
        [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureNearbyList withFilter:AnalyticsFilterAll];
    else if (self.filterType == StopRightMenuAccess)
        [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureNearbyList withFilter:AnalyticsFilterEasyAccess];
    else if (self.filterType == StopRightMenuShelter)
        [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureNearbyList withFilter:AnalyticsFilterShelter];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.locationManager stopUpdatingLocation];
    [self.timer invalidate];
}

- (SectionHeaderView *)sectionHeaderView
{
    if (!_sectionHeaderView)
        _sectionHeaderView = [SectionHeaderView nearbySectionHeaderWithTitle:NSLocalizedString(@"nearby-header-all-stops", @"All Stops")];
    return _sectionHeaderView;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * cellIdentifier = @"NearbyCell";
    NearbyCell      * cell = (NearbyCell *)[self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (self.filterType == StopRightMenuOutlets)
    {
        TicketRetailerDistance  * ticketRetailerDistance = self.data[indexPath.row];
        [cell configureWithTicketRetailerDistance:ticketRetailerDistance];
    }
    else
    {
        StopDistance            * stopDistance = self.data[indexPath.row];
        [cell configureWithStopDistance:stopDistance];
    }
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return self.sectionHeaderView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 36.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80.0f;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if (self.filterType != StopRightMenuOutlets)
    {
        StopDistance            * stopDistance = self.data[indexPath.row];
        
        NearbyViewController * vc = (id)self.parentViewController.parentViewController;
        [vc pushToPID:stopDistance.stop];
    }
    else
    {
        UIStoryboard                        * sb = [UIStoryboard storyboardWithName:kMainStoryboard bundle:nil];
        TicketOutletDetailedViewController  * vc = [sb instantiateViewControllerWithIdentifier:kScreenTicketDetailed];
        TicketRetailerDistance              * ticketOutletDistance = self.data[indexPath.row];

        [vc setCurRetailer:ticketOutletDistance.retailer];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - Logic

- (void)refreshLocation
{
	if ([CLLocationManager locationServicesEnabled] &&
        [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized)
	{
		[self.locationManager startUpdatingLocation];

        if (!self.data.count && !self.activity)
            self.activity = [TTActivityIndicatorView activityIndicatorForView:self.view animated:YES];

        if (!self.timer.isValid || self.timer.timeInterval != 10)
        {
            [self.timer invalidate];
            self.timer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(refreshLocation) userInfo:nil repeats:YES];
        }

		if (![self.refreshControl isRefreshing])
			[self.refreshControl beginRefreshing];

        /* Set start location date */
        self.locatingStarted = [NSDate date];
        self.lastSyncDate = [NSDate date];
	}
    else
    {
        if (!self.timer.isValid || self.timer.timeInterval != 1)
        {
            [self.timer invalidate];
            self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(refreshLocation) userInfo:nil repeats:YES];
        }

        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined)
        {
            if (!self.activity)
                self.activity = [TTActivityIndicatorView activityIndicatorForView:self.view animated:NO];
            else
                [self.activity startAnimatingAnimated:NO];
        }
        else
            [self locationManager:self.locationManager didFailWithError:nil];
    }
}

- (void)displayStopsWithLocation:(CLLocation *)location
{
	if ([self.refreshControl isRefreshing])
		[self.refreshControl endRefreshing];
    
    if (!self.nearbyContainer.searchField.isFirstResponder)
    {
        self.nearbyContainer.navigationItem.leftBarButtonItem.enabled = YES;
        self.nearbyContainer.navigationItem.rightBarButtonItem.enabled = YES;
    }

    Settings *settings = [Settings settings];
    
	/* Find the nearest stops */
	self.stopList = [[StopList sharedManager] getNearestStopsToLocation:location count:[settings.showNearbyStops intValue] shelter:self.filterType == StopRightMenuShelter lowFloor:self.filterType == StopRightMenuAccess];

    /* Find the nearest retailers */
    self.ticketRetailersList = [TicketRetailer nearestTicketRetailersToLocation:location count:[settings.showNearbyStops intValue]];
    
    self.data = [StopRightFilterViewController filterDistanceArray:self.filterType == StopRightMenuOutlets ? self.ticketRetailersList : self.stopList filterType:self.filterType string:self.nearbyContainer.searchField.text];
    
    self.headerImage.image = [UIImage imageNamed:@"EmptyResults"];
    self.headerView.hidden = self.data.count > 0;
    self.tableView.hidden = self.data.count == 0;
//    self.headerView.hidden = YES;
//    self.tableView.hidden = NO;

    [self.tableView reloadData];

    __weak NearbyListViewController * weakSelf = self;

    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(NSEC_PER_SEC / 2));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [weakSelf.activity stopAnimatingAnimated:YES];
    });
}

#pragma mark - CLLocationManager Delegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
	CLLocation *newLocation = [locations lastObject];

    [self.activity stopAnimatingAnimated:YES];

	// The location manager might have a more recent location
	if ([manager.location.timestamp isEqualToDate:[manager.location.timestamp laterDate:newLocation.timestamp]])
		newLocation = manager.location;

	// Is it more than 2 minutes old?
	if (fabs([newLocation.timestamp timeIntervalSinceNow]) > 120)
		return;
	
    [self displayStopsWithLocation:newLocation];
    [self.locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
	if ([self.refreshControl isRefreshing])
		[self.refreshControl endRefreshing];
    [self.activity stopAnimating];
    
    self.nearbyContainer.navigationItem.leftBarButtonItem.enabled = YES;
    self.nearbyContainer.navigationItem.rightBarButtonItem.enabled = YES;

    self.data = nil;
    self.headerImage.image = [UIImage imageNamed:@"Fav_text"];
    self.headerView.hidden = NO;
    self.tableView.hidden = YES;
}

- (void)_turnOnLocationManager
{
    [self.locationManager startUpdatingLocation];
}


@end
