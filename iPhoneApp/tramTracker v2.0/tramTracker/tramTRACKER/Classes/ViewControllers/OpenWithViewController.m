//
//  OpenWithViewController.m
//  tramTRACKER
//
//  Created by Raji on 20/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "OpenWithViewController.h"
#import "Constants.h"
#import "Settings.h"

@interface OpenWithViewController ()

@property (strong, nonatomic) NSArray * stringsToLoad;
@property (nonatomic, assign) NSString * selectedString;
@property (nonatomic, assign) SettingsOptionSelectionMode currentMode;
@property (nonatomic, strong) NSIndexPath * initialSelectedIndex;
@property (nonatomic, assign) BOOL didSelectionChange;

extern NSString * const kNotificationSelection;

@end

@implementation OpenWithViewController

#pragma mark - Inits & Loads

- (void)setCurrentMode:(SettingsOptionSelectionMode)currentMode andSelectedString:(NSString*)selectedString
{
    self.didSelectionChange = NO;
    self.currentMode = currentMode;
    self.selectedString = selectedString;
    
    switch (self.currentMode)
    {
        case OpenWithMode:
        {
            self.stringsToLoad = @[NSLocalizedString(@"settings-startup-favouritelist", nil),
                                   NSLocalizedString(@"settings-startup-favouritemap", nil),
                                   NSLocalizedString(@"settings-startup-nearbylist", nil),
                                   NSLocalizedString(@"settings-startup-nearbymap", nil),
                                   @"Nearest Favourite"];
            self.navigationItem.title = NSLocalizedString(@"settings-startup-openwith", nil);
            break;
        }
        case SynchroniseMode:
        {
            self.stringsToLoad = @[NSLocalizedString(@"settings-sync-type-semi-auto-full", nil),
                                   NSLocalizedString(@"settings-sync-type-manual", nil)];
            self.navigationItem.title = NSLocalizedString(@"settings-sync-type", nil);
            break;
        }
        case ShowNearbyStopsMode:
        {
            self.stringsToLoad = @[@"5 Stops",
                                   @"10 Stops",
                                   @"20 Stops",
                                   @"30 Stops",
                                   @"40 Stops",
                                   @"50 Stops",
                                   @"75 Stops",
                                   @"100 Stops"];
            self.navigationItem.title = NSLocalizedString(@"settings-displayopts-nearbycount", nil);
            break;
        }
        case ShowMostRecentMode:
        {
            self.stringsToLoad = @[@"5 Stops",
                                   @"10 Stops",
                                   @"20 Stops",
                                   @"30 Stops",
                                   @"40 Stops",
                                   @"50 Stops",
                                   @"75 Stops",
                                   @"100 Stops"];
            self.navigationItem.title = NSLocalizedString(@"settings-displayopts-recentcount", nil);
            break;
        }
        default:
            break;
    }
    for (int i = 0; i < self.stringsToLoad.count; i++)
    {
        if ([self.stringsToLoad[i] isEqualToString:self.selectedString])
        {
            self.initialSelectedIndex = [NSIndexPath indexPathForRow:i inSection:0];
            break;
        }
    }
}

#pragma mark - User Actions on Nav Bar

- (IBAction)backAction:(id)sender
{
    if (self.didSelectionChange)
    {
        NSArray *arrayToSend = [NSArray arrayWithObjects:[NSNumber numberWithInt:self.currentMode], self.selectedString, nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSelection object:arrayToSend];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Utility Methods

-(void)saveNewSelection
{
    Settings *settings = [Settings settings];
    switch (self.currentMode)
    {
        case OpenWithMode:
            settings.openWith = self.selectedString;
            break;
        case SynchroniseMode:
            if ([self.selectedString isEqualToString:NSLocalizedString(@"settings-sync-type-semi-auto-full", nil)])
                settings.synchronise = [NSNumber numberWithBool:YES];
            else
                settings.synchronise = [NSNumber numberWithBool:NO];
            break;
        case ShowNearbyStopsMode:
        {
            NSMutableString *strTemp = [NSMutableString stringWithString:self.selectedString];
            strTemp = [[strTemp stringByReplacingOccurrencesOfString:@" Stops" withString:@""] mutableCopy];
            NSNumber *numValue = [NSNumber numberWithInt:[strTemp intValue]];
            settings.showNearbyStops = numValue;
            break;
        }
        case ShowMostRecentMode:
        {
            NSMutableString *strTemp = [NSMutableString stringWithString:self.selectedString];
            strTemp = [[strTemp stringByReplacingOccurrencesOfString:@" Stops" withString:@""] mutableCopy];
            NSNumber *numValue = [NSNumber numberWithInt:[strTemp intValue]];
            settings.showMostRecent = numValue;
            break;
        }
        default:
            break;
    }
    [(tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.stringsToLoad.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *const CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    NSString *stringToDisplay = self.stringsToLoad[indexPath.row];

    cell.textLabel.text = stringToDisplay;
    
     BOOL isSelected = [stringToDisplay isEqualToString:self.selectedString];
     if (isSelected)
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Tick.png"]];
    else
        cell.accessoryView = nil;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell  * cell = (id)[tableView cellForRowAtIndexPath:indexPath];

    self.selectedString = cell.textLabel.text;

    [self.tableView reloadData];

    self.didSelectionChange = YES;

    [self saveNewSelection];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell  * cell = (id)[tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryView = nil;
}

@end
