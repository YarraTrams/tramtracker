//
//  myTramContainerViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 29/11/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIAlertView+Blocks/UIAlertView+Blocks.h>

#import "myTramViewController.h"
#import "PIDViewController.h"
#import "Constants.h"
#import "TTTabBarController.h"
#import "myTramListViewController.h"
#import "TTActivityIndicatorView.h"
#import "PidsService.h"

@interface myTramViewController ()

@property (strong, nonatomic) NSTimer                           * refreshTimer;
@property (strong, nonatomic) PidsService                       * pidService;
@property (strong, nonatomic) Journey                           * finalJourney;

@property (weak, nonatomic) IBOutlet UILabel                    * tramTitle;
@property (weak, nonatomic) IBOutlet UILabel                    * tramSubtitle;
@property (weak, nonatomic) IBOutlet UIButton                   * refreshButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView    * refreshIndicator;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (strong, nonatomic) NSString * tramNo;

@property (strong, nonatomic) TTActivityIndicatorView   * activityView;

@property (assign, nonatomic, getter = isFirstShow) BOOL firstShow;

@property (assign, nonatomic, getter = isUpdating) BOOL updating;
@property (assign, nonatomic, getter = hasAlreadySucceeeded) BOOL alreadySucceeed;

@property (strong, nonatomic) UIView * errorView;

@property (strong, nonatomic) UIAlertView * errorAlertView;

@end

static const NSInteger kMyTramRefreshInterval = 15;

@implementation myTramViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.firstShow = YES;
    }
    return self;
}

#pragma mark - User Actions on Nav Bar

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Inits & Loads

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.accuracyLevel = 100;
    [self updateChildrenWithJourney:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.isFirstShow) {
        self.activityView = [TTActivityIndicatorView activityIndicatorForView:self.view animated:NO];
        self.firstShow = NO;
    }
    
    self.updating = YES;
    self.pidService = [[PidsService alloc] initWithDelegate:self];
    [self refreshPredictions];
    
	self.refreshTimer = [NSTimer scheduledTimerWithTimeInterval:kMyTramRefreshInterval
                                                         target:self
                                                       selector:@selector(refreshPredictions)
                                                       userInfo:nil
                                                        repeats:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
	[[UIApplication sharedApplication] setIdleTimerDisabled:YES];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self stopUpdating];
	[[UIApplication sharedApplication] setIdleTimerDisabled:NO];
}

- (void)dealloc
{
    [self.pidService cancel];
    [self.pidService setDelegate:nil];
}

#pragma mark - Segue Methods

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kSegueShowPID])
        [segue.destinationViewController setCurrentStop:sender];
}

#pragma mark - Utility Methods

- (void)updateChildrenWithJourney:(Journey *)finalJourney
{
    Journey * journeyToUse = !finalJourney ? self.finalJourney : finalJourney;
    
    if (!journeyToUse.tram.route && journeyToUse.stops.lastObject)
    {
        JourneyStop * jstop = journeyToUse.stops.lastObject;
        self.tramTitle.text = [NSString stringWithFormat:@"Route %@ To %@", journeyToUse.tram.headboardRouteNumber, jstop.stop.name];
    }
    else
        self.tramTitle.text = [journeyToUse.tram.route routeNameWithNumberAndDestinationUp:journeyToUse.tram.upDirection];
    self.tramSubtitle.text = journeyToUse.tram.name;
    
    [self.map setFinalJourney:journeyToUse];
    [self.list setFinalJourney:journeyToUse];
}

- (IBAction)toggleListMapAction:(UIBarButtonItem *)sender
{
    TTTabBarController  * tabbarController = self.childViewControllers.firstObject;
    id                  newViewController;
    
    if (tabbarController.selectedViewController == tabbarController.childViewControllers.firstObject)
        newViewController = tabbarController.childViewControllers.lastObject;
    else
        newViewController = tabbarController.childViewControllers.firstObject;
    
    [newViewController setFinalJourney:self.finalJourney];
    [tabbarController toggleListMapAction:sender isRight:YES];
}

- (void)stopUpdating
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    self.updating = NO;
    
    [self.pidService cancel];
    self.pidService = nil;
    [self.refreshTimer invalidate];
    self.refreshTimer = nil;
}

- (IBAction)buttonRefresh
{
    self.hasButtonBeenPressed = YES;
    [self refreshPredictions];
}

- (void)refreshPredictions
{
    if (self.tramNo)
    {
        self.refreshButton.hidden = YES;
        self.refreshIndicator.hidden = NO;
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        [self.pidService getJourneyForTramNumber:@(self.tramNo.integerValue)];
    }
}

#pragma mark - SetCurrentJourneyDelegate

- (void)setTramNumberString:(NSString *)aTramNo {
    self.tramNo = aTramNo;
}

- (void)callPIDServiceForJourneyForTramNumber:(NSNumber *) tramNumber
{
    // Create the service if its not there already
	if (!self.pidService)
		self.pidService = [[PidsService alloc] initWithDelegate:self];
    
    [self.pidService getJourneyForTramNumber:tramNumber];
    
    self.navigationItem.rightBarButtonItem.enabled = NO;
}

- (void)setJourney:(JourneyStub *)journeyStub
{
    Journey *journey = [Journey journeyForStub:journeyStub];
    
    [self.activityView stopAnimatingAnimated:YES];
    self.refreshButton.hidden = NO;
    self.refreshIndicator.hidden = YES;
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    if (!journey.tram)
    {
        // alert to say the tram doesn't exist
        
        [self.refreshTimer invalidate];
        [self.activityView stopAnimating];
        NSString        * message = [[NSString alloc] initWithFormat:NSLocalizedString(@"onboard-tramnotfound", "Tram Not Found message"), self.tramNo];
        
        [self setErrorMessage:message];
        [self stopUpdating];
        return;
    }
    else if ([journey.tram isRouteZero])
    {
        [self.refreshTimer invalidate];
        [self.activityView stopAnimating];
        
        // alert to say the tram doesn't exist
        NSString        * message = [[NSString alloc] initWithFormat:NSLocalizedString(@"onboard-tramreturntodepot", "Tram returning to the depot"), [NSString stringWithFormat:@"Tram %@", self.tramNo]];
        [self setErrorMessage:message];
        [self stopUpdating];
        return;
    }
    
    if (!journeyStub.tramStub.number)
        return;
    
    self.alreadySucceeed = YES;
    
    if (journeyStub != _journey)
    {
        [self setErrorMessage:nil];
        _journey = journeyStub;
        
        if (!self.finalJourney)
            self.atLayover = journey.tram.isAtLayover;
        
        // have we just changed into layover?
        if (self.finalJourney != nil && ![self isAtLayover] && [journey.tram isAtLayover] && ![self.finalJourney.tram isAtLayover])
            [self setAtLayover:YES];
        
        [self updateChildrenWithJourney:journey];
        self.finalJourney = journey;
    }
}

- (void)pidsServiceDidFailWithError:(NSError *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    NSString        * message = @"Unable to contact the tramTRACKER service. Please make sure that your device is connected to the internet and try again.";

    if (!self.hasAlreadySucceeeded) {
        [self.activityView stopAnimating];
        self.refreshButton.hidden = NO;
        self.refreshIndicator.hidden = YES;
        
        [self setErrorMessage:message];
    } else if (!self.errorAlertView) {
        self.errorAlertView = [UIAlertView showWithTitle:@"Error"
                                                 message:message
                                       cancelButtonTitle:@"Ok"
                                       otherButtonTitles:nil tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                           self.errorAlertView = nil;
                                       }];
    }
}

- (BOOL)isSelectedCell:(Stop *)stop withJourneyStop:(JourneyStop *)jstop
{
    BOOL isHighlightableStop = NO;
	BOOL isCBDStop = (self.finalJourney.stops.firstObject && [[(JourneyStop *)[self.finalJourney.stops objectAtIndex:0] stop] isCityStop]);
	BOOL isLowAccuracyStop =  isCBDStop || self.accuracyLevel > 3;
    
    if (self.isStopped || self.isAtLayover)
        isHighlightableStop = NO;
    else if (!stop.isCityStop && self.accuracyLevel <= 3)
        isHighlightableStop = [stop isEqual:self.list.nextStop];
    else if (isLowAccuracyStop && self.list.nextStop != nil)
    {
        // in the city (or with bad GPS) we highlight stops with a predicted arrival <= 1.5 minutes
        isHighlightableStop = ([stop isEqual:self.list.nextStop] || [jstop.predicatedArrivalDateTime timeIntervalSinceNow] <= (isCBDStop ? 150 : 90));
        
        // are we more than 2 away from the next stop?
        if (isHighlightableStop)
        {
            NSInteger indexOfNextStop = self.list.nextStop == nil ? 0 : [self.list.stopList indexOfObject:self.list.nextStop];
            NSInteger indexOfCurrentStop = [self.list.stopList indexOfObject:stop];
            
            if (indexOfNextStop != NSNotFound && indexOfCurrentStop != NSNotFound && indexOfNextStop+2 <= indexOfCurrentStop)
                isHighlightableStop = NO;
        }
        
        // make sure that the previously highlighted stops list doens't disagree
        if ([self.list.previouslyHighlightedStops indexOfObject:stop.trackerID] == NSNotFound)
        {
            // not found in the list. if its to be highlighted add it to the list
            if (isHighlightableStop)
                [self.list.previouslyHighlightedStops addObject:stop.trackerID];
            
        } else {
            // it is in the previously highlighted list, for consistency we dont change our mind about highlights
            // (if its highlighted, it *stays* highlighted)
            if (!isHighlightableStop)
                isHighlightableStop = YES;
        }
    }
    return isHighlightableStop;
}

- (void)setErrorMessage:(NSString *)message {
    if (message) {
        
        [self.errorView removeFromSuperview];
        
        self.errorView = [[UIView alloc] initWithFrame:self.containerView.frame];
        self.errorView.backgroundColor = [UIColor whiteColor];
        
        UITextView * textView = [[UITextView alloc] initWithFrame:CGRectMake(10, 0, self.containerView.frame.size.width - 20, self.containerView.frame.size.height - 20)];
        
        textView.text = message;
        textView.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:14];
        textView.userInteractionEnabled = NO;
        
        [self.errorView addSubview:textView];
        [self.view addSubview:self.errorView];
    } else {
        [self.errorView removeFromSuperview];
        self.errorView = nil;
    }
    
    self.navigationItem.rightBarButtonItem.enabled = message == nil;
}

- (BOOL)hasTramChangedRoute:(Tram *)tram
{
	// if the route itself has changed, then yes
	if (![tram.route isEqual:self.finalJourney.tram.route])
		return YES;
    
	// if the headboard details have changed, then yes
	if (![tram.headboardRouteNumber isEqualToString:self.finalJourney.tram.headboardRouteNumber])
		return YES;
    
	// if the direction has changed, then yes
	if (tram.upDirection != self.finalJourney.tram.upDirection)
		return YES;
    
	// otherwise no
	return NO;
}

- (MapViewController *)map
{
    TTTabBarController * tab = self.childViewControllers.lastObject;
    return tab.childViewControllers.lastObject;
}

- (myTramListViewController *)list
{
    TTTabBarController * tab = self.childViewControllers.lastObject;
    return tab.childViewControllers.firstObject;
}

@end
