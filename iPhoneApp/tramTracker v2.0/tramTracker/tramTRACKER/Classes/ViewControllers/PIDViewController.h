//
//  PIDViewController.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 21/11/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Stop.h"
#import "Filter.h"

@interface PIDViewController : UIViewController

@property (strong, nonatomic) Stop      * currentStop;
@property (strong, nonatomic) Filter    * filter;

- (void)updateFilter:(Filter *)aFilter;
- (void)setPredictions:(NSArray *)predictions;

@end
