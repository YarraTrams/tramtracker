//
//  SearchResultContainerViewController.h
//  tramTracker
//
//  Created by Hugo Cuvillier on 21/02/2014.
//  Copyright (c) 2014 AppsCore. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultContainerViewController : UIViewController

- (void)setLists:(NSArray *)lists;
- (void)setBooleanForEasyAccessStops:(BOOL)hasEasyAccess andShelter:(BOOL)hasShelter;

@end
