//
//  SettingsViewController.m
//  tramTRACKER
//
//  Created by Raji on 19/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "SettingsViewController.h"
#import "OpenWithViewController.h"
#import "Settings.h"
#import "Constants.h"
#import "Analytics.h"
#import "SectionHeaderView.h"

@interface SettingsViewController ()

@property (strong, nonatomic) IBOutlet UILabel *lblOpenWith;
@property (strong, nonatomic) IBOutlet UILabel *lblSynchronise;
@property (strong, nonatomic) IBOutlet UILabel *lblShowNearbyStop;
@property (strong, nonatomic) IBOutlet UILabel *lblShowMostRecent;
@property (strong, nonatomic) Settings * settings;
@property (weak, nonatomic) IBOutlet UISwitch *analytics;
@property (weak, nonatomic) IBOutlet UISwitch *prod;

@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *RightConstrain;


@end

@implementation SettingsViewController

NSString * const kNotificationSelection = @"notificationSelection";

#pragma mark - Inits & Loads

-(void) viewDidLoad
{
    [super viewDidLoad];
    
    self.settings = [Settings settings];
    
    self.lblOpenWith.text = self.settings.openWith;
    if ([self.settings.synchronise boolValue])
        self.lblSynchronise.text = @"Check";    //Automatically
    else
        self.lblSynchronise.text = @"Manually";
    self.lblShowNearbyStop.text = [NSString stringWithFormat:@"%d Stops", [self.settings.showNearbyStops intValue]];
    self.lblShowMostRecent.text = [NSString stringWithFormat:@"%d Stops", [self.settings.showMostRecent intValue]];
    
    //Notification sent from OpenWith, when returning from OpenWith, to update the item selected
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotificationSelection:) name:kNotificationSelection object:nil];

    if (!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {

        for (NSLayoutConstraint * curConstrain in self.RightConstrain)
            curConstrain.constant = 12;
        [self.view layoutIfNeeded];
    }

    self.prod.on = ![[NSUserDefaults standardUserDefaults] boolForKey:@"devAPI"];
    self.analytics.on = [[NSUserDefaults standardUserDefaults] boolForKey:kAnalyticsAllowAnalyticsKey];
}

- (IBAction)prodChanged:(UISwitch *)sender {
    [[NSUserDefaults standardUserDefaults] setBool:!sender.isOn forKey:@"devAPI"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)switchAnalytics:(UISwitch *)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:sender.isOn forKey:kAnalyticsAllowAnalyticsKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureSettings];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - User Actions

#pragma mark - Segue Methods

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if (![[segue identifier] isEqualToString:kSegueUnwindToMore])
    {
        SettingsOptionSelectionMode currentMode = OpenWithMode;
        NSString *stringToSend = self.lblOpenWith.text;
        
        if ([[segue identifier] isEqualToString:kSegueSynchronise])
        {
            currentMode = SynchroniseMode;
            stringToSend = self.lblSynchronise.text;
            if ([stringToSend isEqualToString:NSLocalizedString(@"settings-sync-type-semi-auto", nil)])
                stringToSend = NSLocalizedString(@"settings-sync-type-semi-auto-full", nil);
        }
        if ([[segue identifier] isEqualToString:kSegueShowNearbyStops])
        {
            currentMode = ShowNearbyStopsMode;
            stringToSend = self.lblShowNearbyStop.text;
        }
        if ([[segue identifier] isEqualToString:kSegueShowMostRecent])
        {
            currentMode = ShowMostRecentMode;
            stringToSend = self.lblShowMostRecent.text;
        }
        
        OpenWithViewController *vc = [segue destinationViewController];
        [vc setCurrentMode:currentMode andSelectedString:stringToSend];
    }
}

#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 36.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [SectionHeaderView staticSectionHeaderWithTitle:@[@"Start up", @"Data updates", @"Display options", @"Data analytics", @"API"][section]];
}

#pragma mark - Notification Handlers

- (void)didReceiveNotificationSelection:(NSNotification *)notification
{
    NSArray *arrValues = notification.object;
    
    int mode = [arrValues[0] intValue];
    NSString *stringToDisplay = arrValues[1];
    
    switch (mode)
    {
        case 0:
            self.lblOpenWith.text = stringToDisplay;
           break;
        case 1:
            if ([stringToDisplay isEqualToString:NSLocalizedString(@"settings-sync-type-semi-auto-full", nil)])
                stringToDisplay = NSLocalizedString(@"settings-sync-type-semi-auto", nil);
            self.lblSynchronise.text = stringToDisplay;
            break;
        case 2:
        {
            self.lblShowNearbyStop.text = stringToDisplay;
            break;
        }
        case 3:
            self.lblShowMostRecent.text = stringToDisplay;
            break;
        default:
            break;
    }
}

@end
