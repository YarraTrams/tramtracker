//
//  HelpViewController.m
//  tramTRACKER
//
//  Created by Raji on 23/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <FrameAccessor/FrameAccessor.h>

#import "HelpViewController.h"
#import "HelpWebViewController.h"
#import "Analytics.h"

@interface HelpViewController ()

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation HelpViewController

#pragma mark - Inits & Loads

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Help";

    UIBarButtonItem * back = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Nav-Back"]
                                                              style:UIBarButtonItemStylePlain
                                                             target:self
                                                             action:@selector(backAction:)];

    [self.navigationItem setLeftBarButtonItem:back];
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        self.tableView.y = 0 ;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureHelp];
}

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if (![[segue.destinationViewController class] isSubclassOfClass:[HelpWebViewController class]]) {
        return;
    }
    
    
    NSString * filename = @"";
    NSIndexPath * indexPath;
    
    if([sender isKindOfClass:[UITableViewCell class]]) {
        indexPath = [self.tableView indexPathForCell:sender];
        //Your code here
    }
    
    
    HelpWebViewController * webView = segue.destinationViewController;

    if ([self.controller isEqualToString:@"base"]) {
        
        switch (indexPath.row) {
            case 0:
                filename = @"";
                break;
        }
        
    }else if([self.controller isEqualToString:@"passenger"]){
        switch (indexPath.row) {
            case 0: filename = @"understanding13.html"; break;
            case 1: filename = @"messages15.html"; break;
            case 2: filename = @"menu14.html"; break;
            case 3: filename = @"filter.html"; break;
        }
    }else if([self.controller isEqualToString:@"findStops"]){
        switch (indexPath.row) {
            case 0: filename = @"browse15.html"; break;
            case 1: filename = @"nearby15.html"; break;
            case 2: filename = @"search15.html"; break;
            case 3: filename = @"most_recent15.html"; break;
            case 4: filename = @"enter_id15.html"; break;
        }
    }else if([self.controller isEqualToString:@"favourites"]){
        switch (indexPath.row) {
            case 0: filename = @"adding15.html"; break;
            case 1: filename = @"viewing15.html"; break;
            case 2: filename = @"nearest.html"; break;
            case 3: filename = @"filters.html"; break;
            case 4: filename = @"editing12.html"; break;
            case 5: filename = @"groups.html"; break;
        }
    }else if([self.controller isEqualToString:@"onBoard"]){
        switch (indexPath.row) {
            case 0: filename = @"finding.html"; break;
            case 1: filename = @"understanding15.html"; break;
            case 2: filename = @"special_situations.html"; break;
            case 3: filename = @"low_accuracy.html"; break;
            case 4: filename = @"works.html"; break;
            case 5: filename = @"messages.html"; break;
            case 6: filename = @"connections15.html"; break;
        }
    }else if([self.controller isEqualToString:@"map"]){
        switch (indexPath.row) {
            case 0: filename = @"switching14.html"; break;
            case 1: filename = @"using15.html"; break;
            case 2: filename = @"directions.html"; break;
            case 3: filename = @"pannable14.html"; break;
            case 4: filename = @"ticketoutlets15.html"; break;

        }
    }else if([self.controller isEqualToString:@"scheduled"]){
        switch (indexPath.row) {
            case 0: filename = @"accessing15.html"; break;
            case 1: filename = @"route.html"; break;
            case 2: filename = @"date.html"; break;
            case 3: filename = @"connections.html"; break;
                
        }
    }else if([self.controller isEqualToString:@"ticket"]){
        switch (indexPath.row) {
            case 0: filename = @"browse.html"; break;
            case 1: filename = @"ticketoutlets15.html"; break;
            case 2: filename = @"nearstops.html"; break;
            case 3: filename = @"details.html"; break;
        }
    }else if([self.controller isEqualToString:@"settings"]){
        switch (indexPath.row) {
            case 0: filename = @"accessing16.html"; break;
            case 1: filename = @"startup12.html"; break;
            case 2: filename = @"display_options.html"; break;
            case 3: filename = @"sync15.html"; break;
                
        }
    }else if([self.controller isEqualToString:@"dataUpdates"]){
        switch (indexPath.row) {
            case 0: filename = @"process14.html"; break;
            case 1: filename = @"sync15.html"; break;
        }
    }else if([self.controller isEqualToString:@"dataUsage"]){
        switch (indexPath.row) {
            case 0: filename = @"pid.html"; break;
            case 1: filename = @"schedules.html"; break;
            case 2: filename = @"onboard.html"; break;
            case 3: filename = @"sync.html"; break;
                
        }
    }

    [webView setFilename:filename];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];

}



@end
