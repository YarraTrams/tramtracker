//
//  FavouriteGroupsViewController.m
//  tramTRACKER
//
//  Created by Raji on 4/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "FavouriteGroupsViewController.h"
#import "FavouriteGroupCell.h"
#import "UIAlertView+Blocks.h"
#import "tramTRACKERAppDelegate.h"
#import "StopList.h"

@interface FavouriteGroupsViewController ()

@property (assign, nonatomic) NSInteger currentSection;

@end

@implementation FavouriteGroupsViewController

NSString * const kNotificationGroupSelected = @"notificationGroupSelected";

#pragma mark - User Actions on Nav Bar

-(IBAction)backAction:(id)sender
{
    if (self.currentSection >= 0)
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationGroupSelected object:@(self.currentSection)];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)addGroupAction:(id)sender
{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Create New Group"
                                                     message:@"Please enter a group name."
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];

    [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [[alert textFieldAtIndex:0] setAutocapitalizationType:UITextAutocapitalizationTypeSentences];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex)
    {
        NSInteger   newSectionIndex = [[StopList sharedManager] sectionNamesForFavourites].count;

        [[StopList sharedManager] addFavouriteSection:[alertView textFieldAtIndex:0].text];

        self.currentSection = newSectionIndex;
        [self.tableView reloadData];
    }
}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    NSString *inputText = [[alertView textFieldAtIndex:0] text];
    return [inputText length] > 0;
}

#pragma mark - Inits & Loads

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.currentSection >= 0 && [[StopList sharedManager] sectionNamesForFavourites].count > 0)
    {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:self.currentSection inSection:0]
                                    animated:YES
                              scrollPosition:UITableViewScrollPositionNone];
    }
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[StopList sharedManager] sectionNamesForFavourites].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString     * CellIdentifier = @"GroupCell";
    FavouriteGroupCell  * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    NSString            * sectionName = [[StopList sharedManager] sectionNamesForFavourites][indexPath.row];

    [cell configureWithString:sectionName isSelected:self.currentSection == indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.currentSection = indexPath.row;
    [self.tableView reloadData];
    [self backAction:nil];
}

@end
