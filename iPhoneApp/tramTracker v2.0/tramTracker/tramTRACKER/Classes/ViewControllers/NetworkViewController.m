 //
//  NetworkViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 17/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "NetworkViewController.h"
#import "tramTRACKERAppDelegate.h"
#import "Analytics.h"

@interface NetworkViewController ()<UIWebViewDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation NetworkViewController

static NSString    * kUserKeyTimeMap = @"timeMapDownloaded";

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.webView.scalesPageToFit = YES;
    
    NSDate * lastDate = [[NSUserDefaults standardUserDefaults] objectForKey:kUserKeyTimeMap];

    if (![self isMapPresent] || !lastDate || [[NSDate date] timeIntervalSinceDate:lastDate] > 5184000) // Duration of 60 days
        [self refreshAction:nil];
    [self loadMap];
}

- (IBAction)refreshAction:(id)sender
{
    __weak NetworkViewController * weakSelf = self;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        NSURLRequest    * request = [NSURLRequest requestWithURL:[NSURL URLWithString: @"http://ws2.tramtracker.com.au/TTMetaContent/assets/map.gif"]];
        NSURLResponse   * response = nil;
        NSError         * error = nil;
        NSData          * data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];

        if (!error && data)
        {
            [data writeToFile:[weakSelf pathStringOfMap] atomically:NO];
            [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:kUserKeyTimeMap];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf loadMap];
        });
    });
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureNetwork];
}

- (BOOL)isMapPresent
{
    NSString    *path = [self pathStringOfMap];
    
    return [[NSFileManager defaultManager] fileExistsAtPath:path];
}

- (NSString *)pathStringOfMap
{
    tramTRACKERAppDelegate * d = [[UIApplication sharedApplication] delegate];
    return [d.applicationDocumentsDirectory stringByAppendingPathComponent:@"map.png"];
}

- (void)loadMap
{
    NSString *imgHTMLTag = [NSString stringWithFormat:@"<img src=\"file://%@\" />", [self pathStringOfMap]];

    [self.webView loadHTMLString:imgHTMLTag baseURL:nil];
}

#pragma mark - User Actions on Nav Bar

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
