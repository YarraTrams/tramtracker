//
//  HelpWebViewController.m
//  tramTRACKER
//
//  Created by Alex Louey on 16/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import "HelpWebViewController.h"
#import "UIAlertView+Blocks.h"

@interface HelpWebViewController ()<UIWebViewDelegate>
{
    NSString * baseUrl;
}

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@end

@implementation HelpWebViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"Help";

	// Do any additional setup after loading the view.
    baseUrl = @"http://ws2.tramtracker.com.au/TTMetaContent/IOSHelp/";

    NSString * urlStr = [NSString stringWithFormat:@"%@%@", baseUrl,self.filename];
    NSURLRequest * request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];

    self.webView.delegate = self;
    [self.webView loadRequest:request];

    UIBarButtonItem * back = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Nav-Back"]
                                                              style:UIBarButtonItemStylePlain
                                                             target:self
                                                             action:@selector(backAction:)];

    [self.navigationItem setLeftBarButtonItem:back];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    __weak HelpWebViewController    * weakSelf = self;
    
    [[UIAlertView showWithTitle:@"Unable to load content"
                        message:@"Please make sure that your device is connected to the internet and try again."
              cancelButtonTitle:@"OK"
              otherButtonTitles:nil tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                  [weakSelf.navigationController popViewControllerAnimated:YES];
              }]
     show];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{

    [self.indicator startAnimating];
    self.indicator.hidden = NO;
    
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [self.indicator stopAnimating];
    self.indicator.hidden = YES;

}

@end
