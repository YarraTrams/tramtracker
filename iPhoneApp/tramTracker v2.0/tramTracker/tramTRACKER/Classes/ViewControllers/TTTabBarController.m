//
//  TTTabBarController.m
//  tramTRACKER
//
//  Created by Apscore Mac Mini 3 on 9/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import "TTTabBarController.h"

@interface TTTabBarController ()

@end

@implementation TTTabBarController

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
        {
            for (UIView *view in self.view.subviews)
            {
                if (![view isKindOfClass:[UITabBar class]])
                    view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, view.frame.size.height + 49);
            }
        }
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)toggleListMapAction:(UIBarButtonItem *)sender isRight:(BOOL)isRight
{
    if (self.selectedViewController == self.childViewControllers.firstObject)
    {
        [self setSelectedIndex:kIndexForList animated:YES];
        [sender setImage:[UIImage imageNamed:(isRight?@"Nav-List-Right":@"Nav-List")]];
        [sender setAccessibilityLabel:@"List"];
    }
    else
    {
        [self setSelectedIndex:kIndexForMap animated:YES];
        [sender setImage:[UIImage imageNamed:(isRight?@"Nav-Map-Right":@"Nav-Map-Left")]];
        [sender setAccessibilityLabel:@"Map"];
    }
}

- (void)setSelectedIndex:(NSUInteger)selectedIndex animated:(BOOL)value
{
    if (value)
    {
        NSUInteger controllerIndex = self.selectedIndex;
        // Get views. controllerIndex is passed in as the controller we want to go to.
        UIView * fromView   = [[self.viewControllers objectAtIndex:controllerIndex] view];
        UIView * toView     = [[self.viewControllers objectAtIndex:!controllerIndex] view];
        
        [UIView transitionFromView:fromView
                            toView:toView
                          duration:0.6
                           options:!selectedIndex ? UIViewAnimationOptionTransitionFlipFromRight : UIViewAnimationOptionTransitionFlipFromLeft
                        completion:^(BOOL finished) {
                            if (finished) {
                                
                                // Remove the old view from the tabbar view.
                                [fromView removeFromSuperview];
                                [super setSelectedIndex:!controllerIndex];
                            }
                        }];
    }
    else
        [super setSelectedIndex:selectedIndex];
}

@end
