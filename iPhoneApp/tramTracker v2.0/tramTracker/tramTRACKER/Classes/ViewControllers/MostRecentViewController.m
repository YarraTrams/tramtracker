//
//  MostRecentViewController.m
//  tramTRACKER
//
//  Created by Raji on 23/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//


#import "MostRecentViewController.h"
#import "MostRecentContainerViewController.h"
#import "Constants.h"
#import "FavouriteCell.h"
#import "tramTRACKERAppDelegate.h"
#import "Analytics.h"
#import "StopList.h"

@interface MostRecentViewController ()

@property (strong, nonatomic) tramTRACKERAppDelegate    * d;
@property (strong, nonatomic) NSArray                   * stopList;

@end

@implementation MostRecentViewController

#pragma mark - Inits & Loads

- (void)viewDidLoad {
    [super viewDidLoad];

    self.stopList = [[StopList sharedManager] mostRecentStops];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveStopUpdateNotification) name:kNotificationMostRecent object:nil];
}

- (void)didReceiveStopUpdateNotification {
    self.stopList = [[StopList sharedManager] mostRecentStops];
    [self.tableView reloadData];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];

    [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureMostRecentList];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.stopList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Stop            * stop    = self.stopList[indexPath.row];
    static NSString * CellIdentifier = @"Favourite Cell";
    FavouriteCell   * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

    cell.labelName.text = [NSString stringWithFormat:@"%@", [stop formattedName]];
    cell.labelRouteDescription.text = [stop formattedRouteDescription];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MostRecentContainerViewController   * parent = (id)self.parentViewController.parentViewController;

    [parent pushToPID:self.stopList[indexPath.row]];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
