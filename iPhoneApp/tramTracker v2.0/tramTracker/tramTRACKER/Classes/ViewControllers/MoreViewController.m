//
//  MoreViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 10/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "TDBadgedCell.h"

#import "MoreViewController.h"
#import "Constants.h"
#import "BackgroundSynchroniser.h"
#import "WebViewController.h"
#import "Analytics.h"
#import "tramTRACKERAppDelegate.h"
#import "RoutesListViewController.h"

@interface MoreViewController () <UIAlertViewDelegate>

@property (nonatomic) BOOL areUpdatesAvailable;

@property (weak, nonatomic) IBOutlet TDBadgedCell   * updateCell;

@property (strong, nonatomic) NSMutableArray        * observers;
@property (strong, nonatomic) NSURL                 * currentURL;

@end

@implementation MoreViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    tramTRACKERAppDelegate  * delegate = (id)[[UIApplication sharedApplication] delegate];
    
    NSUInteger          numberOfStopsToBeUpdated = [delegate.updatesFound[TTSyncKeyStops] count];
    NSUInteger          numberOfRoutesToBeUpdated = [delegate.updatesFound[TTSyncKeyRoutes] count];
    NSUInteger          numberOfTicketsToBeUpdated = [delegate.updatesFound[TTSyncKeyTickets] count];
    NSUInteger          numberOfPOIsToBeUpdated = [delegate.updatesFound[TTSyncKeyPOIs] count];
    
    NSUInteger          totalNumberOfUpdates = numberOfPOIsToBeUpdated + numberOfRoutesToBeUpdated + numberOfStopsToBeUpdated + numberOfTicketsToBeUpdated;
    
    if (totalNumberOfUpdates)
        self.updateCell.badgeString = [NSString stringWithFormat:@"%lu", (unsigned long)totalNumberOfUpdates];
    else
        self.updateCell.badgeString = nil;

    [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureMore];
}

#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    enum
    {
        MoreViewControllerTrackerID,
        MoreViewControllerSearch,
        MoreViewControllerTicketOutlets,
        MoreViewControllerServiceUpdates,
        MoreViewControllerTwitter,
        MoreViewControllerPTV,
        MoreViewControllerTimetables,
        MoreViewControllerFeedback,
        MoreViewControllerHelp,
        MoreViewControllerSettings,
        MoreViewControllerMostRecent,
        MoreViewControllerUpdates
    };
    
    switch (indexPath.row)
    {
        case MoreViewControllerTwitter:
        {
            [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureFollowOnTwitter];
            
            self.currentURL = [NSURL URLWithString:@"twitter:///user?screen_name=yarratrams"];//@"twitter://"];

            NSString * title = @"Open in Twitter?";
            NSString * alertText = @"Would you like to close the tramTracker® app and launch Twitter?";
            
            if (![[UIApplication sharedApplication] canOpenURL:self.currentURL])
            {
                title = @"Open in Safari?";
                alertText = @"Would you like to close the tramTracker® app and launch Safari?";
                self.currentURL = [NSURL URLWithString:@"http://twitter.com/yarratrams"];
            }
            
            [[[UIAlertView alloc] initWithTitle:title
                                        message:alertText
                                       delegate:self
                              cancelButtonTitle:@"Cancel"
                              otherButtonTitles:@"OK", nil]
             show];
            break;
        }
        case MoreViewControllerPTV:
        {
            [[Analytics sharedInstance] featureAccessed:AnalyticsFeaturePTV];
            
            WebViewController * webView = [[WebViewController alloc] init];
            [webView setUrlStr:@"http://ptv.vic.gov.au"];
            [webView setTitle:@"PTV Journey Planner"];
            
            [self.navigationController pushViewController:webView animated:YES];
            break;
            
        }
        case MoreViewControllerTimetables:
        {
            UIStoryboard                * sb = [UIStoryboard storyboardWithName:kMainStoryboard bundle:nil];
            RoutesListViewController    * vc = (id)[sb instantiateViewControllerWithIdentifier:kScreenRoutesList];

//            vc.type = RoutesListViewControllerTimetable;

            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        default:
            break;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Alertview Delegate Methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
        [[UIApplication sharedApplication] openURL:self.currentURL];
}

@end
