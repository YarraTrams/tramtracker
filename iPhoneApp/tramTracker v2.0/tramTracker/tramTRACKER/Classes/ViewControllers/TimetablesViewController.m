//
//  TimetablesViewController.m
//  tramTRACKER
//
//  Created by Raji on 24/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "TimetablesViewController.h"
#import "TimetableCell.h"
#import "Constants.h"
#import "PIDViewController.h"
#import "PidsServiceDelegate.h"
#import "SelectRouteViewController.h"
#import "Analytics.h"
#import "SectionHeaderView.h"

@interface TimetablesViewController () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UILabel *lblRoute;
@property (strong, nonatomic) IBOutlet UILabel *lblTimeOfDeparture;
@property (strong, nonatomic) IBOutlet UITextField *hiddenTextFieldForTime;

@property (nonatomic, strong) PidsService * service;

@property (strong, nonatomic) Stop * stop;
@property (strong, nonatomic) Route * currentRoute;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) NSArray * allRoutesThroughThisStop;
@property (nonatomic, assign) NSInteger selectedRouteIndex;
@property (nonatomic, assign) BOOL upDirection;
@property (nonatomic, strong) UIDatePicker  * picker;

@property (nonatomic, strong) NSArray   * schedules;

@property (nonatomic, assign, getter = hasTimeChanged) BOOL timeChanged;

@end

@implementation TimetablesViewController

#define DatePickerTag       13
#define SelectButtonIndex   1
#define CancelButtonIndex   2

#pragma mark - Inits & Loads

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    /* Set Back Action */
   
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    self.allRoutesThroughThisStop = [NSArray arrayWithArray:[self.stop routesThroughStop]];
   
    if (self.currentRoute)
    {
        self.selectedRouteIndex = [self.allRoutesThroughThisStop indexOfObject:self.currentRoute.number];

        if (self.selectedRouteIndex == NSNotFound)
            self.selectedRouteIndex = 0;
    }
    else
        self.selectedRouteIndex = 0;
    [self setCurrentRouteAndRouteLabel];
    [self setAndDisplayCurrentTime:[NSDate date]];
    
    self.navigationItem.title = self.stop.name;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TimeTableCell" bundle:[NSBundle mainBundle]]
                    forCellReuseIdentifier:@"Timetable Cell"];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(dismissKeyboard)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
    
    // Enabling iOS 7 screen-edge-pan-gesture for pop action
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }

    self.service = [[PidsService alloc] initWithDelegate:self];

    self.picker = [[UIDatePicker alloc] init];
    [self.picker setDatePickerMode:UIDatePickerModeDateAndTime];
    [self.picker setTimeZone:[NSTimeZone systemTimeZone]];
    [self.picker setDate:[NSDate date]];
    [self.picker addTarget:self action:@selector(didSelectDateOnPicker:) forControlEvents:UIControlEventValueChanged];

    self.hiddenTextFieldForTime.inputView = self.picker;
    
    [self.service getScheduledDeparturesForStop:self.stop.trackerID.floatValue
                                    routeNumber:self.currentRoute.internalNumber
                                     atDateTime:self.picker.date
                               withLowFloorOnly:NO];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureSchedulesDeparture];
}

- (void)dealloc
{
    [self.service setDelegate:nil];
}

#pragma mark - Utility Methods

- (void)dismissKeyboard
{
    [self.hiddenTextFieldForTime resignFirstResponder];
}

- (void)setStop:(Stop *)aStop direction:(BOOL)upDirection
{
    self.stop = aStop;
    self.upDirection = upDirection;
}

- (void)setCurrentRouteAndRouteLabel
{
    NSString    * routeNo = self.allRoutesThroughThisStop[self.selectedRouteIndex];

    self.currentRoute = [Route routeWithNumber:routeNo];
    self.lblRoute.text = [self.currentRoute routeNameWithNumberAndDestinationUp:[self.currentRoute.upStops containsObject:self.stop.trackerID]];
}

- (void)setAndDisplayCurrentTime:(NSDate *)date
{
    [self.dateFormatter setDateFormat:@"eee dd MMM hh:mm a"];
    self.lblTimeOfDeparture.text = [self.dateFormatter stringFromDate:date];

    //reset the formatter for "next three" cells
    [self.dateFormatter setDateFormat:@"hh:mm"];
}

- (void)pidsServiceDidFailWithError:(NSError *)error {
    [[[UIAlertView alloc] initWithTitle:@"Error"
                                message:@"Unable to contact the tramTRACKER service. Please make sure that your device is connected to the internet and try again."
                               delegate:nil
                      cancelButtonTitle:@"Ok"
                      otherButtonTitles:nil]
     show];
}

- (void)setSchedules:(NSArray *)schedules
{
    NSMutableArray  *rowsToInsert = [NSMutableArray new];
    
    if (self.hasTimeChanged)
        _schedules = nil;

    if (self.hasTimeChanged || !self.schedules.count)
    {
        _schedules = schedules;
        [self.tableView reloadData];
    }
    else
    {
        for (NSInteger i = 0; i < schedules.count; ++i)
            [rowsToInsert addObject:[NSIndexPath indexPathForItem:rowsToInsert.count + _schedules.count inSection:2]];

        _schedules = [_schedules arrayByAddingObjectsFromArray:schedules];
        
        [self.tableView insertRowsAtIndexPaths:rowsToInsert withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView scrollToRowAtIndexPath:rowsToInsert.firstObject atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    self.timeChanged = NO;
}

#pragma mark - Notification Handlers

- (void)updateSelectedRoute:(NSInteger)aSelectedRouteIndex
{
    self.selectedRouteIndex = (NSInteger)aSelectedRouteIndex;

    [self setCurrentRouteAndRouteLabel];

    self.timeChanged = YES;

    [self.service getScheduledDeparturesForStop:self.stop.trackerID.doubleValue
                                    routeNumber:self.currentRoute.internalNumber
                                     atDateTime:self.picker.date
                               withLowFloorOnly:NO];
}

#pragma mark - Segue Methods

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:kSegueTimetablesToSelectRoute])
    {
        [SelectRouteViewController setCurrentMode:FromTimetables];
        [[segue destinationViewController] setSelectedIndex:self.selectedRouteIndex];
        [[segue destinationViewController] setStop:self.stop direction:self.upDirection];
    }
    else if ([[segue identifier] isEqualToString:kSegueTimetablesToStopInfo])
    {
        [[segue destinationViewController] setStop:self.stop];
    }
}

#pragma mark - User Actions

- (IBAction)btnLoadMoreServices:(id)sender
{
    PredictionStub  * lastPredictionStub = self.schedules.lastObject;
    
    if (lastPredictionStub)
    {
        [self.service getScheduledDeparturesForStop:self.stop.trackerID.floatValue
                                        routeNumber:[Route routeWithNumber:self.stop.routesThroughStop[self.selectedRouteIndex]].internalNumber
                                         atDateTime:[lastPredictionStub.predictedArrivalDateTime dateByAddingTimeInterval:120]
                                   withLowFloorOnly:NO];
    }
}

- (IBAction)backAction:(id)sender
{
     [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITextField Delegate methods

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (void)didSelectDateOnPicker:(UIDatePicker *)datePicker
{
    [self setAndDisplayCurrentTime:datePicker.date];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.timeChanged = YES;

    [self.service getScheduledDeparturesForStop:self.stop.trackerID.floatValue
                                    routeNumber:self.stop.routesThroughStop[self.selectedRouteIndex]
                                     atDateTime:self.picker.date
                               withLowFloorOnly:NO];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return section == 2 ? self.schedules.count : 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 2)
         return 60.0f;
    return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}

#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 36.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [SectionHeaderView staticSectionHeaderWithTitle:@[@"Scheduled departures for", @"Departure time", @"Timetable"][section]];
}

- (NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 2)
        return 1;
    return [super tableView:tableView indentationLevelForRowAtIndexPath:indexPath];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 2)
    {
        static NSString *CellIdentifier = @"Timetable Cell";
        
        TimetableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        PredictionStub  * prediction = self.schedules[indexPath.row];
        
        //reset the formatter for "next three" cells
        [self.dateFormatter setDateFormat:@"hh:mm"];

        NSString    * time = [self.dateFormatter stringFromDate:prediction.predictedArrivalDateTime];

        [self.dateFormatter setDateFormat:@"eee"];

        [cell configureCellWithName:prediction.destination
                     andRouteNumber:prediction.routeNo
                            andTime:time
                             andDay:[self.dateFormatter stringFromDate:prediction.predictedArrivalDateTime]];
        return cell;
    }
    return [super tableView:tableView cellForRowAtIndexPath:indexPath];
}

@end
