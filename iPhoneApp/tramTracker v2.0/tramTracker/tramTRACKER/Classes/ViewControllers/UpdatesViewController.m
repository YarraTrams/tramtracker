//
//  UpdatesViewController.m
//  tramTRACKER
//
//  Created by Raji on 20/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "UpdatesViewController.h"
#import "tramTRACKERAppDelegate.h"
#import "Constants.h"
#import "TicketRetailer.h"
#import "Route.h"
#import "PointOfInterest.h"
#import "BackgroundSynchroniser.h"
#import "HistoricalUpdate.h"

@interface UpdatesViewController ()<BackgroundSynchroniserDelegate>

@property (strong, nonatomic) IBOutlet UILabel          * lblStops;
@property (strong, nonatomic) IBOutlet UILabel          * lblRoutes;
@property (strong, nonatomic) IBOutlet UILabel          * lblTicketOutlets;
@property (strong, nonatomic) IBOutlet UILabel          * lblPOIs;
@property (strong, nonatomic) IBOutlet UILabel          * lblSize;
@property (strong, nonatomic) IBOutlet UILabel          * lblPointOfInterests;

@property (strong, nonatomic) BackgroundSynchroniser    * syncManager;

@property (weak, nonatomic) IBOutlet UIProgressView     * progressView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * topSpaceButton;
@property (weak, nonatomic) IBOutlet UILabel            * downloadLabel;
@property (weak, nonatomic) IBOutlet UIButton           * updateButton;

@property (strong, nonatomic) NSNumber                  * totalUpdates;
@property (strong, nonatomic) NSNumber                  * completedUpdates;
@property (strong, nonatomic) NSMutableDictionary       * stopChanges;
@property (strong, nonatomic) NSMutableDictionary       * retailerChanges;
@property (strong, nonatomic) NSMutableDictionary       * poiChanges;
@property (strong, nonatomic) NSMutableDictionary       * routeChanges;
@property (strong, nonatomic) NSString                  * errorMessage;

@property (strong, nonatomic) UIAlertView * alertView;

@property (nonatomic, assign) NSInteger         detectedOperations;
@property (nonatomic, assign) BOOL              isSyncing;
@property (nonatomic, assign) BOOL              isFinished;
@property (nonatomic, assign) BOOL              isSaving;
@property (nonatomic, assign) BOOL              wasAborted;
@property (nonatomic, assign) BOOL              isUpdating;

@property (assign, nonatomic) NSInteger         errorCount;

@property (nonatomic, strong) NSMutableArray    * log;


@property (nonatomic, assign) BOOL errorHasBeenShown;

@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *rightSpaces;

@end

@implementation UpdatesViewController

const NSUInteger kStopUpdateBytes = 765;
const NSUInteger kPOIUpdateBytes = 765;
const NSUInteger kRouteUpdateBytes = 7026;
const NSUInteger kTicketsUpdateBytes = 2000;

#pragma mark - Inits & Loads

- (NSDictionary *)updates
{
    return [(tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate] updatesFound];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.stopChanges = [NSMutableDictionary new];
    self.routeChanges = [NSMutableDictionary new];
    self.poiChanges = [NSMutableDictionary new];
    self.retailerChanges = [NSMutableDictionary new];
    
    [self updateFigures];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateFigures) name:kNotificationHasFinishedUpdating object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncDidFailWithError:) name:kNotificationHasFailedUpdating object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - User Actions

- (void)updateFigures
{
    NSUInteger          numberOfStopsToBeUpdated = [self.updates[TTSyncKeyStops] count];
    NSUInteger          numberOfRoutesToBeUpdated = [self.updates[TTSyncKeyRoutes] count];
    NSUInteger          numberOfTicketsToBeUpdated = [self.updates[TTSyncKeyTickets] count];
    NSUInteger          numberOfPOIsToBeUpdated = [self.updates[TTSyncKeyPOIs] count];
    
    CGFloat updateSize = (numberOfRoutesToBeUpdated * kRouteUpdateBytes) + (numberOfStopsToBeUpdated * kStopUpdateBytes) + (numberOfTicketsToBeUpdated * kTicketsUpdateBytes) + (numberOfPOIsToBeUpdated * kPOIUpdateBytes);
    
    self.lblStops.text = [NSString stringWithFormat:@"%lu", (unsigned long)numberOfStopsToBeUpdated];
    self.lblRoutes.text = [NSString stringWithFormat:@"%lu", (unsigned long)numberOfRoutesToBeUpdated];
    self.lblTicketOutlets.text = [NSString stringWithFormat:@"%lu", (unsigned long)numberOfTicketsToBeUpdated];
    self.lblPOIs.text = [NSString stringWithFormat:@"%lu", (unsigned long)numberOfPOIsToBeUpdated];
    
    self.lblSize.text = [NSString stringWithFormat:@"%.1f kb", updateSize / 8192];
    self.progressView.progress = 0;
}

- (void)showProgressBar:(BOOL)value
{
    self.topSpaceButton.constant = value ? 60.0f : 9.0f;
    self.progressView.alpha = self.downloadLabel.alpha = value ? 1.0f : 0.0f;
    
    [UIView animateWithDuration:0.5f animations:^{
        [self.view layoutIfNeeded];
    }];
    
    if (value)
        [self.updateButton setImage:[UIImage imageNamed:@"cancel-button"] forState:UIControlStateNormal];
    else
        [self.updateButton setImage:[UIImage imageNamed:@"MORE_Updates_UpdateNow"] forState:UIControlStateNormal];
}

- (IBAction)updateAction:(id)sender
{
    if (!self.isSyncing)
    {
        self.completedUpdates = @0;
        self.downloadLabel.text = [NSString stringWithFormat:@"Downloading Updates"];
        
        tramTRACKERAppDelegate * delegate = [(id)[UIApplication sharedApplication] delegate];
        
        NSDate  * lastSyncDate = [[NSUserDefaults standardUserDefaults] objectForKey:kLastSynchronisationDate];
        
        self.syncManager = [[BackgroundSynchroniser alloc] initWithLastSynchronisationDate:lastSyncDate
                                                                persistentStoreCoordinator:delegate.persistentStoreCoordinator];
        
        self.syncManager.delegate = self;
        [self.syncManager performSelectorInBackground:@selector(synchroniseInBackgroundThread:)
                                           withObject:self.updates];
        self.log = [NSMutableArray array];
        self.parentViewController.parentViewController.tabBarController.tabBar.userInteractionEnabled = NO;
        self.parentViewController.navigationItem.leftBarButtonItem.enabled = NO;
    }
    else
    {
        [self.syncManager cancel];
        self.parentViewController.parentViewController.tabBarController.tabBar.userInteractionEnabled = YES;
        self.parentViewController.navigationItem.leftBarButtonItem.enabled = YES;
    }
}

- (void)syncDidStart
{
	self.isSyncing = YES;
	self.isFinished = NO;
	self.isSaving = NO;
    [self showProgressBar:YES];
    
    //	// grab the historical updates list so we can append to it
    //	NSArray *list = [HistoricalUpdate getHistoricalUpdatesList];
    //	updateList = [list mutableCopy];
}


- (void)syncDidFinishAtTime:(NSDate *)finishTime
{
    self.parentViewController.parentViewController.tabBarController.tabBar.userInteractionEnabled = YES;
    self.parentViewController.navigationItem.leftBarButtonItem.enabled = YES;
    
	self.isSyncing = NO;
	self.isFinished = YES;
	self.isSaving = NO;
    
	[self updateProgress];
    [self showProgressBar:NO];
    
    [[[UIAlertView alloc] initWithTitle:@"Updates Successful" message:@"Database has been successfully updated." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    
    [HistoricalUpdate addHistoricalUpdatesList:self.log];
    [[NSUserDefaults standardUserDefaults] setObject:[[NSDate date] dateByAddingTimeInterval:86400] forKey:kLastSynchronisationDate];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    ((tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate]).updatesFound = nil;
    [(tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
    
    self.navigationController.tabBarItem.badgeValue = nil;
    [self.tabBarController setSelectedIndex:1];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSyncFinished object:nil];
}

- (void)syncDidStartSaving
{
	self.isSaving = YES;
	[self updateProgress];
}

- (void)syncWasCancelled
{
    self.parentViewController.parentViewController.tabBarController.tabBar.userInteractionEnabled = YES;
    self.parentViewController.navigationItem.leftBarButtonItem.enabled = YES;
    
	self.isSyncing = NO;
    
	self.isFinished = YES;
	self.isSaving = NO;
    [self showProgressBar:NO];
}

- (void)syncWasAbortedDueToMemoryWarning
{
    self.parentViewController.parentViewController.tabBarController.tabBar.userInteractionEnabled = YES;
    self.parentViewController.navigationItem.leftBarButtonItem.enabled = YES;
    
	self.isSyncing = NO;
	self.isFinished = YES;
	self.isSaving = NO;
    self.wasAborted = YES;
    
	[self updateProgress];
	
	// Alter the cancel button to a close button
    self.errorMessage = [NSString stringWithFormat:NSLocalizedString(@"updates-message-memoryabort", nil), [[UIDevice currentDevice] name]];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Available Updates";
}

- (void)updateProgress
{
    [self.progressView setProgress:[self.completedUpdates doubleValue] / [self.totalUpdates doubleValue]];
    self.downloadLabel.text = [NSString stringWithFormat:@"DOWNLOADING %ld/%ld", (long)self.completedUpdates.integerValue, (long)self.totalUpdates.integerValue];
}

- (void)syncDidStartRoute:(Route *)route
{
	// if its a new route we wont have any of this information yet
    self.routeChanges[route.internalNumber] = route.name ? route.name : [NSNull null];
}

- (void)syncDidStartStop:(Stop *)stop
{
	self.stopChanges[stop.trackerID] = stop.name ? stop.name : [NSNull null];
}

- (void)syncDidStartRetailer:(TicketRetailer *)aTicketRetailer
{
    self.retailerChanges[aTicketRetailer.retailerID] = aTicketRetailer.name ? aTicketRetailer.name : [NSNull null];
}

- (void)syncDidStartPOI:(PointOfInterest *)aPOI
{
    self.poiChanges[aPOI.poiID] = aPOI.name ? aPOI.name : [NSNull null];
}

- (void)syncDidStartRoutesThroughStop:(Stop *)aStop
{
    self.stopChanges[aStop.trackerID] = aStop.name ? aStop.name : [NSNull null];
}

- (void)syncDidFinishRoutesThroughStop:(Stop *)stop
{
	self.completedUpdates = @([self.completedUpdates integerValue] + 1);
	[self updateProgress];
    
	[self updateMessageForStop:stop];
}

- (void)syncDidFinishStop:(Stop *)stop
{
	self.completedUpdates = @([self.completedUpdates integerValue] + 1);
	[self updateProgress];
    
	[self updateMessageForStop:stop];
}

- (void)syncDidFinishRoute:(Route *)route
{
	self.completedUpdates = @([self.completedUpdates integerValue] + 1);
	[self updateProgress];
    
	[self updateMessageForRoute:route];
}

- (void)syncDidFinishRetailer:(TicketRetailer *)aTicketRetailer
{
	self.completedUpdates = @([self.completedUpdates integerValue] + 1);
	[self updateProgress];
    
    [self updateMessageForRetailer:aTicketRetailer];
}

- (void)syncDidFinishPOI:(PointOfInterest *)aPointOfInterest
{
	self.completedUpdates = @([self.completedUpdates integerValue] + 1);
	[self updateProgress];
    
    [self updateMessageForPOI:aPointOfInterest];
}

- (void)deleteDidFinishPOI:(PointOfInterest *)aPOI
{
	self.completedUpdates = @([self.completedUpdates integerValue] + 1);
	[self updateProgress];
    
    [self updateMessageForPOI:aPOI];
}

- (void)deleteDidFinishRoute:(Route *)aRoute
{
	self.completedUpdates = @([self.completedUpdates integerValue] + 1);
	[self updateProgress];
    
    [self updateMessageForRoute:aRoute];
}

- (void)deleteDidFinishStop:(Stop *)aStop
{
	self.completedUpdates = @([self.completedUpdates integerValue] + 1);
	[self updateProgress];
    
    [self updateMessageForStop:aStop];
}

- (void)deleteDidFinishRetailer:(TicketRetailer *)aRetailer
{
	self.completedUpdates = @([self.completedUpdates integerValue] + 1);
	[self updateProgress];
    
    [self updateMessageForRetailer:aRetailer];
}

- (void)syncDidAddOperation:(NSOperation *)operation
{
	self.detectedOperations++;
    
	if (self.detectedOperations > [self.totalUpdates integerValue])
	{
		self.totalUpdates = [NSNumber numberWithInteger:self.detectedOperations];
		[self updateProgress];
	}
}

- (void)syncDidRemoveOperation
{
	self.totalUpdates = @([self.totalUpdates integerValue] - 1);
	[self updateProgress];
}

- (void)syncDidFinishPOIsThroughStop:(Stop *)stop
{
	self.completedUpdates = @([self.completedUpdates integerValue] + 1);
	[self updateProgress];
}

- (void)syncDidUpdateTicketOutlets
{
	self.completedUpdates = @([self.completedUpdates integerValue] + 1);
	[self updateProgress];
}

- (void)syncDidFailWithError:(id)object
{
    NSError * error = [object isKindOfClass:[NSNotification class]] ? [object object] : object;
    
	if ([error code] == PIDServiceErrorNotReachable)
		self.errorMessage = [error localizedDescription];
	else
		self.errorMessage = NSLocalizedString(@"updates-unknown-error-sync", nil);
    
    if (!self.alertView) {
//        self.alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:self.errorMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [self.alertView show];
    }
    
	[self syncWasCancelled];
}

- (void)updateMessageForRetailer:(TicketRetailer *)aTicketRetailer
{
    NSString    * name = NSLocalizedString(@"retailer-name-mixed-list" , aTicketRetailer.name);
    NSString    * message = nil;
    
    if ([aTicketRetailer isDeleted])
		message = [NSString stringWithFormat:NSLocalizedString(@"updates-stop-deleted-template", nil), [aTicketRetailer name]];
    else if (self.retailerChanges[aTicketRetailer.retailerID] != [NSNull null])
        message = [NSString stringWithFormat:NSLocalizedString(@"updates-stop-updated-template", nil), [aTicketRetailer name]];
    else
        message = [NSString stringWithFormat:NSLocalizedString(@"updates-stop-added-template", nil), [aTicketRetailer name]];
    
    [self.log addObject:
     [HistoricalUpdate historicalUpdateWithName:name message:message]];
}

- (void)updateMessageForPOI:(PointOfInterest *)aPointOfInterest
{
    NSString    * name = NSLocalizedString(@"poi-name-mixed-list", [aPointOfInterest name]);
    NSString    * message = nil;
    
    if ([aPointOfInterest isDeleted])
		message = [NSString stringWithFormat:NSLocalizedString(@"updates-stop-deleted-template", nil), [aPointOfInterest name]];
    else if (self.poiChanges[aPointOfInterest.poiID] != [NSNull null])
        message = [NSString stringWithFormat:NSLocalizedString(@"updates-stop-updated-template", nil), [aPointOfInterest name]];
    else
        message = [NSString stringWithFormat:NSLocalizedString(@"updates-stop-added-template", nil), [aPointOfInterest name]];
    
    [self.log addObject:
     [HistoricalUpdate historicalUpdateWithName:name message:message]];
}

- (void)updateMessageForStop:(Stop *)stop
{
	NSString    * name = [NSString stringWithFormat:NSLocalizedString(@"stop-name-mixed-list", nil), stop.number, stop.name];
    NSString    * message = nil;
    
    if ([stop isDeleted])
		message = [NSString stringWithFormat:NSLocalizedString(@"updates-stop-deleted-template", nil), [stop displayedCityDirection]];
    else if (self.stopChanges[stop.trackerID] != [NSNull null])
        message = [NSString stringWithFormat:NSLocalizedString(@"updates-stop-updated-template", nil), [stop displayedCityDirection]];
    else
        message = [NSString stringWithFormat:NSLocalizedString(@"updates-stop-added-template", nil), [stop displayedCityDirection]];
	
    [self.log addObject:
     [HistoricalUpdate historicalUpdateWithName:name message:message]];
}

- (void)updateMessageForRoute:(Route *)route
{
	NSString    * name = NSLocalizedString(@"routes-name", route.number);
    NSString    * message = nil;
    
    if ([route isDeleted])
		message = [NSString stringWithFormat:NSLocalizedString(@"updates-stop-deleted-template", nil), [route number]];
    else if (self.routeChanges[route.internalNumber] != [NSNull null])
        message = [NSString stringWithFormat:NSLocalizedString(@"updates-stop-updated-template", nil), [route number]];
    else
        message = [NSString stringWithFormat:NSLocalizedString(@"updates-stop-added-template", nil), [route number]];
	
    [self.log addObject:
     [HistoricalUpdate historicalUpdateWithName:name message:message]];
}

- (void)resetTotalUpdates:(NSNumber *)newNumber
{
	self.totalUpdates = newNumber;
	[self updateProgress];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    self.alertView = nil;
}

@end
