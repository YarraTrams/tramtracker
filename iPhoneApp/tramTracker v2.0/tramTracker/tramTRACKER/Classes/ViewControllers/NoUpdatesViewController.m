//
//  NoUpdatesViewController.m
//  tramTRACKER
//
//  Created by Raji on 24/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "NoUpdatesViewController.h"
#import "tramTRACKERAppDelegate.h"
#import "Constants.h"

@interface NoUpdatesViewController ()

@property (weak, nonatomic) IBOutlet UIButton *checkForUpdatesButton;
@property (nonatomic) BOOL isUpdating;

@property (nonatomic, assign) BOOL errorHasAlreadyBeenShown;

@end

@implementation NoUpdatesViewController

- (void)viewDidLoad
{
    self.isUpdating = NO;
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveNotificationUpdates:)
                                                 name:kNotificationHasFinishedUpdating
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveNotificationUpdates:)
                                                 name:kNotificationHasFailedUpdating
                                               object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveNotificationUpdates:(NSNotification *)notification
{
    self.isUpdating = NO;
    [self.checkForUpdatesButton setImage:[UIImage imageNamed:@"Updates-Button"] forState:UIControlStateNormal];

    if ([notification.object isKindOfClass:[NSError class]] && !self.errorHasAlreadyBeenShown)
    {
        NSError     *error = notification.object;
        NSString    *errorMessage;
        
        if ([error code] == PIDServiceErrorNotReachable)
            errorMessage = [error localizedDescription];
        else
            errorMessage = NSLocalizedString(@"updates-unknown-error-sync", nil);
        [[[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        self.errorHasAlreadyBeenShown = YES;
    }
}

- (IBAction)checkForUpdatesAction:(id)sender
{
    tramTRACKERAppDelegate * delegate = (id)[[UIApplication sharedApplication] delegate];

    self.errorHasAlreadyBeenShown = NO;

    if (!self.isUpdating)
    {
        [self.checkForUpdatesButton setImage:[UIImage imageNamed:@"cancel-button"] forState:UIControlStateNormal];
        [delegate checkForUpdates];
    }
    else
    {
        [self.checkForUpdatesButton setImage:[UIImage imageNamed:@"Updates-Button"] forState:UIControlStateNormal];
        [delegate cancelUpdate];
    }
    self.isUpdating = !self.isUpdating;
}

@end
