//
//  Route35WebInfoViewController.m
//  tramTRACKER
//
//  Created by Raji on 2/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import "Route35WebInfoViewController.h"

@interface Route35WebInfoViewController ()
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation Route35WebInfoViewController

#pragma mark - User Actions on Nav Bar
- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Inits & Loads

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSString *urlString = @"http://www.yarratrams.com.au/using-trams/visitors-new-users/city-circle-tram/";
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:requestObj];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
