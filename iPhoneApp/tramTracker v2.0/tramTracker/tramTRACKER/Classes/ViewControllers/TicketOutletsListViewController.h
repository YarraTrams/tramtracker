//
//  TicketOutletsListViewController.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 12/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TicketOutletsListViewController : UITableViewController

@end
