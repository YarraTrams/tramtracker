//
//  UpdatesTabBarViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 3/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import "UpdatesTabBarViewController.h"
#import "tramTRACKERAppDelegate.h"
#import "Constants.h"
#import "Analytics.h"

@interface UpdatesTabBarViewController ()

@end

@implementation UpdatesTabBarViewController

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveNotificationUpdates:)
                                                 name:kNotificationHasFinishedUpdating
                                               object:nil];
}

- (void)didReceiveNotificationUpdates:(NSNotification *)notification
{
    typedef enum
    {
        UpdatesViewController,
        EmptyUpdatesViewController
    }   UpdatesViewControllerType;
    
    tramTRACKERAppDelegate * delegate = (id)[[UIApplication sharedApplication] delegate];
    UpdatesViewControllerType type = delegate.updatesFound != nil ? UpdatesViewController : EmptyUpdatesViewController;

    
    if (type == self.selectedIndex)
        return;

    if (!notification)
        [self setSelectedIndex:type];
    else
        [super setSelectedIndex:type animated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self didReceiveNotificationUpdates:nil];

    [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureUpdates];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
