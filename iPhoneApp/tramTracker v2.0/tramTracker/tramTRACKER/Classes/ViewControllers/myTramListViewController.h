//
//  myTramDetailedViewController.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 25/11/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Journey.h"

@interface myTramListViewController : UIViewController

- (void)setFinalJourney:(Journey *)journey;
- (void)setRoute:(Route *)aRoute direction:(BOOL)upDirection isTimeTables:(BOOL)isTimetables;

@property (nonatomic, strong) Stop      * nextStop;
@property (nonatomic, strong) Stop      * atStop;
@property (nonatomic, strong) NSArray   * stopList;
@property (nonatomic, strong) NSMutableArray * previouslyHighlightedStops;

@end

