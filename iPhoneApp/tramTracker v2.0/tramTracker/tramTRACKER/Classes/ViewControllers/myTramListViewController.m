//
//  myTramDetailedViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 25/11/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "myTramListViewController.h"
#import "DisruptionUpdateCell.h"
#import "StopRightFilterViewController.h"
#import "SectionHeaderView.h"
#import "myTramCell.h"
#import "StopList.h"
#import "TimetablesViewController.h"
#import "RightMenuDelegate.h"
#import "Constants.h"
#import "TicketRetailer.h"
#import "myTramViewController.h"
#import "TicketOutletDetailedViewController.h"
#import "Analytics.h"
#import "UIAlertView+Blocks.h"
#import "Prediction.h"
#import "Turn.h"
#import "myTramViewController.h"

@interface myTramListViewController ()<UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate>

@property (nonatomic, weak) IBOutlet UITableView        * disruptionsTableView;
@property (nonatomic, weak) IBOutlet UITableView        * tableView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint * disruptionsUpdatesTableviewHeight;

@property (nonatomic, strong) NSArray                   * ticketRetailersList;

@property (nonatomic, strong) NSArray                   * suburbStopList;

@property (nonatomic, strong) NSArray                   * data;
@property (nonatomic, strong) NSMutableArray            * dataStates;

@property (nonatomic, strong) NSMutableArray            * selectedIndexPaths;

@property (nonatomic, strong) DisruptionUpdateCell      * updatesCell;
@property (nonatomic, strong) DisruptionUpdateCell      * disruptionsCell;

@property (nonatomic, strong) NSDateFormatter           * dateFormatter;

@property (nonatomic, strong) Journey * journey;
@property (nonatomic, strong) Route * route;

@property (nonatomic, assign) BOOL isUp;
@property (nonatomic, assign) BOOL isTimetables;

@property (nonatomic, assign, getter = isFirstAppear) BOOL firstAppear;
@property (nonatomic, assign, getter = isFirstExpanded) BOOL firstExpanded;
@property (nonatomic, assign, getter = isSecondExpanded) BOOL secondExpanded;
@property (nonatomic, assign, getter = hasUserScrolled) BOOL userScrolled;

@property (nonatomic, assign) StopRightMenuType listType;

@property (nonatomic, assign) NSInteger offset;
@property (nonatomic, strong) CLLocationManager * locationManager;
@property (nonatomic, strong) CLLocation    * oldLocation;

@end

@implementation myTramListViewController

#pragma mark - Inits & Loads

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        self.firstAppear = YES;
        self.locationManager = [CLLocationManager new];
        self.locationManager.delegate = self;
        self.dataStates = [NSMutableArray new];
        self.previouslyHighlightedStops = [NSMutableArray new];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    
    [self.dateFormatter setDateFormat:@"HH:mm"]; // -- use built in style to allow for use of 24 hour time setting
    
    [self.disruptionsTableView registerNib:[UINib nibWithNibName:kCellDisruptionFull bundle:[NSBundle mainBundle]]
                    forCellReuseIdentifier:kCellDisruptionFull];
    [self.disruptionsTableView registerNib:[UINib nibWithNibName:kCellDisruptionMinimal bundle:[NSBundle mainBundle]]
                    forCellReuseIdentifier:kCellDisruptionMinimal];
    [self.disruptionsTableView registerNib:[UINib nibWithNibName:kCellUpdateFull bundle:[NSBundle mainBundle]]
                    forCellReuseIdentifier:kCellUpdateFull];
    [self.disruptionsTableView registerNib:[UINib nibWithNibName:kCellUpdateMinimal bundle:[NSBundle mainBundle]]
                    forCellReuseIdentifier:kCellUpdateMinimal];
    
    // Fix scroll to top
    UIViewController * baseCon = self.parentViewController.parentViewController;
    for (UIView * subViews in [baseCon.view subviews ]) {
        if ([[subViews class] isSubclassOfClass:[UITableView class]]) {
            [(UITableView*)subViews setScrollsToTop:NO];
        }
    }
    
    self.tableView.scrollsToTop = YES;

    // Enabling iOS 7 screen-edge-pan-gesture for pop action
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }

    self.firstExpanded = YES;
    self.secondExpanded = NO;
}

- (myTramViewController *)parent
{
    return (myTramViewController *)self.parentViewController.parentViewController;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    AnalyticsFilter     filter = AnalyticsFilterNone;
    AnalyticsFeature    feature = AnalyticsFeatureMyTramDetailedList;
    
    if (self.isTimetables)
        feature = AnalyticsFeatureTimeTablesList;
    else if (!self.journey)
        feature = AnalyticsFeatureRoutesDetailedList;
    
    if (feature == AnalyticsFeatureTimeTablesList || feature == AnalyticsFeatureMyTramDetailedList)
        filter = AnalyticsFilterNone;
    else if (self.listType == StopRightMenuAll)
        filter = AnalyticsFilterAll;
    else if (self.listType == StopRightMenuShelter)
        filter = AnalyticsFilterShelter;
    else if (self.listType == StopRightMenuAccess)
        filter = AnalyticsFilterEasyAccess;
    else if (self.listType == StopRightMenuOutlets)
        filter = AnalyticsFilterOutlets;
    
    [[Analytics sharedInstance] featureAccessed:feature withFilter:filter];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (self.isFirstAppear)
    {
        if (self.nextStop)
            [self scrollToStop:self.atStop ? self.atStop : self.nextStop];
        self.firstAppear = NO;
    }
}

#pragma mark - Utility Methods

- (void)didSelectAccessoryForSectionHeader:(SectionHeaderView *)sectionHeader
{
    NSInteger       dataSection = sectionHeader.section;
    NSInteger       tableSection = self.listType == StopRightMenuOutlets ? dataSection : dataSection + 1;
    
    self.dataStates[dataSection] = @(![self.dataStates[dataSection] boolValue]);
    
    NSMutableArray  * indexPaths = [NSMutableArray new];
    for (NSInteger i = 0; i < [self.data[dataSection] count]; ++i)
        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:tableSection]];
    
    if (![self.dataStates[dataSection] boolValue])
        [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationMiddle];
    else
        [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationMiddle];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.tableView)
    {
        if (self.isTimetables && indexPath.section)
        {
            Stop                        * stop = self.data[indexPath.section - 1][indexPath.row];
            
            UIStoryboard                * sb = [UIStoryboard storyboardWithName:kMainStoryboard bundle:nil];
            TimetablesViewController    * vc = [sb instantiateViewControllerWithIdentifier:kScreenTimetables];
            
            [vc setStop:stop direction:self.isUp];
            [vc setCurrentRoute:self.route];
            [self.navigationController pushViewController:vc animated:YES];
        }
        else
        {
            if (self.listType == StopRightMenuOutlets)
            {
                TicketOutletDetailedViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:kScreenTicketDetailed];
                [vc setCurRetailer:self.data[indexPath.section][indexPath.row]];
                
                [self.parentViewController.parentViewController.navigationController pushViewController:vc animated:YES];
            }
            else if (indexPath.section)
            {
                Stop * s = self.data[indexPath.section - 1][indexPath.row];
                [self.parentViewController.parentViewController performSegueWithIdentifier:kSegueShowPID sender:s];
            }
        }
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    else
    {
        BOOL newState;
        
        if (!indexPath.row)
            newState = self.firstExpanded = !self.isFirstExpanded;
        else
            newState = self.secondExpanded = !self.isSecondExpanded;
        
        [self reloadDisruptionsTableViewNewState:YES indexPath:indexPath];
    }
}

#pragma mark - UITableViewDataSource

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView == self.tableView)
    {
        if (self.listType != StopRightMenuOutlets)
        {
            if (!section)
                return nil;
            section--;
        }
        
        SectionHeaderView * view = [SectionHeaderView expandableSectionHeaderViewWithTitle:[self.data[section][0] suburb]
                                                                                   section:section
                                                                                    target:self
                                                                                    action:@selector(didSelectAccessoryForSectionHeader:)];
        [view setExpandedState:[self.dataStates[section] boolValue]];
        return view;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView == self.tableView)
        return !section && self.listType != StopRightMenuOutlets ? 0.0f : 36.0f;
    else
        return 0.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.tableView)
    {
        if (!indexPath.section && self.listType != StopRightMenuOutlets)
            return !self.journey || !self.parent.isAtLayover ? 85.0f : 110.0f;
        return 65.0f;
    }
    else
    {
        if (!indexPath.row)
            return self.isFirstExpanded ?  98.0f : 30.0f;
        else
            return self.isSecondExpanded ? 98.0f : 30.0f;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == self.tableView)
        return self.listType == StopRightMenuOutlets ? self.data.count : self.data.count + 1;
    else
        return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.tableView)
    {
        if (self.listType == StopRightMenuOutlets)
            return [self.dataStates[section] boolValue] ? [self.data[section] count] : 0;
        
        else if (!section)
            return 1;
        
        BOOL isExpanded = [self.dataStates[section - 1] boolValue];
        return isExpanded ? [self.data[section - 1] count] : 0;
    }
    else
    {
        if (self.journey.tram.isDisrupted && self.journey.tram.hasSpecialEvent)
            return 2;
        else if (self.journey.tram.isDisrupted || self.journey.tram.hasSpecialEvent)
            return 1;
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.tableView)
    {
        myTramCell  * cell;

        if (self.listType == StopRightMenuOutlets)
        {
            static NSString     * const RetailerCellIdentifier = @"RetailerCell";
            
            cell = (id)[tableView dequeueReusableCellWithIdentifier:RetailerCellIdentifier];
            
            TicketRetailer  * retailer = self.data[indexPath.section][indexPath.row];
            
            [cell configureWithRetailer:retailer];
        }
        else if (!indexPath.section)
        {
            /* Legend */
            static NSString     * const LegendCellIdentifier = @"myTramLegendCell";
            static NSString     * const LegendCellIdentifierTerminus = @"myTramLegendTerminusCell";

            cell = (id)[tableView dequeueReusableCellWithIdentifier:!self.journey || !self.parent.isAtLayover ? LegendCellIdentifier : LegendCellIdentifierTerminus];
            [cell configureWithJourney:self.journey];
        }
        else
        {
            /* Regular stop cell */
            static NSString     * const DataCellIdentifier = @"myTramCell";
            
            cell = (id)[tableView dequeueReusableCellWithIdentifier:DataCellIdentifier];
            
            Stop        * curStop = self.data[indexPath.section - 1][indexPath.row];
            JourneyStop * journeyStop = [self.journey journeyStopForStop:curStop];
            NSString    * routeImage = [self routeImageNameFromStop:curStop indexPath:indexPath];

            if (!self.journey)
                [cell configureFromRouteWithStop:curStop routeImage:routeImage];
            else if (journeyStop)
            {
                NSUInteger  isSelected = [self.parent isSelectedCell:curStop withJourneyStop:journeyStop];
                
                [cell configureWithJourneyStop:journeyStop isSelected:isSelected routeImage:routeImage];

                NSDate *offsetPredictedArrivalDate = [journeyStop.predicatedArrivalDateTime dateByAddingTimeInterval:self.offset];
                NSString *predictedArrivalString = [self.dateFormatter stringFromDate:journeyStop.predicatedArrivalDateTime];
                NSString *voiceOverStringForCell = [NSString stringWithFormat:@"%@ is due to arrive at stop %@ (%@) at %@", self.journey.tram.name, curStop.number, curStop.name, [self.dateFormatter stringFromDate:journeyStop.predicatedArrivalDateTime]];

                // if this is the first stop in the journey, and we're closer than 2 minutes then update it
                if (![self.parent isStopped] && self.parent.accuracyLevel <= 3 && ![curStop isCityStop] && [curStop isEqual:self.nextStop] && [offsetPredictedArrivalDate timeIntervalSinceNow] <= 120)
                {
                    if (self.atStop && [self.atStop isEqual:curStop])
                    {
                        predictedArrivalString = NSLocalizedString(@"onboard-now", @"now");
                        voiceOverStringForCell = [NSString stringWithFormat:@"%@ is arriving at stop %@ (%@) now", self.journey.tram.name, curStop.number, curStop.name];
                    }
                    else
                    {
                        predictedArrivalString = NSLocalizedString(@"onboard-next", @"next");
                        voiceOverStringForCell = [NSString stringWithFormat:@"%@ will be arriving at stop %@ (%@) next", self.journey.tram.name, curStop.number, curStop.name];
                    }
                }

                cell.estimatedTime.text = predictedArrivalString;
                [cell setAccessibilityLabel:voiceOverStringForCell];

                Turn *turn = [self.journey.tram.route turnForStop:journeyStop.stop upDirection:[self.journey.tram upDirection]];
                [cell setTurnIndicatorImage:(turn == nil ? nil : [turn image])];
            }
            else
            {
                [cell configureWithStop:curStop routeImage:routeImage];
                
                NSString *voiceOverString = [NSString stringWithFormat:@"%@ has passed stop %@ %@", self.journey.tram.name, curStop.number, curStop.name];
                [cell setAccessibilityLabel:voiceOverString];
                [cell setTurnIndicatorImage:nil];
            }
        }
        return cell;
    }
    else
    {
        /* Disruption tableView */
        DisruptionUpdateCell  *cell;
        BOOL            isExpanded = !indexPath.row ? self.isFirstExpanded : self.isSecondExpanded;
        NSString        * message;
        
        if (!indexPath.row && self.journey.tram.isDisrupted)
        {
            /* Disruptions */
            cell = [tableView dequeueReusableCellWithIdentifier:isExpanded ? kCellDisruptionFull : kCellDisruptionMinimal];
            self.disruptionsCell = cell;
            
            message = self.journey.tram.disruptionMessage;
            
            if (!message.length)
                message = [NSString stringWithFormat:@"Route %1$@ is currently disrupted and delays may occur.", self.journey.tram.route.number];
        }
        else
        {
            /* Service Update */
            cell = [tableView dequeueReusableCellWithIdentifier:isExpanded ? kCellUpdateFull : kCellUpdateMinimal];
            self.updatesCell = cell;
            
            message = self.journey.tram.specialMessage;
        }
        
        [cell configureWithMessage:message];
        return cell;
    }
    return nil;
}

- (NSString *)routeImageNameFromStop:(Stop *)stop indexPath:(NSIndexPath *)indexPath
{
    NSMutableString     * imageName = [@"icn_route_" mutableCopy];
    
    /*
     * Set the route's color
     */
    
    if (self.route)
        [imageName appendString:self.route.colour];
    else if (self.journey.tram.route)
        [imageName appendString:self.journey.tram.route.colour];
    else
        [imageName appendString:@"yellow"];

    /*
     * Set the stop's position on the route
     */
    if (indexPath.section == 1 && !indexPath.row)
        [imageName appendString:@"_start"];
    else if (indexPath.section == self.data.count && indexPath.row == [self.data.lastObject count] - 1)
        [imageName appendString:@"_end"];
    else
        [imageName appendString:@"_middle"];
    return [imageName copy];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    self.userScrolled = YES;
}

- (void)setRoute:(Route *)aRoute direction:(BOOL)upDirection isTimeTables:(BOOL)isTimetables
{
    self.isTimetables = isTimetables;
    
    /* Fetch the stoplist */
    self.stopList = [[StopList sharedManager] getStopListForTrackerIDs:(upDirection ?
                                                                            aRoute.upStops :
                                                                            aRoute.downStops)];

    if (!self.stopList)
        return;
    
    self.route = aRoute;
    
    [self setupSuburbList];
    
    self.isUp = upDirection;
    [self.tableView reloadData];
}

- (void)reloadDisruptionsTableViewNewState:(BOOL)animated indexPath:(NSIndexPath *)indexPath
{
    CGFloat     tableViewHeight = 0.0f;
    
    if (indexPath)
        [self.disruptionsTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    else
        [self.disruptionsTableView reloadData];
    
    // Calculate new height of updates/disruption tableview
    if (self.journey.tram.isDisrupted)
        tableViewHeight += self.isFirstExpanded ? 98.0f : 30.0f;
    
    if (self.journey.tram.hasSpecialEvent)
    {
        BOOL isExpanded = !self.journey.tram.isDisrupted ? self.isFirstExpanded : self.isSecondExpanded;
        tableViewHeight += isExpanded ? 98.0f : 30.0f;
    }

    if (tableViewHeight != self.disruptionsUpdatesTableviewHeight.constant)
    {
        // Set the height
        self.disruptionsUpdatesTableviewHeight.constant = tableViewHeight;
        [UIView animateWithDuration:animated ? 0.3f : 0.0f
                         animations:^{
                             [self.view layoutIfNeeded];
                         }];
    }
}

- (void)setupSuburbList
{
	// make sure that we have a stop list
	NSAssert(self.stopList, @"Cannot build suburb list as no stops have been set.");

    NSMutableArray  * data = [NSMutableArray new];
    NSString        * lastSuburb = nil;

	for (Stop *stop in self.stopList)
	{
		if (!data.count || ![lastSuburb isEqualToString:stop.suburbName])
        {
			[data addObject:[NSMutableArray new]];
            [self.dataStates addObject:@YES];
        }
        lastSuburb = stop.suburbName;
		[data.lastObject addObject:stop];
	}
    self.suburbStopList = [data copy];
    self.data = self.suburbStopList;
}

- (void)setFinalJourney:(Journey *)newJourney
{
	// is this a new journey that we've not seen before?
	if (!self.journey || [self.parent hasTramChangedRoute:newJourney.tram])
	{
		// setup the stop lists
		[self setStopList:[[StopList sharedManager] getStopListForTrackerIDs:(newJourney.tram.upDirection ? newJourney.tram.route.upStops : newJourney.tram.route.downStops)]];
        
		// bail out if we have no stops
		if (self.stopList == nil)
        {
            NSMutableArray * stopList = [NSMutableArray new];
            
            for (JourneyStop * curJourneyStop in newJourney.stops)
                [stopList addObject:curJourneyStop.stop];
            
            if (!stopList.count)
                return;

            self.stopList = [stopList copy];
        }
		[self setupSuburbList];

        self.nextStop = nil;
        
		// clear the highlighting stuff
		[self.previouslyHighlightedStops removeAllObjects];
	}
	
	// if we're at layover but we're moving then force us off layover (if the number of stops in the new journey has shrunk)
	if (self.journey != nil && ![self.parent hasTramChangedRoute:newJourney.tram] &&
		[self.parent isAtLayover] && [newJourney.stops count] > 0 && [newJourney.stops count] < [self.journey.stops count])
	{
		// double check to make sure that the first stop is no longer the same
		Stop *firstStop = self.stopList.firstObject;
		JourneyStop *firstJStop = newJourney.stops.firstObject;
		JourneyStop *oldFirstJStop = self.journey.stops.firstObject;
        
		if (![firstJStop.stop isEqual:firstStop] && ![firstJStop.stop isEqual:oldFirstJStop.stop])
		{
			// we're not on layover anymore
			[self.parent setAtLayover:NO];
			
			// set a timer for the pinpoint message
			[self.parent setStartedLocating:[NSDate date]];
		}
	}
	
	if (newJourney.stops.firstObject && ![self.parent isAtLayover])
	{
		// so we know what the next stop apparently is now
		JourneyStop *nextJStop = newJourney.stops.firstObject;
		Stop *next = nextJStop.stop;

		// Did we have no data previously?
		if (self.nextStop == nil)
		{
			self.journey = newJourney;
            [self.tableView reloadData];
			[self moveToStop:next];
		}
        else
		{
			// find the indexes of those two stops
			NSInteger indexOfNewNextStop = [self.stopList indexOfObject:next];
			NSInteger indexOfOldNextStop = [self.stopList indexOfObject:self.nextStop];
			
            self.journey = newJourney;
            self.offset = 0;

			// move to whichever is further along
			if (indexOfNewNextStop > indexOfOldNextStop)
				[self moveToStop:next];
			else
            {
				// nope, we need to discard yet more stops off the front
				NSInteger indexOfOldNextStopInJourney = [self.journey.stops indexOfObject:[self.journey journeyStopForStop:self.nextStop]];
				if (indexOfOldNextStopInJourney != NSNotFound && indexOfOldNextStopInJourney)
                    self.journey.stops = [self.journey.stops subarrayWithRange:NSMakeRange(indexOfOldNextStopInJourney, self.journey.stops.count - indexOfOldNextStopInJourney)];
			}
            [self.tableView reloadData];
		}
	}
    else
	{
		// update the data but no moving
		self.journey = newJourney;
        [self.tableView reloadData];
	}

    [self reloadDisruptionsTableViewNewState:NO indexPath:nil];
    
	// start the GPS going
	if ([CLLocationManager locationServicesEnabled] && [self isGPSEnabled])
		[self.locationManager startUpdatingLocation];
	else
	{
		[self.parent setAccuracyLevel:5];
		[self.locationManager stopUpdatingLocation];
	}

    NSIndexPath     * selectedRow = [self indexPathOfStop:self.atStop ? self.atStop : self.nextStop];

    if (selectedRow && !self.parent.isAtLayover && (self.atStop || self.nextStop) && [self.dataStates[selectedRow.section - 1] boolValue] &&
        (self.parent.hasButtonBeenPressed || !self.hasUserScrolled))
    {
        [self scrollToStop:self.atStop ? self.atStop : self.nextStop];
        self.userScrolled = NO;
    }
    else if (self.parent.isAtLayover && (self.parent.hasButtonBeenPressed || !self.hasUserScrolled))
    {
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                              atScrollPosition:UITableViewScrollPositionTop
                                      animated:YES];
        self.userScrolled = NO;
    }
    self.parent.hasButtonBeenPressed = NO;
}

//
// We got a new location
//
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation * newLocation = locations.lastObject;

    if (!newLocation)
        return ;

	// the location manager might have a more recent location
	if ([manager.location.timestamp isEqualToDate:[manager.location.timestamp laterDate:newLocation.timestamp]])
		newLocation = manager.location;
    
	// first thing we do is discard the first result, the cell tower location just isn't accurate enough
	self.oldLocation = newLocation;
    
	if (!self.oldLocation && self.oldLocation != newLocation)
		return;

	// is it more than 2 minutes old?
	if (fabs([newLocation.timestamp timeIntervalSinceNow]) > 120)
		return;
	
	// have we moved at all? are we now more than 50m from the terminus
	if ([self.parent isAtLayover] && [self.stopList count] > 1)
	{
		Stop *firstStop = [self.stopList objectAtIndex:0];
		Stop *secondStop = [self.stopList objectAtIndex:1];
		if ([self.parent isAtLayover] && [LocationManager distanceFromLocation:newLocation toLocation:firstStop.location] > ([firstStop.length integerValue] + 50) &&
			[firstStop location:newLocation betweenSelfAndStop:secondStop])
		{
			[self.parent setAtLayover:NO];
		}
	}
	
	// update the accuracy indicator
	NSInteger accuracyLevel;
	if (newLocation.horizontalAccuracy <= 50)
		accuracyLevel = 1;
	else if (newLocation.horizontalAccuracy <= 150)
		accuracyLevel = 2;
	else if (newLocation.horizontalAccuracy <= 250)
		accuracyLevel = 3;
	else if (newLocation.horizontalAccuracy <= 500)
		accuracyLevel = 4;
	else
		accuracyLevel = 5;
	
	// save the new accuracy level
	[self.parent setAccuracyLevel:accuracyLevel];

	// if we're still at layover then discard this result
	if ([self.parent isAtLayover])
		return;

	// throw out anything thats too far away
	if (newLocation.horizontalAccuracy >= 500)
		return;

	// if this is a non-public trip then we don't have scheduled times (and we're in free-flow mode)
	if (![self.journey.tram isAvailable])
	{
		// Find out where the next stop is
		Stop *next = [self nextStopWithLocation:newLocation];

		if (!next)
			return;

        self.nextStop = next;
		[self scrollToStop:next];
		[self.tableView reloadData];
		return;
	}

	// if we don't hae a full data set we discard this again
	if ([self.journey.stops count] == 0)
		return;
	
	// are we in the CBD?
	if ([[(JourneyStop *)[self.journey.stops objectAtIndex:0] stop] isCityStop])
	{
		[self.parent setAccuracyLevel:5];
		return;
	}
	
	// if we're more than 500m from what is considered the current stop then discard this result
	if (self.nextStop != nil && [LocationManager distanceFromLocation:newLocation toLocation:self.nextStop.location] > 500)
	{
		[self.parent setAccuracyLevel:5];
		return;
	}
    
	Stop *next = [self nextStopWithLocation:newLocation];
	if (!next)
		return;

	// Find it in the journey
	JourneyStop *jstop = [self.journey journeyStopForStop:next];
	if (!jstop)
		return;

	// if the predicted arrival time at that stop is more than 60 seconds away then something isn't working, unless its the next stop
	NSDate *offsetPredictedArrivalDate = ([jstop.predicatedArrivalDateTime respondsToSelector:@selector(dateByAddingTimeInterval:)] ? [jstop.predicatedArrivalDateTime dateByAddingTimeInterval:self.offset] : [jstop.predicatedArrivalDateTime dateByAddingTimeInterval:self.offset]);
	//NSLog(@"Predicted Arrival Time: %@, With Offset: %@, Travel Time: %.2f", jstop.predicatedArrivalDateTime, offsetPredictedArrivalDate, [offsetPredictedArrivalDate timeIntervalSinceNow]);
	if (fabs([offsetPredictedArrivalDate timeIntervalSinceNow]) > 60 && [self.journey.stops indexOfObject:jstop] > 1)
		return;

	// update the offset
	NSTimeInterval secondsToNextStop = [self secondsToNextStopWithLocation:newLocation];
	NSTimeInterval secondsPastPreviousStop = [jstop.predicatedArrivalDateTime timeIntervalSinceNow] - secondsToNextStop;

	self.offset = secondsPastPreviousStop * -1;

	// calculate the "now" value
	if (self.parent.accuracyLevel <= 3 && ![next isCityStop])
	{
		NSUInteger distanceForNow = [next.length integerValue] > 20 ? [next.length integerValue] : 20;
		if ([offsetPredictedArrivalDate timeIntervalSinceNow] <= 120 &&								// closer than 2 minutes
			[LocationManager distanceFromLocation:next.location toLocation:newLocation] <= distanceForNow &&						// distance is close enough
			newLocation.speed < 40)																	// tram isn't hurtling past the stop
		{
			if (![self.atStop isEqual:next])
			{
                self.atStop = next;

				// start a timer to kick us on from "now", assuming it hasn't already moved
				[NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(moveOnFromNextStop:) userInfo:nil repeats:NO];
			}
		}
		// are we at the stop but no longer close enough for "next" ?
		else if (self.atStop && [self.atStop isEqual:next])
		{
			// yep, clear the at stop and move to the next stop - we can never go from now->next on the same stop
            self.atStop = nil;

			NSInteger indexOfNextStop = [self.stopList indexOfObject:next];
			if (indexOfNextStop + 1 < [self.stopList count])
			{
				// we can move on to the next stop, reset the offset
                self.offset = 0;
				next = [self.stopList objectAtIndex:indexOfNextStop + 1];
			}
		}
	}
	
	// move to that stop
	[self moveToStop:next];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
	if ([error code] == kCLErrorDenied)
		[manager stopUpdatingLocation];
}

- (void)moveOnFromNextStop:(NSTimer *)aTimer
{
	// if our at stop is still set to the first stop on the journey then move us onward
	if (self.atStop != nil && [self.atStop isEqual:self.nextStop])
	{
		// yes it is, can we move to the next stop?
        self.atStop = nil;
		
		// move it and reset the office
		NSInteger indexOfNextStop = [self.stopList indexOfObject:self.nextStop];
		if (indexOfNextStop + 1 < [self.stopList count])
		{
			// we can move on to the next stop, reset the offset
			[self setOffset:0];
			[self moveToStop:[self.stopList objectAtIndex:indexOfNextStop+1]];
		}
	}
}

/**
 * Find out how far away we are from the next stop using the distance to it
 **/
- (NSTimeInterval)secondsToNextStopWithLocation:(CLLocation *)location
{
	// find the next stop
	Stop *next = [self nextStopWithLocation:location];
	if (next == nil)
		return 0;
	
	// find the previous stop in the list
	NSInteger locationInStopList = [self.stopList indexOfObject:next];
	if (locationInStopList == 0 || locationInStopList == NSNotFound)
		return 0;
	Stop *previous = [self.stopList objectAtIndex:locationInStopList];
	
	// find the distance that we've travelled
	TTDistanceTravelled distance = [previous distanceTravelledToStop:next atLocation:location];
	if (distance.totalDistance == -1 || distance.totalDistance == 0)
		return 0;
	
	// so how much is left to go?
	CGFloat percentageLeftToNextStop = (distance.totalDistance - distance.travelledDistance) / distance.totalDistance;
    
	// work that out
	JourneyStop *jstop = [self.journey journeyStopForStop:next];
	if (jstop == nil)
		return 0;
    
	// so this is the interval until we reach that next stop
	NSTimeInterval secondsToGo = [jstop.predicatedArrivalDateTime timeIntervalSinceNow] * percentageLeftToNextStop;
	
	// all done
	return secondsToGo;
}

/**
 * Find the next stop in the stop list according to the provided location
 **/
- (Stop *)nextStopWithLocation:(CLLocation *)location
{
	// journey times are available
	if ([self.journey.tram isAvailable])
	{
		// we start at the start of the journey - can never go backwards
		if (self.journey.stops == nil || [self.journey.stops count] == 0)
			return nil;
		
		// start counting the location
		NSInteger locationInJourney = 0;
		while (locationInJourney <= 2)
		{
			// gone beyond the end of the journey?
			if ([self.journey.stops count] <= locationInJourney)
				return nil;
            
			JourneyStop *jNext = [self.journey.stops objectAtIndex:locationInJourney];
			NSInteger locationInStopList = [self.stopList indexOfObject:jNext.stop];
            
			// if this is the very first stop in the list then skip it and check the next two
			// we default back to the first anyway if we can't find it elsewhere
			if (locationInStopList == NSNotFound || locationInStopList == 0)
			{
				locationInJourney++;
				continue;
			}
            
			// the "previous" stop according to the stop list
			Stop *previous = [self.stopList objectAtIndex:locationInStopList-1];
			
			// are we between these two stops?
			if ([previous location:location betweenSelfAndStop:jNext.stop])
			{
				// this is our next stop then. Are we so-close to the previous stop that we're likely to be parked at it?
				if ([LocationManager distanceFromLocation:location toLocation:previous.location] <= 20)
					return previous;
				
				// yep, this is our next stop
				return jNext.stop;
			}
            
			// nope, try more
			locationInJourney++;
		}
		
		// nothing, let it go to its default
		return nil;
	} else
	{
		// journey times are not available
		if (self.stopList == nil || [self.stopList count] == 0)
			return nil;
        
		// if we don't have a next stop use the location to find the nearest and use that as our reference
		Stop *next = self.nextStop;
		NSInteger nextIndex = NSNotFound;
		if (next == nil)
		{
			// find the closest
			next = [(StopDistance *)[[StopList sharedManager] getNearestStopToLocation:location withStopList:self.stopList] stop];
			
			// if we have found one use find its index
			if (next != nil)
				nextIndex = [self.stopList indexOfObject:next];
		} else
		{
			// find the index
			nextIndex = [self.stopList indexOfObject:next];
		}
		
		// any good?
		if (next == nil || nextIndex == NSNotFound)
			return nil;
        
		// ok start working forward as normal
		NSInteger locationInStopList = nextIndex;
		while (locationInStopList+1 < [self.stopList count])
		{
			// if this is the very first stop in the list then skip it and check the next two
			// we default back to the first anyway if we can't find it elsewhere
			if (locationInStopList == NSNotFound || locationInStopList == 0)
			{
				locationInStopList++;
				continue;
			}
			
			// the "previous" stop according to the stop list
			Stop *previous = [self.stopList objectAtIndex:locationInStopList-1];
            
			// are we between these two stops?
			if ([previous location:location betweenSelfAndStop:next])
			{
				// this is our next stop then. Are we so-close to the previous stop that we're likely to be parked at it?
				if ([LocationManager distanceFromLocation:location toLocation:previous.location] <= 20)
					return previous;
				
				// yep, this is our next stop
				return next;
			}
			
			// nope, try more
			locationInStopList++;
		}
		
		// nothing, let it go to its default
		return nil;
	}
}


- (BOOL)isGPSEnabled
{
    // make sure we have a journey.
    if (self.journey == nil || [self.journey.stops count] == 0)
        return NO;
    

    JourneyStop * firstStop = self.journey.stops.firstObject;
    
    if ([firstStop.stop isCityStop])
        return NO;

    // otherwise GPS should be on.
    return YES;
}

- (NSIndexPath *)indexPathOfStop:(Stop *)stop
{
	NSInteger section = 0;
	NSInteger row = -1;

    for (NSArray *list in self.suburbStopList)
	{
		row = [list indexOfObject:stop];
		if (row != NSNotFound)
            return [NSIndexPath indexPathForRow:row inSection:section + 1];
		section++;
	}
	return nil;
}

- (void)scrollToStop:(Stop *)stop
{
	NSIndexPath *indexPath = [self indexPathOfStop:stop];
	if (indexPath == nil)
		return;
    
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)moveToStop:(Stop *)stop
{
	if (stop == nil)
		return;
    
	// Find it in the journey
	JourneyStop *jstop = [self.journey journeyStopForStop:stop];
	if (jstop == nil)
		return;
	
	// if it is more than the first stop, make it the first stop
	NSInteger indexOfJourneyStop = [self.journey.stops indexOfObject:jstop];
    
    if (indexOfJourneyStop == NSNotFound)
        return;

	if (indexOfJourneyStop > 0)
		[self.journey setStops:[self.journey.stops subarrayWithRange:NSMakeRange(indexOfJourneyStop, [self.journey.stops count]-indexOfJourneyStop)]];

	// scroll to it
	if (!self.firstAppear && ![stop isEqual:self.nextStop] && !self.hasUserScrolled)
        [self scrollToStop:stop];

    self.nextStop = stop;

	// and refresh the table
	[self.tableView reloadData];
}

- (NSArray *)ticketRetailersList
{
    if (!_ticketRetailersList)
        _ticketRetailersList = [TicketRetailer suburbOutletsFromSuburbStops:self.suburbStopList];
    return _ticketRetailersList;
}

- (void)setAtStop:(Stop *)atStop
{
    _atStop = atStop;
    [self.parent.map setAtStop:atStop];
}

- (void)setNextStop:(Stop *)nextStop
{
    _nextStop = nextStop;
    [self.parent.map setNextStop:nextStop];
}

#pragma mark - StopRightMenuDelegate

- (void)didSelectStopAction:(StopRightMenuType)filterType
{
    if (filterType == StopRightMenuAccess || filterType == StopRightMenuShelter || filterType == StopRightMenuAll)
    {
        NSMutableArray * result = [NSMutableArray array];
        
        for (NSMutableArray *curSuburb in self.suburbStopList)
        {
            NSArray     * resultSuburb = [StopRightFilterViewController filterArray:curSuburb filterType:filterType string:nil];
            
            if (resultSuburb.count)
                [result addObject:resultSuburb];
        }
        self.data = result;
    }
    else if (filterType == StopRightMenuOutlets)
        self.data = self.ticketRetailersList;
    
    self.listType = filterType;
    [self.tableView reloadData];
}

@end
