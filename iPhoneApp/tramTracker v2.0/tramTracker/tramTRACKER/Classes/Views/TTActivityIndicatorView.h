//
//  TTActivityIndicatorView.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 6/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTActivityIndicatorView : UIView

+ (TTActivityIndicatorView *)activityIndicatorForView:(UIView *)view animated:(BOOL)animated;
- (void)stopAnimatingAnimated:(BOOL)animated;
- (void)stopAnimating;
- (void)startAnimatingAnimated:(BOOL)animated;

@end
