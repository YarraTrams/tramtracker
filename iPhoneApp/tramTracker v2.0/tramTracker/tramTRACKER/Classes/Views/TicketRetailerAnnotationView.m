//
//  TicketRetailerAnnotationView.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 20/11/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "TicketRetailerAnnotationView.h"
#import "Constants.h"
#import "TicketRetailer.h"

@implementation TicketRetailerAnnotationView


- (id)initWithAnnotation:(id <MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
	if (self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier])
	{
		// Set the right callout view to a detail disclosure button
		UIButton *disclosure = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];

        self.rightCalloutAccessoryView = disclosure;
		[disclosure addTarget:self
                       action:@selector(didTouchTicketCalloutAccessory:)
             forControlEvents:UIControlEventTouchUpInside];

		self.canShowCallout = YES;
        
        if ([annotation isKindOfClass:[TicketRetailer class]])
            self.image = [UIImage imageNamed:@"Tickets-Pin"];
        else
            self.image = [UIImage imageNamed:@"POI-Pin"];
	}
	return self;
}

- (void)didTouchTicketCalloutAccessory:(id)sender
{
	[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTicketOutletInfoTouchedInPinOnMap
                                                        object:self.annotation];
}

@end
