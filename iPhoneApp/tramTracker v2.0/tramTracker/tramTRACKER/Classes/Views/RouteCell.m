//
//  RouteCell.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 9/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "RouteCell.h"

@interface RouteCell()

@property (weak, nonatomic) IBOutlet UILabel *title;


@end

@implementation RouteCell

- (void)configureWithText:(NSString *)aText
{
    self.title.text = aText;
}

@end
