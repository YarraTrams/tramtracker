//
//  PIDCell.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 21/11/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "PIDCell.h"
#import "Prediction.h"
#import "TTTools.h"

@interface PIDCell()

@property (weak, nonatomic) IBOutlet UILabel        * timeLeft;
@property (weak, nonatomic) IBOutlet UILabel        * tramNo;
@property (weak, nonatomic) IBOutlet UILabel        * title;

@property (weak, nonatomic) IBOutlet UIImageView    * expandImage;
@property (weak, nonatomic) IBOutlet UIImageView    * collapseImage;
@property (weak, nonatomic) IBOutlet UIImageView    * tram;

@property (weak, nonatomic) IBOutlet UIImageView    * disruptions;
@property (weak, nonatomic) IBOutlet UIImageView    * accessible;
@property (weak, nonatomic) IBOutlet UIButton       * rightButton;
@property (weak, nonatomic) IBOutlet UIImageView    * aircon;
@property (weak, nonatomic) IBOutlet UIImageView    * special;

@property (weak, nonatomic) Prediction              * prediction;

@property (weak, nonatomic) id                      target;
@property SEL                                       action;

@end

@implementation PIDCell

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    if (self = [super initWithCoder:aDecoder]) {
        for (UIView *currentView in self.subviews)
        {
            if ([currentView isKindOfClass:[UIScrollView class]])
            {
                ((UIScrollView *)currentView).delaysContentTouches = NO;
                break;
            }
        }
    }
    return self;
}

- (void)configureWithPrediction:(PredictionStub *)prediction target:(id)target action:(SEL)action
{
    /*
     * Manage main data
     */

    self.timeLeft.text = [Prediction minutesUntilArrivalTime:prediction.predictedArrivalDateTime];
    self.tramNo.text = prediction.headboardRouteNumber;
    self.title.text = prediction.destination;
    self.tram.image = self.tram.highlightedImage = prediction.tramStub.image;

    /*
     * Manage right icons
     */

    self.disruptions.hidden = !prediction.disrupted;
    self.accessible.hidden = !prediction.lowFloor;
    self.aircon.hidden = !prediction.displayAirConditioning;
    self.special.hidden = !prediction.hasSpecialEvent;

    /*
     * Manage Views
     */
    
    if (prediction.tramStub.number.integerValue > 0)
    {
        self.action = action;
        self.target = target;
        self.rightButton.hidden = NO;
    }
    else
    {
        self.action = nil;
        self.target = nil;
        self.rightButton.hidden = YES;
    }

    [self setAccessibilityLabel:prediction.voiceOver];
    self.prediction = prediction;
}

- (void)manageExpandCollapseArrows:(BOOL)isExpanded index:(NSInteger)index size:(NSInteger)size isExpandable:(BOOL)isExpandable
{
    if (size == 1 || (isExpanded && index != size - 1) || !isExpandable)
    {
        self.collapseImage.hidden = YES;
        self.expandImage.hidden = YES;
    }
    else if (isExpanded && index == size - 1)
    {
        self.expandImage.hidden = NO;
        self.collapseImage.hidden = YES;
    }
    else if (!isExpanded && size > 1)
    {
        self.collapseImage.hidden = NO;
        self.expandImage.hidden = YES;
    }
    else if (!isExpanded)
    {
        self.collapseImage.hidden = YES;
        self.expandImage.hidden = YES;
    }
}

#pragma mark - Actions

- (IBAction)didSelectAccessoryButton:(id)sender
{
    [self.target performSelector:self.action withObject:self.prediction afterDelay:0];
}

@end
