//
//  myTramCell.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 27/11/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Stop.h"
#import "Journey.h"

@interface myTramCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel        * estimatedTime;

- (void)configureWithJourneyStop:(JourneyStop *)journeyStop isSelected:(BOOL)isSelected routeImage:(NSString *)routeImage;
- (void)configureWithStop:(Stop *)stop routeImage:(NSString *)routeImage;
- (void)configureWithJourney:(Journey *)journey;
- (void)configureFromRouteWithStop:(Stop *)stop routeImage:(NSString *)routeImage;
- (void)configureWithRetailer:(TicketRetailer *)retailer;
- (void)setTurnIndicatorImage:(UIImage *)aImage;

@end
