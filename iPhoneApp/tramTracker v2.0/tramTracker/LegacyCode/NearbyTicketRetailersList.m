//
//  NearbyTicketRetailersList.m
//  tramTRACKER
//
//  Created by Robert Amos on 26/07/10.
//  Copyright 2010 Metro Trains Melbourne. All rights reserved.
//

#import "NearbyTicketRetailersList.h"
#import "Stop.h"
#import "TicketRetailer.h"
#import "NearbyTicketRetailerCell.h"
#import "TicketRetailerViewController.h"

@implementation NearbyTicketRetailersList

@synthesize retailerList, stop;

#pragma mark -
#pragma mark Initialization

- (id)initWithStop:(Stop *)aStop
{
	if ((self = [super initWithStyle:UITableViewStylePlain]))
	{
		// Find our Nearby Ticket Retailers
		[self setStop:aStop];
		[self setRetailerList:[TicketRetailer nearestTicketRetailersToLocation:aStop.location count:10]];
	}
	return self;
}

/*
- (id)initWithStyle:(UITableViewStyle)style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    if ((self = [super initWithStyle:style])) {
    }
    return self;
}
*/

#pragma mark -
#pragma mark View lifecycle

//
// Create the table footer
//
- (UIView *)tableFooterView
{
	CGRect frame = CGRectMake(0, 0, self.tableView.frame.size.width, 10);
	UnderBackgroundView *ub = [[UnderBackgroundView alloc] initWithFrame:frame];
	return ub;
}


- (void)viewDidLoad {
    [super viewDidLoad];

	// Reset our background colour
	[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];

	[self.tableView setTableFooterView:[self tableFooterView]];
}


/*
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
*/
/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.retailerList count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    NearbyTicketRetailerCell *cell = (NearbyTicketRetailerCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[NearbyTicketRetailerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
		[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    }
	
	TicketRetailerDistance *retailerDistance = (TicketRetailerDistance *)[self.retailerList objectAtIndex:indexPath.row];
    
    // Configure the cell...
    [cell.name setText:retailerDistance.retailer.name];
	[cell.address setText:retailerDistance.retailer.address];
	[cell.distance setText:[retailerDistance formattedDistance]];
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.

	// get the ticket retailer, create a view for it and throw it on the stack
	TicketRetailerDistance *retailerDistance = (TicketRetailerDistance *)[self.retailerList objectAtIndex:indexPath.row];
	
	// Create the view
	TicketRetailerViewController *retailerViewController = [[TicketRetailerViewController alloc] initWithTicketRetailer:retailerDistance.retailer];
	[self.navigationController pushViewController:retailerViewController animated:YES];
	 retailerViewController = nil;}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}




@end

