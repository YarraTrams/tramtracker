//
//  NearbyActionMenuViewController.h
//  tramTRACKER
//
//  Created by Tom King on 30/10/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RightMenuDelegate.h"

@protocol NearbyActionMenuDelegate

- (void)didSelectAllStops;
- (void)didSelectStopsWithShelter;
- (void)didSelectEasyAccessStops;
- (void)didSelectTicketOutlets;
- (void)didSelectEnterTrackerID;
- (void)didSelectSearch;

@end

@interface NearbyActionMenuViewController : UITableViewController

@property (nonatomic, weak) id<NearbyActionMenuDelegate> delegate;

+ (NearbyActionMenuViewController *)viewControllerWithDefaultNib;

@end
