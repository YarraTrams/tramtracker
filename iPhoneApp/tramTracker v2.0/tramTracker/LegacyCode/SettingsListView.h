//
//  SettingsListView.h
//  tramTRACKER
//
//  Created by Robert Amos on 17/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SimpleCell.h"
#import "StopListCountSelector.h"
#import "ShowMessagesSetting.h"
#import "StartupOptionSelector.h"


@interface SettingsListView : UITableViewController {
	BOOL backgroundEnabled;
}

- (NSString *)openWithStringValue;
- (NSString *)nearbyStopCountStringValue;
- (NSString *)mostRecentStopCountStringValue;
- (NSString *)showMessagesStringValue;
- (NSString *)synchronisationStringValue;
- (BOOL)jumpToNearestStopValue;
- (BOOL)useGPSValue;
- (BOOL)showTurnIndicators;
- (BOOL)showTramProfiles;
- (BOOL)smartRestore;

/**
 * ARRIVAL ALARM SUPPORT
 *
 * These can be repurposed into controls for the push alarms?
- (BOOL)backgroundEnabled;
- (BOOL)backgroundAlertMessage;
- (NSString *)backgroundAlertSoundType;
- (NSString *)backgroundAlertSoundTypeStringValue;
**/

- (void)pushChangeShowMessages;
- (void)pushNearbyStopCountSelection;
- (void)pushMostRecentStopCountSelection;
- (void)pushStartupOptionSelection;
- (void)pushSynchronisationOptionSelection;


// ARRIVAL ALARM SUPPORT - (void)pushBackgroundAlertSoundOptionSelection;

- (void)toggleJumpToNearestStop;
- (void)toggleUseGPS;
- (void)toggleTurnIndicators;
- (void)toggleTramProfiles;
- (void)toggleSmartRestore;

- (void)setShowMessages:(NSNumber *)showMessageOption;
- (void)setNearbyStopCount:(NSNumber *)nearbyStopCount;
- (void)setMostRecentStopCount:(NSNumber *)mostRecentStopCount;
- (void)setStartupOption:(NSNumber *)startupOption;
- (void)setSynchronisationOption:(NSNumber *)syncOption;

/**
 * ARRIVAL ALARM SUPPORT
 *
- (void)toggleBackgroundEnabled;
- (void)toggleBackgroundAlertMessage;
- (void)setBackgroundAlertSoundType:(NSString *)soundType;
**/

@end
