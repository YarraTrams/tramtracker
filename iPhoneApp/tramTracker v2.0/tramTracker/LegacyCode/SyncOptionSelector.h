//
//  SyncOptionSelector.h
//  tramTRACKER
//
//  Created by Robert Amos on 14/09/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SyncOptionSelector : UITableViewController {
	NSInteger syncOption;
	id target;
	SEL action;
	NSArray *syncOptions;
	
}

- (id)initWithSyncOption:(NSInteger)aSyncOption target:(id)aTarget action:(SEL)anAction;

@end
