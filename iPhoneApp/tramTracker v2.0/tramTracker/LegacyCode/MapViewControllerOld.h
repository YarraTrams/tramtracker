//
//  MapViewController.h
//  tramTRACKER
//
//  Created by Robert Amos on 19/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "StopAnnotationView.h"
#import "PIDViewController.h"
#import "RightMenuDelegate.h"

typedef enum
{
    MapViewControllerTypeOldNone = 0,
    MapViewControllerTypeOldStopList = 1,
    MapViewControllerTypeOldNearbyList = 2,
    MapViewControllerTypeOldFavouriteList,
    MapViewControllerTypeOldMostRecent,
    MapViewControllerTypeOldBrowseList,
    MapViewControllerTypeOldConnectionList,
    MapViewControllerTypeOldSelectedStop,
    MapViewControllerTypeOldNearbyTickets,
    MapViewControllerTypeOldMyTram
}   MapViewControllerTypeOld;

@class StopListController;
@class RouteListController;
@class MainListController;
@class NearbyListController;
@class MostRecentController;
@class tramTRACKERAppDelegate;
@class Route;
@class Stop;
@class TicketRetailer;
@class NearbyViewControllerOld;
@class ActivityBarButtonController;
@class LocationManager;

/**
 * @defgroup Mapping Mapping
**/

/**
 * @ingroup Mapping
**/

/**
 * Displays a Map thats centered around a list of stops
**/
@interface MapViewControllerOld : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate, StopRightMenuDelegate>

/**
 * Outlets
 **/
@property (weak, nonatomic) IBOutlet MKMapView  * mapView;


/**
 * Selected Items
 **/
@property (nonatomic, strong) Stop              * selectedStop;
@property (nonatomic, strong) TicketRetailer    * selectedRetailer;

/**
 * The route we're displaying, if any
 **/
@property (nonatomic, strong) Route             * route;


/**
 * Location
 **/
@property (nonatomic, strong) LocationManager   * locationManager;

/**
 * Data Arrays
 **/
@property (nonatomic, strong) NSArray           * stopList;
@property (nonatomic, strong) NSArray           * purpleStops;
@property (nonatomic, strong) NSArray           * retailerList;


/**
 * Dates
 **/
@property (nonatomic, strong) NSDate            * arrivalDate;
@property (nonatomic, strong) NSDate            * locatingStarted;

/**
 * The List view type
 **/

@property (nonatomic) MapViewControllerTypeOld     listViewType;

@property (nonatomic) BOOL                      pushToScheduledDepartures;
@property (nonatomic) BOOL                      trackUserLocation;
@property (nonatomic) BOOL                      showTicketRetailers;
@property (nonatomic) BOOL                      manuallyTracking;
@property (nonatomic) BOOL                      selectedStopShouldBeShown;

//@property (nonatomic, strong) UIBarButtonItem   * ticketsButton;
//@property (weak, readonly, nonatomic) NearbyViewControllerOld *nearbyViewController;
//@property (nonatomic, strong) UIButton *locateButton;
//@property (nonatomic, strong) UIBarButtonItem *stopButton;
//@property (nonatomic, strong) ActivityBarButtonController *activityController;

/**
 * Custom Initialiser
**/
//- (id)initWithStopList:(NSArray *)stopList;
//- (id)initNearbyMapWithNearbyViewController:(NearbyViewControllerOld *)pVC;

/**
 * Find the region that fits our stops
**/
- (MKCoordinateRegion)regionThatFitsStops:(NSArray *)stops;

/**
 * Callback when someone has clicked on the disclosure indicator
**/
- (void)didTouchRealTimeCalloutAccessory:(NSNotification *)note;
- (void)didTouchScheduledCalloutAccessory:(NSNotification *)note;
- (void)didTouchTicketCalloutAccessory:(NSNotification *)note;
- (void)pushPIDForStop:(Stop *)aStop;

/**
 * Flip back to a list view
**/
//- (void)flipToListView;

/**
 * Show or hide the ticket retailers as appropriate
**/
- (void)displayTicketRetailers;
- (void)hideTicketRetailers;
- (void)toggleTicketRetailers;

- (void)saveCurrentMapRegion;
- (void)stopTracking;

- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)userLocation;

- (void)setFinalJourney:(Journey *)journey;


@end
