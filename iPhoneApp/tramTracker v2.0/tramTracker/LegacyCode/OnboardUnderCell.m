//
//  OnboardUnderCell.m
//  tramTRACKER
//
//  Created by Robert Amos on 7/10/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "OnboardUnderCell.h"
#import "OnboardUnderCellGradient.h"


@implementation OnboardUnderCell

@synthesize delegate, stop, alarmButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier allowsAlarm:(BOOL)alarm
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        // Initialization code
		[self setSelectionStyle:UITableViewCellSelectionStyleNone];
		
		OnboardUnderCellGradient *background = [[OnboardUnderCellGradient alloc] initWithFrame:CGRectZero];
		[self setBackgroundView:background];
		
		
		// Create the Map Icon
		UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
		[button addTarget:self action:@selector(showConnections) forControlEvents:UIControlEventTouchUpInside];
		[button setImage:[UIImage imageNamed:@"map.png"] forState:UIControlStateNormal];
		[button setTitle:NSLocalizedString(@"onboard-button-connections", nil) forState:UIControlStateNormal];
		[button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
		[button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
		[button setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
		CGRect mapRect = CGRectMake(100, 2, 100, 40);
		[button setFrame:mapRect];
		[self.contentView addSubview:button];
		

		// do we support multitasking, and background tasks, and is it even allowed?
		if (alarm)
		{
			// find out about the arrival alarm
			NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
			if ([defaults boolForKey:@"backgroundEnabled"])
			{
				// yep, relocate the map button, and add an arrival alarm button
				CGRect newMapRect = CGRectMake(20, 2, 100, 40);
				[button setFrame:newMapRect];
				
				// create an arrival alarm button
				UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
				[aButton setImage:[UIImage imageNamed:@"AlarmIconLarge.png"] forState:UIControlStateNormal];
				[aButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
				[aButton.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
				[aButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
				CGRect alarmRect = CGRectMake(160, 2, 100, 40);
				[aButton setFrame:alarmRect];

				[self setAlarmButton:aButton];
				[self.contentView addSubview:aButton];
			}
		}
		
		// Create the "close" button
		UIImageView *closeButton = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ButtonPanelClose.png"]];
		[closeButton setFrame:CGRectMake(300, 14, 15, 15)];
		[self.contentView addSubview:closeButton];
    }
    return self;
}

- (void)showConnections
{
	if (self.stop != nil && self.delegate != nil && [self.delegate respondsToSelector:@selector(showConnectionsForStop:)])
	{
		[self.delegate performSelector:@selector(showConnectionsForStop:) withObject:self.stop];
	}
}

- (void)setArrivalAlarm
{
	if (self.stop != nil && self.delegate != nil && [self.delegate respondsToSelector:@selector(setArrivalAlarmAtStop:)])
		[self.delegate performSelector:@selector(setArrivalAlarmAtStop:) withObject:self.stop];
}

- (void)cancelArrivalAlarm
{
	if (self.stop != nil && self.delegate != nil && [self.delegate respondsToSelector:@selector(cancelArrivalAlarm)])
		[self.delegate performSelector:@selector(cancelArrivalAlarm)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}




@end
