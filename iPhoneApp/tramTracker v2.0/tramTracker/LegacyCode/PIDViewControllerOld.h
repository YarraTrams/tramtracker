//
//  PIDViewController.h
//  tramTRACKER
//
//  Created by Robert Amos on 17/02/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Stop.h"
#import "Prediction.h"
#import "StopList.h"
#import "PidsService.h"
#import "PredictionListView.h"
#import <QuartzCore/QuartzCore.h>
#import "SpecialMessage.h"
#import "DepartureListController.h"

@class PagedPIDViewController;
@class SwipeCaptureView;
@class Route;
@class Filter;
@class VoiceOverButton;

extern int const PIDDefaultStop;
typedef NSInteger PIDActionSheetAction;
extern const PIDActionSheetAction PIDActionSheetActionAddToFavourites;
extern const PIDActionSheetAction PIDActionSheetActionEdit;
extern const PIDActionSheetAction PIDActionSheetActionScheduledDepartures;
extern const PIDActionSheetAction PIDActionSheetActionSaveFilter;
extern const PIDActionSheetAction PIDActionSheetActionClearFilter;
extern const PIDActionSheetAction PIDActionSheetActionStopMap;
extern const PIDActionSheetAction PIDActionSheetActionNearbyTicketRetailers;
extern const PIDActionSheetAction PIDActionSheetActionDirections;

/**
 * @defgroup PIDS PIDS
**/

/**
 * @ingroup PIDS
**/

/**
 * A PID is a self-updating view designed to be a soft implementation of the hardware PIDS that
 * have been physically installed around the city.
 *
 * The PIDViewController contains methods for updating the contents of the PID, for refreshing
 * the data displayed on the PID, and for finding the nearest PID to a location.
**/
@interface PIDViewControllerOld : UIViewController <CLLocationManagerDelegate, UIActionSheetDelegate, UIAlertViewDelegate>
{

	/**
	 * @name		Items inside the PIDView NIB file.
	 * @{
	**/
	
	/**
	 * The title of the PID. This is the large white text at the top.
	 * It is usually the stop name.
	**/
	UILabel *stopTitle;

	/**
	 * The subtitle of the PID. This is the text under the title
	 * and is generally the cross street and city direction.
	**/
	UILabel *stopSubtitle;

	/**
	 * The Tracker ID label to display the Tracker ID
	**/
	UILabel *stopTrackerID;

	/**
	 * The label to display the stop number
	**/
	UILabel *stopID;

	/**
	 * A View representing the contents of the PID's "screen"
	**/
	UIView *screen;

	/**
	 * A spinning wheel used to show loading.
	**/
	UIActivityIndicatorView *loading;

	/**
	 * A view representing the a disruption screen.
	**/
	UIView *disruptedScreen;

	/**
	 * The text displayed inside a disruption screen.
	**/
	UITextView *disruptedMessage;
	
	/**
	 * A view representing a special event screen.
	**/
	UIView *specialEventScreen;
	
	/**
	 * The text displayed inside a special event screen.
	**/
	UITextView *specialEventMessage;

	/**
	 * The label for the Route number displayed on the first line of the results screen.
	**/
	UILabel *screenRow1Route;

	/**
	 * The label for the destination displayed on the first line of the results screen.
	**/
	UILabel *screenRow1Destination;

	/**
	 * The label for the arrival time displayed on the first line of the results screen.
	**/
	UILabel *screenRow1Arrival;

	/**
	 * The icon used to indicate that the prediction on the first line of the results screen is for a low floor tram.
	**/
	UIImageView *screenRow1LowFloor;
	
	/**
	 * The icon used to indicate that the prediction on the first line of the results screen is for an air conditioned tram.
	**/
	UIImageView *screenRow1AirConditioned;

	/**
	 * The icon used to indicate that the prediction on the first line of the results screen is for a disrupted route.
	**/
	UIImageView *screenRow1Disrupted;
	
	/**
	 * The icon used to indicate that the prediction on the first line of the results screen is for a route affected by a special event.
	**/
	UIImageView *screenRow1SpecialEvent;
	
	/**
	 * A transparent view that sits over a prediction and improves readability using Voice Over
	**/
	VoiceOverButton *screenRow1VoiceOver;
	
	/**
	 * An icon that appears in the bottom right corner of the arrival and indicates you can touch to see Onboard.
	**/
	UIImageView *screenRow1PIDToOnboardIcon;

	/**
	 * The label for the route number displayed on the second line of the results screen.
	**/
	UILabel *screenRow2Route;

	/**
	 * The label for the destination displayed on the second line of the results screen.
	**/
	UILabel *screenRow2Destination;

	/**
	 * The label for the arrival time displayed on the second line of the results screen.
	**/
	UILabel *screenRow2Arrival;

	/**
	 * The icon used to indicate that the prediction on the second line of the results screen is for a low floor tram.
	**/
	UIImageView *screenRow2LowFloor;

	/**
	 * The icon used to indicate that the prediction on the second line of the results screen is for an air conditioned tram.
	**/
	UIImageView *screenRow2AirConditioned;
	
	/**
	 * The icon used to indicate that the prediction on the second line of the results screen is for a disrupted route.
	**/
	UIImageView *screenRow2Disrupted;

	/**
	 * The icon used to indicate that the prediction on the second line of the results screen is for a route affected by a special event.
	 **/
	UIImageView *screenRow2SpecialEvent;
	
	/**
	 * A transparent view that sits over a prediction and improves readability using Voice Over
	 **/
	VoiceOverButton *screenRow2VoiceOver;

	/**
	 * An icon that appears in the bottom right corner of the arrival and indicates you can touch to see Onboard.
	 **/
	UIImageView *screenRow2PIDToOnboardIcon;
	
	/**
	 * The label for the route number displayed on the third line of the results screen
	**/
	UILabel *screenRow3Route;
	
	/**
	 * The label for the destination displayed on the third line of the results screen.
	**/
	UILabel *screenRow3Destination;

	/**
	 * The label for the arrival time displayed on the third line of the results screen.
	**/
	UILabel *screenRow3Arrival;

	/**
	 * The icon used to indicate that the prediction on the third line of the results screen is for a low floor tram.
	**/
	UIImageView *screenRow3LowFloor;

	/**
	 * The icon used to indicate that the prediction on the third line of the results screen is for an air conditioned tram.
	**/
	UIImageView *screenRow3AirConditioned;
	
	/**
	 * The icon used to indicate that the prediction on the third line of the results screen is for a disrupted route.
	**/
	UIImageView *screenRow3Disrupted;

	/**
	 * The icon used to indicate that the prediction on the third line of the results screen is for a route affected by a special event.
	 **/
	UIImageView *screenRow3SpecialEvent;

	/**
	 * A transparent view that sits over a prediction and improves readability using Voice Over
	 **/
	VoiceOverButton *screenRow3VoiceOver;

	/**
	 * An icon that appears in the bottom right corner of the arrival and indicates you can touch to see Onboard.
	 **/
	UIImageView *screenRow3PIDToOnboardIcon;
	
	/**
	 * The Silhouette for the first result
	**/
	UIImageView *screenRow1Silhouette;

	/**
	 * The Silhouette for the second result
	 **/
	UIImageView *screenRow2Silhouette;

	/**
	 * The Silhouette for the third result
	 **/
	UIImageView *screenRow3Silhouette;
	
	/**
	 * The label for the clock in the bottom right hand corner of the screen
	**/
	UILabel *clock;

	/**
	 * The green light indicator in the bottom left hand screen. Generally means that we've
	 * received a fresh set of predictions in the last 30 seconds.
	**/
	UIImageView *indicatorPlaying;

	/**
	 * The yellow "warning" light indicator in the bottom left hand corner of the screen. Generally means
	 * that is has been more than 30 seconds since our last prediction update. If this appears it could mean
	 * a loss of connectivity.
	**/
	UIImageView *indicatorWarning;
	
	/**
	 * The red "paused" light indicator in the bottom left hand corner of the screen. Generally
	 * means that it has been more than 2 minutes since our last prediction update and that we consider
	 * communications disrupted. We continue counting down arrivals based on the last prediction.
	**/
	UIImageView *indicatorPaused;
	
	/**
	 * @}
	**/

	/**
	 * The NSTimer used to update the clock. Fires every second.
	**/
	NSTimer *timerClock;

	/**
	 * The timer used to change the page to a different set of results. For single-page sets of results
	 * this is used to update the results on the screen. The play/warning/pause indicator is changed based
	 * on cycles of this timer. Fires every 4 seconds.
	**/
	NSTimer *timerPage;

	/**
	 * The current page of results that is being displayed on the screen. Is an index of the currentPredictions NSArray.
	**/
	int currentPage;

	/**
	 * The current iteration since the last set of prediction updates. Used by the page timer to know
	 * when to change the indicator lights.
	**/
	int iterationCount;

	/**
	 * The message that was last displayed on the screen. Is an index of the messages NSArray.
	**/
	int currentMessage;
	
	/**
	 * The current stop as being displayed by the PID title/subtitle/etc. Used by the refresh timer to get new predictions.
	**/
	Stop *currentStop;

	/**
	 * The current set of predictions being displayed on the screen. The page timer selects a "page" of results
	 * from this array to be displayed. This array could be updated at any time by the refresh timer.
	**/
	NSArray *currentPredictions;
	
	/**
	 * The label for the distance displayed at the bottom of the PID. Used on the "Nearby PID" only.
	**/
	UILabel *distance;
	
	/**
	 * The Paged PID that is controlling this PID, if any. This is used to pass messages back up to the Paged PID controller
	 * so that UI changes and interaction is handled correctly and is not isolated to this PID only.
	**/
	PagedPIDViewController *parent;

	/**
	 * A transparent view that sits on top of a PID when it is under the control of a PagedPID.
	 * This is used to capture touch events and pass them through ourselves to the Paged PID so that
	 * we can swipe left and right to change PIDs.
	**/
	SwipeCaptureView *swipeCapture;
	
	/**
	 * An instance of the PidsService used to obtain data from the PIDS Service.
	**/
	PidsService *service;

	/**
	 * The timer to control the refresh/update of the predictions. Fires every 15 seconds.
	**/
	NSTimer *timerRefresh;

	/**
	 * A location manager as used by the location functions to display a PID that is nearest to the user.
	**/
	LocationManager *locationManager;
	LocationManager *directionLocationManager;
	CLLocation *directionLocation;

	/**
	 * Whether our timers have been stopped.
	**/
	BOOL hasBeenStopped;

	/**
	 * The messages to display in between result screens. The page timer will alternate between the predictions
	 * and messages. Contains SpecialMessage objects.
	**/
	NSArray *messages;
	
	/**
	 * Whether we are showing only low floor trams in this PID.
	**/
	Filter *filter;
	
	/**
	 * The welcome screen
	**/
	UIView *welcomeScreen;

	int trackerID;
	UINavigationController *oldNavigationController;
	NSInteger showMessages;
	UIActionSheet *actionSheet;
	NSInteger messageCycleCount;
	
	UILabel *filterLabel;
	UIImageView *lowFloorFilter;
	UIView *routeFilter;
	UILabel *routeFilterLabel;
	NSMutableArray *actionButtons;
	UIView *errorScreen;
	UITextView *errorMessage;
	BOOL editing;
	NSDate *locatingStarted;
	UIImageView *locating;
	UIImageView *locatingBG;
	NSIndexPath *favouriteStopIndexPath;
	BOOL shouldShowSilhouettes;
	
	UIImageView *mr2Logo;
	UIImageView *mr3Logo;
}

//
// Labels that describe the current stop
//
@property (nonatomic, strong) IBOutlet UILabel *stopTitle;
@property (nonatomic, strong) IBOutlet UILabel *stopSubtitle;
@property (nonatomic, strong) IBOutlet UILabel *stopTrackerID;
@property (nonatomic, strong) IBOutlet UILabel *stopID;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

//
// Subviews within the NIB, controls what is shown on the PID screen
//
@property (nonatomic, strong) IBOutlet UIView *screen;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *loading;
@property (nonatomic, strong) IBOutlet UIView *disruptedScreen;
@property (nonatomic, strong) IBOutlet UITextView *disruptedMessage;
@property (nonatomic, strong) IBOutlet UIView *specialEventScreen;
@property (nonatomic, strong) IBOutlet UITextView *specialEventMessage;

//
// The first row of predictions on the screen
//
@property (nonatomic, strong) IBOutlet UILabel *screenRow1Route;
@property (nonatomic, strong) IBOutlet UILabel *screenRow1Destination;
@property (nonatomic, strong) IBOutlet UILabel *screenRow1Arrival;
@property (nonatomic, strong) IBOutlet UIImageView *screenRow1LowFloor;
@property (nonatomic, strong) IBOutlet UIImageView *screenRow1AirConditioned;
@property (nonatomic, strong) IBOutlet UIImageView *screenRow1Disrupted;
@property (nonatomic, strong) IBOutlet UIImageView *screenRow1SpecialEvent;
@property (nonatomic, strong) IBOutlet UIImageView *screenRow1Silhouette;
@property (nonatomic, strong) IBOutlet VoiceOverButton *screenRow1VoiceOver;
@property (nonatomic, strong) IBOutlet UIImageView *screenRow1PIDToOnboardIcon;

//
// The second row of predictions on the screen
//
@property (nonatomic, strong) IBOutlet UILabel *screenRow2Route;
@property (nonatomic, strong) IBOutlet UILabel *screenRow2Destination;
@property (nonatomic, strong) IBOutlet UILabel *screenRow2Arrival;
@property (nonatomic, strong) IBOutlet UIImageView *screenRow2LowFloor;
@property (nonatomic, strong) IBOutlet UIImageView *screenRow2AirConditioned;
@property (nonatomic, strong) IBOutlet UIImageView *screenRow2Disrupted;
@property (nonatomic, strong) IBOutlet UIImageView *screenRow2SpecialEvent;
@property (nonatomic, strong) IBOutlet UIImageView *screenRow2Silhouette;
@property (nonatomic, strong) IBOutlet VoiceOverButton *screenRow2VoiceOver;
@property (nonatomic, strong) IBOutlet UIImageView *screenRow2PIDToOnboardIcon;

//
// The third row of predictions on the screen
//
@property (nonatomic, strong) IBOutlet UILabel *screenRow3Route;
@property (nonatomic, strong) IBOutlet UILabel *screenRow3Destination;
@property (nonatomic, strong) IBOutlet UILabel *screenRow3Arrival;
@property (nonatomic, strong) IBOutlet UIImageView *screenRow3LowFloor;
@property (nonatomic, strong) IBOutlet UIImageView *screenRow3AirConditioned;
@property (nonatomic, strong) IBOutlet UIImageView *screenRow3Disrupted;
@property (nonatomic, strong) IBOutlet UIImageView *screenRow3SpecialEvent;
@property (nonatomic, strong) IBOutlet UIImageView *screenRow3Silhouette;
@property (nonatomic, strong) IBOutlet VoiceOverButton *screenRow3VoiceOver;
@property (nonatomic, strong) IBOutlet UIImageView *screenRow3PIDToOnboardIcon;

//
// The bottom row with the clock and status indicators
//
@property (nonatomic, strong) IBOutlet UILabel *clock;
@property (nonatomic, strong) IBOutlet UIImageView *indicatorPlaying;
@property (nonatomic, strong) IBOutlet UIImageView *indicatorWarning;
@property (nonatomic, strong) IBOutlet UIImageView *indicatorPaused;

//
// The timers used to control the clock and page refreshing
//
@property (nonatomic, strong) NSTimer *timerClock;
@property (nonatomic, strong) NSTimer *timerPage;

//
// The information that we are displaying
//
@property (nonatomic, strong) NSArray *currentPredictions;
@property (nonatomic, strong) Stop *currentStop;

@property (nonatomic, strong) IBOutlet UILabel *filterLabel;
@property (nonatomic, strong) IBOutlet UIImageView *lowFloorFilter;
@property (nonatomic, strong) IBOutlet UIView *routeFilter;
@property (nonatomic, strong) IBOutlet UILabel *routeFilterLabel;
@property (nonatomic, strong) PagedPIDViewController *parent;
@property (nonatomic, strong) IBOutlet UIView *errorScreen;
@property (nonatomic, strong) IBOutlet UITextView *errorMessage;

//
// Controlling information
//
@property (nonatomic, strong) PidsService *service;
@property (nonatomic, strong) NSTimer *timerRefresh;
@property (nonatomic) int trackerID;
@property (nonatomic, strong) LocationManager *locationManager;
@property (nonatomic) BOOL hasBeenStopped;
@property (nonatomic, strong) IBOutlet SwipeCaptureView *swipeCapture;
@property (nonatomic, strong) NSArray *messages;
@property (nonatomic, strong) Filter *filter;
@property (nonatomic) NSInteger showMessages;
@property (nonatomic, strong) IBOutlet UIView *welcomeScreen;
@property (nonatomic, strong) UIActionSheet *actionSheet;
@property (nonatomic, strong) NSMutableArray *actionButtons;
@property (nonatomic) BOOL editing;
@property (nonatomic, strong) NSDate *locatingStarted;
@property (nonatomic, strong) IBOutlet UIImageView *locating;
@property (nonatomic, strong) IBOutlet UIImageView *locatingBG;
@property (nonatomic, strong) NSIndexPath *favouriteStopIndexPath;

@property (nonatomic, strong) IBOutlet UIImageView *mr2Logo;
@property (nonatomic, strong) IBOutlet UIImageView *mr3Logo;


/**
 * The user touched the PID. Fade the navigation and tab bars on/off.
**/
- (IBAction) toggleNavigation:(id)sender;

/**
 * The user touched the Flip button in the bottom left hand corner.
 *
 * If we're under the control of a PagedPID pass it the message, otherwise replace
 * ourselves on the navigation controller with a table view of options.
**/
- (IBAction) flipToListView:(id)sender;

/**
 * Fade the navigation bar and tab bar off.
**/
- (void) hideNavigation:(BOOL)animated;

/**
 * Fade the navigation bar and the tab bar on.
**/
- (void) showNavigation:(BOOL)animated;

/**
 * Adds the current stop to the favourites list and refreshes the interface (to hide the button)
**/
- (void) addToFavourites:(id)sender;

/**
 * Shows the loading indicator.
**/
- (void)showLoading;

/**
 * Hides the loading indicator
**/
- (void)hideLoading;

/**
 * Shows the screen view and restarts the display timers (and not stopped).
 * Will also call -hideLoading if the loading indicator is still visible
**/
- (void)showScreen;

/**
 * Hides the screen view and stops all running display timers.
**/
- (void)hideScreen;

/**
 * Updates the clock in the botton right hand corner of the screen to show the current time.
**/
- (void)updateClock;
- (void)updateClockByTimer:(NSTimer *)aTimer;

/**
 * Displays a specific page of predictions on the screen.
 *
 * @param	page		The page number to display, this is translated to items in the currentPrediction NSArray
**/
- (void)displayPageOfPredictions:(int)page;

/**
 * Displays the next page of predictions by incrementing the currentPage. Wraps around to the first page when
 * we pass the end of the currentPredictions NSArray.
**/
- (void)displayNextPageOfPredictions;
- (void)displayNextPageOfPredictionsByTimer:(NSTimer *)aTimer;

/**
 * Clears the current page by rehiding all labels and icons. This is used to cleanup a page of results
 * before displaying a new one, so that we get empty rows when there is no result, instead of the last result.
**/
- (void)clearPage;

/**
 * Shows the Playing indicator (green light)
**/
- (void)showPlaying;

/**
 * Shows the Warning indicator (yellow light)
**/
- (void)showWarning;

/**
 * Shows the Paused indicator (red light)
**/
- (void)showPaused;

/**
 * Whether the Playing indicator is showing.
 *
 * @returns		YES if the playing indicator is showing, NO otherwise.
**/
- (BOOL)isPlaying;

/**
 * Whether the Warning indicator is showing.
 *
 * @returns		YES if the warning indicator is showing, NO otherwise.
 **/
- (BOOL)isWarning;

/**
 * Whether the Paused indicator is showing.
 *
 * @returns		YES if the paused indicator is showing, NO otherwise.
 **/
- (BOOL)isPaused;

/**
 * Updates the title, subtitle, tracker ID and stop ID of the PID based on the currentStop.
 * Also updates the title of the PID under the navigation controller and shows/hides the
 * add to favourites button as appropriate.
**/
- (void)updateDisplayedStopInformation;

/**
 * Gets a page of prediction from the list of predictions.
 *
 * @param	page		The page number to get predictions of.
 * @returns	An array of Prediction objects in the specified page.
**/
- (NSArray *)getPageOfPredictions:(int)page;

/**
 * Gets the minutes until arrival for a specified arrival time.
 * This is used to update the "minutes" column of the results screen.
 * Will swap to showing an absolute date when the Prediction is more
 * than 60 minutes away.
**/
+ (NSString *)minutesUntilArrivalTime:(NSDate *)arrivalTime;
+ (NSString *)voiceOverMinutesUntilArrivalTime:(NSDate *)arrivalTime;

/**
 * Show or hide the add to favourites button.
 *
 * @param	showButton		YES will show the add to favourites button, NO will hide it.
**/
- (void)showAddToFavouritesButton:(BOOL)showButton;

/**
 * Move our disruption/low floor/air conditioning/special event message icon down vertically.
 *
 * The icons are stacked vertically in the interface. -clearPage will reset their position
 * to be in the same spot in the top right. This method will lower the specified icon to be
 * below the other specified icon.
 *
 * @param	icon		The icon to move relative to the fromIcon.
 * @param	fromIcon	The icon to use as a reference point.
**/
- (void)offsetIcon:(UIImageView *)icon fromIcon:(UIImageView *)fromIcon;

/**
 * Display the next special event message in the messages NSArray. Increments the
 * currentMessage counter and wraps back to the first message when it reaches the end.
**/
- (BOOL)displayNextMessage;

/**
 * Hide the disruption Message and special Event Message screens.
**/
- (void)hideMessages;

/**
 * Start a PID and use CoreLocation to show the nearest stop. Automatically updates the currentStop
 * as the location information gets more accurate or the device moves.
**/
- (void)startWithNearby;

/**
 * Start a PID with the specified stop.
 *
 * @param	stop		The Stop to show on the PID and receive updates for.
**/
- (void)startWithStop:(Stop *)stop;

/**
 * Stops updating the PID. All of the timers are stopped and delegates that we're holding on to
 * are released. It is not possible to restart a PID after this point.
**/
- (void)stop;
- (void)pause;
- (void)resume;

/**
 * Changes the stop that the PID is displaying.
 *
 * @param	stopID		The tracker ID of the stop.
**/
- (void)changeStop:(Stop *)stop;

/**
 * Asks the PidsService for a new set of Predictions for the stop specified in currentStop.
**/
- (void)refreshPredictions;
- (void)refreshPredictionsByTimer:(NSTimer *)aTimer;

/**
 * Starts CoreLocation and finds the nearest stop to our current location.
**/
- (void)showNearestStop;

/**
 * Delegate method for the CLLocationManager. This is called every time the device updates our current location.
 * Checks the StopList service for our closest stop and calls -changeStop if so.
**/
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations;

/**
 * Delegate method for the CLLocationManager. There was an error retrieving the current location (generally the user clicked "NO" when they were asked for permission.
 * Calls -changeStop to change to Federation Square if we have not yet found a nearby stop.
**/
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error;

/**
 * Delegate method for the PidsService. Called when the PidsService has retrieved information about the requested stop
 * and is used to update the currentStop property and calls -updateDisplayedStopInformation
**/
- (void)setInformation:(Stop *)information;

/**
 * Delegate method for the PidsService. Called when the PidsService has retrieved a set of predictions we've updated
 * the currentPredictions NSArray. Also ensures that -showScreen is called.
**/
- (void)setPredictions:(NSArray *)predictions;

/**
 * The SwipeCaptureView transparent view that lives on top of us in the view stack has registered that the user
 * began touching the screen. Pass the touch up to our parent PagedPID if it exists.
 *
 * @param	touches		An NSSet of UITouch objects as generated by the device.
 * @param	event		A UIEvent object as generated by the device.
 * @returns	YES if the SwipeCaptureView handler should cancel the bubble of the touch, NO if not.
**/
- (BOOL)touchesDidBegin:(NSSet *)touches withEvent:(UIEvent *)event;

/**
 * The SwipeCaptureView transparent view that lives on top of us in the view stack has registered that the user
 * has moved their finger on the screen. Pass the touch up to our parent PagedPID if it exists.
 *
 * @param	touches		An NSSet of UITouch objects as generated by the device.
 * @param	event		A UIEvent object as generated by the device.
 * @returns	YES if the SwipeCaptureView handler should cancel the bubble of the touch, NO if not.
 **/
- (BOOL)touchesDidMove:(NSSet *)touches withEvent:(UIEvent *)event;

/**
 * The SwipeCaptureView transparent view that lives on top of us in the view stack has registered a touch
 * on the screen was cancelled. Pass the touch up to our parent PagedPID if it exists.
 *
 * @param	touches		An NSSet of UITouch objects as generated by the device.
 * @param	event		A UIEvent object as generated by the device.
 * @returns	YES if the SwipeCaptureView handler should cancel the bubble of the touch, NO if not.
 **/
- (BOOL)touchesWereCancelled:(NSSet *)touches withEvent:(UIEvent *)event;

/**
 * The SwipeCaptureView transparent view that lives on top of us in the view stack has registered that the user
 * has finsihed touching the screen. Pass the touch up to our parent PagedPID if it exists.
 *
 * @param	touches		An NSSet of UITouch objects as generated by the device.
 * @param	event		A UIEvent object as generated by the device.
 * @returns	YES if the SwipeCaptureView handler should cancel the bubble of the touch, NO if not.
 **/
- (BOOL)touchesDidEnd:(NSSet *)touches withEvent:(UIEvent *)event;

- (void)pidsServiceDidFailWithError:(NSError *)error;

/**
 * Show the Onboard screen
**/
- (IBAction)pushOnboardScreenForSender:(id)sender;
- (void)pushMapForStop;

/**
 * Show the nearby Ticket Retailers
**/
- (void)pushNearbyTicketRetailers;

/**
 * Hides the welcome screen
**/
- (void)hideWelcome:(NSTimer *)aTimer;

- (void)showActionButton;
- (void)presentActionSheet;
- (void)pushScheduledList;

- (void)showFilter;
- (void)hideFilter;
- (void)resetRouteFilterPosition;
- (void)saveFilter;
- (void)clearFilter;
- (void)setSavedFilter;
- (void)displayStopFromTimer:(NSTimer *)aTimer;
- (void)displayStopWithLocation:(CLLocation *)location;

- (void)showLocating;
- (void)hideLocating;
- (void)presentDirections;
@end
