//
//  PidsService.m
//  tramTRACKER
//
//  Created by Robert Amos on 18/02/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "PidsService.h"
#import <SystemConfiguration/SCNetworkReachability.h>
#import "Route.h"
#import "PidsServiceDelegate.h"
#import "Stop.h"

NSString * const PIDClientType = @"IPHONEPID";
NSString * const PIDServiceExpectedVersion = @"6.4.0.0";
NSString * const PIDServiceURI = @"http://ws.tramtracker.com.au/pidsservice/pids.asmx";
//NSString * const PIDServiceURI = @"http://extranetdev.yarratrams.com.au/pidsservice/pids.asmx";
NSString * const PIDServiceNamespace = @"http://www.yarratrams.com.au/pidsservice/";
NSInteger const PIDServiceErrorNotReachable = 1001;
NSInteger const PIDServiceValidationError = 1002;
NSInteger const PIDServiceValidationErrorStopNotFound = 1003;
NSInteger const PIDServiceValidationErrorRouteNotFound = 1004;
NSInteger const PIDServiceErrorOnboardNotReachable = 1005;
NSInteger const PIDServiceErrorTimeoutReached = 1006;

PIDServiceActionType const PIDServiceActionTypeUpdate = 1;
PIDServiceActionType const PIDServiceActionTypeDelete = 2;

@implementation PidsService

@synthesize delegate, guid, trackerID, routeNumber, upDirection;

//
// Get or set the Guid
//
- (id)init
{
    if (self = [super init])
    {
        backgroundedSelf = NO;
        
        // now - do we have a Guid already in the defaults?
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        // Get a new Guid!
        if ([defaults objectForKey:@"guid"] == nil)
        {
            // we handle getting the new guid ourselves if necessary
            //NSLog(@"No guid set, getting guid");
            [self getNewClientGuid];

            // clear the delegate again so that its not accidentally left to self later on
            //[self clearDelegate];
            [self setGuid:@"00000000-0000-0000-0000-000000000000"];

        } else
        {
            [self setGuid:[defaults objectForKey:@"guid"]];
        }
    }
	return self;
}

//
// Clear the delegate. Set the delegate with -setDelegate (as part of the @synthesize)
// The selector you need to implement depends on the method you're calling
//
- (void)clearDelegate
{
	[self setDelegate:nil];
}


//
// Get a new Guid for this client.
// Your delegate must implement -setNewClientGuid:(NSString *)newGuid;
//
- (void)getNewClientGuid
{
	[self callWebServiceMethod:@"GetNewClientGuid"];
}


//
// Get Information about a Stop
// Your delegate must implement -setInformation:(Stop *)stop forStop:(NSNumber *)trackerID;
//
- (void)getInformationForStop:(int)stopID
{
	// make sure that we have a Guid set.
	NSAssert([self guid], @"Unable to get information for stop as no GUID is set.");
	
	// Create the parameters
	NSDictionary *params = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:stopID] forKey:@"stopNo"];
	
	// Keep the tracker ID around for later
	[self setTrackerID:[NSNumber numberWithInt:stopID]];
	
	// call the web service
	[self callWebServiceMethod:@"GetStopInformation" withParameters:params];
}

//
// Get Predicted Arrivals for a Stop
// Your delegate must implement -setPredictions:(NSArray *)predictions forStop:(NSNumber *)trackerID;
//
- (void)getPredictionsForStop:(int)stopID route:(Route *)route lowFloorOnly:(BOOL)lowFloorOnly
{
	// make sure that we have a Guid set.
	NSAssert([self guid], @"Unable to get predictions for stop as no GUID is set.");
	isRealtimeRequest = YES;
	
	// Create the parameters
	NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithCapacity:0];
	[params setObject:[NSNumber numberWithInt:stopID] forKey:@"stopNo"];
	[params setObject:(route == nil ? @"0" : route.number) forKey:@"routeNo"];
	[params setObject:(lowFloorOnly ? @"true" : @"false") forKey:@"lowFloor"];

	// Keep the tracker ID around for later
	[self setTrackerID:[NSNumber numberWithInt:stopID]];

	// Call the web service
	[self callWebServiceMethod:@"GetNextPredictedRoutesCollection" withParameters:params];
    [params release]; params = nil;
}

- (void)getPredictionsForStop:(int)stopID
{
	[self getPredictionsForStop:stopID route:nil lowFloorOnly:NO];
}

//
// Get scheduled departures for a stop
// Your delegate must implement -setDepartures:(NSArray *)departures forStop:(NSNumber *)trackerID;
// 
- (void)getScheduledDeparturesForStop:(int)stopID routeNumber:(NSString *)aRouteNumber atDateTime:(NSDate *)date withLowFloorOnly:(BOOL)lowFloorOnly
{
	// make sure that we have a Guid set.
	NSAssert([self guid], @"Unable to get scheduled departures for stop as no Guid is set.");
	
	// Create the parameters
	NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithCapacity:0];
	[params setObject:[NSNumber numberWithInt:stopID] forKey:@"stopNo"];
	[params setObject:aRouteNumber forKey:@"routeNo"];
	[params setObject:(lowFloorOnly ? @"true" : @"false") forKey:@"lowFloor"];
	[params setObject:[self formatDateForService:date] forKey:@"clientRequestDateTime"];

	// call the web service
	[self callWebServiceMethod:@"GetSchedulesCollection" withParameters:params];
	[params release];
}

//
// Get info on a scheduled trip
//
- (void)schedulesForTrip:(NSNumber *)trip atDateTime:(NSDate *)date
{
	// Create the parameters
	NSDictionary *params = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:trip, [self formatDateForService:date], nil]
													   forKeys:[NSArray arrayWithObjects:@"tripID", @"scheduledDateTime", nil]];
	// all the web service
	[self callWebServiceMethod:@"GetSchedulesForTrip" withParameters:params];
}

//
// Get the Route List
//
- (void)getRouteList
{
	// make sure that we have a Guid set.
	NSAssert([self guid], @"Unable to get route list as no GUID is set.");
	
	// No parameters needed
	[self callWebServiceMethod:@"GetDestinationsForAllRoutes"];
}

//
// Get a list of destinations for a specific route
//
- (void)destinationsForRoute:(Route *)route
{
	// make sure that we have a Guid set.
	NSAssert([self guid], @"Unable to get stop list as no GUID is set.");
	
	// Create the parameters
	NSDictionary *params = [NSDictionary dictionaryWithObject:route.internalNumber forKey:@"routeNo"];
	[self setRouteNumber:route.internalNumber];
	[self callWebServiceMethod:@"GetDestinationsForRoute" withParameters:params];
}

//
// Get the list of stops for a specific route and direction
// Your delegate should implement -setStopList:(NSArray *)stops forRouteNumber:(NSString *)routeNumber;
//
- (void)getStopListForRoute:(Route *)route upDirection:(BOOL)isUpDirection
{
	// make sure that we have a Guid set.
	NSAssert([self guid], @"Unable to get stop list as no GUID is set.");
	
	// Create the parameters
	NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
	[params setObject:[route internalNumber] forKey:@"routeNo"];
	[params setObject:(isUpDirection ? @"true" : @"false") forKey:@"isUpDirection"];
	
	// call the web service
	[self setRouteNumber:route.internalNumber];
	[self setUpDirection:[NSNumber numberWithBool:isUpDirection]];

	[self callWebServiceMethod:@"GetListOfStopsByRouteNoAndDirection" withParameters:params];

	[params release];
}

//
// Get info on a journey for a tram number
//
- (void)getJourneyForTramNumber:(NSNumber *)tramNumber
{
	NSAssert([self guid], @"Unable to get journey as no Guid is set.");
	isRealtimeRequest = YES;
	// Create the parameters
	NSDictionary *params = [NSDictionary dictionaryWithObject:tramNumber forKey:@"tramNo"];
	
	// call the web service
	[self callWebServiceMethod:@"GetNextPredictedArrivalTimeAtStopsForTramNo" withParameters:params];
}


//
// Get a list of updates since date and time
//
- (void)updatesSinceDate:(NSDate *)date
{
	// Create the parameters
	NSDictionary *params = [NSDictionary dictionaryWithObject:[self formatDateForService:date] forKey:@"dateSince"];
	
	// call the web service
	[self callWebServiceMethod:@"GetStopsAndRoutesUpdatesSince" withParameters:params];
}

//
// Get the number of routes through a stop
//
- (void)routesThroughStop:(Stop *)stop
{
	// if it is a stop that is a terminus (end terminus), dont bother the server won't return anything
	if ([stop.trackerID integerValue] >= 8000)
	{
		[self failWithError:[NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
												code:PIDServiceValidationErrorStopNotFound
											userInfo:nil]];
		return;
	}

	// Create the parameters
	NSDictionary *params = [NSDictionary dictionaryWithObject:stop.trackerID forKey:@"stopNo"];
	[self setTrackerID:stop.trackerID];
	
	// call the thingy
	[self callWebServiceMethod:@"GetMainRoutesForStop" withParameters:params];
}

//
// Call a Web Service method with no parameters
//
- (void)callWebServiceMethod:(NSString *)method
{
	// get the SOAP:Header
	NSString *header = [self getHeader];

	// Start building the request string
	NSMutableString *request = [[NSMutableString alloc] init];
	[request appendString:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"];
	[request appendString:@"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"];
	
	// Do we have a SOAP:Header?
	if ([header length] > 0) [request appendString:header];
	
	// Add the SOAP:Body
	[request appendString:@"<soap12:Body>"];
	[request appendFormat:@"<%@ xmlns=\"%@\" />", method, PIDServiceNamespace];
	[request appendString:@"</soap12:Body>"];
	[request appendString:@"</soap12:Envelope>"];

	NSMutableDictionary *p = [[NSMutableDictionary alloc] initWithCapacity:0];
	[p setObject:method forKey:@"method"];
	[p setObject:request forKey:@"body"];
	
	// Make sure all requests aren't performed on the main thread
	backgroundedSelf = NO;
	if ([NSThread isMainThread])
		[self performSelectorInBackground:@selector(callWebServiceMethodWhileInBackground:) withObject:p];
	else
		[self callWebServiceMethod:method withRequestBody:request];

	[request release];
	[p release];
}

//
// Call a web service method with parameters
//
- (void)callWebServiceMethod:(NSString *)method withParameters:(NSDictionary *)parameters
{
	// If we have no parameters then you shouldn't call this method
	NSAssert([parameters count] > 0, @"Don't call -callWebServiceMethod:withParameters when you have no parameters, use -callWebServiceMethod instead");
	
	// hold on to the parameters
	[parameters retain];
	
	// get the SOAP:Header
	NSString *header = [self getHeader];

	// start building the request
	NSMutableString *request = [[NSMutableString alloc] init];
	[request appendString:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"];
	[request appendString:@"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"];
	
	// Do we have a SOAP:Header?
	if ([header length] > 0) [request appendString:header];
	
	// Add the SOAP:Body
	[request appendString:@"<soap12:Body>"];
	[request appendFormat:@"<%@ xmlns=\"%@\">", method, PIDServiceNamespace];
	
	// Add the parameters
	for (id key in parameters)
		[request appendFormat:@"<%@>%@</%@>", key, [parameters objectForKey:key], key];

	// close the method and SOAP:Body
	[request appendFormat:@"</%@>", method];
	[request appendString:@"</soap12:Body>"];
	[request appendString:@"</soap12:Envelope>"];

	// we've finished with the parameters now
	[parameters release];
	
	NSMutableDictionary *p = [[NSMutableDictionary alloc] initWithCapacity:0];
	[p setObject:method forKey:@"method"];
	[p setObject:request forKey:@"body"];

	// Make sure all requests aren't performed on the main thread
	backgroundedSelf = NO;
	if ([NSThread isMainThread])
		[self performSelectorInBackground:@selector(callWebServiceMethodWhileInBackground:) withObject:p];
	else
		[self callWebServiceMethod:method withRequestBody:request];

	[request release];
	[p release];
}


//
// Call a Web Service method with the Request Body passed
//
- (void)callWebServiceMethod:(NSString *)method withRequestBody:(NSString *)body
{
	// initialise the URL Request
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	// Make sure that we have access to the interwebs
	if (![self hasAccessToService])
	{
		[pool drain];
		return;
	}
	
	NSRunLoop *runLoop = nil;
	if (backgroundedSelf)
		runLoop = [NSRunLoop currentRunLoop];

	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:PIDServiceURI]];

	// set the necessary headers
	[request addValue:@"text/xml;charset=utf-8" forHTTPHeaderField:@"Content-Type"];
	[request addValue:[PIDServiceNamespace stringByAppendingString:method] forHTTPHeaderField:@"SOAPAction"];

	// We POST the data
	[request setHTTPMethod:@"POST"];
	
	// And the request body is the XML, generally
	[request setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
	////NSLog(@"[PIDSSERVICE] Calling method %@ with request body: %@", method, body);
	
	// Create the PidsServiceDelegate, it will handle the connection and parsing of the response
	PidsServiceDelegate *pidDelegate = [[PidsServiceDelegate alloc] initWithDelegate:[method isEqualToString:@"GetNewClientGuid"] ? self : [self delegate] isWebServiceMethod:YES];
	
	// if we have a trackerID pass it through to the delegate
	if (trackerID != nil)
		[pidDelegate setTrackerID:trackerID];
	if (routeNumber != nil)
		[pidDelegate setRouteNumber:routeNumber];
	if (upDirection != nil)
		[pidDelegate setUpDirection:upDirection];
	
	// Set the request date and time
	[pidDelegate setRequestTime:[NSDate date]];
	
	// Start the connection
	//NSLog(@"Calling %@ on the PidsService with Guid %@", [PIDServiceNamespace stringByAppendingString:method], self.guid);
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	[[[NSURLConnection alloc] initWithRequest:request delegate:pidDelegate startImmediately:YES] autorelease];

	if (backgroundedSelf)
	{
		[runLoop runUntilDate:[NSDate dateWithTimeIntervalSinceNow:15]];
		CFRunLoopStop([runLoop getCFRunLoop]);
	}

	[pidDelegate release];
	[pool drain];
}

- (void)callWebServiceMethodWhileInBackground:(NSDictionary *)params
{
	backgroundedSelf = YES;
	[self callWebServiceMethod:[params objectForKey:@"method"] withRequestBody:[params objectForKey:@"body"]];
}

//
// Get the SOAP:Header for the request
//
- (NSString *)getHeader
{
	// start creating the header
	NSMutableString *header = [[NSMutableString alloc] init];
	[header appendString:@"<soap12:Header>"];
	[header appendFormat:@"<PidsClientHeader xmlns=\"%@\">", PIDServiceNamespace];

	// do we have a guid?
	if ([self guid])
		[header appendFormat:@"<ClientGuid>%@</ClientGuid>", [self guid]];
	
	// add the remaining header information
	[header appendFormat:@"<ClientType>%@</ClientType>", PIDClientType];
	[header appendFormat:@"<ClientWebServiceVersion>%@</ClientWebServiceVersion>", PIDServiceExpectedVersion];
	
	// and get the bundle version
	NSDictionary *bundle = [[NSBundle mainBundle] infoDictionary];
	[header appendFormat:@"<ClientVersion>%@</ClientVersion>", [bundle objectForKey:@"CFBundleVersion"]];
    
    // and the OS version
    [header appendFormat:@"<OSVersion>%@</OSVersion>", [[UIDevice currentDevice] systemVersion]];
	
	// close off the header
	[header appendString:@"</PidsClientHeader>"];
	[header appendString:@"</soap12:Header>"];
	
	// Auto-release and return the header
	[header autorelease];
	return header;	
}

//
// Format a date for the service
//
- (NSString *)formatDateForService:(NSDate *)date
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:00ZZZ"];
	NSMutableString *dateString = [NSMutableString stringWithString:[formatter stringFromDate:date]];
	[dateString insertString:@":" atIndex:22];
	[formatter release];
	return dateString;
}

//
// Set the Guid
//
- (void)setNewClientGuid:(NSString *)newGuid
{
	// save it in the user defaults
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject:newGuid	forKey:@"guid"];
	[defaults synchronize];
	
	// save it to the pids service
	[self setGuid:newGuid];
}

#pragma mark -
#pragma mark Checking service availablility and error handling

- (void)failWithError:(NSError *)error
{
	// check the delegate to see if we can report the error
	if (self.delegate != nil && [self.delegate respondsToSelector:@selector(pidsServiceDidFailWithError:)])
	{
		[self.delegate performSelectorOnMainThread:@selector(pidsServiceDidFailWithError:) withObject:error waitUntilDone:NO];
	}
}

// Check for the availability of the service
- (BOOL)hasAccessToService
{
	NSURL *host = [NSURL URLWithString:PIDServiceURI];
	SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(NULL, [[host host] UTF8String]);
	SCNetworkReachabilityFlags flags;
	SCNetworkReachabilityGetFlags(reachability, &flags);

	BOOL isReachable = flags & kSCNetworkReachabilityFlagsReachable;
	//BOOL noConnectionRequired = !(flags & kSCNetworkReachabilityFlagsConnectionRequired);
	
	// close the reachability ref
	CFRelease(reachability);
	
	if (isReachable)// || noConnectionRequired)
		return YES;

	// is the service not reachable and a connection is required
	NSString *errorMessage = [NSString stringWithFormat:NSLocalizedString(@"errors-nonetwork", @"No internet error"), (isRealtimeRequest ? NSLocalizedString(@"errors-snippet-realtime", @"Real Time") : NSLocalizedString(@"errors-snippet-scheduled", @"Scheduled")), [[UIDevice currentDevice] model]];
	NSString *recoverySuggestion = [NSString stringWithFormat:NSLocalizedString(@"errors-nonetwork-suggestion", @"No internet suggestion"), [[UIDevice currentDevice] model]];
	NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] initWithCapacity:0];
	[userInfo setObject:errorMessage forKey:NSLocalizedDescriptionKey];
	[userInfo setObject:recoverySuggestion forKey:NSLocalizedRecoverySuggestionErrorKey];
	NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier] code:PIDServiceErrorNotReachable userInfo:[NSDictionary dictionaryWithDictionary:userInfo]];
	[self failWithError:error];
	[userInfo release];
	return NO;
}

//
// Clean Up
//
- (void)dealloc
{
	[delegate release];
	[guid release];
	[trackerID release];
	[routeNumber release];
	
	[super dealloc];
}
@end
