//
//  BackgroundSynchroniser.m
//  tramTRACKER
//
//  Created by Robert Amos on 27/07/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "BackgroundSynchroniser.h"
#import "PidsService.h"
#import "Stop.h"
#import "Route.h"
#import "StopList.h"
#import "RouteUpdateOperation.h"
#import "StopUpdateOperation.h"
#import "RoutesThroughStopUpdateOperation.h"
#import "Turn.h"
#import "StopDeleteOperation.h"
#import "RouteDeleteOperation.h"
#import "TurnDeleteOperation.h"
#import "TicketOutletUpdateOperation.h"
#import "TicketOutletService.h"

NSString * const TTSyncKeyStops = @"stops";
NSString * const TTSyncKeyRoutes = @"routes";
NSString * const TTSyncKeyUpdates = @"updates";
NSString * const TTSyncKeyTickets = @"tickets";
NSString * const TTSyncKeyTicketFileSize = @"ticketFileSize";
NSString * const TTSyncKeyServerTime = @"serverTime";

NSInteger TTSyncTypeAutomatic = 1;
NSInteger TTSyncTypeAutoCheckManualSync = 2;
NSInteger TTSyncTypeManual = 3;

@implementation BackgroundSynchroniser

@synthesize delegate, stopsToBeUpdated, stopsToBeDeleted, routesToBeUpdated, routesToBeDeleted, routesThroughStopsToBeUpdated, synchronisationInProgress;

- (id)initWithLastSynchronisationDate:(NSDate *)date ticketFileDate:(NSDate *)ticketDate persistentStoreCoordinator:(NSPersistentStoreCoordinator *)storeCoordinator
{
	if (self = [super init])
	{
		// retain our objects
		persistentStoreCoordinator = storeCoordinator;
		[persistentStoreCoordinator retain];
        
        synchronisationInProgress = NO;
		
		synchronisationDate = date;
		[synchronisationDate retain];
		ticketSyncDate = ticketDate;
		[ticketSyncDate retain];
		
		service = [[PidsService alloc] init];
		[service setDelegate:self];
		ticketService = [[TicketOutletService alloc] initWithDelegate:self];
		
		shouldKeepRunning = YES;
		isCancelled = NO;
		ticketOutletsChecked = NO;
		ticketOutletsNeedUpdate = YES;
		temporaryUpdateStorage = nil;
		ticketFileSize = nil;
        wasAborted = NO;

		queue = [[NSOperationQueue alloc] init];
		[queue setMaxConcurrentOperationCount:1];
		[queue setSuspended:YES];
		
		deleteQueue = [[NSOperationQueue alloc] init];
		[deleteQueue setMaxConcurrentOperationCount:1];
		[deleteQueue setSuspended:YES];

		ticketQueue = [[NSOperationQueue alloc] init];
		[ticketQueue setMaxConcurrentOperationCount:1];
		[ticketQueue setSuspended:YES];
		
		// we need to keep a list of those stops and routes that are affected, to make sure
		// we don't update them more than once
		stopsToBeUpdated = [[NSMutableArray alloc] initWithCapacity:0];
		stopsToBeDeleted = [[NSMutableArray alloc] initWithCapacity:0];
		routesThroughStopsToBeUpdated = [[NSMutableArray alloc] initWithCapacity:0];
		routesToBeUpdated = [[NSMutableArray alloc] initWithCapacity:0];
		routesToBeDeleted = [[NSMutableArray alloc] initWithCapacity:0];
        
        // register for notifications as long as we're around
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveMemoryWarning:) name:UIApplicationDidReceiveMemoryWarningNotification object:nil];
	}
	return self;
}

- (void)synchroniseInBackgroundThread
{
	[self synchroniseInBackgroundThread:nil];
}

- (void)synchroniseInBackgroundThread:(NSDictionary *)params
{
	// create a pool and run loop
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	NSRunLoop *runLoop = [NSRunLoop currentRunLoop];
	//NSLog(@"Starting background synchronisation");
	isAutomatedSync = YES;

	// let everyone know we're starting
	if (!isCancelled && self.delegate != nil && [self.delegate respondsToSelector:@selector(syncDidStart)])
		[self.delegate performSelectorOnMainThread:@selector(syncDidStart) withObject:nil waitUntilDone:NO];

    synchronisationInProgress = YES;

    
	// start the synchronisation!
	if (!isCancelled)
	{
		if (params != nil)
		{
			// do we already have a copy of the updates?
			NSArray *updates = [params objectForKey:TTSyncKeyUpdates];

			// update things?
			if ([params objectForKey:TTSyncKeyTickets] != nil)
			{
				ticketOutletsChecked = YES;
				ticketOutletsNeedUpdate = [[params objectForKey:TTSyncKeyTickets] boolValue];
			}
			
			if (updates != nil)
			{
				[self startUpdatingWithUpdates:updates];
			} else
				[self synchronise];

		} else
			[self synchronise];
	}
    
    // Check if we're running iOS 4 or higher
    if ([queue respondsToSelector:@selector(operationCount)])
    {
        // support for 4+
        
        
        // start the run loop, run as long as there are operations in the queue (and the queue has been setup)
        while (!isCancelled && (shouldKeepRunning || [queue operationCount] > 0))
        {
            [runLoop runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
            //if (!runLoopResult)
            //	break;
        }
        
        // then we move onto the delete queue
        [deleteQueue setSuspended:NO];
        while (!isCancelled && [deleteQueue operationCount] > 0)
            [runLoop runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        
        // now that that is all finished, update the tickets if necessary
        // start the run loop, run as long as there are operations in the queue (and the queue has been setup)
        [ticketQueue setSuspended:NO];
        while (!isCancelled && (shouldKeepRunning || [ticketQueue operationCount] > 0))
        {
            [runLoop runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
            //if (!runLoopResult)
            //	break;
        }
        
    } else
    {
        // 3.1.3 support

        
        // start the run loop, run as long as there are operations in the queue (and the queue has been setup)
        while (!isCancelled && (shouldKeepRunning || [[queue operations] count] > 0))
        {
            [runLoop runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
            //if (!runLoopResult)
            //	break;
        }
        
        // then we move onto the delete queue
        [deleteQueue setSuspended:NO];
        while (!isCancelled && [[deleteQueue operations] count])
            [runLoop runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        
        // now that that is all finished, update the tickets if necessary
        // start the run loop, run as long as there are operations in the queue (and the queue has been setup)
        [ticketQueue setSuspended:NO];
        while (!isCancelled && (shouldKeepRunning || [[ticketQueue operations] count] > 0))
        {
            [runLoop runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
            //if (!runLoopResult)
            //	break;
        }
    
    }
	
	// save all the core data stuff back into place
	if (!isCancelled)
	{
		NSError *error;
		NSManagedObjectContext *context = [self managedObjectContext];
        [context setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];

		if (self.delegate != nil && [self.delegate respondsToSelector:@selector(syncDidStartSaving)])
			[self.delegate performSelectorOnMainThread:@selector(syncDidStartSaving) withObject:nil waitUntilDone:NO];
		
		if ([context save:&error])
		{
			//NSLog([context hasChanges] ? @"Not really saved." : @"Saved successfully.");

			if (!isCancelled && self.delegate != nil && [self.delegate respondsToSelector:@selector(syncDidFinishAtTime:)])
				[self.delegate performSelectorOnMainThread:@selector(syncDidFinishAtTime:) withObject:serverTimeOfUpdates waitUntilDone:NO];

		} else
		{
			
			// is it a merging error?
			if ([error code] == NSManagedObjectMergeError)
			{
				//NSLog(@"A Merge Error occured while saving: %@", [[error userInfo] objectForKey:@"conflictList"]);
			} else
            {
                //NSLog(@"An error occured while saving: %@, %@", error, [[error userInfo] objectForKey:NSDetailedErrorsKey]);
            }
			[context rollback];

			// send a failure message to the delegate
			if (self.delegate != nil && [self.delegate respondsToSelector:@selector(syncDidFailWithError:)])
				[self.delegate performSelectorOnMainThread:@selector(syncDidFailWithError:) withObject:error waitUntilDone:NO];
		}
	}
    
    synchronisationInProgress = NO;
	
	// send out a stop update message
	[[NSNotificationCenter defaultCenter] postNotificationName:TTStopsAndRoutesHaveBeenUpdatedNotice object:nil];
	
	//NSLog(@"Stopping background thread");
	CFRunLoopStop([runLoop getCFRunLoop]);
	[pool drain];
}

- (void)checkForUpdatesInBackgroundThread:(NSDictionary *)params
{
	// create a pool and run loop
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	NSRunLoop *runLoop = [NSRunLoop currentRunLoop];
	NSLog(@"Checking for updates in the background");
	isAutomatedSync = NO;

	// let everyone know we're starting
	if (!isCancelled && self.delegate != nil && [self.delegate respondsToSelector:@selector(checkForUpdatesDidStart)])
		[self.delegate performSelectorOnMainThread:@selector(checkForUpdatesDidStart) withObject:nil waitUntilDone:NO];
	
    synchronisationInProgress = YES;

	// start the checking!
	if (!isCancelled) [self synchronise];
	
    // Check if we're running iOS 4 or higher
    if ([queue respondsToSelector:@selector(operationCount)])
    {
        // start the run loop, run as long as there are operations in the queue (and the queue has been setup)
        while (!isCancelled && (shouldKeepRunning || [queue operationCount] > 0))
        {
            [runLoop runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
            //if (!runLoopResult)
            //	break;
        }
    } else
    {
        // start the run loop, run as long as there are operations in the queue (and the queue has been setup)
        while (!isCancelled && (shouldKeepRunning || [[queue operations] count] > 0))
        {
            [runLoop runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
            //if (!runLoopResult)
            //	break;
        }
    }
    
    synchronisationInProgress = NO;
	
	NSLog(@"Stopping background thread");
	CFRunLoopStop([runLoop getCFRunLoop]);
	[pool drain];
}

- (void)synchronise
{
	// check with the PidsService for a list of things that have changed since last time
	[service updatesSinceDate:synchronisationDate];

	// Ask the ticket service to update the tickets
	[ticketService hasTicketOutletsBeenUpdatedSinceDate:ticketSyncDate];
}

- (void)cancel
{
	// Cancel any active operations
	if (queue != nil && [[queue operations] count] > 0)
		[queue cancelAllOperations];
	
	if (deleteQueue != nil && [[deleteQueue operations] count] > 0)
		[deleteQueue cancelAllOperations];

	if (ticketQueue != nil && [[ticketQueue operations] count] > 0)
		[ticketQueue cancelAllOperations];
	
	if (self.delegate != nil && [self.delegate respondsToSelector:@selector(syncWasCancelled)])
		[self.delegate performSelectorOnMainThread:@selector(syncWasCancelled) withObject:nil waitUntilDone:NO];
	
    synchronisationInProgress = NO;
	isCancelled = YES;
}

- (void)didReceiveMemoryWarning:(NSNotification *)note 
{
     if (wasAborted)
        return;

	// Cancel any active operations
	if (queue != nil && [[queue operations] count] > 0)
		[queue cancelAllOperations];
	
	if (deleteQueue != nil && [[deleteQueue operations] count] > 0)
		[deleteQueue cancelAllOperations];
    
	if (ticketQueue != nil && [[ticketQueue operations] count] > 0)
		[ticketQueue cancelAllOperations];
	
	if (self.delegate != nil && [self.delegate respondsToSelector:@selector(syncWasAbortedDueToMemoryWarning)])
		[self.delegate performSelectorOnMainThread:@selector(syncWasAbortedDueToMemoryWarning) withObject:nil waitUntilDone:NO];
	
	synchronisationInProgress = NO;
    isCancelled = YES;
    wasAborted = YES;
}

- (void)setHasTicketOutletUpdates:(NSNumber *)hasUpdates withFileSize:(NSNumber *)fileSize;
{
	// do stuff
	ticketOutletsChecked = YES;
	ticketOutletsNeedUpdate = [hasUpdates boolValue];
	ticketFileSize = [fileSize retain];
	
	// if we have a saved one
	if (temporaryUpdateStorage != nil)
	{
		[self setUpdates:temporaryUpdateStorage atDate:nil];
		[temporaryUpdateStorage release];
		temporaryUpdateStorage = nil;
	}
}

- (void)setUpdates:(NSArray *)updates atDate:(NSDate *)atDate
{
    //NSLog(@"[BS] Updates: %@ at %@", updates, atDate);

    if (atDate != nil)
    {
        [atDate retain];
        if (serverTimeOfUpdates != nil)
            [serverTimeOfUpdates release];
        serverTimeOfUpdates = atDate;
    }
	
    // if we've run before the ticket outlets have been updated, wait for it to finish
    if (!ticketOutletsChecked)
	{
		temporaryUpdateStorage = [updates retain];
		return;
	}
    
	
	// Now that we've received a list, is there anything in it?
	if (updates == nil || [updates count] == 0)
	{
		// do we have ticket outlet updates?
		if (ticketOutletsNeedUpdate)
		{
			if (isAutomatedSync == NO)
			{
				// we return a very basic one with zeros
				NSDictionary *params = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSArray array], [NSNumber numberWithBool:ticketOutletsNeedUpdate], ticketFileSize, serverTimeOfUpdates, nil]
																   forKeys:[NSArray arrayWithObjects:TTSyncKeyStops, TTSyncKeyRoutes, TTSyncKeyUpdates, TTSyncKeyTickets, TTSyncKeyTicketFileSize, TTSyncKeyServerTime, nil]];
				
				// post the notification
				if (self.delegate != nil && [self.delegate respondsToSelector:@selector(checkForUpdatesDidFinish:)])
					[self.delegate performSelectorOnMainThread:@selector(checkForUpdatesDidFinish:) withObject:params waitUntilDone:NO];
                synchronisationInProgress = NO;
			}
			else
			{
				// its an auto-update. Update the tickets if necessary
				if (ticketOutletsNeedUpdate)
					[self startUpdatingWithUpdates:updates];
			}

			// let it all die
			shouldKeepRunning = NO;
			return;
		}
		
		//NSLog(@"No updates found.");
		shouldKeepRunning = NO;
		
		// send a notification just in case
		if (isAutomatedSync == NO)
		{
			if (self.delegate != nil && [self.delegate respondsToSelector:@selector(checkForUpdatesDidFinish:)])
				[self.delegate performSelectorOnMainThread:@selector(checkForUpdatesDidFinish:) withObject:nil waitUntilDone:NO];
            synchronisationInProgress = NO;
		}
        
		return;
	}
	
	// are we just checking for updates?
	if (isAutomatedSync == NO)
	{
		// we want to find a number of updates to be had for both
		NSInteger routes = 0;
		NSInteger stops = 0;
		for (id update in updates)
		{
			if ([update isKindOfClass:[RouteUpdate class]])
				routes++;
			else if ([update isKindOfClass:[StopUpdate class]])
				stops++;
		}
		
		// build up the parameters
		NSNumber *numberOfRoutesUpdated = [NSNumber numberWithInteger:routes];
		NSNumber *numberOfStopsUpdated = [NSNumber numberWithInteger:stops];
		NSDictionary *params = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:numberOfStopsUpdated, numberOfRoutesUpdated, updates, [NSNumber numberWithBool:ticketOutletsNeedUpdate], ticketFileSize, serverTimeOfUpdates, nil]
														   forKeys:[NSArray arrayWithObjects:TTSyncKeyStops, TTSyncKeyRoutes, TTSyncKeyUpdates, TTSyncKeyTickets, TTSyncKeyTicketFileSize, TTSyncKeyServerTime, nil]];

		// post the notification
		if (self.delegate != nil && [self.delegate respondsToSelector:@selector(checkForUpdatesDidFinish:)])
			[self.delegate performSelectorOnMainThread:@selector(checkForUpdatesDidFinish:) withObject:params waitUntilDone:NO];
		
		// let everything die
		shouldKeepRunning = NO;
        synchronisationInProgress = NO;
		return;
	}
	
	[self startUpdatingWithUpdates:updates];
}

- (void)startUpdatingWithUpdates:(NSArray *)updates
{
    synchronisationInProgress = YES;

	// Group them
	if (updates != nil && [updates count] > 0)
	{
		NSMutableArray *stopUpdates = [[NSMutableArray alloc] initWithCapacity:0];
		NSMutableArray *routeUpdates = [[NSMutableArray alloc] initWithCapacity:0];
		
		for (id update in updates)
		{
			if ([update isKindOfClass:[RouteUpdate class]])
				[routeUpdates addObject:update];
			else if ([update isKindOfClass:[StopUpdate class]])
				[stopUpdates addObject:update]; 
		}
		
		// update them
		if ([routeUpdates count] > 0)
			[self updateRoutes:routeUpdates];
		if ([stopUpdates count] > 0)
			[self updateStops:stopUpdates];
		
		// all done
		[routeUpdates release]; routeUpdates = nil;
		[stopUpdates release]; stopUpdates = nil;
	}
	
	// update ticket outlets
	if (ticketOutletsNeedUpdate)
		[self updateTicketOutlets];

	// at this point, reset the total on the progress count to be the detected operations
	if (self.delegate != nil && [self.delegate respondsToSelector:@selector(resetTotalUpdates:)])
	{
		// add the queue count
		NSNumber *queueCount = [NSNumber numberWithInteger:([[queue operations] count] + [[deleteQueue operations] count] + [[ticketQueue operations] count])];
		[self.delegate performSelectorOnMainThread:@selector(resetTotalUpdates:) withObject:queueCount waitUntilDone:NO];
	}
	
	// unsuspend the queue - let it run
	[queue setSuspended:NO];
	
	// tell the run loop that we're not holding it open anymore, once queue operations have finished it can close
    synchronisationInProgress = NO;
	shouldKeepRunning = NO;
}

// Indicates a failure of some sort.
- (void)pidsServiceDidFailWithError:(NSError *)error
{
	// send a failure message to the delegate
	if (self.delegate != nil && [self.delegate respondsToSelector:@selector(syncDidFailWithError:)])
		[self.delegate performSelectorOnMainThread:@selector(syncDidFailWithError:) withObject:error waitUntilDone:NO];
    
    // is it the date provided one?
    NSString *errorDescription = [[error userInfo] objectForKey:NSLocalizedDescriptionKey];
    if ([errorDescription rangeOfString:@"Date provided is not within acceptable range"].location != NSNotFound)
    {
        // abort because there is nothing we can do from here
        shouldKeepRunning = NO;
    }
}

- (void)updateStops:(NSArray *)updates
{
	NSManagedObjectContext *context = [self managedObjectContext];
	
	// get all the stops
	NSMutableArray *trackerIDs = [[NSMutableArray alloc] initWithCapacity:0];
	for (StopUpdate *update in updates)
		[trackerIDs addObject:update.trackerID];
	NSDictionary *stops = [self stopsForTrackerIDs:trackerIDs];
	[trackerIDs release]; trackerIDs = nil;
	
	// loop over the stops and do what we need to do
	for (StopUpdate *update in updates)
	{
		Stop *stop = [[stops objectForKey:update.trackerID] retain];
		if (stop == nil)
		{
			if (update.actionType == PIDServiceActionTypeUpdate)
			{
				//NSLog(@"Adding stop with tracker ID %@", update.trackerID);
				stop = [[NSEntityDescription insertNewObjectForEntityForName:@"Stop" inManagedObjectContext:context] retain];
				[stop setTrackerID:update.trackerID];
				[stop setTurns:nil];
				[self addRoutesThroughStopToQueue:stop];
				[self addStopUpdateToQueue:stop];
			}
		} else
		{
			if (update.actionType == PIDServiceActionTypeDelete)
			{
				if (stop != nil)
				{
					//NSLog(@"Deleting stop: %@", stop);
					[self addStopDeleteToQueue:stop];
				}
				else
				{
					// decrease the total number of operations
					if (self.delegate != nil && [self.delegate respondsToSelector:@selector(syncDidRemoveOperation)])
						[self.delegate performSelectorOnMainThread:@selector(syncDidRemoveOperation) withObject:nil waitUntilDone:NO];
				}
			} else
			{
				//NSLog(@"Updating stop: %@", stop);
				[self addStopUpdateToQueue:stop];
			}
		}
        [stop release]; stop = nil;
	}
}

- (void)addStopUpdateToQueue:(Stop *)stop
{
	// double check to make sure that we have a valid stop
	if (stop == nil || stop.trackerID == nil)
		return;
	
	// check to see if this stop's tracker id exists in the list already
	if ([self.stopsToBeDeleted containsObject:stop.trackerID] || [self.stopsToBeUpdated containsObject:stop.trackerID])
		return;
	
	StopUpdateOperation *operation = [[StopUpdateOperation alloc] initWithStop:stop];
	[operation setSyncManager:self];
	[queue addOperation:operation];

	// send a notification
	if (self.delegate != nil && [self.delegate respondsToSelector:@selector(syncDidAddOperation:)])
		[self.delegate performSelectorOnMainThread:@selector(syncDidAddOperation:) withObject:operation waitUntilDone:NO];
	[operation release];

	// add the stop to the list
	NSNumber *trackerID = [NSNumber numberWithInteger:[stop.trackerID integerValue]];
	[self.stopsToBeUpdated addObject:trackerID];
}

- (void)addStopDeleteToQueue:(Stop *)stop
{
	// double check to make sure that we have a valid stop
	if (stop == nil || stop.trackerID == nil)
		return;

	// check to see if this stop's tracker id exists in the list already
	if ([self.stopsToBeDeleted containsObject:stop.trackerID])
		return;
	
	// loop through all routes and check to see if they contain this stop. Make sure it gets updated if they do
	NSArray *routes = [Route allRoutes];
	for (Route *r in routes)
	{
		if ([r.upStops containsObject:stop.trackerID] || [r.downStops containsObject:stop.trackerID])
			[self addRouteUpdateToQueue:r];
	}
	
	// Add the operation to the delete queue
	StopDeleteOperation *operation = [[StopDeleteOperation alloc] initWithStop:stop];
	[operation setSyncManager:self];
	[deleteQueue addOperation:operation];

	// send a notification
	if (self.delegate != nil && [self.delegate respondsToSelector:@selector(syncDidAddOperation:)])
		[self.delegate performSelectorOnMainThread:@selector(syncDidAddOperation:) withObject:operation waitUntilDone:NO];
	[operation release];
	
	// add the stop to the list
	NSNumber *trackerID = [NSNumber numberWithInteger:[stop.trackerID integerValue]];
	[self.stopsToBeDeleted addObject:trackerID];
}




- (void)updateRoutes:(NSArray *)updates
{
	NSManagedObjectContext *context = [self managedObjectContext];

	// get all affected routes
	NSMutableArray *internalRouteNumbers = [[NSMutableArray alloc] initWithCapacity:0];
	for (RouteUpdate *update in updates)
		[internalRouteNumbers addObject:update.internalRouteNumber];
	NSDictionary *routes = [self routesForInternalRouteNumbers:internalRouteNumbers];
    [internalRouteNumbers release]; internalRouteNumbers = nil;

	for (RouteUpdate *update in updates)
	{
		Route *route = [routes objectForKey:update.internalRouteNumber];
		if (route == nil)
		{
			if (update.actionType == PIDServiceActionTypeUpdate)
			{
				route = [[[NSEntityDescription insertNewObjectForEntityForName:@"Route" inManagedObjectContext:context] retain] autorelease];
				[route setInternalNumber:update.internalRouteNumber];
				[route setNumber:update.routeNumber];
				[route setHeadboardNumber:update.headboardRouteNumber];
				[route setSubRoute:[update isSubRoute]];
                
                // dont attempt to overwrite the colour if it is not supplied
                if (update.colour != nil)
                    [route setColour:update.colour];

				[self addRouteUpdateToQueue:route];
				//NSLog(@"Adding route with internal route number: %@", update.internalRouteNumber);
			}
		} else
		{
			if (update.actionType == PIDServiceActionTypeDelete && route != nil)
			{
				//NSLog(@"Deleting route: %@", update.internalRouteNumber);
				[self addRouteDeleteToQueue:route];
			} else
			{
				//NSLog(@"Updating route: %@", update.internalRouteNumber);
				
				// update the headboard and normal route number
				[route setNumber:update.routeNumber];
				[route setHeadboardNumber:update.headboardRouteNumber];
				[route setSubRoute:[update isSubRoute]];

                // dont attempt to overwrite the colour if it is not supplied
                if (update.colour != nil)
                    [route setColour:update.colour];
				
                [self addRouteUpdateToQueue:route];
			}
		}
	}
}

- (void)addRouteUpdateToQueue:(Route *)route
{
	// double check to make sure that we have a valid route
	if (route == nil || route.internalNumber == nil)
		return;

	// check to see if this one is in the list already
	if ([self.routesToBeUpdated containsObject:route.internalNumber] || [self.routesToBeDeleted containsObject:route.internalNumber])
		return;
	
	// create the queue object
	RouteUpdateOperation *operation = [[RouteUpdateOperation alloc] initWithRoute:route];
	[operation setSyncManager:self];
	[queue addOperation:operation];

	// send a notification
	if (self.delegate != nil && [self.delegate respondsToSelector:@selector(syncDidAddOperation:)])
		[self.delegate performSelectorOnMainThread:@selector(syncDidAddOperation:) withObject:operation waitUntilDone:NO];
	[operation release];

	// add it to the update list
	NSString *routeNumber = [NSString stringWithString:route.internalNumber];
	[self.routesToBeUpdated addObject:routeNumber];
}

- (void)addRouteDeleteToQueue:(Route *)route
{
	// double check to make sure that we have a valid route
	if (route == nil || route.internalNumber == nil)
		return;
	
	// check to see if this one is in the list already
	if ([self.routesToBeDeleted containsObject:route.internalNumber])
		return;

	// if we have stops in either direction make sure that we update the routes through those stops
	// since this route will obviously no longer be in those lists
	if (route.upStops != nil && [route.upStops count] > 0)
	{
		for (NSNumber *trackerID in route.upStops)
			[self addRoutesThroughTrackerIDToQueue:trackerID];
	}
	if (route.downStops != nil && [route.downStops count] > 0)
	{
		for (NSNumber *trackerID in route.downStops)
			[self addRoutesThroughTrackerIDToQueue:trackerID];
	}

	// create the queue object
	RouteDeleteOperation *operation = [[RouteDeleteOperation alloc] initWithRoute:route];
	[operation setSyncManager:self];
	[deleteQueue addOperation:operation];
	
	// send a notification
	if (self.delegate != nil && [self.delegate respondsToSelector:@selector(syncDidAddOperation:)])
		[self.delegate performSelectorOnMainThread:@selector(syncDidAddOperation:) withObject:operation waitUntilDone:NO];
	[operation release];
	
	// add it to the update list
	NSString *routeNumber = [NSString stringWithString:route.internalNumber];
	[self.routesToBeDeleted addObject:routeNumber];	
}


- (void)addTurnDeleteToQueue:(Turn *)turn
{
	// double check to make sure that we have a valid turn
	if (turn == nil)
		return;
	
	// Add the operation to the delete queue
	TurnDeleteOperation *operation = [[TurnDeleteOperation alloc] initWithTurn:turn];
	[operation setSyncManager:self];
	[deleteQueue addOperation:operation];
	[operation release];
}


- (void)addRoutesThroughTrackerIDToQueue:(NSNumber *)trackerID
{
    if (trackerID == nil || [trackerID isEqualToNumber:[NSNumber numberWithInt:0]])
        return;

	Stop *stop = [[StopList sharedManager] getStopForTrackerID:trackerID];
	if (stop != nil)
		[self addRoutesThroughStopToQueue:stop];
}

- (void)addRoutesThroughTrackerIDToQueue:(NSNumber *)trackerID fromOperation:(NSOperation *)dependantOperation
{
    if (trackerID == nil || [trackerID isEqualToNumber:[NSNumber numberWithInt:0]])
        return;
    
	Stop *stop = [[StopList sharedManager] getStopForTrackerID:trackerID];
	if (stop != nil)
		[self addRoutesThroughStopToQueue:stop fromOperation:dependantOperation];
}

- (void)addRoutesThroughStopToQueue:(Stop *)stop
{
	[self addRoutesThroughStopToQueue:stop fromOperation:nil];
}

- (void)addRoutesThroughStopToQueue:(Stop *)stop fromOperation:(NSOperation *)dependantOperation
{
	// double check to make sure that we have a valid stop
	if (stop == nil || stop.trackerID == nil)
		return;

	// check to see if its in the stop delete queue or routes through stop update queue
	if ([self.stopsToBeDeleted containsObject:stop.trackerID] || [self.routesThroughStopsToBeUpdated containsObject:stop.trackerID])
		return;
	
	// it's not, add it to the queue
	RoutesThroughStopUpdateOperation *operation = [[RoutesThroughStopUpdateOperation alloc] initWithStop:stop];
	[operation setSyncManager:self];

	// if we have a dependency add it here
	if (dependantOperation != nil)
		[operation addDependency:dependantOperation];
	
	// add the operation to the queue
	[queue addOperation:operation];

	// send a notification
	if (self.delegate != nil && [self.delegate respondsToSelector:@selector(syncDidAddOperation:)])
		[self.delegate performSelectorOnMainThread:@selector(syncDidAddOperation:) withObject:operation waitUntilDone:NO];
	[operation release];
	
	// add it to the update queue
	NSNumber *trackerID = [NSNumber numberWithInteger:[stop.trackerID integerValue]];
	[self.routesThroughStopsToBeUpdated addObject:trackerID];
}

- (void)updateTicketOutlets
{
	// add a ticket outlet update operation to the queue
	TicketOutletUpdateOperation *operation = [[TicketOutletUpdateOperation alloc] init];
	[operation setSyncManager:self];
	
	// add the operation
	[ticketQueue addOperation:operation];

	// send a notification
	if (self.delegate != nil && [self.delegate respondsToSelector:@selector(syncDidAddOperation:)])
		[self.delegate performSelectorOnMainThread:@selector(syncDidAddOperation:) withObject:operation waitUntilDone:NO];
	[operation release];
}

#pragma mark -
#pragma mark Data Retrieval

- (Stop *)stopForTrackerID:(NSNumber *)aTrackerID
{
	NSManagedObjectContext *context = [self managedObjectContext];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Stop" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"trackerID == %@", aTrackerID];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (fetchedObjects == nil || [fetchedObjects count] == 0)
	{
		[fetchRequest release];
		return nil;
	}
	
	// return the object
	[fetchRequest release];
	return [fetchedObjects objectAtIndex:0];
}

- (NSDictionary *)stopsForTrackerIDs:(NSArray *)aTrackerIDArray
{
	// Grab our managed object context and create a fetch request
	NSManagedObjectContext *context = [self managedObjectContext];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Stop" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"trackerID IN %@", aTrackerIDArray];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (fetchedObjects == nil || [fetchedObjects count] == 0)
	{
		[fetchRequest release];
		return nil;
	}
	
	// return the object
	[fetchRequest release];
	
	NSMutableDictionary *list = [[NSMutableDictionary alloc] initWithCapacity:0];
	for (Stop *s in fetchedObjects)
	{
		NSNumber *trackerID = [NSNumber numberWithInteger:[s.trackerID integerValue]];
		[list setObject:s forKey:trackerID];
	}
	return [list autorelease];
}


- (Route *)routeForInternalRouteNumber:(NSString *)internalRouteNumber
{
	NSManagedObjectContext *context = [self managedObjectContext];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Route" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"internalNumber == %@", internalRouteNumber];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (fetchedObjects == nil || [fetchedObjects count] == 0)
	{
		[fetchRequest release];
		return nil;
	}
	
	// return the object
	[fetchRequest release];
	return [fetchedObjects objectAtIndex:0];
}

- (NSDictionary *)routesForInternalRouteNumbers:(NSArray *)anInternalRouteNumberArray
{
	// Grab our managed object context and create a fetch request
	NSManagedObjectContext *context = [self managedObjectContext];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	// set the entity that we're aiming for
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Route" inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"internalNumber IN %@", anInternalRouteNumberArray];
	[fetchRequest setPredicate:predicate];
	
	// fetch it!
	NSError *error;
	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	
	// can't find it?
	if (fetchedObjects == nil || [fetchedObjects count] == 0)
	{
		[fetchRequest release];
		return nil;
	}
	
	// return the object
	[fetchRequest release];
	
	NSMutableDictionary *list = [[NSMutableDictionary alloc] initWithCapacity:0];
	for (Route *r in fetchedObjects)
	{
		NSString *internalRouteNumber = [NSString stringWithString:r.internalNumber];
		[list setObject:r forKey:internalRouteNumber];
	}
	return [list autorelease];
}

- (Turn *)turnForRoute:(Route *)aRoute upDirection:(BOOL)isUpDirection stop:(Stop *)aStop
{
	// and build the predicate
	Stop *xStop = [self stopForTrackerID:aStop.trackerID];
	Route *xRoute = [self routeForInternalRouteNumber:aRoute.internalNumber];
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"upDirection == %@ AND stop == %@", [NSNumber numberWithBool:isUpDirection], xStop];
	return [[xRoute.turns filteredSetUsingPredicate:predicate] anyObject];
}

- (NSArray *)turnsForRoute:(Route *)aRoute upDirection:(BOOL)isUpDirection
{
	// and build the predicate
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"upDirection == %@", [NSNumber numberWithBool:isUpDirection]];
	return [[aRoute.turns filteredSetUsingPredicate:predicate] allObjects];
}


- (NSManagedObjectContext *) managedObjectContext
{
    if (managedObjectContext != nil) {
        return managedObjectContext;
    }
	
    NSPersistentStoreCoordinator *coordinator = persistentStoreCoordinator;
    if (coordinator != nil) {
        if ([NSManagedObjectContext instancesRespondToSelector:@selector(initWithConcurrencyType:)])
            managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSConfinementConcurrencyType];
        else
            managedObjectContext = [[NSManagedObjectContext alloc] init];
        [managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    return managedObjectContext;
}

- (void)stop
{
	[service setDelegate:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
	[persistentStoreCoordinator release];
	[managedObjectContext release];
	[synchronisationDate release];
	[service release];
	[ticketService release];
	[queue release];
	[deleteQueue release];
	[ticketQueue release];
	
	[stopsToBeUpdated release];
	[stopsToBeDeleted release];
	[routesToBeUpdated release];
	[routesToBeDeleted release];
	[routesThroughStopsToBeUpdated release];
	[ticketFileSize release];
    [serverTimeOfUpdates release]; serverTimeOfUpdates = nil;

	[super dealloc];
}

@end
