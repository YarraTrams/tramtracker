//
//  OnboardListView.h
//  tramTRACKER
//
//  Created by Robert Amos on 18/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Location.h"
#import "OnboardListCell.h"
#import "Journey.h"
#import "Prediction.h"
#import "Turn.h"
#import <QuartzCore/QuartzCore.h>

@class OnboardViewController;

/**
 * @ingroup Lists
**/

/**
 * Provides an auto-updating view that acts as a live-view of the tram that you're on
**/
@interface OnboardListView : UITableViewController <CLLocationManagerDelegate, UIAlertViewDelegate> {
	
	/**
	 * A cached copy of the journey
	**/
	Journey *journey;
	
	/**
	 * The stop list for the route
	**/
	NSArray *stopList;

	/**
	 * A list of Suburb names used to generate and maintain the section headers
	 **/
	NSMutableArray *suburbList;
	
	/**
	 * A convenience array that has the stops split into arrays per each suburb. The index of this array matches the index of the suburbList
	 **/
	NSMutableArray *stopListSplitBySuburbs;
	
	/**
	 * The location manager
	**/
	LocationManager *locationManager;
	
	/**
	 * The next stop
	**/
	Stop *nextStop;
	Stop *destination;
	
	/**
	 * The offset from the scheduled time (calculated by a moving tram)
	**/
	NSTimeInterval offset;
	
	BOOL hasAppeared;
	OnboardViewController *parent;
	Stop *atStop;
	NSIndexPath	*indexPathToOpenTurnIndicator;
	BOOL shouldShowTurnIndicators;
	NSMutableArray *previouslyHighlightedStops;
	NSIndexPath *indexPathToSelectedStop;
}

@property (nonatomic, retain) Journey *journey;
@property (nonatomic, retain) NSArray *stopList;
@property (nonatomic, retain) NSMutableArray *suburbList;
@property (nonatomic, retain) NSMutableArray *stopListSplitBySuburbs;
@property (nonatomic, retain) LocationManager *locationManager;
@property (nonatomic, retain) Stop *nextStop;
@property (nonatomic) NSTimeInterval offset;
@property (nonatomic, readonly) OnboardViewController *parent;
@property (nonatomic, retain) Stop *atStop;
@property (nonatomic, retain) NSIndexPath *indexPathToOpenTurnIndicator;

/**
 * Creates the suburbList and stopListSplitBySuburbs arrays using the stopList array.
 **/
- (void)setupSuburbList;

/**
 * Scrolls to the first stop in a journey
**/
- (void)scrollToStop:(Stop *)stop;
- (NSIndexPath *)indexPathOfStop:(Stop *)stop;

- (void)moveToStop:(Stop *)stop;
- (Stop *)nextStopWithLocation:(CLLocation *)location;
- (NSTimeInterval)secondsToNextStopWithLocation:(CLLocation *)location;
- (UIView *)tableFooterView;
- (id)initWithParentViewController:(OnboardViewController *)parent;
- (void)moveOnFromNextStop:(NSTimer *)aTimer;
- (BOOL)isGPSEnabled;

- (void)showTurnIndicatorOnlyForCell:(OnboardListCell *)cell;
- (void)hideTurnIndicatorForCell:(OnboardListCell *)cell;
- (void)setShouldShowTurnIndicators;

- (void)showConnectionsForStop:(Stop *)stop;

// arrival alarm
//- (void)setArrivalAlarmAtStop:(Stop *)stop;
//- (void)cancelArrivalAlarm;

@end

