//
//  MapViewController.h
//  tramTRACKER
//
//  Created by Robert Amos on 19/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "StopAnnotationView.h"
#import "PIDViewController.h"

extern NSInteger const TTMapViewListTypeNone;
extern NSInteger const TTMapViewListTypeStopList;
extern NSInteger const TTMapViewListTypeNearbyList;
extern NSInteger const TTMapViewListTypeFavouriteList;
extern NSInteger const TTMapViewListTypeMostRecent;
extern NSInteger const TTMapViewListTypeBrowseList;
extern NSInteger const TTMapViewListTypeConnectionList;
extern NSInteger const TTMapViewListTypeSelectedStop;
extern NSInteger const TTMapViewListTypeNearbyTickets;

@class StopListController;
@class RouteListController;
@class MainListController;
@class NearbyListController;
@class MostRecentController;
@class tramTRACKERAppDelegate;
@class Route;
@class Stop;
@class TicketRetailer;
@class NearbyViewController;
@class ActivityBarButtonController;
@class LocationManager;

/**
 * @defgroup Mapping Mapping
**/

/**
 * @ingroup Mapping
**/

/**
 * Displays a Map thats centered around a list of stops
**/
@interface MapViewController : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate> {

	/**
	 * Our Map View
	**/
	MKMapView *mapView;
	
	/**
	 * Stop List
	**/
	NSArray *stopList;
	NSArray *purpleStops;
	Stop *selectedStop;
	
	/**
	 * Retailer List
	**/
	NSArray *retailerList;
	TicketRetailer *selectedRetailer;

	/**
	 * The List view type
	**/
	NSInteger listViewType;
	
	/**
	 * The route we're displaying, if any
	**/
	Route *route;
	
	BOOL pushToScheduledDepartures;
	BOOL didHighlightCurrentStop;
	BOOL selectedStopShouldBeShown;
	
	NSDate *arrivalDate;
	NSDate *locatingStarted;
	
	BOOL trackUserLocation;
	
	BOOL showTicketRetailers;
	
	UIBarButtonItem *ticketsButton;
	NearbyViewController *nearbyViewController;
	UIBarButtonItem *stopButton;
	UIBarButtonItem *locateButton;
	ActivityBarButtonController *activityController;
	BOOL manuallyTracking;
	LocationManager *locationManager;
}

@property (nonatomic, retain) MKMapView *mapView;
@property (nonatomic, retain) NSArray *stopList;
@property (nonatomic, retain) NSArray *purpleStops;
@property (nonatomic) NSInteger listViewType;
@property (nonatomic, retain) Route *route;
@property (nonatomic) BOOL pushToScheduledDepartures;
@property (nonatomic, retain) Stop *selectedStop;
@property (nonatomic, retain) NSDate *arrivalDate;
@property (nonatomic) BOOL trackUserLocation;
@property (nonatomic, retain) NSDate *locatingStarted;
@property (nonatomic) BOOL showTicketRetailers;
@property (nonatomic, retain) NSArray *retailerList;
@property (nonatomic, retain) TicketRetailer *selectedRetailer;
@property (nonatomic, retain) UIBarButtonItem *ticketsButton;
@property (readonly, nonatomic) NearbyViewController *nearbyViewController;
@property (nonatomic, retain) UIBarButtonItem *stopButton;
@property (nonatomic, retain) UIBarButtonItem *locateButton;
@property (nonatomic, retain) ActivityBarButtonController *activityController;
@property (nonatomic) BOOL manuallyTracking;
@property (nonatomic, retain) LocationManager *locationManager;

/**
 * Custom Initialiser
**/
- (id)initWithStopList:(NSArray *)stopList;
- (id)initNearbyMapWithNearbyViewController:(NearbyViewController *)pVC;

/**
 * Find the region that fits our stops
**/
- (MKCoordinateRegion)regionThatFitsStops:(NSArray *)stops;

/**
 * Callback when someone has clicked on the disclosure indicator
**/
- (void)didTouchRealTimeCalloutAccessory:(NSNotification *)note;
- (void)didTouchScheduledCalloutAccessory:(NSNotification *)note;
- (void)didTouchTicketCalloutAccessory:(NSNotification *)note;
- (void)pushPIDForStop:(Stop *)aStop;

/**
 * Flip back to a list view
**/
- (void)flipToListView;

/**
 * Show or hide the ticket retailers as appropriate
**/
- (void)displayTicketRetailers;
- (void)hideTicketRetailers;
- (void)toggleTicketRetailers;

- (void)saveCurrentMapRegion;
- (void)stopTracking;
- (void)startTracking;

- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)userLocation;

@end
