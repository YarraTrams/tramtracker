//
//  MapViewController.m
//  tramTRACKER
//
//  Created by Robert Amos on 19/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "MapViewController.h"
#import "StopList.h"
#import "Stop.h"
#import "StopListController.h"
#import "TicketRetailer.h"
#import "TicketRetailerAnnotationView.h"
#import "NearbyViewController.h"
#import "TicketRetailerViewController.h"
#import <objc/runtime.h>
#import "UIDevice-Hardware.h"

NSInteger const TTMapViewListTypeNone = 0;
NSInteger const TTMapViewListTypeStopList = 1;
NSInteger const TTMapViewListTypeNearbyList = 2;
NSInteger const TTMapViewListTypeFavouriteList = 3;
NSInteger const TTMapViewListTypeMostRecent = 4;
NSInteger const TTMapViewListTypeBrowseList = 5;
NSInteger const TTMapViewListTypeConnectionList = 6;
NSInteger const TTMapViewListTypeSelectedStop = 7;
NSInteger const TTMapViewListTypeNearbyTickets = 8;

#define DEFAULT_LATITUDE_DELTA 0.00634111
#define DEFAULT_LONGITUDE_DELTA 0.00699520

#define NEARBY_THRESHOLD_200M 7
#define NEARBY_THRESHOLD_500M 14
#define NEARBY_THRESHOLD_MAX 18

@implementation MapViewController

@synthesize mapView, stopList, listViewType, route, pushToScheduledDepartures, purpleStops, selectedStop,
				arrivalDate, trackUserLocation, locatingStarted, showTicketRetailers, retailerList, selectedRetailer,
			ticketsButton, nearbyViewController, stopButton, locateButton, manuallyTracking, activityController, locationManager;

- (id)initNearbyMapWithNearbyViewController:(NearbyViewController *)pVC
{
	if (self = [super initWithNibName:nil bundle:nil])
	{
		[self setListViewType:TTMapViewListTypeNearbyList];
		[self setTrackUserLocation:YES];
		[self setManuallyTracking:NO];
		nearbyViewController = pVC;
	}
	return self;
}

// Custom initialiser
- (id)initWithStopList:(NSArray *)list
{
	if (self = [super initWithNibName:nil bundle:nil])
	{
		// set our stop list
		[self setStopList:list];
	}
	return self;
}

- (void)viewDidLoad
{
	// load up our MapView
	// reset the list button on the navigation item
	[self setListViewType:self.listViewType];

	// create a map view
	MKMapView *map = [[MKMapView alloc] initWithFrame:self.view.frame];
	[map setDelegate:self];
	
	[self.view addSubview:map];
	
	// center everything on the map
	if (listViewType == TTMapViewListTypeNearbyList)
	{
		// default to the region that was saved.
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		NSDictionary *nearbyMapLastRegion = [defaults dictionaryForKey:@"nearbyMapLastRegion"];
		
		// Create an MKCoordinateRegion from all that
		CLLocationCoordinate2D centreCoord;
		centreCoord.latitude = [[nearbyMapLastRegion objectForKey:@"latitude"] doubleValue];
		centreCoord.longitude = [[nearbyMapLastRegion objectForKey:@"longitude"] doubleValue];
		MKCoordinateRegion defaultCentre = MKCoordinateRegionMake(centreCoord,
																  MKCoordinateSpanMake([[nearbyMapLastRegion objectForKey:@"latitudeDelta"] doubleValue],
																					   [[nearbyMapLastRegion objectForKey:@"longitudeDelta"] doubleValue]));

		// if that worked..
		[map setRegion:defaultCentre];

		// the "locate me" button"
		UIBarButtonItem *locate = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nearbybutton.png"]
																   style:UIBarButtonItemStylePlain
																  target:self
																  action:@selector(startTracking)];
		[self setLocateButton:locate];
		[locate release];
		
		// the "stop locating me" button
		ActivityBarButtonController *activityBarButton = [[ActivityBarButtonController alloc] initWithNibName:nil bundle:nil target:self action:@selector(stopTracking)];
		[activityBarButton setAccuracyLevel:1];
		UIBarButtonItem *stop = [[UIBarButtonItem alloc] initWithCustomView:activityBarButton.view];
		[self setActivityController:activityBarButton];
		[self setStopButton:stop];
		[activityBarButton release];
		[stop release];
		
	} else if (self.stopList != nil && [self.stopList count] > 0)
		[map setRegion:[self regionThatFitsStops:self.stopList]];
	else
		[map setCenterCoordinate:CLLocationCoordinate2DMake(0, 0)];


	// if its a connection map then highlight the current stop
	if ((listViewType == TTMapViewListTypeConnectionList || listViewType == TTMapViewListTypeSelectedStop || listViewType == TTMapViewListTypeNearbyTickets) && self.selectedStop != nil)
		selectedStopShouldBeShown = YES;
	
	// add the map to the view
	[map addAnnotations:self.stopList];
	[self setMapView:map];

	[map release];
	
	// show the tickets button on everything except the "none" map type, and the nearby map type
	if (listViewType == TTMapViewListTypeSelectedStop || listViewType == TTMapViewListTypeConnectionList || listViewType == TTMapViewListTypeNearbyTickets)
	{
		// we need to show the tickets button
		UIBarButtonItem *tickets = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"nearby-button-tickets", @"Tickets")
																	style:UIBarButtonItemStylePlain
																   target:self
																   action:@selector(toggleTicketRetailers)];
		[self setTicketsButton:tickets];
		[self.navigationItem setRightBarButtonItem:tickets];
		[tickets release];
	}

	// if we're pushing to scheduled departures, it means ONLY scheduled departures
	if (![self pushToScheduledDepartures])
	{
		// register to be notified when someone has touched our callout
		NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
		[nc addObserver:self
			   selector:@selector(didTouchRealTimeCalloutAccessory:)
				   name:@"TTDidTouchRealTimeCalloutAccessory"
				 object:nil];
	}
	
	// but we always show the scheduled departures icon
	// register to be notified when someone has touched our callout
	NSNotificationCenter *ncs = [NSNotificationCenter defaultCenter];
	[ncs addObserver:self
            selector:@selector(didTouchScheduledCalloutAccessory:)
                name:@"TTDidTouchScheduledCalloutAccessory"
              object:nil];
    
	// register for ticket notifications
	NSNotificationCenter *nct = [NSNotificationCenter defaultCenter];
	[nct addObserver:self
            selector:@selector(didTouchTicketCalloutAccessory:)
                name:@"TTDidTouchTicketCalloutAccessory"
              object:nil];

}

- (void)viewWillAppear:(BOOL)animated
{
	// make sure we're the right size
	[self.mapView setFrame:CGRectMake(0, 0, 320, 367)];

	// display the button?
	if (self.listViewType == TTMapViewListTypeNearbyList)
	{
		[self.nearbyViewController.navigationItem setLeftBarButtonItem:self.stopButton animated:YES];
		// set a timer to display results
		[NSTimer scheduledTimerWithTimeInterval:NEARBY_THRESHOLD_200M target:self selector:@selector(stopTracking) userInfo:nil repeats:NO];
		[NSTimer scheduledTimerWithTimeInterval:NEARBY_THRESHOLD_500M target:self selector:@selector(stopTracking) userInfo:nil repeats:NO];
		[NSTimer scheduledTimerWithTimeInterval:NEARBY_THRESHOLD_MAX target:self selector:@selector(stopTracking) userInfo:nil repeats:NO];
	}

	// under 4.x we can just call this to get everything we need
	[self.mapView setShowsUserLocation:YES];
	
	// under 3.x we cannot, we need to start our own location service
	struct objc_method_description updateMethod;
	updateMethod = protocol_getMethodDescription(@protocol(MKMapViewDelegate), @selector(mapView:didUpdateUserLocation:), NO, YES);
	if (updateMethod.name == NULL && updateMethod.types == NULL)
	{
		//NSLog(@"[MAP] Creating our own Location Manager");
		LocationManager *manager = [[LocationManager alloc] init];
		[self setLocationManager:manager];
		[manager release];
		
		[self.locationManager setDelegate:self];
		[self.locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
		[self.locationManager startUpdatingLocation];
	}
}

// The view is going away
- (void)viewWillDisappear:(BOOL)animated
{
	// location manager in use?
	if (self.locationManager != nil)
	{
		[self.locationManager stopUpdatingLocation];
		[self setLocationManager:nil];
	}
	
	// save our current centre back to the defaults
	[self saveCurrentMapRegion];
}

//
// Find a coordinate region that fits all of our stops
- (MKCoordinateRegion)regionThatFitsStops:(NSArray *)stops
{
	Stop *stop = [stops objectAtIndex:0];
	MKCoordinateSpan span;
	CLLocationCoordinate2D center;
	
	// only a single stop?
	if ([stops count] == 1)
	{
		// center on the selected stop
		center.latitude = stop.location.coordinate.latitude;
		center.longitude = stop.location.coordinate.longitude;
		
		// and the default span
		span.latitudeDelta = DEFAULT_LATITUDE_DELTA;
		span.longitudeDelta = DEFAULT_LONGITUDE_DELTA;

	// multiple stops, find the region that will fit them all
	} else
	{
		CLLocationDegrees northernBoundary, southernBoundary, easternBoundary, westernBoundary;

		// set the initial boundaries to the first stop
		northernBoundary = stop.location.coordinate.latitude;
		southernBoundary = northernBoundary;
		westernBoundary = stop.location.coordinate.longitude;
		easternBoundary = westernBoundary;
		
		// loop over our stops and find the boundaries
		for (Stop *stop in stops)
		{
			// use latitude to find north/south boundaries
			if (stop.location.coordinate.latitude > northernBoundary)
				northernBoundary = stop.location.coordinate.latitude;
			else if (stop.location.coordinate.latitude < southernBoundary)
				southernBoundary = stop.location.coordinate.latitude;
			
			// and longitude for east/west
			if (stop.location.coordinate.longitude > westernBoundary)
				westernBoundary = stop.location.coordinate.longitude;
			else if (stop.location.coordinate.longitude < easternBoundary)
				easternBoundary = stop.location.coordinate.longitude;
		}
		
		// so we should know our span now between the boundaries
		span.latitudeDelta = (northernBoundary - southernBoundary);
		span.longitudeDelta = (westernBoundary - easternBoundary);
		
		// so now we find the center by using the lower boundary and adding half the span
		center.latitude = southernBoundary + (span.latitudeDelta / 2);
		center.longitude = easternBoundary + (span.longitudeDelta / 2);
	}
	
	// all done!
	return MKCoordinateRegionMake(center, span);
}

- (void)saveCurrentMapRegion
{
	// save our current centre back to the defaults
	if (listViewType == TTMapViewListTypeNearbyList)
	{
		MKCoordinateRegion region = [self.mapView region];
		NSDictionary *nearbyMapLastRegion = [NSDictionary dictionaryWithObjectsAndKeys:
											 [NSNumber numberWithDouble:region.center.latitude], @"latitude",
											 [NSNumber numberWithDouble:region.center.longitude], @"longitude",
											 [NSNumber numberWithDouble:region.span.latitudeDelta], @"latitudeDelta",
											 [NSNumber numberWithDouble:region.span.longitudeDelta], @"longitudeDelta",
											 nil];
		
		// save that to the defaults
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		[defaults setObject:nearbyMapLastRegion forKey:@"nearbyMapLastRegion"];
		[defaults synchronize];
	}
}

// when someone touches our callout accessory
- (void)didTouchRealTimeCalloutAccessory:(NSNotification *)note
{
	// save the map region
	[self saveCurrentMapRegion];
	
	// the stop is the note's object
	Stop *stop = [note object];
    [self pushPIDForStop:stop];
}

- (void)pushPIDForStop:(Stop *)aStop
{
	// make a PID for that stop!
	// Start the pid view
	PIDViewController *PID = [[PIDViewController alloc] initWithNibName:@"PIDView" bundle:[NSBundle mainBundle]];
	
	// get the stop info
	[PID startWithStop:aStop];
	
	// push it onto the stack
	if (self.nearbyViewController != nil)
		[self.nearbyViewController.navigationController pushViewController:PID animated:YES];
	else
		[self.navigationController pushViewController:PID animated:YES];
	[PID release];
}


- (void)didTouchScheduledCalloutAccessory:(NSNotification *)note
{
	// save the map region
	[self saveCurrentMapRegion];
	
	// the stop is the note's object
	Stop *stop = [note object];

	DepartureListController *departures = [[DepartureListController alloc] initWithScheduledListForStop:stop];
	
	if (self.route != nil)
		[departures setSelectedRoute:self.route.number];
	else
		[departures setSelectedRoute:[stop.routesThroughStop objectAtIndex:0]];
	
	
	if (self.arrivalDate != nil)
		[departures setScheduledDepartureTime:self.arrivalDate];
	
	if (self.nearbyViewController != nil)
		[self.nearbyViewController.navigationController pushViewController:departures animated:YES];
	else
		[self.navigationController pushViewController:departures animated:YES];
	[departures release];
}

- (void)didTouchTicketCalloutAccessory:(NSNotification *)note
{
	// save the map region
	[self saveCurrentMapRegion];
	
	// The ticket retailer is the note's object
	TicketRetailer *retailer = (TicketRetailer *)[note object];
	
	// Create the view
	TicketRetailerViewController *retailerViewController = [[TicketRetailerViewController alloc] initWithTicketRetailer:retailer];

	if (self.nearbyViewController != nil)
		[self.nearbyViewController.navigationController pushViewController:retailerViewController animated:YES];
	else 
		[self.navigationController pushViewController:retailerViewController animated:YES];
	
	[retailerViewController release]; retailerViewController = nil;
}

/**
 * Override the list view type to actually show/hide the button
**/
- (void)setListViewType:(NSInteger)type
{
	// take care of setting the property
	listViewType = type;
	
	// if there is no navigation controller yet then we aren't on the stack so don't change anything
	if (self.navigationController == nil)
		return;

	// change the bar button depending on what it is
	if (listViewType == TTMapViewListTypeNone || listViewType == TTMapViewListTypeConnectionList || listViewType == TTMapViewListTypeSelectedStop || listViewType == TTMapViewListTypeNearbyTickets)
		[self.navigationItem setRightBarButtonItem:nil animated:NO];

	else if (listViewType != TTMapViewListTypeNearbyList)
	{
		UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"button-list", @"List")
																   style:UIBarButtonItemStylePlain
																  target:self
																  action:@selector(flipToListView)];
		[self.navigationItem setRightBarButtonItem:button animated:NO];
		[button release];
	}
}

/**
 * Flip back to a list view
**/
- (void)flipToListView
{
	// none? do thing
	if (!self.listViewType || self.listViewType == TTMapViewListTypeNone || self.listViewType == TTMapViewListTypeConnectionList || self.listViewType == TTMapViewListTypeNearbyList)
		return;

	[self.mapView setDelegate:nil];
	[self.mapView setShowsUserLocation:NO];
	
	// using the location manager?
	if (self.locationManager != nil)
	{
		[self.locationManager stopUpdatingLocation];
		[self setLocationManager:nil];
	}

	// start animations
	// flip over to the list view
	[UIView beginAnimations:@"FlipToListView" context:nil];
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:[self.navigationController.view.subviews objectAtIndex:0] cache:YES];
	[UIView setAnimationDuration:1];
	
	// stop list view? create a stop list
	if (self.listViewType == TTMapViewListTypeStopList)
	{
		// Create the stop list
		StopListController *stopListCont= [[StopListController alloc] initWithStyle:UITableViewStylePlain];
		[stopListCont setStopList:self.stopList];
		[stopListCont setTitle:self.title];
		
		UINavigationController *nav = self.navigationController;

		// reset the view controllers array
		[nav popViewControllerAnimated:NO];

		// push the stop list controller onto the navigation controller
		[nav pushViewController:stopListCont animated:NO];
		
		[stopListCont release];
	}
	
	// Favourites list?
	else if (self.listViewType == TTMapViewListTypeFavouriteList)
	{
		// Navigation controller
		UINavigationController *nav = self.navigationController;
		
		// The favourites list is being held on to by the delegate
		tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
		
		// replace the things on the navigation controller stack
		if ([[nav.viewControllers objectAtIndex:0] isEqual:[self.tabBarController.moreNavigationController.viewControllers objectAtIndex:0]])
			[nav setViewControllers:[NSArray arrayWithObjects:[nav.viewControllers objectAtIndex:0], d.favouritesList, nil]];
		else
			[nav setViewControllers:[NSArray arrayWithObject:d.favouritesList]];
		
	}

	// Nearby list?
	/*else if (self.listViewType == TTMapViewListTypeNearbyList)
	{
		// Navigation controller
		UINavigationController *nav = self.navigationController;
		
		// The favourites list is being held on to by our navigation controller as the delegate
		tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
		
		// replace the things on the navigation controller stack
		if ([[nav.viewControllers objectAtIndex:0] isEqual:[self.tabBarController.moreNavigationController.viewControllers objectAtIndex:0]])
			[nav setViewControllers:[NSArray arrayWithObjects:[nav.viewControllers objectAtIndex:0], d.nearby, nil]];
		else
			[nav setViewControllers:[NSArray arrayWithObject:d.nearby]];
		
	} */

	// Most Recent list?
	else if (self.listViewType == TTMapViewListTypeMostRecent)
	{
		// Navigation controller
		UINavigationController *nav = self.navigationController;
		
		// The favourites list is being held on to by our navigation controller as the delegate
		tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
		
		// replace the things on the navigation controller stack
		if ([[nav.viewControllers objectAtIndex:0] isEqual:[self.tabBarController.moreNavigationController.viewControllers objectAtIndex:0]])
			[nav setViewControllers:[NSArray arrayWithObjects:[nav.viewControllers objectAtIndex:0], d.recent, nil]];
		else
			[nav setViewControllers:[NSArray arrayWithObject:d.recent]];
		
	} else if (self.listViewType == TTMapViewListTypeBrowseList)
	{
		// Push back to a stop list
		StopListController *stopListCont = [[StopListController alloc] initWithStyle:UITableViewStylePlain];

		[stopListCont setTitle:self.title];
		[stopListCont setRoute:self.route];
		[stopListCont setPushToScheduledDeparturesScreen:self.pushToScheduledDepartures];
		
		// set the stops
		[stopListCont setStopList:self.stopList];
		
		// Navigation controller
		UINavigationController *nav = self.navigationController;

		// replace the things on the navigation controller stack
		[nav popViewControllerAnimated:NO];
		[nav pushViewController:stopListCont animated:NO];
		
		// release it, and the pids service we're holding on to
		[stopListCont release];
	}
	
	
	// finish animations
	[UIView commitAnimations];
}

#pragma mark MKMapViewDelegate methods

//
// Return a pin to place on the map for a stop
//
- (MKAnnotationView *)mapView:(MKMapView *)map viewForAnnotation:(id <MKAnnotation>)annotation
{
	// is it the user location one? use the default
	if ([annotation isKindOfClass:[MKUserLocation class]])
		return nil;
	
	// is it a stop?
	if ([annotation isKindOfClass:[Stop class]])
	{
		static NSString *annotationIdentifier = @"StopPin";
		
		// create a stop pin
		StopAnnotationView *pin = (StopAnnotationView *)[map dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];
		if (pin == nil)
			pin = [[[StopAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"moo"] autorelease];

		// if its on the purple stop list then use purple!
		if (self.purpleStops != nil && [self.purpleStops containsObject:annotation])
		{
			[pin setPurplePin:YES];
			[pin setPinColor:MKPinAnnotationColorPurple];
		} else
		{
			// Green PIN (too much green? maybe)
			[pin setPurplePin:NO];
			[pin setPinColor:MKPinAnnotationColorGreen];
		}
		
		// make sure we show the accessories
		[pin setShouldShowScheduledAccessory:YES];
		[pin setShouldShowRealTimeAccessory:(![self pushToScheduledDepartures])];

		// animate placing it on the map because it looks pretty
		//[pin setAnimatesDrop:YES];
		
		// let them clicky the name to view a PID
		[pin setCanShowCallout:YES];

		return pin;
	}
	
	// is it a ticket retailer?
	if ([annotation isKindOfClass:[TicketRetailer class]])
	{
		static NSString *annotationIdentifier = @"TicketRetailerPin";
		
		// create a ticket retailer pin
		TicketRetailerAnnotationView *pin = (TicketRetailerAnnotationView *)[map dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];
		if (pin == nil)
			pin = [[[TicketRetailerAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationIdentifier] autorelease];
		else
			[pin setAnnotation:annotation];

		
		return pin;
	}
	
	return nil;
}


- (void)mapView:(MKMapView *)map regionDidChangeAnimated:(BOOL)animated
{
	//NSLog(@"New Region: %.8f,%.8f (+/- %.8f, %.8f)", map.region.center.latitude, map.region.center.longitude, map.region.span.latitudeDelta, map.region.span.longitudeDelta);
	CLLocation *loc = [[CLLocation alloc] initWithLatitude:map.centerCoordinate.latitude longitude:map.centerCoordinate.longitude];
	NSInteger maxStopCount = 100;

	if (listViewType == TTMapViewListTypeNearbyList || listViewType == TTMapViewListTypeNone || listViewType == TTMapViewListTypeConnectionList || listViewType == TTMapViewListTypeSelectedStop || listViewType == TTMapViewListTypeNearbyTickets)
	{
		NSMutableArray *stopsToAdd = [[NSMutableArray alloc] initWithCapacity:0];
		NSMutableArray *stopsToRemove = [[NSMutableArray alloc] initWithCapacity:0];

		NSArray *stopsInRegion;
		
		// performance. if its smaller than where 100 stops would fit in then go with the region direct
		if (map.region.span.latitudeDelta <= 0.01796550 && map.region.span.longitudeDelta <= 0.01982689)
			stopsInRegion = [[StopList sharedManager] getStopsInRegion:map.region];
		else
		{
			// for the lower performing devices we need to lower our limit
			UIDevicePlatform platform = [[UIDevice currentDevice] platformType];
			switch (platform)
			{
				case UIDevice1GiPhone:
				case UIDevice1GiPod:
				case UIDevice2GiPod:
				case UIDevice3GiPhone:
				case UIDevice3GSiPhone:
					maxStopCount = 20;
                default:
                    break;
			}
			
			stopsInRegion = [[StopList sharedManager] getNearestStopsWithoutDistancesToLocation:loc count:(self.showTicketRetailers ? maxStopCount/2 : maxStopCount)];
		}
		
		
		// find ones that we haven't displayed already
		for (Stop *s in stopsInRegion)
		{
			if (![self.stopList containsObject:s])
				[stopsToAdd addObject:s];
		}
		[map addAnnotations:stopsToAdd];

		// now that we've added the extra ones, remove the ones that are outside the list
		for (Stop *s in self.stopList)
		{
			if (![stopsInRegion containsObject:s])
				[stopsToRemove addObject:s];
		}
		if ([stopsToRemove count] > 0)
			[map removeAnnotations:stopsToRemove];
		
		[self setStopList:stopsInRegion];
		[stopsToAdd release];
		[stopsToRemove release];
	}

	// Now we do the same thing for the ticket retailers, if they're shown
	if (self.showTicketRetailers)
	{
		NSMutableArray *retailersToAdd = [[NSMutableArray alloc] initWithCapacity:0];
		NSMutableArray *retailersToRemove = [[NSMutableArray alloc] initWithCapacity:0];
		NSArray *ticketRetailersInRegion;

		// performance. if its smaller than where 100 retailers would fit in then go with the region direct
		if (map.region.span.latitudeDelta <= 0.01796550 && map.region.span.longitudeDelta <= 0.01982689)
			ticketRetailersInRegion = [TicketRetailer ticketRetailersInRegion:map.region];
		else
			ticketRetailersInRegion = [TicketRetailer nearestTicketRetailersWithoutDistancesToLocation:loc count:maxStopCount/2];
		
		
		// find ones that we haven't displayed already
		for (TicketRetailer *r in ticketRetailersInRegion)
		{
			if (self.retailerList == nil || ![self.retailerList containsObject:r])
				[retailersToAdd addObject:r];
		}
		[map addAnnotations:retailersToAdd];
		
		// now that we've added the extra ones, remove the ones that are outside the list
		if (self.retailerList != nil)
		{
			for (TicketRetailer *r in self.retailerList)
			{
				if (![ticketRetailersInRegion containsObject:r])
					[retailersToRemove addObject:r];
			}
			if ([retailersToRemove count] > 0)
				[map removeAnnotations:retailersToRemove];
		}
		
		[self setRetailerList:ticketRetailersInRegion];
		[retailersToAdd release];
		[retailersToRemove release];
	}
	
	
	
	[loc release];
}

- (void)mapViewWillStartLoadingMap:(MKMapView *)aMapView
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

- (void)mapViewDidFinishLoadingMap:(MKMapView *)aMapView
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

	// if its a connection map then highlight the current stop
	if ((listViewType == TTMapViewListTypeConnectionList || listViewType == TTMapViewListTypeSelectedStop || listViewType == TTMapViewListTypeNearbyTickets) && self.selectedStop != nil && selectedStopShouldBeShown)
	{
		[self.mapView selectAnnotation:self.selectedStop animated:NO];
		selectedStopShouldBeShown = NO;
	}
}

- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
	// we track the user's location the same way we do with the list view
	//NSLog(@"[MAP] Received Location Update, Type: %i, Tracking: %@", self.listViewType, self.trackUserLocation ? @"YES" : @"NO");
	if (self.listViewType == TTMapViewListTypeNearbyList && self.trackUserLocation)
	{
		// Set the location
		CLLocation *newLocation = userLocation.location;
		
		// get the 10 nearest stops
		NSArray *nearestStops = [[StopList sharedManager] getNearestStopsWithoutDistancesToLocation:newLocation count:10];
		MKCoordinateRegion newRegion = [self.mapView regionThatFits:[self regionThatFitsStops:nearestStops]];
		
		// re-centre the map thingo
		[self.mapView setRegion:newRegion animated:YES];
		
		// What sort of accuracy do we need for here?
		NSTimeInterval secondsPassed = [[NSDate date] timeIntervalSinceDate:self.locatingStarted];
		NSInteger requiredAccracy = secondsPassed > NEARBY_THRESHOLD_500M ? 500 : (secondsPassed > NEARBY_THRESHOLD_200M ? 200 : 50);
		
		// check this location for accuracy
		if (self.manuallyTracking && newLocation.horizontalAccuracy <= requiredAccracy)
		{
			//NSLog(@"Required accuracy of %dm has been met (%.2fm). Stopping tracking %@", requiredAccracy, newLocation.horizontalAccuracy, newLocation);
			[self stopTracking];
			
		} else
		{
			//NSLog(@"Ignoring location %@ because accuracy %.2fm does not meet required accuracy of %dm", newLocation, newLocation.horizontalAccuracy, requiredAccracy);
		}
	}
}

// CLLocationManagerDelete wrapper!
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
	// we're just a wrapper, call the normal map view one
	[self mapView:self.mapView didUpdateUserLocation:self.mapView.userLocation];
}

- (void)setTrackUserLocation:(BOOL)shouldTrackUserLocation
{
	// is the button there?
	if (self.nearbyViewController.navigationItem.leftBarButtonItem != nil)
	{
		if (shouldTrackUserLocation)
			[self.nearbyViewController.navigationItem setLeftBarButtonItem:self.stopButton animated:YES];
		else
			[self.nearbyViewController.navigationItem setLeftBarButtonItem:self.locateButton animated:YES];
	}
	trackUserLocation = shouldTrackUserLocation;
}
- (void)stopTracking
{
	[self setTrackUserLocation:NO];
}
- (void)startTracking
{
	[self setManuallyTracking:YES];
	[self setTrackUserLocation:YES];
	
	// if we have a known location, move there immediately
	if (self.mapView.userLocation != nil)
		[self mapView:self.mapView didUpdateUserLocation:self.mapView.userLocation];
}

#pragma mark -
#pragma mark Ticket Retailer Methods

- (void)displayTicketRetailers
{
	// do we have a cache?
	if (self.retailerList != nil)
	{
		// re-add these to the map immediately
		[self.mapView addAnnotations:self.retailerList];
	}
	
	// initiate a re-draw of the current set, nothing should change on the stop side, but the retailers might
	[self mapView:self.mapView regionDidChangeAnimated:NO];
}

- (void)hideTicketRetailers
{
	// clear the retailer annotations from the map
	if (self.retailerList != nil)
		[self.mapView removeAnnotations:self.retailerList];

	// then refresh
	[self mapView:self.mapView regionDidChangeAnimated:NO];
	
	// we don't clear the retailer list array, we keep it cached for later
	// it will be removed automatically if we need to clean up some memory
}

- (void)setShowTicketRetailers:(BOOL)shouldShowTicketRetailers
{
	showTicketRetailers = shouldShowTicketRetailers;
	
	if (shouldShowTicketRetailers)
		[self displayTicketRetailers];
	else
		[self hideTicketRetailers];
}

- (void)toggleTicketRetailers
{
	// show or hide?
	if (self.showTicketRetailers)
	{
		// Currently showing ticket retailers, lets hide them
		[self setShowTicketRetailers:NO];
		[self.ticketsButton setStyle:UIBarButtonItemStylePlain];
	} else
	{
		// Currently not showing ticket retailers, show them
		[self setShowTicketRetailers:YES];
		[self.ticketsButton setStyle:UIBarButtonItemStyleDone];
	}
}


#pragma mark -
#pragma mark Housekeeping Methods

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
	if (!self.showTicketRetailers)
		[self setRetailerList:nil];
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;

	// unregister for notifications
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc removeObserver:self];

}


- (void)dealloc {
	[stopList release];
    
    [mapView setDelegate:nil];
	[mapView release]; mapView = nil;
	[route release];
	[purpleStops release];
	[selectedStop release];
	[arrivalDate release];
	[ticketsButton release];
	[retailerList release];
	[selectedRetailer release];
	[activityController release];
	[locateButton release];
	[stopButton release];
	[locationManager release]; locationManager = nil;

    [super dealloc];
}


@end
