//
//  tramTRACKERAppDelegate.h
//  tramTRACKER
//
//  Created by Robert Amos on 17/02/09.
//  Copyright Yarra Trams 2009. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PIDViewController.h"
#import "StopList.h"
#import "RouteList.h"
#import "MainListController.h"
#import "RouteListController.h"
#import "NearbyViewController.h"
#import "SearchListController.h"
#import "EnterTrackerIDController.h"
#import "EnterTramNumberController.h"
#import "MostRecentController.h"
#import "SettingsListView.h"
#import "HelpController.h"
#import "Location.h"
#import "ServiceChangesController.h"
#import "Filter.h"
#import "Tram.h"
#import "OnboardViewController.h"
#import "PagedPIDViewController.h"
#import "UpdatesController.h"
#import "TicketRetailerList.h"

extern CGFloat const TTBackgroundColorRed;
extern CGFloat const TTBackgroundColorGreen;
extern CGFloat const TTBackgroundColorBlue;
extern CGFloat const TTNavigationBarTintColorRed;
extern CGFloat const TTNavigationBarTintColorGreen;
extern CGFloat const TTNavigationBarTintColorBlue;
extern CGFloat const TTSearchBarTintColorRed;
extern CGFloat const TTSearchBarTintColorGreen;
extern CGFloat const TTSearchBarTintColorBlue;

extern NSInteger const PredictionListCellHeight;

extern NSInteger const PIDShowMessagesNone;
extern NSInteger const PIDShowMessagesOnce;
extern NSInteger const PIDShowMessagesTwice;
extern NSInteger const PIDShowMessagesAlways;

extern NSString * const AlertSoundNone;
extern NSString * const AlertSoundSystemDefault;
extern NSString * const AlertSoundTramAClass;
extern NSString * const AlertSoundTramCClass;
extern NSString * const AlertSoundTramWClass;

extern NSString * const TTTabKeyFavourites;
extern NSString * const TTTabKeyNearby;
extern NSString * const TTTabKeyBrowse;
extern NSString * const TTTabKeyOnboard;
extern NSString * const TTTabKeySearch;
extern NSString * const TTTabKeyEnterID;
extern NSString * const TTTabKeySchedules;
extern NSString * const TTTabKeyServiceChanges;
extern NSString * const TTTabKeyMostRecent;
extern NSString * const TTTabKeySettings;
extern NSString * const TTTabKeyHelp;
extern NSString * const TTTabKeyUpdates;
extern NSString * const TTTabKeyTickets;

extern NSString * const TTTicketOutletUpdateURL;

extern NSString * const TTTicketOutletsUpdateRequired;
extern NSString * const TTTicketOutletsUpdateNotRequired;

extern NSTimeInterval const TTTicketsFileModicationDate;
extern NSTimeInterval const TTDatabaseModificationDate;

extern NSString * const TTTicketOutletsHaveBeenUpdatedNotice;
extern NSString * const TTStopsAndRoutesHaveBeenUpdatedNotice;

@class PIDViewController;
@class BackgroundSynchroniser;

@interface tramTRACKERAppDelegate : NSObject <UIApplicationDelegate, UITabBarControllerDelegate, UINavigationControllerDelegate, UIAlertViewDelegate, NSNetServiceDelegate> {
    UIWindow *window;
	UINavigationController *favouritesNavigationController;
	UINavigationController *browseNavigationController;
	MainListController *favouritesList;
	UITabBarController *tabBar;
	PidsService *pidService;
	UINavigationController *searchController;
	UINavigationController *nearbyController;
	UINavigationController *enterIDController;
	UINavigationController *recentController;
	MostRecentController *recent;
	UINavigationController *settingsController;
	UINavigationController *onboardDisplaysController;
	EnterTramNumberController *tramNumberController;
	UINavigationController *aboutController;
	UINavigationController *scheduledController;
	UINavigationController *serviceChangesController;
	UINavigationController *updatesNavigationController;
	UINavigationController *ticketsNavigationController;
	UpdatesController *updatesController;
	NSString *urlToRestore;

    NSManagedObjectModel *managedObjectModel;
    NSManagedObjectContext *managedObjectContext;	    
    NSPersistentStoreCoordinator *persistentStoreCoordinator;
	NSNetService *tramDetector;
	NSDictionary *tabStorage;
	NSArray *defaultTabOrder;
	BackgroundSynchroniser *synchroniser;
	NSDate *lastTapTime;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UINavigationController *favouritesNavigationController;
@property (nonatomic, retain) MainListController *favouritesList;
@property (nonatomic, retain) UITabBarController *tabBar;
@property (nonatomic, retain) UINavigationController *browseNavigationController;
@property (nonatomic, retain) UINavigationController *searchController;
@property (nonatomic, retain) UINavigationController *nearbyController;
@property (nonatomic, retain) UINavigationController *enterIDController;
@property (nonatomic, retain) UINavigationController *recentController;
@property (nonatomic, retain) MostRecentController *recent;
@property (nonatomic, retain) UINavigationController *settingsController;
@property (nonatomic, retain) UINavigationController *onboardDisplaysController;
@property (nonatomic, retain) EnterTramNumberController *tramNumberController;
@property (nonatomic, retain) UINavigationController *aboutController;
@property (nonatomic, retain) UINavigationController *scheduledController;
@property (nonatomic, retain) UINavigationController *serviceChangesController;
@property (nonatomic, retain) UINavigationController *updatesNavigationController;
@property (nonatomic, retain) UINavigationController *ticketsNavigationController;
@property (nonatomic, retain) UpdatesController *updatesController;
@property (nonatomic, retain) NSNetService *tramDetector;

@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, readonly) NSString *applicationDocumentsDirectory;
@property (nonatomic, readonly) NSString *ticketOutletsFile;

@property (nonatomic, retain) NSDate *lastTapTime;


- (void)initFavourites;
- (void)initBrowse;
- (void)initSearch;
- (void)initEnterTrackerID;
- (void)initNearby;
- (void)initMostRecent;
- (void)initTabBar;
- (NSArray *)orderedTabs;
- (void)startupWithPreferencesAndAllowRestore:(BOOL)canRestore;
- (BOOL)startupWithURL:(NSURL *)url allowRestore:(BOOL)canRestore;
- (NSDictionary *)dictionaryFromQueryString:(NSString *)queryString;
- (void)initSettings;
- (void)initOnboard;
- (void)initAbout;
- (void)initScheduled;
- (void)initServiceChanges;
- (void)initUpdates;
- (void)initTicketRetailers;
- (void)registerDefaults;
- (void)presentWelcomeScreen;
- (void)lookForTramsInRange;
- (void)synchroniseData;
- (NSDate *)lastSyncDate;
- (NSDate *)lastTicketSyncDate;
- (void)setLastSyncDate:(NSDate *)lastSyncDate;
- (void)syncDidFailWithMessage:(NSString *)message;
- (NSString *)restoreURL;

- (NSDictionary *)lastOpenedPID;
- (void)setLastOpenedPID:(NSDictionary *)pidInfo;

@end

