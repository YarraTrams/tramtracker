//
//  HelpFavouritesController.h
//  tramTRACKER
//
//  Created by Robert Amos on 5/05/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelpBaseController.h"
#import "SimpleCell.h"
#import "GradientBackgroundCell.h"
#import "HelpWebController.h"

extern const NSString *HelpURLSubdirFavourites;

@interface HelpFavouritesController : HelpBaseController {

}

- (id)initHelpFavouritesController;

@end
