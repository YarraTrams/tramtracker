//
//  ActivityBarButtonController.m
//  tramTRACKER
//
//  Created by Robert Amos on 7/04/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "ActivityBarButtonController.h"

@implementation ActivityBarButtonController

@synthesize activityIndicator, accuracyLevel;

 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil target:(id)clickTarget action:(SEL)clickAction {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
		accuracyLevel = 1;
		
		// hold on to our action
		target = clickTarget;
		action = clickAction;
		
		// Add the UIImageView
		UIImageView *button = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"blankBarButton.png"]];
		UIImageView *buttonHighlighted = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"blankBarButtonHighlighted.png"]];
		[buttonHighlighted setHidden:YES];

		// Fix up the view's frame
		[self.view setFrame:button.frame];

		[self.view addSubview:button];
		[button release];
		
		// Add the highlighted button
		[self.view addSubview:buttonHighlighted];
		[buttonHighlighted release];
		
		// Add the UIActivityIndicator
		UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
		[indicator setHidesWhenStopped:YES];
		[self.view addSubview:indicator];
		[self setActivityIndicator:indicator];
		[indicator startAnimating];
		[indicator release];

		// move the indicator to the middle
		CGRect buttonFrame = self.view.frame;
		CGRect indicatorFrame = self.activityIndicator.frame;
		indicatorFrame.origin.x = (buttonFrame.size.width / 2) - (indicatorFrame.size.width / 2);
		indicatorFrame.origin.y = (buttonFrame.size.height / 2) - (indicatorFrame.size.height / 2);
		[self.activityIndicator setFrame:indicatorFrame];
		
		// slip a button over the top of it
		UIButton *fakeButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[fakeButton setFrame:buttonFrame];
		[fakeButton addTarget:self action:@selector(touchDown) forControlEvents:UIControlEventTouchDown];
		[fakeButton addTarget:self action:@selector(touchUpInside) forControlEvents:UIControlEventTouchUpInside];
		[fakeButton addTarget:self action:@selector(touchUpOutside) forControlEvents:UIControlEventTouchUpOutside];
		
		[self.view addSubview:fakeButton];
	}
    return self;
}

- (void)touchDown
{
	// touched the control, highlight the imageview
	UIImageView *imageView = (UIImageView *)[self.view.subviews objectAtIndex:1];
	[imageView setHidden:NO];
}

- (void)touchUpInside
{
	// unhighlight
	UIImageView *imageView = (UIImageView *)[self.view.subviews objectAtIndex:1];
	[imageView setHidden:YES];

	// pass on our action
	if ([target respondsToSelector:action])
		[target performSelector:action];
}

- (void)touchUpOutside
{
	// unhighlight
	UIImageView *imageView = (UIImageView *)[self.view.subviews objectAtIndex:1];
	[imageView setHidden:YES];
}

- (void)setAccuracyLevel:(NSInteger)anAccuracyLevel
{
	// dont do anything if its the same.
	if (accuracyLevel == anAccuracyLevel)
		return;
	
	// set the accuracy level
	accuracyLevel = anAccuracyLevel;
	
	// change the images on the buttons
	UIImage *normalImage;
	UIImage *highlightImage;
	UIActivityIndicatorViewStyle activityStyle;
	
	switch (accuracyLevel)
	{
		case 1:
		default:
			normalImage = [UIImage imageNamed:@"blankBarButton.png"];
			highlightImage = [UIImage imageNamed:@"blankBarButtonHighlighted.png"];
			activityStyle = UIActivityIndicatorViewStyleWhite;
			break;
			
		case 2:
			normalImage = [UIImage imageNamed:@"blankBarButtonLightGreen.png"];
			highlightImage = [UIImage imageNamed:@"blankBarButtonHighlightedLightGreen.png"];
			activityStyle = UIActivityIndicatorViewStyleGray;
			break;
			
		case 3:
			normalImage = [UIImage imageNamed:@"blankBarButtonYellow.png"];
			highlightImage = [UIImage imageNamed:@"blankBarButtonHighlightedYellow.png"];
			activityStyle = UIActivityIndicatorViewStyleGray;
			break;
			
		case 4:
			normalImage = [UIImage imageNamed:@"blankBarButtonOrange.png"];
			highlightImage = [UIImage imageNamed:@"blankBarButtonHighlightedOrange.png"];
			activityStyle = UIActivityIndicatorViewStyleWhite;
			break;
			
		case 5:
			normalImage = [UIImage imageNamed:@"blankBarButtonRed.png"];
			highlightImage = [UIImage imageNamed:@"blankBarButtonHighlightedRed.png"];
			activityStyle = UIActivityIndicatorViewStyleWhite;
			break;
	}
	// the normal button is the first subview, highlight is the second
	UIImageView *normalButton = (UIImageView *)[[self.view subviews] objectAtIndex:0];
	UIImageView *highlightButton = (UIImageView *)[[self.view subviews] objectAtIndex:1];
	
	// change the images over
	[normalButton setImage:normalImage];
	[highlightButton setImage:highlightImage];
	[self.activityIndicator setActivityIndicatorViewStyle:activityStyle];
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[activityIndicator release];
    [super dealloc];
}


@end
