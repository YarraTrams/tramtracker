//
//  Location.m
//  tramTRACKER
//
//  Created by Robert Amos on 20/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "Location.h"


@implementation LocationManager

// we override a stack of functions should we be using the iPhone Simulator

#if TARGET_IPHONE_SIMULATOR
@synthesize refreshTimer, __oldLocation, location;
#endif

@synthesize stopped;

- (id)init
{
	if (self = [super init])
	{
		stopped = YES;
	}
	return self;
}

- (void)startUpdatingLocation
{
	//NSLog(@"[LOCATION] We've been told to start. %@", self);
	
	// start by creating the timer
#if TARGET_IPHONE_SIMULATOR
	if (self.refreshTimer == nil)
	{
		[self setRefreshTimer:[NSTimer scheduledTimerWithTimeInterval:2
															   target:self
															 selector:@selector(updateLocation:)
															 userInfo:nil
															  repeats:YES]];
	}
#else
	[super startUpdatingLocation];
#endif

	stopped = NO;
}


- (void)stopUpdatingLocation
{
	//NSLog(@"[LOCATION] We've been told to stop. %@", self);
	stopped = YES;
	[super stopUpdatingLocation];

#if TARGET_IPHONE_SIMULATOR
	[self.refreshTimer invalidate];
	[self setRefreshTimer:nil];
#endif
}

#if TARGET_IPHONE_SIMULATOR
- (void)updateLocation:(NSTimer *)aTimer
{
	// have we stopped?
	if (stopped)
		return;
	
	// Connect to the local web server to grab stuff
	NSDictionary *loc = [[NSDictionary alloc] initWithContentsOfURL:[NSURL URLWithString:@"http://127.0.0.1/~bok/location.plist"]];
	// Create a location object
	CLLocationCoordinate2D coord;
	coord.latitude = [(NSNumber *)[loc objectForKey:@"latitude"] floatValue];
	coord.longitude = [(NSNumber *)[loc objectForKey:@"longitude"] floatValue];
	CGFloat accuracy = [(NSNumber *)[loc objectForKey:@"accuracy"] floatValue];
	CLLocation *newLocation = [[CLLocation alloc] initWithCoordinate:coord
															altitude:0
												  horizontalAccuracy:accuracy
													verticalAccuracy:0
														   timestamp:[NSDate date]];
	[self set__oldLocation:self.location];
	[newLocation retain];
	[location release];
	location = newLocation;
	
	// Fire it off to the delegate
	if (!stopped && self.delegate != nil && [self.delegate respondsToSelector:@selector(locationManager:didUpdateToLocation:fromLocation:)])
	{
		// perform it
		[self.delegate locationManager:self didUpdateToLocation:newLocation fromLocation:self.__oldLocation];
	}

	// release and go
	[newLocation release];
	[loc release];
}

#endif


- (BOOL)locationServicesEnabled
{
	// 4.0 or up the class will respond
	if ([CLLocationManager respondsToSelector:@selector(locationServicesEnabled)])
		return [CLLocationManager locationServicesEnabled];
	
	// otherwise, use this instance
	return [super locationServicesEnabled];
}

+ (CLLocationDistance)distanceFromLocation:(CLLocation *)from toLocation:(CLLocation *)to
{
	// 3.1 and below, use getDistanceFrom:
	if ([from respondsToSelector:@selector(getDistanceFrom:)])
	{
		NSMethodSignature *sig = [from methodSignatureForSelector:@selector(getDistanceFrom:)];
		NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:sig];
		[invocation setSelector:@selector(getDistanceFrom:)];
		[invocation setTarget:from];
		[invocation setArgument:&to atIndex:2];
		[invocation invoke];
		
		CLLocationDistance distance = 0;
		[invocation getReturnValue:&distance];
		return distance;
	}

	// 3.2 and up use distanceFromLocation:
	return [from distanceFromLocation:to];
}

- (void)dealloc
{
	//NSLog(@"[LOCATION] Dealloc'ing %@", self);
	[self setDelegate:nil];
	[super dealloc];
}

@end

