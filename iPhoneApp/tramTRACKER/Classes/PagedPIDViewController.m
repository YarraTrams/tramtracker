//
//  PagedPIDViewController.m
//  tramTRACKER
//
//  Created by Robert Amos on 2/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "PagedPIDViewController.h"
#import "FavouriteStop.h"

#define NEARBY_FAVOURITE_THRESHOLD_REGULAR 2
#define NEARBY_FAVOURITE_THRESHOLD_TWICE 4
#define NEARBY_FAVOURITE_THRESHOLD_MAX 6

@implementation PagedPIDViewController

@synthesize favourites, pageControl, currentPID, viewContainer, currentPage, isFavouritePIDs, locationManager, leftFakePID, rightFakePID, showMessages, filter, pagingAvailable, editing;

//
// Initializer
//
- (id)initWithFavourites:(NSArray *)favouriteStops withStartingIndex:(NSUInteger)startingIndex
{
	if (self = [self initWithNibName:@"PagedPID" bundle:[NSBundle mainBundle]])
	{
		// save the stop list
		[self setFavourites:favouriteStops];
		
		// is paging available?
		[self setPagingAvailable:[self.favourites count] <= 13];
		
		// and the starting index
		[self setCurrentPage:startingIndex];

		// register to receive notifications
		NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
		
		[nc addObserver:self
			   selector:@selector(changePageLeftFromSwipe:)
				   name:@"PIDSwipeLeft"
				 object:nil];
		[nc addObserver:self
			   selector:@selector(changePageRightFromSwipe:)
				   name:@"PIDSwipeRight"
				 object:nil];
		
		// hide bottom bar
		[self setHidesBottomBarWhenPushed:YES];
		
		// defaults
		[self setShowMessages:-1];
	}
	
	return self;
}

//
// Initializer with nearest favourite list
//
- (id)initNearestWithFavourites:(NSArray *)favouriteStops
{
	if (self = [self initWithFavourites:favouriteStops withStartingIndex:0])
	{
		[self setIsFavouritePIDs:YES];
		[self setCurrentPage:0];
	}
	return self;
}

- (void)viewDidLoad
{
	if (editing) return;

	// display a PID on there
	PIDViewController *PID = [[PIDViewController alloc] initWithNibName:@"PIDView" bundle:[NSBundle mainBundle]];
	[self setCurrentPID:PID];
	[PID release];

	if (self.filter != nil)
		[self.currentPID setFilter:self.filter];
	if (self.showMessages != -1)
		[self.currentPID setShowMessages:self.showMessages];

	[self.currentPID setParent:self];
}

//
// When the view is ready to appear add the PID
//
- (void)viewWillAppear:(BOOL)animated
{
	// and change the navigation bar over
	//[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque];

	if (editing) return;

	// add this pid to the current view
	[self.currentPID viewWillAppear:animated];
	[self.viewContainer addSubview:self.currentPID.view];
}

//
// When the view loads completely, update the page control
//
- (void)viewDidAppear:(BOOL)animated
{
	if (editing)
	{
		editing = NO;
		return;
	}
	[self.currentPID viewDidAppear:animated];

	// if we're going on a navigation controller make it translucent
	if (self.navigationController)
	{
		if ([self.navigationController.navigationBar respondsToSelector:@selector(setTranslucent:)])
			[self.navigationController.navigationBar setTranslucent:YES];
		else
			[self.navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
	}
	
	// regular startup if we're not a favourite PID
	if (!self.isFavouritePIDs)
	{
		// update the page control
		if (![self isPagingAvailable])
			[self.pageControl setHidden:YES];
		else
		{
			[self.pageControl setNumberOfPages:[self.favourites count]];
			[self.pageControl setCurrentPage:self.currentPage];
		}
		
		// get the stop info
		FavouriteStop *fav = [self.favourites objectAtIndex:self.currentPage];
		[self.currentPID setFavouriteStopIndexPath:fav.indexPath];
		[self setFilter:fav.filter];
		[self.currentPID setFilter:self.filter];
		[self.currentPID startWithStop:fav.stop];

	
	} else
	{
		// create a new location manager
		self.locationManager = [[[LocationManager alloc] init] autorelease];
		
		// if location services are not enabled then just display the first stop
		if (![self.locationManager locationServicesEnabled])
		{
			[self setIsFavouritePIDs:NO];
			
			// update the page control
			[self.pageControl setNumberOfPages:[self.favourites count]];
			[self.pageControl setCurrentPage:self.currentPage];
			
			// get the stop info
			FavouriteStop *fav = [self.favourites objectAtIndex:self.currentPage];
			[self.currentPID setFavouriteStopIndexPath:fav.indexPath];
			[self setFilter:fav.filter];
			[self.currentPID setFilter:self.filter];
			[self.currentPID startWithStop:fav.stop];
		} else
		{
			// location services are available
			// set the delegate and distance requirements
			[self.locationManager setDelegate:self];
			[self.locationManager setDistanceFilter:kCLLocationAccuracyBest];
			
			// display the locating animation
			[self.currentPID hideLoading];
			[self.currentPID showLocating];
			[self.currentPID setFilter:nil];
			
			// start updating the location
			sameStopResult = 0;
			locationUpdateStarted = [NSDate date];
			[locationUpdateStarted retain];
			[locationManager startUpdatingLocation];

			// set a timer to display results
			[NSTimer scheduledTimerWithTimeInterval:NEARBY_FAVOURITE_THRESHOLD_REGULAR target:self selector:@selector(displayStopFromTimer:) userInfo:nil repeats:NO];
			[NSTimer scheduledTimerWithTimeInterval:NEARBY_FAVOURITE_THRESHOLD_TWICE target:self selector:@selector(displayStopFromTimer:) userInfo:nil repeats:NO];
			[NSTimer scheduledTimerWithTimeInterval:NEARBY_FAVOURITE_THRESHOLD_MAX target:self selector:@selector(displayStopFromTimer:) userInfo:nil repeats:NO];
		}
	}
	
	// Draw our fake PIDs off screen to the left and right
	UIImageView *left = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pid.png"]];
	[self.viewContainer addSubview:left];
	[left setFrame:CGRectMake(-left.frame.size.width, 0, left.frame.size.width, left.frame.size.height)];
	[self setLeftFakePID:left];
	[left release];

	UIImageView *right = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pid.png"]];
	[self.viewContainer addSubview:right];
	[right setFrame:CGRectMake(right.frame.size.width, 0, right.frame.size.width, right.frame.size.height)];
	[self setRightFakePID:right];
	[right release];
}

//
// When they touch the page control to change the page
//
- (IBAction)changePage:(id)sender
{
	if (![self isPagingAvailable])
		return;

	[self.pageControl setNumberOfPages:[self.favourites count]];
	
	//NSLog(@"Moving from page %lu to page %lu", self.currentPage, self.pageControl.currentPage);

	// start the animation
	[UIView beginAnimations:@"ChangePID" context:nil];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(changeToNewPIDAfterAnimation:finished:context:)];
	
	// if the navigation controller is visible then kill off the action button
	[self.navigationItem setRightBarButtonItem:nil animated:![self.navigationController isNavigationBarHidden]];

	// work out the animation duration, default to 320px/1sec, overwrite with the speed the user was dragging at otherwise
	CGFloat duration = (320 - fabs(self.currentPID.view.frame.origin.x)) / 320;
	if (touchAnimationSpeed > 0)
	{
		CGFloat newDuration = (320 - fabs(self.currentPID.view.frame.origin.x)) / touchAnimationSpeed;
		touchAnimationSpeed = 0;

		// were they dragging faster than our usual one?
		if (newDuration < duration)
		{
			duration = newDuration;

			// this means the user has already started dragging so don't ease-into the transition
			[UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
		}
	}
	
	[UIView setAnimationDuration:duration];
	
	// which way are we moving?
	if (self.currentPage > self.pageControl.currentPage)
	{
		// moving to the right, slide the fake (the one on the left) PID to the center
		[self.leftFakePID setFrame:CGRectMake(0, 0, 320, 460)];
		
		// and slide the current PID off to the right
		[self.currentPID.view setFrame:CGRectMake(320, 0, 320, 460)];

	} else
	{
		// to the left - slide the fake (the one on the right) PID to the center
		[self.rightFakePID setFrame:CGRectMake(0, 0, 320, 460)];
		
		// slide the current PID off to the left
		[self.currentPID.view setFrame:CGRectMake(-320, 0, 320, 460)];
	}
	
	// and commit the animation
	[UIView commitAnimations];
}

//
// Remove the old page after an animation has hidden it
//
- (void)changeToNewPIDAfterAnimation:(NSString *)animationID finished:(BOOL)finished context:(NSString *)context
{
	// get rid of the current PID
	[self.currentPID setParent:nil];
	[self.currentPID stop];
	[self.currentPID.view removeFromSuperview];
	[self setCurrentPID:nil];

	// draw the new PID on top of the fake PID
	PIDViewController *PID = [[PIDViewController alloc] initWithNibName:@"PIDView" bundle:[NSBundle mainBundle]];
	[self.viewContainer addSubview:PID.view];

	FavouriteStop *fav = [self.favourites objectAtIndex:self.pageControl.currentPage];
	
	// get the filter
	[self setFilter:fav.filter];
	
	[PID setParent:self];
	[PID setFilter:fav.filter];
	[PID setFavouriteStopIndexPath:fav.indexPath];
	[PID startWithStop:fav.stop];
	[PID updateDisplayedStopInformation];
	[PID showActionButton];

	[self setCurrentPID:PID];
	[PID release];

	// then reset our fake PIDs back to their original start positions
	[self.leftFakePID setFrame:CGRectMake(-320, 0, 320, 460)];
	[self.rightFakePID setFrame:CGRectMake(320, 0, 320, 460)];

	[self setCurrentPage:self.pageControl.currentPage];
	
	// Stop looking for the nearest one
	if (self.locationManager)
		[self.locationManager stopUpdatingLocation];
}

//
// The location Service has told us there was an error
//
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
	// if they've clicked "no" and we're not permitted to update, fail over to showing the first favourite
	if ([error code] == kCLErrorDenied)
	{
		[manager stopUpdatingLocation];
		[locationUpdateStarted release];

		[self setIsFavouritePIDs:NO];

		// update the page control
		[self.pageControl setNumberOfPages:[self.favourites count]];
		[self.pageControl setCurrentPage:self.currentPage];
		
		// get the stop info
		
		FavouriteStop *fav = [self.favourites objectAtIndex:self.currentPage];
		[self.currentPID startWithStop:fav.stop];
	}
}

//
// We got a new location
//
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
	// has the location actually changed?
	//NSLog(@"Received location update.");
	

	// the location manager might have a more recent location
	if ([manager.location.timestamp isEqualToDate:[manager.location.timestamp laterDate:newLocation.timestamp]])
		newLocation = manager.location;

	// is it more than 2 minutes old?
	if (fabs([newLocation.timestamp timeIntervalSinceNow]) > 120)
	{
		//NSLog(@"Discarding location more than 2 minutes old");
		return;
	}
	
	// What sort of accuracy do we need for here?
	NSTimeInterval secondsPassed = [[NSDate date] timeIntervalSinceDate:locationUpdateStarted];
	CLLocationAccuracy requiredAccuracy = [[StopList sharedManager] distanceFilterForFavourites];
	requiredAccuracy = secondsPassed > NEARBY_FAVOURITE_THRESHOLD_TWICE ? requiredAccuracy*4 : (secondsPassed > NEARBY_FAVOURITE_THRESHOLD_REGULAR ? requiredAccuracy*2 : requiredAccuracy);
	
	if (oldLocation == nil || [LocationManager distanceFromLocation:newLocation toLocation:oldLocation] > 0)
	{
		//NSLog(@"Found new location %@", newLocation);
		
		// check this location for accuracy
		if (newLocation.horizontalAccuracy <= requiredAccuracy)
		{
			//NSLog(@"Required accuracy of %.2fm has been met (%.2fm). Using location %@", requiredAccuracy, newLocation.horizontalAccuracy, newLocation);
			
			[self displayStopWithLocation:newLocation];
			[manager stopUpdatingLocation];
		} else
		{
			//NSLog(@"Ignoring location %@ because accuracy %.2fm does not meet required accuracy of %.2fm", newLocation, newLocation.horizontalAccuracy, requiredAccuracy);
		}
	}
}


- (void)displayStopWithLocation:(CLLocation *)location
{
	// find the nearest stop
	FavouriteStop *nearestFavourite = [[StopList sharedManager] getNearestFavouriteToLocation:location];
	//NSLog(@"Nearest stop is now %@", nearestFavourite);

	// and update the current page
	[self.pageControl setNumberOfPages:[self.favourites count]];
	[self.pageControl setCurrentPage:[self.favourites indexOfObject:nearestFavourite]];
	[self setCurrentPage:self.pageControl.currentPage];
	[self.currentPID setFavouriteStopIndexPath:nearestFavourite.indexPath];

	// filtering
	[self setFilter:[[self.favourites objectAtIndex:self.currentPage] filter]];
	[self.currentPID setFilter:self.filter];
	
	// display the nearest stop
	[self.currentPID changeStop:[nearestFavourite stop]];
	[self.currentPID setInformation:[nearestFavourite stop]];

	// stop locating and start loading
	[self.currentPID hideLocating];
	[self.currentPID showLoading];
}

- (void)displayStopFromTimer:(NSTimer *)aTimer
{
	// get the current location
	if (self.locationManager == nil || self.locationManager.location == nil || [self.locationManager stopped])
	{
		// some error handling
		return;
	}
	
	// have we reached a threshold?
	// What sort of accuracy do we need for here?
	NSTimeInterval secondsPassed = [[NSDate date] timeIntervalSinceDate:locationUpdateStarted];
	CLLocationAccuracy requiredAccuracy = [[StopList sharedManager] distanceFilterForFavourites];
	requiredAccuracy = secondsPassed > NEARBY_FAVOURITE_THRESHOLD_TWICE ? requiredAccuracy*4 : (secondsPassed > NEARBY_FAVOURITE_THRESHOLD_REGULAR ? requiredAccuracy*2 : requiredAccuracy);
	
	if (secondsPassed >= NEARBY_FAVOURITE_THRESHOLD_MAX || self.locationManager.location.horizontalAccuracy <= requiredAccuracy)
	{
		//NSLog(@"Timeout has been reached, using location %@", self.locationManager.location);
		[self displayStopWithLocation:self.locationManager.location];
		[self.locationManager stopUpdatingLocation];
	}
}


//
// We're stopping
//
- (void)stop
{
	if (self.currentPID)
	{
		[self.currentPID setParent:nil];
		[self.currentPID stop];
		[self.currentPID.view removeFromSuperview];
		//[self setCurrentPID:nil];
	}

	// stop our own location stuff
	if (self.locationManager)
		[self.locationManager stopUpdatingLocation];
}


#pragma mark Gesture capturing

//
// When the user starts touching the screen
//
- (BOOL)touchesDidBegin:(NSSet *)touches withEvent:(UIEvent *)event
{
	if (![self isPagingAvailable])
		return NO;

	UITouch *touch = [touches anyObject];
	touchStarted = [touch locationInView:self.viewContainer];
	touchStartedTime = [touch timestamp];
	lastTouch = touchStarted;
	return NO;
}
- (BOOL)touchesDidMove:(NSSet *)touches withEvent:(UIEvent *)event
{
	if (![self isPagingAvailable])
		return NO;

	// if we don't have a touchStarted then ignore things
	if (CGPointEqualToPoint(touchStarted, CGPointZero))
		return NO;
	
	// move our views according to how the touches went
	CGPoint currentTouch = [[touches anyObject] locationInView:self.viewContainer];
	CGFloat distance = currentTouch.x - lastTouch.x;
	
	// if we haven't moved more than 10px then don't start just yet
	if (fabs(currentTouch.x - touchStarted.x) < 10)
		return NO;

	// make sure that we can move in that direction
	if (distance >= 0 && self.currentPage <= 0)
		return NO;
	else if (distance < 0 && self.currentPage+1 >= self.pageControl.numberOfPages)
		return NO;

	// move the left PID
	CGRect leftFrame = [self.leftFakePID frame];
	leftFrame.origin.x += distance;
	[self.leftFakePID setFrame:leftFrame];

	// move the center (real) PID
	CGRect frame = [self.currentPID.view frame];
	frame.origin.x += distance;
	[self.currentPID.view setFrame:frame];

	// move the left PID
	CGRect rightFrame = [self.rightFakePID frame];
	rightFrame.origin.x += distance;
	[self.rightFakePID setFrame:rightFrame];
	
	lastTouch = currentTouch;

	// if we've moved more than 100px in either direction trigger the end of the touch
	if (fabs(currentTouch.x - touchStarted.x) > 100)
		[self touchesEnded:touches withEvent:event];
	
	// let the touch propegate
	return NO;
}
- (BOOL)touchesWereCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
	return [self touchesDidEnd:touches withEvent:event];
}
- (BOOL)touchesDidEnd:(NSSet *)touches withEvent:(UIEvent *)event
{
	if (![self isPagingAvailable])
		return NO;

	// don't fire if we already have
	if (CGPointEqualToPoint(touchStarted, CGPointZero))
		return YES;
	
	// if we've gone more than 100 px in any direction
	UITouch *touch = [touches anyObject];
	CGPoint currentTouch = [touch locationInView:self.viewContainer];
	CGFloat distance = fabs(currentTouch.x - touchStarted.x);
	if (distance > 100)
	{
		// work out the animation speed
		touchAnimationSpeed = distance / ([touch timestamp] - touchStartedTime);
		
		// change the page in that direction
		if (currentTouch.x < touchStarted.x && self.pageControl.currentPage+2 <= self.pageControl.numberOfPages)
		{
			// yep change the page
			[self.pageControl setCurrentPage:self.pageControl.currentPage+1];
			[self changePage:self];
		}
		
		// change in the other direction
		else if (currentTouch.x > touchStarted.x && self.pageControl.currentPage > 0)
		{
			[self.pageControl setCurrentPage:self.pageControl.currentPage-1];
			[self changePage:self];
		}
		
		touchStarted = CGPointZero;
		return YES;
	}
	
	// in general, if we've moved more than 10px we wouldn't want the touch to trigger anyway
	if (distance > 10)
	{
		touchStarted = CGPointZero;

		// reset the PIDs back to where they should be.
		[UIView beginAnimations:@"ResetPID" context:nil];
		[UIView setAnimationDuration:(distance / 320)];
		
		[self.leftFakePID setFrame:CGRectMake(-320, 0, 320, 460)];
		[self.currentPID.view setFrame:CGRectMake(0, 0, 320, 460)];
		[self.rightFakePID setFrame:CGRectMake(320, 0, 320, 460)];

		[UIView commitAnimations];
		
		return YES;
	}
	
	touchStarted = CGPointZero;
	return NO;
}

#pragma mark Changing to List View

- (void)flipToListView
{
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}

- (void)viewWillDisappear:(BOOL)animated
{
	// and change the navigation bar over
	[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
	
	if (editing) return;

	if (self.navigationController)
	{
		oldNavigationController = self.navigationController;
		[oldNavigationController retain];
	}
}

-(void)viewDidDisappear:(BOOL)animated
{
	if (editing) return;

	[self stop];
	
	if (oldNavigationController)
	{
		if ([oldNavigationController.navigationBar respondsToSelector:@selector(setTranslucent:)])
			[oldNavigationController.navigationBar setTranslucent:NO];
		else
			[oldNavigationController.navigationBar setBarStyle:UIBarStyleBlackOpaque];
		[oldNavigationController release];
		oldNavigationController = nil;
	}
}

- (void)dealloc {

	// play nice and release our properties
	[favourites release];
	[pageControl release];
	[currentPID release];
	[viewContainer release];
	[locationManager release];
	[rightFakePID release];
	[leftFakePID release];
    
	[super dealloc];
}


@end
