//
//  NearbyTicketRetailersList.h
//  tramTRACKER
//
//  Created by Robert Amos on 26/07/10.
//  Copyright 2010 Metro Trains Melbourne. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Stop;

@interface NearbyTicketRetailersList : UITableViewController {
	NSArray *retailerList;
	Stop *stop;
}

@property (nonatomic, retain) NSArray *retailerList;
@property (nonatomic, retain) Stop *stop;

- (id)initWithStop:(Stop *)stop;
- (UIView *)tableFooterView;

@end
