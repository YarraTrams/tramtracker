//
//  TicketRetailerAnnotationView.m
//  tramTRACKER
//
//  Created by Robert Amos on 25/07/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "TicketRetailerAnnotationView.h"


@implementation TicketRetailerAnnotationView

- (id)initWithAnnotation:(id <MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
	if (self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier])
	{
		// Set the Pin Colour to red
		[self setPinColor:MKPinAnnotationColorRed];

		// Set the right callout view to a detail disclosure button
		UIButton *disclosure = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
		[self setRightCalloutAccessoryView:disclosure];
		[disclosure addTarget:self action:@selector(didTouchTicketCalloutAccessory:) forControlEvents:UIControlEventTouchUpInside];
		
		[self setCanShowCallout:YES];
	}
	return self;
}

- (void)didTouchTicketCalloutAccessory:(id)sender
{
	// create a Ticket Retailer view and pop it on the stack
	// get the ticket retailer, create a view for it and throw it on the stack
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	
	// send a notification that we clickied the stop
	[nc postNotificationName:@"TTDidTouchTicketCalloutAccessory" object:self.annotation];
}


@end
