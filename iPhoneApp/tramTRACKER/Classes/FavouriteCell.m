//
//  FavouriteCell.m
//  tramTRACKER
//
//  Created by Robert Amos on 6/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "FavouriteCell.h"


@implementation FavouriteCell

@synthesize name, routeDescription, lowFloorOnly;

- (id)initWithFrame:(CGRect)frame reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier]) {

		// set the background view
		GradientBackgroundCell *background = [[GradientBackgroundCell alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) cellStyle:OnboardCellStyleNone];
		[self setBackgroundView:background];
		[background release];

		// and the name label
		name = [self newLabelForName];
		[self.contentView addSubview:self.name];
		
		// and the route description
		routeDescription = [self newLabelForRouteDescription];
		[self.contentView addSubview:self.routeDescription];
	}
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setLowFloorOnly:(BOOL)newLowFloorOnly
{
	lowFloorOnly = newLowFloorOnly;
	CGRect routeRect = self.routeDescription.frame;

	// hiding the low floor only option?
	if (!lowFloorOnly)
	{
		if (lowFloorOnlyImageView != nil)
			[lowFloorOnlyImageView setHidden:YES];
		
		// make sure the route description label is in the correct place
		if (routeRect.origin.x == 30)
		{
			routeRect.origin.x = 10;
			routeRect.size.width += 20;
			[self.routeDescription setFrame:routeRect];
		}
		return;
	}

	// showing it, if we don't have the image view already then create if
	if (lowFloorOnlyImageView == nil)
	{
		lowFloorOnlyImageView = [self newLowFloorImage];
		[self.contentView addSubview:lowFloorOnlyImageView];
	}
	
	// make sure the image is visible
	[lowFloorOnlyImageView setHidden:NO];
	if (routeRect.origin.x == 10)
	{
		routeRect.origin.x = 30;
		routeRect.size.width -= 20;
		[self.routeDescription setFrame:routeRect];
	}
}

//
// Create a new name label
//
- (UILabel *)newLabelForName
{
	// position it
	CGRect frame = CGRectMake(10, 2, 300, 22);
	UILabel *newLabel = [[UILabel alloc] initWithFrame:frame];
	
	// make some happy happy changes
	[newLabel setBackgroundColor:[UIColor clearColor]];					// background colour
	[newLabel setTextColor:[UIColor blackColor]];						// text colour
	[newLabel setHighlightedTextColor:[UIColor whiteColor]];			// text colour when the cell is selected (background goes blue)
	[newLabel setOpaque:NO];											// no need for transparency here
	[newLabel setFont:[UIFont boldSystemFontOfSize:16]];				// and a 16pt font
	[newLabel setTextAlignment:UITextAlignmentLeft];					// right align the text (so it sits against the right side)
	[newLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];		// Let the UI automatically resize the width when necessary (ie editing)
	
	return newLabel;
}

//
// Create a new route description label
//
- (UILabel *)newLabelForRouteDescription
{
	// position it
	CGRect frame = CGRectMake(10, 23, 300, 18);
	UILabel *newLabel = [[UILabel alloc] initWithFrame:frame];
	
	// make some happy happy changes
	[newLabel setBackgroundColor:[UIColor clearColor]];					// background colour
	[newLabel setTextColor:[UIColor grayColor]];						// text colour
	[newLabel setHighlightedTextColor:[UIColor whiteColor]];			// text colour when the cell is selected (background goes blue)
	[newLabel setOpaque:NO];											// no need for transparency here
	[newLabel setFont:[UIFont systemFontOfSize:14]];					// and a 14pt font
	[newLabel setTextAlignment:UITextAlignmentLeft];					// right align the text (so it sits against the right side)
	[newLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];		// Let the UI automatically resize the width when necessary (ie editing)
	
	return newLabel;
}

- (UIImageView *)newLowFloorImage
{
	UIImageView *v = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lowfloor.png"]];
	CGRect imageRect = v.frame;
	imageRect.origin.x = 10;
	imageRect.origin.y = 22;
	[v setFrame:imageRect];
	return v;
}

- (void)dealloc {
	[name release];
	[routeDescription release];
	[lowFloorOnlyImageView release];

    [super dealloc];
}


@end
