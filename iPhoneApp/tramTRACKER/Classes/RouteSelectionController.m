//
//  RouteSelectionController.m
//  tramTRACKER
//
//  Created by Robert Amos on 7/04/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "RouteSelectionController.h"


@implementation RouteSelectionController

@synthesize selectedRoutes, direction, trackerID, selectedRouteIndex, displayAllRoutesOption;

- (id)initWithAllRoutesTarget:(id)aTarget action:(SEL)selector
{
	if (self = [super initWithStyle:UITableViewStyleGrouped])
	{
		[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
		[self setTitle:NSLocalizedString(@"filter-route-title", @"Select Route")];
		[self setSelectedRouteIndex:0];
		target = aTarget;
		action = selector;
	}
	return self;
}

- (id)initWithRoutes:(NSArray *)routes target:(id)aTarget action:(SEL)selector
{
	if (self = [self initWithAllRoutesTarget:aTarget action:selector])
        [self setSelectedRoutes:routes];
	return self;
}

- (id)initWithRoutes:(NSArray *)routes direction:(BOOL)isUpDirection target:(id)aTarget action:(SEL)selector
{
	if (self = [self initWithRoutes:routes target:aTarget action:selector])
        [self setDirection:isUpDirection];
	return self;
}

- (id)initWithRoutes:(NSArray *)routes trackerID:(NSNumber *)newTrackerID target:(id)aTarget action:(SEL)selector
{
	if (self = [self initWithRoutes:routes target:aTarget action:selector])
        [self setTrackerID:newTrackerID];
	return self;
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger rowCount = self.selectedRoutes == nil ? [[[RouteList sharedManager] getRouteList] count] : [self.selectedRoutes count];
	if (self.displayAllRoutesOption)
		rowCount++;
	return rowCount;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    SimpleCell *cell = (SimpleCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[SimpleCell alloc] initWithCellStyle:TTSimpleCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
	
	NSInteger row = (self.displayAllRoutesOption ? (NSInteger)indexPath.row - 1 : (NSInteger)indexPath.row); 
	
	// setup the checkmark
	if (self.selectedRouteIndex == row)
		[cell setAccessoryType:UITableViewCellAccessoryCheckmark];
	else
		[cell setAccessoryType:UITableViewCellAccessoryNone];
    
    // Set up the cell...
	if (row == -1)
	{
		[cell.textLabel setText:NSLocalizedString(@"filter-route-allroutes", @"All Routes")];
	}
	else if (self.selectedRoutes != nil)
	{
		NSString *destination;
		NSString *selectedRouteNumber = [self.selectedRoutes objectAtIndex:row];
		if (self.trackerID != nil)
			destination = [[RouteList sharedManager] destinationForRouteNumber:selectedRouteNumber trackerID:self.trackerID];
		else
			destination = [[RouteList sharedManager] destinationForRouteNumber:selectedRouteNumber isUpDirection:self.direction];
		[cell.textLabel setText:[NSString stringWithFormat:NSLocalizedString(@"filter-route-template", @"Route Template"), selectedRouteNumber, destination]];
	} else
	{
		NSArray *routeList = [[RouteList sharedManager] getRouteList];
		NSString *selectedRouteNumber = [[routeList objectAtIndex:row] number];
		NSString *destination;
		if (self.trackerID != nil)
			destination = [[RouteList sharedManager] destinationForRouteNumber:selectedRouteNumber trackerID:self.trackerID];
		else
			destination = [[RouteList sharedManager] destinationForRouteNumber:selectedRouteNumber isUpDirection:self.direction];
		[cell.textLabel setText:[NSString stringWithFormat:NSLocalizedString(@"filter-route-template", @"Route Template"), selectedRouteNumber, destination]];
	}
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSInteger row = (self.displayAllRoutesOption ? (NSInteger)indexPath.row - 1 : (NSInteger)indexPath.row);

	// pass it back to the delegate
	if (target != nil && [target respondsToSelector:action])
	{
		if (row == -1)
			[target performSelector:action withObject:@"-1"];
		else
			[target performSelector:action withObject:(self.selectedRoutes ?
													   [self.selectedRoutes objectAtIndex:row] :
													   [[[[RouteList sharedManager] getRouteList] objectAtIndex:row] number])];
	}

	// pop us off
	[self.navigationController popViewControllerAnimated:YES];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


- (void)dealloc {
    [super dealloc];
}


@end

