//
//  HelpWebController.m
//  tramTRACKER
//
//  Created by Robert Amos on 19/06/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "HelpWebController.h"


@implementation HelpWebController

@synthesize url, webView;

- (id)initWithHelpFile:(NSString *)helpFile
{
	if (self = [super init]);
	{
		[self setUrl:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", NSLocalizedString(@"help-url-base", nil), helpFile]]];
	}
	return self;
}

// First thing we do when we're going to appear is create a full-screen loading view
- (void)loadView
{
	// full screen container
	UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 367)];
	[container setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
	
	// add the twirly thing
	loadingIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	CGRect loadingFrame = loadingIndicator.frame;
	loadingFrame.origin.y = (container.frame.size.height / 2) - (loadingFrame.size.height / 2);
	loadingFrame.origin.x = (container.frame.size.width / 2) - (loadingFrame.size.width / 2) - 50;
	[loadingIndicator setFrame:loadingFrame];
	[loadingIndicator startAnimating];
	[container addSubview:loadingIndicator];
	
	// add the loading text
	loadingText = [[UILabel alloc] initWithFrame:CGRectMake(130, loadingFrame.origin.y+1, 100, 20)];
	[loadingText setText:NSLocalizedString(@"help-loading", @"Loading...")];
	[loadingText setBackgroundColor:[UIColor clearColor]];
	[loadingText setOpaque:NO];
	[loadingText setTextColor:[UIColor grayColor]];
	[loadingText setFont:[UIFont boldSystemFontOfSize:14]];
	[container addSubview:loadingText];
	
	// save the view
	[self setView:container];
	[container release];
	
	
	// now that that is all done, create a UIWebView and let it load in the background
	UIWebView *web = [[UIWebView alloc] initWithFrame:container.frame];

	// set the delegate and other conditions
	[web setDelegate:self];
	[web setScalesPageToFit:NO];
	
	// load the page
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	[web loadRequest:[NSURLRequest requestWithURL:self.url]];
	[self setWebView:web];
	[web release];
}


// web view has finished loading, replace the view
- (void)webViewDidFinishLoad:(UIWebView *)web
{
	// change the view over
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	[loadingIndicator stopAnimating];
	[loadingIndicator release];
	[loadingText release];
	loadingIndicator = nil;
	loadingText = nil;

	[self setView:web];
}

- (void)webView:(UIWebView *)web didFailLoadWithError:(NSError *)error
{
	// hide the loading text and error text
	[loadingIndicator stopAnimating];
	[loadingIndicator setHidden:YES];
	[loadingText setHidden:YES];
	
	// show the error text
	errorText = [[UILabel alloc] initWithFrame:CGRectMake(10, loadingText.frame.origin.y, 300, 40)];
	[errorText setTextColor:[UIColor blackColor]];
	[errorText setBackgroundColor:[UIColor clearColor]];
	[errorText setOpaque:NO];
	[errorText setFont:[UIFont boldSystemFontOfSize:14]];
	[errorText setTextAlignment:UITextAlignmentCenter];
	[errorText setNumberOfLines:2];
	[errorText setLineBreakMode:UILineBreakModeWordWrap];
	[errorText setText:NSLocalizedString(@"help-connection-error", nil)];
	[self.view addSubview:errorText];
	
	
	// create a retry button
	UIBarButtonItem *retry = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(retry)];
	[self.navigationItem setRightBarButtonItem:retry animated:YES];
	[retry release];
}

- (void)retry
{
	// remove the error text
	[errorText removeFromSuperview];
	[errorText release];
	errorText = nil;
	
	// re-show the loading
	[loadingText setHidden:NO];
	[loadingIndicator setHidden:NO];
	[loadingIndicator startAnimating];
	
	// reload the web view
	CGRect webFrame = self.webView.frame;
	[webView stopLoading];
	[self setWebView:nil];

	// now that that is all done, create a UIWebView and let it load in the background
	UIWebView *web = [[UIWebView alloc] initWithFrame:webFrame];
	
	// set the delegate and other conditions
	[web setDelegate:self];
	[web setScalesPageToFit:NO];
	
	// load the page
	[web loadRequest:[NSURLRequest requestWithURL:self.url]];
	[self setWebView:web];
	[web release];
	
	// remove the retry button
	[self.navigationItem setRightBarButtonItem:nil animated:YES];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc
{
	[url release];
	[webView release];
	
	if (loadingIndicator != nil)
	{
		[loadingIndicator stopAnimating];
		[loadingIndicator release];
	}
	
	if (loadingText != nil)
		[loadingText release];
	
	if (errorText != nil)
		[errorText release];
	
	[super dealloc];
}


@end
