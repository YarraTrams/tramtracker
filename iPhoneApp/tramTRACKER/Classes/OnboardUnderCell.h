//
//  OnboardUnderCell.h
//  tramTRACKER
//
//  Created by Robert Amos on 7/10/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Stop;

@interface OnboardUnderCell : UITableViewCell {
	id delegate;
	Stop *stop;
	UIButton *alarmButton;
}

@property (nonatomic, assign) id delegate;
@property (nonatomic, retain) Stop *stop;
@property (nonatomic, retain) UIButton *alarmButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier allowsAlarm:(BOOL)alarm;

- (void)showConnections;
- (void)setArrivalAlarm;
- (void)cancelArrivalAlarm;

@end

