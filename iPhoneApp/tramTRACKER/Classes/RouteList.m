//
//  RouteList.m
//  tramTRACKER
//
//  Created by Robert Amos on 6/04/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "RouteList.h"


@implementation RouteList

static RouteList *sharedRouteList = nil;

//
// Shared manager, call this to get a copy of the route list
//
+ (RouteList *)sharedManager
{
    if (sharedRouteList == nil)
        sharedRouteList = [[super allocWithZone:NULL] init];
	return sharedRouteList;
}

//
// Just in case someone tries to init us directly, wrap the call around to the shared instance
//
+ (id)allocWithZone:(NSZone *)zone
{
    return [[self sharedManager] retain];
}

- (id)copyWithZone:(NSZone *)zone
{
	return self;
}

- (id)retain
{
	return self;
}

- (unsigned)retainCount
{
	return NSUIntegerMax; // cannot be released
}

- (void)release
{
	// nothing to do, we can't release
}
- (id)autorelease
{
	return self;
}

- (NSArray *)getRouteList
{
	return [[Route allPublicRoutes] sortedArrayUsingSelector:@selector(compareRoute:)];
}
- (NSArray *)getAllRouteList
{
	return [[Route allRoutes] sortedArrayUsingSelector:@selector(compareRoute:)];
}

// Get a destination for a particular route and direction
- (NSString *)destinationForRouteNumber:(NSString *)routeNumber isUpDirection:(BOOL)upDirection
{
	Route *route = [self routeForRouteNumber:routeNumber];
	return upDirection ? route.upDestination : route.downDestination;
}

// Get a destination for a particular route and tracker id
- (NSString *)destinationForRouteNumber:(NSString *)routeNumber trackerID:(NSNumber *)trackerID
{
	Route *r = [self routeForRouteNumber:routeNumber];
	
	// is it in the up direction?
	if ([r.upStops indexOfObject:trackerID] != NSNotFound)
		return r.upDestination;
	
	// is it in the down direction?
	if ([r.downStops indexOfObject:trackerID] != NSNotFound)
		return r.downDestination;

	return nil;
}

// find the route for a specific route number - public routes only
- (Route *)routeForRouteNumber:(NSString *)routeNumber
{
	return [Route routeWithNumber:routeNumber];
}

// find the route for a specific route number with a different headboard number
- (Route *)routeForRouteNumber:(NSString *)routeNumber headboardRouteNumber:(NSString *)headboardRouteNumber
{
	return [Route routeWithNumber:routeNumber headboardRouteNumber:headboardRouteNumber];
}
@end
