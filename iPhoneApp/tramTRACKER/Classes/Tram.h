//
//  Tram.h
//  tramTRACKER
//
//  Created by Robert Amos on 19/06/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RouteStub;
@class Route;
@class TramStub;


/**
 * Represents a tram travelling on the network
**/
@interface Tram : NSObject <NSCopying>
{
	/**
	 * The Tram/Vehicle number
	**/
	NSNumber *number;

	/**
	 * The route that the tram is travelling on
	**/
	Route *route;

	/**
	 * The route number that the tram should have configured on its headboard.
	**/
	NSString *headboardRouteNumber;

	/**
	 * The route number that this tram should be travelling on.
	**/
	NSString *routeNumber;

	/**
	 * The direction that this tram is travelling on its route
	**/
	BOOL upDirection;

	/**
	 * Whether this tram is at layover
	**/
	BOOL atLayover;

	/**
	 * Whether predicted arrival times are available for this journey
	**/
	BOOL available;

	/**
	 * Whether this journey is affected by a disruption
	**/
	BOOL disrupted;

	/**
	 * Whether this journey is affected by a special event
	**/
	BOOL specialEvent;
}

@property (nonatomic, retain) NSNumber *number;
@property (nonatomic, retain) Route *route;
@property (nonatomic, retain) NSString *headboardRouteNumber;
@property (nonatomic, retain) NSString *routeNumber;
@property (nonatomic) BOOL upDirection;
@property (nonatomic, getter=isAtLayover) BOOL atLayover;
@property (nonatomic, getter=isAvailable) BOOL available;
@property (nonatomic, getter=isDisrupted) BOOL disrupted;
@property (nonatomic, getter=hasSpecialEvent) BOOL specialEvent;

/**
 * The tram's name if it has one. Or "Tram <number>" if it does not.
**/
- (NSString *)name;

/**
 * A small image representing the tram. Used on the Onboard screen
**/
- (UIImage *)image;

/**
 * A side-on profile of the tram. Used in the PID
**/
- (UIImage *)silhouette;

/**
 * A clipped side-on profile of the tram. Used in the PID
**/
- (UIImage *)clippedSilhouette;

/**
 * The class name of the tram
**/
- (NSString *)className;

/**
 * The URL (internal application URL only) to an aiff audio recording of the tram's gong. Used on the Onboard screen
**/
- (NSURL *)URLOfGongSound;
+ (NSString *)nameOfGongSoundForClass:(NSString *)tramClass;

/**
 * Whether this tram is travelling on Route Zero (returning to the depot)
**/
- (BOOL)isRouteZero;

/**
 * Whether the route the tram is travelling on is unknown (no matching route definition could be found)
**/
- (BOOL)isUnknownRoute;

/**
 * Whether this tram is travelling on the City Circle route (Route 35)
**/
- (BOOL)isCityCircle;

/**
 * Whether this tram has a pull cord
**/
- (BOOL)hasPullCord;

/**
 * The destination of the tram
**/
- (NSString *)destination;

- (BOOL)isEqualToTram:(Tram *)otherTram;

+ (Tram *)tramForStub:(TramStub *)stub;

@end

/**
 * A Tram Stub
 *
 * Allows the PIDS Service to return results from the background thread.
 * Pass it through +[Tram tramForStub:] to get a full Tram
**/
@interface TramStub : Tram {
@private
    RouteStub *routeStub;
}

@property (nonatomic, retain) RouteStub *routeStub;

@end