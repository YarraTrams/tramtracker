//
//  FeedbackController.m
//  tramTRACKER
//
//  Created by Robert Amos on 14/05/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "FeedbackController.h"

NSString * const feedbackPhone = @"1800 800 007";
NSString * const feedbackEmail = @"feedback@yarratrams.com.au";

@implementation FeedbackController

@synthesize phoneButton, emailButton;

- (void)viewDidLoad
{
	[self setTitle:NSLocalizedString(@"title-feedback", @"Feedback")];
	
	[self.phoneButton setTitle:feedbackPhone forState:UIControlStateNormal];
	[self.emailButton setTitle:feedbackEmail forState:UIControlStateNormal];
	
	// can we actually use the phone app?
	NSString *phoneURLString = [NSString stringWithFormat:@"tel:%@", [feedbackPhone stringByReplacingOccurrencesOfString:@" " withString:@""]];
	if ([[UIApplication sharedApplication] respondsToSelector:@selector(canOpenURL:)] &&
		![[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:phoneURLString]])
		[self.phoneButton setEnabled:NO];
	
	// can we use the email app?
	if (NSClassFromString(@"MFMailComposeViewController") != nil && ![MFMailComposeViewController canSendMail])
		[self.emailButton setEnabled:NO];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (IBAction)callFeedback:(id)sender
{
	NSString *phoneURLString = [NSString stringWithFormat:@"tel:%@", [feedbackPhone stringByReplacingOccurrencesOfString:@" " withString:@""]];
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneURLString]];
}

- (IBAction)emailFeedback:(id)sender
{
	// 2.2.x devices don't have built in email composing support
	if (NSClassFromString(@"MFMailComposeViewController") == nil || ![MFMailComposeViewController canSendMail])
	{
		NSString *emailURLString = [NSString stringWithFormat:@"mailto:%@", feedbackEmail];
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:emailURLString]];
		return;
	}
	
	// toggle the email composer window
	MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
	[mailComposer setMailComposeDelegate:self];
	
	// set the recipients and subject
	[mailComposer setToRecipients:[NSArray arrayWithObject:feedbackEmail]];
	[mailComposer setSubject:NSLocalizedString(@"feedback-email-subject", @"Email Subject")];
	
	[self presentModalViewController:mailComposer animated:YES];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	[self dismissModalViewControllerAnimated:YES];
}

- (void)dealloc {
    [super dealloc];
}


@end
