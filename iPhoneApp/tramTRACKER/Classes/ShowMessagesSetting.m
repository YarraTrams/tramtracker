//
//  ShowMessagesSetting.m
//  tramTRACKER
//
//  Created by Robert Amos on 30/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "ShowMessagesSetting.h"


@implementation ShowMessagesSetting

@synthesize showMessages;

- (id)initWithShowMessages:(NSInteger)messages
{
	if (self = [super initWithStyle:UITableViewStyleGrouped])
	{
		[self setShowMessages:messages];
		
		// change our background colour
		[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
		[self setHidesBottomBarWhenPushed:NO];
		
		[self setTitle:NSLocalizedString(@"settings-displayopts-showmessages", @"Show Messages on PID")];
	}
	return self;
}

- (id)initWithShowMessages:(NSInteger)messages target:(id)aTarget action:(SEL)aAction
{
	if (self = [self initWithShowMessages:messages])
	{
		target = aTarget;
		action = aAction;
	}
	return self;
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    SimpleCell *cell = (SimpleCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[SimpleCell alloc] initWithCellStyle:TTSimpleCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
	// setup the cell based on stuff
	if (indexPath.row == PIDShowMessagesNone - 1)
		[cell.textLabel setText:NSLocalizedString(@"settings-showmessages-never", @"Never")];
	else if (indexPath.row == PIDShowMessagesOnce - 1)
		[cell.textLabel setText:NSLocalizedString(@"settings-showmessages-once", @"Once")];
	else if (indexPath.row == PIDShowMessagesTwice - 1)
		[cell.textLabel setText:NSLocalizedString(@"settings-showmessages-twice", @"Twice")];
	else if (indexPath.row == PIDShowMessagesAlways - 1)
		[cell.textLabel setText:NSLocalizedString(@"settings-showmessages-always", @"Always")];

	// is this selected?
	if (indexPath.row + 1 == self.showMessages || (!self.showMessages && indexPath.row == PIDShowMessagesTwice - 1))
		[cell setAccessoryType:UITableViewCellAccessoryCheckmark];
	else
		[cell setAccessoryType:UITableViewCellAccessoryNone];
	
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	// push ourselves off the stack
	UINavigationController *nav = self.navigationController;
	[self.navigationController popViewControllerAnimated:YES];

	// set the show messages option
	if (target != nil)
	{
		if ([target respondsToSelector:action])
		{
			[target performSelector:action withObject:[NSNumber numberWithInt:indexPath.row+1]];
		}
		return;
	} else
	{
		PredictionListView *pList = (PredictionListView *)nav.topViewController;
		[pList setShowMessages:indexPath.row+1];
		[pList.tableView reloadData];
	}
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


- (void)dealloc {
    [super dealloc];
}


@end

