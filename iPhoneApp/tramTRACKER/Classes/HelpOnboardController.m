//
//  HelpOnboardController.m
//  tramTRACKER
//
//  Created by Robert Amos on 5/05/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "HelpOnboardController.h"

const NSString *HelpURLSubdirOnboard = @"onboard";

#define HELP_ONBOARD_FIND 0
#define HELP_ONBOARD_UNDERSTANDING 1
#define HELP_ONBOARD_SITUATIONS 2
#define HELP_ONBOARD_LOWACCURACY 3
#define HELP_ONBOARD_HOWITWORKS 4
#define HELP_ONBOARD_MESSAGES 5
#define HELP_ONBOARD_CONNECTIONS 6

@implementation HelpOnboardController

- (id)initHelpOnboardController
{
	if (self = [super initWithStyle:UITableViewStylePlain])
	{
		[self setTitle:NSLocalizedString(@"help-onboard", @"Help")];
		[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
	}
	return self;
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 7;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    SimpleCell *cell = (SimpleCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[SimpleCell alloc] initWithCellStyle:TTSimpleCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
		
		GradientBackgroundCell *background = [[GradientBackgroundCell alloc] initWithFrame:CGRectMake(0, 0, 320, 44) cellStyle:OnboardCellStyleNone];
		[cell setBackgroundView:background];
		[background release];
	}
    
	switch (indexPath.row)
	{
		case HELP_ONBOARD_FIND:
			[cell setTextLabelText:NSLocalizedString(@"help-onboard-find", @"Finding Your Tram Number")];
			break;
		case HELP_ONBOARD_UNDERSTANDING:
			[cell setTextLabelText:NSLocalizedString(@"help-onboard-understanding", @"Understanding the Onboard Display")];
			break;
		case HELP_ONBOARD_SITUATIONS:
			[cell setTextLabelText:NSLocalizedString(@"help-onboard-situations", @"Special Situations")];
			break;
		case HELP_ONBOARD_LOWACCURACY:
			[cell setTextLabelText:NSLocalizedString(@"help-onboard-lowaccuracy", @"Low Accuracy Conditions")];
			break;
		case HELP_ONBOARD_HOWITWORKS:
			[cell setTextLabelText:NSLocalizedString(@"help-onboard-howitworks", @"How it Works")];
			break;
		case HELP_ONBOARD_MESSAGES:
			[cell setTextLabelText:NSLocalizedString(@"help-onboard-messages", @"Disruptions and Special Event Messages")];
			break;
		case HELP_ONBOARD_CONNECTIONS:
			[cell setTextLabelText:NSLocalizedString(@"help-onboard-connections", @"Finding Connections")];
			break;
	}
	
    return cell;	
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

	// we create a UIWebView that we push onto the stack, start by working out our URL
	NSString *fileName = nil;
	
	switch (indexPath.row)
	{
		case HELP_ONBOARD_FIND:
			fileName = @"finding.html";
			break;
			
		case HELP_ONBOARD_UNDERSTANDING:
			fileName = @"understanding15.html";
			break;
			
		case HELP_ONBOARD_SITUATIONS:
			fileName = @"special_situations.html";
			break;
			
		case HELP_ONBOARD_LOWACCURACY:
			fileName = @"low_accuracy.html";
			break;
			
		case HELP_ONBOARD_HOWITWORKS:
			fileName = @"works.html";
			break;
			
		case HELP_ONBOARD_MESSAGES:
			fileName = @"messages.html";
			break;
			
		case HELP_ONBOARD_CONNECTIONS:
			fileName = @"connections15.html";
	}
	
	// nothing found?
	if (fileName == nil)
		return;
	
	// create the web view
	HelpWebController *helpWeb = [[HelpWebController alloc] initWithHelpFile:[NSString stringWithFormat:@"%@/%@", HelpURLSubdirOnboard, fileName]];
	SimpleCell *cell = (SimpleCell *)[self.tableView cellForRowAtIndexPath:indexPath];
	[helpWeb setTitle:cell.textLabel.text];
	[self.navigationController pushViewController:helpWeb animated:YES];
	[helpWeb release];	
}


- (void)dealloc {
    [super dealloc];
}


@end

