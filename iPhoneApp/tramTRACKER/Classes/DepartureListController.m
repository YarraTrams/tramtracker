//
//  DepartureListController.m
//  tramTRACKER
//
//  Created by Robert Amos on 6/04/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "DepartureListController.h"
#import "Journey.h"
#import "ScheduledTripController.h"


@implementation DepartureListController

@synthesize realtime, currentStop, currentPredictions, timerRefresh, service, relativeTime, scheduledDepartureTime, tableHeader, selectedRoute, currentError, tripRoute;

// Initialiser
- (id)initWithListForStop:(Stop *)stop
{
	if (self = [super initWithStyle:UITableViewStylePlain])
	{
		// Set the stop
		[self setCurrentStop:stop];
		[self setSelectedRoute:[stop.routesThroughStop objectAtIndex:0]];

		// title
		[self setTitle:[stop shortName]];
		
		// Reset our background colour
		[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
		[self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
		
		// setup the Pids Service
		PidsService *s = [[PidsService alloc] init];
		[s setDelegate:self];
		[self setService:s];
		[s release];
	}
	return self;
}

// Initialiser with realtime list
- (id)initWithRealtimeListForStop:(Stop *)stop
{
	if (self = [self initWithListForStop:stop])
	{
		[self setRealtime:YES];
		[self setRelativeTime:YES];
		
		// create the realtime/absolute time toggle
		[self.navigationItem setRightBarButtonItem:[self relativeTimeButton] animated:NO];

		// set the table footer
		[self.tableView setTableFooterView:[self tableFooterView]];

		// set the table header
		[self.tableView setTableHeaderView:[self tableHeaderView]];
	}
	return self;
}

// Initialiser with scheduled list
- (id)initWithScheduledListForStop:(Stop *)stop
{
	if (self = [self initWithListForStop:stop])
	{
		[self setRealtime:NO];
		[self setRelativeTime:NO];
		[self setScheduledDepartureTime:[NSDate date]];

		// set the table footer
		[self.tableView setTableFooterView:[self tableFooterView]];

		// set the table header
		[self.tableView setTableHeaderView:[self tableHeaderView]];
	}
	return self;
}


// start loading results if necessary
- (void)viewDidAppear:(BOOL)animated
{
	// Sometimes on 2.2.1 devices we end up off the top somewhere, correct that if it happens.
	if (self.view.frame.origin.y < 0)
	{
		CGRect frame = self.view.frame;
		frame.origin.y = 0;
		[self.view setFrame:frame];
	}

	if (self.realtime)
	{
		// start refreshing
		[self setTimerRefresh:[NSTimer scheduledTimerWithTimeInterval:15
															   target:self
															 selector:@selector(refreshPredictions:)
															 userInfo:nil
															  repeats:YES]];
		[self.timerRefresh fire];
	} else
	{
		[self refreshScheduledDepartures];
	}
}


// View going away? stop refreshing
- (void)viewDidDisappear:(BOOL)animated
{
	if (self.timerRefresh && [self.timerRefresh isValid])
	{
		[self.timerRefresh invalidate];
		[self setTimerRefresh:nil];
	}
}

// Toggle relative/abosolute departure times
- (void)toggleRelativeTime
{
	[self.navigationItem setRightBarButtonItem:(self.relativeTime ? [self absoluteTimeButton] : [self relativeTimeButton]) animated:YES];
	[self setRelativeTime:!self.relativeTime];
	[self.tableView reloadData];
}

- (UIBarButtonItem *)absoluteTimeButton
{
	UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ButtonAbsoluteTime.png"]
															   style:UIBarButtonItemStylePlain
															  target:self
															  action:@selector(toggleRelativeTime)];
	return [button autorelease];
}

- (UIBarButtonItem *)relativeTimeButton
{
	UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ButtonRelativeTime.png"]
															   style:UIBarButtonItemStylePlain
															  target:self
															  action:@selector(toggleRelativeTime)];
	return [button autorelease];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

//
// Create the table footer
//
- (UIView *)tableFooterView
{
	// add a message
	if (self.currentError != nil || self.currentPredictions == nil || [self.currentPredictions count] == 0 || ![[self.currentPredictions objectAtIndex:0] tramTrackerAvailable])
		return nil;

	CGRect frame = CGRectMake(0, 0, self.tableView.frame.size.width, 33);
	UnderBackgroundView *ub = [[UnderBackgroundView alloc] initWithFrame:frame];

	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 3, 300, 30)];
	[label setText:NSLocalizedString(@"scheduled-footer-message", nil)];
	[label setTextColor:[UIColor darkGrayColor]];
	[label setFont:[UIFont systemFontOfSize:14]];
	[label setTextAlignment:UITextAlignmentCenter];
	[label setNumberOfLines:1];
	[label setOpaque:NO];
	[label setBackgroundColor:[UIColor clearColor]];
	[ub addSubview:label];
	[label release];
	
	return [ub autorelease];
}

/**
 * The table header view
**/
- (UIView *)tableHeaderView
{
	DepartureListHeaderController *header = [[DepartureListHeaderController alloc] initWithNibName:@"ScheduledDepartureHeader" bundle:[NSBundle mainBundle]];
	[self setTableHeader:header];
	[header release];
	CGRect frame = self.tableHeader.view.frame;
	frame.size.height = 180;

	// set the titles
	[self.tableHeader.departureTimeButton setTitle:[self formattedScheduledDepartureTime] forState:UIControlStateNormal];
	[self setRouteFilterButtonTitle];
	
	// Attach the action to the button
	[self.tableHeader.departureTimeButton addTarget:self action:@selector(pushTimePicker) forControlEvents:UIControlEventTouchUpInside];
	[self.tableHeader.routeFilterButton addTarget:self action:@selector(pushRoutePicker) forControlEvents:UIControlEventTouchUpInside];
	
	return self.tableHeader.view;
}

- (void)setRouteFilterButtonTitle
{
	if ([self.selectedRoute isEqualToString:@"-1"])
		[self.tableHeader.routeFilterButton setTitle:NSLocalizedString(@"filter-route-allroutes", @"All Routes") forState:UIControlStateNormal];
	else
		[self.tableHeader.routeFilterButton setTitle:[NSString stringWithFormat:NSLocalizedString(@"filter-route-template", @"Route Selection Title"), self.selectedRoute, [[RouteList sharedManager] destinationForRouteNumber:self.selectedRoute trackerID:self.currentStop.trackerID]]
											forState:UIControlStateNormal];
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

	// errors
	if (self.currentError != nil)
		return 1;
	
	// the arrivals
	if (self.currentPredictions != nil && [self.currentPredictions count] > 0)
	{
		// if we have a special event and results we'll have more than one row
		if ([self.currentPredictions count] > 1)
		{
			Prediction *current = [self.currentPredictions objectAtIndex:0];
			if ([current.specialEventMessage length] > 0)

				// tack an extra row on the bottom for the special event message.
				return [self.currentPredictions count] + 1;
		}
		
		// otherwise we have results only, just return it
		return [self.currentPredictions count];
	}

	return 0;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
	static NSString *DepartureCellIdenfitier = @"DepartureCell";
	static NSString *ErrorCellIdentifier = @"ErrorCell";
	
	if ((indexPath.row == 0 && self.currentPredictions && [self.currentPredictions count] == 1 && ![[self.currentPredictions objectAtIndex:0] tramTrackerAvailable]) ||
		(self.currentPredictions && indexPath.row == [self.currentPredictions count] && [[[self.currentPredictions objectAtIndex:0] specialEventMessage] length] > 0))
	{
		SimpleCell *cell = (SimpleCell *)[tableView dequeueReusableCellWithIdentifier:ErrorCellIdentifier];
		if (cell == nil)
		{
			cell = [[[SimpleCell alloc] initWithCellStyle:TTSimpleCellStyleMultiLine reuseIdentifier:ErrorCellIdentifier] autorelease];
			GradientBackgroundCell *background = [[GradientBackgroundCell alloc] initWithFrame:cell.frame cellStyle:OnboardCellStyleNone];
			[cell setMultiLine:YES];
			[cell setBackgroundView:background];
			[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
			[background release];
		}
		Prediction *p = [self.currentPredictions objectAtIndex:0];
		[cell setTextLabelText:[self specialEventMessageForPrediction:p]];
		return cell;
	}

	if (indexPath.row == 0 && self.currentError != nil)
	{
		SimpleCell *cell = (SimpleCell *)[tableView dequeueReusableCellWithIdentifier:ErrorCellIdentifier];
		if (cell == nil)
		{
			cell = [[[SimpleCell alloc] initWithCellStyle:TTSimpleCellStyleMultiLine reuseIdentifier:ErrorCellIdentifier] autorelease];
			[cell setMultiLine:YES];
			GradientBackgroundCell *background = [[GradientBackgroundCell alloc] initWithFrame:cell.frame cellStyle:OnboardCellStyleNone];
			[cell setBackgroundView:background];
			[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
			[background release];
		}
		[cell setTextLabelText:[self.currentError localizedDescription]];
		return cell;
	}
	
	// its a departure
	DepartureCell *cell = (DepartureCell *)[tableView dequeueReusableCellWithIdentifier:DepartureCellIdenfitier];
	if (cell == nil)
	{
		cell = [[[DepartureCell alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 44) reuseIdentifier:DepartureCellIdenfitier] autorelease];
	}

	// set the departures
	Prediction *p = [self.currentPredictions objectAtIndex:indexPath.row];
	[cell.name setText:[p destination]];
	[cell.arrival setText:(self.relativeTime ? [p minutesUntilArrivalTime] : [self absoluteArrivalTime:p.predictedArrivalDateTime])];
	[cell.routeDescription setText:[p formattedRouteDescriptionFromStop:self.currentStop]];
	return cell;
}

- (NSString *)specialEventMessageForPrediction:(Prediction *)prediction
{
	return [prediction.specialEventMessage length] > 0 ? prediction.specialEventMessage : NSLocalizedString(@"scheduled-tramtracker-unavailable", @"Default special event message");
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if ((indexPath.row == 0 && self.currentPredictions && [self.currentPredictions count] == 1 && ![[self.currentPredictions objectAtIndex:0] tramTrackerAvailable]) ||
		(self.currentPredictions && indexPath.row == [self.currentPredictions count] && [[[self.currentPredictions objectAtIndex:0] specialEventMessage] length] > 0))
	{
		Prediction *p = [self.currentPredictions objectAtIndex:0];
		NSString *errorMessage = [self specialEventMessageForPrediction:p];
		CGSize size = [errorMessage sizeWithFont:[UIFont boldSystemFontOfSize:16] constrainedToSize:CGSizeMake(tableView.frame.size.width-20, 2009) lineBreakMode:UILineBreakModeWordWrap];
		size.height += 10;
		return size.height > 2009 ? 2009 : size.height;
	}
	
	// error messages
	if (self.currentError != nil)
	{
		NSString *errorMessage = [self.currentError localizedDescription];
		CGSize size = [errorMessage sizeWithFont:[UIFont boldSystemFontOfSize:16] constrainedToSize:CGSizeMake(tableView.frame.size.width-20, 2009) lineBreakMode:UILineBreakModeWordWrap];
		size.height += 10;
		return size.height > 2009 ? 2009 : size.height;
	}
	return 44;
}

//
// Custom Headers
//
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	// grab a OnboardListSection thingy
	OnboardListSectionHeaderView *view = [[OnboardListSectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 22)];
	
	// set the title
	[view.title setText:[self tableView:tableView titleForHeaderInSection:section]];
	[view.title setLineBreakMode:UILineBreakModeMiddleTruncation];
	
	return [view autorelease];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	if (self.currentError != nil)
		return NSLocalizedString(@"scheduled-sectiontitle-error", @"tramTRACKER Unavailable");

	if (self.currentPredictions && [self.currentPredictions count] == 1 && ![[self.currentPredictions objectAtIndex:0] tramTrackerAvailable])
		return NSLocalizedString(@"scheduled-sectiontitle-error", @"tramTRACKER Unavailable");
	
	if (self.realtime)
		return NSLocalizedString(@"scheduled-sectiontitle-realtime", @"Realtime Arrivals");
	
	if (self.currentPredictions == nil)
		return NSLocalizedString(@"scheduled-sectiontitle-loading", @"Loading");
	
	NSInteger count = [self.currentPredictions count];
	if (count == 0)
		return NSLocalizedString(@"scheduled-sectiontitle-noresults", @"No results");

	if (count == 1)
		return NSLocalizedString(@"scheduled-sectiontitle-single", @"One tram departing");
	
	return [NSString stringWithFormat:NSLocalizedString(@"scheduled-sectiontitle-multiple", @"Multiple trams departing"), count];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	// Special event message? Do nothing
	if ((indexPath.row == 0 && self.currentPredictions && [self.currentPredictions count] == 1 && ![[self.currentPredictions objectAtIndex:0] tramTrackerAvailable]) ||
		(self.currentPredictions && indexPath.row == [self.currentPredictions count] && [[[self.currentPredictions objectAtIndex:0] specialEventMessage] length] > 0))
		return;

	// error message?
	if (indexPath.row == 0 && self.currentError != nil)
		return;

	// find the prediction
	Prediction *p = [self.currentPredictions objectAtIndex:indexPath.row];
	if (p != nil)
	{
		UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
		UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithCustomView:indicator];
		[indicator startAnimating];
		[self.navigationItem setRightBarButtonItem:button animated:YES];
		[indicator release];
		[button release];
		[service schedulesForTrip:p.tripID atDateTime:p.predictedArrivalDateTime];
		
		[self setTripRoute:[[RouteList sharedManager] routeForRouteNumber:p.routeNo headboardRouteNumber:p.headboardRouteNumber]];
	}
}

- (void)refreshScheduledDepartures
{
	[self setCurrentPredictions:nil];
	[self setCurrentError:nil];

	UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
	UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithCustomView:indicator];
	[indicator startAnimating];
	[self.navigationItem setRightBarButtonItem:button animated:YES];
	[indicator release];
	[button release];

	// take our filter options and refresh the predictions
	[self.tableView reloadData];
	[self.tableView setTableFooterView:[self tableFooterView]];
	[self.service getScheduledDeparturesForStop:[self.currentStop.trackerID intValue] routeNumber:self.selectedRoute atDateTime:self.scheduledDepartureTime withLowFloorOnly:NO];
}

- (void)pushTimePicker
{
	// push the time picker
	DepartureTimePicker *picker = [[DepartureTimePicker alloc] initWithNibName:nil bundle:nil scheduledDepartureTime:self.scheduledDepartureTime target:self action:@selector(setScheduledDepartureTimeFromPicker:)];
	[self.navigationController pushViewController:picker animated:YES];
	[picker release];
}

- (void)pushRoutePicker
{
	// push the route picker
	RouteSelectionController *picker = [[RouteSelectionController alloc] initWithRoutes:self.currentStop.routesThroughStop
																			  trackerID:self.currentStop.trackerID
																				 target:self
																				 action:@selector(setSelectedRouteFromPicker:)];
	if ([self.selectedRoute isEqualToString:@"-1"])
		[picker setSelectedRouteIndex:-1];
	else
		[picker setSelectedRouteIndex:[self.currentStop.routesThroughStop indexOfObject:self.selectedRoute]];

	[self.navigationController pushViewController:picker animated:YES];
	[picker release];
}

- (NSString *)absoluteArrivalTime:(NSDate *)arrivalTime
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	
	// use the current calendar to match days
	NSCalendar *cal = [NSCalendar currentCalendar];
	
	NSDate *today = [NSDate date];
	NSDateComponents *todayComponents = [cal components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:today];
	NSDateComponents *arrivalComponents = [cal components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:arrivalTime];
	
	// if its in the year 9999 then it means there is an error, display dashes
	if ([arrivalComponents year] >= 9999)
	{
		[formatter release];
		return @"--";
	}
	
	// check the weekday, if its arriving on a different day then we add the day name.
	if ([todayComponents day] == [arrivalComponents day] && [todayComponents month] == [arrivalComponents month] && [todayComponents year] == [arrivalComponents year])
	{
		//[formatter setDateFormat:@"h:mm a"]; -- use built in style to allow for use of 24 hour time setting
		[formatter setTimeStyle:NSDateFormatterShortStyle];
		[formatter setDateStyle:NSDateFormatterNoStyle];
	}
	else
		[formatter setDateFormat:@"dd/MM/yy h:mm a"];
	
	NSString *formattedArrivalTime = [formatter stringFromDate:arrivalTime];
	[formatter release];
	return formattedArrivalTime;
}

#pragma mark -
#pragma mark Set Options From Picker
- (void)setScheduledDepartureTimeFromPicker:(NSDate *)date
{
	[self setScheduledDepartureTime:date];
}

- (void)setScheduledDepartureTime:(NSDate *)date
{
	[date retain];
	[scheduledDepartureTime release];
	scheduledDepartureTime = date;

	if (self.tableHeader != nil)
	{
		[self.tableHeader.departureTimeButton setTitle:[self formattedScheduledDepartureTime] forState:UIControlStateNormal];
		[self refreshScheduledDepartures];
	}
}

- (void)setSelectedRouteFromPicker:(NSString *)routeNumber
{
	[self setSelectedRoute:routeNumber];
}

- (void)setSelectedRoute:(NSString *)routeNumber
{
	[routeNumber retain];
	[selectedRoute release];
	selectedRoute = routeNumber;
	
	if (self.tableHeader != nil)
	{
		[self setRouteFilterButtonTitle];
		[self refreshScheduledDepartures];
	}
}

#pragma mark -
#pragma mark Formatted Departure Time
- (NSString *)formattedScheduledDepartureTime
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	
	// Use the current calendar to do matching
	NSCalendar *cal = [NSCalendar currentCalendar];
	
	NSDate *today = [NSDate date];
	
	// build the components list
	NSDateComponents *todayComponents = [cal components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:today];
	NSDateComponents *departureComponents = [cal components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:self.scheduledDepartureTime];
	
	// same day?
	if ([todayComponents year] == [departureComponents year] && [todayComponents month] == [departureComponents month] && [todayComponents day] == [departureComponents day])
	{
		// setup the formatter
		[formatter setDateFormat:@"'Today at' hh:mm a"];
		NSString *formattedDate = [formatter stringFromDate:self.scheduledDepartureTime];
		[formatter release];
		return formattedDate;
	}
	
	// otherwise just the date/time
	[formatter setDateFormat:@"dd/MM/yyyy 'at' hh:mm a"];
	NSString *formattedDate = [formatter stringFromDate:self.scheduledDepartureTime];
	[formatter release];
	return formattedDate;
}


#pragma mark -
#pragma mark PidsServiceDelegate methods

- (void)refreshPredictions:(NSTimer *)aTimer
{
	[self.service getPredictionsForStop:[self.currentStop.trackerID integerValue]];
}


//
// We've received a scheduled journey from the pids service
//
- (void)setScheduledJourney:(JourneyStub *)stub
{
    Journey *journey = [Journey journeyForStub:stub];

	[self.navigationItem setRightBarButtonItem:nil animated:YES];
	
	ScheduledTripController *tripController = [[ScheduledTripController alloc] initWithJourney:journey onRoute:self.tripRoute fromStop:self.currentStop];
	[self.navigationController pushViewController:tripController animated:YES];
	[tripController release];
}

//
// We've received new predictions from the service, update things
//
- (void)setPredictions:(NSArray *)somePredictions
{
	//NSLog(@"Received new predictions, updating store");

    // empty predictions?
    if ([somePredictions count] > 0)
        return;
    
    // Convert the prediction stubs to full predictions
    NSMutableArray *predictions = [[NSMutableArray alloc] initWithCapacity:0];
    for (PredictionStub *stub in predictions)
        [predictions addObject:[Prediction predictionForStub:stub]];
    
	// set the current predictions
	[self setCurrentError:nil];
	[self setCurrentPredictions:predictions];
	[self.tableView reloadData];
	[self.navigationItem setRightBarButtonItem:nil animated:YES];
    [predictions release]; predictions = nil;
}

- (void)setDepartures:(NSArray *)departures
{
	//NSLog(@"Received new departures, updating store");
	
	// set the current predictions
	[self setCurrentError:nil];
	[self setCurrentPredictions:departures];
	[self.tableView reloadData];
	[self.tableView setTableFooterView:[self tableFooterView]];
	[self.navigationItem setRightBarButtonItem:nil animated:YES];
}

- (void)pidsServiceDidFailWithError:(NSError *)error
{
	[self setCurrentPredictions:nil];
	[self setCurrentError:error];
	[self.tableView setTableFooterView:[self tableFooterView]];
	[self.tableView reloadData];
	[self.navigationItem setRightBarButtonItem:nil animated:YES];
}


- (void)dealloc {
	
	[currentStop release];
	[currentPredictions release];
	[timerRefresh release];
	[service release];
	[tripRoute release];

    [super dealloc];
}


@end

@implementation DepartureListHeaderController

@synthesize routeFilterButton, departureTimeButton;

@end


