//
//  NearbyViewController.h
//  tramTRACKER
//
//  Created by Robert Amos on 25/07/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Stop.h"
#import "Filter.h"
//#import "NearbyListController.h"
#import "MapViewController.h"

@interface NearbyViewController : UIViewController {
	
	UISegmentedControl *segmentedTitleControl;
	NearbyListController *list;
	MapViewController *map;
	BOOL listViewRefreshing;
	UIBarButtonItem *refreshButton;
	UIBarButtonItem *ticketsButton;
}

@property (readonly, nonatomic, retain) UISegmentedControl *segmentedTitleControl;
@property (readonly, nonatomic, retain) NearbyListController *list;
@property (readonly, nonatomic, retain) MapViewController *map;
@property (nonatomic, getter=isListViewRefreshing) BOOL listViewRefreshing;
@property (nonatomic, retain) UIBarButtonItem *refreshButton;
@property (nonatomic, retain) UIBarButtonItem *ticketsButton;

- (id)initNearbyViewController;

- (void)segmentedTitleControlDidChangeValue;

- (void)showListView;
- (void)hideListView;

- (void)showMapView;
- (void)hideMapView;
- (void)toggleTicketRetailers;

- (void)pushPIDForStop:(Stop *)stop withFilter:(Filter *)f animated:(BOOL)animated;
- (void)refreshLocation;

- (void)applicationDidBecomeActive;

@end
