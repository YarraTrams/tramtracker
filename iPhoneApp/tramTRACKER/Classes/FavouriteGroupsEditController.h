//
//  FavouriteGroupsEditController.h
//  tramTRACKER
//
//  Created by Robert Amos on 21/07/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FavouriteGroupsEditController : UITableViewController <UITextFieldDelegate> {
	NSString *groupName;
	id target;
	SEL action;
}

- (id)initWithTarget:(id)aTarget action:(SEL)anAction;
- (id)initWithGroupName:(NSString *)existingGroupName target:(id)aTarget action:(SEL)anAction;

@end
