//
//  StopList.m
//  tramTRACKER
//
//  Created by Robert Amos on 23/02/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "StopList.h"
#import "FavouriteStop.h"

int const TTNearbyStopCount = 10;
NSString * const TTFavouritesUpdated = @"TTFavouritesUpdated";

@implementation StopList

//
// Singleton instance
//
static StopList *sharedStopList = nil;

@synthesize suburbList;

//
// Shared manager, call this to get a copy of the stop list
//
+ (StopList *)sharedManager
{
    if (sharedStopList == nil)
        sharedStopList = [[super allocWithZone:NULL] init];
	return sharedStopList;
}

//
// Just in case someone tries to init us directly, wrap the call around to the shared instance
//
+ (id)allocWithZone:(NSZone *)zone
{
    return [[self sharedManager] retain];
}

- (id)copyWithZone:(NSZone *)zone
{
	return self;
}

- (id)retain
{
	return self;
}

- (unsigned)retainCount
{
	return NSUIntegerMax; // cannot be released
}

- (oneway void)release
{
	// nothing to do, we can't release
}
- (id)autorelease
{
	return self;
}

//
// Search for a stop by name or suburb
//
- (NSArray *)stopsBySearchingNameOrSuburb:(NSString *)searchString
{
	return [Stop stopsBySearchingNameOrSuburb:searchString];
}

- (NSArray *)getStopsInRegion:(MKCoordinateRegion)region
{
	return [Stop stopsInRegion:region];
}

//
// Return a list of the stops nearest to a particular location
//
- (NSArray *)getNearestStopsToLocation:(CLLocation *)location count:(NSUInteger)numberOfStops
{
	// make sure that we have a stop list
	NSArray *listOfStops = nil;
	
	// start with a decent sized region and go from there
	MKCoordinateRegion region;
	region.center.latitude = location.coordinate.latitude;
	region.center.longitude = location.coordinate.longitude;
	region.span.latitudeDelta = 0.01796550;
	region.span.longitudeDelta = 0.01982689;
	
	// loop through until we have enough stops, cap at 1000
	if (numberOfStops > 1000) numberOfStops = 1000;
	listOfStops = [Stop stopsInRegion:region];
	if ([listOfStops count] < numberOfStops)
	{
		while ([listOfStops count] < numberOfStops)
		{
			region.span.latitudeDelta = region.span.latitudeDelta * 2;
			region.span.longitudeDelta = region.span.longitudeDelta * 2;
			listOfStops = [Stop stopsInRegion:region];
		}
	}
	
	// create an array of stop distances
	NSMutableArray *stopDistances = [[NSMutableArray alloc] initWithCapacity:0];
	
	// loop over the current stops and work out the distance to the location
	for (Stop *stop in listOfStops)
	{
		// if this stop has a zero tracker ID then it can't appear in lists
		if ([stop.trackerID integerValue] >= 8000)
			continue;

		// create a new distance object
		StopDistance *distance = [[StopDistance alloc] init];

		// set the reference stop
		[distance setStop:stop];
		
		// set the distance
		CLLocation *stopLocation = [stop location];
		CLLocationDistance stopDistance = [LocationManager distanceFromLocation:location toLocation:stopLocation];
		[distance setDistance:stopDistance];
		
		// add the stop to the list
		[stopDistances addObject:distance];
		
		// we're finished with the distance object - the array will take it from here
		[distance release];
		distance = nil;
	}
	
	// sort the distance object
	[stopDistances sortUsingSelector:@selector(compareDistance:)];
	
	// grab the nearest and release the whole list
	NSArray *nearestStopDistances;
	if ([stopDistances count] < numberOfStops)
		nearestStopDistances = stopDistances;
	else
		nearestStopDistances = [stopDistances subarrayWithRange:NSMakeRange(0, numberOfStops)];

	[stopDistances release];
	stopDistances = nil;
	
	// return the list
	return nearestStopDistances;
}

- (NSArray *)getNearestStopsWithoutDistancesToLocation:(CLLocation *)location count:(NSUInteger)numberOfStops
{
	NSArray *stops = [self getNearestStopsToLocation:location count:numberOfStops];
	NSMutableArray *newStopsArray = [[NSMutableArray alloc] initWithCapacity:0];
	for (StopDistance *s in stops)
		[newStopsArray addObject:s.stop];
	return [newStopsArray autorelease];
}

//
// Return the nearest favourite stop to a location
//
- (FavouriteStop *)getNearestFavouriteToLocation:(CLLocation *)location
{
	// make a list we can play with
	NSMutableArray *nearestFavourites = [[NSMutableArray alloc] initWithCapacity:0];
	for (FavouriteStop *favourite in [self getFavouriteStopList])
	{
		// create a new distance object
		StopDistance *distance = [[StopDistance alloc] init];
		
		// set the reference stop
		[distance setStop:favourite.stop];
		[distance setFavourite:favourite];
		
		// set the distance
		[distance setDistance:[LocationManager distanceFromLocation:location toLocation:distance.stop.location]];
		
		// and add it to the list
		[nearestFavourites addObject:distance];
		
		// and release
		[distance release];
		distance = nil;
	}
	
	// sort the distance object
	[nearestFavourites sortUsingSelector:@selector(compareDistance:)];
	
	// grab the nearest and get rid of the list
	StopDistance *nearest = [[nearestFavourites objectAtIndex:0] retain];
	[nearestFavourites release];
	nearestFavourites = nil;
	
	[nearest autorelease];
	return [nearest favourite];
}

//
// Get the nearest stop to a location for a particular list of stops
//
- (StopDistance *)getNearestStopToLocation:(CLLocation *)location withStopList:(NSArray *)stopList
{
	// find out how far we are from each stop
	NSMutableArray *nearestStops = [[NSMutableArray alloc] initWithCapacity:0];
	for (Stop *stop in stopList)
	{
		// create the distance object
		StopDistance *distance = [[StopDistance alloc] init];
		
		// set the reference stop
		[distance setStop:stop];
		
		// and find the distance
		[distance setDistance:[LocationManager distanceFromLocation:location toLocation:stop.location]];
		
		// and add it to the list
		[nearestStops addObject:distance];
		
		// and let go
		[distance release];
	}
	
	// so we have a list of distances now, sort it
	[nearestStops sortUsingSelector:@selector(compareDistance:)];
	
	// grab the nearest
	StopDistance *nearest = [[nearestStops objectAtIndex:0] retain];

	// release and return
	[nearestStops release];
	return [nearest autorelease];
}

//
// Return the nearest stop to a location
//
- (StopDistance *)getNearestStopToLocation:(CLLocation *)location
{
	NSArray *nearbyStops = [self getNearestStopsToLocation:location count:1];
	
	// return the first
	return [nearbyStops objectAtIndex:0];
}

//
// Get a filter
//
- (Filter *)filterForFavouriteAtIndexPath:(NSIndexPath *)indexPath
{
	// Get our filters
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSArray *favourites = [defaults arrayForKey:@"favouriteStopsList"];
	
	// find this favourite
	for (NSDictionary *favourite in favourites)
	{
		// does this one match?
		if ([[favourite objectForKey:@"section"] integerValue] == indexPath.section && [[favourite objectForKey:@"row"] integerValue] == indexPath.row)
		{
			// build the filter
			Filter *filter = [[Filter alloc] init];
			[filter setLowFloor:[[favourite objectForKey:@"lowfloor"] isEqualToString:@"YES"]];
			
			NSString *routeNumber = [favourite objectForKey:@"route"];
			if (routeNumber != nil && ![routeNumber isEqualToString:@"0"])
				[filter setRoute:[[RouteList sharedManager] routeForRouteNumber:routeNumber]];
			
			return [filter autorelease];
		}
	}
	
	return nil;
}

// Set a filter
- (void)setFilter:(Filter *)filter forFavouriteAtIndexPath:(NSIndexPath *)indexPath
{
	// Get the filters dictionary
	NSUserDefaults *defaults  = [NSUserDefaults standardUserDefaults];
	NSArray *existingFavourites = [defaults arrayForKey:@"favouriteStopsList"];
	NSMutableArray *favourites = [[NSMutableArray alloc] initWithCapacity:0];
	
	if (existingFavourites != nil)
		[favourites addObjectsFromArray:existingFavourites];

	// get the favourite in question
	NSMutableDictionary *favourite = [[NSMutableDictionary alloc] initWithCapacity:0];
	NSDictionary *existingFavourite = nil;
	NSInteger index = NSNotFound;
	
	// find the existing favourite
	for (NSDictionary *d in existingFavourites)
	{
		if ([[d objectForKey:@"section"] integerValue] == indexPath.section && [[d objectForKey:@"row"] integerValue] == indexPath.row)
		{
			existingFavourite = d;
			index = [existingFavourites indexOfObject:d];
			break;
		}
	}
	
	// not found?
	if (existingFavourite == nil)
	{
		[favourites release];
		favourites = nil;
		[favourite release];
		favourite = nil;
		return;
	}
	
	// copy to the mutable dictionary
	[favourite addEntriesFromDictionary:existingFavourite];

	// if the filter is null attempt to clear it from the dictionary
	if (filter == nil)
	{
		[favourite setObject:@"0" forKey:@"route"];
		[favourite setObject:@"NO" forKey:@"lowfloor"];
	} else
	{
		[favourite setObject:(filter.lowFloor ? @"YES" : @"NO") forKey:@"lowfloor"];
		
		if (filter.route != nil)
			[favourite setObject:filter.route.number forKey:@"route"];
		else
			[favourite setObject:@"0" forKey:@"route"];
	}

	// replace our favourite back in the list
	[favourites replaceObjectAtIndex:index withObject:favourite];
	
	// and save it again
	[defaults setObject:favourites forKey:@"favouriteStopsList"];
	[favourites release];
	[favourite release];
	[defaults synchronize];

	[self populateFavouriteStopList];

	// send out a notification so that anything listening for favourites updates can refresh
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc postNotificationName:TTFavouritesUpdated object:nil];
}


#pragma mark -
#pragma mark Favourites

- (void)populateFavouriteStopList
{
	// open the defaults to see if we have a list
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	// do we have anything?
	NSArray *favouriteStopsFromDefaults = [defaults arrayForKey:@"favouriteStopsList"];

	// if there are no favourites return nil
	if (favouriteStopsFromDefaults == nil)
	{
		[internalFavouriteStopList release];
		internalFavouriteStopList = nil;
		return;
	}
	
	NSMutableArray *favouriteStopList = [[NSMutableArray alloc] initWithCapacity:[favouriteStopsFromDefaults count]];
	for (NSDictionary *favourite in favouriteStopsFromDefaults)
	{
		NSNumber *trackerID = (NSNumber *)[favourite objectForKey:@"trackerid"];
		NSIndexPath *path = [NSIndexPath indexPathForRow:[[favourite objectForKey:@"row"] integerValue]
											   inSection:[[favourite objectForKey:@"section"] integerValue]];
		FavouriteStop *stop = [[FavouriteStop alloc] initWithStop:[self getStopForTrackerID:trackerID]
														   filter:[self filterForFavouriteAtIndexPath:path]
														indexPath:path];
        

        NSManagedObjectContext *context = stop.stop.managedObjectContext;
        
        if([context hasChanges]){
            [context refreshObject:stop.stop mergeChanges:YES];
        }
        
        if(stop.stop != nil){
            [favouriteStopList addObject:stop];
        }
		[stop release];
	}
	
	NSArray *favouriteStops = [favouriteStopList sortedArrayUsingSelector:@selector(compareWithFavouriteStop:)];
	[favouriteStops retain];
	[internalFavouriteStopList release];
	[favouriteStopList release];
	internalFavouriteStopList = favouriteStops;
}

//
// Get the list of favourite stops
//
- (NSArray *)getFavouriteStopList
{
	if (internalFavouriteStopList == nil)
		[self populateFavouriteStopList];
	return internalFavouriteStopList;

}

- (NSArray *)getFavouriteStopListFromCoreData
{
	[self populateFavouriteStopList];
	return internalFavouriteStopList;
}

- (NSArray *)getFavouriteTrackerIDs
{
	// open the defaults to see if we have a list
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	// do we have anything?
	NSArray *favouriteStopsFromDefaults = [defaults arrayForKey:@"favouriteStopsList"];
	
	// if there are no favourites return nil
	if (favouriteStopsFromDefaults == nil)
		return nil;
	
	NSMutableArray *favouriteStopList = [[NSMutableArray alloc] initWithCapacity:[favouriteStopsFromDefaults count]];
	for (NSDictionary *favourite in favouriteStopsFromDefaults)
	{
		NSNumber *trackerID = (NSNumber *)[favourite objectForKey:@"trackerid"];
		[favouriteStopList addObject:trackerID];
	}
	
	return [favouriteStopList autorelease];
}

- (FavouriteStop *)favouriteStopAtIndex:(NSIndexPath *)indexPath
{
	NSArray *favouriteStops = [self getFavouriteStopList];
	
	// loop through until we find a stop that matches our indexPath.
	for (FavouriteStop *stop in favouriteStops)
	{
		if ([stop.indexPath compare:indexPath] == NSOrderedSame)
			return stop;
	}
	return nil;
}

//
// Whether we have favourites
//
- (BOOL)hasFavouriteStopList
{
	// get them from the stop list
	NSArray *list = [self getFavouriteStopList];
	return list != nil && [list count] > 0;
}

//
// Add a New Favourite Stop
//
- (FavouriteStop *)addFavouriteStopID:(NSNumber *)trackerID inSection:(NSInteger)section
{
	// find the right section
	if (section == NSNotFound)
		section = [[self sectionNamesForFavourites] count]-1;
		
	// get the list of stops from the defaults
	// open the defaults to see if we have a list
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	// if there are no favourites return nil
	NSMutableArray *favouriteStops = [[self _sortedMutableFavourites] retain];
	if (favouriteStops == nil)
		favouriteStops = [[NSMutableArray alloc] initWithCapacity:0];
	
	// now that we have the favourites array, add our new stop
	NSMutableDictionary *newFavourite = [[NSMutableDictionary alloc] initWithCapacity:3];
	[newFavourite setObject:trackerID forKey:@"trackerid"];
	[newFavourite setObject:@"0" forKey:@"route"];
	[newFavourite setObject:@"NO" forKey:@"lowfloor"];
	[newFavourite setObject:[NSNumber numberWithInteger:section] forKey:@"section"];
	
	// pop it into our section
	NSMutableArray *sectionToAddTo = [favouriteStops objectAtIndex:section];
	[sectionToAddTo addObject:newFavourite];

	// renumber them all
	NSArray *renumberedFavourites = [[self _flattenedArrayOfRenumberedFavourites:favouriteStops] retain];
	[favouriteStops release];
	
	// all done, save the new favourite stops
	[defaults setObject:renumberedFavourites forKey:@"favouriteStopsList"];
	[defaults synchronize];
	[self populateFavouriteStopList];
	
	// send out a notification so that anything listening for favourites updates can refresh
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc postNotificationName:TTFavouritesUpdated object:nil];
	[renumberedFavourites release];
	
	// build and return the resulting favourite stop
	NSIndexPath *path = [NSIndexPath indexPathForRow:[[newFavourite objectForKey:@"row"] integerValue]
										   inSection:[[newFavourite objectForKey:@"section"] integerValue]];
	FavouriteStop *stop = [[FavouriteStop alloc] initWithStop:[self getStopForTrackerID:trackerID]
													   filter:[self filterForFavouriteAtIndexPath:path]
													indexPath:path];

	[newFavourite release];
	newFavourite = nil;

	return [stop autorelease];
}

//
// Whether a specified tracker ID is a favourite stop
//
- (BOOL)isFavouriteStop:(NSNumber *)trackerID
{
	// get the list of stops from the defaults
	// open the defaults to see if we have a list
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	// do we have anything?
	NSArray *favouriteStopsFromDefaults = [defaults arrayForKey:@"favouriteStopsList"];
	
	// if there are no favourites return NO, obviously
	if (favouriteStopsFromDefaults == nil)
		return NO;
	
	// do we have this stop in the list already?
	for (NSDictionary *existingFavourite in favouriteStopsFromDefaults)
	{
		NSNumber *existingFavouriteTrackerID = (NSNumber *)[existingFavourite objectForKey:@"trackerid"];
		if ([existingFavouriteTrackerID isEqualToNumber:trackerID])
			return YES;
	}
	
	// default to no
	return NO;
}

//
// Remove the favourite at the specified index
//
- (void)removeFavouriteAtIndexPath:(NSIndexPath *)indexPath
{
	// get the list of stops from the defaults
	// open the defaults to see if we have a list
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	NSMutableArray *favouriteStops = [[self _sortedMutableFavourites] retain];
	if (favouriteStops == nil)
		return;
	
	NSMutableArray *section = [favouriteStops objectAtIndex:indexPath.section];
	[section removeObjectAtIndex:indexPath.row];
	
	// if we're back at zero favouritestops then save an empty array instead of sorting
	// renumber it
	NSArray *renumberedFavourites = [[self _flattenedArrayOfRenumberedFavourites:favouriteStops] retain];
	[favouriteStops release];

	// save it back again
	[defaults setObject:renumberedFavourites forKey:@"favouriteStopsList"];
	[renumberedFavourites release];
	[defaults synchronize];
	[self populateFavouriteStopList];
}

- (void)removeFavouriteByStopID:(NSNumber *)trackerID
{
    // get the list of stops from the defaults
	// open the defaults to see if we have a list
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSArray *favouriteStopsFromDefaults = [defaults arrayForKey:@"favouriteStopsList"];
   
    if(favouriteStopsFromDefaults == nil || [favouriteStopsFromDefaults count] == 0)
        return;
 	
    NSUInteger index = [favouriteStopsFromDefaults indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        return [[obj objectForKey:@"trackerid"] intValue] == [trackerID intValue];
    }];
    
    if(index == NSNotFound)
        return;
    
    NSMutableArray *mutableFavorites = [NSMutableArray arrayWithArray:favouriteStopsFromDefaults];
    [mutableFavorites removeObjectAtIndex:index];
    
    NSSortDescriptor *sectionSort = [[NSSortDescriptor alloc] initWithKey:@"section" ascending:YES];
	NSSortDescriptor *rowSort = [[NSSortDescriptor alloc] initWithKey:@"row" ascending:YES];
	NSArray *sortedFavouriteStops = [mutableFavorites sortedArrayUsingDescriptors:[NSArray arrayWithObjects:sectionSort, rowSort, nil]];
	[sectionSort release]; sectionSort = nil;
	[rowSort release]; rowSort = nil;
	
	// create a mutable copy of everything there
	NSArray *sections = [self sectionNamesForFavourites];
	NSMutableArray *favouriteStops = [[NSMutableArray alloc] initWithCapacity:[sections count]];
	for (NSInteger i = 0; i < [sections count]; i++)
		[favouriteStops addObject:[NSMutableArray arrayWithCapacity:0]];
    
	// add all of those to the right one
	for (NSDictionary *dict in sortedFavouriteStops)
	{
		NSMutableArray *section = [favouriteStops objectAtIndex:[[dict objectForKey:@"section"] integerValue]];
		[section addObject:[[dict mutableCopy] autorelease]];
	}
    
    NSArray *renumberedFavourites = [[self _flattenedArrayOfRenumberedFavourites:favouriteStops] retain];
	[favouriteStops release];

	[defaults setObject:renumberedFavourites forKey:@"favouriteStopsList"];
	[renumberedFavourites release];
    
    [defaults synchronize];
    
    [self populateFavouriteStopList];
}


- (NSMutableArray *)_sortedMutableFavourites
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	// get our favourites from the defaults
	NSArray *favouriteStopsFromDefaults = [defaults arrayForKey:@"favouriteStopsList"];
	if (favouriteStopsFromDefaults == nil)
		favouriteStopsFromDefaults = [NSArray array];

		
	// sort it by section and row then throw it into an array
	NSSortDescriptor *sectionSort = [[NSSortDescriptor alloc] initWithKey:@"section" ascending:YES];
	NSSortDescriptor *rowSort = [[NSSortDescriptor alloc] initWithKey:@"row" ascending:YES];
	NSArray *sortedFavouriteStops = [favouriteStopsFromDefaults sortedArrayUsingDescriptors:[NSArray arrayWithObjects:sectionSort, rowSort, nil]];
	[sectionSort release]; sectionSort = nil;
	[rowSort release]; rowSort = nil;
	
	// create a mutable copy of everything there
	NSArray *sections = [self sectionNamesForFavourites];
	NSMutableArray *favouriteStops = [[NSMutableArray alloc] initWithCapacity:[sections count]];
	for (NSInteger i = 0; i < [sections count]; i++)
		[favouriteStops addObject:[NSMutableArray arrayWithCapacity:0]];

	// add all of those to the right one
	for (NSDictionary *dict in sortedFavouriteStops)
	{
		NSMutableArray *section = [favouriteStops objectAtIndex:[[dict objectForKey:@"section"] integerValue]];
		[section addObject:[[dict mutableCopy] autorelease]];
	}
	
	return [favouriteStops autorelease];
}
		
- (NSArray *)_flattenedArrayOfRenumberedFavourites:(NSArray *)favouriteStops
{
	if (favouriteStops == nil)
		return nil;
	
	if ([favouriteStops count] == 0)
		return [[favouriteStops copy] autorelease];
	
	NSMutableArray *flattenedFavourites = [[NSMutableArray alloc] initWithCapacity:0];
	
	
	// go through each section and row and add it to the final array with the new section/row details
	NSInteger sectionIteration = 0;
	NSInteger rowIteration = 0;
	for (NSMutableArray *section in favouriteStops)
	{
		rowIteration = 0;
		for (NSMutableDictionary *mutableFavourite in section)
		{
			[mutableFavourite setObject:[NSNumber numberWithInteger:sectionIteration] forKey:@"section"];
			[mutableFavourite setObject:[NSNumber numberWithInteger:rowIteration] forKey:@"row"];
			[flattenedFavourites addObject:mutableFavourite];
			rowIteration++;
		}
		sectionIteration++;
	}
	
	// send it on back
	return [flattenedFavourites autorelease];
}

	
//
// Move a favourite to a new spot
//
- (void)moveFavouriteAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
	// get the list of stops from the defaults
	// open the defaults to see if we have a list
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	NSMutableArray *favouriteStops = [[self _sortedMutableFavourites] retain];
	if (favouriteStops == nil)
		return;
	
	// find our targets
	NSMutableArray *fromSection = [favouriteStops objectAtIndex:fromIndexPath.section];
	NSMutableArray *toSection = [favouriteStops objectAtIndex:toIndexPath.section];

	// pull it out of the fromSection
	NSMutableDictionary *favourite = [fromSection objectAtIndex:fromIndexPath.row];
	[fromSection removeObjectAtIndex:fromIndexPath.row];
	
	// pop it into the correct position in the new one.
	if (toIndexPath.row >= [toSection count])
		[toSection addObject:favourite];
	else
		[toSection insertObject:favourite atIndex:toIndexPath.row];
	
	// renumber the favourites
	NSArray *renumberedFavourites = [[self _flattenedArrayOfRenumberedFavourites:favouriteStops] retain];
	
	// well that was a highly convulted process, save it all back to the defaults
	[defaults setObject:renumberedFavourites forKey:@"favouriteStopsList"];
	[defaults synchronize];
	[favouriteStops release];
	[renumberedFavourites release];
	[self populateFavouriteStopList];
}

//
// Get the Core Location Accuracy indicator thats most appropriate for the distance between our favourite stops
//
- (CLLocationAccuracy)distanceFilterForFavourites
{
	// if we don't have any favourites just go with the defaults
	if (![self hasFavouriteStopList])
		return kCLLocationAccuracyBest;

	// build a list of all our stops
	NSMutableArray *stopList = [[NSMutableArray alloc] initWithCapacity:0];
	for (FavouriteStop *favourite in [self getFavouriteStopList])
	{
		// add the stop to the array
        [stopList addObject:favourite.stop];
	}

	// so we now have a list of all the stops, lets find the smallest distance between them
	CLLocationDistance shortestDistance = 10000;
	for (Stop *firstStop in stopList)
	{
		// compare it to all the other stops
		for (Stop *secondStop in stopList)
		{
			// but not against itself, obviously
			if (![firstStop isEqual:secondStop])
			{
				CLLocationDistance currentDistance = [LocationManager distanceFromLocation:firstStop.location toLocation:secondStop.location];
				if (currentDistance < shortestDistance)
					shortestDistance = currentDistance;
			}
		}
	}
	
	// we're done with the list
	[stopList release];
	
	// so we now have the shortest distances between the stops, lets associate that back to an accuracy and go
	if (shortestDistance > 3000)
		return kCLLocationAccuracyThreeKilometers;

	if (shortestDistance > 1000)
		return kCLLocationAccuracyKilometer;
	
	if (shortestDistance > 100)
		return kCLLocationAccuracyHundredMeters;
	
	if (shortestDistance > 10)
		return kCLLocationAccuracyNearestTenMeters;
	
	// default to the best we can get
	return kCLLocationAccuracyBest;
}

//
// Get the index for the favourite specified
//
- (NSUInteger)indexForFavourite:(Stop *)favourite
{
	NSUInteger i = 0;
	for (FavouriteStop *f in [self getFavouriteStopList])
	{
		if ([favourite isEqual:f.stop])
			return i;
		i++;
	}
	return NSNotFound;
}

//
// Get a list of section names for favourites
//
- (NSArray *)sectionNamesForFavourites
{
	// look in the user defaults for an array of section names
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSArray *sections = [defaults arrayForKey:@"favouriteSections"];

	// if its empty then return an array with the default section name
	if (sections == nil || [sections count] == 0)
		return [NSArray arrayWithObject:NSLocalizedString(@"favourites-default-section", "Ungrouped")];
	
	return sections;
}

//
// Remove the favourite section at the specified index
//
- (void)removeFavouriteSectionAtIndex:(NSUInteger)index
{
	// get the list of stops from the defaults
	// open the defaults to see if we have a list
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	// do we have anything?
	NSMutableArray *sectionsFromDefaults = [[NSMutableArray alloc] initWithArray:[self sectionNamesForFavourites]];
	NSMutableArray *favouriteStops = [[self _sortedMutableFavourites] retain];
	
	// if we have a list of favourites
	if (sectionsFromDefaults)
	{
		// remove the specified index
		[sectionsFromDefaults removeObjectAtIndex:index];
		
		// save it back again
		[defaults setObject:sectionsFromDefaults forKey:@"favouriteSections"];
		[defaults synchronize];
	}
	
	// go through and remove any stops in this section
	[favouriteStops removeObjectAtIndex:index];
	
	// re-save the favourites
	NSArray *renumberedFavourites = [[self _flattenedArrayOfRenumberedFavourites:favouriteStops] retain];
	[defaults setObject:renumberedFavourites forKey:@"favouriteStopsList"];
	[defaults synchronize];
	[favouriteStops release];
	[renumberedFavourites release];
	[self populateFavouriteStopList];
	
	// send out a notification so that anything listening for favourites updates can refresh
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc postNotificationName:TTFavouritesUpdated object:nil];

	[sectionsFromDefaults release];
}

//
// Move a favourite sections to a new spot
//
- (void)moveFavouriteSectionAtIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex
{
	// get the list of stops from the defaults
	// open the defaults to see if we have a list
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	// do we have anything?
	NSArray *sectionNames = [self sectionNamesForFavourites];
	NSMutableArray *sectionsFromDefaults = [sectionNames mutableCopy];
	
	// if we have a list of favourites
	if (sectionsFromDefaults)
	{
		// get the favourite out
		NSString *section = [[sectionsFromDefaults objectAtIndex:fromIndex] retain];
		
		// and remove it from the current result
		[sectionsFromDefaults removeObjectAtIndex:fromIndex];
		
		// add it again at the new spot
		if (toIndex >= [sectionsFromDefaults count])
			[sectionsFromDefaults addObject:section];
		else
			[sectionsFromDefaults insertObject:section atIndex:toIndex];
		
		// resave it
		[defaults setObject:sectionsFromDefaults forKey:@"favouriteSections"];
		
		// release what we're holding on to
		[section release];
	}
	
	// now we get to go through and similarly move all favourites in that section
	NSMutableArray *favouriteStops = [[self _sortedMutableFavourites] retain];

	// pull out the current section
	NSMutableArray *section = [favouriteStops objectAtIndex:fromIndex];
	[favouriteStops removeObjectAtIndex:fromIndex];
	
	// re-insert it at the new one
	if (toIndex >= [favouriteStops count])
		[favouriteStops addObject:section];
	else
		[favouriteStops insertObject:section atIndex:toIndex];
	
	// then renumber it and save it again
	NSArray *renumberedFavourites = [[self _flattenedArrayOfRenumberedFavourites:favouriteStops] retain];
	[defaults setObject:renumberedFavourites forKey:@"favouriteStopsList"];
	[defaults synchronize];
	[renumberedFavourites release];
	[self populateFavouriteStopList];
	
	// release everything
	[favouriteStops release];
	[sectionsFromDefaults release];
	
	// send out a notification so that anything listening for favourites updates can refresh
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc postNotificationName:TTFavouritesUpdated object:nil];
}


//
// Add a New Favourite Section
//
- (void)addFavouriteSection:(NSString *)sectionName
{
	// get the list of sections from the defaults
	// open the defaults to see if we have a list
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	// do we have anything?
	NSArray *favouriteSectionsFromDefaults = [self sectionNamesForFavourites];

	// all done, save the new favourite sections
	[defaults setObject:[favouriteSectionsFromDefaults arrayByAddingObject:sectionName] forKey:@"favouriteSections"];
	[defaults synchronize];
	
	// send out a notification so that anything listening for favourites updates can refresh
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc postNotificationName:TTFavouritesUpdated object:nil];
}

- (void)changeFavouriteSectionNameAtIndex:(NSUInteger)index toName:(NSString *)newName
{
	// get the list of sections from the defaults
	// open the defaults to see if we have a list
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	// do we have anything?
	NSArray *favouriteSectionsFromDefaults = [self sectionNamesForFavourites];
	//NSLog(@"Current groups: %@", favouriteSectionsFromDefaults);
	// if there are no favourites
	NSMutableArray *favouriteSections = [[NSMutableArray alloc] initWithCapacity:0];
	if (favouriteSectionsFromDefaults != nil)
		[favouriteSections setArray:favouriteSectionsFromDefaults];

	// not there?
	if ([favouriteSections count] <= index)
	{
		[favouriteSections release];
		return;
	}
	
	// replace it now
	[favouriteSections replaceObjectAtIndex:index withObject:newName];
	[defaults setObject:favouriteSections forKey:@"favouriteSections"];
	[defaults synchronize];
	[self populateFavouriteStopList];
	//NSLog(@"New groups: %@", favouriteSections);
	
	// send out a notification so that anything listening for favourites updates can refresh
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc postNotificationName:TTFavouritesUpdated object:nil];
	[favouriteSections release];
}


// Upgrade the favourites to use NSDictionarys
- (void)upgradeFavouritesTo11
{
	if ([self hasUpgradedFavouritesTo11])
		return;

	// upgrade the favourites and filters and such
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSArray *oldFavouritesList = [defaults arrayForKey:@"favouriteStops"];
	NSDictionary *oldFiltersList = [defaults dictionaryForKey:@"filters"];
	
	// create the new array
	NSMutableArray *newFavouritesList = [[NSMutableArray alloc] initWithCapacity:0];
	
	// loop over the old one creating new stuff
	for (NSNumber *trackerID in oldFavouritesList)
	{
		NSMutableDictionary *favourite = [[NSMutableDictionary alloc] initWithCapacity:3];
		[favourite setObject:trackerID forKey:@"trackerid"];
		
		// does it have a filter?
		NSDictionary *filter = (NSDictionary *)[oldFiltersList objectForKey:[trackerID stringValue]];
		if (filter != nil)
		{
			[favourite setObject:[filter objectForKey:@"route"] forKey:@"route"];
			[favourite setObject:[filter objectForKey:@"lowfloor"] forKey:@"lowfloor"];
		} else
		{
			// no filter, default to 'all'
			[favourite setObject:@"0" forKey:@"route"];
			[favourite setObject:@"NO" forKey:@"lowfloor"];
		}
		
		// add this favourite to our list
		[newFavouritesList addObject:favourite];
		[favourite release];
		favourite = nil;
	}
	
	// all done, save the new favourites list
	[defaults setObject:newFavouritesList forKey:@"favouriteStopsList"];
	
	// and remove the old ones
	[defaults removeObjectForKey:@"favouriteStops"];
	[defaults removeObjectForKey:@"filters"];
	
	// and sync
	[defaults synchronize];
	[newFavouritesList release];
	newFavouritesList = nil;
}

// Upgrade the favourites to use section and row information
- (void)upgradeFavouritesTo12
{
	if ([self hasUpgradedFavouritesTo12])
		return;
	
	// find the old list
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSArray *oldFavouritesList = [defaults arrayForKey:@"favouriteStopsList"];
	if (oldFavouritesList == nil || [oldFavouritesList count] == 0)
		return;
	
	// create the new list
	NSMutableArray *newFavouritesList = [[NSMutableArray alloc] initWithCapacity:0];
	
	// loop over the old one
	NSInteger i = 0;
	for (NSDictionary *dict in oldFavouritesList)
	{
		// create a new mutable dictionary
		NSMutableDictionary *newDict = [[NSMutableDictionary alloc] initWithCapacity:0];
		[newDict setDictionary:dict];
		
		// set the section and row
		[newDict setObject:[NSNumber numberWithInteger:0] forKey:@"section"];
		[newDict setObject:[NSNumber numberWithInteger:i] forKey:@"row"];
		
		// add it to the array
		[newFavouritesList addObject:newDict];
		
		//NSLog(@"Favourite %@ has been upgraded to %@", dict, newDict);
		[newDict release];
		newDict = nil;
		i++;
	}
	
	// save the new array
	[defaults setObject:newFavouritesList forKey:@"favouriteStopsList"];
	[defaults synchronize];
	[newFavouritesList release];
	newFavouritesList = nil;
}

// Have these favourites been upgraded?
- (BOOL)hasUpgradedFavouritesTo11
{
	// do we have items in the favouriteStops user default?
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	return ([defaults arrayForKey:@"favouriteStops"] == nil);
}

- (BOOL)hasUpgradedFavouritesTo12
{
	// do we have row and section info in the first result?
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSArray *favouriteStops = [defaults arrayForKey:@"favouriteStopsList"];
	return (favouriteStops == nil || [favouriteStops count] == 0 || [[favouriteStops objectAtIndex:0] objectForKey:@"section"] != nil);
}



//
// Get the full information for a stop matching the given trackerid
//
- (Stop *)getStopForTrackerID:(NSNumber *)trackerID
{
	if ([trackerID integerValue] == 0)
		return nil;

	return [Stop stopForTrackerID:trackerID];
}

/**
 * Get a list of stops for a specific tracker id
**/
- (NSArray *)getStopListForTrackerIDs:(NSArray *)trackerIDs
{
	// loop over the tracker IDS
	return [Stop stopsForTrackerIDs:trackerIDs];
}

//
// Get the number of suburbs in the stop list
//
- (NSInteger)numberOfSuburbs
{
	return [[self listOfSuburbs] count];
}

//
// Get a list of the suburbs
//
- (NSArray *)listOfSuburbs
{
	// do we already have a list?
	if ([self suburbList] != nil)
		return [self suburbList];
	
	NSMutableArray *list = [[NSMutableArray alloc] initWithCapacity:0];
	
	// loop over the stops
	for (Stop *stop in [Stop allStops])
	{
		// suburb not in list?
		if ([list indexOfObject:[stop suburbName]] == NSNotFound)
			[list addObject:[stop suburbName]];
	}
	
	//NSLog(@"List of suburbs: %@", list);
	
	// sort it
	[list sortUsingSelector:@selector(caseInsensitiveCompare:)];
	
	// cache it
	[self setSuburbList:list];

	return [list autorelease];
}

//
// Get all the stops in a suburb
//
- (NSArray *)stopsInSuburb:(NSString *)suburb
{
	return [[Stop stopsInSuburb:suburb] sortedArrayUsingSelector:@selector(compareStop:)];
}

//
// Get the most recent stops from the most recent stop list
//
- (NSArray *)mostRecentStops
{
	// open the defaults to see if we have a list
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	// do we have anything?
	NSArray *mostRecentStopsFromDefaults = [defaults arrayForKey:@"mostRecentStops"];
	
	// if there are no most recents return nil
	if (mostRecentStopsFromDefaults == nil)
		return nil;
	
	// if we have more than the number of stops to remember then only grab the first whatever
	NSUInteger mostRecentStopCount;
	if ([defaults objectForKey:@"mostRecentStopCount"] == nil)
		mostRecentStopCount = 10;
	else
		mostRecentStopCount = [[defaults objectForKey:@"mostRecentStopCount"] intValue];

	if ([mostRecentStopsFromDefaults count] > mostRecentStopCount)
		return [mostRecentStopsFromDefaults subarrayWithRange:NSMakeRange(0, mostRecentStopCount)];

	return mostRecentStopsFromDefaults;
}

//
// Do we have any most recent stops?
//
- (BOOL)hasMostRecentStops
{
	return [self mostRecentStops] != nil;
}

//
// Add a stop ID to the most recent stops list
//
- (void)addMostRecentStopID:(NSNumber *)trackerID
{
	// work around the 1022 bug
	trackerID = [NSNumber numberWithInteger:[trackerID integerValue]];

	if ([trackerID integerValue] > 10000 || [trackerID integerValue] < 1000)
		return;

	// we don't add favourites to the most recent
	if ([self isFavouriteStop:trackerID])
		return;
	
	//NSLog(@"Adding trackerID %@ to most recent stops list", trackerID);

	// open the defaults and get the existing list
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSArray *mostRecentStopsFromDefaults = [defaults arrayForKey:@"mostRecentStops"];
	
	// create a new array with our values
	NSMutableArray *newRecentStopsList = [[NSMutableArray alloc] initWithCapacity:0];

	// if we have existing stops best grab them too to maintain the list
	if (mostRecentStopsFromDefaults != nil)
		[newRecentStopsList addObjectsFromArray:mostRecentStopsFromDefaults];

	// remove any occurances of this stop from the list
	[newRecentStopsList removeObject:trackerID];
	
	// and add the new stop at the top of the list
	//NSNumber *tID = [NSNumber numberWithInteger:[trackerID integerValue]];
	//NSLog(@"%@ - %@", newRecentStopsList, trackerID);
	[newRecentStopsList insertObject:trackerID atIndex:0];
	
	// keep the array trimmed to a max of 100 recent stops
	if ([newRecentStopsList count] > 100)
		[defaults setObject:[newRecentStopsList subarrayWithRange:NSMakeRange(0, 100)] forKey:@"mostRecentStops"];
	else
		// no trimming needed, just update the defaults
		[defaults setObject:newRecentStopsList forKey:@"mostRecentStops"];

	// all done
	[newRecentStopsList release];
	[defaults synchronize];
}

// Get the overriding text for a stop
- (NSString *)directionTextForStopID:(NSNumber *)trackerID
{
	// get the override list
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSDictionary *directionTexts = [defaults dictionaryForKey:@"directionTextOverrides"];

	// none set yet?
	if (directionTexts == nil)
		return nil;
	
	// find the trackerid in the list
	NSString *directionText = [directionTexts objectForKey:[trackerID stringValue]];
	if (directionText == nil)
		return nil;
	return directionText;
}

// save overriding text
- (void)setDirectionText:(NSString *)directionText forStopID:(NSNumber *)trackerID
{
	// get the override list
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSMutableDictionary *directionTexts = [[NSMutableDictionary alloc] initWithCapacity:0];
	NSDictionary *existingDirectionTexts = [defaults dictionaryForKey:@"directionTextOverrides"];
	
	// existing dictionary? copy it to the new one
	if (existingDirectionTexts != nil)
		[directionTexts setDictionary:existingDirectionTexts];

	// set our new one
	[directionTexts setObject:directionText forKey:[trackerID stringValue]];
	
	// save it back
	[defaults setObject:[NSDictionary dictionaryWithDictionary:directionTexts] forKey:@"directionTextOverrides"];
	
	// release and sync
	[directionTexts release];
	[defaults synchronize];
	
	// is this a favourite stop? send out the changed notification if so
	if ([self isFavouriteStop:trackerID])
	{
		// send out a notification so that anything listening for favourites updates can refresh
		NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
		[nc postNotificationName:TTFavouritesUpdated object:nil];
	}
}

// Singleton we may be, at some point we'll be dealloc'd though (probably when the app closes) - no excuse for not playing nice though!
- (void)dealloc
{
	[sharedStopList release];
	[suburbList release];
	[internalFavouriteStopList release];
	
	[super dealloc];
}
@end
