//
//  TicketOutletServiceDelegate.h
//  tramTRACKER
//
//  Created by Robert Amos on 5/08/10.
//  Copyright 2010 Metro Trains Melbourne. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSInteger TTTicketOutletServiceTypeCheck;
extern NSInteger TTTicketOutletServiceTypeUpdate;


@interface TicketOutletServiceDelegate : NSObject {

	NSObject *delegate;
	NSDate *updatesSinceDate;
	NSInteger serviceType;
	NSMutableString *responseText;
}

@property (nonatomic, readonly) NSObject *delegate;
@property (nonatomic, retain) NSDate *updatesSinceDate;
@property (nonatomic) NSInteger serviceType;

- (id)initWithDelegate:(NSObject *)aDelegate;
- (void)failWithError:(NSError *)error;

- (void)doesTicketOutletsHaveUpdatesUsingResponse:(NSHTTPURLResponse *)response;
- (void)updateTicketOutletsWithData:(NSData *)data;

@end
