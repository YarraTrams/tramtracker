//
//  PidsServiceDelegate.h
//  tramTRACKER
//
//  Created by Robert Amos on 18/02/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Stop.h"
#import "Prediction.h"
#import "PidsService.h"
#import "Route.h"
#import "StopList.h"
#import "Journey.h"
#import "Tram.h"
#import "RouteList.h"
#import "StopList.h"

/**
 * @ingroup Data
**/

/**
 * A delegate created by the PidsService to parse the results of a call
**/
@interface PidsServiceDelegate : NSObject <NSXMLParserDelegate> {

	/**
	 * The delegate passed to us by the PidsService that will receive our results
	**/
	id delegate;

	/**
	 * The responseText as an XML string received from the PIDS Service
	**/
	NSMutableString *responseText;

	/**
	 * The Current Property that the XML parser is working on. This is copied to the appropriate place when completed
	**/
	NSString *currentProperty;

	/**
	 * The current Stop that the XML parser is working on.
	**/
	NSMutableDictionary *currentStop;

	/**
	 * The current array of Predictions that the XML parser is building.
	**/
	NSMutableArray *currentPredictions;

	/**
	 * The current Prediction that the XML parser is working on.
	**/
	PredictionStub *currentPrediction;

	/**
	 * The tracker ID that was requested for this lookup. Returned to the delegate.
	**/
	NSNumber *trackerID;
	NSString *routeNumber;

	/**
	 * The local time that we started this request. It is compared to the server time to adjust dates
	**/
	NSDate *requestTime;
	NSDate *updateSinceDate;
	
	/**
	 * The current Route List that the XML parser is working on
	**/
	NSMutableDictionary *routeList;

	/**
	 * The current route (or half a route) that the XML parser is working on.
	**/
	RoutePart *currentRoute;
	
	/**
	 * The current stop list that the XML parser is working on
	**/
	NSMutableArray *stopList;
	NSMutableArray *turnList;
	
	/**
	 * Whether the prediction we're working on is scheduled or realtime
	**/
	BOOL scheduledPrediction;
    
    /**
	 * A copy of the NSManagedObjectContext used to interface with the database.
     **/
	NSManagedObjectContext *managedObjectContext;

	
	NSMutableArray *updateList;
    NSDate *serverTime;
	StopUpdate *currentStopUpdate;
	RouteUpdate *currentRouteUpdate;
	NSMutableDictionary *currentRouteDestinations;
	NSMutableDictionary *currentTurn;
	NSNumber *upDirection;
	
	JourneyStub *currentJourney;
	TramStub *currentTram;
	JourneyStopStub *currentJourneyStop;
	NSMutableArray *routesThroughStop;
	NSString *validationResult;
	BOOL webServiceMethod;
	BOOL ticketOutletUpdateCheck;
	BOOL ticketOutletDownload;
	NSTimer *timeoutTimer;
}

//
// The delegate that will handle the results
//
@property (nonatomic, retain) id delegate;

//
// Data that is in the process of being parsed. Parsing creates, populates and then sends these objects to the appropriate places
//
@property (nonatomic, retain) NSMutableString *responseText;
@property (nonatomic, retain) NSString *currentProperty;
@property (nonatomic, retain) NSMutableDictionary *currentStop;
@property (nonatomic, retain) NSMutableArray *currentPredictions;
@property (nonatomic, retain) PredictionStub *currentPrediction;
@property (nonatomic, retain) NSNumber *trackerID;
@property (nonatomic, retain) NSString *routeNumber;
@property (nonatomic, retain) NSDate *requestTime;
@property (nonatomic, retain) NSDate *updateSinceDate;
@property (nonatomic, retain) NSMutableDictionary *routeList;
@property (nonatomic, retain) RoutePart *currentRoute;
@property (nonatomic, retain) NSMutableArray *stopList;
@property (nonatomic, retain) NSMutableArray *turnList;
@property (nonatomic, retain) NSMutableDictionary *currentTurn;
@property (nonatomic, getter=isScheduledPrediction) BOOL scheduledPrediction;
@property (nonatomic, retain) NSNumber *upDirection;
@property (nonatomic, retain) JourneyStub *currentJourney;
@property (nonatomic, retain) TramStub *currentTram;
@property (nonatomic, retain) JourneyStopStub *currentJourneyStop;
@property (nonatomic, retain) NSMutableArray *updateList;
@property (nonatomic, retain) NSDate *serverTime;
@property (nonatomic, retain) StopUpdate *currentStopUpdate;
@property (nonatomic, retain) RouteUpdate *currentRouteUpdate;
@property (nonatomic, retain) NSMutableDictionary *currentRouteDestinations;
@property (nonatomic, retain) NSMutableArray *routesThroughStop;
@property (nonatomic, retain) NSString *validationResult;
@property (readonly, nonatomic, getter=isWebServiceMethod) BOOL webServiceMethod;
@property (nonatomic, getter=isTicketOutletUpdateCheck) BOOL ticketOutletUpdateCheck;
@property (nonatomic, getter=isTicketOutletDownload) BOOL ticketOutletDownload;


/**
 * Custom Initialiser
 *
 * Initialises ourselves with the delegate passed to us by the PidsService
**/
- (PidsServiceDelegate *)initWithDelegate:(id)newDelegate isWebServiceMethod:(BOOL)isWebService;


/**
 * A NSURLConnectionDelegate method. Called when the connection encountered an error.
**/
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error;

/**
 * A NSURLConnectionDelegate method. Called when we did receive response headers from the PIDS Service.
 *
 * Prepares to receive data from the server
**/
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response;

/**
 * A NSURLConnectionDelegate method. Called when we have received a chunk of data from the service.
**/
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data;

/**
 * A NSURLConnectionDelegate method. Called when we have received the entire response from the server.
 * Calls -parseResponse to begin parsing the PIDS Service's response.
**/
- (void)connectionDidFinishLoading:(NSURLConnection *)connection;

/**
 * Creates and starts an XMLParser to parse the string response that we received from the server.
**/
- (void)parseResponse:(NSString *)response;

/**
 * A NSXMLParserDelegate method. The Parser encountered the start of an element. We create the appropriate object
 * to represent that element in one of the "current" properties.
**/
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributes;

/**
 * A NSXMLParserDelegate method. The Parser encountered the end of an element. We close off that element, copy it to the appropriate
 * location and dispatch it to the delegate if necessary.
**/
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName;

/**
 * A NSXMLParserDelegate method. The Parser encountered some text between elements, copy it to the currentProperty if necessary.
**/
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string;

/**
 * Aborts parsing of the document
**/
- (void)abortParsing:(NSXMLParser *)parser;

/**
 * Parses a Date and Time string returned by the PIDS Service into a NSDate object
 *
 * @param	dateString		A date string as returned by the PIDS Service
 * @returns					An NSDate object
**/
- (NSDate *)parseDateTime:(NSString *)dateString;

/**
 * Find the predicted arrival time relative to the local clock by using the server's predicted arrival time and a comparison
 * of the difference between the two clocks
 *
 * @param	serverPredictedArrival			The predicated arrival time relative to the server's clock
 * @param	serverRequestTime				The time that this prediction was requested relative to the server's clock
 * @param	localRequestTime				The time that this prediction was requested relative to the local clock
 *
 * @returns									The predicted arrival time relative to the local clock
**/
- (NSDate *)calculateLocalPredictedArrivalTimeFromServerTime:(NSDate *)serverPredictedArrival withServerRequestTime:(NSDate *)serverRequestTime withLocalRequestTime:(NSDate *)localRequestTime;
- (NSDate *)calculateLocalScheduledDepartureTimeFromServerTime:(NSDate *)serverPredictedArrival withServerRequestTime:(NSDate *)serverRequestTime withLocalRequestTime:(NSDate *)localRequestTime;

/**
 * Send a failure message to the delegate
 **/
- (void)failWithError:(NSError *)error;
- (void)sendValidationResult;
- (void)timeout:(NSTimer *)timer;

/**
 * Data Retrieval
**/
- (NSManagedObjectContext *)managedObjectContext;
- (StopStub *)stopForTrackerID:(NSNumber *)aTrackerID;
- (RouteStub *)routeWithNumber:(NSString *)aRouteNumber;
- (RouteStub *)routeWithNumber:(NSString *)aRouteNumber headboardRouteNumber:(NSString *)aHeadboardRouteNumber;

@end
