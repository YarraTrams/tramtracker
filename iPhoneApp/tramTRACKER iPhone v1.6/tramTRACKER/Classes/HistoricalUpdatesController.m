//
//  HistoricalUpdatesController.m
//  tramTRACKER
//
//  Created by Robert Amos on 5/08/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "HistoricalUpdatesController.h"
#import "UnderBackgroundView.h"
#import "HistoricalUpdateCell.h"
#import "HistoricalUpdate.h"
#import "OnboardListCell.h"

@implementation HistoricalUpdatesController

- (id)initHistoricalUpdatesController
{
	if (self = [super initWithStyle:UITableViewStylePlain])
	{
		[self setTitle:NSLocalizedString(@"updates-history-title", nil)];
		[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
		[self.tableView setTableFooterView:[self tableFooterView]];
		[self.tableView setAllowsSelection:NO];
	}
	return self;
}

- (void)viewDidLoad
{
	[self _setHistoricalUpdates];
	
	// set the clear button
	UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"updates-history-clear", nil)
															   style:UIBarButtonItemStylePlain
															  target:self
															  action:@selector(clearHistory)];
	[self.navigationItem setRightBarButtonItem:button];
	[button release];
}

- (void)_setHistoricalUpdates
{
	NSArray *updates = [HistoricalUpdate getHistoricalUpdatesList];
	if (updates == nil || [updates count] == 0)
	{
		historicalUpdates = [[NSArray array] retain];
		sectionTitles = [[NSArray array] retain];
	}
	
	// go through the historical updates list and sort them into sections
	NSMutableArray *splitUpdates = [[NSMutableArray alloc] initWithCapacity:0];
	NSMutableArray *sTitles = [[NSMutableArray alloc] initWithCapacity:0];
	
	// create the date formatter
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:NSLocalizedString(@"updates-history-date-format", nil)];
	
	// loop through the original array and split them up into the new one
	NSString *currentSectionTitle = nil;
	NSMutableArray *currentSection = nil;
	for (HistoricalUpdate *u in updates)
	{
		NSString *newSectionTitle = [dateFormatter stringFromDate:u.updatedDate];
		if (![newSectionTitle isEqualToString:currentSectionTitle])
		{
			// put the current section onto the main array
			if (currentSection != nil)
			{
				NSArray *copyOfCurrentSection = [currentSection copy];
				[splitUpdates addObject:copyOfCurrentSection];
				[currentSection release];
				currentSection = nil;
				[copyOfCurrentSection release]; copyOfCurrentSection = nil;
				
				[sTitles addObject:currentSectionTitle];
				currentSectionTitle = nil;
			}

			// create a new current Section
			currentSection = [[NSMutableArray alloc] initWithCapacity:0];
			currentSectionTitle = newSectionTitle;
		}

		// add the update to the current section
		[currentSection addObject:u];
	}
	[dateFormatter release]; dateFormatter = nil;
	
	// now add whatever we have left
	if (currentSection != nil)
	{
		NSArray *copyOfCurrentSection = [currentSection copy];
		[splitUpdates addObject:copyOfCurrentSection];
		[currentSection release];
		currentSection = nil;
		[copyOfCurrentSection release]; copyOfCurrentSection = nil;
		
		[sTitles addObject:currentSectionTitle];
		currentSectionTitle = nil;
	}
	
	// now save them
	historicalUpdates = [splitUpdates copy];
	sectionTitles = [sTitles copy];
	[splitUpdates release];
	splitUpdates = nil;
	[sTitles release];
	sTitles = nil;
}

// Clear the history, create an action sheet confirmation
- (void)clearHistory
{
	UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"updates-history-clear-prompt", nil)
													   delegate:self
											  cancelButtonTitle:NSLocalizedString(@"updates-history-clear-cancel", nil)
										 destructiveButtonTitle:NSLocalizedString(@"updates-history-clear", nil)
											  otherButtonTitles:nil];
	[sheet showFromTabBar:self.navigationController.tabBarController.tabBar];
	[sheet release];
}

- (void)actionSheet:(UIActionSheet *)sheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == 0)
	{
		// clearing the log.
		[HistoricalUpdate clearHistoricalUpdatesList];
		[self.navigationController popViewControllerAnimated:YES];
	}
}

//
// Create the table footer
//
- (UIView *)tableFooterView
{
	CGRect frame = CGRectMake(0, 0, self.tableView.frame.size.width, 10);
	UnderBackgroundView *ub = [[UnderBackgroundView alloc] initWithFrame:frame];
	return [ub autorelease];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	NSInteger count = (sectionTitles == nil ? 0 : [sectionTitles count]);
    return count == 0 ? 1 : count;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (historicalUpdates != nil && [historicalUpdates count] > 0)
		return [[historicalUpdates objectAtIndex:section] count];
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	if (sectionTitles == nil || [sectionTitles count] == 0)
		return 0;
    return 22;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	if (sectionTitles == nil || [sectionTitles count] == 0)
		return nil;
	return [sectionTitles objectAtIndex:section];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	// get the title
	NSString *sectionName = [self tableView:tableView titleForHeaderInSection:section];
	if (sectionName == nil)
		return nil;
	
	// create a view
	OnboardListSectionHeaderView *view = [[OnboardListSectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 22)];
	[view.title setText:sectionName];
	return [view autorelease];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    HistoricalUpdateCell *cell = (HistoricalUpdateCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[HistoricalUpdateCell alloc] initWithCellStyle:TTSimpleCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Set up the cell...
	NSArray *section = [historicalUpdates objectAtIndex:indexPath.section];
	HistoricalUpdate *update = [section objectAtIndex:indexPath.row];
	[cell setTextLabelText:update.name];
	[cell setDetailTextLabelText:update.message];
	
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
	// AnotherViewController *anotherViewController = [[AnotherViewController alloc] initWithNibName:@"AnotherView" bundle:nil];
	// [self.navigationController pushViewController:anotherViewController];
	// [anotherViewController release];
}

- (void)dealloc {
	[sectionTitles release];
	[historicalUpdates release];
    [super dealloc];
}


@end

