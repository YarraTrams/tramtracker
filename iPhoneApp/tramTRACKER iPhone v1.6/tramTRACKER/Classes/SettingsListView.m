//
//  SettingsListView.m
//  tramTRACKER
//
//  Created by Robert Amos on 17/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "SettingsListView.h"
#import "BackgroundSynchroniser.h"
#import "SyncOptionSelector.h"
#import "Tram.h"
#import "BackgroundSoundSelectionController.h"

@class tramTRACKERAppDelegate, MostRecentController;

@implementation SettingsListView


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}

- (void)viewDidLoad
{
	[super viewDidLoad];

	// Reset our background colour
	[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];

	// reset the background enabled value
	backgroundEnabled = [[NSUserDefaults standardUserDefaults] boolForKey:@"backgroundEnabled"];

}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // ARRIVAL ALARM SUPPORT - return 7;
    return 6;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (section == 0)
		return 2;
	if (section == 1)
		return 1;
	if (section == 2)
		return 3;
	if (section == 3)
		return 2;
	if (section == 4)
		return 1;
	if (section == 5)
		return 1;

	// ARRIVAL ALARM SUPPORT
    //if (section == 6)
	//	return ([self backgroundEnabled] ? 3 : 1);
	return 0;
}

// Titles for each section
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	if (section == 0)
		return NSLocalizedString(@"settings-sectiontitle-startup", @"Startup");
	if (section == 1)
		return NSLocalizedString(@"settings-sectiontitle-sync", @"Data Synchronisation");
	if (section == 2)
		return NSLocalizedString(@"settings-sectiontitle-displayopts", @"Display Options");
	if (section == 3)
		return NSLocalizedString(@"settings-sectiontitle-onboard", @"Onboard");
	if (section == 4)
		return NSLocalizedString(@"settings-sectiontitle-pid", @"PID");
	if (section == 5)
		return NSLocalizedString(@"settings-sectiontitle-stoplists", @"Browsing Stop Lists");

    // ARRIVAL ALARM SUPPORT
	//if (section == 6)
	//	return NSLocalizedString(@"settings-sectiontitle-background", @"Arrival Alarm");
	return nil;
}

// Footer for each section
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
	if (section == 0)
		return NSLocalizedString(@"settings-footer-startup", @"Startup Footer");
	if (section == 3)
		return NSLocalizedString(@"settings-footer-onboard", @"Onboard Footer");
	if (section == 4)
		return NSLocalizedString(@"settings-footer-pid", @"PID Footer");

    // ARRIVAL ALARM SUPPORT
    //if (section == 6 && [self backgroundEnabled])
	//	return NSLocalizedString(@"settings-footer-background", @"Background Footer");
	return nil;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
	static NSString *textCell = @"TextCell";
	
	BOOL isSimpleCell = (indexPath.section == 3 || indexPath.section == 4 || /** ARRIVAL ALARM SUPPORT (indexPath.section == 6 && indexPath.row != 2) || **/ indexPath.section == 5 || (indexPath.section == 0 && indexPath.row == 1));
    
    SimpleCell *cell = (SimpleCell *)[tableView dequeueReusableCellWithIdentifier:(isSimpleCell ? CellIdentifier : textCell)];
    if (cell == nil) {
        cell = [[[SimpleCell alloc] initWithCellStyle:(isSimpleCell ? TTSimpleCellStyleDefault : TTSimpleCellStyleValue1)
									  reuseIdentifier:(isSimpleCell ? CellIdentifier : textCell)] autorelease];
		if (isSimpleCell)
		{
			[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
			UISwitch *switchButton = [[UISwitch alloc] initWithFrame:CGRectZero];
			[cell setAccessoryView:switchButton];
			[switchButton release]; 
		} else
		{
			[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
		}
    }
	
	// clear off any events
	if (isSimpleCell)
	{
		UISwitch *s = (UISwitch *)cell.accessoryView;
		NSArray *actions = [s actionsForTarget:self forControlEvent:UIControlEventValueChanged];
		if (actions != nil)
		{
			for (NSString *action in actions)
				[s removeTarget:self action:NSSelectorFromString(action) forControlEvents:UIControlEventValueChanged];
		}
	}
    
    // Set up the cell...
	if (indexPath.section == 0 && indexPath.row == 0)
	{
		[cell setTextLabelText:NSLocalizedString(@"settings-startup-openwith", @"Open With")];
		[cell setDetailTextLabelText:[self openWithStringValue]];
	}
	else if (indexPath.section == 0 && indexPath.row == 1)
	{
		[cell setTextLabelText:NSLocalizedString(@"settings-startup-smartrestore", @"Smart Restore")];
		UISwitch *s = (UISwitch *)cell.accessoryView;
		[s addTarget:self action:@selector(toggleSmartRestore) forControlEvents:UIControlEventValueChanged];
		[s setOn:[self smartRestore] animated:NO];
	}
	else if (indexPath.section == 1 && indexPath.row == 0)
	{
		[cell setTextLabelText:NSLocalizedString(@"settings-sync-type", @"Sync Type")];
		[cell setDetailTextLabelText:[self synchronisationStringValue]];
	}
	else if (indexPath.section == 2 && indexPath.row == 0)
	{
		[cell setTextLabelText:NSLocalizedString(@"settings-displayopts-nearbycount", @"Nearby Count")];
		[cell setDetailTextLabelText:[self nearbyStopCountStringValue]];
	}
	else if (indexPath.section == 2 && indexPath.row == 1)
	{
		[cell setTextLabelText:NSLocalizedString(@"settings-displayopts-recentcount", @"Most Recent Count")];
		[cell setDetailTextLabelText:[self mostRecentStopCountStringValue]];
	}
	else if (indexPath.section == 2 && indexPath.row == 2)
	{
		[cell setTextLabelText:NSLocalizedString(@"settings-displayopts-showmessages", @"Show Mesages on PID")];
		[cell setDetailTextLabelText:[self showMessagesStringValue]];
	}
	
	else if (indexPath.section == 3 && indexPath.row == 0)
	{
		[cell setTextLabelText:NSLocalizedString(@"settings-onboard-turn-indicators", @"Show turn indicators")];
		UISwitch *s = (UISwitch *)cell.accessoryView;
		[s addTarget:self action:@selector(toggleTurnIndicators) forControlEvents:UIControlEventValueChanged];
		[s setOn:[self showTurnIndicators] animated:NO];
	}
	else if (indexPath.section == 3 && indexPath.row == 1)
	{
		[cell setTextLabelText:NSLocalizedString(@"settings-onboard-usegps", @"Use GPS")];
		UISwitch *s = (UISwitch *)cell.accessoryView;
		[s addTarget:self action:@selector(toggleUseGPS) forControlEvents:UIControlEventValueChanged];
		[s setOn:[self useGPSValue] animated:NO];
	}
	
	else if (indexPath.section == 4 && indexPath.row == 0)
	{
		[cell setTextLabelText:NSLocalizedString(@"settings-pid-tram-profiles", @"Show Tram Profiles")];
		UISwitch *s = (UISwitch *)cell.accessoryView;
		[s addTarget:self action:@selector(toggleTramProfiles) forControlEvents:UIControlEventValueChanged];
		[s setOn:[self showTramProfiles] animated:NO];
	}
	else if (indexPath.section == 5 && indexPath.row == 0)
	{
		[cell setTextLabelText:NSLocalizedString(@"settings-stoplists-jumptonearest", @"Jump to Nearest")];
		UISwitch *s = (UISwitch *)cell.accessoryView;
		[s addTarget:self action:@selector(toggleJumpToNearestStop) forControlEvents:UIControlEventValueChanged];
		[s setOn:[self jumpToNearestStopValue] animated:NO];
	} 

    /**
     * ARRIVAL ALARM SUPPORT
     *
	else if (indexPath.section == 6 && indexPath.row == 0)
	{
		[cell setTextLabelText:NSLocalizedString(@"settings-background-enabled", @"Enabled")];
		UISwitch *s = (UISwitch *)cell.accessoryView;
		[s addTarget:self action:@selector(toggleBackgroundEnabled) forControlEvents:UIControlEventValueChanged];
		[s setOn:[self backgroundEnabled] animated:NO];
	}
	else if (indexPath.section == 6 && indexPath.row == 1)
	{
		[cell setTextLabelText:NSLocalizedString(@"settings-background-alertmessage", @"Alert Message")];
		UISwitch *s = (UISwitch *)cell.accessoryView;
		[s addTarget:self action:@selector(toggleBackgroundAlertMessage) forControlEvents:UIControlEventValueChanged];
		[s setOn:[self backgroundAlertMessage] animated:NO];
	}
	else if (indexPath.section == 6 && indexPath.row == 2)
	{
		[cell setTextLabelText:NSLocalizedString(@"settings-background-alertsoundname", @"Sound Name")];
		[cell setDetailTextLabelText:[self backgroundAlertSoundTypeStringValue]];
	}
     **/
	
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

	// Open With Selection
	if (indexPath.section == 0 && indexPath.row == 0)
	{
		[self pushStartupOptionSelection];
	}
	
	// Data Synchronisation
	else if (indexPath.section == 1 && indexPath.row == 0)
	{
		[self pushSynchronisationOptionSelection];
	}
	
	// Display Options - Show Nearby Stops
	else if (indexPath.section == 2 && indexPath.row == 0)
	{
		[self pushNearbyStopCountSelection];
	}

	// Display Options - Show Most Recent
	else if (indexPath.section == 2 && indexPath.row == 1)
	{
		[self pushMostRecentStopCountSelection];
	}
	
	// Display Options - Show Messages On PID
	else if (indexPath.section == 2 && indexPath.row == 2)
	{
		[self pushChangeShowMessages];
	}
	
	// Background Alert Sound
    /**
     * ARRIVAL ALARM SUPPORT
     *
	else if (indexPath.section == 6 && indexPath.row == 2)
	{
		[self pushBackgroundAlertSoundOptionSelection];
	}
     **/
}


#pragma mark -
#pragma mark Values from the defaults

- (NSString *)openWithStringValue
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSNumber *startupOption = [defaults objectForKey:@"startup"];
	NSInteger startupValue;
	
	// if its empty default to 1
	if (startupOption == nil)
		startupValue = 1;
	else
		startupValue = [startupOption integerValue];

	
	// return a text description depending on what it is
	switch (startupValue)
	{
		case 1:
			return NSLocalizedString(@"settings-startup-nearestfavourite", @"Nearest Favourite");
			
		case 2:
			return NSLocalizedString(@"settings-startup-favouriteslist", @"Favourites List");
			
		case 3:
			return NSLocalizedString(@"settings-startup-nearbylist", @"Nearby List");
			
		default:
			return @"";
	}
}

- (NSString *)synchronisationStringValue
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSNumber *syncOption = [defaults objectForKey:@"synchronisationType"];
	NSInteger syncValue = [syncOption integerValue];
	
	// return a text description
	//if (syncValue == TTSyncTypeAutomatic)
	//	return NSLocalizedString(@"settings-sync-type-auto", @"Automatic");
	if (syncValue == TTSyncTypeAutoCheckManualSync || syncValue == TTSyncTypeAutomatic)
		return NSLocalizedString(@"settings-sync-type-semi-auto", @"Semi Automatic");
	else if (syncValue == TTSyncTypeManual)
		return NSLocalizedString(@"settings-sync-type-manual", @"Manual");
	
	return @"";
}

- (NSString *)nearbyStopCountStringValue
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSNumber *nearbyOption = [defaults objectForKey:@"nearbyStopCount"];

	// if its empty default to 10
	if (nearbyOption == nil)
		nearbyOption = [NSNumber numberWithInt:10];

	return [NSString stringWithFormat:NSLocalizedString(@"settings-stopcount", "x Stops"), nearbyOption];
}

- (NSString *)mostRecentStopCountStringValue
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSNumber *recentOption = [defaults objectForKey:@"mostRecentStopCount"];
	
	// if its empty default to 10
	if (recentOption == nil)
		recentOption = [NSNumber numberWithInt:10];
	
	return [NSString stringWithFormat:NSLocalizedString(@"settings-stopcount", "x Stops"), recentOption];
}

- (NSString *)showMessagesStringValue
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSInteger messagesOption = [(NSNumber *)[defaults objectForKey:@"showMessagesOnPID"] integerValue];
	
	if (messagesOption == PIDShowMessagesNone)
		return NSLocalizedString(@"settings-showmessages-never", @"Never");
	else if (messagesOption == PIDShowMessagesAlways)
		return NSLocalizedString(@"settings-showmessages-always", @"Always");
	else if (messagesOption == PIDShowMessagesOnce)
		return NSLocalizedString(@"settings-showmessages-once", @"Once");
	else
		return NSLocalizedString(@"settings-showmessages-twice", @"Twice");
}

- (BOOL)jumpToNearestStopValue
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	return [defaults boolForKey:@"jumpToNearestStopInStopList"];
}

- (BOOL)useGPSValue
{
	return [[NSUserDefaults standardUserDefaults] boolForKey:@"onboardGPSEnabled"];
}

- (BOOL)showTurnIndicators
{
	return [[NSUserDefaults standardUserDefaults] boolForKey:@"showTurnIndicators"];
}

- (BOOL)showTramProfiles
{
	return [[NSUserDefaults standardUserDefaults] boolForKey:@"showTramProfiles"];
}

- (BOOL)smartRestore
{
	return [[NSUserDefaults standardUserDefaults] boolForKey:@"smartRestore"];
}

/**
 * ARRIVAL ALARM SUPPORT
 *
- (BOOL)backgroundEnabled
{
	return backgroundEnabled;
}
- (BOOL)backgroundAlertMessage
{
	return [[NSUserDefaults standardUserDefaults] boolForKey:@"backgroundAlertMessage"];
}
- (NSString *)backgroundAlertSoundType
{
	NSString *soundName = [[NSUserDefaults standardUserDefaults] objectForKey:@"backgroundAlertSoundName"];
	
	// is it not an actual string?
	if (soundName == nil)
		return AlertSoundTramCClass;

	// we have four types to support - system default
	if ([soundName isEqualToString:@"systemSound"])
		return AlertSoundSystemDefault;
	
	// citadis
	if ([soundName isEqualToString:[Tram nameOfGongSoundForClass:@"C"]])
		return AlertSoundTramCClass;
	
	// a class
	if ([soundName isEqualToString:[Tram nameOfGongSoundForClass:@"A1"]])
		return AlertSoundTramAClass;

	// w class
	if ([soundName isEqualToString:[Tram nameOfGongSoundForClass:@"W"]])
		return AlertSoundTramWClass;

	// also none
	if ([soundName isEqualToString:@"none"])
		return AlertSoundNone;
	
	// default is citadis
	return AlertSoundTramCClass;
}

// return the pretty name for each
- (NSString *)backgroundAlertSoundTypeStringValue
{
	// get the type of alert sound
	NSString *alertSoundType = [self backgroundAlertSoundType];

	// go go
	if ([alertSoundType isEqualToString:AlertSoundNone])
		return NSLocalizedString(@"settings-background-nosound", @"None");
	if ([alertSoundType isEqualToString:AlertSoundSystemDefault])
		return NSLocalizedString(@"settings-background-sounddefault", @"System Default");
	if ([alertSoundType isEqualToString:AlertSoundTramAClass])
		return NSLocalizedString(@"settings-background-soundaclass", @"A/B/Z Class Tram");
	if ([alertSoundType isEqualToString:AlertSoundTramCClass])
		return NSLocalizedString(@"settings-background-soundcclass", @"Citadis Tram");
	if ([alertSoundType isEqualToString:AlertSoundTramWClass])
		return NSLocalizedString(@"settings-background-soundwclass", @"W Class Tram");
	
	// uhoh
	return nil;
} **/

#pragma mark -
#pragma mark Subviews for changing list options

- (void)pushChangeShowMessages
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSInteger messagesOption = [(NSNumber *)[defaults objectForKey:@"showMessagesOnPID"] integerValue];

	ShowMessagesSetting *m = [[ShowMessagesSetting alloc] initWithShowMessages:messagesOption target:self action:@selector(setShowMessages:)];
	[self.navigationController pushViewController:m animated:YES];
	[m release];
}

- (void)pushNearbyStopCountSelection
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSNumber *nearbyOption = [defaults objectForKey:@"nearbyStopCount"];
	
	StopListCountSelector *m = [[StopListCountSelector alloc] initWithStopCount:[nearbyOption integerValue] target:self action:@selector(setNearbyStopCount:)];
	[m setTitle:NSLocalizedString(@"settings-displayopts-nearbycount", @"Show Nearby Stops")];
	[self.navigationController pushViewController:m animated:YES];
	[m release];
}

- (void)pushMostRecentStopCountSelection
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSNumber *recentOption = [defaults objectForKey:@"mostRecentStopCount"];
	
	StopListCountSelector *m = [[StopListCountSelector alloc] initWithStopCount:[recentOption integerValue] target:self action:@selector(setMostRecentStopCount:)];
	[m setTitle:NSLocalizedString(@"settings-displayopts-recentcount", @"Show Most Recent")];
	[self.navigationController pushViewController:m animated:YES];
	[m release];
}

- (void)pushStartupOptionSelection
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSNumber *startupOption = [defaults objectForKey:@"startup"];
	
	StartupOptionSelector *s = [[StartupOptionSelector alloc] initWithStartupOption:[startupOption integerValue] target:self action:@selector(setStartupOption:)];
	[s setTitle:NSLocalizedString(@"settings-startup-openwith", @"Open With")];
	[self.navigationController pushViewController:s animated:YES];
	[s release];
}

- (void)pushSynchronisationOptionSelection
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSNumber *syncOption = [defaults objectForKey:@"synchronisationType"];
	
	SyncOptionSelector *s = [[SyncOptionSelector alloc] initWithSyncOption:[syncOption integerValue] target:self action:@selector(setSynchronisationOption:)];
	[s setTitle:NSLocalizedString(@"settings-sync-type", @"Type")];
	[self.navigationController pushViewController:s animated:YES];
	[s release];
}

/**
 * ARRIVAL ALARM SUPPORT
 *
- (void)pushBackgroundAlertSoundOptionSelection
{
	BackgroundSoundSelectionController *b = [[BackgroundSoundSelectionController alloc] initWithSoundType:[self backgroundAlertSoundType] target:self action:@selector(setBackgroundAlertSoundType:)];
	[b setTitle:NSLocalizedString(@"settings-background-alertsound", nil)];
	[self.navigationController pushViewController:b animated:YES];
	[b release];
}
 **/

#pragma mark -
#pragma mark Save options

- (void)toggleJumpToNearestStop
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	BOOL newValue = ![defaults boolForKey:@"jumpToNearestStopInStopList"];
	[defaults setBool:newValue forKey:@"jumpToNearestStopInStopList"];
}

- (void)toggleUseGPS
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	BOOL newValue = ![defaults boolForKey:@"onboardGPSEnabled"];
	[defaults setBool:newValue forKey:@"onboardGPSEnabled"];
	[defaults synchronize];
}

- (void)toggleTurnIndicators
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	BOOL newValue = ![defaults boolForKey:@"showTurnIndicators"];
	[defaults setBool:newValue forKey:@"showTurnIndicators"];
	[defaults synchronize];
}

- (void)toggleTramProfiles
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	BOOL newValue = ![defaults boolForKey:@"showTramProfiles"];
	[defaults setBool:newValue forKey:@"showTramProfiles"];
	[defaults synchronize];
}

- (void)toggleSmartRestore
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	BOOL newValue = ![defaults boolForKey:@"smartRestore"];
	[defaults setBool:newValue forKey:@"smartRestore"];
	[defaults synchronize];
}

/**
 * ARRIVAL ALARM SUPPORT
 *
- (void)toggleBackgroundEnabled
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	backgroundEnabled = ![defaults boolForKey:@"backgroundEnabled"];
	[defaults setBool:backgroundEnabled forKey:@"backgroundEnabled"];
	[defaults synchronize];
	
	// are they enabled? show the two rows below it, and the alert sound section
	NSArray *indexPaths = [NSArray arrayWithObjects:[NSIndexPath indexPathForRow:1 inSection:6], [NSIndexPath indexPathForRow:2 inSection:6], nil];
	[self.tableView beginUpdates];
	if (backgroundEnabled)
	{
		[self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
	}

	// delete the two rows and the section
	else
	{
		[self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
	}
	[self.tableView endUpdates];
	[self.tableView reloadData];
	
	// scroll to the last row
	if (backgroundEnabled)
	{
		NSUInteger lastSection = [self.tableView numberOfSections]-1;
		NSUInteger lastRow = [self.tableView numberOfRowsInSection:lastSection]-1;
		[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:lastRow inSection:lastSection] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
	}
}

- (void)toggleBackgroundAlertMessage
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	BOOL newValue = ![defaults boolForKey:@"backgroundAlertMessage"];
	[defaults setBool:newValue forKey:@"backgroundAlertMessage"];
	[defaults synchronize];
}

- (void)setBackgroundAlertSoundType:(NSString *)soundType
{
	// default
	NSString *soundName = @"citadis.caf";
	
	// go go
	if ([soundType isEqualToString:AlertSoundNone])
		soundName = @"none";
	if ([soundType isEqualToString:AlertSoundSystemDefault])
		soundName = @"systemSound";
	if ([soundType isEqualToString:AlertSoundTramAClass])
		soundName = @"a.caf";
	if ([soundType isEqualToString:AlertSoundTramCClass])
		soundName = @"citadis.caf";
	if ([soundType isEqualToString:AlertSoundTramWClass])
		soundName = @"w.caf";
	//NSLog(@"[SETTINGS] Sound Type: %@", soundName);
	
	// save it
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject:soundName forKey:@"backgroundAlertSoundName"];
	[defaults synchronize];
	[self.tableView reloadData];
}

**/

- (void)setShowMessages:(NSNumber *)showMessagesOption
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject:showMessagesOption forKey:@"showMessagesOnPID"];
	
	// update the cell itself
	[self.tableView reloadData];
}

- (void)setNearbyStopCount:(NSNumber *)nearbyOption
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject:nearbyOption forKey:@"nearbyStopCount"];
	[self.tableView reloadData];
}

- (void)setMostRecentStopCount:(NSNumber *)recentOption
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject:recentOption forKey:@"mostRecentStopCount"];
	[self.tableView reloadData];
	
	// reload the most recent table to prevent problems
	tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];
	MostRecentController *m = (MostRecentController *)[d.recentController topViewController];
	[m.tableView reloadData];
}

- (void)setStartupOption:(NSNumber *)startupOption
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject:startupOption forKey:@"startup"];
	[self.tableView reloadData];
}

- (void)setSynchronisationOption:(NSNumber *)syncOption
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject:syncOption forKey:@"synchronisationType"];
	[self.tableView reloadData];
}


- (void)dealloc {
    [super dealloc];
}


@end

