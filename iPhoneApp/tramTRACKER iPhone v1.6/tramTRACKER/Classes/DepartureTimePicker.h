//
//  DepartureTimePicker.h
//  tramTRACKER
//
//  Created by Robert Amos on 6/04/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DepartureTimePicker : UIViewController {
	UIButton *departureDate;
	UIButton *departureTime;
	NSDate *scheduledDepartureTime;
	UIDatePicker *picker;
	id target;
	SEL action;
}

@property (nonatomic, retain) IBOutlet UIButton *departureDate;
@property (nonatomic, retain) IBOutlet UIButton *departureTime;
@property (nonatomic, retain) NSDate *scheduledDepartureTime;
@property (nonatomic, retain) IBOutlet UIDatePicker *picker;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil scheduledDepartureTime:(NSDate *)date target:(id)target action:(SEL)selector;
- (IBAction)pickerDidChangeValue:(id)sender;
- (void)updateDateAndTimeButtons;

@end
