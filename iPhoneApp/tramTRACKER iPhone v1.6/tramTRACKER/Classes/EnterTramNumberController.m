//
//  EnterTramNumberController.m
//  tramTRACKER
//
//  Created by Robert Amos on 31/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "EnterTramNumberController.h"

@implementation EnterTramNumberController

@synthesize enterNumberTextField, service, loadingView, loadingIndicator, locatingTramNumber, storedTramNumber, unavailableLabel;

- (void)viewDidLoad
{
	// Add a cancel button, disabled
	UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelEditing:)];
	self.navigationItem.leftBarButtonItem = cancelButton;
	[cancelButton setEnabled:NO];
	[cancelButton release];
	
	// add a Go button.
	UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"onboard-go-button", @"Go")
																   style:UIBarButtonItemStyleDone
																  target:self 
																  action:@selector(locateTram)];
	self.navigationItem.rightBarButtonItem = doneButton;
	[doneButton setEnabled:(enterNumberTextField.text.length > 0)];
	[doneButton release];
	
	// bring the loading view to the front
	[[self.view.subviews objectAtIndex:0] bringSubviewToFront:self.loadingView];
	
	// setup the locating indicator
	[self.loadingIndicator setAnimationImages:[NSArray arrayWithObjects:[UIImage imageNamed:@"radar-frame1.png"], [UIImage imageNamed:@"radar-frame2.png"],
									   [UIImage imageNamed:@"radar-frame3.png"], [UIImage imageNamed:@"radar-frame4.png"],
									   [UIImage imageNamed:@"radar-frame5.png"], [UIImage imageNamed:@"radar-frame6.png"],
									   [UIImage imageNamed:@"radar-frame7.png"], [UIImage imageNamed:@"radar-frame8.png"],
									   [UIImage imageNamed:@"radar-frame9.png"], [UIImage imageNamed:@"radar-frame10.png"],
									   [UIImage imageNamed:@"radar-frame11.png"], [UIImage imageNamed:@"radar-frame12.png"],
									   [UIImage imageNamed:@"radar-frame13.png"], [UIImage imageNamed:@"radar-frame14.png"],
									   [UIImage imageNamed:@"radar-frame15.png"], [UIImage imageNamed:@"radar-frame16.png"],
									   [UIImage imageNamed:@"radar-frame17.png"], [UIImage imageNamed:@"radar-frame18.png"],
									   [UIImage imageNamed:@"radar-frame19.png"], [UIImage imageNamed:@"radar-frame20.png"],
									   [UIImage imageNamed:@"radar-frame21.png"], [UIImage imageNamed:@"radar-frame22.png"],
									   [UIImage imageNamed:@"radar-frame23.png"], [UIImage imageNamed:@"radar-frame24.png"],
									   [UIImage imageNamed:@"radar-frame25.png"], [UIImage imageNamed:@"radar-frame26.png"],
									   [UIImage imageNamed:@"radar-frame27.png"], [UIImage imageNamed:@"radar-frame28.png"],
									   [UIImage imageNamed:@"radar-frame29.png"], [UIImage imageNamed:@"radar-frame30.png"],
									   nil]];
	[self.loadingIndicator setAnimationDuration:2];
}

- (void)viewDidAppear:(BOOL)animated
{
	// do we have a stored tram number at this point? start the load if so.
	if (self.storedTramNumber != nil)
	{
		[self locateTram];
	}

	// enable the go button if there is already text
	if ([self.enterNumberTextField.text length] > 0)
		[self.navigationItem.rightBarButtonItem setEnabled:YES];
}
- (IBAction)didStartEditing:(id)sender
{
	[self.navigationItem.leftBarButtonItem setEnabled:YES];
	if ([self.enterNumberTextField.text length] > 0)
		[self.navigationItem.rightBarButtonItem setEnabled:YES];
}

- (void)cancelEditing:(id)sender
{
	// are we currently loading?
	if (self.locatingTramNumber != nil)
		[self setLocatingTramNumber:nil];

	if (![self.loadingView isHidden])
	{
		[self.loadingView setHidden:YES];
		[self.loadingIndicator stopAnimating];
	} else
	{
		[self.enterNumberTextField resignFirstResponder];
		[self.navigationItem.leftBarButtonItem setEnabled:NO];
	}
}

#pragma mark -
#pragma mark UITextFieldDelegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	BOOL shouldChange = NO;
	// if we're loading you can't edit the text field
	if ([self.loadingView isHidden])
	{
		NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
		
		// there can only be numbers in this string
		NSInteger intVal = [newString integerValue];
		if (([newString isEqualToString:[NSString stringWithFormat:@"%i", intVal]] && intVal < 32767) || [newString length] == 0 )
		{
			shouldChange = YES;

			[self.navigationItem.rightBarButtonItem setEnabled:([newString length] > 0)];
		}
	}
	return shouldChange;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
	[self.navigationItem.rightBarButtonItem setEnabled:NO];
	return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	// don't return if the tram number is empty
	if ([textField.text length] == 0)
		return NO;
	
	[self locateTram];
	return YES;
}

- (void)locateTram
{
	// create the service if its not there already
	if (self.service == nil)
	{
		PidsService *s = [[PidsService alloc] init];
		[s setDelegate:self];
		[self setService:s];
		[s release];
	}
	
	// start looking for the tram number
	if ([enterNumberTextField.text length] > 0)
	{
		[self.enterNumberTextField resignFirstResponder];
		[self setLocatingTramNumber:[NSNumber numberWithInteger:[enterNumberTextField.text integerValue]]];

	} else if (self.storedTramNumber != nil && [self.storedTramNumber integerValue] > 0)
	{
		[enterNumberTextField setText:[self.storedTramNumber stringValue]];
		[self setLocatingTramNumber:self.storedTramNumber];

	} else
	{
		return;
	}
	
	// clear the stored tram number
	[self setStoredTramNumber:nil];

	[self.navigationItem.rightBarButtonItem setEnabled:NO];
	[self.service getJourneyForTramNumber:self.locatingTramNumber];
	[self.loadingView setHidden:NO];
	[self.loadingIndicator startAnimating];
	
	timeoutTimer = [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(timeout:) userInfo:nil repeats:NO];
}

- (void)timeout:(NSTimer *)aTimer
{
	// clear the receiving data thing
	[self setLocatingTramNumber:nil];
	[self.navigationItem.rightBarButtonItem setEnabled:YES];
    
    // if we've fired we don't want to keep the timeoutTimer reference anymore
    timeoutTimer = nil;
	
	// hide the loading
	[self.loadingIndicator stopAnimating];
	[self.loadingView setHidden:YES];
	
	// display a message
	// alert to say the tram doesn't exist
	UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"onboard-timeout", @"Could not contact tramTRACKER.")
															 delegate:self
													cancelButtonTitle:nil
											   destructiveButtonTitle:NSLocalizedString(@"onboard-error-okbutton", @"OK")
													otherButtonTitles:nil];
	[actionSheet showFromTabBar:self.navigationController.tabBarController.tabBar];
	[actionSheet release];
}

// found journey
- (void)setJourney:(JourneyStub *)aJourneyStub
{
	//NSLog(@"Journey Set.");
	// hide the loading
	[self.loadingIndicator stopAnimating];
	[self.loadingView setHidden:YES];
	
	// clear the timeout timer
	if (timeoutTimer != nil)
	{
		[timeoutTimer invalidate];
		timeoutTimer = nil;
	}
    
    Journey *journey = [Journey journeyForStub:aJourneyStub];
	
	// is this a valid result?
	if (journey == nil)
	{
		return;
	} else if (journey.tram == nil)
	{
		// alert to say the tram doesn't exist
		NSString *tramNumber = self.enterNumberTextField.text != nil ? self.enterNumberTextField.text : @"";
		NSString *message = [[NSString alloc] initWithFormat:NSLocalizedString(@"onboard-tramnotfound", "Tram Not Found message"), tramNumber];
		UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:message
																 delegate:self
														cancelButtonTitle:nil
												   destructiveButtonTitle:NSLocalizedString(@"onboard-error-okbutton", @"OK")
														otherButtonTitles:nil];
		[actionSheet showFromTabBar:self.navigationController.tabBarController.tabBar];
		[actionSheet release];
		[message release];
		
		return;
	} else if (journey.tram.route == nil && ![journey.tram isRouteZero])
	{
		// alert to say the is not on a public route
		NSString *tramNumber = self.enterNumberTextField.text != nil ? self.enterNumberTextField.text : @"";
		NSString *message = [[NSString alloc] initWithFormat:NSLocalizedString(@"onboard-route-unknown", "Tram Not Found message"), tramNumber];
		UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:message
																 delegate:self
														cancelButtonTitle:nil
												   destructiveButtonTitle:NSLocalizedString(@"onboard-error-okbutton", @"OK")
														otherButtonTitles:nil];
		[actionSheet showFromTabBar:self.navigationController.tabBarController.tabBar];
		[actionSheet release];
		[message release];
		return;
	} else if ([journey.tram isRouteZero])
	{
		// alert to say the tram doesn't exist
		NSString *message = [[NSString alloc] initWithFormat:NSLocalizedString(@"onboard-tramreturntodepot", "Tram returning to the depotd"), [NSString stringWithFormat:@"Tram %@", self.enterNumberTextField.text]];
		UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:message
																 delegate:self
														cancelButtonTitle:nil
												   destructiveButtonTitle:NSLocalizedString(@"onboard-error-okbutton", @"OK")
														otherButtonTitles:nil];
		[actionSheet showFromTabBar:self.navigationController.tabBarController.tabBar];
		[actionSheet release];
		[message release];
		return;
	}
	
	// is this for the wrong tram?
	if (![journey.tram.number isEqualToNumber:self.locatingTramNumber])
		return;

	[self.enterNumberTextField resignFirstResponder];
	[self.navigationItem.leftBarButtonItem setEnabled:NO];
	
	OnboardViewController *onboard = [[OnboardViewController alloc] init];
	[onboard setJourney:aJourneyStub];
	[self.navigationController pushViewController:onboard animated:YES];
	[onboard release];
}

- (void)pidsServiceDidFailWithError:(NSError *)error
{
	[self.loadingIndicator stopAnimating];
	[self.loadingView setHidden:YES];
	
	
	// is the onboard explicitly unavailable?
	if ([error code] == PIDServiceErrorOnboardNotReachable)
	{
		[unavailableLabel setText:[error localizedDescription]];
		[unavailableLabel setHidden:NO];
		return;
	}

	NSString *errorMessage = [error localizedRecoverySuggestion];
	if (errorMessage == nil || [errorMessage length] == 0)
		errorMessage = [error localizedDescription];


	UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:errorMessage
															 delegate:self
													cancelButtonTitle:nil
											   destructiveButtonTitle:NSLocalizedString(@"onboard-error-okbutton", @"OK")
													otherButtonTitles:nil];
	[actionSheet showFromTabBar:self.navigationController.tabBarController.tabBar];
	[actionSheet release];
	return;
}

- (void)detectedTram:(Tram *)tram
{
	// check to see whether we're the top view controller here, if we're not then we're already tracking something
	if (![[self.navigationController topViewController] isEqual:self])
		return;
	
	[self setStoredTramNumber:[tram number]];

	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"tram-detected-title", @"Tram Detected")
													message:[NSString stringWithFormat:NSLocalizedString(@"tram-detected-message", nil), [tram name]]
												   delegate:self
										  cancelButtonTitle:NSLocalizedString(@"tram-detected-cancel", nil)
										  otherButtonTitles:NSLocalizedString(@"tram-detected-ok", nil), nil];
	[alert show];
	[alert release];
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == -1 || buttonIndex == alertView.cancelButtonIndex)
	{
		[self setStoredTramNumber:nil];
		return;
	}
	
	// is there a pid sitting on top of whatever is currently being displayed?
	UITabBarController *tab = self.navigationController.tabBarController;
	UIViewController *selectedTab = [tab selectedViewController];
	if ([selectedTab isKindOfClass:[UINavigationController class]])
	{
		UINavigationController *nav = (UINavigationController *)selectedTab;
		UIViewController *top = [nav topViewController];
		if ([top isKindOfClass:NSClassFromString(@"PIDViewController")] || [top isKindOfClass:NSClassFromString(@"PagedPIDViewController")])
		{
			[nav popViewControllerAnimated:NO];
			[nav.navigationBar setTranslucent:NO];
			[nav setNavigationBarHidden:NO animated:NO];
		}

	}

	// make ourselves the active tab.
	[tab setSelectedViewController:self.navigationController];
}


- (void)viewWillDisappear:(BOOL)animated
{
	if (timeoutTimer != nil)
	{
		[timeoutTimer invalidate];
		timeoutTimer = nil;
	}
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[service release];
	[enterNumberTextField release];
	[storedTramNumber release];
	[unavailableLabel release];

    [super dealloc];
}


@end
