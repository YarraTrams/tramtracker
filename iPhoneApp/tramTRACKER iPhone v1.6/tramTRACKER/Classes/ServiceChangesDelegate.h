//
//  ServiceChangesDelegate.h
//  tramTRACKER
//
//  Created by Robert Amos on 20/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServiceChange.h"


@interface ServiceChangesDelegate : NSObject <NSXMLParserDelegate> {
	id delegate;
	SEL action;
	NSAutoreleasePool *pool;
	NSMutableString *responseText;
	ServiceChange *currentServiceChange;
	NSString *currentProperty;
	NSMutableArray *serviceChangesArray;
}

@property (nonatomic, assign) id delegate;
@property (nonatomic, assign) SEL action;
@property (readonly, nonatomic) NSAutoreleasePool *pool;
@property (nonatomic, retain) NSMutableString *responseText;
@property (nonatomic, retain) ServiceChange *currentServiceChange;
@property (nonatomic, retain) NSString *currentProperty;
@property (nonatomic, retain) NSMutableArray *serviceChangesArray;

- (id)initWithDelegate:(id)aDelegate action:(SEL)anAction;

/**
 * A NSURLConnectionDelegate method. Called when the connection encountered an error.
 **/
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error;

/**
 * A NSURLConnectionDelegate method. Called when we did receive response headers from the PIDS Service.
 *
 * Prepares to receive data from the server
 **/
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response;

/**
 * A NSURLConnectionDelegate method. Called when we have received a chunk of data from the service.
 **/
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data;

/**
 * A NSURLConnectionDelegate method. Called when we have received the entire response from the server.
 * Calls -parseResponse to begin parsing the PIDS Service's response.
 **/
- (void)connectionDidFinishLoading:(NSURLConnection *)connection;


/**
 * Creates and starts an XMLParser to parse the string response that we received from the server.
 **/
- (void)parseResponse:(NSString *)response;

/**
 * A NSXMLParserDelegate method. The Parser encountered the start of an element. We create the appropriate object
 * to represent that element in one of the "current" properties.
 **/
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributes;

/**
 * A NSXMLParserDelegate method. The Parser encountered the end of an element. We close off that element, copy it to the appropriate
 * location and dispatch it to the delegate if necessary.
 **/
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName;

/**
 * A NSXMLParserDelegate method. The Parser encountered some text between elements, copy it to the currentProperty if necessary.
 **/
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string;

/**
 * Aborts parsing of the document
 **/
- (void)abortParsing:(NSXMLParser *)parser;

- (void)failWithError:(NSError *)error;
- (void)drain;

@end
