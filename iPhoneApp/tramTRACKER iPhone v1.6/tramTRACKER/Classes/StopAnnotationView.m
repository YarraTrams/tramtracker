//
//  StopAnnotationView.m
//  tramTRACKER
//
//  Created by Robert Amos on 19/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "StopAnnotationView.h"

@implementation StopAnnotationView

@synthesize purplePin, shouldShowRealTimeAccessory, shouldShowScheduledAccessory;

/**
 * Override the highlighting stuff
**/
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
	// change our pin colour depending on what sort of stop we are
	if ([self isPurplePin])
		[self setPinColor:MKPinAnnotationColorPurple];
	else
		[self setPinColor:MKPinAnnotationColorGreen];
}

/**
 * Return a real-time button
 **/
- (UIView *)rightCalloutAccessoryView
{
	// show a real time button?
	if ([self shouldShowRealTimeAccessory])
	{
		// make a button that calls stuff
		UIButton *realTimeDisclosureButton = [UIButton buttonWithType:UIButtonTypeCustom];
		UIImage *pidIcon = [UIImage imageNamed:@"PIDIcon.png"];
		[realTimeDisclosureButton setImage:pidIcon forState:UIControlStateNormal];
		[realTimeDisclosureButton setFrame:CGRectMake(0, 0, pidIcon.size.width, pidIcon.size.height)];
		
		// configure the target to call ourselves
		[realTimeDisclosureButton addTarget:self action:@selector(didTouchRealTimeCalloutAccessory:) forControlEvents:UIControlEventTouchUpInside];
		
		return realTimeDisclosureButton;
	}
	
	// nope
	return nil;
}

/**
 * Return a scheduled
**/
- (UIView *)leftCalloutAccessoryView
{
	// show a scheduled button?
	if ([self shouldShowScheduledAccessory])
	{
		// make a button that calls stuff
		UIButton *scheduledDisclosureButton = [UIButton buttonWithType:UIButtonTypeCustom];
		UIImage *scheduledIcon = [UIImage imageNamed:@"ScheduledIcon.png"];
		[scheduledDisclosureButton setImage:scheduledIcon forState:UIControlStateNormal];
		[scheduledDisclosureButton setFrame:CGRectMake(0, 0, scheduledIcon.size.width, scheduledIcon.size.height)];
		
		// configure the target to call ourselves
		[scheduledDisclosureButton addTarget:self action:@selector(didTouchScheduledCalloutAccessory:) forControlEvents:UIControlEventTouchUpInside];
		
		return scheduledDisclosureButton;
	}
	
	// nope
	return nil;
}

/**
 * Called when the user touches our callout stuff
**/
- (void)didTouchRealTimeCalloutAccessory:(id)sender
{
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	
	// send a notification that we clickied the stop
	[nc postNotificationName:@"TTDidTouchRealTimeCalloutAccessory" object:self.annotation];
}
- (void)didTouchScheduledCalloutAccessory:(id)sender
{
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	
	// send a notification that we clickied the stop
	[nc postNotificationName:@"TTDidTouchScheduledCalloutAccessory" object:self.annotation];
}

@end
