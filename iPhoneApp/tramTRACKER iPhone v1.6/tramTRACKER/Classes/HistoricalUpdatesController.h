//
//  HistoricalUpdatesController.h
//  tramTRACKER
//
//  Created by Robert Amos on 5/08/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface HistoricalUpdatesController : UITableViewController <UIActionSheetDelegate> {
	NSArray *historicalUpdates;
	NSArray *sectionTitles;
}

- (id)initHistoricalUpdatesController;
- (UIView *)tableFooterView;

- (void)_setHistoricalUpdates;

- (void)clearHistory;


@end
