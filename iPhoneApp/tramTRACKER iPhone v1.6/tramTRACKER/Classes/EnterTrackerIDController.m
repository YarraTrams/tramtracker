//
//  EnterTrackerIDController.m
//  tramTRACKER
//
//  Created by Robert Amos on 6/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "EnterTrackerIDController.h"


@implementation EnterTrackerIDController

@synthesize enterIDTextField;


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	// Add a cancel button, disabled
	UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelEditing:)];
	self.navigationItem.rightBarButtonItem = cancelButton;
	[cancelButton release];
}

// always be cancelled when we appear
- (void)viewDidAppear:(BOOL)animated
{
	[self.navigationItem.rightBarButtonItem setEnabled:NO];

	[self.navigationController setNavigationBarHidden:YES animated:NO];
	[self.navigationController setNavigationBarHidden:NO animated:NO];
}


//
// Start Editing the tracker ID
//
- (IBAction)didStartEditing:(id)sender
{
	// enable the cancel button
	[self.navigationItem.rightBarButtonItem setEnabled:YES];
}

//
// Cancel the editing
//
- (void)cancelEditing:(id)sender
{
	// dismiss the keyboard
	[self.enterIDTextField resignFirstResponder];

	// disable the cancel button again
	[self.navigationItem.rightBarButtonItem setEnabled:NO];
}

//
// When we're entering text watch for a 4 digit trackerID
//
- (IBAction)enterIDTextDidChange:(id)sender
{
	UITextField *field = (UITextField *)sender;


	// if they've entered 4 digits check for a stop
	if ([field.text length] == 4)
	{
		// find the stop
		Stop *stop = [[StopList sharedManager] getStopForTrackerID:[NSNumber numberWithInt:[field.text intValue]]];
		if (stop == nil || [field.text integerValue] >= 8000)
		{
			// alert to say the stop doesn't exist
			NSString *message = [[NSString alloc] initWithFormat:NSLocalizedString(@"entertrackerid-stopnotfound", "Stop Not Found Message"), field.text];
			UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:message
																	 delegate:self
															cancelButtonTitle:NSLocalizedString(@"entertrackerid-error-okbutton", "OK")
													   destructiveButtonTitle:nil
															otherButtonTitles:nil];
			[actionSheet showFromTabBar:self.navigationController.tabBarController.tabBar];
			[actionSheet release];
			[message release];
			return;
		}

		// make a PID with that value
		[field resignFirstResponder];
		[self pushPIDForStop:stop];
	}
}

//
// Push on a PID for that stop
//
- (void)pushPIDForStop:(Stop *)stop
{
	// Start the pid view
	PIDViewController *PID = nil;
    if ([[UIScreen mainScreen] bounds].size.height == 568) {
        PID = [[PIDViewController alloc] initWithNibName:@"PIDView-iPhone5" bundle:[NSBundle mainBundle]];
    }
    else{
        PID = [[PIDViewController alloc] initWithNibName:@"PIDView" bundle:[NSBundle mainBundle]];
    }
	
	// get the stop info
	[PID startWithStop:stop];
	
	// push it onto the stack
	[self.navigationController pushViewController:PID animated:YES];
	[PID release];
}

#pragma mark UITextFieldDelegate methods

//
// When they press a button we do stuff
//
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];

	// A tracker ID can only be 4 digits, if its longer than that do nothing
	if ([newString length] > 4)
		return NO;
	return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}

- (void)dealloc {
	[enterIDTextField release];
	
    [super dealloc];
}


@end
