//
//  TurnDeleteOperation.h
//  tramTRACKER
//
//  Created by Robert Amos on 18/12/2010.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>


@class Turn;
@class BackgroundSynchroniser;

/**
 * A delete operation for a turn
 *
 * @ingroup Sync
 **/
@interface TurnDeleteOperation : NSOperation {
	
	/**
	 * The turn to be deleted
	 **/
	Turn *turn;
	
	/**
	 * The BackgroundSynchroniser object that is managing this update operation
	 **/
	BackgroundSynchroniser *syncManager;
	
	/**
	 * Status - YES if the operation is executing, NO otherwise
	 **/
	BOOL executing;
	
	/**
	 * Status - YES if the operation was finished (or cancelled), NO otherwise
	 **/
	BOOL finished;
}

@property (nonatomic, readonly) Turn *turn;
@property (nonatomic, assign) BackgroundSynchroniser *syncManager;

/**
 * Initialises a turn delete operation for the specified turn.
 *
 * @param	aTurn			The turn to be deleted.
 * @return					An initialised StopDeleteOperation object
 **/
- (id)initWithTurn:(Turn *)aTurn;

/**
 * Required by the NSOperationQueue, whether to execute this operation concurrently.
 **/
- (BOOL)isConcurrent;

/**
 * NSOperationQueue wrapper for the executing ivar.
 **/
- (BOOL)isExecuting;

/**
 * NSOperationQueue wrapper for the finished ivar.
 **/
- (BOOL)isFinished;

/**
 * Starts the Turn Delete Operation
 **/
- (void)start;

/**
 * Finishes the Update operation and handles notifications
 **/
- (void)finish;

/**
 * Deletes the turn from the current context
 **/
- (void)deleteTurn;

@end

