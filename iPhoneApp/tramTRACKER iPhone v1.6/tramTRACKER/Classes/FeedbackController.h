//
//  FeedbackController.h
//  tramTRACKER
//
//  Created by Robert Amos on 14/05/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

extern NSString * const feedbackPhone;
extern NSString * const feedbackEmail;

@interface FeedbackController : UIViewController <MFMailComposeViewControllerDelegate, UINavigationControllerDelegate> {
	UIButton *phoneButton;
	UIButton *emailButton;
}

@property (nonatomic, retain) IBOutlet UIButton *phoneButton;
@property (nonatomic, retain) IBOutlet UIButton *emailButton;

- (IBAction)callFeedback:(id)sender;
- (IBAction)emailFeedback:(id)sender;

@end
