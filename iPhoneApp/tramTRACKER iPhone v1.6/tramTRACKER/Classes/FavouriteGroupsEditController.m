//
//  FavouriteGroupsEditController.m
//  tramTRACKER
//
//  Created by Robert Amos on 21/07/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "FavouriteGroupsEditController.h"
#import "FavouriteGroupEditCell.h"


@implementation FavouriteGroupsEditController

- (id)initWithTarget:(id)aTarget action:(SEL)anAction
{
	if (self = [super initWithStyle:UITableViewStyleGrouped])
	{
		target = aTarget;
		action = anAction;
		[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
	}
	return self;
}

- (id)initWithGroupName:(NSString *)existingGroupName target:(id)aTarget action:(SEL)anAction;
{
	if (self = [self initWithTarget:aTarget action:anAction])
	{
		groupName = existingGroupName;
		[groupName retain];
	}
	return self;
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(textFieldShouldReturn:)];
	[self.navigationItem setRightBarButtonItem:doneButton animated:NO];
	[doneButton release];
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];

	FavouriteGroupEditCell *cell = (FavouriteGroupEditCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];

	// is it the first responder?
	if (![cell.textField isFirstResponder])
		[cell.textField becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
	// save things back to our delegate if we have one
	if (target != nil && [target respondsToSelector:action])
	{
		FavouriteGroupEditCell *cell = (FavouriteGroupEditCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
		
		// pass the text back, if we're editing don't do anything unless it has changed
		if (groupName != nil)
		{
			if (![groupName isEqualToString:cell.textField.text])
				[target performSelector:action withObject:groupName withObject:cell.textField.text];
		} else
		{
			// adding new group, don't do anything unless its not empty
			if (cell.textField.text != nil && ![cell.textField.text isEqualToString:@""])
				[target performSelector:action withObject:cell.textField.text];
		}
	}
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"GroupEditCell";
    
    FavouriteGroupEditCell *cell = (FavouriteGroupEditCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[FavouriteGroupEditCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		[cell.textField setDelegate:self];
    }
    
    // Set up the cell...
	if (groupName != nil)
		[cell.textField setText:groupName];

    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
	// AnotherViewController *anotherViewController = [[AnotherViewController alloc] initWithNibName:@"AnotherView" bundle:nil];
	// [self.navigationController pushViewController:anotherViewController];
	// [anotherViewController release];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark -
#pragma mark UITextFieldDelegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[self.navigationController popViewControllerAnimated:YES];
	return NO;
}

- (void)dealloc {
    [super dealloc];
}


@end

