//
//  EnterTrackerIDController.h
//  tramTRACKER
//
//  Created by Robert Amos on 6/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PIDViewController.h"
#import "Stop.h"
#import "StopList.h"
#import "OnboardListView.h"
#import "Journey.h"

/**
 * A custom UIViewController that displays a text box with instructional picture, allowing the user to enter a tracker ID directly
**/
@interface EnterTrackerIDController : UIViewController <UINavigationControllerDelegate, UITextFieldDelegate, UIActionSheetDelegate> {

	/**
	 * A reference to the text field displayed on the screen so that we can banish it's keyboard.
	**/
	UITextField *enterIDTextField;
}

@property (nonatomic, retain) IBOutlet UITextField *enterIDTextField;

/**
 * The user selected the tracker ID field, enable the Cancel button
**/
- (IBAction)didStartEditing:(id)sender;

/**
 * The user cancelled editing the tracker ID field, banish the keyboard
**/
- (void)cancelEditing:(id)sender;

/**
 * The user has entered a digit in the tracker ID field, check to see if we've reached 4
 * digits and have a valid tracker ID, calls -pushPIDForStop if so.
**/
- (IBAction)enterIDTextDidChange:(id)sender;

/**
 * Creates and pushes a PID onto the navigation controller's stack
 *
 * @param	stop		The stop to create a PID for
**/
- (void)pushPIDForStop:(Stop *)stop;


@end
