//
//  HelpTicketOutletsController.h
//  tramTRACKER
//
//  Created by Robert Amos on 8/08/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

extern const NSString *HelpURLSubdirTicketOutlets;

@interface HelpTicketOutletsController : HelpBaseController {

}

- (id)initHelpTicketOutletsController;

@end
