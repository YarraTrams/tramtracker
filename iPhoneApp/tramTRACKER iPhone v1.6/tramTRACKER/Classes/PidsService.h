//
//  PidsService.h
//  tramTRACKER
//
//  Created by Robert Amos on 18/02/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const PIDClientType;
extern NSString * const PIDServiceExpectedVersion;
extern NSString * const PIDServiceURI;
extern NSString * const PIDServiceNamespace;
extern NSInteger const PIDServiceErrorNotReachable;
extern NSInteger const PIDServiceValidationError;
extern NSInteger const PIDServiceValidationErrorStopNotFound;
extern NSInteger const PIDServiceValidationErrorRouteNotFound;
extern NSInteger const PIDServiceErrorOnboardNotReachable;
extern NSInteger const PIDServiceErrorTimeoutReached;

/**
 * A type of action as returned to the delegate in -setUpdates:
**/
typedef NSInteger PIDServiceActionType;

/**
 * An action requiring updating data
**/
extern PIDServiceActionType const PIDServiceActionTypeUpdate;

/**
 * An action requiring deleting data
**/
extern PIDServiceActionType const PIDServiceActionTypeDelete;

@class Route;
@class PidsServiceDelegate;
@class Stop;

/**
 * @defgroup Data Data Retrieval
**/

/**
 * @ingroup Data
**/

/**
 * Methods for calling the PIDS Service. Calls are asynchronous so you need to supply a delegate to receive the results.
**/
@interface PidsService : NSObject {
	
	/**
	 * The delegate that will receive the results from the service
	**/
	NSObject *delegate;
	
	/**
	 * The GUID that we use to identify ourself.
	**/
	NSString *guid;
	
	/**
	 * A temporary copy of the trackerID so that it can be returned to the delegate
	**/
	NSNumber *trackerID;
	
	/**
	 * A temporary copy of the route number so that it can be returned to the delegate
	**/
	NSString *routeNumber;
	
	/**
	 * Whether this is a real-time request or schdeuled request. Used to choose the appropriate error message
	**/
	BOOL isRealtimeRequest;

	/**
	 * Whether this request is already being performed in a background thread.
	**/
	BOOL backgroundedSelf;

	/**
	 * A temporary copy of the direction so that it can be returned to the delegate.
	**/
	NSNumber *upDirection;
}

// Same delegate
@property (nonatomic, retain) NSObject *delegate;

// And the guid
@property (nonatomic, retain) NSString *guid;
@property (nonatomic, retain) NSNumber *trackerID;
@property (nonatomic, retain) NSString *routeNumber;
@property (nonatomic, retain) NSNumber *upDirection;


/**
 * Clear the delegate that receives the results from the service
 * (Setting is taken care of with @synthesize), use -setDelegate
**/
- (void)clearDelegate;

/**
 * Get the GUID for a New Client
 *
 * Your delegate must implement -setNewClientGuid:(NSString *)newGuid;
**/
- (void)getNewClientGuid;

/**
 * Get the information for a stop
 *
 * Your delegate must implement -setInformation:(Stop *)stop forStop:(NSNumber *)trackerID;
 *
 * @param	stopID		The Tracker ID of a stop
**/
- (void)getInformationForStop:(int)stopID;

/**
 * Get a set of predictions for a stop
 *
 * Your delegate must implement -setPredictions:(NSArray *)predictions forStop:(NSNumber *)trackerID;
 *
 * @param	stopID		The Tracker ID of a stop
**/
- (void)getPredictionsForStop:(int)stopID;

/**
 * Get a set of predictions with a specific filter
 *
 * Your delegate must implement -setPredictions:(NSArray *)predictions forStop:(NSNumber *)trackerID;
 *
 * @param	stopID			The tracker ID of a stop
 * @param	route			A Route object to filter against or nil for all routes
 * @param	lowFloorOnly	YES to only show low floor trams, NO to show all trams
**/
- (void)getPredictionsForStop:(int)stopID route:(Route *)route lowFloorOnly:(BOOL)lowFloorOnly;

/**
 * Get a set of scheduled departures for a stop
 *
 * Your delegate must implement -setDepartures:(NSArray *)departures forStop:(NSNumber *)trackerID;
**/
- (void)getScheduledDeparturesForStop:(int)stopID routeNumber:(NSString *)routeNumber atDateTime:(NSDate *)date withLowFloorOnly:(BOOL)lowFloorOnly;

/**
 * Get information about a scheduled trip
 *
 * Your delegate must implement -setScheduledJourney:(JourneyStub *)journeyStub;
 *
**/
- (void)schedulesForTrip:(NSNumber *)trip atDateTime:(NSDate *)date;

/**
 * Get a list of the Routes and the destinations heading in either direction
 *
 * Your delegate must implement -setRouteList:(NSArray *)routeList;
**/
- (void)getRouteList;

/**
 * Get the destination endpoints for a route
 *
 * Your delegate must implement -setDestinations:(NSDictionary *)destinations forRouteNumber:(NSString *)routeNumber
 *
 * @param	route		A Route object to get results for.
**/
- (void)destinationsForRoute:(Route *)route;

/**
 * Get a list of stops for a route and direction
 *
 * your delegate must implement -setStopList:(NSArray *)stops forRoute:(Route *)route;
 *
 * @param	route			A Route object to get results for
 * @param	upDirection		The direction of travel relative to the up direction (YES for up, NO for down)
**/
- (void)getStopListForRoute:(Route *)route upDirection:(BOOL)upDirection;

/**
 * Get journey information for a tram number
 *
 * Your delegate must implement -setJourney:(Journey *)journey forTramNumber:(NSNumber *)tramNumber;
 *
**/
- (void)getJourneyForTramNumber:(NSNumber *)tramNumber;

/**
 * Get the routes through a stop
 *
 * Your delegate must implement -setRoutes:(NSArray *)routes throughStop:(NSNumber *)trackerID
 *
 * @param	stop			A Stop object
**/
- (void)routesThroughStop:(Stop *)stop;

/**
 * Get a List of changed stops and routes since a specified date and time
 *
 * Your delegate must implement -setUpdates:(NSArray *)updates atDate:(NSDate *)atDate;
 *
 * @param	date			A NSDate time to check for updates since
**/
- (void)updatesSinceDate:(NSDate *)date;

/**
 * Gets a SOAPHeader string to be sent to the server
**/
- (NSString *)getHeader;

/**
 * Call a web service method with no parameters.
 *
 * @param	method		The method to call
**/
- (void)callWebServiceMethod:(NSString *)method;

/**
 * Call a web service method with parameters.
 *
 * @param	method		The method to call
 * @param	params		The parameters to pass to the method
**/
- (void)callWebServiceMethod:(NSString *)method withParameters:(NSDictionary *)params;

/**
 * Call a web service method with a string request body
 *
 * @param	method		The method to call
 * @param	body		The request body to send to the server
**/
- (void)callWebServiceMethod:(NSString *)method withRequestBody:(NSString *)body;

/**
 * Send a failure message to the delegate
**/
- (void)failWithError:(NSError *)error;

/**
 * Check to make sure that we have access to the internet
**/
- (BOOL)hasAccessToService;


/**
 * Format a NSDate to how the web service expects it
 *
 * @param	date				The NSDate to format
**/
- (NSString *)formatDateForService:(NSDate *)date;

/**
 * Allows a web service method to be called from a performSelectorInBackgroundThread message
 *
 * @param	params				An NSDictionary with two keys. "method" and "body". See -callWebServiceMethod:withRequestBody:
**/
- (void)callWebServiceMethodWhileInBackground:(NSDictionary *)params;

@end
