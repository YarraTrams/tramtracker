//
//  ServiceChange.h
//  tramTRACKER
//
//  Created by Robert Amos on 20/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Describes a Service Change
**/
@interface ServiceChange : NSObject {

	/**
	 * The title of the change
	**/
	NSString *title;

	/**
	 * The body of the change
	**/
	NSString *content;

	/**
	 * A date string, if known
	**/
	NSString *dateString;
}

@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *content;
@property (nonatomic, retain) NSString *dateString;

@end
