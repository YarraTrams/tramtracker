/*
 *  BackgroundSynchroniserDelegate.h
 *  tramTRACKER
 *
 *  Created by Robert Amos on 4/08/09.
 *  Copyright 2009 Yarra Trams. All rights reserved.
 *
 */

@class Stop;
@class Route;

/**
 * @defgroup Sync Data Synchronisation
 **/


/**
 * The delegate of a BackgroundSynchroniser object must adopt the BackgroundSynchroniserDelegate protocol.
 * All methods are optional and describe the type of notications you wish to receive during the 
 * synchronisation process.
 *
 * @ingroup Sync
**/
@protocol BackgroundSynchroniserDelegate
@optional

/**
 * Tells the delegate that the synchronisation process (not the check for updates process) started.
**/
- (void)syncDidStart;

/**
 * Tells the delegate that the synchronisation process (not the check for updates process) finished.
**/
- (void)syncDidFinishAtTime:(NSDate *)finishTime;

/**
 * Tells the delegate that the changes applied to the local database are about to be saved to Core Data.
**/
- (void)syncDidStartSaving;

/**
 * Tells the delegate that the synchronisation process did start updating a stop.
 *
 * @param	stop			The stop that is about to be updated.
**/
- (void)syncDidStartStop:(Stop *)stop;

/**
 * Tells the delegate that the synchronisation process did finish updating a stop.
 *
 * @param	stop			The stop that was updated.
**/
- (void)syncDidFinishStop:(Stop *)stop;

/**
 * Tells the delegate that the synchronisation process is starting to updates the list of routes through a stop
 *
 * @param	stop			The stop that is about to have its route list updated.
**/
- (void)syncDidStartRoutesThroughStop:(Stop *)stop;

/**
 * Tells the delegate that the synchronisation process has finished updating the list of routes through a stop
 *
 * @param	stop			The stop that had its list of routes updated.
**/
- (void)syncDidFinishRoutesThroughStop:(Stop *)stop;

/**
 * Tells the delegate that the synchronisation process is about to start updating a route.
 *
 * @param	route			The route that is about to be updated.
**/
- (void)syncDidStartRoute:(Route *)route;

/**
 * Tells the delegate that the synchronisation process has finished updating a route.
 *
 * @param	route			The route that was updated.
**/
- (void)syncDidFinishRoute:(Route *)route;

/**
 * Tells the delegate that the synchronisation process added a new update to the list
 * of operations to be completed.
 *
 * @param	operation		The NSOperation that was added.
**/
- (void)syncDidAddOperation:(NSOperation *)operation;

/**
 * Tells the delegate that the check for updates process has started.
**/
- (void)checkForUpdatesDidStart;

/**
 * Tells the delegate that the check for updates process has finished, including details on the update found.
 *
 * @param	updatesFound	An NSDictionary with details about the updates found.
 *							The TTSyncKeyUpdates key has the total number of updates found.
 *							The TTSyncKeyRoutes key has the number of route updates found.
 *							The TTSyncKeyStops key has the number of stop updates found.
 *							The TTSyncKeyTickets is a bool showing whether ticket outlets have been updated.
**/
- (void)checkForUpdatesDidFinish:(NSDictionary *)updatesFound;

/**
 * Tells the delegate that the synchronisation process has started to delete a stop.
 *
 * @param	stop			The stop about to be deleted.
**/
- (void)deleteDidStartStop:(Stop *)stop;

/**
 * Tells the delegate that the synchronisation process has deleted a stop.
 *
 * @param	stop			The stop that was deleted. (It was deleted from the core data store but not yet saved. It will still exist in memory).
**/
- (void)deleteDidFinishStop:(Stop *)stop;

/**
 * Tells the delegate that the synchronisation process has started to delete a route.
 *
 * @param	route		The route about to be deleted.
**/
- (void)deleteDidStartRoute:(Route *)route;

/**
 * Tells the delegate that the synchronisation process has deleted a route.
 *
 * @param	route			The route that was deleted. (It was deleted from the core data store but has not yet been saved to it. The object will still exist in memory.)
**/
- (void)deleteDidFinishRoute:(Route *)route;

/**
 * Tells the delegate that the synchronisation process has failed because of an error.
 *
 * @param	error			A NSError object describing the reason for the error
**/
- (void)syncDidFailWithError:(NSError *)error;

/**
 * Tells the delegate that the synchronisation process was cancelled.
**/
- (void)syncWasCancelled;

/**
 * Tells the delegate that the synchronisation was aborted due to low memory
**/
- (void)syncWasAbortedDueToMemoryWarning;

@end