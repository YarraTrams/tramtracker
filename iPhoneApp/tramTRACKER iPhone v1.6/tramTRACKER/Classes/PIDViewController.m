//
//  PIDViewController.m
//  tramTRACKER
//
//  Created by Robert Amos on 17/02/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "PIDViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "FavouriteStop.h"
#import "VoiceOverButton.h"
#import "OnboardViewController.h"
#import "NearbyTicketRetailersList.h"

#define NEAREST_STOP_THRESHOLD_200M 4
#define NEAREST_STOP_THRESHOLD_500M 8
#define NEAREST_STOP_THRESHOLD_MAX 12

int const PIDDefaultStop = 3013;

const PIDActionSheetAction PIDActionSheetActionAddToFavourites = 1;
const PIDActionSheetAction PIDActionSheetActionEdit = 2;
const PIDActionSheetAction PIDActionSheetActionScheduledDepartures = 3;
const PIDActionSheetAction PIDActionSheetActionSaveFilter = 4;
const PIDActionSheetAction PIDActionSheetActionClearFilter = 5;
const PIDActionSheetAction PIDActionSheetActionStopMap = 6;
const PIDActionSheetAction PIDActionSheetActionNearbyTicketRetailers = 7;
const PIDActionSheetAction PIDActionSheetActionDirections = 8;

@implementation PIDViewController

//
// Labels that describe the current stop
//
@synthesize stopTitle, stopSubtitle, stopTrackerID, stopID;

//
// Subviews within the NIB, controls what is shown on the PID screen
//
@synthesize screen, loading, disruptedScreen, disruptedMessage, specialEventScreen, specialEventMessage;

//
// The first row of predictions on the screen
//
@synthesize screenRow1Route, screenRow1Destination, screenRow1Arrival, screenRow1LowFloor, screenRow1AirConditioned, screenRow1Disrupted, screenRow1SpecialEvent, screenRow1VoiceOver, screenRow1PIDToOnboardIcon;

//
// The second row of predictions on the screen
//
@synthesize screenRow2Route, screenRow2Destination, screenRow2Arrival, screenRow2LowFloor, screenRow2AirConditioned, screenRow2Disrupted, screenRow2SpecialEvent, screenRow2VoiceOver, screenRow2PIDToOnboardIcon;

//
// The third row of predictions on the screen
//
@synthesize screenRow3Route, screenRow3Destination, screenRow3Arrival, screenRow3LowFloor, screenRow3AirConditioned, screenRow3Disrupted, screenRow3SpecialEvent, screenRow3VoiceOver, screenRow3PIDToOnboardIcon;

//
// The bottom row with the clock and status indicators
//
@synthesize clock, indicatorPlaying, indicatorWarning, indicatorPaused;

//
// The timers used to control the clock and page refreshing
//
@synthesize timerClock, timerPage;

//
// The information that we are displaying
//
@synthesize currentPredictions, currentStop;

@synthesize filterLabel, routeFilter, routeFilterLabel, lowFloorFilter, parent, swipeCapture, welcomeScreen;

@synthesize service, timerRefresh, trackerID, locationManager, hasBeenStopped, messages, filter, showMessages, actionSheet, actionButtons;
@synthesize errorScreen, errorMessage, editing, locatingStarted, locating, locatingBG;
@synthesize screenRow1Silhouette, screenRow2Silhouette, screenRow3Silhouette, favouriteStopIndexPath, mr2Logo, mr3Logo;

// Initing Stuff
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		
		// setup the pids service
		PidsService *pid = [[PidsService alloc] init];
		[self setService:pid];
		[self.service setDelegate:self];
		[pid release];
		[self setHasBeenStopped:NO];
		
		[self setHidesBottomBarWhenPushed:YES];
		
		// set the first message
		currentMessage = 0;
		
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		showMessages = [defaults integerForKey:@"showMessagesOnPID"];
		shouldShowSilhouettes = [defaults boolForKey:@"showTramProfiles"];
    }
    return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

	// show the correct logo
	if ([[NSDate dateWithTimeIntervalSince1970:1259425800] timeIntervalSinceNow] < 0)
		[self.mr3Logo setHidden:NO];
	else
		[self.mr2Logo setHidden:NO];
	
	// show the loading
	[self showLoading];
	
	// make sure the swipe control is on top
	[self.view bringSubviewToFront:(UIView *)self.swipeCapture];
	[self.view bringSubviewToFront:self.screenRow1VoiceOver];
	[self.view bringSubviewToFront:self.screenRow2VoiceOver];
	[self.view bringSubviewToFront:self.screenRow3VoiceOver];
}

//
// When we're ready to appear we get rid of the navigation bar
//
- (void)viewWillAppear:(BOOL)animated
{
	// and change the navigation bar over
	[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque];

	if (editing) return;
	
	[super viewWillAppear:animated];
	
	// and fade the bars out
	[self hideNavigation:animated];
}

//
// Once the view has appeared hide the navigation bar
//
- (void)viewDidAppear:(BOOL)animated {

	if (editing)
	{
		editing = FALSE;
		return;
	}
	
    [super viewDidAppear:animated];
	
	// do we have a current stop stored?
	if (currentStop)
		[self updateDisplayedStopInformation];
	
	// if we're on a navigation controller then we need to make it transparent
	if (self.navigationController)
	{
		if ([self.navigationController.navigationBar respondsToSelector:@selector(setTranslucent:)])
			[self.navigationController.navigationBar setTranslucent:YES];
		else
			[self.navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
	}

	// if this is the first time that we've done this then show the welcome screen
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	BOOL isFirstLoad = ![defaults boolForKey:@"firstFavouritePID"] && [[StopList sharedManager] hasFavouriteStopList] && self.parent != nil;

	if (isFirstLoad)
	{
		[self.view insertSubview:self.welcomeScreen aboveSubview:self.locating];
		[self.welcomeScreen setHidden:NO];
		[NSTimer scheduledTimerWithTimeInterval:10
										 target:self
									   selector:@selector(hideWelcome:)
									   userInfo:nil
										repeats:NO];
		[defaults setBool:YES forKey:@"firstFavouritePID"];
		[defaults synchronize];
	}
	
	// if we're a a favourite stop then grab our filter from the saved filter list
	if (self.parent != nil && self.filter == nil)
		[self setSavedFilter];
	
	// make sure our filtering options are shown
	if (self.filter != nil)
		[self setFilter:self.filter];

}

//
// Toggle whether the navigation bar is hidden
//
- (IBAction) toggleNavigation:(id)sender
{
	if (self.parent)
	{
		if ([self.parent.navigationController isNavigationBarHidden])
			[self showNavigation:YES];
		else
			[self hideNavigation:YES];
	} else
	{
		if ([self.navigationController isNavigationBarHidden])
			[self showNavigation:YES];
		else
			[self hideNavigation:YES];
	}
}

//
// Flip over to the list view if we have one
//
- (IBAction)flipToListView:(id)sender
{
	// if we dont have a stop yet then don't do anything
	if (!self.currentStop)
		return;
	
	[self hideNavigation:YES];
    
    // lets pause!
    [self pause];
	
	UINavigationController *curnav = (self.parent == nil ? self.navigationController : self.parent.navigationController);
	
	if (self.parent != nil)
		[self.parent setEditing:YES];
	else
		[self setEditing:YES];

	PredictionListView *listView = [[PredictionListView alloc] initWithStyle:UITableViewStyleGrouped
																		stop:self.currentStop
																	  filter:self.filter
															showMessagesType:self.showMessages
                                                                    delegate:self];

	UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:listView];
	[nav.navigationBar setTintColor:curnav.navigationBar.tintColor];
    [curnav presentViewController:nav animated:YES completion:NULL];
	[listView release];
	[nav release];
}

//
// Hide the navigation
//
- (void)hideNavigation:(BOOL)animated
{
	if (self.parent)
		[self.parent.navigationController setNavigationBarHidden:YES animated:animated];
	else
		[self.navigationController setNavigationBarHidden:YES animated:animated]; 
}

//
// Show the navigation
//
- (void)showNavigation:(BOOL)animated
{
	if (self.parent)
		[self.parent.navigationController setNavigationBarHidden:NO animated:animated];
	else
		[self.navigationController setNavigationBarHidden:NO animated:animated];
}


//
// Add the currently shown stop to the favourites
//
- (void) addToFavourites:(id)sender
{
	// add the current tracker id to the favourites list
	FavouriteStop *fav = [[[StopList sharedManager] addFavouriteStopID:[currentStop trackerID] inSection:NSNotFound] retain];
	
	// did this stop have a filter?
    //NSLog(@"[PID] New fav: %@, has indexPath: %@", fav, fav.indexPath);
	if (fav != nil && self.filter != nil && ![self.filter isEmpty])
		[[StopList sharedManager] setFilter:self.filter forFavouriteAtIndexPath:fav.indexPath];
	
	// reupdate the contact info to remove the add button
	//[self updateDisplayedStopInformation];
	[fav release];
}




//
// Show the Loading indicator on the PID. Does not hide the screen view.
//
- (void)showLoading
{
	[[self loading] startAnimating];
	[[self loading] setHidden:NO];
	
	// check to see if we have a current stop, if so then display the stop info
	[self clearPage];
	[self setCurrentPredictions:nil];
	if (self.currentStop != nil)
		[self updateDisplayedStopInformation];
}

//
// Hide the Loading indicator on the PID. Does not show the screen view.
//
- (void)hideLoading
{
	[[self loading] setHidden:YES];
	[[self loading] stopAnimating];
}

- (void)showLocating
{
	// load up the image view with our animations
	[self.locating setAnimationImages:[NSArray arrayWithObjects:[UIImage imageNamed:@"radar-frame1.png"], [UIImage imageNamed:@"radar-frame2.png"],
									   [UIImage imageNamed:@"radar-frame3.png"], [UIImage imageNamed:@"radar-frame4.png"],
									   [UIImage imageNamed:@"radar-frame5.png"], [UIImage imageNamed:@"radar-frame6.png"],
									   [UIImage imageNamed:@"radar-frame7.png"], [UIImage imageNamed:@"radar-frame8.png"],
									   [UIImage imageNamed:@"radar-frame9.png"], [UIImage imageNamed:@"radar-frame10.png"],
									   [UIImage imageNamed:@"radar-frame11.png"], [UIImage imageNamed:@"radar-frame12.png"],
									   [UIImage imageNamed:@"radar-frame13.png"], [UIImage imageNamed:@"radar-frame14.png"],
									   [UIImage imageNamed:@"radar-frame15.png"], [UIImage imageNamed:@"radar-frame16.png"],
									   [UIImage imageNamed:@"radar-frame17.png"], [UIImage imageNamed:@"radar-frame18.png"],
									   [UIImage imageNamed:@"radar-frame19.png"], [UIImage imageNamed:@"radar-frame20.png"],
									   [UIImage imageNamed:@"radar-frame21.png"], [UIImage imageNamed:@"radar-frame22.png"],
									   [UIImage imageNamed:@"radar-frame23.png"], [UIImage imageNamed:@"radar-frame24.png"],
									   [UIImage imageNamed:@"radar-frame25.png"], [UIImage imageNamed:@"radar-frame26.png"],
									   [UIImage imageNamed:@"radar-frame27.png"], [UIImage imageNamed:@"radar-frame28.png"],
									   [UIImage imageNamed:@"radar-frame29.png"], [UIImage imageNamed:@"radar-frame30.png"],
									   nil]];
	[self.locating setAnimationDuration:2];
	[self.locating startAnimating];
	[self.locating setHidden:NO];
	[self.locatingBG setHidden:NO];
}

- (void)hideLocating
{
	[self.locatingBG setHidden:YES];
	[self.locating setHidden:YES];
	[self.locating stopAnimating];
	[self.locating setAnimationImages:nil];
}

//
// Show the "screen" subview. The screen contains prediction information.
//
- (void)showScreen
{
	// if we're here and the title isn't displayed then show it
	if ([self.stopTitle isHidden]) [self updateDisplayedStopInformation];
	if (![self.errorScreen isHidden]) [self.errorScreen setHidden:YES];

	
	// Start the timers
	if (!self.hasBeenStopped && (![self timerClock] || ![[self timerClock] isValid]))
	{
		[self updateClock];
		[self setTimerClock:[NSTimer scheduledTimerWithTimeInterval:1
															  target:self
															selector:@selector(updateClockByTimer:)
															userInfo:nil
															 repeats:YES]];
	}

	// start turning the pages
	if (!self.hasBeenStopped && (![self timerPage] || ![[self timerPage] isValid]))
	{
		currentPage = -1;
		[self clearPage];
		[self displayNextPageOfPredictions];
		[self setTimerPage:[NSTimer scheduledTimerWithTimeInterval:4
															 target:self
														   selector:@selector(displayNextPageOfPredictionsByTimer:)
														   userInfo:nil
															repeats:YES]];
	}
	
	// Reset the iteration loop counter
	iterationCount = 0;
	if ([self.currentPredictions count] > 0 || [self.messages count] > 0)
		[self showPlaying];

	if ([[self screen] isHidden]) [[self screen] setHidden:NO];
	
	// if we get to this point and the loading isn't hidden better hide it
	if (![self.loading isHidden]) [self hideLoading];

}


//
// Hide the "screen" subview. The screen contains prediction information.
//
- (void)hideScreen
{
	[[self screen] setHidden:YES];
	
	// stop the timers if they're running
	if ([self timerClock])
	{
		[[self timerClock] invalidate];
		[self setTimerClock:nil];
	}
	
	// stop the page timer if its running
	if ([self timerPage])
	{
		[[self timerPage] invalidate];
		[self setTimerPage:nil];
	}
}


//
// Update the clock in the bottom right hand corner of the screen
//
- (void)updateClock
{
	// get the current time
	NSDate *now = [NSDate date];
	
	// setup the formatter, we want the time to appear like "3:05:50 PM"
	NSDateFormatter *clockFormat = [[NSDateFormatter alloc] init];
	//[clockFormat setDateFormat:@"h:mm:ss a"]; -- use built in style to allow for use of 24 hour time setting
	[clockFormat setTimeStyle:NSDateFormatterMediumStyle];
	[clockFormat setDateStyle:NSDateFormatterNoStyle];
	
	// Update the clock label
	[[self clock] setText:[clockFormat stringFromDate:now]];
	[clockFormat release];
}
//
// Update the clock. This method is a wrapper used by the NSTimers
//
- (void)updateClockByTimer:(NSTimer *)timer
{
	[self updateClock];
}



//
// Displays a page worth of prediction results
//
- (void)displayPageOfPredictions:(int)page
{

	// get a page worth of predictions
	NSArray *results = [self getPageOfPredictions:page];
	
	// Clear the current predictions
	[self clearPage];
	
	// do we have the first result
	if ([results count] > 0)
	{
		Prediction *firstPrediction = [results objectAtIndex:0];

		// Update the Route Number
		[[self screenRow1Route] setText:[firstPrediction headboardRouteNumber]];
		[[self screenRow1Route] setHidden:NO];
		
		// Update the Destination
		CGFloat	fontSize = 20.0;
		CGFloat originalFontSize = 20.0;
		CGFloat minFontSize = 19.0;
		[self.screenRow1Destination setFont:[UIFont systemFontOfSize:originalFontSize]];
		[firstPrediction.destination sizeWithFont:[UIFont systemFontOfSize:originalFontSize]
									  minFontSize:minFontSize
								   actualFontSize:&fontSize
										 forWidth:self.screenRow1Destination.frame.size.width
									lineBreakMode:self.screenRow1Destination.lineBreakMode];
		// is the font size different?
		if (fontSize != originalFontSize)
			[self.screenRow1Destination setFont:[UIFont systemFontOfSize:fontSize]];

		[[self screenRow1Destination] setText:[firstPrediction destination]];
		[[self screenRow1Destination] setHidden:NO];
		
		// Set the silhouette
		CGRect dFrame = self.screenRow1Destination.frame;
		if (shouldShowSilhouettes == 1 && [firstPrediction.tram silhouette] != nil)
		{
			// if all four icons are present we need to show a clipped tram image
			if ([firstPrediction disrupted] && [firstPrediction hasSpecialEvent] && [firstPrediction lowFloor] &&
				[firstPrediction airConditioned] && [firstPrediction displayAirConditioning])
			{
				[self.screenRow1Silhouette setImage:[firstPrediction.tram clippedSilhouette]];
			} else {
				[self.screenRow1Silhouette setImage:[firstPrediction.tram silhouette]];
			}

			CGRect sFrame = self.screenRow1Silhouette.frame;
			sFrame.size.width = self.screenRow1Silhouette.image.size.width;
			[self.screenRow1Silhouette setFrame:sFrame];
			[self.screenRow1Silhouette setHidden:NO];
			dFrame.size.height = 53;
		} else
		{
			dFrame.size.height = 70;
		}
		[self.screenRow1Destination setFrame:dFrame];
		
		// Update the voice over text
		NSString *arrivalTime = [firstPrediction minutesUntilArrivalTime];
		[self.screenRow1VoiceOver setAccessibilityLabel:[NSString stringWithFormat:@"%@%@", [firstPrediction voiceOver], [PIDViewController voiceOverMinutesUntilArrivalTime:[firstPrediction predictedArrivalDateTime]]]];

		// if it says "now" and there is an associated tram number then we enable the hidden button
		if ([arrivalTime isEqualToString:NSLocalizedString(@"pid-now", nil)] && firstPrediction.tram != nil && [firstPrediction.tram.number integerValue] != 0)
		{
			[self.screenRow1VoiceOver setEnabled:YES];
			[self.screenRow1VoiceOver setPrediction:firstPrediction];
			[self.screenRow1PIDToOnboardIcon setHidden:NO];
		}

		// Update the Arrival Time
		[[self screenRow1Arrival] setText:arrivalTime];
		[[self screenRow1Arrival] setHidden:NO];
		
		
		// If our result is disrupted then those the right icon
		if ([firstPrediction disrupted])
			[[self screenRow1Disrupted] setHidden:NO];
		
		// if our result has a special event message then show that icon
		if ([firstPrediction hasSpecialEvent])
		{
			[self.screenRow1SpecialEvent setHidden:NO];
			
			// if it is also disrupted then we need to shove ourselves down
			if ([firstPrediction disrupted])
			{
				[self offsetIcon:self.screenRow1SpecialEvent fromIcon:self.screenRow1Disrupted];
				[self offsetIcon:self.screenRow1LowFloor fromIcon:self.screenRow1Disrupted];
				[self offsetIcon:self.screenRow1AirConditioned fromIcon:self.screenRow1Disrupted];
			}
		}
		
		// If the result is a low floor tram then show the icon
		if ([firstPrediction lowFloor])
		{
			[[self screenRow1LowFloor] setHidden:NO];
			
			// if it is also disrupted then we need to shove ourselves over to the left
			if ([firstPrediction disrupted] || [firstPrediction hasSpecialEvent])
			{
				[self offsetIcon:self.screenRow1LowFloor fromIcon:self.screenRow1SpecialEvent];
				[self offsetIcon:self.screenRow1AirConditioned fromIcon:self.screenRow1SpecialEvent];
			}
		}
		
		// If the result is an aircondited tram then show the icon
		if ([firstPrediction airConditioned] && [firstPrediction displayAirConditioning])
		{
			[[self screenRow1AirConditioned] setHidden:NO];
			
			// if it is also a low floor then we need to shove ourselves over so that we fit in together
			if ([firstPrediction lowFloor] || [firstPrediction disrupted] || [firstPrediction hasSpecialEvent])
				[self offsetIcon:self.screenRow1AirConditioned fromIcon:self.screenRow1LowFloor];
		}
	}

	// do we have the second result?
	if ([results count] > 1)
	{
		Prediction *secondPrediction = [results objectAtIndex:1];
        
		// Update the Route Number
		[[self screenRow2Route] setText:[secondPrediction headboardRouteNumber]];
		[[self screenRow2Route] setHidden:NO];
		
		// Update the Destination
		CGFloat	fontSize = 20.0;
		CGFloat originalFontSize = 20.0;
		CGFloat minFontSize = 19.0;
		[self.screenRow2Destination setFont:[UIFont systemFontOfSize:originalFontSize]];
		[secondPrediction.destination sizeWithFont:[UIFont systemFontOfSize:originalFontSize]
									   minFontSize:minFontSize
									actualFontSize:&fontSize
										  forWidth:self.screenRow2Destination.frame.size.width
									 lineBreakMode:self.screenRow2Destination.lineBreakMode];
		// is the font size different?
		if (fontSize != originalFontSize)
			[self.screenRow2Destination setFont:[UIFont systemFontOfSize:fontSize]];
		[[self screenRow2Destination] setText:[secondPrediction destination]];
		[[self screenRow2Destination] setHidden:NO];

		// Update the voice over text
		NSString *arrivalTime = [secondPrediction minutesUntilArrivalTime];
		[self.screenRow2VoiceOver setAccessibilityLabel:[NSString stringWithFormat:@"%@%@", [secondPrediction voiceOver], [PIDViewController voiceOverMinutesUntilArrivalTime:[secondPrediction predictedArrivalDateTime]]]];
		
		// if it says "now" and there is an associated tram number then we enable the hidden button
		if ([arrivalTime isEqualToString:NSLocalizedString(@"pid-now", nil)] && secondPrediction.tram != nil && [secondPrediction.tram.number integerValue] != 0)
		{
			[self.screenRow2VoiceOver setEnabled:YES];
			[self.screenRow2VoiceOver setPrediction:secondPrediction];
			[self.screenRow2PIDToOnboardIcon setHidden:NO];
		}
		
		// Update the Arrival Time
		[[self screenRow2Arrival] setText:arrivalTime];
		[[self screenRow2Arrival] setHidden:NO];
		
		// Set the silhouette
		CGRect dFrame = self.screenRow2Destination.frame;
		if (shouldShowSilhouettes == 1 && [secondPrediction.tram silhouette] != nil)
		{
			// if all four icons are present we need to show a clipped tram image
			if ([secondPrediction disrupted] && [secondPrediction hasSpecialEvent] && [secondPrediction lowFloor] &&
				[secondPrediction airConditioned] && [secondPrediction displayAirConditioning])
			{
				[self.screenRow2Silhouette setImage:[secondPrediction.tram clippedSilhouette]];
			} else {
				[self.screenRow2Silhouette setImage:[secondPrediction.tram silhouette]];
			}

			CGRect sFrame = self.screenRow2Silhouette.frame;
			sFrame.size.width = self.screenRow2Silhouette.image.size.width;
			[self.screenRow2Silhouette setFrame:sFrame];
			[self.screenRow2Silhouette setHidden:NO];
			dFrame.size.height = 53;
		} else
		{
			dFrame.size.height = 70;
		}
		[self.screenRow2Destination setFrame:dFrame];
		
		// If our result is disrupted then those the right icon
		if ([secondPrediction disrupted])
			[[self screenRow2Disrupted] setHidden:NO];
		
		// if our result has a special event message then show that icon
		if ([secondPrediction hasSpecialEvent])
		{
			[self.screenRow2SpecialEvent setHidden:NO];
			
			// if it is also disrupted then we need to shove ourselves down
			if ([secondPrediction disrupted])
			{
				[self offsetIcon:self.screenRow2SpecialEvent fromIcon:self.screenRow2Disrupted];
				[self offsetIcon:self.screenRow2LowFloor fromIcon:self.screenRow2SpecialEvent];
				[self offsetIcon:self.screenRow2AirConditioned fromIcon:self.screenRow2LowFloor];
			}
		}

		// If the result is a low floor tram then show the icon
		if ([secondPrediction lowFloor])
		{
			[[self screenRow2LowFloor] setHidden:NO];

			// if it is also disrupted then we need to shove ourselves over to the left
			if ([secondPrediction disrupted] || [secondPrediction hasSpecialEvent])
			{
				[self offsetIcon:self.screenRow2LowFloor fromIcon:self.screenRow2SpecialEvent];
				[self offsetIcon:self.screenRow2AirConditioned fromIcon:self.screenRow2SpecialEvent];
			}
		}

		// If the result is an aircondited tram then show the icon
		if ([secondPrediction airConditioned] && [secondPrediction displayAirConditioning])
		{
			[[self screenRow2AirConditioned] setHidden:NO];
			
			// if it is also a low floor then we need to shove ourselves over so that we fit in together
			if ([secondPrediction lowFloor] || [secondPrediction disrupted] || [secondPrediction hasSpecialEvent])
				[self offsetIcon:self.screenRow2AirConditioned fromIcon:self.screenRow2LowFloor];
		}
	}
	
	// do we have the third result?
	if ([results count] > 2)
	{
		Prediction *thirdPrediction = [results objectAtIndex:2];

		// Update the Route Number
		[[self screenRow3Route] setText:[thirdPrediction headboardRouteNumber]];
		[[self screenRow3Route] setHidden:NO];
		
		// Update the Destination
		CGFloat	fontSize = 20.0;
		CGFloat originalFontSize = 20.0;
		CGFloat minFontSize = 19.0;
		[self.screenRow3Destination setFont:[UIFont systemFontOfSize:originalFontSize]];
		[thirdPrediction.destination sizeWithFont:[UIFont systemFontOfSize:originalFontSize]
									  minFontSize:minFontSize
								   actualFontSize:&fontSize
										 forWidth:self.screenRow3Destination.frame.size.width
									lineBreakMode:self.screenRow3Destination.lineBreakMode];
		// is the font size different?
		if (fontSize != originalFontSize)
			[self.screenRow3Destination setFont:[UIFont systemFontOfSize:fontSize]];
		
		[[self screenRow3Destination] setText:[thirdPrediction destination]];
		[[self screenRow3Destination] setHidden:NO];

		// Update the voice over text
		NSString *arrivalTime = [thirdPrediction minutesUntilArrivalTime];
		[self.screenRow3VoiceOver setEnabled:NO];
		[self.screenRow3VoiceOver setAccessibilityLabel:[NSString stringWithFormat:@"%@%@", [thirdPrediction voiceOver], [PIDViewController voiceOverMinutesUntilArrivalTime:[thirdPrediction predictedArrivalDateTime]]]];

		// if it says "now" and there is an associated tram number then we enable the hidden button
		if ([arrivalTime isEqualToString:NSLocalizedString(@"pid-now", nil)] && thirdPrediction.tram != nil && [thirdPrediction.tram.number integerValue] != 0)
		{
			[self.screenRow3VoiceOver setEnabled:YES];
			[self.screenRow3VoiceOver setPrediction:thirdPrediction];
			[self.screenRow3PIDToOnboardIcon setHidden:NO];
		}

		// Update the Arrival Time
		[[self screenRow3Arrival] setText:arrivalTime];
		[[self screenRow3Arrival] setHidden:NO];
		
		// Set the silhouette
		CGRect dFrame = self.screenRow3Destination.frame;
		if (shouldShowSilhouettes == 1 && [thirdPrediction.tram silhouette] != nil)
		{
			// if all four icons are present we need to show a clipped tram image
			if ([thirdPrediction disrupted] && [thirdPrediction hasSpecialEvent] && [thirdPrediction lowFloor] &&
				[thirdPrediction airConditioned] && [thirdPrediction displayAirConditioning])
			{
				[self.screenRow3Silhouette setImage:[thirdPrediction.tram clippedSilhouette]];
			} else {
				[self.screenRow3Silhouette setImage:[thirdPrediction.tram silhouette]];
			}
			
			CGRect sFrame = self.screenRow3Silhouette.frame;
			sFrame.size.width = self.screenRow3Silhouette.image.size.width;
			[self.screenRow3Silhouette setFrame:sFrame];
			[self.screenRow3Silhouette setHidden:NO];
			dFrame.size.height = 53;
		} else
		{
			dFrame.size.height = 70;
		}
		[self.screenRow3Destination setFrame:dFrame];
		
		// If our result is disrupted then those the right icon
		if ([thirdPrediction disrupted])
			[[self screenRow3Disrupted] setHidden:NO];
		
		// if our result has a special event message then show that icon
		if ([thirdPrediction hasSpecialEvent])
		{
			[self.screenRow3SpecialEvent setHidden:NO];
			
			// if it is also disrupted then we need to shove ourselves down
			if ([thirdPrediction disrupted])
			{
				[self offsetIcon:self.screenRow3SpecialEvent fromIcon:self.screenRow3Disrupted];
				[self offsetIcon:self.screenRow3LowFloor fromIcon:self.screenRow3Disrupted];
				[self offsetIcon:self.screenRow3AirConditioned fromIcon:self.screenRow3Disrupted];
			}
		}

		// If the result is a low floor tram then show the icon
		if ([thirdPrediction lowFloor])
		{
			[[self screenRow3LowFloor] setHidden:NO];
			
			// if it is also disrupted then we need to shove ourselves over to the left
			if ([thirdPrediction disrupted] || [thirdPrediction hasSpecialEvent])
			{
				[self offsetIcon:self.screenRow3LowFloor fromIcon:self.screenRow3SpecialEvent];
				[self offsetIcon:self.screenRow3AirConditioned fromIcon:self.screenRow3SpecialEvent];
			}
		}
		
		// If the result is an aircondited tram then show the icon
		if ([thirdPrediction airConditioned] && [thirdPrediction displayAirConditioning])
		{
			[[self screenRow3AirConditioned] setHidden:NO];
			
			// if it is also a low floor then we need to shove ourselves over so that we fit in together
			if ([thirdPrediction lowFloor] || [thirdPrediction disrupted] || [thirdPrediction hasSpecialEvent])
				[self offsetIcon:self.screenRow3AirConditioned fromIcon:self.screenRow3LowFloor];
		}   
	}
}

//
// Offset an icon to the left of another icon
//
- (void)offsetIcon:(UIImageView *)icon fromIcon:(UIImageView *)fromIcon
{
	[icon setFrame:CGRectMake(fromIcon.frame.origin.x, (fromIcon.frame.origin.y + fromIcon.frame.size.height + 2), icon.frame.size.width, icon.frame.size.height)];
}

//
// Display the next message in the list
//
- (BOOL)displayNextMessage
{
	// wrap around if needed (and permitted)
	if (currentMessage+1 > [self.messages count])
	{
		messageCycleCount++;
		if ([self.currentPredictions count] > 0 && (showMessages == PIDShowMessagesOnce || showMessages == 0))
			return NO;
		else if ([self.currentPredictions count] > 0 && (showMessages == PIDShowMessagesTwice && messageCycleCount > 1))
			return NO;
		currentMessage = 0;
	}

	// display that message
	SpecialMessage *message = [self.messages objectAtIndex:currentMessage];
	if (message.type == SpecialMessageTypeDisruption)
	{
		[self.disruptedMessage setText:message.message];

		// fade it on
		[self.disruptedScreen setAlpha:0];
		[self.disruptedScreen setHidden:NO];
		[self.screen bringSubviewToFront:self.disruptedScreen];
		
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:0.5];
		
		[self.disruptedScreen setAlpha:1];

		[UIView commitAnimations];

	} else if(message.type == SpecialMessageTypeSpecialEvent)
	{
		[self.specialEventMessage setText:message.message];
		
		// fade it on
		[self.specialEventScreen setAlpha:0];
		[self.specialEventScreen setHidden:NO];
		[self.screen bringSubviewToFront:self.specialEventScreen];
		
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:0.5];
		
		[self.specialEventScreen setAlpha:1];
		
		[UIView commitAnimations];
	}
	currentMessage++;
	return YES;
}

//
// Hide any messages showing
//
- (void)hideMessages
{
	// fade it offf
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.5];
	
	if ([self.disruptedScreen alpha] != 0)
		[self.disruptedScreen setAlpha:0];
	if ([self.specialEventScreen alpha] != 0)
		[self.specialEventScreen setAlpha:0];
	
	[UIView commitAnimations];
}



//
// Display the next page of predictions
//
- (void)displayNextPageOfPredictions
{
	// if we have no results and a message just display that and that alone
	if ([self.currentPredictions count] == 1 && ![[self.currentPredictions objectAtIndex:0] tramTrackerAvailable] && [self.messages count] > 0)
	{
		
		if ([self.messages count] > 1 || (([self.disruptedScreen isHidden] || [self.disruptedScreen alpha] == 0) && ([self.specialEventScreen isHidden] || [self.specialEventScreen alpha] == 0)))
			[self displayNextMessage];
		return;
	}
	
	// are we currently looking at a message or a prediction?
	if (currentPage != -1 && ([self.disruptedScreen isHidden] || [self.disruptedScreen alpha] == 0) && ([self.specialEventScreen isHidden] || [self.specialEventScreen alpha] == 0))
	{
		// not looking at a message, show one if we have one (and they're enabled)
		if (self.messages && [self.messages count] > 0)
		{
			// we have messages, can we show them?
			if (showMessages == PIDShowMessagesAlways || (currentMessage+1 <= [self.messages count] && showMessages != PIDShowMessagesNone) || (messageCycleCount <= 1 && showMessages == PIDShowMessagesTwice))
			{
				if ([self displayNextMessage])
					return;
			}
		}
	}
	
	// hide any messages if we're showing them
	[self hideMessages];
	
	// determine our current page
	if ((currentPage+1)*3 >= [[self currentPredictions] count])
		currentPage = 0;		// gone past the end, reset to the start
	else
		currentPage++;			// move to the next page

	// display the current page of results
	[self displayPageOfPredictions:currentPage];
	
	
	//
	// Update the indicator lights based on the number of pages we've ticked over
	//
	iterationCount++;

	// More than 8 counts, past 30 seconds without an update, show warning
	if (iterationCount >= 8 && [self isPlaying])
		[self showWarning];

	// More than 30 seconds, so past 2 minutes without an update, show paused
	else if (iterationCount >= 30 && [self isWarning])
		[self showPaused];
}


//
// Display the next page of predictions. This is a wrapper called by NSTimer
//
- (void)displayNextPageOfPredictionsByTimer:(NSTimer *)aTimer
{
	[self displayNextPageOfPredictions];
}

//
// Clear the current page
//
- (void)clearPage
{
	// clear all the labels and indicators in each row
	[[self screenRow1Route] setHidden:YES];
	[[self screenRow1Destination] setHidden:YES];
	[[self screenRow1Arrival] setHidden:YES];
	[[self screenRow1Silhouette] setHidden:YES];
	[[self screenRow1Disrupted] setHidden:YES];
	[[self screenRow1LowFloor] setHidden:YES];
	[[self screenRow1LowFloor] setFrame:[self.screenRow1Disrupted frame]];
	[[self screenRow1AirConditioned] setHidden:YES];
	[[self screenRow1AirConditioned] setFrame:[self.screenRow1LowFloor frame]];
	[[self screenRow1SpecialEvent] setHidden:YES];
	[[self screenRow1SpecialEvent] setFrame:[self.screenRow1AirConditioned frame]];
	[self.screenRow1VoiceOver setEnabled:NO];
	[self.screenRow1VoiceOver setAccessibilityLabel:@""];
	[self.screenRow1PIDToOnboardIcon setHidden:YES];
	
	[[self screenRow2Route] setHidden:YES];
	[[self screenRow2Destination] setHidden:YES];
	[[self screenRow2Arrival] setHidden:YES];
	[[self screenRow2Silhouette] setHidden:YES];
	[[self screenRow2Disrupted] setHidden:YES];
	[[self screenRow2LowFloor] setHidden:YES];
	[[self screenRow2LowFloor] setFrame:[self.screenRow2Disrupted frame]];
	[[self screenRow2AirConditioned] setHidden:YES];
	[[self screenRow2AirConditioned] setFrame:[self.screenRow2LowFloor frame]];
	[[self screenRow2SpecialEvent] setHidden:YES];
	[[self screenRow2SpecialEvent] setFrame:[self.screenRow2AirConditioned frame]];
	[self.screenRow2VoiceOver setEnabled:NO];
	[self.screenRow2VoiceOver setAccessibilityLabel:@""];
	[self.screenRow2PIDToOnboardIcon setHidden:YES];
	
	[[self screenRow3Route] setHidden:YES];
	[[self screenRow3Destination] setHidden:YES];
	[[self screenRow3Arrival] setHidden:YES];
	[[self screenRow3Silhouette] setHidden:YES];
	[[self screenRow3Disrupted] setHidden:YES];
	[[self screenRow3LowFloor] setHidden:YES];
	[[self screenRow3LowFloor] setFrame:[self.screenRow3Disrupted frame]];
	[[self screenRow3AirConditioned] setHidden:YES];
	[[self screenRow3AirConditioned] setFrame:[self.screenRow3LowFloor frame]];
	[[self screenRow3SpecialEvent] setHidden:YES];
	[[self screenRow3SpecialEvent] setFrame:[self.screenRow3AirConditioned frame]];
	[self.screenRow3VoiceOver setEnabled:NO];
	[self.screenRow3VoiceOver setAccessibilityLabel:@""];
	[self.screenRow3PIDToOnboardIcon setHidden:YES];
}


//
// Show the playing indicator
//
- (void)showPlaying
{
	[[self indicatorPlaying] setHidden:NO];
	[[self indicatorWarning] setHidden:YES];
	[[self indicatorPaused] setHidden:YES];
}

//
// Show the warning indicator
//
- (void)showWarning
{
	[[self indicatorWarning] setHidden:NO];
	[[self indicatorPlaying] setHidden:YES];
	[[self indicatorPaused] setHidden:YES];
}


//
// Show the paused indicator
//
- (void)showPaused
{
	[[self indicatorPaused] setHidden:NO];
	[[self indicatorPlaying] setHidden:YES];
	[[self indicatorWarning] setHidden:YES];
}

//
// Is the current indicator set to playing?
//
- (BOOL)isPlaying
{
	return ![[self indicatorPlaying] isHidden];
}

//
// Is the current indicator set to warning?
//
- (BOOL)isWarning
{
	return ![[self indicatorWarning] isHidden];
}

//
// Is the current indicator set to paused?
//
- (BOOL)isPaused
{
	return ![[self indicatorPaused] isHidden];
}


//
// Update the displayed stop information
//
- (void)updateDisplayedStopInformation
{
	// Don't try to update it if we have no stop information
	NSAssert([self currentStop], @"No stop information stored");
	
	// We have to rebuild the title and subtitle information
	//
	// By default, the stop name looks like: Spring St & Bourke St
	// We move the second street name into the subtitle with the direction
	NSString *title = [NSString stringWithString:[[self currentStop] name]];
	NSString *subtitle = [NSString stringWithString:[[self currentStop] displayedCityDirection]];
	
	// If it has an ampersand (&) then it has a secondary street name
	if ([title rangeOfString:@"&"].location != NSNotFound)
	{
		// split it up around the ampersand
		NSArray *pieces = [title componentsSeparatedByString:@"&"];
		
		// take the first piece for the title
		title = [[pieces objectAtIndex:0]
				 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
		subtitle = [NSString stringWithFormat:@"%@ - %@",
					[[pieces objectAtIndex:1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]],
					[[self currentStop] displayedCityDirection]];
	}
	
	//
	// Update the stop information
	//
	if (self.parent)
		[self.parent setTitle:title];
	else
		[self setTitle:title];
	
	// set the action button
	[self showActionButton];
	
	// Update the title
	[[self stopTitle] setText:title];
	[[self stopTitle] setHidden:NO];
	
	//NSLog(@"Title is at %@, %@ (%@)", NSStringFromCGRect([self stopTitle].frame), [self.stopTitle isHidden] ? @"is hidden" : @"not hidden", self);
	
	// Update the subtitle
	[[self stopSubtitle] setText:subtitle];
	[[self stopSubtitle] setHidden:NO];
	
	// Update the tramTRACKER ID
	[[self stopTrackerID] setText:[[[self currentStop] trackerID] stringValue]];
	[[self stopTrackerID] setHidden:NO];
	
	// Update the Stop Number
	[[self stopID] setText:[[self currentStop] number]];
	[[self stopID] setHidden:NO];
}

- (void)showActionButton
{
	UINavigationItem *nav = (self.parent ? self.parent.navigationItem : self.navigationItem);
	UINavigationController *navController = (self.parent ? self.parent.navigationController : self.navigationController);

	// only if we don't already have one
	if (nav == nil || nav.rightBarButtonItem == nil)
	{
		UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(presentActionSheet)];
		[nav setRightBarButtonItem:button animated:![navController isNavigationBarHidden]];
		[button release];
	}
}

- (void)presentActionSheet
{
	// make the actions
	NSMutableArray *actions = [[NSMutableArray alloc] initWithCapacity:0];
	NSMutableArray *actionTitles = [[NSMutableArray alloc] initWithCapacity:0];
	
	// add to favourites button
	if (self.parent == nil)
	{
		[actions addObject:[NSNumber numberWithInteger:PIDActionSheetActionAddToFavourites]];
		[actionTitles addObject:NSLocalizedString(@"pid-actionsheet-addtofavourites", @"Add To Favourites")];

	
	// it already is a favourite - does it have an active filter?
	} else
	{
		Filter *f = (self.parent != nil ? [[StopList sharedManager] filterForFavouriteAtIndexPath:self.favouriteStopIndexPath] : nil);

		// allow them to clear the filter
		if ((f != nil && ![f isEmpty]) || (self.filter != nil && ![self.filter isEmpty]))
		{
			[actions addObject:[NSNumber numberWithInteger:PIDActionSheetActionClearFilter]];
			[actionTitles addObject:NSLocalizedString(@"pid-actionsheet-clearfilter", @"Clear Filter")];
		}
		
		// has the filter changed, allow them to save it
		if (self.filter != nil && ![self.filter isEmpty] && ![self.filter isEqualToFilter:f])
		{
			[actions addObject:[NSNumber numberWithInteger:PIDActionSheetActionSaveFilter]];
			[actionTitles addObject:NSLocalizedString(@"pid-actionsheet-savefilter", @"Save Filter")];
		}
	}
	
	// change the pid and shows schedules
	[actions addObject:[NSNumber numberWithInteger:PIDActionSheetActionEdit]];
	[actionTitles addObject:NSLocalizedString(@"pid-actionsheet-changepid", @"Change PID")];
	[actions addObject:[NSNumber numberWithInteger:PIDActionSheetActionScheduledDepartures]];
	[actionTitles addObject:NSLocalizedString(@"pid-actionsheet-scheduled", @"Scheduled Departures")];
	[actions addObject:[NSNumber numberWithInteger:PIDActionSheetActionStopMap]];
	[actionTitles addObject:NSLocalizedString(@"pid-actionsheet-map", @"Stop Map")];
	[actions addObject:[NSNumber numberWithInteger:PIDActionSheetActionNearbyTicketRetailers]];
	[actionTitles addObject:NSLocalizedString(@"pid-actionsheet-nearbytickets", @"Nearby Ticket Outlets")];
	[actions addObject:[NSNumber numberWithInteger:PIDActionSheetActionDirections]];
	[actionTitles addObject:NSLocalizedString(@"pid-actionsheet-directions", @"Directions To Stop")];

	UIActionSheet *sheet;
	
	// This is really bad looking, please fix this apple so my buttons are not reverse ordered (i want the cancel button at the bottom!)
	switch ([actions count])
	{
		case 2:
			sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"pid-actionsheet-cancel", @"Cancel") destructiveButtonTitle:nil
									   otherButtonTitles:[actionTitles objectAtIndex:0], [actionTitles objectAtIndex:1], nil];
			break;

		case 3:
			sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"pid-actionsheet-cancel", @"Cancel") destructiveButtonTitle:nil
									   otherButtonTitles:[actionTitles objectAtIndex:0], [actionTitles objectAtIndex:1],
										[actionTitles objectAtIndex:2], nil];
			break;

		case 4:
			sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"pid-actionsheet-cancel", @"Cancel") destructiveButtonTitle:nil
									   otherButtonTitles:[actionTitles objectAtIndex:0], [actionTitles objectAtIndex:1],
										[actionTitles objectAtIndex:2], [actionTitles objectAtIndex:3], nil];
			break;

		case 5:
			sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"pid-actionsheet-cancel", @"Cancel") destructiveButtonTitle:nil
									   otherButtonTitles:[actionTitles objectAtIndex:0], [actionTitles objectAtIndex:1],
										[actionTitles objectAtIndex:2], [actionTitles objectAtIndex:3], [actionTitles objectAtIndex:4], nil];
			break;

		case 6:
			sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"pid-actionsheet-cancel", @"Cancel") destructiveButtonTitle:nil
									   otherButtonTitles:[actionTitles objectAtIndex:0], [actionTitles objectAtIndex:1],
										[actionTitles objectAtIndex:2], [actionTitles objectAtIndex:3], [actionTitles objectAtIndex:4], [actionTitles objectAtIndex:5], nil];
			break;

		case 7:
			sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"pid-actionsheet-cancel", @"Cancel") destructiveButtonTitle:nil
									   otherButtonTitles:[actionTitles objectAtIndex:0], [actionTitles objectAtIndex:1],
					 [actionTitles objectAtIndex:2], [actionTitles objectAtIndex:3], [actionTitles objectAtIndex:4], [actionTitles objectAtIndex:5], [actionTitles objectAtIndex:6], nil];
			break;

		case 8:
			sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"pid-actionsheet-cancel", @"Cancel") destructiveButtonTitle:nil
									   otherButtonTitles:[actionTitles objectAtIndex:0], [actionTitles objectAtIndex:1],
					 [actionTitles objectAtIndex:2], [actionTitles objectAtIndex:3], [actionTitles objectAtIndex:4], [actionTitles objectAtIndex:5], [actionTitles objectAtIndex:6], [actionTitles objectAtIndex:7], nil];
			break;
			
		default:
			sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"pid-actionsheet-cancel", @"Cancel") destructiveButtonTitle:nil otherButtonTitles:nil];

			// add our buttons
			for (NSString *t in actionTitles)
				[sheet addButtonWithTitle:t];
	}

	[sheet setActionSheetStyle:UIActionSheetStyleBlackOpaque];
	[sheet showInView:self.view];
	[self setActionSheet:sheet];
	[actionTitles release];
	[self setActionButtons:actions];
	[actions release];
	[sheet release];
	
	// let the locationmanager do its magic
	if (directionLocationManager == nil)
		directionLocationManager = [[LocationManager alloc] init];
	[directionLocationManager setDelegate:self];
	if ([CLLocationManager locationServicesEnabled])
		[directionLocationManager startUpdatingLocation];
}

// deal with the results at the end of the action sheet
- (void)actionSheet:(UIActionSheet *)sheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	// Cancelling?
	if (buttonIndex == -1 || buttonIndex >= [actionButtons count])
		return;

	PIDActionSheetAction action = [(NSNumber *)[actionButtons objectAtIndex:buttonIndex] integerValue];
	if (action == PIDActionSheetActionAddToFavourites)
	{
		// adding to favourites
		[self addToFavourites:nil];
		[directionLocationManager stopUpdatingLocation];
	}

	else if (action == PIDActionSheetActionEdit)
	{
		// editing the PID
		[self flipToListView:nil];
		[directionLocationManager stopUpdatingLocation];
	}

	else if (action == PIDActionSheetActionScheduledDepartures)
	{
		// viewing scheduled list
		[self pushScheduledList];
		[directionLocationManager stopUpdatingLocation];
	}
	else if (action == PIDActionSheetActionSaveFilter)
	{
		[self saveFilter];
		[directionLocationManager stopUpdatingLocation];
	}
	else if (action == PIDActionSheetActionClearFilter)
	{
		[self clearFilter];
		[directionLocationManager stopUpdatingLocation];
		
	} else if (action == PIDActionSheetActionStopMap)
	{
		[self pushMapForStop];
		[directionLocationManager stopUpdatingLocation];

	} else if (action == PIDActionSheetActionNearbyTicketRetailers)
	{
		[self pushNearbyTicketRetailers];
		[directionLocationManager stopUpdatingLocation];
		
	} else if (action == PIDActionSheetActionDirections)
	{
		// about to do directions, check first to see if we need to popup an alert
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		NSInteger seenAlertCount = [defaults integerForKey:@"seenDirectionsAlertCount"];
		if (seenAlertCount > 1)
		{
			[self presentDirections];
		} else
		{
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
															message:NSLocalizedString(@"pid-launch-gmaps", nil)
														   delegate:self
												  cancelButtonTitle:NSLocalizedString(@"pid-launch-gmaps-cancel", nil)
												  otherButtonTitles:NSLocalizedString(@"pid-launch-gmaps-ok", nil), nil];
			[alert show];

			[defaults setInteger:seenAlertCount+1 forKey:@"seenDirectionsAlertCount"];
			[defaults synchronize];
			[alert release];
		}
	}
}

- (void)presentDirections
{
	[directionLocationManager stopUpdatingLocation];
	if ([CLLocationManager locationServicesEnabled] && directionLocation != nil)
		[self.currentStop launchGoogleMapsWithDirectionsFromLocation:directionLocation];
	else if (self.parent != nil && self.parent.locationManager != nil && self.parent.locationManager.location != nil)
		[self.currentStop launchGoogleMapsWithDirectionsFromLocation:self.parent.locationManager.location];
	else
		[self.currentStop launchGoogleMapsWithDirections];
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == -1 || buttonIndex == alertView.cancelButtonIndex)
	{
		[directionLocationManager stopUpdatingLocation];
		return;
	}
	
	// present the directions
	[self presentDirections];
}

- (void)pushScheduledList
{
	UINavigationController *nav = (self.parent != nil ? self.parent.navigationController : self.navigationController);
	DepartureListController *departures = [[DepartureListController alloc] initWithScheduledListForStop:self.currentStop];
	
	// do we have a filter?
	if (self.filter != nil && self.filter.route != nil)
		[departures setSelectedRoute:self.filter.route.number];

	if ([nav isNavigationBarHidden])
		[self toggleNavigation:nil];
	
	if ([nav.navigationBar respondsToSelector:@selector(setTranslucent:)])
		[nav.navigationBar setTranslucent:NO];
	else
		[nav.navigationBar setBarStyle:UIBarStyleBlackOpaque];	
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:[nav.view.subviews objectAtIndex:0] cache:YES];
	[UIView setAnimationDuration:1];

	[nav popViewControllerAnimated:NO];
	[nav pushViewController:departures animated:NO];

	[UIView commitAnimations];
	[departures release];
}
//
// Setup the Add to Favourites button
//
- (void)showAddToFavouritesButton:(BOOL)showButton
{
	// find our navigation bar
	UINavigationItem *nav = (self.parent ? self.parent.navigationItem : self.navigationItem);
	[nav retain];

	// set the favourite
	/*if (showButton && !nav.rightBarButtonItem)
	{
		// create the button
		UIBarButtonItem *addToFavouritesButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
																							   target:self
																							   action:@selector(addToFavourites:)];
		
		// add it to the bar
		[nav setRightBarButtonItem:addToFavouritesButton];
	}
	
	// otherwise it must be a favourite already
	else if (!showButton && nav.rightBarButtonItem)
		[nav setRightBarButtonItem:nil]; */
	// set the action button
	
	// all done
	[nav release];
}



//
// Get the requested page of predictions
//
- (NSArray *)getPageOfPredictions:(int)page
{
	// get the lower bounds of the page
	int lowerBounds = page * 3;
	NSRange range = NSMakeRange(lowerBounds, 3);
	NSInteger count = [self.currentPredictions count];
	
	// make sure that our page bounds are within range, if not wrap around and grab the first page
	if (NSMaxRange(range) > count)
	{
		// make sure that we're not on the first page, if we are then theres no predictions!
		if (page == 0)
			return [NSArray array];
		
		// can we get a sub array?
		if (lowerBounds < count)
			return [self.currentPredictions subarrayWithRange:NSMakeRange(lowerBounds, count-lowerBounds)];

		return [[self currentPredictions] subarrayWithRange:NSMakeRange(0, 3)];
	}
	
	// return the predictions
	return [[self currentPredictions] subarrayWithRange:range];
}

//
// Calculate the number of minutes until the arrival time
// 
+ (NSString *)minutesUntilArrivalTime:(NSDate *)arrivalTime
{
	NSTimeInterval secondsUntilArrivalTime = [arrivalTime timeIntervalSinceDate:[NSDate date]];
	
	// work out the number of minutes to go
	int minutes = floor(secondsUntilArrivalTime / 60);
	
	// if we're less than a minute (but not more than a minute past) return now
	if (minutes >= -1 && minutes < 1)
		return NSLocalizedString(@"pid-now", "now");
	
	// or if less than 60 minutes from now return the number
	else if (minutes > 0 && minutes < 61)
		return [NSString stringWithFormat:@"%d", minutes];
	
	// have we gone more than 60 seconds past this one?
	else if (minutes < -1)
		return NSLocalizedString(@"pid-tramnotarriving", "--");
	
	// more than 60 seconds mean we switch over to using the exact arrival time
	else
	{
		// find the date
		NSDateFormatter *formatter = [[NSDateFormatter alloc] init];

		// use the current calendar to match days
		NSCalendar *cal = [NSCalendar currentCalendar];
		
		NSDate *today = [NSDate date];
		NSDateComponents *todayComponents = [cal components:(NSDayCalendarUnit | NSWeekdayCalendarUnit | NSYearCalendarUnit) fromDate:today];
		NSDateComponents *arrivalComponents = [cal components:(NSDayCalendarUnit | NSWeekdayCalendarUnit | NSYearCalendarUnit) fromDate:arrivalTime];
		
		// if its in the year 9999 then it means there is an error, display dashes
		if ([arrivalComponents year] >= 9999)
		{
			[formatter release];
			return NSLocalizedString(@"pid-tramnotarriving", "--");
		}			
		// check the weekday, if its arriving on a different day then we add the day name.
		if ([todayComponents weekday] != [arrivalComponents weekday])
			[formatter setDateFormat:@"ccc H:mm"];
		else
		{
			[formatter setTimeStyle:NSDateFormatterShortStyle];
			[formatter setDateStyle:NSDateFormatterNoStyle];
		}
			//[formatter setDateFormat:@"h:mm a"]; -- use built in style to allow for use of 24 hour time setting
		
		NSString *formattedArrivalTime = [formatter stringFromDate:arrivalTime];
		[formatter release];
		return formattedArrivalTime;
	}
}

+ (NSString *)voiceOverMinutesUntilArrivalTime:(NSDate *)arrivalTime
{
	NSTimeInterval secondsUntilArrivalTime = [arrivalTime timeIntervalSinceDate:[NSDate date]];
	
	// work out the number of minutes to go
	int minutes = floor(secondsUntilArrivalTime / 60);
	
	// if we're less than a minute (but not more than a minute past) return now
	if (minutes >= -1 && minutes < 1)
		return NSLocalizedString(@"pid-now", "now");
	
	else if (minutes == 1)
		return [NSString stringWithFormat:@"in %d minute", minutes];

	// or if less than 60 minutes from now return the number
	else if (minutes > 1 && minutes < 61)
		return [NSString stringWithFormat:@"in %d minutes", minutes];
	
	// have we gone more than 60 seconds past this one?
	else if (minutes < -1)
		return NSLocalizedString(@"pid-tramnotarriving", "--");
	
	// more than 60 seconds mean we switch over to using the exact arrival time
	else
	{
		// find the date
		NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
		
		// use the current calendar to match days
		NSCalendar *cal = [NSCalendar currentCalendar];
		
		NSDate *today = [NSDate date];
		NSDateComponents *todayComponents = [cal components:(NSDayCalendarUnit | NSWeekdayCalendarUnit | NSYearCalendarUnit) fromDate:today];
		NSDateComponents *arrivalComponents = [cal components:(NSDayCalendarUnit | NSWeekdayCalendarUnit | NSYearCalendarUnit) fromDate:arrivalTime];
		NSString *formattedArrivalTime;

		// if its in the year 9999 then it means there is an error, display dashes
		if ([arrivalComponents year] >= 9999)
		{
			[formatter release];
			return NSLocalizedString(@"pid-tramnotarriving", "--");
		}			
		// check the weekday, if its arriving on a different day then we add the day name.
		if ([todayComponents weekday] != [arrivalComponents weekday])
		{
			[formatter setDateFormat:@"'on' cccc 'at' hh:mm a"];
			formattedArrivalTime = [formatter stringFromDate:arrivalTime];
		} else
		{
			[formatter setTimeStyle:NSDateFormatterShortStyle];
			[formatter setDateStyle:NSDateFormatterNoStyle];
			formattedArrivalTime = [NSString stringWithFormat:@"at %@", [formatter stringFromDate:arrivalTime]];
		}
		//[formatter setDateFormat:@"h:mm a"]; -- use built in style to allow for use of 24 hour time setting
		
		[formatter release];
		return formattedArrivalTime;
	}
}


//
// Start
//
- (void)startWithNearby
{
	[self showNearestStop];
}

//
// Start using a particular stop
//
- (void)startWithStop:(Stop *)stop
{
	[self changeStop:stop];
}

//
// Change the Stop to something new
//
- (void)changeStop:(Stop *)stop
{
	[self setTrackerID:[[stop trackerID] integerValue]];
	[self setCurrentStop:stop];
	
	// if its a favourite pid then we need to check for a saved route filter
	if (self.parent != nil && self.filter == nil)
		[self setSavedFilter];
	
	// Add this new stop to our recent list
	[[StopList sharedManager] addMostRecentStopID:[stop trackerID]];
	
	// clear out the tracker information
	[self clearPage];
	[self hideScreen];
	[self showLoading];
	
	//[[self service]getInformationForStop:stopID];
	//[self refreshPredictions];
	
	// if we've already got a refreshing timer running better stop it
	if ([self timerRefresh] && [[self timerRefresh] isValid])
	{
		[[self timerRefresh] invalidate];
		[self setTimerRefresh:nil];
	}
	
	[self resume];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}

- (void)viewWillDisappear:(BOOL)animated
{
	//NSLog(@"[PID] Will Disappear");
	// and change the navigation bar over
	[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];

	if (editing) return;

	if (self.navigationController)
	{
		oldNavigationController = self.navigationController;
		[oldNavigationController retain];
		[self.navigationItem setRightBarButtonItem:nil animated:![oldNavigationController isNavigationBarHidden]];
	}
}

-(void)viewDidDisappear:(BOOL)animated
{
	//NSLog(@"[PID] Did Disappear");
	if (editing) return;

    // if we're paused it means we're coming back later. Otherwise stop so that we can release delegates and timers.
    if (![self isPaused])
        [self stop];

	if (oldNavigationController)
	{
		if ([oldNavigationController.navigationBar respondsToSelector:@selector(setTranslucent:)])
			[oldNavigationController.navigationBar setTranslucent:NO];
		else
			[oldNavigationController.navigationBar setBarStyle:UIBarStyleBlackOpaque];
		[oldNavigationController release];
		oldNavigationController = nil;
	}
}

//
// Stop
// 
- (void)stop
{
	// stop the timer
	//NSLog(@"Stopping PID timers.");
	[self pause];
	
	// release our delegate
	[self.service clearDelegate];
	
	// release our parent
	[self setParent:nil];
	
	// get rid of the link that the swipe controller is holding on to us with
	[self.swipeCapture setDelegate:nil];
	
	// and mark that we've stopped
	[self setHasBeenStopped:YES];
	[self setLocationManager:nil];
}

- (void)pause
{
	// stop the location service
	[self.locationManager stopUpdatingLocation];
	
	if ([self timerRefresh] && [[self timerRefresh] isValid])
	{
		[[self timerRefresh] invalidate];
		[self setTimerRefresh:nil];
	}
	
	// stop the timers if they're running
	if ([self timerClock])
	{
		[[self timerClock] invalidate];
		[self setTimerClock:nil];
	}
	
	// stop the page timer if its running
	if ([self timerPage])
	{
		[[self timerPage] invalidate];
		[self setTimerPage:nil];
	}
    
    // show paused
    [self showPaused];
}

- (void)resume
{
    // if we had been stopped we will need to restart again with our delegate
    if (self.service.delegate == nil)
        [self.service setDelegate:self];

	// find the refresh interval
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSInteger interval = [defaults integerForKey:@"pid_refresh"];
	
	// start the timer
	[self setTimerRefresh:[NSTimer scheduledTimerWithTimeInterval:(interval <= 0 ? 15 : interval)
														   target:self
														 selector:@selector(refreshPredictionsByTimer:)
														 userInfo:nil
														  repeats:YES]];
	[self.timerRefresh fire];
}


//
// Update the predictions
//
- (void)refreshPredictions
{
	NSAssert([self trackerID], @"Tracker ID Must be set");
	
	//NSLog(@"[PID] Refreshing Predictions with %@", self.filter);

	if (self.filter != nil)
		[[self service] getPredictionsForStop:[self trackerID] route:self.filter.route lowFloorOnly:self.filter.lowFloor];
	else
		[[self service] getPredictionsForStop:self.trackerID];
}
- (void)refreshPredictionsByTimer:(NSTimer *)aTimer
{
	[self refreshPredictions];
}


//
// Show the nearest stop (auto-updates)
//
- (void)showNearestStop
{
	// create a new location manager
	[self setLocationManager:[[[LocationManager alloc] init] autorelease]];
	
	// set the delegate and distance requirements
	[locationManager setDelegate:self];
	[locationManager setDistanceFilter:kCLDistanceFilterNone];
	
	// start updating the location
	[locationManager startUpdatingLocation];
	[self setLocatingStarted:[NSDate date]];

	// set a timer to display results
	[NSTimer scheduledTimerWithTimeInterval:NEAREST_STOP_THRESHOLD_200M target:self selector:@selector(displayStopFromTimer:) userInfo:nil repeats:NO];
	[NSTimer scheduledTimerWithTimeInterval:NEAREST_STOP_THRESHOLD_500M target:self selector:@selector(displayStopFromTimer:) userInfo:nil repeats:NO];
	[NSTimer scheduledTimerWithTimeInterval:NEAREST_STOP_THRESHOLD_MAX target:self selector:@selector(displayStopFromTimer:) userInfo:nil repeats:NO];
}


//
// The location Service has told us there was an error
//
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
	// if they've clicked "no" and we're not permitted to update, fail over to showing the default stop
	if ([error code] == kCLErrorDenied)
	{
		//NSLog(@"Denied using Core Location, showing %d", PIDDefaultStop);
		[manager stopUpdatingLocation];
		//Stop *stop = [[StopList sharedManager] getStopForTrackerID:[NSNumber numberWithInt:3013]];
		//[self changeStop:stop];
		//[self setInformation:stop];
	}
}


//
// We got a new location
//
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
	// has the location actually changed?
	//NSLog(@"Received location update.");

	// the location manager might have a more recent location
	if ([manager.location.timestamp isEqualToDate:[manager.location.timestamp laterDate:newLocation.timestamp]])
		newLocation = manager.location;

	[newLocation retain];
	[directionLocation release];
	directionLocation = newLocation;

	// What sort of accuracy do we need for here?
	/*NSTimeInterval secondsPassed = [[NSDate date] timeIntervalSinceDate:self.locatingStarted];
	NSInteger requiredAccracy = secondsPassed > NEAREST_STOP_THRESHOLD_500M ? 500 : (secondsPassed > NEAREST_STOP_THRESHOLD_200M ? 200 : 50);
	
	if (oldLocation == nil || [LocationManager distanceFromLocation:newLocation toLocation:oldLocation] > 0)
	{
		//NSLog(@"Found new location %@", newLocation);

		// check this location for accuracy
		if (newLocation.horizontalAccuracy <= requiredAccracy)
		{
			//NSLog(@"Required accuracy of %dm has been met (%.2fm). Using location %@", requiredAccracy, newLocation.horizontalAccuracy, newLocation);
			
			[self displayStopWithLocation:newLocation];
			[manager stopUpdatingLocation];
		} else
		{
			//NSLog(@"Ignoring location %@ because accuracy %.2fm does not meet required accuracy of %dm", newLocation, newLocation.horizontalAccuracy, requiredAccracy);
		}
	}*/
}

- (void)displayStopWithLocation:(CLLocation *)location
{
	// find the nearest stop
	StopDistance *nearestStop = [[StopList sharedManager] getNearestStopToLocation:location];
	//NSLog(@"Nearest stop is now %@", nearestStop);
	[self changeStop:[nearestStop stop]];
	[self setInformation:[nearestStop stop]];
}

- (void)displayStopFromTimer:(NSTimer *)aTimer
{
	// get the current location
	if (self.locationManager == nil || self.locationManager.location == nil || [self.locationManager stopped])
	{
		// some error handling
		return;
	}
	
	// have we reached a threshold?
	// What sort of accuracy do we need for here?
	NSTimeInterval secondsPassed = [[NSDate date] timeIntervalSinceDate:self.locatingStarted];
	NSInteger requiredAccracy = secondsPassed > NEAREST_STOP_THRESHOLD_500M ? 500 : (secondsPassed > NEAREST_STOP_THRESHOLD_200M ? 200 : 50);
	
	if (secondsPassed >= NEAREST_STOP_THRESHOLD_MAX || self.locationManager.location.horizontalAccuracy <= requiredAccracy)
	{
		//NSLog(@"Timeout has been reached, using location %@", self.locationManager.location);
		[self displayStopWithLocation:self.locationManager.location];
		[self.locationManager stopUpdatingLocation];
	}
}


//
// We've received new stop information from the service, this is a wrapper to pass it back around
//
- (void)setInformation:(Stop *)stop
{
	//NSLog(@"Received new stop information from PidsService, changing to stop %@", stop);
	
	// Display the loading

	[self hideScreen];
	[self showLoading];
	
	// set the current stop
	[self setCurrentStop:stop];
	
	// Update the displayed stop information
	[self updateDisplayedStopInformation];
	messageCycleCount = 0;
}

//
// We've received new predictions from the service, update things
//
- (void)setPredictions:(NSArray *)somePredictions
{
    // empty predictions?
    if ([somePredictions count] == 0)
        return;
    
    // Convert the prediction stubs to full predictions
    NSMutableArray *newPredictions = [[NSMutableArray alloc] initWithCapacity:0];
    for (PredictionStub *stub in somePredictions)
        [newPredictions addObject:[Prediction predictionForStub:stub]];
    
	// are the predictions for the right stop?
	if (![self.currentStop isEqual:[(Prediction *)[newPredictions objectAtIndex:0] stop]])
	{
		//NSLog(@"Discarding predictions that are for the wrong stop.");
        [newPredictions release];
		return;
	}
	
	
	//NSLog(@"[PID] Received new predictions, updating store");
	
	// set the current predictions
	[self setCurrentPredictions:newPredictions];

	// loop through the predictions and build special event messages and disruptions
	NSMutableArray *disruptions = [[NSMutableArray alloc] initWithCapacity:0];
	NSMutableArray *specialEventMessages = [[NSMutableArray alloc] initWithCapacity:0];
	for (Prediction *p in newPredictions)
	{
		// is it disrupted? (and not already in the array)
		if ([p disrupted] && [disruptions indexOfObject:p.routeNo] == NSNotFound)
			[disruptions addObject:[p routeNo]];
			
		// is there a special event message? and do we already have that message
		if (p.specialEventMessage && [p.specialEventMessage length] > 0 && [specialEventMessages indexOfObject:p.specialEventMessage] == NSNotFound)
			[specialEventMessages addObject:p.specialEventMessage];
	}

	// so do we have disruptions?
	NSMutableArray *messageList = [[NSMutableArray alloc] initWithCapacity:0];
	if ([disruptions count] > 0)
	{
		SpecialMessage *m = [[SpecialMessage alloc] init];
		if ([disruptions count] == 1)
			[m setMessage:[NSString stringWithFormat:NSLocalizedString(@"pid-message-disruption-single", @"Single route disruption"), [disruptions objectAtIndex:0]]];
		else
			 [m setMessage:[NSString stringWithFormat:NSLocalizedString(@"pid-message-disruption-multiple", @"Multiple route disruption"), [disruptions componentsJoinedByString:@", "]]];

		[m setType:SpecialMessageTypeDisruption];
		[messageList addObject:m];
		[m release];
	}
	[disruptions release];
	
	// and any special events?
	if ([specialEventMessages count] > 0)
	{
		for (NSString *sEM in specialEventMessages)
		{
			SpecialMessage *m = [[SpecialMessage alloc] init];
			[m setMessage:sEM];
			[m setType:SpecialMessageTypeSpecialEvent];
			[messageList addObject:m];
			[m release];
		}
	}
	[specialEventMessages release];
	
	// do we have messages then?
	[self setMessages:[NSArray arrayWithArray:messageList]];
	[messageList release];
	
	// make sure the view controller is set to show
	if (![[self loading] isHidden]) [self hideLoading];
	[self showScreen];
    
    [newPredictions release]; newPredictions = nil;
}

- (void)hideWelcome:(NSTimer *)aTimer
{
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:1];
	
	[self.welcomeScreen setAlpha:0];
	
	[UIView commitAnimations];
}

#pragma mark -
#pragma mark Error Handling from the Pids Service

- (void)pidsServiceDidFailWithError:(NSError *)error
{
	if ((error.code == PIDServiceErrorNotReachable || error.code == PIDServiceValidationError) && (([self.currentPredictions count] == 0 && [self.messages count] == 0) || [self isPaused]))
	{
		// display the error screen
		[self showScreen];
		[self showPaused];
		[self.errorMessage setText:[error localizedDescription]];
		[self.errorScreen setHidden:NO];
	}
}

#pragma mark -
#pragma mark Controlling the Filter

- (void)showFilter
{
	[self.filterLabel setHidden:NO];
}

- (void)hideFilter
{
	[self.filterLabel setHidden:YES];
	[self.routeFilter setHidden:YES];
	[self.lowFloorFilter setHidden:YES];
}

- (void)setFilter:(Filter *)newFilter
{
	[newFilter retain];
	[filter release];
	filter = newFilter;
	
    //NSLog(@"[PID] Received new filter: %@", filter);
    
	if (self.filterLabel != nil)
	{
		// make sure the filter is shown if necessary
		if (filter == nil || (filter.route == nil && !filter.lowFloor))
			[self hideFilter];
		
		else
		{
			[self showFilter];
			[self.lowFloorFilter setHidden:!filter.lowFloor];
			[self resetRouteFilterPosition];
			if (filter.route == nil)
				[self.routeFilter setHidden:YES];
			else
			{
				[self.routeFilter setHidden:NO];
				[self.routeFilterLabel setText:filter.route.number];
			}
		}
	}
}


// Reset the position of the route filter
- (void)resetRouteFilterPosition
{
	// if the low floor icon is hidden then we just copy it's X origin
	if ([self.lowFloorFilter isHidden])
	{
		CGRect routeFilterRect = self.routeFilter.frame;
		routeFilterRect.origin.x = self.lowFloorFilter.frame.origin.x;
		[self.routeFilter setFrame:routeFilterRect];
	} else
	{
		// if its visible then we set it 2px to the right of the low floor icon
		CGRect routeFilterRect = self.routeFilter.frame;
		routeFilterRect.origin.x = self.lowFloorFilter.frame.origin.x + self.lowFloorFilter.frame.size.width + 2;
		[self.routeFilter setFrame:routeFilterRect];
	}
}

// Save a filter to the database for this stop
- (void)saveFilter
{
	//NSLog(@"Filter: %@. Parent: %@, Page: %@", self.filter, self.parent, self.favouriteStopIndexPath);
	if (self.filter != nil && self.parent != nil)
		[[StopList sharedManager] setFilter:self.filter forFavouriteAtIndexPath:self.favouriteStopIndexPath];
}
- (void)clearFilter
{
	// remove the saved filter
	if (self.parent != nil)
		[[StopList sharedManager] setFilter:nil forFavouriteAtIndexPath:self.favouriteStopIndexPath];
	
	// clear the current filter
	[self setFilter:nil];
	
	// and hide the filter option
	[self hideFilter];
	
	// and refresh the predictions
	[self hideScreen];
	[self showLoading];
	[self refreshPredictions];
}

// Get a saved filter
- (void)setSavedFilter
{
	if (!self.currentStop || self.parent == nil)
		return;

	Filter *savedFilter = [[StopList sharedManager] filterForFavouriteAtIndexPath:self.favouriteStopIndexPath];
	if (savedFilter != nil)
		[self setFilter:savedFilter];
	else
		[self setFilter:nil];
}

#pragma mark -
#pragma mark Push to an Onboard Screen

- (IBAction)pushOnboardScreenForSender:(id)sender;
{
	if ([sender isKindOfClass:NSClassFromString(@"VoiceOverButton")])
	{
		Prediction *prediction = [(VoiceOverButton *)sender prediction];
		//NSLog(@"Pushing to: %@", prediction);

		// ok if we have a valid tram
		if ([prediction tram] != nil)
		{
			// find the app delegate, we're going to be crude
			tramTRACKERAppDelegate *d = (tramTRACKERAppDelegate *)[[UIApplication sharedApplication] delegate];

			// Start an animation transaction
			//[CATransaction begin];

			// Create a cross-fade
			//CATransition *fade = [CATransition animation];
			//[fade setType:kCATransitionPush];
			//[fade setSubtype:(d.tabBar.selectedIndex < [d.tabBar.viewControllers indexOfObject:d.onboardDisplaysController] ? kCATransitionFromRight : kCATransitionFromLeft)];
			//[fade setDuration:0.41];
			
			// add the cross-fade to the tab bar view
			//[d.tabBar.view.layer addAnimation:fade forKey:@"Fade"];
			
			// bring the menu bar back
			[self showNavigation:NO];

			// pop the pid off where it lives
			UINavigationController *nav = self.parent != nil ? self.parent.navigationController : self.navigationController;
			[nav popViewControllerAnimated:NO];
			[nav.navigationBar setTranslucent:NO];
			
			// make sure that the Onboard screen is right back at the beginning
			[d.onboardDisplaysController popToRootViewControllerAnimated:NO];
			
			// set the tram number
			[d.tramNumberController setStoredTramNumber:[prediction.tram number]];
			
			// clear out any existing numbers to prevent problems
			if (d.tramNumberController.enterNumberTextField != nil)
				[d.tramNumberController.enterNumberTextField setText:nil];

			// swap to the tab that contains the Onboard screen
			[d.tabBar setSelectedViewController:d.onboardDisplaysController];
			
			// commit the animation transaction
			//[CATransaction commit];
		}
	}
}

#pragma mark -
#pragma mark Push to a Map of the Stop

- (void)pushMapForStop
{
	//NSLog(@"Push Stop Map");

	// build a stop list based on ourselves
	MapViewController *mapView = [[MapViewController alloc] initWithStopList:[NSArray arrayWithObject:self.currentStop]];
	
	[mapView setListViewType:TTMapViewListTypeSelectedStop];
	[mapView setPurpleStops:[NSArray arrayWithObject:self.currentStop]];
	[mapView setTitle:NSLocalizedString(@"pid-actionsheet-map", @"Stop Map")];
	[mapView setSelectedStop:self.currentStop];
	[self showNavigation:NO];
	
	// flip over to the map view
	UINavigationController *nav = self.parent == nil ? self.navigationController : self.parent.navigationController;
	[UIView beginAnimations:@"FlipToMapView" context:nil];
	[[nav.view.subviews objectAtIndex:0] setBackgroundColor:[UIColor blackColor]];
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:[nav.view.subviews objectAtIndex:0] cache:YES];
	[UIView setAnimationDuration:1];
	
	
	// pop ourselves off the view controller
	[nav.navigationBar setTranslucent:NO];
	[nav popViewControllerAnimated:NO];
	[nav pushViewController:mapView animated:NO];
	
	[UIView commitAnimations];
	
	[mapView release];
}

#pragma mark -
#pragma mark Ticket Retailers

- (void)pushNearbyTicketRetailers
{
	// create the nearby view
	NearbyTicketRetailersList *retailerList = [[NearbyTicketRetailersList alloc] initWithStop:self.currentStop];
	[retailerList setTitle:NSLocalizedString(@"pid-actionsheet-nearbytickets", @"Nearby Ticket Outlets")];

	UINavigationController *nav = (self.parent != nil ? self.parent.navigationController : self.navigationController);
	
	if ([nav isNavigationBarHidden])
		[self toggleNavigation:nil];
	
	if ([nav.navigationBar respondsToSelector:@selector(setTranslucent:)])
		[nav.navigationBar setTranslucent:NO];
	else
		[nav.navigationBar setBarStyle:UIBarStyleBlackOpaque];	
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:[nav.view.subviews objectAtIndex:0] cache:YES];
	[UIView setAnimationDuration:1];
	
	[nav popViewControllerAnimated:NO];
	[nav pushViewController:retailerList animated:NO];
	
	[UIView commitAnimations];
	[retailerList release];
	
}

#pragma mark -
#pragma mark Touch Event handling

- (BOOL)touchesDidBegin:(NSSet *)touches withEvent:(UIEvent *)event
{
	// pass it up to the PagedPIDController if it exists
	if (self.parent)
		return [self.parent touchesDidBegin:touches withEvent:event];
	
	return NO;
}

- (BOOL)touchesDidMove:(NSSet *)touches withEvent:(UIEvent *)event
{
	// pass it up to the PagedPIDController if it exists
	if (self.parent)
		return [self.parent touchesDidMove:touches withEvent:event];
	
	return NO;
}
- (BOOL)touchesWereCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
	// pass it up to the PagedPIDController if it exists
	if (self.parent)
		return [self.parent touchesWereCancelled:touches withEvent:event];
	
	return NO;
}	
- (BOOL)touchesDidEnd:(NSSet *)touches withEvent:(UIEvent *)event
{
	// pass it up to the PagedPIDController if it exists
	if (self.parent)
		return [self.parent touchesDidEnd:touches withEvent:event];
	
	return NO;
}


#pragma mark -
#pragma mark Application becoming active/inactive

- (void)applicationDidEnterBackground
{
	//NSLog(@"PID Pausing, app resigning");
	[self pause];
}

- (void)applicationDidBecomeActive
{
	//NSLog(@"PID Resuming, app active");
	// reshow the screen (forces screen refresh)
	[self showScreen];
	[self resume];
}


#pragma mark -
#pragma mark Cleanup


- (void)dealloc {

	//NSLog(@"PID View being dealloc'd");
	
	[locationManager stopUpdatingLocation];
	[locationManager release];

	// release everything we're holding on to
	[stopTitle release];
	[stopSubtitle release];
	[stopTrackerID release];
	[stopID release];
	[screen release];
	[loading release];
	[screenRow1Route release];
	[screenRow1Destination release];
	[screenRow1Arrival release];
	[screenRow1LowFloor release];
	[screenRow1VoiceOver release];
	[screenRow1PIDToOnboardIcon release];
	[screenRow2Route release];
	[screenRow2Destination release];
	[screenRow2Arrival release];
	[screenRow2LowFloor release];
	[screenRow2VoiceOver release];
	[screenRow2PIDToOnboardIcon release];
	[screenRow3Route release];
	[screenRow3Destination release];
	[screenRow3Arrival release];
	[screenRow3LowFloor release];
	[screenRow3VoiceOver release];
	[screenRow3PIDToOnboardIcon release];
	[clock release];
	[indicatorPlaying release];
	[indicatorWarning release];
	[indicatorPaused release];
	[timerClock release];
	[timerPage release];
	[currentPredictions release];
	[currentStop release];
	[distance release];
	[parent release];
	[service release];
	[timerRefresh release];
	[directionLocationManager release];
	[directionLocation release];
	[favouriteStopIndexPath release];
	[mr2Logo release];
	[mr3Logo release];
	
    [super dealloc];
}


@end
