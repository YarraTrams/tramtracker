//
//  TicketRetailerCell.m
//  tramTRACKER
//
//  Created by Robert Amos on 24/07/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "TicketRetailerCell.h"


@implementation TicketRetailerCell

- (id)initWithCellStyle:(TTSimpleCellStyle)cellStyle reuseIdentifier:(NSString *)reuseIdentifier {
	
	if (self = [super initWithCellStyle:cellStyle reuseIdentifier:reuseIdentifier]) {
		GradientBackgroundCell *background = [[GradientBackgroundCell alloc] initWithFrame:CGRectMake(0, 0, 320, 44) cellStyle:OnboardCellStyleNone];
		[self setBackgroundView:background];
		[background release];
	}
    return self;
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)dealloc {
    [super dealloc];
}


@end
