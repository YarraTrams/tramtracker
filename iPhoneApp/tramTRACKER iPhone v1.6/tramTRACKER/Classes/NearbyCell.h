//
//  NearbyCell.h
//  tramTRACKER
//
//  Created by Robert Amos on 4/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GradientBackgroundCell.h"

/**
 * @defgroup CustomTableCells Custom Table Cells
**/

/**
 * @ingroup CustomTableCells
**/

/**
 * A custom UITableViewCell that includes labels for the stop name, route description and distance to the stop.
**/
@interface NearbyCell : UITableViewCell {
	/**
	 * The label for the distance to the stop from a specific location.
	**/
	UILabel *distance;
	
	/**
	 * The label for the stop's name
	**/
	UILabel *name;

	/**
	 * The label for the route description
	**/
	UILabel *routeDescription;
}

@property (nonatomic, retain) UILabel *distance;
@property (nonatomic, retain) UILabel *name;
@property (nonatomic, retain) UILabel *routeDescription;

/**
 * Creates and positions the distance label
 * @private
**/
- (UILabel *)newLabelForDistance;

/**
 * Creates and positions the name label
 * @private
 **/
- (UILabel *)newLabelForName;

/**
 * Creates and positions the route description label
 * @private
 **/
- (UILabel *)newLabelForRouteDescription;

@end
