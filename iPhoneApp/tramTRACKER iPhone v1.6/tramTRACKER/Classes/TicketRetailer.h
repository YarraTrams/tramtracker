//
//  TicketRetailer.h
//  tramTRACKER
//
//  Created by Robert Amos on 22/07/10.
//  Copyright 2010 Yarra Trams. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <MapKit/MapKit.h>


@interface TicketRetailer :  NSManagedObject  <MKAnnotation>
{
}

@property (nonatomic, retain) NSNumber * open24Hour;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSNumber * sellsMyki;
@property (nonatomic, retain) NSDecimalNumber * latitude;
@property (nonatomic, retain) NSNumber * sellsMetcards;
@property (nonatomic, retain) NSDecimalNumber * longitude;
@property (nonatomic, retain) NSString * suburb;

/**
 * Returns an NSArray of all available Ticket Retailers
 *
 * @return		An NSArray of all Ticket Retailers
**/
+ (NSArray *)allTicketRetailers;


/**
 * Returns all Ticket Retailers within a specific geographic region.
 *
 * @param		region			An MKCoordinateRegion that describes a geographic area
 * @return		NSArray		A NSArray of ticket retailers, or an empty array if none found.
**/
+ (NSArray *)ticketRetailersInRegion:(MKCoordinateRegion)region;

/**
 * Gets the nearest ticket retailers by distance from a location
**/
+ (NSArray *)nearestTicketRetailersToLocation:(CLLocation *)location count:(NSUInteger)count;
+ (NSArray *)nearestTicketRetailersWithoutDistancesToLocation:(CLLocation *)location count:(NSUInteger)count;

- (BOOL)isOpen24Hours;
- (BOOL)doesSellMetcard;
- (BOOL)doesSellMyki;

/**
 * The Location of the Ticket Retailer
 *
 * @return	A Location object with the coordinates of the Ticket Retailer
 **/
- (CLLocation *)location;

/**
 * The 2D Coordinates of a ticket retailer as required by MKAnnotation
 *
 * @return	A CLLocationCoordinate2D struct containing the latitude and longitude of the ticket retailer, as per the MKAnnotation protocol.
 **/
- (CLLocationCoordinate2D)coordinate;

/**
 * Get the ticket retailers title. This is just a convience method for -name to allow us to plot this retailer on the map
 **/
- (NSString *)title;

/**
 * Get the retailer's subtitle. this is just a convenience method for -address to allow us to plot this retailer on the map
 */
- (NSString *)subtitle;

/**
 * Opens the Google Maps app to show directions
**/
- (void)launchGoogleMapsWithDirectionsToHere;
- (void)launchGoogleMapsWithDirectionsFromHere;

@end


@interface TicketRetailerDistance : NSObject
{
	TicketRetailer *retailer;
	CLLocationDistance distance;
}

@property (nonatomic, retain) TicketRetailer *retailer;
@property (nonatomic) CLLocationDistance distance;

- (NSComparisonResult)compareDistance:(TicketRetailerDistance *)otherTicketRetailerDistance;
- (NSString *)formattedDistance;

@end



