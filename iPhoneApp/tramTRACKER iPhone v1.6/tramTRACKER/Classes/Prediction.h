//
//  Prediction.h
//  tramTRACKER
//
//  Created by Robert Amos on 10/02/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Stop.h"
#import "Tram.h"

@class PredictionStub;

/**
 * Represents a predicted arrival at a stop of a tram travelling on a route.
**/
@interface Prediction : NSObject <NSCopying> {
	
	/**
	 * The destination of this tram as displayed on the tram's headboard.
	**/
	NSString *destination;

	/**
	 * The "primary" route number that this tram is travelling on. The tram may actually be travelling on
	 * a sub-route of this route number. This is specified in the headboardRouteNumber
	**/
	NSString *routeNo;

	/**
	 * The route number as displayed on the tram's headboard. This is usually identical to the routeNo, but
	 * could be a sub-route of it.
	**/
	NSString *headboardRouteNumber;
	
	/**
	 * The Tram
	**/
	Tram *tram;
	
	/**
	 * Whether this tram is a low floor tram.
	**/
	bool lowFloor;
	
	/**
	 * Whether this tram has an air conditioner installed.
	**/
	bool airConditioned;
	
	/**
	 * Whether this tram is likely to have it's air conditioner switched on.
	**/
	bool displayAirConditioning;

	/**
	 * The predicted date and time of arrival at the stop. This is adjusted to the devices date and time.
	**/
	NSDate *predictedArrivalDateTime;

	/**
	 * The date and time that the server saw the request for a prediction. This is used to calculate the adjustment
	 * to bring the predicted arrival time into the local time.
	**/
	NSDate *requestDateTime;

	/**
	 * The special event message, if any. Is generally used to indicate trackwork and other changes.
	**/
	NSString *specialEventMessage;
	NSNumber *tripID;

	/**
	 * Whether the route the tram is travelling on is disrupted.
	**/
	bool disrupted;

	/**
	 * Whether tramTRACKER is available for where this prediction was requested.
	 * tramTRACKER may not be available for example, when there is a diversion
	 * and the selected stop is not in service.
	**/
	bool tramTrackerAvailable;
	
	/**
	 * Whether this prediction is affected by a special event
	**/
	BOOL hasSpecialEvent;
	
	/**
	 * When the tramTRACKER service is switched to debug mode, this will contain the distance
	 * the tram is along the route
	**/
	CGFloat distance;

	/**
	 * The stop that the prediction is for
	**/
	Stop *stop;
}

@property (nonatomic, retain) NSString *destination;
@property (nonatomic, retain) NSString *routeNo;
@property (nonatomic, retain) NSString *headboardRouteNumber;
@property (nonatomic, retain) Tram *tram;
@property (nonatomic) bool lowFloor;
@property (nonatomic) bool airConditioned;
@property (nonatomic) bool displayAirConditioning;
@property (nonatomic, retain) NSDate *predictedArrivalDateTime;
@property (nonatomic, retain) NSDate *requestDateTime;
@property (nonatomic, retain) NSString *specialEventMessage;
@property (nonatomic, retain) NSNumber *tripID;
@property (nonatomic) bool disrupted;
@property (nonatomic) bool tramTrackerAvailable;
@property (nonatomic) CGFloat distance;
@property (nonatomic) BOOL hasSpecialEvent;
@property (nonatomic, retain) Stop *stop;

/**
 * Returns a nicely formatted version of the predicted arrival date and time
**/
- (NSString *)formattedPredictedArrivalDateTime;

/**
 * Static method creating a formatted predicted arrival date and time for the given arrival NSDate object
 *
 * @param	arrival		The arrival time of the tram
 * @return				A NSString with a formatted arrival date and time
**/
+ (NSString *)formattedPredictedArrivalDateTime:(NSDate *)arrival;

/**
 * Static method creating a formatted predicted arrival date and time for the given arrival NSDate object.
 *
 * This format is aimed at VoiceOver users
 *
 * @param	arrival		The arrival time of the tram
 * @return				A NSString with a formatted arrival date and time
 **/
+ (NSString *)voiceOverPredictedArrivalDateTime:(NSDate *)arrival;

/**
 * Compares one prediction to another based on their arrival time.
 *
 * Allows you to use -sortedArrayUsingSelector:\@*selector(comparePredictionByArrivalTime:) on a NSArray of Predictions.
 *
 * @param	otherPrediction		The prediction to compare this prediction against.
**/
- (NSComparisonResult)comparePredictionByArrivalTime:(Prediction *)otherPrediction;

/**
 * Returns the number of minutes from now until the predicted arrival time.
**/
- (NSString *)minutesUntilArrivalTime;

/**
 * Static method returning the number of minutes from now until a specified arrival time
 *
 * @param	arrivalTime			A NSDate object with a predicted arrival time
 * @return						The number of minutes until that predicted arrival time
**/
+ (NSString *)minutesUntilArrivalTime:(NSDate *)arrivalTime;

/**
 * A formatted route description for this tram when departing a particular stop
 *
 * @param	stop			A stop object
 * @return					A formatted route description with via information.
**/
- (NSString *)formattedRouteDescriptionFromStop:(Stop *)stop;

/**
 * The text to be read out by Voice Over when viewing this prediction
**/
- (NSString *)voiceOver;

/**
 * Convert a Prediction Stub to a full Prediction
**/
+ (Prediction *)predictionForStub:(PredictionStub *)predictionStub;

@end

/**
 * A prediction stub
 *
 * Allows the PIDS Service to pass the prediction back from the background thread.
 * Is passed to +[Prediction predictionForStub:predictionStub] to get the full object.
**/

@interface PredictionStub : Prediction {
@private
	TramStub *tramStub;
	StopStub *stopStub;
}

@property (nonatomic, retain) TramStub *tramStub;
@property (nonatomic, retain) StopStub *stopStub;

@end
