//
//  EnterTramNumberController.h
//  tramTRACKER
//
//  Created by Robert Amos on 31/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PidsService.h"
#import "Journey.h"
#import "OnboardViewController.h"
#import "Tram.h"


@interface EnterTramNumberController : UIViewController <UIActionSheetDelegate, UIAlertViewDelegate> {
	/**
	 * A reference to the text field displayed on the screen so that we can banish it's keyboard.
	 **/
	UITextField *enterNumberTextField;
	
	PidsService *service;
	UIView *loadingView;
	UIImageView *loadingIndicator;
	NSNumber *locatingTramNumber;
	
	NSTimer *timeoutTimer;
	NSNumber *storedTramNumber;
	UILabel *unavailableLabel;
}

@property (nonatomic, strong) IBOutlet UITextField *enterNumberTextField;
@property (nonatomic, strong) IBOutlet UIView *loadingView;
@property (nonatomic, strong) IBOutlet UIImageView *loadingIndicator;
@property (nonatomic, strong) IBOutlet NSNumber *locatingTramNumber;
@property (nonatomic, strong) IBOutlet UILabel *unavailableLabel;
@property (nonatomic, strong) PidsService *service;
@property (nonatomic, strong) NSNumber *storedTramNumber;

/**
 * The user selected the tracker ID field, enable the Cancel button
 **/
- (IBAction)didStartEditing:(id)sender;

/**
 * The user cancelled editing the tracker ID field, banish the keyboard
 **/
- (void)cancelEditing:(id)sender;

// delegate
- (void)setJourney:(JourneyStub *)journey;

// look for a tram
- (void)locateTram;

- (void)timeout:(NSTimer *)aTimer;
- (void)detectedTram:(Tram *)tram;


@end
