//
//  HelpWebController.h
//  tramTRACKER
//
//  Created by Robert Amos on 19/06/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface HelpWebController : UIViewController <UIWebViewDelegate> {
	NSURL *url;
	UIWebView *webView;
	
	UIActivityIndicatorView *loadingIndicator;
	UILabel *loadingText;
	UILabel *errorText;
}

@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) UIWebView *webView;

- (id)initWithHelpFile:(NSString *)helpFile;
- (void)retry;

@end
