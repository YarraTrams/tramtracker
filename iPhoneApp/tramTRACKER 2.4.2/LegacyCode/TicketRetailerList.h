//
//  TicketRetailerList.h
//  tramTRACKER
//
//  Created by Robert Amos on 24/07/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TicketRetailer.h"
#import "TicketRetailerCell.h"
#import "UnderBackgroundView.h"
#import "OnboardListCell.h"


@interface TicketRetailerList : UITableViewController <UISearchDisplayDelegate> {
	NSFetchedResultsController *resultsController;
	UISearchBar	*searchBar;
	NSFetchedResultsController *searchResultsController;
	
}

@property (nonatomic, strong) UISearchBar *searchBar;
@property (weak, nonatomic, readonly) NSFetchedResultsController *searchResultsController;

- (id)initTicketRetailerList;

- (void)importTicketRetailers;

- (UIView *)tableFooterView;
- (void)refreshResults;
- (void)refreshResults:(NSNotification *)note;

- (NSFetchedResultsController *)searchResultsController;
- (void)setSearchResultsControllerSearchString:(NSString *)searchString;

@end
