//
//  BackgroundSoundSelectionController.h
//  tramTRACKER
//
//  Created by Robert Amos on 17/08/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>


@interface BackgroundSoundSelectionController : UITableViewController {
	NSString * soundType;
	id target;
	SEL action;
	NSDictionary *sounds;
}

- (id)initWithSoundType:(NSString *)aSoundType target:(id)aTarget action:(SEL)anAction;

- (void)playSelectedSound;

@end
