////
////  TicketOutletService.h
////  tramTRACKER
////
////  Created by Robert Amos on 5/08/10.
////  Copyright 2010 Metro Trains Melbourne. All rights reserved.
////
//
//#import <Foundation/Foundation.h>
//
//
//@interface TicketOutletService : NSObject {
//
//	NSObject *__weak delegate;
//}
//
//@property (weak, readonly, nonatomic) NSObject *delegate;
//
//- (id)initWithDelegate:(NSObject *)aDelegate;
//
///**
// * Tells the delegate whether the ticket outlets have been updated since the specified date.
// *
// * Your delegate must implement -setHasTicketOutletUpdates:(NSNumber *)updated withFileSize:(NSNumber *)fileSize;
// **/
//- (void)hasTicketOutletsBeenUpdatedSinceDate:(NSDate *)date;
//
///**
// * Downloads and returns to the delegate an updated list of ticket outlets.
// *
// * Your delegate must implement -setTicketOutlets:(NSArray *)outlets;
// **/
//- (void)getTicketOutletUpdates;
//
//@end
