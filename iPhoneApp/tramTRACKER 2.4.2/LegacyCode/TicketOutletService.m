////
////  TicketOutletService.m
////  tramTRACKER
////
////  Created by Robert Amos on 5/08/10.
////  Copyright 2010 Metro Trains Melbourne. All rights reserved.
////
//
//#import "TicketOutletService.h"
//#import "TicketOutletServiceDelegate.h"
//
//
//@implementation TicketOutletService
//
//@synthesize delegate;
//
//static NSString * const TTTicketOutletUpdateURL = @"http://ws.tramtracker.com.au/MetlinkRetailOutletInfo/TicketRetailers.plist";
//
//- (id)initWithDelegate:(NSObject *)aDelegate
//{
//	if (self = [super init])
//	{
//		delegate = aDelegate;
//	}
//	return self;
//}
//
//- (void)hasTicketOutletsBeenUpdatedSinceDate:(NSDate *)date
//{
//	// Create the NSURLRequest
//	//NSLog(@"[TOS] Checking for ticket outlet updates since %@", date);
//	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:TTTicketOutletUpdateURL]];
//	
//	// HEAD only
//	[request setHTTPMethod:@"HEAD"];
//	
//	// create the delegate
//	TicketOutletServiceDelegate *d = [[TicketOutletServiceDelegate alloc] initWithDelegate:self.delegate];
//	[d setUpdatesSinceDate:date];
//	[d setServiceType:TicketOutletServiceDelegateCheck];
//	
//	// Create the NSURLConnection
//	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
//	[NSURLConnection connectionWithRequest:request delegate:d];
//	
//	//Make sure NSURLConnection retains it's delegate under ARC
////	[d autorelease];
//}
//
//- (void)getTicketOutletUpdates
//{
//	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL	URLWithString:TTTicketOutletUpdateURL]];
//
//	// create the delegate
//	TicketOutletServiceDelegate *d = [[TicketOutletServiceDelegate alloc] initWithDelegate:self.delegate];
//	[d setServiceType:TicketOutletServiceDelegateUpdate];
//
//	// Create the NSURLConnection
//	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
//	NSURLResponse *response = nil;
//	NSError *error = nil;
//	NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//	if (returnData == nil || error != nil)
//		[d failWithError:error];
//	else
//	{
//		[d updateTicketOutletsWithData:returnData];
//	}
////	[d autorelease];
//}
//
//
//
//@end
