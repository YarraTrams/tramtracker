//
//  SimpleCell.m
//  tramTRACKER
//
//  Created by Robert Amos on 31/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "SimpleCell.h"

TTSimpleCellStyle const TTSimpleCellStyleDefault = UITableViewCellStyleDefault;
TTSimpleCellStyle const TTSimpleCellStyleValue1 = UITableViewCellStyleValue1;
TTSimpleCellStyle const TTSimpleCellStyleMultiLine = UITableViewCellStyleDefault;
TTSimpleCellStyle const TTSimpleCellStyleSubtitle = UITableViewCellStyleSubtitle;

@implementation SimpleCell

- (id)initWithCellStyle:(TTSimpleCellStyle)cellStyle reuseIdentifier:(NSString *)reuseIdentifier {
	
	if (self = [super initWithStyle:cellStyle reuseIdentifier:reuseIdentifier]) {
		multiLine = NO;
	}
    return self;
}

- (void)layoutSubviews
{
	[super layoutSubviews];

	// clear the labels and resize the text
	if (self.textLabel != nil)
	{
		[self.textLabel setBackgroundColor:[UIColor clearColor]];
		[self.textLabel setOpaque:NO];
		[self.textLabel setFont:[UIFont boldSystemFontOfSize:16]];
		
		if (multiLine)
		{
			[self.textLabel setNumberOfLines:10];
			[self.textLabel setAutoresizingMask:UIViewAutoresizingFlexibleHeight];
			[self.textLabel setLineBreakMode:NSLineBreakByWordWrapping];	
		}
	}

	// make sure that the background on the other one is clear too
	if (self.detailTextLabel != nil)
	{
		[self.detailTextLabel setBackgroundColor:[UIColor clearColor]];
		[self.detailTextLabel setOpaque:NO];
	}
}

- (void)setMultiLine:(BOOL)newMultiLine
{
	multiLine = newMultiLine;
	if (self.textLabel == nil) return;

	if (newMultiLine)
	{
		[self.textLabel setNumberOfLines:10];
		[self.textLabel setAutoresizingMask:UIViewAutoresizingFlexibleHeight];
		[self.textLabel setLineBreakMode:NSLineBreakByWordWrapping];	
	} else
	{
		[self.textLabel setNumberOfLines:1];
		[self.textLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
	}
}

- (void)setTextLabelText:(NSString *)string
{
		[self.textLabel setText:string];
}
- (void)setDetailTextLabelText:(NSString *)string
{
	[self.detailTextLabel setText:string];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}




@end
