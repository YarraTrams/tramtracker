//
//  TramImage.m
//  tramTRACKER
//
//  Created by Robert Amos on 11/05/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "TramImage.h"


@implementation TramImage

- (id)initWithImage:(UIImage *)anImage target:(id)aTarget action:(SEL)anAction
{
	if (self = [super initWithImage:anImage])
	{
		[self setUserInteractionEnabled:YES];
		target = aTarget;
		action = anAction;
	}
	return self;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	// touch up?
	if (target != nil && [target respondsToSelector:action])
	{
		SuppressPerformSelectorLeakWarning([target performSelector:action]);
	}
}

@end
