//
//  TicketRetailerHeaderView.m
//  tramTRACKER
//
//  Created by Robert Amos on 26/07/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "TicketRetailerHeaderView.h"


@implementation TicketRetailerHeaderView

@synthesize retailer;

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {

		// we want to add a label around half way down
		UILabel *x = [self retailerName];
		[self addSubview:x];
	}
    return self;
}

- (UILabel *)retailerName
{
	// not set yet?
	if (retailerName == nil)
	{
		// position it
		CGRect frame = CGRectMake(20, 10, 280, 40);
		retailerName = [[UILabel alloc] initWithFrame:frame];
		
		// make some happy happy changes
		[retailerName setBackgroundColor:[UIColor clearColor]];					// background colour
		[retailerName setTextColor:[UIColor blackColor]];						// text colour
		[retailerName setOpaque:NO];											// no need for transparency here
		[retailerName setFont:[UIFont systemFontOfSize:16]];				// and a 16pt font
		[retailerName setTextAlignment:NSTextAlignmentLeft];					// right align the text (so it sits against the right side)
	}
	return retailerName;
}


- (void)setRetailer:(TicketRetailer *)aRetailer
{
	retailer = aRetailer;
	
	// change the name of the label
	[self.retailerName setText:retailer.name];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)dealloc {
	
    // if we're stuck holding things make sure its gone
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
	 retailerName = nil;
}


@end
