//
//  NearbyActionMenuViewController.m
//  tramTRACKER
//
//  Created by Tom King on 30/10/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#define kAllStopsCellIdentifier @"AllStopsCell"
#define kStopsWithShelterCellIdentifier @"StopsWithShelterCell"
#define kEasyAccessStopsCellIdentifier @"EasyAccessStopsCell"
#define kTicketOutletsCellIdentifier @"TicketOutletsCell"
#define kEnterTrackerIDCellIdentifier @"EnterTrackerIDCell"
#define kSearchCellIdentifier @"SearchCell"

static NSArray *_actionMenuIdentifiers = nil;

#import "NearbyActionMenuViewController.h"

@interface NearbyActionMenuViewController ()

@end

@implementation NearbyActionMenuViewController

+ (void)load
{	
	_actionMenuIdentifiers = @[kAllStopsCellIdentifier, kStopsWithShelterCellIdentifier, kEasyAccessStopsCellIdentifier, kTicketOutletsCellIdentifier, kEnterTrackerIDCellIdentifier, kSearchCellIdentifier];
}

+ (NearbyActionMenuViewController *)viewControllerWithDefaultNib
{	
	NearbyActionMenuViewController *controller = [[UIStoryboard storyboardWithName:NSStringFromClass([NearbyActionMenuViewController class]) bundle:[NSBundle mainBundle]] instantiateInitialViewController];
	
	return controller;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nearby-Action-Filters-BKG-.png"]];	
	[self.tableView setBackgroundView:imageView];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	if ([self.tableView indexPathForSelectedRow] == nil)
	{
		[self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionTop];

	}
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_actionMenuIdentifiers count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = [_actionMenuIdentifiers objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
	UIView *selectedBackgroundView = [[UIView alloc] init];
	[selectedBackgroundView setBackgroundColor:[UIColor clearColor]];
	
	[cell setSelectedBackgroundView:selectedBackgroundView];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section 
{ 	
	return 44.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *headerView = [[UIView alloc] init];
	[headerView setBackgroundColor:[UIColor clearColor]];
	
	return headerView;
}

 #pragma mark - Table view delegate
 
 - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
 {
	 NSString *cellIdentifier = [_actionMenuIdentifiers objectAtIndex:indexPath.row];
	 
	 if ([cellIdentifier isEqualToString:kAllStopsCellIdentifier])
	 {
		 [self.delegate didSelectAllStops];
	 }
	 else if ([cellIdentifier isEqualToString:kStopsWithShelterCellIdentifier])
	 {
		 [self.delegate didSelectStopsWithShelter];
	 }
	 else if ([cellIdentifier isEqualToString:kEasyAccessStopsCellIdentifier])
	 {
		 [self.delegate didSelectEasyAccessStops];
	 }
	 else if ([cellIdentifier isEqualToString:kTicketOutletsCellIdentifier])
	 {
		 [self.delegate didSelectTicketOutlets];
	 }
	 else if ([cellIdentifier isEqualToString:kEnterTrackerIDCellIdentifier])
	 {
		 [self.delegate didSelectEnterTrackerID];
	 }
	 else if ([cellIdentifier isEqualToString:kSearchCellIdentifier])
	 {
		 [self.delegate didSelectSearch];
	 }
 }
 
 

@end
