//
//  HelpSchedulesController.m
//  tramTRACKER
//
//  Created by Robert Amos on 5/05/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "HelpSchedulesController.h"

const NSString *HelpURLSubdirSchedules = @"scheduled_departures";

#define HELP_SCHEDULES_ACCESS 0
#define HELP_SCHEDULES_ROUTE 1
#define HELP_SCHEDULES_DATE 2
#define HELP_SCHEDULES_JOURNEY 3
#define HELP_SCHEDULES_CONNECTIONS 4

@implementation HelpSchedulesController

- (id)initHelpSchedulesController
{
	if (self = [super initWithStyle:UITableViewStylePlain])
	{
		[self setTitle:NSLocalizedString(@"help-scheduled-departures", @"Help")];
		[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
	}
	return self;
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}




#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    SimpleCell *cell = (SimpleCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[SimpleCell alloc] initWithCellStyle:TTSimpleCellStyleDefault reuseIdentifier:CellIdentifier];
		[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
		
		GradientBackgroundCell *background = [[GradientBackgroundCell alloc] initWithFrame:CGRectMake(0, 0, 320, 44) cellStyle:OnboardCellStyleNone];
		[cell setBackgroundView:background];
	}
    
	switch (indexPath.row)
	{
		case HELP_SCHEDULES_ACCESS:
			[cell setTextLabelText:NSLocalizedString(@"help-schedules-access", @"Accessing Scheduled Departures")];
			break;
		case HELP_SCHEDULES_ROUTE:
			[cell setTextLabelText:NSLocalizedString(@"help-schedules-route", @"Changing the Route")];
			break;
		case HELP_SCHEDULES_DATE:
			[cell setTextLabelText:NSLocalizedString(@"help-schedules-date", @"Changing the Date")];
			break;
		case HELP_SCHEDULES_JOURNEY:
			[cell setTextLabelText:NSLocalizedString(@"help-schedules-journey", @"Scheduled Journey")];
			break;
		case HELP_SCHEDULES_CONNECTIONS:
			[cell setTextLabelText:NSLocalizedString(@"help-schedules-connections", @"Finding Connections")];
			break;
	}
	
    return cell;	
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

	// we create a UIWebView that we push onto the stack, start by working out our URL
	NSString *fileName = nil;
	
	switch (indexPath.row)
	{
		case HELP_SCHEDULES_ACCESS:
			fileName = @"accessing15.html";
			break;
			
		case HELP_SCHEDULES_ROUTE:
			fileName = @"route.html";
			break;
			
		case HELP_SCHEDULES_DATE:
			fileName = @"date.html";
			break;
			
		case HELP_SCHEDULES_JOURNEY:
			fileName = @"journey15.html";
			break;
			
		case HELP_SCHEDULES_CONNECTIONS:
			fileName = @"connections.html";
			break;
	}
	
	// nothing found?
	if (fileName == nil)
		return;
	
	// create the web view
	HelpWebController *helpWeb = [[HelpWebController alloc] initWithHelpFile:[NSString stringWithFormat:@"%@/%@", HelpURLSubdirSchedules, fileName]];
	SimpleCell *cell = (SimpleCell *)[self.tableView cellForRowAtIndexPath:indexPath];
	[helpWeb setTitle:cell.textLabel.text];
	[self.navigationController pushViewController:helpWeb animated:YES];
}






@end

