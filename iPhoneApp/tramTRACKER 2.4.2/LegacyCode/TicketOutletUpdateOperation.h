////
////  TicketOutletUpdateOperation.h
////  tramTRACKER
////
////  Created by Robert Amos on 5/08/10.
////  Copyright 2010 __MyCompanyName__. All rights reserved.
////
//
//#import <Foundation/Foundation.h>
//
//@class BackgroundSynchroniser;
//@class TicketOutletService;
//
//
//@interface TicketOutletUpdateOperation : NSOperation {
//
//	/**
//	 * An instance of the PidsService to retrieve ticket outlet information from.
//	 **/
//	TicketOutletService *service;
//
//	/**
//	 * The BackgroundSynchroniser object that is managing this update operation
//	 **/
//	BackgroundSynchroniser *__weak syncManager;
//	
//	/**
//	 * Status - YES if the operation is executing, NO otherwise
//	 **/
//	BOOL executing;
//	
//	/**
//	 * Status - YES if the operation was finished (or cancelled), NO otherwise
//	 **/
//	BOOL finished;
//	
//}
//
//@property (nonatomic, weak) BackgroundSynchroniser *syncManager;
//
//
///**
// * Required by the NSOperationQueue, whether to execute this operation concurrently.
// **/
//- (BOOL)isConcurrent;
//
///**
// * NSOperationQueue wrapper for the executing ivar.
// **/
//- (BOOL)isExecuting;
//
///**
// * NSOperationQueue wrapper for the finished ivar.
// **/
//- (BOOL)isFinished;
//
///**
// * Starts the Stop Delete Operation
// **/
//- (void)start;
//
///**
// * Finishes the Update operation and handles notifications
// **/
//- (void)finish;
//
//- (void)updateTicketOutlets;
//- (void)setTicketOutlets:(NSArray *)ticketOutlets;
//- (void)updateCoreDataTicketOutletsFromFile:(NSString *)file;
//
//@end
