//
//  MostRecentCell.h
//  tramTRACKER
//
//  Created by Robert Amos on 10/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GradientBackgroundCell.h"

/**
 * @ingroup CustomTableCells
 **/

/**
 * A custom UITableViewCell that shows a stop in the MostRecent stop list
**/
@interface MostRecentCell : UITableViewCell {
	/**
	 * The label for the stop's name
	**/
	UILabel *name;

	/**
	 * The label for the description of the routes running through that stop
	**/
	UILabel *routeDescription;
}

@property (nonatomic, strong) UILabel *name;
@property (nonatomic, strong) UILabel *routeDescription;

/**
 * Creates and positions the label for the name
**/
- (UILabel *)newLabelForName;

/**
 * Creates and positions the label for the route description
**/
- (UILabel *)newLabelForRouteDescription;

@end
