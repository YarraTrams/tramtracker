//
//  DepartureCell.m
//  tramTRACKER
//
//  Created by Robert Amos on 6/04/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "DepartureCell.h"


@implementation DepartureCell

@synthesize name, arrival, routeDescription;

- (id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    if (self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier]) {
        // Initialization code

		// set the background view
		GradientBackgroundCell *background = [[GradientBackgroundCell alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width-60, self.frame.size.height) cellStyle:OnboardCellStyleMinutesOnRightDisabled];
		[self setBackgroundView:background];

		// and the stop name
		name = [self newLabelForName];
		[self.contentView addSubview:self.name];
		
		// and the arrival time
		arrival = [self newLabelForArrival];
		[self.contentView addSubview:self.arrival];
		
		// and the route info
		routeDescription = [self newLabelForRouteDescription];
		[self.contentView addSubview:self.routeDescription];
	}
    return self;
}

//
// Create a new arrival label
//
- (UILabel *)newLabelForArrival
{
	// position it
	CGRect frame = CGRectMake(self.frame.size.width-58, 2, 56, 40);
	UILabel *newLabel = [[UILabel alloc] initWithFrame:frame];
	
	// make some happy happy changes
	[newLabel setBackgroundColor:[UIColor clearColor]];					// background colour
	[newLabel setTextColor:[UIColor whiteColor]];						// text colour
	[newLabel setHighlightedTextColor:[UIColor whiteColor]];			// text colour when the cell is selected (background goes blue)
	[newLabel setOpaque:YES];											// no need for transparency here
	[newLabel setFont:[UIFont boldSystemFontOfSize:12]];					// and a 18pt font
	[newLabel setTextAlignment:NSTextAlignmentCenter];					// right align the text (so it sits against the right side)
	[newLabel setLineBreakMode:NSLineBreakByTruncatingHead];			// truncate at the start
	[newLabel setAdjustsFontSizeToFitWidth:YES];						// adjust the font size down
	//[newLabel setMinimumFontSize:10];
	[newLabel setNumberOfLines:2];
	
	return newLabel;
}

//
// Create a new name label
//
- (UILabel *)newLabelForName
{
	// position it
	CGRect frame = CGRectMake(10, 2, 220, 22);
	UILabel *newLabel = [[UILabel alloc] initWithFrame:frame];
	
	// make some happy happy changes
	[newLabel setBackgroundColor:[UIColor clearColor]];					// background colour
	[newLabel setTextColor:[UIColor blackColor]];						// text colour
	[newLabel setHighlightedTextColor:[UIColor whiteColor]];			// text colour when the cell is selected (background goes blue)
	[newLabel setOpaque:YES];											// no need for transparency here
	[newLabel setFont:[UIFont boldSystemFontOfSize:14]];				// and a 16pt font
	[newLabel setTextAlignment:NSTextAlignmentLeft];					// right align the text (so it sits against the right side)
	[newLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];		// Let the UI automatically resize the width when necessary (ie editing)
	
	return newLabel;
}

//
// Create a new route description label
//
- (UILabel *)newLabelForRouteDescription
{
	// position it
	CGRect frame = CGRectMake(10, 23, 220, 18);
	UILabel *newLabel = [[UILabel alloc] initWithFrame:frame];
	
	// make some happy happy changes
	[newLabel setBackgroundColor:[UIColor clearColor]];					// background colour
	[newLabel setTextColor:[UIColor grayColor]];						// text colour
	[newLabel setHighlightedTextColor:[UIColor whiteColor]];			// text colour when the cell is selected (background goes blue)
	[newLabel setOpaque:NO];											// no need for transparency here
	[newLabel setFont:[UIFont systemFontOfSize:13]];					// and a 14pt font
	[newLabel setTextAlignment:NSTextAlignmentLeft];					// right align the text (so it sits against the right side)
	[newLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];		// Let the UI automatically resize the width when necessary (ie editing)
	
	return newLabel;
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:animated];
	
	//[disclosure setImage:[UIImage imageNamed:(selected ? @"DepartureDisclosureSelected.png" : @"DepartureDisclosure.png")]];

    // Configure the view for the selected state
}




@end
