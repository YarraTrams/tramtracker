//
//  FavouriteGroupsController.h
//  tramTRACKER
//
//  Created by Robert Amos on 21/07/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FavouriteGroupsController : UITableViewController {
	NSArray *groups;
}

- (id)initWithGroups:(NSArray *)newGroups;
- (UIView *)tableFooterView;

- (void)addGroup:(NSString *)newGroupName;
- (void)changeGroupName:(NSString *)existingGroupName toName:(NSString *)newGroupName;
- (void)saveGroups;

@end
