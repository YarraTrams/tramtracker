//
//  PredictionListView.m
//  tramTRACKER
//
//  Created by Robert Amos on 11/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "PredictionListView.h"


@implementation PredictionListView

@synthesize currentStop, filter, cellBeingEdited, showMessages;

- (id)initWithStyle:(UITableViewStyle)style stop:(Stop *)stop filter:(Filter *)aFilter showMessagesType:(NSInteger)messages delegate:(PIDViewControllerOld *)theDelegate
{
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    if (self = [super initWithStyle:style]) {
		[self setCurrentStop:stop];
		[self setFilter:aFilter];
        delegate = theDelegate;
		[self setShowMessages:messages];
		[self setHidesBottomBarWhenPushed:NO];
        //NSLog(@"[PredictionListView] Opening, filter: %@", aFilter);
    }
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];

	// Change our Background color
	[self.tableView setBackgroundColor:[UIColor colorWithRed:TTBackgroundColorRed green:TTBackgroundColorGreen blue:TTBackgroundColorBlue alpha:1.0]];
	
	// set our title
	[self setTitle:[self.currentStop shortName]];

	// add ourselves a done button
	UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(flipToPIDView)];
	[self.navigationItem setRightBarButtonItem:button animated:NO];
}

// Sometimes on 2.2.1 devices we end up off the top somewhere, correct that if it happens.
- (void)viewDidAppear:(BOOL)animated
{
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 2;
    //return 4;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *TextCellIdentifier = @"TextDetailCell";
	static NSString *SwitchCellIdentifier = @"SwitchCell";
	SimpleCell *textCell;
	SimpleCell *switchCell;

	// set the text on the cell
	switch ([indexPath indexAtPosition:0]) {

		case 0: {

			// Filter options
			switch ([indexPath indexAtPosition:1])
			{
				case 0:
				{
					// get a text accessory cell
					textCell = (SimpleCell *)[tableView dequeueReusableCellWithIdentifier:TextCellIdentifier];
					if (textCell == nil) {
						textCell = [[SimpleCell alloc] initWithCellStyle:TTSimpleCellStyleValue1 reuseIdentifier:TextCellIdentifier];
					}
					
					NSString *routeLabel = NSLocalizedString(@"filter-route-allroutes", @"All Routes");
					if (self.filter.route != nil)
						routeLabel = [NSString stringWithFormat:NSLocalizedString(@"filter-route-template-short", @"Route x"), self.filter.route.number];
					
					[textCell setTextLabelText:NSLocalizedString(@"pid-options-filter-route", @"Show")];
					[textCell setDetailTextLabelText:routeLabel];
					[textCell setSelectionStyle:UITableViewCellSelectionStyleBlue];
					[textCell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
					
					return textCell;
				}
					break;
				case 1:
				{
					switchCell = (SimpleCell *)[tableView dequeueReusableCellWithIdentifier:SwitchCellIdentifier];
					if (switchCell == nil)
					{
						switchCell = [[SimpleCell alloc] initWithCellStyle:TTSimpleCellStyleDefault reuseIdentifier:SwitchCellIdentifier];
						[switchCell setSelectionStyle:UITableViewCellSelectionStyleNone];
						UISwitch *switchButton = [[UISwitch alloc] initWithFrame:CGRectZero];
						[switchButton setOn:self.filter.lowFloor animated:NO];
						[switchButton addTarget:self action:@selector(toggleLowFloorTramFilter:) forControlEvents:UIControlEventValueChanged];
						[switchCell setAccessoryView:switchButton];
						[switchCell setTextLabelText:NSLocalizedString(@"pid-options-filter-lowfloor", @"Low Floor Only")];
					}
					return switchCell;
				}
					break;
			}
			break;
		
		}
		case 1:
			
			// View Options
			switch ([indexPath indexAtPosition:1])
			{
				case 0:
					// get a text accessory cell
					textCell = (SimpleCell *)[tableView dequeueReusableCellWithIdentifier:TextCellIdentifier];
					if (textCell == nil) {
						textCell = [[SimpleCell alloc] initWithCellStyle:TTSimpleCellStyleValue1 reuseIdentifier:TextCellIdentifier];
					}

					[textCell setSelectionStyle:UITableViewCellSelectionStyleNone];
					[textCell setTextLabelText:NSLocalizedString(@"pid-options-view-direction", @"Direction Text")];
					[textCell setDetailTextLabelText:[self.currentStop displayedCityDirection]];
					[textCell setAccessoryType:UITableViewCellAccessoryNone];
					return textCell;

					break;
				case 1:
					// get a text accessory cell
					textCell = (SimpleCell *)[tableView dequeueReusableCellWithIdentifier:TextCellIdentifier];
					if (textCell == nil) {
						textCell = [[SimpleCell alloc] initWithCellStyle:TTSimpleCellStyleValue1 reuseIdentifier:TextCellIdentifier];
					}
					
					[textCell setSelectionStyle:UITableViewCellSelectionStyleBlue];
					[textCell setTextLabelText:NSLocalizedString(@"settings-displayopts-showmessages", @"Show Messages")];

					if (self.showMessages == PIDShowMessagesNone)
						[textCell setDetailTextLabelText:NSLocalizedString(@"settings-showmessages-never", @"Never")];
					else if (self.showMessages == PIDShowMessagesAlways)
						[textCell setDetailTextLabelText:NSLocalizedString(@"settings-showmessages-always", @"Always")];
					else if (self.showMessages == PIDShowMessagesOnce)
						[textCell setDetailTextLabelText:NSLocalizedString(@"settings-showmessages-once", @"Once")];
					else
						[textCell setDetailTextLabelText:NSLocalizedString(@"settings-showmessages-twice", @"Twice")];

					[textCell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
					return textCell;
					
			}
			break;
	}
	
    
	return nil;
}

// Get the header of the first section
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	if (section == 0)
		return NSLocalizedString(@"pid-options-filter", @"Filter Results");

	if (section == 1)
		return NSLocalizedString(@"pid-options-viewoptions", @"View Options");

	return nil;
}



// When they select a row
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

	// selecting the route
	if (indexPath.section == 0 && indexPath.row == 0)
		[self pushRoutePicker];
	
	// toggling low floor only
	else if (indexPath.section == 0 && indexPath.row == 1)
		return;

	// editing the direction text?
	else if (indexPath.section == 1 && indexPath.row == 0)
		[self editTextAtIndexPath:indexPath];
	
	// changing the show messages option?
	else if (indexPath.section == 1 && indexPath.row == 1)
		[self pushChangeShowMessages];

}

#pragma mark -
#pragma mark Editing the direction text

// Edit the direction text
- (void)editTextAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
	if (!cell) return;

	// first thing we need to do is create a UITextField over the top of the UILabel
	CGRect labelFrame = cell.detailTextLabel.frame;
	CGFloat newWidth = 150;
	labelFrame.origin.x -= newWidth - labelFrame.size.width;
	labelFrame.origin.y = 11;
	labelFrame.size.height -= 11;
	labelFrame.size.width = newWidth;

	// create a UITextField at that frame
	UITextField *field = [[UITextField alloc] initWithFrame:labelFrame];
	
	// set the text
	[field setText:cell.detailTextLabel.text];
	[field setTextColor:cell.detailTextLabel.textColor];
	[field setBackgroundColor:[UIColor whiteColor]];
	[field setTextAlignment:NSTextAlignmentRight];

	// and what happens when they finish typing
	[field setReturnKeyType:UIReturnKeyDone];
	[field setDelegate:self];
	
	// add it to the view
	[cell.contentView addSubview:field];
	
	// make it active
	[field becomeFirstResponder];
}

// when they've hit the return button
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	// save the new text
	[[StopList sharedManager] setDirectionText:textField.text forStopID:self.currentStop.trackerID];
	
	// dismiss the keyboard
	[textField resignFirstResponder];

	// remove the text field
	[textField removeFromSuperview];
	
	// reload the table
	[self.tableView reloadData];
	
	return YES;
}

#pragma mark -
#pragma mark Change Results Filter

//
// Toggle the low floor tram filter
//
- (void)toggleLowFloorTramFilter:(id)sender
{
	// we do the opposite of whats there
	if (self.filter == nil)
		[self setFilter:[Filter filterForRoute:nil lowFloor:NO]];

	[self.filter setLowFloor:!(self.filter.lowFloor)];
}

//
// Change the show messages option
//
- (void)pushChangeShowMessages
{
	ShowMessagesSetting *m = [[ShowMessagesSetting alloc] initWithShowMessages:self.showMessages];
	[self.navigationController pushViewController:m animated:YES];
}

- (void)pushRoutePicker
{
	// push the route picker
	RouteSelectionController *picker = [[RouteSelectionController alloc] initWithRoutes:self.currentStop.routesThroughStop
																			  trackerID:self.currentStop.trackerID
																				 target:self
																				 action:@selector(setSelectedRouteFromPicker:)];
	if (self.filter.route == nil)
		[picker setSelectedRouteIndex:-1];
	else
		[picker setSelectedRouteIndex:[self.currentStop.routesThroughStop indexOfObject:self.filter.route.number]];
	[picker setDisplayAllRoutesOption:YES];
	
	[self.navigationController pushViewController:picker animated:YES];
}

/**
 * Set the selected route
**/
- (void)setSelectedRouteFromPicker:(NSString *)routeIndex
{
	if (self.filter == nil)
		[self setFilter:[Filter filterForRoute:nil lowFloor:NO]];

	if ([routeIndex isEqualToString:@"-1"])
		[self.filter setRoute:nil];
	else
		[self.filter setRoute:[[RouteList sharedManager] routeForRouteNumber:routeIndex]];

	[self.tableView reloadData];
}

#pragma mark -
#pragma mark Back to PID View

//
// Flip back to the PID View
//
- (void)flipToPIDView
{
    if (delegate != nil)
    {
        //NSLog(@"[PredictionListView] Closing, filter: %@, PID: %@", self.filter, delegate);
        
        [delegate setFilter:self.filter];
        [delegate setShowMessages:self.showMessages];
        [delegate clearPage];
        [delegate hideScreen];
        [delegate showLoading];
        [delegate resume];
        
        [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
    }
}

#pragma mark -
#pragma mark Departure Lists

- (void)pushScheduledList
{
	DepartureListController *departures = [[DepartureListController alloc] initWithScheduledListForStop:self.currentStop];
	[self.navigationController pushViewController:departures animated:YES];
}



@end

