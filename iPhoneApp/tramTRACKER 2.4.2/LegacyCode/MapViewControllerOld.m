//
//  MapViewController.m
//  tramTRACKER
//
//  Created by Robert Amos on 19/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "MapViewControllerOld.h"
#import "StopList.h"
#import "Stop.h"
#import "StopListController.h"
#import "TicketRetailer.h"
#import "TicketRetailerAnnotationView.h"
#import "NearbyViewControllerOld.h"
#import "TicketRetailerViewController.h"
#import <objc/runtime.h>
#import "UIDevice-Hardware.h"
#import "Constants.h"
#import "TTTools.h"

@interface MapViewControllerOld()

@property (weak, nonatomic) IBOutlet UIButton   * locateMeButton;
@property (strong, nonatomic) NSArray           * data;
@property (nonatomic) StopRightMenuType       filterType;

@end

@implementation MapViewControllerOld

- (void)viewDidLoad
{
	[super viewDidLoad];

	[self setListViewType:self.listViewType];

	/* Center everything on the map */

	if (self.listViewType == MapViewControllerTypeOldNearbyList)
	{
		// default to the region that was saved.
		NSUserDefaults  * defaults = [NSUserDefaults standardUserDefaults];
		NSDictionary    * nearbyMapLastRegion = [defaults dictionaryForKey:@"nearbyMapLastRegion"];
		
		// Create an MKCoordinateRegion from all that
		CLLocationCoordinate2D centreCoord;

		centreCoord.latitude = [[nearbyMapLastRegion objectForKey:@"latitude"] doubleValue];
		centreCoord.longitude = [[nearbyMapLastRegion objectForKey:@"longitude"] doubleValue];

		MKCoordinateRegion defaultCentre = MKCoordinateRegionMake(centreCoord,
																  MKCoordinateSpanMake([[nearbyMapLastRegion objectForKey:@"latitudeDelta"] doubleValue], [[nearbyMapLastRegion objectForKey:@"longitudeDelta"] doubleValue]));
        self.mapView.region = defaultCentre;

        // Show the refresh button
        self.locateMeButton.hidden = NO;
	}
    else if (self.stopList.count > 0)
		[self.mapView setRegion:[self regionThatFitsStops:self.stopList]];
	else
		[self.mapView setCenterCoordinate:CLLocationCoordinate2DMake(0, 0)];


	// if its a connection map then highlight the current stop
	if ((self.listViewType == MapViewControllerTypeOldConnectionList || self.listViewType == MapViewControllerTypeOldSelectedStop || self.listViewType == MapViewControllerTypeOldNearbyTickets) && self.selectedStop != nil)
		self.selectedStopShouldBeShown = YES;
	
	// add the map to the view
	[self.mapView addAnnotations:self.stopList];
	
//	// show the tickets button on everything except the "none" map type, and the nearby map type
//	if (self.listViewType == MapViewControllerTypeOldSelectedStop ||
//        self.listViewType == MapViewControllerTypeOldConnectionList ||
//        self.listViewType == MapViewControllerTypeOldNearbyTickets)
//	{
//		// we need to show the tickets button
//		UIBarButtonItem *tickets = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"nearby-button-tickets", @"Tickets")
//																	style:UIBarButtonItemStylePlain
//																   target:self
//																   action:@selector(toggleTicketRetailers)];
//		[self setTicketsButton:tickets];
//		[self.navigationItem setRightBarButtonItem:tickets];
//	}

	// if we're pushing to scheduled departures, it means ONLY scheduled departures
	if (![self pushToScheduledDepartures])
	{
		// register to be notified when someone has touched our callout
		NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
		[nc addObserver:self
			   selector:@selector(didTouchRealTimeCalloutAccessory:)
				   name:@"TTDidTouchRealTimeCalloutAccessory"
				 object:nil];
	}
	
	// but we always show the scheduled departures icon
	// register to be notified when someone has touched our callout
	NSNotificationCenter *ncs = [NSNotificationCenter defaultCenter];
	[ncs addObserver:self
            selector:@selector(didTouchScheduledCalloutAccessory:)
                name:@"TTDidTouchScheduledCalloutAccessory"
              object:nil];
    
	// register for ticket notifications
	NSNotificationCenter *nct = [NSNotificationCenter defaultCenter];
	[nct addObserver:self
            selector:@selector(didTouchTicketCalloutAccessory:)
                name:@"TTDidTouchTicketCalloutAccessory"
              object:nil];

}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	// display the button?
	if (self.listViewType == MapViewControllerTypeOldNearbyList)
	{
		//[self.nearbyViewController.navigationItem setLeftBarButtonItem:self.stopButton animated:YES];
		// set a timer to display results
		[NSTimer scheduledTimerWithTimeInterval:kNearby_Threshold_200M target:self selector:@selector(stopTracking) userInfo:nil repeats:NO];
		[NSTimer scheduledTimerWithTimeInterval:kNearby_Threshold_500M target:self selector:@selector(stopTracking) userInfo:nil repeats:NO];
		[NSTimer scheduledTimerWithTimeInterval:kNearby_Threshold_MAX target:self selector:@selector(stopTracking) userInfo:nil repeats:NO];
	}
	[self.mapView setShowsUserLocation:YES];
}

// The view is going away
- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	
	// location manager in use?
	if (self.locationManager != nil)
	{
		[self.locationManager stopUpdatingLocation];
		[self setLocationManager:nil];
	}
	
	// save our current centre back to the defaults
	[self saveCurrentMapRegion];
}

//
// Find a coordinate region that fits all of our stops
- (MKCoordinateRegion)regionThatFitsStops:(NSArray *)stops
{
	Stop *stop = [stops objectAtIndex:0];
	MKCoordinateSpan span;
	CLLocationCoordinate2D center;
	
	// only a single stop?
	if (stops.count == 1)
	{
		// center on the selected stop
		center.latitude = stop.location.coordinate.latitude;
		center.longitude = stop.location.coordinate.longitude;
		
		// and the default span
		span.latitudeDelta = kDefaultLatDelta;
		span.longitudeDelta = kDefaultLonDelta;

	// multiple stops, find the region that will fit them all
	}
    else
	{
		CLLocationDegrees northernBoundary, southernBoundary, easternBoundary, westernBoundary;

		// set the initial boundaries to the first stop
		northernBoundary = stop.location.coordinate.latitude;
		southernBoundary = northernBoundary;
		westernBoundary = stop.location.coordinate.longitude;
		easternBoundary = westernBoundary;
		
		// loop over our stops and find the boundaries
		for (Stop *stop in stops)
		{
			// use latitude to find north/south boundaries
			if (stop.location.coordinate.latitude > northernBoundary)
				northernBoundary = stop.location.coordinate.latitude;
			else if (stop.location.coordinate.latitude < southernBoundary)
				southernBoundary = stop.location.coordinate.latitude;
			
			// and longitude for east/west
			if (stop.location.coordinate.longitude > westernBoundary)
				westernBoundary = stop.location.coordinate.longitude;
			else if (stop.location.coordinate.longitude < easternBoundary)
				easternBoundary = stop.location.coordinate.longitude;
		}
		
		// so we should know our span now between the boundaries
		span.latitudeDelta = (northernBoundary - southernBoundary);
		span.longitudeDelta = (westernBoundary - easternBoundary);
		
		// so now we find the center by using the lower boundary and adding half the span
		center.latitude = southernBoundary + (span.latitudeDelta / 2);
		center.longitude = easternBoundary + (span.longitudeDelta / 2);
	}
	
	// all done!
	return MKCoordinateRegionMake(center, span);
}

- (void)saveCurrentMapRegion
{
	// save our current centre back to the defaults
	if (self.listViewType == MapViewControllerTypeOldNearbyList)
	{
		MKCoordinateRegion region = [self.mapView region];
		NSDictionary *nearbyMapLastRegion = [NSDictionary dictionaryWithObjectsAndKeys:
											 [NSNumber numberWithDouble:region.center.latitude], @"latitude",
											 [NSNumber numberWithDouble:region.center.longitude], @"longitude",
											 [NSNumber numberWithDouble:region.span.latitudeDelta], @"latitudeDelta",
											 [NSNumber numberWithDouble:region.span.longitudeDelta], @"longitudeDelta",
											 nil];
		
		// save that to the defaults
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		[defaults setObject:nearbyMapLastRegion forKey:@"nearbyMapLastRegion"];
		[defaults synchronize];
	}
}

// when someone touches our callout accessory
- (void)didTouchRealTimeCalloutAccessory:(NSNotification *)notification
{
	// save the map region
	[self saveCurrentMapRegion];
    [self.tabBarController.parentViewController performSegueWithIdentifier:kSegueShowPID sender:notification.object];
}

//- (void)pushPIDForStop:(Stop *)aStop
//{
//	// make a PID for that stop!
//	// Start the pid view
//	PIDViewControllerOld *PID = nil;
//    if ([[UIScreen mainScreen] bounds].size.height == 568) {
//        PID = [[PIDViewControllerOld alloc] initWithNibName:@"PIDView-iPhone5" bundle:[NSBundle mainBundle]];
//    }
//    else{
//        PID = [[PIDViewControllerOld alloc] initWithNibName:@"PIDView" bundle:[NSBundle mainBundle]];
//    }
//	
//	// get the stop info
//	[PID startWithStop:aStop];
//	
//	// push it onto the stack
//	if (self.nearbyViewController != nil)
//		[self.nearbyViewController.navigationController pushViewController:PID animated:YES];
//	else
//		[self.navigationController pushViewController:PID animated:YES];
//}
//

- (void)didTouchScheduledCalloutAccessory:(NSNotification *)notification
{
	/* Save the map region */
	[self saveCurrentMapRegion];
	
	/* The stop is the note's object */
	Stop *stop = notification.object;

//	DepartureListController *departures = [[DepartureListController alloc] initWithScheduledListForStop:stop];
//	if (self.route != nil)
//		[departures setSelectedRoute:self.route.number];
//	else
//		[departures setSelectedRoute:[stop.routesThroughStop objectAtIndex:0]];
//	
//	
//	if (self.arrivalDate != nil)
//		[departures setScheduledDepartureTime:self.arrivalDate];
	
//	if (self.nearbyViewController != nil)
//		[self.nearbyViewController.navigationController pushViewController:departures animated:YES];
//	else
//		[self.navigationController pushViewController:departures animated:YES];
}

- (void)didTouchTicketCalloutAccessory:(NSNotification *)notification
{
	/* Save the map region */
	[self saveCurrentMapRegion];
	
	// The ticket retailer is the note's object
	TicketRetailer *retailer = notification.object;

	
    
    
//	// Create the view
//	TicketRetailerViewController *retailerViewController = [[TicketRetailerViewController alloc] initWithTicketRetailer:retailer];
//
//	if (self.nearbyViewController != nil)
//		[self.nearbyViewController.navigationController pushViewController:retailerViewController animated:YES];
//	else 
//		[self.navigationController pushViewController:retailerViewController animated:YES];
//	
//	 retailerViewController = nil;
}

///**
// * Override the list view type to actually show/hide the button
//**/
//- (void)setListViewType:(NSInteger)type
//{
//	// take care of setting the property
//	_listViewType = type;
//	
////	// if there is no navigation controller yet then we aren't on the stack so don't change anything
////	if (self.navigationController == nil)
////		return;
////
////	// change the bar button depending on what it is
////	if (listViewType == MapViewControllerTypeOldNone || listViewType == MapViewControllerTypeOldConnectionList || listViewType == MapViewControllerTypeOldSelectedStop || listViewType == MapViewControllerTypeOldNearbyTickets)
////		[self.navigationItem setRightBarButtonItem:nil animated:NO];
////
//
//}

#pragma mark MKMapViewDelegate methods

//
// Return a pin to place on the map for a stop
//
- (MKAnnotationView *)mapView:(MKMapView *)map viewForAnnotation:(id <MKAnnotation>)annotation
{
	// is it the user location one? use the default
	if ([annotation isKindOfClass:[MKUserLocation class]])
		return nil;
	
	// is it a stop?
	if ([annotation isKindOfClass:[Stop class]])
	{
		static NSString *annotationIdentifier = @"StopPin";
		
		// create a stop pin
		StopAnnotationView *pin = (StopAnnotationView *)[map dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];
		if (pin == nil)
			pin = [[StopAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"moo"];
        
		// if its on the purple stop list then use purple!
		if (self.purpleStops != nil && [self.purpleStops containsObject:annotation])
		{
			[pin setPurplePin:YES];
			[pin setPinColor:MKPinAnnotationColorPurple];
		}
        else
		{
			// Green PIN (too much green? maybe)
			[pin setPurplePin:NO];
			[pin setPinColor:MKPinAnnotationColorGreen];
		}
		
		// make sure we show the accessories
		[pin setShouldShowScheduledAccessory:YES];
		[pin setShouldShowRealTimeAccessory:(![self pushToScheduledDepartures])];

		// animate placing it on the map because it looks pretty
		//[pin setAnimatesDrop:YES];
		
		// let them clicky the name to view a PID
		[pin setCanShowCallout:YES];

		return pin;
	}
	
	// is it a ticket retailer?
	if ([annotation isKindOfClass:[TicketRetailer class]])
	{
		static NSString *annotationIdentifier = @"TicketRetailerPin";
		
		// create a ticket retailer pin
		TicketRetailerAnnotationView *pin = (TicketRetailerAnnotationView *)[map dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];
		if (pin == nil)
			pin = [[TicketRetailerAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationIdentifier];
		else
			[pin setAnnotation:annotation];

		
		return pin;
	}
	
	return nil;
}

- (NSArray *)filterArrayWithFilterType:(NSArray *)items
{
    switch (self.filterType) {
        case StopRightMenuShelter:
            return [items filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(isShelterStop = YES) OR (isYtShelterStop = YES)"]];
            
        case StopRightMenuAccess:
            return [items filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"isEasyAccessStop = YES"]];
            
        default:
            return items;
    }
}

- (void)mapView:(MKMapView *)map regionDidChangeAnimated:(BOOL)animated
{
	CLLocation *loc = [[CLLocation alloc] initWithLatitude:map.centerCoordinate.latitude longitude:map.centerCoordinate.longitude];
	NSInteger maxStopCount = 100;

	if (self.listViewType == MapViewControllerTypeOldNearbyList ||
        self.listViewType == MapViewControllerTypeOldNone ||
        self.listViewType == MapViewControllerTypeOldConnectionList ||
        self.listViewType == MapViewControllerTypeOldSelectedStop ||
        self.listViewType == MapViewControllerTypeOldNearbyTickets)
	{
		NSArray         * stopsInRegion;
		
		// performance. if its smaller than where 100 stops would fit in then go with the region direct
		if (map.region.span.latitudeDelta <= 0.01796550 && map.region.span.longitudeDelta <= 0.01982689)
			stopsInRegion = [[StopList sharedManager] getStopsInRegion:map.region];
		else
		{
			// for the lower performing devices we need to lower our limit
			UIDevicePlatform platform = [[UIDevice currentDevice] platformType];
			switch (platform)
			{
				case UIDevice1GiPhone:
				case UIDevice1GiPod:
				case UIDevice2GiPod:
				case UIDevice3GiPhone:
				case UIDevice3GSiPhone:
					maxStopCount = 20;
                default:
                    break;
			}
			
			stopsInRegion = [[StopList sharedManager] getNearestStopsWithoutDistancesToLocation:loc
                                                                                          count:(self.showTicketRetailers ? maxStopCount / 2 : maxStopCount)];
		}
        
        /* Filter with current filter */
        stopsInRegion = [self filterArrayWithFilterType:stopsInRegion];

        NSPredicate * relativeComplementPredicateAdd = [NSPredicate predicateWithFormat:@"NOT SELF IN %@", self.stopList];
        NSPredicate * relativeComplementPredicateDel = [NSPredicate predicateWithFormat:@"NOT SELF IN %@", stopsInRegion];

        /* Add new stops */
		[map addAnnotations:[stopsInRegion filteredArrayUsingPredicate:relativeComplementPredicateAdd]];

        /* Remove out of region stops */
        [map removeAnnotations:[self.stopList filteredArrayUsingPredicate:relativeComplementPredicateDel]];

        self.stopList = stopsInRegion;
	}

	// Now we do the same thing for the ticket retailers, if they're shown
	if (self.showTicketRetailers)
	{
		NSMutableArray *retailersToAdd = [[NSMutableArray alloc] initWithCapacity:0];
		NSMutableArray *retailersToRemove = [[NSMutableArray alloc] initWithCapacity:0];
		NSArray *ticketRetailersInRegion;

		// performance. if its smaller than where 100 retailers would fit in then go with the region direct
		if (map.region.span.latitudeDelta <= 0.01796550 && map.region.span.longitudeDelta <= 0.01982689)
			ticketRetailersInRegion = [TicketRetailer ticketRetailersInRegion:map.region];
		else
			ticketRetailersInRegion = [TicketRetailer nearestTicketRetailersWithoutDistancesToLocation:loc count:maxStopCount/2];
		
		
		// find ones that we haven't displayed already
		for (TicketRetailer *r in ticketRetailersInRegion)
		{
			if (self.retailerList == nil || ![self.retailerList containsObject:r])
				[retailersToAdd addObject:r];
		}
		[map addAnnotations:retailersToAdd];
		
		// now that we've added the extra ones, remove the ones that are outside the list
		if (self.retailerList != nil)
		{
			for (TicketRetailer *r in self.retailerList)
			{
				if (![ticketRetailersInRegion containsObject:r])
					[retailersToRemove addObject:r];
			}
			if ([retailersToRemove count] > 0)
				[map removeAnnotations:retailersToRemove];
		}
		
		[self setRetailerList:ticketRetailersInRegion];
	}
	
	
	
}

- (void)mapViewWillStartLoadingMap:(MKMapView *)aMapView
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

- (void)mapViewDidFinishLoadingMap:(MKMapView *)aMapView
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

	// if its a connection map then highlight the current stop
	if ((self.listViewType == MapViewControllerTypeOldConnectionList || self.listViewType == MapViewControllerTypeOldSelectedStop || self.listViewType == MapViewControllerTypeOldNearbyTickets) && self.selectedStop != nil && self.selectedStopShouldBeShown)
	{
		[self.mapView selectAnnotation:self.selectedStop animated:NO];
		self.selectedStopShouldBeShown = NO;
	}
}

- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
	if (self.listViewType == MapViewControllerTypeOldNearbyList && self.trackUserLocation)
	{
		// Set the location
		CLLocation *newLocation = userLocation.location;
		
		// get the 10 nearest stops
		NSArray *nearestStops = [[StopList sharedManager] getNearestStopsWithoutDistancesToLocation:newLocation count:10];
		MKCoordinateRegion newRegion = [self.mapView regionThatFits:[self regionThatFitsStops:nearestStops]];
		
		// re-centre the map thingo
		[self.mapView setRegion:newRegion animated:YES];
		
		// What sort of accuracy do we need for here?
		NSTimeInterval secondsPassed = [[NSDate date] timeIntervalSinceDate:self.locatingStarted];
		NSInteger requiredAccracy = secondsPassed > kNearby_Threshold_500M ? 500 : (secondsPassed > kNearby_Threshold_200M ? 200 : 50);
		
		// check this location for accuracy
		if (self.manuallyTracking && newLocation.horizontalAccuracy <= requiredAccracy)
		{
			//NSLog(@"Required accuracy of %dm has been met (%.2fm). Stopping tracking %@", requiredAccracy, newLocation.horizontalAccuracy, newLocation);
			[self stopTracking];
			
		} else
		{
			//NSLog(@"Ignoring location %@ because accuracy %.2fm does not meet required accuracy of %dm", newLocation, newLocation.horizontalAccuracy, requiredAccracy);
		}
	}
}

// CLLocationManagerDelete wrapper!
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
	// we're just a wrapper, call the normal map view one
	[self mapView:self.mapView didUpdateUserLocation:self.mapView.userLocation];
}

- (void)stopTracking
{
    self.trackUserLocation = NO;
}

- (IBAction)startTracking
{
    self.manuallyTracking = self.trackUserLocation = YES;
	
	// if we have a known location, move there immediately
	if (self.mapView.userLocation != nil)
		[self mapView:self.mapView didUpdateUserLocation:self.mapView.userLocation];
}

#pragma mark -
#pragma mark Ticket Retailer Methods

- (void)displayTicketRetailers
{
	// do we have a cache?
	if (self.retailerList != nil)
	{
		// re-add these to the map immediately
		[self.mapView addAnnotations:self.retailerList];
	}
	
	// initiate a re-draw of the current set, nothing should change on the stop side, but the retailers might
	[self mapView:self.mapView regionDidChangeAnimated:NO];
}

- (void)hideTicketRetailers
{
	// clear the retailer annotations from the map
	if (self.retailerList != nil)
		[self.mapView removeAnnotations:self.retailerList];

	// then refresh
	[self mapView:self.mapView regionDidChangeAnimated:NO];
	
	// we don't clear the retailer list array, we keep it cached for later
	// it will be removed automatically if we need to clean up some memory
}

- (void)setShowTicketRetailers:(BOOL)shouldShowTicketRetailers
{
	if ((_showTicketRetailers = shouldShowTicketRetailers))
		[self displayTicketRetailers];
	else
		[self hideTicketRetailers];
}

- (void)toggleTicketRetailers
{
	// show or hide?
	if (self.showTicketRetailers)
	{
		// Currently showing ticket retailers, lets hide them
		[self setShowTicketRetailers:NO];
//		[self.ticketsButton setStyle:UIBarButtonItemStylePlain];
	} else
	{
		// Currently not showing ticket retailers, show them
		[self setShowTicketRetailers:YES];
//		[self.ticketsButton setStyle:UIBarButtonItemStyleDone];
	}
}


#pragma mark -
#pragma mark Housekeeping Methods

- (void)didReceiveMemoryWarning
{
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

	// Release any cached data, images, etc that aren't in use.
	if (!self.showTicketRetailers)
		[self setRetailerList:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - NearbyRightFilterDelegate

- (void)didSelectStopAction:(StopRightMenuType)filterType
{
//    NSPredicate     * predicate = [TTTools predicateForNearbyType:filterType string:nil];
//    
//    self.filterType = filterType;
//    self.data = [self.stopList filteredArrayUsingPredicate:predicate];
}

#pragma mark - SetCurrentJourneyDelegate

- (void)setFinalJourney:(Journey *)journey
{
    self.stopList = [[StopList sharedManager] getStopListForTrackerIDs:(journey.tram.upDirection ?
                                                                            journey.tram.route.upStops :
                                                                            journey.tram.route.downStops)];
}

@end
