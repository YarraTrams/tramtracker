//
//  OnboardListCell.m
//  tramTRACKER
//
//  Created by Robert Amos on 18/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "OnboardListCell.h"

const NSInteger OnboardCellStyleNone = 0;
const NSInteger OnboardCellStyleMinutesOnRight = 1;
const NSInteger OnboardCellStyleMinutesOnRightDisabled = 2;
const NSInteger OnboardCellStyleMinutesOnRightLowAccuracy = 3;

@implementation OnboardListCell

@synthesize name, arrival, trackSegmentView, connectingTrain, connectingTram, connectingBus, destinationStop,
			highlightedStop, style, turnIndicator, turnMessage, turnMessageLabel, delegate, alarmIndicator;

- (id)initWithFrame:(CGRect)frame reuseIdentifier:(NSString *)reuseIdentifier cellStyle:(NSInteger)cellStyle {
    if (self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier]) {
        // Initialization code
		[self setStyle:cellStyle];
		
		// set the background view
		GradientBackgroundCell *background = [[GradientBackgroundCell alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) cellStyle:cellStyle];
		[self setBackgroundView:background];
		
		// and the stop name
		name = [self newLabelForName];
		[self.contentView addSubview:self.name];

		// and the arrival time
		if (cellStyle != OnboardCellStyleNone)
		{
			arrival = [self newLabelForArrival];
			[self.contentView addSubview:self.arrival];
		}

		// and the track segment
		[self.contentView addSubview:self.trackSegmentView];
		
		// and the connection icons
		[self drawConnectionIcons];
		[self drawAlarmIndicator];
    }
    return self;
}


//
// Create a new arrival label
//
- (UILabel *)newLabelForArrival
{
	// position it
	CGRect frame = CGRectMake(self.frame.size.width-58, 0, 56, 44);
	UILabel *newLabel = [[UILabel alloc] initWithFrame:frame];
	
	// make some happy happy changes
	[newLabel setBackgroundColor:[UIColor clearColor]];					// background colour
	[newLabel setTextColor:[UIColor whiteColor]];						// text colour
	[newLabel setHighlightedTextColor:[UIColor whiteColor]];			// text colour when the cell is selected (background goes blue)
	[newLabel setOpaque:YES];											// no need for transparency here
	[newLabel setFont:[UIFont boldSystemFontOfSize:18]];					// and a 18pt font
	[newLabel setTextAlignment:NSTextAlignmentCenter];					// right align the text (so it sits against the right side)
	[newLabel setLineBreakMode:NSLineBreakByTruncatingHead];			// truncate at the start
	[newLabel setAdjustsFontSizeToFitWidth:YES];
    //[newLabel setMinimumFontSize:12];
	[newLabel setShadowColor:[UIColor colorWithWhite:0.14 alpha:1.0]];
	[newLabel setShadowOffset:CGSizeMake(0, -1)];
	
	return newLabel;
}

//
// Create a new name label
//
- (UILabel *)newLabelForName
{
	// position it
	CGRect frame = CGRectMake(50, 0, self.style != OnboardCellStyleNone ? 200 : 260, 44);
	UILabel *newLabel = [[UILabel alloc] initWithFrame:frame];
	
	// make some happy happy changes
	[newLabel setBackgroundColor:[UIColor clearColor]];					// background colour
	[newLabel setTextColor:[UIColor blackColor]];						// text colour
	[newLabel setHighlightedTextColor:[UIColor whiteColor]];			// text colour when the cell is selected (background goes blue)
	[newLabel setOpaque:YES];											// no need for transparency here
	[newLabel setFont:[UIFont boldSystemFontOfSize:14]];				// and a 16pt font
	[newLabel setTextAlignment:NSTextAlignmentLeft];					// right align the text (so it sits against the right side)
	[newLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];		// Let the UI automatically resize the width when necessary (ie editing)
	
	return newLabel;
}


// Draw the image views with the connection icons on the right hand side
- (void)drawConnectionIcons
{
	// draw the connecting train icon
	UIImageView *trainIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"connecting_train.png"]];
	[trainIcon setFrame:CGRectMake(4, 2, trainIcon.image.size.width, trainIcon.image.size.height)];
	//[trainIcon setFrame:CGRectMake(self.frame.size.width-60-2-trainIcon.image.size.width, 2, trainIcon.image.size.width, trainIcon.image.size.height)];
	[self setConnectingTrain:trainIcon];
	[trainIcon setHidden:YES];
	[self.contentView addSubview:trainIcon];

	// draw the connecting tram icon
	UIImageView *tramIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"connecting_tram.png"]];
	[tramIcon setFrame:CGRectMake(4, 4 + trainIcon.image.size.height, tramIcon.image.size.width, tramIcon.image.size.height)];
	//[tramIcon setFrame:CGRectMake(self.frame.size.width-60-2-tramIcon.image.size.width, 4 + trainIcon.image.size.height, tramIcon.image.size.width, tramIcon.image.size.height)];
	[self setConnectingTram:tramIcon];
	[tramIcon setHidden:YES];
	[self.contentView addSubview:tramIcon];

	// draw the connecting bus icon
	UIImageView *busIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"connecting_bus.png"]];
	[busIcon setFrame:CGRectMake(4, 6 + trainIcon.image.size.height + tramIcon.image.size.height, busIcon.image.size.width, busIcon.image.size.height)];
	//[busIcon setFrame:CGRectMake(self.frame.size.width-60-2-busIcon.image.size.width, 6 + trainIcon.image.size.height + tramIcon.image.size.height, busIcon.image.size.width, busIcon.image.size.height)];
	[self setConnectingBus:busIcon];
	[busIcon setHidden:YES];
	[self.contentView addSubview:busIcon];
	
	// and release
}

// display an arrival alarm icon
- (void)drawAlarmIndicator
{
	CGFloat x = (self.style == OnboardCellStyleNone ? self.frame.size.width-18 : self.frame.size.width-78);
	UIImageView *indicator = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"AlarmIconSmall.png"]];
	CGRect f = indicator.frame;
	f.origin.x = x;
	f.origin.y = 24;
	[indicator setFrame:f];
	[indicator setHidden:YES];
	[self setAlarmIndicator:indicator];
	[self.contentView addSubview:indicator];
	 indicator = nil;
}

// Get a new turn indicator
- (UIButton *)newTurnIndicator
{
	// get an empty UIImageView
	CGFloat width = (self.style == OnboardCellStyleNone ? self.frame.size.width-34 : self.frame.size.width-94);
	UIButton *indicator = [UIButton buttonWithType:UIButtonTypeCustom];
	[indicator setFrame:CGRectMake(width, 7, 30, 30)];
	[indicator addTarget:self action:@selector(toggleTurnMessage) forControlEvents:UIControlEventTouchUpInside];
	return indicator;
}

- (void)setTrackSegmentView:(UIView *)newTrackSegmentView
{
    // retain this, and add it to the bottom of the view stack
    [self insertSubview:newTrackSegmentView atIndex:1];

    // remove the existing one
    if (trackSegmentView != nil)
    {
        [trackSegmentView removeFromSuperview];
    }
    
    // set it
    trackSegmentView = newTrackSegmentView;
}

- (void)setTurnIndicatorImage:(UIImage *)indicator withMessage:(NSString *)message
{
	// no indicator holder yet?
	if (self.turnIndicator == nil)
	{
		// don't do anything if its nil
		if (indicator == nil)
			return;
		
		turnMessage = [self newTurnMessage];
		[self.contentView addSubview:self.turnMessage];

		turnMessageLabel = [self newTurnMessageLabel];
		[self.turnMessage addSubview:self.turnMessageLabel];
		[self.turnMessage setHidden:YES];
		
		// and the turning indicators
		turnIndicator = [self newTurnIndicator];
		[self.contentView addSubview:self.turnIndicator];
	}
	
	// set the turning message
	[self.turnMessageLabel setText:message];
	
	// if theres an indicator, resize the stop name
	CGRect frame = self.name.frame;
	CGRect alarmFrame = self.alarmIndicator.frame;
	if (indicator != nil && [self.turnIndicator imageForState:UIControlStateNormal] == nil)
	{
		frame.size.width -= 38;
		alarmFrame.origin.x -= 34;
	} else if (indicator == nil && [self.turnIndicator imageForState:UIControlStateNormal] != nil)
	{
		frame.size.width += 38;
		alarmFrame.origin.x += 34;
	}
	[self.name setFrame:frame];
	[self.alarmIndicator setFrame:alarmFrame];

	// set the image
	[self.turnIndicator setImage:indicator forState:UIControlStateNormal];
	
	// show/hide as appropriate
	if (indicator == nil)
	{
		[self.turnIndicator setHidden:YES];
		[self.turnMessage setHidden:YES];
	} else
		[self.turnIndicator setHidden:NO];
}

// The turn message background
- (UIImageView *)newTurnMessage
{
	UIImageView *message = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Turn Background.png"]];
	CGRect frame = message.frame;
	frame.origin.x = 3;
	frame.origin.y = 2;
	[message setFrame:frame];
	return message;
}

// The turn message label
- (UILabel *)newTurnMessageLabel
{
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5, 2, 215, 36)];
	[label setTextColor:[UIColor whiteColor]];
	[label setBackgroundColor:[UIColor clearColor]];
	[label setOpaque:NO];
	[label setFont:[UIFont boldSystemFontOfSize:14]];
	//[label setMinimumFontSize:12];
	[label setAdjustsFontSizeToFitWidth:YES];
	[label setTextAlignment:NSTextAlignmentCenter];
	[label setLineBreakMode:NSLineBreakByWordWrapping];
	[label setNumberOfLines:2];
	[label setText:@"Some text here"];
	return label;
}

// Show the turning message
- (void)showTurnMessage
{
	[self.turnMessage setHidden:NO];
}

// Hide the turning message
- (void)hideTurnMessage
{
	[self.turnMessage setHidden:YES];
}

// Toggle the showing/hiding of the turning message
- (void)toggleTurnMessage
{
	if ([self.turnMessage isHidden])
	{
		[self showTurnMessage];

		// hide all others
		if (delegate != nil && [delegate respondsToSelector:@selector(showTurnIndicatorOnlyForCell:)])
			[delegate performSelector:@selector(showTurnIndicatorOnlyForCell:) withObject:self];
		
	} else
	{
		[self hideTurnMessage];
		
		// hide this turn message
		if (delegate != nil && [delegate respondsToSelector:@selector(hideTurnIndicatorForCell:)])
			[delegate performSelector:@selector(hideTurnIndicatorForCell:) withObject:self];
	}
}

// highlight this cell, or not
- (void)setHighlightedStop:(BOOL)shouldHighlight
{
	// save it
	highlightedStop = shouldHighlight;
	
	// show/hide the background view's highlight layer appropriately
	GradientBackgroundCell *b = (GradientBackgroundCell *)self.backgroundView;
	[b.highlightLayer setHidden:!shouldHighlight];
}

// highlight this cell, or not
- (void)setDestinationStop:(BOOL)isDestination
{
	// save it
	destinationStop = isDestination;
	
	// show/hide the background view's highlight layer appropriately
	GradientBackgroundCell *b = (GradientBackgroundCell *)self.backgroundView;
	[b.destinationHighlightLayer setHidden:!isDestination];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}




@end


//
// Section Headers
//
@implementation OnboardListSectionHeaderView

@synthesize imageView, title;

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        // Initialization code

		// white background!
		[self setBackgroundColor:[UIColor whiteColor]];
		
		// placeholder imageview on the left
		UIImageView *i = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 60, frame.size.height)];
		[self setImageView:i];
		
		// placeholder text label
		title = [self newLabelForTitleWithFrame:frame];
		
		// add these to the view
		[self addSubview:self.imageView];

		// and paint a nice grey square over it
		OnboardListSectionHeaderMask *overlayMask = [[OnboardListSectionHeaderMask alloc] initWithFrame:frame];
		[self addSubview:overlayMask];

		// add the title
		[self addSubview:self.title];
	}
    return self;
}

// Text label
- (UILabel *)newLabelForTitleWithFrame:(CGRect)frame
{
	// position it
	UILabel *newLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, frame.size.width-20, frame.size.height)];
	
	// make some happy happy changes
	[newLabel setBackgroundColor:[UIColor clearColor]];					// background colour
	[newLabel setTextColor:[UIColor whiteColor]];						// text colour
	[newLabel setHighlightedTextColor:[UIColor whiteColor]];			// text colour when the cell is selected (background goes blue)
	[newLabel setOpaque:NO];											// transparent is good
	[newLabel setFont:[UIFont boldSystemFontOfSize:18]];					// and a 18pt font
	[newLabel setTextAlignment:NSTextAlignmentCenter];					// right align the text (so it sits against the right side)
	[newLabel setLineBreakMode:NSLineBreakByTruncatingHead];			// truncate at the start
	
	return newLabel;
}


@end

@implementation OnboardListSectionHeaderMask

- (id)initWithFrame:(CGRect)frame
{
	if (self = [super initWithFrame:frame])
	{
		// we're not opaque
		[self setOpaque:NO];
		
		// our background is a transparent black
		[self setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.4]];
	}
	return self;
}


@end

