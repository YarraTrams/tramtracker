//
//  OLInterstitialViewController.h
//  OtherLevels
//
//  Copyright (c) 2015 OtherLevels. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OtherLevels.h"

@interface OLInterstitialViewController : UIViewController<UIWebViewDelegate, InterstitialInfo>

+ (void)getInterstitial:(NSString *)placement;

@property (nonatomic, assign) BOOL isInterstitialContentLoaded;
@property (nonatomic, assign) BOOL isInterstitialLinkClicked;

#pragma mark InterstitialInfo protocol

// Do not change these properties as it is used in passing data from
// the OtherLevels Library, so that you have access to the contents of the Interstitial
@property (strong) NSString *htmlContent;
@property (strong) NSString *pHash;
@property (strong) NSDictionary *messageContent;

@end
