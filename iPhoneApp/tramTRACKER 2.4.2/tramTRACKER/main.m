//
//  main.m
//  tramTracker
//
//  Created by Hugo Cuvillier on 25/01/2014.
//  Copyright (c) 2014 AppsCore. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "tramTRACKERAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([tramTRACKERAppDelegate class]));
    }
}
