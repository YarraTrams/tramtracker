//
//  FeaturePopup.m
//  tramTracker
//
//  Created by Jonathan Head on 21/08/2015.
//  Copyright (c) 2015 AppsCore. All rights reserved.
//

#import "FeaturePopup.h"
#import "TTActivityIndicatorView.h"

NSString* kLastFeaturePopupDate = @"LastFeaturePopupDate";

@interface FeaturePopup()
@property (weak, nonatomic) IBOutlet UIImageView* yarraTramsLogo;
@property (weak, nonatomic) IBOutlet UIImageView* ptvLogo;
@property (weak, nonatomic) IBOutlet UIScrollView* webViewContainer;
@property (weak, nonatomic) IBOutlet UIPageControl* pageControl;
@property (weak, nonatomic) IBOutlet TTActivityIndicatorView* indicator;
@property (weak, nonatomic) IBOutlet UIView* indicatorContainer;
@property (weak, nonatomic) IBOutlet UILabel* failedToLoadLabel;
@property (nonatomic) NSInteger loadingWebViews;

- (IBAction)onContinue:(UIButton*)sender;
- (IBAction)onRemindMeLater:(UIButton*)sender;
@end

@implementation FeaturePopup

+ (FeaturePopup*)createAndShowPopupOverView:(UIView*)view
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    double lastDisplayTime = [defaults doubleForKey:kLastFeaturePopupDate];
    const NSTimeInterval secondsPerDay = 60*60*24;
    if([NSDate date].timeIntervalSince1970 - lastDisplayTime < secondsPerDay)
    { return nil; }
    
    FeaturePopup* popup = [[NSBundle mainBundle] loadNibNamed:@"FeaturePopup" owner:self options:nil][0];
    popup.frame = view.bounds;
    [popup layoutIfNeeded];
    popup.alpha = 0.0;
    [view addSubview:popup];
    [view bringSubviewToFront:popup]; // Probably not necessary. Good to do all the same, in case the default subview adding behaviour changes in later iOS versions.
    [UIView animateWithDuration:0.5f animations:^{ popup.alpha = 1.0; }];
    popup.webViewContainer.pagingEnabled = YES;
    return popup;
}

- (void)dismissPopup
{
    [UIView animateWithDuration:0.2 animations:^{
        self.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)setFeatures:(NSArray *)features
{
    self.indicator = [TTActivityIndicatorView activityIndicatorForView:self.indicatorContainer animated:NO];
    [self.indicator startAnimatingAnimated:NO];
    
    NSMutableArray* sortedFeatures = [[features sortedArrayUsingComparator:^NSComparisonResult(NSDictionary* firstDictionary, NSDictionary* secondDictionary) {
        NSNumber* firstSort = firstDictionary[@"sortOrder"];
        NSNumber* secondSort = secondDictionary[@"sortOrder"];
        return [firstSort compare:secondSort];
    }] mutableCopy];
    _features = [FeaturePopup purgeSeenFeatures:sortedFeatures];
    
    
    if(self.webViewContainer == nil)
    {
        NSLog(@"Attempted to set features for feature popup, but the container was not properly initalised");
        return;
    }

    self.loadingWebViews = self.features.count;
    
    UIWebView* previousWebView;
    CGSize size = self.webViewContainer.frame.size;
    self.webViewContainer.contentSize = CGSizeMake(size.width*self.features.count, size.height);
    for(NSInteger index = 0; index < self.features.count; index++)
    {
        // Create and initialise the content.
        NSDictionary* featureInfo = self.features[index];
        UIWebView* webView = [[UIWebView alloc] initWithFrame:CGRectMake(index*size.width, 0, size.width, size.height)];
        NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:featureInfo[@"url"]]];
        webView.delegate = self;
        [webView loadRequest:request];

        [self.webViewContainer addSubview:webView];
        [self.webViewContainer addConstraints:@[
           [NSLayoutConstraint constraintWithItem:webView attribute:NSLayoutAttributeWidth  relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:size.width],
           [NSLayoutConstraint constraintWithItem:webView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:size.height]
        ]];
        
        NSMutableArray* constraintsGroup = [NSMutableArray array];
        [constraintsGroup addObject:[NSLayoutConstraint constraintWithItem:self.webViewContainer attribute:NSLayoutAttributeTop    relatedBy:NSLayoutRelationEqual toItem:webView attribute:NSLayoutAttributeTop    multiplier:1.0 constant:0.0]];
        [constraintsGroup addObject:[NSLayoutConstraint constraintWithItem:self.webViewContainer attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:webView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
        
        if(index == 0)
        { [constraintsGroup addObject:[NSLayoutConstraint constraintWithItem:self.webViewContainer attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:webView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.0]]; }
        else
        { [constraintsGroup addObject:[NSLayoutConstraint constraintWithItem:previousWebView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:webView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.0]]; }
        if(index == self.features.count-1)
        { [constraintsGroup addObject:[NSLayoutConstraint constraintWithItem:self.webViewContainer attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:webView attribute:NSLayoutAttributeRight multiplier:1.0 constant:0.0]]; }
        
        [self.webViewContainer addConstraints:constraintsGroup];
        
        previousWebView = webView;
    }
    self.pageControl.numberOfPages = self.features.count;
}

// This forces responsive websites to snap to the width of the web view (instead of the width of the device). It's a bit hacky, but it works.
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.querySelector('meta[name=viewport]').setAttribute('content', 'width=%d;', false); ", (int)webView.frame.size.width]];
    [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitUserSelect='none';"];
    [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitTouchCallout='none';"];
    self.loadingWebViews--;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    self.loadingWebViews--;

    NSString* failureHtml = [NSString stringWithFormat:@"<head><style type=\"text/css\">div { width:%fpx;height:%fpx;text-align:center;line-height:%fpx } body{font-family:%@;font-size:15}</style></head><body><div>Unable to load content.</div></body></html>", webView.frame.size.width-10, webView.frame.size.height-10, webView.frame.size.width-10, @"HelveticaNeue-Light"];//[UIFont systemFontOfSize:12].familyName];
    [webView loadHTMLString:failureHtml baseURL:nil];
    webView.userInteractionEnabled = NO;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat width = self.webViewContainer.frame.size.width;
    self.pageControl.currentPage = (self.webViewContainer.contentOffset.x / width);
}

- (void)setLoadingWebViews:(NSInteger)loadingWebViews
{
    _loadingWebViews = loadingWebViews;
    if(loadingWebViews <= 0)
    {
        [self.indicator stopAnimating];
        self.indicator.hidden = YES;
        self.webViewContainer.hidden = NO;
    }
    else
    {
        self.indicator.hidden = NO;
        self.webViewContainer.hidden = YES;
    }
}

+ (NSMutableArray*)purgeSeenFeatures:(NSMutableArray*)featureList
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray* seenIDs = [defaults objectForKey:@"seenIDs"];
    if(seenIDs == nil)
    { seenIDs = [NSMutableArray array]; }
    
    for(int index = 0; index < featureList.count; index++)
    {
        NSDictionary* feature = featureList[index];
        if([seenIDs containsObject:feature[@"id"]])
        {
            [featureList removeObjectAtIndex:index];
            index--;
            continue;
        }
        
        BOOL isValidDeviceType = [feature[@"deviceType"] isEqualToNumber:@1] || [feature[@"deviceType"] isEqualToNumber:@2];
        if(!isValidDeviceType)
        {
            [featureList removeObjectAtIndex:index];
            index--;
            continue;
        }
    }
    
    return featureList;
}

+ (BOOL)shouldShowFeaturePopup:(NSMutableArray*)featureList
{
    NSMutableArray* purgedFeatureList = [FeaturePopup purgeSeenFeatures:featureList];
    return (purgedFeatureList.count > 0);
}

- (void)setAllTutorialsShown
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray* seenIDs = [[defaults objectForKey:@"seenIDs"] mutableCopy];
    if(seenIDs == nil)
    { seenIDs = [NSMutableArray array]; }
    for(NSDictionary* featureInfo in self.features)
    {
        if(![seenIDs containsObject:featureInfo[@"id"]])
        { [seenIDs addObject:featureInfo[@"id"]]; }
    }
    
    [defaults setObject:seenIDs forKey:@"seenIDs"];
    [defaults synchronize];
}

- (IBAction)pageChanged:(UIPageControl*)sender
{ [self.webViewContainer scrollRectToVisible:CGRectMake(self.pageControl.currentPage*self.webViewContainer.frame.size.width, 0, self.webViewContainer.frame.size.width, self.webViewContainer.frame.size.height) animated:YES]; }

/// Dismisses the popup. All the tutorials currently being displayed should be flagged as 'have seen', and not be shown again.
- (void)onContinue:(UIButton *)sender
{
    [self setAllTutorialsShown];
    [self dismissPopup];
}

/// Dismisses the popup. None of the displayed tutorials should be flagged (however, those which were already flagged should remain flagged).
- (void)onRemindMeLater:(UIButton *)sender
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setDouble:[NSDate date].timeIntervalSince1970 forKey:kLastFeaturePopupDate];
    [defaults synchronize];
    [self dismissPopup];
}

@end
