//
//  RouteCell.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 9/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Route.h"

@interface RouteCell : UITableViewCell

- (void)configureWithText:(NSString *)aText;

@end
