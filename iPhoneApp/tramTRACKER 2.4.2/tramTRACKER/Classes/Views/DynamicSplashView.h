//
//  DynamicSplashView.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 24/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DynamicSplashView : UIView

+ (DynamicSplashView *)splashWithFrame:(CGRect)frame;
- (void)runUpdatesAndDisappear;
@end
