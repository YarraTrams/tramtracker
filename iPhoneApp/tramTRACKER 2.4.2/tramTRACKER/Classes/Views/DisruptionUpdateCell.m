//
//  PIDUpdatesCell.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 21/11/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "DisruptionUpdateCell.h"
#import "SpecialMessage.h"

#import <QuartzCore/QuartzCore.h>

@interface DisruptionUpdateCell()

@property (weak, nonatomic) IBOutlet UIImageView    * expandCollapseImage;
@property (weak, nonatomic) IBOutlet UITextView     * textArea;

@end

/*
 * Cell Identifiers
 */

NSString * const kCellDisruptionMinimal = @"DisruptionMinimalCell";
NSString * const kCellDisruptionFull    = @"DisruptionFullCell";
NSString * const kCellUpdateMinimal     = @"UpdateMinimalCell";
NSString * const kCellUpdateFull        = @"UpdateFullCell";

@implementation DisruptionUpdateCell

- (void)configureWithMessage:(NSString *)message
{
    self.textArea.text = message;
    self.textArea.layer.cornerRadius = 5.0f;
    
    if ([self.reuseIdentifier rangeOfString:@"Full"].location != NSNotFound)
    {
        [self setAccessibilityLabel:message];
        [self.textArea setAccessibilityLabel:message];
    }
}

@end
