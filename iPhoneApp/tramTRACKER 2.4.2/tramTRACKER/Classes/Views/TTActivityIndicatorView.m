//
//  TTActivityIndicatorView.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 6/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "TTActivityIndicatorView.h"
#import "ActivityIndicator.h"

#import "tramTRACKERAppDelegate.h"

@interface TTActivityIndicatorView()

@property (weak, nonatomic) IBOutlet ActivityIndicator * imageView;
@property (weak, nonatomic) UIView *mySuper;

@end

@implementation TTActivityIndicatorView

static NSString * const kAnimationImagePrefix = @"Tracking-Animation";

+ (TTActivityIndicatorView *)activityIndicatorForView:(UIView *)view animated:(BOOL)animated
{
    TTActivityIndicatorView * instance;

    if (!instance)
    {
        instance = [[NSBundle mainBundle] loadNibNamed:@"TTActivityIndicatorView" owner:self options:nil][0];
        instance.backgroundColor = [UIColor whiteColor];
    }
    
    if (!view)
    {
        tramTRACKERAppDelegate * delegate = (id)[[UIApplication sharedApplication] delegate];
        instance.frame = delegate.window.frame;
        [delegate.window addSubview:instance];
        instance.mySuper = delegate.window;
    }
    else
    {
        instance.frame = CGRectMake(0, 0, view.frame.size.width, view.frame.size.height);
        [view addSubview:instance];
        [view layoutIfNeeded];
        instance.mySuper = view;
    }

    [UIView animateWithDuration:animated ? 0.5 : 0.01 animations:^{
        instance.alpha = 1.0f;
    }];
    [instance.imageView startAnimating];
    return instance;
}

- (void)stopAnimating
{
    [self stopAnimatingAnimated:NO];
}

- (void)startAnimatingAnimated:(BOOL)animated
{
    [self.imageView startAnimating];
    [self.mySuper addSubview:self];
    [UIView animateWithDuration:animated ? 0.5 : 0.01 animations:^
     {
         self.alpha = 1.0;
     }];
}

- (void)stopAnimatingAnimated:(BOOL)animated
{
    if (self.imageView.isAnimating) {
        [UIView animateWithDuration:animated ? 0.5 : 0.01 animations:^
         {
             self.alpha = 0.0;
         } completion:^(BOOL finished)
         {
             [self removeFromSuperview];
             [self.imageView stopAnimating];
         }];
    }
}

@end
