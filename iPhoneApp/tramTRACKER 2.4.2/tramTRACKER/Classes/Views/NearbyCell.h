//
//  NearbyCell.h
//  tramTRACKER
//
//  Created by Robert Amos on 4/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@class StopDistance;
@class TicketRetailerDistance;
@class PointOfInterest;

@interface NearbyCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView * image;

@property (nonatomic, weak) IBOutlet UILabel * distance;
@property (nonatomic, weak) IBOutlet UILabel * name;
@property (nonatomic, weak) IBOutlet UILabel * routeDescription;

- (void)configureWithStop:(Stop *)aStop andEasyAccess:(BOOL)hasEasyAccess andShelter:(BOOL)hasShelter;
- (void)configureWithTR:(TicketRetailer *)aTicketRetailer;
- (void)configureWithPOI:(PointOfInterest *)aPOI;

- (void)configureWithTicketRetailer:(TicketRetailer *)aTicketRetailer distance:(NSString *)distance;
- (void)configureWithStopDistance:(StopDistance *)aStopDistance;
- (void)configureWithTicketRetailerDistance:(TicketRetailerDistance *)aTicketRetailerDistance;

@end
