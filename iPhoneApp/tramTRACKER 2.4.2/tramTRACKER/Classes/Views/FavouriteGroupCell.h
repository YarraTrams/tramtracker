//
//  FavouriteGroupCell.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 16/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FavouriteSection;

@interface FavouriteGroupCell : UITableViewCell

- (void)configureWithString:(NSString *)string isSelected:(BOOL)isSelected;

@end
