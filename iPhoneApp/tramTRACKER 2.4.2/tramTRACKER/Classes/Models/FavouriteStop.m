//
//  FavouriteStop.m
//  tramTracker
//
//  Created by Hugo Cuvillier on 8/05/2014.
//  Copyright (c) 2014 AppsCore. All rights reserved.
//

#import "FavouriteStop.h"

@implementation FavouriteStop

- (instancetype)initWithStop:(Stop *)stop filter:(Filter *)filter indexPath:(NSIndexPath *)indexPath name:(NSString *)name {
    if (self = [super init]) {
        self.filter = filter;
        self.stop = stop;
        self.indexPath = indexPath;
        self.name = name;
    }
    return self;
}

- (NSComparisonResult)compareWithFavouriteStop:(FavouriteStop *)otherStop
{
	return [self.indexPath compare:otherStop.indexPath];
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"%@ with filter %@ at %@", self.stop, self.filter, self.indexPath];
}

@end
