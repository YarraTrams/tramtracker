//
//  Filter.h
//  tramTRACKER
//
//  Created by Robert Amos on 24/04/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Route;

/**
 * Represents a filter as applied to the results in a PID
**/
@interface Filter : NSObject {

	/**
	 * Whether to show only low floor trams
	**/
	BOOL lowFloor;
	
	/**
	 * Only show trams travelling on this route. Set to nil for all routes.
	**/
	Route *route;
}

@property (nonatomic) BOOL lowFloor;
@property (nonatomic, strong) Route *route;

/**
 * Returns a filter object configured for the specific route and low floor options
 *
 * @param	aRoute			A Route object
 * @param	lowFloorOnly	Only show low floor trams?
 * @return					An auto-released filter object
**/
+ (Filter *)filterForRoute:(Route *)aRoute lowFloor:(BOOL)lowFloorOnly;

/**
 * Creates a filter object from the specified favourites dictionary.
 *
 * @param	favourite		A NSDictionary describing a favourite
 * @return					An auto-released filter object
**/
+ (Filter *)filterFromDictionary:(NSDictionary *)favourite;

/**
 * Creates an empty filter object (all routes, all trams)
**/
+ (Filter *)emptyFilter;

/**
 * Creates a NSDictionary favourite object describing this filter.
**/
- (NSDictionary *)dictionary;

/**
 * Compares whether one filter is the same as another filter.
 *
 * @param	otherFilter			Another Filter object
 * @return						Whether they are considiered to be equal
**/
- (BOOL)isEqualToFilter:(Filter *)otherFilter;

/**
 * Compares this filter to an empty filter (all routes, all trams)
**/
- (BOOL)isEmpty;

@end
