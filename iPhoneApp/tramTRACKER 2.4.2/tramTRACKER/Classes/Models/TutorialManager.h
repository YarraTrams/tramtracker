//
//  TutorialManager.h
//  tramTracker
//
//  Created by Jonathan Head on 25/08/2015.
//  Copyright (c) 2015 AppsCore. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMPopTip.h"

typedef enum : NSUInteger {
    /// The first PID message. This indicates to users how to switch to sorting by ETA.
    TutorialPIDSortByMinutes,
    /// The second PID message. This indicates to users how to switch to sorting by routes.
    TutorialPIDSortByRoutes,
    /// The third PID message. This indicates to users how to set a Alarm.
    TutorialPIDTramArrivalAlarm,
    /// The first Settings message. This indicates to users that they should switch on disruptions.
    TutorialSettingsActivateDisruptions,
    /// The second Settings message. This indicates to users that they should go to the disruptions configuration page.
    TutorialSettingsGotoDisruptions,
    /// The third Settings message. This indicates to users that they should select a time on the disruptions screen.
    TutorialSettingsSelectDisruptionTime,
    /// The fourth Settings message. This indicates to users that they should select a route on the disruptions screen.
    TutorialSettingsSelectDisruptionRoute,
    /// The fifth Settings message. This indicates to users that they should save their disruption changes.
    TutorialSettingsSaveDisruptions,
    /// The sixth Settings message. This indicates to users that they should go to the Alarm configuration page.
    TutorialSettingsManageAlarms,
    /// The seventh Settings message. This indicates to users how to delete an alarm from the Alarm configuration page. If no alarms are set, this will be skipped.
    TutorialSettingsDeleteAlarm,
    /// The seventh Settings message. This indicates to users to go back from the Alarm configuration page.
    TutorialSettingsBackFromAlarms,
    /// The myTram tutorial. Explains how to set a Alarm.
    TutorialMyTram,
    /// The myTram tutorial. A special condition for when there is no cell that can be swiped currently visible.
    TutorialMyTramNoEntry,
    /// The Timetables tutorial. Explains how to set a Alarm.
    TutorialTimetables,
    /// Indicates no tutorial. Only used as a default setting for the current tutorial states.
    TutorialNone
} TutorialMessage;

typedef enum : NSUInteger {
    TutorialScreenPID,
    TutorialScreenSettings,
    TutorialScreenMyTram,
    TutorialScreenTimetables
} TutorialScreen;

typedef enum : NSUInteger {
    /// A tag representing the alert shown at the beginning of the tutorial. The user's action on this will determine whether to show the tutorial (and flag it as shown) or not.
    TutorialAlertStart = 1,
    /// A tag representing the alert shown at the end of the tutorial. This should not need to respond to user action.
    TutorialAlertEnd   = 2,
} TutorialAlertTag;

@interface TutorialTableView : UITableView
@end


@interface TutorialManager : NSObject
+ (TutorialManager*)instance;
- (void)setAllTutorialsShown:(BOOL)shown;
- (BOOL)shouldShowTutorial:(TutorialScreen)type;
- (BOOL)allTutorialsShown;
- (void)setTutorialShown:(TutorialScreen)type wasSeen:(BOOL)wasSeen;
- (void)showTutorial:(TutorialMessage)type container:(UIView*)container alertDelegate:(id<UIAlertViewDelegate>)alertDelegate frame:(CGRect)frame dismiss:(void(^)(void))dismissHandler;
- (void)hideTutorial:(TutorialScreen)type;
- (void)updateTutorialShouldShowFirstTime:(BOOL)bValue;
- (BOOL)tutorialShouldShowFirstTime;
- (UIAlertView*)alertForTutorialStart:(TutorialScreen)type;
- (UIAlertView*)alertForTutorialEnd:(TutorialScreen)type;

@property (nonatomic) TutorialMessage currentTutorialPID;
@property (nonatomic) TutorialMessage currentTutorialMyTram;
@property (nonatomic) TutorialMessage currentTutorialScheduled;
@property (nonatomic) TutorialMessage currentTutorialSettings;
@end
