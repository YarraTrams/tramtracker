//
//  Location.m
//  tramTRACKER
//
//  Created by Robert Amos on 20/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import <objc/message.h>

#import "Location.h"


@implementation LocationManager

+ (CLLocationDistance)distanceFromLocation:(CLLocation *)from toLocation:(CLLocation *)to
{
	// 3.2 and up use distanceFromLocation:
	return [from distanceFromLocation:to];
}

@end
