//
//  AdManager.m
//  tramTracker
//
//  Created by Jonathan Head on 18/05/2015.
//  Copyright (c) 2015 AppsCore. All rights reserved.
//

#import "AdManager.h"
#import "Constants.h"
#import "OLRichInboxBannerCardViewController.h"

static AdManager* instance;
const NSInteger AdHeight = 49;
@implementation AdManager

+ (AdManager *)instance
{
    static dispatch_once_t createToken;
    dispatch_once(&createToken, ^{
        instance = [AdManager new];
    });
    return instance;
}

+ (BOOL)adsEnabled
{ return [[NSUserDefaults standardUserDefaults] boolForKey:kKillSwitchShowAdvertising]; }

- (instancetype)init
{
    self = [super init];
    if(self)
    { _adControllers = [NSMapTable mapTableWithKeyOptions:NSMapTableWeakMemory valueOptions:NSMapTableStrongMemory]; }
    return self;
}

- (void)createAdsForViewController:(UIViewController *)controller
{
    if([self.adControllers objectForKey:controller] == nil)
    {
        OLRichInboxBannerCardViewController* adController = [OLRichInboxBannerCardViewController new];
        
        [adController attachToBottomOfView:controller];
        [self.adControllers setObject:adController forKey:controller];
    }
    else
    { NSLog(@"Warning: Attempted to add advertising to a view controller that has already been configured with ads."); }
}

- (void)removeAdsForViewController:(UIViewController *)controller
{
    OLRichInboxBannerCardViewController* adController = [self.adControllers objectForKey:controller];
    if(adController != nil)
    {
        [self.adControllers removeObjectForKey:controller];
        [adController.view  removeFromSuperview];
    }
    else
    { NSLog(@"Warning: Attempted to remove advertising from view controller that has not been configured with ads."); }
}

@end

@implementation UIViewController (TramTrackerAds)

/// Returns the constraint that governs the spacing that ads should fit into.
- (NSLayoutConstraint *)adSpacingConstraint
{ return nil; }

- (void)showAds
{
    if([AdManager adsEnabled])
    {
        if(self.adSpacingConstraint)
        {
            self.adSpacingConstraint.constant = AdHeight;
            [self.view layoutSubviews];
        }
        [[AdManager instance] createAdsForViewController:self];
    }
}

- (void)removeAds:(BOOL)updateLayout
{
    if(updateLayout && self.adSpacingConstraint)
    {
        self.adSpacingConstraint.constant = 0;
        [self.view layoutIfNeeded]; // This is causing a crash on the MapViewController - it is sent a release call at the time of being deallocated.
    }
    [[AdManager instance] removeAdsForViewController:self];
}

- (void)respondToKillSwitchUpdate:(NSNotification*)notification
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if([notification.userInfo[kKillSwitchShowAdvertising] boolValue] == YES)
        { [self showAds]; }
        else if([notification.userInfo[kKillSwitchShowAdvertising] boolValue] == NO)
        { [self removeAds:YES]; }
    });
}

@end
