//
//  SpecialMessage.m
//  tramTRACKER
//
//  Created by Robert Amos on 16/03/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "SpecialMessage.h"

NSInteger const SpecialMessageTypeSpecialEvent = 1;
NSInteger const SpecialMessageTypeDisruption = 2;


@implementation SpecialMessage

@synthesize message, type;

+ (SpecialMessage *)specialMessageWithMessage:(NSString *)aMessage type:(NSInteger)aType
{
    SpecialMessage  * specialMessage = [SpecialMessage new];
    
    specialMessage.message = aMessage;
    specialMessage.type = aType;
    return specialMessage;
}

@end
