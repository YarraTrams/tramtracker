//
//  MostRecentContainerViewController.h
//  tramTRACKER
//
//  Created by Raji on 7/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Stop;

@interface MostRecentContainerViewController : UIViewController

- (void)pushToPID:(Stop *)stop;

@end
