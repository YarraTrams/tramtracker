//
//  HelpViewController.m
//  tramTRACKER
//
//  Created by Raji on 23/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <FrameAccessor/FrameAccessor.h>

#import "HelpViewController.h"
#import "HelpWebViewController.h"
#import "Analytics.h"

@interface HelpViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint* tableToBottomConstraint;

@end

NSString* const ControllerNameBase        = @"base";
NSString* const ControllerNamePassenger   = @"passenger";
NSString* const ControllerNameFindStops   = @"findStops";
NSString* const ControllerNameFavourites  = @"favourites";
NSString* const ControllerNameOnBoard     = @"onBoard";
NSString* const ControllerNameMap         = @"map";
NSString* const ControllerNameScheduled   = @"scheduled";
NSString* const ControllerNameTicket      = @"ticket";
NSString* const ControllerNameSettings    = @"settings";
NSString* const ControllerNameDataUpdates = @"dataUpdates";
NSString* const ControllerNameDataUsage   = @"dataUsage";

@implementation HelpViewController

#pragma mark - Inits & Loads

- (NSLayoutConstraint *)adSpacingConstraint
{ return self.tableToBottomConstraint; }

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self showAds];
    
    self.title = @"Help";

    UIBarButtonItem * back = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Nav-Back"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction:)];

    [self.navigationItem setLeftBarButtonItem:back];
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        self.tableView.y = 0 ;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureHelp];
}

- (void)dealloc
{
    self.tableView.delegate   = nil;
    self.tableView.dataSource = nil;
    [self removeAds:NO];
}

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{ return 1; }

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
         if([self.controller isEqualToString:ControllerNameBase])        { return 13; }
    else if([self.controller isEqualToString:ControllerNamePassenger])   { return 5;  }
    else if([self.controller isEqualToString:ControllerNameFindStops])   { return 5;  }
    else if([self.controller isEqualToString:ControllerNameFavourites])  { return 6;  }
    else if([self.controller isEqualToString:ControllerNameOnBoard])     { return 8;  }
    else if([self.controller isEqualToString:ControllerNameMap])         { return 5;  }
    else if([self.controller isEqualToString:ControllerNameScheduled])   { return 5;  }
    else if([self.controller isEqualToString:ControllerNameTicket])      { return 4;  }
    else if([self.controller isEqualToString:ControllerNameSettings])    { return 8;  }
    else if([self.controller isEqualToString:ControllerNameDataUpdates]) { return 1;  }
    else if([self.controller isEqualToString:ControllerNameDataUsage])   { return 3;  }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"%lu", (unsigned long)indexPath.row]];
    
    if(cell == nil)
    {
        // This is a little bit of a hack aimed to simply cell creation. Because this view controller is used for several different tables, and the requirement for ads prevents us from using a
        // UITableViewController, using the row number as the identifier allows us to easily instantiate the cells without having to write identifiers for every cell.
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[NSString stringWithFormat:@"%lu", (unsigned long)indexPath.row]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{ [tableView deselectRowAtIndexPath:indexPath animated:YES]; }

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if (![[segue.destinationViewController class] isSubclassOfClass:[HelpWebViewController class]]) { return; }
    
    NSString * filename = @"";
    NSIndexPath * indexPath;
    
    if([sender isKindOfClass:[UITableViewCell class]])
    { indexPath = [self.tableView indexPathForCell:sender]; }
    
    
    HelpWebViewController * webView = segue.destinationViewController;

    if ([self.controller isEqualToString:ControllerNameBase])
    {
        if(indexPath == 0)
        { filename = @""; }
        
        switch(indexPath.row)
        {
            case 11: filename = @"Beacons.html";        break;
            case 12: filename = @"DataAnalytics.html";  break;
        }
    }
    else if([self.controller isEqualToString:ControllerNamePassenger])
    {
        switch (indexPath.row)
        {
            case 0: filename = @"understanding13.html";  break;
            case 1: filename = @"TramAlarm.html";        break;
            case 2: filename = @"messages15.html";       break;
            case 3: filename = @"menu14.html";           break;
            case 4: filename = @"filter.html";           break;
        }
    }
    else if([self.controller isEqualToString:ControllerNameFindStops])
    {
        switch (indexPath.row)
        {
            case 0: filename = @"browse15.html";       break;
            case 1: filename = @"nearby15.html";       break;
            case 2: filename = @"search15.html";       break;
            case 3: filename = @"most_recent15.html";  break;
            case 4: filename = @"enter_id15.html";     break;
        }
    }
    else if([self.controller isEqualToString:ControllerNameFavourites])
    {
        switch (indexPath.row)
        {
            case 0: filename = @"adding15.html";   break;
            case 1: filename = @"viewing15.html";  break;
            case 2: filename = @"nearest.html";    break;
            case 3: filename = @"filters.html";    break;
            case 4: filename = @"editing12.html";  break;
            case 5: filename = @"groups.html";     break;
        }
    }
    else if([self.controller isEqualToString:ControllerNameOnBoard])
    {
        switch (indexPath.row)
        {
            case 0: filename = @"finding.html";            break;
            case 1: filename = @"understanding15.html";    break;
            case 2: filename = @"StopAlarm.html";          break;
            case 3: filename = @"special_situations.html"; break;
            case 4: filename = @"low_accuracy.html";       break;
            case 5: filename = @"works.html";              break;
            case 6: filename = @"messages.html";           break;
            case 7: filename = @"connections15.html";      break;
        }
    }
    else if([self.controller isEqualToString:ControllerNameMap])
    {
        switch (indexPath.row)
        {
            case 0: filename = @"switching14.html";      break;
            case 1: filename = @"using15.html";          break;
            case 2: filename = @"directions.html";       break;
            case 3: filename = @"pannable14.html";       break;
            case 4: filename = @"ticketoutlets15.html";  break;
        }
    }
    else if([self.controller isEqualToString:ControllerNameScheduled])
    {
        switch (indexPath.row)
        {
            case 0: filename = @"accessing15.html";              break;
            case 1: filename = @"ScheduledDepartureAlarm.html";  break;
            case 2: filename = @"route.html";                    break;
            case 3: filename = @"date.html";                     break;
            case 4: filename = @"connections.html";              break;
        }
    }
    else if([self.controller isEqualToString:ControllerNameTicket])
    {
        switch (indexPath.row)
        {
            case 0: filename = @"browse.html";           break;
            case 1: filename = @"ticketoutlets15.html";  break;
            case 2: filename = @"nearstops.html";        break;
            case 3: filename = @"details.html";          break;
        }
    }
    else if([self.controller isEqualToString:ControllerNameSettings])
    {
        switch (indexPath.row)
        {
            case 0: filename = @"accessing16.html";                       break;
            case 1: filename = @"startup12.html";                         break;
            case 2: filename = @"display_options.html";                   break;
            case 3: filename = @"my%20TramTRACKER%20Notifications.html";  break;
            case 4: filename = @"StopAlarm.html";                         break;
            case 5: filename = @"TramAlarm.html";                         break;
            case 6: filename = @"ManageAlarms.html";                      break;
        }
    }
    else if([self.controller isEqualToString:ControllerNameDataUpdates])
    {
        switch (indexPath.row)
        {
            case 0: filename = @"process14.html"; break;
            case 1: filename = @"sync15.html";    break;
        }
    }
    else if([self.controller isEqualToString:ControllerNameDataUsage])
    {
        switch (indexPath.row)
        {
            case 0: filename = @"pid.html";       break;
            case 1: filename = @"schedules.html"; break;
            case 2: filename = @"onboard.html";   break;
            case 3: filename = @"sync.html";      break;
        }
    }

    [webView setFilename:filename];
}
/*
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{ [tableView deselectRowAtIndexPath:indexPath animated:NO]; }
*/
- (NSString*)stringForCell:(NSInteger)row
{
    if([self.controller isEqualToString:ControllerNameBase])
    {
        switch(row)
        {
            case 0: return @"About tramTRACKER®";
            case 1: return @"Tram Arrival Screen";
            case 2: return @"Finding Stops";
            case 3: return @"Favourites";
            case 4: return @"myTram";
            case 5: return @"Maps";
            case 6: return @"Scheduled Departures";
            case 7: return @"Ticket Outlets";
            case 8: return @"Data Updates";
            case 9: return @"my tramTRACKER";
            case 10: return @"Data Usage";
            case 11: return @"Beacons";
            case 12: return @"Data Analytics";
        }
    }
    else if([self.controller isEqualToString:ControllerNamePassenger])
    {
        switch(row)
        {
            case 0: return @"Understanding Tram Arrival Screen";
            case 1: return @"Tram Alarm";
            case 2: return @"Disruptions and Special Events";
            case 3: return @"Options Menu";
            case 4: return @"Filtering Results";
        }
    }
    else if([self.controller isEqualToString:ControllerNameFindStops])
    {
        switch(row)
        {
            case 0: return @"Browse";
            case 1: return @"Nearby";
            case 2: return @"Search";
            case 3: return @"Most Recent";
            case 4: return @"Enter tracker ID";
        }
    }
    else if([self.controller isEqualToString:ControllerNameFavourites])
    {
        switch(row)
        {
            case 0: return @"Adding Favourites";
            case 1: return @"Viewing Favourites";
            case 2: return @"Nearest Favourite";
            case 3: return @"Favourites and Filter";
            case 4: return @"Editing Favourites";
            case 5: return @"Grouping Favourites";
        }
    }
    else if([self.controller isEqualToString:ControllerNameOnBoard])
    {
        switch(row)
        {
            case 0: return @"Finding Your Tram Number";
            case 1: return @"Understanding myTram";
            case 2: return @"Stop Alarm";
            case 3: return @"Special Situations";
            case 4: return @"Low Accuracy Conditions";
            case 5: return @"How it Works";
            case 6: return @"Disruptions and Special Events";
            case 7: return @"Finding Connections";
        }
    }
    else if([self.controller isEqualToString:ControllerNameMap])
    {
        switch(row)
        {
            case 0: return @"Switching to Map View";
            case 1: return @"Using the Map View";
            case 2: return @"Finding Directions";
            case 3: return @"Pannable Maps";
            case 4: return @"Ticket Outlet";
        }
    }
    else if([self.controller isEqualToString:ControllerNameScheduled])
    {
        switch(row)
        {
            case 0: return @"Accessing Scheduled Departures";
            case 1: return @"Scheduled Departure Alarm";
            case 2: return @"Changing the Route";
            case 3: return @"Changing the Date & Time";
            case 4: return @"Finding Connections";
        }
    }
    else if([self.controller isEqualToString:ControllerNameTicket])
    {
        switch(row)
        {
            case 0: return @"Browsing and Searching";
            case 1: return @"Maps";
            case 2: return @"Outlets Near Stops";
            case 3: return @"Outlet Details";
        }
    }
    else if([self.controller isEqualToString:ControllerNameSettings])
    {
        switch(row)
        {
            case 0: return @"Accessing my tramTRACKER";
            case 1: return @"Default Startup Screen";
            case 2: return @"Display Options";
            case 3: return @"Disruption Notification";
            case 4: return @"Stop Alarm";
            case 5: return @"Tram Alarm";
            case 6: return @"Manage Alarm";
        }
    }
    else if([self.controller isEqualToString:ControllerNameDataUpdates])
    {
        switch(row)
        {
            case 0: return @"Update Process";
            case 1: return @"Update Settings";
        }
    }
    else if([self.controller isEqualToString:ControllerNameDataUsage])
    {
        switch(row)
        {
            case 0: return @"Tram Arrival Screen";
            case 1: return @"Scheduled Departures";
            case 2: return @"myTram";
        }
    }
    return @"";
}




@end
