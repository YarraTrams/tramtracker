//
//  ManageAlarmsViewController.h
//  tramTracker
//
//  Created by Jonathan Head on 16/07/2015.
//  Copyright (c) 2015 AppsCore. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tram.h"
#import "Prediction.h"
#import "CustomIOSAlertView.h"

static const NSInteger ToastMessageDelay = 4;
typedef void(^CompletionBlock)(BOOL);

@interface AlarmCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel* labelTitle;
@property (weak, nonatomic) IBOutlet UILabel* labelSubtitle;
@property (weak, nonatomic) IBOutlet UILabel* labelTime;
@property (weak, nonatomic) IBOutlet UIImageView* imageFTZ;
@end

@interface ManageAlarmsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
/// Creates a new stop Alarm (user is on tram, approaching stop). This should be done from myTRAM screen only.
+ (BOOL)addNotificationForStop:(Stop*)stop tram:(Tram*)tram date:(NSDate*)date;
/// Creates a new tram Alarm (user is at stop, tram is approaching). This should be done from PID or Timetables screens.
+ (BOOL)addNotificationForTram:(Stop*)stop prediction:(Prediction*)prediction timeOffset:(NSTimeInterval)offset;
+ (CustomIOSAlertView *)popupForView:(UIView*)view stop:(Stop*)stop prediction:(Prediction*)prediction times:(NSArray*)times completion:(CompletionBlock)completion isEstimate:(BOOL)isEstimate;
@end
