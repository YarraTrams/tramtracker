//
//  ManageDisruptionsViewController.h
//  tramTracker
//
//  Created by Jonathan Head on 6/08/2015.
//  Copyright (c) 2015 AppsCore. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ManageDisruptionsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>
@end
