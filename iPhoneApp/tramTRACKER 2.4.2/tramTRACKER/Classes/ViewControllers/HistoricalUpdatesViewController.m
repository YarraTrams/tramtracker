//
//  HistoricalUpdatesViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 16/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import "HistoricalUpdatesViewController.h"
#import "HistoricalUpdate.h"

@interface HistoricalUpdatesViewController ()

@property (nonatomic, strong) NSArray   * updates;

@end

@implementation HistoricalUpdatesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.updates = [HistoricalUpdate getHistoricalUpdatesList];
}

- (void)dealloc
{
    self.tableView.delegate   = nil;
    self.tableView.dataSource = nil;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.updates.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.updates[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    NSArray         *data = self.updates[indexPath.section];

    cell.textLabel.text = data[indexPath.row];
    return cell;
}

@end
