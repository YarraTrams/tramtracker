//
//  FavouriteGroupsTableViewController.h
//  tramTRACKER
//
//  Created by Raji on 3/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavouritesViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

typedef enum
{
    FavouritesViewControllerFavourites,
    FavouritesViewControllerManageFavourites,
    FavouritesViewControllerManageGroups,
    ManageAlarmCell
} FavouritesViewControllerType;


- (void)editAction:(id)sender;
- (BOOL)mapAction:(id)sender;

@end
