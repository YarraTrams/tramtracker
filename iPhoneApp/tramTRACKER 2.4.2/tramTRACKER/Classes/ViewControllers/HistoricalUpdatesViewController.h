//
//  HistoricalUpdatesViewController.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 16/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoricalUpdatesViewController : UITableViewController

@end
