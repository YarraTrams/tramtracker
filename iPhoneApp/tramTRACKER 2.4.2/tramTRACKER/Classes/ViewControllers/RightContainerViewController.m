//
//  NearbyRightContainerViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 5/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "RightContainerViewController.h"
#import "NearbyViewController.h"
#import "RouteContainerViewController.h"
#import "PIDViewController.h"

@class NearbyViewController;

typedef enum
{
    NearbyRightContainerViewControllerNearby = 0,
    NearbyRightContainerViewControllerPID = 1,
    NearbyRightContainerViewControllerRoutes = 2
}       NearbyRightContainerViewControllerType;

@interface RightContainerViewController ()

@property (weak, nonatomic) IBOutlet UIView         * fakeStatusBar;
@property (weak, nonatomic) UITabBarController      * childTabBarViewController;
@property (weak, nonatomic) UITabBarController      * mainTabBar;
@property (weak, nonatomic) IBOutlet UIImageView    * image;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageToTop;

@end

@implementation RightContainerViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    UINavigationController      * selectedNavigationController = (id)self.mainTabBar.selectedViewController;
    
    if (selectedNavigationController.topViewController.class == [NearbyViewController class])
        [self.childTabBarViewController setSelectedIndex:NearbyRightContainerViewControllerNearby];

    else if (selectedNavigationController.topViewController.class == [RouteContainerViewController class])
        [self.childTabBarViewController setSelectedIndex:NearbyRightContainerViewControllerRoutes];

    else if (selectedNavigationController.topViewController.class == [PIDViewController class])
        [self.childTabBarViewController setSelectedIndex:NearbyRightContainerViewControllerPID];

    [UIView animateWithDuration:0.5f animations:^{
        self.fakeStatusBar.alpha = 1.0f;
    }];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [UIView animateWithDuration:0.5f animations:^{
        self.fakeStatusBar.alpha = 0.0f;
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.childTabBarViewController = self.childViewControllers.firstObject;
    self.mainTabBar = self.parentViewController.childViewControllers.firstObject;

    if (!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        [self.fakeStatusBar removeFromSuperview];
        
        self.imageToTop.constant = 0.0;
        [self.view layoutIfNeeded];
    }
}

@end
