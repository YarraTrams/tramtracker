//
//  SettingsViewController.m
//  tramTRACKER
//
//  Created by Raji on 19/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "SettingsViewController.h"
#import "OpenWithViewController.h"
#import "Settings.h"
#import "Constants.h"
#import "Analytics.h"
#import "SectionHeaderView.h"
#import "TutorialManager.h"
#import "MBProgressHUD.h"
#import "BlueCatsSDK.h"
#import "BCMicroLocationManager.h"
#import "BCBeacon.h"
#import "BCEventManager.h"
#import "AFNetworking.h"

static const NSInteger SectionStartUp        = 0;
static const NSInteger SectionDisplayOptions = 1;
static const NSInteger SectionDisruptions    = 2;
static const NSInteger SectionAlarms         = 3;
static const NSInteger SectionDataAnalytics  = 4;
static const NSInteger SectionTutorial       = 5;
static const NSInteger SectionBluecats       = 6;

@interface SettingsViewController ()

@property (strong, nonatomic) IBOutlet UILabel*     lblOpenWith;
@property (strong, nonatomic) IBOutlet UILabel*     lblSynchronise;
@property (strong, nonatomic) IBOutlet UILabel*     lblShowNearbyStop;
@property (strong, nonatomic) IBOutlet UILabel*     lblShowMostRecent;
@property (weak,   nonatomic) UISwitch*    analytics;
@property (weak,   nonatomic) UISwitch*    notifications;
@property (weak,   nonatomic) UISwitch*    tutorials;
@property (weak,   nonatomic) UISwitch*    beacons;
@property (weak,   nonatomic) IBOutlet UISwitch*    prod;
@property (weak,   nonatomic) IBOutlet UITableView* tableView;

@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *RightConstrain;
@property (weak,   nonatomic) IBOutlet NSLayoutConstraint* scrollToBottomConstraint;

@property (strong, nonatomic) MBProgressHUD* loadIcon;
@property (strong, nonatomic) NSTimer* notificationsTimer;
@property (nonatomic) BOOL waitingForDeviceToken;
@property (nonatomic) BOOL shownPreviously;

@property (strong, nonatomic) BCMicroLocationManager* locationManager;
@property (strong, nonatomic) NSString* beaconID;
@property (strong, nonatomic) NSString* beaconName;
@property (nonatomic) BCProximity beaconProximity;
@property (nonatomic) BOOL isPurring;
@end

@implementation BlueCatsCell
@end

@implementation SettingsCell
@end

@implementation SettingsViewController

NSString* const kNotificationSelection = @"notificationSelection";

#pragma mark - Inits & Loads

- (NSLayoutConstraint *)adSpacingConstraint
{ return self.scrollToBottomConstraint; }

-(void) viewDidLoad
{
    [super viewDidLoad];
    [self showAds];
    [TutorialManager instance].currentTutorialSettings = TutorialNone;

    //Notification sent from OpenWith, when returning from OpenWith, to update the item selected
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotificationSelection:) name:kNotificationSelection object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveDeviceToken:)           name:kNotificationDeviceTokenAvailable object:nil];
    
    for (NSLayoutConstraint * curConstrain in self.RightConstrain)
    { curConstrain.constant = 12; }
    [self.view layoutIfNeeded];

    self.prod.on = [[NSUserDefaults standardUserDefaults] boolForKey:@"devAPI"];
    self.analytics.on = [[NSUserDefaults standardUserDefaults] boolForKey:kAnalyticsAllowAnalyticsKey];
    self.isPurring = [BlueCatsSDK status] == kBCStatusPurring;
}

- (void)microLocationManager:(BCMicroLocationManager *)microLocationManager didRangeBeacons:(NSArray *)beacons inSite:(BCSite *)site
{
    if(beacons.count > 0)
    {
        BCBeacon* closest = beacons[0];
        self.beaconID = closest.beaconID;
        self.beaconName = [NSString stringWithFormat:@"%@ - %@", closest.name, site.name];
        self.beaconProximity = closest.proximity;
    }
    else
    {
        self.beaconID = nil;
        self.beaconName = nil;
        self.beaconProximity = BCProximityUnknown;
    }
    
    if([self.tableView numberOfRowsInSection:SectionBluecats] > 1)
    { [self.tableView reloadRowsAtIndexPaths:@[ [NSIndexPath indexPathForRow:1 inSection:SectionBluecats] ] withRowAnimation:UITableViewRowAnimationFade]; }
}

- (void)microLocationManager:(BCMicroLocationManager *)microLocationManager rangingBeaconsDidFailForSite:(BCSite *)site withError:(NSError *)error
{ NSLog(@"Failed to range beacons for site"); }

- (void)microLocationManager:(BCMicroLocationManager *)microLocationManager didEnterSite:(BCSite *)site
{
    NSLog(@"Did enter site!");
    [self.locationManager startRangingBeaconsInSite:site];
}

- (void)microLocationManager:(BCMicroLocationManager *)microLocationManager didExitSite:(BCSite *)site
{
    NSLog(@"Did exit site!");
    [self.locationManager stopRangingBeaconsInSite:site];
    self.beaconID = nil;
    self.beaconName = nil;
    if([self.tableView numberOfRowsInSection:SectionBluecats] > 1)
    { [self.tableView reloadRowsAtIndexPaths:@[ [NSIndexPath indexPathForRow:1 inSection:SectionBluecats] ] withRowAnimation:UITableViewRowAnimationFade]; }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureSettings];
    
    TutorialManager* instance = [TutorialManager instance];

    if((!self.shownPreviously) && (instance.currentTutorialSettings == TutorialNone) && ([instance shouldShowTutorial:TutorialScreenSettings]))
    {
        self.shownPreviously = YES;
        UIAlertView* alert = [instance alertForTutorialStart:TutorialScreenSettings];
        alert.delegate = self;
        [alert show];
    }
    else if((instance.currentTutorialSettings == TutorialSettingsSaveDisruptions) || (instance.currentTutorialSettings == TutorialSettingsSelectDisruptionRoute) || (instance.currentTutorialSettings == TutorialSettingsSelectDisruptionTime))
    {
        UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:3]];
        instance.currentTutorialSettings = TutorialSettingsManageAlarms;
        [instance showTutorial:TutorialSettingsManageAlarms container:self.tableView alertDelegate:self frame:[self.tableView convertRect:cell.contentView.frame fromView:cell.contentView] dismiss:nil];
    }
    else if(instance.currentTutorialSettings == TutorialSettingsBackFromAlarms)
    {
        instance.currentTutorialSettings = TutorialNone;
        UIAlertView* alert = [instance alertForTutorialEnd:TutorialScreenSettings];
        alert.delegate = self;
        [alert show];
    }
}

- (IBAction)prodChanged:(UISwitch *)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:sender.isOn forKey:@"devAPI"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)switchAnalytics:(UISwitch *)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:sender.isOn forKey:kAnalyticsAllowAnalyticsKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.tableView.delegate = nil;
    self.tableView.dataSource = nil;
    self.locationManager.delegate = nil;
    [self.locationManager stopMonitoringAllSites];
    [self.locationManager stopUpdatingMicroLocation];
    self.locationManager = nil;
    [self removeAds:NO];
}

#pragma mark - User Actions

- (IBAction)toggleBeacons:(UISwitch*)sender
{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    if(sender.on)
    {
        if(self.locationManager == nil)
        {
            self.locationManager = [BCMicroLocationManager sharedManager];
            self.locationManager.delegate = self;
        }
        [sender setOn:NO animated:YES];
        MBProgressHUD* loadIcon = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        tramTRACKERAppDelegate* appDelegate = (tramTRACKERAppDelegate*)[UIApplication sharedApplication].delegate;
        [appDelegate createBeaconManager];
        [BlueCatsSDK startPurringWithAppToken:BlueCatsKeyYarraTrams completion:^(BCStatus status) {
            [loadIcon hide:YES];
            if(status == kBCStatusPurring)
            {
                self.isPurring = YES;
                [sender setOn:YES animated:YES];
                [self.locationManager startUpdatingMicroLocation];
            }
            else
            {
                if(![BlueCatsSDK isBluetoothEnabled])
                { [[[UIAlertView alloc] initWithTitle:@"Enable Bluetooth" message:@"Please turn ON Bluetooth on your device to enable this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show]; }
                else if(![BlueCatsSDK isLocationAuthorized])
                { [[[UIAlertView alloc] initWithTitle:@"Enable Location Services" message:@"Please turn ON Location Services on your device to enable this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show]; }
                else if(![BlueCatsSDK isNetworkReachable])
                { [[[UIAlertView alloc] initWithTitle:@"Enable Data Connection" message:@"Please turn ON your Data Connection to enable this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show]; }
            }
        }];
        [userDefaults setBool:NO forKey:kSettingsBluecatsDisabled];
    }
    else
    {
        self.isPurring = NO;
        if([BlueCatsSDK status] == kBCStatusPurring)
        { [BlueCatsSDK stopPurring]; }
        [userDefaults setBool:YES forKey:kSettingsBluecatsDisabled];
//        [self.tableView deleteRowsAtIndexPaths:@[ [NSIndexPath indexPathForRow:1 inSection:SectionBluecats] ] withRowAnimation:UITableViewRowAnimationFade];
    }
    [userDefaults synchronize];
}

- (IBAction)toggleShowNotifications:(UISwitch*)sender
{
    if(![Settings showNotifications]) // was switched from 'off' to 'on'
    {
        if(self.loadIcon == nil) { self.loadIcon = [MBProgressHUD showHUDAddedTo:self.view animated:YES]; }
        else { [self.loadIcon show:true]; }
        
        [sender setOn:NO animated:YES];
        self.notificationsTimer = [NSTimer scheduledTimerWithTimeInterval:8 target:self selector:@selector(notificationsFailed:) userInfo:nil repeats:NO];
        self.waitingForDeviceToken = YES;
        
        [self registerForNotifications];
    }
    else // was switched from 'on' to 'off'
    {
        static PidsService* service;
        if(service == nil)
        { service = [PidsService new]; }
        
        if(self.loadIcon == nil) { self.loadIcon = [MBProgressHUD showHUDAddedTo:self.view animated:YES]; }
        else { [self.loadIcon show:true]; }
        
        [sender setOn:YES animated:YES];
        [service submitNotificationRegistrationDeleteRequestForUUID:service.guid completion:^(BOOL successful, NSError *error) {
            [self.loadIcon hide:YES];
            self.loadIcon = nil;
            
            if(successful)
            {
//                [[[UIAlertView alloc] initWithTitle:@"" message:@"Deregistration successful" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                [Settings setShowNotifications:NO];
                if([self.tableView numberOfRowsInSection:2] >= 2) // only delete the second row if it's present (error conditions can cause this to be incorrect), otherwise failsoft and do nothing.
                { [self.tableView deleteRowsAtIndexPaths:@[ [NSIndexPath indexPathForRow:1 inSection:2] ] withRowAnimation:UITableViewRowAnimationRight]; }
                [sender setOn:NO animated:YES];
            }
        }];
    }
}

- (IBAction)toggleResetTutorials:(UISwitch*)sender
{
    if(sender.on)
    {
        [[TutorialManager instance] updateTutorialShouldShowFirstTime:YES];
        [[TutorialManager instance] setAllTutorialsShown:NO];
    }else
    {
        [[TutorialManager instance] updateTutorialShouldShowFirstTime:NO];
        [[TutorialManager instance] setAllTutorialsShown:YES];
    }
}

- (void)didReceiveDeviceToken:(NSNotification*)notification
{
    [self.notificationsTimer invalidate];
    self.notificationsTimer = nil;
    [self.loadIcon hide:YES];
    self.loadIcon = nil;
    [Settings setShowNotifications:YES];
    [self.notifications setOn:YES animated:YES];
//    [[[UIAlertView alloc] initWithTitle:@"" message:@"Registration successful" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    if([self.tableView numberOfRowsInSection:2] == 1)
    { [self.tableView insertRowsAtIndexPaths:@[ [NSIndexPath indexPathForRow:1 inSection:2] ] withRowAnimation:UITableViewRowAnimationRight]; }
    
    self.waitingForDeviceToken = NO;
    
    
    if([TutorialManager instance].currentTutorialSettings == TutorialSettingsActivateDisruptions)
    {
        [TutorialManager instance].currentTutorialSettings = TutorialSettingsGotoDisruptions;
        UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:2]];
        [[TutorialManager instance] showTutorial:TutorialSettingsGotoDisruptions container:self.tableView alertDelegate:self frame:[self.tableView convertRect:cell.contentView.bounds fromView:cell.contentView] dismiss:nil];
    }
}

- (void)notificationsFailed:(NSTimer*)timer
{
    if([timer isValid] && self.waitingForDeviceToken)
    {
        [self.loadIcon hide:YES];
        self.loadIcon = nil;
        self.waitingForDeviceToken = NO;
        [timer invalidate];
        timer = nil;
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"There was a problem trying to register for push notifications. Please check your Internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alert.tag = 999;
        alert.delegate = self;
        [alert show];
    }
}

- (void)registerForNotifications
{
    UIUserNotificationType types = UIUserNotificationTypeAlert | UIUserNotificationTypeSound;
    UIUserNotificationSettings* settings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}

#pragma mark - Segue Methods

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if (![[segue identifier] isEqualToString:kSegueUnwindToMore])
    {
        NSString* segueIdentifier = [segue identifier];
        if([segueIdentifier isEqualToString:kSegueSynchronise] || [segueIdentifier isEqualToString:kSegueShowNearbyStops] || [segueIdentifier isEqualToString:kSegueShowMostRecent] || (segueIdentifier.length == 0))
        {
            SettingsOptionSelectionMode currentMode = OpenWithMode;
            NSString *stringToSend = self.lblOpenWith.text;
        
            if ([[segue identifier] isEqualToString:kSegueSynchronise])
            {
                currentMode = SynchroniseMode;
                stringToSend = self.lblSynchronise.text;
                if ([stringToSend isEqualToString:NSLocalizedString(@"settings-sync-type-semi-auto", nil)])
                { stringToSend = NSLocalizedString(@"settings-sync-type-semi-auto-full", nil); }
            }
            if ([[segue identifier] isEqualToString:kSegueShowNearbyStops])
            {
                currentMode = ShowNearbyStopsMode;
                stringToSend = self.lblShowNearbyStop.text;
            }
            if ([[segue identifier] isEqualToString:kSegueShowMostRecent])
            {
                currentMode = ShowMostRecentMode;
                stringToSend = self.lblShowMostRecent.text;
            }
        
            OpenWithViewController *vc = [segue destinationViewController];
            [vc setCurrentMode:currentMode andSelectedString:stringToSend];
        }
        else if([segueIdentifier isEqualToString:kSegueManageDisruptions])
        {
            if([TutorialManager instance].currentTutorialSettings == TutorialSettingsGotoDisruptions)
            { [[TutorialManager instance] hideTutorial:TutorialScreenSettings]; }
        }
        else if([segueIdentifier isEqualToString:kSegueManageAlarms])
        {
            if([TutorialManager instance].currentTutorialSettings == TutorialSettingsManageAlarms)
            { [[TutorialManager instance] hideTutorial:TutorialScreenSettings]; }
        }
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    if([defaults boolForKey:kKillSwitchUseBluecats])
    { return 7; }
    else
    { return 6; }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch(section)
    {
        case SectionStartUp:        return @"START UP";
        case SectionDisplayOptions: return @"DISPLAY OPTIONS";
        case SectionDisruptions:    return @"DISRUPTION NOTIFICATIONS";
        case SectionAlarms:         return @"ALARMS";
        case SectionDataAnalytics:  return @"DATA ANALYTICS";
        case SectionTutorial:       return @"INTERACTIVE TUTORIALS";
        case SectionBluecats:       return @"BEACONS";
        default:                    return @"";
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch(section)
    {
        case SectionStartUp:        return 1;
        case SectionDisplayOptions: return 2;
        case SectionDisruptions:    return [Settings showNotifications] ? 2 : 1; // only show the switch if notifications are unauthorised.
        case SectionAlarms:         return 1;
        case SectionDataAnalytics:  return 1;
        case SectionTutorial:       return 1;
        case SectionBluecats:       return 1; // return self.isPurring ? 2 : 1;
        default:                    return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SettingsCell* settingsCell;
    switch(indexPath.section)
    {
        case SectionStartUp:
            settingsCell = [tableView dequeueReusableCellWithIdentifier:@"StartUp" forIndexPath:indexPath];
            settingsCell.detail.text = [Settings openWith];
            self.lblOpenWith = settingsCell.detail;
            break;
        case SectionDisplayOptions:
            if(indexPath.row == 0)
            {
                settingsCell = [tableView dequeueReusableCellWithIdentifier:@"DisplayNearbyStops" forIndexPath:indexPath];
                settingsCell.detail.text = [NSString stringWithFormat:@"%ld Stops", (long)[Settings showNearbyStops]];
                self.lblShowNearbyStop = settingsCell.detail;
            }
            else if(indexPath.row == 1)
            {
                settingsCell = [tableView dequeueReusableCellWithIdentifier:@"DisplayRecentStops" forIndexPath:indexPath];
                settingsCell.detail.text = [NSString stringWithFormat:@"%ld Stops", (long)[Settings showMostRecentStops]];
                self.lblShowMostRecent = settingsCell.detail;
            }
            break;
        case SectionDisruptions:
            if(indexPath.row == 0)
            {
                settingsCell = [tableView dequeueReusableCellWithIdentifier:@"AllowNotifications" forIndexPath:indexPath];
                settingsCell.toggle.on = [Settings showNotifications];
                self.notifications = settingsCell.toggle;
            }
            else if(indexPath.row == 1)
            {
                settingsCell = [tableView dequeueReusableCellWithIdentifier:@"ManageDisruptions" forIndexPath:indexPath];
                settingsCell.detail.text = @"Manage Notifications";
            }
            break;
        case SectionAlarms:
            settingsCell = [tableView dequeueReusableCellWithIdentifier:@"ManageAlarms" forIndexPath:indexPath];
            settingsCell.detail.text = @"Manage Alarms";
            break;
        case SectionDataAnalytics:
            settingsCell = [tableView dequeueReusableCellWithIdentifier:@"AllowAnalytics" forIndexPath:indexPath];
            settingsCell.toggle.on = [[NSUserDefaults standardUserDefaults] boolForKey:kAnalyticsAllowAnalyticsKey];
            self.analytics = settingsCell.toggle;
            break;
        case SectionTutorial:
            settingsCell = [tableView dequeueReusableCellWithIdentifier:@"ResetTutorial" forIndexPath:indexPath];
            settingsCell.toggle.on = ![[TutorialManager instance] allTutorialsShown] && [[TutorialManager instance] tutorialShouldShowFirstTime];
            self.tutorials = settingsCell.toggle;
            break;
        case SectionBluecats:
        {
            if(indexPath.row == 0)
            {
                settingsCell = [tableView dequeueReusableCellWithIdentifier:@"BluecatsSwitch" forIndexPath:indexPath];
                NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
                settingsCell.toggle.on = ![userDefaults boolForKey:kSettingsBluecatsDisabled]; //self.isPurring;
                self.beacons = settingsCell.toggle;
                break;
            }
            else if(indexPath.row == 1)
            {
                BlueCatsCell* cell = [tableView dequeueReusableCellWithIdentifier:@"Bluecats" forIndexPath:indexPath];
                if((self.beaconName) || (self.beaconID))
                {
                    cell.beaconID.text = self.beaconID;
                    cell.beaconIndicator.image = [UIImage imageNamed:(self.beaconProximity == BCProximityUnknown) ? @"icon_beaconyellow" : @"icon_beacongreen"];
                    cell.beaconName.text = self.beaconName;
                    cell.beaconRange.text = @"";
                    cell.beaconName.textColor = [UIColor colorWithRed: 0.0 green: 0.5039 blue: 0.0661 alpha: 1.0];
                    switch(self.beaconProximity)
                    {
                        case BCProximityFar: cell.beaconRange.text = @"Far"; break;
                        case BCProximityNear: cell.beaconRange.text = @"Near"; break;
                        case BCProximityImmediate: cell.beaconRange.text = @"Immediate"; break;
                        case BCProximityUnknown: cell.beaconRange.text = @"Unknown"; break;
                    }
                }
                else
                {
                    cell.beaconID.text = @"No ID";
                    cell.beaconIndicator.image = [UIImage imageNamed:@"icon_beaconred"];
                    cell.beaconName.text = @"No beacon in range";
                    cell.beaconRange.text = @"";
                    cell.beaconName.textColor = [UIColor colorWithRed: 0.6153 green: 0.0734 blue: 0.0 alpha: 1.0];
                }
                return cell;
            }
            break;
        }
    }
    return settingsCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{ return 36.0f; }

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{ return [SectionHeaderView staticSectionHeaderWithTitle:@[@"Start up", @"Display options", @"Disruption Notifications", @"Alarms", @"Data analytics", @"Interactive Tutorials", @"Beacons"][section]]; }

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{ [tableView deselectRowAtIndexPath:indexPath animated:YES]; }

#pragma mark - AlertView Delegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if((alertView.tag == TutorialAlertStart) && (buttonIndex != alertView.cancelButtonIndex))
    {
        if([Settings showNotifications])
        {
            [TutorialManager instance].currentTutorialSettings = TutorialSettingsGotoDisruptions;
            UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:2]];
            [[TutorialManager instance] showTutorial:TutorialSettingsGotoDisruptions container:self.tableView alertDelegate:self frame:[self.tableView convertRect:cell.contentView.bounds fromView:cell.contentView] dismiss:nil];
        }
        else
        {
            [TutorialManager instance].currentTutorialSettings = TutorialSettingsActivateDisruptions;
            [[TutorialManager instance] showTutorial:TutorialSettingsActivateDisruptions container:self.tableView alertDelegate:self frame:[self.tableView convertRect:self.notifications.frame fromView:self.notifications.superview] dismiss:nil];
        }
    }
    else if (alertView.tag == TutorialAlertEnd)
    { [[TutorialManager instance] setTutorialShown:TutorialScreenSettings wasSeen:YES]; }
    else if(alertView.tag == 999)
    { [[TutorialManager instance] hideTutorial:TutorialScreenSettings]; }
}

#pragma mark - Notification Handlers

- (void)didReceiveNotificationSelection:(NSNotification *)notification
{
    NSArray *arrValues = notification.object;
    
    int mode = [arrValues[0] intValue];
    NSString *stringToDisplay = arrValues[1];
    
    switch (mode)
    {
        case 0:
            self.lblOpenWith.text = stringToDisplay;
           break;
        case 1:
            if ([stringToDisplay isEqualToString:NSLocalizedString(@"settings-sync-type-semi-auto-full", nil)])
                stringToDisplay = NSLocalizedString(@"settings-sync-type-semi-auto", nil);
            self.lblSynchronise.text = stringToDisplay;
            break;
        case 2:
        {
            self.lblShowNearbyStop.text = stringToDisplay;
            break;
        }
        case 3:
            self.lblShowMostRecent.text = stringToDisplay;
            break;
        default:
            break;
    }
}

@end
