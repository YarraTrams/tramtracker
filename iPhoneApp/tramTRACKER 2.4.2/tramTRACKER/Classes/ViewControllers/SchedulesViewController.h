//
//  SchedulesViewController.h
//  tramTracker
//
//  Created by Jonathan Head on 27/04/2015.
//  Copyright (c) 2015 AppsCore. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SchedulesViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic)         NSInteger tripID;
@property (strong, nonatomic) NSDate*   tripDate;
@property (strong, nonatomic) Route*    route;
@property (strong, nonatomic) Stop*     stop;
@end
