//
//  RouteListViewController.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 9/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoutesListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

//typedef enum
//{
//    RoutesListViewControllerRegular,
//    RoutesListViewControllerTimetable
//}   RoutesListViewControllerType;
//
//@property (nonatomic, assign) RoutesListViewControllerType type;

@end
