//
//  TimetablesViewController.m
//  tramTRACKER
//
//  Created by Raji on 24/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "TimetablesViewController.h"
#import "TimetableCell.h"
#import "SelectRouteViewController.h"
#import "Analytics.h"
#import "SectionHeaderView.h"
#import "SchedulesViewController.h"
#import "ManageAlarmsViewController.h"
#import "TutorialManager.h"

@interface TimetablesViewController () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UILabel *lblRoute;
@property (strong, nonatomic) IBOutlet UILabel *lblTimeOfDeparture;
@property (strong, nonatomic) IBOutlet UITextField *hiddenTextFieldForTime;
@property (nonatomic, weak) IBOutlet UITableView* tableView;
@property (nonatomic, weak) IBOutlet UILabel* emptyTableViewLabel;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint* tableToBottomConstraint;

@property (strong, nonatomic) Prediction* selectedPrediction;

@property (nonatomic, strong) PidsService * service;

@property (strong, nonatomic) Stop * stop;
@property (strong, nonatomic) Route * currentRoute;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) NSArray * allRoutesThroughThisStop;
@property (nonatomic, assign) NSInteger selectedRouteIndex;
@property (nonatomic, assign) BOOL upDirection;
@property (nonatomic, assign) BOOL isTerminus;
@property (nonatomic, strong) UIDatePicker  * picker;

@property (nonatomic, strong) NSArray   * schedules;

@property (nonatomic, assign, getter = hasTimeChanged) BOOL timeChanged;
@property (nonatomic) BOOL shownTutorial;

@end

@implementation DeparturesCell
@end

@implementation TimetablesViewController

#define DatePickerTag       13
#define SelectButtonIndex   1
#define CancelButtonIndex   2

#pragma mark - Inits & Loads

- (NSLayoutConstraint *)adSpacingConstraint
{ return self.tableToBottomConstraint; }

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.shownTutorial = NO;
    
    /* Set Back Action */
   
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    self.allRoutesThroughThisStop = [NSArray arrayWithArray:[self.stop routesThroughStop]];
   
    if (self.currentRoute)
    {
        self.selectedRouteIndex = [self.allRoutesThroughThisStop indexOfObject:self.currentRoute.number];

        if (self.selectedRouteIndex == NSNotFound)
            self.selectedRouteIndex = 0;
    }
    else
        self.selectedRouteIndex = 0;
    [self setCurrentRouteAndRouteLabel];
    [self setAndDisplayCurrentTime:[NSDate date]];
    
    self.navigationItem.title = self.stop.name;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TimeTableCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"Timetable Cell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"DeparturesRouteCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"DeparturesRouteCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"DeparturesTimeCell"  bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"DeparturesTimeCell"];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(dismissKeyboard)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
    
    // Enabling iOS 7 screen-edge-pan-gesture for pop action
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    self.navigationController.interactivePopGestureRecognizer.delegate = nil;

    self.service = [[PidsService alloc] initWithDelegate:self];

    self.picker = [[UIDatePicker alloc] init];
    [self.picker setDatePickerMode:UIDatePickerModeDateAndTime];
    [self.picker setTimeZone:[NSTimeZone systemTimeZone]];
    [self.picker setDate:[NSDate date]];
    [self.picker addTarget:self action:@selector(didSelectDateOnPicker:) forControlEvents:UIControlEventValueChanged];
    
    [self.service getScheduledDeparturesForStop:self.stop.trackerID.floatValue routeNumber:self.currentRoute.internalNumber atDateTime:self.picker.date withLowFloorOnly:NO];
    
    self.tableView.hidden           = self.isTerminus;
    self.emptyTableViewLabel.hidden = !self.isTerminus;
    
    [self showAds];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureSchedulesDeparture];
}

- (void)dealloc
{
    [self.service setDelegate:nil];
    
    self.tableView.delegate = nil;
    self.tableView.dataSource = nil;
    self.hiddenTextFieldForTime.delegate = nil;
    [self removeAds:NO];
}

#pragma mark - Utility Methods

- (void)dismissKeyboard
{
    [self.hiddenTextFieldForTime resignFirstResponder];
}

- (void)setStop:(Stop *)aStop direction:(BOOL)upDirection isTerminus:(BOOL)isTerminus
{
    self.stop = aStop;
    self.upDirection = upDirection;
    self.isTerminus = isTerminus;
}

- (NSString*)setCurrentRouteAndRouteLabel
{
    if (self.selectedRouteIndex < self.allRoutesThroughThisStop.count) {
        NSString    * routeNo = self.allRoutesThroughThisStop[self.selectedRouteIndex];
        
        self.currentRoute = [Route routeWithNumber:routeNo];
        return [self.currentRoute routeNameWithNumberAndDestinationUp:[self.currentRoute.upStops containsObject:self.stop.trackerID]];
    }
    return @"";
}

- (NSString*)setAndDisplayCurrentTime:(NSDate *)date
{
    [self.dateFormatter setDateFormat:@"eee dd MMM hh:mm a"];
    NSString* newDate = [self.dateFormatter stringFromDate:date];
    
    //reset the formatter for "next three" cells
    self.dateFormatter.dateStyle = NSDateFormatterNoStyle;
    self.dateFormatter.timeStyle = NSDateFormatterShortStyle;
    return newDate;
}

- (void)pidsServiceDidFailWithError:(NSError *)error {
    [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Unable to contact the tramTRACKER service. Please make sure that your device is connected to the internet and try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
}

- (void)setSchedules:(NSArray *)schedules
{
    NSMutableArray  *rowsToInsert = [NSMutableArray new];
    
    if (self.hasTimeChanged)
        _schedules = nil;

    if (self.hasTimeChanged || !self.schedules.count)
    {
        _schedules = schedules;
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    else
    {
        for (NSInteger i = 0; i < schedules.count; ++i)
            [rowsToInsert addObject:[NSIndexPath indexPathForItem:rowsToInsert.count + _schedules.count inSection:2]];

        _schedules = [_schedules arrayByAddingObjectsFromArray:schedules];
        
        [self.tableView insertRowsAtIndexPaths:rowsToInsert withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView scrollToRowAtIndexPath:rowsToInsert.firstObject atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    self.timeChanged = NO;
    
    if((schedules.count > 0) && [[TutorialManager instance] shouldShowTutorial:TutorialScreenTimetables] && (self.shownTutorial == NO))
    {
        self.shownTutorial = YES;
        UIAlertView* alert = [[TutorialManager instance] alertForTutorialStart:TutorialScreenTimetables];
        alert.delegate = self;
        [alert show];
    }
}

#pragma mark - Notification Handlers

- (void)updateSelectedRoute:(NSInteger)aSelectedRouteIndex
{
    self.selectedRouteIndex = (NSInteger)aSelectedRouteIndex;

    [self setCurrentRouteAndRouteLabel];

    self.timeChanged = YES;

    [self.service getScheduledDeparturesForStop:self.stop.trackerID.doubleValue routeNumber:self.currentRoute.internalNumber atDateTime:self.picker.date withLowFloorOnly:NO];
}

#pragma mark - Segue Methods

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:kSegueTimetablesToSelectRoute])
    {
        [SelectRouteViewController setCurrentMode:FromTimetables];
        [[segue destinationViewController] setSelectedIndex:self.selectedRouteIndex];
        [[segue destinationViewController] setStop:self.stop direction:self.upDirection];
    }
    else if ([[segue identifier] isEqualToString:kSegueTimetablesToStopInfo])
    {
        [[segue destinationViewController] setStop:self.stop];
    }
    else if([[segue identifier] isEqualToString:kSegueScheduleScreen])
    {
        SchedulesViewController* controller = (SchedulesViewController*)segue.destinationViewController;
        controller.tripID = [self.selectedPrediction.tripID integerValue];
        controller.tripDate = self.selectedPrediction.predictedArrivalDateTime;
        controller.route = self.currentRoute;
        controller.stop  = self.stop;
    }
}

#pragma mark - User Actions

- (IBAction)btnLoadMoreServices:(id)sender
{
    PredictionStub  * lastPredictionStub = self.schedules.lastObject;
    
    if (lastPredictionStub)
    { [self.service getScheduledDeparturesForStop:self.stop.trackerID.floatValue routeNumber:[Route routeWithNumber:self.stop.routesThroughStop[self.selectedRouteIndex]].internalNumber atDateTime:[lastPredictionStub.predictedArrivalDateTime dateByAddingTimeInterval:120] withLowFloorOnly:NO]; }
}

- (IBAction)backAction:(id)sender
{
     [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UIAlertView Delegate methods

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if((alertView.tag == TutorialAlertStart) && (buttonIndex != alertView.cancelButtonIndex))
    {
        UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:2]];
        if(cell)
        {
            [TutorialManager instance].currentTutorialScheduled = TutorialTimetables;
            [[TutorialManager instance] showTutorial:TutorialTimetables container:self.tableView alertDelegate:self frame:cell.frame dismiss:nil];
        }
    }
    else if(alertView.tag == TutorialAlertEnd)
    { [[TutorialManager instance] setTutorialShown:TutorialScreenTimetables wasSeen:YES]; }
}

#pragma mark - UITextField Delegate methods

- (void)didSelectDateOnPicker:(UIDatePicker *)datePicker
{
    self.lblTimeOfDeparture.text = [self setAndDisplayCurrentTime:datePicker.date];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // do nothing if there's no data in the array. This is an exceptional state and should never occur, this is only a safeguard.
    if(self.selectedRouteIndex < self.stop.routesThroughStop.count)
    {
        self.timeChanged = YES;

        [self.service getScheduledDeparturesForStop:self.stop.trackerID.floatValue routeNumber:self.stop.routesThroughStop[self.selectedRouteIndex] atDateTime:self.picker.date withLowFloorOnly:NO];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{ return 3; }

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{ return section == 2 ? self.schedules.count : 1; }

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 2)
    { return 60.0f; }
    
    return tableView.rowHeight;
}

#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{ return 36.0f; }


- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{ return indexPath.section == 2; }
 
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{ return [SectionHeaderView staticSectionHeaderWithTitle:@[@"Scheduled departures for", @"Departure time", @"Timetable"][section]]; }

- (NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath
{ return 1; }

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Selected row!");
    if(indexPath.section == 2)
    {
        self.selectedPrediction = self.schedules[indexPath.row];
        [self performSegueWithIdentifier:@"ShowSchedules" sender:self];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 2)
    {
        static NSString *CellIdentifier = @"Timetable Cell";
        
        TimetableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        PredictionStub  * prediction = self.schedules[indexPath.row];
        
        //reset the formatter for "next three" cells
        self.dateFormatter.dateStyle = NSDateFormatterNoStyle;
        self.dateFormatter.timeStyle = NSDateFormatterShortStyle;

        NSString    * time = [self.dateFormatter stringFromDate:prediction.predictedArrivalDateTime];

        [self.dateFormatter setDateFormat:@"eee"];

        [cell configureCellWithName:prediction.destination andRouteNumber:prediction.routeNo andTime:time andDay:[self.dateFormatter stringFromDate:prediction.predictedArrivalDateTime]];
        
        __weak TimetablesViewController* weakSelf = self;
        
        UIImageView* imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 70, cell.contentView.frame.size.height)];
        imageView.image = [UIImage imageNamed:@"icon_alarm"];
        imageView.contentMode = UIViewContentModeCenter;
        [cell setSwipeGestureWithView:imageView color:[UIColor colorWithRed:0.4723 green:0.7533 blue:0.0 alpha:1.0] mode:MCSwipeTableViewCellModeSwitch state:MCSwipeTableViewCellState3 completionBlock:^(MCSwipeTableViewCell *cell, MCSwipeTableViewCellState state, MCSwipeTableViewCellMode mode)
         {
             NSTimeInterval timeDifference = [prediction.predictedArrivalDateTime timeIntervalSinceDate:[NSDate date]];
             if(timeDifference >= 360)
             {
                 CustomIOSAlertView* alert = [ManageAlarmsViewController popupForView:weakSelf.view stop:weakSelf.stop prediction:prediction times:@[ @5, @10, @20 ] completion:^(BOOL successful) {
                    if(successful && ([TutorialManager instance].currentTutorialScheduled == TutorialTimetables))
                    {
                        [TutorialManager instance].currentTutorialScheduled = TutorialNone;
                        [[TutorialManager instance] hideTutorial:TutorialScreenTimetables];
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(ToastMessageDelay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            UIAlertView* alert = [[TutorialManager instance] alertForTutorialEnd:TutorialScreenTimetables];
                            alert.delegate = weakSelf;
                            [alert show];
                        });
                     }
                 } isEstimate:NO];
                 [alert show];
             }
             else
             {
                 NSInteger numberMinutes = (int)timeDifference/60;
                 NSString* message;
                 if(numberMinutes <= 0)
                 { message = @"now"; }
                 else if(numberMinutes == 1)
                 { message = @"within 1 minute"; }
                 else
                 { message = [NSString stringWithFormat:@"within %lu minutes", (long)numberMinutes]; }
                 
                 [[[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"You can't set this alarm as your tram is due to arrive %@.", message] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
             }
             
         }];
        cell.firstTrigger = 0.2;

        return cell;
    }
    else
    {
        DeparturesCell* cell;
        if(indexPath.section == 0)
        {
            cell = (DeparturesCell*)[self.tableView dequeueReusableCellWithIdentifier:@"DeparturesRouteCell"];
            cell.label.text = [self setCurrentRouteAndRouteLabel];
            return cell;
        }
        else if(indexPath.section == 1)
        {
            cell = (DeparturesCell*)[self.tableView dequeueReusableCellWithIdentifier:@"DeparturesTimeCell"];
            cell.label.text = [self setAndDisplayCurrentTime:self.picker.date];
            self.hiddenTextFieldForTime = cell.textField;
            self.hiddenTextFieldForTime.inputView = self.picker;
            self.hiddenTextFieldForTime.delegate = self;
            self.lblTimeOfDeparture = cell.label;
            return cell;
        }
    }
    
    return nil;
}

@end
