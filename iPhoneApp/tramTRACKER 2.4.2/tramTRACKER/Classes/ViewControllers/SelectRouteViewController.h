//
//  SelectRouteViewController.h
//  tramTRACKER
//
//  Created by Raji on 24/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectRouteViewController : UITableViewController

typedef enum
{
    FromRouteFilter = 0,
    FromTimetables = 1
}   viewControllerType;

+ (void)setCurrentMode:(viewControllerType)mode;
- (void)setStop:(Stop *)aStop direction:(BOOL)upDirection;

@end
