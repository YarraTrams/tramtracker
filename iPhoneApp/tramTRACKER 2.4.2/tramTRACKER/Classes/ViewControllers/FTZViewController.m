//
//  FTZViewController.m
//  tramTracker
//
//  Created by Hugo Cuvillier on 10/12/2014.
//  Copyright (c) 2014 AppsCore. All rights reserved.
//

#import "FTZViewController.h"

@interface FTZViewController ()
@property (unsafe_unretained, nonatomic) IBOutlet NSLayoutConstraint *ftzTopSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint* scrollToBottomConstraint;
@property (weak, nonatomic) IBOutlet UILabel* paragraphTop;
@property (weak, nonatomic) IBOutlet UILabel* paragraphBottom;
@property BOOL isLink;

@end

@implementation FTZViewController

- (NSLayoutConstraint *)adSpacingConstraint
{ return self.scrollToBottomConstraint; }

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self showAds];
    
    // Enabling iOS 7 screen-edge-pan-gesture for pop action
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    
    if((floor(NSFoundationVersionNumber) < NSFoundationVersionNumber_iOS_7_1) || (ceil(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_7_1))
    {
        [self.paragraphTop addConstraint:[NSLayoutConstraint constraintWithItem:self.paragraphTop attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:184]];
        [self.paragraphBottom addConstraint:[NSLayoutConstraint constraintWithItem:self.paragraphBottom attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:85]];
    }
}

- (void)dealloc
{ [self removeAds:NO]; }

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)ftzAction:(id)sender {
    self.isLink = NO;
    [[[UIAlertView alloc] initWithTitle:@"Open in Safari?"
                                message:@"Would you like to close the tramTracker® app and launch Safari?"
                               delegate:self
                      cancelButtonTitle:@"Cancel"
                      otherButtonTitles:@"OK", nil]
     show];
    
}

- (IBAction)linkAction:(id)sender {
    self.isLink = YES;
    [[[UIAlertView alloc] initWithTitle:@"Open in Safari?"
                                message:@"Would you like to close the tramTracker® app and launch Safari?"
                               delegate:self
                      cancelButtonTitle:@"Cancel"
                      otherButtonTitles:@"OK", nil]
     show];
    
}

#pragma mark - Alertview Delegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        if (self.isLink) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://yarratrams.com.au"]];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://ptv.vic.gov.au"]];
        }
    }
}

@end
