//
//  FavouriteContainerViewController.h
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 16/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FavouriteStop.h"

@interface FavouriteContainerViewController : UIViewController

- (IBAction)toggleListMapAction:(UIBarButtonItem *)sender;
- (IBAction)editAction:(id)sender;

- (void)locateAndPushNearestFavourite;
- (void)pushToPID:(FavouriteStop *)favouriteStop;

@end
