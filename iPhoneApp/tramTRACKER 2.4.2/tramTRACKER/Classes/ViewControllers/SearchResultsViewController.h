//
//  SearchResultsViewController.h
//  tramTRACKER
//
//  Created by Raji on 8/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultsViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

- (void)setLists:(NSArray *)lists;
- (void)setBooleanForEasyAccessStops:(BOOL)hasEasyAccess andShelter:(BOOL)hasShelter;

@end
