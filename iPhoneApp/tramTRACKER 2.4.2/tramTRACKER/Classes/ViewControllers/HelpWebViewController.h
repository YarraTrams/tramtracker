//
//  HelpWebViewController.h
//  tramTRACKER
//
//  Created by Alex Louey on 16/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpWebViewController : UIViewController

@property(nonatomic,strong) NSString * filename;
@property(nonatomic,strong) NSString * urlStr;
@property(nonatomic,strong) NSString * fileTitle;

@end
