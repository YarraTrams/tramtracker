//
//  NetworkViewController.m
//  tramTRACKER
//
//  Created by Hugo Cuvillier on 17/12/2013.
//  Copyright (c) 2013 Yarra Trams. All rights reserved.
//

#import "NetworkViewController.h"
#import "tramTRACKERAppDelegate.h"
#import "Analytics.h"

@interface NetworkViewController ()<UIWebViewDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint* webToBottomConstraint;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) UIImage *image;

@end

@implementation NetworkViewController

static NSString    * kUserKeyTimeMap = @"timeMapDownloaded";

- (NSLayoutConstraint *)adSpacingConstraint
{ return self.webToBottomConstraint; }

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self showAds];
    self.webView.delegate = self;
    
    self.webView.scalesPageToFit = YES;
    
    NSString    *fileName = [[NSBundle mainBundle] pathForResource:@"network_map_201705010000" ofType:@"png"];
    NSArray     * maps = [[NSUserDefaults standardUserDefaults] objectForKey:@"maps"];
    NSDate      *mapDate = [NSDate dateWithTimeIntervalSince1970:1420070400]; // 1st Jan 2015
    
    for (NSDictionary * map in [maps reverseObjectEnumerator]) {
        if (map) {
            NSDate* currentDate = map[@"ActiveDate"];

            NSString* currentPath;
            if(map[@"FileName"])
            { currentPath = [[tramTRACKERAppDelegate applicationDocumentsDirectory] stringByAppendingPathComponent:map[@"FileName"]]; }
            else if(map[@"fileName"])
            { currentPath = [[tramTRACKERAppDelegate applicationDocumentsDirectory] stringByAppendingPathComponent:map[@"fileName"]]; }
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:currentPath] && [currentDate compare:mapDate] == NSOrderedDescending) {
                mapDate = currentDate;
                fileName = currentPath;
            }
        }
    }
    
    NSString * header = [NSString stringWithFormat:@"<meta name='viewport' content='width=%ld; initial-scale=0.1; maximum-scale=1.0; user-scalable=1;'/>", (long)self.webView.frame.size.width];
    NSString * html = [NSString stringWithFormat:@"<html>%@<img src='file://%@'/></html>", header, fileName];
    
    [self.webView loadHTMLString:html baseURL:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureNetwork];
}

- (void)dealloc
{
    self.webView.delegate = nil;
}

#pragma mark - User Actions on Nav Bar

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
