//
//  SearchResultsViewController.m
//  tramTRACKER
//
//  Created by Raji on 8/01/2014.
//  Copyright (c) 2014 Yarra Trams. All rights reserved.
//

#import "SearchResultsViewController.h"
#import "NearbyCell.h"
#import "SectionHeaderView.h"
#import "Constants.h"
#import "TicketOutletDetailedViewController.h"
#import "POIViewController.h"
#import "PIDViewController.h"
#import "Analytics.h"
#import "UIAlertView+Blocks.h"

@interface SearchResultsViewController ()

@property (strong, nonatomic) SectionHeaderView         * sectionHeaderView;

@property (nonatomic, strong) NSArray                   * data;
@property (nonatomic, strong) NSMutableArray            * dataStates;

@property (weak, nonatomic) IBOutlet UITableView* tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint* tableToBottomConstraint;

@property (nonatomic, assign) BOOL hasEasyAccessStops;
@property (nonatomic, assign) BOOL hasShelterStops;

enum
{
    Stops,
    TicketOutlets,
    PointsOfInterest
};

@end

@implementation SearchResultsViewController

#pragma mark - Inits & Loads

- (NSLayoutConstraint *)adSpacingConstraint
{ return self.tableToBottomConstraint; }

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self showAds];
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[Analytics sharedInstance] featureAccessed:AnalyticsFeatureSearchResultList];
}

- (void)dealloc
{
    self.tableView.delegate = nil;
    self.tableView.dataSource = nil;
    
    [self removeAds:NO];
}

#pragma mark - Utility Methods

- (void)setLists:(NSArray *)lists
{
    self.data = [[NSArray alloc] initWithArray:lists];
    
    self.dataStates = [NSMutableArray new];
    for (NSInteger i = 0; i < self.data.count; ++i)
    { [self.dataStates addObject:@YES]; }
}

- (void)setBooleanForEasyAccessStops:(BOOL)hasEasyAccess andShelter:(BOOL)hasShelter
{
    self.hasEasyAccessStops = hasEasyAccess;
    self.hasShelterStops    = hasShelter;
}

#pragma mark - User Actions

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.data.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString * name = [self.data[section] valueForKey:@"Name"];
    _sectionHeaderView = [SectionHeaderView expandableSectionHeaderViewWithTitle:name section:section target:self action:@selector(didSelectAccessoryView:)];
    [_sectionHeaderView setExpandedState:[self.dataStates[section] boolValue]];
    return self.sectionHeaderView;
}

- (void)didSelectAccessoryView:(SectionHeaderView *)view
{
    NSInteger       dataSection = view.section;

    self.dataStates[dataSection] = @(![self.dataStates[dataSection] boolValue]);
    
    NSMutableArray  * indexPaths = [NSMutableArray new];
    for (NSInteger i = 0; i < [self.data[dataSection][@"List"] count]; ++i)
        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:dataSection]];
    
    if (![self.dataStates[dataSection] boolValue])
        [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationMiddle];
    else
        [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationMiddle];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{ return (CGFloat)36; }

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section
{ return 36; }

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{ return 80; }

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{ return 80; }

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (![self.dataStates[section] boolValue])
        return 0;

    NSArray * arrAtSection = [self.data[section] valueForKey:@"List"];
    return arrAtSection.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL isFree = NO;
    
    NSString * name = [self.data[indexPath.section] valueForKey:@"Name"];
    NSArray  * arrAtSection = [self.data[indexPath.section] valueForKey:@"List"];

    if([name isEqualToString:kStops])
    {
        Stop* stop = arrAtSection[indexPath.row];
        if(stop.isFTZStop)
        { isFree = YES; }
    }
    
    NSString *CellIdentifier = isFree ? @"NearbyCellFree" : @"NearbyCell";
    NearbyCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if ([name isEqualToString:kStops])
    { [cell configureWithStop:arrAtSection[indexPath.row] andEasyAccess:self.hasEasyAccessStops andShelter:self.hasShelterStops]; }
    else if ([name isEqualToString:kTicketOutlets])
    { [cell configureWithTR:arrAtSection[indexPath.row]]; }
    else if ([name isEqualToString:kPointsOfInterest])
    { [cell configureWithPOI:arrAtSection[indexPath.row]]; }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * name = [self.data[indexPath.section] valueForKey:@"Name"];
    NSArray  * arrAtSection = [self.data[indexPath.section] valueForKey:@"List"];
    
    if ([name isEqualToString:kStops])
    {
        Stop * stop = arrAtSection[indexPath.row];


        UIStoryboard        * sb = [UIStoryboard storyboardWithName:kMainStoryboard bundle:nil];
        PIDViewController    * vc = [sb instantiateViewControllerWithIdentifier:kScreenPID];
        [vc setCurrentStop:stop];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if ([name isEqualToString:kTicketOutlets])
    {
        UIStoryboard        * sb = [UIStoryboard storyboardWithName:kMainStoryboard bundle:nil];
        TicketOutletDetailedViewController   * vc = [sb instantiateViewControllerWithIdentifier:kScreenTicketDetailed];
        [vc setCurRetailer:arrAtSection[indexPath.row]];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if ([name isEqualToString:kPointsOfInterest])
        [self.tabBarController.parentViewController performSegueWithIdentifier:kSegueShowPOI
                                                                        sender:arrAtSection[indexPath.row]];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
