//
//  ServiceChangesDelegate.m
//  tramTRACKER
//
//  Created by Robert Amos on 20/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "ServiceChangesDelegate.h"


@implementation ServiceChangesDelegate

@synthesize delegate, action, responseText, currentProperty, currentServiceChange, serviceChangesArray;

- (id)initWithDelegate:(id)aDelegate action:(SEL)anAction
{
	if (self = [super init])
	{
		// assign the delegate
		delegate = aDelegate;
		action = anAction;

		//NSLog(@"ServiceChangesDelegate created");

	}
	return self;
}

#pragma mark -
#pragma mark NSURLConnection Delegate methods

// Connection failed with error
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	// For now, just log the error and silently fail
	//NSLog(@"Error with NSURLConnection %@ - %@", connection, error);
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	
	[self failWithError:error];
}

// We received a response from the server
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	// Start a new empty responseText that we can append to
	[self setResponseText:[NSMutableString string]];
	
	//NSLog(@"Received response to connection %@, text encoding is %@", connection, [response textEncodingName]);
}


// We received a chunk of data from the server
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	// append the data to our responseText string
	NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    if(![str isKindOfClass:[NSString class]])
    { return; }
    
	[[self responseText] appendString:str];
	str = nil;
}

// The connection has finished loading, start parsing
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	// parse the response
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	[self parseResponse:[self responseText]];
	
	// clear the response
	[self setResponseText:nil];
}


#pragma mark -
#pragma mark NSXMLParser Delegate methods

//
// Start parsing the response
//
- (void)parseResponse:(NSString *)text
{
	//NSLog(@"%@", text);
	// if we have no delegate there is no need to parse the results
	if (delegate == nil)
	{
		//NSLog(@"No delegate set, not parsing response.");
		return;
	}
	
	// Initialise the parser
	NSXMLParser *parser = [[NSXMLParser alloc] initWithData:[text dataUsingEncoding:NSUTF8StringEncoding]];
	
	// was there a parsing error?
	if (!parser)
	{
		//NSLog(@"Parsing XML Response failed for text: %@", text);
		return;
	}
	
	// Hold on to the parser and set ourselves as the delegate
	[parser setDelegate:self];
	
	// We don't need these features of the parser so disable them
	[parser setShouldResolveExternalEntities:NO];
	
	// start the parsing
	[parser parse];
}

//
// NSXMLParser Delegate - called when we encounter the start of an element. eg: <element>
//
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributes
{
	// use the qualified name if we have one
	if (qualifiedName) elementName = qualifiedName;

	// a channel is the outer body
	if ([elementName isEqualToString:@"channel"])
	{
		NSMutableArray *array = [NSMutableArray new];
		[self setServiceChangesArray:array];
	}
	
	// is it an item?
	else if ([elementName isEqualToString:@"item"])
	{
		ServiceChange *s = [[ServiceChange alloc] init];
		[self setCurrentServiceChange:s];

	// the other properties are all just text (for now)
	} else if ([elementName isEqualToString:@"title"] || [elementName isEqualToString:@"description"] || [elementName isEqualToString:@"content:encoded"])
	{
		NSMutableString *s = [[NSMutableString alloc] initWithCapacity:0];
		[self setCurrentProperty:s];
	}
}	

//
// NSXMLParser Delegate - called when we reach the end of an element. eg: </element>
//
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName
{
	// use the qualified name if we have one
	if (qualifiedName) elementName = qualifiedName;

	// found a title?
	if ([elementName isEqualToString:@"title"])
	{
		NSCharacterSet *chars = [NSCharacterSet characterSetWithRange:NSMakeRange(0x2010, 6)];
		NSRange r = [self.currentProperty rangeOfCharacterFromSet:chars];
		while (r.location != NSNotFound)
		{
			[self setCurrentProperty:[self.currentProperty stringByReplacingCharactersInRange:r withString:@"-"]];
			r = [self.currentProperty rangeOfCharacterFromSet:chars];
		}

		[self.currentServiceChange setTitle:self.currentProperty];
		[self setCurrentProperty:nil];
		
		// does the title have a date component?
		NSRange locationOfDateComponent = [self.currentServiceChange.title rangeOfString:@" - " options:NSBackwardsSearch];
		if (locationOfDateComponent.location != NSNotFound)
		{
			// pull that bit off the string
			[self.currentServiceChange setDateString:[self.currentServiceChange.title substringFromIndex:(locationOfDateComponent.location+locationOfDateComponent.length)]];
			[self.currentServiceChange setTitle:[self.currentServiceChange.title substringToIndex:locationOfDateComponent.location]];
		}
	}

	// found the description?
	else if ([elementName isEqualToString:@"description"])
	{
		if (self.currentServiceChange.content == nil || [self.currentServiceChange.content length] == 0)
			[self.currentServiceChange setContent:self.currentProperty];
		[self setCurrentProperty:nil];
	}
	
	// found some content?
	else if ([elementName isEqualToString:@"content:encoded"])
	{
		[self.currentServiceChange setContent:self.currentProperty];
		[self setCurrentProperty:nil];
	}
	
	// finished a service change?
	else if ([elementName isEqualToString:@"item"])
	{
		[self.serviceChangesArray addObject:self.currentServiceChange];
		[self setCurrentServiceChange:nil];
	}
	
	// finished the channel?
	else if ([elementName isEqualToString:@"channel"])
	{
		// if the delegate supports it, send it through
		if (delegate != nil && action != NULL && [delegate respondsToSelector:action])
		{
			[delegate performSelectorOnMainThread:action withObject:self.serviceChangesArray waitUntilDone:NO];
			[self setServiceChangesArray:nil];
		}
	}
}


//
// NSXMLParser Delegate - Called when we encounter a set of characters outside of an element
//
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
	// If we have a property open for reading add it to it
	if ([self currentProperty])
		[self setCurrentProperty:[self.currentProperty stringByAppendingString:string]];
}

//
// Abort parsing the XML string and cleanup
//
- (void)abortParsing:(NSXMLParser *)parser
{
	// aborting the parsing
	[parser abortParsing];
}

#pragma mark - 
#pragma mark Error Handling and Draining/tidying up after ourselves

- (void)failWithError:(NSError *)error
{
	// check the delegate to see if we can report the error
	if (delegate != nil && [delegate respondsToSelector:@selector(serviceChangesServiceDidFailWithError:)])
	{
		[delegate performSelectorOnMainThread:@selector(serviceChangesServiceDidFailWithError:) withObject:error waitUntilDone:NO];
	}
}



@end
