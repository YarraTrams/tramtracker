
//
//  PidsService.m
//  tramTRACKER
//
//  Created by Robert Amos on 18/02/09.
//  Copyright 2009 Yarra Trams. All rights reserved.
//

#import "PidsService.h"
#import <SystemConfiguration/SCNetworkReachability.h>
#import "Route.h"
#import "PidsServiceDelegate.h"
#import "Stop.h"
#import "Settings.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "BlueCatsSDK.h"

NSInteger const PIDServiceErrorNotReachable = 1001;
NSInteger const PIDServiceValidationError = 1002;
NSInteger const PIDServiceValidationErrorStopNotFound = 1003;
NSInteger const PIDServiceValidationErrorRouteNotFound = 1004;
NSInteger const PIDServiceValidationErrorOutletNotFound = 1004;
NSInteger const PIDServiceErrorOnboardNotReachable = 1005;
NSInteger const PIDServiceErrorTimeoutReached = 1006;

PIDServiceActionType const PIDServiceActionTypeUpdate = 1;
PIDServiceActionType const PIDServiceActionTypeDelete = 2;

#define FeatureIdentifierAdvertising @1
#define FeatureIdentifierBluecats @2

NSString * const kAppID = @"TTIOSJSON";

//Returns information for a stop - /GetStopsAndRoutesUpdatesSince/{lastUpdateDate}/?aid={aid}&tkn={tkn}
NSString * const kGetStopsAndRoutesUpdatesSinceURLFormat = @"GetStopsAndRoutesUpdatesSince/%@/?aid=%@&tkn=%@";

//Returns information for a POI - /GetPointsOfInterestChangesSince/{lastUpdateDate}/?aid={aid}&tkn={tkn}
NSString * const kGetPOIUpdatesSinceURLFormat = @"GetPointsOfInterestChangesSince/%@/?aid=%@&tkn=%@";

//Returns information for a POI - /GetTicketOutletChangesSince/{lastUpdateDate}/?aid={aid}&tkn={tkn}
NSString * const kGetOutletsUpdatesSinceURLFormat = @"GetTicketOutletChangesSince/%@/?aid=%@&tkn=%@";

NSString * const kGetMapsUpdates = @"GetLatestNetworkMaps/?aid=%@&tkn=%@";

//Returns a list of stops for a given route number and direction
// /GetListOfStopsByRouteNoAndDirection/{routeNo}/{isUpDirection}/?aid={aid}&tkn={tkn}
NSString * const kGetListOfStopsByRouteNoAndDirectionURLFormat = @"GetListOfStopsByRouteNoAndDirection/%@/%@/?aid=%@&tkn=%@";

//Return the main (all) routes for a stop - /GetMainRoutesForStop/{stopNo}/?aid={aid}&tkn={tkn}
NSString * const kGetMainRoutesForStopURLFormat = @"GetMainRoutesForStop/%@/?aid=%@&tkn=%@";

NSString * const kGetMainPOIsForStopURLFormat = @"GetPointsOfInterestByStopNo/%@/?aid=%@&tkn=%@";

//Return the destinations for a route - /GetDestinationsForRoute/{routeId}/?aid={aid}&tkn={tkn}
NSString * const kGetDestinationsForRouteURLFormat = @"GetDestinationsForRoute/%@/?aid=%@&tkn=%@";

//Return the information for a stop - /GetStopInformation/{stopNo}/?aid={aid}&tkn={tkn}
NSString * const kGetStopInformationURLFormat = @"GetStopInformation/%@/?aid=%@&tkn=%@";

//Return the information for a stop - /GetStopInformation/{stopNo}/?aid={aid}&tkn={tkn}
NSString * const kGetRetailerInformationURLFormat = @"GetTicketOutletById/%@/?aid=%@&tkn=%@";

NSString * const kGetPOIInformationURLFormat = @"GetPointOfInterestById/%@/?aid=%@&tkn=%@";

NSString * const kGetStopsForPOIURLFormat = @"GetStopsByPointOfInterestId/%@/?aid=%@&tkn=%@";

//Return all the updates since a specific date (yyyy-mm-dd) - /Updates/{lastUpdateDate}?authid={authid}&token={tkn}
NSString * const kGetGlobalUpdatesFormat = @"Updates/%@?authid=%@&token=%@";

// Returns predictions for a stop and route - /GetNextPredictedRoutesCollection/{stopNo}/{routeNo}/{lowFloor}/?aid={aid}&cid={cid}&tkn={tkn}
NSString * const kGetNextPredictedRoutesCollectionURLFormat = @"GetNextPredictedRoutesCollection/%@/%@/%@/?aid=%@&cid=2&tkn=%@";

NSString * const kGetNextPredictedArrivalTmeAtStopsForTramNoURLFormat = @"GetNextPredictedArrivalTimeAtStopsForTramNo/%ld/?aid=%@&tkn=%@";

NSString * const kGetDeviceTokenURLFormat = @"%@/GetDeviceToken/?aid=%@&devInfo=%@";

@interface PidsService()

@property (nonatomic, assign) BOOL backgroundedSelf;
@property (nonatomic, assign, getter = isRealtimeRequest) BOOL realtimeRequest;
//@property (strong, nonatomic) PidsServiceDelegate * serviceDelegate;
@property (strong, nonatomic) NSURLConnection * connection;

@property (strong, nonatomic) NSMutableArray * connections;
@property (strong, nonatomic) NSMutableArray * delegates;

@end

@implementation PidsService

//
// Get or set the Guid
//

+ (NSString *)baseURL:(BOOL)webAPI
{
    if(webAPI)
    { return @"http://ws3.tramtracker.com.au/TramTrackerWebAPI/api/"; }
    else
    { return @"http://ws3.tramtracker.com.au/TramTracker/RestService/"; }
    
//    if(webAPI)
//    { return @"http://qa-json.tramtracker.com.au/TramTrackerWebAPI/api/"; }
//    else
//    { return @"http://qa-json.tramtracker.com.au/TramTracker/RestService/"; }
}

- (id)init
{
    if (self = [super init])
    {
        self.backgroundedSelf = NO;
        
        self.delegates = [NSMutableArray new];
        self.connections = [NSMutableArray new];

        NSUUID * vendorID = [[UIDevice currentDevice] identifierForVendor];
        
        if (vendorID)
        { [self setGuid:[vendorID UUIDString]]; }
        else
        { [self setGuid:@"00000000-0000-0000-0000-000000000000"]; }
    }
	return self;
}

- (instancetype)initWithDelegate:(id)aDelegate
{
    if (self = [self init])
    {
        self.delegate = aDelegate;
    }
    return self;
}

//
// Clear the delegate. Set the delegate with -setDelegate (as part of the @synthesize)
// The selector you need to implement depends on the method you're calling
//
- (void)clearDelegate
{
	[self setDelegate:nil];
}


+ (AFHTTPRequestOperation*)createGETRequestForApi:(NSString*)api data:(NSDictionary*)data
{
    NSString* urlString = [NSString stringWithFormat:@"%@%@", [self baseURL:YES], api];
    BOOL firstParam = YES;
    for(NSString* key in data.allKeys)
    {
        NSString* value;
        if([data[key] isKindOfClass:[NSString class]])
        { value = data[key]; }
        else if([data[key] isKindOfClass:[NSNumber class]])
        { value = [NSString stringWithFormat:@"%f", [data[key] doubleValue]]; }
        else
        { continue; }
        urlString = [urlString stringByAppendingString:[NSString stringWithFormat:@"%@%@=%@", firstParam ? @"?" : @"&", key, value]];
        firstParam = NO;
    }
    NSURL* apiUrl = [NSURL URLWithString:urlString];
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:apiUrl];
//    [request setValue:LooclApiKey forHTTPHeaderField:@"X-API-KEY"];
    [request setHTTPMethod:@"GET"];
    AFHTTPRequestOperation* operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    return operation;
}

+ (AFHTTPRequestOperation*)createPOSTRequestForApi:(NSString*)api data:(NSDictionary*)data
{ return [self createRequestForApi:api data:data hasSerializer:YES verb:@"POST"]; }

+ (AFHTTPRequestOperation*)createRequestForApi:(NSString*)api data:(NSDictionary*)data hasSerializer:(BOOL)hasSerializer verb:(NSString*)verb
{
    NSString* urlString = [NSString stringWithFormat:@"%@%@", [self baseURL:YES], api];
    NSURL* apiUrl = [NSURL URLWithString:urlString];
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:apiUrl];
    [request setValue:@"application/json" forHTTPHeaderField:@"content-type"];
    [request setHTTPMethod:verb];
    if(data)
    { [request setHTTPBody:[NSJSONSerialization dataWithJSONObject:data options:0 error:nil]]; }
    AFHTTPRequestOperation* operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    if(hasSerializer) { operation.responseSerializer = [AFJSONResponseSerializer serializer]; }
    return operation;
}

- (void)submitNotificationRegistrationRequest:(NSString*)deviceToken UUID:(NSString*)uuid completion:(NetworkCompletionBlock)completion
{
    if((deviceToken.length == 0) || (uuid.length == 0))
    {
        NSLog(@"Failed to submit notification registration. Invalid device token/UUID supplied.\nDevice token: %@\nUUID:%@", deviceToken, uuid);
        return;
    }
    
    NSString* apiUrl = [NSString stringWithFormat:@"Registrations/?authId=%@&token=%@", kAppID, self.guid];
    NSDictionary* params = @{
        @"deviceId":uuid,
        @"notificationToken":deviceToken,
        @"deviceType":@"iOS"
    };
    AFHTTPRequestOperation* operation = [PidsService createPOSTRequestForApi:apiUrl data:params];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        completion(YES, nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        completion(NO, error);
    }];
    [operation start];
}

- (void)submitNotificationUpdateRequestWithToken:(NSString*)deviceToken routes:(NSArray*)routes times:(NSArray*)times completion:(NetworkCompletionBlock)completion
{
    if(deviceToken.length == 0)
    {
        NSLog(@"Failed to submit notification update. Invalid device token supplied.\nDevice token: %@", deviceToken);
        return;
    }
    
    NSString* apiUrl = [NSString stringWithFormat:@"NotificationRequests/?authId=%@&token=%@", kAppID, self.guid];
    NSDictionary* params = @{
        @"deviceId":self.guid,
        @"routes":routes,
        @"travelTimeFrames":times
    };
    AFHTTPRequestOperation* operation = [PidsService createPOSTRequestForApi:apiUrl data:params];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Successfully updated push notification settings");
        completion(YES, nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failed to update push notification settings");
        completion(NO, error);
    }];
    [operation start];
}

- (void)submitNotificationRegistrationDeleteRequestForUUID:(NSString*)uuid completion:(NetworkCompletionBlock)completion
{
    if(uuid.length == 0)
    {
        NSLog(@"Failed to submit notification deletetion. Invalid device token/UUID supplied.\nUUID:%@", uuid);
        return;
    }
    
    NSString* apiUrl = [NSString stringWithFormat:@"Registrations/%@?authId=%@&token=%@", uuid, kAppID, self.guid];
    AFHTTPRequestOperation* operation = [PidsService createRequestForApi:apiUrl data:@{} hasSerializer:YES verb:@"DELETE"];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Successfully deregistered for push notifications");
        completion(YES, nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failed to deregister for push notifications: %@", error.localizedDescription);
        completion(NO, error);
    }];
    [operation start];
}

- (void)submitInfoPageRequest:(NetworkCompletionBlockWithData)completion
{
    NSString* apiUrl = [NSString stringWithFormat:@"infopages/?authId=%@&token=%@", kAppID, self.guid];
    AFHTTPRequestOperation* operation = [PidsService createRequestForApi:apiUrl data:nil hasSerializer:YES verb:@"GET"];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, NSArray* responseObject) {
        NSLog(@"Successfully fetched info pages. %@ objects", @(responseObject.count));
        completion(responseObject, nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failed to fetch info pages. %@", error.localizedDescription);
        completion(nil, error);
    }];
    [operation start];
}

- (void)submitKillSwitchInfoRequest:(NetworkCompletionBlock)completion
{
    NSString* apiUrl = [NSString stringWithFormat:@"Features?token=%@&authId=%@", self.guid, kAppID];
    AFHTTPRequestOperation* operation = [PidsService createRequestForApi:apiUrl data:nil hasSerializer:YES verb:@"GET"];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, NSArray* responseObject) {
        NSLog(@"Successfully retrieved kill switch information");
        NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
        BOOL advertisingEnabled = NO;
        BOOL bluecatsEnabled = NO;
        for(NSDictionary* feature in responseObject)
        {
            NSNumber* identifier = feature[@"id"];
            NSNumber* isEnabled = feature[@"isEnabled"];
            if([identifier isEqual:FeatureIdentifierAdvertising]) // Advertising identifier
            {
                [userDefaults setBool:[isEnabled boolValue] forKey:kKillSwitchShowAdvertising];
                advertisingEnabled = [isEnabled boolValue];
            }
            else if([identifier isEqual:FeatureIdentifierBluecats]) // Bluecats identifier
            {
                [userDefaults setBool:[isEnabled boolValue] forKey:kKillSwitchUseBluecats];
                bluecatsEnabled = [isEnabled boolValue];
            }
        }
        
        if((![userDefaults boolForKey:kSettingsBluecatsDisabled]) && [userDefaults boolForKey:kKillSwitchUseBluecats])
        {
            tramTRACKERAppDelegate* appDelegate = (tramTRACKERAppDelegate*)[UIApplication sharedApplication].delegate;
            [appDelegate createBeaconManager];
            [BlueCatsSDK startPurringWithAppToken:BlueCatsKeyYarraTrams completion:^(BCStatus status) {
                if(status == kBCStatusPurring)
                { NSLog(@"Started purring"); }
                else
                { NSLog(@"Failed to start purring"); }
            }];
        }
        
        [userDefaults synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationKillSwitchUpdate object:nil userInfo:@{ kKillSwitchShowAdvertising:@(advertisingEnabled), kKillSwitchUseBluecats:@(bluecatsEnabled) }];
        completion(YES, nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failed to retrieve kill switch information");
        completion(NO, error);
    }];
    [operation start];
}

//
// Get Information about a Stop
// Your delegate must implement -setInformation:(Stop *)stop forStop:(NSNumber *)trackerID;
//
- (void)getInformationForStop:(int)stopID
{
	// make sure that we have a Guid set.
	NSAssert([self guid], @"Unable to get information for stop as no GUID is set.");
	
	// Keep the tracker ID around for later
	[self setTrackerID:[NSNumber numberWithInt:stopID]];
	
	NSMutableString *url = [NSMutableString stringWithString:[PidsService baseURL:NO]];
	
	//Return the information for a stop - /GetStopInformation/{stopNo}/?aid={aid}&tkn={tkn}
	[url appendFormat:kGetStopInformationURLFormat, [[NSNumber numberWithInt:stopID] stringValue], kAppID, self.guid];
	
	[self callRESTServiceWithURL:[NSString stringWithString:url]];
}

- (void)getInformationForRetailer:(int)retailerID
{
    self.retailerID = @(retailerID);
    
    NSMutableString *url = [NSMutableString stringWithString:[PidsService baseURL:NO]];
	[url appendFormat:kGetRetailerInformationURLFormat, [self.retailerID stringValue], kAppID, self.guid];
	[self callRESTServiceWithURL:[NSString stringWithString:url]];
}

- (void)getInformationForPOI:(int)poiID
{
    self.poiID = @(poiID);

	NSMutableString *url = [NSMutableString stringWithString:[PidsService baseURL:NO]];
    
	[url appendFormat:kGetPOIInformationURLFormat, [self.poiID stringValue], kAppID, self.guid];
	[self callRESTServiceWithURL:[NSString stringWithString:url]];
    
    
    url = [NSMutableString stringWithString:[PidsService baseURL:NO]];
    
    [url appendFormat:kGetStopsForPOIURLFormat, @(poiID).stringValue, kAppID, self.guid];
    
    [self callRESTServiceWithURL:url];
}

//
// Get Predicted Arrivals for a Stop
// Your delegate must implement -setPredictions:(NSArray *)predictions forStop:(NSNumber *)trackerID;
//
- (void)getPredictionsForStop:(int)stopID route:(Route *)route lowFloorOnly:(BOOL)lowFloorOnly
{
	NSAssert([self guid], @"Unable to get predictions for stop as no GUID is set.");
	self.realtimeRequest = YES;

	[self setTrackerID:[NSNumber numberWithInt:stopID]];

	NSMutableString *url = [NSMutableString stringWithString:[PidsService baseURL:NO]];
	
	[url appendFormat:kGetNextPredictedRoutesCollectionURLFormat, [[NSNumber numberWithInt:stopID] stringValue], route == nil ? @"0" : [@(route.internalNumber.integerValue) stringValue], lowFloorOnly ? @"true" : @"false", kAppID, self.guid];

	[self callRESTServiceWithURL:[NSString stringWithString:url]];
}

- (void)getPredictionsForStop:(int)stopID
{
	[self getPredictionsForStop:stopID route:nil lowFloorOnly:NO];
}

//
// Get scheduled departures for a stop
// Your delegate must implement -setDepartures:(NSArray *)departures forStop:(NSNumber *)trackerID;
// 
- (void)getScheduledDeparturesForStop:(int)stopID routeNumber:(NSString *)aRouteNumber atDateTime:(NSDate *)date withLowFloorOnly:(BOOL)lowFloorOnly
{
	// make sure that we have a Guid set.
	NSAssert([self guid], @"Unable to get scheduled departures for stop as no Guid is set.");
	
    [self setTrackerID:@(stopID)];

    NSMutableString * url = [[PidsService baseURL:NO] mutableCopy];

    [url appendFormat:@"GetSchedulesCollection/%@/%@/%@/%@/?aid=%@&tkn=%@", [@(stopID) stringValue], aRouteNumber, lowFloorOnly ? @"true" : @"false", [self formatDateForService:date], kAppID, self.guid];

	// call the web service
	[self callRESTServiceWithURL:url];
}

- (void)getScheduledStopsForTrip:(NSInteger)tripID scheduledDateTime:(NSDate *)date
{
    NSAssert([self guid], @"Unable to get scheduled stops for trip as no Guid is set.");
    
    NSMutableString* url = [[PidsService baseURL:NO] mutableCopy];
    
    NSString* dateString;
    NSString* timeString;
    NSString* timezoneString;
    
    // date time in format 'yyyy-MM-ddThh:mm:ss+<timezone_hours>:<timezone_minutes>
    NSDateFormatter* formatter = [NSDateFormatter new];
    formatter.dateFormat = @"yyyy-MM-dd";
    dateString = [formatter stringFromDate:date];
    formatter.dateFormat = @"HH:mm:ss";
    timeString = [formatter stringFromDate:date];
    
    long secondsOffset = [[NSTimeZone systemTimeZone] secondsFromGMT];
    timezoneString = [NSString stringWithFormat:@"%02d:%02d", (int)(secondsOffset/3600), (int)((secondsOffset%3600)/60)];
    
    NSString* assembledTimeString = [NSString stringWithFormat:@"%@T%@+%@", dateString, timeString, timezoneString];
    
    [url appendFormat:@"GetSchedulesForTrip/%@/%@/?aid=%@&tkn=%@", @(tripID), assembledTimeString, kAppID, self.guid];
    [self callRESTServiceWithURL:url];
}

//
// Get the Route List
//
- (void)getRouteList
{
	// make sure that we have a Guid set.
	NSAssert([self guid], @"Unable to get route list as no GUID is set.");
	
    NSAssert(NO, @"getRouteList not defined");

	// No parameters needed
//	[self callWebServiceMethod:@"GetDestinationsForAllRoutes"];
}

//
// Get a list of destinations for a specific route
//
- (void)destinationsForRoute:(Route *)route
{
	// make sure that we have a Guid set.
	NSAssert([self guid], @"Unable to get stop list as no GUID is set.");
	
	[self setRouteNumber:route.internalNumber];
	
	NSMutableString *url = [NSMutableString stringWithString:[PidsService baseURL:NO]];
	
	//Return the destinations for a route - /GetDestinationsForRoute/{routeId}/?aid={aid}&tkn={tkn}
	[url appendFormat:kGetDestinationsForRouteURLFormat, route.internalNumber, kAppID, self.guid];
	
	[self callRESTServiceWithURL:[NSString stringWithString:url]];
}

//
// Get the list of stops for a specific route and direction
// Your delegate should implement -setStopList:(NSArray *)stops forRouteNumber:(NSString *)routeNumber;
//
- (void)getStopListForRoute:(Route *)route upDirection:(BOOL)isUpDirection
{
	// make sure that we have a Guid set.
	NSAssert([self guid], @"Unable to get stop list as no GUID is set.");

	NSMutableString *url = [NSMutableString stringWithString:[PidsService baseURL:NO]];
	
	[url appendFormat:kGetListOfStopsByRouteNoAndDirectionURLFormat, route.internalNumber, isUpDirection ? @"true" : @"false", kAppID, self.guid];
	
	[self setRouteNumber:route.internalNumber];
	[self setUpDirection:[NSNumber numberWithBool:isUpDirection]];
	
	[self callRESTServiceWithURL:[NSString stringWithString:url]];
}

//
// Get info on a journey for a tram number
//
- (void)getJourneyForTramNumber:(NSNumber *)tramNumber
{
	NSAssert([self guid], @"Unable to get journey as no Guid is set.");
	self.realtimeRequest = YES;

//	NSDictionary *params = [NSDictionary dictionaryWithObject:tramNumber forKey:@"tramNo"];
//	[self callWebServiceMethod:@"GetNextPredictedArrivalTimeAtStopsForTramNo" withParameters:params];

    NSMutableString * url = [NSMutableString stringWithString:[PidsService baseURL:NO]];

    [url appendFormat:kGetNextPredictedArrivalTmeAtStopsForTramNoURLFormat, (long)tramNumber.integerValue, kAppID, self.guid];
    [self callRESTServiceWithURL:url];
}

//
// Get a list of updates since date and time
//
- (void)updatesSinceDate:(NSDate *)date
{
	NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];

	[dateFormatter setDateFormat:@"yyyy-MM-dd"];
	NSString        * dateString = [dateFormatter stringFromDate:date];
	NSMutableString * url = [NSMutableString stringWithString:[PidsService baseURL:YES]];
    [url appendFormat:kGetGlobalUpdatesFormat, dateString, kAppID, self.guid];
    [self callRESTServiceWithURL:url];
}

//
// Get the number of routes through a stop
//
- (void)routesThroughStop:(Stop *)stop
{
	// if it is a stop that is a terminus (end terminus), dont bother the server won't return anything
	if ([stop.trackerID integerValue] >= 8000)
	{
		[self failWithError:[NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
												code:PIDServiceValidationErrorStopNotFound
											userInfo:nil]];
		return;
	}

	// Create the parameters
//	NSDictionary *params = [NSDictionary dictionaryWithObject:stop.trackerID forKey:@"stopNo"];
//	[self setTrackerID:stop.trackerID];
//	
//	// call the thingy
//	[self callWebServiceMethod:@"GetMainRoutesForStop" withParameters:params];
	
	[self setTrackerID:stop.trackerID];
	
	NSMutableString *url = [NSMutableString stringWithString:[PidsService baseURL:NO]];
	
	[url appendFormat:kGetMainRoutesForStopURLFormat, [stop.trackerID stringValue], kAppID, self.guid];
	
	[self callRESTServiceWithURL:[NSString stringWithString:url]];
}

- (void)POIsThroughStop:(Stop *)aStop
{
    NSMutableString * url = [NSMutableString stringWithString:[PidsService baseURL:NO]];
    
    [url appendFormat:kGetMainPOIsForStopURLFormat, aStop.trackerID.stringValue, kAppID, self.guid];

    [self callRESTServiceWithURL:url];
}



- (void)callRESTServiceWithURL:(NSString *)url
{
	// Make sure all requests aren't performed on the main thread
	if ([NSThread isMainThread])
	{
		self.backgroundedSelf = YES;

		[self performSelectorInBackground:@selector(callRESTServiceWithURL:) withObject:url];
		return;
	}

    NSLog(@"Calling: %@", url);
    
	//Autorelease pool because we're in the background and apple non-arc code can use autorelease,
	//and without an autorelease pool they will leak
	@autoreleasepool {
		// Make sure that we have access to the interwebs
		if (![self hasAccessToService])
			return;

		NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];

        request.timeoutInterval = 60;

		// We POST the data
		[request setHTTPMethod:@"GET"];

		//NSLog(@"[PIDSSERVICE] Calling REST API with URL: %@", url);

        PidsServiceDelegate * delegate = [[PidsServiceDelegate alloc] initWithDelegate:[self delegate]];
        
		[self.delegates addObject:delegate];

		// if we have a trackerID pass it through to the delegate
		if (self.trackerID != nil)
			[delegate setTrackerID:self.trackerID];
		if (self.routeNumber != nil)
			[delegate setRouteNumber:self.routeNumber];
		if (self.upDirection != nil)
			[delegate setUpDirection:self.upDirection];

        [delegate manageTimer];

		// Set the request date and time
		[delegate setRequestTime:[NSDate date]];

		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        
        if([NSThread isMainThread])
        { [self.connections addObject:[NSURLConnection connectionWithRequest:request delegate:delegate]]; }
        else
        {
            NSURLResponse* response;
            NSError* error;
            NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            if(response)
            {
                [delegate connection:nil didReceiveResponse:response];
                if(data)
                { [delegate connection:nil didReceiveData:data]; }
                [delegate connectionDidFinishLoading:nil];
            }
            else
            { [delegate connection:nil didFailWithError:error]; }
        }
	}
}

- (void)cancel
{
    for (NSURLConnection * connection in self.connections) {
        [connection cancel];
    }
    
    for (PidsServiceDelegate * delegate in self.delegates) {
        [delegate cancel];
    }
    
    self.connections = [NSMutableArray new];
    self.delegates = [NSMutableArray new];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

- (void)dealloc
{
    [self cancel];
}

//
// Format a date for the service
//
- (NSString *)formatDateForService:(NSDate *)date
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:00ZZZ"];
	NSMutableString *dateString = [NSMutableString stringWithString:[formatter stringFromDate:date]];
	[dateString insertString:@":" atIndex:22];
	return dateString;
}

#pragma mark -
#pragma mark Checking service availablility and error handling

- (void)failWithError:(NSError *)error
{
	// check the delegate to see if we can report the error
	if ([self.delegate respondsToSelector:@selector(pidsServiceDidFailWithError:)])
	{
		[self.delegate performSelectorOnMainThread:@selector(pidsServiceDidFailWithError:) withObject:error waitUntilDone:NO];
	}
}

// Check for the availability of the service
- (BOOL)hasAccessToService
{
	NSURL *host = [NSURL URLWithString:[[self class] baseURL:NO]];
	SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(NULL, [[host host] UTF8String]);
	SCNetworkReachabilityFlags flags;
	SCNetworkReachabilityGetFlags(reachability, &flags);

	BOOL isReachable = flags & kSCNetworkReachabilityFlagsReachable;
	
	// close the reachability ref
	CFRelease(reachability);
	
	if (isReachable)
		return YES;

	// is the service not reachable and a connection is required
	NSString *errorMessage = [NSString stringWithFormat:NSLocalizedString(@"errors-nonetwork", @"No internet error"), (self.isRealtimeRequest ? NSLocalizedString(@"errors-snippet-realtime", @"Real Time") : NSLocalizedString(@"errors-snippet-scheduled", @"Scheduled")), [[UIDevice currentDevice] model]];
	NSString *recoverySuggestion = [NSString stringWithFormat:NSLocalizedString(@"errors-nonetwork-suggestion", @"No internet suggestion"), [[UIDevice currentDevice] model]];
	NSMutableDictionary *userInfo = [NSMutableDictionary new];
	[userInfo setObject:errorMessage forKey:NSLocalizedDescriptionKey];
	[userInfo setObject:recoverySuggestion forKey:NSLocalizedRecoverySuggestionErrorKey];
	NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier] code:PIDServiceErrorNotReachable userInfo:[NSDictionary dictionaryWithDictionary:userInfo]];
	[self failWithError:error];
	return NO;
}

//
// Clean Up
//
@end
