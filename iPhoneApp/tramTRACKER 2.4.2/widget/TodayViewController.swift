//
//  TodayViewController.swift
//  widget
//
//  Created by Jonathan Head on 30/06/2015.
//  Copyright (c) 2015 AppsCore. All rights reserved.
//

import UIKit
import NotificationCenter
import Fabric
import Crashlytics

enum TramTrackerLaunchMode {
    case Home
    case Nearby
    case Favourites
    case PID
}

extension NSDate {
    class func dateFromString(dateStr: String) -> NSDate
    {
        let dateFormatter: NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        return dateFormatter.dateFromString(dateStr)!
    }
}

class TodayViewController: UIViewController, NSURLConnectionDataDelegate, NCWidgetProviding {
    @IBOutlet weak var stopName:              UILabel!
    @IBOutlet weak var stopDescription:       UILabel!
    @IBOutlet weak var stopFreeTramZone:      UIImageView!
    
    @IBOutlet var containerUniversal: UIView! // this shows the stop name, description, and FTZ icon. It should always be shown if any of the predictions are shown.
    @IBOutlet var containerIOS9: UIView!
    @IBOutlet var containerIOS10Compact: UIView!
    @IBOutlet var containerIOS10Expanded: UIView!
    
    @IBOutlet var firstRouteNumbers         : [UILabel]!
    @IBOutlet var firstRouteNames           : [UILabel]!
    @IBOutlet var firstRoutePredictions     : [UILabel]!
    @IBOutlet var firstRouteAirconIcons     : [UIImageView]!
    @IBOutlet var firstRouteDisruptionIcons : [UIImageView]!
    @IBOutlet var firstRouteUpdateIcons     : [UIImageView]!
    @IBOutlet var firstRouteLowFloorIcons   : [UIImageView]!
    
    @IBOutlet var secondRouteNumbers         : [UILabel]!
    @IBOutlet var secondRouteNames           : [UILabel]!
    @IBOutlet var secondRoutePredictions     : [UILabel]!
    @IBOutlet var secondRouteAirconIcons     : [UIImageView]!
    @IBOutlet var secondRouteDisruptionIcons : [UIImageView]!
    @IBOutlet var secondRouteUpdateIcons     : [UIImageView]!
    @IBOutlet var secondRouteLowFloorIcons   : [UIImageView]!
    @IBOutlet var secondRouteDividers        : [UIView]!
    
    @IBOutlet var thirdRouteNumbers         : [UILabel]!
    @IBOutlet var thirdRouteNames           : [UILabel]!
    @IBOutlet var thirdRoutePredictions     : [UILabel]!
    @IBOutlet var thirdRouteAirconIcons     : [UIImageView]!
    @IBOutlet var thirdRouteDisruptionIcons : [UIImageView]!
    @IBOutlet var thirdRouteUpdateIcons     : [UIImageView]!
    @IBOutlet var thirdRouteLowFloorIcons   : [UIImageView]!
    @IBOutlet var thirdRouteDividers        : [UIView]!
    
    @IBOutlet var routesShownLabels : [UILabel]!
    @IBOutlet var lastUpdateLabels  : [UILabel]!
    
    @IBOutlet var errorMessage     : UILabel!
    @IBOutlet var loadingContainer : UIView!
    @IBOutlet var loadingLabel     : UILabel!
    @IBOutlet var loadingIndicator : UIActivityIndicatorView!
    
    @IBOutlet var errorToTop         : NSLayoutConstraint! // This constraint snaps the top of the error message to the top of the container. It should be used when the message should cover the entire container.
    @IBOutlet var errorToStopDetails : NSLayoutConstraint! // This constraint snaps the top of the error message to the bottom of the stop details. It should be used when both the stop details and the error message should be shown.
    
    var favouritesList: [AnyObject!]?
    let locationManager: CLLocationManager = CLLocationManager()
    var closestTrackerId:  NSNumber?
    var closestLowFloor:   String?
    var closestRoutes:     String?
    
    var isUpdating: Bool = false;
    var launchMode: TramTrackerLaunchMode = .Home
    var numberResults: Int = 0
    
    let singleResultHeight = 53
    var expandedHeight: Int {
        if numberResults == 0 { return 250 }
        else
        { return 250 - (singleResultHeight * (3 - self.numberResults)) }
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        Crashlytics.startWithAPIKey("1c523be2ad73a1438832cbb47a77cebdef92e66e")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.errorToStopDetails.active   = false
        self.errorToStopDetails.constant = 0
        
        if #available(iOSApplicationExtension 10.0, *) {
            self.stopName.textColor        = UIColor.blackColor()
            self.stopDescription.textColor = UIColor.blackColor()
            self.errorMessage.textColor    = UIColor.blackColor()
            self.loadingLabel.textColor    = UIColor.blackColor()
            self.stopFreeTramZone.image    = UIImage(named:"FTZ_Logo_Black")
            self.loadingIndicator.activityIndicatorViewStyle = .Gray
            self.extensionContext!.widgetLargestAvailableDisplayMode = .Expanded
        }
        else
        {
            self.stopName.textColor        = UIColor.whiteColor()
            self.stopDescription.textColor = UIColor.whiteColor()
            self.errorMessage.textColor    = UIColor.whiteColor()
            self.loadingLabel.textColor    = UIColor.whiteColor()
            self.stopFreeTramZone.image    = UIImage(named: "PID_FTZ")
            self.loadingIndicator.activityIndicatorViewStyle = .White
        }
        
        self.preferredContentSize = CGSize(width: 200, height:self.expandedHeight)
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    deinit
    { locationManager.stopUpdatingLocation() }
    
    func setText(text: String?, labels: [UILabel])
    { for label in labels { label.text = text } }
    
    func setVisibility(visible: Bool, views: [UIView])
    { for view in views { view.hidden = !visible } }
    
    func setAlpha(alpha: CGFloat, views: [UIView])
    { for view in views { view.alpha = alpha } }
    
    @available(iOSApplicationExtension 10.0, *)
    func widgetActiveDisplayModeDidChange(activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize) {
        switch activeDisplayMode {
        case .Compact:
            self.preferredContentSize = CGSize(width: 200, height: 110)
            if (self.containerIOS10Compact.alpha == 0.0) && (self.containerIOS10Expanded.alpha == 1.0) {
                UIView.animateWithDuration(0.3, animations: { 
                    self.containerIOS10Expanded.alpha = 0.0
                    self.containerIOS10Compact.alpha = 1.0
                })
            }
            break
        case .Expanded:
            if (self.errorMessage.alpha >= 0.9) || (self.loadingContainer.alpha >= 0.9)
            { self.preferredContentSize = CGSize(width: 200, height: 60) }
            else
            { self.preferredContentSize = CGSize(width: 200, height: self.expandedHeight) }
            if (self.containerIOS10Expanded.alpha == 0.0) && (self.containerIOS10Compact.alpha == 1.0) {
                UIView.animateWithDuration(0.3, animations: { 
                    self.containerIOS10Expanded.alpha = 1.0
                    self.containerIOS10Compact.alpha = 0.0
                })
            }
            break
        }
    }
    
    func widgetPerformUpdateWithCompletionHandler(completionHandler: ((NCUpdateResult) -> Void))
    {
        completionHandler(.NewData)
        if(isUpdating)
        { completionHandler(NCUpdateResult.NoData) }
        else
        {
            isUpdating = true
            getFavourites(completionHandler)
        }
    }
    
    func getFavourites(completionHandler: ((NCUpdateResult) -> Void)?)
    {
        let appId:     String   = "TTIOSJSON"
        let defaults = NSUserDefaults(suiteName: "group.au.com.yarratrams.tramtracker")!
        
        let lastUpdateTime: Double = defaults.doubleForKey("lastUpdate")
        
        if lastUpdateTime == 0
        { self.setDisplayLastUpdate(lastUpdateTime) }
        
        if let favouritesData = defaults.arrayForKey("favouriteStopsList")
        {
            if(favouritesData.count <= 0)
            {
                self.setDisplayErrorNoFavourites();
                completionHandler?(NCUpdateResult.NewData)
                return
            }
            
            self.favouritesList = []
            var closestFavourite: NSDictionary?
            let currentLocation: CLLocation? = locationManager.location
            var closestDistance: Double = 2000.0
            
            if let location = currentLocation
            {
                for favourite in favouritesData
                {
                    let latitude: NSNumber  = favourite["latitude"]  as! NSNumber
                    let longitude: NSNumber = favourite["longitude"] as! NSNumber
                    let distance = location.distanceFromLocation(CLLocation(latitude: latitude.doubleValue, longitude: longitude.doubleValue))
                    if(distance < closestDistance)
                    {
                        closestDistance = distance
                        closestFavourite = favourite as? NSDictionary
                    }
                }
                
                if let favourite = closestFavourite
                {
                    let trackerId:    NSNumber = favourite["trackerid"]     as! NSNumber
                    let routeNum:     String   = favourite["route"]         as! String
                    let isLowFloor:   Bool     = ((favourite["lowfloor"]    as! Int) != 0)
                    let lowFloor:     String   = (isLowFloor ? "true" : "false")
                    let freeTramZone: Bool     = (favourite["freetramzone"] as! Int) != 0
                    let routeName:    String   = favourite["name"]          as! String
                    let routeSubName: String   = favourite["subname"]       as! String
                    let guid: String
                    if let vendorID = UIDevice.currentDevice().identifierForVendor
                    { guid = vendorID.UUIDString }
                    else
                    { guid = "00000000-0000-0000-0000-000000000000" }
                    self.closestTrackerId = trackerId
                    self.closestLowFloor  = lowFloor
                    self.closestRoutes    = routeNum
                    
                    self.stopName.text = routeName
                    self.stopDescription.text = routeSubName
                    self.stopFreeTramZone.hidden = !freeTramZone
                    
                    self.setDisplayShowLoading()
//                    let url: String = "http://qa-json.tramtracker.com.au/TramTrackerWebAPI/api/predictions/widget/\(trackerId)"
                    let url: String = "http://ws3.tramtracker.com.au/TramTrackerWebAPI/api/predictions/widget/\(trackerId)"
                    request(Method.GET, url, parameters: ["authId":appId, "token":guid, "routeNo":routeNum, "lowFloorOnly":lowFloor], encoding: ParameterEncoding.URL)
                        .responseJSON { (data) in
                            
                            if let json = data.result.value as? NSArray
                            {
                                print("json: \(json)")
                                
                                if json.count <= 0
                                {
                                    self.setDisplayErrorNoResults([ "1" ], lowFloor: isLowFloor)
                                    completionHandler?(NCUpdateResult.Failed)
                                    return
                                }
                                
                                var routes = Set<String>()
                                
                                if let firstEntry = json[0] as? NSDictionary
                                {
                                    let routeNo = firstEntry["routeNo"] as? NSNumber
                                    
                                    self.setText("\(routeNo!)", labels: self.firstRouteNumbers)
                                    self.setText(firstEntry["destination"] as? String, labels: self.firstRouteNames)
                                    self.setText(self.getMinutesRemainingFromPrediction(firstEntry["arrivalTime"] as! String), labels: self.firstRoutePredictions)
                                    self.setVisibility((firstEntry["hasAirCon"]       as? Int) != 0, views: self.firstRouteAirconIcons)
                                    self.setVisibility((firstEntry["isLowFloor"]      as? Int) != 0, views: self.firstRouteLowFloorIcons)
                                    self.setVisibility((firstEntry["hasDisruption"]   as? Int) != 0, views: self.firstRouteDisruptionIcons)
                                    self.setVisibility((firstEntry["hasSpecialEvent"] as? Int) != 0, views: self.firstRouteUpdateIcons)
                                    
                                    routes.insert(routeNo!.stringValue)
                                }
                            
                                if json.count > 1 {
                                    if let secondEntry = json[1] as? NSDictionary
                                    {
                                        let routeNo = secondEntry["routeNo"] as? NSNumber
                                        
                                        self.setText("\(routeNo!)", labels: self.secondRouteNumbers)
                                        self.setText(secondEntry["destination"] as? String, labels: self.secondRouteNames)
                                        self.setText(self.getMinutesRemainingFromPrediction(secondEntry["arrivalTime"] as! String), labels: self.secondRoutePredictions)
                                        self.setVisibility((secondEntry["hasAirCon"]       as? Int) != 0, views: self.secondRouteAirconIcons)
                                        self.setVisibility((secondEntry["isLowFloor"]      as? Int) != 0, views: self.secondRouteLowFloorIcons)
                                        self.setVisibility((secondEntry["hasDisruption"]   as? Int) != 0, views: self.secondRouteDisruptionIcons)
                                        self.setVisibility((secondEntry["hasSpecialEvent"] as? Int) != 0, views: self.secondRouteUpdateIcons)
                                        
                                        routes.insert(routeNo!.stringValue)
                                    }
                                }
                                else
                                {
                                    self.setText("-", labels: self.secondRouteNumbers)
                                    self.setText("-", labels: self.secondRouteNames)
                                    self.setText("-", labels: self.secondRoutePredictions)
                                    self.setVisibility(false, views: self.secondRouteAirconIcons)
                                    self.setVisibility(false, views: self.secondRouteDisruptionIcons)
                                    self.setVisibility(false, views: self.secondRouteUpdateIcons)
                                    self.setVisibility(false, views: self.secondRouteLowFloorIcons)
                                }
                                
                            
                                if json.count > 2
                                {
                                    if let thirdEntry = json[2] as? NSDictionary
                                    {
                                        let routeNo = thirdEntry["routeNo"] as? NSNumber
                                        
                                        self.setText("\(routeNo!)", labels: self.thirdRouteNumbers)
                                        self.setText(thirdEntry["destination"] as? String, labels: self.thirdRouteNames)
                                        self.setText(self.getMinutesRemainingFromPrediction(thirdEntry["arrivalTime"] as! String), labels: self.thirdRoutePredictions)
                                        self.setVisibility((thirdEntry["hasAirCon"]       as? Int) != 0, views: self.thirdRouteAirconIcons)
                                        self.setVisibility((thirdEntry["isLowFloor"]      as? Int) != 0, views: self.thirdRouteLowFloorIcons)
                                        self.setVisibility((thirdEntry["hasDisruption"]   as? Int) != 0, views: self.thirdRouteDisruptionIcons)
                                        self.setVisibility((thirdEntry["hasSpecialEvent"] as? Int) != 0, views: self.thirdRouteUpdateIcons)
                                        
                                        routes.insert(routeNo!.stringValue)
                                    }
                                }
                                else // only two JSON fields were returned. Hide the third, and resize the widget as required.
                                {
                                    self.setText("-", labels: self.thirdRouteNumbers)
                                    self.setText("-", labels: self.thirdRouteNames)
                                    self.setText("-", labels: self.thirdRoutePredictions)
                                    self.setVisibility(false, views: self.thirdRouteAirconIcons)
                                    self.setVisibility(false, views: self.thirdRouteDisruptionIcons)
                                    self.setVisibility(false, views: self.thirdRouteUpdateIcons)
                                    self.setVisibility(false, views: self.thirdRouteLowFloorIcons)
                                }
                                
                                
                                
                                self.setDisplayShowFavourites(json.count)
                                
                                self.setDisplayRoutes(routes, lowFloor: isLowFloor)
                                self.setDisplayLastUpdate(NSDate().timeIntervalSince1970)
                                defaults.setDouble(NSDate().timeIntervalSince1970, forKey: "lastUpdate")
                                defaults.synchronize()
                                completionHandler?(NCUpdateResult.NewData)
                                return
                            }
                            else
                            {
                                self.setDisplayErrorNoDataConnection()
                                completionHandler?(NCUpdateResult.Failed)
                                return
                            }
                    }
                    completionHandler?(NCUpdateResult.NewData)
                    return
                }
                else
                {
                    self.setDisplayErrorNearbyFavourites()
                    completionHandler?(NCUpdateResult.NewData)
                    return
                }
            }
            else
            {
                self.setDisplayErrorNoLocation()
                completionHandler?(NCUpdateResult.NewData)
                return
            }
        }
        else
        { self.setDisplayErrorNoFavourites() }
        
        completionHandler?(NCUpdateResult.Failed)
        return
    }
    
    @IBAction func gotoApp(sender: UIButton) {
        let url: NSURL
        switch self.launchMode {
        case .Home:
            url = NSURL(string: "tramtracker://home")!
        case .Nearby:
            url = NSURL(string: "tramtracker://nearby")!
        case .Favourites:
            url = NSURL(string: "tramtracker://favourites")!
        case .PID:
            url = NSURL(string: "tramtracker://pid?trackerId=\(self.closestTrackerId!)&lowFloor=\(self.closestLowFloor!)&routes=\(self.closestRoutes!)")!
        }
        self.extensionContext?.openURL(url, completionHandler: nil)
    }
    
    func getMinutesRemainingFromPrediction(predictionTimeStr: String) -> String {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let predictionTime: NSTimeInterval = formatter.dateFromString(predictionTimeStr)!.timeIntervalSince1970
        
        let currentTime: NSTimeInterval = NSDate().timeIntervalSince1970
        let timeDelta = predictionTime - currentTime;
        if(timeDelta < -60)
        { return "departed" }
        if(timeDelta < 60)
        { return "now" }
        
        if(predictionTime - currentTime < 3600) // 60 minutes
        {
            let minutes: Int = Int((predictionTime - currentTime) / 60)
            return "\(minutes)"
        }
        else
        {
            let dateFormatter: NSDateFormatter = NSDateFormatter()
            dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
            return dateFormatter.stringFromDate(NSDate(timeIntervalSince1970: predictionTime))
        }
    }
    
    func setDisplayLastUpdate(lastUpdateDate: NSTimeInterval) {
        let dateFormatter: NSDateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
        dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
        
        let date: NSDate = NSDate(timeIntervalSince1970: lastUpdateDate)
        let dateString: String = dateFormatter.stringFromDate(date)
        self.setText("Last update: \(dateString)", labels: self.lastUpdateLabels)
    }
    
    func setDisplayRoutes(routes: Set<String>, lowFloor: Bool) {
        let tramType: String = lowFloor ? "low floor" : "all"
        
        
        let routeArray = Array(routes).sort( { (firstObj: String, secondObj: String) -> Bool in
            return firstObj.localizedStandardCompare(secondObj) == NSComparisonResult.OrderedAscending
        })
        
        if(routes.count == 1)
        {
            self.setText("Showing \(tramType) trams on route \(routes.first!)", labels: self.routesShownLabels)
        }
        else if(routes.count > 1)
        {
            var message = "Showing \(tramType) trams on routes "
            for str in routeArray
            { message += "\(str), " }
            self.setText(message.substringToIndex(message.endIndex.predecessor().predecessor()), labels: self.routesShownLabels)
        }
    }
    
    func setDisplayErrorNoResults(routes: Set<String>, lowFloor: Bool) {
        let tramType: String = lowFloor ? "low floor " : ""

        self.setDisplayErrorWithStopDetails("There may be no \(tramType)trams on your route")
        self.launchMode = .PID
    }
    
    func setDisplayErrorNoDataConnection() {
        self.setDisplayError("tramTRACKER was unable to get predictions.\n\nPlease check your data connection.")
        self.launchMode = .Home
    }
    
    func setDisplayErrorNoLocation() {
        self.setDisplayError("tramTRACKER was unable to get your location.\n\nTap here to open tramTRACKER.")
        self.launchMode = .Home
    }
    
    func setDisplayErrorNearbyFavourites() {
        self.setDisplayError("You have no favourites within 15 minutes walking distance.\n\nTap here to open tramTRACKER to view nearby stops.")
        self.launchMode = .Nearby
    }
    
    func setDisplayErrorNoFavourites() {
        self.setDisplayError("You currently have no favourites saved.\n\nTap here to open tramTRACKER to set up your favourites.")
        self.launchMode = .Nearby
    }
    
    func setDisplayErrorWithStopDetails(message: String) {
        self.errorMessage!.text = message
        
        self.errorToStopDetails.active = true
        self.errorToTop.active = false
        
        self.preferredContentSize = CGSize(width: 0, height: 110)
        if #available(iOSApplicationExtension 10.0, *) { self.extensionContext!.widgetLargestAvailableDisplayMode = .Compact }
        
        UIView.animateWithDuration(0.7, animations: { () -> Void in
            self.containerUniversal.alpha     = 1.0
            self.containerIOS9.alpha          = 0.0
            self.containerIOS10Compact.alpha  = 0.0
            self.containerIOS10Expanded.alpha = 0.0
            self.loadingContainer!.alpha      = 0.0
            self.errorMessage!.alpha          = 1.0
            self.view.layoutIfNeeded()
        })
    }
    
    func setDisplayError(message: String) {
        self.errorMessage!.text = message

        self.errorToStopDetails.active = false
        self.errorToTop.active = true
        
        self.preferredContentSize = CGSize(width: 0, height: 60)
        if #available(iOSApplicationExtension 10.0, *) { self.extensionContext!.widgetLargestAvailableDisplayMode = .Compact }
        
        UIView.animateWithDuration(0.7, animations: { () -> Void in
            self.containerUniversal.alpha     = 0.0
            self.containerIOS9.alpha          = 0.0
            self.containerIOS10Compact.alpha  = 0.0
            self.containerIOS10Expanded.alpha = 0.0
            self.loadingContainer!.alpha      = 0.0
            self.errorMessage!.alpha          = 1.0
            self.view.layoutIfNeeded()
        })
    }
    
    func setDisplayShowLoading() {
        self.launchMode = .Home
        
        self.preferredContentSize = CGSize(width: 0, height: 60)
        if #available(iOSApplicationExtension 10.0, *) { self.extensionContext!.widgetLargestAvailableDisplayMode = .Compact }
        
        UIView.animateWithDuration(0.7, animations: { () -> Void in
            self.containerUniversal.alpha     = 0.0
            self.containerIOS9.alpha          = 0.0
            self.containerIOS10Compact.alpha  = 0.0
            self.containerIOS10Expanded.alpha = 0.0
            self.loadingContainer!.alpha      = 1.0
            self.errorMessage!.alpha          = 0.0
            
            self.view.layoutIfNeeded()
        })
    }
    
    func setDisplayShowFavourites(resultsCount: Int) {
        self.numberResults = resultsCount
        self.launchMode = .PID
        
        if #available(iOSApplicationExtension 10.0, *) {
            self.extensionContext!.widgetLargestAvailableDisplayMode = .Expanded
            if self.extensionContext?.widgetActiveDisplayMode == .Expanded
            { self.preferredContentSize = CGSize(width: 200, height: self.expandedHeight) }
            else
            { self.preferredContentSize = CGSize(width: 200, height: 110) }
        }
        else
        { self.preferredContentSize = CGSize(width: 200, height: self.expandedHeight) }
        
        
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        
        
        UIView.animateWithDuration(0.7, animations: { () -> Void in
            self.containerUniversal.alpha = 1.0
            if #available(iOSApplicationExtension 10.0, *) {
                self.containerIOS10Expanded.alpha = self.extensionContext?.widgetActiveDisplayMode == .Expanded ? 1.0 : 0.0
                self.containerIOS10Compact.alpha  = self.extensionContext?.widgetActiveDisplayMode == .Compact  ? 1.0 : 0.0
                self.containerIOS9.alpha = 0.0
            } else {
                self.containerIOS10Expanded.alpha = 0.0
                self.containerIOS10Compact.alpha = 0.0
                self.containerIOS9.alpha = 1.0
            }
            
            self.loadingContainer!.alpha               = 0.0
            self.errorMessage!.alpha                   = 0.0
            
            self.setAlpha(resultsCount >= 2 ? 1.0 : 0.0, views: self.secondRouteNames)
            self.setAlpha(resultsCount >= 2 ? 1.0 : 0.0, views: self.secondRouteNumbers)
            self.setAlpha(resultsCount >= 2 ? 1.0 : 0.0, views: self.secondRoutePredictions)
            self.setAlpha(resultsCount >= 2 ? 1.0 : 0.0, views: self.secondRouteAirconIcons)
            self.setAlpha(resultsCount >= 2 ? 1.0 : 0.0, views: self.secondRouteDisruptionIcons)
            self.setAlpha(resultsCount >= 2 ? 1.0 : 0.0, views: self.secondRouteUpdateIcons)
            self.setAlpha(resultsCount >= 2 ? 1.0 : 0.0, views: self.secondRouteLowFloorIcons)
            self.setAlpha(resultsCount >= 2 ? 1.0 : 0.0, views: self.secondRouteDividers)
            
            self.setAlpha(resultsCount >= 3 ? 1.0 : 0.0, views: self.thirdRouteNames)
            self.setAlpha(resultsCount >= 3 ? 1.0 : 0.0, views: self.thirdRouteNumbers)
            self.setAlpha(resultsCount >= 3 ? 1.0 : 0.0, views: self.thirdRoutePredictions)
            self.setAlpha(resultsCount >= 3 ? 1.0 : 0.0, views: self.thirdRouteAirconIcons)
            self.setAlpha(resultsCount >= 3 ? 1.0 : 0.0, views: self.thirdRouteDisruptionIcons)
            self.setAlpha(resultsCount >= 3 ? 1.0 : 0.0, views: self.thirdRouteUpdateIcons)
            self.setAlpha(resultsCount >= 3 ? 1.0 : 0.0, views: self.thirdRouteLowFloorIcons)
            self.setAlpha(resultsCount >= 3 ? 1.0 : 0.0, views: self.thirdRouteDividers)
            
            self.view.layoutIfNeeded()
        })
    }

    func widgetMarginInsetsForProposedMarginInsets(defaultMarginInsets: UIEdgeInsets) -> (UIEdgeInsets)
    { return UIEdgeInsets(top: 0, left: defaultMarginInsets.left, bottom: 0, right: 0); }
    
}
