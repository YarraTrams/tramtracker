package com.yarratrams.tramtracker.tasks;

import java.util.ArrayList;
import java.util.Date;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.db.TTDB;
import com.yarratrams.tramtracker.objects.Constants;
import com.yarratrams.tramtracker.objects.NearbyFavourite;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.singleton.TTDBSingleton;
import com.yarratrams.tramtracker.ui.PIDActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;

public class NearbyFavouriteTask extends AsyncTask<Void, Void, Void> {
	private LocationManager locationManager;
	private MyLocationListener locationListener;
	private Location location = null;
	PIDActivity pidActivity;
	boolean bestLocationRetrieved = false;
	public boolean islocationServiceAvailable = true;
	final NearbyFavouriteTask parent = this;
	GPSLocationUpdateTask.GPSLocationListener gpsLocationListener;
	private ArrayList<NearbyFavourite> alNearbyFavourites;
	
	public boolean cancelTask = false;
	
	public boolean isCancelTask() {
		return cancelTask;
	}

	public void cancelTask(boolean cancelTask) {
		this.cancelTask = cancelTask;
	}
	
	public NearbyFavouriteTask(PIDActivity pidActivity) {
		this.pidActivity = pidActivity;
	}
	
	public Location getLocation() {
		return location;
	}
	
	public void removeUpdates(){
		if(locationManager != null){
			if(locationListener != null)
				locationManager.removeUpdates(locationListener);
			if(gpsLocationListener != null)
				locationManager.removeUpdates(gpsLocationListener);
			locationManager = null;
		}
	}
	
	public void displayErrorMessage(String strMessage){
		Message message = new Message();
		Bundle bundle = new Bundle();
		bundle.putString("error", strMessage);
		message.setData(bundle);
		TramTrackerMainActivity.getAppManager().handlerToast.sendMessage(message); 
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		
		// initialize location manager
		locationManager = (LocationManager) pidActivity.getSystemService(Context.LOCATION_SERVICE);
		
		if(locationManager == null || (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))){
			islocationServiceAvailable = false;
			displayErrorMessage(pidActivity.getString(R.string.error_locationservices_gps_notavailable));
		}
		else{
//			System.out.println("NearbyFavouriteTask  location listener initialized");
			// Define a listener that responds to location updates
			locationListener = new MyLocationListener();
			// Register the listener with the Location Manager to receive location updates
			locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, Constants.kLocationServicesMinUpdateTime, Constants.kLocationServicesMinUpdateDistance, locationListener);
			islocationServiceAvailable = true;
		}
	}
	
	protected Void doInBackground(Void... args) {
		//while (!bestLocationRetreived)
		if(islocationServiceAvailable){
//			System.out.println("NearbyFavouriteTask  doInBackground");
			while(bestLocationRetrieved == false){
				try {
					Thread.sleep(250);
					if(isCancelled()){
						return null;
					}
				} catch (Exception e) {
				}
			}
			TTDB ttdb = TTDBSingleton.getInstance(pidActivity.getApplicationContext());
			alNearbyFavourites = ttdb.getNearbyFavouriteGivenLocation(location);
		}
		return null;
	}
	
	protected void onPostExecute(Void result) {
		try{
			locationManager.removeUpdates(locationListener);
			//		if(islocationServiceAvailable){
			////			nearbyActivity.update();
			//			if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
			//				GPSLocationUpdateTask task = new GPSLocationUpdateTask();
			//				task.execute((Void)null);
			//			}
			//		}
			//		else{
			//			TramTrackerMainActivity.getAppManager().displayErrorMessage(TramTrackerMainActivity.getAppManager().getResources().getString(R.string.error_locationservicesnotavailable));
			//		}
			//TODO update ui method
//			System.out.println("alNearbyFavourites = " + alNearbyFavourites);
			Stop stop = new Stop();
			if(alNearbyFavourites != null){
				if(alNearbyFavourites.size() > 0){
					stop = alNearbyFavourites.get(0).getFavourite().getStop();
				}
				pidActivity.updateWithNearestFavourite(stop);
			} else {
				pidActivity.updateWithNearestFavourite(null);
			}
		}catch (Exception e) {
//			System.out.println("Exception handled");
			e.printStackTrace();
		}
	}
	
	public class MyLocationListener implements LocationListener {
		Location currentBestLocation;
		private static final int TWO_MINUTES = 1000 * 60 * 2;
		
		@Override
		public void onLocationChanged(Location location) {
			Log.w("onLocationChanged","onLocationChanged");
			if(currentBestLocation == null){
				currentBestLocation = location;
				parent.location = location;
				bestLocationRetrieved = true;
				locationManager.removeUpdates(locationListener);
			}
			else{
				//TODO If network based location, remove updates and then requestLocationUpdates using GPS 
				Log.w("onLocationChanged", "else, location object = " + location);
				if(isBetterLocation(location)){
					currentBestLocation = location;
					parent.location = location;
					bestLocationRetrieved = true;
					locationManager.removeUpdates(locationListener);
				}
			}
		}

		@Override
		public void onProviderDisabled(String provider) {
			if(provider.equalsIgnoreCase(LocationManager.NETWORK_PROVIDER))
				displayErrorMessage("Please enable location functionality in the device and try again");
		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			if(provider.equalsIgnoreCase(LocationManager.NETWORK_PROVIDER)){
				if(status == LocationProvider.OUT_OF_SERVICE){
					displayErrorMessage("tramTracker may be inaccurate because location services are out of service.");
				}
				else if(status == LocationProvider.TEMPORARILY_UNAVAILABLE){
					displayErrorMessage("tramTracker may be inaccurate because location services are temporarily unavailable.");
				}
			}

		}
		
		protected boolean isBetterLocation(Location location) {
		    if (currentBestLocation == null) {
		        // A new location is always better than no location
		        return true;
		    }

		    // Check whether the new location fix is newer or older
		    long timeDelta = location.getTime() - currentBestLocation.getTime();
		    boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
		    boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
		    boolean isNewer = timeDelta > 0;

		    // If it's been more than two minutes since the current location, use the new location
		    // because the user has likely moved
		    if (isSignificantlyNewer) {
		        return true;
		    // If the new location is more than two minutes older, it must be worse
		    } else if (isSignificantlyOlder) {
		        return false;
		    }

		    // Check whether the new location fix is more or less accurate
		    int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
		    boolean isLessAccurate = accuracyDelta > 0;
		    boolean isMoreAccurate = accuracyDelta < 0;
		    boolean isSignificantlyLessAccurate = accuracyDelta > 200;

		    // Check if the old and new location are from the same provider
		    boolean isFromSameProvider = isSameProvider(location.getProvider(),
		            currentBestLocation.getProvider());

		    // Determine location quality using a combination of timeliness and accuracy
		    if (isMoreAccurate) {
		        return true;
		    } else if (isNewer && !isLessAccurate) {
		        return true;
		    } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
		        return true;
		    }
		    return false;
		}

		/** Checks whether two providers are the same */
		private boolean isSameProvider(String provider1, String provider2) {
		    if (provider1 == null) {
		      return provider2 == null;
		    }
		    return provider1.equals(provider2);
		}

	}
	
	class GPSLocationUpdateTask extends AsyncTask<Void, Void,  Boolean>{
		
		Location gpsLocation;
		boolean timeoutOccurred = false;
		Date dateTimeOut;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Define a listener that responds to location updates
			if(locationListener != null){
				locationManager.removeUpdates(locationListener);
				locationManager.removeUpdates(locationListener);
			}
			gpsLocationListener = new GPSLocationListener();
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, Constants.kLocationServicesMinUpdateTime, Constants.kLocationServicesMinUpdateDistance, gpsLocationListener);
			dateTimeOut = new Date();
		}
		
		@Override
		protected Boolean doInBackground(Void... params) {
			long timeOutValue = 15000;
			if(locationManager != null){
//				Log.e("GPSLocationUpdateTask", "locationManager != null");
				if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
//					Log.d("GPSLocationUpdateTask", "GPS location started");
					Date now;
					while(gpsLocation == null && timeoutOccurred == false){
						now = new Date();
//						Log.e("GPSLocationUpdateTask", "timeout seconds = " + (now.getTime() - dateTimeOut.getTime()));
						if(now.getTime() - dateTimeOut.getTime() > timeOutValue){
//							Log.e("GPSLocationUpdateTask", "GPS timed out");
							timeoutOccurred = true;
						}
						try {
							Thread.sleep(200);
							if(isCancelled()){
								return null;
							}
						} catch (Exception e) {
						}
					}
					if(!timeoutOccurred){
//					Log.d("GPSLocationUpdateTask", "GPS location retreived");
//					TTDB tTDB = TTDBSingleton.getInstance(nearbyActivity.getApplicationContext());
					return true;
					}
				}
			}
			return false;
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			try{
				if(result == true){
					Log.d("GPSLocationUpdateTask", "Have a GPS location, ready to update");
					//				nearbyActivity.update();
				}
				if(locationManager != null)
					locationManager.removeUpdates(gpsLocationListener);
				locationManager = null;
			}catch (Exception e) {
//				System.out.println("Exception handled");
				e.printStackTrace();
			}
			super.onPostExecute(result);
		}
		
		public class GPSLocationListener implements LocationListener {
			@Override
			public void onLocationChanged(Location location) {
//				System.out.println("onLocationChanged");
				if(gpsLocation == null){
					gpsLocation = location;
					locationManager.removeUpdates(gpsLocationListener);
				}
			}
		

			@Override
			public void onProviderDisabled(String provider) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onProviderEnabled(String provider) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStatusChanged(String provider, int status, Bundle extras) {
				// TODO Auto-generated method stub

			}
		}	
	}

}
