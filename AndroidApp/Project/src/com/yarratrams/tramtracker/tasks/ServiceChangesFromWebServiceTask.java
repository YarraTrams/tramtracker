package com.yarratrams.tramtracker.tasks;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.Constants;
import com.yarratrams.tramtracker.objects.ServiceChangeRSSItem;
import com.yarratrams.tramtracker.ui.ServiceFeedActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;

import android.os.AsyncTask;

import com.yarratrams.tramtracker.webserviceinteraction.ServiceChangeRSS;

public class ServiceChangesFromWebServiceTask extends AsyncTask<Void, Void, ArrayList<ServiceChangeRSSItem>> {
	static final int kErrorClientProtocol = 4;
	static final int kErrorIO = 3; 
	static final int kErrorReturnedByServer = 2; 
	static final int kErrorParsing = 1; 
	static final int kErrorNoError = 0; 
	ServiceFeedActivity activity;
	int errorType;
	public ServiceChangesFromWebServiceTask(ServiceFeedActivity activity) {
		this.activity = activity;
	}
	
	protected ArrayList<ServiceChangeRSSItem> doInBackground(Void... params) {
		HttpClient httpclient = new DefaultHttpClient();  
		HttpPost httppost = new HttpPost(Constants.kURLServiceChangeRSS);  
		try {  
			// Execute HTTP Post Request  
			HttpResponse response = httpclient.execute(httppost);  
			errorType = kErrorNoError;
			int status = response.getStatusLine().getStatusCode();

			if (status != HttpStatus.SC_OK) {
				ByteArrayOutputStream ostream = new ByteArrayOutputStream();
				response.getEntity().writeTo(ostream);
//				System.out.println("HTTP CLIENT " + ostream.toString());
				errorType = kErrorReturnedByServer;
			} else {
				//				ByteArrayOutputStream ostream = new ByteArrayOutputStream();
				//				response.getEntity().writeTo(ostream);
				//				byte [] responseBody = ostream.toByteArray();
				//				System.out.println("Content " + (new String(responseBody)));
				try {
					/** Handling XML */
					SAXParserFactory spf = SAXParserFactory.newInstance();
					SAXParser sp = spf.newSAXParser();
					XMLReader xr = sp.getXMLReader();
					/** Send URL to parse XML Tags */
					ServiceChangeRSS myXMLHandler = new ServiceChangeRSS();
					xr.setContentHandler(myXMLHandler);
					xr.parse(new InputSource(response.getEntity().getContent()));
					ArrayList<ServiceChangeRSSItem> alServiceChangeRSS = myXMLHandler.getArrayListNews();
					return alServiceChangeRSS;
				} catch (Exception e) {
					errorType = kErrorParsing;
//					System.out.println("XML parsing exception = " + e);
					e.printStackTrace();
				}

			}
		} catch (ClientProtocolException e) {  
			e.printStackTrace();  
		} catch (IOException e) {  
			errorType = kErrorIO;
			e.printStackTrace();  
		} 
		return null;
	}
	
	@Override
	protected void onPostExecute(ArrayList<ServiceChangeRSSItem> result) {
		super.onPostExecute(result);
		TramTrackerMainActivity ttMain = TramTrackerMainActivity.getAppManager();
		switch (errorType) {
			case kErrorParsing :
				ttMain.displayErrorMessage(ttMain.getResources().getString(R.string.error_service_changesRSS_parsing));
				break;
			case kErrorReturnedByServer :
				ttMain.displayErrorMessage(ttMain.getResources().getString(R.string.error_service_changesRSS_parsing));
				break;
			case kErrorIO :
				ttMain.displayErrorMessage(ttMain.getResources().getString(R.string.error_service_changesRSS_parsing));
				break;
			case kErrorClientProtocol :
				ttMain.displayErrorMessage(ttMain.getResources().getString(R.string.error_service_changesRSS_client_protocol));
				break;
		}
//		System.out.println("result = " + result);
		activity.updateUI(result);
		
	}

}
