/**
 * 
 */
package com.yarratrams.tramtracker.tasks;

import java.util.ArrayList;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.db.TTDB;
import com.yarratrams.tramtracker.objects.PredictedArrivalTime;
import com.yarratrams.tramtracker.objects.PredictedTimeResult;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.objects.Tram;
import com.yarratrams.tramtracker.singleton.PredictedTimeResultSingleton;
import com.yarratrams.tramtracker.singleton.TTDBSingleton;
import com.yarratrams.tramtracker.ui.MyTramActivity;
import com.yarratrams.tramtracker.ui.OnBoardActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;
import com.yarratrams.tramtracker.webserviceinteraction.TTWebService;
import com.yarratrams.tramtracker.webserviceinteraction.TTWebServiceSingleton;

import android.os.AsyncTask;
import android.util.Log;

/**
 * @author appscore1
 *
 */
public class MyTramTask extends AsyncTask<Void, Void, PredictedTimeResult> {
	
	TTWebService webService;
	OnBoardActivity onboardActivity;
	MyTramActivity myTramActivity;
	TTDB ttdb;
	ArrayList<PredictedArrivalTime> predictedArrivalTimes = new ArrayList<PredictedArrivalTime>();
	Tram tram;
	TramTrackerMainActivity tramTrackerMainActivity;
	boolean routeHasStops;
	
//	public MyTramTask(MyTramActivity myTramActivity, Tram tram, String routeNo, boolean isUpDirection){
//		this.myTramActivity = myTramActivity;
//		this.tram = tram;
//		this.routeNo = routeNo;
//		this.isUpDirection = isUpDirection;
//	}
	
	public ArrayList<PredictedArrivalTime> getPredictedArrivalTimes() {
		return predictedArrivalTimes;
	}

	public MyTramTask(OnBoardActivity myTramActivity, Tram tram){
		this.onboardActivity = myTramActivity;
		this.tram = tram;
	}

	public MyTramTask(MyTramActivity myTramActivity, Tram tram){
		this.myTramActivity = myTramActivity;
		this.tram = tram;
	}
	
	
	public MyTramTask(TramTrackerMainActivity tramTrackerMainActivity, Tram tram){
		this.tramTrackerMainActivity = tramTrackerMainActivity;
		this.tram = tram;
	}

	protected PredictedTimeResult doInBackground(Void... arg0) {
		webService = TTWebServiceSingleton.getInstance();
		if(onboardActivity != null)
			ttdb = TTDBSingleton.getInstance(onboardActivity);
		else if(myTramActivity != null)
			ttdb = TTDBSingleton.getInstance(myTramActivity);
		else if(tramTrackerMainActivity != null)
			ttdb = TTDBSingleton.getInstance(tramTrackerMainActivity);
		short i = 0;
		Stop stopFromDb;
		PredictedTimeResult predictedTimeResult = webService.getNextPredictedArrivalTimeAtStopsForTram(tram);
		routeHasStops = true;
		ArrayList<PredictedArrivalTime> predictedArrivalTimes = null;
		if(predictedTimeResult != null && predictedTimeResult.getAlPredictedArrivalTime() != null
				&& predictedTimeResult.getAlPredictedArrivalTime().size() > 0){
			ttdb.populateRoute(predictedTimeResult.getRoute());
			predictedArrivalTimes = predictedTimeResult.getAlPredictedArrivalTime();
			ArrayList<Stop> alStops =  ttdb.getStopsForRoute(predictedTimeResult.getRoute().getRouteNumber(), predictedTimeResult.isUpDirection());
			if(alStops == null || alStops.size() == 0){
				//the tram is not on a public network. Populate the stops returned by the webservice, set the starting index to 0
				Stop predictedStop;
				for(int j=0; j < predictedArrivalTimes.size(); j++){
					predictedStop = predictedArrivalTimes.get(j).getStop();
					ttdb.populateStop(predictedStop);
					predictedStop.setRoutes(ttdb.getRoutesArrayOfStringsForStop(predictedStop));
				}
				ttdb.populateRoute(predictedTimeResult.getRoute());
				System.out.println("predictedTimeResult.getRoute() = " + predictedTimeResult.getRoute());
				predictedTimeResult.setStartingIndex((short) 0);
//				System.out.println("predictedTimeResult = " + predictedTimeResult.getAlPredictedArrivalTime());
			}
			else{
				if(predictedArrivalTimes != null && predictedArrivalTimes.size() > 0){
					Stop predictedStop = predictedArrivalTimes.get(0).getStop();
					for(i=0; i< alStops.size(); i++){
						stopFromDb = alStops.get(i);
						//if predicted stop is the same as stop from db, mark as starting index
						if(stopFromDb.getTrackerID() == predictedStop.getTrackerID()){
							predictedTimeResult.setStartingIndex(i);
							break;
						}
						else{
							if(stopFromDb.getTrackerID() > 0)
								predictedArrivalTimes.add(i,new PredictedArrivalTime(null, stopFromDb));
							stopFromDb.setRoutes(ttdb.getRoutesArrayOfStringsForStop(stopFromDb));
						}
					}
					for(int j=i; j < predictedArrivalTimes.size(); j++){
						predictedStop = predictedArrivalTimes.get(j).getStop();
						ttdb.populateStop(predictedStop);
						predictedStop.setRoutes(ttdb.getRoutesArrayOfStringsForStop(predictedStop));
					}
					//				System.out.println("predictedArrivalTimes = " + predictedArrivalTimes);
				}
				else{
					for(i=0; i< alStops.size(); i++){
						stopFromDb = alStops.get(i);
						if(stopFromDb.getTrackerID() > 0)
							predictedArrivalTimes.add(i,new PredictedArrivalTime(null, stopFromDb));
						stopFromDb.setRoutes(ttdb.getRoutesArrayOfStringsForStop(stopFromDb));
					}
				}
			}
		}
		else{
			return null;
		}
		if(predictedTimeResult != null && predictedTimeResult.getAlPredictedArrivalTime() != null && predictedTimeResult.getAlPredictedArrivalTime().size() > 0){
			PredictedTimeResultSingleton.setResult(predictedTimeResult);
//			System.out.println("startingindex = " + predictedTimeResult.getStartingIndex());
		}
		if(predictedTimeResult != null){
			if(predictedTimeResult.getAlPredictedArrivalTime() != null){
				for(PredictedArrivalTime pat : predictedTimeResult.getAlPredictedArrivalTime()){
					if(pat.getStop().getTrackerID() == 0){
//						Log.e("PredictedTimeResult doInBackground","Stop Number 0 found");
					}
				}
			}
		}
//		System.out.println("predictedTimeResult = " + predictedTimeResult);
		return predictedTimeResult;
	}
	
	@Override
	protected void onPostExecute(PredictedTimeResult result) {
		super.onPostExecute(result);
		try{
			//		System.out.println("PredictedTimeResult = " + result);
			if(PredictedTimeResultSingleton.errorType == PredictedTimeResultSingleton.kErrorNoConnection){
				TramTrackerMainActivity ttMain = TramTrackerMainActivity.getAppManager();
				ttMain.displayErrorMessage(ttMain.getResources().getString(R.string.error_internet_unavailable));
			}
			else if(PredictedTimeResultSingleton.errorType == PredictedTimeResultSingleton.kErrorTimeOut){
				TramTrackerMainActivity ttMain = TramTrackerMainActivity.getAppManager();
				ttMain.displayErrorMessage(ttMain.getResources().getString(R.string.error_internet_timeout));
			}
			
			if(!routeHasStops){
				TramTrackerMainActivity ttMain = TramTrackerMainActivity.getAppManager();
				ttMain.displayErrorMessage(ttMain.getResources().getString(R.string.error_service_no_stops_found_for_route));
			}
			if(onboardActivity != null)
				onboardActivity.updateUI(result);
			else if(myTramActivity != null){
				myTramActivity.updateUI(result);
			}
			else if(tramTrackerMainActivity != null){
				//Test
			}
		}catch (Exception e) {
//			System.out.println("Exception handled");
			e.printStackTrace();
		}
	}

}
