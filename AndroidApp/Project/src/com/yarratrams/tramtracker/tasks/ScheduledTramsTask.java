/**
 * 
 */
package com.yarratrams.tramtracker.tasks;

import java.util.ArrayList;
import java.util.Calendar;

import com.yarratrams.tramtracker.objects.Tram;
import com.yarratrams.tramtracker.singleton.TTDBSingleton;
import com.yarratrams.tramtracker.ui.TimetableActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;
import com.yarratrams.tramtracker.webserviceinteraction.TTWebService;
import com.yarratrams.tramtracker.webserviceinteraction.TTWebServiceSingleton;

import android.os.AsyncTask;

/**
 * @author appscore1
 *
 */
public class ScheduledTramsTask extends AsyncTask<Void, Void, Void> {
	TTWebService webService;
	String stopTrackerID;
	String routeNo;
	boolean lowFloor;
	Calendar date;
//	TramTrackerMainActivity activity;
	TimetableActivity activity;
	ArrayList<Tram> allTrams;
	
	//Use this to access all trams
	public ArrayList<Tram> getAllTrams() {
		return allTrams;
	}

//	public ScheduledTramsTask(TramTrackerMainActivity activity, String stopTrackerID, String routeNo, Calendar date, boolean lowFloor) {
//		this.activity = activity;
//		this.stopTrackerID = stopTrackerID;
//		this.routeNo = routeNo;
//		this.lowFloor = lowFloor;
//		this.date = date;
//	}
	
	public ScheduledTramsTask(TimetableActivity activity, String stopTrackerID, String routeNo, Calendar date, boolean lowFloor) {
		this.activity = activity;
		this.stopTrackerID = stopTrackerID;
		this.routeNo = routeNo;
		this.lowFloor = lowFloor;
		this.date = date;
	}
	
	protected Void doInBackground(Void... arg0) {
		webService = TTWebServiceSingleton.getInstance();
		String intRouteNo =  TTDBSingleton.getInstance(TramTrackerMainActivity.getAppManager()).getInternalRouteNoForRouteNo(routeNo);
//		System.out.println("intRouteNo = " + intRouteNo);
		allTrams = webService.getSchedulesForStopandRoute(stopTrackerID, intRouteNo, date, lowFloor);
//		System.out.println("next 3 tram schedules = " + allTrams);
		return null;
	}
	
	@Override
	protected void onPostExecute(Void result) {
		try{
			activity.updateUI();
			super.onPostExecute(result);
		}catch (Exception e) {
//			System.out.println("Exception handled");
			e.printStackTrace();
		}
	}

}
