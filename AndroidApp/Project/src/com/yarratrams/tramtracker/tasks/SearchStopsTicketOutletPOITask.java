/**
 * This class runs asynchronously the task of searching for stops, ticket outlets and points of interest using the class TTDB.
 */
package com.yarratrams.tramtracker.tasks;

import com.yarratrams.tramtracker.db.TTDB;
import com.yarratrams.tramtracker.objects.SearchResult;
import com.yarratrams.tramtracker.singleton.TTDBSingleton;
import com.yarratrams.tramtracker.ui.SearchMainActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;

import android.content.Context;
import android.os.AsyncTask;

/***
 * Use constructor arguments to provide search arguments.
 * @author appscore1
 *
 */
public class SearchStopsTicketOutletPOITask extends AsyncTask<Void, Void, SearchResult>{
	 SearchMainActivity activity;
	 private String keyword;
	 private boolean tramStops;
	 private boolean shelterStops;
	 private boolean easyAccessStops;
	 private boolean ticketOutlets;
	 private boolean poi;
	 private Context context = null;
	
	public SearchStopsTicketOutletPOITask(SearchMainActivity searchMainActivity, String keyword,
			boolean tramStops, boolean shelterStops, boolean easyAccessStops,
			boolean ticketOutlets, boolean poi) {

		this.activity = searchMainActivity;
		this.keyword = keyword;
		this.tramStops = tramStops;
		this.shelterStops = shelterStops;
		this.easyAccessStops = easyAccessStops;
		this.ticketOutlets = ticketOutlets;
		this.poi = poi;
	}
	
	public SearchStopsTicketOutletPOITask(TramTrackerMainActivity ttMainActivity, String keyword,
			boolean tramStops, boolean shelterStops, boolean easyAccessStops,
			boolean ticketOutlets, boolean poi) {

		this.context = ttMainActivity;
		this.keyword = keyword;
		this.tramStops = tramStops;
		this.shelterStops = shelterStops;
		this.easyAccessStops = easyAccessStops;
		this.ticketOutlets = ticketOutlets;
		this.poi = poi;
	}
	
	protected SearchResult doInBackground(Void... params) {
		TTDB ttdb;
		if(context != null)
			ttdb = TTDBSingleton.getInstance(context.getApplicationContext());
		else
			ttdb = TTDBSingleton.getInstance(activity.getApplicationContext());
		keyword = keyword.trim();
		SearchResult searchResult = ttdb.getSearchResultsGivenKeyword(keyword, tramStops, shelterStops, easyAccessStops, ticketOutlets, poi);

		if(searchResult == null){
			return searchResult;
		}
		
		searchResult.setKeyword(keyword);
		searchResult.setSearchStop(tramStops);
		searchResult.setSearchShelter(shelterStops);
		searchResult.setSearchAccessible(easyAccessStops);
		searchResult.setSearchOutlet(ticketOutlets);
		searchResult.setSearchPOI(poi);
		
		return searchResult;
	}
	
	protected void onPostExecute(SearchResult result) {
		super.onPostExecute(result);
		try{
			if(context == null)
				activity.updateUI(result);
//			else
//				System.out.println("SearchStopsTicketOutletPOITask result = " + result);
		}catch (Exception e) {
//			System.out.println("Exception handled");
			e.printStackTrace();
		}
	}
}
