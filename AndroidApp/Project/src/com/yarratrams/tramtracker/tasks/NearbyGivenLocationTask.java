package com.yarratrams.tramtracker.tasks;

import java.util.ArrayList;

import com.yarratrams.tramtracker.db.TTDB;
import com.yarratrams.tramtracker.objects.NearbyStop;
import com.yarratrams.tramtracker.objects.NearbyStopsLimitedByNoOfStopsGivenLocation;
import com.yarratrams.tramtracker.objects.NearbyTicketOutlet;
import com.yarratrams.tramtracker.singleton.TTDBSingleton;
import com.yarratrams.tramtracker.ui.NearbyActivity;

import android.location.Location;
import android.os.AsyncTask;

public class NearbyGivenLocationTask extends AsyncTask<Void, Void, Void> {
	private Location mapCentreLocation, lastKnownLocation;
	private ArrayList<NearbyStop> nearbyStops;
	private ArrayList<NearbyStop> shelterStops;
	private ArrayList<NearbyStop> easyStops;
	private ArrayList<NearbyTicketOutlet> nearbyOutlets;
	NearbyActivity nearbyActivity;
	
	public NearbyGivenLocationTask(NearbyActivity nearbyActivity, Location loc, Location lastKnownLocation) {
		this.nearbyActivity = nearbyActivity;
		this.lastKnownLocation = lastKnownLocation;
		mapCentreLocation = loc;
	}
	
	public ArrayList<NearbyStop> getNearbyStops() {
		return nearbyStops;
	}
	
	public ArrayList<NearbyStop> getShelterStops() {
		return shelterStops;
	}
	
	public ArrayList<NearbyStop> getEasyStops() {
		return easyStops;
	}
	
	public ArrayList<NearbyTicketOutlet> getNearbyOutlets() {
		return nearbyOutlets;
	}
	
	protected Void doInBackground(Void... args) {
		if(lastKnownLocation != null){
			TTDB ttdb = TTDBSingleton.getInstance(nearbyActivity.getApplicationContext());
			NearbyStopsLimitedByNoOfStopsGivenLocation n = ttdb.getNearbyStopsLimitedByNoOfStopsGivenLocation(mapCentreLocation, lastKnownLocation);
			nearbyStops = n.getNearbyStops();
			shelterStops = n.getShelterStops();
			easyStops = n.getEasyAccessStops();

//			nearbyStops = ttdb.getNearbyStopsLimitedByNoOfStopsGivenLocation(mapCentreLocation, lastKnownLocation);
//			shelterStops = ttdb.getNearbyStopsWithShelterLimitedByNoOfStopsGivenLocation(mapCentreLocation, lastKnownLocation);
//			easyStops = ttdb.getNearbyStopsWithEasyAccessLimitedByNoOfStopsGivenLocation(mapCentreLocation, lastKnownLocation);
			nearbyOutlets = ttdb.getNearbyTicketOutletsLimitedByNoOfOutletsGivenLocation(mapCentreLocation, lastKnownLocation);
		}
		return null;
	}
	
	protected void onPostExecute(Void result) {
		try{
			nearbyActivity.update();
		}catch (Exception e) {
//			System.out.println("Exception handled");
			e.printStackTrace();
		}
	}
	
}
