package com.yarratrams.tramtracker.tasks;

import java.util.ArrayList;
import com.yarratrams.tramtracker.db.TTDB;
import com.yarratrams.tramtracker.objects.Route;
import com.yarratrams.tramtracker.objects.ServiceInfo;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.singleton.TTDBSingleton;
import com.yarratrams.tramtracker.ui.PIDActivity;
import com.yarratrams.tramtracker.webserviceinteraction.TTWebService;
import com.yarratrams.tramtracker.webserviceinteraction.TTWebServiceSingleton;

import android.os.AsyncTask;

public class PIDTask extends AsyncTask<Stop, Void, Void> {
	private TTDB ttdb;
	private TTWebService webService;
	private ServiceInfo info;
	private Stop stop;
	private PIDActivity pidActivity;
	private boolean lowFloor;
	
	public ServiceInfo getInfo() {
		return info;
	}

	public void setPidActivity(PIDActivity pidActivity) {
		this.pidActivity = pidActivity;
	}
	
	public PIDTask(PIDActivity pidActivity, boolean lowFloor) {
		this.pidActivity = pidActivity;
		this.lowFloor = lowFloor;
	}

	protected Void doInBackground(Stop... params) {
//		System.out.println("PIDTask");
		stop = params[0];
		webService = TTWebServiceSingleton.getInstance();
		if(ttdb == null)
			ttdb = TTDBSingleton.getInstance(pidActivity.getApplicationContext());
		info = webService.getNextPredictedRoutesCollectionRouteZero(stop, lowFloor);
		if(info != null){
			ArrayList<Route> listRoutes = info.getRoutes();
			if(listRoutes != null){
				for(Route route : listRoutes){
					ttdb.populateRoute(route);
				}
			}
		}
//		System.out.println("PIDTask info = " + info);
		return null;
	}
	
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		try{
//			System.out.println("pidActivity.update");
			pidActivity.update();
		}catch (Exception e) {
//			System.out.println("Exception handled");
			e.printStackTrace();
		}
	}

}
