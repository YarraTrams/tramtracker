package com.yarratrams.tramtracker.tasks;

import java.util.ArrayList;
import com.yarratrams.tramtracker.db.TTDB;
import com.yarratrams.tramtracker.objects.Route;
import com.yarratrams.tramtracker.objects.ServiceInfo;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.singleton.TTDBSingleton;
import com.yarratrams.tramtracker.ui.PIDActivity;
import com.yarratrams.tramtracker.webserviceinteraction.TTWebService;
import com.yarratrams.tramtracker.webserviceinteraction.TTWebServiceSingleton;

import android.os.AsyncTask;

public class NextTramsForRouteTask extends AsyncTask<Void, Void, Void> {
	private TTDB ttdb;
	private TTWebService webService;
	private Stop stop;
	private Route route;
	private PIDActivity pidActivity;
	private ServiceInfo info;
	private boolean lowFloor;
	
	public void setPidActivity(PIDActivity pidActivity) {
		this.pidActivity = pidActivity;
	}
	
	public ServiceInfo getInfo() {
		return info;
	}

	public NextTramsForRouteTask(PIDActivity pidActivity, Stop stop, Route route, boolean lowFloor) {
		this.pidActivity = pidActivity;
		this.stop = stop;
		this.route = route;
		this.lowFloor = lowFloor;
	}

	protected Void doInBackground(Void... params) {
//		System.out.println("NextTramsForRouteTask");
		webService = TTWebServiceSingleton.getInstance();
		if(ttdb == null)
			ttdb = TTDBSingleton.getInstance(pidActivity.getApplicationContext());
		ArrayList<Route> listRoutes = new ArrayList<Route>();
//		System.out.println("Route no  = " + route.getRouteNumber());
//		System.out.println("Internal route no = " + ttdb.getInternalRouteNoForRouteNo(route.getRouteNumber()));
		if(route.getMainRouteNo() == null)
			route.setMainRouteNo(ttdb.getInternalRouteNoForRouteNo(route.getRouteNumber()));
		listRoutes.add(route);
		info = webService.getNextPredictedRoutesCollection(stop, listRoutes , lowFloor);
		if(listRoutes != null && info != null){
			for(Route route : listRoutes){
				ttdb.populateRoute(route);
			}
		}
//		System.out.println("NextTramsForRouteTask info = " + info);
		return null;
	}
	
	@Override
	protected void onPostExecute(Void result) {
		try{
			super.onPostExecute(result);
			pidActivity.update();
		}catch (Exception e) {
			System.out.println("Exception handled");
			e.printStackTrace();
		}
	}

}
