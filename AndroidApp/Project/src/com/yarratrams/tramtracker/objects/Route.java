package com.yarratrams.tramtracker.objects;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

public class Route implements Parcelable{
	private String routeNumber = null;
	private String description = null; 
	private boolean isUpDestination;		// Direction of the route [up - down], set from web service
	private String upDestination = null;
	private String downDestination = null;
	private String variantDestination = null;	//Info obtained from web service
	private ArrayList<Tram> nextServices;	//Obtained from web service
	private String colour;
	private boolean isMainRoute;
	private String mainRouteNo;
	
	public Route() {
		nextServices = new ArrayList<Tram>();
	}
	
	private Route(Parcel in) {
		this();
		readFromParcel(in);
	}
	

	public String getRouteNumber() {
		return this.routeNumber;
	}

	public void setRouteNumber(String routeNumber) {
		this.routeNumber = routeNumber;
	}
	
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isUpDestination() {
		return this.isUpDestination;
	}

	public void setIsUpDestination(boolean isUpDestination) {
		this.isUpDestination = isUpDestination;
	}

	public String getUpDestination() {
		return this.upDestination;
	}

	public void setUpDestination(String upDestination) {
		this.upDestination = upDestination;
	}

	public String getDownDestination() {
		return this.downDestination;
	}

	public void setDownDestination(String downDestination) {
		this.downDestination = downDestination;
	}

	public String getVariantDestination() {
		return this.variantDestination;
	}

	public void setVariantDestination(String variantDestination) {
		this.variantDestination = variantDestination;
	}

	
	public ArrayList<Tram> getNextServices() {
		return this.nextServices;
	}

	public void setNextServices(ArrayList<Tram> nextServices) {
		this.nextServices = nextServices;
	}
	
	public String getDestination(){
		if(isUpDestination){
//			System.out.println("returned upDestination = " + upDestination);
			return upDestination;
		} else {
//			System.out.println("returned downDestination = " + downDestination);
			return downDestination;
		}
	}
	
	public boolean isMainRoute() {
		return isMainRoute;
	}

	public void setMainRoute(boolean isMainRoute) {
		this.isMainRoute = isMainRoute;
	}

	/**
	 * Main route no = Internal route number
	 * @return
	 */
	public String getMainRouteNo() {
		return mainRouteNo;
	}

	public void setMainRouteNo(String mainRouteNo) {
		this.mainRouteNo = mainRouteNo;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(routeNumber);
		out.writeString(description);
		out.writeByte((byte)(isUpDestination == true?1:0));
		out.writeString(upDestination);
		out.writeString(downDestination);
		out.writeString(variantDestination);
		out.writeTypedList(nextServices);
		out.writeString(colour);
		out.writeByte((byte)(isMainRoute == true?1:0));
		out.writeString(mainRouteNo);
		
	}
	
	public void readFromParcel(Parcel in) {
		routeNumber = in.readString();
		description = in.readString();
		isUpDestination = in.readByte()==1?true:false;
		upDestination = in.readString();
		downDestination = in.readString();
		variantDestination = in.readString();
		in.readTypedList(nextServices, Tram.CREATOR);
		colour = in.readString();
		isMainRoute = in.readByte()==1?true:false;
		mainRouteNo = in.readString();
	}
	
	public static final Parcelable.Creator<Route> CREATOR = new Parcelable.Creator<Route>() {
		public Route createFromParcel(Parcel in) {
			return new Route(in);
		}

		public Route[] newArray(int size) {
			return new Route[size];
		}
		
	};
	
	public String toString() {
//		return "route no = " + routeNumber + ", description = " + description + ", next services = " + nextServices;
		return "route no = " + routeNumber + ", nextServices = " + nextServices + ", destination = " + upDestination + " & " + downDestination  + "mainRouteNo = " + mainRouteNo;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	};
	
	
}
