package com.yarratrams.tramtracker.objects;

public class Constants {
	public static int kNearestDistance = 500;
	public static final String kDBNameNew = "tramtrackernew.sqlite";
	public static final String kDBName = "tramTRACKER.sqlite";
	public static final String kDBLocalPath = "/data/data/com.yarratrams.tramtracker/databases/";//The Android's default system path of your application database.
	public static String strURLBase = "http://extranetdev.yarratrams.com.au/PIDSServiceWCF/RestService/";
	//public static String strURLBase = "http://ws2.tramtracker.com.au/TramTracker/RestService/"; //Production URL
	public static String kURLServiceChangeRSS = "http://www.yarratrams.com.au/servicechanges.rss";
	public static String strAID = "/?aid=4997245C-6D85-11E1-8E28-84224924019B";
	public static String strCID = "&cid=2";
	public static String token = "&tkn=3e982c6b-d57a-43fe-bff7-a44e9adb9e1e";
	public static short kThresholdSecondsCity = 60;
	public static short kThresholdSecondsOther = 120;
	//public static int kLocationServicesMinUpdateTime = 1000; //milliseconds to wait before next update
	public static int kLocationServicesMinUpdateTime = 0; //milliseconds to wait before next update
	public static int kLocationServicesMinUpdateDistance = 0; //the minimum distance interval for notifications, in meters
	public static String kDefaultGroupName = "General";
	public static int kWebServerUpdateTime = 60000;
	public static String kErrorGenericDisruptionMultipleSuffix = " are currently disrupted. Delays may occur.";
	public static String kErrorGenericDisruptionSingleSuffix = " is currently disrupted. Delays may occur.";
	public static String kErrorGenericDisruptionMultiplePrefix = "Routes ";
	public static String kErrorGenericDisruptionSinglePrefix = "Route ";
	public static String kErrorTTNotAvailable = "Real time information is not available due to disruption or service change.";
	public static String kErrorTTNotAvailableForTram = "tramTRACKER is unavailable for this tram.";
	public static String kErrorInvalidTram = "Tram could not be found on the network. If inside a tram, tram number is located on the top corners near the exits ,or if outside, it is located at the front of the tram";
	public static String kErrorHasResponseFalse = "tramTRACKER had a problem fetching information.";
	public static String kErrorHasResponseFalseTimeTablePrefix = "tramTRACKER does not have information for route number ";
	public static String kErrorHasResponseFalseTimeTableSuffix = " at the moment";
	public static String kErrorHasResponseFalseLowTram = "tramTRACKER could not fetch information. There may be no low floor trams on the selected route.";
	
	public static String kSharedPrefIdentifier = "com.yarratrams.tramtracker";
	public static String kLastUpdateDate = "com.yarratrams.tramtracker.lastupdatedate";
	public static String kKeyDeviceInfo = "com.yarratrams.tramtracker.deviceinfo";
	
	public static int kLastUpdateDayOfMonth = 12;
	public static int kLastUpdateMonth = 8;
	public static int kLastUpdateYear = 2012;
	
	public static int kNoOfStops = 20;
	
}
