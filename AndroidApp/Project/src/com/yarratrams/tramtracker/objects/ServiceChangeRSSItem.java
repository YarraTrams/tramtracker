package com.yarratrams.tramtracker.objects;

import android.os.Parcel;
import android.os.Parcelable;

public class ServiceChangeRSSItem implements Parcelable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String content;
	String date;
	String title;
	String description;
	
	public ServiceChangeRSSItem() {
	}	
	
	public ServiceChangeRSSItem(Parcel in) {
		this();
		readFromParcel(in);
	}
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public String toString() {
		return "Title = " + title + ", Date = " + date + ", Content = " + content;
	}
	
	
	@Override
	public int describeContents() {
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(content);
		out.writeString(date);
		out.writeString(title);
		out.writeString(description);
	}
	
	public void readFromParcel(Parcel in) {
		content = in.readString();
		date = in.readString();
		title = in.readString();
		description = in.readString();
	}
	
	
	public static final Parcelable.Creator<ServiceChangeRSSItem> CREATOR = new Parcelable.Creator<ServiceChangeRSSItem>() {
		public ServiceChangeRSSItem createFromParcel(Parcel in) {
			return new ServiceChangeRSSItem(in);
		}

		public ServiceChangeRSSItem[] newArray(int size) {
			return new ServiceChangeRSSItem[size];
		}
		
	};
}
