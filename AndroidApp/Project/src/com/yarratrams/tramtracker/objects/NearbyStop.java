package com.yarratrams.tramtracker.objects;

import android.os.Parcel;
import android.os.Parcelable;

public class NearbyStop implements Parcelable {
	private Stop stop;
	private int intDistance;
	
	public Stop getStop() {
		return stop;
	}

	public void setStop(Stop stop) {
		this.stop = stop;
	}

	public int getIntDistance() {
		return intDistance;
	}

	public void setIntDistance(int intDistance) {
		this.intDistance = intDistance;
	}
	
	public NearbyStop(){
	}
	
	public NearbyStop(Stop stop, int intDistance){
		this.stop = stop;
		this.intDistance = intDistance;
	}
	
	private NearbyStop(Parcel in) {
		readFromParcel(in);
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeParcelable(stop, 0);
		dest.writeInt(intDistance);
	}
	
	public void readFromParcel(Parcel in) {
		stop = (Stop)in.readParcelable(NearbyStop.class.getClassLoader());
		intDistance = in.readInt();
	}
	
	public static final Parcelable.Creator<NearbyStop> CREATOR = new Parcelable.Creator<NearbyStop>() {
		public NearbyStop createFromParcel(Parcel in) {
			return new NearbyStop(in);
		}

		public NearbyStop[] newArray(int size) {
			return new NearbyStop[size];
		}
	};
	
	public String toString() {
		return "Stop name = " + stop.getStopName() + ", distance = " + intDistance;
	};

}
