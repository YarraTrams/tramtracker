package com.yarratrams.tramtracker.objects;

import java.util.ArrayList;

public class RouteStopsBySuburb {
	String suburb;
	ArrayList<Stop> stops;
	
	public RouteStopsBySuburb() {
		this.suburb = "";
		this.stops = new ArrayList<Stop>();
	}
	
	public String getSuburb() {
		return suburb;
	}
	
	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}
	
	public ArrayList<Stop> getStops() {
		return stops;
	}
	
	public void setStops(ArrayList<Stop> stops) {
		this.stops = stops;
	}
	
	public void addStop(Stop stop){
		stops.add(stop);
	}
	
	public String toString() {
		return "Suburb = " + suburb + ", Stop = " + stops;
	}
	
}
