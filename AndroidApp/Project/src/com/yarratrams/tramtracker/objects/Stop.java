package com.yarratrams.tramtracker.objects;

import android.os.Parcel;
import android.os.Parcelable;

public class Stop implements Parcelable{
	private int trackerID;
	private String stopNumber;
	
	private String stopName;
	private String cityDirection;
	private String stopDescription; 
	private String suburb;
	
	private String[] routes; 
	
	private String zone; 
	private boolean isPlatformStop;		
	private boolean isEasyAccessStop; 
	private boolean hasShelter; 
	private boolean isCityStop;
	private boolean isFavouriteStop;
	private String turnType; 
	
	private String connectingTrains;
	private String connectingTrams; 
	private String connectingBuses; 
	private String pointsOfInterest; 
	
	private int latitudeE6;
	private int longitudeE6;
	private int length;
	
	public Stop(){
		routes = new String[1];
	}

	public Stop(int numberRoutes){
		routes = new String[numberRoutes];
	}
	
	private Stop(Parcel in) {
		this();
		readFromParcel(in);
	}
	
	public boolean isFavouriteStop() {
		return isFavouriteStop;
	}

	public void setFavouriteStop(boolean isFavouriteStop) {
		this.isFavouriteStop = isFavouriteStop;
	}

	public int getTrackerID() {
		return this.trackerID;
	}

	public void setTrackerID(int trackerID) {
		this.trackerID = trackerID;
	}

	public String getStopNumber() {
		return this.stopNumber;
	}

	public void setStopNumber(String stopNumber) {
		this.stopNumber = stopNumber;
	}

	public String getStopName() {
		return this.stopName;
	}

	public void setStopName(String stopName) {
		this.stopName = stopName;
	}

	public String getCityDirection() {
		return this.cityDirection;
	}

	public void setCityDirection(String cityDirection) {
		this.cityDirection = cityDirection;
	}

	public String getStopDescription() {
		return this.stopDescription;
	}

	public void setStopDescription(String stopDescription) {
		this.stopDescription = stopDescription;
	}

	public String getSuburb() {
		return this.suburb;
	}

	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}

	public String[] getRoutes() {
		return this.routes;
	}

	public void setRoutes(String[] routes) {
		this.routes = routes;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public boolean isPlatformStop() {
		return this.isPlatformStop;
	}

	public void setPlatformStop(boolean isPlatformStop) {
		this.isPlatformStop = isPlatformStop;
	}

	public boolean isEasyAccessStop() {
		return this.isEasyAccessStop;
	}

	public void setEasyAccessStop(boolean isEasyAccessStop) {
		this.isEasyAccessStop = isEasyAccessStop;
	}

	public boolean isHasShelter() {
		return this.hasShelter;
	}

	public void setHasShelter(boolean hasShelter) {
		this.hasShelter = hasShelter;
	}

	public boolean isCityStop() {
		return this.isCityStop;
	}

	public void setCityStop(boolean isCityStop) {
		this.isCityStop = isCityStop;
	}

	public String getTurnType() {
		return this.turnType;
	}

	public void setTurnType(String turnType) {
		this.turnType = turnType;
	}

	public String getConnectingTrains() {
		return this.connectingTrains;
	}

	public void setConnectingTrains(String connectingTrains) {
		this.connectingTrains = connectingTrains;
	}

	public String getConnectingTrams() {
		return this.connectingTrams;
	}

	public void setConnectingTrams(String connectingTrams) {
		this.connectingTrams = connectingTrams;
	}

	public String getConnectingBuses() {
		return this.connectingBuses;
	}

	public void setConnectingBuses(String connectingBuses) {
		this.connectingBuses = connectingBuses;
	}

	public String getPointsOfInterest() {
		return this.pointsOfInterest;
	}

	public void setPointsOfInterest(String pointsOfInterest) {
		this.pointsOfInterest = pointsOfInterest;
	}

	public int getLatitudeE6() {
		return this.latitudeE6;
	}

	public void setLatitudeE6(int latitudeE6) {
		this.latitudeE6 = latitudeE6;
	}

	public int getLongitudeE6() {
		return this.longitudeE6;
	}

	public void setLongitudeE6(int longitudeE6) {
		this.longitudeE6 = longitudeE6;
	}
	
	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeInt(trackerID);
		out.writeString(stopNumber);
		
		out.writeString(stopName == null?" ":stopName);
		out.writeString(cityDirection == null?" ":cityDirection);
		out.writeString(stopDescription == null?" ":stopDescription);
		out.writeString(suburb == null?" ":suburb);
		
		out.writeStringArray(routes == null?new String[]{" "}:routes);
		
		out.writeString(zone);
		out.writeByte((byte)(isPlatformStop == true?1:0));
		out.writeByte((byte)(isEasyAccessStop == true?1:0));
		out.writeByte((byte)(hasShelter == true?1:0));
		out.writeByte((byte)(isCityStop == true?1:0));
		out.writeString(turnType == null?" ":turnType);
		
		out.writeString(connectingTrains == null?" ":connectingTrains);
		out.writeString(connectingTrams == null?" ":connectingTrams);
		out.writeString(connectingBuses == null?" ":connectingBuses);
		out.writeString(pointsOfInterest == null?" ":pointsOfInterest);

		out.writeInt(latitudeE6);
		out.writeInt(longitudeE6);
	}
	
	public void readFromParcel(Parcel in) {
		trackerID = in.readInt();
		stopNumber = in.readString();
		
		stopName = in.readString();
		cityDirection = in.readString();
		stopDescription = in.readString();
		suburb = in.readString();
		
		routes = in.createStringArray();
		
		zone = in.readString();
		isPlatformStop = in.readByte()==1?true:false;
		isEasyAccessStop = in.readByte()==1?true:false;
		hasShelter = in.readByte()==1?true:false;
		isCityStop = in.readByte()==1?true:false;
		turnType = in.readString();
		
		connectingTrains = in.readString();
		connectingTrams = in.readString();
		connectingBuses = in.readString();
		pointsOfInterest = in.readString();
		
		latitudeE6 = in.readInt();
		longitudeE6 = in.readInt();
	}
	
	public static final Parcelable.Creator<Stop> CREATOR = new Parcelable.Creator<Stop>() {
		public Stop createFromParcel(Parcel in) {
			return new Stop(in);
		}

		public Stop[] newArray(int size) {
			return new Stop[size];
		}
	};
	
	public String toString() {
//		return "Tracker ID = " + trackerID + " Stop name = " + stopName + " Stop number = " + stopNumber;
//		return "Tracker ID = " + trackerID;
		return "Stop number = " + stopNumber + " ,stop name = " + stopName + ", suburb = " + suburb;
//		return "Stop number = " + stopNumber;
	};
	
}
