package com.yarratrams.tramtracker.objects;

import android.os.Parcel;
import android.os.Parcelable;

public class NearbyTicketOutlet implements Parcelable {
	private TicketOutlet ticketOutlet;
	private int intDistance;

	public TicketOutlet getTicketOutlet() {
		return ticketOutlet;
	}

	public void setTicketOutlet(TicketOutlet ticketOutlet) {
		this.ticketOutlet = ticketOutlet;
	}

	public int getIntDistance() {
		return intDistance;
	}

	public void setIntDistance(int intDistance) {
		this.intDistance = intDistance;
	}
	
	public NearbyTicketOutlet(){
	}
	
	public NearbyTicketOutlet(TicketOutlet to, int intDistance){
		this.ticketOutlet = to;
		this.intDistance = intDistance;
	}
	
	private NearbyTicketOutlet(Parcel in) {
		readFromParcel(in);
	}
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeParcelable(ticketOutlet, 0);
		dest.writeInt(intDistance);
	}
	
	public void readFromParcel(Parcel in) {
		ticketOutlet = (TicketOutlet)in.readParcelable(NearbyTicketOutlet.class.getClassLoader());
		intDistance = in.readInt();
	}
	
	public static final Parcelable.Creator<NearbyTicketOutlet> CREATOR = new Parcelable.Creator<NearbyTicketOutlet>() {
		public NearbyTicketOutlet createFromParcel(Parcel in) {
			return new NearbyTicketOutlet(in);
		}

		public NearbyTicketOutlet[] newArray(int size) {
			return new NearbyTicketOutlet[size];
		}
	};
	
	public String toString() {
		return "Outlet name = " + ticketOutlet.getName() + ", distance = " + intDistance;
	};

}
