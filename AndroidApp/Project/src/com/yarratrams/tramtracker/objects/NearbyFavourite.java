package com.yarratrams.tramtracker.objects;

public class NearbyFavourite {
	private Favourite favourite;
	private int distance;
	
	public Favourite getFavourite() {
		return favourite;
	}
	public void setFavourite(Favourite favourite) {
		this.favourite = favourite;
	}
	public int getDistance() {
		return distance;
	}
	public void setDistance(int distance) {
		this.distance = distance;
	}
	
	public NearbyFavourite() {
		
	}
	
	public NearbyFavourite(Favourite favourite, int distance) {
		this.favourite = favourite;
		this.distance = distance;
	}
	
	@Override
	public String toString() {
		return "favourite = " + favourite + " distance = " + distance;
	}
}
