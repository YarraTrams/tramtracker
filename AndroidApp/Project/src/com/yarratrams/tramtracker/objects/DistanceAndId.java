package com.yarratrams.tramtracker.objects;

public class DistanceAndId {
	int distance;
	short id;
	
	public DistanceAndId(int distance, short id) {
		this.distance = distance;
		this.id = id;
  	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public short getId() {
		return id;
	}

	public void setId(short id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return "id = " + id + ", distance = " + distance;
	}
}
