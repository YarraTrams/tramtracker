package com.yarratrams.tramtracker.objects;

import java.util.ArrayList;

public class PredictedTimeResult {
	
	public ArrayList<PredictedArrivalTime> alPredictedArrivalTime;
	boolean upDirection;
	Route route;
	short startingIndex;
	Tram tram;
	boolean hasDisruption;
	boolean hasServiceChange;
	boolean isAccurate;
	ServiceDisruption serviceDisruption;
	ServiceChange serviceChange;
	
	public void setAlPredictedArrivalTime(
			ArrayList<PredictedArrivalTime> alPredictedArrivalTime) {
		this.alPredictedArrivalTime = alPredictedArrivalTime;
	}
	public void setUpDirection(boolean upDirection) {
		this.upDirection = upDirection;
	}
	public ArrayList<PredictedArrivalTime> getAlPredictedArrivalTime() {
		return alPredictedArrivalTime;
	}
	public boolean isUpDirection() {
		return upDirection;
	}
	public Route getRoute() {
		return route;
	}
	public void setRoute(Route route) {
		this.route = route;
	}
	public short getStartingIndex() {
		return startingIndex;
	}
	public void setStartingIndex(short startingIndex) {
		this.startingIndex = startingIndex;
	}
	public Tram getTram() {
		return tram;
	}
	public void setTram(Tram tram) {
		this.tram = tram;
	}
	public boolean isHasDisruption() {
		return hasDisruption;
	}
	public void setHasDisruption(boolean hasDisruption) {
		this.hasDisruption = hasDisruption;
	}
	public boolean isAccurate() {
		return isAccurate;
	}
	public void setAccurate(boolean isAccurate) {
		this.isAccurate = isAccurate;
	}
	public ServiceDisruption getServiceDisruption() {
		return serviceDisruption;
	}
	public void setServiceDisruption(ServiceDisruption serviceDisruption) {
		this.serviceDisruption = serviceDisruption;
	}
	public boolean isHasServiceChange() {
		return hasServiceChange;
	}
	public void setHasServiceChange(boolean hasServiceChange) {
		this.hasServiceChange = hasServiceChange;
	}
	public ServiceChange getServiceChange() {
		return serviceChange;
	}
	public void setServiceChange(ServiceChange serviceChange) {
		this.serviceChange = serviceChange;
	}
	
}
