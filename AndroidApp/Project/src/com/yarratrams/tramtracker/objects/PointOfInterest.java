package com.yarratrams.tramtracker.objects;

import android.os.Parcel;
import android.os.Parcelable;

public class PointOfInterest implements Parcelable{
	private int poiId;
	private String name;
	private String description;
	private String phone;
	private String webAddress;
	private String email;
	private boolean hasDisabled;
	private boolean hasToilet;
	private String openingHours;
	private String entryFee;
	private String streetAddress;
	private String suburb;
	private String postCode;
	private double latitude;
	private double longitude;
	private String connectingBuses, connectingTrains, connectingTrams;
	
	public PointOfInterest() {
		name = "";
		description = "";
		phone = "";
		webAddress = "";
		email = "";
		openingHours = "";
		entryFee = "";
		streetAddress = "";
		suburb = "";
		postCode = "";
	}
	
	public PointOfInterest(Parcel in) {
		readFromParcel(in);
	}
	
	public int getPoiId() {
		return poiId;
	}
	
	public void setPoiId(int poiId) {
		this.poiId = poiId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getPhone() {
		return phone;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getWebAddress() {
		return webAddress;
	}
	
	public void setWebAddress(String webAddress) {
		this.webAddress = webAddress;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public boolean isHasDisabled() {
		return hasDisabled;
	}
	
	public void setHasDisabled(boolean hasDisabled) {
		this.hasDisabled = hasDisabled;
	}
	
	public boolean isHasToilet() {
		return hasToilet;
	}
	
	public void setHasToilet(boolean hasToilet) {
		this.hasToilet = hasToilet;
	}
	
	public String getOpeningHours() {
		return openingHours;
	}
	
	public void setOpeningHours(String openingHours) {
		this.openingHours = openingHours;
	}
	
	public String getEntryFee() {
		return entryFee;
	}
	
	public void setEntryFree(String entryFee) {
		this.entryFee = entryFee;
	}
	
	public String getStreetAddress() {
		return streetAddress;
	}
	
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}
	
	public String getSuburb() {
		return suburb;
	}
	
	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}
	
	public String getPostCode() {
		return postCode;
	}
	
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	
	public double getLatitude() {
		return latitude;
	}
	
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	public double getLongitude() {
		return longitude;
	}
	
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	public String getConnectingBuses() {
		return connectingBuses;
	}

	public void setConnectingBuses(String connectingBuses) {
		this.connectingBuses = connectingBuses;
	}

	public String getConnectingTrains() {
		return connectingTrains;
	}

	public void setConnectingTrains(String connectingTrains) {
		this.connectingTrains = connectingTrains;
	}

	public String getConnectingTrams() {
		return connectingTrams;
	}

	public void setConnectingTrams(String connectingTrams) {
		this.connectingTrams = connectingTrams;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeInt(poiId);
		out.writeString(name);
		out.writeString(description);
		out.writeString(phone);
		out.writeString(webAddress);
		out.writeString(email);
		out.writeByte((byte)(hasDisabled == true?1:0));
		out.writeByte((byte)(hasToilet == true?1:0));
		out.writeString(openingHours);
		out.writeString(entryFee);
		out.writeString(streetAddress);
		out.writeString(suburb);
		out.writeString(postCode);
		out.writeDouble(latitude);
		out.writeDouble(longitude);
		out.writeString(connectingBuses);
		out.writeString(connectingTrains);
		out.writeString(connectingTrams);
	}
	
	public void readFromParcel(Parcel in) {
		poiId = in.readInt();
		name = in.readString();
		description = in.readString();
		phone = in.readString();
		webAddress = in.readString();
		email = in.readString();
		hasDisabled = in.readByte()==1?true:false;
		hasToilet = in.readByte()==1?true:false;
		openingHours = in.readString();
		entryFee = in.readString();
		streetAddress = in.readString();
		suburb = in.readString();
		postCode = in.readString();
		latitude = in.readDouble();
		longitude = in.readDouble();
		connectingBuses = in.readString();
		connectingTrains = in.readString();
		connectingTrams = in.readString();
	}
	
	public static final Parcelable.Creator<PointOfInterest> CREATOR = new Parcelable.Creator<PointOfInterest>() {
		public PointOfInterest createFromParcel(Parcel in) {
			return new PointOfInterest(in);
		}

		public PointOfInterest[] newArray(int size) {
			return new PointOfInterest[size];
		}
	};
	
	@Override
	public String toString() {
		return "name = " + name + ", connectingBuses = " + connectingBuses + ", connectingTrains = " + connectingTrains + ", connectingTrams = " + connectingTrams;
//		return "name = " + name + ", latitude = " + latitude + ", longitude = " + longitude;
	}
}
