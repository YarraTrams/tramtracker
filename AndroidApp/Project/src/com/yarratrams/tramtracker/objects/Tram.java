package com.yarratrams.tramtracker.objects;

import java.util.Date;

import android.os.Parcel;
import android.os.Parcelable;



public class Tram implements Parcelable{
	public static final int CLASS_Z1 = 1;
	public static final int CLASS_Z2 = 2;
	public static final int CLASS_Z3 = 3;
	public static final int CLASS_A1 = 4;
	public static final int CLASS_A2 = 5;
	public static final int CLASS_W = 6;
	public static final int CLASS_B1 = 7;
	public static final int CLASS_B2 = 8;
	public static final int CLASS_C1 = 9;
	public static final int CLASS_D1 = 10;
	public static final int CLASS_D2 = 11;
	public static final int CLASS_C2 = 12;
	
	private String headboardNo;
	private int internalRouteNo;
	private String destination;
	private int vehicleNumber;
	private int tramClass;				// Type of tram placeholder in the form of index or id?
	private boolean isLowFloor;			// wheelchair access availability in Tram  
	private boolean hasAirCon;			// Air conditioning in the Tram
	private boolean isServiceChanged;	// Indicates if the line had a service change
	private String changeMessage;
	private boolean isDisrupted;		// Indicates if the line had a service disruption
	private String disrruptionMessage;
	
	private int minutesFromServerResponseTime;	//Time to arrive to stop in minutes as difference from serverReferencetime
	private Date arrivalTime;					// Time to arrive to stop
	private boolean hasSpecialEvent;		// Indicates if the line had a service disruption
	private String specialEventMessage;
	
	
	public int getInternalRouteNo() {
		return internalRouteNo;
	}

	public void setInternalRouteNo(int internalRouteNo) {
		this.internalRouteNo = internalRouteNo;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getHeadboardNo() {
		return headboardNo;
	}

	public void setHeadboardNo(String headboardNo) {
		this.headboardNo = headboardNo;
	}
	
	public String getSpecialEventMessage() {
		return specialEventMessage;
	}

	public boolean hasSpecialEvent() {
		return hasSpecialEvent;
	}

	public void setHasSpecialEvent(boolean hasSpecialEvent) {
		this.hasSpecialEvent = hasSpecialEvent;
	}

	public void setSpecialEventMessage(String specialEventMessage) {
		this.specialEventMessage = specialEventMessage;
	}

	public Tram(){
		arrivalTime = new Date();
	}
	
	private Tram(Parcel in) {
		this();
		readFromParcel(in);
	}
	

	public int getVehicleNumber() {
		return this.vehicleNumber;
	}

	public void setVehicleNumber(int vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
		identifyTramClass(vehicleNumber);
	}

	public int getTramClass() {
		return this.tramClass;
	}

	public void identifyTramClass(int vehicleNumber){
		if(vehicleNumber >= 1 && vehicleNumber <= 100){
			setTramClass(CLASS_Z1);
		} else if(vehicleNumber >= 101 && vehicleNumber <= 115){
			setTramClass(CLASS_Z2);
		} else if(vehicleNumber >= 116 && vehicleNumber <= 230){
			setTramClass(CLASS_Z3);
		} else if(vehicleNumber >= 231 && vehicleNumber <= 258){
			setTramClass(CLASS_A1);
		} else if(vehicleNumber >= 259 && vehicleNumber <= 300){
			setTramClass(CLASS_A2);
		} else if(vehicleNumber >= 681 && vehicleNumber <= 1040){
			setTramClass(CLASS_W);
		} else if(vehicleNumber >= 2001 && vehicleNumber <= 2002){
			setTramClass(CLASS_B1);
		} else if(vehicleNumber >= 2003 && vehicleNumber <= 2132){
			setTramClass(CLASS_B2);
		} else if(vehicleNumber >= 3001 && vehicleNumber <= 3036){
			setTramClass(CLASS_C1);
		} else if(vehicleNumber >= 3501 && vehicleNumber <= 3600){
			setTramClass(CLASS_D1);
		} else if(vehicleNumber >= 5001 && vehicleNumber <= 5100){
			setTramClass(CLASS_D2);
		} else if(vehicleNumber >= 5101 && vehicleNumber <= 5200){
			setTramClass(CLASS_C2);
		}
	}
	
	public void setTramClass(int tramClass) {
		this.tramClass = tramClass;
	}

	public boolean isLowFloor() {
		return this.isLowFloor;
	}

	public void setLowFloor(boolean isLowFloor) {
		this.isLowFloor = isLowFloor;
	}

	public boolean isHasAirCon() {
		return this.hasAirCon;
	}

	public void setHasAirCon(boolean hasAirCon) {
		this.hasAirCon = hasAirCon;
	}

	public boolean isServiceChanged() {
		return this.isServiceChanged;
	}

	public void setServiceChanged(boolean isServiceChanged) {
		this.isServiceChanged = isServiceChanged;
	}

	public String getChangeMessage() {
		return this.changeMessage;
	}

	public void setChangeMessage(String changeMessage) {
		this.changeMessage = changeMessage;
	}

	public boolean isDisrupted() {
		return this.isDisrupted;
	}

	public void setDisrupted(boolean isDisrupted) {
		this.isDisrupted = isDisrupted;
	}

	public String getDisrruptionMessage() {
		return this.disrruptionMessage;
	}

	public void setDisrruptionMessage(String disrruptionMessage) {
		this.disrruptionMessage = disrruptionMessage;
	}
	
	public int getMinutesFromServerResponseTime() {
		return minutesFromServerResponseTime;
	}

	public void setMinutesFromServerResponseTime(int minutesFromServerResponseTime) {
		this.minutesFromServerResponseTime = minutesFromServerResponseTime;
	}


	public Date getArrivalTime() {
		return this.arrivalTime;
	}

	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeInt(vehicleNumber);
		out.writeInt(tramClass);
		out.writeByte((byte)(isLowFloor == true?1:0));
		out.writeByte((byte)(hasAirCon == true?1:0));
		out.writeByte((byte)(isServiceChanged == true?1:0));
		out.writeString(changeMessage);
		out.writeByte((byte)(isDisrupted == true?1:0));
		out.writeString(disrruptionMessage);
		out.writeLong(arrivalTime.getTime());
	}

	
	public void readFromParcel(Parcel in) {
		vehicleNumber = in.readInt();
		tramClass = in.readInt();
		isLowFloor = in.readByte()==1?true:false;
		hasAirCon = in.readByte()==1?true:false;
		isServiceChanged = in.readByte()==1?true:false;
		changeMessage = in.readString();
		isDisrupted = in.readByte()==1?true:false;
		disrruptionMessage = in.readString();
		arrivalTime.setTime(in.readLong());
	}
	
	public static final Parcelable.Creator<Tram> CREATOR = new Parcelable.Creator<Tram>() {
		public Tram createFromParcel(Parcel in) {
			return new Tram(in);
		}

		public Tram[] newArray(int size) {
			return new Tram[size];
		}
	};
	
	public String toString() {
//		return "Vehicle Number = " + vehicleNumber + " Arrival time = " + arrivalTime + ", minutes from = " + minutesFromServerResponseTime;
		return "Vehicle Number = " + vehicleNumber + " HasSpecialEvent = " + hasSpecialEvent;
	};
	
}
