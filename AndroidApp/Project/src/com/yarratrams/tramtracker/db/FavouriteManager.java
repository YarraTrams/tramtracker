package com.yarratrams.tramtracker.db;

import java.util.ArrayList;

import com.yarratrams.tramtracker.objects.Favourite;
import com.yarratrams.tramtracker.objects.FavouritesGroup;
import com.yarratrams.tramtracker.objects.Route;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.singleton.TTDBSingleton;
import com.yarratrams.tramtracker.singleton.TTDBUpdateSingleton;

import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;

public class FavouriteManager {
	
	private Context context;
	private TTDBUpdate ttdbUpdate = null;
	
	public FavouriteManager(Context context) {
		this.context = context;
		if(ttdbUpdate == null)
			ttdbUpdate = TTDBUpdateSingleton.getTTDBUpdateInstance(context);
		ttdbUpdate.createFavouritesTables();
	}
	
	public boolean addFavouriteGroup (FavouritesGroup favGroup) throws SQLiteConstraintException{
		if(ttdbUpdate == null)
			ttdbUpdate = TTDBUpdateSingleton.getTTDBUpdateInstance(context);
		return ttdbUpdate.addFavouriteGroup(favGroup);
	}
	
	public boolean editFavouriteGroup (FavouritesGroup oldFavGroup, FavouritesGroup newFavGroup) throws SQLiteConstraintException{
		if(ttdbUpdate == null)
			ttdbUpdate = TTDBUpdateSingleton.getTTDBUpdateInstance(context);
		return ttdbUpdate.editFavouriteGroup(oldFavGroup, newFavGroup);
	}
	
	public boolean removeFavouriteGroup (FavouritesGroup favGroup)  throws SQLiteConstraintException{
		if(ttdbUpdate == null)
			ttdbUpdate = TTDBUpdateSingleton.getTTDBUpdateInstance(context);
		return ttdbUpdate.removeFavouriteGroup(favGroup);
	}
	
	public boolean addFavourite (FavouritesGroup favGroup, Favourite favourite) throws SQLiteConstraintException{
		if(ttdbUpdate == null)
			ttdbUpdate = TTDBUpdateSingleton.getTTDBUpdateInstance(context);
		return ttdbUpdate.addFavourite(favGroup, favourite);	
	}
	
	public boolean removeFavourite (FavouritesGroup favGroup, Favourite favourite) throws SQLiteConstraintException{
		if(ttdbUpdate == null)
			ttdbUpdate = TTDBUpdateSingleton.getTTDBUpdateInstance(context);
		return ttdbUpdate.removeFavourite(favGroup, favourite);	
	}
	
	public boolean editFavourite (FavouritesGroup favGroup, Favourite fav) throws SQLiteConstraintException{
		if(ttdbUpdate == null)
			ttdbUpdate = TTDBUpdateSingleton.getTTDBUpdateInstance(context);
		return ttdbUpdate.editFavourite(favGroup, fav);	
	}
	
	public ArrayList<FavouritesGroup> getAllFavourites(){
		TTDB ttdb = TTDBSingleton.getInstance(context);
		ArrayList<FavouritesGroup> alFavGroups = ttdb.getAllFavouriteGroups();	
		if(alFavGroups != null)
			for(FavouritesGroup group : alFavGroups){
				ttdb.getFavouritesForGroup(group);
			}
		return alFavGroups;
	}
	
	public ArrayList<Route> getRoutesForStop(Stop stop){
		ArrayList<Route> alRoutes = null;
		TTDB ttdb = TTDBSingleton.getInstance(context);
		if(stop != null){
			alRoutes = ttdb.getRoutesForStop(stop);
		}
		return alRoutes;
	}
	
	public Favourite getFavouriteForStop(int stopNo){
		return TTDBSingleton.getInstance(context).getFavouriteForStop(stopNo);
	}
	
	public boolean isStopFavourite(int stopNo){
		return TTDBSingleton.getInstance(context).isStopFavourite(stopNo);
	}
}
