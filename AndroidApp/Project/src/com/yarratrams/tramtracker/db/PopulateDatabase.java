package com.yarratrams.tramtracker.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.yarratrams.tramtracker.objects.Constants;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.singleton.TTDBSingleton;
import com.yarratrams.tramtracker.singleton.TTDBUpdateSingleton;
import com.yarratrams.tramtracker.webserviceinteraction.TTWebService;
import com.yarratrams.tramtracker.webserviceinteraction.TTWebServiceSingleton;

public class PopulateDatabase {
	TTWebService webservice;
	TTDBUpdate ttdbUpdate;
	Context context;
	
	public PopulateDatabase(Context context) {
		webservice = TTWebServiceSingleton.getInstance();
		ttdbUpdate = TTDBUpdateSingleton.getTTDBUpdateInstance(context);
		this.context = context;
	}
	
	public boolean populateAll(){
		boolean result = true;
		ttdbUpdate.createAllTables();
		if(!getTicketOutlets())
			result = false;
		ArrayList<String> alRoutes = new ArrayList<String>();
		boolean resultGetRoute = getRoutes(alRoutes);
		if(resultGetRoute){
			Log.d("alroutes","Alroutes = " + alRoutes);
			for(int i=0; i< alRoutes.size(); i++){
				if(!getStopsForRoute(alRoutes.get(i)))
					result = false;
			}
		}
		else
			result = false;
		if(!getPOIs())
			result = false;
		if(result){
			SharedPreferences appSharedPrefs = context.getSharedPreferences(Constants.kSharedPrefIdentifier, Activity.MODE_PRIVATE);
		    Editor prefsEditor = appSharedPrefs.edit();
		    Date dateNow = new Date();
		    prefsEditor.putLong(Constants.kLastUpdateDate, dateNow.getTime());
		    prefsEditor.commit();
		}
		return result;
	}
	
	public boolean getTicketOutlets(){
		boolean result = false;
		JSONObject object = webservice.getAllTicketOutlets();
		if(object != null && object != JSONObject.NULL){
			result = ttdbUpdate.createTicketOutlets(object);
		}
//		ttdbUpdate.createStopsAndNearbyOutlets();
		return result;		
	}
	
	public boolean getRoutes(ArrayList<String> alRoutes){
		boolean result = false;
		JSONObject object = webservice.getAllRoutes();
		if(object != null && object != JSONObject.NULL){
			ArrayList<JSONObject> alRouteDirections = new ArrayList<JSONObject>();
			ArrayList<JSONObject> alRouteObjects = new ArrayList<JSONObject>();
			try {
//				if(!object.getBoolean("hasError") && object.getBoolean("hasResponse")){
				if(!object.getBoolean("hasError") && object.getBoolean("hasResponse")){
					JSONObject responseObject = object.getJSONObject("responseObject");
					if(responseObject != null && responseObject != JSONObject.NULL){
//						Log.e("responseObject", "responseObject != null");
						JSONArray routeChanges = responseObject.getJSONArray("RouteChanges");
						JSONObject routeChange;
						String routeNo;
						JSONObject routeDirections;
						for(int i=0; i< routeChanges.length(); i++){
							routeChange = routeChanges.getJSONObject(i);
							if(routeChange.getString("Action").equalsIgnoreCase("UPDATE")){
								routeNo = routeChange.getString("ID");
								routeDirections = webservice.getDestinationsForRoute(routeNo);
								if(routeDirections != null && routeDirections != JSONObject.NULL){
									if(!routeDirections.getBoolean("hasError") && routeDirections.getBoolean("hasResponse")){
										alRouteDirections.add(routeDirections);
										alRouteObjects.add(routeChange);
									}
									else{
//										Log.e("getRoutes","route directions error for routeNo = " + routeNo);
									}
								}
							}
						}
						result = ttdbUpdate.createRoutes(alRouteObjects,alRouteDirections, alRoutes);
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return result;	
	}
	
//	public boolean getStopsForRoute(String routeNo){
//		boolean result = false;
//		boolean isUpDirection = true;
////		JSONObject object = webservice.getAllStopsForRoute(routeNo, isUpDirection);
//		JSONObject object = webservice.getAllStopsForRoute(routeNo);
//		ArrayList<Integer> alStops = new ArrayList<Integer>();
//		if(object != null && object != JSONObject.NULL){
//			result = ttdbUpdate.createRoutesAndStops(object, routeNo, isUpDirection, alStops);
//			if(alStops != null){
//				Integer stopNo;
//				JSONObject stopObject;
//				ArrayList<JSONObject> alObject = new ArrayList<JSONObject>();
//				for(int i=0; i< alStops.size(); i++){
//					stopNo = alStops.get(i);
//					stopObject = webservice.getStopForTrackerId(stopNo.toString());
//					if(stopObject != null)
//						alObject.add(stopObject);
//					else
//						System.out.println("Problem fetching stop info for stopNo = " + stopNo);
//				}
//				ttdbUpdate.updateStopsCreatedInitially(alStops, alObject);
//			}
//			isUpDirection = false;
//			object = webservice.getAllStopsForRoute(routeNo, isUpDirection);
//			alStops = new ArrayList<Integer>();
//			result = ttdbUpdate.createRoutesAndStops(object, routeNo, isUpDirection, alStops);
//			if(alStops != null){
//				Integer stopNo;
//				JSONObject stopObject;
//				ArrayList<JSONObject> alObject = new ArrayList<JSONObject>();
//				for(int i=0; i< alStops.size(); i++){
//					stopNo = alStops.get(i);
//					stopObject = webservice.getStopForTrackerId(stopNo.toString());
//					if(stopObject != null)
//						alObject.add(stopObject);
//					else
//						System.out.println("Problem fetching stop info for stopNo = " + stopNo);
//				}
//				ttdbUpdate.updateStopsCreatedInitially(alStops, alObject);
//			}
//		}
//		return result;			
//	}
	
	public boolean getStopsForRoute(String routeNo){
		boolean result = false;
		JSONObject object = webservice.getAllStopsForRoute(routeNo);
		ArrayList<Integer> alStops = new ArrayList<Integer>();
		if(object != null && object != JSONObject.NULL){
			result = ttdbUpdate.createRoutesAndStops(object, routeNo, alStops);
			if(alStops != null){
				Integer stopNo;
				JSONObject stopObject;
				ArrayList<JSONObject> alObject = new ArrayList<JSONObject>();
				for(int i=0; i< alStops.size(); i++){
					stopNo = alStops.get(i);
					stopObject = webservice.getStopForTrackerId(stopNo.toString());
					if(stopObject != null)
						alObject.add(stopObject);
//					else
//						System.out.println("Problem fetching stop info for stopNo = " + stopNo);
				}
				ttdbUpdate.updateStopsCreatedInitially(alStops, alObject);
			}
		}
		return result;			
	}
	
	public boolean getPOIs(){
		boolean result = false;
		JSONObject object = webservice.getAllPOIs();
		ttdbUpdate.createPOI(object, webservice);
		return result;
	}
	
	public boolean createTicketOutletsForStops(){
		boolean result = false;
		TTDB ttdb = TTDBSingleton.getInstance(context);
		return result;
	}
	
}
