package com.yarratrams.tramtracker.db;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.Constants;
import com.yarratrams.tramtracker.objects.Favourite;
import com.yarratrams.tramtracker.objects.FavouritesGroup;
import com.yarratrams.tramtracker.objects.NearbyTicketOutlet;
import com.yarratrams.tramtracker.objects.PointOfInterest;
import com.yarratrams.tramtracker.objects.Route;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.objects.TicketOutlet;
import com.yarratrams.tramtracker.webserviceinteraction.TTWebService;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.location.Location;
import android.util.Log;
import android.widget.Toast;

public class TTDBUpdate extends SQLiteOpenHelper{

	private SQLiteDatabase db;
	private Context context;
	
	public TTDBUpdate(Context context) {
		super(context,Constants.kDBName, null, 1);
		this.context = context;
		try {
			createDataBase();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void onCreate(SQLiteDatabase arg0) {
		
	}

	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		
	}
	
	public void openDataBase() throws SQLException{
		//Open the database
		String myPath = Constants.kDBLocalPath + Constants.kDBName;
		db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
		//     	db.close();
		//     	db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE); 
	}
	
	  /**
     * Creates a empty database on the system and rewrites it with your own database.
     * */
    public void createDataBase() throws IOException{
 
    	boolean dbExist = checkDataBase();
 
    	if(dbExist){
    		//do nothing - database already exist
    	}else{
 
    		//By calling this method and empty database will be created into the default system path
               //of your application so we are going be able to overwrite that database with our database.
        	this.getReadableDatabase();
        	this.close();
   			constructNewDatabaseFromResources();
   			this.close();
    	}
 
    }
    
    /*
     * Takes split database (due to size limitation from raw directory) and combines them to the dataabse file.
     */
    
    public void constructNewDatabaseFromResources()
    {
    	//Log.v("DataBase Installation", "Before creating files");
    	int resourceList[] = new int[] {
    			R.raw.xaa,
    			R.raw.xab
    	};
    	
//    	int resourceList[] = new int[] {
//    			R.raw.xaa
//    	};

    	try
    	{
    		// Path to the just created empty db
    		String outFileName = Constants.kDBLocalPath + Constants.kDBName;

    		//Open the empty db as the output stream
    		FileOutputStream fos = new FileOutputStream(outFileName);
    		for ( int fileId : resourceList )
    		{
    			InputStream inputFile = context.getResources().openRawResource(fileId);

    			int totalLength = 0;
    			try
    			{
    				totalLength = inputFile.available();
    			}
    			catch ( IOException e)
    			{
    				Toast.makeText( context,"Error Reading File",Toast.LENGTH_SHORT).show();
    			}

    			// Reading and writing the file Method 1 :
    			byte[] buffer = new byte[totalLength];
    			int len = 0;
    			try
    			{
    				len = inputFile.read(buffer);
    			}
    			catch ( IOException e)
    			{
    				Toast.makeText(context,"Error Reading File",Toast.LENGTH_SHORT).show();
    			}
    			fos.write( buffer );
    			inputFile.close();
    		}    
    		fos.close();
    	}
    	catch( IOException e)
    	{
    		Toast.makeText(context,"IO Error Reading/writing File",Toast.LENGTH_SHORT).show();
    	}
//    	System.out.println("DataBase Installation, End of creating files");
    }	
    
    /**
     * Check if the database already exists to prevent re-copying the file each time you open the application.
     * @return true if db exists, false if it doesn't
     */
    private boolean checkDataBase(){
 
    	SQLiteDatabase checkDB = null;
 
    	try{
    		String myPath = Constants.kDBLocalPath + Constants.kDBName;
    		checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
 
    	}catch(SQLiteException e){
 
    		//database does't exist yet.
 
    	}
 
    	if(checkDB != null){
 
    		checkDB.close();
 
    	}
 
    	return checkDB != null ? true : false;
    }

    
    
	public synchronized void close() {
		 
	    if(db != null)
		    db.close();

	    super.close();

	}
	

	/**
	 * Add a favourite given a favourite group 
	 * @param group (if null, use default group), favourite. Needs the display order and stop object with name at least.
	 * @return
	 */
	public boolean addFavourite(FavouritesGroup group, Favourite favourite){
		boolean boolResult = false;
		if(db == null){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
				return false;
			}
		}
		if(!db.isOpen()){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
				return false;
			}
		}
		if(db.isOpen()){
			try{
				if(favourite != null){
					//If group is null, assume default group
					if(group == null){
						group = new FavouritesGroup();
						group.setName(Constants.kDefaultGroupName);
					}
					if(favourite.getName() == null){
						//if stop name not specified, user stop name
						favourite.setName(favourite.getStop().getStopName());
					}
					ContentValues values = new ContentValues();
					values.put("GroupName",group.getName());
					values.put("FavouriteName", favourite.getName());
					values.put("DisplayOrder", favourite.getOrder());
					values.put("StopNo", favourite.getStop().getTrackerID());
					long result = db.insert("favouritesandfavgroups", "FavouriteName", values);
					if(result != -1)
						boolResult = true;
					if(favourite.getAlRoutes() != null){
						for(String routeNo : favourite.getAlRoutes()){
							values = new ContentValues();
							values.put("StopNo",favourite.getStop().getTrackerID());
							values.put("RouteNo", routeNo);
							result = (int)db.insert("favouritestoproutes","StopNo",values);
//							System.out.println("Routeno = " + routeNo + ", route list save result = " + result);
						}
					}
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
			
		return boolResult;
	}
	
	/**
	 * Edit a favourite given a favourite group 
	 * @param group (if null, use default group), favourite. Needs the display order and stop object with name at least.
	 * @return
	 */
	public boolean editFavourite(FavouritesGroup group, Favourite fav){
		boolean boolResult = false;
		if(db == null){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
				return false;
			}
		}
		if(!db.isOpen()){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
				return false;
			}
		}
		if(db.isOpen()){
			try{
				if(fav != null){
					if(group == null){
						group = new FavouritesGroup();
						group.setName(Constants.kDefaultGroupName);
					}
					ContentValues values = new ContentValues();
					values.put("GroupName",group.getName());
					values.put("FavouriteName", fav.getName());
					values.put("DisplayOrder", fav.getOrder());
					String whereClause = "StopNo=" + fav.getStop().getTrackerID();
//					System.out.println("whereClause = " + whereClause);	
					int result = db.update("favouritesandfavgroups", values, whereClause, null);
					db.delete("favouritestoproutes", whereClause, null);
					if(fav.getAlRoutes() != null){
						for(String routeNo : fav.getAlRoutes()){
							values = new ContentValues();
							values.put("StopNo",fav.getStop().getTrackerID());
							values.put("RouteNo", routeNo);
							result = (int)db.insert("favouritestoproutes","StopNo",values);
						}
					}
//					String sql = "update favouritesandfavgroups SET GroupName=?, FavouriteName=?, DisplayOrder=?, TramTrackerId=? where GroupName=? and FavouriteName=? and DisplayOrder=?";
//					SQLiteStatement stmt = db.compileStatement(sql);
//					stmt.bindString(1, group.getName());
//					stmt.bindString(2, newFav.getName());
//					stmt.bindLong(3, newFav.getOrder());
//					stmt.bindLong(4, newFav.getStop().getTrackerID());
//					stmt.bindString(5, group.getName());
//					stmt.bindString(6, oldFav.getName());
//					stmt.bindLong(7, oldFav.getOrder());
//					stmt.execute();
					if(result > 0)
						boolResult = true;
//					if (result > 1)
//						System.out.println("More than one rows affected");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return boolResult;
	}
	
	/**
	 * Remove/delete a favourite given a favourite group 
	 * @param group (if null, use default group), favourite. Needs the display order and stop object with name at least.
	 * @return
	 */
	public boolean removeFavourite(FavouritesGroup group, Favourite favourite){
		boolean boolResult = false;
		if(db == null){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
				return false;
			}
		}
		if(!db.isOpen()){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
				return false;
			}
		}
		if(db.isOpen()){
			try{
				if(group == null){
					group = new FavouritesGroup();
					group.setName(Constants.kDefaultGroupName);
				}
				if(favourite.getName() == null){
					//if stop name not specified, user stop name
					favourite.setName(favourite.getStop().getStopName());
				}
				String whereClause = "StopNo=" + favourite.getStop().getTrackerID();
				int result = db.delete("favouritesandfavgroups", whereClause, null);
				if(result > 0)
					boolResult = true;
				db.delete("favouritestoproutes", whereClause, null);
//				if (result > 1)
//					System.out.println("More than one rows affected");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return boolResult;
	}
	
	/**
	 * Create new favourite group
	 * @param group must consist of group name and display order
	 * @return
	 */
	
	public boolean addFavouriteGroup(FavouritesGroup group) throws SQLiteConstraintException{
		boolean boolResult = false;
		if(db == null){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
				return false;
			}
		}
		if(!db.isOpen()){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
				return false;
			}
		}
		if(db.isOpen()){
			if(group.getName() != null){
				try{
					ContentValues insertFavGroup = new ContentValues();
					insertFavGroup.put("Name", group.getName());
					insertFavGroup.put("DisplayOrder", group.getOrder());
					short result = (short)db.insert("favouritegroup", null, insertFavGroup);
//					System.out.println("insertFavGroup result = " + result);
					if(result == -1)
						boolResult = false;
					else
						boolResult = true;
				}catch(SQLiteException sqlex){
					if(sqlex instanceof SQLiteConstraintException)
						throw sqlex;
				}
			}
		}

		db.close();
		return boolResult;
	}
	
	/**
	 * Remove/delete favourite group. Removes all favourites associated with the group
	 * @param group (Uses the groupName as key)
	 * @return
	 */
	
	public boolean removeFavouriteGroup(FavouritesGroup group){
		boolean boolResult = false;
		if(db == null){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
				return false;
			}
		}
		if(!db.isOpen()){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
				return false;
			}
		}
		if(group.getName() != null){
			try{
				int result = db.delete("favouritegroup", "Name=?", new String[]{group.getName()});
				if(result != -1){
					if(group.getFavourites() != null){
						Favourite favourite;
						for(int i=0; i<group.getFavourites().size(); i++){
							favourite = group.getFavourites().get(i);
							if(favourite.getName() != null){
								result = db.delete("favouritesandfavgroups", "GroupName=?", new String[]{group.getName()});
							}
						}
					}
				}
//				System.out.println("delete result = " + result);
				if(result > 0)
					boolResult = false;
				else{
					boolResult = true;
//					if(result > 1)
//						System.out.println("Multiple rows deleted");
				}
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		db.close();
		return boolResult;
	}
	
	
	/**
	 * Change name/order of favourite group
	 * @param group
	 * @return true if changed successfully
	 */
	public boolean editFavouriteGroup(FavouritesGroup oldFavGroup, FavouritesGroup newFavGroup) throws SQLiteConstraintException{
		boolean boolResult = false;
		if(db == null){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
				return false;
			}
		}
		if(!db.isOpen()){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
				return false;
			}
		}
		if(oldFavGroup != null && newFavGroup != null){
			if(oldFavGroup.getName() != null && newFavGroup.getName() != null){
				try{
					ContentValues values = new ContentValues();
					values.put("Name", newFavGroup.getName());
					values.put("DisplayOrder", newFavGroup.getOrder());
					int result = db.update("favouritegroup", values, "Name=?", new String[]{oldFavGroup.getName()});
//					System.out.println("editFavGroup result = " + result);
					if(result == -1)
						boolResult = false;
					else {
						boolResult = true;
						if(!oldFavGroup.getName().equals(newFavGroup.getName())){
							values = new ContentValues();
							values.put("GroupName", newFavGroup.getName());
							result = db.update("favouritesandfavgroups", values, "GroupName=?", new String[]{oldFavGroup.getName()});
						}
						if(result == -1)
							boolResult = false;
						else
							boolResult = true;
					}

				}catch(SQLiteException sqlex){
					if(sqlex instanceof SQLiteConstraintException)
						throw sqlex;
				}
			}
		}
		db.close();
		return boolResult;
	}
	
	
	public boolean updateRoutesAndStopsViaUpdate(JSONObject object, ArrayList<JSONObject> alAddRoutes, ArrayList<Integer> alUpdateStops){
		boolean result = true;
		if(db == null){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
				return false;
			}
		}
		if(!db.isOpen()){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
				return false;
			}
		}
		try {
			if(object.getBoolean("hasError") == false && object.getBoolean("hasResponse") == true){
				SQLiteStatement updateRoutes = db.compileStatement("UPDATE routes SET Colour = ?, HeadboardRouteNo = ?, IsMainRoute=? WHERE RouteNo = ?");
				SQLiteStatement deleteRoute = db.compileStatement("DELETE FROM routes WHERE RouteNo = ?");
				SQLiteStatement deleteStops = db.compileStatement("DELETE FROM stops WHERE StopNo = ?");
				if(object != null && object != JSONObject.NULL){
					JSONArray routeChanges = object.getJSONArray("RouteChanges");
					db.beginTransaction();
					if(routeChanges != null && routeChanges != JSONObject.NULL){
						JSONObject routeChange;
						for(int  i=0; i < routeChanges.length(); i++){
							routeChange = routeChanges.getJSONObject(i);
							if(routeChange.getString("Action").equalsIgnoreCase("UPDATE")){
								Cursor c = db.query("routes", 
										new String[] { "RouteNo"}, 
										"RouteNo = '"  + routeChange.getString("RouteNo") + "'", 
										null, 
										null, 
										null, 
										null, 
										null);
					
								if (c.getCount() > 0) {		
									Log.d("updateRoutesAndStopsViaUpdate","colour = " + routeChange.getString("Colour") + 
											" HeadboardRouteNo " + routeChange.getString("HeadboardRouteNo") + "IsMainRoute"
											+ routeChange.getBoolean("IsMainRoute") + " RouteNo = " + routeChange.getString("RouteNo"));
									updateRoutes.bindString(1, routeChange.getString("Colour"));
									updateRoutes.bindString(2, routeChange.getString("HeadboardRouteNo"));
									updateRoutes.bindLong(3, routeChange.getBoolean("IsMainRoute")?1:0);
									updateRoutes.bindString(4, routeChange.getString("RouteNo"));
									updateRoutes.execute();
									updateRoutes.clearBindings();
								}
								else
									alAddRoutes.add(routeChange);
								c.close();
							}
							else if(routeChange.getString("Action").equalsIgnoreCase("DELETE")){
								deleteRoute.bindString(1, routeChange.getString("RouteNo"));
								deleteRoute.execute();
								deleteRoute.clearBindings();
							}
						}
					}
					db.setTransactionSuccessful();
					db.endTransaction();
					JSONArray stopChanges = object.getJSONArray("StopChanges");
					if(stopChanges != null && stopChanges != JSONObject.NULL){
						JSONObject stopChange;
						db.beginTransaction();
						for(int  i=0; i < routeChanges.length(); i++){
							stopChange = stopChanges.getJSONObject(i);
							if(stopChange.getString("Action").equalsIgnoreCase("DELETE")){
								deleteStops.bindLong(1, stopChange.getInt("StopNo"));
								deleteStops.execute();
								deleteStops.clearBindings();
							}
							else if(stopChange.getString("Action").equalsIgnoreCase("UPDATE")){
								alUpdateStops.add(new Integer(stopChange.getInt("StopNo")));
							}
						}
						db.setTransactionSuccessful();
						db.endTransaction();
					}
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
	public boolean createRouteViaUpdate(JSONObject route, JSONObject directions, JSONArray routeStops, ArrayList<Integer> alStops){
		boolean result = true;
		if(db == null){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
				return false;
			}
		}
		if(!db.isOpen()){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
				return false;
			}
		}
		try {
			String routeNo = route.getString("ID");
			SQLiteStatement insertRoutes = db.compileStatement("INSERT INTO routes (RouteNo, Description, UpDestination, DownDestination, HeadboardRouteNo, Colour, IsMainRoute) " +
					"VALUES (?,?,?,?,?,?,?)");
			db.beginTransaction();
			SQLiteStatement deleteRoute = db.compileStatement("DELETE FROM routes WHERE RouteNo = ?");
			SQLiteStatement deleteRoutesAndStops = db.compileStatement("DELETE FROM routesandstops WHERE RouteNo = ?");
			deleteRoute.bindString(1, routeNo);
			deleteRoute.execute();
			deleteRoute.clearBindings();
			deleteRoutesAndStops.bindString(1, routeNo);
			deleteRoutesAndStops.execute();
			deleteRoutesAndStops.clearBindings();
//			System.out.println("DELETE FROM routes WHERE RouteNo = " + routeNo);
//			System.out.println("DELETE FROM routesandstops WHERE RouteNo = " + routeNo);
			
			insertRoutes.bindString(1, routeNo);
			String upDestination = directions.getString("UpDestination");
			upDestination = upDestination.equalsIgnoreCase("null")?"":upDestination;
			String downDestination = directions.getString("DownDestination");
			downDestination = downDestination.equalsIgnoreCase("null")?"":downDestination;
			String description = upDestination;
			if(downDestination.equals("")){
				description = description + " - " + downDestination;			insertRoutes.bindString(2, upDestination + "-" + downDestination);
			}
			insertRoutes.bindString(2, description);
			insertRoutes.bindString(3, upDestination);
			insertRoutes.bindString(4, downDestination);
			insertRoutes.bindString(5, route.getString("HeadboardRouteNo"));
			insertRoutes.bindString(6, route.getString("Colour"));
			insertRoutes.bindLong(7, route.getBoolean("IsMainRoute")?1:0);
			insertRoutes.execute();
			insertRoutes.clearBindings();
//			System.out.println("INSERT INTO routes (RouteNo, Description, UpDestination, DownDestination, HeadboardRouteNo, Colour, IsMainRoute) " + route.getString("ID"));
			SQLiteStatement insertRouteStops = null;
			insertRouteStops = db.compileStatement("INSERT OR IGNORE INTO routesandstops (RouteNo, StopNo, StopSequence, IsUpDestination) VALUES (?,?,?,?)");
			SQLiteStatement insertTurns = null;
			insertTurns = db.compileStatement("INSERT OR IGNORE INTO turns (RouteNo, StopNo, StopSequence, TurnType, TurnMessage) VALUES (?,?,?,?,?)");
			SQLiteStatement insertStops = null;
			insertStops = db.compileStatement("INSERT OR IGNORE	INTO stops (StopNo, StopSuburbName) VALUES (?,?)");
			
			JSONObject routeStop;
			for(int i=0; i < routeStops.length();i++){
				routeStop = routeStops.getJSONObject(i);
				int stopNo = routeStop.getInt("StopNo");
				alStops.add(stopNo);
//				System.out.println("INSERT OR IGNORE INTO routesandstops " + routeNo + "," + stopNo + "," + routeStop.getInt("StopSequence") + "," + routeStop.getBoolean("UpStop"));
				insertRouteStops.bindString(1, routeNo);
				insertRouteStops.bindLong(2, stopNo);
				insertRouteStops.bindLong(3, routeStop.getInt("StopSequence"));
				insertRouteStops.bindLong(4, routeStop.getBoolean("UpStop")?1:0);
				insertRouteStops.execute();
				insertRouteStops.clearBindings();
				if(routeStop.get("TurnType") == JSONObject.NULL && routeStop.get("TurnMessage") == JSONObject.NULL){
				}
				else{
					insertTurns.bindString(1, routeNo);
					insertTurns.bindLong(2, stopNo);
					insertTurns.bindLong(3, routeStop.getInt("StopSequence"));
					insertTurns.bindString(4, routeStop.getString("TurnType"));
					insertTurns.bindString(5, routeStop.getString("TurnMessage"));
					insertTurns.execute();
					insertTurns.clearBindings();
//					System.out.println("INSERT OR IGNORE INTO turns , routeno = " + routeNo);
				}
				insertStops.bindLong(1, stopNo);
				insertStops.bindString(2, routeStop.getString("SuburbName"));
				insertStops.execute();
				insertStops.clearBindings();
//				System.out.println("INSERT OR IGNORE INTO stops , routeno = " + routeNo);
			}

			db.setTransactionSuccessful();
			db.endTransaction();
		} catch (JSONException e) {
			e.printStackTrace();
			db.endTransaction();
			result = false;
		}
		catch (SQLiteException e){
			e.printStackTrace();
			db.endTransaction();
			result = false;
		}
		return result;
	}
	
	
	public boolean deleteRouteViaUpdate(JSONObject object){
		boolean result = true;
		if(db == null){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
				return false;
			}
		}
		if(!db.isOpen()){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
				return false;
			}
		}
		try {
			SQLiteStatement deleteRoute = db.compileStatement("DELETE FROM routes WHERE RouteNo = ?");
			SQLiteStatement deleteRoutesAndStops = db.compileStatement("DELETE FROM routesandstops WHERE RouteNo = ?");
			db.beginTransaction();
			deleteRoute.bindString(1, object.getString("ID"));
			deleteRoute.execute();
			deleteRoute.clearBindings();
			deleteRoutesAndStops.bindString(1, object.getString("ID"));
			deleteRoutesAndStops.execute();
			deleteRoutesAndStops.clearBindings();
//			System.out.println("DELETE FROM routes WHERE RouteNo = " + object.getString("ID"));
//			System.out.println("DELETE FROM routesandstops WHERE RouteNo = " + object.getString("ID"));
			db.setTransactionSuccessful();
			db.endTransaction();

		} catch (JSONException e) {
			e.printStackTrace();
			result = false;
			db.endTransaction();
		}
		catch (SQLException	e) {
			e.printStackTrace();
			result = false;
			db.endTransaction();
		}
		return result;
	}
	
	
	public boolean updateTicketOutlets(JSONObject object){
		boolean result = true;
		if(db == null){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
				return false;
			}
		}
		if(!db.isOpen()){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
				return false;
			}
		}
		try {
			if(object.getBoolean("hasError") == false && object.getBoolean("hasResponse") == true){
				SQLiteStatement updateRoutes = db.compileStatement("UPDATE ticketoutlets SET Colour = ?, HeadboardRouteNo = ?, IsMainRoute=? WHERE RouteNo = ?");
				SQLiteStatement deleteRoute = db.compileStatement("DELETE FROM routes WHERE RouteNo = ?");
				SQLiteStatement deleteStops = db.compileStatement("DELETE FROM stops WHERE StopNo = ?");
				JSONObject responseObject = object.getJSONObject("responseObject");
				if(responseObject != null && responseObject != JSONObject.NULL){
					JSONArray routeChanges = object.getJSONArray("RouteChanges");
					db.beginTransaction();
					if(routeChanges != null){
						JSONObject routeChange;
						for(int  i=0; i < routeChanges.length(); i++){
							routeChange = routeChanges.getJSONObject(i);
							if(routeChange.getString("Action").equalsIgnoreCase("UPDATE")){
								updateRoutes.bindString(1, routeChange.getString("Colour"));
								updateRoutes.bindString(2, routeChange.getString("HeadboardRouteNo"));
								updateRoutes.bindLong(3, routeChange.getBoolean("IsMainRoute")?1:0);
								updateRoutes.bindString(4, routeChange.getString("RouteNo"));
								updateRoutes.execute();
							}
							else if(routeChange.getString("Action").equalsIgnoreCase("DELETE")){
								deleteRoute.bindString(1, routeChange.getString("RouteNo"));
								deleteRoute.execute();
							}
						}
					}
					db.setTransactionSuccessful();
					db.endTransaction();
					JSONArray stopChanges = object.getJSONArray("StopChanges");
					if(stopChanges != null){
						JSONObject stopChange;
						db.beginTransaction();
						for(int  i=0; i < routeChanges.length(); i++){
							stopChange = stopChanges.getJSONObject(i);
							if(stopChange.getString("Action").equalsIgnoreCase("DELETE")){
								deleteStops.bindLong(1, stopChange.getInt("StopNo"));
								deleteStops.execute();
								deleteStops.clearBindings();
							}
							else if(stopChange.getString("Action").equalsIgnoreCase("UPDATE")){
								
							}
						}
						db.setTransactionSuccessful();
						db.endTransaction();
					}
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
	public void createAllTables(){
		if(db == null){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		if(!db.isOpen()){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		db.execSQL("DROP TABLE IF EXISTS ticketoutlets");
		db.execSQL( "CREATE TABLE ticketoutlets (TicketOutletId INTEGER PRIMARY KEY, Name TEXT, Address TEXT, Suburb TEXT, HasMetcard INTEGER, HasMyki INTEGER, HasMykiTopUp INTEGER, " +
				"Is24Hour INTEGER, Latitude FLOAT, Longitude FLOAT);");
		db.execSQL("DROP TABLE IF EXISTS routes");
		db.execSQL( "CREATE TABLE routes (RouteNo INTEGER PRIMARY KEY, Description TEXT, UpDestination TEXT, DownDestination TEXT, HeadboardRouteNo TEXT, Colour TEXT, IsMainRoute INTEGER)");
		db.execSQL("DROP TABLE IF EXISTS routesandstops" );
		db.execSQL( "CREATE TABLE routesandstops (RouteNo INTEGER, StopNo INTEGER, StopSequence INTEGER, IsUpDestination INTEGER, PRIMARY KEY(RouteNo, StopNo, StopSequence, IsUpDestination))");
		db.execSQL("DROP TABLE IF EXISTS turns" );
		db.execSQL( "CREATE TABLE turns (RouteNo INTEGER, StopNo INTEGER, StopSequence INTEGER, TurnType TEXT, TurnMessage TEXT, PRIMARY KEY(RouteNo, StopNo, StopSequence))");
		db.execSQL("DROP TABLE IF EXISTS stops" );
		db.execSQL( "CREATE TABLE stops (StopNo INTEGER PRIMARY KEY, StopName TEXT, StopDescription TEXT, StopSuburbName TEXT, StopLatitude FLOAT, StopLongitude FLOAT " +
				", StopLength INTEGER, Zones TEXT, CityDirection TEXT, FlagStopNo TEXT, IsCityStop INTEGER, IsEasyAccess INTEGER, IsPlatFormStop INTEGER, IsShelter INTEGER, " +
				"IsYTShelter INTEGER)");
		db.execSQL("DROP TABLE IF EXISTS connections" );
		db.execSQL("CREATE TABLE connections (StopNo INTEGER PRIMARY KEY, ConnectingBuses TEXT, ConnectingTrains TEXT, ConnectingTrams TEXT)");
		db.execSQL("DROP TABLE IF EXISTS pois" );
		db.execSQL("CREATE TABLE pois (POIId INTEGER PRIMARY KEY, CategoryName TEXT, EmailAddress TEXT, HasDisabledAccess INTEGER, HasEntryFee INTEGER, HasToilets INTEGER, " +
				"Latitude FLOAT, Longitude FLOAT, MoreInfo TEXT, Name TEXT, OpeningHours TEXT, POIDescription TEXT, PhoneNumber TEXT, PostCode INTEGER, StreetAddress TEXT, " +
				"Suburb TEXT, WebAddress TEXT)");
		db.execSQL("DROP TABLE IF EXISTS poistops" );
		db.execSQL("CREATE TABLE poistops (POIId INTEGER, StopNo INTEGER, PRIMARY KEY(POIId, StopNo))");
		db.execSQL("DROP TABLE IF EXISTS stopsandnearbyoutlets" );
		db.execSQL("CREATE TABLE stopsandnearbyoutlets (StopNo INTEGER, TicketOutletId INTEGER, Distance INTEGER, PRIMARY KEY(StopNo, TicketOutletId))");
	}
	
	
	public void createFavouritesTables(){
		if(db == null){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		if(!db.isOpen()){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		db.execSQL( "CREATE TABLE IF NOT EXISTS favouritesandfavgroups (GroupName TEXT  NOT NULL , FavouriteName TEXT NOT NULL , DisplayOrder NOT NULL , " +
				"StopNo PRIMARY KEY NOT NULL UNIQUE)");
		db.execSQL( "CREATE TABLE IF NOT EXISTS favouritegroup (Name TEXT PRIMARY KEY NOT NULL UNIQUE , DisplayOrder)");
		db.execSQL( "CREATE TABLE IF NOT EXISTS favouritestoproutes (StopNo TEXT NOT NULL, RouteNo, PRIMARY KEY(StopNo, RouteNo)) ");
	}
	
	public boolean createTicketOutlets(JSONObject object){
		boolean result = true;
		try {
			if(!object.getBoolean("hasError")){
				String tableName = "ticketoutlets";
				
				if(db == null){
					try{
						openDataBase();
					}catch(SQLiteException sqlex){
						sqlex.printStackTrace();
						return false;
					}
				}
				if(!db.isOpen()){
					try{
						openDataBase();
					}catch(SQLiteException sqlex){
						sqlex.printStackTrace();
						return false;
					}
				}
				JSONArray response = object.getJSONArray("responseObject");
				if(response != JSONObject.NULL){
					JSONObject outlet;
					for(int i=0; i < response.length();i++){
						outlet = (JSONObject)response.get(i);
						ContentValues values = new ContentValues();
						values.put("TicketOutletId",outlet.getInt("TicketOutletId"));
						values.put("Name",outlet.getString("Name"));
						values.put("Address", outlet.getString("Address"));
						values.put("Suburb", outlet.getString("Suburb"));
						values.put("HasMetcard", outlet.getBoolean("HasMetcard")==true?1:0);
						values.put("HasMyki", outlet.getBoolean("HasMyki")==true?1:0);
						values.put("HasMykiTopUp", outlet.getBoolean("HasMykiTopUp")==true?1:0);
						values.put("Is24Hour", outlet.getBoolean("Is24Hour")==true?1:0);
						values.put("Latitude", outlet.getDouble("Latitude"));
						values.put("Longitude", outlet.getDouble("Longitude"));
						long col = db.insert(tableName, "Name", values);
						if(col == -1)
							result = false;
					}
				}
				else
					result = false;
			}
		} catch (JSONException e) {
			e.printStackTrace();
			result = false;
		}
		return result;
	}
	
	
	/**
	 * Used to create routes when using updates
	 * @param alObjects
	 * @param alRoutes List of RouteNo to add
	 * @return
	 */
	public boolean createRoutesFromUpdate(ArrayList<JSONObject> alRouteUpdateObjects, ArrayList<JSONObject> alRouteDirectionObjects){
		boolean result = true;
		JSONObject routeUpdate, routeDirection;
		String routeNo;
		SQLiteStatement insertRoute = db.compileStatement("INSERT INTO routes (RouteNo, Description, UpDestination, DownDestination, HeadboardRouteNo, Colour, IsMainRoute) " +
				"VALUES (?,?,?,?,?,?,?)");
		for(int j=0; j<alRouteUpdateObjects.size(); j++){
			routeUpdate = alRouteUpdateObjects.get(j);
			routeDirection = alRouteDirectionObjects.get(j);
			db.beginTransaction();
			try {

				if(db == null){
					try{
						openDataBase();
					}catch(SQLiteException sqlex){
						sqlex.printStackTrace();
						return false;
					}
				}
				if(!db.isOpen()){
					try{
						openDataBase();
					}catch(SQLiteException sqlex){
						sqlex.printStackTrace();
						return false;
					}
				}
				insertRoute.bindString(1, routeUpdate.getString("RouteNo"));
				//don't know where description can be found
				String upDestination = routeDirection.getString("UpDestination");
				upDestination = upDestination.equalsIgnoreCase("null")?"":upDestination;
				String downDestination = routeDirection.getString("DownDestination");
				downDestination = downDestination.equalsIgnoreCase("null")?"":downDestination;
				String description = upDestination;
				if(downDestination.equals(""))
					description = description + " - " + downDestination;
				insertRoute.bindString(2, description);
				insertRoute.bindString(3, upDestination);
				insertRoute.bindString(4, downDestination);
				//don't know where we get this from either
				insertRoute.bindLong(5, 0);
				insertRoute.bindString(6, routeUpdate.getString("HeadboardRouteNo"));
				insertRoute.bindString(7, routeUpdate.getString("Colour"));
				insertRoute.bindLong(8, routeUpdate.getBoolean("IsMainRoute")?1:0);
				insertRoute.execute();
				insertRoute.clearBindings();
			} catch (JSONException e) {
				e.printStackTrace();
				result = false;
			}
			db.setTransactionSuccessful();
			db.endTransaction();
		}
		return result;
	}
	
//	public boolean createRoutes(JSONObject object, ArrayList<String> alRoutes){
//		boolean result = true;
//		try {
//			if(!object.getBoolean("hasError")){
//				SQLiteStatement insertRouteStops = db.compileStatement("INSERT INTO routes (RouteNo, Description, UpDestination, DownDestination, HasLowFloor, HeadboardRouteNo, Colour, IsMainRoute) " +
//						"VALUES (?,?,?,?,?,?,?,?)");
//				if(db == null){
//					try{
//						openDataBase();
//					}catch(SQLiteException sqlex){
//						sqlex.printStackTrace();
//						return false;
//					}
//				}
//				if(!db.isOpen()){
//					try{
//						openDataBase();
//					}catch(SQLiteException sqlex){
//						sqlex.printStackTrace();
//						return false;
//					}
//				}
//				JSONArray response = object.getJSONArray("responseObject");
//				if(response != JSONObject.NULL){
//					JSONObject outlet;
//					db.beginTransaction();
//					for(int i=0; i < response.length();i++){
//						outlet = (JSONObject)response.get(i);
//						alRoutes.add(outlet.getString("RouteNo"));
//						insertRouteStops.bindString(1, outlet.getString("RouteNo"));
//						insertRouteStops.bindString(2, outlet.getString("Description"));
//						insertRouteStops.bindString(3, outlet.getString("UpDestination"));
//						insertRouteStops.bindString(4, outlet.getString("DownDestination"));
//						insertRouteStops.bindLong(5, outlet.getBoolean("HasLowFloor")?1:0);
//						insertRouteStops.bindString(6, outlet.getString("HeadBoardRouteNo"));
//						insertRouteStops.bindString(7, outlet.getString("RouteColour"));
//						insertRouteStops.bindString(8, outlet.getString("IsMainRoute"));
//						insertRouteStops.execute();
//						insertRouteStops.clearBindings();
//					}
//					db.setTransactionSuccessful();
//					db.endTransaction();
//				}
//				else
//					result = false;
//			}
//		} catch (JSONException e) {
//			e.printStackTrace();
//			result = false;
//		}
//		return result;
//	}
	
	public boolean createRoutes(ArrayList<JSONObject> alRouteObjects,ArrayList<JSONObject> alRouteDirections,ArrayList<String> alRoutes){
		boolean result = true;
		try {
			if(alRouteObjects != null && alRouteDirections != null && alRouteObjects.size() > 0){
				SQLiteStatement insertRouteStops = db.compileStatement("INSERT OR REPLACE INTO routes (RouteNo, Description, UpDestination, DownDestination, HeadboardRouteNo, Colour, IsMainRoute) " +
						"VALUES (?,?,?,?,?,?,?)");
				if(db == null){
					try{
						openDataBase();
					}catch(SQLiteException sqlex){
						sqlex.printStackTrace();
						return false;
					}
				}
				if(!db.isOpen()){
					try{
						openDataBase();
					}catch(SQLiteException sqlex){
						sqlex.printStackTrace();
						return false;
					}
				}
				JSONObject route, routeDirection, response;
				JSONArray responseArray;
				db.beginTransaction();
				for(int i=0; i< alRouteObjects.size(); i++){
					route = alRouteObjects.get(i);
					routeDirection = alRouteDirections.get(i);
					if(!routeDirection.getBoolean("hasError") && routeDirection.getBoolean("hasResponse")){
//						if(!routeDirection.has("responseObject"))
//						Log.e("responseObject", "routeObject = " + route + " routeDirection = " + routeDirection);
						responseArray = routeDirection.getJSONArray("responseObject");
						routeDirection = responseArray.getJSONObject(0);
						if(routeDirection != null && routeDirection != JSONObject.NULL){
							insertRouteStops.bindString(1, route.getString("ID"));
							alRoutes.add(route.getString("ID"));
							if(!routeDirection.has("UpDestination")){
//								Log.e("createRoutes","route.getString(\"ID\") = " + route.getString("ID"));
//								Log.e("createRoutes","routeDirection = " + routeDirection);
							}
							String upDestination = routeDirection.getString("UpDestination");
							upDestination = upDestination.equalsIgnoreCase("null")?"":upDestination;
							String downDestination = routeDirection.getString("DownDestination");
							downDestination = downDestination.equalsIgnoreCase("null")?"":downDestination;
							String description = upDestination;
							if(downDestination.equals(""))
								description = description + " - " + downDestination;
							insertRouteStops.bindString(2, description);
							insertRouteStops.bindString(3, upDestination);
							insertRouteStops.bindString(4, downDestination);
							insertRouteStops.bindString(5, route.getString("HeadboardRouteNo"));
							insertRouteStops.bindString(6, route.getString("Colour"));
							insertRouteStops.bindLong(7, route.getBoolean("IsMainRoute")?1:0);
							insertRouteStops.execute();
							insertRouteStops.clearBindings();
						}
					}
					else{
//						Log.e("createRoutes","routeDirection.getBoolean(\"hasError\") && routeDirection.getBoolean(\"hasResponse\")");
//						Log.e("createRoutes","routeDirection = " + routeDirection + " route = " + route);
					}
				}
				db.setTransactionSuccessful();
				db.endTransaction();
			}
		} catch (JSONException e) {
			e.printStackTrace();
			result = false;
//			Log.e("createRoutes","exception");
		}
		return result;
	}
	
//	public boolean createRoutesAndStops(JSONObject object, String routeNo, boolean isUpDirection, ArrayList<Integer> alStops){
//		boolean result = true;
//		try {
//			if(!object.getBoolean("hasError")){
//				if(db == null){
//					try{
//						openDataBase();
//					}catch(SQLiteException sqlex){
//						sqlex.printStackTrace();
//						return false;
//					}
//				}
//				if(!db.isOpen()){
//					try{
//						openDataBase();
//					}catch(SQLiteException sqlex){
//						sqlex.printStackTrace();
//						return false;
//					}
//				}
//				JSONArray response = object.getJSONArray("responseObject");
//				if(response != JSONObject.NULL){
//					JSONObject routestop;
//					db.beginTransaction();
//					try{
//						SQLiteStatement insertRouteStops = null;
//						insertRouteStops = db.compileStatement("INSERT INTO routesandstops (RouteNo, StopNo, StopSequence, IsUpDestination) VALUES (?,?,?,?)");
//						SQLiteStatement insertTurns = null;
//						insertTurns = db.compileStatement("INSERT OR IGNORE INTO turns (RouteNo, StopNo, StopSequence, TurnType, TurnMessage) VALUES (?,?,?,?,?)");
//						SQLiteStatement insertStops = null;
//						insertStops = db.compileStatement("INSERT OR IGNORE INTO stops (StopNo, StopSuburbName) VALUES (?,?)");
//
//						for(int i=0; i < response.length();i++){
//							routestop = (JSONObject)response.get(i);
//							int stopNo = routestop.getInt("StopNo");
//							alStops.add(stopNo);
//							insertRouteStops.bindString(1, routeNo);
//							insertRouteStops.bindLong(2, stopNo);
//							insertRouteStops.bindLong(3, i+1);
//							insertRouteStops.bindLong(4, isUpDirection?1:0);
//							insertRouteStops.execute();
//							insertRouteStops.clearBindings();
//							if(routestop.getString("TurnType") != JSONObject.NULL){
//								insertTurns.bindString(1, routeNo);
//								insertTurns.bindLong(2, stopNo);
//								insertTurns.bindLong(3, i+1);
//								insertTurns.bindString(4, routestop.getString("TurnType"));
//								insertTurns.bindString(5, routestop.getString("TurnMessage"));
//								insertTurns.execute();
//								insertTurns.clearBindings();
//							}
//							insertStops.bindLong(1, stopNo);
//							insertStops.bindString(2, routestop.getString("SuburbName"));
//							insertStops.execute();
//							insertStops.clearBindings();
//						}
//					}
//					catch (Exception e) {
//						System.out.println("Error bulk inserting routes and stops");
//						e.printStackTrace();
//					}
//					finally {
//						db.setTransactionSuccessful();
//						db.endTransaction();
//					}
//				}
//				else
//					result = false;
//			}
//		} catch (JSONException e) {
//			e.printStackTrace();
//			result = false;
//		}
//		return result;
//	}
	
	
	public boolean createRoutesAndStops(JSONObject object, String routeNo, ArrayList<Integer> alStops){
		boolean result = true;
		try {
			if(!object.getBoolean("hasError")){
				if(db == null){
					try{
						openDataBase();
					}catch(SQLiteException sqlex){
						sqlex.printStackTrace();
						return false;
					}
				}
				if(!db.isOpen()){
					try{
						openDataBase();
					}catch(SQLiteException sqlex){
						sqlex.printStackTrace();
						return false;
					}
				}
				
				JSONArray response = object.getJSONArray("responseObject");
				if(response != JSONObject.NULL){
					JSONObject routestop;
					db.beginTransaction();
					try{
						SQLiteStatement insertRouteStops = null;
						insertRouteStops = db.compileStatement("INSERT INTO routesandstops (RouteNo, StopNo, StopSequence, IsUpDestination) VALUES (?,?,?,?)");
						SQLiteStatement insertTurns = null;
						insertTurns = db.compileStatement("INSERT OR IGNORE INTO turns (RouteNo, StopNo, StopSequence, TurnType, TurnMessage) VALUES (?,?,?,?,?)");
						SQLiteStatement insertStops = null;
						insertStops = db.compileStatement("INSERT OR IGNORE INTO stops (StopNo, StopName, StopSuburbName) VALUES (?,?,?)");

						for(int i=0; i < response.length();i++){
							routestop = (JSONObject)response.get(i);
							int stopNo = routestop.getInt("StopNo");
							alStops.add(stopNo);
							insertRouteStops.bindString(1, routeNo);
							insertRouteStops.bindLong(2, stopNo);
							insertRouteStops.bindLong(3, routestop.getInt("StopSequence"));
							insertRouteStops.bindLong(4, routestop.getBoolean("UpStop")?1:0);
							insertRouteStops.execute();
							insertRouteStops.clearBindings();
							if(routestop.get("TurnType") == JSONObject.NULL && routestop.get("TurnMessage") == JSONObject.NULL){
								
							}
							else{
								insertTurns.bindString(1, routeNo);
								insertTurns.bindLong(2, stopNo);
								insertTurns.bindLong(3, routestop.getInt("StopSequence"));
								insertTurns.bindString(4, routestop.getString("TurnType"));
								insertTurns.bindString(5, routestop.getString("TurnMessage"));
								insertTurns.execute();
								insertTurns.clearBindings();
							}
							insertStops.bindLong(1, stopNo);
							insertStops.bindString(2, routestop.getString("StopName"));
							insertStops.bindString(3, routestop.getString("SuburbName"));
							insertStops.execute();
							insertStops.clearBindings();
						}
					}
					catch (Exception e) {
//						System.out.println("Error bulk inserting routes and stops");
						e.printStackTrace();
					}
					finally {
						db.setTransactionSuccessful();
						db.endTransaction();
					}
				}
				else
					result = false;
			}
		} catch (JSONException e) {
			e.printStackTrace();
			result = false;
		}
		return result;
	}
	
	public boolean updateStopsCreatedInitially(ArrayList<Integer> alStopNo,ArrayList<JSONObject> alObject){
		boolean result = true;
		if(alObject != null){
			JSONObject object;
			Integer stopNo;
			if(db == null){
				try{
					openDataBase();
				}catch(SQLiteException sqlex){
					sqlex.printStackTrace();
					return false;
				}
			}
			if(!db.isOpen()){
				try{
					openDataBase();
				}catch(SQLiteException sqlex){
					sqlex.printStackTrace();
					return false;
				}
			}
			db.beginTransaction();
			for(int i=0; i< alObject.size(); i++){
				object = alObject.get(i);
				stopNo = alStopNo.get(i);
				try {
					SQLiteStatement deleteStops = db.compileStatement("DELETE from stops WHERE StopNo = ?");
					SQLiteStatement deleteFromRoutesAndStops = db.compileStatement("DELETE from routesandstops WHERE StopNo = ?");
					if(!object.getBoolean("hasError")){
						//The stop object is being sent in an array
						if(!(object.get("responseObject") instanceof JSONArray)){
//							Log.e("TTDBUpdate updateStops error", "The stopNo = " + stopNo);
						}
						else{
							JSONArray stopArray = object.getJSONArray("responseObject");
							JSONObject stop = stopArray.getJSONObject(0);
							SQLiteStatement updateStops = db.compileStatement("UPDATE stops SET StopLatitude = ?, StopLongitude = ?, StopName = ?, CityDirection = ?, FlagStopNo = ?, " +
									"IsCityStop = ?, IsEasyAccess = ?, IsPlatFormStop = ?, IsShelter = ?, IsYTShelter = ?, StopLength = ?, Zones = ?, StopDescription = ? " +
									"WHERE StopNo = ?");
							SQLiteStatement insertConnections = db.compileStatement("INSERT OR IGNORE INTO connections (StopNo, ConnectingBuses, ConnectingTrams, ConnectingTrains) VALUES (?,?,?,?)");
							if(stop != JSONObject.NULL){
								updateStops.bindDouble(1,stop.getDouble("Latitude"));
								updateStops.bindDouble(2,stop.getDouble("Longitude"));
								updateStops.bindString(3, stop.getString("StopName"));
								updateStops.bindString(4, stop.getString("CityDirection"));
								updateStops.bindString(5, stop.getString("FlagStopNo"));
								updateStops.bindLong(6, stop.getBoolean("IsCityStop")?1:0);
								if(stop.get("IsEasyAccess") != JSONObject.NULL)
									updateStops.bindLong(7, stop.getBoolean("IsEasyAccess")?1:0);
								else
									updateStops.bindLong(7, 0);
								updateStops.bindLong(8, stop.getBoolean("IsPlatformStop")?1:0);
								if(stop.get("IsShelter") != JSONObject.NULL)
									updateStops.bindLong(9, stop.getBoolean("IsShelter")?1:0);
								else
									updateStops.bindLong(9, 0);
								if(stop.get("IsYTShelter") != JSONObject.NULL)
									updateStops.bindLong(10, stop.getBoolean("IsYTShelter")?1:0);
								else
									updateStops.bindLong(10, 0);
								updateStops.bindLong(11, stop.getLong("StopLength"));
								updateStops.bindString(12, stop.getString("Zones"));
								updateStops.bindString(13, "Stop " + stop.getString("FlagStopNo") + " " + stop.getString("StopName"));
								updateStops.bindLong(14, stopNo.intValue());
								updateStops.execute();
								updateStops.clearBindings();
								if(stop.getBoolean("HasConnectingBuses") || stop.getBoolean("HasConnectingTrains") || stop.getBoolean("HasConnectingTrams")){
									insertConnections.bindLong(1, stopNo.intValue());
									if(stop.getBoolean("HasConnectingBuses"))
										insertConnections.bindString(2, stop.getString("ConnectingBus"));
									else
										insertConnections.bindString(2, " ");
									if(stop.getBoolean("HasConnectingTrams"))
										insertConnections.bindString(3, stop.getString("ConnectingTrams"));
									else
										insertConnections.bindString(3, " ");
									if(stop.getBoolean("HasConnectingTrains"))
										insertConnections.bindString(4, stop.getString("ConnectingTrains"));
									else
										insertConnections.bindString(4, " ");
									insertConnections.execute();
									insertConnections.clearBindings();
								}

							}
							else{
								result = false;
								deleteStops.bindLong(1, stopNo);
								deleteStops.execute();
								deleteStops.clearBindings();
								deleteFromRoutesAndStops.bindLong(1, stopNo);
								deleteFromRoutesAndStops.execute();
								deleteFromRoutesAndStops.clearBindings();
							}
						}
					}
					else{
						deleteStops.bindLong(1, stopNo);
						deleteStops.execute();
						deleteStops.clearBindings();
						deleteFromRoutesAndStops.bindLong(1, stopNo);
						deleteFromRoutesAndStops.execute();
						deleteFromRoutesAndStops.clearBindings();
					}
					
				} catch (JSONException e) {
					e.printStackTrace();
					result = false;
				}
			}
			db.setTransactionSuccessful();
			db.endTransaction();
		}
		return result;
	}
	
	public boolean updateStopsViaUpdate(ArrayList<Integer> alUpdateStops, ArrayList<Integer> alDeleteStops, ArrayList<JSONArray> alObject){
		boolean result = true;
		if(alObject != null){
			Integer stopNo;
			if(db == null){
				try{
					openDataBase();
				}catch(SQLiteException sqlex){
					sqlex.printStackTrace();
					return false;
				}
			}
			if(!db.isOpen()){
				try{
					openDataBase();
				}catch(SQLiteException sqlex){
					sqlex.printStackTrace();
					return false;
				}
			}
			db.beginTransaction();
			for(int i=0; i< alObject.size(); i++){
				stopNo = alUpdateStops.get(i);
				try {
					JSONArray stopArray = alObject.get(i);
					JSONObject stop = stopArray.getJSONObject(0);
					SQLiteStatement updateStops = db.compileStatement("UPDATE stops SET StopLatitude = ?, StopLongitude = ?, StopName = ?, CityDirection = ?, FlagStopNo = ?, " +
							"IsCityStop = ?, IsEasyAccess = ?, IsPlatFormStop = ?, IsShelter = ?, IsYTShelter = ?, StopLength = ?, Zones = ?, StopDescription = ? " +
							"WHERE StopNo = ?");
					SQLiteStatement insertConnections = db.compileStatement("INSERT OR REPLACE INTO connections (StopNo, ConnectingBuses, ConnectingTrams, ConnectingTrains) VALUES (?,?,?,?)");
					if(stop != JSONObject.NULL){
						updateStops.bindDouble(1,stop.getDouble("Latitude"));
						updateStops.bindDouble(2,stop.getDouble("Longitude"));
						updateStops.bindString(3, stop.getString("StopName"));
						updateStops.bindString(4, stop.getString("CityDirection"));
						updateStops.bindString(5, stop.getString("FlagStopNo"));
						updateStops.bindLong(6, stop.getBoolean("IsCityStop")?1:0);
						updateStops.bindLong(7, stop.getBoolean("IsEasyAccess")?1:0);
						updateStops.bindLong(8, stop.getBoolean("IsPlatformStop")?1:0);
						updateStops.bindLong(9, stop.getBoolean("IsShelter")?1:0);
						updateStops.bindLong(10, stop.getBoolean("IsYTShelter")?1:0);
						updateStops.bindLong(11, stop.getLong("StopLength"));
						updateStops.bindString(12, stop.getString("Zones"));
						updateStops.bindString(13, "Stop " + stop.getString("FlagStopNo") + " " + stop.getString("StopName"));
						updateStops.bindLong(14, stopNo.intValue());
						updateStops.execute();
						updateStops.clearBindings();
//						System.out.println("updated Stop no = " + stopNo.intValue() + ", StopName = " + stop.getString("StopName"));
						if(stop.getBoolean("HasConnectingBuses") || stop.getBoolean("HasConnectingTrains") || stop.getBoolean("HasConnectingTrams")){
							insertConnections.bindLong(1, stopNo.intValue());
							if(stop.getBoolean("HasConnectingBuses"))
								insertConnections.bindString(2, stop.getString("ConnectingBus"));
							else
								insertConnections.bindString(2, " ");
							if(stop.getBoolean("HasConnectingTrams"))
								insertConnections.bindString(3, stop.getString("ConnectingTrams"));
							else
								insertConnections.bindString(3, " ");
							if(stop.getBoolean("HasConnectingTrains"))
								insertConnections.bindString(4, stop.getString("ConnectingTrains"));
							else
								insertConnections.bindString(4, " ");
							insertConnections.execute();
							insertConnections.clearBindings();
						}

					}
					else
						result = false;
				} catch (JSONException e) {
					e.printStackTrace();
					result = false;
				}
				catch (SQLiteException e) {
					db.endTransaction();
					e.printStackTrace();
					result = false;
				}
			}
			if(alDeleteStops.size() > 0){
				try{
					SQLiteStatement deleteStops = db.compileStatement("DELETE FROM stops " +
							"WHERE StopNo = ?");
					SQLiteStatement deleteConnections = db.compileStatement("DELETE FROM connections " +
							"WHERE StopNo = ?");
					SQLiteStatement deleteRoutesAndStops = db.compileStatement("DELETE FROM routesandstops " +
							"WHERE StopNo = ?");
					SQLiteStatement deleteFavouritesAndFavGroups = db.compileStatement("DELETE FROM favouritesandfavgroups " +
							"WHERE StopNo = ?");
					SQLiteStatement deleteFavouriteStopRoutes = db.compileStatement("DELETE FROM favouritestoproutes " +
							"WHERE StopNo = ?");
					SQLiteStatement deleteStopsAndNearbyOutlets = db.compileStatement("DELETE FROM stopsandnearbyoutlets " +
							"WHERE StopNo = ?");
					for(int i=0; i<alDeleteStops.size(); i++){
						stopNo = alDeleteStops.get(i);
						deleteStops.bindLong(1, stopNo);
						deleteStops.execute();
						deleteStops.clearBindings();
//						System.out.println("Deleted stops = " + stopNo);
						deleteConnections.bindLong(1, stopNo);
						deleteConnections.execute();
						deleteConnections.clearBindings();
//						System.out.println("Deleted connections = " + stopNo);
						deleteRoutesAndStops.bindLong(1, stopNo);
						deleteRoutesAndStops.execute();
						deleteRoutesAndStops.clearBindings();
//						System.out.println("Deleted RoutesAndStops = " + stopNo);
						deleteFavouritesAndFavGroups.bindLong(1, stopNo);
						deleteFavouritesAndFavGroups.execute();
						deleteFavouritesAndFavGroups.clearBindings();
//						System.out.println("Deleted Favourites And FavGroups = " + stopNo);
						deleteFavouriteStopRoutes.bindLong(1, stopNo);
						deleteFavouriteStopRoutes.execute();
						deleteFavouriteStopRoutes.clearBindings();
//						System.out.println("Deleted Favourite Stop Routes = " + stopNo);
						deleteStopsAndNearbyOutlets.bindLong(1, stopNo);
						deleteStopsAndNearbyOutlets.execute();
						deleteStopsAndNearbyOutlets.clearBindings();
//						System.out.println("Deleted Stops and NearbyOutlets = " + stopNo);
					}
				}catch (SQLiteException e) {
					db.endTransaction();
					e.printStackTrace();
					result = false;
				}
			}
			db.setTransactionSuccessful();
			db.endTransaction();
		}
		return result;
	}
	
	
	public boolean updateTicketOutletsViaUpdate(ArrayList<Integer> alUpdateOutletId, ArrayList<Integer> alDeleteOutletId, ArrayList<JSONObject> alObject){
		boolean result = true;
		if(alObject != null){
			JSONObject object;
			Integer outletId;
			if(db == null){
				try{
					openDataBase();
				}catch(SQLiteException sqlex){
					sqlex.printStackTrace();
					return false;
				}
			}
			if(!db.isOpen()){
				try{
					openDataBase();
				}catch(SQLiteException sqlex){
					sqlex.printStackTrace();
					return false;
				}
			}
			db.beginTransaction();
			for(int i=0; i< alObject.size(); i++){
				object = alObject.get(i);
				outletId = alUpdateOutletId.get(i);
				try {
					if(!object.getBoolean("hasError")){
						//The stop object is being sent in an array
						if(!(object.get("responseObject") instanceof JSONArray)){
//							Log.e("TTDBUpdate updateTicketOutlets error", " outletId = " + outletId);
						}
						else{
							JSONArray arTicketOutlet = object.getJSONArray("responseObject");
							if(arTicketOutlet != null && arTicketOutlet.length() > 0){
								JSONObject ticketOutlet = arTicketOutlet.getJSONObject(0);
								SQLiteStatement updateTicketOutlets = db.compileStatement("INSERT OR REPLACE INTO ticketoutlets (TicketOutletId, Name, Address, Suburb, HasMetcard, " +
										"HasMyki, HasMykiTopUp, Is24Hour, Latitude, Longitude) VALUES (?,?,?,?,?,?,?,?,?,?)");
								if(ticketOutlet != JSONObject.NULL){
									updateTicketOutlets.bindDouble(1,outletId);
									updateTicketOutlets.bindString(2,ticketOutlet.getString("Name"));
									updateTicketOutlets.bindString(3, ticketOutlet.getString("Address"));
									updateTicketOutlets.bindString(4, ticketOutlet.getString("Suburb"));
									updateTicketOutlets.bindLong(5, ticketOutlet.getBoolean("HasMetcard")==true?1:0);
									updateTicketOutlets.bindLong(6, ticketOutlet.getBoolean("HasMyki")==true?1:0);
									updateTicketOutlets.bindLong(7, ticketOutlet.getBoolean("HasMykiTopUp")==true?1:0);
									updateTicketOutlets.bindLong(8, ticketOutlet.getBoolean("Is24Hour")==true?1:0);
									updateTicketOutlets.bindDouble(9, ticketOutlet.getDouble("Latitude"));
									updateTicketOutlets.bindDouble(10, ticketOutlet.getDouble("Longitude"));
									updateTicketOutlets.execute();
									updateTicketOutlets.clearBindings();
//									System.out.println("Outletid = " + outletId + ", name = " + ticketOutlet.getString("Name"));
								}
								else{
									result = false;
									return result;
								}
							}
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
					result = false;
					db.endTransaction();
					return result;
				}
			}
			try{
			SQLiteStatement deleteTicketOutlets = db.compileStatement("DELETE FROM ticketoutlets WHERE TicketOutletId=?");
			SQLiteStatement deleteStopsAndNearbyOutlets = db.compileStatement("DELETE FROM stopsandnearbyoutlets WHERE TicketOutletId=?");
			Integer ticketOutletId;
			for(int i=0; i< alDeleteOutletId.size(); i++){
				ticketOutletId = alDeleteOutletId.get(i);
				deleteTicketOutlets.bindLong(1, ticketOutletId);
				deleteTicketOutlets.execute();
				deleteTicketOutlets.clearBindings();
//				System.out.println("Deleted ticket outlet " + ticketOutletId);
				deleteStopsAndNearbyOutlets.bindLong(1, ticketOutletId);
				deleteStopsAndNearbyOutlets.execute();
				deleteStopsAndNearbyOutlets.clearBindings();
//				System.out.println("Deleted from stopsandnearbyoutlets " + ticketOutletId);
			}
			} catch (SQLiteException e) {
				result = false;
				db.endTransaction();
				return result;
			}
			db.setTransactionSuccessful();
			db.endTransaction();
		}
		return result;
	}
	
	public boolean updatePOIsViaUpdate(ArrayList<Integer> alUpdatePoisId, ArrayList<Integer> alDeletePoisId, ArrayList<JSONObject> alPoiObject, ArrayList<JSONObject> alPoiStops){
		boolean result = true;
		if(alPoiObject != null){
			JSONObject object, poiStop, poiStopElement;
			JSONArray poiStopArray;
			Integer poiId;
			if(db == null){
				try{
					openDataBase();
				}catch(SQLiteException sqlex){
					sqlex.printStackTrace();
					return false;
				}
			}
			if(!db.isOpen()){
				try{
					openDataBase();
				}catch(SQLiteException sqlex){
					sqlex.printStackTrace();
					return false;
				}
			}
			db.beginTransaction();
			for(int i=0; i< alPoiObject.size(); i++){
				object = alPoiObject.get(i);
				poiId = alUpdatePoisId.get(i);
				poiStop = alPoiStops.get(i);
				try {
					if(!object.getBoolean("hasError") && object.getBoolean("hasResponse")){
						//The stop object is being sent in an array
						if(!(object.get("responseObject") instanceof JSONArray)){
//							Log.e("TTDBUpdate updatePOIs error", " poiId = " + poiId);
						}
						else{
							JSONArray arPois = object.getJSONArray("responseObject");
							if(arPois != null && arPois.length() > 0){
								JSONObject poi = arPois.getJSONObject(0);
								SQLiteStatement updatePoi = db.compileStatement("INSERT OR REPLACE INTO pois (POIId, CategoryName, EmailAddress, HasDisabledAccess, HasEntryFee, HasToilets, Latitude, Longitude, " +
										"MoreInfo, Name, OpeningHours, POIDescription, PhoneNumber, PostCode, StreetAddress, Suburb, WebAddress) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
								SQLiteStatement updatePoiStops = db.compileStatement("INSERT OR REPLACE INTO poistops (POIId, StopNo) VALUES (?,?)");
								if(poi != JSONObject.NULL){
									updatePoi.bindDouble(1,poiId);
									updatePoi.bindString(2,poi.getString("CategoryName"));
									updatePoi.bindString(3, poi.getString("EmailAddress"));
									updatePoi.bindLong(4, poi.getBoolean("DisabledAccess")?1:0);
									updatePoi.bindLong(5, poi.getBoolean("HasEntryFee")?1:0);
									updatePoi.bindLong(6, poi.getBoolean("HasToilets")?1:0);
									updatePoi.bindDouble(7, poi.getDouble("Latitude"));
									updatePoi.bindDouble(8, poi.getDouble("Longitude"));
									updatePoi.bindString(9, poi.getString("MoreInfo"));
									updatePoi.bindString(10, poi.getString("Name"));
									updatePoi.bindString(11, poi.getString("OpeningHours"));
									updatePoi.bindString(12, poi.getString("POIDescription"));
									updatePoi.bindString(13, poi.getString("PhoneNumber"));
									updatePoi.bindLong(14, poi.getInt("Postcode"));
									updatePoi.bindString(15, poi.getString("StreetAddress"));
									updatePoi.bindString(16, poi.getString("Suburb"));
									updatePoi.bindString(17, poi.getString("WebAddress"));
									updatePoi.execute();
									updatePoi.clearBindings();
//									System.out.println("INSERT OR REPLACE INTO pois = " + poiId + "Name" + poi.getString("Name"));
									poiStopArray = poiStop.getJSONArray("responseObject");
									if(poiStopArray.length() > 0){
										for(int j=0; j<poiStopArray.length(); j++){
											poiStopElement = poiStopArray.getJSONObject(j);
											updatePoiStops.bindLong(1, poiId);
											updatePoiStops.bindLong(2, poiStopElement.getInt("StopNo"));
											updatePoiStops.execute();
											updatePoiStops.clearBindings();
//											System.out.println("INSERT OR REPLACE INTO poistops = " + poiId + ", stop" + poiStopElement.getInt("StopNo"));
										}
									}
								}
								else{
									result = false;
									db.endTransaction();
									return result;
								}	
							}
							else{
								result = false;
								db.endTransaction();
								return result;
							}	
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
					result = false;
					return result;
				} catch (SQLiteException e) {
					e.printStackTrace();
					result = false;
					return result;
				}
			}
			SQLiteStatement deletePOI = db.compileStatement("DELETE FROM pois WHERE POIId=?");
			SQLiteStatement deletePOIStops = db.compileStatement("DELETE FROM poistops WHERE POIId=?");
			for(int i=0; i< alDeletePoisId.size(); i++){
				poiId = alDeletePoisId.get(i);
				deletePOI.bindLong(1, poiId);
				deletePOI.execute();
				deletePOI.clearBindings();
//				System.out.println("Delete poiid " + poiId);
				deletePOIStops.bindLong(1, poiId);
				deletePOIStops.execute();
				deletePOIStops.clearBindings();
			}
			db.setTransactionSuccessful();
			db.endTransaction();
		}
		return result;
	}
	
	public boolean createPOI(JSONObject object, TTWebService webservice){
		boolean result = true;
		try {
			if(!object.getBoolean("hasError")){
				if(db == null){
					try{
						openDataBase();
					}catch(SQLiteException sqlex){
						sqlex.printStackTrace();
						return false;
					}
				}
				if(!db.isOpen()){
					try{
						openDataBase();
					}catch(SQLiteException sqlex){
						sqlex.printStackTrace();
						return false;
					}
				}
				JSONArray response = object.getJSONArray("responseObject");
				JSONObject poiStops;
				if(response != JSONObject.NULL){
					JSONObject poi;
					db.beginTransaction();
					try{
						SQLiteStatement insertPoi = null;
						insertPoi = db.compileStatement("INSERT OR REPLACE INTO pois (POIId, CategoryName, EmailAddress, HasDisabledAccess, HasEntryFee, HasToilets, Latitude, Longitude, MoreInfo, " +
								"Name, OpeningHours, POIDescription, PhoneNumber, PostCode, StreetAddress, Suburb, WebAddress) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
						SQLiteStatement insertPoiStops = db.compileStatement("INSERT OR REPLACE INTO poistops (POIId, StopNo) VALUES (?,?)");
						for(int i=0; i < response.length();i++){
							poi = (JSONObject)response.get(i);
							insertPoi.bindLong(1, poi.getInt("POIId"));
							insertPoi.bindString(2, poi.getString("CategoryName"));
							insertPoi.bindString(3, poi.getString("EmailAddress"));
							insertPoi.bindLong(4, poi.getBoolean("DisabledAccess")?1:0);
							insertPoi.bindLong(5, poi.getBoolean("HasEntryFee")?1:0);
							insertPoi.bindLong(6, poi.getBoolean("HasToilets")?1:0);
							insertPoi.bindDouble(7, poi.getDouble("Latitude"));
							insertPoi.bindDouble(8, poi.getDouble("Longitude"));
							insertPoi.bindString(9, poi.getString("MoreInfo"));
							insertPoi.bindString(10, poi.getString("Name"));
							insertPoi.bindString(11, poi.getString("OpeningHours"));
							insertPoi.bindString(12, poi.getString("POIDescription"));
							insertPoi.bindString(13, poi.getString("PhoneNumber"));
							if(!(poi.get("Postcode") instanceof Integer) && poi.get("Postcode") != JSONObject.NULL){
								Log.d("Insert POI", "POIId = " + poi.getInt("POIId") + " does not have a integer postcode");
							}
							if(poi.get("Postcode") != JSONObject.NULL)
								insertPoi.bindLong(14, poi.getInt("Postcode"));
							else
								insertPoi.bindLong(14, 0);
							insertPoi.bindString(15, poi.getString("StreetAddress"));
							insertPoi.bindString(16, poi.getString("Suburb"));
							insertPoi.bindString(17, poi.getString("WebAddress"));
							insertPoi.execute();
							insertPoi.clearBindings();
							poiStops = webservice.getStopsForPoiId(poi.getString("POIId"));
							if(poiStops == null || poiStops.getBoolean("hasError") || !poiStops.getBoolean("hasResponse")){
								result = false;
//								Log.e("createPOI","Error getting stops for poi");
							}
							else{
								JSONArray poiStopsArray = poiStops.getJSONArray("responseObject");
								for(int j=0;j < poiStopsArray.length();j++){
									JSONObject poiStop = poiStopsArray.getJSONObject(j);
									insertPoiStops.bindLong(1, poi.getInt("POIId"));
									insertPoiStops.bindLong(2, poiStop.getInt("StopNo"));
									insertPoiStops.execute();
									insertPoiStops.clearBindings();
								}
							}
						}
					}
					catch (Exception e) {
//						System.out.println("Error bulk inserting pois");
						e.printStackTrace();
					}
					finally {
						db.setTransactionSuccessful();
						db.endTransaction();
					}
				}
				else
					result = false;
			}
		} catch (JSONException e) {
			e.printStackTrace();
			result = false;
		}
		return result;
	}
	
	
	public boolean createStopsAndNearbyOutlets(){
		boolean result = false;
		if(db == null){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		if(!db.isOpen()){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}

		try{
//			db.execSQL("DROP TABLE IF EXISTS stopsandnearbyoutlets" );
//			db.execSQL("CREATE TABLE stopsandnearbyoutlets (StopNo INTEGER, TicketOutletId INTEGER, Distance INTEGER, PRIMARY KEY(StopNo, TicketOutletId))");			
			db.beginTransaction();
			SQLiteStatement insertStopsAndNearbyOutlets = db.compileStatement("INSERT OR REPLACE INTO stopsandnearbyoutlets (StopNo, TicketOutletId, Distance) VALUES (?,?,?)");
			Location location = new Location("");
			Cursor c = db.query("stops", 
					new String[]{"StopNo", "StopLatitude","StopLongitude"}, 
					null, 
					null, 
					null, 
					null, 
					null, 
					null);

			if (c.moveToFirst()) {		
				do {
					Integer stopNo = c.getInt(c.getColumnIndex("StopNo"));
					location.setLatitude(c.getFloat(c.getColumnIndex("StopLatitude")));
					location.setLongitude(c.getFloat(c.getColumnIndex("StopLongitude")));
					ArrayList<NearbyTicketOutlet> alOutlets =  getNearbyTicketOutlets(location);
					if(alOutlets != null && alOutlets.size() > 0){
						for(int i=0; i<alOutlets.size() && i<2; i++){
							insertStopsAndNearbyOutlets.bindLong(1,stopNo.intValue());
							insertStopsAndNearbyOutlets.bindLong(2, alOutlets.get(i).getTicketOutlet().getOutletId());
							insertStopsAndNearbyOutlets.bindLong(3, alOutlets.get(i).getIntDistance());
							insertStopsAndNearbyOutlets.execute();
							insertStopsAndNearbyOutlets.clearBindings();
//							System.out.println("insertStopsAndNearbyOutlets StopNo=" + c.getInt(c.getColumnIndex("StopNo"))
//									+ ", Outlet Id = " + alOutlets.get(i).getTicketOutlet().getOutletId());
						}
					}
				} while(c.moveToNext());
			}
			c.close();
			db.setTransactionSuccessful();
			db.endTransaction();
		} catch (Exception e) {
		}
		return result;
	}
	
	
	public boolean createStopsAndNearbyOutletsViaUpdate(ArrayList<Integer> alOutlets){
		boolean result = false;
		if(db == null){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		if(!db.isOpen()){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}

		try{
			db.beginTransaction();
			SQLiteStatement insertStopsAndNearbyOutlets = db.compileStatement("INSERT OR REPLACE INTO stopsandnearbyoutlets (StopNo, TicketOutletId, Distance) VALUES (?,?,?)");
			Location locStop = new Location("");
			Location locTO = new Location("");
			int distance1=0, distance2=0;
			int stop1=0, stop2=0;
			Cursor cursorStop = db.query("stops", 
					new String[]{"StopNo", "StopLatitude","StopLongitude"}, 
					null, 
					null, 
					null, 
					null, 
					null, 
					null);
			if(alOutlets != null && alOutlets.size() > 0){
				for(int i=0; i<alOutlets.size(); i++){
					Cursor cursorTO = db.query("ticketoutlets", 
							new String[]{"Latitude","Longitude"}, 
							"TicketOutletId = " + alOutlets.get(i), 
							null, 
							null, 
							null, 
							null, 
							null);
					if(cursorTO.moveToFirst()){
						locTO.setLatitude(cursorTO.getDouble(0));
						locTO.setLongitude(cursorTO.getDouble(1));
						stop1 = 0;
						stop2 = 0;
						distance1 = 0;
						distance2 = 0;
						if (cursorStop.moveToFirst()) {		
							do {
								Integer stopNo = cursorStop.getInt(cursorStop.getColumnIndex("StopNo"));
								locStop.setLatitude(cursorStop.getFloat(cursorStop.getColumnIndex("StopLatitude")));
								locStop.setLongitude(cursorStop.getFloat(cursorStop.getColumnIndex("StopLongitude")));
								if(locTO.distanceTo(locStop) < 500){
									if(stop1 == 0){
										distance1 = (int)locTO.distanceTo(locStop);
										stop1 = stopNo;
									}
									else{
										if(stop2 == 0){
											distance2 = (int)locTO.distanceTo(locStop);
											stop2 = stopNo;											
										}
										else{
											if(locTO.distanceTo(locStop) <= distance1){
												distance1 = (int)locTO.distanceTo(locStop);
												stop1 = stopNo;
											}
											else{
												if(locTO.distanceTo(locStop) <= distance2){
													distance2 = (int)locTO.distanceTo(locStop);
													stop2 = stopNo;
												}
											}
										}
									}
								}

							} while(cursorStop.moveToNext());
						}
						if(stop1 > 0 || stop2 > 0){
							if(stop1 > 0 && distance1 < 500){
								insertStopsAndNearbyOutlets.bindLong(1,stop1);
								insertStopsAndNearbyOutlets.bindLong(2, alOutlets.get(i));
								insertStopsAndNearbyOutlets.bindLong(3, distance1);
								insertStopsAndNearbyOutlets.execute();
								insertStopsAndNearbyOutlets.clearBindings();
//								System.out.println("stop1 inserted.");
							}
							if(stop2 > 0 && stop2 != stop1 && distance2 < 500){
								insertStopsAndNearbyOutlets.bindLong(1,stop2);
								insertStopsAndNearbyOutlets.bindLong(2, alOutlets.get(i));
								insertStopsAndNearbyOutlets.bindLong(3, distance2);
								insertStopsAndNearbyOutlets.execute();
								insertStopsAndNearbyOutlets.clearBindings();
//								System.out.println("stop2 inserted.");
							}
						}
					}
					cursorTO.close();
				}
			}
			cursorStop.close();
			db.setTransactionSuccessful();
			db.endTransaction();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
	public boolean createStopsAndNearbyOutletsTake2(){
		boolean result = false;
		int ticketOutletId1 = 0, ticketOutletId2= 0;
		int distance1 = 0, distance2 = 0;
		
		NearbyTicketOutlet to2 = new NearbyTicketOutlet();
		if(db == null){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		if(!db.isOpen()){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}

		try{
			db.execSQL("DROP TABLE IF EXISTS stopsandnearbyoutlets" );
			db.execSQL("CREATE TABLE IF NOT EXISTS stopsandnearbyoutlets (StopNo INTEGER, TicketOutletId INTEGER, Distance INTEGER)");
			db.beginTransaction();
			SQLiteStatement insertStopsAndNearbyOutlets = db.compileStatement("INSERT OR REPLACE INTO stopsandnearbyoutlets (StopNo, TicketOutletId, Distance) VALUES (?,?)");
			Location location = new Location("");
			Cursor stops = db.query("stops", 
					new String[]{"StopNo", "StopLatitude","StopLongitude"}, 
					null, 
					null, 
					null, 
					null, 
					null, 
					null);
			Cursor outlets = db.query("ticketoutlets", 
					new String[]{"TicketOutletId", "Latitude","Longitude"}, 
					null, 
					null, 
					null, 
					null, 
					null, 
					null);

			if (stops.moveToFirst()) {		
				do {
//					if(ticketOutletId1)
//					Integer stopNo = stops.getInt(stops.getColumnIndex("StopNo"));
					location.setLatitude(stops.getFloat(stops.getColumnIndex("StopLatitude")));
					location.setLongitude(stops.getFloat(stops.getColumnIndex("StopLongitude")));
					if(outlets.moveToFirst()){
						do{
							
						} while(outlets.moveToNext());
					}
//							insertStopsAndNearbyOutlets.bindLong(1,stopNo.intValue());
//							insertStopsAndNearbyOutlets.bindLong(2, alOutlets.get(i).getTicketOutlet().getOutletId());
//							insertStopsAndNearbyOutlets.execute();
//							insertStopsAndNearbyOutlets.clearBindings();
//							System.out.println("insertStopsAndNearbyOutlets StopNo=" + stops.getInt(stops.getColumnIndex("StopNo"))
//									+ ", Outlet Id = " + alOutlets.get(i).getTicketOutlet().getOutletId());
				} while(stops.moveToNext());
				stops.close();
				outlets.close();
			}
			db.setTransactionSuccessful();
			db.endTransaction();
		} catch (Exception e) {
		}
		return result;
	}
	
	
	
	/**
	 * Get a list of nearest outlets
	 * @param Location
	 * @return ArrayList<NearbyTicketOutlet>
	 */
	public ArrayList<NearbyTicketOutlet> getNearbyTicketOutlets(Location location) {
		String tableOutlets = "ticketoutlets";
		ArrayList<NearbyTicketOutlet> outlets = new ArrayList<NearbyTicketOutlet>();
		if(db == null){
			openDataBase();
		}
		if(!db.isOpen()){
			openDataBase(); 
		}
		Cursor c = db.query(tableOutlets, 
							null, 
							null, 
							null, 
							null, 
							null, 
							null, 
							null);
	
		if (c.moveToFirst()) {		
			do {
				NearbyTicketOutlet nto = getNearbyOutletFromCursor(c, location);
				NearbyTicketOutlet temp;
				int i=0;
				if(nto != null){
					for(i=0; i< outlets.size(); i++){
						temp = outlets.get(i);
						if(nto.getIntDistance() < temp.getIntDistance()){
							break;
						}
					}
					outlets.add(i, nto);
				}
			} while(c.moveToNext());
		}
		c.close();
//		Log.e("getNearbyTicketOutlets", "outlets = " + outlets);
		return outlets;
	}	
	
	/**
	 * Build a NearbyTicketOutlet object from a given cursor
	 * @param Cursor,Location 
	 * @return NearbyTicketOutlet (consisting of distance and TicketOutlet objects)
	 */
	private NearbyTicketOutlet getNearbyOutletFromCursor(Cursor c, Location location) {
		int colLatitude = c.getColumnIndexOrThrow("Latitude");
		int colLongitude = c.getColumnIndexOrThrow("Longitude");
		Location l2 = new Location("");
		l2.setLatitude(c.getDouble(colLatitude));
		l2.setLongitude(c.getDouble(colLongitude));
		int distance = (int)Math.abs(l2.distanceTo(location));
		if(distance < Constants.kNearestDistance){
			int colName = c.getColumnIndexOrThrow("Name");
			int colAddress = c.getColumnIndexOrThrow("Address");
			int colSuburbName = c.getColumnIndexOrThrow("Suburb");
			int colHasMetcard = c.getColumnIndexOrThrow("HasMetcard");
			int colHasMyki = c.getColumnIndexOrThrow("HasMyki");
			int colHasMykiTopup = c.getColumnIndexOrThrow("HasMykiTopUp");
			int colHas24Hour = c.getColumnIndexOrThrow("Is24Hour");
			int colOutletId = c.getColumnIndexOrThrow("TicketOutletId");

			TicketOutlet outlet = new TicketOutlet();
			outlet.setName(c.getString(colName));
			outlet.setAddress(c.getString(colAddress));
			outlet.setLatitudeE6((int)(c.getDouble(colLatitude) * 1E6));
			outlet.setLongitudeE6((int)(c.getDouble(colLongitude) * 1E6));
			outlet.setSuburb(c.getString(colSuburbName));
			if(c.getInt(colHasMetcard) == 1)
				outlet.setHasMetcard(true);
			else
				outlet.setHasMetcard(false);
			if(c.getInt(colHasMyki) == 1)
				outlet.setHasMykiCard(true);
			else
				outlet.setHasMykiCard(false);
			if(c.getInt(colHasMykiTopup) == 1)
				outlet.setHasMykiTopUp(true);
			else
				outlet.setHasMykiTopUp(false);
			
			if(c.getInt(colHas24Hour) == 1)
				outlet.setIs24Hours(true);
			else
				outlet.setIs24Hours(false);
			outlet.setOutletId(c.getInt(colOutletId));
			if(c.getInt(colHasMykiTopup) == 1)
				outlet.setHasMykiTopUp(true);
			else
				outlet.setHasMykiTopUp(false);
			NearbyTicketOutlet nearby = new NearbyTicketOutlet(outlet, distance);
			return nearby;
		}
		return null;
	}	
	
	public int getInternalRouteNoForRouteNo(String routeNo){
		int intRouteNo = 0;
		if(db == null){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		if(!db.isOpen()){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		if(db.isOpen()){
			try{
				Cursor c = db.query("routes", 
						new String[]{"RouteNo"},
						"HeadboardRouteNo="+routeNo,
						null,
						null, 
						null, 
						null, 
						null);	
				if(c.getCount() > 0){
					if(c.moveToFirst()){
						do{
							intRouteNo = c.getInt(0);
						}while(c.moveToNext());
					}
				}
				c.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return intRouteNo;
	}
}
