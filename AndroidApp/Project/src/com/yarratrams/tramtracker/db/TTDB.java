package com.yarratrams.tramtracker.db;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.Constants;
import com.yarratrams.tramtracker.objects.Favourite;
import com.yarratrams.tramtracker.objects.FavouritesGroup;
import com.yarratrams.tramtracker.objects.NearbyFavourite;
import com.yarratrams.tramtracker.objects.NearbyStop;
import com.yarratrams.tramtracker.objects.NearbyStopsLimitedByNoOfStops;
import com.yarratrams.tramtracker.objects.NearbyStopsLimitedByNoOfStopsGivenLocation;
import com.yarratrams.tramtracker.objects.NearbyTicketOutlet;
import com.yarratrams.tramtracker.objects.PointOfInterest;
import com.yarratrams.tramtracker.objects.Route;
import com.yarratrams.tramtracker.objects.SearchResult;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.objects.DistanceAndId;
import com.yarratrams.tramtracker.objects.TicketOutlet;
import com.yarratrams.tramtracker.objects.TicketOutletBySuburb;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

public class TTDB extends SQLiteOpenHelper  {

    
	private SQLiteDatabase db;
	private Context context;
	
	public TTDB(Context myContext) {
		super(myContext, Constants.kDBName, null, 1);
		context = myContext;
		try {
			createDataBase();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void onCreate(SQLiteDatabase arg0) {
		
	}

	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
	}
	
	  /**
     * Creates a empty database on the system and rewrites it with your own database.
     * */
    public void createDataBase() throws IOException{
 
    	boolean dbExist = checkDataBase();
 
    	if(dbExist){
    		//do nothing - database already exist
    	}else{
 
    		//By calling this method and empty database will be created into the default system path
               //of your application so we are going be able to overwrite that database with our database.
        	this.getReadableDatabase();
        	this.close();
   			constructNewDatabaseFromResources();
   			this.close();
    	}
 
    }
    
    /**
     * Check if the database already exists to prevent re-copying the file each time you open the application.
     * @return true if db exists, false if it doesn't
     */
    private boolean checkDataBase(){
 
    	SQLiteDatabase checkDB = null;
 
    	try{
    		String myPath = Constants.kDBLocalPath + Constants.kDBName;
    		checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
 
    	}catch(SQLiteException e){
 
    		//database does't exist yet.
 
    	}
 
    	if(checkDB != null){
 
    		checkDB.close();
 
    	}
 
    	return checkDB != null ? true : false;
    }
	
    public void openDataBase() throws SQLException{
    	 
    	//Open the database
        String myPath = Constants.kDBLocalPath + Constants.kDBName;
    	db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
//    	db.close();
//    	db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
    }
 
    @Override
	public synchronized void close() {
 
    	    if(db != null)
    		    db.close();
 
    	    super.close();
 
	}

    /*
     * Takes split database (due to size limitation from raw directory) and combines them to the dataabse file.
     */
    
    public void constructNewDatabaseFromResources()
    {
    	//Log.v("DataBase Installation", "Before creating files");
    	int resourceList[] = new int[] {
    			R.raw.xaa,
    			R.raw.xab
    	};
//    	int resourceList[] = new int[] {
//    			R.raw.xaa
//    	};

    	try
    	{
    		// Path to the just created empty db
    		String outFileName = Constants.kDBLocalPath + Constants.kDBName;

    		//Open the empty db as the output stream
    		FileOutputStream fos = new FileOutputStream(outFileName);
    		for ( int fileId : resourceList )
    		{
    			InputStream inputFile = context.getResources().openRawResource(fileId);

    			int totalLength = 0;
    			try
    			{
    				totalLength = inputFile.available();
    			}
    			catch ( IOException e)
    			{
    				Toast.makeText( context,"Error Reading File",Toast.LENGTH_SHORT).show();
    			}

    			// Reading and writing the file Method 1 :
    			byte[] buffer = new byte[totalLength];
    			int len = 0;
    			try
    			{
    				len = inputFile.read(buffer);
    			}
    			catch ( IOException e)
    			{
    				Toast.makeText(context,"Error Reading File",Toast.LENGTH_SHORT).show();
    			}
    			fos.write( buffer );
    			inputFile.close();
    		}    
    		fos.close();
    	}
    	catch( IOException e)
    	{
    		Toast.makeText(context,"IO Error Reading/writing File",Toast.LENGTH_SHORT).show();
    	}
//    	System.out.println("DataBase Installation, End of creating files");
    }

    public static void copyDBToSD(){
    	try {
    		File sd = Environment.getExternalStorageDirectory();
    		File data = Environment.getDataDirectory();

    		if (sd.canWrite()) {
    			String currentDBPath = "/data/com.yarratrams.tramtracker/databases/" + Constants.kDBName;
    			String backupDBPath = Constants.kDBNameNew;
    			File currentDB = new File(data, currentDBPath);
    			File backupDB = new File(sd, backupDBPath);

    			if (currentDB.exists()) {
    				FileChannel src = new FileInputStream(currentDB).getChannel();
    				FileChannel dst = new FileOutputStream(backupDB).getChannel();
    				dst.transferFrom(src, 0, src.size());
    				src.close();
    				dst.close();
//    				System.out.println("Copied to SDCard");
    			}
    		}
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }

	 /**
     * Copies your database from your local assets-folder to the just created empty database in the
     * system folder, from where it can be accessed and handled.
     * This is done by transfering bytestream.
     * */
    private void copyDataBase() throws IOException{
 
    	//Open your local db as the input stream
    	InputStream myInput = context.getAssets().open(Constants.kDBName);
 
    	// Path to the just created empty db
    	String outFileName = Constants.kDBLocalPath + "something";
 
    	//Open the empty db as the output stream
    	OutputStream myOutput = new FileOutputStream(outFileName);
 
    	//transfer bytes from the inputfile to the outputfile
    	byte[] buffer = new byte[1024];
    	int length;
    	while ((length = myInput.read(buffer))>0){
    		myOutput.write(buffer, 0, length);
    	}
 
    	//Close the streams
    	myOutput.flush();
    	myOutput.close();
    	myInput.close();
 
    }
    
    
	/**
	 * Get a list of stops
	 * @param Stop Tram tracker id is used to populate the stop object
	 * @return ArrayList<NearbyStop>
	 */
	public void populateStop(Stop stop) {
		if(db == null){
			openDataBase();
		}
		if(!db.isOpen()){
			openDataBase(); 
		}
		String columns[] = {"ConnectingTrams","IsCityStop","ConnectingBuses","ConnectingTrains","IsPlatFormStop","stops.StopNo as trackerId","StopLongitude",
				"StopLatitude","IsShelter", "IsYTShelter","IsEasyAccess","StopDescription","StopName", "FlagStopNo","StopSuburbName", "CityDirection", "Zones", "StopLength"};
		Cursor c = db.query("stops left outer join connections on connections.StopNo = stops.StopNo", 
							columns, 
							"stops.StopNo=?", 
							new String[]{Integer.toString(stop.getTrackerID())}, 
							null, 
							null, 
							null, 
							null);
		
		if (c.moveToFirst()) {		
			getStopFromCursor(c, stop);
			String pois = getPOIForStopAsCSV(stop.getTrackerID());
			if(pois != null)
				stop.setPointsOfInterest(pois);
		}
		c.close();
	}	
	
	public void populateRoute(Route route){
		if(db == null){
			openDataBase();
		}
		if(!db.isOpen()){
			openDataBase(); 
		}
		String columns[] = {"Colour", "UpDestination", "DownDestination", "Description", "IsMainRoute"};
		Cursor c = db.query("routes", 
				columns, 
				"HeadboardRouteNo=?", 
				new String[]{route.getRouteNumber()}, 
				null, 
				null, 
				null, 
				null);

		if (c.moveToFirst()) {		
			getRouteFromCursor(c, route);
		}
		c.close();
	}
	
	/**
	 * Fills up an empty (but initialized) stop object (using reference passed in argument) from a given cursor
	 * @param Cursor, Stop
	 * @return NearbyStop
	 */
	private void getStopFromCursor(Cursor c ,Stop stop) {
		int colLatitude = c.getColumnIndex("StopLatitude");
		int colLongitude = c.getColumnIndex("StopLongitude");
		int colTrackerID = c.getColumnIndex("trackerId");
		int colStopName = c.getColumnIndex("StopName");
		int colIsPlatformStop = c.getColumnIndex("IsPlatFormStop");
		int colCityStop = c.getColumnIndex("IsCityStop");

		int colDescription = c.getColumnIndex("StopDescription");
		int colConnectingBuses = c.getColumnIndex("ConnectingBuses");
		int colConnectingTrains = c.getColumnIndex("ConnectingTrains");
		int colConnectingTrams = c.getColumnIndex("ConnectingTrams");
		int colIsShelter = c.getColumnIndex("IsShelter");
		int colIsYTShelter = c.getColumnIndex("IsYTShelter");
		int colIsEasyAccess = c.getColumnIndex("IsEasyAccess");
		int colFlagStopNo = c.getColumnIndex("FlagStopNo");
		int colCityDirection = c.getColumnIndex("CityDirection");
		int colSuburbName = c.getColumnIndex("StopSuburbName");
		int colZones = c.getColumnIndex("Zones");
		int colStopLength = c.getColumnIndex("StopLength");
//		int colTurnType = c.getColumnIndex("Turn");
		
		stop.setTrackerID(c.getInt(colTrackerID));
		if(c.getInt(colCityStop) == 1)
			stop.setCityStop(true);
		else
			stop.setCityStop(false);
		stop.setCityDirection(c.getString(colCityDirection));
		if(stop.getCityDirection() == null)
			stop.setCityDirection("test direction");
//		if(c.getInt(colHasShelter) == 1)
//			stop.setHasShelter(true);
//		else
//			stop.setHasShelter(false);
		stop.setLatitudeE6((int)(c.getDouble(colLatitude) * 1E6));
		stop.setLongitudeE6((int)(c.getDouble(colLongitude) * 1E6));
		if(c.getInt(colIsPlatformStop) == 1)
			stop.setPlatformStop(true);
		else
			stop.setPlatformStop(false);
		stop.setStopDescription(c.getString(colDescription));
		stop.setSuburb(c.getString(colSuburbName));
		if(stop.getSuburb() == null)
			stop.setSuburb("c1234sd");
		stop.setStopNumber(c.getString(colFlagStopNo));
		stop.setStopName(c.getString(colStopName));
		stop.setEasyAccessStop(c.getInt(colIsEasyAccess)==1?true:false);

		String connectingTrains = c.getString(colConnectingTrains);
		if(connectingTrains != null){
			if(connectingTrains.equalsIgnoreCase("null"))
				connectingTrains = "";
			else if(connectingTrains.equalsIgnoreCase("-"))
				connectingTrains = "";
		}
		String connectingTrams = c.getString(colConnectingTrams);
		if(connectingTrams != null){
			if(connectingTrams.equalsIgnoreCase("null"))
				connectingTrams = "";
			else if(connectingTrams.equalsIgnoreCase("-"))
				connectingTrams = "";
		}
		String connectingBuses = c.getString(colConnectingBuses);
		if(connectingBuses != null){
			if(connectingBuses.equalsIgnoreCase("null"))
				connectingBuses = "";
			else if(connectingBuses.equalsIgnoreCase("-"))
				connectingBuses = "";
		}
		stop.setConnectingTrains(connectingTrains);
		stop.setConnectingTrams(connectingTrams);
		stop.setConnectingBuses(connectingBuses);
		
		boolean isShelter=false;
		if(colIsShelter > 0)
			isShelter = c.getShort(colIsShelter) == 1?true:false;
//		else
//			Log.e("getStopFromCursor(Cursor c ,Stop stop)", "colIsShelter = -1, stopno = " + c.getInt(colTrackerID));
		boolean isYTShelter=false;
		if(colIsYTShelter > 0)
			isYTShelter = c.getShort(colIsYTShelter) == 1? true:false;
//		else
//			Log.e("getStopFromCursor(Cursor c ,Stop stop)", "colIsYTShelter = -1, stopno = " + c.getInt(colTrackerID));
		//as per phone discussion with Prashanth, for use with db provided by YT
		stop.setHasShelter(isShelter || isYTShelter);
		stop.setZone(c.getString(colZones));
		if(colStopLength > -1)
			stop.setLength(c.getInt(colStopLength));
	}	

	/**
	 * Get a list of stops
	 * @param Location
	 * @return ArrayList<NearbyStop>
	 */
	public ArrayList<NearbyStop> getNearbyStops(Location location) {
		ArrayList<NearbyStop> stops = new ArrayList<NearbyStop>();
		if(db == null){
			openDataBase();
		}
		if(!db.isOpen()){
			openDataBase(); 
		}
		String columns[] = {"ConnectingTrams","IsCityStop","ConnectingBuses","ConnectingTrains","IsPlatFormStop","stops.StopNo as trackerId","StopLongitude",
				"StopLatitude","IsShelter", "IsYTShelter", "IsEasyAccess", "StopDescription", "StopName", "FlagStopNo", "StopSuburbName", "CityDirection", "StopLength"};
		Cursor c = db.query("stops left outer join connections on stops.StopNo = connections.StopNo", 
							columns, 
							null, 
							null, 
							null, 
							null, 
							null, 
							null);
		if (c.moveToFirst()) {		
			do {
				NearbyStop stop = getNearbyStopFromCursor(c, location);
				if(stop != null){
					Stop tempStop = stop.getStop();
					ArrayList<String> alRoutes = getRoutesStringArrayForStop(stop.getStop());
					String[] arrRoutes = new String[alRoutes.size()];
					int i=0;
					for(String r : alRoutes){
						arrRoutes[i++] = r;
					}
					tempStop.setRoutes(arrRoutes);
					String pois = getPOIForStopAsCSV(tempStop.getTrackerID());
					if(pois != null)
						tempStop.setPointsOfInterest(pois);
					NearbyStop temp;
					for(i=0; i < stops.size(); i++){
						temp = stops.get(i);
						if(stop.getIntDistance() < temp.getIntDistance())
							break;
					}
					stops.add(i,stop);
				}
			} while(c.moveToNext());
		}
		c.close();
		
		return stops;
	}
	
	
	/**
	 * Get a list of stops nearby limited to x number of  stops
	 * @param Location
	 * @return ArrayList<NearbyStop>
	 */
	public ArrayList<NearbyStop> getNearbyStopsLimitedByNoOfStopsOld(Location location) {
		ArrayList<NearbyStop> stops = new ArrayList<NearbyStop>();
		if(db == null){
			openDataBase();
		}
		if(!db.isOpen()){
			openDataBase(); 
		}
		String columns[] = {"StopNo","StopLongitude","StopLatitude"};
		Cursor c = db.query("stops", 
							columns, 
							null, 
							null, 
							null, 
							null, 
							null, 
							null);
		if (c.moveToFirst()) {	
			Location stopLocation = new Location("");
			class MyIntComparable implements Comparator<DistanceAndId>{
				 
			    @Override
			    public int compare(DistanceAndId o1, DistanceAndId o2) {
			        return (o1.getDistance()<o2.getDistance() ? -1 : (o1.getDistance()==o2.getDistance() ? 0 : 1));
			    }
			}
			DistanceAndId distanceAndId;
			ArrayList<DistanceAndId> distanceAndIds = new ArrayList<DistanceAndId>();
			do {
				stopLocation.setLatitude(c.getDouble(2));
				stopLocation.setLongitude(c.getDouble(1));
				distanceAndId = new DistanceAndId((int)location.distanceTo(stopLocation), c.getShort(0));
				distanceAndIds.add(distanceAndId);
			} while(c.moveToNext());
			if(distanceAndIds.size() > 0){
				Collections.sort(distanceAndIds, new MyIntComparable());
				NearbyStop nearbyStop = new NearbyStop();
				Stop stop;
				for (int i = 0; i < distanceAndIds.size() && i < Constants.kNoOfStops; i++) {
					nearbyStop = new NearbyStop();
					stop = new Stop();
					stop.setTrackerID(distanceAndIds.get(i).getId());
					populateStop(stop);
					ArrayList<String> alRoutes = getRoutesStringArrayForStop(stop);
					String[] arrRoutes = new String[alRoutes.size()];
					int j=0;
					for(String r : alRoutes){
						arrRoutes[j++] = r;
					}
					stop.setRoutes(arrRoutes);
					String pois = getPOIForStopAsCSV(stop.getTrackerID());
					if(pois != null)
						stop.setPointsOfInterest(pois);
					nearbyStop.setStop(stop);
					nearbyStop.setIntDistance(distanceAndIds.get(i).getDistance());
					stops.add(nearbyStop);
				}
			}
		}
		c.close();
//		System.out.println("stops = " + stops);
		return stops;
	}
	
//	public NearbyStopsLimitedByNoOfStops getNearbyStopsLimitedByNoOfStops(Location location) {//Original function above
//		long startTime = System.currentTimeMillis();
//		ArrayList<NearbyStop> nearbyStops = new ArrayList<NearbyStop>();
//		ArrayList<NearbyStop> easyAccessStops = new ArrayList<NearbyStop>();
//		ArrayList<NearbyStop> shelterStops = new ArrayList<NearbyStop>();
//		if(db == null){
//			openDataBase();
//		}
//		if(!db.isOpen()){
//			openDataBase(); 
//		}
//		String columns[] = {"StopNo","StopLongitude","StopLatitude", "IsShelter", "IsYTShelter", "IsEasyAccess"};
//		Cursor c = db.query("stops", 
//							columns, 
//							null, 
//							null, 
//							null, 
//							null, 
//							null, 
//							null);
//		if (c.moveToFirst()) {	
//			Location stopLocation = new Location("");
//			class MyIntComparable implements Comparator<DistanceAndId>{
//				 
//			    @Override
//			    public int compare(DistanceAndId o1, DistanceAndId o2) {
//			        return (o1.getDistance()<o2.getDistance() ? -1 : (o1.getDistance()==o2.getDistance() ? 0 : 1));
//			    }
//			}
//			DistanceAndId distanceAndId;
//			ArrayList<DistanceAndId> distanceAndIdsStops = new ArrayList<DistanceAndId>();
//			do {
//				stopLocation.setLatitude(c.getDouble(2));
//				stopLocation.setLongitude(c.getDouble(1));
//				distanceAndId = new DistanceAndId((int)location.distanceTo(stopLocation), c.getShort(0));
//				distanceAndIdsStops.add(distanceAndId);
//			} while(c.moveToNext());
//			if(distanceAndIdsStops.size() > 0){
//				Collections.sort(distanceAndIdsStops, new MyIntComparable());
//				NearbyStop nearbyStop = new NearbyStop();
//				Stop stop;
//				for (int i = 0; i < distanceAndIdsStops.size() && !(nearbyStops.size() == Constants.kNoOfStops && easyAccessStops.size() == Constants.kNoOfStops && shelterStops.size() == Constants.kNoOfStops); i++) {
//					nearbyStop = new NearbyStop();
//					stop = new Stop();
//					stop.setTrackerID(distanceAndIdsStops.get(i).getId());
//					populateStop(stop);
//					ArrayList<String> alRoutes = getRoutesStringArrayForStop(stop);
//					String[] arrRoutes = new String[alRoutes.size()];
//					int j=0;
//					for(String r : alRoutes){
//						arrRoutes[j++] = r;
//					}
//					stop.setRoutes(arrRoutes);
//					String pois = getPOIForStopAsCSV(stop.getTrackerID());
//					if(pois != null)
//						stop.setPointsOfInterest(pois);
//					nearbyStop.setStop(stop);
//					nearbyStop.setIntDistance(distanceAndIdsStops.get(i).getDistance());
//					if(nearbyStops.size() < Constants.kNoOfStops)
//						nearbyStops.add(nearbyStop);
//					if(stop.isEasyAccessStop() && easyAccessStops.size() < Constants.kNoOfStops){
//						easyAccessStops.add(nearbyStop);
//					}
//					if(stop.isHasShelter() && shelterStops.size() < Constants.kNoOfStops){
//						shelterStops.add(nearbyStop);
//					}	
//				}
//			}
//		}
//		c.close();
//		Log.e("Infor","nearby = " + nearbyStops);
//		Log.e("Infor","easyaccess = " + easyAccessStops);
//		Log.e("Infor","shelter = " + shelterStops);
//		Log.e("Infor","Time taken = " + (System.currentTimeMillis() - startTime)/1000 );
//		NearbyStopsLimitedByNoOfStops n = new NearbyStopsLimitedByNoOfStops();
//		n.setEasyAccessStops(easyAccessStops);
//		n.setNearbyStops(nearbyStops);
//		n.setShelterStops(shelterStops);
//		return n;
//	}
	
	
	public NearbyStopsLimitedByNoOfStops getNearbyStopsLimitedByNoOfStops(Location location) {//Original function above
		long startTime = System.currentTimeMillis();
		ArrayList<NearbyStop> nearbyStops = new ArrayList<NearbyStop>();
		ArrayList<NearbyStop> easyAccessStops = new ArrayList<NearbyStop>();
		ArrayList<NearbyStop> shelterStops = new ArrayList<NearbyStop>();
		if(db == null){
			openDataBase();
		}
		if(!db.isOpen()){
			openDataBase(); 
		}
		String columns[] = {"ConnectingTrams","IsCityStop","ConnectingBuses","ConnectingTrains","IsPlatFormStop","stops.StopNo as trackerId","StopLongitude",
				"StopLatitude","IsShelter", "IsYTShelter","IsEasyAccess","StopDescription","StopName", "FlagStopNo","StopSuburbName", "CityDirection", "Zones", "StopLength"};
		Cursor c = db.query("stops left outer join connections on connections.StopNo = stops.StopNo", 
				columns, 
				null, 
				null, 
				null, 
				null, 
				null, 
				null);
		if (c.moveToFirst()) {	
			Location stopLocation = new Location("");
			class MyIntComparable implements Comparator<NearbyStop>{
			    @Override
			    public int compare(NearbyStop o1, NearbyStop o2) {
			        return (o1.getIntDistance()<o2.getIntDistance() ? -1 : (o1.getIntDistance()==o2.getIntDistance() ? 0 : 1));
			    }
			}
			ArrayList<NearbyStop> stopsList = new ArrayList<NearbyStop>();
			NearbyStop nearbyStop = new NearbyStop();
			Stop stop = new Stop();
			
			do {
				stop = new Stop();
				getStopFromCursor(c, stop);
				stopLocation.setLatitude(c.getDouble(c.getColumnIndex("StopLatitude")));
				stopLocation.setLongitude(c.getDouble(c.getColumnIndex("StopLongitude")));

				nearbyStop = new NearbyStop();
				nearbyStop.setStop(stop);
				nearbyStop.setIntDistance((int)location.distanceTo(stopLocation));
				
				stopsList.add(nearbyStop);
			} while(c.moveToNext());
			
			if(stopsList.size() > 0){
				Collections.sort(stopsList, new MyIntComparable());
				
				for (int i = 0; i < stopsList.size() && !(nearbyStops.size() == Constants.kNoOfStops && easyAccessStops.size() == Constants.kNoOfStops && shelterStops.size() == Constants.kNoOfStops); i++) {
					nearbyStop = stopsList.get(i); 
					stop = nearbyStop.getStop();

					ArrayList<String> alRoutes = getRoutesStringArrayForStop(stop);
					String[] arrRoutes = new String[alRoutes.size()];
					int j=0;
					for(String r : alRoutes){
						arrRoutes[j++] = r;
					}
					stop.setRoutes(arrRoutes);
					String pois = getPOIForStopAsCSV(stop.getTrackerID());
					if(pois != null)
						stop.setPointsOfInterest(pois);

					nearbyStop.setStop(stop);
					
					if(nearbyStops.size() < Constants.kNoOfStops)
						nearbyStops.add(nearbyStop);
					if(stop.isEasyAccessStop() && easyAccessStops.size() < Constants.kNoOfStops){
						easyAccessStops.add(nearbyStop);
					}
					if(stop.isHasShelter() && shelterStops.size() < Constants.kNoOfStops){
						shelterStops.add(nearbyStop);
					}	
				}
			}
		}
		c.close();
//		Log.e("Infor","nearby = " + nearbyStops);
//		Log.e("Infor","easyaccess = " + easyAccessStops);
//		Log.e("Infor","shelter = " + shelterStops);
//		Log.e("Infor","Time taken = " + (System.currentTimeMillis() - startTime)/1000 );
		NearbyStopsLimitedByNoOfStops n = new NearbyStopsLimitedByNoOfStops();
		n.setEasyAccessStops(easyAccessStops);
		n.setNearbyStops(nearbyStops);
		n.setShelterStops(shelterStops);
		return n;
	}
	
	
	
	
	/**
	 * Get a list of stops nearby limited to x number of  stops
	 * @param Location
	 * @return ArrayList<NearbyStop>
	 */
	public ArrayList<NearbyStop> getNearbyStopsWithShelterLimitedByNoOfStops(Location location) {
		ArrayList<NearbyStop> stops = new ArrayList<NearbyStop>();
		if(db == null){
			openDataBase();
		}
		if(!db.isOpen()){
			openDataBase(); 
		}
		String columns[] = {"StopNo","StopLongitude","StopLatitude"};
		Cursor c = db.query("stops", 
							columns, 
							"IsShelter=\"1\" or IsYTShelter=\"1\"", 
							null, 
							null, 
							null, 
							null, 
							null);
		if (c.moveToFirst()) {	
			Location stopLocation = new Location("");
			class MyIntComparable implements Comparator<DistanceAndId>{
				 
			    @Override
			    public int compare(DistanceAndId o1, DistanceAndId o2) {
			        return (o1.getDistance()<o2.getDistance() ? -1 : (o1.getDistance()==o2.getDistance() ? 0 : 1));
			    }
			}
			DistanceAndId distanceAndId;
			ArrayList<DistanceAndId> distanceAndIds = new ArrayList<DistanceAndId>();
			do {
				stopLocation.setLatitude(c.getDouble(2));
				stopLocation.setLongitude(c.getDouble(1));
				distanceAndId = new DistanceAndId((int)location.distanceTo(stopLocation), c.getShort(0));
				distanceAndIds.add(distanceAndId);
			} while(c.moveToNext());
			if(distanceAndIds.size() > 0){
				Collections.sort(distanceAndIds, new MyIntComparable());
				NearbyStop nearbyStop = new NearbyStop();
				Stop stop;
				for (int i = 0; i < distanceAndIds.size() && i < Constants.kNoOfStops; i++) {
					nearbyStop = new NearbyStop();
					stop = new Stop();
					stop.setTrackerID(distanceAndIds.get(i).getId());
					populateStop(stop);
					ArrayList<String> alRoutes = getRoutesStringArrayForStop(stop);
					String[] arrRoutes = new String[alRoutes.size()];
					int j=0;
					for(String r : alRoutes){
						arrRoutes[j++] = r;
					}
					stop.setRoutes(arrRoutes);
					String pois = getPOIForStopAsCSV(stop.getTrackerID());
					if(pois != null)
						stop.setPointsOfInterest(pois);
					nearbyStop.setStop(stop);
					nearbyStop.setIntDistance(distanceAndIds.get(i).getDistance());
					stops.add(nearbyStop);
				}
			}
		}
		c.close();
//		System.out.println("stops = " + stops);
		return stops;
	}

	/**
	 * Get a list of stops nearby limited to x number of  stops
	 * @param Location
	 * @return ArrayList<NearbyStop>
	 */
	public ArrayList<NearbyStop> getNearbyStopsWithEasyAccessLimitedByNoOfStops(Location location) {
		ArrayList<NearbyStop> stops = new ArrayList<NearbyStop>();
		if(db == null){
			openDataBase();
		}
		if(!db.isOpen()){
			openDataBase(); 
		}
		String columns[] = {"StopNo","StopLongitude","StopLatitude"};
		Cursor c = db.query("stops", 
							columns, 
							"IsEasyAccess=\"1\"", 
							null, 
							null, 
							null, 
							null, 
							null);
		if (c.moveToFirst()) {	
			Location stopLocation = new Location("");
			class MyIntComparable implements Comparator<DistanceAndId>{
				 
			    @Override
			    public int compare(DistanceAndId o1, DistanceAndId o2) {
			        return (o1.getDistance()<o2.getDistance() ? -1 : (o1.getDistance()==o2.getDistance() ? 0 : 1));
			    }
			}
			DistanceAndId distanceAndId;
			ArrayList<DistanceAndId> distanceAndIds = new ArrayList<DistanceAndId>();
			do {
				stopLocation.setLatitude(c.getDouble(2));
				stopLocation.setLongitude(c.getDouble(1));
				distanceAndId = new DistanceAndId((int)location.distanceTo(stopLocation), c.getShort(0));
				distanceAndIds.add(distanceAndId);
			} while(c.moveToNext());
			if(distanceAndIds.size() > 0){
				Collections.sort(distanceAndIds, new MyIntComparable());
				NearbyStop nearbyStop = new NearbyStop();
				Stop stop;
				for (int i = 0; i < distanceAndIds.size() && i < Constants.kNoOfStops; i++) {
					nearbyStop = new NearbyStop();
					stop = new Stop();
					stop.setTrackerID(distanceAndIds.get(i).getId());
					populateStop(stop);
					ArrayList<String> alRoutes = getRoutesStringArrayForStop(stop);
					String[] arrRoutes = new String[alRoutes.size()];
					int j=0;
					for(String r : alRoutes){
						arrRoutes[j++] = r;
					}
					stop.setRoutes(arrRoutes);
					String pois = getPOIForStopAsCSV(stop.getTrackerID());
					if(pois != null)
						stop.setPointsOfInterest(pois);
					nearbyStop.setStop(stop);
					nearbyStop.setIntDistance(distanceAndIds.get(i).getDistance());
					stops.add(nearbyStop);
				}
			}
		}
		c.close();
//		System.out.println("stops = " + stops);
		return stops;
	}
	
	/**
	 * Get a list of stops nearby limited to x number of  stops
	 * @param Location
	 * @return ArrayList<NearbyStop>
	 */
	public ArrayList<NearbyStop> getNearbyStopsLimitedByNoOfStopsGivenLocationOld(Location mapCentreLocation, Location lastKnownLocation) {
		ArrayList<NearbyStop> stops = new ArrayList<NearbyStop>();
		if(db == null){
			openDataBase();
		}
		if(!db.isOpen()){
			openDataBase(); 
		}
		String columns[] = {"StopNo","StopLongitude","StopLatitude"};
		Cursor c = db.query("stops", 
							columns, 
							null, 
							null, 
							null, 
							null, 
							null, 
							null);
		if (c.moveToFirst()) {	
			Location stopLocation = new Location("");
			class MyIntComparable implements Comparator<DistanceAndId>{
				 
			    @Override
			    public int compare(DistanceAndId o1, DistanceAndId o2) {
			        return (o1.getDistance()<o2.getDistance() ? -1 : (o1.getDistance()==o2.getDistance() ? 0 : 1));
			    }
			}
			DistanceAndId distanceAndId;
			ArrayList<DistanceAndId> distanceAndIds = new ArrayList<DistanceAndId>();
			do {
				stopLocation.setLatitude(c.getDouble(2));
				stopLocation.setLongitude(c.getDouble(1));
				distanceAndId = new DistanceAndId((int)mapCentreLocation.distanceTo(stopLocation), c.getShort(0));
				distanceAndIds.add(distanceAndId);
			} while(c.moveToNext());
			if(distanceAndIds.size() > 0){
				Collections.sort(distanceAndIds, new MyIntComparable());
				NearbyStop nearbyStop = new NearbyStop();
				Stop stop;
//				System.out.println("distanceAndIds = " + distanceAndIds);
				for (int i = 0; i < distanceAndIds.size() && i < Constants.kNoOfStops; i++) {
					nearbyStop = new NearbyStop();
					stop = new Stop();
					stop.setTrackerID(distanceAndIds.get(i).getId());
					populateStop(stop);
					ArrayList<String> alRoutes = getRoutesStringArrayForStop(stop);
					String[] arrRoutes = new String[alRoutes.size()];
					int j=0;
					for(String r : alRoutes){
						arrRoutes[j++] = r;
					}
					stop.setRoutes(arrRoutes);

					String pois = getPOIForStopAsCSV(stop.getTrackerID());
					if(pois != null)
						stop.setPointsOfInterest(pois);
					nearbyStop.setStop(stop);
					stopLocation.setLatitude(stop.getLatitudeE6()/1E6);
					stopLocation.setLongitude(stop.getLongitudeE6()/1E6);
					nearbyStop.setIntDistance((int)stopLocation.distanceTo(lastKnownLocation));
					stops.add(nearbyStop);
				}
				class MyIntDistanceComparable implements Comparator<NearbyStop>{
					 
				    @Override
				    public int compare(NearbyStop o1, NearbyStop o2) {
				        return (o1.getIntDistance()<o2.getIntDistance() ? -1 : (o1.getIntDistance()==o2.getIntDistance() ? 0 : 1));
				    }
				}
				Collections.sort(stops, new MyIntDistanceComparable());
			}
		}
		c.close();
		//System.out.println("stops = " + stops);
		return stops;
	}
	
	
//	public NearbyStopsLimitedByNoOfStopsGivenLocation getNearbyStopsLimitedByNoOfStopsGivenLocation(Location mapCentreLocation, Location lastKnownLocation) {//Original function above
//		long startTime = System.currentTimeMillis();
//		ArrayList<NearbyStop> nearbyStops = new ArrayList<NearbyStop>();
//		ArrayList<NearbyStop> easyAccessStops = new ArrayList<NearbyStop>();
//		ArrayList<NearbyStop> shelterStops = new ArrayList<NearbyStop>();
//		if(db == null){
//			openDataBase();
//		}
//		if(!db.isOpen()){
//			openDataBase(); 
//		}
//		String columns[] = {"StopNo","StopLongitude","StopLatitude", "IsShelter", "IsYTShelter", "IsEasyAccess"};
//		Cursor c = db.query("stops", 
//							columns, 
//							null, 
//							null, 
//							null, 
//							null, 
//							null, 
//							null);
//		if (c.moveToFirst()) {	
//			Location stopLocation = new Location("");
//			class MyIntComparable implements Comparator<DistanceAndId>{
//				 
//			    @Override
//			    public int compare(DistanceAndId o1, DistanceAndId o2) {
//			        return (o1.getDistance()<o2.getDistance() ? -1 : (o1.getDistance()==o2.getDistance() ? 0 : 1));
//			    }
//			}
//			DistanceAndId distanceAndId;
//			ArrayList<DistanceAndId> distanceAndIdsStops = new ArrayList<DistanceAndId>();
//			do {
//				stopLocation.setLatitude(c.getDouble(2));
//				stopLocation.setLongitude(c.getDouble(1));
//				distanceAndId = new DistanceAndId((int)mapCentreLocation.distanceTo(stopLocation), c.getShort(0));
//				distanceAndIdsStops.add(distanceAndId);
//			} while(c.moveToNext());
//			if(distanceAndIdsStops.size() > 0){
//				Collections.sort(distanceAndIdsStops, new MyIntComparable());
//				NearbyStop nearbyStop = new NearbyStop();
//				Stop stop;
//				for (int i = 0; i < distanceAndIdsStops.size() && !(nearbyStops.size() == Constants.kNoOfStops && easyAccessStops.size() == Constants.kNoOfStops && shelterStops.size() == Constants.kNoOfStops); i++) {
//					nearbyStop = new NearbyStop();
//					stop = new Stop();
//					stop.setTrackerID(distanceAndIdsStops.get(i).getId());
//					populateStop(stop);
//					ArrayList<String> alRoutes = getRoutesStringArrayForStop(stop);
//					String[] arrRoutes = new String[alRoutes.size()];
//					int j=0;
//					for(String r : alRoutes){
//						arrRoutes[j++] = r;
//					}
//					stop.setRoutes(arrRoutes);
//					String pois = getPOIForStopAsCSV(stop.getTrackerID());
//					if(pois != null)
//						stop.setPointsOfInterest(pois);
//					nearbyStop.setStop(stop);
//					nearbyStop.setIntDistance((int)stopLocation.distanceTo(lastKnownLocation));
//					if(nearbyStops.size() < Constants.kNoOfStops)
//						nearbyStops.add(nearbyStop);
//					if(stop.isEasyAccessStop() && easyAccessStops.size() < Constants.kNoOfStops){
//						easyAccessStops.add(nearbyStop);
//					}
//					if(stop.isHasShelter() && shelterStops.size() < Constants.kNoOfStops){
//						shelterStops.add(nearbyStop);
//					}	
//				}
//			}
//		}
//		c.close();
//		Log.e("Infor given location","nearby = " + nearbyStops);
//		Log.e("Infor given location","easyaccess = " + easyAccessStops);
//		Log.e("Infor given location","shelter = " + shelterStops);
//		Log.e("Infor given location","Time taken = " + (System.currentTimeMillis() - startTime)/1000 );
//		NearbyStopsLimitedByNoOfStopsGivenLocation n = new NearbyStopsLimitedByNoOfStopsGivenLocation();
//		n.setEasyAccessStops(easyAccessStops);
//		n.setNearbyStops(nearbyStops);
//		n.setShelterStops(shelterStops);
//		return n;
//	}
	
	public NearbyStopsLimitedByNoOfStopsGivenLocation getNearbyStopsLimitedByNoOfStopsGivenLocation(Location mapCentreLocation, Location lastKnownLocation) {//Original function above
		long startTime = System.currentTimeMillis();
		ArrayList<NearbyStop> nearbyStops = new ArrayList<NearbyStop>();
		ArrayList<NearbyStop> easyAccessStops = new ArrayList<NearbyStop>();
		ArrayList<NearbyStop> shelterStops = new ArrayList<NearbyStop>();

		if(db == null){
			openDataBase();
		}
		if(!db.isOpen()){
			openDataBase(); 
		}
		String columns[] = {"ConnectingTrams","IsCityStop","ConnectingBuses","ConnectingTrains","IsPlatFormStop","stops.StopNo as trackerId","StopLongitude",
				"StopLatitude","IsShelter", "IsYTShelter","IsEasyAccess","StopDescription","StopName", "FlagStopNo","StopSuburbName", "CityDirection", "Zones", "StopLength"};
		Cursor c = db.query("stops left outer join connections on connections.StopNo = stops.StopNo", 
				columns, 
				null, 
				null, 
				null, 
				null, 
				null, 
				null);
		if (c.moveToFirst()) {	
			Location stopLocation = new Location("");
			class MyIntComparable implements Comparator<NearbyStop>{
			    @Override
			    public int compare(NearbyStop o1, NearbyStop o2) {
			        return (o1.getIntDistance()<o2.getIntDistance() ? -1 : (o1.getIntDistance()==o2.getIntDistance() ? 0 : 1));
			    }
			}
			ArrayList<NearbyStop> stopsList = new ArrayList<NearbyStop>();
			NearbyStop nearbyStop = new NearbyStop();
			Stop stop = new Stop();
			
			do {
				stop = new Stop();
				getStopFromCursor(c, stop);
				stopLocation.setLatitude(c.getDouble(c.getColumnIndex("StopLatitude")));
				stopLocation.setLongitude(c.getDouble(c.getColumnIndex("StopLongitude")));
				
				nearbyStop = new NearbyStop();
				nearbyStop.setStop(stop);
				nearbyStop.setIntDistance((int)mapCentreLocation.distanceTo(stopLocation));
				
				stopsList.add(nearbyStop);
			} while(c.moveToNext());
			
			if(stopsList.size() > 0){
				Collections.sort(stopsList, new MyIntComparable());
				
				for (int i = 0; i < stopsList.size() && !(nearbyStops.size() == Constants.kNoOfStops && easyAccessStops.size() == Constants.kNoOfStops && shelterStops.size() == Constants.kNoOfStops); i++) {
					nearbyStop = stopsList.get(i); 
					stop = nearbyStop.getStop();
					ArrayList<String> alRoutes = getRoutesStringArrayForStop(stop);
					String[] arrRoutes = new String[alRoutes.size()];
					int j=0;
					for(String r : alRoutes){
						arrRoutes[j++] = r;
					}
					stop.setRoutes(arrRoutes);
					String pois = getPOIForStopAsCSV(stop.getTrackerID());
					if(pois != null)
						stop.setPointsOfInterest(pois);
					
					nearbyStop.setStop(stop);
					
					stopLocation.setLatitude(stop.getLatitudeE6()/1E6);
					stopLocation.setLongitude(stop.getLongitudeE6()/1E6);
					nearbyStop.setIntDistance((int)stopLocation.distanceTo(lastKnownLocation));
					
					if(nearbyStops.size() < Constants.kNoOfStops)
						nearbyStops.add(nearbyStop);
					if(stop.isEasyAccessStop() && easyAccessStops.size() < Constants.kNoOfStops){
						easyAccessStops.add(nearbyStop);
					}
					if(stop.isHasShelter() && shelterStops.size() < Constants.kNoOfStops){
						shelterStops.add(nearbyStop);
					}	
				}
				Collections.sort(nearbyStops, new MyIntComparable());
				Collections.sort(easyAccessStops, new MyIntComparable());
				Collections.sort(shelterStops, new MyIntComparable());
			}
		}
		c.close();
//		Log.e("Infor given location","nearby = " + nearbyStops);
//		Log.e("Infor given location","easyaccess = " + easyAccessStops);
//		Log.e("Infor given location","shelter = " + shelterStops);
//		Log.e("Infor given location","Time taken = " + (System.currentTimeMillis() - startTime)/1000 );
		NearbyStopsLimitedByNoOfStopsGivenLocation n = new NearbyStopsLimitedByNoOfStopsGivenLocation();
		n.setEasyAccessStops(easyAccessStops);
		n.setNearbyStops(nearbyStops);
		n.setShelterStops(shelterStops);
		return n;
	}
	
	
	
	/**
	 * Get a list of stops nearby limited to x number of  stops
	 * @param Location
	 * @return ArrayList<NearbyStop>
	 */
	public ArrayList<NearbyStop> getNearbyStopsWithEasyAccessLimitedByNoOfStopsGivenLocation(Location mapCentreLocation, Location lastKnownLocation) {
		ArrayList<NearbyStop> stops = new ArrayList<NearbyStop>();
		if(db == null){
			openDataBase();
		}
		if(!db.isOpen()){
			openDataBase(); 
		}
		String columns[] = {"StopNo","StopLongitude","StopLatitude"};
		Cursor c = db.query("stops", 
							columns, 
							"IsEasyAccess=\"1\"", 
							null, 
							null, 
							null, 
							null, 
							null);
		if (c.moveToFirst()) {	
			Location stopLocation = new Location("");
			class MyIntComparable implements Comparator<DistanceAndId>{
				 
			    @Override
			    public int compare(DistanceAndId o1, DistanceAndId o2) {
			        return (o1.getDistance()<o2.getDistance() ? -1 : (o1.getDistance()==o2.getDistance() ? 0 : 1));
			    }
			}
			DistanceAndId distanceAndId;
			ArrayList<DistanceAndId> distanceAndIds = new ArrayList<DistanceAndId>();
			do {
				stopLocation.setLatitude(c.getDouble(2));
				stopLocation.setLongitude(c.getDouble(1));
				distanceAndId = new DistanceAndId((int)mapCentreLocation.distanceTo(stopLocation), c.getShort(0));
				distanceAndIds.add(distanceAndId);
			} while(c.moveToNext());
			if(distanceAndIds.size() > 0){
				Collections.sort(distanceAndIds, new MyIntComparable());
				NearbyStop nearbyStop = new NearbyStop();
				Stop stop;
//				System.out.println("distanceAndIds = " + distanceAndIds);
				for (int i = 0; i < distanceAndIds.size() && i < Constants.kNoOfStops; i++) {
					nearbyStop = new NearbyStop();
					stop = new Stop();
					stop.setTrackerID(distanceAndIds.get(i).getId());
					populateStop(stop);
					ArrayList<String> alRoutes = getRoutesStringArrayForStop(stop);
					String[] arrRoutes = new String[alRoutes.size()];
					int j=0;
					for(String r : alRoutes){
						arrRoutes[j++] = r;
					}
					stop.setRoutes(arrRoutes);

					String pois = getPOIForStopAsCSV(stop.getTrackerID());
					if(pois != null)
						stop.setPointsOfInterest(pois);
					nearbyStop.setStop(stop);
					stopLocation.setLatitude(stop.getLatitudeE6()/1E6);
					stopLocation.setLongitude(stop.getLongitudeE6()/1E6);
					nearbyStop.setIntDistance((int)stopLocation.distanceTo(lastKnownLocation));
					stops.add(nearbyStop);
				}
				class MyIntDistanceComparable implements Comparator<NearbyStop>{
					 
				    @Override
				    public int compare(NearbyStop o1, NearbyStop o2) {
				        return (o1.getIntDistance()<o2.getIntDistance() ? -1 : (o1.getIntDistance()==o2.getIntDistance() ? 0 : 1));
				    }
				}
				Collections.sort(stops, new MyIntDistanceComparable());
			}
		}
		c.close();
		//System.out.println("stops = " + stops);
		return stops;
	}
	
	/**
	 * Get a list of stops nearby limited to x number of  stops
	 * @param Location
	 * @return ArrayList<NearbyStop>
	 */
	public ArrayList<NearbyStop> getNearbyStopsWithShelterLimitedByNoOfStopsGivenLocation(Location mapCentreLocation, Location lastKnownLocation) {
		ArrayList<NearbyStop> stops = new ArrayList<NearbyStop>();
		if(db == null){
			openDataBase();
		}
		if(!db.isOpen()){
			openDataBase(); 
		}
		String columns[] = {"StopNo","StopLongitude","StopLatitude"};
		Cursor c = db.query("stops", 
							columns, 
							"IsShelter=\"1\" or IsYTShelter=\"1\"", 
							null, 
							null, 
							null, 
							null, 
							null);
		if (c.moveToFirst()) {	
			Location stopLocation = new Location("");
			class MyIntComparable implements Comparator<DistanceAndId>{
				 
			    @Override
			    public int compare(DistanceAndId o1, DistanceAndId o2) {
			        return (o1.getDistance()<o2.getDistance() ? -1 : (o1.getDistance()==o2.getDistance() ? 0 : 1));
			    }
			}
			DistanceAndId distanceAndId;
			ArrayList<DistanceAndId> distanceAndIds = new ArrayList<DistanceAndId>();
			do {
				stopLocation.setLatitude(c.getDouble(2));
				stopLocation.setLongitude(c.getDouble(1));
				distanceAndId = new DistanceAndId((int)mapCentreLocation.distanceTo(stopLocation), c.getShort(0));
				distanceAndIds.add(distanceAndId);
			} while(c.moveToNext());
			if(distanceAndIds.size() > 0){
				Collections.sort(distanceAndIds, new MyIntComparable());
				NearbyStop nearbyStop = new NearbyStop();
				Stop stop;
//				System.out.println("distanceAndIds = " + distanceAndIds);
				for (int i = 0; i < distanceAndIds.size() && i < Constants.kNoOfStops; i++) {
					nearbyStop = new NearbyStop();
					stop = new Stop();
					stop.setTrackerID(distanceAndIds.get(i).getId());
					populateStop(stop);
					ArrayList<String> alRoutes = getRoutesStringArrayForStop(stop);
					String[] arrRoutes = new String[alRoutes.size()];
					int j=0;
					for(String r : alRoutes){
						arrRoutes[j++] = r;
					}
					stop.setRoutes(arrRoutes);

					String pois = getPOIForStopAsCSV(stop.getTrackerID());
					if(pois != null)
						stop.setPointsOfInterest(pois);
					nearbyStop.setStop(stop);
					stopLocation.setLatitude(stop.getLatitudeE6()/1E6);
					stopLocation.setLongitude(stop.getLongitudeE6()/1E6);
					nearbyStop.setIntDistance((int)stopLocation.distanceTo(lastKnownLocation));
					stops.add(nearbyStop);
				}
				class MyIntDistanceComparable implements Comparator<NearbyStop>{
					 
				    @Override
				    public int compare(NearbyStop o1, NearbyStop o2) {
				        return (o1.getIntDistance()<o2.getIntDistance() ? -1 : (o1.getIntDistance()==o2.getIntDistance() ? 0 : 1));
				    }
				}
				Collections.sort(stops, new MyIntDistanceComparable());
			}
		}
		c.close();
		//System.out.println("stops = " + stops);
		return stops;
	}
	
	
	/**
	 * Get a list of outlets nearby limited to x number of outlets (same number as stops)
	 * @param Location
	 * @return ArrayList<NearbyStop>
	 */
	public ArrayList<NearbyTicketOutlet> getNearbyTicketOutletsLimitedByNoOfOutlets(Location location) {
		ArrayList<NearbyTicketOutlet> outlets = new ArrayList<NearbyTicketOutlet>();
		if(db == null){
			openDataBase();
		}
		if(!db.isOpen()){
			openDataBase(); 
		}
		String columns[] = {"TicketOutletId","Longitude","Latitude"};
		Cursor c = db.query("ticketoutlets", 
							columns, 
							null, 
							null, 
							null, 
							null, 
							null, 
							null);
		if (c.moveToFirst()) {	
			Location outletLocation = new Location("");
			class MyIntComparable implements Comparator<DistanceAndId>{
			    public int compare(DistanceAndId o1, DistanceAndId o2) {
			        return (o1.getDistance()<o2.getDistance() ? -1 : (o1.getDistance()==o2.getDistance() ? 0 : 1));
			    }
			}
			DistanceAndId distanceAndId;
			ArrayList<DistanceAndId> distanceAndIds = new ArrayList<DistanceAndId>();
			do {
				outletLocation.setLatitude(c.getDouble(2));
				outletLocation.setLongitude(c.getDouble(1));
				distanceAndId = new DistanceAndId((int)location.distanceTo(outletLocation), c.getShort(0));
				distanceAndIds.add(distanceAndId);
			} while(c.moveToNext());
			if(distanceAndIds.size() > 0){
				Collections.sort(distanceAndIds, new MyIntComparable());
				NearbyTicketOutlet nearbyTicketOutlet;
				TicketOutlet ticketOutlet;
				for (int i = 0; i < distanceAndIds.size() && i < Constants.kNoOfStops; i++) {
					nearbyTicketOutlet = new NearbyTicketOutlet();
					ticketOutlet = populateTicketOutlet(distanceAndIds.get(i).getId());
					nearbyTicketOutlet.setTicketOutlet(ticketOutlet);
					nearbyTicketOutlet.setIntDistance(distanceAndIds.get(i).getDistance());
					outlets.add(nearbyTicketOutlet);
				}
			}
		}
		c.close();
//		System.out.println("outlets = " + outlets);
		return outlets;
	}

// Slower performance	
//	public ArrayList<NearbyTicketOutlet> getNearbyTicketOutletsLimitedByNoOfOutlets(Location location) {
//		ArrayList<NearbyTicketOutlet> outlets = new ArrayList<NearbyTicketOutlet>();
//		if(db == null){
//			openDataBase();
//		}
//		if(!db.isOpen()){
//			openDataBase(); 
//		}
//		Cursor c = db.query("ticketoutlets", 
//							null, 
//							null, 
//							null, 
//							null, 
//							null, 
//							null, 
//							null);
//		if (c.moveToFirst()) {	
//			Location outletLocation = new Location("");
//			class MyIntComparable implements Comparator<NearbyTicketOutlet>{
//			    public int compare(NearbyTicketOutlet o1, NearbyTicketOutlet o2) {
//			        return (o1.getIntDistance()<o2.getIntDistance() ? -1 : (o1.getIntDistance()==o2.getIntDistance() ? 0 : 1));
//			    }
//			}
//			
//			ArrayList<NearbyTicketOutlet> outletsList = new ArrayList<NearbyTicketOutlet>();
//			NearbyTicketOutlet nearbyTicketOutlet;
//			TicketOutlet ticketOutlet;
//			do {
//				ticketOutlet = getTicketOutletFromCursor(c);
//				outletLocation.setLatitude(c.getDouble(c.getColumnIndexOrThrow("Latitude")));
//				outletLocation.setLongitude(c.getDouble(c.getColumnIndexOrThrow("Longitude")));
//				
//				nearbyTicketOutlet = new NearbyTicketOutlet();
//				nearbyTicketOutlet.setTicketOutlet(ticketOutlet);
//				nearbyTicketOutlet.setIntDistance((int)location.distanceTo(outletLocation));
//				outletsList.add(nearbyTicketOutlet);
//			} while(c.moveToNext());
//			
//			if(outletsList.size() > 0){
//				Collections.sort(outletsList, new MyIntComparable());
//
//				for (int i = 0; i < outletsList.size() && i < Constants.kNoOfStops; i++) {
//					outlets.add(outletsList.get(i));
//				}
//			}
//		}
//		c.close();
////		System.out.println("outlets = " + outlets);
//		return outlets;
//	}

	
	
	
	/**
	 * Get a list of outlets nearby limited to x number of outlets (same number as stops)
	 * @param Location
	 * @return ArrayList<NearbyStop>
	 */
	public ArrayList<NearbyTicketOutlet> getNearbyTicketOutletsLimitedByNoOfOutletsGivenLocation(Location mapCentreLocation, Location lastKnownLocation) {
		ArrayList<NearbyTicketOutlet> outlets = new ArrayList<NearbyTicketOutlet>();
		if(db == null){
			openDataBase();
		}
		if(!db.isOpen()){
			openDataBase(); 
		}
		String columns[] = {"TicketOutletId","Longitude","Latitude"};
		Cursor c = db.query("ticketoutlets", 
							columns, 
							null, 
							null, 
							null, 
							null, 
							null, 
							null);
		if (c.moveToFirst()) {	
			Location outletLocation = new Location("");
			class MyIntComparable implements Comparator<DistanceAndId>{
			    public int compare(DistanceAndId o1, DistanceAndId o2) {
			        return (o1.getDistance()<o2.getDistance() ? -1 : (o1.getDistance()==o2.getDistance() ? 0 : 1));
			    }
			}
			DistanceAndId distanceAndId;
			ArrayList<DistanceAndId> distanceAndIds = new ArrayList<DistanceAndId>();
			do {
				outletLocation.setLatitude(c.getDouble(2));
				outletLocation.setLongitude(c.getDouble(1));
				distanceAndId = new DistanceAndId((int)mapCentreLocation.distanceTo(outletLocation), c.getShort(0));
				distanceAndIds.add(distanceAndId);
			} while(c.moveToNext());
			if(distanceAndIds.size() > 0){
				Collections.sort(distanceAndIds, new MyIntComparable());
				NearbyTicketOutlet nearbyTicketOutlet;
				TicketOutlet ticketOutlet;
				for (int i = 0; i < distanceAndIds.size() && i < Constants.kNoOfStops; i++) {
					nearbyTicketOutlet = new NearbyTicketOutlet();
					ticketOutlet = populateTicketOutlet(distanceAndIds.get(i).getId());
					nearbyTicketOutlet.setTicketOutlet(ticketOutlet);
					outletLocation.setLatitude(ticketOutlet.getLatitudeE6()/1E6);
					outletLocation.setLongitude(ticketOutlet.getLongitudeE6()/1E6);
					nearbyTicketOutlet.setIntDistance((int)outletLocation.distanceTo(lastKnownLocation));
					outlets.add(nearbyTicketOutlet);
				}
				class MyIntDistanceComparable implements Comparator<NearbyTicketOutlet>{
					 
				    @Override
				    public int compare(NearbyTicketOutlet o1, NearbyTicketOutlet o2) {
				        return (o1.getIntDistance()<o2.getIntDistance() ? -1 : (o1.getIntDistance()==o2.getIntDistance() ? 0 : 1));
				    }
				}
				Collections.sort(outlets, new MyIntDistanceComparable());
			}
		}
		c.close();
//		System.out.println("outlets = " + outlets);
		return outlets;
	}
	
// Slow on performance	
//	public ArrayList<NearbyTicketOutlet> getNearbyTicketOutletsLimitedByNoOfOutletsGivenLocation(Location mapCentreLocation, Location lastKnownLocation) {
//		ArrayList<NearbyTicketOutlet> outlets = new ArrayList<NearbyTicketOutlet>();
//		if(db == null){
//			openDataBase();
//		}
//		if(!db.isOpen()){
//			openDataBase(); 
//		}
//		Cursor c = db.query("ticketoutlets", 
//							null, 
//							null, 
//							null, 
//							null, 
//							null, 
//							null, 
//							null);
//		if (c.moveToFirst()) {	
//			Location outletLocation = new Location("");
//			class MyIntComparable implements Comparator<NearbyTicketOutlet>{
//			    public int compare(NearbyTicketOutlet o1, NearbyTicketOutlet o2) {
//			        return (o1.getIntDistance()<o2.getIntDistance() ? -1 : (o1.getIntDistance()==o2.getIntDistance() ? 0 : 1));
//			    }
//			}
//
//			ArrayList<NearbyTicketOutlet> outletsList = new ArrayList<NearbyTicketOutlet>();
//			NearbyTicketOutlet nearbyTicketOutlet;
//			TicketOutlet ticketOutlet;
//			do {
//				ticketOutlet = getTicketOutletFromCursor(c);
//				outletLocation.setLatitude(c.getDouble(c.getColumnIndexOrThrow("Latitude")));
//				outletLocation.setLongitude(c.getDouble(c.getColumnIndexOrThrow("Longitude")));
//				
//				nearbyTicketOutlet = new NearbyTicketOutlet();
//				nearbyTicketOutlet.setTicketOutlet(ticketOutlet);
//				nearbyTicketOutlet.setIntDistance((int)mapCentreLocation.distanceTo(outletLocation));
//				outletsList.add(nearbyTicketOutlet);
//			} while(c.moveToNext());
//			
//			if(outletsList.size() > 0){
//				Collections.sort(outletsList, new MyIntComparable());
//				
//				for (int i = 0; i < outletsList.size() && i < Constants.kNoOfStops; i++) {
//					nearbyTicketOutlet = outletsList.get(i);
//					ticketOutlet = nearbyTicketOutlet.getTicketOutlet();
//					outletLocation.setLatitude(ticketOutlet.getLatitudeE6()/1E6);
//					outletLocation.setLongitude(ticketOutlet.getLongitudeE6()/1E6);
//					nearbyTicketOutlet.setIntDistance((int)outletLocation.distanceTo(lastKnownLocation));
//					outlets.add(nearbyTicketOutlet);
//				}
//				Collections.sort(outlets, new MyIntComparable());
//			}
//		}
//		c.close();
////		System.out.println("outlets = " + outlets);
//		return outlets;
//	}
	
	
	
	/**
	 * Get a list of stops near a location
	 * @param Location
	 * @return ArrayList<NearbyStop>
	 */
	public ArrayList<NearbyStop> getNearbyStopsGivenLocation(Location mapCentreLocation, Location lastKnownLocation) {
		ArrayList<NearbyStop> stops = new ArrayList<NearbyStop>();
		if(db == null){
			openDataBase();
		}
		if(!db.isOpen()){
			openDataBase(); 
		}
		String columns[] = {"ConnectingTrams","IsCityStop","ConnectingBuses","ConnectingTrains","IsPlatFormStop","stops.StopNo as trackerId","StopLongitude",
				"StopLatitude","IsShelter", "IsYTShelter", "IsEasyAccess", "StopDescription", "StopName", "FlagStopNo", "StopSuburbName", "CityDirection", "StopLength"};
		Cursor c = db.query("stops left outer join connections on stops.StopNo = connections.StopNo", 
							columns, 
							null, 
							null, 
							null, 
							null, 
							null, 
							null);
		if (c.moveToFirst()) {		
			do {
				NearbyStop stop = getNearbyStopFromCursor(c, mapCentreLocation);
				if(stop != null){
					Stop tempStop = stop.getStop();
					ArrayList<String> alRoutes = getRoutesStringArrayForStop(stop.getStop());
					String[] arrRoutes = new String[alRoutes.size()];
					int i=0;
					for(String r : alRoutes){
						arrRoutes[i++] = r;
					}
					tempStop.setRoutes(arrRoutes);
					String pois = getPOIForStopAsCSV(tempStop.getTrackerID());
					if(pois != null)
						tempStop.setPointsOfInterest(pois);
					Location loc = new Location("");
					loc.setLatitude(tempStop.getLatitudeE6()/1E6);
					loc.setLongitude(tempStop.getLongitudeE6()/1E6);
					stop.setIntDistance((int)Math.abs(loc.distanceTo(lastKnownLocation)));
					NearbyStop temp;
					for(i=0; i < stops.size(); i++){
						temp = stops.get(i);
						if(stop.getIntDistance() < temp.getIntDistance())
							break;
					}
					stops.add(i,stop);
				}
			} while(c.moveToNext());
		}
		c.close();
		return stops;
	}	
	
	
	/**
	 * Get a list of stops near a location
	 * @param Location
	 * @return ArrayList<NearbyStop>
	 */
	public ArrayList<NearbyFavourite> getNearbyFavouriteGivenLocation(Location location) {
		ArrayList<NearbyFavourite> alFavourites = null;
		if(db == null){
			openDataBase();
		}
		if(!db.isOpen()){
			openDataBase(); 
		}
		String columns[] = {""}; 
		Cursor c = db.query("favouritesandfavgroups", 
							null, //get all columns 
							null, 
							null, 
							null, 
							null, 
							null, 
							null);
//		System.out.println("c.getCount() = " + c.getCount());
		if (c.moveToFirst()) {
			int distance;
			Location loc;
			Stop stop;
			Favourite favourite;
			alFavourites = new ArrayList<NearbyFavourite>();
			NearbyFavourite tempNearbyFavourite;
			do {
				favourite = new Favourite();
				favourite.setName(c.getString(c.getColumnIndex("FavouriteName")));
				favourite.setOrder((int)c.getLong(c.getColumnIndex("DisplayOrder")));
				stop = new Stop();
				stop.setTrackerID((int)c.getLong(c.getColumnIndex("StopNo")));
				populateStop(stop);
				favourite.setStop(stop);
				loc = new Location("");
				loc.setLatitude(stop.getLatitudeE6()/1E6);
				loc.setLongitude(stop.getLongitudeE6()/1E6);
				Cursor routes = db.query("favouritestoproutes", 
						null, 
						"StopNo=" + stop.getTrackerID(), 
						null, 
						null, 
						null, 
						null, 
						null);
				if(routes.moveToFirst()){
					ArrayList<String> alRoutes = new ArrayList<String>();
					do{
						if(c.getColumnIndex("RouteNo") > -1)
							alRoutes.add(routes.getString(c.getColumnIndex("RouteNo")));
					} while (routes.moveToNext());
					favourite.setAlRoutes(alRoutes);
				}
				routes.close();
				distance = Math.round(location.distanceTo(loc));
				NearbyFavourite nearbyFavourite = new NearbyFavourite(favourite, distance);
				if(alFavourites.size() == 0)
					alFavourites.add(nearbyFavourite);
				else{
					int i;
					for(i=0; i< alFavourites.size(); i++){
						tempNearbyFavourite = alFavourites.get(i);
						if(nearbyFavourite.getDistance() < tempNearbyFavourite.getDistance()){
							alFavourites.add(i, nearbyFavourite);
							break;
						}
					}
					if(i == alFavourites.size() - 1){
						if(!alFavourites.contains(nearbyFavourite))
							alFavourites.add(nearbyFavourite);
					}
				}
			} while(c.moveToNext());
		}
		c.close();
		return alFavourites;
	}	
	
	
	/**
	 * Build a stop object from a given cursor (withing Constants.kNearestDistance)
	 * @param Cursor, Location
	 * @return NearbyStop
	 */
	private NearbyStop getNearbyStopFromCursor(Cursor c ,Location location) {
		int colLatitude = c.getColumnIndexOrThrow("StopLatitude");
		int colLongitude = c.getColumnIndexOrThrow("StopLongitude");
		Location l2 = new Location("");
		l2.setLatitude(c.getDouble(colLatitude));
		l2.setLongitude(c.getDouble(colLongitude));
		int distance = Math.abs((int)l2.distanceTo(location));
		if(distance < Constants.kNearestDistance){
			int colTrackerID = c.getColumnIndexOrThrow("trackerId");
			int colStopName = c.getColumnIndexOrThrow("StopName");
			int colIsPlatformStop = c.getColumnIndexOrThrow("IsPlatFormStop");
			int colCityStop = c.getColumnIndexOrThrow("IsCityStop");

			int colHasShelter = c.getColumnIndexOrThrow("IsShelter");
			int colHasYTShelter = c.getColumnIndexOrThrow("IsYTShelter");
			int colDescription = c.getColumnIndexOrThrow("StopDescription");
			int colConnectingBuses = c.getColumnIndexOrThrow("ConnectingBuses");
			int colConnectingTrains = c.getColumnIndexOrThrow("ConnectingTrains");
			int colConnectingTrams = c.getColumnIndexOrThrow("ConnectingTrams");
			//		int colRouteNo = c.getColumnIndexOrThrow("RouteNo");
			int colIsEasyAccess = c.getColumnIndexOrThrow("IsEasyAccess");
			int colFlagStopNo = c.getColumnIndexOrThrow("FlagStopNo");
			int colCityDirection = c.getColumnIndexOrThrow("CityDirection");
			int colSuburbName = c.getColumnIndexOrThrow("StopSuburbName");
			int colStopLength =  c.getColumnIndexOrThrow("StopLength");
//			int colZones = c.getColumnIndexOrThrow("Zones");
			//		int colTurnType = c.getColumnIndexOrThrow("TurnType");

			Stop stop = new Stop();
			stop.setTrackerID(c.getInt(colTrackerID));
			if(c.getInt(colCityStop) == 1)
				stop.setCityStop(true);
			else
				stop.setCityStop(false);
			stop.setCityDirection(c.getString(colCityDirection));
			if(stop.getCityDirection() == null)
				stop.setCityDirection("test direction");
			String connectingTrains = c.getString(colConnectingTrains);
			if(connectingTrains != null){
				if(connectingTrains.equalsIgnoreCase("null"))
					connectingTrains = "";
				else if(connectingTrains.equalsIgnoreCase("-"))
					connectingTrains = "";
			}
			String connectingTrams = c.getString(colConnectingTrams);
			if(connectingTrams != null){
				if(connectingTrams.equalsIgnoreCase("null"))
					connectingTrams = "";
				else if(connectingTrams.equalsIgnoreCase("-"))
					connectingTrams = "";
			}
			String connectingBuses = c.getString(colConnectingBuses);
			if(connectingBuses != null){
				if(connectingBuses.equalsIgnoreCase("null"))
					connectingBuses = "";
				else if(connectingBuses.equalsIgnoreCase("-"))
					connectingBuses = "";
			}
			stop.setConnectingTrains(connectingTrains);
			stop.setConnectingTrams(connectingTrams);
			stop.setConnectingBuses(connectingBuses);
			if(c.getInt(colIsEasyAccess) == 1)
				stop.setEasyAccessStop(true);
			else
				stop.setEasyAccessStop(false);
			if(c.getInt(colHasShelter) == 1 || c.getInt(colHasYTShelter) == 1)
				stop.setHasShelter(true);
			else
				stop.setHasShelter(false);
			stop.setLatitudeE6((int)(c.getDouble(colLatitude) * 1E6));
			stop.setLongitudeE6((int)(c.getDouble(colLongitude) * 1E6));
			if(c.getInt(colIsPlatformStop) == 1)
				stop.setPlatformStop(true);
			else
				stop.setPlatformStop(false);
			//		stop.setPointsOfInterest(pointsOfInterest)
			stop.setStopDescription(c.getString(colDescription));
			stop.setSuburb(c.getString(colSuburbName));
			stop.setStopNumber(c.getString(colFlagStopNo));
			stop.setStopName(c.getString(colStopName));
			if(colStopLength > -1)
				stop.setLength(c.getInt(colStopLength));
			//		stop.setTurnType(c.getString(colTurnType));
//			stop.setZone(c.getShort(colZones));
			NearbyStop nearbyStop = new NearbyStop(stop, distance);
			return nearbyStop;
		}
		return null;
	}	
	
	
	/**
	 * Build a route object from a given cursor. Uses a Join as table.
	 * @param Cursor
	 * @return Route
	 */
	private Route getRouteFromCursorForStop(Cursor c) {
		int colRouteNo = c.getColumnIndexOrThrow("HeadboardRouteNo");
		int colRouteDescription = c.getColumnIndexOrThrow("Description");
		int colUpDestination = c.getColumnIndexOrThrow("UpDestination");
		int colDownDestination = c.getColumnIndexOrThrow("DownDestination");
		int colColour = c.getColumnIndexOrThrow("Colour");
		int colMainRoute = c.getColumnIndexOrThrow("IsMainRoute");
		int colIsUpDirection = c.getColumnIndexOrThrow("IsUpDestination");
		Route route = new Route();
		route.setDescription(c.getString(colRouteDescription));
		String strDownDestination = c.getString(colDownDestination);
		if(strDownDestination == null){
//			System.out.println("strDownDestination = " + strDownDestination);
			strDownDestination = "null in db";
		}
		route.setDownDestination(strDownDestination);
		route.setRouteNumber(c.getString(colRouteNo));
		String strUpDestination = c.getString(colUpDestination);
		if(strUpDestination == null){
//			System.out.println("strUpDestination = " + strUpDestination);
			strUpDestination = "null in db";
		}
		route.setIsUpDestination(c.getInt(colIsUpDirection)==1?true:false);
		route.setUpDestination(strUpDestination);
		route.setColour(c.getString(colColour));
		route.setMainRoute(c.getInt(colMainRoute)==1?true:false);
		return route;
	}	
	
	
	/**
	 * Get a list of nearest outlets
	 * @param Location
	 * @return ArrayList<NearbyTicketOutlet>
	 */
	public ArrayList<NearbyTicketOutlet> getNearbyTicketOutlets(Location location) {
		String tableOutlets = "ticketoutlets";
		ArrayList<NearbyTicketOutlet> outlets = new ArrayList<NearbyTicketOutlet>();
		if(db == null){
			openDataBase();
		}
		if(!db.isOpen()){
			openDataBase(); 
		}
		Cursor c = db.query(tableOutlets, 
							null, 
							null, 
							null, 
							null, 
							null, 
							null, 
							null);
	
		if (c.moveToFirst()) {		
			do {
				NearbyTicketOutlet nto = getNearbyOutletFromCursor(c, location);
				NearbyTicketOutlet temp;
				int i=0;
				if(nto != null){
					for(i=0; i< outlets.size(); i++){
						temp = outlets.get(i);
						if(nto.getIntDistance() < temp.getIntDistance()){
							break;
						}
					}
					outlets.add(i, nto);
				}
			} while(c.moveToNext());
		}
		c.close();
		Log.d("getNearbyTicketOutlets", "outlets = " + outlets);
		return outlets;
	}	
	
	
	/**
	 * Get a list of nearest outlets
	 * @param Location
	 * @return ArrayList<NearbyTicketOutlet>
	 */
	public ArrayList<NearbyTicketOutlet> getNearbyTicketOutletsGivenLocation(Location mapCentreLocation, Location lastKnownLocation) {
		String tableOutlets = "ticketoutlets";
		ArrayList<NearbyTicketOutlet> outlets = new ArrayList<NearbyTicketOutlet>();
		if(db == null){
			openDataBase();
		}
		if(!db.isOpen()){
			openDataBase(); 
		}
		Cursor c = db.query(tableOutlets, 
							null, 
							null, 
							null, 
							null, 
							null, 
							null, 
							null);
	
		if (c.moveToFirst()) {		
			do {
				NearbyTicketOutlet nto = getNearbyOutletFromCursor(c, mapCentreLocation);
				if(nto != null){
					TicketOutlet outlet = nto.getTicketOutlet();
					Location loc = new Location("");
					loc.setLatitude(outlet.getLatitudeE6()/1E6);
					loc.setLongitude(outlet.getLongitudeE6()/1E6);
					nto.setIntDistance((int)Math.abs(loc.distanceTo(lastKnownLocation)));
					NearbyTicketOutlet temp;
					int i=0;
					if(nto != null){
						for(i=0; i< outlets.size(); i++){
							temp = outlets.get(i);
							if(nto.getIntDistance() < temp.getIntDistance()){
								break;
							}
						}
						outlets.add(i, nto);
					}
				}
			} while(c.moveToNext());
		}
		c.close();
		Log.d("getNearbyTicketOutlets", "outlets = " + outlets);
		return outlets;
	}	
	
	
	/**
	 * Populate an outlet given the outlet as input
	 * @param outlet
	 */
	public TicketOutlet populateTicketOutlet(long outletId) {
		String tableOutlets = "ticketoutlets";
		TicketOutlet outlet = null;
		if(db == null){
			openDataBase();
		}
		if(!db.isOpen()){
			openDataBase(); 
		}
		try{
			Cursor c = db.query(tableOutlets, 
					null, 
					"TicketOutletId=" + outletId, 
					null, 
					null, 
					null, 
					null, 
					null);
//			System.out.println("populateTicketOutlet c.getCount() = " + c.getCount() );
			if (c.moveToFirst()) {		
				outlet = getTicketOutletFromCursor(c);
			}
			c.close();
//			Log.d("getNearbyTicketOutlets", "Populated outlet = " + outlet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return outlet;
	}	
	
	
	/**
	 * Build a NearbyTicketOutlet object from a given cursor
	 * @param Cursor,Location 
	 * @return NearbyTicketOutlet (consisting of distance and TicketOutlet objects)
	 */
	private NearbyTicketOutlet getNearbyOutletFromCursor(Cursor c, Location location) {
		int colLatitude = c.getColumnIndexOrThrow("Latitude");
		int colLongitude = c.getColumnIndexOrThrow("Longitude");
		Location l2 = new Location("");
		l2.setLatitude(c.getDouble(colLatitude));
		l2.setLongitude(c.getDouble(colLongitude));
		int distance = (int)Math.abs(l2.distanceTo(location));
		if(distance < Constants.kNearestDistance){
			int colName = c.getColumnIndexOrThrow("Name");
			int colAddress = c.getColumnIndexOrThrow("Address");
			int colSuburbName = c.getColumnIndexOrThrow("Suburb");
			int colHasMetcard = c.getColumnIndexOrThrow("HasMetcard");
			int colHasMyki = c.getColumnIndexOrThrow("HasMyki");
			int colHasMykiTopup = c.getColumnIndexOrThrow("HasMykiTopUp");
			int colHas24Hour = c.getColumnIndexOrThrow("Is24Hour");
			int colOutletId = c.getColumnIndexOrThrow("TicketOutletId");

			TicketOutlet outlet = new TicketOutlet();
			outlet.setName(c.getString(colName));
			outlet.setAddress(c.getString(colAddress));
			outlet.setLatitudeE6((int)(c.getDouble(colLatitude) * 1E6));
			outlet.setLongitudeE6((int)(c.getDouble(colLongitude) * 1E6));
			outlet.setSuburb(c.getString(colSuburbName));
			if(c.getInt(colHasMetcard) == 1)
				outlet.setHasMetcard(true);
			else
				outlet.setHasMetcard(false);
			if(c.getInt(colHasMyki) == 1)
				outlet.setHasMykiCard(true);
			else
				outlet.setHasMykiCard(false);
			if(c.getInt(colHasMykiTopup) == 1)
				outlet.setHasMykiTopUp(true);
			else
				outlet.setHasMykiTopUp(false);
			
			if(c.getInt(colHas24Hour) == 1)
				outlet.setIs24Hours(true);
			else
				outlet.setIs24Hours(false);
			outlet.setOutletId(c.getInt(colOutletId));
			if(c.getInt(colHasMykiTopup) == 1)
				outlet.setHasMykiTopUp(true);
			else
				outlet.setHasMykiTopUp(false);
			NearbyTicketOutlet nearby = new NearbyTicketOutlet(outlet, distance);
			return nearby;
		}
		return null;
	}	
	
	/**
	 * Build a TicketOutlet object from a given cursor
	 * @param Cursor 
	 * @return TicketOutlet
	 */
	private TicketOutlet getTicketOutletFromCursor(Cursor c) {
		int colLatitude = c.getColumnIndexOrThrow("Latitude");
		int colLongitude = c.getColumnIndexOrThrow("Longitude");
		int colName = c.getColumnIndexOrThrow("Name");
		int colAddress = c.getColumnIndexOrThrow("Address");
		int colSuburbName = c.getColumnIndexOrThrow("Suburb");
		int colHasMetcard = c.getColumnIndexOrThrow("HasMetcard");
		int colHasMyki = c.getColumnIndexOrThrow("HasMyki");
		int colHasMykiTopup = c.getColumnIndexOrThrow("HasMykiTopUp");
		int colHas24Hour = c.getColumnIndexOrThrow("Is24Hour");
		int colOutletId = c.getColumnIndexOrThrow("TicketOutletId");

		TicketOutlet outlet = new TicketOutlet();
		outlet.setName(c.getString(colName));
		outlet.setAddress(c.getString(colAddress));
		outlet.setLatitudeE6((int)(c.getDouble(colLatitude) * 1E6));
		outlet.setLongitudeE6((int)(c.getDouble(colLongitude) * 1E6));
		outlet.setSuburb(c.getString(colSuburbName));
		if(c.getInt(colHasMetcard) == 1)
			outlet.setHasMetcard(true);
		else
			outlet.setHasMetcard(false);
		if(c.getInt(colHasMyki) == 1)
			outlet.setHasMykiCard(true);
		else
			outlet.setHasMykiCard(false);
		if(c.getInt(colHasMykiTopup) == 1)
			outlet.setHasMykiTopUp(true);
		else
			outlet.setHasMykiTopUp(false);

		if(c.getInt(colHas24Hour) == 1)
			outlet.setIs24Hours(true);
		else
			outlet.setIs24Hours(false);
		outlet.setOutletId(c.getInt(colOutletId));
		if(c.getInt(colHasMykiTopup) == 1)
			outlet.setHasMykiTopUp(true);
		else
			outlet.setHasMykiTopUp(false);
		return outlet;
	}	

	
	/**
	 * Build a PointOfInterest object from a given cursor
	 * @param Cursor 
	 * @return PointOfInterest
	 */
	private PointOfInterest getPointOfInterestFromCursor(Cursor c) {
		int colPoiId = c.getColumnIndexOrThrow("POIId");
		int colPOIDescription = c.getColumnIndexOrThrow("POIDescription");
//		int colTramTrackerId = c.getColumnIndexOrThrow("TramTrackerId");
		int colLatitude = c.getColumnIndexOrThrow("Latitude");
		int colLongitude = c.getColumnIndexOrThrow("Longitude");
		int colName = c.getColumnIndexOrThrow("Name");
		int colPhone = c.getColumnIndexOrThrow("PhoneNumber");
		int colWeb = c.getColumnIndexOrThrow("WebAddress");
		int colEmail = c.getColumnIndexOrThrow("EmailAddress");
		int colAddress = c.getColumnIndexOrThrow("StreetAddress");
		int colSuburbName = c.getColumnIndexOrThrow("Suburb");
		int colPostcode = c.getColumnIndexOrThrow("PostCode");
		int colDisabledAccess = c.getColumnIndexOrThrow("HasDisabledAccess");
		int colHasToilets = c.getColumnIndexOrThrow("HasToilets");
		int colOpeningHours = c.getColumnIndexOrThrow("OpeningHours");
		int colEntryFee = c.getColumnIndexOrThrow("HasEntryFee");

		PointOfInterest poi = new PointOfInterest();
		poi.setPoiId(c.getInt(colPoiId));
		poi.setDescription(c.getString(colPOIDescription));
//		poi.setTrackerId(c.getInt(colTramTrackerId));
		poi.setLatitude(c.getDouble(colLatitude));
		poi.setLongitude(c.getDouble(colLongitude));
		poi.setName(c.getString(colName));
		poi.setPhone(c.getString(colPhone));
		poi.setWebAddress(c.getString(colWeb));
		poi.setEmail(c.getString(colEmail));
		poi.setStreetAddress(c.getString(colAddress));
		poi.setSuburb(c.getString(colSuburbName));
		poi.setPostCode(c.getString(colPostcode));
		if(c.getInt(colDisabledAccess) == 1)
			poi.setHasDisabled(true);
		else
			poi.setHasDisabled(false);
		if(c.getInt(colHasToilets) == 1)
			poi.setHasToilet(true);
		else
			poi.setHasToilet(false);
		poi.setOpeningHours(c.getString(colOpeningHours));
		poi.setEntryFree(c.getString(colEntryFee));
		return poi;
	}	
	
//	/**
//	 * Get a list of stops for a given route no
//	 * @param ArrayList<Stops>
//	 * @return List<Route>
//	 */
//	public ArrayList<Stop> getStopsForRoute(String routeNo, boolean upStop) {
//		//according to Prashant, a stop on a route can only be in 1 zone
//		//also, if either isShelter or isYTshelter are true, then the stop has a shelter
//		String tableName = "routestop join stop on stop.StopID = routestop.StopID";
//		String direction = "0", sortOrder="asc";
//		ArrayList<Stop> alStops = new ArrayList<Stop>();
//		if(upStop){
//			direction = "1";
//			sortOrder="asc";
//		}
//		else{
//			direction = "0";
//			sortOrder="desc";
//		}
//		if(db == null){
//			openDataBase();
//		}
//		if(!db.isOpen()){
//			openDataBase(); 
//		}
//		
//		Cursor c = db.query(tableName, 
//							new String[] {"IsPlatformStop","stop.TramTrackerID as trackerID","StopLength","Longitude","IsCityStop","Latitude","IsPlatformStop","Description","StopName", "FlagStopNo", 
//				"CityDirection", "TurnType", "SuburbID" }, 
//							"RouteNo =? and UpStop" + "=?", 
//							new String[]{routeNo, direction}, 
//							null, 
//							null, 
//							"StopSequence asc", 
//							null);
//		System.out.println("Cursor size = " + c.getCount());
//		if (c.moveToFirst()) {		
//			do {	
//				Stop stop = new Stop();
//				getStopFromCursor(c,stop);
//				String turnType = c.getString(c.getColumnIndex("TurnType"));
//				if(turnType != null){
//					Cursor d = db.query("turntype", 
//							new String[] {"TurnType"}, 
//							"TurnID =?", 
//							new String[]{turnType}, 
//							null, 
//							null, 
//							null, 
//							null);
//					d.moveToFirst();
//					if(d.getCount() > 0){
//					turnType = d.getString(d.getColumnIndex("TurnType"));
//					if(turnType != null)
//						stop.setTurnType(turnType);
//					}
//					d.close();
//				}
//				Cursor d = db.query("suburb", 
//						new String[] {"SuburbName"}, 
//						"SuburbID =?", 
//						new String[]{c.getString(c.getColumnIndex("SuburbID"))}, 
//						null, 
//						null, 
//						null, 
//						null);
//				d.moveToFirst();
//				if(d.getCount() > 0)
//					stop.setSuburb(d.getString(d.getColumnIndex("SuburbName")));
//				if(stop.getSuburb() == null || stop.getSuburb().equalsIgnoreCase("null"))
//					stop.setSuburb("Not in database");
//				d.close();
//				d = db.query("connections", 
//						new String[] {"IsEasyAccess", "ConnectingTrains", "ConnectingBuses","IsShelter", "ConnectingTrams","IsYTShelter"}, 
//						"TramTrackerID =?", 
//						new String[]{Integer.toString(stop.getTrackerID())}, 
//						null, 
//						null, 
//						null, 
//						null);
//				d.moveToFirst();
//				if(d.getCount() > 0){
//					stop.setEasyAccessStop(d.getInt(d.getColumnIndex("IsEasyAccess"))==1?true:false);
//					stop.setConnectingTrains(d.getString(d.getColumnIndex("ConnectingTrains")));
//					stop.setConnectingTrams(d.getString(d.getColumnIndex("ConnectingTrams")));
//					stop.setConnectingBuses(d.getString(d.getColumnIndex("ConnectingBuses")));
//					boolean isShelter = c.getShort(d.getColumnIndex("IsShelter")) == 1?true:false;
//					boolean isYTShelter = c.getShort(d.getColumnIndex("IsYTShelter")) == 1? true:false;
//					//as per phone discussion with Prashanth, for use with db provided by YT
//					stop.setHasShelter(isShelter || isYTShelter);
//				}
//				d.close();
//				d = db.query("zones", 
//						new String[] {"Zone"}, 
//						"TrackerID =?", 
//						new String[]{Integer.toString(stop.getTrackerID())}, 
//						null, 
//						null, 
//						null, 
//						null);
//				d.moveToFirst();
//				if(d.getCount() > 0)
//					stop.setZone(d.getShort(d.getColumnIndex("Zone")));
//				d.close();
//				alStops.add(stop);
//			} 
//			while(c.moveToNext());
//			c.close();
//		}
//		
//		
//		System.out.println("alStops = " + alStops);
//		return alStops;
//	}	
	
	
	/**
	 * Get a list of stops for a given route no
	 * @param ArrayList<Stops>
	 * @return List<Route>
	 */
	public ArrayList<Stop> getStopsForRoute(String routeNo, boolean upStop) {
		//according to Prashant, a stop on a route can only be in 1 zone
		//also, if either isShelter or isYTshelter are true, then the stop has a shelter
		String tableName = "routesandstops left outer join turns on routesandstops.RouteNo = turns.RouteNo and routesandstops.StopNo = turns.StopNo and " +
				"routesandstops.StopSequence = turns.StopSequence " +
				"join stops on stops.StopNo = routesandstops.StopNo left outer join connections on stops.StopNo = connections.StopNo join routes on routesandstops.RouteNo = routes.RouteNo";
		String direction = upStop?"1":"0";
		ArrayList<Stop> alStops = new ArrayList<Stop>();
		if(db == null){
			openDataBase();
		}
		if(!db.isOpen()){
			openDataBase(); 
		}
		
		StringBuffer whereClause = new StringBuffer();
		whereClause.append("HeadboardRouteNo =? and IsUpDestination =?");
		if(routeNo.equalsIgnoreCase("3a"))
			whereClause.append(" and routes.RouteNo=4");
		
		Cursor c = db.query(tableName, 
				new String[] {"IsPlatFormStop","stops.StopNo as trackerId","StopLength","StopLongitude","IsCityStop","StopLatitude","StopDescription","StopName", "FlagStopNo", 
	"CityDirection", "TurnType", "TurnMessage", "StopSuburbName", "IsEasyAccess", "ConnectingTrains", "ConnectingBuses","IsShelter", "ConnectingTrams","IsYTShelter", "Zones", "StopLength" }, 
				whereClause.toString(), 
				new String[]{routeNo, direction}, 
				null, 
				null, 
				"routesandstops.StopSequence asc", 
				null);
//		Log.e("getStopsForRoute", "route = " + routeNo);
//		System.out.println("Cursor size = " + c.getCount());
		if (c.moveToFirst()) {		
			do {	
				Stop stop = new Stop();
				getStopFromCursor(c,stop);
				String turnType = c.getString(c.getColumnIndex("TurnType"));
				stop.setTurnType(turnType);
				stop.setRoutes(getRoutesArrayOfStringsForStop(stop));
				String pois = getPOIForStopAsCSV(stop.getTrackerID());
				if(pois != null)
					stop.setPointsOfInterest(pois);
				if(stop.getTrackerID() > 0)
					alStops.add(stop);
			} 
			while(c.moveToNext());
		}
		c.close();
//		System.out.println("alStops = " + alStops + " for route no = " + routeNo);
		return alStops;
	}	
	
	
	/**
	 * Get a list of stops for a given route no
	 * @param ArrayList<Stops>
	 * @return List<Route>
	 */
	public ArrayList<Stop> getStopsForPOI(int poiId) {
		//according to Prashant, a stop on a route can only be in 1 zone
		//also, if either isShelter or isYTshelter are true, then the stop has a shelter
		ArrayList<Stop> alStops = new ArrayList<Stop>();
		if(db == null){
			openDataBase();
		}
		if(!db.isOpen()){
			openDataBase(); 
		}
		
		
		Cursor c = db.query("poistops", 
				new String[] {"StopNo" }, 
				"POIId = " + poiId, 
				null, 
				null, 
				null, 
				null, 
				null);
//		Log.e("getStopsForRoute", "route = " + routeNo);
//		System.out.println("Cursor size = " + c.getCount());
		if (c.moveToFirst()) {		
			do {	
				Stop stop = new Stop();
				stop.setTrackerID(c.getInt(0));
				populateStop(stop);
				if(stop.getTrackerID() > 0)
					alStops.add(stop);
			} 
			while(c.moveToNext());
		}
		c.close();
//		System.out.println("alStops = " + alStops);
		return alStops;
	}	
	
	
	/**
	 * Get a list of routes for a given TramTracker ID
	 * @param Stop
	 * @return List<Route>
	 */
	public ArrayList<Route> getRoutesForStop(Stop stop) {
		String tableName = "routesandstops join routes on routesandstops.RouteNo = routes.RouteNo";
		
		if(db == null){
			openDataBase();
		}
		if(!db.isOpen()){
			openDataBase(); 
		}
		ArrayList<Route> routes = new ArrayList<Route>();
		
		Cursor c = db.query(tableName, 
							new String[] { "HeadboardRouteNo", "Description", "UpDestination", "DownDestination", "Colour", "IsMainRoute", "IsUpDestination"}, 
							"routesandstops.StopNo = " + stop.getTrackerID() + " and IsMainRoute = 1", 
							null, 
							null, 
							null, 
							"routes.RouteNo", 
							null);
		
		if (c.moveToFirst()) {		
			do {	
				Route route = getRouteFromCursorForStop(c);
				routes.add(route);
			} 
			while(c.moveToNext());
		}
		c.close();
		return routes;
	}	
	

	/**
	 * Get a list of routes for a given TramTracker ID
	 * @param Stop
	 * @return ArrayList<String>
	 */
	public ArrayList<String> getRoutesStringArrayForStop(Stop stop) {
		String tableName = "stops join routesandstops on stops.StopNo = routesandstops.StopNo join routes on routesandstops.RouteNo = routes.RouteNo";
		Cursor c = db.query(tableName, 
				new String[] { "DISTINCT HeadboardRouteNo"}, 
				"stops.StopNo = " + stop.getTrackerID() + " and IsMainRoute = 1", 
				null, 
				null, 
				null, 
				"routesandstops.RouteNo", 
				null);
		ArrayList<String> alRoutes = new ArrayList<String>();
		if (c.moveToFirst()) {		
			do{
			alRoutes.add(c.getString(0));
			}while(c.moveToNext());
		}
		c.close();
		return alRoutes;
	}	
	
	/**
	 * Get a list of routes for a given TramTracker ID as String[]
	 * @param Stop
	 * @return String[]
	 */
	public String[] getRoutesArrayOfStringsForStop(Stop stop) {
		String tableName = "stops join routesandstops on stops.StopNo = routesandstops.StopNo  join routes on routesandstops.RouteNo = routes.RouteNo";
		Cursor c = db.query(tableName, 
				new String[] { "DISTINCT HeadboardRouteNo"}, 
				"stops.StopNo = "  + stop.getTrackerID() + " and IsMainRoute = 1", 
				null, 
				null, 
				null, 
				"routesandstops.RouteNo", 
				null);
		ArrayList<String> alRoutes = new ArrayList<String>();
		if (c.moveToFirst()) {		
			do{
			alRoutes.add(c.getString(0));
			}while(c.moveToNext());
		}
		c.close();
		String[] allRoutes = new String[alRoutes.size()];
		int i=0;
		for(String route : alRoutes){
			allRoutes[i++] = route;
		}
		return allRoutes;
	}	
	
	//TODO, fix to incorporate changes in db structure
	public SearchResult getSearchResultsGivenKeyword(String keyword, boolean tramStops, boolean shelterStops, boolean easyAccessStops, boolean ticketOutlets, boolean poi){
		SearchResult result = null;
		String moddedKeyword = "%" + keyword + "%";
		if(db == null){
			openDataBase();
		}
		if(!db.isOpen()){
			openDataBase(); 
		}
		if(tramStops || shelterStops || easyAccessStops){
			String columns[] = {"ConnectingTrams", "IsCityStop", "ConnectingBuses", "ConnectingTrains", "IsPlatFormStop", "stops.StopNo AS trackerId", "StopLength", "StopLongitude",
					"StopLatitude", "IsShelter", "IsYTShelter","IsEasyAccess", "StopDescription", "StopName", "FlagStopNo", "StopSuburbName", "Zones", "CityDirection"};
//			StringBuffer selection = new StringBuffer("(StopSuburbName like ? or StopName like ? or stops.StopNo=" + keyword + ")");
			StringBuffer selection = new StringBuffer("(StopSuburbName like ? or StopName like ? or stops.StopNo=?)");
			if(shelterStops && easyAccessStops){
				selection.append(" and (IsShelter=\"1\" or IsYTShelter=\"1\" or IsEasyAccess=\"1\")");
			}
			else{
				if(shelterStops){
					selection.append(" and (IsShelter=\"1\" or IsYTShelter=\"1\")");
				}
				if(easyAccessStops){
					selection.append(" and IsEasyAccess=\"1\"");
				}				
			}

			Cursor c = db.query("stops left outer join connections on stops.StopNo = connections.StopNo", 
					columns, 
					selection.toString(), 
					new String[]{moddedKeyword, moddedKeyword, keyword}, 
					null, 
					null, 
					null, 
					null);
			ArrayList<Stop> alStops = new ArrayList<Stop>();
			if (c.moveToFirst()) {	
				Stop stop;
				if(result == null)
					result = new SearchResult();
				do {
					stop = new Stop();
					getStopFromCursor(c, stop);
					String pois = getPOIForStopAsCSV(stop.getTrackerID());
					if(pois != null)
						stop.setPointsOfInterest(pois);
					ArrayList<String> alRoutes = getRoutesStringArrayForStop(stop);
					String[] arrRoutes = new String[alRoutes.size()];
					int i=0;
					for(String r : alRoutes){
						arrRoutes[i++] = r;
					}
					stop.setRoutes(arrRoutes);
					alStops.add(stop);
				} while(c.moveToNext());
				result.setAlStops(alStops);
			}
			c.close();
		}
		if(ticketOutlets){
			String columns[] = {"TicketOutletId", "Name" , "Address" , "Suburb" , "HasMetcard" , "HasMyki" , "Is24Hour" , "Latitude" , "Longitude" , "HasMykiTopUp"};
			Cursor c = db.query("ticketoutlets", 
					columns, 
					"Name like ? or Suburb like ? or Address like ?", 
					new String[]{moddedKeyword, moddedKeyword, moddedKeyword}, 
					null, 
					null, 
					null, 
					null);
			ArrayList<TicketOutlet> alTicketOutlet = new ArrayList<TicketOutlet>();
			if (c.moveToFirst()) {	
				TicketOutlet to;
				if(result == null)
					result = new SearchResult();
				do {
					to = getTicketOutletFromCursor(c); 
					if(to != null)
						alTicketOutlet.add(to);
				} while(c.moveToNext());
				result.setAlTicketOutlet(alTicketOutlet);
			}
			c.close();
		}
		if(poi){
			String columns[] = {"POIId", "Name" , "POIDescription", "PhoneNumber" , "WebAddress" , "EmailAddress" , "HasDisabledAccess" , "HasToilets" , "OpeningHours" , "HasEntryFee" , 
					"StreetAddress" , "Suburb" , "PostCode" , "Latitude" , "Longitude"};
			Cursor c = db.query("pois", 
					columns, 
					"Name like ? or POIDescription like ? or PostCode like ? or StreetAddress like ? or Suburb like ?", 
					new String[]{moddedKeyword,moddedKeyword,moddedKeyword,moddedKeyword,moddedKeyword}, 
					null, 
					null, 
					null, 
					null);	
			ArrayList<PointOfInterest> alPointOfInterests = new ArrayList<PointOfInterest>();
			if (c.moveToFirst()) {	
				PointOfInterest pointOfInterest;
				if(result == null)
					result = new SearchResult();
				do {
					pointOfInterest = getPointOfInterestFromCursor(c); 
					
					//add connections
					Cursor connections = db.query("poistops join connections on poistops.StopNo = connections.StopNo", 
							new String[]{"ConnectingBuses", "ConnectingTrains","ConnectingTrams"}, 
							"poistops.POIId = " + pointOfInterest.getPoiId(), 
							null, 
							null, 
							null, 
							null, 
							null);	
					if (connections.moveToFirst()) {
						//adding structures to remove duplicates from multiple stops
						ArrayList<String> alConnectingBuses = new ArrayList<String>();
						ArrayList<String> alConnectingTrains = new ArrayList<String>();
						ArrayList<String> alConnectingTrams = new ArrayList<String>();
						String strConn;
						do {
							strConn = connections.getString(connections.getColumnIndex("ConnectingBuses"));
							if(!strConn.equalsIgnoreCase(" ") && !strConn.equalsIgnoreCase("null")){
								String[] strSplit = strConn.split(",");
								for(String comp : strSplit){
									if(!alConnectingBuses.contains(comp))
										alConnectingBuses.add(comp);
								}
							}
							strConn = connections.getString(connections.getColumnIndex("ConnectingTrains"));
							if(!strConn.equalsIgnoreCase(" ") && !strConn.equalsIgnoreCase("null")){
								String[] strSplit = strConn.split(",");
								for(String comp : strSplit){
									if(!alConnectingTrains.contains(comp))
										alConnectingTrains.add(comp);
								}
							}
							strConn = connections.getString(connections.getColumnIndex("ConnectingTrams"));
							if(!strConn.equalsIgnoreCase(" ") && !strConn.equalsIgnoreCase("null")){
								String[] strSplit = strConn.split(",");
								for(String comp : strSplit){
									if(!alConnectingTrams.contains(comp))
										alConnectingTrams.add(comp);
								}
							}
						} while(connections.moveToNext());
						
						Comparator comparator = new Comparator<String>() {
					        @Override
					        public int compare(String s1, String s2) {
					            return s1.compareToIgnoreCase(s2);
					        }
					    };
						Collections.sort(alConnectingBuses, comparator);
						Collections.sort(alConnectingTrains, comparator);
						Collections.sort(alConnectingTrams, comparator);
						
						//Connecting buses
						StringBuffer csv = new StringBuffer();
						for(int i=0;i< alConnectingBuses.size();i++){
							csv.append(alConnectingBuses.get(i) + ",");
						}
						if(csv.length() > 0){
							if(csv.charAt(csv.length() - 1) == ',')
								csv.deleteCharAt(csv.length() - 1);
							pointOfInterest.setConnectingBuses(csv.toString());
						}
						
						//Connecting trains
						csv = new StringBuffer();
						for(int i=0;i< alConnectingTrains.size();i++){
							csv.append(alConnectingTrains.get(i) + ",");
						}
						if(csv.length() > 0){
							if(csv.charAt(csv.length() - 1) == ',')
								csv.deleteCharAt(csv.length() - 1);

							pointOfInterest.setConnectingTrains(csv.toString());
						}
						
						//Connecting trams
						csv = new StringBuffer();
						for(int i=0;i< alConnectingTrams.size();i++){
							csv.append(alConnectingTrams.get(i) + ",");
						}
						if(csv.length() > 0){
							if(csv.charAt(csv.length() - 1) == ',')
								csv.deleteCharAt(csv.length() - 1);
							pointOfInterest.setConnectingTrams(csv.toString());
						}
					}
					connections.close();
					addConnectionsToPOI(pointOfInterest);
					if(pointOfInterest != null)
						alPointOfInterests.add(pointOfInterest);
				} while(c.moveToNext());
//				System.out.println("alPointOfInterests = " + alPointOfInterests);
				result.setAlPointOfInterest(alPointOfInterests);
			}
			c.close();
		}
		return result;
	}
	
	public void addConnectionsToPOI(PointOfInterest pointOfInterest){
		ArrayList<Stop> alStops = getStopsForPOI(pointOfInterest.getPoiId());
		if(alStops != null){
			Set<String> setBuses = new HashSet<String>();
			Set<String> setTrains = new HashSet<String>();
			Set<String> setTrams = new HashSet<String>();
			String[] csv;
			for(Stop stop : alStops){
				if(stop.getConnectingBuses() != null && !stop.getConnectingBuses().equals("") && !stop.getConnectingBuses().equals("null")){
					csv = stop.getConnectingBuses().split(",");
					if(csv != null){
						for(String a : csv){
							if(!a.equalsIgnoreCase("") && !a.equalsIgnoreCase(" "))
								setBuses.add(a);
						}
					}
				}
				if(stop.getConnectingTrains() != null && !stop.getConnectingTrains().equals("") && !stop.getConnectingTrains().equals("null")){
					csv = stop.getConnectingTrains().split(",");
					if(csv != null){
						for(String a : csv){
							if(!a.equalsIgnoreCase("") && !a.equalsIgnoreCase(" "))
								setTrains.add(a);
						}
					}
				}
				if(stop.getConnectingTrams() != null && !stop.getConnectingTrams().equals("") && !stop.getConnectingTrams().equals("null")){
					csv = stop.getConnectingTrams().split(",");
					if(csv != null){
						for(String a : csv){
							if(!a.equalsIgnoreCase("") && !a.equalsIgnoreCase(" "))
								setTrams.add(a);
						}
					}
				}
			}
			StringBuffer sb = new StringBuffer();
			for(String a : setBuses){
				if(!a.equalsIgnoreCase("")){
					if(sb.length() == 0)
						sb.append(a);
					else
						sb.append("," + a);
				}
			}
			pointOfInterest.setConnectingBuses(sb.toString());
			sb = new StringBuffer();
			for(String a : setTrains){
				if(!a.equalsIgnoreCase("")){
					if(sb.length() == 0)
						sb.append(a);
					else
						sb.append("," + a);
				}
			}
			pointOfInterest.setConnectingTrains(sb.toString());
			sb = new StringBuffer();
			for(String a : setTrams){
				if(!a.equalsIgnoreCase("")){
					if(sb.length() == 0)
						sb.append(a);
					else
						sb.append("," + a);
				}
			}
			pointOfInterest.setConnectingTrams(sb.toString());
		}
	}
	
	/**
	 * 
	 * @return ArrayList of FavouritesGroups with group names and order. Needs to be populated with ArrayList<Favourite>
	 */
	public ArrayList<FavouritesGroup> getAllFavouriteGroups(){
		ArrayList<FavouritesGroup> alFavGroups = null;
		if(db == null){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
				return null;
			}
		}
		if(!db.isOpen()){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
				return null;
			}
		}
		if(db.isOpen()){
			try{
				String columns[] = {"Name", "DisplayOrder"};
				Cursor c = db.query("favouritegroup", 
						columns,
						null,
						null,
						null, 
						null, 
						"DisplayOrder", 
						null);	
				FavouritesGroup group;
				if (c.moveToFirst()) {		
					alFavGroups = new ArrayList<FavouritesGroup>();
					do{
						group = new FavouritesGroup();
						group.setName(c.getString(c.getColumnIndex("Name")));
						group.setOrder(c.getInt(c.getColumnIndex("DisplayOrder")));
						alFavGroups.add(group);
					}while(c.moveToNext());
				}
				c.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
			
		return alFavGroups;		
	}
	
	public boolean isStopFavourite(int stopNo){
		boolean result = false;
		if(db == null){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		if(!db.isOpen()){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		try{
			Cursor c = db.query("favouritesandfavgroups", 
					new String[]{"StopNo"},
					"StopNo=" + stopNo,
					null,
					null, 
					null, 
					null, 
					null);
			Log.d("isStopFavourite","c.getCount() = " + c.getCount());
			if(c.getCount() > 0){
				result = true;
			}
			c.close();
		} catch (SQLiteException e) {
			return result;
		}
		return result;
	}
	
	
	/**
	 * Create a favourite with unpopulated stop
	 * @return ArrayList of FavouritesGroups with group names and 
	 */
	public Favourite getFavouriteForStop(int stopNo){
		Favourite favourite = null;
		if(db == null){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		if(!db.isOpen()){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		if(db.isOpen()){
			try{
				String columns[] = {"FavouriteName", "DisplayOrder"};
				Cursor c = db.query("favouritesandfavgroups", 
						columns,
						"StopNo=" + stopNo,
						null,
						null, 
						null, 
						null, 
						null);	
				if (c.moveToFirst()) {	
					do{
						favourite = new Favourite();
						favourite.setName(c.getString(c.getColumnIndex("FavouriteName")));
						favourite.setOrder(c.getInt(c.getColumnIndex("DisplayOrder")));
						Stop stop = new Stop();
						stop.setTrackerID(stopNo);
						favourite.setStop(stop);
					}while(c.moveToNext());
					c.close();
					
					c = db.query("favouritestoproutes", 
							new String[]{"StopNo", "RouteNo"},
							"StopNo=" + stopNo,
							null,
							null, 
							null, 
							null, 
							null);
					ArrayList<String> alRoutes = new ArrayList<String>();
					if (c.moveToFirst()) {	
						alRoutes = new ArrayList<String>();
						do{
							alRoutes.add(c.getString(c.getColumnIndex("RouteNo")));
						}while(c.moveToNext());
					}
					c.close();
					favourite.setAlRoutes(alRoutes);
				}
			}catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		return favourite;
	}
	

	/**
	 * Populate FavouritesGroup with all favourites that are associated with it
	 * @return ArrayList of FavouritesGroups with group names and 
	 */
	public void getFavouritesForGroup(FavouritesGroup group){
		ArrayList<Favourite> alFavs = null;
		if(db == null){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		if(!db.isOpen()){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		if(db.isOpen()){
			try{
				String columns[] = {"FavouriteName", "DisplayOrder", "StopNo"};
				Cursor c = db.query("favouritesandfavgroups", 
						columns,
						"GroupName=?",
						new String[]{group.getName()},
						null, 
						null, 
						"DisplayOrder", 
						null);	
				Favourite favourite;
				if (c.moveToFirst()) {	
					alFavs = new ArrayList<Favourite>();
					Cursor d;
					do{
						favourite = new Favourite();
						favourite.setName(c.getString(c.getColumnIndex("FavouriteName")));
						favourite.setOrder(c.getInt(c.getColumnIndex("DisplayOrder")));
						Stop stop = new Stop();
						stop.setTrackerID(c.getInt(c.getColumnIndex("StopNo")));
						populateStop(stop);
						stop.setRoutes(getRoutesArrayOfStringsForStop(stop));
						favourite.setStop(stop);
						alFavs.add(favourite);
						d = db.query("favouritestoproutes", 
								new String[]{"StopNo", "RouteNo"},
								"StopNo="+favourite.getStop().getTrackerID(),
								null,
								null, 
								null, 
								"RouteNo ASC", 
								null);
						ArrayList<String> alRoutes = new ArrayList<String>();
						if (d.moveToFirst()) {	
							alRoutes = new ArrayList<String>();
							do{
								alRoutes.add(d.getString(d.getColumnIndex("RouteNo")));
							}while(d.moveToNext());
						}
						d.close();
						favourite.setAlRoutes(alRoutes);
//						System.out.println("route list retrieve alRoutes = " + alRoutes);
						group.setFavourites(alFavs);
//						System.out.println("route list retrieve alFavs = " + alFavs);
						for(Favourite favour : alFavs){
//							System.out.println("favour.getAlRoutes() = " + favour.getAlRoutes());
						}
					}while(c.moveToNext());
				}
				c.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Build a route object from a given cursor. Uses the route table
	 * @param Cursor
	 * @return Route
	 */
	private Route getRouteFromCursor(Cursor c) {
		int colRouteNo = c.getColumnIndexOrThrow("HeadboardRouteNo");
		int colRouteDescription = c.getColumnIndexOrThrow("Description");
		int colUpDestination = c.getColumnIndexOrThrow("UpDestination");
		int colDownDestination = c.getColumnIndexOrThrow("DownDestination");
		int colColour = c.getColumnIndexOrThrow("Colour");
		int colMainRoute = c.getColumnIndexOrThrow("IsMainRoute");

		Route route = new Route();
		route.setDescription(c.getString(colRouteDescription));
		String strDownDestination = c.getString(colDownDestination);
		if(strDownDestination == null){
//			System.out.println("strDownDestination = " + strDownDestination);
			strDownDestination = "null in db";
		}
		route.setDownDestination(strDownDestination);
		route.setRouteNumber(c.getString(colRouteNo));
		String strUpDestination = c.getString(colUpDestination);
		if(strUpDestination == null){
//			System.out.println("strUpDestination = " + strUpDestination);
			strUpDestination = "null in db";
		}
		route.setUpDestination(strUpDestination);
		route.setColour(c.getString(colColour));
		route.setMainRoute(c.getInt(colMainRoute)==1?true:false);
		return route;
	}	
	
	
	/**
	 * Build a route object from a given cursor. Uses the route table
	 * @param c
	 * @param route
	 * @return
	 */

	private Route getRouteFromCursor(Cursor c, Route route) {
		int colRouteDescription = c.getColumnIndexOrThrow("Description");
		int colUpDestination = c.getColumnIndexOrThrow("UpDestination");
		int colDownDestination = c.getColumnIndexOrThrow("DownDestination");
		int colColour = c.getColumnIndexOrThrow("Colour");
		int colMainRoute = c.getColumnIndexOrThrow("IsMainRoute");

		route.setDescription(c.getString(colRouteDescription));
		if(route.getDownDestination() == null){
			String strDownDestination = c.getString(colDownDestination);
			if(strDownDestination == null){
//				System.out.println("strDownDestination = " + strDownDestination);
				strDownDestination = "null in db";
			}
			route.setDownDestination(strDownDestination);
//			route.setDownDestination("from db");
		}
		
		if(route.getUpDestination() == null){
			String strUpDestination = c.getString(colUpDestination);
			if(strUpDestination == null){
//				System.out.println("strUpDestination = " + strUpDestination);
				strUpDestination = "null in db";
			}
			route.setUpDestination(strUpDestination);
//			route.setUpDestination("from db");
		}
		route.setColour(c.getString(colColour));
		route.setMainRoute(c.getInt(colMainRoute)==1?true:false);
		return route;
	}
	
	public ArrayList<Route> getAllRoutes(){
		ArrayList<Route> alRoutes = null;
		if(db == null){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		if(!db.isOpen()){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		if(db.isOpen()){
			try{
				String columns[] = {"HeadboardRouteNo", "Description", "DownDestination", "UpDestination", "Colour", "IsMainRoute"};
				Cursor c = db.query("routes", 
						columns,
						"IsMainRoute = 1",
						null,
						null, 
						null, 
						"RouteNo", 
						null);	
				Route route;
				if (c.moveToFirst()) {	
					alRoutes = new ArrayList<Route>();
					do{
						route = getRouteFromCursor(c);
						alRoutes.add(route);
					}while(c.moveToNext());
				}
				c.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}		
		return alRoutes;
	}
	
	public ArrayList<TicketOutletBySuburb> getAllTicketOutlets(){
		TicketOutlet outlet;
		ArrayList<TicketOutletBySuburb> alTicketOutletsBySuburb = new ArrayList<TicketOutletBySuburb>();
		TicketOutletBySuburb ticketOutletBySuburb;
		if(db == null){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		if(!db.isOpen()){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		if(db.isOpen()){
			try{
				Cursor c = db.query("ticketoutlets", 
						null,
						null,
						null,
						null, 
						null, 
						"Suburb, Name", 
						null);	
				if(c.moveToFirst()){
					do{
						outlet = getTicketOutletFromCursor(c);
						if(outlet != null){
							if(alTicketOutletsBySuburb.size() == 0){
								ArrayList<TicketOutlet> al = new ArrayList<TicketOutlet>();
								al.add(outlet);
								ticketOutletBySuburb = new TicketOutletBySuburb(outlet.getSuburb(), al);
								alTicketOutletsBySuburb.add(ticketOutletBySuburb);
							}
							else{
								TicketOutletBySuburb lastTicketOutletBySuburb = alTicketOutletsBySuburb.get(alTicketOutletsBySuburb.size() - 1);
								if(lastTicketOutletBySuburb.getSuburb().equalsIgnoreCase(outlet.getSuburb())){
									ArrayList<TicketOutlet> al  = lastTicketOutletBySuburb.getTicketOutlets();
									al.add(outlet);
								}
								else{
									ArrayList<TicketOutlet> al = new ArrayList<TicketOutlet>();
									al.add(outlet);
									ticketOutletBySuburb = new TicketOutletBySuburb(outlet.getSuburb(), al);
									alTicketOutletsBySuburb.add(ticketOutletBySuburb);
								}
							}
						}
					}while(c.moveToNext());
				}
				c.close();
			}catch (Exception e) {
			}
		}
		return alTicketOutletsBySuburb;
	}
	
	
//	public ArrayList<TicketOutletBySuburb> getTicketOutletsForStops(ArrayList<Stop> alStops){
//		TicketOutlet outlet;
//		ArrayList<TicketOutletBySuburb> alTicketOutletsBySuburb = new ArrayList<TicketOutletBySuburb>();
//		ArrayList<TicketOutlet> alOutlets;
//		ArrayList<TicketOutlet> alTempOutlets;
//		TicketOutletBySuburb ticketOutletBySuburb;
//		if(db == null){
//			try{
//				openDataBase();
//			}catch(SQLiteException sqlex){
//				sqlex.printStackTrace();
//			}
//		}
//		if(!db.isOpen()){
//			try{
//				openDataBase();
//			}catch(SQLiteException sqlex){
//				sqlex.printStackTrace();
//			}
//		}
//		if(db.isOpen()){
//			try{
//				for(Stop stop : alStops){
//					alOutlets = getTicketOutletsForStop(stop);
//					for(int i=0; i<alOutlets.size(); i++){
//						outlet = alOutlets.get(i);
//						if(alTicketOutletsBySuburb.size() == 0){
//							alTempOutlets = new ArrayList<TicketOutlet>();
//							alTempOutlets.add(outlet);
//							ticketOutletBySuburb = new TicketOutletBySuburb(outlet.getSuburb(), alTempOutlets);
//							alTicketOutletsBySuburb.add(ticketOutletBySuburb);
//						}
//						else{
//							for(int j=0; int )
//							TicketOutletBySuburb lastTicketOutletBySuburb = alTicketOutletsBySuburb.get(alTicketOutletsBySuburb.size() - 1);
//							if(lastTicketOutletBySuburb.getSuburb().equalsIgnoreCase(outlet.getSuburb())){
//								ArrayList<TicketOutlet> al  = lastTicketOutletBySuburb.getTicketOutlets();
//								al.add(outlet);
//							}
//							else{
//								ArrayList<TicketOutlet> al = new ArrayList<TicketOutlet>();
//								al.add(outlet);
//								ticketOutletBySuburb = new TicketOutletBySuburb(outlet.getSuburb(), al);
//								alTicketOutletsBySuburb.add(ticketOutletBySuburb);
//							}
//						}
//					}
//				}
//			}catch (Exception e) {
//				
//			}
//		}
//		return alTicketOutletsBySuburb;
//	}
	
	public ArrayList<TicketOutletBySuburb> getTicketOutletsForStops(ArrayList<Stop> alStops){
		TicketOutlet outlet;
		ArrayList<TicketOutletBySuburb> alTicketOutletsBySuburb = new ArrayList<TicketOutletBySuburb>();
		ArrayList<TicketOutlet> alTempOutlets;
		TicketOutletBySuburb ticketOutletBySuburb;
		if(db == null){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		if(!db.isOpen()){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		if(db.isOpen()){
			try{
				StringBuffer sb = new StringBuffer();
				for(int i=0; i<alStops.size(); i++){
					sb.append(alStops.get(i).getTrackerID());
					if(i != alStops.size() - 1){
						sb.append(',');
					}
				}
//				System.out.println("Get stopsandnearbyoutlets " + "StopNo IN (" + sb + ")");
				Cursor c = db.query("stopsandnearbyoutlets", 
						new String[]{"DISTINCT TicketOutletId"},
						"StopNo IN (" + sb + ")",
						null,
						null, 
						null, 
						null, 
						null);	
				long outletId;
				if(c.moveToFirst()){
					do{
						outletId = c.getLong(c.getColumnIndex("TicketOutletId"));
//						System.out.println("Outletid = " + outletId);
						outlet = populateTicketOutlet(outletId);
//						System.out.println("outlet = " + outlet);
						if(alTicketOutletsBySuburb.size() == 0){
							alTempOutlets = new ArrayList<TicketOutlet>();
							alTempOutlets.add(outlet);
							ticketOutletBySuburb = new TicketOutletBySuburb(outlet.getSuburb(), alTempOutlets);
							alTicketOutletsBySuburb.add(ticketOutletBySuburb);
						}
						else{
							TicketOutletBySuburb lastTicketOutletBySuburb = null;
							int i=0;
							for(i=0; i< alTicketOutletsBySuburb.size(); i++){
								lastTicketOutletBySuburb = alTicketOutletsBySuburb.get(i);
								if(lastTicketOutletBySuburb.getSuburb().compareToIgnoreCase(outlet.getSuburb()) > 0){
									if(i > 0)
										lastTicketOutletBySuburb = alTicketOutletsBySuburb.get(i-1);
									else
										i = 0;
									break;
								}
							}
							if(lastTicketOutletBySuburb.getSuburb().equalsIgnoreCase(outlet.getSuburb())){
								ArrayList<TicketOutlet> al  = lastTicketOutletBySuburb.getTicketOutlets();
								al.add(outlet);
							}
							else{
								ArrayList<TicketOutlet> al = new ArrayList<TicketOutlet>();
								al.add(outlet);
								ticketOutletBySuburb = new TicketOutletBySuburb(outlet.getSuburb(), al);
								alTicketOutletsBySuburb.add(i, ticketOutletBySuburb);
							}
						}

					}while(c.moveToNext());
				}
				c.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return alTicketOutletsBySuburb;
	}
	
//	public ArrayList<TicketOutlet> getTicketOutletsForStop(Stop stop){
//		TicketOutlet outlet;
//		ArrayList<TicketOutlet> alTicketOutlets = new ArrayList<TicketOutlet>();
//		if(db == null){
//			try{
//				openDataBase();
//			}catch(SQLiteException sqlex){
//				sqlex.printStackTrace();
//			}
//		}
//		if(!db.isOpen()){
//			try{
//				openDataBase();
//			}catch(SQLiteException sqlex){
//				sqlex.printStackTrace();
//			}
//		}
//		if(db.isOpen()){
//			try{
//				Cursor c = db.query("ticketoutlets", 
//						null,
//						null,
//						null,
//						null, 
//						null, 
//						"Suburb, Name", 
//						null);	
//				if(c.moveToFirst()){
//					do{
//						outlet = getTicketOutletFromCursor(c);
//						alTicketOutlets.add(outlet);
//					}while(c.moveToNext());
//				}
//				c.close();
//			}catch (Exception e) {
//			}
//		}
//		return alTicketOutlets;
//	}
	
	
	public ArrayList<PointOfInterest> getPOIForStop(int stopNo){
		ArrayList<PointOfInterest> alPoi = new ArrayList<PointOfInterest>();
		PointOfInterest poi;
		if(db == null){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		if(!db.isOpen()){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		if(db.isOpen()){
			try{
				Cursor c = db.query("poistops", 
						new String[]{"POIId"},
						"StopNo="+stopNo,
						null,
						null, 
						null, 
						null, 
						null);	
				if(c.moveToFirst()){
					do{
						poi = new PointOfInterest();
						poi.setPoiId(c.getInt(c.getColumnIndex("POIId")));
						populatePOI(poi);
						alPoi.add(poi);
//						System.out.println("alpoi = " + alPoi);
					}while(c.moveToNext());
				}
				c.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return alPoi;
	}
	
	public String getPOIForStopAsCSV(int stopNo){
		StringBuffer csv = new StringBuffer();
		PointOfInterest poi;
		if(db == null){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		if(!db.isOpen()){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		if(db.isOpen()){
			try{
				Cursor c = db.query("poistops join pois on pois.POIId = poistops.POIId", 
						new String[]{"Name"},
						"StopNo="+stopNo,
						null,
						null, 
						null, 
						null, 
						null);	
				if(c.moveToFirst()){
					
					do{
						csv.append(c.getString(c.getColumnIndex("Name")) + ",");
					}while(c.moveToNext());
					if(csv.length() > 0){
						if(csv.charAt(csv.length() - 1) == ',')
							csv.deleteCharAt(csv.length() - 1);
					}
				}
				c.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		if(csv.length() == 0)
			return null;
		return csv.toString();
	}
	
	public void populatePOI(PointOfInterest poi){
		if(db == null){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		if(!db.isOpen()){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		if(db.isOpen()){
			try{
				Cursor c = db.query("pois", 
						null,
						"POIId="+poi.getPoiId(),
						null,
						null, 
						null, 
						null, 
						null);	
				if(c.moveToFirst()){
					do{
//						System.out.println("Has result for " + poi.getPoiId());
						poi.setDescription(c.getString(c.getColumnIndex("POIDescription")));
						poi.setEmail(c.getString(c.getColumnIndex("EmailAddress")).equalsIgnoreCase("null")?"":c.getString(c.getColumnIndex("EmailAddress")));
						poi.setEntryFree(c.getString(c.getColumnIndex("HasEntryFee")));
						poi.setHasDisabled(c.getInt(c.getColumnIndex("HasDisabledAccess")) == 1?true:false);
						poi.setHasToilet(c.getInt(c.getColumnIndex("HasToilets")) == 1?true:false);
						poi.setLatitude(c.getFloat(c.getColumnIndex("Latitude")));
						poi.setLongitude(c.getFloat(c.getColumnIndex("Longitude")));
						poi.setName(c.getString(c.getColumnIndex("Name")));
						poi.setOpeningHours(c.getString(c.getColumnIndex("OpeningHours")).equalsIgnoreCase("null")?"":c.getString(c.getColumnIndex("OpeningHours")));
						poi.setPhone(c.getString(c.getColumnIndex("PhoneNumber")).equalsIgnoreCase("null")?"":c.getString(c.getColumnIndex("PhoneNumber")));
						poi.setPostCode(c.getString(c.getColumnIndex("PostCode")).equalsIgnoreCase("null")?"":c.getString(c.getColumnIndex("PostCode")));
						poi.setStreetAddress(c.getString(c.getColumnIndex("StreetAddress")).equalsIgnoreCase("null")?"":c.getString(c.getColumnIndex("StreetAddress")));
						poi.setSuburb(c.getString(c.getColumnIndex("Suburb")).equalsIgnoreCase("null")?"":c.getString(c.getColumnIndex("Suburb")));
						poi.setWebAddress(c.getString(c.getColumnIndex("WebAddress")).equalsIgnoreCase("null")?"":c.getString(c.getColumnIndex("WebAddress")));
						addConnectionsToPOI(poi);
					}while(c.moveToNext());
				}
				c.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public String getInternalRouteNoForRouteNo(String routeNo){
		String intRouteNo = null;
		if(db == null){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		if(!db.isOpen()){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		if(db.isOpen()){
			try{
				Cursor c = db.query("routes", 
						new String[]{"RouteNo"},
						"HeadboardRouteNo=?",
						new String[]{routeNo},
						null, 
						null, 
						null, 
						null);	
				if(c.getCount() > 0){
					if(c.moveToFirst()){
						do{
							intRouteNo = c.getString(0);
						}while(c.moveToNext());
					}
				}
				if(intRouteNo.equalsIgnoreCase("2"))
					intRouteNo = "4";
				c.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return intRouteNo;
	}
	
	public String getRouteNoForInternalRouteNo(int intRouteNo){
		String routeNo=null;
		if(db == null){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		if(!db.isOpen()){
			try{
				openDataBase();
			}catch(SQLiteException sqlex){
				sqlex.printStackTrace();
			}
		}
		if(db.isOpen()){
			try{
				Cursor c = db.query("routes", 
						new String[]{"HeadboardRouteNo"},
						"RouteNo="+intRouteNo,
						null,
						null, 
						null, 
						null, 
						null);		
				if(c.moveToFirst()){
					do{
						routeNo = c.getString(0);
					}while(c.moveToNext());
				}
				c.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return routeNo;
	}

}
