package com.yarratrams.tramtracker.db;

import java.util.ArrayList;

import com.yarratrams.tramtracker.objects.Favourite;
import com.yarratrams.tramtracker.objects.FavouritesGroup;
import com.yarratrams.tramtracker.objects.PointOfInterest;
import com.yarratrams.tramtracker.objects.Route;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.singleton.TTDBSingleton;
import com.yarratrams.tramtracker.singleton.TTDBUpdateSingleton;

import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;

public class POIManager {
	
//	
//	public POIManager(Context context) {
//		this.context = context;
//		if(ttdbUpdate == null)
//			ttdbUpdate = TTDBUpdateSingleton.getTTDBUpdateInstance(context);
//		ttdbUpdate.createFavouritesTables();
//	}
//	
	public static ArrayList<PointOfInterest> getPOIForStop(int stopNo, Context context){
		TTDB ttdb = TTDBSingleton.getInstance(context);
		ArrayList<PointOfInterest> alPoi = new ArrayList<PointOfInterest>();
		alPoi = ttdb.getPOIForStop(stopNo);
		return alPoi;
	}
	
	public static String getPOIForStopAsCSV(int stopNo, Context context){
		TTDB ttdb = TTDBSingleton.getInstance(context);
		return ttdb.getPOIForStopAsCSV(stopNo);
	}
	
}
