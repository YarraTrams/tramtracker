package com.yarratrams.tramtracker.ui;


import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.ServiceChangeRSSItem;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;


public class ServiceActivity extends Activity {
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_MORE;
	public static final String INTENT_KEY_SERVICECHANGE = "service_change_info";

	private ServiceChangeRSSItem serviceChange;
	
	private WebView webView;
	private ProgressBar progessBar;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.service_screen);
	}
	
	
	public void startWebView() {
		webView = (WebView) findViewById(R.id.web_view);
		progessBar = (ProgressBar) findViewById(R.id.ProgressBar);
		
		WebSettings webSettings = webView.getSettings();

		webSettings.setUserAgentString(webSettings.getUserAgentString());
		webSettings.setJavaScriptEnabled(true);
		webSettings.setSupportZoom(true);
		webSettings.setBuiltInZoomControls(true);
		webSettings.setLoadsImagesAutomatically(true);

		webView.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				progessBar.setVisibility(View.VISIBLE);
				super.onPageStarted(view, url, favicon);
			}
			@Override
			public void onPageFinished(WebView view, String url) {
				progessBar.setVisibility(View.GONE);
			}
		});

		webView.loadDataWithBaseURL(getString(R.string.feed_url_base), serviceChange.getContent(), 
				"text/html", "US-ASCII", null);
	}
	
	
	@Override
	protected void onResume() {
		Bundle extras = getIntent().getExtras(); 
		if(extras != null){
			serviceChange = extras.getParcelable(INTENT_KEY_SERVICECHANGE);
		}
		startWebView();
		
		super.onResume();
	}
	
	
	@Override
	public void onPause() {
		super.onPause();
		webView.stopLoading();
//		try {
//			Class.forName("android.webkit.WebView").getMethod("onPause", (Class[]) null).invoke(webView, (Object[]) null);
//		} catch (Exception e) {
//		}
	}
	
	
	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	
}
