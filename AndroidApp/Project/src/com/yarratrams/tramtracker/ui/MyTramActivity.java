package com.yarratrams.tramtracker.ui;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.PredictedTimeResult;
import com.yarratrams.tramtracker.objects.Tram;
import com.yarratrams.tramtracker.tasks.MyTramTask;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;


public class MyTramActivity extends Activity implements OnClickListener{
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_MYTRAM;

	private Tram tram;
	private ProgressDialog loadDialog;
	private InputMethodManager imm;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mytram_entry_screen);
		
		Button onBoardButton = (Button) findViewById(R.id.search_button);
		onBoardButton.setOnClickListener(this);
		
		final EditText tramNoEditText = (EditText) findViewById(R.id.mytram_tramno);
		tramNoEditText.setOnClickListener(this);
		tramNoEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
		    @Override
		    public void onFocusChange(View v, boolean hasFocus) {
		        if(hasFocus){
		            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
		        }
		    }
		});
		tramNoEditText.setOnEditorActionListener(new OnEditorActionListener() {        
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if(actionId==EditorInfo.IME_ACTION_DONE || event.getKeyCode() == KeyEvent.KEYCODE_ENTER){
					search(tramNoEditText);
				}
				return false;
			}
		});
		
		tram = new Tram();
		loadDialog = new ProgressDialog(this);
	}
	
	
	private Context getDialogContext() {
		Context context;
		if (getParent() != null){
			context = getParent();
		} else {
			context = this;
		}
		return context;
	}
	
	
	@Override
	public void onClick(View v) {
		EditText tramNoEditText = (EditText) findViewById(R.id.mytram_tramno);
		Button onBoardButton = (Button) findViewById(R.id.search_button);
		if(v == onBoardButton){
			search(tramNoEditText);
		
		} else if(v == tramNoEditText){
			tramNoEditText.clearFocus();
			imm.showSoftInput(tramNoEditText, InputMethodManager.SHOW_FORCED);
		}
	}
	
	private void search(EditText tramNoEditText){
		TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_mytram));
		if(tramNoEditText.length() < 1){
			TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.error_mytram_nonumber));
			return;
		}
		
		imm.hideSoftInputFromWindow(tramNoEditText.getWindowToken(), 0);
		
		if(!loadDialog.isShowing()){
			loadDialog = ProgressDialog.show(getDialogContext(), "", getResources().getString(R.string.dialog_loading), true, true);
		}
		tram.setVehicleNumber(Integer.parseInt(tramNoEditText.getText().toString()));
		MyTramTask task = new MyTramTask(this, tram);
		task.execute();
	}
	
	
	public void updateUI(PredictedTimeResult results) {
		if(loadDialog.isShowing()){
			loadDialog.dismiss();
		}
		if(results == null){
//			TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.error_mytram_invalidtram));
		} else {
			goToOnBoardScreen(tram);
		}
	}
	
	
	private void goToOnBoardScreen(Tram tram){
		Intent intent = new Intent(this, OnBoardActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(OnBoardActivity.INTENT_KEY, tram);
		
		TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MYTRAM, 
				getResources().getString(R.string.tag_onboard_screen), intent);
	}
	
	@Override
	protected void onResume() {
		EditText tramNoEditText = (EditText) findViewById(R.id.mytram_tramno);
		tramNoEditText.clearFocus();
		imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		super.onResume();
	}
	
	
	@Override
	protected void onPause() {
		EditText tramNoEditText = (EditText) findViewById(R.id.mytram_tramno);
		imm.hideSoftInputFromWindow(tramNoEditText.getWindowToken(), 0);
		super.onPause();
	}


	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}
	
}
