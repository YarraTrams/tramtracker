package com.yarratrams.tramtracker.ui;

import java.util.ArrayList;
import java.util.List;

import android.app.ActivityGroup;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.ViewFlipper;

import com.yarratrams.tramtracker.R;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.google.android.maps.Overlay;
import com.yarratrams.tramtracker.objects.NearbyStop;
import com.yarratrams.tramtracker.objects.NearbyTicketOutlet;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.tasks.NearbyGivenLocationTask;
import com.yarratrams.tramtracker.tasks.NearbyStopsTask;
import com.yarratrams.tramtracker.ui.util.MapEasyStopOverlay;
import com.yarratrams.tramtracker.ui.util.MapOutletOverlay;
import com.yarratrams.tramtracker.ui.util.MapShelterStopOverlay;
import com.yarratrams.tramtracker.ui.util.MapStopOverlay;
import com.yarratrams.tramtracker.ui.util.MenuNearbyAdapter;
import com.yarratrams.tramtracker.ui.util.NearbyOutletListArrayAdapter;
import com.yarratrams.tramtracker.ui.util.NearbyStopListArrayAdapter;
import com.yarratrams.tramtracker.ui.util.SensibleMapView;
import com.yarratrams.tramtracker.ui.util.SensibleMapView.OnPanListener;
import com.yarratrams.tramtracker.ui.util.WrappingSlidingDrawer;

public class NearbyActivity extends MapActivity implements OnClickListener{
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_NEARBY;
	public static final String INTENT_CENTRE_KEY = "map_centre";
	public static final int VIEW_LIST = 0;
	public static final int VIEW_MAP = 1;
	
	public static final int FILTER_STOPS_ALL = 0;
	public static final int FILTER_STOPS_SHELTER = 1;
	public static final int FILTER_STOPS_ACCESS = 2;
	public static final int FILTER_OUTLETS = 3;
	
	public static final int ACTION_REFRESH = 0;
	public static final int ACTION_PAN = 1;
	public static final int SOURCE_FROMPID = 0;
	public static final int SOURCE_TABCLICK = 1;
	
	private boolean isListViewSelected;
	private ViewFlipper flipper;
	
	private ListView nearbyList;
	private TextView listTitle;
	
	private SensibleMapView nearbyMap;
	private List<Overlay> mapOverlays;
	public MyLocationOverlay myOverlay;
	
	private ProgressDialog loadDialog;
	private ArrayList<NearbyStop> stopList;
	private ArrayList<NearbyStop> shelterList;
	private ArrayList<NearbyStop> easyList;
	private ArrayList<NearbyTicketOutlet> outletList;
	private NearbyOutletListArrayAdapter outletAdapter;
	private NearbyStopListArrayAdapter stopAdapter;
	
	private int action;
	private int source;
	private int filter;
	private NearbyStopsTask nearbyUpdateTask;
	private NearbyGivenLocationTask nearbyGivenLocationTask;
	private GeoPoint currentCentre;
	private boolean activeScreen;
	
	private WrappingSlidingDrawer slidingMenu;

	
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nearby_screen);

		isListViewSelected = true;
		flipper = (ViewFlipper) findViewById(R.id.view_flipper);
		
		ToggleButton listButton = (ToggleButton) findViewById(R.id.list_view_button);
		listButton.setOnClickListener(this);
		ToggleButton mapButton = (ToggleButton) findViewById(R.id.map_view_button);
		mapButton.setOnClickListener(this);
		Button refreshButton = (Button) findViewById(R.id.refresh_button);
		refreshButton.setOnClickListener(this);
		
		filter = NearbyActivity.FILTER_STOPS_ALL;
        listTitle = (TextView) findViewById(R.id.list_title);
		nearbyList = (ListView) findViewById(R.id.simple_list);
		stopList = new ArrayList<NearbyStop>();
		shelterList = new ArrayList<NearbyStop>();
		easyList = new ArrayList<NearbyStop>();
		outletList = new ArrayList<NearbyTicketOutlet>();
		stopAdapter = new NearbyStopListArrayAdapter(this, new ArrayList<NearbyStop>());
		nearbyList.setAdapter(stopAdapter);
		
		nearbyMap = (SensibleMapView)findViewById(R.id.map);
		nearbyMap.setBuiltInZoomControls(true);
        mapOverlays = nearbyMap.getOverlays();
        myOverlay = new MyLocationOverlay(this, nearbyMap);
        
        final NearbyActivity activity = this;
        nearbyMap.setOnPanListener(new OnPanListener() {
			@Override
			public void onPan(MapView view, GeoPoint oldCenter, GeoPoint newCenter) {
				action = NearbyActivity.ACTION_PAN;
				
				Location location = new Location("");
				location.setLatitude(newCenter.getLatitudeE6() / 1E6);
				location.setLongitude(newCenter.getLongitudeE6() / 1E6);
				
				if(source == NearbyActivity.SOURCE_TABCLICK){
					nearbyGivenLocationTask = new NearbyGivenLocationTask(activity, location, nearbyUpdateTask.getLocation());
				} else if(source == NearbyActivity.SOURCE_FROMPID){
					Location sourceLocation = new Location("");
					sourceLocation.setLatitude(currentCentre.getLatitudeE6() / 1E6);
					sourceLocation.setLongitude(currentCentre.getLongitudeE6() / 1E6);
					nearbyGivenLocationTask = new NearbyGivenLocationTask(activity, location, sourceLocation);
				}
				nearbyGivenLocationTask.execute();
			}
		});
        
        slidingMenu = (WrappingSlidingDrawer) findViewById(R.id.sliding_menu);
        GridView menuContainer = (GridView) findViewById(R.id.sliding_menu_container);
        MenuNearbyAdapter menuAdapter = new MenuNearbyAdapter(this);
        menuContainer.setAdapter(menuAdapter);
        
        loadDialog = new ProgressDialog(this);
        
        //System.out.println("....... TEST: Nearby on create finished");
	}

	public Context getDialogContext() {
		Context context;
		if (getParent() != null) {
			context = getParent();
		} else {
			context = this;
		}
		return context;
	}
	
	public void update(){
		//System.out.println("Nearby update");
		
		if(!activeScreen){
			return;
		}
		
		updateUI();
	}
	
	public void updateUI() {
		if(source == NearbyActivity.SOURCE_TABCLICK){
			if(action == NearbyActivity.ACTION_REFRESH){
				stopList = nearbyUpdateTask.getNearbyStops();
				shelterList = nearbyUpdateTask.getShelterStops();
				easyList = nearbyUpdateTask.getEasyStops();
				outletList = nearbyUpdateTask.getNearbyOutlets();

				filter();
				centerMapOnMe();

			} else if(action == NearbyActivity.ACTION_PAN){
				ArrayList<NearbyStop> newStops = nearbyGivenLocationTask.getNearbyStops();
				ArrayList<NearbyStop> newShelters = nearbyGivenLocationTask.getShelterStops();
				ArrayList<NearbyStop> newEasy = nearbyGivenLocationTask.getEasyStops();
				ArrayList<NearbyTicketOutlet> newOutlets = nearbyGivenLocationTask.getNearbyOutlets();
				
				if(newStops != null){
					newStops = appendStops(newStops);
				} else {
					newStops = new ArrayList<NearbyStop>();
				}
				if(newShelters != null){
					newShelters = appendShelters(newShelters);
				} else {
					newShelters = new ArrayList<NearbyStop>();
				}
				if(newEasy != null){
					newEasy = appendEasy(newEasy);
				} else {
					newEasy = new ArrayList<NearbyStop>();
				}
				if(newOutlets != null){
					newOutlets = appendOutlets(newOutlets);
				} else {
					newOutlets = new ArrayList<NearbyTicketOutlet>();
				}
				append(newStops, newShelters, newEasy, newOutlets);
				
			} 
		} else if(source == NearbyActivity.SOURCE_FROMPID){
			if(action == NearbyActivity.ACTION_REFRESH){
				stopList = nearbyGivenLocationTask.getNearbyStops();
				shelterList = nearbyGivenLocationTask.getShelterStops();
				easyList = nearbyGivenLocationTask.getEasyStops();
				outletList = nearbyGivenLocationTask.getNearbyOutlets();

				filter();
				centerMapOn(currentCentre);

			} else if(action == NearbyActivity.ACTION_PAN){
				ArrayList<NearbyStop> newStops = nearbyGivenLocationTask.getNearbyStops();
				ArrayList<NearbyStop> newShelters = nearbyGivenLocationTask.getShelterStops();
				ArrayList<NearbyStop> newEasy = nearbyGivenLocationTask.getEasyStops();
				ArrayList<NearbyTicketOutlet> newOutlets = nearbyGivenLocationTask.getNearbyOutlets();
				
				if(newStops != null){
					newStops = appendStops(newStops);
				} else {
					newStops = new ArrayList<NearbyStop>();
				}
				if(newShelters != null){
					newShelters = appendShelters(newShelters);
				} else {
					newShelters = new ArrayList<NearbyStop>();
				}
				if(newEasy != null){
					newEasy = appendEasy(newEasy);
				} else {
					newEasy = new ArrayList<NearbyStop>();
				}
				if(newOutlets != null){
					newOutlets = appendOutlets(newOutlets);
				} else {
					newOutlets = new ArrayList<NearbyTicketOutlet>();
				}
				append(newStops, newShelters, newEasy, newOutlets);
				
			} 
		} 
		if(loadDialog.isShowing()){
			loadDialog.dismiss();
		}
	}
	
	private ArrayList<NearbyStop> appendStops(ArrayList<NearbyStop> newStops){
		ArrayList<NearbyStop> newList = new ArrayList<NearbyStop>();
		int index = 0;
		
		for(NearbyStop newStop: newStops){
			if(!containsStop(stopList, newStop)){
				for(;index <= stopList.size(); index++){
					if(index == stopList.size()){
						stopList.add(index, newStop);
						break;
					} else {
						NearbyStop stop = stopList.get(index);
						if(newStop.getIntDistance() < stop.getIntDistance()){
							stopList.add(index, newStop);
							break;
						}
					}
				}
				newList.add(newStop);
			}
		}
		return newList;
	}
	
	private ArrayList<NearbyStop> appendShelters(ArrayList<NearbyStop> newShelters){
		ArrayList<NearbyStop> newList = new ArrayList<NearbyStop>();
		int index = 0;
		for(NearbyStop newStop: newShelters){
			if(!containsStop(shelterList, newStop)){
				for(;index <= shelterList.size(); index++){
					if(index == shelterList.size()){
						shelterList.add(index, newStop);
						break;
					} else {
						NearbyStop stop = shelterList.get(index);
						if(newStop.getIntDistance() < stop.getIntDistance()){
							shelterList.add(index, newStop);
							break;
						}
					}
				}
				newList.add(newStop);
			}
		}
		return newList;
	}
	
	private ArrayList<NearbyStop> appendEasy(ArrayList<NearbyStop> newEasy){
		ArrayList<NearbyStop> newList = new ArrayList<NearbyStop>();
		int index = 0;
		for(NearbyStop newStop: newEasy){
			if(!containsStop(easyList, newStop)){
				for(;index <= easyList.size(); index++){
					if(index == easyList.size()){
						easyList.add(index, newStop);
						break;
					} else {
						NearbyStop stop = easyList.get(index);
						if(newStop.getIntDistance() < stop.getIntDistance()){
							easyList.add(index, newStop);
							break;
						}
					}
				}
				newList.add(newStop);
			}
		}
		return newList;
	}
	
	private boolean containsStop(ArrayList<NearbyStop> original, NearbyStop newStop){
		for(NearbyStop stop: original){
			if(newStop.getStop().getTrackerID() == stop.getStop().getTrackerID()){
				return true;
			}
		}
		return false;
	}

	
	private ArrayList<NearbyTicketOutlet> appendOutlets(ArrayList<NearbyTicketOutlet> newOutlets){
		ArrayList<NearbyTicketOutlet> newList = new ArrayList<NearbyTicketOutlet>(); 
		int index = 0;
		for(NearbyTicketOutlet newOutlet: newOutlets){
			if(!containsOutlet(newOutlet)){
				for(;index <= outletList.size(); index++){
					if(index == outletList.size()){
						outletList.add(index, newOutlet);
						break;
					} else{
						NearbyTicketOutlet outlet = outletList.get(index);
						if(newOutlet.getIntDistance() < outlet.getIntDistance()){
							outletList.add(index, newOutlet);
							break;
						}
					}
				}
				newList.add(newOutlet);
			}
		}
		return newList;
	}
	
	private boolean containsOutlet(NearbyTicketOutlet newOutlet){
		for(NearbyTicketOutlet outlet: outletList){
			if(newOutlet.getTicketOutlet().getOutletId() == outlet.getTicketOutlet().getOutletId()){
				return true;
			}
		}
		return false;
	}
	
	
	public void filter(){
		mapOverlays.clear();
		mapOverlays.add(myOverlay);
		
		switch (filter) {
			case NearbyActivity.FILTER_STOPS_ALL :
				// Setup list
				if(nearbyList.getAdapter() instanceof NearbyStopListArrayAdapter){
					stopAdapter.updateList(stopList);
				} else {
					ArrayList<NearbyStop> resultsList = new ArrayList<NearbyStop>(stopList);
					stopAdapter = new NearbyStopListArrayAdapter(this, resultsList);
					nearbyList.setAdapter(stopAdapter);
				}
				
				listTitle.setText(getResources().getString(R.string.nearby_title_all));
				
				// Setup map
				MapStopOverlay overlay = new MapStopOverlay(this, nearbyMap);
				overlay.updateTramStopsList(stopList);
		        mapOverlays.add(overlay);
		        
//		        if(stopList.isEmpty()){
//		        	TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.notify_nearby_nostops));
//		        }
		        
				break;

			case NearbyActivity.FILTER_STOPS_SHELTER :
				// Setup list
				//ArrayList<NearbyStop> shelters = getStopsWithShelter(stopList);
				if(nearbyList.getAdapter() instanceof NearbyStopListArrayAdapter){
					stopAdapter.updateList(shelterList);
				} else {
					ArrayList<NearbyStop> resultsList = new ArrayList<NearbyStop>(shelterList);
					stopAdapter = new NearbyStopListArrayAdapter(this, resultsList);
					nearbyList.setAdapter(stopAdapter);
				}
				
				listTitle.setText(getResources().getString(R.string.nearby_title_shelters));
				
				// Setup map
				MapShelterStopOverlay shelterOverlay = new MapShelterStopOverlay(this, nearbyMap);
				shelterOverlay.updateTramStopsList(shelterList);
		        mapOverlays.add(shelterOverlay);
		        
//		        if(shelterList.isEmpty()){
//		        	TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.notify_nearby_noshelters));
//		        }
		        
				break;
				
			case NearbyActivity.FILTER_STOPS_ACCESS :
				// Setup list
				//ArrayList<NearbyStop> access = getStopsWithEasyAccess(stopList);
				if(nearbyList.getAdapter() instanceof NearbyStopListArrayAdapter){
					stopAdapter.updateList(easyList);
				} else {
					ArrayList<NearbyStop> resultsList = new ArrayList<NearbyStop>(easyList);
					stopAdapter = new NearbyStopListArrayAdapter(this, resultsList);
					nearbyList.setAdapter(stopAdapter);
				}
				
				listTitle.setText(getResources().getString(R.string.nearby_title_accessible));
				
				// Setup map
				MapEasyStopOverlay easyOverlay = new MapEasyStopOverlay(this, nearbyMap);
				easyOverlay.updateTramStopsList(easyList);
		        mapOverlays.add(easyOverlay);
				
//		        if(easyList.isEmpty()){
//		        	TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.notify_nearby_noeasyaccess));
//		        }
		        
				break;
				
			case NearbyActivity.FILTER_OUTLETS :
				// Setup list
				if(nearbyList.getAdapter() instanceof NearbyOutletListArrayAdapter){
					outletAdapter.updateList(outletList);
				} else {
					ArrayList<NearbyTicketOutlet> resultsList = new ArrayList<NearbyTicketOutlet>(outletList);
					outletAdapter = new NearbyOutletListArrayAdapter(this, resultsList);
					nearbyList.setAdapter(outletAdapter);
				}
				
				listTitle.setText(getResources().getString(R.string.nearby_title_outlets));
				
				// Setup map
				MapOutletOverlay outletOverlay = new MapOutletOverlay(this, nearbyMap);
				outletOverlay.updateTicketOutletsList(outletList);
		        mapOverlays.add(outletOverlay);
				
//		        if(outletList.isEmpty()){
//		        	TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.notify_nearby_nooutlets));
//		        }
		        
				break;
				
			default :
				break;
		}
		nearbyMap.invalidate();
	}
	
	public void append(ArrayList<NearbyStop> newStops, ArrayList<NearbyStop> newShelters, 
			ArrayList<NearbyStop> newEasy, ArrayList<NearbyTicketOutlet> newOutlets){
		switch (filter) {
			case NearbyActivity.FILTER_STOPS_ALL :
				// Setup list
				if(nearbyList.getAdapter() instanceof NearbyStopListArrayAdapter){
					stopAdapter.updateList(stopList);
				} else {
					ArrayList<NearbyStop> resultsList = new ArrayList<NearbyStop>(stopList);
					stopAdapter = new NearbyStopListArrayAdapter(this, resultsList);
					nearbyList.setAdapter(stopAdapter);
				}
				
				// Setup map
				MapStopOverlay overlay = new MapStopOverlay(this, nearbyMap);
				overlay.updateTramStopsList(newStops);
		        mapOverlays.add(overlay);
		        
				break;

			case NearbyActivity.FILTER_STOPS_SHELTER :
				// Setup list
				if(nearbyList.getAdapter() instanceof NearbyStopListArrayAdapter){
					stopAdapter.updateList(shelterList);
				} else {
					ArrayList<NearbyStop> resultsList = new ArrayList<NearbyStop>(shelterList);
					stopAdapter = new NearbyStopListArrayAdapter(this, resultsList);
					nearbyList.setAdapter(stopAdapter);
				}
				
				// Setup map
				MapShelterStopOverlay shelterOverlay = new MapShelterStopOverlay(this, nearbyMap);
				shelterOverlay.updateTramStopsList(newShelters);
		        mapOverlays.add(shelterOverlay);
		        
				break;
				
			case NearbyActivity.FILTER_STOPS_ACCESS :
				// Setup list
				if(nearbyList.getAdapter() instanceof NearbyStopListArrayAdapter){
					stopAdapter.updateList(easyList);
				} else {
					ArrayList<NearbyStop> resultsList = new ArrayList<NearbyStop>(easyList);
					stopAdapter = new NearbyStopListArrayAdapter(this, resultsList);
					nearbyList.setAdapter(stopAdapter);
				}
				
				// Setup map
				MapEasyStopOverlay easyOverlay = new MapEasyStopOverlay(this, nearbyMap);
				easyOverlay.updateTramStopsList(newEasy);
		        mapOverlays.add(easyOverlay);
				
				break;
				
			case NearbyActivity.FILTER_OUTLETS :
				// Setup list
				if(nearbyList.getAdapter() instanceof NearbyOutletListArrayAdapter){
					outletAdapter.updateList(outletList);
				} else {
					ArrayList<NearbyTicketOutlet> resultsList = new ArrayList<NearbyTicketOutlet>(outletList);
					outletAdapter = new NearbyOutletListArrayAdapter(this, resultsList);
					nearbyList.setAdapter(outletAdapter);
				}
				
				// Setup map
				MapOutletOverlay outletOverlay = new MapOutletOverlay(this, nearbyMap);
				outletOverlay.updateTicketOutletsList(newOutlets);
		        mapOverlays.add(outletOverlay);
				
				break;
				
			default :
				break;
		}
		nearbyMap.invalidate();
	}
	
	public void centerMapOnMe(){
		centerMapOn(myOverlay.getMyLocation());
	}
	
	public void centerMapOn(GeoPoint center){
		MapController mapController = nearbyMap.getController();
		//mapController.setZoom(17); // Zoom 1 is world view
		setMapZoomLevel(mapController, center);
		if(center != null){
			mapController.animateTo(center);
		}
	}
	
	public void setMapZoomLevel(MapController mapController, GeoPoint center){
		if(center == null){
			mapController.setZoom(17);
			return;
		}
		int maxLat = center.getLatitudeE6();
		int minLat = center.getLatitudeE6();
		int maxLon = center.getLongitudeE6();
		int minLon = center.getLongitudeE6();
		
		switch (filter) {
			case NearbyActivity.FILTER_STOPS_ALL :
				for(NearbyStop stop: stopList){
					maxLat = stop.getStop().getLatitudeE6() > maxLat? stop.getStop().getLatitudeE6(): maxLat;
					minLat = stop.getStop().getLatitudeE6() < minLat? stop.getStop().getLatitudeE6(): minLat;
					maxLon = stop.getStop().getLongitudeE6() > maxLon? stop.getStop().getLongitudeE6(): maxLon;
					minLon = stop.getStop().getLongitudeE6() < minLon? stop.getStop().getLongitudeE6(): minLon;
				}
				break;
				
			case NearbyActivity.FILTER_STOPS_SHELTER :
				for(NearbyStop stop: shelterList){
					maxLat = stop.getStop().getLatitudeE6() > maxLat? stop.getStop().getLatitudeE6(): maxLat;
					minLat = stop.getStop().getLatitudeE6() < minLat? stop.getStop().getLatitudeE6(): minLat;
					maxLon = stop.getStop().getLongitudeE6() > maxLon? stop.getStop().getLongitudeE6(): maxLon;
					minLon = stop.getStop().getLongitudeE6() < minLon? stop.getStop().getLongitudeE6(): minLon;
				}
				break;
				
			case NearbyActivity.FILTER_STOPS_ACCESS :
				for(NearbyStop stop: easyList){
					maxLat = stop.getStop().getLatitudeE6() > maxLat? stop.getStop().getLatitudeE6(): maxLat;
					minLat = stop.getStop().getLatitudeE6() < minLat? stop.getStop().getLatitudeE6(): minLat;
					maxLon = stop.getStop().getLongitudeE6() > maxLon? stop.getStop().getLongitudeE6(): maxLon;
					minLon = stop.getStop().getLongitudeE6() < minLon? stop.getStop().getLongitudeE6(): minLon;
				}
				break;
				
			case NearbyActivity.FILTER_OUTLETS :
				for(NearbyTicketOutlet outlet: outletList){
					maxLat = outlet.getTicketOutlet().getLatitudeE6() > maxLat? outlet.getTicketOutlet().getLatitudeE6(): maxLat;
					minLat = outlet.getTicketOutlet().getLatitudeE6() < minLat? outlet.getTicketOutlet().getLatitudeE6(): minLat;
					maxLon = outlet.getTicketOutlet().getLongitudeE6() > maxLon? outlet.getTicketOutlet().getLongitudeE6(): maxLon;
					minLon = outlet.getTicketOutlet().getLongitudeE6() < minLon? outlet.getTicketOutlet().getLongitudeE6(): minLon;
				}
				break;
				
			default :
				break;
		}

		int maxCentreLat = maxLat - center.getLatitudeE6() > center.getLatitudeE6() - minLat? maxLat - center.getLatitudeE6() : center.getLatitudeE6() - minLat;
		int maxCentreLon = maxLon - center.getLongitudeE6() > center.getLongitudeE6() - minLon? maxLon - center.getLongitudeE6() : center.getLongitudeE6() - minLon;
		int dLat = maxCentreLat * 2;
		int dLon = maxCentreLon * 2;
		
		mapController.zoomToSpan(dLat, dLon);
	}

	
	
	@Override
	public void onResume() {
		super.onResume();
		activeScreen = true;
		action = NearbyActivity.ACTION_REFRESH;
		myOverlay.enableMyLocation();

		if(!loadDialog.isShowing()){
			loadDialog = ProgressDialog.show(getDialogContext(), "", getResources().getString(R.string.dialog_loading), true, true);
		}
		
		Bundle extras = getIntent().getExtras(); 
		if(extras != null){
			source = NearbyActivity.SOURCE_FROMPID;
			filter = NearbyActivity.FILTER_OUTLETS;
			
			Stop stop = extras.getParcelable(INTENT_CENTRE_KEY);
			
			Location location = new Location("");
			location.setLatitude(stop.getLatitudeE6() / 1E6);
			location.setLongitude(stop.getLongitudeE6() / 1E6);
			currentCentre = new GeoPoint(stop.getLatitudeE6(), stop.getLongitudeE6());
			centerMapOn(currentCentre);
			nearbyGivenLocationTask = new NearbyGivenLocationTask(this, location, location);
			nearbyGivenLocationTask.execute();
			
		} else {
			source = NearbyActivity.SOURCE_TABCLICK;
			//centerMapOnMe();
			
        	nearbyUpdateTask = new NearbyStopsTask(this);
        	nearbyUpdateTask.execute();
		}
	}
	
	@Override
	public void onPause() {
		super.onPause();
		activeScreen = false;
		myOverlay.disableMyLocation();
		
		if(nearbyUpdateTask != null){
			nearbyUpdateTask.cancelTask(true);
			nearbyUpdateTask.cancel(true);
		}
		if(nearbyGivenLocationTask != null){
			nearbyGivenLocationTask.cancel(true);
		}
		
		if(stopList != null){
			stopList.clear();
			shelterList.clear();
			easyList.clear();
			outletList.clear();
		}

		if(loadDialog.isShowing()){
			loadDialog.dismiss();
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();

		if(stopList != null){
			stopList.clear();
			shelterList.clear();
			easyList.clear();
			outletList.clear();
		}
		stopList = null;
		shelterList = null;
		easyList = null;
		outletList = null;	
		System.gc();
	}
	
	private void switchViews(){
		ToggleButton listButton = (ToggleButton) findViewById(R.id.list_view_button);
		ToggleButton mapButton = (ToggleButton) findViewById(R.id.map_view_button);
		if (isListViewSelected) {
			isListViewSelected = false;
			listButton.setChecked(false);
			mapButton.setChecked(true);
	        flipper.setDisplayedChild(VIEW_MAP);
	        TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_header_mapview));
		} else {
			isListViewSelected = true;
			listButton.setChecked(true);
			mapButton.setChecked(false);
	        flipper.setDisplayedChild(VIEW_LIST);
	        TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_header_listview));
		}
	}
	
	private void refresh(){
		if(!loadDialog.isShowing()){
			loadDialog = ProgressDialog.show(getDialogContext(), "", getResources().getString(R.string.dialog_loading), true, true);
		}
		action = NearbyActivity.ACTION_REFRESH;

		if(nearbyUpdateTask != null){
			nearbyUpdateTask.cancelTask(true);
			nearbyUpdateTask.cancel(true);
		}
		if(nearbyGivenLocationTask != null){
			nearbyGivenLocationTask.cancel(true);
		}
		
		if(source == NearbyActivity.SOURCE_TABCLICK){
			nearbyUpdateTask = new NearbyStopsTask(this);
			nearbyUpdateTask.execute();			
		} else if(source == NearbyActivity.SOURCE_FROMPID){
			Location location = new Location("");
			location.setLatitude(currentCentre.getLatitudeE6() / 1E6);
			location.setLongitude(currentCentre.getLongitudeE6() / 1E6);
			nearbyGivenLocationTask = new NearbyGivenLocationTask(this, location, location);
			nearbyGivenLocationTask.execute();
		}

	}
	
	// STOP FILTERS: NOT USED SINCE 1.3
	private ArrayList<NearbyStop> getStopsWithShelter(ArrayList<NearbyStop> origin){
		ArrayList<NearbyStop> list = new ArrayList<NearbyStop>();
		for(NearbyStop stop : origin){
			if(stop.getStop().isHasShelter()){
				list.add(stop);
			}
		}
		return list;
	}
	private ArrayList<NearbyStop> getStopsWithEasyAccess(ArrayList<NearbyStop> origin){
		ArrayList<NearbyStop> list = new ArrayList<NearbyStop>();
		
		for(NearbyStop stop : origin){
			if(stop.getStop().isEasyAccessStop()){
				list.add(stop);
			}
		}
		return list;
	}
	

	@Override
	public void onClick(View v) {
		ToggleButton listButton = (ToggleButton) findViewById(R.id.list_view_button);
		ToggleButton mapButton = (ToggleButton) findViewById(R.id.map_view_button);
		Button refreshButton = (Button) findViewById(R.id.refresh_button);
		if (v == listButton || v == mapButton) {
			switchViews();
		} else if(v == refreshButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_header_refresh));
			refresh();
		}
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	@Override
	public void onBackPressed() {
		if(nearbyUpdateTask != null)
			nearbyUpdateTask.removeUpdates();
		TramTrackerMainActivity.getAppManager().back();
	}

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if(source != NearbyActivity.SOURCE_FROMPID){
//			MenuInflater inflater = getMenuInflater();
//			inflater.inflate(R.menu.nearby_menu, menu);
//			TramTrackerMainActivity.getAppManager().setMenuBackground(this);
		}
		return true;
	}
	
	@Override
	public void openOptionsMenu() {
		slidingMenu.toggle();
	}
	@Override
	public void closeOptionsMenu() {
		slidingMenu.close();
	}
	
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_MENU){
			openOptionsMenu();
			return true;
		} else if(keyCode == KeyEvent.KEYCODE_BACK){
			onBackPressed();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent;
		switch (item.getItemId()) {
			case R.id.menu_nearby_allStops :
				filter = NearbyActivity.FILTER_STOPS_ALL; 
				filter();
				setMapZoomLevel(nearbyMap.getController(), nearbyMap.getMapCenter());
				return true;
				
			case R.id.menu_nearby_shelters :
				filter = NearbyActivity.FILTER_STOPS_SHELTER;
				filter();
				setMapZoomLevel(nearbyMap.getController(), nearbyMap.getMapCenter());
				return true;
				
			case R.id.menu_nearby_easyAccess :
				filter = NearbyActivity.FILTER_STOPS_ACCESS;
				filter();
				setMapZoomLevel(nearbyMap.getController(), nearbyMap.getMapCenter());
				return true;
				
			case R.id.menu_nearby_outlets :
				filter = NearbyActivity.FILTER_OUTLETS;
				filter();
				setMapZoomLevel(nearbyMap.getController(), nearbyMap.getMapCenter());
				return true;
				
			case R.id.menu_nearby_help :
				intent = new Intent(getDialogContext(), HelpActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_NEARBY, getResources().getString(R.string.tag_help_screen), intent);
				return true;
				
			case R.id.menu_nearby_search :
				intent = new Intent(getDialogContext(), SearchMainActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_NEARBY, getResources().getString(R.string.tag_search_main_screen), intent);
				return true;
			default :
				return super.onOptionsItemSelected(item);
		}
	}

	
	public void runAllStopsMenuOption(){
		closeOptionsMenu();
		filter = NearbyActivity.FILTER_STOPS_ALL; 
		filter();
		setMapZoomLevel(nearbyMap.getController(), nearbyMap.getMapCenter());
	}
	public void runSheltersMenuOption(){
		closeOptionsMenu();
		filter = NearbyActivity.FILTER_STOPS_SHELTER;
		filter();
		setMapZoomLevel(nearbyMap.getController(), nearbyMap.getMapCenter());
	}
	public void runEasyAccessMenuOption(){
		closeOptionsMenu();
		filter = NearbyActivity.FILTER_STOPS_ACCESS;
		filter();
		setMapZoomLevel(nearbyMap.getController(), nearbyMap.getMapCenter());
	}
	public void runOutletsMenuOption(){
		closeOptionsMenu();
		filter = NearbyActivity.FILTER_OUTLETS;
		filter();
		setMapZoomLevel(nearbyMap.getController(), nearbyMap.getMapCenter());
	}
	public void runTrackerIDMenuOption(){
		closeOptionsMenu();
		ActivityGroup group = TramTrackerMainActivity.getAppManager().requestDifferentTab(TramTrackerMainActivity.TAB_MORE);
		Intent intent = new Intent(group, SearchTrackerIDActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, getResources().getString(R.string.tag_trackerid_screen), intent);
	}
	public void runSearchMenuOption(){
		closeOptionsMenu();
		Intent intent = new Intent(getDialogContext(), SearchMainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_NEARBY, getResources().getString(R.string.tag_search_main_screen), intent);
	}
	
	
}
