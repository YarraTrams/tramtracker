package com.yarratrams.tramtracker.ui;

import com.google.android.maps.GeoPoint;
import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.TicketOutlet;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class OutletActivity extends Activity implements OnClickListener{
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_NEARBY;
	public static final String INTENT_KEY = "outlet_info";
	
	public TicketOutlet outlet;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.outlet_screen);
		
		Button directionsButton = (Button) findViewById(R.id.directions_button);
		directionsButton.setOnClickListener(this);
		
	}


	
	private void setOutletInfo(){
		TextView outletName = (TextView) findViewById(R.id.ticketOutlet_name);
		TextView outletAddress1 = (TextView) findViewById(R.id.ticketOutlet_address1);
		ImageView outlet24Hours = (ImageView) findViewById(R.id.ticketOutlet_24hours_check);
		ImageView outletMykicard = (ImageView) findViewById(R.id.ticketOutlet_mykicard_check);
		ImageView outletMykitopup = (ImageView) findViewById(R.id.ticketOutlet_mykitopup_check);
		outletName.setText(outlet.getName());
		outletAddress1.setText(outlet.getAddress());
		if(outlet.isIs24Hours()){
			outlet24Hours.setVisibility(View.VISIBLE);
		} else{
			outlet24Hours.setVisibility(View.INVISIBLE);
		}
		if(outlet.isHasMykiCard()){
			outletMykicard.setVisibility(View.VISIBLE);
		} else{
			outletMykicard.setVisibility(View.INVISIBLE);
		}
		if(outlet.isHasMykiTopUp()){
			outletMykitopup.setVisibility(View.VISIBLE);
		} else{
			outletMykitopup.setVisibility(View.INVISIBLE);
		}
	}
	

	@Override
	public void onClick(View v) {
		Button directionsButton = (Button)findViewById(R.id.directions_button);
		if(v == directionsButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_header_directions));
			GeoPoint destination = new GeoPoint(outlet.getLatitudeE6(), outlet.getLongitudeE6());
			TramTrackerMainActivity.getAppManager().requestDirectionsService(destination);
		}
	}

	@Override
	protected void onResume() {
		Bundle extras = getIntent().getExtras(); 
		if(extras != null){
			outlet = extras.getParcelable(INTENT_KEY);
		}
		setOutletInfo();
		
		super.onResume();
	}


	
	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}
	
	
}
