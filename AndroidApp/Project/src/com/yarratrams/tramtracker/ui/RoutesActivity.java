package com.yarratrams.tramtracker.ui;

import java.util.ArrayList;
import java.util.List;

import com.yarratrams.tramtracker.R;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.google.android.maps.Overlay;
import com.yarratrams.tramtracker.objects.Route;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.objects.TicketOutletBySuburb;
import com.yarratrams.tramtracker.singleton.RoutesManager;
import com.yarratrams.tramtracker.ui.util.MapRouteEasyStopOverlay;
import com.yarratrams.tramtracker.ui.util.MapRouteOutletOverlay;
import com.yarratrams.tramtracker.ui.util.MapRouteShelterStopOverlay;
import com.yarratrams.tramtracker.ui.util.MapRouteStopOverlay;
import com.yarratrams.tramtracker.ui.util.MenuRoutesAdapter;
import com.yarratrams.tramtracker.ui.util.RouteOutletsExpandableListArrayAdapter;
import com.yarratrams.tramtracker.ui.util.RouteStopsExpandableListArrayAdapter;
import com.yarratrams.tramtracker.ui.util.WrappingSlidingDrawer;

import android.app.ActivityGroup;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.ViewFlipper;


public class RoutesActivity extends MapActivity implements OnClickListener{
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_ROUTES;
	public static final String INTENT_KEY = "route_info";
	public static final int VIEW_LIST = 0;
	public static final int VIEW_MAP = 1;
	
	public static final int FILTER_STOPS_ALL = 0;
	public static final int FILTER_STOPS_SHELTER = 1;
	public static final int FILTER_STOPS_ACCESS = 2;
	public static final int FILTER_OUTLETS = 3;
	
	private boolean isListViewSelected;
	private ViewFlipper flipper;
	
	private Route route;
	
	private int filter;
	
	private ExpandableListView stopsList;
	private RouteStopsExpandableListArrayAdapter routeStopsAdapter;
	private ArrayList<Stop> stops;
	private int listIndex;
	private int listTop;
	
	private RouteOutletsExpandableListArrayAdapter outletsAdapter;
	private ArrayList<TicketOutletBySuburb> suburbs;
	
	private MapView stopsMap;
	private List<Overlay> mapOverlays;
	private MyLocationOverlay myOverlay;
	private GeoPoint mapCentre;
	private int zoomLevel;
	
	private WrappingSlidingDrawer slidingMenu;
	
	private ProgressDialog loadDialog;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.routes_screen);
		
		isListViewSelected = true;
		flipper = (ViewFlipper) findViewById(R.id.view_flipper);
		
		route = new Route();
		filter = RoutesActivity.FILTER_STOPS_ALL;
		
		ToggleButton listButton = (ToggleButton) findViewById(R.id.list_view_button);
		listButton.setOnClickListener(this);
		ToggleButton mapButton = (ToggleButton) findViewById(R.id.map_view_button);
		mapButton.setOnClickListener(this);
		Button refreshButton = (Button) findViewById(R.id.refresh_button);
		refreshButton.setOnClickListener(this);
		refreshButton.setVisibility(View.INVISIBLE);
		
		Button networkMapButton = (Button) findViewById(R.id.network_button);
		networkMapButton.setOnClickListener(this);
		
		stops = new ArrayList<Stop>();
		suburbs = new ArrayList<TicketOutletBySuburb>();
		stopsList = (ExpandableListView) findViewById(R.id.expandable_list);
		stopsList.addHeaderView(getTableView());
		listIndex = 0;
		listTop = 0;
		
		Display display = getWindowManager().getDefaultDisplay();
		Drawable groupIndicator = getResources().getDrawable(R.drawable.icn_list_expandable);
		stopsList.setGroupIndicator(groupIndicator);
		stopsList.setChildIndicator(null);
		stopsList.setIndicatorBounds(display.getWidth()-GetDipsFromPixel(30), display.getWidth());
		
		stopsMap = (MapView)findViewById(R.id.map);
		stopsMap.setBuiltInZoomControls(true);
		myOverlay = new MyLocationOverlay(this, stopsMap);
        mapOverlays = stopsMap.getOverlays();
        
        slidingMenu = (WrappingSlidingDrawer) findViewById(R.id.sliding_menu);
        GridView menuContainer = (GridView) findViewById(R.id.sliding_menu_container);
        MenuRoutesAdapter menuAdapter = new MenuRoutesAdapter(this);
        menuContainer.setAdapter(menuAdapter);
        
        loadDialog = new ProgressDialog(this);
	}
	
	public Context getDialogContext() {
		Context context;
		if (getParent() != null) {
			context = getParent();
		} else {
			context = this;
		}
		return context;
	}
	
	public int GetDipsFromPixel(float pixels) {
		final float scale = getResources().getDisplayMetrics().density;
		return (int) (pixels * scale + 0.5f);
	}
	
	private View getTableView(){
		LayoutInflater inflater = getLayoutInflater();
		View rowView = inflater.inflate(R.layout.routes_stops_list_view_table, null, true);
		return rowView;
	}
	
	public void retrieveStops(){
		if(!loadDialog.isShowing()){
			loadDialog = ProgressDialog.show(getDialogContext(), "", getResources().getString(R.string.dialog_loading), true, true);
		}
		
		TextView routeName = (TextView) findViewById(R.id.route_name);
		routeName.setText(getRouteName());
		
		// Setup map
		mapOverlays.clear();
		mapOverlays.add(myOverlay);
		
		RoutesManager routesManager = new RoutesManager(this);
		stops = routesManager.getStopsForRoute(route.getRouteNumber(), route.isUpDestination());
		suburbs = routesManager.getTicketOutletsForStops(stops);
		
		filter();
        
        centerMapOnStops();
		
		if(loadDialog.isShowing()){
			loadDialog.dismiss();
		}
	}
	
	private void expandAllStops(){
		for(int i = 0; i < routeStopsAdapter.getGroupCount(); i ++){
			stopsList.expandGroup(i);
		}
	}

	private void expandAllOutlets(){
		for(int i = 0; i < outletsAdapter.getGroupCount(); i ++){
			stopsList.expandGroup(i);
		}
	}
	
	
	private String getRouteName(){
		String text = "";
		
		text = route.getRouteNumber();
		text = text.concat(getResources().getString(R.string.routes_entry_name_dash));
		if(route.isUpDestination()){
			if(route.getDownDestination() != null){
				if(route.getDownDestination().length() > 0){
					text = text.concat(route.getDownDestination());
				}
			}
			if(route.getUpDestination() != null){
				if(route.getUpDestination().length() > 0){
					text = text.concat(getResources().getString(R.string.routes_entry_name_to));
					text = text.concat(route.getUpDestination());
				}
			}
		} else {
			if(route.getUpDestination() != null){
				if(route.getUpDestination().length() > 0){
					text = text.concat(route.getUpDestination());
				}
			}
			if(route.getDownDestination() != null){
				if(route.getDownDestination().length() > 0){
					text = text.concat(getResources().getString(R.string.routes_entry_name_to));
					text = text.concat(route.getDownDestination());
				}
			}
		}
		
		return text;
	}
	
	
	public void centerMapOn(GeoPoint center, int max, boolean islat){
		MapController mapController = stopsMap.getController();
		
		if(islat){
			if(max < 20000){
				zoomLevel = 16; // Zoom 16 few blocks
			} else if(max < 30000){
				zoomLevel = 15; 
			} else if(max < 50000){
				zoomLevel = 14; 
			} else if(max < 70000){
				zoomLevel = 13; 
			} else if(max < 131000){
				zoomLevel = 12; 
			} else if(max < 150000){
				zoomLevel = 11; 
			} else {
				zoomLevel = 10; // Zoom 10 is city view
			}
		} else {
			if(max < 25000){
				zoomLevel = 16; // Zoom 16 few blocks
			} else if(max < 30000){
				zoomLevel= 15; 
			} else if(max < 80000){
				zoomLevel = 14; 
			} else if(max < 100000){
				zoomLevel = 13; 
			} else if(max < 190000){
				zoomLevel = 12; 
			} else if(max < 250000){
				zoomLevel = 11; 
			} else {
				zoomLevel = 10; // Zoom 10 is city view
			}
		}
		mapController.setZoom(zoomLevel);
		if(center != null){
			mapCentre = center;
			mapController.animateTo(mapCentre);
		}
	}
	
	public void centerMapOnStops(){
		int maxLat = 0;
		int minLat = 0;
		int maxLon = 0;
		int minLon = 0;
		
		for(Stop stop: stops){
			if(maxLat == 0){
				maxLat = stop.getLatitudeE6();
				minLat = stop.getLatitudeE6();
				maxLon = stop.getLongitudeE6();
				minLon = stop.getLongitudeE6();
			}
			maxLat = stop.getLatitudeE6() > maxLat? stop.getLatitudeE6(): maxLat;
			minLat = stop.getLatitudeE6() < minLat? stop.getLatitudeE6(): minLat;
			maxLon = stop.getLongitudeE6() > maxLon? stop.getLongitudeE6(): maxLon;
			minLon = stop.getLongitudeE6() < minLon? stop.getLongitudeE6(): minLon;
		}
		if(maxLat == 0){
			maxLat = (int) (-37.817491 * 1E6);
			minLat = (int) (-37.817491 * 1E6);
			maxLon = (int) (144.967445 * 1E6);
			minLon = (int) (144.967445 * 1E6);			
		}
		
		int dLat = maxLat - minLat;
		int dLon = maxLon - minLon;
		
		int cLat = minLat + (dLat/2);
		int cLon = minLon + (dLon/2);
		
		int max = 0;
		boolean isLat = false;
		if(dLat == 0 && dLon == 0){
			max = 150000;
		} else {
			if(dLat > dLon){
				max = dLat;
				isLat = true;
			} else{
				max = dLon;
				isLat = false;
			}
		}
		
		GeoPoint centre = new GeoPoint(cLat, cLon);
		centerMapOn(centre, max, isLat);
	}
	
	public void onResume() {
		super.onResume();
		
		if(!isListViewSelected){
			myOverlay.enableMyLocation();
		}
		
		if(mapCentre != null){
			MapController mapController = stopsMap.getController();
			mapController.setCenter(mapCentre);
			mapController.setZoom(zoomLevel);
		}
		
		if(!stops.isEmpty()){
			stopsList.setSelectionFromTop(listIndex, listTop);
			return;
		}
		
		Bundle extras = getIntent().getExtras();
		if(extras != null){
			route = extras.getParcelable(INTENT_KEY);
		}
		
		retrieveStops();
	}
	
	@Override
	public void onPause() {
		super.onPause();
		
		if(!isListViewSelected){
			myOverlay.disableMyLocation();
		}
		zoomLevel = stopsMap.getZoomLevel();
		mapCentre = stopsMap.getMapCenter();
		
		listIndex = stopsList.getFirstVisiblePosition();
		View v = stopsList.getChildAt(0);
		listTop = (v == null)? 0 : v.getTop();
	}
	
	
	private void switchViews(){
		ToggleButton listButton = (ToggleButton) findViewById(R.id.list_view_button);
		ToggleButton mapButton = (ToggleButton) findViewById(R.id.map_view_button);
		Button refreshButton = (Button) findViewById(R.id.refresh_button);
		if (isListViewSelected) {
			isListViewSelected = false;
			listButton.setChecked(false);
			mapButton.setChecked(true);
			refreshButton.setVisibility(View.VISIBLE);
	        flipper.setDisplayedChild(VIEW_MAP);
	        myOverlay.enableMyLocation();
	        TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_header_mapview));
		} else {
			isListViewSelected = true;
			listButton.setChecked(true);
			mapButton.setChecked(false);
			refreshButton.setVisibility(View.INVISIBLE);
	        flipper.setDisplayedChild(VIEW_LIST);
	        myOverlay.disableMyLocation();
	        TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_header_listview));
		}
	}
	
	
	private void refresh(){
		centerMapOnStops();
	}
	
	@Override
	public void onClick(View v) {
		ToggleButton listButton = (ToggleButton) findViewById(R.id.list_view_button);
		ToggleButton mapButton = (ToggleButton) findViewById(R.id.map_view_button);
		Button refreshButton = (Button) findViewById(R.id.refresh_button);
		Button networkMapButton = (Button) findViewById(R.id.network_button);
		if (v == listButton || v == mapButton) {
			switchViews();
		} else if(v == refreshButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_header_refresh));
			refresh();
		} else if(v == networkMapButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_header_networkmap));
			Intent intent = new Intent(this, NetworkMapActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_ROUTES, 
					getResources().getString(R.string.tag_networkmap_screen), intent);
		}
	}
		
	private ArrayList<Stop> getStopsWithShelter(ArrayList<Stop> origin){
		ArrayList<Stop> list = new ArrayList<Stop>();
		for(Stop stop : origin){
			if(stop.isHasShelter()){
				list.add(stop);
			}
		}
		
		return list;
	}
	
	private ArrayList<Stop> getStopsWithEasyAccess(ArrayList<Stop> origin){
		ArrayList<Stop> list = new ArrayList<Stop>();
		
		for(Stop stop : origin){
			if(stop.isEasyAccessStop()){
				list.add(stop);
			}
		}
		
		return list;
	}
	
	private void showSymbolTable(boolean show){
		ImageView symbolTable = (ImageView) findViewById(R.id.route_symbols);
		if(show){
			symbolTable.setVisibility(View.VISIBLE);
		} else {
			symbolTable.setVisibility(View.GONE);
		}
	}
	
	public void filter(){
		mapOverlays.clear();
		mapOverlays.add(myOverlay); 
		showSymbolTable(true);
		
		switch (filter) {
			case RoutesActivity.FILTER_STOPS_ALL :
				// Setup list
				routeStopsAdapter = new RouteStopsExpandableListArrayAdapter(this, route, stops);
				stopsList.setAdapter(routeStopsAdapter);
				TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.routes_title_all));
				expandAllStops();
				
				// Setup map
				MapRouteStopOverlay overlay = new MapRouteStopOverlay(this, stopsMap);
				overlay.updateTramStopsList(stops);
		        mapOverlays.add(overlay);
		        
				break;

			case RoutesActivity.FILTER_STOPS_SHELTER :
				// Setup list
				ArrayList<Stop> shelters = getStopsWithShelter(stops);
				routeStopsAdapter = new RouteStopsExpandableListArrayAdapter(this, route, shelters);
				stopsList.setAdapter(routeStopsAdapter);
				TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.routes_title_shelters));
				expandAllStops();
				
				// Setup map
				MapRouteShelterStopOverlay shelterOverlay = new MapRouteShelterStopOverlay(this, stopsMap);
				shelterOverlay.updateTramStopsList(shelters);
		        mapOverlays.add(shelterOverlay);
		        
				break;
				
			case RoutesActivity.FILTER_STOPS_ACCESS :
				// Setup list
				ArrayList<Stop> access = getStopsWithEasyAccess(stops);
				routeStopsAdapter = new RouteStopsExpandableListArrayAdapter(this, route, access);
				stopsList.setAdapter(routeStopsAdapter);
				TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.routes_title_accessible));
				expandAllStops();
				
				// Setup map
				MapRouteEasyStopOverlay easyOverlay = new MapRouteEasyStopOverlay(this, stopsMap);
				easyOverlay.updateTramStopsList(access);
		        mapOverlays.add(easyOverlay);
				
				break;
				
			case RoutesActivity.FILTER_OUTLETS :
				showSymbolTable(false);
				
				// Setup list
				outletsAdapter = new RouteOutletsExpandableListArrayAdapter(this, suburbs);
				stopsList.setAdapter(outletsAdapter);
				TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.routes_title_outlets));
				expandAllOutlets();
				
				// Setup map
				MapRouteOutletOverlay outletOverlay = new MapRouteOutletOverlay(this, stopsMap);
				outletOverlay.updateTicketOutletsList(suburbs);
		        mapOverlays.add(outletOverlay);
				
				break;
				
			default :
				break;
		}
		stopsMap.invalidate();
	}

	
	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}
	
	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
//		MenuInflater inflater = getMenuInflater();
//		inflater.inflate(R.menu.routes_menu, menu);
//		TramTrackerMainActivity.getAppManager().setMenuBackground(this);

		return true;
	}
	
	@Override
	public void openOptionsMenu() {
		slidingMenu.toggle();
	}
	@Override
	public void closeOptionsMenu() {
		slidingMenu.close();
	}
	
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_MENU){
			openOptionsMenu();
			return true;
		} else if(keyCode == KeyEvent.KEYCODE_BACK){
			onBackPressed();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent;
		switch (item.getItemId()) {
			case R.id.menu_route_allStops :
				filter = RoutesActivity.FILTER_STOPS_ALL; 
				filter();
				return true;
				
			case R.id.menu_route_shelters :
				filter = RoutesActivity.FILTER_STOPS_SHELTER;
				filter();
				return true;
				
			case R.id.menu_route_easyAccess :
				filter = RoutesActivity.FILTER_STOPS_ACCESS;
				filter();
				return true;
				
			case R.id.menu_route_outlets :
				filter = RoutesActivity.FILTER_OUTLETS;
				filter();
				return true;
				
			case R.id.menu_route_help :
				intent = new Intent(getDialogContext(), HelpActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_ROUTES, getResources().getString(R.string.tag_help_screen), intent);
				return true;
				
			case R.id.menu_route_search :
				intent = new Intent(getDialogContext(), SearchMainActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_ROUTES, getResources().getString(R.string.tag_search_main_screen), intent);
				return true;
			default :
				return super.onOptionsItemSelected(item);
		}
	}

	public void runAllStopsMenuOption(){
		closeOptionsMenu();
		filter = RoutesActivity.FILTER_STOPS_ALL; 
		filter();
	}
	public void runSheltersMenuOption(){
		closeOptionsMenu();
		filter = RoutesActivity.FILTER_STOPS_SHELTER;
		filter();
	}
	public void runEasyAccessMenuOption(){
		closeOptionsMenu();
		filter = RoutesActivity.FILTER_STOPS_ACCESS;
		filter();
	}
	public void runOutletsMenuOption(){
		closeOptionsMenu();
		filter = RoutesActivity.FILTER_OUTLETS;
		filter();
	}
	public void runTrackerIDMenuOption(){
		closeOptionsMenu();
		ActivityGroup group = TramTrackerMainActivity.getAppManager().requestDifferentTab(TramTrackerMainActivity.TAB_MORE);
		Intent intent = new Intent(group, SearchTrackerIDActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, getResources().getString(R.string.tag_trackerid_screen), intent);
	}
	public void runSearchMenuOption(){
		closeOptionsMenu();
		Intent intent = new Intent(getDialogContext(), SearchMainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_ROUTES, getResources().getString(R.string.tag_search_main_screen), intent);
	}

}
