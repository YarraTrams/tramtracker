package com.yarratrams.tramtracker.ui;


import com.yarratrams.tramtracker.R;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.RelativeLayout;

public class MoreActivity extends Activity implements OnClickListener, OnTouchListener{
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_MORE;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.more_screen);

		RelativeLayout trackerButton = (RelativeLayout) findViewById(R.id.more_tracker);
		RelativeLayout feedbackButton = (RelativeLayout) findViewById(R.id.more_feedback);
		RelativeLayout timetableButton = (RelativeLayout) findViewById(R.id.more_timetables);
		RelativeLayout serviceFeedButton = (RelativeLayout) findViewById(R.id.more_servicefeed);
		RelativeLayout twitterButton = (RelativeLayout) findViewById(R.id.more_twitter);
		RelativeLayout searchButton = (RelativeLayout) findViewById(R.id.more_search);
		RelativeLayout entryButton = (RelativeLayout) findViewById(R.id.more_entryscreen);
		RelativeLayout helpButton = (RelativeLayout) findViewById(R.id.more_help);
		RelativeLayout outletsButton = (RelativeLayout) findViewById(R.id.more_outlets);
		RelativeLayout updateButton = (RelativeLayout) findViewById(R.id.more_update);
		trackerButton.setOnClickListener(this);
		feedbackButton.setOnClickListener(this);
		timetableButton.setOnClickListener(this);
		serviceFeedButton.setOnClickListener(this);
		twitterButton.setOnClickListener(this);
		searchButton.setOnClickListener(this);
		entryButton.setOnClickListener(this);
		helpButton.setOnClickListener(this);
		outletsButton.setOnClickListener(this);
		updateButton.setOnClickListener(this);
		
		trackerButton.setOnTouchListener(this);
		feedbackButton.setOnTouchListener(this);
		timetableButton.setOnTouchListener(this);
		serviceFeedButton.setOnTouchListener(this);
		twitterButton.setOnTouchListener(this);
		searchButton.setOnTouchListener(this);
		entryButton.setOnTouchListener(this);
		helpButton.setOnTouchListener(this);
		outletsButton.setOnTouchListener(this);
		updateButton.setOnTouchListener(this);
	}		

	
	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}


	@Override
	public void onClick(View v) {
		Intent intent;
		RelativeLayout trackerButton = (RelativeLayout) findViewById(R.id.more_tracker);
		RelativeLayout feedbackButton = (RelativeLayout) findViewById(R.id.more_feedback);
		RelativeLayout timetableButton = (RelativeLayout) findViewById(R.id.more_timetables);
		RelativeLayout serviceFeedButton = (RelativeLayout) findViewById(R.id.more_servicefeed);
		RelativeLayout twitterButton = (RelativeLayout) findViewById(R.id.more_twitter);
		RelativeLayout searchButton = (RelativeLayout) findViewById(R.id.more_search);
		RelativeLayout entryButton = (RelativeLayout) findViewById(R.id.more_entryscreen);
		RelativeLayout helpButton = (RelativeLayout) findViewById(R.id.more_help);
		RelativeLayout outletsButton = (RelativeLayout) findViewById(R.id.more_outlets);
		RelativeLayout updateButton = (RelativeLayout) findViewById(R.id.more_update);
		

		if(v == trackerButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.more_tracker));
			intent = new Intent(this, SearchTrackerIDActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, 
					getResources().getString(R.string.tag_trackerid_screen), intent);
			
		} else if(v == feedbackButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.more_feedback));
			intent = new Intent(this, FeedbackActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, 
					getResources().getString(R.string.tag_feedback_screen), intent);
			
		} else if(v == timetableButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.more_timetables));
			intent = new Intent(this, TimetableRoutesActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, 
					getResources().getString(R.string.tag_timetable_routes_screen), intent);
			
		} else if(v == serviceFeedButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.more_servicefeed));
			intent = new Intent(this, ServiceFeedActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, 
					getResources().getString(R.string.tag_service_feed_screen), intent);

		} else if(v == twitterButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.more_twitter));
			intent = new Intent(Intent.ACTION_VIEW);
			intent.setType("application/twitter");
			intent.setData(Uri.parse(getString(R.string.twitter_url_base)));
			startActivity(intent);
			
		} else if(v == searchButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.more_search));
			intent = new Intent(this, SearchMainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, 
					getResources().getString(R.string.tag_search_main_screen), intent);
			
		} else if(v == entryButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.more_entryscreen));
			intent = new Intent(this, DefaultEntryActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, 
					getResources().getString(R.string.tag_defaultentry_screen), intent);
			
		} else if(v == helpButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.more_help));
			intent = new Intent(this, HelpActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, 
					getResources().getString(R.string.tag_help_screen), intent);
			
		} else if(v == outletsButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.more_outlets));
			intent = new Intent(this, TicketOutletsListActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, 
					getResources().getString(R.string.tag_ticketoutlets_list_screen), intent);
			
		} else if(v == updateButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.more_update));
			intent = new Intent(this, UpdateActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, 
					getResources().getString(R.string.tag_update_screen), intent);
		}
	}
	
	@Override
	protected void onResume() { 
		super.onResume();
		
	}


	@Override
	public boolean onTouch(View v, MotionEvent event) {
		v.setPressed(true);
		return false;
	}

}
