package com.yarratrams.tramtracker.ui;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.yarratrams.tramtracker.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class UpdateActivity extends Activity implements OnClickListener{
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_MORE;
	
	private ProgressDialog loadDialog;
	
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.update_screen);
		
		Button updateButton = (Button)findViewById(R.id.update_button);
		updateButton.setOnClickListener(this);
		
		loadDialog = new ProgressDialog(this);
	}
	
	
	@Override
	protected void onResume() {
		super.onResume();
		
		if(TramTrackerMainActivity.getAppManager().isUpdateAvailable()){
			allowUpdate(true);
		} else{
			allowUpdate(false);
		}
	}


	public void allowUpdate(boolean hasUpgrade){
		RelativeLayout noUpdateView = (RelativeLayout)findViewById(R.id.update_noupdate);
		RelativeLayout updateView = (RelativeLayout)findViewById(R.id.update_update);
		TextView updateDate = (TextView)findViewById(R.id.update_last_date);
		
		if(hasUpgrade){
			noUpdateView.setVisibility(View.GONE);
			updateView.setVisibility(View.VISIBLE);
		} else {
			noUpdateView.setVisibility(View.VISIBLE);
			updateDate.setText(getLastUpdateString());
			updateView.setVisibility(View.GONE);
		}
	}
	
	private String getLastUpdateString(){
		String text = getString(R.string.update_noupdate_last); 
		text = text.concat(getLastUpdateDate());
		return text;
	}
	
	private String getLastUpdateDate(){
		Calendar calendar = TramTrackerMainActivity.getAppManager().getLastUpdateDate();
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		DateFormat timeFormat = new SimpleDateFormat(getResources().getString(R.string.update_date_format));
		Date date = new Date(calendar.getTimeInMillis()); 
		String timeString = timeFormat.format(date);
		return timeString;
	}
	
	
	@Override
	public void onClick(View v) {
		Button updateButton = (Button)findViewById(R.id.update_button);
		
		if(v == updateButton){
			if(!loadDialog.isShowing()){
				loadDialog = ProgressDialog.show(getDialogContext(), "", getResources().getString(R.string.dialog_loading), true, true);
			}
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_update));
			TramTrackerMainActivity.getAppManager().update(this);
		}
	}
	
	public void notifyFinishUpdate(boolean successfull){
		if(successfull){
			allowUpdate(false);
			TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.notify_update_successfull));
		} else {
			TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.notify_update_fail));
		}
		
		if(loadDialog.isShowing()){
			loadDialog.dismiss();
		}
	}
	
	public Context getDialogContext() {
		Context context;
		if (getParent() != null) {
			context = getParent();
		} else {
			context = this;
		}
		return context;
	}
	
	
	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

}
