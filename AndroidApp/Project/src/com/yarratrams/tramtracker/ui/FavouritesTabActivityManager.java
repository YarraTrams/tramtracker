package com.yarratrams.tramtracker.ui;

import android.app.ActivityGroup;
import android.os.Bundle;
import android.view.KeyEvent;


public class FavouritesTabActivityManager extends ActivityGroup {
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_FAVOURITES;
	public static FavouritesTabActivityManager instance;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		instance = this;
	}

	static public FavouritesTabActivityManager getGroup() {
		return instance;
	}
	
    
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_MENU){
			getLocalActivityManager().getCurrentActivity().openOptionsMenu();
			return true;
		} else if(keyCode == KeyEvent.KEYCODE_BACK){
			getLocalActivityManager().getCurrentActivity().onBackPressed();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	
}