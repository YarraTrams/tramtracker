package com.yarratrams.tramtracker.ui;

import java.util.ArrayList;
import java.util.List;

import com.yarratrams.tramtracker.R;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.yarratrams.tramtracker.objects.Route;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.singleton.RoutesManager;
import com.yarratrams.tramtracker.ui.util.MapTimetableRouteStopOverlay;
import com.yarratrams.tramtracker.ui.util.TimetableRouteStopsExpandableListArrayAdapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.ViewFlipper;


public class TimetableRouteActivity extends MapActivity implements OnClickListener{
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_MORE;
	public static final String INTENT_KEY = "route_info";
	public static final int VIEW_LIST = 0;
	public static final int VIEW_MAP = 1;
	
	private boolean isListViewSelected;
	private ViewFlipper flipper;
	
	private Route route;
	
	private ExpandableListView stopsList;
	private TimetableRouteStopsExpandableListArrayAdapter routeStopsAdapter;
	private ArrayList<Stop> stops;
	private int listIndex;
	private int listTop;
	
	private MapView stopsMap;
	private List<Overlay> mapOverlays;
	private GeoPoint mapCentre;
	private int zoomLevel;
	
	private ProgressDialog loadDialog;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.timetable_routes_screen);
		
		isListViewSelected = true;
		flipper = (ViewFlipper) findViewById(R.id.view_flipper);
		
		route = new Route();
		
		ToggleButton listButton = (ToggleButton) findViewById(R.id.list_view_button);
		listButton.setOnClickListener(this);
		ToggleButton mapButton = (ToggleButton) findViewById(R.id.map_view_button);
		mapButton.setOnClickListener(this);
		Button refreshButton = (Button) findViewById(R.id.refresh_button);
		refreshButton.setOnClickListener(this);
		refreshButton.setVisibility(View.INVISIBLE);
		
		Button networkMapButton = (Button) findViewById(R.id.network_button);
		networkMapButton.setOnClickListener(this);
		
		stops = new ArrayList<Stop>();
		stopsList = (ExpandableListView) findViewById(R.id.expandable_list);
		stopsList.addHeaderView(getTableView());
		listIndex = 0;
		listTop = 0;
		
		Display display = getWindowManager().getDefaultDisplay();
		Drawable groupIndicator = getResources().getDrawable(R.drawable.icn_list_expandable);
		stopsList.setGroupIndicator(groupIndicator);
		stopsList.setChildIndicator(null);
		stopsList.setIndicatorBounds(display.getWidth()-GetDipsFromPixel(30), display.getWidth());
		
		stopsMap = (MapView)findViewById(R.id.map);
		stopsMap.setBuiltInZoomControls(true);
        mapOverlays = stopsMap.getOverlays();
        
        loadDialog = new ProgressDialog(this);
	}
	
	public Context getDialogContext() {
		Context context;
		if (getParent() != null) {
			context = getParent();
		} else {
			context = this;
		}
		return context;
	}
	
	public int GetDipsFromPixel(float pixels) {
		final float scale = getResources().getDisplayMetrics().density;
		return (int) (pixels * scale + 0.5f);
	}
	
	private View getTableView(){
		LayoutInflater inflater = getLayoutInflater();
		View rowView = inflater.inflate(R.layout.routes_stops_list_view_table, null, true);
		return rowView;
	}
	
	public void retrieveStops(){
		if(!loadDialog.isShowing()){
			loadDialog = ProgressDialog.show(getDialogContext(), "", getResources().getString(R.string.dialog_loading), true, true);
		}
		
		TextView routeName = (TextView) findViewById(R.id.route_name);
		routeName.setText(getRouteName());
		
		// Setup map
		mapOverlays.clear();
		
		RoutesManager routesManager = new RoutesManager(this);
		stops = routesManager.getStopsForRoute(route.getRouteNumber(), route.isUpDestination());
		
		routeStopsAdapter = new TimetableRouteStopsExpandableListArrayAdapter(this, route, stops);
		stopsList.setAdapter(routeStopsAdapter);
		
		expandAll();
		
		MapTimetableRouteStopOverlay overlay = new MapTimetableRouteStopOverlay(this, stopsMap);
		overlay.updateTramStopsList(route, stops);
        mapOverlays.add(overlay);
        stopsMap.invalidate();
        
        centerMapOnStops();
		
		if(loadDialog.isShowing()){
			loadDialog.dismiss();
		}
	}
	
	private void expandAll(){
		for(int i = 0; i < routeStopsAdapter.getGroupCount(); i ++){
			stopsList.expandGroup(i);
		}
	}
	
	public void centerMapOn(GeoPoint center, int max, boolean islat){
		MapController mapController = stopsMap.getController();
		
		if(islat){
			if(max < 20000){
				zoomLevel = 16; // Zoom 16 few blocks
			} else if(max < 30000){
				zoomLevel = 15; 
			} else if(max < 50000){
				zoomLevel = 14; 
			} else if(max < 70000){
				zoomLevel = 13; 
			} else if(max < 131000){
				zoomLevel = 12; 
			} else if(max < 150000){
				zoomLevel = 11; 
			} else {
				zoomLevel = 10; // Zoom 10 is city view
			}
		} else {
			if(max < 25000){
				zoomLevel = 16; // Zoom 16 few blocks
			} else if(max < 30000){
				zoomLevel= 15; 
			} else if(max < 80000){
				zoomLevel = 14; 
			} else if(max < 100000){
				zoomLevel = 13; 
			} else if(max < 190000){
				zoomLevel = 12; 
			} else if(max < 250000){
				zoomLevel = 11; 
			} else {
				zoomLevel = 10; // Zoom 10 is city view
			}
		}
		mapController.setZoom(zoomLevel);
		if(center != null){
			mapCentre = center;
			mapController.animateTo(mapCentre);
		}
	}
	
	public void centerMapOnStops(){
		int maxLat = 0;
		int minLat = 0;
		int maxLon = 0;
		int minLon = 0;
		
		for(Stop stop: stops){
			if(maxLat == 0){
				maxLat = stop.getLatitudeE6();
				minLat = stop.getLatitudeE6();
				maxLon = stop.getLongitudeE6();
				minLon = stop.getLongitudeE6();
			}
			maxLat = stop.getLatitudeE6() > maxLat? stop.getLatitudeE6(): maxLat;
			minLat = stop.getLatitudeE6() < minLat? stop.getLatitudeE6(): minLat;
			maxLon = stop.getLongitudeE6() > maxLon? stop.getLongitudeE6(): maxLon;
			minLon = stop.getLongitudeE6() < minLon? stop.getLongitudeE6(): minLon;
		}
		if(maxLat == 0){
			maxLat = (int) (-37.817491 * 1E6);
			minLat = (int) (-37.817491 * 1E6);
			maxLon = (int) (144.967445 * 1E6);
			minLon = (int) (144.967445 * 1E6);			
		}
		
		int dLat = maxLat - minLat;
		int dLon = maxLon - minLon;
		
		int cLat = minLat + (dLat/2);
		int cLon = minLon + (dLon/2);
		
		int max = 0;
		boolean isLat = false;
		if(dLat == 0 && dLon == 0){
			max = 150000;
		} else {
			if(dLat > dLon){
				max = dLat;
				isLat = true;
			} else{
				max = dLon;
				isLat = false;
			}
		}
		
		GeoPoint centre = new GeoPoint(cLat, cLon);
		centerMapOn(centre, max, isLat);
	}
	
	public void onResume() {
		super.onResume();
		
		if(mapCentre != null){
			MapController mapController = stopsMap.getController();
			mapController.setCenter(mapCentre);
			mapController.setZoom(zoomLevel);
		}
		
		if(!stops.isEmpty()){
			stopsList.setSelectionFromTop(listIndex, listTop);
			return;
		}
		
		Bundle extras = getIntent().getExtras();
		if(extras != null){
			route = extras.getParcelable(INTENT_KEY);
		}
		
		retrieveStops();
	}
	
	@Override
	public void onPause() {
		super.onPause();
		
		zoomLevel = stopsMap.getZoomLevel();
		mapCentre = stopsMap.getMapCenter();
		
		listIndex = stopsList.getFirstVisiblePosition();
		View v = stopsList.getChildAt(0);
		listTop = (v == null)? 0 : v.getTop();
	}
	
	
	private void switchViews(){
		ToggleButton listButton = (ToggleButton) findViewById(R.id.list_view_button);
		ToggleButton mapButton = (ToggleButton) findViewById(R.id.map_view_button);
		Button refreshButton = (Button) findViewById(R.id.refresh_button);
		if (isListViewSelected) {
			isListViewSelected = false;
			listButton.setChecked(false);
			mapButton.setChecked(true);
			refreshButton.setVisibility(View.VISIBLE);
	        flipper.setDisplayedChild(VIEW_MAP);
	        TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_header_mapview));
		} else {
			isListViewSelected = true;
			listButton.setChecked(true);
			mapButton.setChecked(false);
			refreshButton.setVisibility(View.INVISIBLE);
	        flipper.setDisplayedChild(VIEW_LIST);
	        TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_header_listview));
		}
	}
	
	
	private void refresh(){
		centerMapOnStops();
	}
	
	@Override
	public void onClick(View v) {
		ToggleButton listButton = (ToggleButton) findViewById(R.id.list_view_button);
		ToggleButton mapButton = (ToggleButton) findViewById(R.id.map_view_button);
		Button refreshButton = (Button) findViewById(R.id.refresh_button);
		Button networkMapButton = (Button) findViewById(R.id.network_button);
		if (v == listButton || v == mapButton) {
			switchViews();
		} else if(v == refreshButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_header_refresh));
			refresh();
		} else if(v == networkMapButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_header_networkmap));
			Intent intent = new Intent(this, NetworkMapActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, 
					getResources().getString(R.string.tag_networkmap_screen), intent);
		}
	}
	
	private String getRouteName(){
		String text = "";
		
		text = route.getRouteNumber();
		text = text.concat(getResources().getString(R.string.routes_entry_name_dash));
		if(route.isUpDestination()){
			if(route.getDownDestination() != null){
				if(route.getDownDestination().length() > 0){
					text = text.concat(route.getDownDestination());
				}
			}
			if(route.getUpDestination() != null){
				if(route.getUpDestination().length() > 0){
					text = text.concat(getResources().getString(R.string.routes_entry_name_to));
					text = text.concat(route.getUpDestination());
				}
			}
		} else {
			if(route.getUpDestination() != null){
				if(route.getUpDestination().length() > 0){
					text = text.concat(route.getUpDestination());
				}
			}
			if(route.getDownDestination() != null){
				if(route.getDownDestination().length() > 0){
					text = text.concat(getResources().getString(R.string.routes_entry_name_to));
					text = text.concat(route.getDownDestination());
				}
			}
		}
		
		return text;
	}
	
	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}
	
	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	

}
