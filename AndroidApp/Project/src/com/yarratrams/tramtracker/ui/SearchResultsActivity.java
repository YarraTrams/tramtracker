package com.yarratrams.tramtracker.ui;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.ViewFlipper;

import com.yarratrams.tramtracker.R;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.google.android.maps.Overlay;
import com.yarratrams.tramtracker.objects.PointOfInterest;
import com.yarratrams.tramtracker.objects.SearchResult;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.objects.TicketOutlet;
import com.yarratrams.tramtracker.ui.util.MapSearchEasyStopOverlay;
import com.yarratrams.tramtracker.ui.util.MapSearchOutletOverlay;
import com.yarratrams.tramtracker.ui.util.MapSearchPOIOverlay;
import com.yarratrams.tramtracker.ui.util.MapSearchShelterAccessStopOverlay;
import com.yarratrams.tramtracker.ui.util.MapSearchShelterStopOverlay;
import com.yarratrams.tramtracker.ui.util.MapSearchStopOverlay;
import com.yarratrams.tramtracker.ui.util.SearchResultsListArrayAdapter;


public class SearchResultsActivity extends MapActivity implements OnClickListener{
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_MORE;
	public static final String INTENT_RESULTS_KEY = "results_info";
	public static final int VIEW_LIST = 0;
	public static final int VIEW_MAP = 1;
	
	private boolean isListViewSelected;
	private ViewFlipper flipper;
	
	private ListView resultsList;
	private TextView listTitle;
	
	private MapView resultsMap;
	private List<Overlay> mapOverlays;
	private MyLocationOverlay myOverlay;
	

	private SearchResult resultObject;
	private ArrayList<Object> results;
	private SearchResultsListArrayAdapter resultsAdapter;
	

	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_results_screen);

		isListViewSelected = true;
		flipper = (ViewFlipper) findViewById(R.id.view_flipper);
		
		ToggleButton listButton = (ToggleButton) findViewById(R.id.list_view_button);
		listButton.setOnClickListener(this);
		ToggleButton mapButton = (ToggleButton) findViewById(R.id.map_view_button);
		mapButton.setOnClickListener(this);
		Button refreshButton = (Button) findViewById(R.id.refresh_button);
		refreshButton.setOnClickListener(this);
		
        listTitle = (TextView) findViewById(R.id.list_title);
        resultsList = (ListView) findViewById(R.id.simple_list);
        resultObject = new SearchResult();
        results = new ArrayList<Object>();
		resultsAdapter = new SearchResultsListArrayAdapter(this, resultObject, results);
		resultsList.setAdapter(resultsAdapter);
		
		resultsMap = (MapView)findViewById(R.id.map);
		resultsMap.setBuiltInZoomControls(true);
        mapOverlays = resultsMap.getOverlays();
        myOverlay = new MyLocationOverlay(this, resultsMap);
        
	}

	
	
	public void updateUI() {
		String title = getResources().getString(R.string.search_results);
		title = title.concat(resultObject.getKeyword());
		listTitle.setText(title);
		
		results = resultObject.getAllResults();
		if(results != null){
			if(results.size() > 150){
				resultObject.limitTo150Results();
				results = resultObject.getAllResults();
				TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.error_search_toomanyresults));
			}
		}
		ArrayList<Object> orderedList = new ArrayList<Object>();
		
		
		mapOverlays.clear();
		mapOverlays.add(myOverlay);
		
		ArrayList<Stop> stopList = resultObject.getAlStops();
		ArrayList<Stop> filteredNormalList = new ArrayList<Stop>();
		ArrayList<Stop> filteredShelterList = new ArrayList<Stop>();
		ArrayList<Stop> filteredAccessList = new ArrayList<Stop>();
		ArrayList<Stop> filteredShelterAccessList = new ArrayList<Stop>();
		if(stopList != null){
			for(Stop stop : stopList){
				if(stop.isEasyAccessStop() && stop.isHasShelter() && 
						resultObject.isSearchAccessible() && resultObject.isSearchShelter()){
					filteredShelterAccessList.add(stop);
				} else if(stop.isHasShelter() && resultObject.isSearchShelter()){
					filteredShelterList.add(stop);
				} else if(stop.isEasyAccessStop() && resultObject.isSearchAccessible()){
					filteredAccessList.add(stop);
				} else {
					filteredNormalList.add(stop);
				}
			}

			if(!filteredShelterAccessList.isEmpty()){
				MapSearchShelterAccessStopOverlay overlay = new MapSearchShelterAccessStopOverlay(this, resultsMap);
				overlay.updateTramStopsList(filteredShelterAccessList);
		        mapOverlays.add(overlay);
		        orderedList.addAll(filteredShelterAccessList);
			}
			if(!filteredShelterList.isEmpty()){
				MapSearchShelterStopOverlay overlay = new MapSearchShelterStopOverlay(this, resultsMap);
				overlay.updateTramStopsList(filteredShelterList);
		        mapOverlays.add(overlay);
		        orderedList.addAll(filteredShelterList);
			}
			if(!filteredAccessList.isEmpty()){
				MapSearchEasyStopOverlay overlay = new MapSearchEasyStopOverlay(this, resultsMap);
				overlay.updateTramStopsList(filteredAccessList);
		        mapOverlays.add(overlay);
		        orderedList.addAll(filteredAccessList);
			}
			if(!filteredNormalList.isEmpty()){
				MapSearchStopOverlay overlay = new MapSearchStopOverlay(this, resultsMap);
				overlay.updateTramStopsList(filteredNormalList);
		        mapOverlays.add(overlay);
		        orderedList.addAll(filteredNormalList);
			}
		} 
		
		ArrayList<TicketOutlet> outletList = resultObject.getAlTicketOutlet();
		if(outletList != null){	
			MapSearchOutletOverlay overlay = new MapSearchOutletOverlay(this, resultsMap);
			overlay.updateTicketOutletsList(outletList);
	        mapOverlays.add(overlay);
	        orderedList.addAll(outletList);
		} 
		
		ArrayList<PointOfInterest> poiList = resultObject.getAlPointOfInterest();
		if(poiList != null){
			MapSearchPOIOverlay overlay = new MapSearchPOIOverlay(this, resultsMap);
			overlay.updatePointsOfInterestList(poiList);
	        mapOverlays.add(overlay);
	        orderedList.addAll(poiList);
		} 
		
		centerMapOnResults();
		
		resultsAdapter = new SearchResultsListArrayAdapter(this, resultObject, orderedList);
		resultsList.setAdapter(resultsAdapter);
		
	}
	
	
	public void centerMapOn(GeoPoint center, int max, boolean islat){
		MapController mapController = resultsMap.getController();

		if(islat){
			if(max < 20000){
				mapController.setZoom(16); // Zoom 16 few blocks
			} else if(max < 30000){
				mapController.setZoom(15); 
			} else if(max < 50000){
				mapController.setZoom(14); 
			} else if(max < 70000){
				mapController.setZoom(13); 
			} else if(max < 131000){
				mapController.setZoom(12); 
			} else if(max < 150000){
				mapController.setZoom(11); 
			} else {
				mapController.setZoom(10); // Zoom 10 is city view
			}
		} else {
			if(max < 25000){
				mapController.setZoom(16); // Zoom 16 few blocks
			} else if(max < 30000){
				mapController.setZoom(15); 
			} else if(max < 80000){
				mapController.setZoom(14); 
			} else if(max < 100000){
				mapController.setZoom(13); 
			} else if(max < 190000){
				mapController.setZoom(12); 
			} else if(max < 250000){
				mapController.setZoom(11); 
			} else {
				mapController.setZoom(10); // Zoom 10 is city view
			}			
		}
		
		if(center != null){
			mapController.animateTo(center);
		}
	}
	
	public void centerMapOnResults(){
		int maxLat = 0;
		int minLat = 0;
		int maxLon = 0;
		int minLon = 0;
		int latitude = 0;
		int longitude = 0;
		
		for(Object result: results){
			if(result instanceof Stop){
				Stop stop = (Stop) result;
				latitude = stop.getLatitudeE6();
				longitude = stop.getLongitudeE6();
			} else if(result instanceof TicketOutlet){
				TicketOutlet outlet = (TicketOutlet) result;
				latitude = outlet.getLatitudeE6();
				longitude = outlet.getLongitudeE6();
			} else if(result instanceof PointOfInterest){
				PointOfInterest poi = (PointOfInterest) result;
				latitude = (int)(poi.getLatitude() * 1E6);
				longitude = (int)(poi.getLongitude() * 1E6);
			}
				
			if(maxLat == 0){
				maxLat = latitude;
				minLat = latitude;
				maxLon = longitude;
				minLon = longitude;
			}
			maxLat = latitude > maxLat? latitude: maxLat;
			minLat = latitude < minLat? latitude: minLat;
			maxLon = longitude > maxLon? longitude: maxLon;
			minLon = longitude < minLon? longitude: minLon;
		}
		if(maxLat == 0){
			maxLat = (int) (-37.817491 * 1E6);
			minLat = (int) (-37.817491 * 1E6);
			maxLon = (int) (144.967445 * 1E6);
			minLon = (int) (144.967445 * 1E6);			
		}
		
		int dLat = maxLat - minLat;
		int dLon = maxLon - minLon;
		
		int cLat = minLat + (dLat/2);
		int cLon = minLon + (dLon/2);
		
		int max = 0;
		boolean isLat = false;
		if(dLat == 0 && dLon == 0){
			max = 150000;
		} else {
			if(dLat > dLon){
				max = dLat;
				isLat = true;
			} else{
				max = dLon;
				isLat = false;
			}
		}
		
		GeoPoint centre = new GeoPoint(cLat, cLon);
		centerMapOn(centre, max, isLat);
	}
	
	
	@Override
	public void onResume() {
		super.onResume();
		if(!isListViewSelected){
			myOverlay.enableMyLocation();
		}
		resultsMap.invalidate();
		
		Bundle extras = getIntent().getExtras(); 
		if(extras != null){
			resultObject = extras.getParcelable(INTENT_RESULTS_KEY);
			if(resultObject != null){
				updateUI();
			}
		}
	}
	
	@Override
	public void onPause() {
		super.onPause();
		if(!isListViewSelected){
			myOverlay.disableMyLocation();
		}
	}
	
	private void switchViews(){
		ToggleButton listButton = (ToggleButton) findViewById(R.id.list_view_button);
		ToggleButton mapButton = (ToggleButton) findViewById(R.id.map_view_button);
		if (isListViewSelected) {
			isListViewSelected = false;
			listButton.setChecked(false);
			mapButton.setChecked(true);
	        flipper.setDisplayedChild(VIEW_MAP);
	        myOverlay.enableMyLocation();
	        TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_header_mapview));
		} else {
			isListViewSelected = true; 
			listButton.setChecked(true);
			mapButton.setChecked(false);
	        flipper.setDisplayedChild(VIEW_LIST);
	        myOverlay.disableMyLocation();
	        TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_header_listview));
		}
	}
	
	
	private void refresh(){
		centerMapOnResults();
	}
	

	@Override
	public void onClick(View v) {
		ToggleButton listButton = (ToggleButton) findViewById(R.id.list_view_button);
		ToggleButton mapButton = (ToggleButton) findViewById(R.id.map_view_button);
		Button refreshButton = (Button) findViewById(R.id.refresh_button);
		if (v == listButton || v == mapButton) {
			switchViews();
		} else if(v == refreshButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_header_refresh));
			refresh();
		}
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}
	
}
