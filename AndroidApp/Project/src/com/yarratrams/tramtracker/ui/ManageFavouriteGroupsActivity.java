package com.yarratrams.tramtracker.ui;

import java.util.ArrayList;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.db.FavouriteManager;
import com.yarratrams.tramtracker.objects.FavouritesGroup;
import com.yarratrams.tramtracker.ui.util.DragNDropListArrayAdapter;
import com.yarratrams.tramtracker.ui.util.DragNDropListView;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;


public class ManageFavouriteGroupsActivity extends Activity implements OnClickListener {
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_FAVOURITES;
	public static final String INTENT_KEY = "favourites_list";
	
	FavouriteManager favouriteManager;
	
	private DragNDropListView groupsList;
	private DragNDropListArrayAdapter groupsAdapter;
	private ArrayList<FavouritesGroup> groups;
	private RelativeLayout noFavouritesMessage;
	
	private ProgressDialog loadDialog;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.favourites_manage_groups_screen);
		
		favouriteManager = new FavouriteManager(getApplicationContext());
		
		groups = new ArrayList<FavouritesGroup>();
		groupsList = (DragNDropListView) findViewById(R.id.simple_list);
		groupsAdapter = new DragNDropListArrayAdapter(groupsList, this, groups);
		groupsList.setAdapter(groupsAdapter);
		groupsList.setContext(getDialogContext());

		noFavouritesMessage = (RelativeLayout)findViewById(R.id.nofavourite_label);
        
		Button addGroup = (Button) findViewById(R.id.add_button);
		addGroup.setOnClickListener(this);
		
        loadDialog = new ProgressDialog(this);
	}
	
	public Context getDialogContext() {
		Context context;
		if (getParent() != null) {
			context = getParent();
		} else {
			context = this;
		}
		return context;
	}
	
	public void updateUI() {
		if(!loadDialog.isShowing()){
			loadDialog = ProgressDialog.show(getDialogContext(), "", getResources().getString(R.string.dialog_loading), true, true);
		}
		
		groups = favouriteManager.getAllFavourites();

		if(!isFavouriteListEmpty()){
			noFavouritesMessage.setVisibility(View.GONE);
			groupsList.setVisibility(View.VISIBLE);
			
			// Setup list
			groupsAdapter = new DragNDropListArrayAdapter(groupsList, this, groups);
			groupsList.setAdapter(groupsAdapter);
			
		} else {
			noFavouritesMessage.setVisibility(View.VISIBLE);
			groupsList.setVisibility(View.INVISIBLE);
		}
		
		if(loadDialog.isShowing()){
			loadDialog.dismiss();
		}
	}

	private boolean isFavouriteListEmpty(){
		if(groups != null && !groups.isEmpty()){
			return false;
		}
		return true;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		updateUI();
	}
	@Override
	public void onPause() {
		super.onPause();
	}
	
	
	@Override
	public void onClick(View v) {
		Button addGroup = (Button) findViewById(R.id.add_button);
		if (v == addGroup) {
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_header_addgroup));
			Intent intent = new Intent(getDialogContext(), AddFavouriteGroupActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_FAVOURITES, getResources().getString(R.string.tag_addfavouritegroup_screen), intent);
		}
	}
	
	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}
	
}
