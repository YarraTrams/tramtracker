package com.yarratrams.tramtracker.ui;

import com.google.android.maps.GeoPoint;
import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.PointOfInterest;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;


public class PointOfInterestActivity extends Activity implements OnClickListener{
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_NEARBY;
	public static final String INTENT_KEY = "poi_info";
	
	public PointOfInterest poi;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.poi_screen);
		
		Button directionsButton = (Button) findViewById(R.id.directions_button);
		directionsButton.setOnClickListener(this);
		
	}


	
	private void setPOIInfo(){
		TextView poiName = (TextView) findViewById(R.id.poi_name);
		TextView poiDescription = (TextView) findViewById(R.id.poi_description);
		TextView connectingTrain = (TextView) findViewById(R.id.connecting_train);
		TextView connectingTram = (TextView) findViewById(R.id.connecting_tram);
		TextView connectingBus = (TextView) findViewById(R.id.connecting_bus);

		String connectingTrainText = poi.getConnectingTrains() != null?poi.getConnectingTrains():getResources().getString(R.string.stop_empty_service);
		String connectingTramText = poi.getConnectingTrams() != null?poi.getConnectingTrams():getResources().getString(R.string.stop_empty_service);
		String connectingBusText = poi.getConnectingBuses() != null?poi.getConnectingBuses():getResources().getString(R.string.stop_empty_service);
		
		poiName.setText(poi.getName());
		poiDescription.setText(poi.getDescription());
		connectingTrain.setText(connectingTrainText);
		connectingTram.setText(connectingTramText);
		connectingBus.setText(connectingBusText);

	}
	

	@Override
	public void onClick(View v) {
		Button directionsButton = (Button)findViewById(R.id.directions_button);
		if(v == directionsButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_header_directions));
			GeoPoint destination = new GeoPoint((int)(poi.getLatitude()*1E6), (int)(poi.getLongitude()*1E6));
			TramTrackerMainActivity.getAppManager().requestDirectionsService(destination);
		}
	}

	@Override
	protected void onResume() {
		Bundle extras = getIntent().getExtras(); 
		if(extras != null){
			poi = extras.getParcelable(INTENT_KEY);
		}
		setPOIInfo();
		
		super.onResume();
	}


	
	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}
	
	
}
