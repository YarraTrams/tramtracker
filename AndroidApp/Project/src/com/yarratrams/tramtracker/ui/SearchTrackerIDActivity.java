package com.yarratrams.tramtracker.ui;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.tasks.SearchTrackerIDTask;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;


public class SearchTrackerIDActivity extends Activity implements OnClickListener{
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_MORE;

	private Stop stop;
	private ProgressDialog loadDialog;
	private InputMethodManager imm;
	
	private SearchTrackerIDTask searchTask;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_trackerid_screen);
		
		Button trackButton = (Button) findViewById(R.id.search_button);
		trackButton.setOnClickListener(this);
		
		final EditText trackerIDEditText = (EditText) findViewById(R.id.search_trackerid);
		trackerIDEditText.setOnClickListener(this);
		trackerIDEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
		    @Override
		    public void onFocusChange(View v, boolean hasFocus) {
		        if(hasFocus){
		            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
		        }
		    }
		});
		trackerIDEditText.setOnEditorActionListener(new OnEditorActionListener() {        
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if(actionId==EditorInfo.IME_ACTION_DONE || event.getKeyCode() == KeyEvent.KEYCODE_ENTER){
					search(trackerIDEditText);
				}
				return false;
			}
		});
		
		stop = new Stop();
		loadDialog = new ProgressDialog(this);
	}
	
	
	private Context getDialogContext() {
		Context context;
		if (getParent() != null){
			context = getParent();
		} else {
			context = this;
		}
		return context;
	}
	
	
	@Override
	public void onClick(View v) {
		EditText trackerIDEditText = (EditText) findViewById(R.id.search_trackerid);
		Button trackButton = (Button) findViewById(R.id.search_button);
		if(v == trackButton){
			search(trackerIDEditText);
		
		} else if(v == trackerIDEditText){
			trackerIDEditText.clearFocus();
			imm.showSoftInput(trackerIDEditText, InputMethodManager.SHOW_FORCED);
		}
	}
	
	private void search(EditText trackerIDEditText){
		TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_trackerid));
		if(trackerIDEditText.length() < 1){
			TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.error_trackerid_nonumber));
			return;
		}
		
		imm.hideSoftInputFromWindow(trackerIDEditText.getWindowToken(), 0);
		
		if(!loadDialog.isShowing()){
			loadDialog = ProgressDialog.show(getDialogContext(), "", getResources().getString(R.string.dialog_loading), true, true);
		}
		stop.setTrackerID(Integer.parseInt(trackerIDEditText.getText().toString()));

		if(searchTask != null){
			searchTask.cancel(true);
		}
		searchTask = new SearchTrackerIDTask(this, String.valueOf(stop.getTrackerID())); 
		searchTask.execute();
	}
	
	
	public void updateUI(Stop result) {
		if(loadDialog.isShowing()){
			loadDialog.dismiss();
		}
		if(result == null){
			TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.error_trackerid_invalidid));
		} else {
			stop = result;
			goToPIDScreen(stop);
		}
	}
	
	
	private void goToPIDScreen(Stop stop){
		Intent intent = new Intent(this, PIDActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(PIDActivity.INTENT_KEY, stop);
		
		TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, 
				getResources().getString(R.string.tag_pid_screen), intent);
	}
	
	@Override
	protected void onResume() {
		EditText trackerIDEditText = (EditText) findViewById(R.id.search_trackerid);
		trackerIDEditText.clearFocus();
		imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		super.onResume();
	}
	
	
	@Override
	protected void onPause() {
		EditText trackerIDEditText = (EditText) findViewById(R.id.search_trackerid);
		imm.hideSoftInputFromWindow(trackerIDEditText.getWindowToken(), 0);
		super.onPause();
	}


	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}
	
}
