package com.yarratrams.tramtracker.ui;

import java.util.ArrayList;
import java.util.List;

import com.yarratrams.tramtracker.R;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.yarratrams.tramtracker.objects.PredictedArrivalTime;
import com.yarratrams.tramtracker.objects.PredictedTimeResult;
import com.yarratrams.tramtracker.objects.ServiceChange;
import com.yarratrams.tramtracker.objects.ServiceDisruption;
import com.yarratrams.tramtracker.objects.Tram;
import com.yarratrams.tramtracker.singleton.PredictedTimeResultSingleton;
import com.yarratrams.tramtracker.tasks.MyTramLocationListener;
import com.yarratrams.tramtracker.tasks.MyTramTask;
import com.yarratrams.tramtracker.ui.util.MapMyTramCurrentOverlay;
import com.yarratrams.tramtracker.ui.util.MapMyTramOverlay;
import com.yarratrams.tramtracker.ui.util.OnBoardChangeAlertArrayAdapter;
import com.yarratrams.tramtracker.ui.util.OnBoardDisruptionAlertArrayAdapter;
import com.yarratrams.tramtracker.ui.util.OnBoardExpandableListArrayAdapter;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.ViewFlipper;


public class OnBoardActivity extends MapActivity implements OnClickListener{
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_MYTRAM;
	public static final String INTENT_KEY = "tram_info";
	public static final int VIEW_LIST = 0;
	public static final int VIEW_MAP = 1;
	
	private boolean isListViewSelected;
	private ViewFlipper flipper;
	
	private Tram tram;
	
	private ExpandableListView stopsList;
	private OnBoardExpandableListArrayAdapter onBoardAdapter;
	private PredictedTimeResult results;
	private ArrayList<PredictedArrivalTime> stops;
	private int startingIndex;
	private boolean isAccurateIndex;
	private int scrollingPosition;
	private int firstPosition;
	
	private MapView stopsMap;
	private List<Overlay> mapOverlays;
	private GeoPoint mapCentre;
	private int zoomLevel;
	
	private ArrayList<ServiceDisruption> serviceDisruptions;
	private ArrayList<ServiceChange> serviceChanges;
	private ListView disruptionAlertList;
	private ListView changeAlertList;
	private OnBoardDisruptionAlertArrayAdapter disruptionAdapter;
	private OnBoardChangeAlertArrayAdapter changeAdapter;
	
	private boolean isChangeAlertDisplayed;
	private boolean isChangeAlertExpanded;
	private boolean isDisruptionAlertDisplayed;
	private boolean isDisruptionAlertExpanded;

	private boolean showAlerts;
	private boolean pauseWebServiceUpdate;
	private LocationManager locationManager;
	private MyTramLocationListener locationListener;
	
	private ProgressDialog loadDialog;
	
	private MyTramTask task;
	private Handler updatesHandler;
	private Runnable timeUpdater = new Runnable() {
		public void run() {
			//System.out.println("........... WS: " + pauseWebServiceUpdate);
			if(pauseWebServiceUpdate == false){
				getTimesUpdate();
				updatesHandler.postDelayed(this, 20000);
			}
		}
	};
	
	public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			PredictedTimeResult result = PredictedTimeResultSingleton.getResult();
			if(!PredictedTimeResultSingleton.isAvailable()){
				TramTrackerMainActivity.getAppManager().displayErrorMessage(getString(R.string.notify_onboard_notramtracker));
				noTimesUpdate(result);
			}
			if(result != null)
				updateUI(result); 
		}
	};
	
	Intent alarmServiceIntent;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.onboard_screen);
		
		isListViewSelected = true;
		flipper = (ViewFlipper) findViewById(R.id.view_flipper);
		
		tram = new Tram();
		
		ToggleButton listButton = (ToggleButton) findViewById(R.id.list_view_button);
		listButton.setOnClickListener(this);
		ToggleButton mapButton = (ToggleButton) findViewById(R.id.map_view_button);
		mapButton.setOnClickListener(this);
		Button refreshButton = (Button) findViewById(R.id.refresh_button);
		refreshButton.setOnClickListener(this);
		refreshButton.setVisibility(View.INVISIBLE);
		
		stops = new ArrayList<PredictedArrivalTime>();
		stopsList = (ExpandableListView) findViewById(R.id.expandable_list);
		stopsList.setKeepScreenOn(true);
		stopsList.addHeaderView(getTableView());
		
		Display display = getWindowManager().getDefaultDisplay();
		Drawable groupIndicator = getResources().getDrawable(R.drawable.icn_list_expandable);
		stopsList.setGroupIndicator(groupIndicator);
		stopsList.setChildIndicator(null);
		stopsList.setIndicatorBounds(display.getWidth()-GetDipsFromPixel(30), display.getWidth());
		//stopsList.setIndicatorBounds(stopsList.getRight()-GetDipsFromPixel(30), stopsList.getWidth());
		startingIndex = -1;
		isAccurateIndex = false;
//		onBoardAdapter = new OnBoardExpandableListArrayAdapter(stopsList, this, new PredictedTimeResult());
//		stopsList.setAdapter(onBoardAdapter);
		
		stopsMap = (MapView)findViewById(R.id.map);
		stopsMap.setBuiltInZoomControls(true);
        mapOverlays = stopsMap.getOverlays();
		
        serviceChanges = new ArrayList<ServiceChange>();
		RelativeLayout changeAlert = (RelativeLayout) findViewById(R.id.change_alert);
		changeAlert.setOnClickListener(this);
		changeAlertList = (ListView) findViewById(R.id.change_list_view);
		changeAdapter = new OnBoardChangeAlertArrayAdapter(this, serviceChanges);
		changeAlertList.setAdapter(changeAdapter);
		createChangeAlert(serviceChanges);
        
        serviceDisruptions = new ArrayList<ServiceDisruption>();
		RelativeLayout disruptionAlert = (RelativeLayout) findViewById(R.id.disruption_alert);
		disruptionAlert.setOnClickListener(this);
		disruptionAlertList = (ListView) findViewById(R.id.disruption_list_view);
		disruptionAdapter = new OnBoardDisruptionAlertArrayAdapter(this, serviceDisruptions);
		disruptionAlertList.setAdapter(disruptionAdapter);
		createDisruptionAlert(serviceDisruptions);
		
		expandAlert();
        
        updatesHandler = new Handler();
        
        loadDialog = new ProgressDialog(this);
        
        //added by Ab
//        AlarmManager.registerBroastcastReceiver(this);
        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        locationListener = new MyTramLocationListener(this, locationManager);
        
	}
	
	
	public Context getDialogContext() {
		Context context;
		if (getParent() != null) {
			context = getParent();
		} else {
			context = this;
		}
		return context;
	}
	
	
	public int GetDipsFromPixel(float pixels) {
		final float scale = getResources().getDisplayMetrics().density;
		return (int) (pixels * scale + 0.5f);
	}
	
	private View getTableView(){
		LayoutInflater inflater = getLayoutInflater();
		View rowView = inflater.inflate(R.layout.routes_stops_list_view_table, null, true);
		return rowView;
	}
	
	
	private void createChangeAlert(ArrayList<ServiceChange> serviceChanges){
		RelativeLayout changeAlert = (RelativeLayout)findViewById(R.id.change_alert);
		TextView routeTitle = (TextView)findViewById(R.id.pid_change_title);
		routeTitle.setVisibility(View.GONE);
		if(serviceChanges.isEmpty()){
			changeAlert.setVisibility(View.GONE);
			isChangeAlertDisplayed = false;
		} else {
			if(!isChangeAlertDisplayed){
				showAlerts = true;
			}
			changeAlert.setVisibility(View.VISIBLE);
			isChangeAlertDisplayed = true;
		}
	}
	
	private void createDisruptionAlert(ArrayList<ServiceDisruption> serviceDisruptions){
		RelativeLayout disruptionAlert = (RelativeLayout)findViewById(R.id.disruption_alert);
		TextView routeTitle = (TextView)findViewById(R.id.pid_disruption_title);
		if(serviceDisruptions.isEmpty()){
			disruptionAlert.setVisibility(View.GONE);
			isDisruptionAlertDisplayed = false;
		} else {
			if(!isDisruptionAlertDisplayed){
				showAlerts = true;
			}
			disruptionAlert.setVisibility(View.VISIBLE);
			isDisruptionAlertDisplayed = true;
			if(serviceDisruptions.get(0).isText()){
				routeTitle.setVisibility(View.GONE);
			} else {
				routeTitle.setVisibility(View.VISIBLE);
			}
		}
	}
	
	
	public void updateUI(PredictedTimeResult results) {
		boolean refreshMap = true;
		if(stops != null){
			if(!stops.isEmpty()){
				refreshMap = false;
			}
		}
		//System.out.println("....... UPDATE test!!!");
//		PredictedTimeResultSingleton.setResult(results);
		if(results != null){
			if(this.results != null){
				if(!this.results.getAlPredictedArrivalTime().isEmpty() && !results.getAlPredictedArrivalTime().isEmpty()){
					if(this.results.getAlPredictedArrivalTime().get(0).getStop().getTrackerID() != results.getAlPredictedArrivalTime().get(0).getStop().getTrackerID())
						startingIndex = -1;
				}
			}

			if(!PredictedTimeResultSingleton.isAvailable()){
				TramTrackerMainActivity.getAppManager().displayErrorMessage(getString(R.string.notify_onboard_notramtracker));
				noTimesUpdate(results);
			}
			this.results = results;

			TextView routeName = (TextView) findViewById(R.id.route_name);
			TextView tramNumber = (TextView) findViewById(R.id.tram_number);
			routeName.setText(getRouteName());
			tramNumber.setText(getTramNumber());
			
//			System.out.println("............ STOPS UPDATE");
//			System.out.println("............ STOPS " +  results.getAlPredictedArrivalTime().size());
//			System.out.println("............ INDEX " +  startingIndex);
//			System.out.println("............ NEW " +  results.getStartingIndex());
			if(results.getAlPredictedArrivalTime() != null && !results.getAlPredictedArrivalTime().isEmpty()){
				stops = results.getAlPredictedArrivalTime();
			}
			if(stops == null){
				stops = new ArrayList<PredictedArrivalTime>();
			}
			
			// Setup list
			if(onBoardAdapter == null){
				onBoardAdapter = new OnBoardExpandableListArrayAdapter(this, results);
				stopsList.setAdapter(onBoardAdapter);
			} else {
				getListPosition();
				onBoardAdapter = new OnBoardExpandableListArrayAdapter(this, results);
				stopsList.setAdapter(onBoardAdapter);
				onBoardAdapter.setStartingIndex(startingIndex, isAccurateIndex);

				restoreListPosition();
				//setListPosition();

				//onBoardAdapter.updateList(results, startingIndex);
			}
			calculateSelectedIndex(results.getStartingIndex());

			expandAll();
			
			if(refreshMap){
				centerMapOnStops();
			}
			
			serviceChanges.clear();
			serviceDisruptions.clear();

			if(results.getServiceChange() != null){
				serviceChanges.add(results.getServiceChange());
			}
			if(results.getServiceDisruption() != null){
				serviceDisruptions.add(results.getServiceDisruption());
			}
			createChangeAlert(serviceChanges);
			createDisruptionAlert(serviceDisruptions);
			if(showAlerts){
				expandAlert();
			}
		}
		
		if(loadDialog.isShowing()){
			loadDialog.dismiss();
		}
	}
	
	private void calculateSelectedIndex(int newIndex){
//		System.out.println("............ FROM WS");
		if(newIndex == -1){
			startingIndex = 0;
			isAccurateIndex = false;
			if(onBoardAdapter != null){
				onBoardAdapter.setStartingIndex(startingIndex, isAccurateIndex);
				updateMapStartingIndex();
				setListPosition();
			}
			
		} else if(startingIndex == -1 || newIndex > startingIndex){
			startingIndex = newIndex;
			isAccurateIndex = false;
			if(onBoardAdapter != null){
				onBoardAdapter.setStartingIndex(startingIndex, isAccurateIndex);
				updateMapStartingIndex();
				setListPosition();
			}
		} else {
			//manualTimesUpdate(startingIndex);
		}
	}
	
	public void updateStartingIndex(int newIndex){
//		System.out.println("............ FROM GPS");
		if(startingIndex == -1 || newIndex >= startingIndex){
			int oldStartingIndex = startingIndex;
			startingIndex = newIndex;
			isAccurateIndex = true;
			manualTimesUpdate(startingIndex);
			if(onBoardAdapter != null){
				Log.e("","kickoff stop = "  + stops.get(startingIndex).getStop().getStopName() +  " startingIndex = " + startingIndex);
				onBoardAdapter.setStartingIndex(results, startingIndex, isAccurateIndex);
				updateMapStartingIndex();
				if(newIndex > oldStartingIndex)
					setListPosition();
			}
		} else {
			manualTimesUpdate(startingIndex);
		}
	}
	
	public void setListPosition(){
		ArrayList<String> alSuburb = new ArrayList<String>();
		Log.w("setListPosition","stops.size() = " + stops.size());
		for(int i=0;i < stops.size() && i <= startingIndex; i++){
			if(alSuburb.size() > 0){
//				Log.w("setListPosition","alSuburb.get(alSuburb.size() - 1) = " + alSuburb.get(alSuburb.size() - 1));
//				Log.w("setListPosition","stops.get(i).getStop().getSuburb() = " + stops.get(i).getStop().getSuburb());
				if(!alSuburb.get(alSuburb.size() - 1).equalsIgnoreCase(stops.get(i).getStop().getSuburb())){
					alSuburb.add(stops.get(i).getStop().getSuburb());
				}
			}
			else
				alSuburb.add(stops.get(i).getStop().getSuburb());
		}
		Log.w("setListPosition","startingIndex = " + startingIndex);
		Log.w("setListPosition","alSuburb = " + alSuburb);
		int finalIndex = startingIndex + alSuburb.size() + 1;
		Log.w("setListPosition","stopsList.getCount() = " + stopsList.getCount());
		Log.w("setListPosition","setSelectionFromTop  = startingIndex + alSuburb.size() + 1 = " + finalIndex);
		stopsList.setSelectionFromTop(finalIndex, 0);
	}
	
	private void getListPosition(){
		if(stopsList == null){
			return;
		}
		firstPosition = stopsList.getFirstVisiblePosition();
		View vfirst = stopsList.getChildAt(0);
		scrollingPosition = 0;
		if (vfirst != null) scrollingPosition = vfirst.getTop();
	}
	
	private void restoreListPosition(){
		if(stopsList == null){
			return;
		}
		stopsList.setSelectionFromTop(firstPosition, scrollingPosition);
	}
	
	public void updateStopToNow(){
		if(onBoardAdapter != null){
			onBoardAdapter.setNowIndex();
		}
	}
	
	private void manualTimesUpdate(int index){
		if(results != null){
			ArrayList<PredictedArrivalTime> times = results.getAlPredictedArrivalTime(); 
			for(int i = 0; i < index; i++){
				PredictedArrivalTime stop = times.get(index);
				stop.setArrivalTime(null);
			}
		}
	}
	private void noTimesUpdate(PredictedTimeResult result){
		if(result != null){
			ArrayList<PredictedArrivalTime> times = result.getAlPredictedArrivalTime(); 
			for(int i = 0; i < times.size(); i++){
				PredictedArrivalTime stop = times.get(i);
				stop.setArrivalTime(null);
			}
		}
	}
	
	private void updateMapStartingIndex(){
//		System.out.println("............ STOPS" + stops.size());
		if(stops.isEmpty()){
			return;
		}
			
		ArrayList<PredictedArrivalTime> normalStops = new ArrayList<PredictedArrivalTime>();
		normalStops.addAll(stops);
		PredictedArrivalTime stop = stops.get(startingIndex);
		normalStops.remove(startingIndex);
		
		ArrayList<PredictedArrivalTime> currentStops = new ArrayList<PredictedArrivalTime>();
		currentStops.add(stop);
		
		if(!isAccurateIndex){
			if(startingIndex < stops.size()-1){
				stop = stops.get(startingIndex + 1);
				normalStops.remove(startingIndex);
				currentStops.add(stop);
			}
		}
		
		mapOverlays.clear();
		
		MapMyTramOverlay overlay = new MapMyTramOverlay(this, stopsMap);
		overlay.updateStopsList(normalStops);
		mapOverlays.add(overlay);

		MapMyTramCurrentOverlay currentOverlay = new MapMyTramCurrentOverlay(this, stopsMap);
		currentOverlay.updateStopsList(currentStops);
		mapOverlays.add(currentOverlay);
		
		stopsMap.invalidate();
	}
	
	private String getRouteName(){
		String text = "";
		
		text = results.getRoute().getRouteNumber();
		text = text.concat(getResources().getString(R.string.routes_entry_name_dash));
		if(results.getRoute().isUpDestination()){
			if(results.getRoute().getDownDestination() != null){
				if(results.getRoute().getDownDestination().length() > 0){
					text = text.concat(results.getRoute().getDownDestination());
				}
			}
			if(results.getRoute().getUpDestination() != null){
				if(results.getRoute().getUpDestination().length() > 0){
					text = text.concat(getResources().getString(R.string.routes_entry_name_to));
					text = text.concat(results.getRoute().getUpDestination());
				}
			}
		} else {
			if(results.getRoute().getUpDestination() != null){
				if(results.getRoute().getUpDestination().length() > 0){
					text = text.concat(results.getRoute().getUpDestination());
				}
			}
			if(results.getRoute().getDownDestination() != null){
				if(results.getRoute().getDownDestination().length() > 0){
					text = text.concat(getResources().getString(R.string.routes_entry_name_to));
					text = text.concat(results.getRoute().getDownDestination());
				}
			}
		}
		
		return text;
	}
	
	private String getTramNumber(){
		String text = "";
		text = getResources().getString(R.string.onboard_title_tram);
		text = text.concat(String.valueOf(results.getTram().getVehicleNumber()));
		return text;
	}
		
	
	private void expandAlert(){
		if(isDisruptionAlertDisplayed){
			switchChangeAlertState(false);
			switchDisruptionAlertState(true);
		} else if(isChangeAlertDisplayed){
			switchChangeAlertState(true);
		}
		showAlerts = false;
	}
	
	public void switchDisruptionAlertState() {
		LinearLayout list = (LinearLayout) findViewById(R.id.disruption_list);
		TextView showText = (TextView) findViewById(R.id.disruption_show);
		ImageView showIcon = (ImageView) findViewById(R.id.disruption_show_image);
		TextView hideText = (TextView) findViewById(R.id.disruption_hide);
		ImageView hideIcon = (ImageView) findViewById(R.id.disruption_hide_image);
		if (isDisruptionAlertExpanded) {
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_pid_hide));
			isDisruptionAlertExpanded = false;
			list.setVisibility(View.GONE);
			hideText.setVisibility(View.GONE);
			hideIcon.setVisibility(View.GONE);
			showText.setVisibility(View.VISIBLE);
			showIcon.setVisibility(View.VISIBLE);
		} else {
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_pid_show));
			isDisruptionAlertExpanded = true;
			list.setVisibility(View.VISIBLE);
			hideText.setVisibility(View.VISIBLE);
			hideIcon.setVisibility(View.VISIBLE);
			showText.setVisibility(View.GONE);
			showIcon.setVisibility(View.GONE);
		}
	}
	
	private void switchDisruptionAlertState(boolean expand){
		LinearLayout list = (LinearLayout) findViewById(R.id.disruption_list);
		TextView showText = (TextView) findViewById(R.id.disruption_show);
		ImageView showIcon = (ImageView) findViewById(R.id.disruption_show_image);
		TextView hideText = (TextView) findViewById(R.id.disruption_hide);
		ImageView hideIcon = (ImageView) findViewById(R.id.disruption_hide_image);
		if (!expand) {
			isDisruptionAlertExpanded = false;
			list.setVisibility(View.GONE);
			hideText.setVisibility(View.GONE);
			hideIcon.setVisibility(View.GONE);
			showText.setVisibility(View.VISIBLE);
			showIcon.setVisibility(View.VISIBLE);
		} else {
			isDisruptionAlertExpanded = true;
			list.setVisibility(View.VISIBLE);
			hideText.setVisibility(View.VISIBLE);
			hideIcon.setVisibility(View.VISIBLE);
			showText.setVisibility(View.GONE);
			showIcon.setVisibility(View.GONE);
		}
	}
	
	public void switchChangeAlertState() {
		LinearLayout list = (LinearLayout) findViewById(R.id.change_list);
		TextView showText = (TextView) findViewById(R.id.change_show);
		ImageView showIcon = (ImageView) findViewById(R.id.change_show_image);
		TextView hideText = (TextView) findViewById(R.id.change_hide);
		ImageView hideIcon = (ImageView) findViewById(R.id.change_hide_image);
		if (isChangeAlertExpanded) {
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_pid_hide));
			isChangeAlertExpanded = false;
			list.setVisibility(View.GONE);
			hideText.setVisibility(View.GONE);
			hideIcon.setVisibility(View.GONE);
			showText.setVisibility(View.VISIBLE);
			showIcon.setVisibility(View.VISIBLE);
		} else {
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_pid_show));
			isChangeAlertExpanded = true;
			list.setVisibility(View.VISIBLE);
			hideText.setVisibility(View.VISIBLE);
			hideIcon.setVisibility(View.VISIBLE);
			showText.setVisibility(View.GONE);
			showIcon.setVisibility(View.GONE);
		}
	} 
	
	private void switchChangeAlertState(boolean expand) {
		LinearLayout list = (LinearLayout) findViewById(R.id.change_list);
		TextView showText = (TextView) findViewById(R.id.change_show);
		ImageView showIcon = (ImageView) findViewById(R.id.change_show_image);
		TextView hideText = (TextView) findViewById(R.id.change_hide);
		ImageView hideIcon = (ImageView) findViewById(R.id.change_hide_image);
		if (!expand) {
			isChangeAlertExpanded = false;
			list.setVisibility(View.GONE);
			hideText.setVisibility(View.GONE);
			hideIcon.setVisibility(View.GONE);
			showText.setVisibility(View.VISIBLE);
			showIcon.setVisibility(View.VISIBLE);
		} else {
			isChangeAlertExpanded = true;
			list.setVisibility(View.VISIBLE);
			hideText.setVisibility(View.VISIBLE);
			hideIcon.setVisibility(View.VISIBLE);
			showText.setVisibility(View.GONE);
			showIcon.setVisibility(View.GONE);
		}
	}
	
	
	private void expandAll(){
		for(int i = 0; i < onBoardAdapter.getGroupCount(); i ++){
			stopsList.expandGroup(i);
		}
	}
	
	
	public void onResume() {
		super.onResume();
		pauseWebServiceUpdate = false;
		Bundle extras = getIntent().getExtras();
		if(extras != null){
			tram = extras.getParcelable(INTENT_KEY);
		}
		
		if(mapCentre != null){
			MapController mapController = stopsMap.getController();
			mapController.setCenter(mapCentre);
			mapController.setZoom(zoomLevel);
		}
		
		if(results != null){
			updateUI(results);
//			System.out.println("........... ON RESUME");
//			System.out.println("..........." + results.getAlPredictedArrivalTime());
//			System.out.println("..........." + results.getAlPredictedArrivalTime().size());
//			onBoardAdapter.updateList(results, startingIndex);
//			stopsList.setAdapter(onBoardAdapter);
		} else {
			if(!loadDialog.isShowing()){
				loadDialog = ProgressDialog.show(getDialogContext(), "", getResources().getString(R.string.dialog_loading), true, true);
			}
		}
		
		if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
		}
		
//		locationListener = new MyTramLocationListener(this);
//		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, Constants.kLocationServicesMinUpdateTime, Constants.kLocationServicesMinUpdateDistance, locationListener);
		updatesHandler.post(timeUpdater);
	}
	
	@Override
	public void onPause() {
		super.onPause();
		pauseWebServiceUpdate = true;
		locationManager.removeUpdates(locationListener);
		zoomLevel = stopsMap.getZoomLevel();
		mapCentre = stopsMap.getMapCenter();
	}

	
	private void switchViews(){
		ToggleButton listButton = (ToggleButton) findViewById(R.id.list_view_button);
		ToggleButton mapButton = (ToggleButton) findViewById(R.id.map_view_button);
		Button refreshButton = (Button) findViewById(R.id.refresh_button);
		if (isListViewSelected) {
			isListViewSelected = false;
			listButton.setChecked(false);
			mapButton.setChecked(true);
			refreshButton.setVisibility(View.VISIBLE);
	        flipper.setDisplayedChild(VIEW_MAP);
	        TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_header_mapview));
		} else {
			isListViewSelected = true;
			listButton.setChecked(true);
			mapButton.setChecked(false);
			refreshButton.setVisibility(View.INVISIBLE);
	        flipper.setDisplayedChild(VIEW_LIST);
	        TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_header_listview));
		}
	}
	
	
//	public void centerMapOn(GeoPoint center, int max, boolean islat){
//		MapController mapController = stopsMap.getController();
//		
//		if(islat){
//			if(max < 20000){
//				zoomLevel = 16; // Zoom 16 few blocks
//			} else if(max < 30000){
//				zoomLevel = 15; 
//			} else if(max < 50000){
//				zoomLevel = 14; 
//			} else if(max < 70000){
//				zoomLevel = 13; 
//			} else if(max < 131000){
//				zoomLevel = 12; 
//			} else if(max < 150000){
//				zoomLevel = 11; 
//			} else {
//				zoomLevel = 10; // Zoom 10 is city view
//			}
//		} else {
//			if(max < 25000){
//				zoomLevel = 16; // Zoom 16 few blocks
//			} else if(max < 30000){
//				zoomLevel= 15; 
//			} else if(max < 80000){
//				zoomLevel = 14; 
//			} else if(max < 100000){
//				zoomLevel = 13; 
//			} else if(max < 190000){
//				zoomLevel = 12; 
//			} else if(max < 250000){
//				zoomLevel = 11; 
//			} else {
//				zoomLevel = 10; // Zoom 10 is city view
//			}
//		}
//		
//		mapController.setZoom(zoomLevel);
//		if(center != null){
//			mapCentre = center;
//			mapController.animateTo(mapCentre);
//		}
//	}
	
	public void centerMapOnStops(){
		int maxLat = 0;
		int minLat = 0;
		int maxLon = 0;
		int minLon = 0;
		
		for(PredictedArrivalTime stop: results.getAlPredictedArrivalTime()){
			if(maxLat == 0){
				maxLat = stop.getStop().getLatitudeE6();
				minLat = stop.getStop().getLatitudeE6();
				maxLon = stop.getStop().getLongitudeE6();
				minLon = stop.getStop().getLongitudeE6();
			}
			maxLat = stop.getStop().getLatitudeE6() > maxLat? stop.getStop().getLatitudeE6(): maxLat;
			minLat = stop.getStop().getLatitudeE6() < minLat? stop.getStop().getLatitudeE6(): minLat;
			maxLon = stop.getStop().getLongitudeE6() > maxLon? stop.getStop().getLongitudeE6(): maxLon;
			minLon = stop.getStop().getLongitudeE6() < minLon? stop.getStop().getLongitudeE6(): minLon;
		}
		if(maxLat == 0){
			// The map is zoomed and centered at Flinders St Station
			maxLat = (int) (-37.817491 * 1E6);
			minLat = (int) (-37.817491 * 1E6);
			maxLon = (int) (144.967445 * 1E6);
			minLon = (int) (144.967445 * 1E6);			
		}
		
		int dLat = maxLat - minLat;
		int dLon = maxLon - minLon;
		
		int cLat = minLat + (dLat/2);
		int cLon = minLon + (dLon/2);
		
//		int max = 0;
//		boolean isLat = false;
//		if(dLat == 0 && dLon == 0){
//			max = 150000;
//		} else {
//			if(dLat > dLon){
//				max = dLat;
//				isLat = true;
//			} else{
//				max = dLon;
//				isLat = false;
//			}
//		}
		
		GeoPoint centre = new GeoPoint(cLat, cLon);
		//centerMapOn(centre, max, isLat);
		MapController mapController = stopsMap.getController();
		mapController.zoomToSpan(dLat, dLon);
		if(centre != null){
			mapCentre = centre;
			mapController.animateTo(mapCentre);
		}		
	}
	
	
	private void refresh(){
		centerMapOnStops();
	}
	
	@Override
	public void onClick(View v) {
		ToggleButton listButton = (ToggleButton) findViewById(R.id.list_view_button);
		ToggleButton mapButton = (ToggleButton) findViewById(R.id.map_view_button);
		Button refreshButton = (Button) findViewById(R.id.refresh_button);
		RelativeLayout changeAlert = (RelativeLayout) findViewById(R.id.change_alert);
		RelativeLayout disruptionAlert = (RelativeLayout) findViewById(R.id.disruption_alert);
		
		if (v == listButton || v == mapButton) {
			switchViews();
		} else if(v == refreshButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_header_refresh));
			refresh();
		} else if (v == changeAlert) {
			switchChangeAlertState();
		} else if (v == disruptionAlert) {
			switchDisruptionAlertState();
		}
	}
	
	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}
	
	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}
	
	
	private void getTimesUpdate(){
		task = new MyTramTask(this, tram);
		task.execute();
	}

}
