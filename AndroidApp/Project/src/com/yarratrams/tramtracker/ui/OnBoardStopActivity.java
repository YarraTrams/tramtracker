package com.yarratrams.tramtracker.ui;

import java.util.ArrayList;

import com.google.android.maps.GeoPoint;
import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.db.FavouriteManager;
import com.yarratrams.tramtracker.db.POIManager;
import com.yarratrams.tramtracker.objects.PointOfInterest;
import com.yarratrams.tramtracker.objects.PredictedArrivalTime;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.objects.Tram;
import com.yarratrams.tramtracker.ui.util.MenuOnBoardStopAdapter;
import com.yarratrams.tramtracker.ui.util.PointsOfInterestListAdapter;
import com.yarratrams.tramtracker.ui.util.WrappingSlidingDrawer;

import android.app.Activity;
import android.app.ActivityGroup;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class OnBoardStopActivity extends Activity implements OnClickListener{
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_MYTRAM;
	public static final String INTENT_KEY = "stop_info";
	public static final String INTENT_KEY_PREDICTED = "predicted_info";
	
	public Stop stop;
	public Tram tram;
	public ArrayList<PredictedArrivalTime> stops;
	
	private ArrayList<PointOfInterest> pois;
	private LinearLayout poiList; 
	
	private WrappingSlidingDrawer slidingMenu;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.onboard_stop_screen);
				
//		ToggleButton setAlarm = (ToggleButton) findViewById(R.id.alarm_button);
//		setAlarm.setOnClickListener(this);
		Button setAlarm = (Button) findViewById(R.id.alarm_button);
		setAlarm.setOnClickListener(this);
		
		poiList = (LinearLayout) findViewById(R.id.simple_list);
		
		TextView footer = (TextView) findViewById(R.id.stop_footer);
		footer.setMovementMethod(LinkMovementMethod.getInstance());
		
		slidingMenu = (WrappingSlidingDrawer) findViewById(R.id.sliding_menu);
        GridView menuContainer = (GridView) findViewById(R.id.sliding_menu_container);
        MenuOnBoardStopAdapter menuAdapter = new MenuOnBoardStopAdapter(this);
        menuContainer.setAdapter(menuAdapter);
	}

	
	private void setStopInfo(){
//		Button setAlarm = (Button) findViewById(R.id.alarm_button);
		TextView stopName = (TextView) findViewById(R.id.stop_name);
		TextView connectingTrain = (TextView) findViewById(R.id.connecting_train);
		TextView connectingTram = (TextView) findViewById(R.id.connecting_tram);
		TextView connectingBus = (TextView) findViewById(R.id.connecting_bus);
//		ToggleButton setAlarm = (ToggleButton) findViewById(R.id.alarm_button);
		
		String connectingTrainText = getConnectingService(stop.getConnectingTrains());
		String connectingTramText = getConnectingService(stop.getConnectingTrams());
		String connectingBusText = getConnectingService(stop.getConnectingBuses());
		
		stopName.setText(stop.getStopName());
		connectingTrain.setText(connectingTrainText);
		connectingTram.setText(connectingTramText);
		connectingBus.setText(connectingBusText);
		
		if(pois.size() == 0){
			RelativeLayout poiTitle = (RelativeLayout) findViewById(R.id.poi_title);
			poiTitle.setVisibility(View.GONE);
		}
		
//		if(TramTrackerMainActivity.getAppManager().hasAlarm(stop)){
//			setAlarm.setChecked(true);
//		} else {
//			setAlarm.setChecked(false);
//		}
		
//		if(stop.getTrackerID() >= 8000){
//			setAlarm.setVisibility(View.INVISIBLE);
//		}
	}
	
	private String getConnectingService(String service){
		if(service != null){
			if(!service.trim().equals("")){
				return service;
			}
		}
		
		return getResources().getString(R.string.stop_empty_service);
	}

	@Override
	public void onClick(View v) {
//		ToggleButton setAlarm = (ToggleButton) findViewById(R.id.alarm_button);
		Button setAlarm = (Button) findViewById(R.id.alarm_button);

		if(v == setAlarm){
//			if(setAlarm.isChecked()){
//				AlarmManager.startAlarmService(stop, stops,PredictedTimeResultSingleton.getResult().getTram());
//			} else {
//				AlarmManager.stopAlarmService(this);
//			}
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_header_directions));
			GeoPoint destination = new GeoPoint(stop.getLatitudeE6(), stop.getLongitudeE6());
			TramTrackerMainActivity.getAppManager().requestDirectionsService(destination);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		Bundle extras = getIntent().getExtras(); 
		if(extras != null){
			stop = extras.getParcelable(INTENT_KEY);
			stops = extras.getParcelableArrayList(INTENT_KEY_PREDICTED);
		}

		pois = POIManager.getPOIForStop(stop.getTrackerID(), this);
		if(pois == null){
			pois = new ArrayList<PointOfInterest>();
		}
		new PointsOfInterestListAdapter(poiList, this, pois);
		
		setStopInfo();
	}


	
	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
//		MenuInflater inflater = getMenuInflater();
//		inflater.inflate(R.menu.stop_menu, menu);
//
//		TramTrackerMainActivity.getAppManager().setMenuBackground(this);
		return true;
	}
	
	@Override
	public void openOptionsMenu() {
		slidingMenu.toggle();
	}
	@Override
	public void closeOptionsMenu() {
		slidingMenu.close();
	}
	
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_MENU){
			openOptionsMenu();
			return true;
		} else if(keyCode == KeyEvent.KEYCODE_BACK){
			onBackPressed();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		ActivityGroup group;
		Intent intent;
		switch (item.getItemId()) {
			case R.id.menu_stop_favourites :
				if(stop.getTrackerID() >= 8000){
					TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.notify_favourites_terminus));
					return true;
				}
				
				FavouriteManager favouriteManager = new FavouriteManager(getApplicationContext());
				if(favouriteManager.isStopFavourite(stop.getTrackerID())){
					TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.error_favourite_added));
					
				} else {
					group = TramTrackerMainActivity.getAppManager().requestDifferentTab(TramTrackerMainActivity.TAB_FAVOURITES);
					intent = new Intent(group, AddFavouriteActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.putExtra(AddFavouriteActivity.INTENT_KEY_STOP, stop);
					TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_FAVOURITES, getResources().getString(R.string.tag_addfavourite_screen), intent);
				}
				return true;
				
			case R.id.menu_stop_outlets :
				intent = new Intent(this, NearbyActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra(NearbyActivity.INTENT_CENTRE_KEY, stop);
				TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.getAppManager().getCurrentTabID(), getResources().getString(R.string.tag_nearby_screen), intent);
				return true;
				
			case R.id.menu_stop_directions :
				GeoPoint destination = new GeoPoint(stop.getLatitudeE6(), stop.getLongitudeE6());
				TramTrackerMainActivity.getAppManager().requestDirectionsService(destination);
				return true;
				
			default :
				return super.onOptionsItemSelected(item);
		}
	}
	
	public void runFavouriteStopMenuOption(){
		closeOptionsMenu();
		if(stop.getTrackerID() >= 8000){
			TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.notify_favourites_terminus));
			return;
		}
		
		FavouriteManager favouriteManager = new FavouriteManager(getApplicationContext());
		if(favouriteManager.isStopFavourite(stop.getTrackerID())){
			TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.error_favourite_added));
			
		} else {
			ActivityGroup group = TramTrackerMainActivity.getAppManager().requestDifferentTab(TramTrackerMainActivity.TAB_FAVOURITES);
			Intent intent = new Intent(group, AddFavouriteActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.putExtra(AddFavouriteActivity.INTENT_KEY_STOP, stop);
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_FAVOURITES, getResources().getString(R.string.tag_addfavourite_screen), intent);
		}
	}
	public void runOutletsMenuOption(){
		closeOptionsMenu();
		Intent intent = new Intent(this, NearbyActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(NearbyActivity.INTENT_CENTRE_KEY, stop);
		TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.getAppManager().getCurrentTabID(), getResources().getString(R.string.tag_nearby_screen), intent);
	}
	public void runDirectionsMenuOption(){
		closeOptionsMenu();
		GeoPoint destination = new GeoPoint(stop.getLatitudeE6(), stop.getLongitudeE6());
		TramTrackerMainActivity.getAppManager().requestDirectionsService(destination);
	}
	
	
}
