package com.yarratrams.tramtracker.ui;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.yarratrams.tramtracker.R;
import com.google.android.maps.GeoPoint;
import com.yarratrams.tramtracker.db.FavouriteManager;
import com.yarratrams.tramtracker.db.POIManager;
import com.yarratrams.tramtracker.db.PopulateDatabase;
import com.yarratrams.tramtracker.db.TTDB;
import com.yarratrams.tramtracker.db.TTDBUpdate;
import com.yarratrams.tramtracker.objects.Constants;
import com.yarratrams.tramtracker.objects.Favourite;
import com.yarratrams.tramtracker.objects.FavouritesGroup;
import com.yarratrams.tramtracker.objects.PredictedArrivalTime;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.objects.Tram;
import com.yarratrams.tramtracker.services.FavouritesService;
import com.yarratrams.tramtracker.singleton.RoutesManager;
import com.yarratrams.tramtracker.singleton.TTDBSingleton;
import com.yarratrams.tramtracker.singleton.TTDBUpdateSingleton;
import com.yarratrams.tramtracker.tasks.DeviceTokenTask;
import com.yarratrams.tramtracker.tasks.GetMyLocationTask;
import com.yarratrams.tramtracker.tasks.MyTramTask;
import com.yarratrams.tramtracker.tasks.NearbyFavouriteTask;
import com.yarratrams.tramtracker.tasks.PIDTask;
import com.yarratrams.tramtracker.tasks.ScheduledTramsTask;
import com.yarratrams.tramtracker.tasks.SearchStopsTicketOutletPOITask;
import com.yarratrams.tramtracker.tasks.ServiceChangesFromWebServiceTask;
import com.yarratrams.tramtracker.ui.util.ScreenDescriptor;
import com.yarratrams.tramtracker.webserviceinteraction.UpdateManager;

import android.app.Activity;
import android.app.ActivityGroup;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.Toast;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;


public class TramTrackerMainActivity extends TabActivity implements OnTabChangeListener{
	public static final String SHARED_PREFERENCES = "com.yarratrams.tramtracker";
	public static final String DEFAULT_ENTRY_SCREEN = "com.yarratrams.tramtracker.DefaultEntryScreen";
	public static final String FAVOURITES_NEWFAVOURITES = "com.yarratrams.tramtracker.FavouritesActivity";
	public static final int DEFAULT_ENTRY_NEARBY = 0;
	public static final int DEFAULT_ENTRY_FAVOURITES = 1;
	public static final int DEFAULT_ENTRY_NEARESTFAVOURITE = 2;
	public static final int DEFAULT_ENTRY_TRACKERID = 3;
	
	public static final int TAB_NEARBY = 0;
	public static final int TAB_FAVOURITES = 1;
	public static final int TAB_ROUTES = 2;
	public static final int TAB_MYTRAM = 3;
	public static final int TAB_MORE = 4;
	
	public static TramTrackerMainActivity instance;
	public TabHost tabHost;
	private boolean onTabChanged;
	
	private ArrayList<ScreenDescriptor> activityHistory;
	
	private Stop currentAlarmStop;
	private Intent alarmServiceIntent;

	private SharedPreferences preferences;
	private int selectedEntryScreen;
	
	private boolean updateAvailable;
	private UpdateManager updateManager;
	private UpdateActivity updateActivity;
	
	public Handler handlerToast = new Handler(){
        public void handleMessage(Message m) {
        	TramTrackerMainActivity.getAppManager().displayErrorMessage(m.getData().getString("error"));
        }
	};
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_screen);
		
//		FavouriteManager manager = new FavouriteManager(getApplicationContext());
//		System.out.println("manager.getAllFavourites() " + manager.getAllFavourites());
//		try{
//			FavouritesGroup favGroup = new FavouritesGroup();
//			favGroup.setName("test ab");
//			System.out.println("manager.addFavouriteGroup(favGroup) = " + manager.addFavouriteGroup(favGroup));
//			try{
//				FavouritesGroup newFavGroup = new FavouritesGroup(favGroup);
//				newFavGroup.setName("test ab new");
//				System.out.println("manager.editFavouriteGroup(favGroup, newFavGroup) = " + manager.editFavouriteGroup(favGroup, newFavGroup));

//				Favourite fav = new Favourite();
//				fav.setName("test ab fav 1 new");
//				fav.setOrder(1);
//				Stop stop = new Stop();
//				stop.setTrackerID(1234);
//				fav.setStop(stop);
//				System.out.println("manager.addFavourite = " + manager.addFavourite(favGroup, fav));

//				fav.setName("test ab fav 1 different");
//				System.out.println("manager.editFavourite = " + manager.editFavourite(favGroup, fav));
//				System.out.println("fav delete result = " + manager.removeFavourite(favGroup, fav));
//				System.out.println("favgroup delete result = " + manager.removeFavouriteGroup(favGroup));
//				System.out.println("favgroup delete result = " + manager.removeFavouriteGroup(newFavGroup));

//			}catch (SQLiteConstraintException e) {
//				System.out.println("Unable to edit fav group, check for duplication");
//				Toast.makeText(this, "Broblem bro!", Toast.LENGTH_LONG);
//			}
//		}catch (SQLiteConstraintException e) {
//			System.out.println("Unable to create fav group, check for duplication");
//			Toast.makeText(this, "Broblem bro!", Toast.LENGTH_LONG);
//		}

		
//		Calendar calNow = Calendar.getInstance();
//		String strDate = (String)DateFormat.format("yyyy-MM-dd'T'kk:mm:'00'z", calNow);
//		StringBuffer buffer = new StringBuffer(strDate);
//		buffer.insert(22, ':');
		
//		TTDBUpdate ttdb = TTDBUpdateSingleton.getTTDBUpdateInstance(this);
//		ttdb.createStopsAndNearbyOutlets();
		
//		Tram tram = new Tram();
//		tram.setVehicleNumber(-1);
//		MyTramTask task = new MyTramTask(this, tram);
//		task.execute();
		
//		SearchStopsTicketOutletPOITask search = new SearchStopsTicketOutletPOITask(this, "col", true, false, false, true, true);
//		search.execute();
		
//		Calendar calNow = Calendar.getInstance();
//		ScheduledTramsTask scheduledTramsTask = new ScheduledTramsTask(this, "3404", "109", calNow, false);
//		scheduledTramsTask.execute();

//		Intent contentIntent = new Intent(getApplicationContext(),AlarmBroadcastReceiver.class);
//		PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0,contentIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//		AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
//		Calendar cal = Calendar.getInstance();
//		cal.add(Calendar.SECOND, 5);
//		alarmManager.set(AlarmManager.RTC,cal.getTimeInMillis(), pendingIntent);
//		System.out.println("Alarm set up");
		
//		System.out.println("POIManager.getPOIForStop(2224, this)" + POIManager.getPOIForStop(2224, this));
//		System.out.println("POIManager.getPOIForStopAsCSV(2224, this)" + POIManager.getPOIForStopAsCSV(2224, this));
		
//		TTDB ttdb = TTDBSingleton.getInstance(this);
//		System.out.println("ttdb.getAllRoutes() = " + ttdb.getAllTicketOutlets());
		
//		RoutesManager routesManager = new RoutesManager(this);
//		System.out.println("routesManager.getStopsForRoute = " + routesManager.getStopsForRoute("109", true));
//		System.out.println("routesManager.getTicketOutletsForStops = " + routesManager.getTicketOutletsForStops(routesManager.getStopsForRoute("109", true)));
		
//		GetMyLocationTask task = new GetMyLocationTask(this);
//		task.execute(null);
		
//		NearbyFavouriteTask task = new NearbyFavouriteTask(this);
//		task.execute(null);
		
//		FavouriteManager favman = new FavouriteManager(this);
//		Log.e("FavouriteManager","favman.isStopFavourite(1010) = " + favman.isStopFavourite(1010));
//		Log.e("FavouriteManager","favman.isStopFavourite(1021) = " + favman.isStopFavourite(1021));
//		Log.e("FavouriteManager","favman.isStopFavourite(1030) = " + favman.isStopFavourite(1030));
//		try{	
//			Object nul = null;
//			if(nul == updateManager)
//				System.out.println("Test");
//			try {
//				BufferedWriter bos = new BufferedWriter(new FileWriter("/sdcard/ab.txt"));
//				bos.write("Just something");
//				bos.flush();
//				bos.close();
//			} catch (IOException e1) {
//				e1.printStackTrace();
//			}
//		} catch (Exception e) {
//			try {
//				BufferedWriter bos = new BufferedWriter(new FileWriter("/sdcard/ab.txt"));
//				bos.write(e.getStackTrace().toString());
//				bos.flush();
//				bos.close();
//			} catch (IOException e1) {
//				e1.printStackTrace();
//			}
//		}
		
 		instance = this;
 		
//		PopulateDatabase pd = new PopulateDatabase(this);
//		pd.populateAll();
//		System.out.println("Populated");
//		TTDB.copyDBToSD();
// 		
//		TTDBUpdate ttdb = TTDBUpdateSingleton.getTTDBUpdateInstance(this);
//		ttdb.createStopsAndNearbyOutlets();
 		
//		TTDB ttdb = TTDBSingleton.getInstance(this);
//		Location location = new Location("");
//		location.setLatitude(-32.010396);
//		location.setLongitude(135.119128);
// 		System.out.println("getNearbyStopsWithShelterLimitedByNoOfStopsGivenLocation = " + ttdb.getNearbyStopsWithShelterLimitedByNoOfStopsGivenLocation(location, location));
		
		UpdateManager updateManager = new UpdateManager();
		updateManager.checkForUpdate(this);
 		
// 		SharedPreferences appSharedPrefs = getSharedPreferences(Constants.kSharedPrefIdentifier, Activity.MODE_PRIVATE);
// 		Editor prefsEditor = appSharedPrefs.edit();
// 		
//		Calendar cal = Calendar.getInstance();
//		System.out.println("new GregorianCalendar(2012, 7, 21) = " + DateFormat.format("dd-MM-yyyy", (new GregorianCalendar(2012, 7, 21))));
//		if(cal.after(new GregorianCalendar(2012, 7, 21))){
//			Log.e("is after","true");
//			System.exit(0);
//		}
//		else
//			Log.e("is after","false");
//		if(appSharedPrefs.get)
//		cal.set(Calendar.DAY_OF_MONTH, 9);
//		cal.set(Calendar.MONTH, 7);
//		prefsEditor.putLong(Constants.kLastUpdateDate, cal.getTimeInMillis());
//		prefsEditor.commit();
		
		
 		
// 		SharedPreferences appSharedPrefs = getSharedPreferences(Constants.kSharedPrefIdentifier, Activity.MODE_PRIVATE);
//		Editor prefsEditor = appSharedPrefs.edit();
//		prefsEditor.putString(Constants.kKeyDeviceInfo, null);
//		prefsEditor.commit();
		
		DeviceTokenTask dtt = new DeviceTokenTask();
		dtt.execute(null);
 		
// 		ServiceChangesFromWebServiceTask task = new ServiceChangesFromWebServiceTask();
// 		task.execute(null);
 		
 		updateAvailable = false;
		activityHistory = new ArrayList<ScreenDescriptor>();
		onTabChanged = false;
		
		tabHost = getTabHost();
		tabHost.addTab(createTab(NearbyTabActivityManager.class, getResources().getString(R.string.tag_nearby_tab),
				getResources().getString(R.string.tab_nearby), R.drawable.tab_nearby));
		tabHost.addTab(createTab(FavouritesTabActivityManager.class, getResources().getString(R.string.tag_favourites_tab),
				getResources().getString(R.string.tab_favourites), R.drawable.tab_favourites));
		tabHost.addTab(createTab(RoutesTabActivityManager.class, getResources().getString(R.string.tag_routes_tab),
				getResources().getString(R.string.tab_routes), R.drawable.tab_routes));
		tabHost.addTab(createTab(MyTramTabActivityManager.class, getResources().getString(R.string.tag_mytram_tab),
				getResources().getString(R.string.tab_mytram), R.drawable.tab_mytram));
		tabHost.addTab(createTab(MoreTabActivityManager.class, getResources().getString(R.string.tag_more_tab),
				getResources().getString(R.string.tab_more), R.drawable.tab_more));
		
		tabHost.setOnTabChangedListener(this);
		
		
		preferences = getSharedPreferences(SHARED_PREFERENCES, MODE_PRIVATE);
		selectedEntryScreen = preferences.getInt(DEFAULT_ENTRY_SCREEN, 0);
		setFirstScreen(selectedEntryScreen);
		
	}
	
	
	public void savePreferences(int selectedEntryScreen){
		this.selectedEntryScreen = selectedEntryScreen;
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(DEFAULT_ENTRY_SCREEN, selectedEntryScreen);
        editor.commit();
	}

	
//	Not used any more!
//	public void notifyAddedFavourite(){ 
//        SharedPreferences.Editor editor = preferences.edit();
//        editor.putBoolean(FAVOURITES_NEWFAVOURITES, false);
//        editor.commit();
//	}
//	public boolean isNewFavouriteScreen(){
//		preferences = getSharedPreferences(SHARED_PREFERENCES, MODE_PRIVATE);
//		boolean isNewFavourite = preferences.getBoolean(FAVOURITES_NEWFAVOURITES, true);
//		return isNewFavourite;
//	}

	private TabSpec createTab(final Class<?> intentClass, final String tag, final String title, final int drawable) {
		final Intent intent = new Intent().setClass(this, intentClass);
		final View tab = LayoutInflater.from(getTabHost().getContext()).inflate(R.layout.tab, null);
		((TextView) tab.findViewById(R.id.tab_text)).setText(title);
		((ImageView) tab.findViewById(R.id.tab_icon)).setImageResource(drawable);

		return getTabHost().newTabSpec(tag).setIndicator(tab).setContent(intent);
	}
	
	static public TramTrackerMainActivity getAppManager() {
		return instance;
	}

	public void showTab(int activityID){
		tabHost.setCurrentTab(activityID);
	}
	
	public void back() {
		if (activityHistory.size() > 1) {
			activityHistory.remove(activityHistory.size() - 1);
			
			ScreenDescriptor screen = activityHistory.get(activityHistory.size() - 1);
			if(tabHost.getCurrentTab() != screen.getTabID()){
				onTabChanged = true;
				tabHost.setCurrentTab(screen.getTabID());
			}
			
			Intent intent = screen.getActivity().getIntent().setFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY);
			Window window = screen.getGroup().getLocalActivityManager().startActivity(screen.getScreenID(), intent);
			screen.getGroup().setContentView(window.getDecorView());
		} else {
			finish();
		}
	}
	
	
	public ActivityGroup requestDifferentTab(int tabID){
		onTabChanged = true;
		showTab(tabID);
		ActivityGroup group = getTabGroup(tabID);
		if(group == null){
			group = getTabGroup(tabID);
		}
		return group;
	}
	
	public int getCurrentTabID(){
		return tabHost.getCurrentTab();
	}
	
	public void show(int tabID, String screenID, Intent intent){
		ActivityGroup group = getTabGroup(tabID);
		if(tabHost.getCurrentTab() != tabID){
			onTabChanged = true;
			tabHost.setCurrentTab(tabID);
		}

		View view = group.getLocalActivityManager().startActivity(screenID, intent).getDecorView();
		addToHistory(tabID, screenID, group);
		group.setContentView(view);
		view.requestFocus();
	}
	
	public void addToHistory(int tabID, String screenID, ActivityGroup group){
		ScreenDescriptor screen = new ScreenDescriptor();
		screen.setTabID(tabID);
		screen.setScreenID(screenID);
		screen.setGroup(group);
		screen.setActivity(group.getCurrentActivity());
		
		activityHistory.add(screen);
	}
	
	public void clearHistory(){
		activityHistory.clear();
	}
	
	
	public ActivityGroup getTabGroup(int tabID){
		ActivityGroup group = null;
		switch (tabID) {
			case TAB_NEARBY :
				group = NearbyTabActivityManager.getGroup();
				break;
			case TAB_FAVOURITES :
				group = FavouritesTabActivityManager.getGroup();
				break;
			case TAB_ROUTES :
				group = RoutesTabActivityManager.getGroup();
				break;
			case TAB_MYTRAM :
				group = MyTramTabActivityManager.getGroup();
				break;
			case TAB_MORE :
				group = MoreTabActivityManager.getGroup();
				break;
		}
		return group;
	}
	
	
	@Override
	public void onTabChanged(String tabId) {
		if(onTabChanged){
			onTabChanged = false;
			return;
		}
		clearHistory();
		setTabFirstScreen(tabId);
	}
	
	private void setFirstScreen(int entryScreen){
		Intent intent;
		if(entryScreen == DEFAULT_ENTRY_NEARBY){
			tabHost.setCurrentTab(TAB_NEARBY);
			//intent = new Intent(getTabGroup(TAB_NEARBY), NearbyActivity.class);
			//show(TAB_NEARBY, getResources().getString(R.string.tag_nearby_screen), intent);
		} else if(entryScreen == DEFAULT_ENTRY_FAVOURITES){
			tabHost.setCurrentTab(TAB_FAVOURITES);
			intent = new Intent(getTabGroup(TAB_FAVOURITES), FavouritesActivity.class);
			show(TAB_FAVOURITES, getResources().getString(R.string.tag_favourites_screen), intent);			
		} else if(entryScreen == DEFAULT_ENTRY_NEARESTFAVOURITE){
			tabHost.setCurrentTab(TAB_FAVOURITES);
			intent = new Intent(getTabGroup(TAB_FAVOURITES), PIDActivity.class);
			show(TAB_FAVOURITES, getResources().getString(R.string.tag_pid_screen), intent);
		} else if(entryScreen == DEFAULT_ENTRY_TRACKERID){
			tabHost.setCurrentTab(TAB_MORE);
			intent = new Intent(getTabGroup(TAB_MORE), SearchTrackerIDActivity.class);
			show(TAB_MORE, getResources().getString(R.string.tag_trackerid_screen), intent);
		}
	}
	
	private void setTabFirstScreen(String tabID){
		if(tabID.equalsIgnoreCase(getResources().getString(R.string.tag_nearby_tab))){
			Intent intent = new Intent(getTabGroup(TAB_NEARBY), NearbyActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
			show(TAB_NEARBY, getResources().getString(R.string.tag_nearby_screen), intent);
			
		} else if(tabID.equalsIgnoreCase(getResources().getString(R.string.tag_favourites_tab))){
			Intent intent = new Intent(getTabGroup(TAB_FAVOURITES), FavouritesActivity.class);
			show(TAB_FAVOURITES, getResources().getString(R.string.tag_favourites_screen), intent);
			
		} else if(tabID.equalsIgnoreCase(getResources().getString(R.string.tag_routes_tab))){
			Intent intent = new Intent(getTabGroup(TAB_ROUTES), RoutesEntryActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
			show(TAB_ROUTES, getResources().getString(R.string.tag_routes_entry_screen), intent);
			
		} else if(tabID.equalsIgnoreCase(getResources().getString(R.string.tag_mytram_tab))){
			Intent intent = new Intent(getTabGroup(TAB_MYTRAM), MyTramActivity.class);
			show(TAB_MYTRAM, getResources().getString(R.string.tag_mytram_screen), intent);
			
		} else if(tabID.equalsIgnoreCase(getResources().getString(R.string.tag_more_tab))){
			Intent intent = new Intent(getTabGroup(TAB_MORE), MoreActivity.class);
			show(TAB_MORE, getResources().getString(R.string.tag_more_screen), intent);
		}
	}
	
	
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_MENU){
			getLocalActivityManager().getCurrentActivity().openOptionsMenu();
			return true;
		} else if(keyCode == KeyEvent.KEYCODE_BACK){
			this.back();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	
	public void setMenuBackground(final Activity activity){
		activity.getLayoutInflater().setFactory(new LayoutInflater.Factory() {
			@Override
			public View onCreateView(String name, Context context, AttributeSet attributeSet) {
				if (name.equalsIgnoreCase("com.android.internal.view.menu.IconMenuItemView")) {
					try{
						final LayoutInflater f = activity.getLayoutInflater();
						final View view = f.createView(name, null, attributeSet);
						new Handler().post(new Runnable() {
							public void run() {
								view.setBackgroundResource(R.drawable.darkgrey_cell_background);
								//((TextView) view).setTextColor(Color.WHITE);
								setTextColor(view);
							}
						});
						return view;
					} catch(Exception e){
						//System.out.println("OPTIONS BACKGROUND ERROR");
					}
				}
				return null;
			}
		});
	}
	
	private void setTextColor(final View view) {
		try {
			final Method setTextColor = view.getClass().getMethod("setTextColor", int.class);
			setTextColor.invoke(view, "0xFFFFFFFF");
		} catch (Exception e) {
//			System.out.println("OPTIONS TEXT ERROR");
			try{
				((TextView) view).setTextColor(Color.WHITE);
			} catch(Exception ex){}
		}
	}
	
	
	public void requestDirectionsService(GeoPoint destination){
		String daddr = String.valueOf(destination.getLatitudeE6()/1E6) + "," + String.valueOf(destination.getLongitudeE6()/1E6);
		String url = getResources().getString(R.string.directions_url_base);
		url = url.concat(daddr);
		
		Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(url));
		startActivity(intent);
	}
	
	
	public void displayErrorMessage(String message){
		Toast.makeText(this, message, Toast.LENGTH_LONG).show();
//		AlertDialog.Builder builder = new AlertDialog.Builder(this);
//		builder.setMessage(message).setCancelable(true);
//		AlertDialog alert = builder.create();
//		try{
//			alert.show();
//		} catch(Exception e){
//		}
	}
	
	public void startAlarmService(Stop stop, ArrayList<PredictedArrivalTime> stops, Tram tram){
//		alarmServiceIntent.putExtra(AlarmServiceInterface.kAlarmStopKey, stop);
//		alarmServiceIntent.putExtra(AlarmServiceInterface.kAlarmPredictedArrayList, stops);		
//		alarmServiceIntent.putExtra(AlarmServiceInterface.kAlarmTramKey, tram);
//		registerReceiver(broadcastReceiver, new IntentFilter(AlarmServiceInterface.kAlarmServiceName));
		currentAlarmStop = stop;
//		startService(alarmServiceIntent);
	}
	
	public void stopAlarmService(){
		stopService(alarmServiceIntent);
		currentAlarmStop = null;
	}
	
	public boolean hasAlarm(Stop stop){
		if(currentAlarmStop != null){
			if(stop.getTrackerID() == currentAlarmStop.getTrackerID()){
				return true;
			}
		}
		return false;
	}
	
	public void notifyUpdateAvailable(UpdateManager updateManager){
		updateAvailable = true; 
		this.updateManager = updateManager; 
		displayErrorMessage(getResources().getString(R.string.notify_update_available));
	}
	
	public boolean isUpdateAvailable(){
		return updateAvailable;
	}
	
	public void update(UpdateActivity activity){
		this.updateActivity = activity;
		updateManager.update();
	}
	
	public void notifyFinishUpdate(boolean successfull){
		updateActivity.notifyFinishUpdate(successfull);
		if(successfull){
			updateAvailable = false;
		}
	}
	
	public void callSelection(String selection){
		AccessibilityManager accessibilityManager = (AccessibilityManager) getSystemService(Context.ACCESSIBILITY_SERVICE);
	    if (accessibilityManager.isEnabled()) {
	    	AccessibilityEvent event = AccessibilityEvent.obtain(AccessibilityEvent.TYPE_VIEW_FOCUSED);
	    	event.setContentDescription(selection);
	    	accessibilityManager.sendAccessibilityEvent(event);
	    }
	}
	
	public Calendar getLastUpdateDate(){
		preferences = getSharedPreferences(Constants.kSharedPrefIdentifier, Activity.MODE_PRIVATE);
		Calendar calendar = new GregorianCalendar(Constants.kLastUpdateYear, Constants.kLastUpdateMonth, Constants.kLastUpdateDayOfMonth);
		calendar.setTime(new Date(preferences.getLong(Constants.kLastUpdateDate, calendar.getTimeInMillis())));
		return calendar;
	}
	
}
