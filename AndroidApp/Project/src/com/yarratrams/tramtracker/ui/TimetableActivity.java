package com.yarratrams.tramtracker.ui;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.Route;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.objects.Tram;
import com.yarratrams.tramtracker.tasks.ScheduledTramsTask;
import com.yarratrams.tramtracker.ui.util.TimetableListArrayAdapter;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;


public class TimetableActivity extends Activity implements OnClickListener{
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_MORE;
	public static final String INTENT_ROUTE_KEY = "route_info";
	public static final String INTENT_STOP_KEY = "stop_info";
	private static final int DIALOG_DATE_ID = 0;
	private static final int DIALOG_TIME_ID = 1;
	public static final int ACTION_NEW = 0;
	public static final int ACTION_MORE = 1;
	
	private Route route;
	private Stop stop;
	
	private int action; 
	private Calendar calendar;
	private Calendar lastService;
	private ScheduledTramsTask scheduledTramsTask;
	
	int startYear;
	int startMonth;
	int startDay;
	
	private ArrayList<Tram> services;
	
	private ListView list;
	private TimetableListArrayAdapter timetableAdapter;
	private View footerView;
	
	private ProgressDialog loadDialog;
	
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.timetable_screen);
		
		calendar = Calendar.getInstance();
		lastService = Calendar.getInstance();
		
		list = (ListView) findViewById(R.id.simple_list);
		Button dateButton = (Button) findViewById(R.id.schedule_datepicker_button);
		Button timeButton = (Button) findViewById(R.id.schedule_timepicker_button);
		dateButton.setOnClickListener(this);
		timeButton.setOnClickListener(this);
		
		services = new ArrayList<Tram>();

		getMoreView();
		list.addFooterView(footerView);
		
		loadDialog = new ProgressDialog(this);
	}
	
	private Context getDialogContext() {
		Context context;
		if (getParent() != null){
			context = getParent();
		} else {
			context = this;
		}
		return context;
	}
	
	private void getMoreView(){
		LayoutInflater inflater = getLayoutInflater();
		final View rowView = inflater.inflate(R.layout.timetable_list_view_more, null, true);
		
		rowView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_timetable_more));
				getNext3Services(lastService, true);
			}
		});
		
		footerView = rowView; 
	}
	
	private void saveLastServiceTime(){
		if(services.size() > 0){
			Tram service = services.get(services.size()-1);
			lastService.setTime(service.getArrivalTime());
			lastService.add(Calendar.MINUTE, 1);
		}
	}
	
	
	@Override
	protected void onResume() {
		super.onResume();
		
		calendar = Calendar.getInstance();
		
		Bundle extras = getIntent().getExtras(); 
		if(extras != null){
			stop = (Stop)extras.getParcelable(INTENT_STOP_KEY);
			route = (Route)extras.getParcelable(INTENT_ROUTE_KEY);
		}
		
		timetableAdapter = new TimetableListArrayAdapter(this, stop, route, services);
		list.setAdapter(timetableAdapter);
		
		updateHeader();
		
		getNext3Services(calendar, false);
	}


	public void updateHeader(){
		TextView stopName = (TextView) findViewById(R.id.stop_name);
		TextView stopID = (TextView) findViewById(R.id.stop_trackerid_number);
		TextView routeDescription = (TextView) findViewById(R.id.schedule_route_description);
		stopName.setText(getStopDescription());
		stopID.setText(String.valueOf(stop.getTrackerID()));
		routeDescription.setText(getRouteDescription());
	}
	
	public void updateUI(){
		ArrayList<Tram> newServices = scheduledTramsTask.getAllTrams();
		if(newServices != null){
			int index = services.size();
			if(action == ACTION_NEW){
				index = 0;
				services = newServices;
				if(list.getFooterViewsCount() == 0){
					list.addFooterView(footerView);
				}
			} else {
				if(index >= 7){
					for(int i = 0; services.size() < 10; i++){
						services.add(newServices.get(i));
					}
					list.removeFooterView(footerView);
				} else {
					services.addAll(newServices);
				}
			}
			timetableAdapter.updateList(services);
			saveLastServiceTime();
			try{
				list.setSelection(index);
			} catch (Exception e){}
		}
		
		if(loadDialog.isShowing()){
			loadDialog.dismiss();
		}
	}
	
	private void updateTime(){
		TextView timeField = (TextView) findViewById(R.id.schedule_timepicker_button);
		DateFormat timeFormat = new SimpleDateFormat(getResources().getString(R.string.timetable_time_format));
		Date date = new Date(calendar.getTimeInMillis()); 
		String timeString = timeFormat.format(date); 
		timeField.setText(timeString);
		getNext3Services(calendar, false);
	}
	
	private void updateDate(){
		TextView dateField = (TextView) findViewById(R.id.schedule_datepicker_button);
		DateFormat timeFormat = new SimpleDateFormat(getResources().getString(R.string.timetable_date_format));
		Date date = new Date(calendar.getTimeInMillis()); 
		String dateString = timeFormat.format(date); 
		dateField.setText(dateString);
		getNext3Services(calendar, false);
	}
	
	private String getStopDescription(){
		String text = "";
		
		text = text.concat(String.valueOf(stop.getStopNumber()));
		text = text.concat(getResources().getString(R.string.stop_name_colon));
		text = text.concat(stop.getStopName());
		
		return text;
	}
	
	private String getRouteDescription(){
		String text = "";
		
		text = getResources().getString(R.string.timetable_route);
		text = text.concat(route.getRouteNumber());
		text = text.concat(getResources().getString(R.string.timetable_dash));
		if(route.isUpDestination()){
			text = text.concat(route.getUpDestination());
		} else {
			text = text.concat(route.getDownDestination());
		}
		
		return text;
	}
	
	@Override
	public void onClick(View v) {
		Button dateButton = (Button) findViewById(R.id.schedule_datepicker_button);
		Button timeButton = (Button) findViewById(R.id.schedule_timepicker_button);
		if(v == dateButton){
			showDialog(DIALOG_DATE_ID);
		} else if(v == timeButton){
			showDialog(DIALOG_TIME_ID);
		}
		
	}
	
	public void getNext3Services(Calendar date, boolean more){
		if(!loadDialog.isShowing()){
			loadDialog = ProgressDialog.show(getDialogContext(), "", getResources().getString(R.string.dialog_loading), true, true);
		}
		
		if(more){
			action = ACTION_MORE;
		} else {
			action = ACTION_NEW;
		}
		scheduledTramsTask = new ScheduledTramsTask(this, String.valueOf(stop.getTrackerID()), route.getRouteNumber(), date, false);
		scheduledTramsTask.execute();
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
            case DIALOG_DATE_ID:
            	startYear = calendar.get(Calendar.YEAR);
            	startMonth = calendar.get(Calendar.MONTH);
            	startDay = calendar.get(Calendar.DAY_OF_MONTH);
            	DatePickerDialog datePicker = new DatePickerDialog(getDialogContext(),datePickerListener, startYear, startMonth, startDay){
//            		boolean dateFixed = false;
            		Calendar currentDate = Calendar.getInstance();
            		Calendar previousDate = Calendar.getInstance();
            		DateFormat dateFormat;
            		
					@Override
					public void onDateChanged(DatePicker view, int year, int month, int day) {
						dateFormat = DateFormat.getDateInstance(DateFormat.FULL);
						Calendar setDate = Calendar.getInstance();
						setDate.set(Calendar.DAY_OF_MONTH, day);
						setDate.set(Calendar.MONTH, month);
						setDate.set(Calendar.YEAR, year);
						
						Calendar maxDate = Calendar.getInstance();
						maxDate.add(Calendar.DAY_OF_MONTH, 7);
						
// This code does not work with devices with 4.x and after.
// Code changed for version 1.03
//						if (!dateFixed) {
//							if(day == 1){
//								if(previousDate.get(Calendar.DAY_OF_MONTH) == previousDate.getActualMaximum(Calendar.DAY_OF_MONTH)){
//									setDate.add(Calendar.MONTH, 1);
//									dateFixed = true;
//								}
//							} else if(day == setDate.getActualMaximum(Calendar.DAY_OF_MONTH)){
//								if(previousDate.get(Calendar.DAY_OF_MONTH) == 1){
//									setDate.add(Calendar.MONTH, -1);
//									dateFixed = true;
//								}
//							}
//							
//							if(setDate.before(currentDate) || setDate.after(maxDate)){
//								setDate.set(Calendar.DAY_OF_MONTH, previousDate.get(Calendar.DAY_OF_MONTH));
//								setDate.set(Calendar.MONTH, previousDate.get(Calendar.MONTH));
//								setDate.set(Calendar.YEAR, previousDate.get(Calendar.YEAR));
//								dateFixed = true;
//							}
//							
//							if(dateFixed){
//								this.updateDate(setDate.get(Calendar.YEAR), setDate.get(Calendar.MONTH), setDate.get(Calendar.DAY_OF_MONTH));
//							}
//						}else {
//							dateFixed = false;
//						}
//						
//						previousDate.set(Calendar.YEAR, setDate.get(Calendar.YEAR));
//						previousDate.set(Calendar.MONTH, setDate.get(Calendar.MONTH));
//						previousDate.set(Calendar.DAY_OF_MONTH, setDate.get(Calendar.DAY_OF_MONTH));
//						super.onDateChanged(view, setDate.get(Calendar.YEAR), setDate.get(Calendar.MONTH), setDate.get(Calendar.DAY_OF_MONTH));
					
// Code added to support devices with OS v4.x and after
						if(day == 1){
							if(previousDate.get(Calendar.DAY_OF_MONTH) == previousDate.getActualMaximum(Calendar.DAY_OF_MONTH)){
								setDate.add(Calendar.MONTH, 1);
							}
						} else if(day == setDate.getActualMaximum(Calendar.DAY_OF_MONTH)){
							if(previousDate.get(Calendar.DAY_OF_MONTH) == 1){
								setDate.add(Calendar.MONTH, -1);
							}
						}
						
						if(setDate.before(currentDate) || setDate.after(maxDate)){
							setDate.set(Calendar.YEAR, previousDate.get(Calendar.YEAR));
							setDate.set(Calendar.MONTH, previousDate.get(Calendar.MONTH));
							setDate.set(Calendar.DAY_OF_MONTH, previousDate.get(Calendar.DAY_OF_MONTH));
						}
						
						view.init(setDate.get(Calendar.YEAR), setDate.get(Calendar.MONTH), setDate.get(Calendar.DAY_OF_MONTH), this);
						setTitle(dateFormat.format(setDate.getTime()));
						
						previousDate.set(Calendar.YEAR, setDate.get(Calendar.YEAR));
						previousDate.set(Calendar.MONTH, setDate.get(Calendar.MONTH));
						previousDate.set(Calendar.DAY_OF_MONTH, setDate.get(Calendar.DAY_OF_MONTH));
// End of code
					}
            	};
                return datePicker;
                
			case DIALOG_TIME_ID:
            	int hour = calendar.get(Calendar.HOUR_OF_DAY);
            	int minute = calendar.get(Calendar.MINUTE);
            	return new TimePickerDialog(getDialogContext(),timePickerListener, hour, minute, false);
        }
		return super.onCreateDialog(id);
	}

	
	
    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
			calendar.set(Calendar.MINUTE, minute);
			updateTime();
		}
    };

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
			calendar.set(Calendar.YEAR, year);
			calendar.set(Calendar.MONTH, monthOfYear);
			calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			updateDate();
		}
    };
	
	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

}
