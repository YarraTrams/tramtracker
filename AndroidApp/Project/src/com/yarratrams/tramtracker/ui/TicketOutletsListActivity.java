package com.yarratrams.tramtracker.ui;

import java.util.ArrayList;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.db.TTDB;
import com.yarratrams.tramtracker.objects.TicketOutletBySuburb;
import com.yarratrams.tramtracker.singleton.TTDBSingleton;
import com.yarratrams.tramtracker.ui.util.TicketOutletsExpandableListArrayAdapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.widget.ExpandableListView;


public class TicketOutletsListActivity extends Activity {
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_MORE;
	
	private ExpandableListView outletsList;
	private TicketOutletsExpandableListArrayAdapter outletsAdapter;
	private ArrayList<TicketOutletBySuburb> suburbs;

	private ProgressDialog loadDialog;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.outlets_list_screen);

		
		suburbs = new ArrayList<TicketOutletBySuburb>();
		outletsList = (ExpandableListView) findViewById(R.id.expandable_list);
		
		Display display = getWindowManager().getDefaultDisplay();
		Drawable groupIndicator = getResources().getDrawable(R.drawable.icn_list_expandable);
		outletsList.setGroupIndicator(groupIndicator);
		outletsList.setChildIndicator(null);
		outletsList.setIndicatorBounds(display.getWidth()-GetDipsFromPixel(30), display.getWidth());
		
		outletsAdapter = new TicketOutletsExpandableListArrayAdapter(this, suburbs);
		outletsList.setAdapter(outletsAdapter);
		
        loadDialog = new ProgressDialog(this);
        
        retrieveOutlets();
	}
	
	public int GetDipsFromPixel(float pixels) {
		final float scale = getResources().getDisplayMetrics().density;
		return (int) (pixels * scale + 0.5f);
	}
	
	public Context getDialogContext() {
		Context context;
		if (getParent() != null) {
			context = getParent();
		} else {
			context = this;
		}
		return context;
	}

	
	@Override
	public void onResume() {
		super.onResume();
		
	}
	@Override
	public void onPause() {
		super.onPause();
	}
	
	public void retrieveOutlets(){
		if(!loadDialog.isShowing()){
			loadDialog = ProgressDialog.show(getDialogContext(), "", getResources().getString(R.string.dialog_loading), true, true);
		}
		
		TTDB ttdb = TTDBSingleton.getInstance(this);
		suburbs = ttdb.getAllTicketOutlets();
		
		outletsAdapter = new TicketOutletsExpandableListArrayAdapter(this, suburbs);
		outletsList.setAdapter(outletsAdapter);
		//expandAll();
		
		if(loadDialog.isShowing()){
			loadDialog.dismiss();
		}
	}
	
	
	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}
	
}
