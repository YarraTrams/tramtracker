package com.yarratrams.tramtracker.ui;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.webserviceinteraction.Test;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;

public class TTTestActivity extends Activity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.help_screen);
		Context context = this.getBaseContext();
		Test.testWebServiceGetNextPredictedRoutesCollection(context);
//		Test.testDBGetRoutes(context);
	}
	
	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}
	
}
