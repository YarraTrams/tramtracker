package com.yarratrams.tramtracker.ui.util;

import java.util.ArrayList;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.ServiceChangeRSSItem;
import com.yarratrams.tramtracker.ui.ServiceActivity;
import com.yarratrams.tramtracker.ui.ServiceFeedActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ServiceFeedListArrayAdapter extends ArrayAdapter<ServiceChangeRSSItem> {
	private final ServiceFeedActivity context;
	private final ArrayList<ServiceChangeRSSItem> feed;

	public ServiceFeedListArrayAdapter(ServiceFeedActivity context, ArrayList<ServiceChangeRSSItem> feed) {
		super(context, R.layout.feed_list_view_detail, feed);
		this.context = context;
		this.feed = feed;
	}

	public void updateList(ArrayList<ServiceChangeRSSItem> feed) {
		this.feed.clear();
		this.feed.addAll(feed);
		super.notifyDataSetChanged();
	}
	
	static class FeedViewHolder{
		TextView title;
		TextView description;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		FeedViewHolder viewHolder;
		
		if(convertView == null){
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(R.layout.feed_list_view_detail, parent, false);
			
			viewHolder = new FeedViewHolder();
			viewHolder.title = (TextView)convertView.findViewById(R.id.change_title);
			viewHolder.description = (TextView)convertView.findViewById(R.id.change_date);
			convertView.setTag(viewHolder);
		
		} else {
			viewHolder = (FeedViewHolder) convertView.getTag();
		}
		
		final ServiceChangeRSSItem serviceChange = feed.get(position);
		
		viewHolder.title.setText(serviceChange.getTitle());
		viewHolder.description.setText(serviceChange.getDate());
		
		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				TramTrackerMainActivity.getAppManager().callSelection(serviceChange.getTitle());
				goToServiceChangeScreen(serviceChange);
			}
		});
		
		convertView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				v.setPressed(true);
				
				return false;
			}
		});
		
		return convertView;
	}
	
	private void goToServiceChangeScreen(ServiceChangeRSSItem serviceChange){
		Intent intent = new Intent(this.context, ServiceActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(ServiceActivity.INTENT_KEY_SERVICECHANGE, serviceChange);
		
		TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, 
				context.getResources().getString(R.string.tag_service_screen), intent);
	}

}
