package com.yarratrams.tramtracker.ui.util;

import com.yarratrams.tramtracker.R;
import com.google.android.maps.MapView;

import android.app.Activity;
import android.graphics.drawable.Drawable;


public class MapEasyStopOverlay extends MapStopOverlay {

	private static Drawable getDefaultMarker(Activity context){
		return context.getResources().getDrawable(R.drawable.icn_search_easyaccess);
	}
	
	public MapEasyStopOverlay(Activity context, MapView mapView) {
		super(getDefaultMarker(context), context, mapView);
	}
	
}
