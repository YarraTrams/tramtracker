package com.yarratrams.tramtracker.ui.util;

import java.util.ArrayList;

import com.yarratrams.tramtracker.R;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;
import com.yarratrams.tramtracker.objects.TicketOutlet;
import com.yarratrams.tramtracker.objects.TicketOutletBySuburb;

import android.app.Activity;
import android.graphics.drawable.Drawable;


public class MapRouteOutletOverlay extends ItemizedOverlay<OverlayItem> {
	private ArrayList<OverlayItem> overlays = new ArrayList<OverlayItem>();
	private final Activity context;
	final MapController mc;
	
	private ArrayList<TicketOutlet> outlets;
	private MapRouteOutletDialog dialog;
	

	private static Drawable getDefaultMarker(Activity context){
		return context.getResources().getDrawable(R.drawable.icn_search_tickets);
	}
	
	public MapRouteOutletOverlay(Activity context, MapView mapView) {
		super(boundCenterBottom(getDefaultMarker(context)));
		this.context = context;
		mc = mapView.getController();
		
		outlets = new ArrayList<TicketOutlet>();
	}
	
	
	public void updateTicketOutletsList(ArrayList<TicketOutletBySuburb> suburbs) {
		this.outlets.clear();
		for(TicketOutletBySuburb suburb: suburbs){
			this.outlets.addAll(suburb.getTicketOutlets());
		}
		
		resetOverlays();
		if(dialog != null){
			dialog.dismiss();
		}
		for(TicketOutlet outlet : outlets){
	        GeoPoint point = new GeoPoint(outlet.getLatitudeE6(), outlet.getLongitudeE6());
	        OverlayItem overlayitem = new OverlayItem(point, null, null);
	        addOverlay(overlayitem);
		}
	}
	
	
	public void addOverlay(OverlayItem overlay) {
		overlays.add(overlay);
		populate();
	}
	
	public void resetOverlays(){
		overlays.clear();
		populate();
	}

	@Override
	protected OverlayItem createItem(int i) {
		return overlays.get(i);
	}

	@Override
	public int size() {
		return overlays.size();
	}
	
	@Override
	protected boolean onTap(int index) {
		mc.animateTo(createItem(index).getPoint());
		TicketOutlet outlet = outlets.get(index);
		dialog = new MapRouteOutletDialog(context, outlet);
		
		dialog.show();
		return true;
	}
	
	
}
