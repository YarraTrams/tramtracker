package com.yarratrams.tramtracker.ui.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.OnBoardStopsBySuburb;
import com.yarratrams.tramtracker.objects.PredictedArrivalTime;
import com.yarratrams.tramtracker.objects.PredictedTimeResult;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.ui.OnBoardActivity;
import com.yarratrams.tramtracker.ui.OnBoardStopActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class OnBoardExpandableListArrayAdapter extends BaseExpandableListAdapter {
	private static final int TYPE_START = 0;
	private static final int TYPE_MIDDLE = 1;
	private static final int TYPE_FINISH = 2;
	
	private OnBoardActivity context;
	private PredictedTimeResult results;
	private ArrayList<PredictedArrivalTime> stops;
	private ArrayList<OnBoardStopsBySuburb> suburbs;
	private int selectedIndex;
	private boolean isNowIndex;
	private boolean isAccurateIndex;
	
	
	public OnBoardExpandableListArrayAdapter(OnBoardActivity context, PredictedTimeResult results) {
		super();
		this.context = context;
		this.results = results;
		this.isAccurateIndex = false;
		this.stops = results.getAlPredictedArrivalTime();
		this.suburbs = new ArrayList<OnBoardStopsBySuburb>();
		generateSuburbsList();
		this.selectedIndex = results.getStartingIndex();
	}


	public void updateList(PredictedTimeResult results, int selectedIndex) {
		this.stops.clear();
		this.results = results;
		this.stops.addAll(results.getAlPredictedArrivalTime());
		this.suburbs.clear();
		generateSuburbsList();
		this.selectedIndex = selectedIndex;
		super.notifyDataSetChanged();
	}
	
	
	public void generateSuburbsList(){
		OnBoardStopsBySuburb suburb;
		
		for(PredictedArrivalTime stop : stops){
			if(suburbs.size() >= 1){
				suburb = suburbs.get(suburbs.size()-1);
				if(!suburb.getSuburb().equalsIgnoreCase(stop.getStop().getSuburb())){
					suburb = new OnBoardStopsBySuburb();
					suburbs.add(suburb);
				}
			} else{
				suburb = new OnBoardStopsBySuburb();
				suburbs.add(suburb);
			}
			suburb.setSuburb(stop.getStop().getSuburb());
			suburb.addStop(stop);
		}
	}
	
	public void setStartingIndex(PredictedTimeResult results, int index, boolean isAccurate){
//		this.stops.clear();
//		this.results = results;
//		this.stops.addAll(results.getAlPredictedArrivalTime());
//		this.suburbs.clear();
//		generateSuburbsList();
		this.selectedIndex = index;
		this.isAccurateIndex = isAccurate;
		this.isNowIndex = false;
		super.notifyDataSetChanged();
	}
	
	public void setStartingIndex(int index, boolean isAccurate){
		this.selectedIndex = index;
		this.isAccurateIndex = isAccurate;
		this.isNowIndex = false;
		super.notifyDataSetChanged();
	}
	
	public void setNowIndex(){
		isNowIndex = true;
		super.notifyDataSetChanged();
	}
	
	public boolean isCurrentIndex(Stop selected){
		if(selectedIndex >= stops.size()){
			return false;
		}
		
		PredictedArrivalTime stop = stops.get(selectedIndex);
		if(stop.getStop().getTrackerID() == selected.getTrackerID()){
			return true;
		}
		
		if(!isAccurateIndex){
			if(selectedIndex < stops.size()-1){
				stop = stops.get(selectedIndex + 1);
				if(stop.getStop().getTrackerID() == selected.getTrackerID()){
					return true;
				}
			}
		}
		
		return false;
	}
	
	public ArrayList<PredictedArrivalTime> getStopsInSuburb(int index){
		OnBoardStopsBySuburb suburb = suburbs.get(index);
		return suburb.getStops(); 
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return getStop(groupPosition, childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return (groupPosition * 100) + childPosition;
	}

	static class RouteStopViewHolder{
		TextView stopName;
		TextView poi;
		TextView time;
		ImageView stopOverlay;
		ImageView routeCBD;
		ImageView routeZone2;
		ImageView connectingTrain;
		ImageView connectingTrams;
		ImageView connectingBuses;
		ImageView turnIcon;
		ImageView routePlatform;
		ImageView alarmIcon;
		ImageView routeColor;
	}
	
	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
		RouteStopViewHolder viewHolder;
		
		if(convertView == null){
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(R.layout.onboard_stops_list_view_child, parent, false);
			
			viewHolder = new RouteStopViewHolder();
			viewHolder.stopName = (TextView)convertView.findViewById(R.id.stop_name);
			viewHolder.poi = (TextView)convertView.findViewById(R.id.points_interest);
			viewHolder.time = (TextView)convertView.findViewById(R.id.predicted_time);
			viewHolder.stopOverlay = (ImageView)convertView.findViewById(R.id.stop_overlay);
			viewHolder.routeCBD = (ImageView)convertView.findViewById(R.id.route_cbd);
			viewHolder.routeZone2 = (ImageView)convertView.findViewById(R.id.route_zone2);
			viewHolder.connectingTrain = (ImageView)convertView.findViewById(R.id.connecting_train);
			viewHolder.connectingTrams = (ImageView)convertView.findViewById(R.id.connecting_tram);
			viewHolder.connectingBuses = (ImageView)convertView.findViewById(R.id.connecting_bus);
			viewHolder.turnIcon = (ImageView)convertView.findViewById(R.id.turn);
			viewHolder.routePlatform = (ImageView)convertView.findViewById(R.id.route_platform);
			viewHolder.alarmIcon = (ImageView)convertView.findViewById(R.id.alarm_icon);
			viewHolder.routeColor = (ImageView)convertView.findViewById(R.id.route_colour);
			convertView.setTag(viewHolder);
			
		} else {
			viewHolder = (RouteStopViewHolder) convertView.getTag();
		}
		
		final PredictedArrivalTime stop = (PredictedArrivalTime) getChild(groupPosition, childPosition);
		
		int type = TYPE_MIDDLE;
		if(isLastChild && (groupPosition == suburbs.size()-1)){
			type = TYPE_FINISH;
		} else if(groupPosition == 0 && childPosition == 0){
			type = TYPE_START;
		}
		setRouteColour(viewHolder.routeColor, results.getRoute().getColour(), type);
		
		viewHolder.stopName.setText(getStopDescription(stop.getStop()));
		if(stop.getStop().getPointsOfInterest() != null){
			viewHolder.poi.setText(stop.getStop().getPointsOfInterest());
			viewHolder.poi.setVisibility(View.VISIBLE);
		} else {
			viewHolder.poi.setVisibility(View.GONE);
		}
		
		viewHolder.time.setText(getArrivalTime(stop));
		if(stop.getStop().getConnectingTrains() != null){
			if(!stop.getStop().getConnectingTrains().trim().equalsIgnoreCase("")){
				viewHolder.connectingTrain.setVisibility(View.VISIBLE);
			} else {
				viewHolder.connectingTrain.setVisibility(View.INVISIBLE);
			}
		} else {
			viewHolder.connectingTrain.setVisibility(View.INVISIBLE);
		}
		if(stop.getStop().getConnectingTrams() != null){
			if(!stop.getStop().getConnectingTrams().trim().equalsIgnoreCase("")){
				viewHolder.connectingTrams.setVisibility(View.VISIBLE);
			} else {
				viewHolder.connectingTrams.setVisibility(View.INVISIBLE);
			}
		} else {
			viewHolder.connectingTrams.setVisibility(View.INVISIBLE);
		}
		if(stop.getStop().getConnectingBuses() != null){
			if(!stop.getStop().getConnectingBuses().trim().equalsIgnoreCase("")){
				viewHolder.connectingBuses.setVisibility(View.VISIBLE);
			} else {
				viewHolder.connectingBuses.setVisibility(View.INVISIBLE);
			}
		} else {
			viewHolder.connectingBuses.setVisibility(View.INVISIBLE);
		}

		if(stop.getStop().isEasyAccessStop()){
			viewHolder.routePlatform.setVisibility(View.VISIBLE);
		} else {
			viewHolder.routePlatform.setVisibility(View.INVISIBLE);
		}
		
		if(isCurrentIndex(stop.getStop())){
			viewHolder.stopOverlay.setVisibility(View.VISIBLE);
		} else {
			viewHolder.stopOverlay.setVisibility(View.GONE);
		}
		
		if(TramTrackerMainActivity.getAppManager().hasAlarm(stop.getStop())){
			viewHolder.alarmIcon.setVisibility(View.VISIBLE);
		} else {
			viewHolder.alarmIcon.setVisibility(View.GONE);
		}
		
		if(stop.getStop().isCityStop()){
			viewHolder.routeCBD.setVisibility(View.VISIBLE);
			viewHolder.routeZone2.setVisibility(View.INVISIBLE);
		} else {
			if(stop.getStop().getZone() == null)
				stop.getStop().setZone("1");
			if(stop.getStop().getZone().contains("1") && !stop.getStop().getZone().contains("2")){
				viewHolder.routeCBD.setVisibility(View.INVISIBLE);
				viewHolder.routeZone2.setVisibility(View.VISIBLE);
			} else if(stop.getStop().getZone().contains("2")){
				viewHolder.routeCBD.setVisibility(View.INVISIBLE);
				viewHolder.routeZone2.setVisibility(View.INVISIBLE);
			}
		}
		
		String turnType = stop.getStop().getTurnType();
		if(turnType != null){
			setTurn(viewHolder.turnIcon, stop.getStop().getTurnType());
		} else {
			viewHolder.turnIcon.setVisibility(View.INVISIBLE);
		}
		
		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				TramTrackerMainActivity.getAppManager().callSelection(getStopDescription(stop.getStop()));
				goToStopDescriptionScreen(stop);
			}
		});
		
		convertView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				v.setPressed(true);
				
				return false;
			}
		});
		
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return getStopsInSuburb(groupPosition).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return suburbs.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return suburbs.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	
	static class SuburbViewHolder{
		TextView suburbName;
	}
	
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		SuburbViewHolder viewHolder;
		
		if(convertView == null){
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(R.layout.route_stops_list_view_group, parent, false);
			
			viewHolder = new SuburbViewHolder();
			viewHolder.suburbName = (TextView) convertView.findViewById(R.id.group_name);
			convertView.setTag(viewHolder);
		
		} else {
			viewHolder = (SuburbViewHolder) convertView.getTag();
		}
		
		OnBoardStopsBySuburb suburb = (OnBoardStopsBySuburb) getGroup(groupPosition);
		viewHolder.suburbName.setText(suburb.getSuburb());
		
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

    @Override
    public boolean areAllItemsEnabled(){
        return true;
    }
	
    
	private void goToStopDescriptionScreen(PredictedArrivalTime stop){
		Intent intent = new Intent(this.context, OnBoardStopActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(OnBoardStopActivity.INTENT_KEY, stop.getStop());
		intent.putExtra(OnBoardStopActivity.INTENT_KEY_PREDICTED, stops);

		TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MYTRAM, 
				context.getResources().getString(R.string.tag_onboard_stop_screen), intent);
	}

	
	private PredictedArrivalTime getStop(int groupPosition, int childPosition){
		ArrayList<PredictedArrivalTime> suburb = getStopsInSuburb(groupPosition); 
		return suburb.get(childPosition);
	}
	
	
	private String getStopDescription(Stop stop){
		String text = "";
		
		text = text.concat(String.valueOf(stop.getStopNumber()));
		text = text.concat(context.getResources().getString(R.string.stop_name_colon));
		if(stop.getStopName() == null){
			Log.e("getStopDescription","Stop name not available for trackerid = " + stop.getStopNumber());
		}
		text = text.concat(stop.getStopName());
		
		return text;
	}
	
	private String getArrivalTime(PredictedArrivalTime stop){
		String string = "";
		Date date = stop.getArrivalTime();
		
		if(isAccurateIndex && isCurrentIndex(stop.getStop())){
			if(isNowIndex){
				string = context.getResources().getString(R.string.onboard_current_now);
			} else {
				string = context.getResources().getString(R.string.onboard_current_next);
			}
		} else if(date == null){
			string = context.getResources().getString(R.string.onboard_notime_format);
		} else {
			DateFormat format = new SimpleDateFormat(context.getResources().getString(R.string.onboard_time_format));
			string = format.format(date);
		}
		return string;
	}

	
	private void setTurn(ImageView turnIcon, String turn){
		int resourceID = 0;
		if(turn.equalsIgnoreCase("right")){
			resourceID = R.drawable.icn_mytram_arrowright;
		} else if(turn.equalsIgnoreCase("straight")){
			resourceID = R.drawable.icn_mytram_arrowstraight;
		} else if(turn.equalsIgnoreCase("veer_left")){
			resourceID = R.drawable.icn_mytram_veerleft;
		} else if(turn.equalsIgnoreCase("left")){
			resourceID = R.drawable.icn_mytram_arrowleft;
		} else if(turn.equalsIgnoreCase("s_to_right")){
			resourceID = R.drawable.icn_mytram_sturn;
		} else if(turn.equalsIgnoreCase("veer_right")){
			resourceID = R.drawable.icn_mytram_veerright;
		}
		turnIcon.setImageResource(resourceID);
		turnIcon.setVisibility(View.VISIBLE);
	}
	
	private void setRouteColour(ImageView routeColor, String colour, int type){
		int resourceID = 0;
		if(colour == null)
			colour = "GRAY";
		if(colour.equalsIgnoreCase("GREEN")){
			if(type == TYPE_START){
				resourceID = R.drawable.icn_route_green_start;
			} else if(type == TYPE_FINISH){
				resourceID = R.drawable.icn_route_green_end;
			} else {
				resourceID = R.drawable.icn_route_green_mid;
			}
		} else if(colour.equalsIgnoreCase("CYAN")){
			if(type == TYPE_START){
				resourceID = R.drawable.icn_route_cyan_start;
			} else if(type == TYPE_FINISH){
				resourceID = R.drawable.icn_route_cyan_end;
			} else {
				resourceID = R.drawable.icn_route_cyan_mid;
			}
		} else if(colour.equalsIgnoreCase("YELLOW")){
			if(type == TYPE_START){
				resourceID = R.drawable.icn_route_yellow_start;
			} else if(type == TYPE_FINISH){
				resourceID = R.drawable.icn_route_yellow_end;
			} else {
				resourceID = R.drawable.icn_route_yellow_mid;
			}
		} else if(colour.equalsIgnoreCase("PINK")){
			if(type == TYPE_START){
				resourceID = R.drawable.icn_route_pink_start;
			} else if(type == TYPE_FINISH){
				resourceID = R.drawable.icn_route_pink_end;
			} else {
				resourceID = R.drawable.icn_route_pink_mid;
			}
		} else if(colour.equalsIgnoreCase("ORANGE")){
			if(type == TYPE_START){
				resourceID = R.drawable.icn_route_orange_start;
			} else if(type == TYPE_FINISH){
				resourceID = R.drawable.icn_route_orange_end;
			} else {
				resourceID = R.drawable.icn_route_orange_mid;
			}
		} else if(colour.equalsIgnoreCase("TEAL")){
			if(type == TYPE_START){
				resourceID = R.drawable.icn_route_teal_start;
			} else if(type == TYPE_FINISH){
				resourceID = R.drawable.icn_route_teal_end;
			} else {
				resourceID = R.drawable.icn_route_teal_mid;
			}
		} else {
			if(type == TYPE_START){
				resourceID = R.drawable.icn_route_grey_start;
			} else if(type == TYPE_FINISH){
				resourceID = R.drawable.icn_route_grey_end;
			} else {
				resourceID = R.drawable.icn_route_grey_mid;
			}
		}
		routeColor.setImageResource(resourceID);
	}
}
