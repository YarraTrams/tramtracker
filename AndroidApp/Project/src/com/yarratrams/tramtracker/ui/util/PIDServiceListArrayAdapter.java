package com.yarratrams.tramtracker.ui.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.DestinationGroup;
import com.yarratrams.tramtracker.objects.Tram;
import com.yarratrams.tramtracker.ui.OnBoardActivity;
import com.yarratrams.tramtracker.ui.PIDActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;

import android.app.ActivityGroup;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class PIDServiceListArrayAdapter extends ArrayAdapter<DestinationGroup> {
	private final PIDActivity context;
	private ArrayList<DestinationGroup> groups;
	private ArrayList<String> expanded;

	public PIDServiceListArrayAdapter(PIDActivity context, ArrayList<DestinationGroup> groups) {
		super(context, R.layout.pid_routes_detail, groups);
		this.context = context;
		this.groups = groups;
		this.expanded = new ArrayList<String>();
	}
	
	public void updateList(ArrayList<DestinationGroup> groups) {
		this.groups.clear();
		this.groups.addAll(groups);
		super.notifyDataSetChanged();
	}

	static class ServiceGroupViewHolder{
		TableLayout routeTable;
		TableRow routeCell1;
		TableRow routeCell2;
		ImageView expandArrow;
		ImageView retractArrow;
		//ImageView expandDivider;
		
		ServiceViewHolder service;
		ServiceViewHolder service1;
		ServiceViewHolder service2;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ServiceGroupViewHolder viewHolder;
		
		if(convertView == null){
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(R.layout.pid_routes_detail, parent, false);
			
			viewHolder = new ServiceGroupViewHolder();
			viewHolder.routeTable = (TableLayout) convertView.findViewById(R.id.service_table);
			viewHolder.routeCell1 = (TableRow) convertView.findViewById(R.id.service_row1);
			viewHolder.routeCell2 = (TableRow) convertView.findViewById(R.id.service_row2);
			viewHolder.expandArrow = (ImageView) convertView.findViewById(R.id.imageView_expand_cell);
			viewHolder.retractArrow = (ImageView) convertView.findViewById(R.id.imageView_retract_cell);
			//viewHolder.expandDivider = (ImageView) convertView.findViewById(R.id.imageView_expand_cell_divider);
			
			TableRow row = (TableRow)convertView.findViewById(R.id.service_row);
			viewHolder.service = createServiceViewHolder(row);
			TableRow row1 = (TableRow)convertView.findViewById(R.id.service_row1);
			viewHolder.service1 = createServiceViewHolder(row1);
			TableRow row2 = (TableRow)convertView.findViewById(R.id.service_row2);
			viewHolder.service2 = createServiceViewHolder(row2);
			convertView.setTag(viewHolder);
		
		} else {
			viewHolder = (ServiceGroupViewHolder) convertView.getTag();
		}
		
		final DestinationGroup group = groups.get(position);
		setServiceInfo(viewHolder.service, group, 0);
		setServiceInfo(viewHolder.service1, group, 1);
		setServiceInfo(viewHolder.service2, group, 2);
		
		if(group.isGroup()){
			expandGroup(viewHolder, group);
			viewHolder.routeTable.setOnClickListener(null);
			
		} else {
			//viewHolder.expandDivider.setVisibility(View.VISIBLE);
			viewHolder.routeTable.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					switchCellState(viewHolder, group, position);
				}
			});
			
			if(expanded(group)){
				setCellState(viewHolder, true);
			} else {
				setCellState(viewHolder, false);
			}
		}
		
		return convertView;
	}
	
	public void goToOnBoardScreen(Tram service){
		ActivityGroup group = TramTrackerMainActivity.getAppManager().requestDifferentTab(TramTrackerMainActivity.TAB_MYTRAM);
		Intent intent = new Intent(group, OnBoardActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(OnBoardActivity.INTENT_KEY, service);
		Log.e("goToOnBoardScreen", "service = " + service);
		TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MYTRAM, 
				context.getResources().getString(R.string.tag_onboard_screen), intent);
	}
	
	public void switchCellState(ServiceGroupViewHolder viewHolder, DestinationGroup group, int index){
		if(expanded(group)){					// EXPANDED
			TramTrackerMainActivity.getAppManager().callSelection(context.getString(R.string.accessibility_click_pid_collapse));
			setCellState(viewHolder, false);
			removeFromExpandList(group);
		} else{									// RETRACTED
			TramTrackerMainActivity.getAppManager().callSelection(context.getString(R.string.accessibility_click_pid_expand));
			setCellState(viewHolder, true);
			addToExpandList(group);
			context.setCurrentIndex(index);
		}
	}
	
	public void setCellState(ServiceGroupViewHolder viewHolder, boolean expand){
		if(expand) {							 			// EXPAND
			viewHolder.expandArrow.setVisibility(View.INVISIBLE);
			viewHolder.retractArrow.setVisibility(View.VISIBLE);
			viewHolder.routeCell1.setVisibility(View.VISIBLE);
			viewHolder.routeCell2.setVisibility(View.VISIBLE);
		} else { 											// RETRACT
			viewHolder.expandArrow.setVisibility(View.VISIBLE);
			viewHolder.retractArrow.setVisibility(View.INVISIBLE);
			viewHolder.routeCell1.setVisibility(View.GONE);
			viewHolder.routeCell2.setVisibility(View.GONE); 
		}
	}
	
	private void expandGroup(ServiceGroupViewHolder viewHolder, DestinationGroup group){
		viewHolder.expandArrow.setVisibility(View.INVISIBLE);
		viewHolder.retractArrow.setVisibility(View.INVISIBLE);
		//viewHolder.expandDivider.setVisibility(View.INVISIBLE);
		viewHolder.routeCell1.setVisibility(View.VISIBLE);
		viewHolder.routeCell2.setVisibility(View.VISIBLE);
		
		addToExpandList(group);
	}
	
	public void addToExpandList(DestinationGroup group){
		if(!expanded(group)){
			if(group.isGroup()){
				if(group.isGroupByDestination()){
					expanded.add(group.getDestination());
				} else {
					expanded.add(group.getHeadboardRoute());
				}
			} else {
				expanded.add(group.getDestination());
			}
		}
	}
	
	public void removeFromExpandList(DestinationGroup group){
		expanded.remove(group.getDestination());
		expanded.remove(group.getHeadboardRoute());
	}
	
	public boolean expanded(DestinationGroup group){
		for(String cell: expanded){
			if(cell.equals(group.getDestination()) || cell.equals(group.getHeadboardRoute())){
				return true;
			}
		}
		return false;
	}
	
	static class ServiceViewHolder{
		TextView routeNumber;
		TextView tramDestination;
		ImageView tramType;
		ImageView hasAccess;
		ImageView hasAirCon;
		ImageView isServiceChanged;
		ImageView isDisrupted;
		TextView tramMins;
		TextView tramDay;
		TextView tramAM;
		RelativeLayout onboardButton;
		ImageView onboardChevron;
	}
	
	private ServiceViewHolder createServiceViewHolder(TableRow rowView){
		ServiceViewHolder viewHolder = new ServiceViewHolder();
		viewHolder.routeNumber = (TextView) rowView.findViewById(R.id.service_route_number);
		viewHolder.tramDestination = (TextView) rowView.findViewById(R.id.service_route_direction);
		viewHolder.tramType = (ImageView) rowView.findViewById(R.id.service_tram_type);
		viewHolder.hasAccess = (ImageView) rowView.findViewById(R.id.service_tram_access);
		viewHolder.hasAirCon = (ImageView) rowView.findViewById(R.id.service_tram_aircon);
		viewHolder.isServiceChanged = (ImageView) rowView.findViewById(R.id.service_tram_change);
		viewHolder.isDisrupted = (ImageView) rowView.findViewById(R.id.service_tram_disruption);
		viewHolder.tramMins = (TextView) rowView.findViewById(R.id.service_route_mins);
		viewHolder.tramDay = (TextView) rowView.findViewById(R.id.service_route_day);
		viewHolder.tramAM = (TextView) rowView.findViewById(R.id.service_route_am);
		viewHolder.onboardButton = (RelativeLayout) rowView.findViewById(R.id.tram_onboard);
		viewHolder.onboardChevron = (ImageView) rowView.findViewById(R.id.tram_onboard_chevron);
		
		return viewHolder;
	}
	
	public void setServiceInfo(ServiceViewHolder viewHolder, final DestinationGroup group, int next){
		if(group.size() <= next){
			setEmptyServiceInfo(viewHolder);
			return;
		}

		final Tram service = group.getNextServiceTram(next);
		
		if(next == 0){
			viewHolder.routeNumber.setText(service.getHeadboardNo());
		} else {
			if(group.isGroup()){
				viewHolder.routeNumber.setText(service.getHeadboardNo());
			} else {
				viewHolder.routeNumber.setText("");
			}
		}
		
		viewHolder.tramDestination.setText(service.getDestination());
		viewHolder.tramType.setVisibility(View.VISIBLE);
		if(service.getTramClass() == Tram.CLASS_Z1){
			viewHolder.tramType.setImageResource(R.drawable.tram_z);
		} else if(service.getTramClass() == Tram.CLASS_Z2){
			viewHolder.tramType.setImageResource(R.drawable.tram_z);
		} else if(service.getTramClass() == Tram.CLASS_Z3){
			viewHolder.tramType.setImageResource(R.drawable.tram_z3);
		} else if(service.getTramClass() == Tram.CLASS_A1){
			viewHolder.tramType.setImageResource(R.drawable.tram_a);
		} else if(service.getTramClass() == Tram.CLASS_A2){
			viewHolder.tramType.setImageResource(R.drawable.tram_a);
		} else if(service.getTramClass() == Tram.CLASS_W){
			viewHolder.tramType.setImageResource(R.drawable.tram_w);
		} else if(service.getTramClass() == Tram.CLASS_B1){
			viewHolder.tramType.setImageResource(R.drawable.tram_b);
		} else if(service.getTramClass() == Tram.CLASS_B2){
			viewHolder.tramType.setImageResource(R.drawable.tram_b);
		} else if(service.getTramClass() == Tram.CLASS_C1){
			viewHolder.tramType.setImageResource(R.drawable.tram_c1);
		} else if(service.getTramClass() == Tram.CLASS_D1){
			viewHolder.tramType.setImageResource(R.drawable.tram_d1);
		} else if(service.getTramClass() == Tram.CLASS_D2){
			viewHolder.tramType.setImageResource(R.drawable.tram_d2);
		} else if(service.getTramClass() == Tram.CLASS_C2){
			viewHolder.tramType.setImageResource(R.drawable.tram_c2);
		} else {
			viewHolder.tramType.setVisibility(View.INVISIBLE);
		}

		if(service.isLowFloor()){
			viewHolder.hasAccess.setVisibility(View.VISIBLE);
		} else {
			viewHolder.hasAccess.setVisibility(View.GONE);
		}
		if(service.isHasAirCon()){
			viewHolder.hasAirCon.setVisibility(View.VISIBLE);
		} else {
			viewHolder.hasAirCon.setVisibility(View.GONE);
		}
		if(service.hasSpecialEvent()){
			viewHolder.isServiceChanged.setVisibility(View.VISIBLE);
		} else {
			viewHolder.isServiceChanged.setVisibility(View.GONE);
		}
		if(service.isDisrupted()){
			viewHolder.isDisrupted.setVisibility(View.VISIBLE);
		} else {
			viewHolder.isDisrupted.setVisibility(View.GONE);
		}
		
		int minutes = service.getMinutesFromServerResponseTime();
		if(minutes == 0){
			viewHolder.tramMins.setText(context.getResources().getString(R.string.route_route0Mins));
			viewHolder.tramMins.setTextSize(16);
			viewHolder.tramDay.setVisibility(View.GONE);
			viewHolder.tramAM.setVisibility(View.GONE);
		} else if(minutes > 60){
			viewHolder.tramMins.setText(getArrivalTimeString(service.getArrivalTime()));
			viewHolder.tramMins.setTextSize(13);
			viewHolder.tramDay.setText(getArrivalDayString(service.getArrivalTime()));
			viewHolder.tramDay.setVisibility(View.VISIBLE);
			viewHolder.tramDay.setTextSize(11);
			viewHolder.tramAM.setText(getArrivalAMString(service.getArrivalTime()));
			viewHolder.tramAM.setVisibility(View.VISIBLE);
			viewHolder.tramAM.setTextSize(10);
		} else {
			viewHolder.tramMins.setText(String.valueOf(minutes));
			viewHolder.tramMins.setTextSize(20);
			viewHolder.tramDay.setVisibility(View.GONE);
			viewHolder.tramAM.setVisibility(View.GONE);
		}
		
		if(service.getVehicleNumber() > 0){  //  && route.isMainRoute()
			viewHolder.onboardChevron.setVisibility(View.VISIBLE);
			viewHolder.onboardButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					TramTrackerMainActivity.getAppManager().callSelection(getDescription(service));
					goToOnBoardScreen(service);
				}
			});
		} else {
			viewHolder.onboardChevron.setVisibility(View.INVISIBLE);
			viewHolder.onboardButton.setOnClickListener(null);
		}
	}
	
	public void setEmptyServiceInfo(ServiceViewHolder viewHolder){
		viewHolder.routeNumber.setText("");
		viewHolder.tramDestination.setText("");
		viewHolder.tramType.setVisibility(View.INVISIBLE);
		viewHolder.hasAccess.setVisibility(View.GONE);
		viewHolder.hasAirCon.setVisibility(View.GONE);
		viewHolder.isServiceChanged.setVisibility(View.GONE);
		viewHolder.isDisrupted.setVisibility(View.GONE);
		viewHolder.tramMins.setText("");
		viewHolder.tramDay.setVisibility(View.GONE);
		viewHolder.tramAM.setVisibility(View.GONE);
		viewHolder.onboardChevron.setVisibility(View.INVISIBLE);
		viewHolder.onboardButton.setOnClickListener(null);
	}
	
	
	private String getDescription(Tram service){
		String description = "";
		
		description = service.getHeadboardNo();
		description = description.concat(context.getString(R.string.route_filter_space));
		description = description.concat(service.getDestination());
		description = description.concat(context.getString(R.string.route_filter_space));
		description = description.concat(context.getString(R.string.accessibility_click_pid_onboard));
		
		return description;
	}
	
	public String getArrivalTimeString(Date date){
		DateFormat timeFormat = new SimpleDateFormat(context.getResources().getString(R.string.route_time_format));
		String timeString = timeFormat.format(date); 
		return timeString;
	}
	
	public String getArrivalDayString(Date date){
		DateFormat timeFormat = new SimpleDateFormat(context.getResources().getString(R.string.route_day_format));
		String timeString = timeFormat.format(date); 
		return timeString;
	}
	public String getArrivalAMString(Date date){
		DateFormat timeFormat = new SimpleDateFormat(context.getResources().getString(R.string.route_am_format));
		String timeString = timeFormat.format(date); 
		return timeString;
	}
	
}
