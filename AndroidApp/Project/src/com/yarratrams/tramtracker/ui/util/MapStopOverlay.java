package com.yarratrams.tramtracker.ui.util;

import java.util.ArrayList;

import com.yarratrams.tramtracker.R;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;
import com.yarratrams.tramtracker.objects.NearbyStop;
import com.yarratrams.tramtracker.objects.Stop;

import android.app.Activity;
import android.graphics.drawable.Drawable;


public class MapStopOverlay extends ItemizedOverlay<OverlayItem> {
	private ArrayList<OverlayItem> overlays = new ArrayList<OverlayItem>();
	private final Activity context;
	final MapController mc;
	
	private ArrayList<NearbyStop> stops;
	private MapStopDialog dialog;
	
	
	private static Drawable getDefaultMarker(Activity context){
		return context.getResources().getDrawable(R.drawable.icn_search_stop);
	}
	
	public MapStopOverlay(Drawable defaultMarker, Activity context, MapView mapView) {
		super(boundCenterBottom(defaultMarker));
		this.context = context;
		mc = mapView.getController();
		
		stops = new ArrayList<NearbyStop>();
	}

	public MapStopOverlay(Activity context, MapView mapView) {
		super(boundCenterBottom(getDefaultMarker(context)));
		this.context = context;
		mc = mapView.getController();
		
		stops = new ArrayList<NearbyStop>();
	}
	
	public void updateTramStopsList(ArrayList<NearbyStop> stops) {
		this.stops.clear();
		this.stops.addAll(stops);
		
		resetOverlays();
		if(dialog != null){
			dialog.dismiss();
		}
		
		for(NearbyStop nearbyStop : stops){
			Stop stop = nearbyStop.getStop();
	        GeoPoint point = new GeoPoint(stop.getLatitudeE6(), stop.getLongitudeE6());
	        OverlayItem overlayitem = new OverlayItem(point, null, null);
	        addOverlay(overlayitem);
		}
	}
	
	
	
	public void addOverlay(OverlayItem overlay) {
		overlays.add(overlay);
		populate();
	}
	
	public void resetOverlays(){
		overlays.clear();
		populate();
	}

	@Override
	protected OverlayItem createItem(int i) {
		return overlays.get(i);
	}

	@Override
	public int size() {
		return overlays.size();
	}
	
	@Override
	protected boolean onTap(int index) {
		mc.animateTo(createItem(index).getPoint());
		NearbyStop nearbyStop = stops.get(index);
		dialog = new MapStopDialog(context, nearbyStop);
		
		dialog.show();
		return true;
	}
	
	
}
