package com.yarratrams.tramtracker.ui.util;

import java.util.ArrayList;

import com.yarratrams.tramtracker.R;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;
import com.yarratrams.tramtracker.objects.Favourite;
import com.yarratrams.tramtracker.objects.FavouritesGroup;
import com.yarratrams.tramtracker.objects.Stop;

import android.app.Activity;
import android.graphics.drawable.Drawable;


public class MapFavouriteOverlay extends ItemizedOverlay<OverlayItem> {
	private ArrayList<OverlayItem> overlays = new ArrayList<OverlayItem>();
	private final Activity context;
	final MapController mc;
	
	private ArrayList<FavouritesGroup> groups;
	private ArrayList<Favourite> favourites;
	private MapFavouriteDialog dialog;
	
	
	private static Drawable getDefaultMarker(Activity context){
		return context.getResources().getDrawable(R.drawable.icn_search_stop);
	}
	
	public MapFavouriteOverlay(Drawable defaultMarker, Activity context, MapView mapView) {
		super(boundCenterBottom(defaultMarker));
		this.context = context;
		mc = mapView.getController();
		
		groups = new ArrayList<FavouritesGroup>();
		favourites = new ArrayList<Favourite>();
	}

	public MapFavouriteOverlay(Activity context, MapView mapView) {
		super(boundCenterBottom(getDefaultMarker(context)));
		this.context = context;
		mc = mapView.getController();
		
		groups = new ArrayList<FavouritesGroup>();
		favourites = new ArrayList<Favourite>();
	}
	
	public void updateFavouritesStopsList(ArrayList<FavouritesGroup> groups) {
		this.groups.clear();
		this.groups.addAll(groups);
		
		resetOverlays();
		if(dialog != null){
			dialog.dismiss();
		}
		
		for(FavouritesGroup group: groups){
			ArrayList<Favourite> groupFavourites = group.getFavourites();
			favourites.addAll(groupFavourites);
			for(Favourite favourite : groupFavourites){
				Stop stop = favourite.getStop();
		        GeoPoint point = new GeoPoint(stop.getLatitudeE6(), stop.getLongitudeE6());
		        OverlayItem overlayitem = new OverlayItem(point, null, null);
		        addOverlay(overlayitem);
			}
		}
	}
	
	
	
	public void addOverlay(OverlayItem overlay) {
		overlays.add(overlay);
		populate();
	}
	
	public void resetOverlays(){
		overlays.clear();
		populate();
	}

	@Override
	protected OverlayItem createItem(int i) {
		return overlays.get(i);
	}

	@Override
	public int size() {
		return overlays.size();
	}
	
	@Override
	protected boolean onTap(int index) {
		mc.animateTo(createItem(index).getPoint());
		
		Favourite favourite = favourites.get(index);
		dialog = new MapFavouriteDialog(context, favourite);
		
		dialog.show();
		return true;
	}
	
	
}
