package com.yarratrams.tramtracker.ui.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.Route;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.objects.Tram;
import com.yarratrams.tramtracker.ui.TimetableActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


public class TimetableListArrayAdapter extends ArrayAdapter<Tram> {
	private final TimetableActivity context;
	private Route route; 
	private Stop stop; 
	private final ArrayList<Tram> services;

	public TimetableListArrayAdapter(TimetableActivity context, Stop stop, Route route, ArrayList<Tram> services) {
		super(context, R.layout.timetable_list_view_detail, services);
		this.context = context;
		this.route = route;
		this.stop = stop;
		this.services = services;
	}

	public void updateList(ArrayList<Tram> services) {
		this.services.clear();
		this.services.addAll(services);
		super.notifyDataSetChanged();
	}

	static class TimetableViewHolder{
		TextView destination;
		TextView routeNumber;
		TextView date;
		TextView time;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		TimetableViewHolder viewHolder;
		
		if(convertView == null){
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(R.layout.timetable_list_view_detail, parent, false);
			
			viewHolder = new TimetableViewHolder();
			viewHolder.destination = (TextView)convertView.findViewById(R.id.route_destination);
			viewHolder.routeNumber = (TextView)convertView.findViewById(R.id.route_number);
			viewHolder.date = (TextView)convertView.findViewById(R.id.predicted_date);
			viewHolder.time = (TextView)convertView.findViewById(R.id.predicted_time);
			convertView.setTag(viewHolder);
		
		} else {
			viewHolder = (TimetableViewHolder) convertView.getTag();
		}
		
		final Tram service = services.get(position);
		viewHolder.destination.setText(route.getDestination());
		viewHolder.routeNumber.setText(getRouteDescription());
		if(isTodayDate(service.getArrivalTime())){
			viewHolder.date.setVisibility(View.GONE);
			viewHolder.date.setText("");
		} else {
			viewHolder.date.setVisibility(View.VISIBLE);
			viewHolder.date.setText(getArrivalDateString(service.getArrivalTime()));
		}
		viewHolder.time.setText(getArrivalTimeString(service.getArrivalTime()));
		
		return convertView;
	}
	
	private String getRouteDescription(){
		String text = "";
		
		text = context.getResources().getString(R.string.timetable_route);
		text = text.concat(route.getRouteNumber());
		
		if(stop.getCityDirection() != null){
			if(stop.getCityDirection().contains(context.getResources().getString(R.string.timetable_route_direction))){
				text = text.concat(context.getResources().getString(R.string.timetable_via_city));
			}
		}
		
		return text;
	}
	
	public String getArrivalTimeString(Date date){
		DateFormat timeFormat = new SimpleDateFormat(context.getResources().getString(R.string.timetable_result_time_format));
		String timeString = timeFormat.format(date); 
		return timeString;
	}
	public String getArrivalDateString(Date date){
		DateFormat dateFormat = new SimpleDateFormat(context.getResources().getString(R.string.timetable_result_date_format));
		String dateString = dateFormat.format(date); 
		return dateString;
	}
	public boolean isTodayDate(Date timetableDate){
		Calendar today = Calendar.getInstance();
		Calendar date = Calendar.getInstance();
		date.setTime(timetableDate);
		if(today.get(Calendar.DAY_OF_YEAR) == date.get(Calendar.DAY_OF_YEAR)){
			return true;
		}
		return false;
	}
}
