package com.yarratrams.tramtracker.ui.util;

import java.text.DecimalFormat;

import com.google.android.maps.GeoPoint;
import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.NearbyStop;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.ui.PIDActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;

import android.content.Context;
import android.content.Intent;
import android.view.View;


public class MapStopDialog extends MapDialog{
	private Context context;
	private NearbyStop nearbyStop;
	private Stop stop;

	public MapStopDialog(Context context, NearbyStop nearbyStop) {
		super(context);
		this.context = context;
		this.nearbyStop = nearbyStop;
		this.stop = nearbyStop.getStop();
		
		createDialog();
	}
	
	
	private void createDialog(){
		setTramStopInfo();
		
		setOnDirectionsClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				TramTrackerMainActivity.getAppManager().callSelection(context.getString(R.string.accessibility_click_goto_directions));
				GeoPoint destination = new GeoPoint(stop.getLatitudeE6(), stop.getLongitudeE6());
				TramTrackerMainActivity.getAppManager().requestDirectionsService(destination);
			}
		});
		setOnGoClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				TramTrackerMainActivity.getAppManager().callSelection(context.getString(R.string.accessibility_click_goto_pid));
				Intent intent = new Intent(context, PIDActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra(PIDActivity.INTENT_KEY, stop);
				TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_NEARBY, 
						context.getResources().getString(R.string.tag_pid_screen), intent);
			}
		});
		
	}
	
	
	private void setTramStopInfo(){
		setNameText(getStopDescription());
		setDetailText(getRoutesDescription());
		setDistanceText(getDistance());
	}
	
	private String getStopDescription(){
		String text = "";
		
		text = text.concat(String.valueOf(stop.getStopNumber()));
		text = text.concat(context.getResources().getString(R.string.stop_name_colon));
		text = text.concat(stop.getStopName());
		
		return text;
	}
	
	
	private String getRoutesDescription(){
		String text = "";
		
		text = context.getResources().getString(R.string.stop_direction_routes);
		String[] array = stop.getRoutes();
		for(int i = 0; i < array.length; i++){
			text = text.concat(array[i]);
			if(i < array.length-1){
				text = text.concat(context.getResources().getString(R.string.stop_routes_coma));
			}
		}
		text = text.concat(context.getResources().getString(R.string.stop_name_space));
		text = text.concat(stop.getCityDirection());
		
		return text;
	}

	
	private String getDistance(){
		String text = "";
		
		if(nearbyStop.getIntDistance() < 1000){
			text = String.valueOf(nearbyStop.getIntDistance());
			text = text.concat(context.getResources().getString(R.string.stop_distance_meters));
		} else {
			double distance = (double)nearbyStop.getIntDistance()/1000;
			DecimalFormat df = new DecimalFormat("##.0");
			text = df.format(distance);
			text = text.concat(context.getResources().getString(R.string.stop_distance_kilometers));
		}
		
		return text;
	}
	
}