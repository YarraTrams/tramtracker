package com.yarratrams.tramtracker.ui.util;

import java.util.ArrayList;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.MenuOption;
import com.yarratrams.tramtracker.ui.NearbyActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;

import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class MenuNearbyAdapter extends ArrayAdapter<MenuOption>{
	private static final int MENU_OPTION_ALLSTOPS = 0;
	private static final int MENU_OPTION_SHELTERS = 1;
	private static final int MENU_OPTION_EASYACCESS = 2;
	private static final int MENU_OPTION_OUTLETS = 3;
	private static final int MENU_OPTION_TRACKERID = 4;
	private static final int MENU_OPTION_SEARCH = 5;
	
	private NearbyActivity activity;
	private static ArrayList<MenuOption> options;
	
	
	public MenuNearbyAdapter(NearbyActivity activity) {
		super(activity, R.layout.menu_container_detail, createMenuList());
		
		this.activity = activity;
	}
	
	
	private static ArrayList<MenuOption> createMenuList(){
		options = new ArrayList<MenuOption>();
		
		MenuOption option; 
		option = new MenuOption(MENU_OPTION_ALLSTOPS, R.drawable.icn_menu_allstops, R.string.menu_all_stops);
		options.add(option);
		option = new MenuOption(MENU_OPTION_SHELTERS, R.drawable.icn_menu_shelters, R.string.menu_shelters);
		options.add(option);
		option = new MenuOption(MENU_OPTION_EASYACCESS, R.drawable.icn_menu_easyaccess, R.string.menu_easy_access);
		options.add(option);
		option = new MenuOption(MENU_OPTION_OUTLETS, R.drawable.icn_menu_outlets, R.string.menu_ticketOutlets);
		options.add(option);
		option = new MenuOption(MENU_OPTION_TRACKERID, R.drawable.icn_menu_trackerid, R.string.menu_trackerid);
		options.add(option);
		option = new MenuOption(MENU_OPTION_SEARCH, R.drawable.icn_menu_search, R.string.menu_search);
		options.add(option);
		
		return options;
	}
	
	
	static class MenuOptionViewHolder{
		ImageView iconView;
		TextView nameView;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		MenuOptionViewHolder viewHolder;
		
		if(convertView == null){
			LayoutInflater inflater = activity.getLayoutInflater();
			convertView = inflater.inflate(R.layout.menu_container_detail, parent, false);
			
			viewHolder = new MenuOptionViewHolder();
			viewHolder.iconView = (ImageView) convertView.findViewById(R.id.menu_icon);
			viewHolder.nameView = (TextView) convertView.findViewById(R.id.menu_name);
			convertView.setTag(viewHolder);
		
		} else {
			viewHolder = (MenuOptionViewHolder) convertView.getTag();
		}
		
		MenuOption option = options.get(position);
		
		viewHolder.iconView.setImageDrawable(activity.getResources().getDrawable(option.getMenuResourceIcon()));
		viewHolder.nameView.setText(activity.getString(option.getMenuResourceName()));
		
		createMenuOptions(option, convertView);
		
		convertView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				v.setPressed(true);
				return false;
			}
		});
		
		return convertView;
	}
	
	
	protected void createMenuOptions(final MenuOption option, View convertView){
		
		switch (option.getMenuID()) {
			case MENU_OPTION_ALLSTOPS :
				convertView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						TramTrackerMainActivity.getAppManager().callSelection(activity.getString(option.getMenuResourceName()));
						activity.runAllStopsMenuOption();
					}
				});
				break;
				
			case MENU_OPTION_SHELTERS :
				convertView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						TramTrackerMainActivity.getAppManager().callSelection(activity.getString(option.getMenuResourceName()));
						activity.runSheltersMenuOption();
					}
				});
				break;
				
			case MENU_OPTION_EASYACCESS :
				convertView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						TramTrackerMainActivity.getAppManager().callSelection(activity.getString(option.getMenuResourceName()));
						activity.runEasyAccessMenuOption();
					}
				});
				break;
				
			case MENU_OPTION_OUTLETS :
				convertView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						TramTrackerMainActivity.getAppManager().callSelection(activity.getString(option.getMenuResourceName()));
						activity.runOutletsMenuOption();
					}
				});
				break;
				
			case MENU_OPTION_SEARCH :
				convertView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						TramTrackerMainActivity.getAppManager().callSelection(activity.getString(option.getMenuResourceName()));
						activity.runSearchMenuOption();
					}
				});
				break;
				
			case MENU_OPTION_TRACKERID :
				convertView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						TramTrackerMainActivity.getAppManager().callSelection(activity.getString(option.getMenuResourceName()));
						activity.runTrackerIDMenuOption();
					}
				});
				break;

			default :
				break;
		}
		
	}

}
