package com.yarratrams.tramtracker.ui.util;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.Route;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.ArrayList;


public class RoutesFilterAdapter {
	private LinearLayout list;
	private final Activity context;
	private final ArrayList<Route> routes;
	private ArrayList<String> selected;

	public RoutesFilterAdapter(LinearLayout list, Activity context, ArrayList<Route> routes, ArrayList<String> preSelected) {
		this.list = list;
		this.context = context;
		this.routes = routes;
		this.selected = preSelected;
		populate();
	}
	

	public void populate(){
		list.removeAllViews();

		View view = getLowFloorView();
		list.addView(view);
		
		view = getAllView();
		list.addView(view);

		
		for(int i = 0; i < routes.size(); i++){
			view = getView(i);
			list.addView(view);
		}
	}
	
	private View getAllView() {
		LayoutInflater inflater = context.getLayoutInflater();
		final View rowView = inflater.inflate(R.layout.routes_filter_detail, null, true);
		
		TextView routeName = (TextView)rowView.findViewById(R.id.routes_filter_description);
		final ToggleButton checkButton = (ToggleButton)rowView.findViewById(R.id.routes_filter_checkButton);
		
		routeName.setText(context.getResources().getString(R.string.route_filter_all));
		
		rowView.setFocusable(false);
		rowView.setFocusableInTouchMode(false);
		
		if(areAllSelected()){
			checkButton.setChecked(true);
		} else {
			checkButton.setChecked(false);
		}
		
		checkButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(checkButton.isChecked()){
					TramTrackerMainActivity.getAppManager().callSelection(context.getString(R.string.accessibility_click_filter_allstops_checked));
					selectAll();
				} else {
					TramTrackerMainActivity.getAppManager().callSelection(context.getString(R.string.accessibility_click_filter_allstops_notchecked));
					unselectAll();
				}
			}
		});
		
		return rowView;
	}
	
	private View getLowFloorView() {
		LayoutInflater inflater = context.getLayoutInflater();
		final View rowView = inflater.inflate(R.layout.routes_filter_detail, null, true);
		
		TextView routeName = (TextView)rowView.findViewById(R.id.routes_filter_description);
		final ToggleButton checkButton = (ToggleButton)rowView.findViewById(R.id.routes_filter_checkButton);
		ImageView separator = (ImageView) rowView.findViewById(R.id.routes_filter_separator);
		
		routeName.setText(context.getResources().getString(R.string.route_filter_lowfloor));
		separator.setVisibility(View.VISIBLE);
		
		rowView.setFocusable(false);
		rowView.setFocusableInTouchMode(false);
		
		if(isLowFloorSelected()){
			checkButton.setChecked(true);
		} else {
			checkButton.setChecked(false);
		}
		
		checkButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(checkButton.isChecked()){
					TramTrackerMainActivity.getAppManager().callSelection(context.getString(R.string.accessibility_click_filter_lowfrloor_checked));
					selectLowFloor();
				} else {
					TramTrackerMainActivity.getAppManager().callSelection(context.getString(R.string.accessibility_click_filter_lowfrloor_notchecked));
					unSelectFlowFloor();
				}
			}
		});
		
		return rowView;
	}
	
	private View getView(final int position) {
		LayoutInflater inflater = context.getLayoutInflater();
		final View rowView = inflater.inflate(R.layout.routes_filter_detail, null, true);
		
		final Route route = routes.get(position);
		
		TextView routeName = (TextView)rowView.findViewById(R.id.routes_filter_description);
		final ToggleButton checkButton = (ToggleButton)rowView.findViewById(R.id.routes_filter_checkButton);
		
		routeName.setText(generateRouteDescription(route));

		rowView.setFocusable(false);
		rowView.setFocusableInTouchMode(false);

		if(selected(route.getRouteNumber())){
			checkButton.setChecked(true);
		} else {
			checkButton.setChecked(false);
		}
		
		checkButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				TramTrackerMainActivity.getAppManager().callSelection(getDescription(generateRouteDescription(route)));
				selectOne(route.getRouteNumber(), v);
			}
		});
		
		return rowView;
	}
	
	private String getDescription(String route){
		String description = "";
		
		description = route;
		description = description.concat(context.getString(R.string.route_filter_space));
		description = description.concat(context.getString(R.string.accessibility_click_filter_lowfrloor_checked));
		
		return description;
	}
	
	private boolean isLowFloorSelected(){
		if(!selected.isEmpty()){
			for(String option: selected){
				if(option.equalsIgnoreCase(context.getResources().getString(R.string.route_filter_dbtag_lowfloor))){
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean areAllSelected(){
		if(!selected.isEmpty()){
			for(String option: selected){
				if(option.equalsIgnoreCase(context.getResources().getString(R.string.route_filter_dbtag_all))){
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean selected(String route){
		if(!selected.isEmpty()){
			for(String option: selected){
				if(option.equalsIgnoreCase(context.getResources().getString(R.string.route_filter_dbtag_all)) ||
						option.equalsIgnoreCase(route)){
					return true;
				}
			}
		}
		return false;
	}
	
	
	private void clearSelected(){
		if(!selected.isEmpty()){
			for(int i = 0; i < selected.size(); i++){
				String option = selected.get(i);
				if(!option.equalsIgnoreCase(context.getResources().getString(R.string.route_filter_dbtag_lowfloor))){
					selected.remove(option);
					i--;
				}
			}
		}
	}
	
	private void selectAll(){
		View view;
		ToggleButton checkButton;
		
		clearSelected();
		selected.add(context.getResources().getString(R.string.route_filter_dbtag_all));
		for(int i = 1; i < list.getChildCount(); i++){
			view = list.getChildAt(i);
			checkButton = (ToggleButton)view.findViewById(R.id.routes_filter_checkButton);
			checkButton.setChecked(true);
		}
	}
	
	private void unselectAll(){
		View view;
		ToggleButton checkButton;
		
		clearSelected();
		for(int i = 1; i < list.getChildCount(); i++){
			view = list.getChildAt(i);
			checkButton = (ToggleButton)view.findViewById(R.id.routes_filter_checkButton);
			checkButton.setChecked(false);
		}
	}
	
	private void selectOne(String route, View v){
		View view;
		ToggleButton checkButton;
		
		clearSelected();
		selected.add(route);
		for(int i = 1; i < list.getChildCount(); i++){
			view = list.getChildAt(i);
			checkButton = (ToggleButton)view.findViewById(R.id.routes_filter_checkButton);
			if(checkButton == v){
				checkButton.setChecked(true);
			} else {
				checkButton.setChecked(false);
			}
		}
	}
	
	
	private void selectLowFloor(){
		if(!isLowFloorSelected()){
			selected.add(context.getResources().getString(R.string.route_filter_dbtag_lowfloor));
		}
	}
	
	private void unSelectFlowFloor(){
		if(!selected.isEmpty()){
			for(int i = 0; i < selected.size(); i++){
				String option = selected.get(i);
				if(option.equalsIgnoreCase(context.getResources().getString(R.string.route_filter_dbtag_lowfloor))){
					selected.remove(option);
					i--;
				}
			}
		}
	}
	
	
	public ArrayList<String> getSelected(){
		return selected;
	}

	private String generateRouteDescription(Route route){
		String description = "";
		
		description = description.concat(context.getResources().getString(R.string.route_filter_route));
		description = description.concat(route.getRouteNumber());
		description = description.concat(context.getResources().getString(R.string.route_filter_space));
		description = description.concat(route.getDestination());
		
		return description;
	}

		
}
