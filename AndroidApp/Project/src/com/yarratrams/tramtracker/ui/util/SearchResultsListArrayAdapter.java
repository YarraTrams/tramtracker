package com.yarratrams.tramtracker.ui.util;

import java.util.ArrayList;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.PointOfInterest;
import com.yarratrams.tramtracker.objects.SearchResult;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.objects.TicketOutlet;
import com.yarratrams.tramtracker.ui.OutletActivity;
import com.yarratrams.tramtracker.ui.PointOfInterestActivity;
import com.yarratrams.tramtracker.ui.SearchResultsActivity;
import com.yarratrams.tramtracker.ui.StopActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class SearchResultsListArrayAdapter extends ArrayAdapter<Object> {
	private final SearchResultsActivity context;
	private SearchResult resultObject;
	private ArrayList<Object> results;

	public SearchResultsListArrayAdapter(SearchResultsActivity context, SearchResult resultObject, ArrayList<Object> results) {
		super(context, R.layout.search_results_list_view_detail, results);
		this.context = context;
		this.resultObject = resultObject;
		this.results = results;
	}

	public void updateList(SearchResult resultObject, ArrayList<Object> results) {
		this.resultObject = resultObject;
		this.results.clear();
		this.results.addAll(results);
		super.notifyDataSetChanged();
	}
	
	
	static class SearchResultsViewHolder{
		View view;
		TextView nameView;
		TextView descriptionView;
		TextView distanceView;
		ImageView icon;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		SearchResultsViewHolder viewHolder;
		
		if(convertView == null){
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(R.layout.search_results_list_view_detail, parent, false);
			
			viewHolder = new SearchResultsViewHolder();
			viewHolder.view = convertView;
			viewHolder.nameView = (TextView)convertView.findViewById(R.id.result_name);
			viewHolder.descriptionView = (TextView)convertView.findViewById(R.id.result_description);
			viewHolder.distanceView = (TextView)convertView.findViewById(R.id.result_meters);
			viewHolder.icon = (ImageView) convertView.findViewById(R.id.result_icon);
			convertView.setTag(viewHolder);
			
		} else {
			viewHolder = (SearchResultsViewHolder) convertView.getTag();
		}
		
		Object object = results.get(position);
		
		if (object instanceof Stop) {
			Stop stop = (Stop) object;
			getStopView(viewHolder, stop);
			
		} else if (object instanceof TicketOutlet) {
			TicketOutlet outlet = (TicketOutlet) object;
			getOutletView(viewHolder, outlet);
		
		} else if (object instanceof PointOfInterest) {
			PointOfInterest poi = (PointOfInterest) object;
			getPOIView(viewHolder, poi);
		}
		
		convertView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				v.setPressed(true);
				
				return false;
			}
		});
		
		return convertView;
	}
	
	private void getStopView(SearchResultsViewHolder viewHolder, final Stop stop){
		setStopIconResource(viewHolder.icon, stop);
		String name = getStopDescription(stop);
		String desription = getRoutesDescription(stop);
		//String distance = getDistance(nearbyStop.getIntDistance());
		setTextData(viewHolder, name, desription, "");
		
		viewHolder.view.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				TramTrackerMainActivity.getAppManager().callSelection(getStopDescription(stop));
				goToStopScreen(stop);
			}
		});
	}
	
	private void setStopIconResource(ImageView icon, Stop stop){
		if(stop.isEasyAccessStop() && stop.isHasShelter() && 
				resultObject.isSearchAccessible() && resultObject.isSearchShelter()){
			icon.setImageResource(R.drawable.icn_search_shelters_access);
			icon.setPadding(10, 0, 10, 0);
		} else if(stop.isHasShelter() && resultObject.isSearchShelter()){
			icon.setImageResource(R.drawable.icn_search_shelters);
			icon.setPadding(10, 0, 10, 0);
		} else if(stop.isEasyAccessStop() && resultObject.isSearchAccessible()){
			icon.setImageResource(R.drawable.icn_search_easyaccess);
			icon.setPadding(10, 0, 10, 0);
		} else {
			icon.setImageResource(R.drawable.icn_search_stop);
			icon.setPadding(16, 0, 10, 0);
		}
	}
	
	private void getOutletView(SearchResultsViewHolder viewHolder, final TicketOutlet outlet){
		String name = outlet.getName();
		String desription = getFullAddress(outlet);
		//String distance = getDistance(nearbyStop.getIntDistance());
		setTextData(viewHolder, name, desription, "");
		viewHolder.icon.setImageResource(R.drawable.icn_search_tickets);
		viewHolder.icon.setPadding(10, 0, 10, 0);
		
		viewHolder.view.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				TramTrackerMainActivity.getAppManager().callSelection(outlet.getName());
				goToOutletScreen(outlet);
			}
		});
	}
	
	private void getPOIView(SearchResultsViewHolder viewHolder, final PointOfInterest poi){ 
		String name = poi.getName();
		String desription = poi.getDescription();
		//String distance = getDistance(nearbyStop.getIntDistance());
		setTextData(viewHolder, name, desription, "");
		viewHolder.icon.setImageResource(R.drawable.icn_search_poi);
		viewHolder.icon.setPadding(10, 0, 10, 0);
		
		viewHolder.view.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				TramTrackerMainActivity.getAppManager().callSelection(poi.getName());
				goToPOIScreen(poi);
			}
		});
	}
	
	
	private void setTextData(SearchResultsViewHolder viewHolder, String name, String detail, String distance){
		viewHolder.nameView.setText(name);
		viewHolder.descriptionView.setText(detail);
		viewHolder.distanceView.setText(distance);
	}
	
	private String getStopDescription(Stop stop){
		String text = "";
		
		text = text.concat(String.valueOf(stop.getStopNumber()));
		text = text.concat(context.getResources().getString(R.string.stop_name_colon));
		text = text.concat(stop.getStopName());
		
		return text;
	}
	
	
	private String getRoutesDescription(Stop stop){
		String text = "";
		
		text = context.getResources().getString(R.string.stop_direction_routes);
		String[] array = stop.getRoutes();
		if(array != null){
			for(int i = 0; i < array.length; i++){
				text = text.concat(array[i]);
				if(i < array.length-1){
					text = text.concat(context.getResources().getString(R.string.stop_routes_coma));
				}
			}
			text = text.concat(context.getResources().getString(R.string.stop_name_space));
			text = text.concat(stop.getCityDirection());
		}
		return text;
	}
	
	public String getFullAddress(TicketOutlet outlet){
		String text = "";
		
		text = outlet.getAddress();
//		text = text.concat(context.getResources().getString(R.string.outlet_address_coma));
//		text = text.concat(outlet.getSuburb());
//		text = text.concat(context.getResources().getString(R.string.outlet_address_space));
		return text;
	}
	
	
	private void goToStopScreen(Stop stop){
		Intent intent = new Intent(this.context, StopActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(StopActivity.INTENT_STOP_KEY, stop);
		
		TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.getAppManager().getCurrentTabID(), 
				context.getResources().getString(R.string.tag_stop_screen), intent);
	}
	
	private void goToOutletScreen(TicketOutlet outlet){
		Intent intent = new Intent(this.context, OutletActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(OutletActivity.INTENT_KEY, outlet);
		
		TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.getAppManager().getCurrentTabID(), 
				context.getResources().getString(R.string.tag_outlet_screen), intent);
	}
	
	private void goToPOIScreen(PointOfInterest poi){
		Intent intent = new Intent(this.context, PointOfInterestActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(PointOfInterestActivity.INTENT_KEY, poi);
		
		TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.getAppManager().getCurrentTabID(), 
				context.getResources().getString(R.string.tag_poi_screen), intent);
	}
}
