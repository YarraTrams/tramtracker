package com.yarratrams.tramtracker.ui.util;

import com.google.android.maps.GeoPoint;
import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.TicketOutlet;
import com.yarratrams.tramtracker.ui.OutletActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;

import android.content.Context;
import android.content.Intent;
import android.view.View;


public class MapSearchOutletDialog extends MapDialog{
	private Context context;
	private TicketOutlet outlet;

	public MapSearchOutletDialog(Context context, TicketOutlet outlet) {
		super(context);
		this.context = context;
		this.outlet = outlet;
		
		createDialog();
	}
	
	
	private void createDialog(){
		setTicketOutletInfo();
		
		setOnDirectionsClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				TramTrackerMainActivity.getAppManager().callSelection(context.getString(R.string.accessibility_click_goto_directions));
				GeoPoint destination = new GeoPoint(outlet.getLatitudeE6(), outlet.getLongitudeE6());
				TramTrackerMainActivity.getAppManager().requestDirectionsService(destination);
			}
		});
		setOnGoClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				TramTrackerMainActivity.getAppManager().callSelection(context.getString(R.string.accessibility_click_goto_outlet));
				Intent intent = new Intent(context, OutletActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra(OutletActivity.INTENT_KEY, outlet);
				TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.getAppManager().getCurrentTabID(), 
						context.getResources().getString(R.string.tag_outlet_screen), intent);
			}
		});
	}
	
	
	private void setTicketOutletInfo(){
		setNameText(outlet.getName());
		setDetailText(getFullAddress());
		//setDistanceText(outlet.getSuburb());
	}

	
	public String getFullAddress(){
		String text = "";
		
		text = outlet.getAddress();
//		text = text.concat(context.getResources().getString(R.string.outlet_address_coma));
//		text = text.concat(outlet.getSuburb());
//		text = text.concat(context.getResources().getString(R.string.outlet_address_space));
//		text = text.concat(String.valueOf(outlet.getTicketOutlet().getPostCode()));
		return text;
	}
	
}