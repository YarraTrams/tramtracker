package com.yarratrams.tramtracker.ui.util;

import com.google.android.maps.GeoPoint;
import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.PointOfInterest;
import com.yarratrams.tramtracker.ui.PointOfInterestActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;

import android.content.Context;
import android.content.Intent;
import android.view.View;


public class MapSearchPOIDialog extends MapDialog{
	private Context context;
	private PointOfInterest poi;

	public MapSearchPOIDialog(Context context, PointOfInterest poi) {
		super(context);
		this.context = context;
		this.poi = poi;
		
		createDialog();
	}
	
	
	private void createDialog(){
		setPointOfInterestInfo();
		
		setOnDirectionsClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				TramTrackerMainActivity.getAppManager().callSelection(context.getString(R.string.accessibility_click_goto_directions));
				GeoPoint destination = new GeoPoint((int)(poi.getLatitude()*1E6), (int)(poi.getLongitude()*1E6));
				TramTrackerMainActivity.getAppManager().requestDirectionsService(destination);
			}
		});
		setOnGoClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				TramTrackerMainActivity.getAppManager().callSelection(context.getString(R.string.accessibility_click_goto_poi));
				Intent intent = new Intent(context, PointOfInterestActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra(PointOfInterestActivity.INTENT_KEY, poi);
				TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.getAppManager().getCurrentTabID(), 
						context.getResources().getString(R.string.tag_poi_screen), intent);
			}
		});
	}
	
	
	private void setPointOfInterestInfo(){
		setNameText(poi.getName());
		setDetailText(poi.getDescription());
		//setDistanceText(poi.getOpeningHours());
	}
	
}