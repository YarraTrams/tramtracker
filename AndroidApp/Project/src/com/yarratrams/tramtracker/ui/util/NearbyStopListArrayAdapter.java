package com.yarratrams.tramtracker.ui.util;

import java.text.DecimalFormat;
import java.util.ArrayList;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.NearbyStop;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.ui.NearbyActivity;
import com.yarratrams.tramtracker.ui.PIDActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.ArrayAdapter;
import android.widget.TextView;


public class NearbyStopListArrayAdapter extends ArrayAdapter<NearbyStop> {
	private final NearbyActivity context;
	private final ArrayList<NearbyStop> stops;

	public NearbyStopListArrayAdapter(NearbyActivity context, ArrayList<NearbyStop> stops) {
		super(context, R.layout.nearby_list_view_detail, stops);
		this.context = context;
		this.stops = stops;
	}

	public void updateList(ArrayList<NearbyStop> stops) {
		this.stops.clear();
		this.stops.addAll(stops);
		super.notifyDataSetChanged();
	}
	
	static class StopViewHolder{
		TextView name;
		TextView address;
		TextView distance;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final StopViewHolder viewHolder;
		
		if(convertView == null){
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(R.layout.nearby_list_view_detail, parent, false);
			
			viewHolder = new StopViewHolder();
			viewHolder.name = (TextView)convertView.findViewById(R.id.stop_name);
			viewHolder.address = (TextView)convertView.findViewById(R.id.stop_routes);
			viewHolder.distance = (TextView)convertView.findViewById(R.id.stop_meters);
			convertView.setTag(viewHolder);
		
		} else {
			viewHolder = (StopViewHolder) convertView.getTag();
		}
		
		final NearbyStop nearbyStop = stops.get(position);
		final Stop stop = nearbyStop.getStop();
		
		viewHolder.name.setText(getStopDescription(stop));
		viewHolder.address.setText(getRoutesDescription(stop));
		viewHolder.distance.setText(getDistance(nearbyStop));
		
		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				TramTrackerMainActivity.getAppManager().callSelection(getStopDescription(stop));
				goToPIDScreen(stop);
			}
		});
		
		convertView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				v.setPressed(true);
				
				return false;
			}
		});
		
		return convertView;
	}
	
	
	private String getStopDescription(Stop stop){
		String text = "";
		
		text = text.concat(String.valueOf(stop.getStopNumber()));
		text = text.concat(context.getResources().getString(R.string.stop_name_colon));
		text = text.concat(stop.getStopName());
		
		return text;
	}
	
	
	private String getRoutesDescription(Stop stop){
		String text = "";
		
		text = context.getResources().getString(R.string.stop_direction_routes);
		String[] array = stop.getRoutes();
		if(array != null){
			for(int i = 0; i < array.length; i++){
				text = text.concat(array[i]);
				if(i < array.length-1){
					text = text.concat(context.getResources().getString(R.string.stop_routes_coma));
				}
			}
			text = text.concat(context.getResources().getString(R.string.stop_name_space));
			text = text.concat(stop.getCityDirection());
		}
		return text;
	}
	
	
	private void goToPIDScreen(Stop stop){
		Intent intent = new Intent(context, PIDActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(PIDActivity.INTENT_KEY, stop);
		
		TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_NEARBY, 
				context.getResources().getString(R.string.tag_pid_screen), intent);
	}
	
	
	private String getDistance(NearbyStop stop){
		String text = "";
		
		if(stop.getIntDistance() < 1000){
			text = String.valueOf(stop.getIntDistance());
			text = text.concat(context.getResources().getString(R.string.stop_distance_meters));
		} else {
			double distance = (double)stop.getIntDistance()/1000;
			DecimalFormat df = new DecimalFormat("##.0");
			text = df.format(distance);
			text = text.concat(context.getResources().getString(R.string.stop_distance_kilometers));
		}
		
		return text;
	}
}
