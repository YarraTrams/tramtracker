package com.yarratrams.tramtracker.ui.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.google.android.maps.GeoPoint;
import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.PredictedArrivalTime;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.ui.OnBoardStopActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;

import android.content.Context;
import android.content.Intent;
import android.view.View;


public class MapMyTramDialog extends MapDialog{
	private Context context;
	private PredictedArrivalTime routeStop;
	private ArrayList<PredictedArrivalTime> stops;
	private Stop stop;
	private boolean isCurrentIndex;  

	public MapMyTramDialog(Context context, PredictedArrivalTime routeStop) {
		super(context);
		this.context = context;
		this.routeStop = routeStop;
		this.stop = routeStop.getStop();
		this.stops = new ArrayList<PredictedArrivalTime>();
		this.isCurrentIndex = false;
		
		createDialog();
	}
	
	public MapMyTramDialog(Context context, PredictedArrivalTime routeStop, ArrayList<PredictedArrivalTime> stops, int type) {
		//super(context, type);
		super(context, MapDialog.TYPE_PID);
		this.context = context;
		this.routeStop = routeStop;
		this.stop = routeStop.getStop();
		this.stops = stops;
		this.isCurrentIndex = false;
		
		createDialog();
	}
	
	public MapMyTramDialog(Context context, PredictedArrivalTime routeStop, ArrayList<PredictedArrivalTime> stops, int type, boolean isCurrentIndex) {
		//super(context, type);
		super(context, MapDialog.TYPE_PID);
		this.context = context;
		this.routeStop = routeStop;
		this.stop = routeStop.getStop();
		this.stops = stops;
		this.isCurrentIndex = isCurrentIndex;
		
		createDialog();
	}
	
	
	private void createDialog(){
		setTramStopInfo();
		
		setLeftButtonOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				TramTrackerMainActivity.getAppManager().callSelection(context.getString(R.string.accessibility_click_goto_directions));
				GeoPoint destination = new GeoPoint(stop.getLatitudeE6(), stop.getLongitudeE6());
				TramTrackerMainActivity.getAppManager().requestDirectionsService(destination);
//				if(setAlarm.isChecked()){
//					AlarmManager.startAlarmService(stop, stops,PredictedTimeResultSingleton.getResult().getTram());
//				} else {
//					AlarmManager.stopAlarmService(this);
//				}
			}
		});
		
		setRightButtonOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				TramTrackerMainActivity.getAppManager().callSelection(context.getString(R.string.accessibility_click_goto_stop));
				Intent intent = new Intent(context, OnBoardStopActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra(OnBoardStopActivity.INTENT_KEY, routeStop.getStop());
				intent.putExtra(OnBoardStopActivity.INTENT_KEY_PREDICTED, stops);

				TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MYTRAM, 
						context.getResources().getString(R.string.tag_onboard_stop_screen), intent);
			}
		});

	}
	
	
	private void setTramStopInfo(){
		setNameText(getStopDescription());
		setDetailText(getRoutesDescription());
		setDistanceText(getArrivalTime());
	}
	
	private String getStopDescription(){
		String text = "";
		
		text = text.concat(String.valueOf(stop.getStopNumber()));
		text = text.concat(context.getResources().getString(R.string.stop_name_colon));
		text = text.concat(stop.getStopName());
		
		return text;
	}
	
	
	private String getRoutesDescription(){
		String text = "";
		
		text = context.getResources().getString(R.string.stop_direction_routes);
		String[] array = stop.getRoutes();
		for(int i = 0; i < array.length; i++){
			text = text.concat(array[i]);
			if(i < array.length-1){
				text = text.concat(context.getResources().getString(R.string.stop_routes_coma));
			}
		}
		text = text.concat(context.getResources().getString(R.string.stop_name_space));
		text = text.concat(stop.getCityDirection());
		
		return text;
	}
	

	private String getArrivalTime(){
		String string = "";
		Date date = routeStop.getArrivalTime();
		
		if(isCurrentIndex){
			string = context.getResources().getString(R.string.onboard_current_next);
		} else if(date == null){
			string = context.getResources().getString(R.string.onboard_notime_format);
		} else {
			DateFormat format = new SimpleDateFormat(context.getResources().getString(R.string.onboard_time_format));
			string = format.format(date);
		}
		return string;
	}
	
}