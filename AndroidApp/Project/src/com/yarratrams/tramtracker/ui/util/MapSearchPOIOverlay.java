package com.yarratrams.tramtracker.ui.util;

import java.util.ArrayList;

import com.yarratrams.tramtracker.R;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;
import com.yarratrams.tramtracker.objects.PointOfInterest;

import android.app.Activity;
import android.graphics.drawable.Drawable;


public class MapSearchPOIOverlay extends ItemizedOverlay<OverlayItem> {
	private ArrayList<OverlayItem> overlays = new ArrayList<OverlayItem>();
	private final Activity context;
	final MapController mc;
	
	private ArrayList<PointOfInterest> pois;
	private MapSearchPOIDialog dialog;
	

	private static Drawable getDefaultMarker(Activity context){
		return context.getResources().getDrawable(R.drawable.icn_search_poi);
	}
	
	public MapSearchPOIOverlay(Activity context, MapView mapView) {
		super(boundCenterBottom(getDefaultMarker(context)));
		this.context = context;
		mc = mapView.getController();
		
		pois = new ArrayList<PointOfInterest>();
	}
	
	
	public void updatePointsOfInterestList(ArrayList<PointOfInterest> pois) {
		this.pois.clear();
		this.pois.addAll(pois);
		
		resetOverlays();
		if(dialog != null){
			dialog.dismiss();
		}
		for(PointOfInterest poi : pois){
	        GeoPoint point = new GeoPoint((int)(poi.getLatitude() * 1E6), (int)(poi.getLongitude() * 1E6));
	        OverlayItem overlayitem = new OverlayItem(point, null, null);
	        addOverlay(overlayitem);
		}
	}
	
	
	public void addOverlay(OverlayItem overlay) {
		overlays.add(overlay);
		populate();
	}
	
	public void resetOverlays(){
		overlays.clear();
		populate();
	}

	@Override
	protected OverlayItem createItem(int i) {
		return overlays.get(i);
	}

	@Override
	public int size() {
		return overlays.size();
	}
	
	@Override
	protected boolean onTap(int index) {
		mc.animateTo(createItem(index).getPoint());
		PointOfInterest poi = pois.get(index);
		dialog = new MapSearchPOIDialog(context, poi);
		
		dialog.show();
		return true;
	}
	
}
