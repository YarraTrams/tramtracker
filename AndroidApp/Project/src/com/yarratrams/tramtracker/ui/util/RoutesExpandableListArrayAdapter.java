package com.yarratrams.tramtracker.ui.util;

import java.util.ArrayList;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.Route;
import com.yarratrams.tramtracker.ui.CityCircleActivity;
import com.yarratrams.tramtracker.ui.RoutesActivity;
import com.yarratrams.tramtracker.ui.RoutesEntryActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;


public class RoutesExpandableListArrayAdapter extends BaseExpandableListAdapter {
	private RoutesEntryActivity context;
	private ArrayList<Route> routes;
	
	public RoutesExpandableListArrayAdapter(RoutesEntryActivity context, ArrayList<Route> routes) {
		super();
		this.context = context;
		this.routes = routes;
	}

	public void updateList(ArrayList<Route> routes) {
		this.routes.clear();
		this.routes.addAll(routes);
		super.notifyDataSetChanged();
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		if(childPosition == 0){
			return routes.get(groupPosition).getUpDestination();
		}
		return routes.get(groupPosition).getDownDestination();
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return (groupPosition * 10) + childPosition;
	}

	
	static class DestinationViewHolder{
		TextView routeDestination;
	}
	
	@Override
	public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
		DestinationViewHolder viewHolder;
		
		if(convertView == null){
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(R.layout.routes_list_view_child, parent, false);
			
			viewHolder = new DestinationViewHolder();
			viewHolder.routeDestination = (TextView)convertView.findViewById(R.id.route_destination);
			convertView.setTag(viewHolder);
			
		} else {
			viewHolder = (DestinationViewHolder) convertView.getTag();
		}
		
		final Route route = (Route) getGroup(groupPosition);
		final String destination = (String) getChild(groupPosition, childPosition);
		
		if(route.getRouteNumber().equals("35")){
			viewHolder.routeDestination.setText(context.getResources().getString(R.string.routes_entry_destination_citycircle));
		} else {
			viewHolder.routeDestination.setText(getRouteDestination(destination));
		}
		
		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				TramTrackerMainActivity.getAppManager().callSelection(getDescription(route, destination));
				goToRouteScreen(route, childPosition);
			}
		});
		
		convertView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				v.setPressed(true);
				
				return false;
			}
		});
		
		return convertView;
	}
	
	private String getDescription(Route route, String destination){
		String description = "";
		
		description = route.getRouteNumber();
		description = description.concat(context.getString(R.string.route_filter_space));
		description = description.concat(destination);
		
		return description;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		Route route = routes.get(groupPosition);
		int count = 0;
		if(route.getUpDestination() != null){
			if(route.getUpDestination().length() > 0){
				count++;
			}
		}
		if(route.getDownDestination() != null){
			if(route.getDownDestination().length() > 0){
				count++;
			}
		}
		return count;
	}

	@Override
	public Object getGroup(int groupPosition) {
		return routes.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return routes.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	static class RouteViewHolder{
		TextView routeName;
	}
	
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		RouteViewHolder viewHolder;
		
		if(convertView == null){
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(R.layout.routes_list_view_group, parent, false);
		
			viewHolder = new RouteViewHolder();
			viewHolder.routeName = (TextView) convertView.findViewById(R.id.group_name);
			convertView.setTag(viewHolder);
			
		} else {
			viewHolder = (RouteViewHolder) convertView.getTag();
		}

		Route route = (Route) getGroup(groupPosition);
		viewHolder.routeName.setText(getRouteName(route));
		
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

    @Override
    public boolean areAllItemsEnabled(){
        return true;
    }
	
    
	private void goToRouteScreen(Route route, int position){
		if(route.getRouteNumber().equals("35")){
			Intent intent = new Intent(this.context, CityCircleActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_ROUTES, 
					context.getResources().getString(R.string.tag_routes_citycircle_screen), intent);
			
		} else {
			if(position == 0){
				route.setIsUpDestination(true);
			} else {
				route.setIsUpDestination(false);
			}
			Intent intent = new Intent(this.context, RoutesActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.putExtra(RoutesActivity.INTENT_KEY, route);
			
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_ROUTES, 
					context.getResources().getString(R.string.tag_routes_screen), intent);
		}
	}
	
	
	private String getRouteDestination(String destination){
		String text = "";
		
		text = context.getResources().getString(R.string.routes_entry_destination_to);
		text = text.concat(destination);
		
		return text;
	}
	
	
	private String getRouteName(Route route){
		String text = "";
		
		text = route.getRouteNumber();
		text = text.concat(context.getResources().getString(R.string.routes_entry_name_dash));
		if(route.getUpDestination() != null){
			text = text.concat(route.getUpDestination());
		}
		if(route.getDownDestination() != null){
			if(!route.getDownDestination().trim().equals("")){
				text = text.concat(context.getResources().getString(R.string.routes_entry_name_to));
				text = text.concat(route.getDownDestination());
			}
		}
		
		return text;
	}

}
