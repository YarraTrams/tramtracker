package com.yarratrams.tramtracker.ui.util;

import java.util.ArrayList;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.ServiceDisruption;
import com.yarratrams.tramtracker.ui.DisruptionHelpActivity;
import com.yarratrams.tramtracker.ui.OnBoardActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class OnBoardDisruptionAlertArrayAdapter extends ArrayAdapter<ServiceDisruption> {
	private final OnBoardActivity context;
	private final ArrayList<ServiceDisruption> disruptions;

	public OnBoardDisruptionAlertArrayAdapter(OnBoardActivity context, ArrayList<ServiceDisruption> disruptions) {
		super(context, R.layout.pid_disruptions_detail, disruptions);
		this.context = context;
		this.disruptions = disruptions;
	}

	static class DisruptionViewHolder{
		TextView routeNumber;
		TextView disruptionDescription;
		ImageView disruptionGOTO;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		DisruptionViewHolder viewHolder;
		
		if(convertView == null){
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(R.layout.pid_disruptions_detail, parent, false);
			
			viewHolder = new DisruptionViewHolder();
			viewHolder.routeNumber = (TextView) convertView.findViewById(R.id.disruption_route_number);
			viewHolder.disruptionDescription = (TextView) convertView.findViewById(R.id.disruption_route_description);
			viewHolder.disruptionGOTO = (ImageView) convertView.findViewById(R.id.disruption_goto);
			convertView.setTag(viewHolder);
			
		} else {
			viewHolder = (DisruptionViewHolder) convertView.getTag();
		}
		
		final ServiceDisruption disruption = disruptions.get(position);
		if(disruption.isText()){
			viewHolder.routeNumber.setVisibility(View.GONE);
		} else {
			viewHolder.routeNumber.setVisibility(View.VISIBLE);
			viewHolder.routeNumber.setText(String.valueOf(disruption.getRouteNumber()));
		}
		if(disruption.isHasAdditionalInfoOnWebsite()){
			viewHolder.disruptionGOTO.setVisibility(View.VISIBLE);
			viewHolder.disruptionGOTO.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					TramTrackerMainActivity.getAppManager().callSelection(context.getString(R.string.accessibility_click_goto_disruptionhelp));
					Intent intent = new Intent(context, DisruptionHelpActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					
					TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.getAppManager().getCurrentTabID(), 
							context.getResources().getString(R.string.tag_disruptionhelp_screen), intent);
				}
			});
		} else {
			viewHolder.disruptionGOTO.setVisibility(View.GONE);
		}
		
		viewHolder.disruptionDescription.setText(disruption.getDisruptionDescription());
		
		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				context.switchDisruptionAlertState();
			}
		});
		
		return convertView;
	}
}
