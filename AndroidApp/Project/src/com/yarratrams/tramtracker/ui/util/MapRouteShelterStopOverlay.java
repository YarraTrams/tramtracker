package com.yarratrams.tramtracker.ui.util;

import com.yarratrams.tramtracker.R;
import com.google.android.maps.MapView;

import android.app.Activity;
import android.graphics.drawable.Drawable;


public class MapRouteShelterStopOverlay extends MapRouteStopOverlay {

	private static Drawable getDefaultMarker(Activity context){
		return context.getResources().getDrawable(R.drawable.icn_search_shelters);
	}
	
	public MapRouteShelterStopOverlay(Activity context, MapView mapView) {
		super(getDefaultMarker(context), context, mapView);
	}
	
}
