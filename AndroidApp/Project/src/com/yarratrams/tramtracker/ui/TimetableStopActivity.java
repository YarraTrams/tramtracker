package com.yarratrams.tramtracker.ui;


import java.util.ArrayList;

import com.google.android.maps.GeoPoint;
import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.db.FavouriteManager;
import com.yarratrams.tramtracker.db.POIManager;
import com.yarratrams.tramtracker.objects.PointOfInterest;
import com.yarratrams.tramtracker.objects.Route;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.ui.util.MenuTimetableStopAdapter;
import com.yarratrams.tramtracker.ui.util.PointsOfInterestListAdapter;
import com.yarratrams.tramtracker.ui.util.WrappingSlidingDrawer;

import android.app.Activity;
import android.app.ActivityGroup;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class TimetableStopActivity extends Activity implements OnClickListener{
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_MORE;
	public static final String INTENT_ROUTE_KEY = "route_info";
	public static final String INTENT_STOP_KEY = "stop_info";
	
	private Stop stop;
	private Route route;
	
	private ArrayList<PointOfInterest> pois;
	private LinearLayout poiList; 
	
	private WrappingSlidingDrawer slidingMenu;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.timetable_stop_screen);
		
		Button gotoTimetable = (Button) findViewById(R.id.timetable_button);
		gotoTimetable.setOnClickListener(this);
		
		poiList = (LinearLayout) findViewById(R.id.simple_list);
		
		TextView footer = (TextView) findViewById(R.id.stop_footer);
		footer.setMovementMethod(LinkMovementMethod.getInstance());
		
		slidingMenu = (WrappingSlidingDrawer) findViewById(R.id.sliding_menu);
        GridView menuContainer = (GridView) findViewById(R.id.sliding_menu_container);
        MenuTimetableStopAdapter menuAdapter = new MenuTimetableStopAdapter(this);
        menuContainer.setAdapter(menuAdapter);
	}


	
	private void setStopInfo(){
		Button gotoTimetable = (Button) findViewById(R.id.timetable_button);
		TextView stopName = (TextView) findViewById(R.id.stop_name);
		TextView connectingTrain = (TextView) findViewById(R.id.connecting_train);
		TextView connectingTram = (TextView) findViewById(R.id.connecting_tram);
		TextView connectingBus = (TextView) findViewById(R.id.connecting_bus);
		
		String connectingTrainText = getConnectingService(stop.getConnectingTrains());
		String connectingTramText = getConnectingService(stop.getConnectingTrams());
		String connectingBusText = getConnectingService(stop.getConnectingBuses());
		
		stopName.setText(getStopDescription());
		connectingTrain.setText(connectingTrainText);
		connectingTram.setText(connectingTramText);
		connectingBus.setText(connectingBusText);
		
		if(pois.size() == 0){
			RelativeLayout poiTitle = (RelativeLayout) findViewById(R.id.poi_title);
			poiTitle.setVisibility(View.GONE);
		}
		
		if(stop.getTrackerID() >= 8000){
			gotoTimetable.setVisibility(View.INVISIBLE);
		}
	}
	
	private String getConnectingService(String service){
		if(service != null){
			if(!service.trim().equals("")){
				return service;
			}
		}
		
		return getResources().getString(R.string.stop_empty_service);
	}
	
	private String getStopDescription(){
		String text = "";
		
		text = text.concat(String.valueOf(stop.getStopNumber()));
		text = text.concat(getResources().getString(R.string.stop_name_colon));
		text = text.concat(stop.getStopName());
		
		return text;
	}
	

	@Override
	public void onClick(View v) {
		Button gotoTimetable = (Button) findViewById(R.id.timetable_button);
		if(v == gotoTimetable){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_timetable_seetimetable));
			Intent intent = new Intent(this, TimetableActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.putExtra(TimetableActivity.INTENT_STOP_KEY, stop);
			intent.putExtra(TimetableActivity.INTENT_ROUTE_KEY, route);
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, 
					getResources().getString(R.string.tag_timetable_screen), intent);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		Bundle extras = getIntent().getExtras(); 
		if(extras != null){
			stop = extras.getParcelable(INTENT_STOP_KEY);
			route = extras.getParcelable(INTENT_ROUTE_KEY);
		}
		
		pois = POIManager.getPOIForStop(stop.getTrackerID(), this);
		if(pois == null){
			pois = new ArrayList<PointOfInterest>();
		}
		new PointsOfInterestListAdapter(poiList, this, pois);
		
		setStopInfo();
	}


	
	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
//		MenuInflater inflater = getMenuInflater();
//		inflater.inflate(R.menu.stop_menu, menu);
//
//		TramTrackerMainActivity.getAppManager().setMenuBackground(this);
		return true;
	}
	
	@Override
	public void openOptionsMenu() {
		slidingMenu.toggle();
	}
	@Override
	public void closeOptionsMenu() {
		slidingMenu.close();
	}
	
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_MENU){
			openOptionsMenu();
			return true;
		} else if(keyCode == KeyEvent.KEYCODE_BACK){
			onBackPressed();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		ActivityGroup group;
		Intent intent;
		switch (item.getItemId()) {
			case R.id.menu_stop_favourites :
				if(stop.getTrackerID() >= 8000){
					TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.notify_favourites_terminus));
					return true;
				}
				
				FavouriteManager favouriteManager = new FavouriteManager(getApplicationContext());
				if(favouriteManager.isStopFavourite(stop.getTrackerID())){
					TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.error_favourite_added));
					
				} else {
					group = TramTrackerMainActivity.getAppManager().requestDifferentTab(TramTrackerMainActivity.TAB_FAVOURITES);
					intent = new Intent(group, AddFavouriteActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.putExtra(AddFavouriteActivity.INTENT_KEY_STOP, stop);
					TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_FAVOURITES, getResources().getString(R.string.tag_addfavourite_screen), intent);
				}
				return true;
				
			case R.id.menu_stop_outlets :
				intent = new Intent(this, NearbyActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra(NearbyActivity.INTENT_CENTRE_KEY, stop);
				TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.getAppManager().getCurrentTabID(), getResources().getString(R.string.tag_nearby_screen), intent);
				return true;
				
			case R.id.menu_stop_directions :
				GeoPoint destination = new GeoPoint(stop.getLatitudeE6(), stop.getLongitudeE6());
				TramTrackerMainActivity.getAppManager().requestDirectionsService(destination);
				return true;
				
			default :
				return super.onOptionsItemSelected(item);
		}
	}
	
	public void runFavouriteStopMenuOption(){
		closeOptionsMenu();
		if(stop.getTrackerID() >= 8000){
			TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.notify_favourites_terminus));
			return;
		}
		
		FavouriteManager favouriteManager = new FavouriteManager(getApplicationContext());
		if(favouriteManager.isStopFavourite(stop.getTrackerID())){
			TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.error_favourite_added));
			
		} else {
			ActivityGroup group = TramTrackerMainActivity.getAppManager().requestDifferentTab(TramTrackerMainActivity.TAB_FAVOURITES);
			Intent intent = new Intent(group, AddFavouriteActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.putExtra(AddFavouriteActivity.INTENT_KEY_STOP, stop);
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_FAVOURITES, getResources().getString(R.string.tag_addfavourite_screen), intent);
		}
	}
	public void runOutletsMenuOption(){
		closeOptionsMenu();
		Intent intent = new Intent(this, NearbyActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(NearbyActivity.INTENT_CENTRE_KEY, stop);
		TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.getAppManager().getCurrentTabID(), getResources().getString(R.string.tag_nearby_screen), intent);
	}
	public void runDirectionsMenuOption(){
		closeOptionsMenu();
		GeoPoint destination = new GeoPoint(stop.getLatitudeE6(), stop.getLongitudeE6());
		TramTrackerMainActivity.getAppManager().requestDirectionsService(destination);
	}
	
	
}
