package com.yarratrams.tramtracker.webserviceinteraction;

import java.util.ArrayList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.yarratrams.tramtracker.objects.ServiceChangeRSSItem;

public class ServiceChangeRSS extends DefaultHandler{
	ArrayList<ServiceChangeRSSItem> alServiceChange;
	StringBuffer stringBufferData = new StringBuffer();
	ServiceChangeRSSItem serviceChangeRSSItem = null;
	
	public ArrayList<ServiceChangeRSSItem> getArrayListNews() {
		return alServiceChange;
	}
	
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		// use the qualified name if we have one
		if (qName != null && !qName.equals("")) 
			localName = qName;
//		System.out.println("localName = " + localName);
		// a channel is the outer body
		if (localName.equalsIgnoreCase("channel")){
			alServiceChange = new ArrayList<ServiceChangeRSSItem>();
		}
		
		// is it an item?
		else if (localName.equalsIgnoreCase("item")){
			serviceChangeRSSItem = new ServiceChangeRSSItem();
		// the other properties are all just text (for now)
		} 
		
		else if (localName.equalsIgnoreCase("title") || localName.equalsIgnoreCase("description") || localName.equalsIgnoreCase("encoded")  || 
				localName.equalsIgnoreCase("content:encoded")){
			if(stringBufferData.length() > 0)
				stringBufferData.delete(0, stringBufferData.length());
		}
	}
	
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		// use the qualified name if we have one
		if (qName != null && !qName.equals(""))  
			localName = qName;

		// found a title?
		if (localName.equalsIgnoreCase("title"))
		{
			if(serviceChangeRSSItem != null){
				// does the title have a date component?
				int lastIndex = stringBufferData.lastIndexOf("-");
				if(lastIndex != -1){
					serviceChangeRSSItem.setDate(stringBufferData.substring(lastIndex + 2));
					stringBufferData.delete(lastIndex - 1, stringBufferData.length());
				}
				serviceChangeRSSItem.setTitle(stringBufferData.toString());
			}
			if(stringBufferData.length() > 0)
				stringBufferData.delete(0, stringBufferData.length());
		}

		// found the description?
		else if (localName.equalsIgnoreCase("description"))
		{
			if(serviceChangeRSSItem != null){
				if (serviceChangeRSSItem.getContent() == null || serviceChangeRSSItem.getContent().length() == 0)
					serviceChangeRSSItem.setContent(stringBufferData.toString());
				if(stringBufferData.length() > 0)
					stringBufferData.delete(0, stringBufferData.length());
			}
		}
		
		// found some content?
		else if (localName.equalsIgnoreCase("encoded") || localName.equalsIgnoreCase("content:encoded"))
		{
//			System.out.println("stringBufferData = " + stringBufferData);
			serviceChangeRSSItem.setContent(stringBufferData.toString());
			if(stringBufferData.length() > 0)
				stringBufferData.delete(0, stringBufferData.length());
		}
		
		// finished a service change?
		else if (localName.equalsIgnoreCase("item"))
		{
			alServiceChange.add(serviceChangeRSSItem);
		}
		
		// finished channel?
		else if (localName.equalsIgnoreCase("channel"))
		{
			
		}
	}
	
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		stringBufferData.append(String.valueOf(ch).substring(start, length));
	}
}
