package com.yarratrams.tramtracker.webserviceinteraction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.format.DateFormat;
import android.util.Log;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.db.TTDB;
import com.yarratrams.tramtracker.objects.Constants;
import com.yarratrams.tramtracker.objects.PredictedArrivalTime;
import com.yarratrams.tramtracker.objects.PredictedTimeResult;
import com.yarratrams.tramtracker.objects.Route;
import com.yarratrams.tramtracker.objects.ServiceChange;
import com.yarratrams.tramtracker.objects.ServiceDisruption;
import com.yarratrams.tramtracker.objects.ServiceInfo;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.objects.Tram;
import com.yarratrams.tramtracker.singleton.PredictedTimeResultSingleton;
import com.yarratrams.tramtracker.singleton.TTDBSingleton;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;


public class TTWebService {


	
	private static String strServerResponseTimeString;
	
//	private static String message;
//	
//	private Runnable toaster = new Runnable() {
//		public void run() {
//			TramTrackerMainActivity.getAppManager().displayErrorMessage(message);
//		}
//	};

	/**
	 * Check if connection is available
	 * @return
	 */
	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager 
		= (ConnectivityManager) TramTrackerMainActivity.getAppManager().getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}


	/**
	 *  Download data from JSON service
	 */
	private InputStream getJSONData(String url) {
        HttpParams httpParameters = new BasicHttpParams();
        //Set time out in milliseconds
        int timeoutConnection = 120000;
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
        int timeoutSocket = 120000;
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
        URI uri;
        InputStream data = null;
        if(!isNetworkAvailable()){
//        	Log.e("getJSONData","!isNetworkAvailable()");
        	TramTrackerMainActivity ttMain = TramTrackerMainActivity.getAppManager();
			Message message = new Message();
			Bundle bundle = new Bundle();
			bundle.putString("error", ttMain.getResources().getString(R.string.error_internet_unavailable));
			message.setData(bundle);
        	ttMain.handlerToast.sendMessage(message);
        	
//        	ttMain.displayErrorMessage(ttMain.getResources().getString(R.string.error_internet_unavailable));
        	PredictedTimeResultSingleton.errorType = PredictedTimeResultSingleton.kErrorNoConnection;
        }
        else{
        	try {
        		uri = new URI(url);
        		HttpGet method = new HttpGet(uri);
        		HttpResponse response;
        		response = httpClient.execute(method);
        		data = response.getEntity().getContent();
        		PredictedTimeResultSingleton.errorType = PredictedTimeResultSingleton.kErrorNone;
        	} catch (URISyntaxException e) {
        		e.printStackTrace();
        	} catch (ClientProtocolException e) {
        		e.printStackTrace();
        	} catch (IOException e) {
//    			TramTrackerMainActivity ttMain = TramTrackerMainActivity.getAppManager();
//    			ttMain.displayErrorMessage(ttMain.getResources().getString(R.string.error_internet_timeout));
    			PredictedTimeResultSingleton.errorType = PredictedTimeResultSingleton.kErrorTimeOut;       		
        		if(e.getClass() == SocketTimeoutException.class){
//        			Log.e("getJSONData","SocketTimeoutException");
                	TramTrackerMainActivity ttMain = TramTrackerMainActivity.getAppManager();
        			Message message = new Message();
        			Bundle bundle = new Bundle();
        			bundle.putString("error", ttMain.getResources().getString(R.string.error_internet_timeout));
        			message.setData(bundle);
                	ttMain.handlerToast.sendMessage(message);
//        			TramTrackerMainActivity ttMain = TramTrackerMainActivity.getAppManager();
//        			ttMain.displayErrorMessage(ttMain.getResources().getString(R.string.error_internet_timeout));
        			PredictedTimeResultSingleton.errorType = PredictedTimeResultSingleton.kErrorTimeOut;
        		}
        	} 
        }
        return data;
    }
	
	
//	/**
//	 *  Download data from JSON service
//	 */
//	private InputStream getJSONDataPredictedArrival(String url) {
//        HttpParams httpParameters = new BasicHttpParams();
//        //Set time out in milliseconds
//        int timeoutConnection = 120000;
//        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
//        int timeoutSocket = 120000;
//        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
//        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
//        URI uri;
//        InputStream data = null;
//        if(!isNetworkAvailable()){
//        	TramTrackerMainActivity ttMain = TramTrackerMainActivity.getAppManager();
//        	ttMain.displayErrorMessage(ttMain.getResources().getString(R.string.error_internet_unavailable));
//        }
//        else{
//        	try {
//        		uri = new URI(url);
//        		HttpGet method = new HttpGet(uri);
//        		HttpResponse response;
//        		response = httpClient.execute(method);
//        		data = response.getEntity().getContent();
//        	} catch (URISyntaxException e) {
//        		e.printStackTrace();
//        	} catch (ClientProtocolException e) {
//        		e.printStackTrace();
//        	} catch (IOException e) {
//        		if(e.getClass() == SocketTimeoutException.class){
//        			TramTrackerMainActivity ttMain = TramTrackerMainActivity.getAppManager();
//        			ttMain.displayErrorMessage(ttMain.getResources().getString(R.string.error_internet_timeout));
//        		}
//        	} 
//        }
//        return data;
//    }
	
    private JSONArray getResponseArray(InputStream is) {
    	String timeRequested,timeResponded;
    	JSONArray responseObject = null;
    	JSONObject serviceData = parseJSONStream(is);
        try {
        	if (serviceData.getBoolean("hasError") || !serviceData.getBoolean("hasResponse")){
        		if(serviceData.getBoolean("hasError"))
        			displayErrorMessage(serviceData.getString("errorMessage"));
        		else
        			displayErrorMessage(Constants.kErrorHasResponseFalse);
			} 
        	else{
        		if(serviceData.get("timeRequested") != JSONObject.NULL){
        			timeRequested = serviceData.getString("timeRequested");
        		}
        		if(serviceData.get("timeResponded") != JSONObject.NULL){
        			timeResponded = serviceData.getString("timeResponded");
        			strServerResponseTimeString = timeResponded;
        		}				
        		Object object = serviceData.get("responseObject");
				if(object.getClass() == JSONArray.class)
					responseObject = (JSONArray)object;
				else
					return null;
        	}
		} catch (JSONException e) {
			e.printStackTrace();
		}
        
        return responseObject;
    }
    
    private JSONObject getResponseObject(InputStream is) {
    	
    	JSONObject responseObject = null;
    	JSONObject serviceData = parseJSONStream(is);
        try {
        	if (!serviceData.getBoolean("hasError") && serviceData.getBoolean("hasResponse")){
				Object object = serviceData.get("responseObject");
				if(object.getClass() == JSONObject.class)
					responseObject = (JSONObject)object;
				else
					return null;
			} 
        	else{
        		if(serviceData.getBoolean("hasError"))
        			displayErrorMessage(serviceData.getString("errorMessage"));
        		else
        			displayErrorMessage(Constants.kErrorHasResponseFalse);
        	}
		} catch (JSONException e) {
			e.printStackTrace();
		}
        
        return responseObject;
    }
    
    /**
     * Parse InputStream into JSONObject.
     */
    private static JSONObject parseJSONStream(InputStream is) {
    	JSONObject jsonObject = null;
    	try {
    		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
    		StringBuilder sb = new StringBuilder();
    		String line = null;
    		while ((line = reader.readLine()) != null) {
    			sb.append(line);
    		}
    		is.close();
    		String jsonData = sb.toString();
//            System.out.println("JSON Response: " + jsonData);
            jsonObject = new JSONObject(jsonData);
    	} 
    	catch(Exception e){
    		e.printStackTrace();
    	}
		return jsonObject;
    }
    
    
	/**
	 * Convert JSON date to java.util.Date
	 * @param jsonDate String expected format = \/Date(1334560248000+1000)\/
	 * @return java.util.Date
	 */
	public static Date jsonDateToDate(String jsonDate)
	{
	    //  "/Date(1321867151710)/"
	    int idx1 = jsonDate.indexOf("(");
	    int idx2 = jsonDate.indexOf("+");
	    String s = jsonDate.substring(idx1+1, idx2);
	    long l = Long.valueOf(s);
	    return new Date(l);
	}
    
    /**
     * Convert JSONobject to Stop 
     * @return Stop
     */
    private static void jsonToStop(Stop stop,JSONObject jSonObject){
    	try {
	        String stopName = jSonObject.getString ("StopName");
	        String cityDirection = jSonObject.getString("CityDirection");
	        String flagStopNo = jSonObject.getString("FlagStopNo");
			stop.setStopName(stopName);
			stop.setCityDirection(cityDirection);
			stop.setStopNumber(flagStopNo);
			stop.setCityStop(jSonObject.getBoolean("IsCityStop"));
			stop.setPlatformStop(jSonObject.getBoolean("IsPlatformStop"));
			stop.setLatitudeE6((int)(jSonObject.getDouble("Latitude")/1E6));
			stop.setLongitudeE6((int)(jSonObject.getDouble("Longitude")/1E6));
			stop.setZone(jSonObject.getString("Zones"));
    	}
		catch (Exception e) {
			e.printStackTrace();
		} 
    }
    
    /**
     * Convert JSONobject to Stop 
     * @return Stop
     */
    private String jSonToDeviceToken(JSONObject jSonObject){
    	try {
	        String deviceToken = jSonObject.getString ("DeviceToken");
	    	return deviceToken;
    	}
		catch (Exception e) {
			e.printStackTrace();
		} 
    	return null;
    }
	
    /**
     * Convert JSONArray to ArrayList of Trams for next predicted trams
     * @return ArrayList<Tram>
     */
    private static ArrayList<Tram> jsonToTrams(Route route, JSONArray jSonArray, Date serverReferenceTime){
    	try {
    		ArrayList<Tram> alTram = new ArrayList<Tram>();
    		Tram tram;
    		JSONObject jsonObject;
    		for(int i=0; i<jSonArray.length() ; i++){
    			jsonObject = (JSONObject)jSonArray.get(i);
    			
//    			int tripID = jSonObject.getInt("TripID");
    			int vehicleNo = jsonObject.getInt("VehicleNo");
    			tram = new Tram();
    			String destination = jsonObject.getString("Destination");

    			//	        JSONObject objDisruption = jSonObject.getJSONObject("DisruptionMessage");
    			boolean isLowFloorTram = jsonObject.getBoolean("IsLowFloorTram");
    			boolean isAirConditioned = jsonObject.getBoolean("AirConditioned");
    			boolean displayAC = jsonObject.getBoolean("DisplayAC");
    			//    			boolean isTTAvailable = jSonObject.getBoolean("IsTTAvailable");
    			boolean hasDisruption = jsonObject.getBoolean("HasDisruption");
    			boolean hasSpecialEvent = jsonObject.getBoolean("HasSpecialEvent");
    			String specialEventMessage = jsonObject.getString("SpecialEventMessage");
//    			if(specialEventMessage != null && specialEventMessage != JSONObject.NULL && !specialEventMessage.equals("") && !specialEventMessage.equalsIgnoreCase("null"))
//    				hasSpecialEvent = true;
    			Date datePredictedArrival = jsonDateToDate(jsonObject.getString("PredictedArrivalDateTime"));	
    			if(datePredictedArrival != JSONObject.NULL){
    				long timeInMils = datePredictedArrival.getTime() - serverReferenceTime.getTime();
    				tram.setArrivalTime(datePredictedArrival);
    				int minutes = (int)((int)timeInMils/(1000 * 60));
    				tram.setMinutesFromServerResponseTime(minutes);
    			}
    			if(hasDisruption){
    				JSONObject disruptionMessage = jsonObject.getJSONObject("DisruptionMessage");
    				if(disruptionMessage != JSONObject.NULL){
    					JSONArray messages = disruptionMessage.getJSONArray("Messages");
    					if(messages != JSONObject.NULL){
    						JSONObject eachMessage;
    						for(int j=0;j < messages.length();j++){
    							eachMessage = (JSONObject)messages.get(j);
    							if(eachMessage != JSONObject.NULL){
    								tram.setDisrruptionMessage(eachMessage.getString("Message"));
    							}
    						}
    					}
    				}
    			}
    			tram.setDisrupted(hasDisruption);
   				tram.setHasAirCon(isAirConditioned && displayAC);
    			tram.setLowFloor(isLowFloorTram);
    			tram.setVehicleNumber(vehicleNo);
    			tram.setHasSpecialEvent(hasSpecialEvent);
    			tram.setSpecialEventMessage(specialEventMessage);
    			int j=0;
    			Tram tempTram;
    			for(;j < alTram.size(); j++){
    				tempTram = alTram.get(j);
    				if(tram.getArrivalTime().before(tempTram.getArrivalTime()))
    					break;
    			}
    			alTram.add(j,tram);
//    			System.out.println("destination = " + destination);
//    			System.out.println("route.getUpDestination() = " + route.getUpDestination());
//    			System.out.println("route.getDownDestination() = " + route.getDownDestination());

    			if(destination.equalsIgnoreCase(route.getUpDestination())){
    				route.setIsUpDestination(true);
    			}
    			else
    				route.setIsUpDestination(false);
    			route.setUpDestination(destination);
    			route.setDownDestination(destination);
//    			route.setMainRouteNo(jsonObject.getString("InternalRouteNo"));
    			route.setMainRouteNo(jsonObject.getString("RouteNo"));
    			StringBuffer routeNo = new StringBuffer(jsonObject.getString("RouteNo"));
        		if(routeNo.charAt(routeNo.length() - 1) == 'a' || routeNo.charAt(routeNo.length() - 1) == 'b' 
        				||routeNo.charAt(routeNo.length() - 1) == 'c' || routeNo.charAt(routeNo.length() - 1) == 'd'){
        			route.setMainRoute(false);
        			route.setMainRouteNo(routeNo.substring(0, routeNo.length() - 1));
        		}
//    			route.setUpDestination("set up from webservice");
//    			route.setDownDestination("set down from webservice");
    		}
    		return alTram;
    	}
		catch (Exception e) {
			e.printStackTrace();
		} 
    	return null;
    }
    
    /**
     * Convert JSONArray to ArrayList of Trams for scheduled trams
     * @return ArrayList<Tram>
     */
    private ArrayList<Tram> jsonToTramsForSchedules(Route route, JSONArray jSonArray, Date serverReferenceTime){
    	try {
    		ArrayList<Tram> alTram = new ArrayList<Tram>();
    		Tram tram;
    		JSONObject jsonObject;
    		for(int i=0; i<jSonArray.length() ; i++){
    			jsonObject = (JSONObject)jSonArray.get(i);
    			
//    			int tripID = jSonObject.getInt("TripID");
    			int vehicleNo = jsonObject.getInt("VehicleNo");
    			tram = new Tram();
    			String destination = jsonObject.getString("Destination");

    			//	        JSONObject objDisruption = jSonObject.getJSONObject("DisruptionMessage");
    			boolean isLowFloorTram = jsonObject.getBoolean("IsLowFloorTram");
    			boolean isAirConditioned = jsonObject.getBoolean("AirConditioned");
    			boolean displayAC = jsonObject.getBoolean("DisplayAC");
    			//    			boolean isTTAvailable = jSonObject.getBoolean("IsTTAvailable");
    			boolean hasDisruption = jsonObject.getBoolean("HasDisruption");
    			boolean hasSpecialEvent = jsonObject.getBoolean("HasSpecialEvent");
    			String specialEventMessage = jsonObject.getString("SpecialEventMessage");
//    			if(specialEventMessage != null && specialEventMessage != JSONObject.NULL && !specialEventMessage.equals("") && !specialEventMessage.equalsIgnoreCase("null"))
//    				hasSpecialEvent = true;
//    			Date currentTime = new Date();
    			Date datePredictedArrival = jsonDateToDate(jsonObject.getString("PredictedArrivalDateTime"));	
   				tram.setArrivalTime(datePredictedArrival);
    			if(hasDisruption){
    				JSONObject disruptionMessage = jsonObject.getJSONObject("DisruptionMessage");
    				if(disruptionMessage != JSONObject.NULL){
    					JSONArray messages = disruptionMessage.getJSONArray("Messages");
    					if(messages != JSONObject.NULL){
    						JSONObject eachMessage;
    						for(int j=0;j < messages.length();j++){
    							eachMessage = (JSONObject)messages.get(j);
    							if(eachMessage != JSONObject.NULL){
    								tram.setDisrruptionMessage(eachMessage.getString("Message"));
    							}
    						}
    					}
    				}
    			}
    			tram.setDisrupted(hasDisruption);
    			tram.setHasAirCon(isAirConditioned && displayAC);
    			tram.setLowFloor(isLowFloorTram);
    			tram.setVehicleNumber(vehicleNo);
    			tram.setHasSpecialEvent(hasSpecialEvent);
    			tram.setSpecialEventMessage(specialEventMessage);
    			int j=0;
    			Tram tempTram;
    			for(;j < alTram.size(); j++){
    				tempTram = alTram.get(j);
    				if(tram.getArrivalTime().before(tempTram.getArrivalTime()))
    					break;
    			}
    			alTram.add(j,tram);
//    			System.out.println("destination = " + destination);
//    			System.out.println("route.getUpDestination() = " + route.getUpDestination());
//    			System.out.println("route.getDownDestination() = " + route.getDownDestination());

    			if(destination.equalsIgnoreCase(route.getUpDestination())){
    				route.setIsUpDestination(true);
    			}
    			else
    				route.setIsUpDestination(false);
    		}
    		return alTram;
    	}
		catch (Exception e) {
			e.printStackTrace();
		} 
    	return null;
    }
    
    /**
     * Convert JSONArray to ArrayList of Trams for next predicted trams, called when the 1st, 4th,7th... NextPredictedArrivaTime array element is encountered 
     * 
     */
    private void jsonToRoute(JSONObject jsonObject, Date serverReferenceTime, Route route, Tram tram){
    	try {
    		String destination = jsonObject.getString("Destination");
    		StringBuffer routeNo = new StringBuffer(jsonObject.getString("RouteNo"));
    		String headboardRouteNo = jsonObject.getString("HeadBoardRouteNo");
    		route.setRouteNumber(headboardRouteNo);
//    		route.setMainRouteNo(jsonObject.getString("InternalRouteNo"));
    		route.setMainRouteNo(routeNo.toString());
    		if(routeNo.charAt(routeNo.length() - 1) == 'a' || routeNo.charAt(routeNo.length() - 1) == 'b' 
    				||routeNo.charAt(routeNo.length() - 1) == 'c' || routeNo.charAt(routeNo.length() - 1) == 'd'){
    			route.setMainRoute(false);
    			route.setMainRouteNo(routeNo.substring(0, routeNo.length() - 1));
    		}
    		if(jsonObject.getString("RouteNo").equalsIgnoreCase("3a")){
    			route.setMainRouteNo(jsonObject.getString("InternalRouteNo"));
    		}
    		route.setUpDestination(destination);
    		route.setDownDestination(destination);
//    		route.setUpDestination("Set from web service");
//    		route.setDownDestination("Set from web service");
    		int vehicleNo = jsonObject.getInt("VehicleNo");
    		tram.setVehicleNumber(vehicleNo);
    		
    		//added for web service change
    		tram.setHeadboardNo(headboardRouteNo);
    		tram.setDestination(destination);
    		tram.setInternalRouteNo(jsonObject.getInt("InternalRouteNo"));

//	        JSONObject objDisruption = jSonObject.getJSONObject("DisruptionMessage");
    		boolean isLowFloorTram = jsonObject.getBoolean("IsLowFloorTram");
    		boolean isAirConditioned = jsonObject.getBoolean("AirConditioned");
    		boolean displayAC = jsonObject.getBoolean("DisplayAC");
//    			boolean isTTAvailable = jSonObject.getBoolean("IsTTAvailable");
    		boolean hasDisruption = jsonObject.getBoolean("HasDisruption");
    		boolean hasSpecialEvent = jsonObject.getBoolean("HasSpecialEvent");
    		String specialEventMessage = jsonObject.getString("SpecialEventMessage");
//    		if(specialEventMessage != null && specialEventMessage != JSONObject.NULL && !specialEventMessage.equals("") && !specialEventMessage.equalsIgnoreCase("null"))
//    			hasSpecialEvent = true;
    		Date datePredictedArrival = jsonDateToDate(jsonObject.getString("PredictedArrivalDateTime"));	
    		if(datePredictedArrival != JSONObject.NULL){
    			long timeInMils = datePredictedArrival.getTime() - serverReferenceTime.getTime();
    			tram.setArrivalTime(datePredictedArrival);
    			int minutes = (int)((int)timeInMils/(1000 * 60));
    			tram.setMinutesFromServerResponseTime(minutes);
    		}
    		if(hasDisruption){
    			JSONObject disruptionMessage = jsonObject.getJSONObject("DisruptionMessage");
    			if(disruptionMessage != JSONObject.NULL){
    				JSONArray messages = disruptionMessage.getJSONArray("Messages");
    				if(messages != JSONObject.NULL){
    					JSONObject eachMessage;
    					for(int j=0;j < messages.length();j++){
    						eachMessage = (JSONObject)messages.get(j);
    						if(eachMessage != JSONObject.NULL){
    							tram.setDisrruptionMessage(eachMessage.getString("Message"));
    						}
    					}
    				}
    			}
    		}
    		tram.setDisrupted(hasDisruption);
   			tram.setHasAirCon(isAirConditioned && displayAC);
    		tram.setLowFloor(isLowFloorTram);
    		tram.setVehicleNumber(vehicleNo);
    		tram.setHasSpecialEvent(hasSpecialEvent);
    		tram.setSpecialEventMessage(specialEventMessage);
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	} 
    }
    
    
    /**
     * Convert JSONArray to ArrayList of Trams for next predicted trams, called when the 2nd, 3rd,5th, 6th... NextPredictedArrivaTime array elements are encountered 
     * 
     */
    private void jsonToRoute(JSONObject jsonObject, Date serverReferenceTime, Tram tram){
    	try {
    		String destination = jsonObject.getString("Destination");
    		String headboardRouteNo = jsonObject.getString("HeadBoardRouteNo");
    		int vehicleNo = jsonObject.getInt("VehicleNo");
    		tram.setVehicleNumber(vehicleNo);
    		
    		//added for web service change
    		tram.setHeadboardNo(headboardRouteNo);
    		tram.setDestination(destination);
    		tram.setInternalRouteNo(jsonObject.getInt("InternalRouteNo"));

//	        JSONObject objDisruption = jSonObject.getJSONObject("DisruptionMessage");
    		boolean isLowFloorTram = jsonObject.getBoolean("IsLowFloorTram");
    		boolean isAirConditioned = jsonObject.getBoolean("AirConditioned");
    		boolean displayAC = jsonObject.getBoolean("DisplayAC");
//    			boolean isTTAvailable = jSonObject.getBoolean("IsTTAvailable");
    		boolean hasDisruption = jsonObject.getBoolean("HasDisruption");
    		boolean hasSpecialEvent = jsonObject.getBoolean("HasSpecialEvent");
    		String specialEventMessage = jsonObject.getString("SpecialEventMessage");
//    		if(specialEventMessage != null && specialEventMessage != JSONObject.NULL && !specialEventMessage.equals("") && !specialEventMessage.equalsIgnoreCase("null"))
//    			hasSpecialEvent = true;
    		Date datePredictedArrival = jsonDateToDate(jsonObject.getString("PredictedArrivalDateTime"));	
    		if(datePredictedArrival != JSONObject.NULL){
    			long timeInMils = datePredictedArrival.getTime() - serverReferenceTime.getTime();
    			tram.setArrivalTime(datePredictedArrival);
    			int minutes = (int)((int)timeInMils/(1000 * 60));
    			tram.setMinutesFromServerResponseTime(minutes);
    		}
    		if(hasDisruption){
    			JSONObject disruptionMessage = jsonObject.getJSONObject("DisruptionMessage");
    			if(disruptionMessage != JSONObject.NULL){
    				JSONArray messages = disruptionMessage.getJSONArray("Messages");
    				if(messages != JSONObject.NULL){
    					JSONObject eachMessage;
    					for(int j=0;j < messages.length();j++){
    						eachMessage = (JSONObject)messages.get(j);
    						if(eachMessage != JSONObject.NULL){
    							tram.setDisrruptionMessage(eachMessage.getString("Message"));
    						}
    					}
    				}
    			}
    		}
    		tram.setDisrupted(hasDisruption);
   			tram.setHasAirCon(isAirConditioned && displayAC);
    		tram.setLowFloor(isLowFloorTram);
    		tram.setVehicleNumber(vehicleNo);
    		tram.setHasSpecialEvent(hasSpecialEvent);
    		tram.setSpecialEventMessage(specialEventMessage);
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	} 
    }
  
    /**
     * Convert JSONArray to Tram for scheduled tram departure(GetSchedulesCollection)
     * @return Tram
     * Unused variables in result listed below
     *     	    			 "BaseTimestamp":"\/Date(1336572000000+1000)\/",
    			"Destination":"Box Hill",
    			"DisplayAC":false,
    			"HeadBoardRouteNo":"109",
    			"InternalRouteNo":109,
    			"IsTTAvailable":true,
    			"PredictedArrivalDateTime":"\/Date(1336572960000+1000)\/",
    			"RequestDateTime":"\/Date(1336572000000+1000)\/",
    			"RouteNo":"109",
    			"SpecialEventMessage":"",
    			"TripID":29678233,		
     */
    private static ArrayList<Tram> jsonToTrams(JSONArray jSonArray){
    	try {
    		ArrayList<Tram> alTram = new ArrayList<Tram>();
    		Tram tram;
    		JSONObject jSonObject;
    		for(int i=0; i<jSonArray.length() ; i++){
    			jSonObject = (JSONObject)jSonArray.get(i);
    			tram = new Tram();
//    			int tripID = jSonObject.getInt("TripID");
    			int vehicleNo = jSonObject.getInt("VehicleNo");
//    			String destination = jSonObject.getString("Destination");
    			//	        JSONObject objDisruption = jSonObject.getJSONObject("DisruptionMessage");
    			boolean isLowFloorTram = jSonObject.getBoolean("IsLowFloorTram");
    			boolean isAirConditioned = jSonObject.getBoolean("AirConditioned");
    			boolean displayAC = jSonObject.getBoolean("DisplayAC");
//    			boolean isTTAvailable = jSonObject.getBoolean("IsTTAvailable");
    			boolean hasDisruption = jSonObject.getBoolean("HasDisruption");
    			boolean hasSpecialEvent = jSonObject.getBoolean("HasSpecialEvent");
    			String specialEventMessage = jSonObject.getString("SpecialEventMessage");
//    			if(specialEventMessage != null && specialEventMessage != JSONObject.NULL && !specialEventMessage.equals("") && !specialEventMessage.equalsIgnoreCase("null"))
//    				hasSpecialEvent = true;
    			
    			//TODO minutesfromserverresponsetime
    			Date datePredictedArrival = jsonDateToDate(jSonObject.getString("PredictedArrivalDateTime"));	
    			if(datePredictedArrival != JSONObject.NULL)
    				tram.setArrivalTime(datePredictedArrival);
    			tram.setDisrupted(hasDisruption);
    			if(isAirConditioned && displayAC)
    				tram.setHasAirCon(isAirConditioned);
    			tram.setLowFloor(isLowFloorTram);
    			tram.setVehicleNumber(vehicleNo);
    			tram.setHasSpecialEvent(hasSpecialEvent);
    			tram.setSpecialEventMessage(specialEventMessage);
    			int j=0;
    			Tram tempTram;
    			for(;j < alTram.size(); j++){
    				tempTram = alTram.get(j);
    				if(tram.getArrivalTime().before(tempTram.getArrivalTime()))
    					break;
    			}
    			alTram.add(j,tram);
    		}
    		return alTram;
    	}
		catch (Exception e) {
			e.printStackTrace();
		} 
    	return null;
    }
    
    /**
     * Convert JSONobject to Service Change 
     * @return ArrayList<ServiceChange>
     */
    private static void jSonToServiceChange(ArrayList<ServiceChange> alServiceChange, JSONArray jSonArray){
    	try {
    		ServiceChange serviceChange;
    		JSONObject jSonObject;
    		boolean flag = true;
    		Set<String> setSpecialEventMessage = new HashSet<String>();
    		for(int i=0; i<jSonArray.length() ; i++){
    			jSonObject = (JSONObject)jSonArray.get(i);
    			serviceChange = new ServiceChange();
//    			boolean hasSpecialEvent = jSonObject.getBoolean("HasSpecialEvent");
    			String specialEventMessage = jSonObject.getString("SpecialEventMessage");
    			if(specialEventMessage != null && !specialEventMessage.equalsIgnoreCase("null") && !specialEventMessage.equals("") &&
    					!setSpecialEventMessage.contains(specialEventMessage)){
        			serviceChange.setChangeDescription(specialEventMessage);
        			setSpecialEventMessage.add(specialEventMessage);
        			serviceChange.setRouteNumber(jSonObject.getString("RouteNo"));
        			for(ServiceChange change : alServiceChange){
        				if(change.getRouteNumber().equalsIgnoreCase(serviceChange.getRouteNumber())){
        					flag = false;
        					break;
        				}

        			}
    			}
    			if(flag)
    				alServiceChange.add(serviceChange);
    			flag = true;
    		}
    	}
		catch (Exception e) {
			e.printStackTrace();
		} 
    }
    
    
    public static <T extends Comparable<? super T>> List<T> asSortedList(Collection<T> c) {
      List<T> list = new ArrayList<T>(c);
      java.util.Collections.sort(list);
      return list;
    }
    
    public TreeSet<Integer> sortRoutes(Set<String> setRoutes){
    	TreeSet<Integer> treeSet = new TreeSet<Integer>();
    	TTDB ttdb = TTDBSingleton.getInstance(TramTrackerMainActivity.getAppManager());
    	for(String route : setRoutes){
   			treeSet.add(Integer.valueOf(ttdb.getInternalRouteNoForRouteNo(route)));
    	}
    	return treeSet;
    }

    
    /**
     * Convert JSONobject to Service Disruption 
     * @return ArrayList<ServiceDisruption>
     */
    private void jSonToServiceDisruption(ArrayList<ServiceDisruption> alServiceDisruption,JSONArray jSonArray){
    	try {
    		ServiceDisruption serviceDisruption;
    		Set<String> setRoutes = new HashSet<String>();
    		JSONObject jsonObject;
    		boolean flag = true;
    		for(int i=0; i<jSonArray.length() ; i++){
    			jsonObject = (JSONObject)jSonArray.get(i);
    			serviceDisruption = new ServiceDisruption();
//    			Log.e("jSonToServiceDisruption", "jSonObject = " + jSonObject);
    			boolean hasDisruption = jsonObject.getBoolean("HasDisruption");
    			if(hasDisruption){
    				JSONObject disruptionMessage = jsonObject.getJSONObject("DisruptionMessage");
//    				Log.e("jSonToServiceDisruption", "disruptionMessage = " + disruptionMessage);
    				if(disruptionMessage != JSONObject.NULL){
    					int messageCount = disruptionMessage.getInt("MessageCount");
    					if(messageCount == 0){
    						if(alServiceDisruption.size() == 0){
    							alServiceDisruption.add(serviceDisruption);
    							serviceDisruption.setText(true);
    							setRoutes.add(jsonObject.getString("HeadBoardRouteNo"));
    							if(jSonArray.length() == 1)
    								serviceDisruption.setDisruptionDescription(Constants.kErrorGenericDisruptionSinglePrefix + jsonObject.getString("HeadBoardRouteNo") + Constants.kErrorGenericDisruptionSingleSuffix);
    							else{
    								StringBuffer sb = new StringBuffer();
    								setRoutes.add(jsonObject.getString("HeadBoardRouteNo"));
    								if(setRoutes.size() > 1)
    									sb.append(Constants.kErrorGenericDisruptionMultiplePrefix);
    								else
    									sb.append(Constants.kErrorGenericDisruptionSinglePrefix);
    								//Sort routes
    								TreeSet<Integer> sortedSet = sortRoutes(setRoutes);
//    								System.out.println("sortedSet = " + sortedSet);
    								TTDB ttdb = TTDBSingleton.getInstance(TramTrackerMainActivity.getAppManager());
    								for(Integer intRTemp : sortedSet){
    									sb.append(ttdb.getRouteNoForInternalRouteNo(intRTemp) + ", ");
    								}
//    								System.out.println("sb.substring(sb.length() - 2, sb.length()) = \"" + sb.substring(sb.length() - 2, sb.length()) + "\", sb.length() - 2 = " + (sb.length() - 2)
//    										);
    								if(sb.substring(sb.length() - 2, sb.length()).equalsIgnoreCase(", "))
    									sb.delete(sb.length() - 2, sb.length());
    								if(setRoutes.size() > 1)
    									sb.append(Constants.kErrorGenericDisruptionMultipleSuffix);
    								else
    									sb.append(Constants.kErrorGenericDisruptionSingleSuffix);
    								serviceDisruption.setDisruptionDescription(sb.toString());
    							}
    						}
    						else{
    							serviceDisruption = alServiceDisruption.get(0);
								StringBuffer sb = new StringBuffer();
								setRoutes.add(jsonObject.getString("HeadBoardRouteNo"));
								if(setRoutes.size() > 1)
									sb.append(Constants.kErrorGenericDisruptionMultiplePrefix);
								else
									sb.append(Constants.kErrorGenericDisruptionSinglePrefix);
								//Sort routes
								TreeSet<Integer> sortedSet = sortRoutes(setRoutes);
//								System.out.println("sortedSet = " + sortedSet);
								TTDB ttdb = TTDBSingleton.getInstance(TramTrackerMainActivity.getAppManager());
								for(Integer intRTemp : sortedSet){
									sb.append(ttdb.getRouteNoForInternalRouteNo(intRTemp) + ", ");
								}
//								System.out.println("sb.substring(sb.length() - 2, sb.length()) = \"" + sb.substring(sb.length() - 2, sb.length()) + "\", sb.length() - 2 = " + (sb.length() - 2)
//										);
								if(sb.substring(sb.length() - 2, sb.length()).equalsIgnoreCase(", "))
									sb.delete(sb.length() - 2, sb.length());								
								if(setRoutes.size() > 1)
									sb.append(Constants.kErrorGenericDisruptionMultipleSuffix);
								else
									sb.append(Constants.kErrorGenericDisruptionSingleSuffix);
								serviceDisruption.setDisruptionDescription(sb.toString());
    						}
    						serviceDisruption.setRouteNumber(jsonObject.getString("HeadBoardRouteNo"));
    					}
    					else if(messageCount > 0){
    						if(disruptionMessage.getString("DisplayType").equalsIgnoreCase("Text")){
    							JSONArray messages = disruptionMessage.getJSONArray("Messages");
    							if(messages != JSONObject.NULL){
    								JSONObject eachMessage;
    								for(int j=0;j < messages.length();j++){
    									eachMessage = (JSONObject)messages.get(j);
    									if(eachMessage != JSONObject.NULL){
    										serviceDisruption.setText(true);
    										serviceDisruption.setRouteNumber(eachMessage.getString("Route"));
    										serviceDisruption.setDisruptionDescription(eachMessage.getString("Message"));
    										if(eachMessage.has("AdditionalInfoOnWebsite")){
    											serviceDisruption.setHasAdditionalInfoOnWebsite(eachMessage.getBoolean("AdditionalInfoOnWebsite"));
    											serviceDisruption.setMessageId(eachMessage.getString("MessageId"));
    										}
    										else{
    											serviceDisruption.setHasAdditionalInfoOnWebsite(false);
    											
    										}
//    										for(ServiceDisruption disruption : alServiceDisruption){
//    											if(disruption.getRouteNumber().equalsIgnoreCase(serviceDisruption.getRouteNumber())){
//    												flag = false;
//    												break;
//    											}
//    										}
//    										if(flag)
    										alServiceDisruption.add(serviceDisruption);
//    										flag = true;
    									}
    								}
    								break;
    							}
    						}
    						else if(disruptionMessage.getString("DisplayType").equalsIgnoreCase("Tabular")){
    							JSONArray messages = disruptionMessage.getJSONArray("Messages");
//    							Log.e("jSonToServiceDisruption", "messages = " + messages);
    							if(messages != JSONObject.NULL){
    								JSONObject eachMessage;
    								for(int j=0;j < messages.length();j++){
    									eachMessage = messages.getJSONObject(j);
//    									Log.e("jSonToServiceDisruption", "eachMessage = " + eachMessage);
    									serviceDisruption.setText(false);
    									serviceDisruption.setDisruptionDescription(eachMessage.getString("Message"));
    									if(eachMessage != JSONObject.NULL){
    										serviceDisruption.setRouteNumber(eachMessage.getString("Route"));
        									if(eachMessage.has("AdditionalInfoOnWebsite")){
        										serviceDisruption.setHasAdditionalInfoOnWebsite(eachMessage.getBoolean("AdditionalInfoOnWebsite"));
        										serviceDisruption.setMessageId(eachMessage.getString("MessageId"));
        									}
        									else{
        										serviceDisruption.setHasAdditionalInfoOnWebsite(false);
        									}
//    										for(ServiceDisruption disruption : alServiceDisruption){
//    											if(disruption.getRouteNumber().equalsIgnoreCase(serviceDisruption.getRouteNumber())){
//    												flag = false;
//    												break;
//    											}
//    										}
//    										if(flag)
    										alServiceDisruption.add(serviceDisruption);
    										serviceDisruption = new ServiceDisruption();
//    										flag = true;
    									}
    								}
    								break;
    							}
    						}
    					}
    				}
    			}
    		}
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	} 
//    	System.out.println("alServiceDisruption = " + alServiceDisruption);
    }
    
	/**
	 * Get tram stop given TramTracker ID
	 */
	public void modifyStopInformation(Stop stop){
		try {
			String url = Constants.strURLBase + "GetStopInformation/" + stop.getTrackerID() + Constants.strAID + Constants.token;
			InputStream jsonData = getJSONData(url);
			JSONArray jSonArray = getResponseArray(jsonData);
			if(jSonArray != null){
				JSONObject jSonObject = (JSONObject)jSonArray.get(0);
				jsonToStop(stop,jSonObject);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Uses jsonDateToDate for json string to date conversion
	 */
	
	private PredictedArrivalTime jsonToPredictedArrivalTime(JSONObject object){
		try {
			if(object.getString("PredictedArrivalDateTime") != JSONObject.NULL){
				Date date = jsonDateToDate(object.getString("PredictedArrivalDateTime"));
				Stop stop = new Stop();
				stop.setTrackerID(object.getInt("StopNo"));
				PredictedArrivalTime predictedArrivalTime = new PredictedArrivalTime(date, stop);
				return predictedArrivalTime;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Get tram stop times given Tram Number
	 */
	public PredictedTimeResult getNextPredictedArrivalTimeAtStopsForTram(Tram tram){
			String url = Constants.strURLBase + "GetNextPredictedArrivalTimeAtStopsForTramNo/" + tram.getVehicleNumber() + Constants.strAID + Constants.token;
			ArrayList<PredictedArrivalTime> predictedArrivalTimes;
			Set<String> setServiceDisruption = new HashSet<String>();
			try {
				InputStream jsonData = getJSONData(url);
				if(jsonData != null){
					JSONObject serviceData = parseJSONStream(jsonData);
					//				PredictedTimeResultSingleton.setResult(null);
					if (!serviceData.getBoolean("hasError")){
						if (serviceData.getBoolean("hasResponse")){
							if(serviceData.get("responseObject") != JSONObject.NULL){
								JSONObject object = serviceData.getJSONObject("responseObject");
								JSONArray array = object.getJSONArray("NextPredictedStopsDetails");
								boolean isUpDirection = object.getBoolean("Up");
								String routeNo = object.getString("HeadBoardRouteNo");
								PredictedTimeResultSingleton.setAvailable(object.getBoolean("Available"));
								PredictedTimeResultSingleton.setAtLayover(object.getBoolean("AtLayover"));

								if(array != null){
									PredictedArrivalTime predictedArrivalTime;
									if(array.length() > 0){
										predictedArrivalTimes = new ArrayList<PredictedArrivalTime>();
//										System.out.println("array.length() = " + array.length());
										for(int i=0;i<array.length(); i++){
											object = array.getJSONObject(i);
											predictedArrivalTime = jsonToPredictedArrivalTime(object);
											if(predictedArrivalTime != null){
												if(predictedArrivalTime.getStop().getTrackerID() > 0)
													predictedArrivalTimes.add(predictedArrivalTime);
											}
										}
										PredictedTimeResult ptr = new PredictedTimeResult();
										ptr.setAlPredictedArrivalTime(predictedArrivalTimes);
										ptr.setUpDirection(isUpDirection);
										Route route = new Route();
										route.setIsUpDestination(isUpDirection);
										route.setRouteNumber(routeNo);
//										route.setMainRouteNo(object.getString("RouteNo"));
										ptr.setRoute(route);
										ptr.setTram(tram);
										ServiceDisruption serviceDisruption = new ServiceDisruption();
										//								System.out.println("Object = " + object);
										JSONObject responseObject = serviceData.getJSONObject("responseObject");
										if(responseObject.getBoolean("HasDisruption")){
											serviceDisruption.setText(true);
											if(responseObject.get("DisruptionMessage") != null && responseObject.get("DisruptionMessage") != JSONObject.NULL){
												JSONObject disruptionObject = responseObject.getJSONObject("DisruptionMessage");
												if(disruptionObject.getBoolean("AdditionalInfoOnWebsite"))
													serviceDisruption.setMessageId(disruptionObject.getString("MessageId"));
												serviceDisruption.setDisruptionDescription(disruptionObject.getString("Message"));
											}
											else{
												serviceDisruption.setDisruptionDescription("Service disrupted. Delays may occur.");
											}
											serviceDisruption.setRouteNumber(routeNo);
											serviceDisruption.setHasAdditionalInfoOnWebsite(false);
											ptr.setHasDisruption(true);
											ptr.setServiceDisruption(serviceDisruption);
										}
										else{
											ptr.setHasDisruption(false);
											ptr.setServiceDisruption(null);
										}
										String changeDescription = responseObject.getString("SpecialEventMessage");
										if(changeDescription != null && !changeDescription.equalsIgnoreCase("null") && !changeDescription.equals("")
												&& !setServiceDisruption.contains(changeDescription)){
											ServiceChange change = new ServiceChange();
											change.setChangeDescription(changeDescription);
											setServiceDisruption.add(changeDescription);
											change.setRouteNumber(responseObject.getString("RouteNo"));
											if(responseObject.getBoolean("HasSpecialEvent")){
												ptr.setHasServiceChange(true);
											}
											ptr.setServiceChange(change);
										}

//										System.out.println("ptr.getServiceChange() = " + ptr.getServiceChange());
										return ptr;
									}
								}
							}
							else{
								Message message = new Message();
								Bundle bundle = new Bundle();
								bundle.putString("error", Constants.kErrorTTNotAvailableForTram);
								message.setData(bundle);
								TramTrackerMainActivity.getAppManager().handlerToast.sendMessage(message); 							
							}
						}
						//hasresponse false
						else{
							Message message = new Message();
							Bundle bundle = new Bundle();
							bundle.putString("error", Constants.kErrorInvalidTram);
							message.setData(bundle);
							TramTrackerMainActivity.getAppManager().handlerToast.sendMessage(message); 						
						}
					}
					else{
						Message message = new Message();
						Bundle bundle = new Bundle();
						bundle.putString("error", serviceData.getString("errorMessage"));
						message.setData(bundle);
						TramTrackerMainActivity.getAppManager().handlerToast.sendMessage(message); 
					}
				} 
			} catch (JSONException e) {
				e.printStackTrace();
			}catch (Exception e) {
				e.printStackTrace();
			}
		
		return null;
	}
	
	public void displayErrorMessage(String strMessage){
		Message message = new Message();
		Bundle bundle = new Bundle();
		bundle.putString("error", strMessage);
		message.setData(bundle);
		TramTrackerMainActivity.getAppManager().handlerToast.sendMessage(message); 
	}
	
	/**
	 * Get device token
	 */
	public void getDeviceToken(){
		try {
			TramTrackerMainActivity ttmain = TramTrackerMainActivity.getAppManager();
			SharedPreferences appSharedPrefs = ttmain.getSharedPreferences(Constants.kSharedPrefIdentifier, Activity.MODE_PRIVATE);
			if(appSharedPrefs.getString(Constants.kKeyDeviceInfo, null) == null){
				String url = Constants.strURLBase + "GetDeviceToken" + Constants.strAID + "&devInfo=" + URLEncoder.encode(android.os.Build.MODEL, "UTF-8") 
						+ "," + URLEncoder.encode(android.os.Build.PRODUCT, "UTF-8") + "," + URLEncoder.encode(android.os.Build.VERSION.RELEASE, "UTF-8");
//				System.out.println("url = " + url);
				InputStream jsonData = getJSONData(url);
				JSONArray responseArray = getResponseArray(jsonData);
				Editor prefsEditor = appSharedPrefs.edit();
				if(responseArray.getJSONObject(0) != null && responseArray.getJSONObject(0).getString("DeviceToken") != null 
						&& responseArray.getJSONObject(0).getString("DeviceToken") != JSONObject.NULL){
					prefsEditor.putString(Constants.kKeyDeviceInfo, responseArray.getJSONObject(0).getString("DeviceToken"));
					prefsEditor.commit();
//					System.out.println("appSharedPrefs.getString(Constants.kKeyDeviceInfo, 3e982c6b-d57a-43fe-bff7-a44e9adb9e1e) = " 
//							+ appSharedPrefs.getString(Constants.kKeyDeviceInfo, "3e982c6b-d57a-43fe-bff7-a44e9adb9e1e"));
					Constants.token = "&tkn=" + appSharedPrefs.getString(Constants.kKeyDeviceInfo, "3e982c6b-d57a-43fe-bff7-a44e9adb9e1e");
				}
			}
			else{
				Constants.token = "&tkn=" + appSharedPrefs.getString(Constants.kKeyDeviceInfo, "3e982c6b-d57a-43fe-bff7-a44e9adb9e1e");
			}
//			System.out.println("Constants.token = " + Constants.token);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//	/**
//	 * Get next arrival times at stops for trams, routes that don't have next services should be stripped?
//	 */
//	public ServiceInfo getNextPredictedRoutesCollection(Stop stop, ArrayList<Route> alRoutes, boolean lowFloor){
//		try {
//			ArrayList<Tram> alTrams = new ArrayList<Tram>();
//			ArrayList<ServiceChange> alServiceChange = new ArrayList<ServiceChange>();
//			ArrayList<ServiceDisruption> alServiceDisruption = new ArrayList<ServiceDisruption>();
//			JSONArray jsonArray = null;
//			boolean isTTAvailable = true;
//			ServiceInfo serviceInfo = new ServiceInfo();
//			Route route = alRoutes.get(0);
//			String url = Constants.strURLBase + "GetNextPredictedRoutesCollection/" + stop.getTrackerID() + "/" + route.getMainRouteNo() 
//					+ "/" + lowFloor + Constants.strAID + Constants.strCID + Constants.token;
//			System.out.println("getNextPredictedRoutesCollection url = " + url);
//			InputStream jsonData = getJSONData(url);
//			String timeRequested,timeResponded;
//			JSONObject serviceData = parseJSONStream(jsonData);
//			if (serviceData.getBoolean("hasError") || !serviceData.getBoolean("hasResponse")){
//				if(serviceData.getBoolean("hasError")){
//					serviceInfo.setHasEmptyResponse(false);
//					displayErrorMessage(serviceData.getString("errorMessage"));
//				}
//				else{
//					serviceInfo.setHasEmptyResponse(true);
//					if(!lowFloor)
//						displayErrorMessage(Constants.kErrorHasResponseFalse);
//					else
//						displayErrorMessage(Constants.kErrorHasResponseFalseLowTram);
//				}
//			} 
//			else{
//				if(serviceData.get("timeRequested") != JSONObject.NULL){
//					timeRequested = serviceData.getString("timeRequested");
//				}
//				if(serviceData.get("timeResponded") != JSONObject.NULL){
//					timeResponded = serviceData.getString("timeResponded");
//					strServerResponseTimeString = timeResponded;
//				}				
//				Object object = serviceData.get("responseObject");
//				if(object.getClass() == JSONArray.class)
//					jsonArray = (JSONArray)object;
//			}
//			System.out.println("jsonArray = " + jsonArray);
//
//			Date serverResponseDate = jsonDateToDate(strServerResponseTimeString);
//			if(jsonArray != null){
//				serviceInfo.setHasEmptyResponse(false);
//				JSONObject firstObject = jsonArray.getJSONObject(0);
//				if(!firstObject.getBoolean("IsTTAvailable")){
//					isTTAvailable = false;
//					serviceInfo.setHasEmptyResponse(false);
//				}
//				serviceInfo.setServerReferenceDate(serverResponseDate);
//				alTrams = jsonToTrams(route,jsonArray, serverResponseDate);
//				if(alTrams != null){
//					if(alTrams.size() > 0){
//						Log.d("getNextPredictedRoutesCollection","alTrams.size() > 0");
//						route.setNextServices(alTrams);
//					}
//					jSonToServiceChange(alServiceChange, jsonArray);
//					jSonToServiceDisruption(alServiceDisruption, jsonArray);
//				}
//			}
//			serviceInfo.setRoutes(alRoutes);
//			if(!isTTAvailable){
//				serviceInfo.setHasEmptyResponse(false);
//				serviceInfo.setRoutes(new ArrayList<Route>());
//				displayErrorMessage(Constants.kErrorTTNotAvailable);
//			}
//			if(jsonArray == null){
//				serviceInfo.setHasEmptyResponse(true);
//				serviceInfo.setRoutes(new ArrayList<Route>());
//    			if(!lowFloor)
//    				displayErrorMessage(Constants.kErrorHasResponseFalse);
//    			else
//    				displayErrorMessage(Constants.kErrorHasResponseFalseLowTram);
//			}
//			if(alServiceChange.size() > 0){
//				serviceInfo.setServiceChanges(alServiceChange);
//			}
//			if(alServiceDisruption.size() > 0)
//				serviceInfo.setServiceDisruptions(alServiceDisruption);
//			return serviceInfo;
//		} 
//		catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}
	
//	/**
//	 * Get next arrival times at stops for trams, routes that don't have next services should be stripped?
//	 */
//	public ServiceInfo getNextPredictedRoutesCollection(Stop stop, ArrayList<Route> alRoutes, boolean lowFloor){
//		try {
//			ArrayList<ServiceChange> alServiceChange = new ArrayList<ServiceChange>();
//			ArrayList<ServiceDisruption> alServiceDisruption = new ArrayList<ServiceDisruption>();
//			JSONArray jsonArray = null;
//			boolean isTTAvailable = true;;
//			ServiceInfo serviceInfo = new ServiceInfo();
//			String url = Constants.strURLBase + "GetNextPredictedRoutesCollection/" + stop.getTrackerID() + "/" + alRoutes.get(0).getMainRouteNo()  
//					+ "/" + lowFloor + Constants.strAID + Constants.strCID + Constants.token;
//			InputStream jsonData = getJSONData(url);
//			alRoutes.clear();
//			if(jsonData != null){
//		    	String timeRequested,timeResponded;
//		    	JSONObject serviceData = parseJSONStream(jsonData);
//		    	if (serviceData.getBoolean("hasError") || !serviceData.getBoolean("hasResponse")){
//		    		if(serviceData.getBoolean("hasError")){
//		    			serviceInfo.setHasEmptyResponse(false);
//		    			displayErrorMessage(serviceData.getString("errorMessage"));
//		    		}
//		    		else{
//		    			serviceInfo.setHasEmptyResponse(true);
//		    			if(!lowFloor)
//		    				displayErrorMessage(Constants.kErrorHasResponseFalse);
//		    			else
//		    				displayErrorMessage(Constants.kErrorHasResponseFalseLowTram);
//		    		}
//		    	} 
//		    	else{
//		    		if(serviceData.get("timeRequested") != JSONObject.NULL){
//		    			timeRequested = serviceData.getString("timeRequested");
//		    		}
//		    		if(serviceData.get("timeResponded") != JSONObject.NULL){
//		    			timeResponded = serviceData.getString("timeResponded");
//		    			strServerResponseTimeString = timeResponded;
//		    		}				
//		    		Object object = serviceData.get("responseObject");
//		    		if(object.getClass() == JSONArray.class)
//		    			jsonArray = (JSONArray)object;
//		    	}
////				System.out.println("jsonArray = " + jsonArray);
//				if(jsonArray != null){
//					serviceInfo.setHasEmptyResponse(false);
//					Date serverResponseDate = jsonDateToDate(strServerResponseTimeString);
//					serviceInfo.setServerReferenceDate(serverResponseDate);
//					JSONObject object;
//					Route route;
//					Tram tram;
//					for(int i = 0; i< jsonArray.length(); i++){
//						object = jsonArray.getJSONObject(i);
//						if(!object.getBoolean("IsTTAvailable")){
//							serviceInfo.setHasEmptyResponse(false);
//							isTTAvailable = false;
//							break;
//						}
//						route = new Route();
//						tram = new Tram();
//						jsonToRoute(object, serverResponseDate, route, tram);
//						if(alRoutes.size() == 0){
//							ArrayList<Tram> alTrams = new ArrayList<Tram>();
//							alTrams.add(tram);
//							route.setNextServices(alTrams);
//							alRoutes.add(route);
//						}
//						else{
//							Route r = getRouteWithDestination(route.getDestination(), route.getRouteNumber(), alRoutes);
//							if(r != null){
//								ArrayList<Tram> alTrams = r.getNextServices();
//								alTrams.add(tram);
//							}
//							else{
//								ArrayList<Tram> alTrams = new ArrayList<Tram>();
//								alTrams.add(tram);
//								route.setNextServices(alTrams);
//								alRoutes.add(route);
//							}
//						}
//					}
//					jSonToServiceChange(alServiceChange, jsonArray);
//					jSonToServiceDisruption(alServiceDisruption, jsonArray);
//					serviceInfo.setRoutes(alRoutes);
//					if(!isTTAvailable){
//						serviceInfo.setHasEmptyResponse(false);
//						serviceInfo.setRoutes(new ArrayList<Route>());
//						displayErrorMessage(Constants.kErrorTTNotAvailable);
//					}
//					serviceInfo.setServiceChanges(alServiceChange);
//					serviceInfo.setServiceDisruptions(alServiceDisruption);
//				}
//				if(jsonArray == null){
//					serviceInfo.setHasEmptyResponse(true);
//					serviceInfo.setRoutes(new ArrayList<Route>());
//	    			if(!lowFloor)
//	    				displayErrorMessage(Constants.kErrorHasResponseFalse);
//	    			else
//	    				displayErrorMessage(Constants.kErrorHasResponseFalseLowTram);
//				}
//				return serviceInfo;
//			}
//			else{
//				Message message = new Message();
//    			Bundle bundle = new Bundle();
//    			bundle.putString("error", "tramTRACKER was unable to retrieve the requested information. Please try again later");
//    			message.setData(bundle);
//				TramTrackerMainActivity.getAppManager().handlerToast.sendMessage(message);
//			}
//		} 
//		catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}
	
	/**
	 * Get next arrival times at stops for trams, routes that don't have next services should be stripped?
	 */
	public ServiceInfo getNextPredictedRoutesCollection(Stop stop, ArrayList<Route> alRoutes, boolean lowFloor){
		try {
			ArrayList<ServiceChange> alServiceChange = new ArrayList<ServiceChange>();
			ArrayList<ServiceDisruption> alServiceDisruption = new ArrayList<ServiceDisruption>();
			JSONArray jsonArray = null;
			boolean isTTAvailable = true;;
			ServiceInfo serviceInfo = new ServiceInfo();
			String url = Constants.strURLBase + "GetNextPredictedRoutesCollection/" + stop.getTrackerID() + "/" + alRoutes.get(0).getMainRouteNo()  
					+ "/" + lowFloor + Constants.strAID + Constants.strCID + Constants.token;
			InputStream jsonData = getJSONData(url);
			alRoutes.clear();
			if(jsonData != null){
		    	String timeRequested,timeResponded;
		    	JSONObject serviceData = parseJSONStream(jsonData);
		    	if (serviceData.getBoolean("hasError") || !serviceData.getBoolean("hasResponse")){
		    		if(serviceData.getBoolean("hasError")){
		    			serviceInfo.setHasEmptyResponse(false);
		    			displayErrorMessage(serviceData.getString("errorMessage"));
		    		}
		    		else{
		    			serviceInfo.setHasEmptyResponse(true);
		    			if(!lowFloor)
		    				displayErrorMessage(Constants.kErrorHasResponseFalse);
		    			else
		    				displayErrorMessage(Constants.kErrorHasResponseFalseLowTram);
		    		}
		    	} 
		    	else{
		    		if(serviceData.get("timeRequested") != JSONObject.NULL){
		    			timeRequested = serviceData.getString("timeRequested");
		    		}
		    		if(serviceData.get("timeResponded") != JSONObject.NULL){
		    			timeResponded = serviceData.getString("timeResponded");
		    			strServerResponseTimeString = timeResponded;
		    		}				
		    		Object object = serviceData.get("responseObject");
		    		if(object.getClass() == JSONArray.class)
		    			jsonArray = (JSONArray)object;
		    	}
//				System.out.println("jsonArray = " + jsonArray);
				if(jsonArray != null){
					serviceInfo.setHasEmptyResponse(false);
					Date serverResponseDate = jsonDateToDate(strServerResponseTimeString);
					serviceInfo.setServerReferenceDate(serverResponseDate);
					JSONObject object;
					Route route = null;
					Tram tram = null;
					for(int i = 0; i< jsonArray.length(); i++){
						object = jsonArray.getJSONObject(i);
						if(!object.getBoolean("IsTTAvailable")){
							serviceInfo.setHasEmptyResponse(false);
							isTTAvailable = false;
							break;
						}
						//If it's the first tram or every 4th tram example (0, 3, 6...)
						if(i % 3 == 0){
							route = new Route();
							tram = new Tram();
							jsonToRoute(object, serverResponseDate, route, tram);
							ArrayList<Tram> alTrams = new ArrayList<Tram>();
							alTrams.add(tram);
							route.setNextServices(alTrams);
							alRoutes.add(route);
						}
						//(1,2,4,5,...)
						else{
							tram = new Tram();
							//The new route is ignore, done so that nothing is broken
							jsonToRoute(object, serverResponseDate, tram);
							route.getNextServices().add(tram);
						}
					}
					jSonToServiceChange(alServiceChange, jsonArray);
					jSonToServiceDisruption(alServiceDisruption, jsonArray);
					serviceInfo.setRoutes(alRoutes);
					if(!isTTAvailable){
						serviceInfo.setHasEmptyResponse(false);
						serviceInfo.setRoutes(new ArrayList<Route>());
						displayErrorMessage(Constants.kErrorTTNotAvailable);
					}
					serviceInfo.setServiceChanges(alServiceChange);
					serviceInfo.setServiceDisruptions(alServiceDisruption);
				}
				if(jsonArray == null){
					serviceInfo.setHasEmptyResponse(true);
					serviceInfo.setRoutes(new ArrayList<Route>());
	    			if(!lowFloor)
	    				displayErrorMessage(Constants.kErrorHasResponseFalse);
	    			else
	    				displayErrorMessage(Constants.kErrorHasResponseFalseLowTram);
				}
				return serviceInfo;
			}
			else{
				Message message = new Message();
    			Bundle bundle = new Bundle();
    			bundle.putString("error", "tramTRACKER was unable to retrieve the requested information. Please try again later");
    			message.setData(bundle);
				TramTrackerMainActivity.getAppManager().handlerToast.sendMessage(message);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * Get next arrival times at stops for trams, routes that don't have next services should be stripped?
	 */
//	public ServiceInfo getNextPredictedRoutesCollectionRouteZero(Stop stop, boolean lowFloor){
//		try {
//			ArrayList<ServiceChange> alServiceChange = new ArrayList<ServiceChange>();
//			ArrayList<ServiceDisruption> alServiceDisruption = new ArrayList<ServiceDisruption>();
//			ArrayList<Route> alRoutes = new ArrayList<Route>();
//			JSONArray jsonArray = null;
//			boolean isTTAvailable = true;;
//			ServiceInfo serviceInfo = new ServiceInfo();
//			String url = Constants.strURLBase + "GetNextPredictedRoutesCollection/" + stop.getTrackerID() + "/0"  
//					+ "/" + lowFloor + Constants.strAID + Constants.strCID + Constants.token;
//			InputStream jsonData = getJSONData(url);
//			if(jsonData != null){
//		    	String timeRequested,timeResponded;
//		    	JSONObject serviceData = parseJSONStream(jsonData);
//		    	if (serviceData.getBoolean("hasError") || !serviceData.getBoolean("hasResponse")){
//		    		if(serviceData.getBoolean("hasError")){
//		    			serviceInfo.setHasEmptyResponse(false);
//		    			displayErrorMessage(serviceData.getString("errorMessage"));
//		    		}
//		    		else{
//		    			serviceInfo.setHasEmptyResponse(true);
//		    			if(!lowFloor)
//		    				displayErrorMessage(Constants.kErrorHasResponseFalse);
//		    			else
//		    				displayErrorMessage(Constants.kErrorHasResponseFalseLowTram);
//		    		}
//		    	} 
//		    	else{
//		    		if(serviceData.get("timeRequested") != JSONObject.NULL){
//		    			timeRequested = serviceData.getString("timeRequested");
//		    		}
//		    		if(serviceData.get("timeResponded") != JSONObject.NULL){
//		    			timeResponded = serviceData.getString("timeResponded");
//		    			strServerResponseTimeString = timeResponded;
//		    		}				
//		    		Object object = serviceData.get("responseObject");
//		    		if(object.getClass() == JSONArray.class)
//		    			jsonArray = (JSONArray)object;
//		    	}
////				System.out.println("jsonArray = " + jsonArray);
//				if(jsonArray != null){
//					serviceInfo.setHasEmptyResponse(false);
//					Date serverResponseDate = jsonDateToDate(strServerResponseTimeString);
//					serviceInfo.setServerReferenceDate(serverResponseDate);
//					JSONObject object;
//					Route route;
//					Tram tram;
//					for(int i = 0; i< jsonArray.length(); i++){
//						object = jsonArray.getJSONObject(i);
//						if(!object.getBoolean("IsTTAvailable")){
//							serviceInfo.setHasEmptyResponse(false);
//							isTTAvailable = false;
//							break;
//						}
//						route = new Route();
//						tram = new Tram();
//						jsonToRoute(object, serverResponseDate, route, tram);
//						if(alRoutes.size() == 0){
//							ArrayList<Tram> alTrams = new ArrayList<Tram>();
//							alTrams.add(tram);
//							route.setNextServices(alTrams);
//							alRoutes.add(route);
//						}
//						else{
//							Route r = getRouteWithDestination(route.getDestination(), route.getRouteNumber(), alRoutes);
//							if(r != null){
//								ArrayList<Tram> alTrams = r.getNextServices();
//								alTrams.add(tram);
//							}
//							else{
//								ArrayList<Tram> alTrams = new ArrayList<Tram>();
//								alTrams.add(tram);
//								route.setNextServices(alTrams);
//								alRoutes.add(route);
//							}
//						}
//					}
//					jSonToServiceChange(alServiceChange, jsonArray);
//					jSonToServiceDisruption(alServiceDisruption, jsonArray);
//					serviceInfo.setRoutes(alRoutes);
//					if(!isTTAvailable){
//						serviceInfo.setHasEmptyResponse(false);
//						serviceInfo.setRoutes(new ArrayList<Route>());
//						displayErrorMessage(Constants.kErrorTTNotAvailable);
//					}
//					serviceInfo.setServiceChanges(alServiceChange);
//					serviceInfo.setServiceDisruptions(alServiceDisruption);
//				}
//				if(jsonArray == null){
//					serviceInfo.setHasEmptyResponse(true);
//					serviceInfo.setRoutes(new ArrayList<Route>());
//	    			if(!lowFloor)
//	    				displayErrorMessage(Constants.kErrorHasResponseFalse);
//	    			else
//	    				displayErrorMessage(Constants.kErrorHasResponseFalseLowTram);
//				}
//				System.out.println("serviceInfo.getRoutes() = " + serviceInfo.getRoutes());
//				return serviceInfo;
//			}
//			else{
//				Message message = new Message();
//    			Bundle bundle = new Bundle();
//    			bundle.putString("error", "tramTRACKER was unable to retrieve the requested information. Please try again later");
//    			message.setData(bundle);
//				TramTrackerMainActivity.getAppManager().handlerToast.sendMessage(message);
//			}
//		} 
//		catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}
	
	public ServiceInfo getNextPredictedRoutesCollectionRouteZero(Stop stop, boolean lowFloor){
		try {
			ArrayList<ServiceChange> alServiceChange = new ArrayList<ServiceChange>();
			ArrayList<ServiceDisruption> alServiceDisruption = new ArrayList<ServiceDisruption>();
			ArrayList<Route> alRoutes = new ArrayList<Route>();
			JSONArray jsonArray = null;
			boolean isTTAvailable = true;;
			ServiceInfo serviceInfo = new ServiceInfo();
			String url = Constants.strURLBase + "GetNextPredictedRoutesCollection/" + stop.getTrackerID() + "/0"  
					+ "/" + lowFloor + Constants.strAID + Constants.strCID + Constants.token;
			InputStream jsonData = getJSONData(url);
			if(jsonData != null){
		    	String timeRequested,timeResponded;
		    	JSONObject serviceData = parseJSONStream(jsonData);
		    	if (serviceData.getBoolean("hasError") || !serviceData.getBoolean("hasResponse")){
		    		if(serviceData.getBoolean("hasError")){
		    			serviceInfo.setHasEmptyResponse(false);
		    			displayErrorMessage(serviceData.getString("errorMessage"));
		    		}
		    		else{
		    			serviceInfo.setHasEmptyResponse(true);
		    			if(!lowFloor)
		    				displayErrorMessage(Constants.kErrorHasResponseFalse);
		    			else
		    				displayErrorMessage(Constants.kErrorHasResponseFalseLowTram);
		    		}
		    	} 
		    	else{
		    		if(serviceData.get("timeRequested") != JSONObject.NULL){
		    			timeRequested = serviceData.getString("timeRequested");
		    		}
		    		if(serviceData.get("timeResponded") != JSONObject.NULL){
		    			timeResponded = serviceData.getString("timeResponded");
		    			strServerResponseTimeString = timeResponded;
		    		}				
		    		Object object = serviceData.get("responseObject");
		    		if(object.getClass() == JSONArray.class)
		    			jsonArray = (JSONArray)object;
		    	}
//				System.out.println("jsonArray = " + jsonArray);
				if(jsonArray != null){
					serviceInfo.setHasEmptyResponse(false);
					Date serverResponseDate = jsonDateToDate(strServerResponseTimeString);
					serviceInfo.setServerReferenceDate(serverResponseDate);
					JSONObject object;
					Route route = null;
					Tram tram = null;
					for(int i = 0; i< jsonArray.length(); i++){
						object = jsonArray.getJSONObject(i);
						if(!object.getBoolean("IsTTAvailable")){
							serviceInfo.setHasEmptyResponse(false);
							isTTAvailable = false;
							break;
						}
						//If it's the first tram or every 4th tram example (0, 3, 6...)
						if(i % 3 == 0){
							route = new Route();
							tram = new Tram();
							jsonToRoute(object, serverResponseDate, route, tram);
							ArrayList<Tram> alTrams = new ArrayList<Tram>();
							alTrams.add(tram);
							route.setNextServices(alTrams);
							alRoutes.add(route);
						}
						//(1,2,4,5,...)
						else{
							tram = new Tram();
							//The new route is ignore, done so that nothing is broken
							jsonToRoute(object, serverResponseDate, tram);
							route.getNextServices().add(tram);
						}
					}
					jSonToServiceChange(alServiceChange, jsonArray);
					jSonToServiceDisruption(alServiceDisruption, jsonArray);
					serviceInfo.setRoutes(alRoutes);
					if(!isTTAvailable){
						serviceInfo.setHasEmptyResponse(false);
						serviceInfo.setRoutes(new ArrayList<Route>());
						displayErrorMessage(Constants.kErrorTTNotAvailable);
					}
					serviceInfo.setServiceChanges(alServiceChange);
					serviceInfo.setServiceDisruptions(alServiceDisruption);
				}
				if(jsonArray == null){
					serviceInfo.setHasEmptyResponse(true);
					serviceInfo.setRoutes(new ArrayList<Route>());
	    			if(!lowFloor)
	    				displayErrorMessage(Constants.kErrorHasResponseFalse);
	    			else
	    				displayErrorMessage(Constants.kErrorHasResponseFalseLowTram);
				}
				System.out.println("serviceInfo.getRoutes() = " + serviceInfo.getRoutes());
				return serviceInfo;
			}
			else{
				Message message = new Message();
    			Bundle bundle = new Bundle();
    			bundle.putString("error", "tramTRACKER was unable to retrieve the requested information. Please try again later");
    			message.setData(bundle);
				TramTrackerMainActivity.getAppManager().handlerToast.sendMessage(message);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	//Utility method for above method
	private Route getRouteWithDestination(String destination, String routeNo, ArrayList<Route> alRoutes){
		Route r;
		for (int i = 0; i < alRoutes.size(); i++) {
			r = alRoutes.get(i);
			if(r.getRouteNumber().equalsIgnoreCase(routeNo) && r.getDestination().equalsIgnoreCase(destination)){
				return r;
			}
		}
		return null;
	}
	
//	public String ge
	
//	/**
//	 * Get next arrival times at stops for trams, routes that don't have next services should be stripped?
//	 */
//	public ArrayList<Route> getMainRoutesForStop(Stop stop){
//		try {
//			ArrayList<Route> alRoutes = new ArrayList<Route>();
//			JSONArray jsonArray;
//			String url = Constants.strURLBase + "GetMainRoutesForStop/" + stop.getTrackerID() + Constants.strAID + Constants.strCID + token;
//			InputStream jsonData = getJSONData(url);
//			jsonArray = getResponseArray(jsonData);
//			JSONObject object;
//			Route route;
//			if(jsonArray != null){
//				for (int i = 0; i < jsonArray.length(); i++) {
//					object = jsonArray.getJSONObject(i);
//					route = new Route();
//					route.setRouteNumber(object.getString("RouteNo"));
//					alRoutes.add(route);
//				}
//			}
//			return alRoutes;
//		} 
//		catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}
	
	/**
	 * Get all tram routes and directions
	 */
	public void getDestinationsForAllRoutes(int tramNo){
		try {
			String url = Constants.strURLBase + "GetDestinationsForAllRoutes" + Constants.strAID;
			InputStream jsonData = getJSONData(url);
			JSONArray jSonArray = getResponseArray(jsonData);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Get scheduled departures/time tables (according to spec, next 3 trams) given stop tracker ID, route no
	 */
	public ArrayList<Tram> getSchedulesForStopandRoute(String stopTrackerID, String routeNo, Calendar calNow, boolean lowFloor){
		try {
			String strDate = (String)DateFormat.format("yyyy-MM-dd'T'kk:mm:'00'z", calNow);
			StringBuffer buffer = new StringBuffer(strDate);
			buffer.insert(22, ':');
//			System.out.println("buffer = " + buffer);
//			String strDate = (String)DateFormat.format("yyyy-MM-dd'T'HH:mm:00ZZZ", calNow);
			String url = Constants.strURLBase + "GetSchedulesCollection" + "/" + stopTrackerID + "/" + routeNo + "/" + lowFloor + "/" + buffer.toString() + Constants.strAID + Constants.token;
			InputStream jsonData = getJSONData(url);
			JSONArray jsonArray = null;
			if(jsonData != null){
				String timeRequested,timeResponded;
				JSONObject serviceData = parseJSONStream(jsonData);
				if (serviceData.getBoolean("hasError") || !serviceData.getBoolean("hasResponse")){
					if(serviceData.getBoolean("hasError"))
						displayErrorMessage(serviceData.getString("errorMessage"));
					else{
						displayErrorMessage(Constants.kErrorHasResponseFalseTimeTablePrefix + routeNo + Constants.kErrorHasResponseFalseTimeTableSuffix);
					}
				} 
				else{
					if(serviceData.get("timeRequested") != JSONObject.NULL){
						timeRequested = serviceData.getString("timeRequested");
					}
					if(serviceData.get("timeResponded") != JSONObject.NULL){
						timeResponded = serviceData.getString("timeResponded");
						strServerResponseTimeString = timeResponded;
					}				
					Object object = serviceData.get("responseObject");
					if(object.getClass() == JSONArray.class)
						jsonArray = (JSONArray)object;
				}
//				System.out.println("jsonArray = " + jsonArray);
				if(jsonArray != null){
					return jsonToTrams(jsonArray);
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public JSONObject getStopForTrackerId(String trackerId){
		JSONObject result = null;
		String url = Constants.strURLBase + "GetStopInformation" + "/" + trackerId + Constants.strAID + Constants.token;
		InputStream jsonData = getJSONData(url);
		result = parseJSONStream(jsonData);
//		System.out.println("StopNo = " + trackerId + ", result = " + result);
		return result;
	}
	
	public JSONObject getAllTicketOutlets(){
		JSONObject result = null;
		String url = Constants.strURLBase + "GetTicketOutlets" + Constants.strAID + Constants.token;
		InputStream jsonData = getJSONData(url);
		result = parseJSONStream(jsonData);
//		System.out.println("result = " + result);
		return result;
	}
	
	public JSONObject getAllRoutes(){
		JSONObject result = null;
//		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		String url = Constants.strURLBase + "GetStopsAndRoutesUpdatesSince" + "/01-07-2009" + Constants.strAID + Constants.token;
		InputStream jsonData = getJSONData(url);
		result = parseJSONStream(jsonData);
//		System.out.println("result = " + result);
//		Log.e("url", url);
		return result;
	}
	
//	public JSONObject getAllStopsForRoute(String routeNo, boolean isUpDirection){
//		JSONObject result = null;
//		String url = Constants.strURLBase + "GetListOfStopsByRouteNoAndDirection" + "/" + routeNo + "/" + (isUpDirection?"true":"false") + Constants.strAID + token;
//		InputStream jsonData = getJSONData(url);
//		result = parseJSONStream(jsonData);
//		System.out.println("result = " + result);
//		return result;
//	}
	
	
	public JSONObject getAllStopsForRoute(String routeNo){
		JSONObject result = null;
		String url = Constants.strURLBase + "GetRouteStopsByRoute" + "/" + routeNo + Constants.strAID + Constants.token;
		InputStream jsonData = getJSONData(url);
		result = parseJSONStream(jsonData);
//		System.out.println("result = " + result);
		return result;
	}
	
	public JSONObject getAllPOIs(){
		JSONObject result = null;
		String url = Constants.strURLBase + "GetPointsOfInterest" + Constants.strAID + Constants.token;
		InputStream jsonData = getJSONData(url);
		result = parseJSONStream(jsonData);
//		System.out.println("result = " + result);
		return result;
	}
	
	public JSONObject getDestinationsForRoute( String routeNo){
		JSONObject result = null;
		String url = Constants.strURLBase + "GetDestinationsForRoute" + "/" + routeNo + Constants.strAID + Constants.token;
		InputStream jsonData = getJSONData(url);
		result = parseJSONStream(jsonData);
//		System.out.println("result = " + result);
		return result;
	}
	
	public JSONObject getTicketOutletForId(String ticketOutletId){
		JSONObject result = null;
		String url = Constants.strURLBase + "GetTicketOutletById" + "/" + ticketOutletId + Constants.strAID + Constants.token;
		InputStream jsonData = getJSONData(url);
		result = parseJSONStream(jsonData);
//		System.out.println("result = " + result);
		return result;
	}
	
	public JSONObject getPoiForId(String poiId){
		JSONObject result = null;
		String url = Constants.strURLBase + "GetPointOfInterestById" + "/" + poiId + Constants.strAID + Constants.token;
		InputStream jsonData = getJSONData(url);
		result = parseJSONStream(jsonData);
//		System.out.println("result = " + result);
		return result;
	}
	
	public JSONObject getStopsForPoiId(String poiId){
		JSONObject result = null;
		String url = Constants.strURLBase + "GetStopsByPointOfInterestId" + "/" + poiId + Constants.strAID + Constants.token;
		InputStream jsonData = getJSONData(url);
		result = parseJSONStream(jsonData);
//		System.out.println("result = " + result);
		return result;
	}
	
}
