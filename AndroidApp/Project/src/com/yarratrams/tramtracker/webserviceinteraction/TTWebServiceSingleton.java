package com.yarratrams.tramtracker.webserviceinteraction;

public class TTWebServiceSingleton {
	private static TTWebService webService;
	public static TTWebService getInstance(){
		if(webService == null)
			webService = new TTWebService();
		return webService;
	}
}
