package com.yarratrams.tramtracker.webserviceinteraction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.yarratrams.tramtracker.db.TTDBUpdate;
import com.yarratrams.tramtracker.objects.Constants;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.singleton.TTDBUpdateSingleton;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.text.format.DateFormat;
import android.util.Log;

public class UpdateManager{
	
	private String token = "&tkn=3e982c6b-d57a-43fe-bff7-a44e9adb9e1e";
	private JSONObject updateStopsAndRoutes; 
	private JSONArray updatePOIs, updateTicketOutlets;
	private TramTrackerMainActivity tramTrackerMainActivity;
	private SharedPreferences appSharedPrefs;
    private Editor prefsEditor;
    private UpdateManager parent = this;
	
	public void checkForUpdate(TramTrackerMainActivity callBack){
		appSharedPrefs = callBack.getSharedPreferences(Constants.kSharedPrefIdentifier, Activity.MODE_PRIVATE);
        prefsEditor = appSharedPrefs.edit();
		this.tramTrackerMainActivity = callBack;
		new UpdateCheckTask().execute();
	}
	
	public void displayErrorMessage(String strMessage){
		Message message = new Message();
		Bundle bundle = new Bundle();
		bundle.putString("error", strMessage);
		message.setData(bundle);
		TramTrackerMainActivity.getAppManager().handlerToast.sendMessage(message); 
	}
	
	public void update(){
		new UpdateDatabaseTask().execute(tramTrackerMainActivity);
	}
	
	//TODO Must add POI update too
	private class UpdateCheckTask extends AsyncTask<Void, Void, Boolean>{

		protected Boolean doInBackground(Void... params) {
			
			Log.w("UpdateCheckTask","Update process started");
			Calendar cal = new GregorianCalendar(Constants.kLastUpdateYear, Constants.kLastUpdateMonth, Constants.kLastUpdateDayOfMonth);
			cal.setTime(new Date(appSharedPrefs.getLong(Constants.kLastUpdateDate, cal.getTimeInMillis())));
			String strDate = (String)DateFormat.format("dd-MM-yyyy", cal);
//			System.out.println("Last update date = " + strDate);
			//TODO remove and uncomment above
//			String strDate = "01-07-2012";
			String url = Constants.strURLBase + "GetStopsAndRoutesUpdatesSince" + "/" + strDate + Constants.strAID + token;
//			System.out.println("url = " + url);
			boolean flag = false;
			try{
				//Get stop and route updates
				InputStream jsonData = getJSONData(url);
				if(jsonData != null){
					JSONObject serviceData = parseJSONStream(jsonData);
					if(serviceData != null){
						if(!serviceData.getBoolean("hasError") && serviceData.getBoolean("hasResponse")){
							JSONObject responseObject = serviceData.getJSONObject("responseObject");
							if(responseObject != null && responseObject != JSONObject.NULL){
								updateStopsAndRoutes = responseObject;
								if(responseObject.get("RouteChanges") == JSONObject.NULL && responseObject.get("StopChanges") == JSONObject.NULL)
									updateStopsAndRoutes = null;
								else{
									Log.w("UpdateCheckTask","routes and stops update available");
									flag = true;
								}
							}
						}
					}
				}
				//Get ticket outlet updates
				url = Constants.strURLBase + "GetTicketOutletChangesSince" + "/" + strDate + Constants.strAID + token;
//				System.out.println("ticket outlet update URL = " + url);
				jsonData = getJSONData(url);
				if(jsonData != null){
					JSONObject serviceData = parseJSONStream(jsonData);
					if(serviceData != null){
						if(!serviceData.getBoolean("hasError") && serviceData.getBoolean("hasResponse")){
							JSONArray responseObject = serviceData.getJSONArray("responseObject");
							if(responseObject != null && responseObject != JSONObject.NULL){
								updateTicketOutlets = responseObject;
								Log.w("UpdateCheckTask","ticket outlets update available");
								flag = true;
							}
						}
					}
				}
				//Get POI updates
				url = Constants.strURLBase + "GetPointsOfInterestChangesSince" + "/" + strDate + Constants.strAID + token;
				jsonData = getJSONData(url);
				if(jsonData != null){
					JSONObject serviceData = parseJSONStream(jsonData);
					if(serviceData != null){
						if(!serviceData.getBoolean("hasError") && serviceData.getBoolean("hasResponse")){
							JSONArray responseObject = serviceData.getJSONArray("responseObject");
							if(responseObject != null && responseObject != JSONObject.NULL){
								updatePOIs = responseObject;
								Log.w("UpdateCheckTask","pois update available");
								flag = true;
							}
						}
					}
				}
			}	
			catch (Exception e) {
				e.printStackTrace();
			}
			return flag;			
		}
		
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try{
			if(result == true){
				tramTrackerMainActivity.notifyUpdateAvailable(parent);
//				System.out.println("Update available");
			}
//			else
//				System.out.println("No update available");
			}catch (Exception e) {
//				System.out.println("Exception handled");
				e.printStackTrace();
			}
		}
		
		/**
		 *  Download data from JSON service
		 */
		private InputStream getJSONData(String url) {
	        DefaultHttpClient httpClient = new DefaultHttpClient();
	        URI uri;
	        InputStream data = null;
	        try {
	        	uri = new URI(url);
	        	HttpGet method = new HttpGet(uri);
	        	HttpResponse response;
	        	response = httpClient.execute(method);
	        	data = response.getEntity().getContent();
	        } catch (URISyntaxException e) {
	        	e.printStackTrace();
	        } catch (ClientProtocolException e) {
	        	e.printStackTrace();
	        } catch (IOException e) {
	        	e.printStackTrace();
	        }
	        return data;
	    }
		
	    /**
	     * Parse InputStream into JSONObject.
	     */
	    private JSONObject parseJSONStream(InputStream is) {
	    	JSONObject jsonObject = null;
	    	try {
	    		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	    		StringBuilder sb = new StringBuilder();
	    		String line = null;
	    		while ((line = reader.readLine()) != null) {
	    			sb.append(line);
	    		}
	    		is.close();
	    		String jsonData = sb.toString();
	            jsonObject = new JSONObject(jsonData);
	    	} 
	    	catch(Exception e){
	    		e.printStackTrace();
	    	}
			return jsonObject;
	    }
		
	}

	
	private class UpdateDatabaseTask extends AsyncTask<Context, Void, Boolean>{
		TTDBUpdate ttdbUpdate;
		TTWebService webservice = TTWebServiceSingleton.getInstance();
		protected Boolean doInBackground(Context... params) {
			Boolean result = true;
			try{
				ttdbUpdate = TTDBUpdateSingleton.getTTDBUpdateInstance(params[0]);
				if(updateStopsAndRoutes != null){
//					ArrayList<Integer> alUpdateRoutes = new ArrayList<Integer>();
//					ArrayList<Integer> alDeleteRoutes = new ArrayList<Integer>();

					JSONArray arrRoutes=null, arrStops=null;
					JSONObject route, stop;
					if(updateStopsAndRoutes.get("RouteChanges") instanceof JSONArray)
						arrRoutes = updateStopsAndRoutes.getJSONArray("RouteChanges");
					ArrayList<Integer> alStops = new ArrayList<Integer>();
					if(arrRoutes != null && arrRoutes != JSONObject.NULL){
						JSONObject directions, response;
						JSONArray routeStops;
						if(arrRoutes.length() > 0){
							for(int i=0; i < arrRoutes.length(); i++){
								route = arrRoutes.getJSONObject(i);
								if(route.getString("Action").equalsIgnoreCase("DELETE")){
									result = ttdbUpdate.deleteRouteViaUpdate(route);
									if(result == false)
										return result;
								}
								else{
									directions = webservice.getDestinationsForRoute(route.getString("ID"));
									if(!directions.getBoolean("hasError") && directions.getBoolean("hasResponse")){
										response = webservice.getAllStopsForRoute(route.getString("ID"));
										if(!response.getBoolean("hasError") && response.getBoolean("hasResponse")){
											routeStops = response.getJSONArray("responseObject");
											if(routeStops != null && routeStops != JSONObject.NULL){
//												result = ttdbUpdate.deleteRouteViaUpdate(route);
												result = true;
												if(result){
													result = ttdbUpdate.createRouteViaUpdate(route, directions.getJSONArray("responseObject").getJSONObject(0), 
															routeStops, alStops);
													if(!result)
														return result;
												}
												else{
													return result;
												}
											}
										}
									}
								}
							}
						}
					}
					
					ArrayList<Integer> alUpdateStops = new ArrayList<Integer>();
					ArrayList<Integer> alDeleteStops = new ArrayList<Integer>();
					ArrayList<JSONArray> alUpdateObjects = new ArrayList<JSONArray>();
					JSONObject updateObject;
					if(updateStopsAndRoutes.get("StopChanges") instanceof JSONArray)
						arrStops = updateStopsAndRoutes.getJSONArray("StopChanges");
					if(arrStops != null && arrStops != JSONObject.NULL){
						if(arrStops.length() > 0){
							for(int i=0; i<arrStops.length(); i++){
								stop = arrStops.getJSONObject(i);
								if(stop.getString("Action").equalsIgnoreCase("UPDATE")){
									updateObject = webservice.getStopForTrackerId(stop.getString("StopNo"));
									if(!updateObject.getBoolean("hasError") && updateObject.getBoolean("hasResponse")){
										alUpdateObjects.add(updateObject.getJSONArray("responseObject"));
										alUpdateStops.add(stop.getInt("StopNo"));
									}
									else{
//										Log.e("UpdateDatabaseTask doInBackground", "stopId = " + stop.getString("StopNo") + " response = " + updateObject);
//										result = false;
//										return null;
									}
									
								}
								else{
									alDeleteStops.add(stop.getInt("StopNo"));
								}
							}
							System.out.println("alUpdateStops = " + alUpdateStops);
							System.out.println("alDeleteStops = " + alDeleteStops);
							System.out.println("alUpdateObjects = " + alUpdateObjects);
							result = ttdbUpdate.updateStopsViaUpdate(alUpdateStops, alDeleteStops, alUpdateObjects);
							if(!result){
								displayErrorMessage("Error updating stops.");
								return result;
							}
						}
					}
				}
//				updateTicketOutlets = null;
				if(updateTicketOutlets != null){
					if(updateTicketOutlets != null && updateTicketOutlets != JSONObject.NULL){
						JSONObject ticketOutlet;
						ArrayList<Integer> alAddTicketOutletId = new ArrayList<Integer>();
						ArrayList<Integer> alDeleteTicketOutletId = new ArrayList<Integer>();
						ArrayList<JSONObject> alObjects = new ArrayList<JSONObject>();
						JSONObject object;
						for(int i=0; i< updateTicketOutlets.length(); i++){
							ticketOutlet = updateTicketOutlets.getJSONObject(i);
							if(ticketOutlet.getString("Action").equalsIgnoreCase("UPDATE")){
								alAddTicketOutletId.add(new Integer(ticketOutlet.getInt("ID")));
								object = webservice.getTicketOutletForId(ticketOutlet.getString("ID"));
								alObjects.add(object);
							}
							else if(ticketOutlet.getString("Action").equalsIgnoreCase("DELETE")){
								alDeleteTicketOutletId.add(new Integer(ticketOutlet.getInt("ID")));
							}
						}
						result = ttdbUpdate.updateTicketOutletsViaUpdate(alAddTicketOutletId, alDeleteTicketOutletId, alObjects);
						ttdbUpdate.createStopsAndNearbyOutletsViaUpdate(alAddTicketOutletId);
						if(!result){
							displayErrorMessage("Error updating ticket outlets.");
							return result;
						}
					}
				}
//				updatePOIs = null;
				if(updatePOIs != null){
					if(updatePOIs != null && updatePOIs != JSONObject.NULL){
						JSONObject poi;
						ArrayList<Integer> alAddPoiId = new ArrayList<Integer>();
						ArrayList<Integer> alDeletePoiId = new ArrayList<Integer>();
						ArrayList<JSONObject> alPoiObjects = new ArrayList<JSONObject>();
						ArrayList<JSONObject> alPoiStops = new ArrayList<JSONObject>();
						JSONObject object, poisStopObject;
						for(int i=0; i< updatePOIs.length(); i++){
							poi = updatePOIs.getJSONObject(i);
							if(poi.getString("Action").equalsIgnoreCase("UPDATE")){
								object = webservice.getPoiForId(poi.getString("POIId"));
								if(!object.getBoolean("hasError") && object.getBoolean("hasResponse")){
									poisStopObject = webservice.getStopsForPoiId(poi.getString("POIId"));
									if(!poisStopObject.getBoolean("hasError") && poisStopObject.getBoolean("hasResponse")){
										alAddPoiId.add(new Integer(poi.getInt("POIId")));
										alPoiObjects.add(object);
										alPoiStops.add(poisStopObject);
									}
								}
							}
							else if(poi.getString("Action").equalsIgnoreCase("DELETE")){
								alDeletePoiId.add(new Integer(poi.getInt("POIId")));
							}
						}
						result = ttdbUpdate.updatePOIsViaUpdate(alAddPoiId, alDeletePoiId, alPoiObjects, alPoiStops);
						if(!result){
							displayErrorMessage("Error updating points of interest.");
							return result;
						}
					}
				}
				if(result){
//					System.out.println("Updated");
					Calendar calNow  = Calendar.getInstance();
					calNow.add(Calendar.DAY_OF_MONTH, 1);
					prefsEditor.putLong(Constants.kLastUpdateDate, calNow.getTimeInMillis());
					displayErrorMessage("Update completed");
					prefsEditor.commit();
				}
				ttdbUpdate.close();
			} catch(Exception e){
				e.printStackTrace();
			}
			return result;
		}
		
//		private JSONArray getRouteChanges(JSONObject responseObject){
//			JSONArray jaStopChanges = null;
//			try {
//				jaStopChanges = responseObject.getJSONArray("StopChanges");
//				if(jaStopChanges == JSONObject.NULL)
//					jaStopChanges = null;
//			} catch (JSONException e) {
//				e.printStackTrace();
//			}
//			return jaStopChanges;
//		}
		
		
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			//TODO call ui method for indicating availability of update
			tramTrackerMainActivity.notifyFinishUpdate(result);
		}
	}
}
