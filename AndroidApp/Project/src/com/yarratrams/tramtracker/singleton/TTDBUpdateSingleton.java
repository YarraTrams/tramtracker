package com.yarratrams.tramtracker.singleton;

import com.yarratrams.tramtracker.db.TTDBUpdate;

import android.content.Context;

public class TTDBUpdateSingleton {
	static TTDBUpdate ttdbUpdate;
	
	public static TTDBUpdate getTTDBUpdateInstance(Context context){
		if(ttdbUpdate == null)
			ttdbUpdate = new TTDBUpdate(context);
		return ttdbUpdate;
	}
}
