package com.yarratrams.tramtracker.singleton;

import com.yarratrams.tramtracker.db.TTDB;

import android.content.Context;

public class TTDBSingleton {
	private static TTDB ttdb;
	
	public static TTDB getInstance(Context context){
		if(ttdb == null){
			ttdb = new TTDB(context);
		}
		return ttdb;
	}
}
