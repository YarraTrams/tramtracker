package com.yarratrams.tramtracker.backgroundservice;

import java.util.Date;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.yarratrams.tramtracker.objects.Constants;

public class BootCompelteReceiver extends BroadcastReceiver {




	private AlarmManager alarmManager;
	private Intent intentTooBroadcastRec;
	private PendingIntent pendingIntentBroadcastRec;

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub

		System.out.println("-- action: " + intent.getAction());

		if(intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED))
		{


			System.out.println("-- boot Completed");

			startAlarm(context);
		}

		if(intent.getAction().equals(Intent.ACTION_TIME_CHANGED))
		{
			System.out.println("-- time changed");

			startAlarm(context);
		}
	}

	private void startAlarm(Context context) {
		
		System.out.println("-- strt the alarm fro broadcast: " );
		Date d = new Date();
		long timeTilMinuteChange = 60*1000-d.getSeconds()*1000;
		long startTime = System.currentTimeMillis() +  timeTilMinuteChange;


		alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

		intentTooBroadcastRec = new Intent(context, AlaramReceiver.class);

		intentTooBroadcastRec.putExtra(AlaramReceiver.ACTION_ALARM, AlaramReceiver.ACTION_ALARM);

		pendingIntentBroadcastRec = PendingIntent.getBroadcast(context, 99150, intentTooBroadcastRec, PendingIntent.FLAG_UPDATE_CURRENT);

		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP	, startTime, Constants.SERVICE_REPEAT_INTERVAL , pendingIntentBroadcastRec);
	}

}
