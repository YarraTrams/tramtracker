package com.yarratrams.tramtracker.backgroundservice;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.v4.app.NotificationCompat;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.RandomGenerator;
import com.yarratrams.tramtracker.objects.Reminders;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.objects.Tram;
import com.yarratrams.tramtracker.ui.OnBoardActivity;
import com.yarratrams.tramtracker.ui.PIDActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;
import com.yarratrams.tramtracker.ui.util.ReminderPreferences;

public class TaskService extends IntentService {

	Handler handler;
	Context context;

	ReminderPreferences reminderPreferences;
	Reminders currReminders;
	ArrayList<Reminders> arrayListReminders;
	
	int notificationIndex = 0;


	public static String TASK_MESSAGE = "From_Task";

	Date nowDate, reminderData;
	String dateFormat = "dd-MM-yyyy HH:mm";
	SimpleDateFormat simpleDateFormat;

	public TaskService() {
		// TODO Auto-generated constructor stub
		super("TaskService");

		context = this;

		handler = new Handler() {
			public void handleMessage(android.os.Message msg) {

				// Toast.makeText(context, "I am running", 0).show();
				// generateNotification();

			};
		};

	}

	public TaskService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub


		handler.sendEmptyMessage(0);

		reminderPreferences = new ReminderPreferences(context);

		arrayListReminders = reminderPreferences.getRemindersArrayList();


		// stopService(intent);

		if (arrayListReminders != null) {

			//			System.out.println("-- arrlst size in service: "+ arrayListReminders.size());

			for (int i = 0; i < arrayListReminders.size(); i++) {
				currReminders = arrayListReminders.get(i);

				nowDate = new Date(System.currentTimeMillis());
				reminderData = new Date(currReminders.getReminderTime());

				simpleDateFormat = new SimpleDateFormat(dateFormat);

				System.err.println("-- nowdate: " + simpleDateFormat.format(nowDate) + " reminddate: " + simpleDateFormat.format(reminderData) +  " is act " + currReminders.isActive()  );

				if (simpleDateFormat.format(nowDate).equals(simpleDateFormat.format(reminderData)) && currReminders.isActive() ) {
					generateNotification();
					reminderPreferences.updateReminder(currReminders, false);
					reminderPreferences.deleteReminder(currReminders);
				}

			}
		} else {
			System.out.println("-- arrlist is null");
		}

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

		System.out.println("-- service destroyed");
	}

	public void generateNotification() {

		PendingIntent resultPendingIntent = null;
		Intent resultIntent;
		String title = "tramTRACKER", content = "", ticker = "tramTRACKER";
		Uri  sound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.a);

		WakeLock screenLock = ((PowerManager) getSystemService(POWER_SERVICE))
				.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK
						| PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");
		screenLock.acquire();

		// later
		screenLock.release();


		switch (currReminders.getREMINDER_TYPE()) {
			case Reminders.STOP_ALARM :
				title = "tramTRACKER";
				content = "Your Stop: " + currReminders.getStopName() + " is approaching. Check myTRAM for the latest real-time info.";
				ticker = "Tram Approaching";


				resultIntent = new Intent(this, TramTrackerMainActivity.class);
				resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
				System.out.println("-- before sending: " + currReminders.getTram().getHeadboardNo());
				resultIntent.putExtra(OnBoardActivity.INTENT_KEY, currReminders.getTram());
				resultIntent.putExtra(TASK_MESSAGE, currReminders.getREMINDER_TYPE());
				resultPendingIntent = PendingIntent.getActivity(this,RandomGenerator.getRandomInt(),resultIntent,PendingIntent.FLAG_UPDATE_CURRENT);

				break;


			case Reminders.SCHEDULE_DEP_ALARM :
				title = "tramTRACKER";
				//content = "Your Stop: " + currReminders.getStopName() + " is approaching soon. Please check tramTRACKER for real-time info";
				content = "Your tram is scheduled to arrive in "+ getReminderTime(currReminders.getReminderTime(), currReminders.getSetTime()) + " minutes. Check tramTRACKER for the latest real-time info.";
				//content = "Schedule alarm went off";
				ticker = "Tram Approaching";



				resultIntent = new Intent(this, TramTrackerMainActivity.class);
				resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
				resultIntent.putExtra(PIDActivity.INTENT_KEY, currReminders.getStop());
				resultIntent.putExtra(TASK_MESSAGE, currReminders.getREMINDER_TYPE());
				resultPendingIntent = PendingIntent.getActivity(this,RandomGenerator.getRandomInt(),resultIntent,PendingIntent.FLAG_UPDATE_CURRENT);


				break;


			case Reminders.TRAM_ALARM :
				title = "tramTRACKER";
				//content = "Your Stop: " + currReminders.getStopName() + " is approaching soon. Please check tramTRACKER for real-time info";
				content = "Your tram is scheduled to arrive in "+ getReminderTime(currReminders.getReminderTime(), currReminders.getSetTime()) + " minutes. Check tramTRACKER for the latest real-time info.";
				ticker = "Stop Approaching";




				//				TramTrackerMainActivity.getAppManager().callSelection(getStopDescription(currReminders.getStop()));
				//				Intent resultIntent = new Intent(context, PIDActivity.class);
				//				resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				//				resultIntent.putExtra(PIDActivity.INTENT_KEY, currReminders.getStop());
				//
				//				TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_NEARBY, 
				//						context.getResources().getString(R.string.tag_pid_screen), resultIntent);
				//
				//				resultPendingIntent = PendingIntent.getActivity(this,0,resultIntent,PendingIntent.FLAG_UPDATE_CURRENT);


				resultIntent = new Intent(this, TramTrackerMainActivity.class);
				resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
				resultIntent.putExtra(PIDActivity.INTENT_KEY, currReminders.getStop());
				resultIntent.putExtra(TASK_MESSAGE, currReminders.getREMINDER_TYPE());
				resultPendingIntent = PendingIntent.getActivity(this,RandomGenerator.getRandomInt(),resultIntent,PendingIntent.FLAG_UPDATE_CURRENT);

				break;


		}



		NotificationCompat.Builder builder = new NotificationCompat.Builder(
				this);
		builder.setContentTitle(title);
		builder.setContentText(content);
//		builder.setSmallIcon(R.drawable.icn_app);
		builder.setSmallIcon(R.drawable.notcenter);
		builder.setLights(Color.GREEN, 1000, 1000);
		builder.setTicker(ticker);
		builder.setPriority(NotificationCompat.PRIORITY_HIGH);
		builder.setAutoCancel(true);




		//set sound for notification based on tram class
		switch (currReminders.getTramClass()) {
			case Tram.CLASS_W :
				sound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.w);
				break;

			case Tram.CLASS_C1 | Tram.CLASS_C2 | Tram.CLASS_D1 | Tram.CLASS_D2 | Tram.CLASS_E:
				sound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.citadis);
				break;



			default :
				sound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.a);
				break;
		}




		builder.setSound(sound);


		//		Intent resultIntent = new Intent(this, TramTrackerMainActivity.class);
		//		resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		//		resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		//		PendingIntent resultPendingIntent = PendingIntent.getActivity(this,0,resultIntent,PendingIntent.FLAG_UPDATE_CURRENT);
		builder.setContentIntent(resultPendingIntent);

		NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		notificationIndex =  RandomGenerator.getRandomInt();
		
		
		mNotificationManager.cancel(notificationIndex);

		// notificationID allows you to update the notification later on.
		mNotificationManager.notify(notificationIndex, builder.build());
	}





	private String getStopDescription(Stop stop){
		String text = "";

		text = text.concat(String.valueOf(stop.getStopNumber()));		
		//phoenix changed 20.Jun.2014
		text = text.concat(": ");
		//		text = text.concat(context.getResources().getString(R.string.stop_name_colon));
		text = text.concat(stop.getStopName());

		return text;
	}	





	public String getReminderTime(long reminderTime, long oriTime)
	{
		Date date = new Date(oriTime - reminderTime);

		return  String.valueOf(date.getMinutes());

	}
}
