package com.yarratrams.tramtracker.services;

import java.util.ArrayList;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;

import com.yarratrams.tramtracker.db.TTDB;
import com.yarratrams.tramtracker.objects.Route;
import com.yarratrams.tramtracker.objects.ServiceInfo;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.singleton.TTDBSingleton;
import com.yarratrams.tramtracker.webserviceinteraction.TTWebService;

public class PIDUpdateService extends Service {
	public static final String PID_UPDATE = "com.yarratrams.tramtracker.ui.PIDUpdate";
	public static final String kStopKey = "com.yarratrams.tramtracker.services.stopkey";
	public static final String INTENT_KEY = "services_update";
	private final Handler handler = new Handler();
	private Intent intent;
	private TTDB ttdb;
	private TTWebService webService;
	private ServiceInfo info;
	private Stop stop;

	@Override
	public void onCreate() {
		super.onCreate();
		intent = new Intent(PID_UPDATE);
	}
                 

	@Override
	public void onStart(Intent intent, int startId) {
		Bundle extras = intent.getExtras();
		stop = (Stop)extras.get(kStopKey);
		handler.removeCallbacks(servicesUpdater);
		handler.post(servicesUpdater);
	}
	
	
	private Runnable servicesUpdater = new Runnable() {
		public void run() {
			getServiceUpdate();
			// TODO set timer
			handler.postDelayed(this, 15000); // Obtain next update in 15 seconds
		}
	};
	
	private void getServiceUpdate(){
//		generateDummyInfo();
		if(webService == null)
			webService = new TTWebService();
//		service.modifyStopInformation(stop);
		if(ttdb == null)
			ttdb = TTDBSingleton.getInstance(getApplicationContext());
		ArrayList<Route> listRoutes = (ArrayList<Route>)ttdb.getRoutesForStop(stop);
		info = webService.getNextPredictedRoutesCollection(stop,listRoutes , false);
		intent.putExtra(INTENT_KEY, info);
		sendBroadcast(intent);
	}
	
	
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onDestroy() {
		handler.removeCallbacks(servicesUpdater);
		super.onDestroy();
	}
	
	
}
