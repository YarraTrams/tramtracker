package com.yarratrams.tramtracker.services;

import java.util.ArrayList;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;

import com.yarratrams.tramtracker.objects.Favourite;
import com.yarratrams.tramtracker.objects.FavouritesGroup;

public class FavouritesService extends Service {
	public static final String FAVOURITES_SERVICE = "com.yarratrams.tramtracker.ui.FavouritesService";
	public static final String INTENT_KEY = "favourites_service";
	public static final String INTENT_ACTION = "action";
	public static final String INTENT_NEW_GROUP = "new_group";
	public static final String INTENT_NEW_FAVOURITE = "new_favourite";
	public static final String ACTION_GET_GROUPS = "action_get_groups";
	public static final String ACTION_SAVE_GROUP = "action_save_group";
	public static final String ACTION_SAVE_FAVOURITE = "action_save_favourite";
	private final Handler handler = new Handler();
	private Intent responseIntent;
	private Intent requestIntent;

	private ArrayList<FavouritesGroup> groups;



	@Override
	public void onCreate() {
		super.onCreate();
		responseIntent = new Intent(FAVOURITES_SERVICE);
		
		// TODO this line
//		generateDummyInfo();
	}


	@Override
	public void onStart(Intent intent, int startId) {
		requestIntent = intent;
		servicesUpdater.run();
	}


	private Runnable servicesUpdater = new Runnable() {
		public void run() {
			getServiceUpdate();
		}
	};

	private void getServiceUpdate(){
		// TODO retrieve info from web service
		// Using generateDummyInfo
		// info variable used to store services
		Bundle extras = requestIntent.getExtras();
		String action = extras.getString(INTENT_ACTION);
		
		// Get favourite groups with favourite stops
		if(action.equalsIgnoreCase(ACTION_GET_GROUPS)){
			responseIntent.putExtra(INTENT_KEY, groups);
			
		// Save the new favouirte
		} else if(action.equalsIgnoreCase(ACTION_SAVE_FAVOURITE)){
			 Favourite favourite = extras.getParcelable(INTENT_NEW_FAVOURITE);
			
		// Save the new group
		} else if(action.equalsIgnoreCase(ACTION_SAVE_GROUP)){
			FavouritesGroup group = extras.getParcelable(INTENT_NEW_GROUP);
			groups.add(group);
		}
		
		sendBroadcast(responseIntent);
	}


	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onDestroy() {
		handler.removeCallbacks(servicesUpdater);
		super.onDestroy();
	}



	// TODO Remove and replace this function
//	public void generateDummyInfo(){
//		groups = new ArrayList<FavouritesGroup>();
//		Stop stop = new Stop();
//		Favourite favourite = new Favourite();
//		FavouritesGroup group = new FavouritesGroup();
//		group.setName("My Group");
//
//		favourite.setName("Home");
//		stop.setStopName(getResources().getString(R.string.dummy_stop_stopName1));
//		stop.setStopDescription(getResources().getString(R.string.dummy_stop_streetName1));
//		stop.setCityDirection(getResources().getString(R.string.dummy_stop_streetDirection1));
//		stop.setStopNumber(Integer.parseInt(getResources().getString(R.string.dummy_stop_lineNumber1)));
//		stop.setTrackerID(Integer.parseInt(getResources().getString(R.string.dummy_stopID)));
//		String[] array = {"11","42","48","109","112"};
//		stop.setRoutes(array);
//		stop.setLatitudeE6(-37817778);
//		stop.setLongitudeE6(144961721);
//		favourite.setStop(stop);
//		favourite.setOrder(0);
//		group.addFavourite(favourite);
//
//		favourite = new Favourite();
//		favourite.setName("Work");
//		stop = new Stop();
//		stop.setStopName(getResources().getString(R.string.dummy_stop_stopName2));
//		stop.setStopDescription(getResources().getString(R.string.dummy_stop_streetName2));
//		stop.setCityDirection(getResources().getString(R.string.dummy_stop_streetDirection2));
//		stop.setStopNumber(Integer.parseInt(getResources().getString(R.string.dummy_stop_lineNumber2)));
//		stop.setTrackerID(Integer.parseInt(getResources().getString(R.string.dummy_stopID)));
//		String[] array2 = {"48","109","112"};
//		stop.setRoutes(array2);
//		stop.setLatitudeE6(-37817978);
//		stop.setLongitudeE6(144962721);
//		favourite.setStop(stop);
//		favourite.setOrder(1);
//		group.addFavourite(favourite);
//		groups.add(group);
//
//		group = new FavouritesGroup();
//		group.setName("My Second Group");
//		favourite = new Favourite();
//		favourite.setName("Work");		
//		stop = new Stop();
//		stop.setStopName(getResources().getString(R.string.dummy_stop_stopName3));
//		stop.setStopDescription(getResources().getString(R.string.dummy_stop_streetName3));
//		stop.setCityDirection(getResources().getString(R.string.dummy_stop_streetDirection3));
//		stop.setStopNumber(Integer.parseInt(getResources().getString(R.string.dummy_stop_lineNumber3)));
//		stop.setTrackerID(Integer.parseInt(getResources().getString(R.string.dummy_stopID)));
//		String[] array3 = {"55"};
//		stop.setRoutes(array3);
//		stop.setLatitudeE6(-37817378);
//		stop.setLongitudeE6(144961921);
//		favourite.setStop(stop);
//		favourite.setOrder(0);
//		group.addFavourite(favourite);
//		groups.add(group);
//	}


}