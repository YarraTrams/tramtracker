package com.yarratrams.tramtracker.services;

import java.util.ArrayList;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.os.IBinder;

import com.yarratrams.tramtracker.db.TTDB;
import com.yarratrams.tramtracker.objects.NearbyStop;
import com.yarratrams.tramtracker.objects.NearbyTicketOutlet;
import com.yarratrams.tramtracker.singleton.TTDBSingleton;

public class NearbyUpdateService extends Service {
	public static final String NEARBY_UPDATE = "com.yarratrams.tramtracker.ui.NearbyUpdate";
	public static final String INTENT_STOPS_KEY = "nearby_stop_update";
	public static final String INTENT_OUTLETS_KEY = "nearby_outlet_update";
	public static final String LOCATION_LATITUDE = "latitude";
	public static final String LOCATION_LONGITUDE = "longitude";
	
	private final Handler handler = new Handler();
	private Intent intent;
	private ArrayList<NearbyStop> nearbyStops;
	private ArrayList<NearbyTicketOutlet> nearbyOutlets;
	private LocationListener locationListener;
	public LocationManager locationManager;
	public static final String kLocationProviderNetwork = LocationManager.NETWORK_PROVIDER;
	public static final String kLocationProviderGPS = LocationManager.GPS_PROVIDER;
	
	private Location myPreviousLocation = null;
	
	@Override
	public void onCreate() {
		super.onCreate();
		intent = new Intent(NEARBY_UPDATE);
	}


	@Override
	public void onStart(Intent intent, int startId) {
//		handler.removeCallbacks(servicesUpdater);
		
		this.nearbyStops = new ArrayList<NearbyStop>();
		this.nearbyOutlets = new ArrayList<NearbyTicketOutlet>();
//		handler.post(servicesUpdater); // TODO set first update to 0
		
		getLocation();
	}
	
	
//	private Runnable servicesUpdater = new Runnable() {
//		public void run() {
//			getServiceUpdate();
//		}
//	};
	
	public void getLocation(){
		//retrieve user's position
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		
		// Define a listener that responds to location updates
		locationListener = new MyLocationListener(this);
		
//		Criteria crit2 = new Criteria();
//		crit2.setAccuracy(Criteria.ACCURACY_FINE);
//		String provider2 = locationManager.getBestProvider(crit2, false);
		
		// Register the listener with the Location Manager to receive location updates
		locationManager.requestLocationUpdates(kLocationProviderNetwork, 60000, 50, locationListener);
	}
	
	private void getNearbyStops(Location location){
		//Tram tracker database helper object
		TTDB tTDB = TTDBSingleton.getInstance(getApplicationContext());
		nearbyStops = tTDB.getNearbyStops(location);
		nearbyOutlets = tTDB.getNearbyTicketOutlets(location);
		intent.putExtra(LOCATION_LATITUDE, location.getLatitude() * 1E6);
		intent.putExtra(LOCATION_LONGITUDE, location.getLongitude() * 1E6);
		intent.putExtra(INTENT_STOPS_KEY, nearbyStops);
		intent.putExtra(INTENT_OUTLETS_KEY, nearbyOutlets);
		
		sendBroadcast(intent);
//		new CalculateDistance(location).execute();
	}
	
//	private void getServiceUpdate(){
//		
////		Location lastKnownLocation = locationManager.getLastKnownLocation(kLocationProviderNetwork);
//		
//		// TODO retrieve info from web service
//		
//		// Obtain GEOPOSITION of user
//		// generateDummyPosition
//		GeoPoint me = generateDummyPosition();
//		intent.putExtra(LOCATION_LATITUDE, me.getLatitudeE6());
//		intent.putExtra(LOCATION_LONGITUDE, me.getLongitudeE6());
//		
//		// Using generateDummyStops and generateDummyOutlets
//		// stops variable used to store objects of TramStop
//		// outlets variable used to store objects of TicketOutlet
//		generateDummyStops();
//		generateDummyOutlets();
//		intent.putExtra(INTENT_STOPS_KEY, stops);
//		intent.putExtra(INTENT_OUTLETS_KEY, outlets);
//		
//		sendBroadcast(intent);
//	}
	
	
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onDestroy() {
//		handler.removeCallbacks(servicesUpdater);
		//locationManager.removeUpdates(locationListener);
		super.onDestroy();
	}
	
	
	public void onUpdateLocation(Location location){
		getNearbyStops(location);	
	}
	
	public void fetchStopList(){
//		generateDummyStops();
//		generateDummyOutlets();
	}
	
//    /**
//     * Stop distance calculator
//     *
//     */
//	private class CalculateDistance extends AsyncTask<Void, Void, Void> {
//		
//		private final Location myLocation;
//		private boolean bFlag = true;
//		
//		public CalculateDistance(Location location){
//			myLocation = location;
//		}
//		
//		// Can use UI thread here
//		protected void onPreExecute() {
//
//		}
//
//		// Automatically done on worker thread (separate from UI thread)
//		protected Void doInBackground(Void... params) {
//			
//			if(myPreviousLocation == null)
//				bFlag = true;
//			else{
//				if(myPreviousLocation.distanceTo(myLocation) > 50){
//					bFlag = true;
//					myPreviousLocation = myLocation;
//				}
//				else
//					bFlag = false;
//			}
//			if(bFlag){
//				nearbyStops = new ArrayList<NearbyStop>();
//				int i;
//				double distance;
//				Location location = new Location("");
//				for(Stop stop : stops){
//					location.setLatitude(stop.getLatitudeE6()/1e6);
//					location.setLongitude(stop.getLongitudeE6()/1e6);
//					distance = myLocation.distanceTo(location);
//					if(distance < 500){
//						i=0;
//						for(; i<nearbyStops.size(); i++){
//							if(nearbyStops.get(i).getDblDistance() > distance)
//								break;
//						}
//
//						nearbyStops.add(i,new NearbyStop(stop,distance));
//					}
//				}
//				nearbyOutlets = new ArrayList<NearbyTicketOutlet>();
//				for(TicketOutlet to : outlets){
//					location.setLatitude(to.getLatitudeE6()/1e6);
//					location.setLongitude(to.getLongitudeE6()/1e6);
//					distance = myLocation.distanceTo(location);
//					if(distance < 500){
//						i=0;
//						for(; i<nearbyOutlets.size(); i++){
//							if(nearbyOutlets.get(i).getDblDistance() > distance)
//								break;
//						}
//						nearbyOutlets.add(i,new NearbyTicketOutlet(to,distance));
//					}
//				}
//			}
//			return null;
//		}
//		
//		// Can use UI thread here
//		protected void onPostExecute(Void vVoid) {
//			if(bFlag){
//				intent.putExtra(LOCATION_LATITUDE, myLocation.getLatitude() * 1E6);
//				intent.putExtra(LOCATION_LONGITUDE, myLocation.getLongitude() * 1E6);
////				System.out.println("nearbyStops size = " + nearbyStops.size() + " nearbystops "  + nearbyStops);
//				intent.putExtra(INTENT_STOPS_KEY, nearbyStops);
////				System.out.println("nearbyOutlets size = " + nearbyOutlets.size() + " nearbyOutlets " + nearbyOutlets);
//				intent.putExtra(INTENT_OUTLETS_KEY, nearbyOutlets);
//				sendBroadcast(intent);
//			}
//		}
//	}
//	// TODO Replace this function
//	public GeoPoint generateDummyPosition(){
//		// GET USER LOCATION
//		GeoPoint me = new GeoPoint(-37817778, 144961721);
//		return me;
//	}
//	
//	
//	// TODO Replace this function
//	public void generateDummyStops(){
//		Stop stop = new Stop();
//		
//		stop.setStopName(getResources().getString(R.string.dummy_stop_stopName1));
//		stop.setStopDescription(getResources().getString(R.string.dummy_stop_streetName1));
////		stop.setCityDirection(getResources().getString(R.string.dummy_stop_streetDirection1));
//		stop.setStopNumber(Integer.parseInt(getResources().getString(R.string.dummy_stop_lineNumber1)));
//		stop.setTrackerID(Integer.parseInt(getResources().getString(R.string.dummy_stopID)));
//		String[] array = {"11","42","48","109","112"};
//		stop.setRoutes(array);
//		stop.setLatitudeE6(-37817778);
//		stop.setLongitudeE6(144961721);
//		stops.add(stop);
//		
//		stop = new Stop();
//		stop.setStopName(getResources().getString(R.string.dummy_stop_stopName2));
//		stop.setStopDescription(getResources().getString(R.string.dummy_stop_streetName2));
////		stop.setCityDirection(getResources().getString(R.string.dummy_stop_streetDirection2));
//		stop.setStopNumber(Integer.parseInt(getResources().getString(R.string.dummy_stop_lineNumber2)));
//		stop.setTrackerID(Integer.parseInt(getResources().getString(R.string.dummy_stopID)));
//		String[] array2 = {"48","109","112"};
//		stop.setRoutes(array2);
//		stop.setLatitudeE6(-37817978);
//		stop.setLongitudeE6(144962721);
//		stops.add(stop);
//		
//		stop = new Stop();
//		stop.setStopName(getResources().getString(R.string.dummy_stop_stopName3));
//		stop.setStopDescription(getResources().getString(R.string.dummy_stop_streetName3));
////		stop.setCityDirection(getResources().getString(R.string.dummy_stop_streetDirection3));
//		stop.setStopNumber(Integer.parseInt(getResources().getString(R.string.dummy_stop_lineNumber3)));
//		stop.setTrackerID(Integer.parseInt(getResources().getString(R.string.dummy_stopID)));
//		String[] array3 = {"55"};
//		stop.setRoutes(array3);
//		stop.setLatitudeE6(-37817378);
//		stop.setLongitudeE6(144961921);
//		stops.add(stop);
//	}
//	
//	
//	// TODO Remove and replace this function
//	public void generateDummyOutlets(){
//		TicketOutlet outlet = new TicketOutlet();
//		
//		outlet.setOutletId(1);
//		outlet.setName(getResources().getString(R.string.dummy_outlet_name1));
//		outlet.setAddress(getResources().getString(R.string.dummy_outlet_address1));
//		outlet.setSuburb(getResources().getString(R.string.dummy_outlet_location));
////		outlet.setPostCode(Integer.parseInt(getResources().getString(R.string.dummy_outlet_pcode)));
//		outlet.setIs24Hours(true);
//		outlet.setHasMykiCard(false);
//		outlet.setHasMykiTopUp(true);
//		outlet.setHasMetcard(true);
//		outlet.setLatitudeE6(-37817778);
//		outlet.setLongitudeE6(144961721);
//		outlet.setDistance(50);
//		outlets.add(outlet);
//		
//		outlet = new TicketOutlet();
//		outlet.setOutletId(2);
//		outlet.setName(getResources().getString(R.string.dummy_outlet_name2));
//		outlet.setAddress(getResources().getString(R.string.dummy_outlet_address2));
//		outlet.setSuburb(getResources().getString(R.string.dummy_outlet_location));
////		outlet.setPostCode(Integer.parseInt(getResources().getString(R.string.dummy_outlet_pcode)));
//		outlet.setIs24Hours(true);
//		outlet.setHasMykiCard(false);
//		outlet.setHasMykiTopUp(true);
//		outlet.setHasMetcard(true);
//		outlet.setLatitudeE6(-37817978);
//		outlet.setLongitudeE6(144962721);
//		outlet.setDistance(200);
//		outlets.add(outlet);
//		
//		outlet = new TicketOutlet();
//		outlet.setOutletId(3);
//		outlet.setName(getResources().getString(R.string.dummy_outlet_name3));
//		outlet.setAddress(getResources().getString(R.string.dummy_outlet_address3));
//		outlet.setSuburb(getResources().getString(R.string.dummy_outlet_location));
////		outlet.setPostCode(Integer.parseInt(getResources().getString(R.string.dummy_outlet_pcode)));
//		outlet.setIs24Hours(true);
//		outlet.setHasMykiCard(false);
//		outlet.setHasMykiTopUp(true);
//		outlet.setHasMetcard(true);
//		outlet.setLatitudeE6(-37817378);
//		outlet.setLongitudeE6(144961921);
//		outlet.setDistance(350);
//		outlets.add(outlet);
//	}
	
}
