package com.yarratrams.tramtracker.webserviceinteraction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.text.format.DateFormat;
import android.util.Log;

import com.yarratrams.tramtracker.db.TTDBUpdate;
import com.yarratrams.tramtracker.objects.Constants;
import com.yarratrams.tramtracker.objects.NetworkMap;
import com.yarratrams.tramtracker.singleton.TTDBUpdateSingleton;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;
import com.yarratrams.tramtracker.ui.util.Console;
import com.yarratrams.tramtracker.ui.util.FeaturesPreferences;
import com.yarratrams.tramtracker.ui.util.NetworkMapPreferences;
import com.yarratrams.tramtracker.ui.util.UpdateDataPreference;

public class UpdateManager {

	private String token = "&tkn=3e982c6b-d57a-43fe-bff7-a44e9adb9e1e";
	private JSONObject updateStopsAndRoutes;
	private JSONArray updatePOIs, updateTicketOutlets, updateNetworkMaps; // Added
																			// for
																			// updating
																			// maps
	private TramTrackerMainActivity tramTrackerMainActivity;
	private SharedPreferences appSharedPrefs;
	private Editor prefsEditor;
	private UpdateManager parent = this;

	/*
	 * FREE TRAM ZONE
	 */
	NetworkMapPreferences networkMapPreferences;
	ArrayList<NetworkMap> arrlstNetworkMaps;
	boolean flag = false;

	/*
	 * adil added update2
	 */
	UpdateDataPreference updateDataPreference;
	private String authid = "4997245C-6D85-11E1-8E28-84224924019B";
	private String tokenid = "3e982c6b-d57a-43fe-bff7-a44e9adb9e1e";

	public void checkForUpdate(TramTrackerMainActivity callBack) {

		appSharedPrefs = callBack.getSharedPreferences(
				Constants.kSharedPrefIdentifier, Activity.MODE_PRIVATE);
		networkMapPreferences = new NetworkMapPreferences(callBack);
		prefsEditor = appSharedPrefs.edit();
		this.tramTrackerMainActivity = callBack;
		// Toast.makeText(tramTrackerMainActivity, "check update called",
		// 0).show();
		new UpdateCheckTask().execute();
	}

	public void displayErrorMessage(String strMessage) {
		Message message = new Message();
		Bundle bundle = new Bundle();
		bundle.putString("error", strMessage);
		message.setData(bundle);
		TramTrackerMainActivity.getAppManager().handlerToast
				.sendMessage(message);
	}

	public void update() {
		new UpdateDatabaseTask().execute(tramTrackerMainActivity);
	}

	// TODO Must add POI update too
	private class UpdateCheckTask extends AsyncTask<Void, Void, Boolean> {

		protected Boolean doInBackground(Void... params) {

			Log.w("UpdateCheckTask", "Update process started");
			Calendar presetCalendar = new GregorianCalendar(
					Constants.kLastUpdateYear, Constants.kLastUpdateMonth,
					Constants.kLastUpdateDayOfMonth);
			Calendar lastUpdateCalendar = new GregorianCalendar();
			lastUpdateCalendar.setTime(new Date(
					appSharedPrefs.getLong(Constants.kLastUpdateDate,
							presetCalendar.getTimeInMillis())));

			Calendar realDatabaseCalendar = new GregorianCalendar();
			if (presetCalendar.after(lastUpdateCalendar)) {
				realDatabaseCalendar = presetCalendar;
			} else {
				realDatabaseCalendar = lastUpdateCalendar;
			}
			/*
			 * for testing FTZ Change date to 02-07-2009 when scraping data
			 */
			// String strDate = (String)DateFormat.format("dd-MM-yyyy",
			// realDatabaseCalendar);

			// Changed in update2 by Adil
			String strDate = (String) DateFormat.format("yyyy-MM-dd",
					realDatabaseCalendar);
			// String strDate = "2009-07-02";

			/*
			 * Old Update Check Things
			 */

			/*
			 * String url = Constants.strURLBase +
			 * "GetStopsAndRoutesUpdatesSince" + "/" + strDate +
			 * Constants.strAID + token; Log.e("TT", url);
			 * System.out.println("-- GetStopsAndRoutesUpdatesSince url = " +
			 * url);
			 * 
			 * try{ //Get stop and route updates InputStream jsonData =
			 * getJSONData(url); if(jsonData != null){ JSONObject serviceData =
			 * parseJSONStream(jsonData); if(serviceData != null){
			 * if(!serviceData.getBoolean("hasError") &&
			 * serviceData.getBoolean("hasResponse")){ JSONObject responseObject
			 * = serviceData.getJSONObject("responseObject"); if(responseObject
			 * != null && responseObject != JSONObject.NULL){
			 * updateStopsAndRoutes = responseObject;
			 * if(responseObject.get("RouteChanges") == JSONObject.NULL &&
			 * responseObject.get("StopChanges") == JSONObject.NULL) {
			 * updateStopsAndRoutes = null;
			 * Log.v("TT","nothing to update on stops or route"); } else{
			 * Log.w("UpdateCheckTask","routes and stops update available");
			 * Log.v("TT",updateStopsAndRoutes.toString()); flag = true; } } } }
			 * } //Get ticket outlet updates url = Constants.strURLBase +
			 * "GetTicketOutletChangesSince" + "/" + strDate + Constants.strAID
			 * + token; System.out.println("-- ticket outlet update URL = " +
			 * url); Log.e("TT", url); jsonData = getJSONData(url); if(jsonData
			 * != null){ JSONObject serviceData = parseJSONStream(jsonData);
			 * if(serviceData != null){ if(!serviceData.getBoolean("hasError")
			 * && serviceData.getBoolean("hasResponse")){ JSONArray
			 * responseObject = serviceData.getJSONArray("responseObject");
			 * if(responseObject != null && responseObject != JSONObject.NULL){
			 * updateTicketOutlets = responseObject;
			 * Log.w("UpdateCheckTask","ticket outlets update available"); flag
			 * = true; } } } } //Get POI updates url = Constants.strURLBase +
			 * "GetPointsOfInterestChangesSince" + "/" + strDate +
			 * Constants.strAID + token;
			 * System.out.println("-- GetPointsOfInterestChangesSince URL = " +
			 * url); Log.e("TT", url); jsonData = getJSONData(url); if(jsonData
			 * != null){ JSONObject serviceData = parseJSONStream(jsonData);
			 * if(serviceData != null){ if(!serviceData.getBoolean("hasError")
			 * && serviceData.getBoolean("hasResponse")){ JSONArray
			 * responseObject = serviceData.getJSONArray("responseObject");
			 * if(responseObject != null && responseObject != JSONObject.NULL){
			 * updatePOIs = responseObject;
			 * Log.w("UpdateCheckTask","pois update available"); flag = true; }
			 * } } }
			 */

			try {

				String url = "";
				InputStream jsonData;

				InputStream jsonDataFeatures;
				/*
				 * s Get new Global update Api response and save it in shared
				 * pref for next time use
				 */

				// original url

				url = Constants.strUpdateURLBase + "updates/" + strDate
						+ "?authid=" + authid + "&token=" + tokenid;

				// testing urls
				// url =
				// "http://qa-json.tramtracker.com.au/TramTrackerWebAPI/api/updates/2015-05-22?authid=4997245C-6D85-11E1-8E28-84224924019B&token=3e982c6b-d57a-43fe-bff7-a44e9adb9e1e";

				// url =
				// "http://qa-json.tramtracker.com.au/TramTrackerWebAPI/api/updates/2015-04-04?authid=4997245C-6D85-11E1-8E28-84224924019B&token=3e982c6b-d57a-43fe-bff7-a44e9adb9e1e";
				// url
				// ="http://qa-json.tramtracker.com.au/TramTrackerWebAPI/api/updates/2014-12-31?authid=4997245C-6D85-11E1-8E28-84224924019B&token=3e982c6b-d57a-43fe-bff7-a44e9adb9e1e";
				// url =
				// "http://qa-json.tramtracker.com.au/TramTrackerWebAPI/api/updates/2015-01-01?authid=4997245C-6D85-11E1-8E28-84224924019B&token=3e982c6b-d57a-43fe-bff7-a44e9adb9e1e";
				// url =
				// "http://ws2.tramtracker.com.au/TramTrackerWebAPI/api/updates/2015-01-23?authid=4997245C-6D85-11E1-8E28-84224924019B&token=3e982c6b-d57a-43fe-bff7-a44e9adb9e1e";

				System.out.println("-- global update url: " + url);

				jsonData = getJSONData(url);
				updateDataPreference = new UpdateDataPreference(
						tramTrackerMainActivity);
				updateDataPreference.setIsUpdateAvailable(false);
				updateDataPreference.setIsUpdateCompleted(false);
				if (jsonData != null) {

					JSONObject updateData = parseJSONStream(jsonData);

					if (updateData != null) {
						System.out.println("-- global update data: "
								+ updateData.toString());
						updateDataPreference.setUpdateData(updateData
								.toString());

						
						if (updateData.getJSONArray("updatedStops").length() > 0
								|| updateData.getJSONArray("deletedStops").length() > 0
								|| updateData.getJSONArray("updatedRoutes").length() > 0
								|| updateData.getJSONArray("deletedRoutes").length() > 0
								|| updateData.getJSONArray("updatedTicketOutlets").length() > 0
								|| updateData.getJSONArray("deletedTicketOutlets").length() > 0
								|| updateData.getJSONArray("updatedPoi").length() > 0
								|| updateData.getJSONArray("deletedPoi").length() > 0) {
							flag = true;
						}else{
							flag = false;
						}

						updateDataPreference.setIsUpdateAvailable(flag);

					} else {

						System.out.println("-- no update data");
					}

				}

				// get features from server
				// moved to a separate async task UpdateFeatureTask

				// url =
				// "http://qa-json.tramtracker.com.au/TramTrackerWebAPI/api/Features/"+strDate+"?authid="+authid+"&token="+
				// tokenid;
				// Console.print("features url:" + url);
				//
				// jsonDataFeatures = getJSONData(url);
				// FeaturesPreferences featuresPreferences = new
				// FeaturesPreferences(TramTrackerMainActivity.instance);
				// Console.print("is  input stream null: " + ((jsonDataFeatures
				// == null) ? "true" : "false"));
				// if(jsonDataFeatures!=null)
				// {
				// JSONArray jaFeatureData =
				// parseJSONStreamtToArray(jsonDataFeatures);
				//
				// for(int i = 0 ; i < jaFeatureData.length(); i++)
				// {
				// JSONObject jsonObject = jaFeatureData.getJSONObject(i);
				// if(jsonObject.getString("featureName").equalsIgnoreCase("Advertising"))
				// {
				// Console.print(" ads enable: " +
				// jsonObject.getBoolean("isEnabled"));
				// featuresPreferences.setAdsflag(jsonObject.getBoolean("isEnabled"));
				// }
				// }
				//
				// }
				// else
				// {
				// Console.print("I am in false");
				// }
				//

				// ------------------------------------------------------------------------------------------------------------------------------------------------

				// Get Network Map Updates
				/*
				 * Free Tram ZOne
				 */
				/*
				 * url = Constants.strURLBase + "GetLatestNetworkMaps" + "/" +
				 * Constants.strAID + token; System.out.println("-- map url: "+
				 * url);
				 * 
				 * 
				 * // url =
				 * "http://extranetdev.yarratrams.com.au/pidsservicewcf/RestService/GetLatestNetworkMaps/?aid=TTRESTTEST&cid=2&tkn=3e982c6b-d57a-43fe-bff7-a44e9adb9e1e"
				 * ; // url =
				 * "http://private-6fbf8-sodastream.apiary-mock.com/get_maps";
				 * 
				 * jsonData = getJSONData(url); if(jsonData != null){ JSONObject
				 * serviceData = parseJSONStream(jsonData); if(serviceData !=
				 * null){ if(!serviceData.getBoolean("hasError") &&
				 * serviceData.getBoolean("hasResponse")){ JSONArray
				 * responseObject = serviceData.getJSONArray("responseObject");
				 * if(responseObject != null && responseObject !=
				 * JSONObject.NULL){ updateNetworkMaps = responseObject;
				 * 
				 * 
				 * 
				 * Log.v("TT", updateNetworkMaps.toString());
				 * networkMapPreferences = new
				 * NetworkMapPreferences(tramTrackerMainActivity);
				 * 
				 * 
				 * if(networkMapPreferences.getNetworkMaps().length() < 1) {
				 * 
				 * Log.v("TT","in if");
				 * 
				 * networkMapPreferences.setNetworkMapList(updateNetworkMaps.
				 * toString()); arrlstNetworkMaps =
				 * networkMapPreferences.getNetworkMapList();
				 * 
				 * Log.v("TT","saved arrlst: "+arrlstNetworkMaps.size());
				 * Log.v("TT"
				 * ,"saved data: "+networkMapPreferences.getNetworkMaps());
				 * 
				 * 
				 * if(arrlstNetworkMaps.size() == 1 &&
				 * arrlstNetworkMaps.get(0).getFileName
				 * ().equalsIgnoreCase(NetworkMapActivity.bundleMap)) { flag =
				 * false; } else { flag = true; } } else {
				 * Log.v("TT","in else"); ArrayList<NetworkMap>
				 * arrlstTempUpdateNetworkMaps;
				 * 
				 * arrlstTempUpdateNetworkMaps =
				 * NetworkMap.getArraylistFromJSOnArray
				 * (updateNetworkMaps.toString());
				 * 
				 * 
				 * 
				 * arrlstNetworkMaps =
				 * networkMapPreferences.getNetworkMapList();
				 * 
				 * Log.v("TT","temp  arrlst: "+arrlstTempUpdateNetworkMaps.size()
				 * ); Log.v("TT","saved arrlst: "+arrlstNetworkMaps.size());
				 * Log.
				 * v("TT","saved data: "+networkMapPreferences.getNetworkMaps
				 * ());
				 * 
				 * 
				 * addNewMaps(arrlstTempUpdateNetworkMaps);
				 * networkMapPreferences.setNetworkMapList(arrlstNetworkMaps);
				 * 
				 * 
				 * Log.v("TT","2temp  arrlst: "+arrlstTempUpdateNetworkMaps.size(
				 * )); Log.v("TT","2saved arrlst: "+arrlstNetworkMaps.size());
				 * Log
				 * .v("TT","2saved data: "+networkMapPreferences.getNetworkMaps
				 * ());
				 * 
				 * //After comparing response, (if applicable), recheck for any
				 * new maps added for(int i = 0; i <
				 * arrlstNetworkMaps.size();i++) {
				 * 
				 * System.out.println("-- map checker: " +
				 * !arrlstNetworkMaps.get
				 * (i).getFileName().equalsIgnoreCase(NetworkMapActivity
				 * .bundleMap)); if(arrlstNetworkMaps.get(i).getIsMapAvailable()
				 * == 0 &&
				 * !arrlstNetworkMaps.get(i).getFileName().equalsIgnoreCase
				 * (NetworkMapActivity.bundleMap)) { flag = true; } }
				 * 
				 * }
				 * 
				 * // flag = true; } } } }// ends here
				 */

			} catch (Exception e) {
				e.printStackTrace();
			}

			/*
			 * FREE TRAM ZONE : map downloads method called here to invoke auto
			 * map download
			 */
			// try {
			// downloadNetworkMaps();
			// } catch (Exception e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }

			return flag;
		}

		// public void playGround()
		// {
		// String data = "/Date(1420030800000+1100)/";
		// String date = "";
		// date = data.substring(data.indexOf("(")+1, data.indexOf("+"));
		// Date date2 = new Date((Long.parseLong(date)));
		// System.out.println("date : " + date2);
		//
		// }

		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try {
				if (result == true) {
//					tramTrackerMainActivity.notifyUpdateAvailable(parent);
					// System.out.println("Update available");
				}
				// else
				// System.out.println("No update available");
			} catch (Exception e) {
				// System.out.println("Exception handled");
				e.printStackTrace();
			}
		}

		/**
		 * Download data from JSON service
		 */
		private InputStream getJSONData(String url) {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			URI uri;
			InputStream data = null;
			try {
				uri = new URI(url);
				HttpGet method = new HttpGet(uri);
				HttpResponse response;
				response = httpClient.execute(method);
				data = response.getEntity().getContent();
			} catch (URISyntaxException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return data;
		}

		/**
		 * Parse InputStream into JSONObject.
		 */
		private JSONObject parseJSONStream(InputStream is) {
			JSONObject jsonObject = null;
			try {
				Console.print("trying to parse1");
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(is));
				StringBuilder sb = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null) {
					sb.append(line);
				}
				is.close();
				String jsonData = sb.toString();
				jsonObject = new JSONObject(jsonData);
				Console.print("trying to parse2");
			} catch (Exception e) {
				e.printStackTrace();
				Console.print("error in parsing: " + e.getMessage());
			}
			return jsonObject;
		}

	}

	/*
	 * Adil added to parse to json Array
	 */

	/**
	 * Parse InputStream into JSONObject.
	 */
	private JSONArray parseJSONStreamtToArray(InputStream is) {
		JSONArray jsonArray = null;
		try {
			Console.print("trying to parse1");
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(is));
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			is.close();
			String jsonData = sb.toString();
			jsonArray = new JSONArray(jsonData);
			Console.print("trying to parse2");
		} catch (Exception e) {
			e.printStackTrace();
			Console.print("error in parsing: " + e.getMessage());
		}
		return jsonArray;
	}

	private class UpdateDatabaseTask extends AsyncTask<Context, Void, Boolean> {
		TTDBUpdate ttdbUpdate;
		TTWebService webservice = TTWebServiceSingleton.getInstance();
		protected Boolean doInBackground(Context... params) {
			Boolean result = true;
			try {

				/*
				 * FREE TRAM ZONE : downloading new maps Line commented below to
				 * check auto map download.
				 */

				// downloadNetworkMaps();

				ttdbUpdate = TTDBUpdateSingleton
						.getTTDBUpdateInstance(params[0]);
				if (updateStopsAndRoutes != null) {
					// ArrayList<Integer> alUpdateRoutes = new
					// ArrayList<Integer>();
					// ArrayList<Integer> alDeleteRoutes = new
					// ArrayList<Integer>();

					JSONArray arrRoutes = null, arrStops = null;
					JSONObject route, stop;
					if (updateStopsAndRoutes.get("RouteChanges") instanceof JSONArray)
						arrRoutes = updateStopsAndRoutes
								.getJSONArray("RouteChanges");
					ArrayList<Integer> alStops = new ArrayList<Integer>();
					if (arrRoutes != null && arrRoutes != JSONObject.NULL) {
						JSONObject directions, response;
						JSONArray routeStops;
						if (arrRoutes.length() > 0) {
							for (int i = 0; i < arrRoutes.length(); i++) {
								route = arrRoutes.getJSONObject(i);
								if (route.getString("Action").equalsIgnoreCase(
										"DELETE")) {
									result = ttdbUpdate
											.deleteRouteViaUpdate(route);
									if (result == false)
										return result;
								} else {
									directions = webservice
											.getDestinationsForRoute(route
													.getString("ID"));
									if (!directions.getBoolean("hasError")
											&& directions
													.getBoolean("hasResponse")) {
										response = webservice
												.getAllStopsForRoute(route
														.getString("ID"));
										if (!response.getBoolean("hasError")
												&& response
														.getBoolean("hasResponse")) {
											routeStops = response
													.getJSONArray("responseObject");
											if (routeStops != null
													&& routeStops != JSONObject.NULL) {
												// result =
												// ttdbUpdate.deleteRouteViaUpdate(route);
												result = true;
												if (result) {
													result = ttdbUpdate
															.createRouteViaUpdate(
																	route,
																	directions
																			.getJSONArray(
																					"responseObject")
																			.getJSONObject(
																					0),
																	routeStops,
																	alStops);
													if (!result)
														return result;
												} else {
													return result;
												}
											}
										}
									}
								}
							}
						}
					}

					ArrayList<Integer> alUpdateStops = new ArrayList<Integer>();
					ArrayList<Integer> alDeleteStops = new ArrayList<Integer>();
					ArrayList<JSONArray> alUpdateObjects = new ArrayList<JSONArray>();
					JSONObject updateObject;
					if (updateStopsAndRoutes.get("StopChanges") instanceof JSONArray)
						arrStops = updateStopsAndRoutes
								.getJSONArray("StopChanges");
					if (arrStops != null && arrStops != JSONObject.NULL) {
						if (arrStops.length() > 0) {
							for (int i = 0; i < arrStops.length(); i++) {
								stop = arrStops.getJSONObject(i);
								if (stop.getString("Action").equalsIgnoreCase(
										"UPDATE")) {
									updateObject = webservice
											.getStopForTrackerId(stop
													.getString("StopNo"));
									if (!updateObject.getBoolean("hasError")
											&& updateObject
													.getBoolean("hasResponse")) {

										alUpdateObjects
												.add(updateObject
														.getJSONArray("responseObject"));
										alUpdateStops
												.add(stop.getInt("StopNo"));
									} else {
										// Log.e("UpdateDatabaseTask doInBackground",
										// "stopId = " +
										// stop.getString("StopNo") +
										// " response = " + updateObject);
										// result = false;
										// return null;
									}

								} else {
									alDeleteStops.add(stop.getInt("StopNo"));
								}
							}
							System.out.println("alUpdateStops = "
									+ alUpdateStops);
							System.out.println("alDeleteStops = "
									+ alDeleteStops);
							System.out.println("alUpdateObjects = "
									+ alUpdateObjects);
							result = ttdbUpdate.updateStopsViaUpdate(
									alUpdateStops, alDeleteStops,
									alUpdateObjects);
							if (!result) {
								displayErrorMessage("Error updating stops.");
								return result;
							}
						}
					}
				}
				// updateTicketOutlets = null;
				if (updateTicketOutlets != null) {
					if (updateTicketOutlets != null
							&& updateTicketOutlets != JSONObject.NULL) {
						JSONObject ticketOutlet;
						ArrayList<Integer> alAddTicketOutletId = new ArrayList<Integer>();
						ArrayList<Integer> alDeleteTicketOutletId = new ArrayList<Integer>();
						ArrayList<JSONObject> alObjects = new ArrayList<JSONObject>();
						JSONObject object;
						for (int i = 0; i < updateTicketOutlets.length(); i++) {
							ticketOutlet = updateTicketOutlets.getJSONObject(i);
							if (ticketOutlet.getString("Action")
									.equalsIgnoreCase("UPDATE")) {
								alAddTicketOutletId.add(new Integer(
										ticketOutlet.getInt("ID")));
								object = webservice
										.getTicketOutletForId(ticketOutlet
												.getString("ID"));
								alObjects.add(object);
							} else if (ticketOutlet.getString("Action")
									.equalsIgnoreCase("DELETE")) {
								alDeleteTicketOutletId.add(new Integer(
										ticketOutlet.getInt("ID")));
							}
						}
						result = ttdbUpdate.updateTicketOutletsViaUpdate(
								alAddTicketOutletId, alDeleteTicketOutletId,
								alObjects);
						ttdbUpdate
								.createStopsAndNearbyOutletsViaUpdate(alAddTicketOutletId);
						if (!result) {
							displayErrorMessage("Error updating ticket outlets.");
							return result;
						}
					}
				}
				// updatePOIs = null;
				if (updatePOIs != null) {
					if (updatePOIs != null && updatePOIs != JSONObject.NULL) {
						JSONObject poi;
						ArrayList<Integer> alAddPoiId = new ArrayList<Integer>();
						ArrayList<Integer> alDeletePoiId = new ArrayList<Integer>();
						ArrayList<JSONObject> alPoiObjects = new ArrayList<JSONObject>();
						ArrayList<JSONObject> alPoiStops = new ArrayList<JSONObject>();
						JSONObject object, poisStopObject;
						for (int i = 0; i < updatePOIs.length(); i++) {
							poi = updatePOIs.getJSONObject(i);
							if (poi.getString("Action").equalsIgnoreCase(
									"UPDATE")) {
								object = webservice.getPoiForId(poi
										.getString("POIId"));
								if (!object.getBoolean("hasError")
										&& object.getBoolean("hasResponse")) {
									poisStopObject = webservice
											.getStopsForPoiId(poi
													.getString("POIId"));
									if (!poisStopObject.getBoolean("hasError")
											&& poisStopObject
													.getBoolean("hasResponse")) {
										alAddPoiId.add(new Integer(poi
												.getInt("POIId")));
										alPoiObjects.add(object);
										alPoiStops.add(poisStopObject);
									}
								}
							} else if (poi.getString("Action")
									.equalsIgnoreCase("DELETE")) {
								alDeletePoiId.add(new Integer(poi
										.getInt("POIId")));
							}
						}
						result = ttdbUpdate.updatePOIsViaUpdate(alAddPoiId,
								alDeletePoiId, alPoiObjects, alPoiStops);
						if (!result) {
							displayErrorMessage("Error updating points of interest.");
							return result;
						}
					}
				}

				if (result) {
					// System.out.println("Updated");
					Calendar calNow = Calendar.getInstance();
					calNow.add(Calendar.DAY_OF_MONTH, 1);
					prefsEditor.putLong(Constants.kLastUpdateDate,
							calNow.getTimeInMillis());
					displayErrorMessage("Update completed");
					prefsEditor.commit();
				}
				ttdbUpdate.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return result;
		}

		// private JSONArray getRouteChanges(JSONObject responseObject){
		// JSONArray jaStopChanges = null;
		// try {
		// jaStopChanges = responseObject.getJSONArray("StopChanges");
		// if(jaStopChanges == JSONObject.NULL)
		// jaStopChanges = null;
		// } catch (JSONException e) {
		// e.printStackTrace();
		// }
		// return jaStopChanges;
		// }

		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			// TODO call ui method for indicating availability of update
			tramTrackerMainActivity.notifyFinishUpdate(result);

			// checking downloaded image

			// Bitmap bitmap = loadBitmap(tramTrackerMainActivity,
			// arrlstNetworkMaps.get(0).getFileName());
			//
			// AlertDialog.Builder builder = new
			// Builder(tramTrackerMainActivity);
			// final ImageView imageView = new
			// ImageView(tramTrackerMainActivity);
			//
			// imageView.setImageBitmap(bitmap);
			// builder.setView(imageView);
			// AlertDialog alertDialog = builder.create();
			// alertDialog.show();
		}
	}

	/*
	 * FREE TRAM ZONE
	 */

	// public void addNewMaps(ArrayList<NetworkMap> arrlstTempUpdateNetworkMaps)
	// {
	// // TODO Auto-generated method stub
	//
	//
	//
	// for(int o = 0; o < arrlstTempUpdateNetworkMaps.size(); o++)
	// {
	//
	// boolean isNew = true;
	//
	// for (int i = 0; i < arrlstNetworkMaps.size() ; i++)
	// {
	// if(arrlstNetworkMaps.get(i).getFileName().equals(arrlstTempUpdateNetworkMaps.get(o).getFileName()))
	// {
	// Log.v("TT","compare: "+arrlstNetworkMaps.get(i).getFileName().equals(arrlstTempUpdateNetworkMaps.get(o).getFileName()));
	// isNew = false;
	//
	// }
	// }
	// if(isNew) {
	// arrlstNetworkMaps.add(arrlstTempUpdateNetworkMaps.get(o));
	// }
	//
	// }
	//
	//
	//
	//
	//
	// }

	// public void downloadNetworkMaps()
	// {
	//
	// for (int i = 0; i < arrlstNetworkMaps.size(); i++) {
	//
	// if(arrlstNetworkMaps.get(i).getIsMapAvailable() == 0)
	// {
	//
	// HttpURLConnection connection = null;
	// InputStream is = null;
	//
	//
	// try {
	//
	// // String url =
	// "http://extranetdev.yarratrams.com.au/TTMetaContent/assets/network_map_201412081141.png";
	// URL get_url = new URL(arrlstNetworkMaps.get(i).getUrl());
	//
	// // URL get_url = new URL(url);
	//
	// Log.v("TT", "-- url for image : " + arrlstNetworkMaps.get(i).getUrl());
	//
	// connection = (HttpURLConnection) get_url.openConnection();
	// // connection.setDoInput(true);
	// connection.setDoOutput(false);
	// connection.connect();
	// is = new BufferedInputStream(connection.getInputStream());
	// final Bitmap bitmap = BitmapFactory.decodeStream(is);
	//
	// saveFile(tramTrackerMainActivity, bitmap,
	// arrlstNetworkMaps.get(i).getFileName(), i);
	//
	// connection.disconnect();
	// is.close();
	//
	// }
	//
	//
	// // catch (MalformedURLException e)
	// // {
	// // e.printStackTrace();
	// // }
	// // catch (IOException e)
	// // {
	// // e.printStackTrace();
	// // }
	// catch (Exception e) {
	// // TODO: handle exception
	// e.printStackTrace();
	// }
	//
	//
	//
	// }
	//
	// }
	//
	//
	// try {
	// networkMapPreferences.setNetworkMapList(arrlstNetworkMaps);
	// } catch (JSONException e) {
	// // TODO Auto-generated catch block
	// Log.v("TT", "error in saving bk data: " + e.getMessage());
	// e.printStackTrace();
	// }
	// catch (Exception e) {
	// // TODO: handle exception
	// }
	//
	//
	// }
	//
	//
	// public void saveFile(Context context, Bitmap b, String picName, int i){
	// FileOutputStream fos;
	// try {
	// fos = context.openFileOutput(picName, Context.MODE_PRIVATE);
	// b.compress(Bitmap.CompressFormat.PNG, 100, fos);
	// fos.close();
	// arrlstNetworkMaps.get(i).setIsMapAvailable(1);
	//
	// }
	// catch (FileNotFoundException e) {
	// Log.d("TT", "file not found");
	// e.printStackTrace();
	// }
	// catch (IOException e) {
	// Log.d("TT", "io exception");
	// e.printStackTrace();
	// }
	// catch (Exception e) {
	// // TODO: handle exception
	// Log.d("TT","  exception");
	// }
	//
	// }

	// public Bitmap loadBitmap(Context context, String picName){
	// Bitmap b = null;
	// FileInputStream fis;
	// try {
	// fis = context.openFileInput(picName);
	//
	// b = BitmapFactory.decodeStream(fis);
	// fis.close();
	//
	// }
	// catch (FileNotFoundException e) {
	// Log.d("TT", "file not found");
	// e.printStackTrace();
	// }
	// catch (IOException e) {
	// Log.d("TT", "io exception");
	// e.printStackTrace();
	// }
	// return b;
	// }

}
