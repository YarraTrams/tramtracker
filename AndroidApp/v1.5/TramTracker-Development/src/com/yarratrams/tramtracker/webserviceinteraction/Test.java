package com.yarratrams.tramtracker.webserviceinteraction;

import java.util.ArrayList;

import android.content.Context;

import com.yarratrams.tramtracker.db.TTDB;
import com.yarratrams.tramtracker.objects.Route;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.objects.Tram;

public class Test {
	public static void testWebServiceGetNextPredictedRoutesCollection(Context context){
		Stop stop = new Stop();
		stop.setTrackerID(1232);
		TTWebService service = new TTWebService();
		service.modifyStopInformation(stop);
//		System.out.println("Stop downloaded = " + stop);
		TTDB ttdb = new TTDB(context);
		ArrayList<Route> listRoutes = (ArrayList<Route>)ttdb.getRoutesForStop(stop);
		service.getNextPredictedRoutesCollection(stop,listRoutes , false);
		ArrayList<Tram> alTram;
		for(Route route : listRoutes){
			alTram = route.getNextServices();
//			System.out.println("Route = " + route + " TramArray info = " + alTram);
		}
	}
	
	public static void testDBGetRoutes(Context context){
		Stop stop = new Stop();
		stop.setTrackerID(1232);
		TTDB ttdb = new TTDB(context) ;
		ArrayList<Route> listRoutes = (ArrayList<Route>)ttdb.getRoutesForStop(stop);
	}
}
