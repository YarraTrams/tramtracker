package com.yarratrams.tramtracker.tasks;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;
import com.yarratrams.tramtracker.objects.Constants;
import com.yarratrams.tramtracker.ui.SettingsActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;
import com.yarratrams.tramtracker.ui.util.CustomProgressDialog;
import com.yarratrams.tramtracker.ui.util.DisruptionsPreference;
import com.yarratrams.tramtracker.ui.util.SettingsPreference;
import com.yarratrams.tramtracker.ui.util.UniqueIdPref;

public class PushNotificationTask extends AsyncTask < String, String, Boolean > {



	Context context;

	ProgressDialog progressDialog;
	CustomProgressDialog customProgressDialog;
	SettingsPreference settingsPreference;


	HttpClient httpClient;

	HttpResponse httpResponse;
	StringEntity stringEntity;
	JSONObject jsonObject;

	String content = "";
	String Error = "";
	boolean register;
	UniqueIdPref uniqueIdPref;


	public PushNotificationTask(Context context, boolean register) {
		// TODO Auto-generated constructor stub

		this.context = context;
		this.register = register;
		uniqueIdPref = new UniqueIdPref(context);
		settingsPreference = new SettingsPreference(context);


	}




	@
	Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();


		//customProgressDialog = new  CustomProgressDialog(context);
		//customProgressDialog.show();
	}




	@
	Override
	protected Boolean doInBackground(String...params) {


		try {

			String url = Constants.strUpdateURLBase + "Registrations/?token=3e982c6b-d57a-43fe-bff7-a44e9adb9e1e&authid=4997245C-6D85-11E1-8E28-84224924019B";

			System.out.println("-- push notification reg url: " + url);

			httpClient = new DefaultHttpClient();
			jsonObject = new JSONObject();



			//if (register == true && uniqueIdPref.getFirstTimeRegistration() == true) {
			if (register == true ) {
				System.out.println("-- first time registeration");

				HttpPost httpPost;
				httpPost = new HttpPost(url);
				jsonObject.put("deviceId", uniqueIdPref.getUniqueId());
				jsonObject.put("notificationToken", GCMRegistrar.getRegistrationId(context));
				jsonObject.put("deviceType", "Android");
				System.out.println("-- JSON Object : " + jsonObject.toString());

				stringEntity = new StringEntity(jsonObject.toString());
				stringEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

				httpPost.setEntity(stringEntity);

				httpResponse = httpClient.execute(httpPost);

				//            } else if (register == true && uniqueIdPref.getFirstTimeRegistration() == false) {
				//
				//                System.out.println("-- update registeration");
				//
				//
				//
				//                url = "http://qa-json.tramtracker.com.au/TramTrackerWebAPI/api/Registrations/" + uniqueIdPref.getUniqueId() + "?token=3e982c6b-d57a-43fe-bff7-a44e9adb9e1e&authid=4997245C-6D85-11E1-8E28-84224924019B";
				//
				//                HttpPut httpPut;
				//                httpPut = new HttpPut(url);
				//                jsonObject.put("deviceId", uniqueIdPref.getUniqueId());
				//                jsonObject.put("notificationToken", GCMRegistrar.getRegistrationId(context));
				//                jsonObject.put("deviceType", "Android");
				//                System.out.println("-- JSON Object : " + jsonObject.toString());
				//
				//                stringEntity = new StringEntity(jsonObject.toString());
				//                stringEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
				//
				//                httpPut.setEntity(stringEntity);
				//
				//                httpResponse = httpClient.execute(httpPut);

			} else if (register == false) {
				url = Constants.strUpdateURLBase + "Registrations/" + uniqueIdPref.getUniqueId() + "?token=3e982c6b-d57a-43fe-bff7-a44e9adb9e1e&authid=4997245C-6D85-11E1-8E28-84224924019B";

				System.out.println("-- delete registeration");

				HttpDelete httpDelete;
				httpDelete = new HttpDelete(url);




				httpResponse = httpClient.execute(httpDelete);

			}




			System.out.println("-- code: " + httpResponse.getStatusLine().getStatusCode());
			content = EntityUtils.toString(httpResponse.getEntity());
			

			System.out.println("-- content: " + content);

			try {
				JSONObject jsonCheckResponse = new JSONObject(content);


				if (jsonCheckResponse.has("msg")) {
					if (jsonCheckResponse.getString("msg").equalsIgnoreCase("success")) {
						return true;
					} else {
						return false;
					}
				} else {
					return false;
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("-- data in content: " + content);
				return false;
			}






		} catch (ClientProtocolException e) {
			Error = "ClientProtocolException: " + e.getMessage();
			return false;
		} catch (IOException e) {
			Error = "IOException: " + e.getMessage();
			return false;
		} catch (Exception e) {

			Error = e.getMessage();
			e.printStackTrace();
			return false;
		}


	}




	@
	Override
	protected void onPostExecute(Boolean result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);

		if (result) {



			//            if (register == true && uniqueIdPref.getFirstTimeRegistration() == true) {
			if (register == true ) {

				uniqueIdPref.setFirstTimeRegistration(false);
				Toast.makeText(TramTrackerMainActivity.instance, "Registration successful", 0).show();

				//            } else if (register == true && uniqueIdPref.getFirstTimeRegistration() == false) {
				//
				//                Toast.makeText(TramTrackerMainActivity.instance, "Registration successful", 0).show();

			} else if (register == false) {
				Toast.makeText(TramTrackerMainActivity.instance, "Unregistration successful", 0).show();
				DisruptionsPreference disruptionsPreference = new DisruptionsPreference(context);
				disruptionsPreference.setRoutes(null);
				disruptionsPreference.setTimmings(null);


			}




		} else {
			System.out.println(" -- Error : " + Error);
			boolean status = settingsPreference.getReciveNotifications();

			settingsPreference.setReciveNotifications(!status);




		}

		//progressDialog.dismiss();
		//customProgressDialog.dismiss();	
		if(SettingsActivity.progressDialog!=null){
			if (SettingsActivity.progressDialog.isShowing()) {
				SettingsActivity.progressDialog.dismiss();
			}
		}
	}

}