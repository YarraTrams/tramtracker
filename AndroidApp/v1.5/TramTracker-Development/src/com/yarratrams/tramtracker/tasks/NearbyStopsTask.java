package com.yarratrams.tramtracker.tasks;

import java.util.ArrayList;
import java.util.Date;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.db.TTDB;
import com.yarratrams.tramtracker.objects.Constants;
import com.yarratrams.tramtracker.objects.NearbyStop;
import com.yarratrams.tramtracker.objects.NearbyStopsLimitedByNoOfStops;
import com.yarratrams.tramtracker.objects.NearbyTicketOutlet;
import com.yarratrams.tramtracker.singleton.TTDBSingleton;
import com.yarratrams.tramtracker.ui.NearbyActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;

public class NearbyStopsTask extends AsyncTask<Void, Void, Void> {
	private LocationManager locationManager;
	private MyLocationListener locationListener;
	private Location location = null;
	private ArrayList<NearbyStop> nearbyStops;
	private ArrayList<NearbyStop> shelterStops;
	private ArrayList<NearbyStop> easyStops;
	private ArrayList<NearbyTicketOutlet> nearbyOutlets;
	NearbyActivity nearbyActivity;
	boolean bestLocationRetrieved = false;
	public boolean islocationServiceAvailable = true;
	final NearbyStopsTask parent = this;
	GPSLocationUpdateTask.GPSLocationListener gpsLocationListener;
	
	public boolean cancelTask = false;
	
	public boolean isCancelTask() {
		return cancelTask;
	}

	public void cancelTask(boolean cancelTask) {
		this.cancelTask = cancelTask;
	}

	public void setNearbyActivity(NearbyActivity na) {
		this.nearbyActivity = na;
	}

	public ArrayList<NearbyStop> getNearbyStops() {
		return nearbyStops;
	}

	public ArrayList<NearbyStop> getShelterStops() {
		return shelterStops;
	}
	
	public ArrayList<NearbyStop> getEasyStops() {
		return easyStops;
	}
	
	public ArrayList<NearbyTicketOutlet> getNearbyOutlets() {
		return nearbyOutlets;
	}
	
	public NearbyStopsTask(NearbyActivity na) {
		this.nearbyActivity = na;
	}
	
	public Location getLocation() {
		return location;
	}
	
	public void removeUpdates(){
		if(locationManager != null){
			if(locationListener != null)
				locationManager.removeUpdates(locationListener);
			if(gpsLocationListener != null)
				locationManager.removeUpdates(gpsLocationListener);
			locationManager = null;
		}
	}
	
	public void displayErrorMessage(String strMessage){
		Message message = new Message();
		Bundle bundle = new Bundle();
		bundle.putString("error", strMessage);
		message.setData(bundle);
		TramTrackerMainActivity.getAppManager().handlerToast.sendMessage(message); 
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();

		// initialize location manager
		locationManager = (LocationManager) nearbyActivity.getSystemService(Context.LOCATION_SERVICE);
/*		Location l1 = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		if(l1 == null)
			l1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		if(l1 != null){
		displayErrorMessage("Using last known location");
		TTDB ttdb = TTDBSingleton.getInstance(nearbyActivity.getApplicationContext());
		NearbyStopsLimitedByNoOfStops n = ttdb.getNearbyStopsLimitedByNoOfStops(l1);
		nearbyStops = n.getNearbyStops();
		shelterStops = n.getShelterStops();
		easyStops = n.getEasyAccessStops();
		nearbyOutlets = ttdb.getNearbyTicketOutletsLimitedByNoOfOutlets(l1);
		nearbyActivity.update();
	}*/
		
		
		// Added to avoid using last known location
		if(locationManager == null || (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))){
			islocationServiceAvailable = false;
		}
		else{
			// Define a listener that responds to location updates
			locationListener = new MyLocationListener();

			// Register the listener with the Location Manager to receive location updates
			if(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
				locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, Constants.kLocationServicesMinUpdateTime, Constants.kLocationServicesMinUpdateDistance, locationListener);
			if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
				locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, Constants.kLocationServicesMinUpdateTime, Constants.kLocationServicesMinUpdateDistance, locationListener);
			islocationServiceAvailable = true;
		}
		// END - Added to avoid using last known location
		
		
		// MOVED to run in background
//		if(locationManager == null || (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))){
//			islocationServiceAvailable = false;
//		}
//		else{
//			//retrieve user's position from last know location and use it to get nearby stops
//			location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//			if(location != null){
//				displayErrorMessage("Using last known location");
//				TTDB ttdb = TTDBSingleton.getInstance(nearbyActivity.getApplicationContext());
//				NearbyStopsLimitedByNoOfStops n = ttdb.getNearbyStopsLimitedByNoOfStops(location);
//				nearbyStops = n.getNearbyStops();
//				shelterStops = n.getShelterStops();
//				easyStops = n.getEasyAccessStops();
////				nearbyStops = tTDB.getNearbyStopsLimitedByNoOfStops(gpsLocation);
////				shelterStops = tTDB.getNearbyStopsWithShelterLimitedByNoOfStops(gpsLocation);
////				easyStops = tTDB.getNearbyStopsWithEasyAccessLimitedByNoOfStops(gpsLocation);
//				nearbyOutlets = ttdb.getNearbyTicketOutletsLimitedByNoOfOutlets(location);
//				
//			}
//
//			if(cancelTask){
//				return;
//			}
//			
//			if(location != null){
//				if(!nearbyActivity.isFinishing())
//					nearbyActivity.update();
//			}
//			
//			//Get a better location from provider
//
//			// Define a listener that responds to location updates
//			locationListener = new MyLocationListener();
//			// Register the listener with the Location Manager to receive location updates
//			locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, Constants.kLocationServicesMinUpdateTime, Constants.kLocationServicesMinUpdateDistance, locationListener);
//			islocationServiceAvailable = true;
//			
//		}
		
		// END - MOVED to run in background
	}
	
	@Override
	protected void onProgressUpdate(Void... values) {
		super.onProgressUpdate(values);
		if(cancelTask){
			return;
		}
		
		if(location != null){
			nearbyActivity.update();
		} 
		
		//Get a better location from provider

		// Define a listener that responds to location updates
		//locationListener = new MyLocationListener();
		// Register the listener with the Location Manager to receive location updates
		//locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, Constants.kLocationServicesMinUpdateTime, Constants.kLocationServicesMinUpdateDistance, locationListener);
		//islocationServiceAvailable = true;
	}
	
	

	@Override
	protected Void doInBackground(Void... args) {
		// Previously in onPreExecute
//		if(locationManager == null || (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))){
//			islocationServiceAvailable = false;
//		}
//		else{
//			//retrieve user's position from last know location and use it to get nearby stops
//			location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//			if(location != null){
//				displayErrorMessage("Using last known location");
//				TTDB ttdb = TTDBSingleton.getInstance(nearbyActivity.getApplicationContext());
//				NearbyStopsLimitedByNoOfStops n = ttdb.getNearbyStopsLimitedByNoOfStops(location);
//				nearbyStops = n.getNearbyStops();
//				shelterStops = n.getShelterStops();
//				easyStops = n.getEasyAccessStops();
////				nearbyStops = tTDB.getNearbyStopsLimitedByNoOfStops(gpsLocation);
////				shelterStops = tTDB.getNearbyStopsWithShelterLimitedByNoOfStops(gpsLocation);
////				easyStops = tTDB.getNearbyStopsWithEasyAccessLimitedByNoOfStops(gpsLocation);
//				nearbyOutlets = ttdb.getNearbyTicketOutletsLimitedByNoOfOutlets(location);
//				
//			}
//			publishProgress();
//
//		}
		// END - Previously in onPreExecute
		
		
		
		//while (!bestLocationRetreived)

		int something = 1;
		if(islocationServiceAvailable){
			while(bestLocationRetrieved == false && something == 1){
				try {
//					Thread.sleep(200);
					if(isCancelled()){
						return null;
					}
				} catch (Exception e) {
				}
			}
			
			if(location != null){

	//			displayErrorMessage("Using current location");
				TTDB ttdb = TTDBSingleton.getInstance(nearbyActivity.getApplicationContext());
				NearbyStopsLimitedByNoOfStops n = null;
				try{
					n = ttdb.getNearbyStopsLimitedByNoOfStops(location);
				}
				catch(Exception e)
				{
					return null;
				}
				nearbyStops = n.getNearbyStops();
				shelterStops = n.getShelterStops();
				easyStops = n.getEasyAccessStops();
	//			nearbyStops = tTDB.getNearbyStopsLimitedByNoOfStops(gpsLocation);
	//			shelterStops = tTDB.getNearbyStopsWithShelterLimitedByNoOfStops(location);
	//			easyStops = tTDB.getNearbyStopsWithEasyAccessLimitedByNoOfStops(location);
				nearbyOutlets = ttdb.getNearbyTicketOutletsLimitedByNoOfOutlets(location);
			}
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		try{
			if(locationManager != null && locationListener != null)
				locationManager.removeUpdates(locationListener);
			if(islocationServiceAvailable && location != null){
				nearbyActivity.update();
				if(locationManager != null){
					if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !cancelTask){
						GPSLocationUpdateTask task = new GPSLocationUpdateTask();
						task.execute((Void)null);
					}
				}
			}
			else{
				displayErrorMessage(TramTrackerMainActivity.getAppManager().getResources().getString(R.string.error_locationservices_gps_notavailable));
				nearbyStops = new ArrayList<NearbyStop>();
				shelterStops = new ArrayList<NearbyStop>();
				easyStops = new ArrayList<NearbyStop>();
				nearbyOutlets = new ArrayList<NearbyTicketOutlet>();
				nearbyActivity.update();
			}
		}catch (Exception e) {
//			System.out.println("Exception handled");
			e.printStackTrace();
		}

	}
	
	public class MyLocationListener implements LocationListener {
		Location currentBestLocation = null;
		private static final int TWO_MINUTES = 1000 * 60 * 2;
		
		@Override
		public void onLocationChanged(Location location) {
			Log.w("onLocationChanged","onLocationChanged");
//			System.out.println("............Timestamp: " + location.getTime());
//			System.out.println("............Accuracy: " + location.getAccuracy());
//			System.out.println("............Fix: " + location.toString());

			// This codes checks for the location of the map view and uses its location if it is different from this fix 
			// This assumes that the locaiton from the map view will always be better. 
			// ** This code have not been tested
//			if(nearbyActivity != null){
//				if(nearbyActivity.myOverlay != null){
//					if(nearbyActivity.myOverlay.getLastFix() != null){
//						if(nearbyActivity.myOverlay.getLastFix().getLatitude() != location.getLatitude() &&
//							nearbyActivity.myOverlay.getLastFix().getLongitude() != location.getLongitude()){
//								location = nearbyActivity.myOverlay.getLastFix();
//						}
//					}
//				}
//			}
			// If this is the first fix, set as currentBestLocation
			if(currentBestLocation == null){
				currentBestLocation = location;
				parent.location = location;
				bestLocationRetrieved = true;
				//phoenix added if condition 20.Jan.2014
				if(locationManager != null && locationListener != null)
					locationManager.removeUpdates(locationListener);
			
			// If this is the second fix, use this fix. 
			} else {
				// TODO check if this is a better location
				// Actually not used
				currentBestLocation = location;
				parent.location = location;
				bestLocationRetrieved = true;
				//phoenix added if condition 20.Jan.2014
				if(locationManager != null && locationListener != null)
					locationManager.removeUpdates(locationListener);
			}

		}

//		private void useLocation(){
//			TTDB ttdb = TTDBSingleton.getInstance(nearbyActivity.getApplicationContext());
//			NearbyStopsLimitedByNoOfStops n = ttdb.getNearbyStopsLimitedByNoOfStops(location);
//			nearbyStops = n.getNearbyStops();
//			shelterStops = n.getShelterStops();
//			easyStops = n.getEasyAccessStops();
//			nearbyOutlets = ttdb.getNearbyTicketOutletsLimitedByNoOfOutlets(location);
//			
//			publishProgress();
//		}
		
		@Override
		public void onProviderDisabled(String provider) {
			if(provider.equalsIgnoreCase(LocationManager.NETWORK_PROVIDER))
				displayErrorMessage(nearbyActivity.getResources().getString(R.string.error_locationservices_notavailable));
		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			if(provider.equalsIgnoreCase(LocationManager.NETWORK_PROVIDER)){
				if(status == LocationProvider.OUT_OF_SERVICE){
					displayErrorMessage(nearbyActivity.getResources().getString(R.string.error_gps_out_of_service));
				}
				else if(status == LocationProvider.TEMPORARILY_UNAVAILABLE){
					displayErrorMessage(nearbyActivity.getResources().getString(R.string.error_gps_temporarily_unavailable));
				}
			}		
		}
		
		protected boolean isBetterLocation(Location location) {
		    if (currentBestLocation == null) {
		        // A new location is always better than no location
		        return true;
		    }

		    // Check whether the new location fix is newer or older
		    long timeDelta = location.getTime() - currentBestLocation.getTime();
		    boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
		    boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
		    boolean isNewer = timeDelta > 0;

		    // If it's been more than two minutes since the current location, use the new location
		    // because the user has likely moved
		    if (isSignificantlyNewer) {
		    	return true;
		     // If the new location is more than two minutes older, it must be worse
		    } else if (isSignificantlyOlder) {
		    	return false;
		    }

		    // Check whether the new location fix is more or less accurate
		    int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
		    boolean isLessAccurate = accuracyDelta > 0;
		    boolean isMoreAccurate = accuracyDelta < 0;
		    boolean isSignificantlyLessAccurate = accuracyDelta > 200;

		    // Check if the old and new location are from the same provider
		    boolean isFromSameProvider = isSameProvider(location.getProvider(),
		            currentBestLocation.getProvider());

		    // Determine location quality using a combination of timeliness and accuracy
		    if (isMoreAccurate) {
		        return true;
		    } else if (isNewer && !isLessAccurate) {
		        return true;
		    } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
		        return true;
		    }
		    return false;
		}
		
		protected boolean isNewLocation(Location location) {
		    if (currentBestLocation == null) {
		        // A new location is always better than no location
		        return true;
		    }

		    // Check whether the new location fix is newer or older
		    long timeDelta = location.getTime() - currentBestLocation.getTime();
		    boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
		    boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;

		    // If it's been more than two minutes since the current location, use the new location
		    // because the user has likely moved
		    if (isSignificantlyNewer) {
		    	return true;
		     // If the new location is the same as the last known location
		    } else if (timeDelta == 0) {
		    	return false;
		    // If the new location is more than two minutes older, it must be worse
		    } else if (isSignificantlyOlder) {
		    	return false;
		    }
		    
		    return true;
		}

		/** Checks whether two providers are the same */
		private boolean isSameProvider(String provider1, String provider2) {
		    if (provider1 == null) {
		      return provider2 == null;
		    }
		    return provider1.equals(provider2);
		}

	}
	
	class GPSLocationUpdateTask extends AsyncTask<Void, Void,  Boolean>{
		
		Location gpsLocation;
		boolean timeoutOccurred = false;
		Date dateTimeOut;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Define a listener that responds to location updates
			if(locationListener != null && locationManager != null){
				locationManager.removeUpdates(locationListener);
//				locationManager.removeUpdates(locationListener);
			}
			gpsLocationListener = new GPSLocationListener();
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, Constants.kLocationServicesMinUpdateTime, Constants.kLocationServicesMinUpdateDistance, gpsLocationListener);
			dateTimeOut = new Date();
		}
		
		@Override
		protected Boolean doInBackground(Void... params) {
			long timeOutValue = 300000;
			if(locationManager != null){
//				Log.e("GPSLocationUpdateTask", "locationManager != null");
				if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
//					Log.d("GPSLocationUpdateTask", "GPS location started");
					Date now;
					while(gpsLocation == null && timeoutOccurred == false && !cancelTask){
						now = new Date();
//						Log.e("GPSLocationUpdateTask", "timeout seconds = " + (now.getTime() - dateTimeOut.getTime()));
						if(now.getTime() - dateTimeOut.getTime() > timeOutValue){
//							Log.e("GPSLocationUpdateTask", "GPS timed out");
							timeoutOccurred = true;
						}
						try {
							Thread.sleep(200);
							if(isCancelled()){
								return null;
							}
						} catch (Exception e) {
						}
					}
					if(!timeoutOccurred && !cancelTask){
//						displayErrorMessage("got location from gps");
//						Log.d("GPSLocationUpdateTask", "GPS location retreived");
						TTDB ttdb = TTDBSingleton.getInstance(nearbyActivity.getApplicationContext());
						NearbyStopsLimitedByNoOfStops n = null;
						try{
							n = ttdb.getNearbyStopsLimitedByNoOfStops(gpsLocation);
						}
						catch(Exception e)
						{
							return false;
						}
						nearbyStops = n.getNearbyStops();
						shelterStops = n.getShelterStops();
						easyStops = n.getEasyAccessStops();
//						nearbyStops = tTDB.getNearbyStopsLimitedByNoOfStops(gpsLocation);
//						shelterStops = tTDB.getNearbyStopsWithShelterLimitedByNoOfStops(gpsLocation);
//						easyStops = tTDB.getNearbyStopsWithEasyAccessLimitedByNoOfStops(gpsLocation);
						nearbyOutlets = ttdb.getNearbyTicketOutletsLimitedByNoOfOutlets(gpsLocation);
						return true;
					}
				}
			}
			return false;
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			try{
				if(result == true){
//					Log.d("GPSLocationUpdateTask", "Have a GPS location, ready to update");
					nearbyActivity.update();
				}
				if(locationManager != null)
					locationManager.removeUpdates(gpsLocationListener);
				locationManager = null;
			}catch (Exception e) {
//				System.out.println("Exception handled");
				e.printStackTrace();
			}
			super.onPostExecute(result);
		}
		
		public class GPSLocationListener implements LocationListener {
			@Override
			public void onLocationChanged(Location location) {
//				System.out.println("onLocationChanged");
				if(gpsLocation == null){
					if(location.hasAccuracy() && location.getAccuracy() < 50.0){
						gpsLocation = location;
						
						//phoenix added the if condition	16.Jan.2014 
						//Jiazhou amended condition 18/02/16
						if(gpsLocationListener != null && locationManager != null)
							locationManager.removeUpdates(gpsLocationListener);
					}
				}
			}
		

			@Override
			public void onProviderDisabled(String provider) {
				if(provider.equalsIgnoreCase(LocationManager.GPS_PROVIDER))
					displayErrorMessage(nearbyActivity.getResources().getString(R.string.error_gps_disabled));

			}

			@Override
			public void onProviderEnabled(String provider) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStatusChanged(String provider, int status, Bundle extras) {
				if(provider.equalsIgnoreCase(LocationManager.GPS_PROVIDER)){
					if(status == LocationProvider.OUT_OF_SERVICE){
						displayErrorMessage(nearbyActivity.getResources().getString(R.string.error_gps_out_of_service));
					}
					else if(status == LocationProvider.TEMPORARILY_UNAVAILABLE){
						displayErrorMessage(nearbyActivity.getResources().getString(R.string.error_gps_temporarily_unavailable));
					}
				}			
			}
		}	
	}

}
