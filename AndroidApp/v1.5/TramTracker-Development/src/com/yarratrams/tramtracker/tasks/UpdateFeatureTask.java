package com.yarratrams.tramtracker.tasks;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import com.yarratrams.tramtracker.objects.Constants;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;
import com.yarratrams.tramtracker.ui.util.Console;
import com.yarratrams.tramtracker.ui.util.FeaturesPreferences;

import android.content.Context;
import android.os.AsyncTask;
import android.text.format.DateFormat;

public class UpdateFeatureTask extends AsyncTask<Void, Void, Void> {

	private String authid = "4997245C-6D85-11E1-8E28-84224924019B";
	private String tokenid = "3e982c6b-d57a-43fe-bff7-a44e9adb9e1e";

	Context context;
	
	public UpdateFeatureTask(Context context){
		this.context = context;
	}
	
	@Override
	protected Void doInBackground(Void... arg0) {
		// TODO Auto-generated method stub
		try {
			InputStream jsonDataFeatures;

			String url = Constants.strUpdateURLBase + "Features/"
					+ "?authid=" + authid + "&token=" + tokenid;
			Console.print("features url:" + url);

			jsonDataFeatures = getJSONData(url);
			FeaturesPreferences featuresPreferences = new FeaturesPreferences(
					context);
			Console.print("is  input stream null: "
					+ ((jsonDataFeatures == null) ? "true" : "false"));
			if (jsonDataFeatures != null) {
				JSONArray jaFeatureData = parseJSONStreamtToArray(jsonDataFeatures);

				for (int i = 0; i < jaFeatureData.length(); i++) {
					JSONObject jsonObject = jaFeatureData.getJSONObject(i);
					if (jsonObject.getString("featureName").equalsIgnoreCase(
							"Advertising")) {
						Console.print(" ads enable: "
								+ jsonObject.getBoolean("isEnabled"));
						featuresPreferences.setAdsflag(jsonObject
								.getBoolean("isEnabled"));
					}
				}

			} else {
				Console.print("I am in false");
			}
		} catch (Exception e) {

		}

		return null;
	}

	private InputStream getJSONData(String url) {
		DefaultHttpClient httpClient = new DefaultHttpClient();
		URI uri;
		InputStream data = null;
		try {
			uri = new URI(url);
			HttpGet method = new HttpGet(uri);
			HttpResponse response;
			response = httpClient.execute(method);
			data = response.getEntity().getContent();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return data;
	}

	/**
	 * Parse InputStream into JSONObject.
	 */
	private JSONArray parseJSONStreamtToArray(InputStream is) {
		JSONArray jsonArray = null;
		try {
			Console.print("trying to parse1");
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(is));
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			is.close();
			String jsonData = sb.toString();
			jsonArray = new JSONArray(jsonData);
			Console.print("trying to parse2");
		} catch (Exception e) {
			e.printStackTrace();
			Console.print("error in parsing: " + e.getMessage());
		}
		return jsonArray;
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		System.out.println("feature get!");
	}

}
