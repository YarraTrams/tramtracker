package com.yarratrams.tramtracker.tasks;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.yarratrams.tramtracker.objects.Constants;
import com.yarratrams.tramtracker.ui.util.MultiPageDialog;
import com.yarratrams.tramtracker.ui.util.MultiPagePreferences;

public class MultiPageUrlsTask extends AsyncTask<String, Integer, Boolean> {

	// variables
	Activity activity;
	MultiPagePreferences multiPagePreferences;

	HttpClient httpClient;
	HttpGet httpGet = null;
	HttpResponse httpResponse;
	StringEntity stringEntity;
	String content = "";
	String Error = "";
	Context context;

	public MultiPageUrlsTask(Activity activity, Context context) {
		// TODO Auto-generated constructor stub

		this.activity = activity;
		this.context = context;
		multiPagePreferences = new MultiPagePreferences(context);
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
	}

	@Override
	protected Boolean doInBackground(String... arg0) {
		// TODO Auto-generated method stub
		try {

			// String url =
			// "http://qa-json.tramtracker.com.au/TramTrackerWebAPI/api/tutorialurls"
			// + Constants.strAID + Constants.token;
			String url = Constants.strUpdateURLBase + "infopages/?authid=4997245C-6D85-11E1-8E28-84224924019B&token=3e982c6b-d57a-43fe-bff7-a44e9adb9e1e";

			System.out.println("-- url for multi tutorial: " + url);

			httpClient = new DefaultHttpClient();

			httpGet = new HttpGet(url);

			httpResponse = httpClient.execute(httpGet);

			content = EntityUtils.toString(httpResponse.getEntity());

			/*
			 * Uncomment when access token issue fixed
			 */

			if (content.length() > 1) {

				// System.out.println("-- header : " + httpPost.`);
				// System.out.println("-- Data receieved : " + content);

				return true;
			} else {
				Error = "No data found";
				return false;
			}

		}

		catch (Exception e) {
			System.out.println("Exception : " + e.getMessage());
			// TODO: handle exception
			Error = e.getMessage();
			e.printStackTrace();
			return false;
		}
	}

	@Override
	protected void onPostExecute(Boolean result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);

		if (result) {
			System.out.println("-- content of multi page: " + content);
			multiPagePreferences.setMultiUrls(content);
		} else {
			System.out.println("-- error in multi page:  " + Error);

		}
		if (multiPagePreferences.hasNewUrl()) {
			MultiPageDialog multiPageDialog;
			multiPageDialog = new MultiPageDialog(activity, context);
			multiPageDialog.setOnDismissListener(new OnDismissListener() {

				@Override
				public void onDismiss(DialogInterface arg0) {
					// TODO Auto-generated method stub
					multiPagePreferences.removeStoredUrl();
				}

			});

			WindowManager manager = (WindowManager) context
					.getSystemService(Activity.WINDOW_SERVICE);
			int width, height;

			if (Build.VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB_MR2) {
				DisplayMetrics metrics = new DisplayMetrics();
				manager.getDefaultDisplay().getMetrics(metrics);

				height = metrics.heightPixels;
				width = metrics.widthPixels;
			} else {
				width = manager.getDefaultDisplay().getWidth();
				height = manager.getDefaultDisplay().getHeight();
			}

			WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
			lp.copyFrom(multiPageDialog.getWindow().getAttributes());
			//setwindow size here
			lp.width = width * 95 / 100;
			lp.height = height * 9 / 10;
			multiPageDialog.show();
			multiPageDialog.getWindow().setAttributes(lp);

		}
	}

}
