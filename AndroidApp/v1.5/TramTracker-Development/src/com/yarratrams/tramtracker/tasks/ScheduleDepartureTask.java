package com.yarratrams.tramtracker.tasks;

import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.text.format.DateFormat;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.db.TTDB;
import com.yarratrams.tramtracker.objects.Constants;
import com.yarratrams.tramtracker.objects.PredictedArrivalTime;
import com.yarratrams.tramtracker.ui.ScheduleDepartureActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;

public class ScheduleDepartureTask extends AsyncTask<String, String, Boolean> {
	
	
	Activity activity;
	ProgressDialog progressDialog;
	TTDB ttdb;
	
	HttpClient httpClient;
	HttpGet httpGet = null;
	HttpResponse httpResponse;
	StringEntity stringEntity;
	//JSONObject jsonObject ;
	String content = "";
	String Error= "";
	Context context;
	int tripId; 
	long tripTime;
	
	
	public ScheduleDepartureTask(Activity activity, Context context, int tripId, long tripTime) {
		// TODO Auto-generated constructor stub
		
		this.activity = activity;
		ttdb = new TTDB(activity);
		this.context = context;
		this.tripId = tripId;
		this.tripTime = tripTime;
		
	}
	
	
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		progressDialog = new ProgressDialog(TramTrackerMainActivity.instance);
		progressDialog.setMessage(activity.getResources().getString(R.string.dialog_loading));
		progressDialog.show();
		progressDialog.setCanceledOnTouchOutside(false);
		
		
		
	}
	
	
	
	
	@Override
	protected Boolean doInBackground(String... params) {
		
		
		try 
		{

			String strDate = (String)DateFormat.format("yyyy-MM-dd'T'kk:mm:'00'z", tripTime);
			StringBuffer buffer = new StringBuffer(strDate);
			buffer.insert(22, ':');
			//String url = "http://ws2.tramtracker.com.au/TramTracker/RestService/GetSchedulesForTrip/44145427/2015-04-21/?aid=TTIOSJSON&tkn=8BC44518-55AF-41D6-A597-290CA94A676D";
			String url = Constants.strURLBase + "GetSchedulesForTrip" + "/" + tripId + "/" +  buffer.toString() + Constants.strAID + Constants.token;
			
			System.out.println("-- url for trip details: " + url);


			httpClient = new DefaultHttpClient();

			httpGet = new HttpGet(url);
			
			httpResponse = httpClient.execute(httpGet);



			content = EntityUtils.toString(httpResponse.getEntity());


			/*
			 * Uncomment when access token issue fixed
			 */

			if(content.length() >3)
			{


				//				System.out.println("-- header : " + httpPost.`);
				//System.out.println("-- Data receieved : " + content);

				

				return true;
			}
			else
			{
				Error = "Sorry, no Schedule Available";
				return false;
			}






		} 
		
		
		catch (Exception e) 
		{
			System.out.println("Exception : " + e.getMessage() );
			// TODO: handle exception
			
			e.printStackTrace();
			return false;
		}
	}
	
	
	@Override
	protected void onPostExecute(Boolean result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		
		//Toast.makeText(activity, "Completed", 1).show();
		
		JSONObject jsonObject;
		try {
			
			jsonObject = new JSONObject(content);
			ArrayList<PredictedArrivalTime> arrlstPredictedArrivalTimes =  ttdb.getStopsForScheduleDeparture(jsonObject.getJSONArray("responseObject"));
			
			((ScheduleDepartureActivity)activity).updateUI(arrlstPredictedArrivalTimes);
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		progressDialog.dismiss();
	}

}
