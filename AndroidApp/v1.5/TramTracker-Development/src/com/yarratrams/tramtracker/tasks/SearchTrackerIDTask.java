/**
 * This class runs asynchronously the task of searching for stops, ticket outlets and points of interest using the class TTDB.
 */
package com.yarratrams.tramtracker.tasks;

import java.util.ArrayList;

import android.os.AsyncTask;

import com.yarratrams.tramtracker.db.TTDB;
import com.yarratrams.tramtracker.objects.SearchResult;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.singleton.TTDBSingleton;
import com.yarratrams.tramtracker.ui.SearchTrackerIDActivity;


/***
 * Use constructor arguments to provide search arguments.
 * @author appscore1
 *
 */
public class SearchTrackerIDTask extends AsyncTask<Void, Void, Stop>{
	 private SearchTrackerIDActivity activity;
	 private String trackerID;
	 private boolean tramStops = true;
	 private boolean shelterStops = false;
	 private boolean easyAccessStops = false;
	 private boolean ticketOutlets = false;
	 private boolean poi = false;
	
	public SearchTrackerIDTask(SearchTrackerIDActivity searchTrackerIDActivity, String trackerID) {
		this.activity = searchTrackerIDActivity;
		this.trackerID = trackerID;
	}

	
	protected Stop doInBackground(Void... params) {
		TTDB ttdb = TTDBSingleton.getInstance(activity.getApplicationContext());
		
		trackerID = trackerID.trim();
		SearchResult searchResult = ttdb.getSearchResultsGivenKeyword(trackerID, tramStops, shelterStops, easyAccessStops, ticketOutlets, poi);

		if(searchResult == null){
			return null;
		}
		
		ArrayList<Stop> stops = searchResult.getAlStops();
		if(stops == null || stops.isEmpty()){
			return null;
		}
		
		Stop stop = stops.get(0);
		
		return stop;
	}
	
	protected void onPostExecute(Stop stop) {
		super.onPostExecute(stop);
		
		try{
			activity.updateUI(stop);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}
