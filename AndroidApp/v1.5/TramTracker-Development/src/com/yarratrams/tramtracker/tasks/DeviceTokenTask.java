package com.yarratrams.tramtracker.tasks;

import android.os.AsyncTask;

import com.yarratrams.tramtracker.webserviceinteraction.TTWebService;
import com.yarratrams.tramtracker.webserviceinteraction.TTWebServiceSingleton;

public class DeviceTokenTask extends AsyncTask<Void, Void, Void> {

	protected Void doInBackground(Void... params) {
		TTWebService webService = TTWebServiceSingleton.getInstance();
		webService.getDeviceToken();
		return null;
	}
	
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
	}

}
