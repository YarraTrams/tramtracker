package com.yarratrams.tramtracker.tasks;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.yarratrams.tramtracker.objects.Constants;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;
import com.yarratrams.tramtracker.ui.util.DisruptionsPreference;
import com.yarratrams.tramtracker.ui.util.UniqueIdPref;

public class DisruptionNotificationTask extends AsyncTask<String, String, Boolean> {


	Context context;
	DisruptionsPreference disruptionsPreference;
	ProgressDialog progressDialog;
	UniqueIdPref uniqueIdPref;

	HttpClient httpClient;
	HttpResponse httpResponse;
	StringEntity stringEntity;

	String content = "";
	String Error = "";
	String url = "";
	 JSONObject jsonObject;


	public DisruptionNotificationTask(Context context) {
		// TODO Auto-generated constructor stub

		this.context = context;
		disruptionsPreference = new DisruptionsPreference(context);
		uniqueIdPref = new UniqueIdPref(context);
		
	}


	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		progressDialog = new ProgressDialog(TramTrackerMainActivity.instance);
		progressDialog.setTitle("Please Wait");
		progressDialog.setMessage("Updating disruption notifications");
		progressDialog.setCanceledOnTouchOutside(false);
		progressDialog.show();
	}




	@Override
	protected Boolean doInBackground(String... params) {
		// TODO Auto-generated method stub
		

		try {
			
			httpClient = new DefaultHttpClient();
			
			url  = Constants.strUpdateURLBase + "NotificationRequests/?token=3e982c6b-d57a-43fe-bff7-a44e9adb9e1e&authid=4997245C-6D85-11E1-8E28-84224924019B";
			
			 HttpPost httpPost;
             httpPost = new HttpPost(url);
             System.out.println("-- unique id: " + uniqueIdPref.getUniqueId());
             jsonObject = new JSONObject();
             jsonObject.put("deviceId", uniqueIdPref.getUniqueId());
             jsonObject.put("routes", disruptionsPreference.getRoutesArr());
		     jsonObject.put("travelTimeFrames", disruptionsPreference.getTimmingsArr());
             System.out.println("-- JSON Object : " + jsonObject.toString());

             stringEntity = new StringEntity(jsonObject.toString());
             stringEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

             httpPost.setEntity(stringEntity);

             httpResponse = httpClient.execute(httpPost);
             
             content = EntityUtils.toString(httpResponse.getEntity());
			System.out.println("-- content: " + content);
			System.out.println("-- content len : " + content.length());
			if(content.length() < 1)
			{
				return false;
			}
			
			if(httpResponse.getStatusLine().getStatusCode() != 200){
				return false;
			}
			return true;
			
		} catch (ClientProtocolException e) {
            Error = "ClientProtocolException: " + e.getMessage();
            return false;
        } catch (IOException e) {
            Error = "IOException: " + e.getMessage();
            return false;
        } catch (Exception e) {

            Error = e.getMessage();
            e.printStackTrace();
            return false;
        }
	}



	@Override
	protected void onPostExecute(Boolean result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		
		
		
		if(result)
		{
			Toast.makeText(TramTrackerMainActivity.instance, "Disruption notification updated successfully", 1).show();
			//transit back to disruptions page
			TramTrackerMainActivity.getAppManager().back();
		}
		else
		{
			Toast.makeText(TramTrackerMainActivity.instance, "Disruption notification update failed, please try again", 1).show();
		}
		
		progressDialog.dismiss();
	}




}
