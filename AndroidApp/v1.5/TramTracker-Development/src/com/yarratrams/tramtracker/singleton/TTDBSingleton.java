package com.yarratrams.tramtracker.singleton;

import android.content.Context;

import com.yarratrams.tramtracker.db.TTDB;

public class TTDBSingleton {
	private static TTDB ttdb;
	
	public static TTDB getInstance(Context context){
		if(ttdb == null){
			ttdb = new TTDB(context);
		}
		return ttdb;
	}
}
