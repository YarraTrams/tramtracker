package com.yarratrams.tramtracker.singleton;

import java.util.ArrayList;

import android.content.Context;

import com.yarratrams.tramtracker.db.TTDB;
import com.yarratrams.tramtracker.objects.Route;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.objects.TicketOutletBySuburb;

public class RoutesManager {
	
	public RoutesManager(Context context) {
		this.context = context;
	}
	
	TTDB ttdb;
	Context context;
	public ArrayList<Route> getAllRoutes(){
		if(ttdb == null)
			ttdb = TTDBSingleton.getInstance(context);
		return ttdb.getAllRoutes();
	}
	
	public ArrayList<Stop> getStopsForRoute(String routeNo, boolean upStop){
		if(ttdb == null)
			ttdb = TTDBSingleton.getInstance(context);
		return ttdb.getStopsForRoute(routeNo, upStop);
	}
	
	public ArrayList<TicketOutletBySuburb> getTicketOutletsForStops(ArrayList<Stop> alStops){
		if(ttdb == null)
			ttdb = TTDBSingleton.getInstance(context);
		return ttdb.getTicketOutletsForStops(alStops);
	}
}
