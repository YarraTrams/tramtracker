package com.yarratrams.tramtracker.singleton;

import com.yarratrams.tramtracker.objects.PredictedTimeResult;

public class PredictedTimeResultSingleton {
	private static PredictedTimeResult result;
	public static boolean isAtLayover; 
	public static boolean isAvailable; 
	public static final short kErrorNone = 0;
	public static final short kErrorTimeOut = 1; 
	public static final short kErrorNoConnection = 2;
	
	public static short errorType = kErrorNone;
	
	public static PredictedTimeResult getResult() {
		
		return result;
	}

	public static void setResult(PredictedTimeResult result) {
		PredictedTimeResultSingleton.result = result;
	}

	public static boolean isAtLayover() {
		return isAtLayover;
	}

	public static void setAtLayover(boolean isAtLayover) {
		PredictedTimeResultSingleton.isAtLayover = isAtLayover;
	}

	public static boolean isAvailable() {
		return isAvailable;
	}

	public static void setAvailable(boolean isAvailable) {
		PredictedTimeResultSingleton.isAvailable = isAvailable;
	}
	
	
	
}
