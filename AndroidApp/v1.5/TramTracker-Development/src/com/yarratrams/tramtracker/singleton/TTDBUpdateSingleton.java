package com.yarratrams.tramtracker.singleton;

import android.content.Context;

import com.yarratrams.tramtracker.db.TTDBUpdate;

public class TTDBUpdateSingleton {
	static TTDBUpdate ttdbUpdate;
	
	public static TTDBUpdate getTTDBUpdateInstance(Context context){
		if(ttdbUpdate == null)
			ttdbUpdate = new TTDBUpdate(context);
		return ttdbUpdate;
	}
}
