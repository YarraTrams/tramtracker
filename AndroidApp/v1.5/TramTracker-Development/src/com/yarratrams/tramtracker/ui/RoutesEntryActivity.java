package com.yarratrams.tramtracker.ui;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ExpandableListView;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.Route;
import com.yarratrams.tramtracker.singleton.RoutesManager;
import com.yarratrams.tramtracker.ui.util.FeaturesPreferences;
import com.yarratrams.tramtracker.ui.util.OtherLevelAds;
import com.yarratrams.tramtracker.ui.util.RoutesExpandableListArrayAdapter;


public class RoutesEntryActivity extends Activity implements OnClickListener {
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_ROUTES;
	
	private ExpandableListView routesList;
	private RoutesExpandableListArrayAdapter routesAdapter;
	private ArrayList<Route> routes;
	private int listIndex;
	private int listTop;

	private ProgressDialog loadDialog;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.routes_entry_screen);
		
		Button networkButton = (Button) findViewById(R.id.network_button);
		networkButton.setOnClickListener(this);
		
		routes = new ArrayList<Route>();
		routesList = (ExpandableListView) findViewById(R.id.expandable_list);
		Display display = getWindowManager().getDefaultDisplay();
		Drawable groupIndicator = getResources().getDrawable(R.drawable.icn_list_expandable);
		routesList.setGroupIndicator(groupIndicator);
		routesList.setChildIndicator(null);

		if(android.os.Build.VERSION.SDK_INT < 18)
			routesList.setIndicatorBounds(display.getWidth()-GetDipsFromPixel(30), display.getWidth());
		else
			routesList.setIndicatorBoundsRelative(display.getWidth()-GetDipsFromPixel(30), display.getWidth());
		
		listIndex = 0;
		listTop = 0;
		
		routesAdapter = new RoutesExpandableListArrayAdapter(this, routes);
		routesList.setAdapter(routesAdapter);
		
        loadDialog = new ProgressDialog(this);
        
        //call ads
        OtherLevelAds.showAds(this, R.id.rich_banner_fragment1016,FeaturesPreferences.getAdsFlag(TramTrackerMainActivity.instance));
	}
	
	public Context getDialogContext() {
		Context context;
		if (getParent() != null) {
			context = getParent();
		} else {
			context = this;
		}
		return context;
	}

	public int GetDipsFromPixel(float pixels) {
		final float scale = getResources().getDisplayMetrics().density;
		return (int) (pixels * scale + 0.5f);
	}
	
	
	@Override
	public void onResume() {
		super.onResume();
		
		if(!routes.isEmpty()){
			routesList.setSelectionFromTop(listIndex, listTop);
			return;
		}
		
		retrieveRoutes();
	}
	@Override
	public void onPause() {
		super.onPause();
		
		listIndex = routesList.getFirstVisiblePosition();
		View v = routesList.getChildAt(0);
		listTop = (v == null)? 0 : v.getTop();
	}
	
	public void retrieveRoutes(){
		if(!loadDialog.isShowing()){
			loadDialog = ProgressDialog.show(getDialogContext(), "", getResources().getString(R.string.dialog_loading), true, true);
		}
		
		RoutesManager routesManager = new RoutesManager(this);
		routes = routesManager.getAllRoutes();
		
		routesAdapter = new RoutesExpandableListArrayAdapter(this, routes);
		routesList.setAdapter(routesAdapter);
		expandAll();
		
		if(loadDialog.isShowing()){
			loadDialog.dismiss();
		}
	}
	
	
	
	private void expandAll(){
		for(int i = 0; i < routes.size(); i ++){
			routesList.expandGroup(i);
		}
	}
	
	@Override
	public void onClick(View v) {
		Button networkButton = (Button) findViewById(R.id.network_button);
		if (v == networkButton) {
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_header_networkmap));
			Intent intent = new Intent(this, NetworkMapActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_ROUTES, 
					getResources().getString(R.string.tag_networkmap_screen), intent);
		}
	}
	
	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}
	
}
