package com.yarratrams.tramtracker.ui.util;

import java.util.ArrayList;

import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.MenuOption;
import com.yarratrams.tramtracker.ui.OnBoardStopActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;


public class MenuOnBoardStopAdapter extends ArrayAdapter<MenuOption>{
	private static final int MENU_OPTION_FAVOURITE = 0;
	private static final int MENU_OPTION_OUTLETS = 1;
	private static final int MENU_OPTION_DIRECTIONS = 2;
	
	private OnBoardStopActivity activity;
	private static ArrayList<MenuOption> options;
	
	
	public MenuOnBoardStopAdapter(OnBoardStopActivity activity) {
		super(activity, R.layout.menu_container_detail, createMenuList());
		
		this.activity = activity;
	}
	
	
	private static ArrayList<MenuOption> createMenuList(){
		options = new ArrayList<MenuOption>();
		
		MenuOption option; 
		option = new MenuOption(MENU_OPTION_FAVOURITE, R.drawable.icn_menu_favourites, R.string.menu_addToFavourites);
		options.add(option);
		option = new MenuOption(MENU_OPTION_OUTLETS, R.drawable.icn_menu_outlets, R.string.menu_ticketOutlets);
		options.add(option);
		option = new MenuOption(MENU_OPTION_DIRECTIONS, R.drawable.icn_menu_directions, R.string.menu_directionsToStop);
		options.add(option);
		
		return options;
	}
	
	
	static class MenuOptionViewHolder{
		ImageView iconView;
		TextView nameView;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		MenuOptionViewHolder viewHolder;
		
		if(convertView == null){
			LayoutInflater inflater = activity.getLayoutInflater();
			convertView = inflater.inflate(R.layout.menu_container_detail, parent, false);
			
			viewHolder = new MenuOptionViewHolder();
			viewHolder.iconView = (ImageView) convertView.findViewById(R.id.menu_icon);
			viewHolder.nameView = (TextView) convertView.findViewById(R.id.menu_name);
			convertView.setTag(viewHolder);
		
		} else {
			viewHolder = (MenuOptionViewHolder) convertView.getTag();
		}
		
		MenuOption option = options.get(position);
		
		viewHolder.iconView.setImageDrawable(activity.getResources().getDrawable(option.getMenuResourceIcon()));
		viewHolder.nameView.setText(activity.getString(option.getMenuResourceName()));
		
		createMenuOptions(option, convertView);
		
		convertView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				v.setPressed(true);
				return false;
			}
		});
		
		return convertView;
	}
	
	
	protected void createMenuOptions(final MenuOption option, View convertView){
		switch (option.getMenuID()) {
			case MENU_OPTION_FAVOURITE :
				convertView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						TramTrackerMainActivity.getAppManager().callSelection(activity.getString(option.getMenuResourceName()));
						activity.runFavouriteStopMenuOption();
					}
				});
				break;
				
			case MENU_OPTION_OUTLETS :
				convertView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						TramTrackerMainActivity.getAppManager().callSelection(activity.getString(option.getMenuResourceName()));
						activity.runOutletsMenuOption();
					}
				});
				break;
				
			case MENU_OPTION_DIRECTIONS :
				convertView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						TramTrackerMainActivity.getAppManager().callSelection(activity.getString(option.getMenuResourceName()));
						activity.runDirectionsMenuOption();
					}
				});
				break;

			default :
				break;
		}
		
	}

}
