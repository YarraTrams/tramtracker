package com.yarratrams.tramtracker.ui.util;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Handler;
import android.util.Log;

import com.otherlevels.android.sdk.rich.view.RichCardBannerFragment;
import com.otherlevels.android.sdk.rich.view.RichCardBannerHandler;
import com.otherlevels.android.sdk.rich.view.RichCardGridFragment;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;

public class OtherLevelAds {

	public static void showAds(final Activity activity,final int layoutId, boolean flag) {

		System.out.println("feature flag = " + flag);
		
		if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			if (flag) {
				FragmentManager fragmentManager = activity.getFragmentManager();
				FragmentTransaction fragmentTransaction = fragmentManager
						.beginTransaction();

				// add the fragment to a parent view container
				RichCardBannerFragment richBannerFragment = new RichCardBannerFragment();
				fragmentTransaction.add(layoutId, richBannerFragment);
				fragmentTransaction.commit();

				richBannerFragment
						.setRichCardBannerHandler(new RichCardBannerHandler() {

							@Override
							public void willDisplayBanner() {
								System.err.println("willDisplayBanner--->");

							}

							@Override
							public void bannerDidDisplay() {

								System.err.println("willDisplayBanner--->");
							}
						});
			} 	
		}
	}
	
	private void show(Activity activity, int layoutId, boolean flag){
		
	}
	

	// public static void showAds(Activity activity, int layoutId)
	// {
	//
	// if(android.os.Build.VERSION.SDK_INT >
	// android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH )
	// {
	//
	// FragmentManager fragmentManager = activity.getFragmentManager();
	// FragmentTransaction fragmentTransaction =
	// fragmentManager.beginTransaction();
	//
	// //add the fragment to a parent view container
	// RichCardBannerFragment richBannerFragment = new RichCardBannerFragment();
	// fragmentTransaction.add(layoutId, richBannerFragment);
	// fragmentTransaction.commit();
	// }
	// }

	public static void showSpecialOfferGrid(Activity activity, int layoutId) {

		// if(android.os.Build.VERSION.SDK_INT >
		// android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)
		// {

		FragmentManager fragmentManager = activity.getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();

		// add the fragment to a parent view container
		RichCardGridFragment richGridFragment = new RichCardGridFragment();
		fragmentTransaction.add(layoutId, richGridFragment);
		fragmentTransaction.commit();
		// }

	}
}
