package com.yarratrams.tramtracker.ui.util;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.drawable.Drawable;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;
import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.NearbyTicketOutlet;


public class MapOutletOverlay extends ItemizedOverlay<OverlayItem> {
	private ArrayList<OverlayItem> overlays = new ArrayList<OverlayItem>();
	private final Activity context;
	final MapController mc;
	
	private ArrayList<NearbyTicketOutlet> outlets;
	private MapOutletDialog dialog;
	

	private static Drawable getDefaultMarker(Activity context){
		return context.getResources().getDrawable(R.drawable.icn_search_tickets);
	}
	
	public MapOutletOverlay(Activity context, MapView mapView) {
		super(boundCenterBottom(getDefaultMarker(context)));
		this.context = context;
		mc = mapView.getController();
		
		outlets = new ArrayList<NearbyTicketOutlet>();
	}
	
	
	public void updateTicketOutletsList(ArrayList<NearbyTicketOutlet> outlets) {
		this.outlets.clear();
		this.outlets.addAll(outlets);
		
		resetOverlays();
		if(dialog != null){
			dialog.dismiss();
		}
		for(NearbyTicketOutlet outlet : outlets){
	        GeoPoint point = new GeoPoint(outlet.getTicketOutlet().getLatitudeE6(), outlet.getTicketOutlet().getLongitudeE6());
	        OverlayItem overlayitem = new OverlayItem(point, null, null);
	        addOverlay(overlayitem);
		}
	}
	
	
	public void addOverlay(OverlayItem overlay) {
		overlays.add(overlay);
		populate();
	}
	
	public void resetOverlays(){
		overlays.clear();
		populate();
	}

	@Override
	protected OverlayItem createItem(int i) {
		return overlays.get(i);
	}

	@Override
	public int size() {
		return overlays.size();
	}
	
	@Override
	protected boolean onTap(int index) {
		mc.animateTo(createItem(index).getPoint());
		NearbyTicketOutlet outlet = outlets.get(index);
		dialog = new MapOutletDialog(context, outlet);
		
		dialog.show();
		return true;
	}
	
	
}
