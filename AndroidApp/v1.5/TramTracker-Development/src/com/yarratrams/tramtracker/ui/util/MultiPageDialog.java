package com.yarratrams.tramtracker.ui.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.json.JSONArray;
import org.json.JSONException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.MultiPageUrlModel;

public class MultiPageDialog extends Dialog {

	// Varaibles
	Activity activity;
	Context context;
	MultiPageAdapter multiPageAdapter;
	ArrayList<MultiPageUrlModel> alPageUrlModels;
	android.view.View.OnClickListener onImageClickListener;
	ArrayList<ImageButton> alIndicators;
	MultiPagePreferences multiPagePreferences;

	// UI Elements
	ImageButton bDialogClose, bDialogRemindLater;
	ViewPager viewPager;
	LinearLayout layIndicators;
	
	public static MultiPageDialog multiPageDialog;

	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	public MultiPageDialog(Activity activity, Context context) {
		super(context, android.R.style.Theme_Dialog);

		// //set dialog to full screen
		//
		// WindowManager manager = (WindowManager)
		// context.getSystemService(Activity.WINDOW_SERVICE);
		// int width, height;
		//
		//
		// if (Build.VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB_MR2) {
		// DisplayMetrics metrics = new DisplayMetrics();
		// manager.getDefaultDisplay().getMetrics(metrics);
		//
		// height = metrics.heightPixels;
		// width = metrics.widthPixels;
		// } else {
		// width = manager.getDefaultDisplay().getWidth();
		// height = manager.getDefaultDisplay().getHeight();
		// }
		//
		// WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		// lp.copyFrom(getWindow().getAttributes());
		// lp.width = width;
		// lp.height = height;
		// getWindow().setAttributes(lp);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.multipage_dialog);
		setTitle("");
		setCanceledOnTouchOutside(false);
		getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		// TODO Auto-generated constructor stub
		this.activity = activity;
		this.context = context;

		multiPagePreferences = new MultiPagePreferences(context);

		initUI();

		// alPageUrlModels = new ArrayList<MultiPageUrlModel>();
		//
		// MultiPageUrlModel temp = new MultiPageUrlModel();
		// temp.setUrl("http://mobile.twitter.com/");
		// alPageUrlModels.add(temp);
		//
		//
		// temp = new MultiPageUrlModel();
		// temp.setUrl("http://m.facebook.com/");
		// alPageUrlModels.add(temp);
		//
		//
		//
		// temp = new MultiPageUrlModel();
		// temp.setUrl("http://mail.live.com/m");
		// alPageUrlModels.add(temp);

		loadTutorial();

		viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageScrolled(int position, float positionOffset,
					int positionOffsetPixels) {

			}

			@Override
			public void onPageSelected(int position) {

				refreshIndicators();
				alIndicators.get(position).setImageResource(
						R.drawable.paginator_active);
			}

			@Override
			public void onPageScrollStateChanged(int state) {

			}
		});

		// button click listeners

		bDialogClose.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// multiPagePreferences.setIsNewAvailable(false);
				String s = multiPagePreferences.getMultiUrls();

				if (s != null) {
					try {
						JSONArray ja = new JSONArray(s);
						for (int i = 0; i < ja.length(); i++) {
							multiPagePreferences.addViewedId(ja
									.getJSONObject(i).getInt("id"));
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				dismiss();

			}
		});

		bDialogRemindLater.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// multiPagePreferences.setIsNewAvailable(true);

				dismiss();
				multiPagePreferences.setLastOpenTime(System.currentTimeMillis());

			}
		});

		
		multiPageDialog = this;
	}

	private void loadTutorial() {
		// TODO Auto-generated method stub

		alPageUrlModels = multiPagePreferences.getMultiUrlList();

		Collections.sort(alPageUrlModels, new sortOrderCompartor());

		multiPageAdapter = new MultiPageAdapter(activity, alPageUrlModels);
		viewPager.setAdapter(multiPageAdapter);
		viewPager.setCurrentItem(0);
		viewPager.setOffscreenPageLimit(alPageUrlModels.size());

		onImageClickListener = new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				// setIndicators();
				refreshIndicators();
				int id = v.getId();

				viewPager.setCurrentItem(id);
				((ImageButton) v).setImageResource(R.drawable.paginator_active);

			}
		};

		setIndicators();

		// ImageButton imageButton;
		// for(int i = 0 ; i < alPageUrlModels.size(); i++)
		// {
		// System.out.println("-- inside loop");
		// imageButton = new ImageButton(activity);
		// imageButton.setLayoutParams(new
		// LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		// imageButton.setId(i);
		// imageButton.setOnClickListener(onImageClickListener);
		// imageButton.setImageResource(R.drawable.icn_more_search);
		// imageButton.setBackgroundColor(Color.TRANSPARENT);
		// layIndicators.addView(imageButton);
		//
		//
		// }

	}

	private void initUI() {
		// TODO Auto-generated method stub
		viewPager = (ViewPager) findViewById(R.id.pager);
		layIndicators = (LinearLayout) findViewById(R.id.layIndicators);
		bDialogClose = (ImageButton) findViewById(R.id.bDialogClose);
		bDialogRemindLater = (ImageButton) findViewById(R.id.bDialogRemindLater);

	}

	public void setIndicators() {
		alIndicators = new ArrayList<ImageButton>();
		ImageButton imageButton;
		for (int i = 0; i < alPageUrlModels.size(); i++) {

			imageButton = new ImageButton(activity);
			imageButton.setLayoutParams(new LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			imageButton.setId(i);
			imageButton.setOnClickListener(onImageClickListener);
			imageButton.setImageResource(R.drawable.paginator_inactive);
			imageButton.setBackgroundColor(Color.TRANSPARENT);
			alIndicators.add(imageButton);
			layIndicators.addView(alIndicators.get(i));

		}

		try {
			alIndicators.get(0).setImageResource(R.drawable.paginator_active);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void refreshIndicators() {
		for (int i = 0; i < alIndicators.size(); i++) {
			alIndicators.get(i).setImageResource(R.drawable.paginator_inactive);
		}
	}

	private class sortOrderCompartor implements Comparator<MultiPageUrlModel> {

		@Override
		public int compare(MultiPageUrlModel arg0, MultiPageUrlModel arg1) {
			// TODO Auto-generated method stub
			if (arg0.getSortOrder() > arg1.getSortOrder())
				return 1;
			if (arg0.getSortOrder() < arg1.getSortOrder())
				return -1;
			if (arg0.getSortOrder() == arg1.getSortOrder())
				return 0;
			return 0;
		}

	}
	
	public static boolean isVisible()
	{
		
		
		return multiPageDialog.isShowing();
	}

}
