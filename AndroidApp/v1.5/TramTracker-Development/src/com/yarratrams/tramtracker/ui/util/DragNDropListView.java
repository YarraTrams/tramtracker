package com.yarratrams.tramtracker.ui.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;


public class DragNDropListView extends ListView {
	boolean dragMode;

	int startPosition;
	int endPosition;
	int dragPointOffset;

	ImageView dragView;
	GestureDetector gestureDetector;
	Context context;
	
	DragListener dragListener;
	DropListener dropListener;
	RemoveListener removeListener;


	public DragNDropListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
	}
	
	public void setContext(Context context){
		this.context = context;
	}
	
	public interface DragListener {
		void onStartDrag(View itemView);
		void onDrag(int x, int y, DragNDropListView listView);
		void onStopDrag(View itemView);
	}
	
	public interface DropListener {
		void onDrop(int from, int to);
	}
	
	public interface RemoveListener {
		void onRemove(int which);
	}
	
	
	
	
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		final int action = event.getAction();
		final int x = (int) event.getX();
		final int y = (int) event.getY();	
		
		if (action == MotionEvent.ACTION_DOWN && x > this.getWidth()-50) {  
			dragMode = true;
		}
		if(!dragMode){
			return super.onTouchEvent(event);
		}
		
		switch (action) {
			case MotionEvent.ACTION_DOWN:
				startPosition = pointToPosition(x,y);
				if (startPosition != INVALID_POSITION) {
					int itemPosition = startPosition - getFirstVisiblePosition();
                    dragPointOffset = y - getChildAt(itemPosition).getTop();
                    dragPointOffset -= ((int)event.getRawY()) - y;
					startDrag(itemPosition,y);
					drag(x,y);
				}	
				break;
			case MotionEvent.ACTION_MOVE:
				drag(x,y);
				break;
			case MotionEvent.ACTION_CANCEL:
			case MotionEvent.ACTION_UP:
			default:
				dragMode = false;
				
				if(y < 0){
					endPosition = getFirstVisiblePosition();
				} else if(y > getChildAt(getLastVisiblePosition()).getBottom()){
					endPosition = getLastVisiblePosition();
				} else {
					endPosition = pointToPosition(x,y);
					if(endPosition == INVALID_POSITION){
						endPosition = -1;
					}
				}
				
				stopDrag(startPosition - getFirstVisiblePosition());
				if (dropListener != null && startPosition != INVALID_POSITION && endPosition != INVALID_POSITION) 
					dropListener.onDrop(startPosition, endPosition);
				break;
		}
		return true;
	}	
	
	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
	    return super.onInterceptTouchEvent(ev);
	}
	
	private void drag(int x, int y) {
		if (dragView != null) {
			WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams) dragView.getLayoutParams();
			layoutParams.x = 0;
			layoutParams.y = y - dragPointOffset;
			WindowManager manager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
			manager.updateViewLayout(dragView, layoutParams);

			if (dragListener != null)
				dragListener.onDrag(x, y, this);
		}
	}
	
	
	private void startDrag(int itemIndex, int y) {
		stopDrag(itemIndex);

		View item = getChildAt(itemIndex);
		if (item == null) return;
		item.setDrawingCacheEnabled(true);
		if (dragListener != null)
			dragListener.onStartDrag(item);
		
        Bitmap bitmap = Bitmap.createBitmap(item.getDrawingCache());
        
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.gravity = Gravity.TOP;
        layoutParams.x = 0;
        layoutParams.y = y - dragPointOffset;

        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
                | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS;
        layoutParams.format = PixelFormat.TRANSLUCENT;
        layoutParams.windowAnimations = 0;
        
        ImageView view = new ImageView(context);
        view.setImageBitmap(bitmap);      

        WindowManager manager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        manager.addView(view, layoutParams);
        dragView = view;
	}	
	
	private void stopDrag(int itemIndex) {
		if (dragView != null) {
			if (dragListener != null)
				dragListener.onStopDrag(getChildAt(itemIndex));
            dragView.setVisibility(GONE);
            WindowManager manager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
            manager.removeView(dragView);
            dragView.setImageDrawable(null);
            dragView = null;
        }
	}
	
	
	
	public void setDropListener(DropListener listener) {
		dropListener = listener;
	}

	public void setRemoveListener(RemoveListener listener) {
		removeListener = listener;
	}
	
	public void setDragListener(DragListener listener) {
		dragListener = listener;
	}
	
}