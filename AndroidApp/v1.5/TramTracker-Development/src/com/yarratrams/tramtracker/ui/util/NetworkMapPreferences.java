package com.yarratrams.tramtracker.ui.util;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.SharedPreferences;
import android.util.Log;

import com.yarratrams.tramtracker.objects.NetworkMap;

public class NetworkMapPreferences {

	Activity activity;
	SharedPreferences sharedPreferences;
	SharedPreferences.Editor editor;
	
	
	final String PREF_NAME = "networkmappref";
	final String NETWORK_MAPS =	"networkmaps";
	
	public NetworkMapPreferences(Activity _activity) {
		// TODO Auto-generated constructor stub
		
		activity =  _activity;
		sharedPreferences  = activity.getSharedPreferences(PREF_NAME, Activity.MODE_PRIVATE);
		editor= sharedPreferences.edit();
				
					
	}
	
	
	
	public void setNetworkMaps(String maps)
	{
		editor.putString(NETWORK_MAPS, maps);
		editor.commit();
	}
	
	public void setNetworkMapList(String data) throws JSONException
	{
		JSONObject jsonObject;
		JSONArray jsonArray = new JSONArray();
		JSONArray jsonArrayData =  new JSONArray(data);
		
		
		
		for(int i = 0 ; i < jsonArrayData.length(); i++)
		{
			jsonObject = new JSONObject();
			
			jsonObject.put(NetworkMap.ACTIVE_DATE, jsonArrayData.getJSONObject(i).get(NetworkMap.ACTIVE_DATE));
			jsonObject.put(NetworkMap.FILE_NAME, jsonArrayData.getJSONObject(i).get(NetworkMap.FILE_NAME));
			jsonObject.put(NetworkMap.IsMapAvailable, 0);
			jsonObject.put(NetworkMap.URL, jsonArrayData.getJSONObject(i).get(NetworkMap.URL));
			
			
			jsonArray.put(jsonObject);
			jsonObject = null;
		}
		
		
		
		editor.putString(NETWORK_MAPS, jsonArray.toString());
		editor.commit();
		
	}
	
	
	public String getNetworkMaps()
	{
		return sharedPreferences.getString(NETWORK_MAPS, "");
	}
	
	
	
	public ArrayList<NetworkMap> getNetworkMapList()
	{
		ArrayList<NetworkMap> arrlstNetworkMaps = new ArrayList<NetworkMap>();
		NetworkMap temp = null;
		JSONArray jsonArray;
		
		if(sharedPreferences.getString(NETWORK_MAPS, "").length() > 1)
		{
			try {
				 jsonArray =  new JSONArray(sharedPreferences.getString(NETWORK_MAPS, ""));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				jsonArray = null;
				e.printStackTrace();
				return null;
			}
			
			
			
			
			if(jsonArray!=null)
			{
				for(int i = 0 ; i < jsonArray.length() ;i++)
				{
					JSONObject jsonObject=  null;
					temp = new NetworkMap();
					
					try {
						jsonObject = new JSONObject(jsonArray.getString(i));
						
						
						temp.setActivateDate(jsonObject.getString(NetworkMap.ACTIVE_DATE));
						temp.setFileName(jsonObject.getString(NetworkMap.FILE_NAME));
						temp.setUrl(jsonObject.getString(NetworkMap.URL));
						temp.setIsMapAvailable(jsonObject.getInt(NetworkMap.IsMapAvailable));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						Log.v("TT", "error frm saved pref: " + e.getMessage());
					}
					
					
					arrlstNetworkMaps.add(temp);
					temp = null;
					jsonObject = null;
				}
			}
			
		}
		else
		{
			return null;
		}
		
	
		
		return arrlstNetworkMaps;
	}
	
	
	public void setNetworkMapList(ArrayList<NetworkMap> arrlstNetworkMaps) throws JSONException
	{
		JSONObject jsonObject;
		JSONArray jsonArray = new JSONArray();
		
		
		
		for(int i = 0 ; i < arrlstNetworkMaps.size(); i++)
		{
			jsonObject = new JSONObject();
			
			jsonObject.put(NetworkMap.ACTIVE_DATE, arrlstNetworkMaps.get(i).getActivateDate());
			jsonObject.put(NetworkMap.FILE_NAME, arrlstNetworkMaps.get(i).getFileName());
			jsonObject.put(NetworkMap.IsMapAvailable, arrlstNetworkMaps.get(i).getIsMapAvailable());
			jsonObject.put(NetworkMap.URL, arrlstNetworkMaps.get(i).getUrl());
			
			
			jsonArray.put(jsonObject);
			jsonObject = null;
		}
		
		
		
		editor.putString(NETWORK_MAPS, jsonArray.toString());
		editor.commit();
		
	}
}
