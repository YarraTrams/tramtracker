package com.yarratrams.tramtracker.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.MailTo;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.ui.util.FeaturesPreferences;
import com.yarratrams.tramtracker.ui.util.OtherLevelAds;

public class FeedbackActivity extends Activity {
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_MORE;
	
	private WebView webView;
	private ProgressBar progessBar;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.feedback_screen);
		
		startWebView();
		
		//call ads
		OtherLevelAds.showAds(this, R.id.rich_banner_fragment88,FeaturesPreferences.getAdsFlag(TramTrackerMainActivity.instance));
	}

	
	public void startWebView() {
		webView = (WebView) findViewById(R.id.web_view);
		progessBar = (ProgressBar) findViewById(R.id.ProgressBar);
		
		WebSettings webSettings = webView.getSettings();

		webSettings.setUserAgentString(webSettings.getUserAgentString());
		webSettings.setJavaScriptEnabled(true);
		webSettings.setSupportZoom(true);
		webSettings.setBuiltInZoomControls(true);
		webSettings.setLoadsImagesAutomatically(true);

		webView.setWebViewClient(new WebViewClient() {
		    Intent newEmailIntent(Context context, String address, String subject, String body, String cc) {
			      Intent intent = new Intent(Intent.ACTION_SEND);
			      intent.putExtra(Intent.EXTRA_EMAIL, new String[] { address });
			      intent.putExtra(Intent.EXTRA_TEXT, body);
			      intent.putExtra(Intent.EXTRA_SUBJECT, subject);
			      intent.putExtra(Intent.EXTRA_CC, cc);
			      intent.setType("message/rfc822");
			      return intent;
				};
		
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				progessBar.setVisibility(View.VISIBLE);
				super.onPageStarted(view, url, favicon);
			}
			@Override
			public void onPageFinished(WebView view, String url) {
				progessBar.setVisibility(View.GONE);
			}

	        @Override
	        public boolean shouldOverrideUrlLoading(WebView view, String url) {
	            if(url.startsWith("mailto:")){
	                MailTo mt = MailTo.parse(url);
	                Intent intent = newEmailIntent(FeedbackActivity.this, mt.getTo(), mt.getSubject(), mt.getBody(), mt.getCc());
	                startActivity(intent);
	                view.reload();
	                return true;
	                
	            } else if(url.startsWith("tel:")) { 
	                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url)); 
	                startActivity(intent);
	                return true;
	                
	            } else {
					view.loadUrl(url);
				}
	            return true;
	        }
		});

		webView.loadUrl(getHelpURL());
	}
	
	
	private String getHelpURL(){
		String url = "";
		
		url = getResources().getString(R.string.webview_url_base);
		url = url.concat(getResources().getString(R.string.webview_url_feedback));
		
		return url;
	}
	
	@Override
	public void onPause() {
		super.onPause();
		webView.stopLoading();
//		try {
//			Class.forName("android.webkit.WebView").getMethod("onPause", (Class[]) null).invoke(webView, (Object[]) null);
//		} catch (Exception e) {
//		}
	}
	
	
	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}
	
	
}
