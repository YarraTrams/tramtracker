package com.yarratrams.tramtracker.ui;


import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ToggleButton;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.ui.util.FeaturesPreferences;
import com.yarratrams.tramtracker.ui.util.OtherLevelAds;


public class DefaultEntryActivity extends Activity implements OnClickListener{
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_MORE;
	
	private SharedPreferences preferences;
	private int selectedEntryScreen;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.defaultentry_screen);
		
		preferences = getSharedPreferences(TramTrackerMainActivity.SHARED_PREFERENCES, MODE_PRIVATE);
		selectedEntryScreen = preferences.getInt(TramTrackerMainActivity.DEFAULT_ENTRY_SCREEN, 0);
		
		ToggleButton nearestFavouriteCheckButton = (ToggleButton)findViewById(R.id.entry_nearestfavourite_checkButton);
		ToggleButton favouritesCheckButton = (ToggleButton)findViewById(R.id.entry_favourites_checkButton);
		ToggleButton nearbyCheckButton = (ToggleButton)findViewById(R.id.entry_nearby_checkButton);
		ToggleButton trackerIDCheckButton = (ToggleButton)findViewById(R.id.entry_trackerid_checkButton);
		
		nearestFavouriteCheckButton.setOnClickListener(this);
		favouritesCheckButton.setOnClickListener(this);
		nearbyCheckButton.setOnClickListener(this);
		trackerIDCheckButton.setOnClickListener(this);
		
		Button saveButton = (Button)findViewById(R.id.save_button);
		saveButton.setOnClickListener(this);
		
		
		//loads ads
		OtherLevelAds.showAds(this, R.id.rich_banner_fragment11,FeaturesPreferences.getAdsFlag(TramTrackerMainActivity.instance));
		
	}
	
	
	@Override
	public void onClick(View v) {
		ToggleButton nearestFavouriteCheckButton = (ToggleButton)findViewById(R.id.entry_nearestfavourite_checkButton);
		ToggleButton favouritesCheckButton = (ToggleButton)findViewById(R.id.entry_favourites_checkButton);
		ToggleButton nearbyCheckButton = (ToggleButton)findViewById(R.id.entry_nearby_checkButton);
		ToggleButton trackerIDCheckButton = (ToggleButton)findViewById(R.id.entry_trackerid_checkButton);
		Button saveButton = (Button)findViewById(R.id.save_button);
		
		if(v == saveButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_header_save));
			TramTrackerMainActivity.getAppManager().savePreferences(selectedEntryScreen);
			TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.notify_entryscreen_saved));
			TramTrackerMainActivity.getAppManager().back();
		
		} else if(v == nearestFavouriteCheckButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_entryscreen_nearfavourite));
			nearbyCheckButton.setChecked(false);
			favouritesCheckButton.setChecked(false);
			nearestFavouriteCheckButton.setChecked(true);
			trackerIDCheckButton.setChecked(false);
			selectedEntryScreen = TramTrackerMainActivity.DEFAULT_ENTRY_NEARESTFAVOURITE;
			
		} else if(v == favouritesCheckButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_entryscreen_favourites));
			nearbyCheckButton.setChecked(false);
			favouritesCheckButton.setChecked(true);
			nearestFavouriteCheckButton.setChecked(false);
			trackerIDCheckButton.setChecked(false);
			selectedEntryScreen = TramTrackerMainActivity.DEFAULT_ENTRY_FAVOURITES;
			
		} else if(v == nearbyCheckButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_entryscreen_nearby));
			nearbyCheckButton.setChecked(true);
			favouritesCheckButton.setChecked(false);
			nearestFavouriteCheckButton.setChecked(false);
			trackerIDCheckButton.setChecked(false);
			selectedEntryScreen = TramTrackerMainActivity.DEFAULT_ENTRY_NEARBY;
			
		} else if(v == trackerIDCheckButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_entryscreen_trackerid));
			nearbyCheckButton.setChecked(false);
			favouritesCheckButton.setChecked(false);
			nearestFavouriteCheckButton.setChecked(false);
			trackerIDCheckButton.setChecked(true);
			selectedEntryScreen = TramTrackerMainActivity.DEFAULT_ENTRY_TRACKERID;
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		setSelected();
	}

	private void setSelected(){
		ToggleButton nearestFavouriteCheckButton = (ToggleButton)findViewById(R.id.entry_nearestfavourite_checkButton);
		ToggleButton favouritesCheckButton = (ToggleButton)findViewById(R.id.entry_favourites_checkButton);
		ToggleButton nearbyCheckButton = (ToggleButton)findViewById(R.id.entry_nearby_checkButton);
		ToggleButton trackerIDCheckButton = (ToggleButton)findViewById(R.id.entry_trackerid_checkButton);
		
		if(selectedEntryScreen == TramTrackerMainActivity.DEFAULT_ENTRY_NEARBY){
			nearbyCheckButton.setChecked(true);
			favouritesCheckButton.setChecked(false);
			nearestFavouriteCheckButton.setChecked(false);
			trackerIDCheckButton.setChecked(false);
		} else if(selectedEntryScreen == TramTrackerMainActivity.DEFAULT_ENTRY_FAVOURITES){
			nearbyCheckButton.setChecked(false);
			favouritesCheckButton.setChecked(true);
			nearestFavouriteCheckButton.setChecked(false);
			trackerIDCheckButton.setChecked(false);
		} else if(selectedEntryScreen == TramTrackerMainActivity.DEFAULT_ENTRY_NEARESTFAVOURITE){
			nearbyCheckButton.setChecked(false);
			favouritesCheckButton.setChecked(false);
			nearestFavouriteCheckButton.setChecked(true);
			trackerIDCheckButton.setChecked(false);
		} else if(selectedEntryScreen == TramTrackerMainActivity.DEFAULT_ENTRY_TRACKERID){
			nearbyCheckButton.setChecked(false);
			favouritesCheckButton.setChecked(false);
			nearestFavouriteCheckButton.setChecked(false);
			trackerIDCheckButton.setChecked(true);
		}
	}
	
	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return true;
	}
	
	
}
