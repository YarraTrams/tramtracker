package com.yarratrams.tramtracker.ui.util;

import java.util.ArrayList;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.TicketOutlet;
import com.yarratrams.tramtracker.objects.TicketOutletBySuburb;
import com.yarratrams.tramtracker.ui.OutletActivity;
import com.yarratrams.tramtracker.ui.TicketOutletsListActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;


public class TicketOutletsExpandableListArrayAdapter extends BaseExpandableListAdapter {
	private TicketOutletsListActivity context;
	private ArrayList<TicketOutletBySuburb> suburbs;
	
	public TicketOutletsExpandableListArrayAdapter(TicketOutletsListActivity context, ArrayList<TicketOutletBySuburb> suburbs) {
		super();
		this.context = context;
		this.suburbs = suburbs;
	}

	public void updateList(ArrayList<TicketOutletBySuburb> suburbs) {
		this.suburbs.clear();
		this.suburbs.addAll(suburbs);
		super.notifyDataSetChanged();
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return suburbs.get(groupPosition).getTicketOutlets().get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return (groupPosition * 1000) + childPosition;
	}

	static class OutletViewHolder{
		TextView outletName;
		TextView outletAddress;
	}
	
	@Override
	public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
		OutletViewHolder viewHolder;
		
		if(convertView == null){
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(R.layout.outlets_list_view_child, parent, false);
	
			viewHolder = new OutletViewHolder();
			viewHolder.outletName = (TextView)convertView.findViewById(R.id.outlet_name);
			viewHolder.outletAddress = (TextView)convertView.findViewById(R.id.outlet_address);
			convertView.setTag(viewHolder);
		
		} else {
			viewHolder = (OutletViewHolder) convertView.getTag();
		}
		
		final TicketOutlet outlet = (TicketOutlet) getChild(groupPosition, childPosition);
		viewHolder.outletName.setText(outlet.getName());
		viewHolder.outletAddress.setText(outlet.getAddress());
		
		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				TramTrackerMainActivity.getAppManager().callSelection(outlet.getName());
				goToTicketOutletScreen(outlet);
			}
		});
		
		convertView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				v.setPressed(true);
				
				return false;
			}
		});
		
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return suburbs.get(groupPosition).getTicketOutlets().size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return suburbs.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return suburbs.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	
	static class SuburbViewHolder{
		TextView suburbName;
	}
	
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		SuburbViewHolder viewHolder;
		
		if(convertView == null){
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(R.layout.outlets_list_view_group, parent, false);
			
			viewHolder = new SuburbViewHolder();
			viewHolder.suburbName = (TextView) convertView.findViewById(R.id.group_name);
			convertView.setTag(viewHolder);
			
		} else {
			viewHolder = (SuburbViewHolder) convertView.getTag();
		}
		
		TicketOutletBySuburb suburb = (TicketOutletBySuburb) getGroup(groupPosition);
		viewHolder.suburbName.setText(suburb.getSuburb());
		
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

    @Override
    public boolean areAllItemsEnabled(){
        return true;
    }
	
    
	private void goToTicketOutletScreen(TicketOutlet outlet){
		Intent intent = new Intent(this.context, OutletActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(OutletActivity.INTENT_KEY, outlet);
		
		TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, 
				context.getResources().getString(R.string.tag_outlet_screen), intent);
	}

}
