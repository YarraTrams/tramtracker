package com.yarratrams.tramtracker.ui.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.Reminders;
import com.yarratrams.tramtracker.ui.RemindersActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;

public class RemindersAdapter extends BaseExpandableListAdapter {

	// Variables
	Activity activity;
	ArrayList<Reminders> alReminders;
	ReminderPreferences reminderPreferences;
	HashMap<String, ArrayList<Reminders>> hmReminders;
	ArrayList<String> alReminderHeaders;

	// UI Elements
	AlertDialog.Builder builder;
	AlertDialog alertDialog;

	public RemindersAdapter(Activity activity,
			HashMap<String, ArrayList<Reminders>> hmReminders,
			ArrayList<String> alReminderHeaders) {
		// TODO Auto-generated constructor stub
		this.activity = activity;
		this.hmReminders = hmReminders;
		this.alReminderHeaders = alReminderHeaders;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return hmReminders.get(alReminderHeaders.get(groupPosition)).get(
				childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childPosition;
	}

	@Override
	public View getChildView(final int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		ChildViewHolder childViewHolder;

		if (convertView == null) {
			LayoutInflater layoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater.inflate(
					R.layout.reminders_list_view_child, null);

			childViewHolder = new ChildViewHolder();

			initUI(convertView, childViewHolder);

			convertView.setTag(childViewHolder);

		} else {
			childViewHolder = (ChildViewHolder) convertView.getTag();
		}

		setChildValues(groupPosition, childPosition, childViewHolder);

		// if (getChildrenCount(0) != 0) {
		// if (groupPosition == 0 && childPosition == 0) {
		// ((RemindersActivity) activity).tutorialPoint =
		// childViewHolder.bDelReminder;
		// }
		// } else if (getChildrenCount(1) != 0) {
		// if (groupPosition == 1 && childPosition == 0) {
		// ((RemindersActivity) activity).tutorialPoint =
		// childViewHolder.bDelReminder;
		// }
		// }

		childViewHolder.bDelReminder.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				final Reminders currReminder = (Reminders) getChild(
						groupPosition, childPosition);

				// Toast.makeText(TramTrackerMainActivity.instance, "delete: " +
				// currReminder.getStopName(), 0).show();

				builder = new Builder(TramTrackerMainActivity.instance);
				builder.setTitle("Alarms");
				builder.setMessage("Are you sure you want to delete alarm: "
						+ currReminder.getStopName());
				builder.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								reminderPreferences = new ReminderPreferences(
										activity);
								reminderPreferences
										.deleteReminder(currReminder);

								((RemindersActivity) activity)
										.refreshRemindersList();

								((RemindersActivity) activity).closeToolTip();
								TutorialPreferences tutorialPreferences = new TutorialPreferences(
										activity);
								if (!tutorialPreferences.getAlarmTutorial()) {
									((RemindersActivity) activity).closeToolTip();
									Toast.makeText(
											activity,
											"Thank you for completing this tutorial. Get more tutorials in the tram arrival screen, myTRAM and Timetables.",
											1).show();
									tutorialPreferences.setAlarmTutorial(true);
									tutorialPreferences
											.setDisruptionTutorial(true);
									tutorialPreferences
											.setSettingsTutorial(true);
								}

							}
						});

				builder.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

							}
						});

				alertDialog = builder.create();
				alertDialog.setCanceledOnTouchOutside(false);
				alertDialog.show();

				

			}
		});

		return convertView;
	}

	private void setChildValues(int groupPosition, final int childPosition,
			ChildViewHolder childViewHolder) {
		Reminders currReminder = (Reminders) getChild(groupPosition,
				childPosition);

		childViewHolder.tvRoutes.setText("Routes " + currReminder.getRoute());
		childViewHolder.tvStopName.setText(currReminder.getStopName());
		childViewHolder.tvTime.setText(getReminderTime(currReminder
				.getReminderTime()));

		if (currReminder.isFreeTramZone()) {
			childViewHolder.ivFTZ.setVisibility(View.VISIBLE);
		} else {
			childViewHolder.ivFTZ.setVisibility(View.INVISIBLE);
		}

	}
	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		return this.hmReminders.get(alReminderHeaders.get(groupPosition))
				.size();
	}
	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return alReminderHeaders.get(groupPosition);
	}
	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return alReminderHeaders.size();
	}
	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		HeaderViewHolder headerViewHolder;

		if (convertView == null) {
			LayoutInflater layoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater.inflate(
					R.layout.reminders_list_view_header, null);

			headerViewHolder = new HeaderViewHolder();

			headerViewHolder.tvHeader = (TextView) convertView
					.findViewById(R.id.tvHeader);

			convertView.setTag(headerViewHolder);

		} else {
			headerViewHolder = (HeaderViewHolder) convertView.getTag();
		}

		headerViewHolder.tvHeader.setText((String) getGroup(groupPosition));

		return convertView;

	}
	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean isChildSelectable(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return true;
	}

	static class ChildViewHolder {
		ImageButton bDelReminder;
		TextView tvStopName, tvRoutes, tvTime;
		ImageView ivFTZ;
	}

	static class HeaderViewHolder {
		TextView tvHeader;

	}

	private void initUI(View convertView, ChildViewHolder childViewHolder) {

		childViewHolder.bDelReminder = (ImageButton) convertView
				.findViewById(R.id.bDelReminder);
		childViewHolder.ivFTZ = (ImageView) convertView
				.findViewById(R.id.ivFTZ);

		childViewHolder.tvRoutes = (TextView) convertView
				.findViewById(R.id.tvRoutes);
		childViewHolder.tvStopName = (TextView) convertView
				.findViewById(R.id.tvStopName);
		childViewHolder.tvTime = (TextView) convertView
				.findViewById(R.id.tvTime);

	}

	public String getReminderTime(long t) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
		Date date;

		date = new Date(t);

		if (android.text.format.DateFormat.is24HourFormat(activity)) {
			dateFormat = new SimpleDateFormat("HH:mm");
		} else {
			dateFormat = new SimpleDateFormat("h:mm a");
		}

		return dateFormat.format(date);

	}

	/*
	 * public RemindersAdapter(Activity activity, ArrayList<Reminders> objects)
	 * { super(activity, R.layout.reminders_list_view_child, objects); // TODO
	 * Auto-generated constructor stub this.activity = activity; alReminders =
	 * objects; }
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * @Override public View getView(final int position, View convertView,
	 * ViewGroup parent) { // TODO Auto-generated method stub
	 * 
	 * 
	 * ViewHolder viewHolder;
	 * 
	 * if(convertView == null) { LayoutInflater layoutInflater =
	 * (LayoutInflater)
	 * activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE); convertView =
	 * layoutInflater.inflate(R.layout.reminders_list_view_child, parent,false);
	 * 
	 * viewHolder = new ViewHolder();
	 * 
	 * initUI(convertView, viewHolder);
	 * 
	 * convertView.setTag(viewHolder);
	 * 
	 * 
	 * } else { viewHolder = (ViewHolder) convertView.getTag(); }
	 * 
	 * setValues(position, viewHolder);
	 * 
	 * 
	 * viewHolder.bDelReminder.setOnClickListener(new OnClickListener() {
	 * 
	 * @Override public void onClick(View v) { // TODO Auto-generated method
	 * stub
	 * 
	 * final Reminders currReminder = alReminders.get(position);
	 * 
	 * Toast.makeText(TramTrackerMainActivity.instance, "delete: " +
	 * currReminder.getStopName(), 0).show();
	 * 
	 * builder = new Builder(TramTrackerMainActivity.instance);
	 * builder.setTitle("Alarms");
	 * builder.setMessage("Are you sure you want to delete alarm: " +
	 * currReminder.getStopName() ); builder.setPositiveButton("Yes", new
	 * DialogInterface.OnClickListener() {
	 * 
	 * @Override public void onClick(DialogInterface dialog, int which) { //
	 * TODO Auto-generated method stub reminderPreferences = new
	 * ReminderPreferences(activity);
	 * reminderPreferences.deleteReminder(currReminder);
	 * 
	 * ((RemindersActivity)activity).refreshRemindersList();
	 * 
	 * 
	 * } });
	 * 
	 * 
	 * builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
	 * {
	 * 
	 * @Override public void onClick(DialogInterface dialog, int which) { //
	 * TODO Auto-generated method stub
	 * 
	 * } });
	 * 
	 * 
	 * alertDialog = builder.create();
	 * alertDialog.setCanceledOnTouchOutside(false); alertDialog.show();
	 * 
	 * } });
	 * 
	 * 
	 * 
	 * return convertView; }
	 * 
	 * 
	 * 
	 * 
	 * private void setValues(final int position, ViewHolder viewHolder) {
	 * 
	 * System.out.println("-- routes: " + alReminders.get(position).getRoute());
	 * viewHolder.tvRoutes.setText("Routes " +
	 * alReminders.get(position).getRoute());
	 * viewHolder.tvStopName.setText(alReminders.get(position).getStopName());
	 * viewHolder
	 * .tvTime.setText(getReminderTime(alReminders.get(position).getReminderTime
	 * ()));
	 * 
	 * if(alReminders.get(position).isFreeTramZone()) {
	 * viewHolder.ivFTZ.setVisibility(View.VISIBLE); } else {
	 * viewHolder.ivFTZ.setVisibility(View.INVISIBLE); } }
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * private void initUI(View convertView, ViewHolder viewHolder) {
	 * 
	 * viewHolder.bDelReminder = (ImageButton)
	 * convertView.findViewById(R.id.bDelReminder); viewHolder.ivFTZ =
	 * (ImageView) convertView.findViewById(R.id.ivFTZ);
	 * 
	 * viewHolder.tvRoutes = (TextView) convertView.findViewById(R.id.tvRoutes);
	 * viewHolder.tvStopName = (TextView)
	 * convertView.findViewById(R.id.tvStopName); viewHolder.tvTime = (TextView)
	 * convertView.findViewById(R.id.tvTime);
	 * 
	 * }
	 * 
	 * 
	 * public String getReminderTime(long t) {
	 * 
	 * 
	 * SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm"); Date date;
	 * 
	 * date = new Date(t);
	 * 
	 * 
	 * 
	 * if(android.text.format.DateFormat.is24HourFormat(activity)) { dateFormat
	 * = new SimpleDateFormat("HH:mm"); } else { dateFormat = new
	 * SimpleDateFormat("h:mm a"); }
	 * 
	 * 
	 * 
	 * 
	 * return dateFormat.format(date);
	 * 
	 * }
	 * 
	 * 
	 * 
	 * 
	 * static class ViewHolder { ImageButton bDelReminder; TextView tvStopName,
	 * tvRoutes , tvTime; ImageView ivFTZ; }
	 */

}
