package com.yarratrams.tramtracker.ui.util;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.yarratrams.tramtracker.R;

public class AlarmDialog extends Dialog {

	Context context;

//	public final static String STOP_ALARM_MSG_1 = "How long before the tram is scheduled to arrive at ";
//	public final static String STOP_ALARM_MSG_2 = " would you like to be alerted?";
	
	public final static String STOP_ALARM_MSG_1 = "Your tram is predicted to arrive at ";
	public final static String STOP_ALARM_MSG_2 = ".\n\nHow long before the estimated arrival time would you like to be alerted?\n\nIf there are disruptions on your route, please check and rely on real-time information.";
	
	//UI elements
	public Button bAlarmDialogOk, bAlarmDialogCancel;
	public RadioGroup rgIntervals;
	public RadioButton rbOne, rbTwo, rbThree;
	public TextView tvAlarmDialogMsg;
	

	@SuppressLint("NewApi")
	public AlarmDialog(Context context) {
		super(context);

		this.context = context;
		//getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		
		setContentView(R.layout.alarm_dialog);

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

		getWindow().setAttributes(lp);
		
		initUI();
		
	}
	
	
	


	private void initUI() {
		
		bAlarmDialogOk =  (Button) findViewById(R.id.bAlarmDialogOk);
		bAlarmDialogCancel =  (Button) findViewById(R.id.bAlarmDialogCancel);
		
		rgIntervals =  (RadioGroup) findViewById(R.id.rgIntervals);
		
		rbOne =  (RadioButton) findViewById(R.id.rbOne);
		rbTwo =  (RadioButton) findViewById(R.id.rbTwo);
		rbThree =  (RadioButton) findViewById(R.id.rbThree);
		
		tvAlarmDialogMsg =  (TextView) findViewById(R.id.tvAlarmDialogMsg);
		
	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}
	

}
