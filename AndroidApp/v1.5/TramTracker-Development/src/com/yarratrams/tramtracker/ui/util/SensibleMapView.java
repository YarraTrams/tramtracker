package com.yarratrams.tramtracker.ui.util;

import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;



public class SensibleMapView extends MapView {
	private GeoPoint currentCenter;
	private GeoPoint transitionCenter;
	private long eventTimeout = 300L;
	private boolean isTouched = false;
	private boolean beenTouched = false;
	private Timer panEventDelayTimer = new Timer();

	public interface OnPanListener {
	    public void onPan(MapView view, GeoPoint oldCenter, GeoPoint newCenter);
	}
	private OnPanListener panListener;


	
	public SensibleMapView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		currentCenter = this.getMapCenter();
		transitionCenter = this.getMapCenter();
	}

	public SensibleMapView(Context context, String apiKey) {
		super(context, apiKey);
		currentCenter = this.getMapCenter();
		transitionCenter = this.getMapCenter();
	}
	
	public SensibleMapView(Context context, AttributeSet attrs) {
		super(context, attrs);
		currentCenter = this.getMapCenter();
		transitionCenter = this.getMapCenter();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent ev) { 
		if (ev.getAction() == MotionEvent.ACTION_DOWN) {
			isTouched = true;
			beenTouched = true;
		} else if(ev.getAction() == MotionEvent.ACTION_UP){
			isTouched = false;
		}
		return super.onTouchEvent(ev);
	}
	
	@Override
	public void computeScroll() {
		super.computeScroll();

		if(beenTouched){
		    if (!transitionCenter.equals(getMapCenter()) && !isTouched) {
		    	transitionCenter = getMapCenter();
		    	panEventDelayTimer.cancel();
		    	panEventDelayTimer = new Timer();
		    	panEventDelayTimer.schedule(new TimerTask() {
		            @Override
		            public void run() {
		            	firePanEvent(currentCenter, getMapCenter());
		                currentCenter = getMapCenter();
		                beenTouched = false;
		            }
		        }, eventTimeout);
		    }
		}
	}
	
    private void firePanEvent(GeoPoint old, GeoPoint current){
    	panListener.onPan(this, old, current);
    }
    
    public void setOnPanListener(OnPanListener listener){
        this.panListener = listener;
    }
    
	
}