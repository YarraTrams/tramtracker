package com.yarratrams.tramtracker.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.ViewFlipper;

import com.android.androidtooltip.TooltipManager;
import com.android.androidtooltip.TooltipManager.Gravity;
import com.android.androidtooltip.TutorialTooltip;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.OnBoardStopsBySuburb;
import com.yarratrams.tramtracker.objects.PredictedArrivalTime;
import com.yarratrams.tramtracker.objects.PredictedTimeResult;
import com.yarratrams.tramtracker.objects.Range;
import com.yarratrams.tramtracker.objects.ServiceChange;
import com.yarratrams.tramtracker.objects.ServiceDisruption;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.objects.Tram;
import com.yarratrams.tramtracker.singleton.PredictedTimeResultSingleton;
import com.yarratrams.tramtracker.tasks.MyTramLocationListener;
import com.yarratrams.tramtracker.tasks.MyTramTask;
import com.yarratrams.tramtracker.ui.util.FeaturesPreferences;
import com.yarratrams.tramtracker.ui.util.MapMyTramCurrentOverlay;
import com.yarratrams.tramtracker.ui.util.MapMyTramOverlay;
import com.yarratrams.tramtracker.ui.util.OnBoardChangeAlertArrayAdapter;
import com.yarratrams.tramtracker.ui.util.OnBoardDisruptionAlertArrayAdapter;
import com.yarratrams.tramtracker.ui.util.OnBoardExpandableListArrayAdapter;
import com.yarratrams.tramtracker.ui.util.OtherLevelAds;
import com.yarratrams.tramtracker.ui.util.TutorialPreferences;

@SuppressLint("NewApi")
public class OnBoardActivity extends MapActivity
implements
OnClickListener,
TooltipManager.onTooltipClosingCallback {
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_MYTRAM;
	public static final String INTENT_KEY = "tram_info";
	public static final int VIEW_LIST = 0;
	public static final int VIEW_MAP = 1;

	private boolean isListViewSelected;
	private ViewFlipper flipper;

	private Tram tram;

	private ExpandableListView stopsList;
	private OnBoardExpandableListArrayAdapter onBoardAdapter;
	private PredictedTimeResult results;
	private ArrayList<PredictedArrivalTime> stops;
	private int startingIndex;
	private boolean isAccurateIndex;
	private int scrollingPosition;
	private int firstPosition;

	private MapView stopsMap;
	private List<Overlay> mapOverlays;
	private GeoPoint mapCentre;
	private int zoomLevel;

	private ArrayList<ServiceDisruption> serviceDisruptions;
	private ArrayList<ServiceChange> serviceChanges;
	private ListView disruptionAlertList;
	private ListView changeAlertList;
	private OnBoardDisruptionAlertArrayAdapter disruptionAdapter;
	private OnBoardChangeAlertArrayAdapter changeAdapter;

	private boolean isChangeAlertDisplayed;
	private boolean isChangeAlertExpanded;
	private boolean isDisruptionAlertDisplayed;
	private boolean isDisruptionAlertExpanded;

	private boolean showAlerts;
	private boolean pauseWebServiceUpdate;
	private LocationManager locationManager;
	private MyTramLocationListener locationListener;

	private ProgressDialog loadDialog;

	/*
	 * FREE TRAM ZONE
	 */
	public static Map<String, Integer> mapFTZ = new HashMap<String, Integer>();

	/*
	 * FREE TRAM ZONE
	 */
	private ArrayList<OnBoardStopsBySuburb> suburbs;

	// Tutorial Variables
	TooltipManager tooltipManager;
	TutorialTooltip tempTutorialTooltip;
	ArrayList<TutorialTooltip> alTutorialTooltips;
	public boolean isTutorialRunning = false;
	int tooltipIndex = 0;
	TutorialPreferences tutorialPreferences;
	boolean isFirstTime = true;

	private MyTramTask task;
	private Handler updatesHandler;
	private Runnable timeUpdater = new Runnable() {
		public void run() {
			// System.out.println("........... WS: " + pauseWebServiceUpdate);
			if (pauseWebServiceUpdate == false) {
				getTimesUpdate();
				updatesHandler.postDelayed(this, 20000);
			}
		}
	};

	public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			PredictedTimeResult result = PredictedTimeResultSingleton
					.getResult();
			if (!PredictedTimeResultSingleton.isAvailable()) {
				TramTrackerMainActivity.getAppManager().displayErrorMessage(
						getString(R.string.notify_onboard_notramtracker));
				noTimesUpdate(result);
			}
			if (result != null)
				updateUI(result, false);
		}
	};

	Intent alarmServiceIntent;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.onboard_screen);

		isListViewSelected = true;
		flipper = (ViewFlipper) findViewById(R.id.view_flipper);

		tram = new Tram();

		ToggleButton listButton = (ToggleButton) findViewById(R.id.list_view_button);
		listButton.setOnClickListener(this);
		ToggleButton mapButton = (ToggleButton) findViewById(R.id.map_view_button);
		mapButton.setOnClickListener(this);
		Button refreshButton = (Button) findViewById(R.id.refresh_button);
		refreshButton.setOnClickListener(this);
		refreshButton.setVisibility(View.INVISIBLE);

		stops = new ArrayList<PredictedArrivalTime>();
		stopsList = (ExpandableListView) findViewById(R.id.expandable_list);
		stopsList.setKeepScreenOn(true);
		stopsList.addHeaderView(getTableView());

		Display display = getWindowManager().getDefaultDisplay();
		Drawable groupIndicator = getResources().getDrawable(
				R.drawable.icn_list_expandable);
		stopsList.setGroupIndicator(groupIndicator);
		stopsList.setChildIndicator(null);

		if (android.os.Build.VERSION.SDK_INT < 18)
			stopsList.setIndicatorBounds(display.getWidth()
					- GetDipsFromPixel(30), display.getWidth());
		else
			stopsList.setIndicatorBoundsRelative(display.getWidth()
					- GetDipsFromPixel(30), display.getWidth());

		// stopsList.setIndicatorBounds(stopsList.getRight()-GetDipsFromPixel(30),
		// stopsList.getWidth());
		startingIndex = -1;
		isAccurateIndex = false;
		// onBoardAdapter = new OnBoardExpandableListArrayAdapter(stopsList,
		// this, new PredictedTimeResult());
		// stopsList.setAdapter(onBoardAdapter);

		stopsMap = (MapView) findViewById(R.id.map);
		stopsMap.setBuiltInZoomControls(true);
		mapOverlays = stopsMap.getOverlays();

		serviceChanges = new ArrayList<ServiceChange>();
		RelativeLayout changeAlert = (RelativeLayout) findViewById(R.id.change_alert);
		changeAlert.setOnClickListener(this);
		changeAlertList = (ListView) findViewById(R.id.change_list_view);
		changeAdapter = new OnBoardChangeAlertArrayAdapter(this, serviceChanges);
		changeAlertList.setAdapter(changeAdapter);
		createChangeAlert(serviceChanges);

		serviceDisruptions = new ArrayList<ServiceDisruption>();
		RelativeLayout disruptionAlert = (RelativeLayout) findViewById(R.id.disruption_alert);
		disruptionAlert.setOnClickListener(this);
		disruptionAlertList = (ListView) findViewById(R.id.disruption_list_view);
		disruptionAdapter = new OnBoardDisruptionAlertArrayAdapter(this,
				serviceDisruptions);
		disruptionAlertList.setAdapter(disruptionAdapter);
		createDisruptionAlert(serviceDisruptions);

		expandAlert();

		updatesHandler = new Handler();

		loadDialog = new ProgressDialog(this);

		// added by Ab
		// AlarmManager.registerBroastcastReceiver(this);
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		locationListener = new MyTramLocationListener(this, locationManager);

		// call ads
		OtherLevelAds.showAds(this, R.id.rich_banner_fragment1086,FeaturesPreferences.getAdsFlag(TramTrackerMainActivity.instance));

		// tutorial init
		tutorialPreferences = new TutorialPreferences(this);
		tooltipManager = new TooltipManager(TramTrackerMainActivity.instance);
		isFirstTime = true;

	}

	public Context getDialogContext() {
		Context context;
		if (getParent() != null) {
			context = getParent();
		} else {
			context = this;
		}
		return context;
	}

	public int GetDipsFromPixel(float pixels) {
		final float scale = getResources().getDisplayMetrics().density;
		return (int) (pixels * scale + 0.5f);
	}

	private View getTableView() {
		LayoutInflater inflater = getLayoutInflater();
		View rowView = inflater.inflate(R.layout.routes_stops_list_view_table,
				null, true);
		return rowView;
	}

	private void createChangeAlert(ArrayList<ServiceChange> serviceChanges) {
		RelativeLayout changeAlert = (RelativeLayout) findViewById(R.id.change_alert);
		TextView routeTitle = (TextView) findViewById(R.id.pid_change_title);
		routeTitle.setVisibility(View.GONE);
		if (serviceChanges.isEmpty()) {
			changeAlert.setVisibility(View.GONE);
			isChangeAlertDisplayed = false;
		} else {
			if (!isChangeAlertDisplayed) {
				showAlerts = true;
			}
			changeAlert.setVisibility(View.VISIBLE);
			isChangeAlertDisplayed = true;
		}
	}

	private void createDisruptionAlert(
			ArrayList<ServiceDisruption> serviceDisruptions) {
		RelativeLayout disruptionAlert = (RelativeLayout) findViewById(R.id.disruption_alert);
		TextView routeTitle = (TextView) findViewById(R.id.pid_disruption_title);
		if (serviceDisruptions.isEmpty()) {
			disruptionAlert.setVisibility(View.GONE);
			isDisruptionAlertDisplayed = false;
		} else {
			if (!isDisruptionAlertDisplayed) {
				showAlerts = true;
			}
			disruptionAlert.setVisibility(View.VISIBLE);
			isDisruptionAlertDisplayed = true;
			if (serviceDisruptions.get(0).isText()) {
				routeTitle.setVisibility(View.GONE);
			} else {
				routeTitle.setVisibility(View.VISIBLE);
			}
		}
	}

	public void updateUI(PredictedTimeResult results, boolean from_resume) {
		boolean refreshMap = true;
		if (stops != null) {
			if (!stops.isEmpty()) {
				refreshMap = false;
			}
		}

		// System.out.println("....... UPDATE test!!!");
		// PredictedTimeResultSingleton.setResult(results);
		if (results != null) {
			if (this.results != null) {
				if (!this.results.getAlPredictedArrivalTime().isEmpty()
						&& !results.getAlPredictedArrivalTime().isEmpty()) {
					if (this.results.getAlPredictedArrivalTime().get(0)
							.getStop().getTrackerID() != results
							.getAlPredictedArrivalTime().get(0).getStop()
							.getTrackerID())
						startingIndex = -1;
				}
			}

			if (!PredictedTimeResultSingleton.isAvailable()) {
				TramTrackerMainActivity.getAppManager().displayErrorMessage(
						getString(R.string.notify_onboard_notramtracker));
				noTimesUpdate(results);
			}
			this.results = results;

			TextView routeName = (TextView) findViewById(R.id.route_name);
			TextView tramNumber = (TextView) findViewById(R.id.tram_number);
			routeName.setText(getRouteName());
			tramNumber.setText(getTramNumber());

			// System.out.println("............ STOPS UPDATE");
			// System.out.println("............ STOPS " +
			// results.getAlPredictedArrivalTime().size());
			// System.out.println("............ INDEX " + startingIndex);
			// System.out.println("............ NEW " +
			// results.getStartingIndex());
			if (results.getAlPredictedArrivalTime() != null
					&& !results.getAlPredictedArrivalTime().isEmpty()) {
				/*
				 * FREE TRAM ZONE
				 */

				stops = results.getAlPredictedArrivalTime();

				// System.out.println("-- stops: " + stops.size() );

				generateSuburbsList();
			}
			if (stops == null) {
				stops = new ArrayList<PredictedArrivalTime>();
			}

			// ----------------------

			// Setup list
			if (onBoardAdapter == null) {

				System.out.println("-- 1");
				onBoardAdapter = new OnBoardExpandableListArrayAdapter(this,
						results);
				stopsList.setAdapter(onBoardAdapter);
			} else {
				System.out.println("-- 2");
				getListPosition();
				onBoardAdapter = new OnBoardExpandableListArrayAdapter(this,
						results);
				stopsList.setAdapter(onBoardAdapter);
				onBoardAdapter.setStartingIndex(startingIndex, isAccurateIndex);

				restoreListPosition();
				// setListPosition();

				// onBoardAdapter.updateList(results, startingIndex);
			}

			calculateSelectedIndex(results.getStartingIndex());

			expandAll();

			if (refreshMap) {
				centerMapOnStops();
			}

			serviceChanges.clear();
			serviceDisruptions.clear();

			if (results.getServiceChange() != null) {
				serviceChanges.add(results.getServiceChange());
				// System.out.println("-- service changes: " +
				// results.getServiceChange().toString());
			}
			if (results.getServiceDisruption() != null) {
				serviceDisruptions.add(results.getServiceDisruption());
			}
			createChangeAlert(serviceChanges);
			createDisruptionAlert(serviceDisruptions);
			if (showAlerts) {
				expandAlert();
			}

			// show tutorial tip here once list is loaded
			// show tutorial if first time
			if (tutorialPreferences.getMyTramTutorial() == false && isFirstTime && !from_resume) {
				showTutorialDialog();
				isFirstTime = false;
			}

		}

		if (loadDialog.isShowing()) {
			loadDialog.dismiss();
		}
	}

	/*
	 * FREE TRAM ZONE
	 */

	public void generateSuburbsList() {

		// System.out.println("-- In generate suburb list");

		ArrayList<PredictedArrivalTime> ftzstops = results
				.getAlPredictedArrivalTime();
		suburbs = new ArrayList<OnBoardStopsBySuburb>();
		OnBoardStopsBySuburb suburb;

		// Get unique suburbs
		for (PredictedArrivalTime stop : ftzstops) {

			if (suburbs.size() >= 1) {
				suburb = suburbs.get(suburbs.size() - 1);

				// System.out.println("-- suburb.getSuburb():"+suburb.getSuburb()+" stop.getStop():"+stop.getStop()+" stop.getStop().getSuburb():"+stop.getStop().getSuburb());

				try {
					if (!suburb.getSuburb().equalsIgnoreCase(
							stop.getStop().getSuburb())) {
						suburb = new OnBoardStopsBySuburb();
						suburbs.add(suburb);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				suburb = new OnBoardStopsBySuburb();
				suburbs.add(suburb);
			}

			suburb.setSuburb(stop.getStop().getSuburb());
			suburb.addStop(stop);
		}

		/*
		 * Free Tram Zone
		 */

		// Get count of free tram zones inside a suburb
		for (int i = 0; i < suburbs.size(); i++) {
			int counts = 0;
			for (int j = 0; j < suburbs.get(i).getStops().size(); j++) {
				// if(suburbs.get(i).getStops().get(j).getStop().isCityStop())
				try {
					if (suburbs.get(i).getStops().get(j).getStop()
							.IsInFreeZone())
						counts++;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			suburbs.get(i).setftzStopCount(counts);
		}

		// for(int i = 0; i < suburbs.size();i++)
		// {
		// //System.out.println("-- " + suburbs.get(i).getSuburb() + " count: "
		// + suburbs.get(i).getftzStopCount());
		// }

		// Set ranges of FTZ logo
		int index = 0;
		ArrayList<Range> arrlstRanges = new ArrayList<Range>();
		Range tempRange;
		int startIndex = 0, len = 0;

		for (int i = 0; i < suburbs.size(); i++) {
			for (int j = 0; j < suburbs.get(i).getStops().size(); j++) {
				Stop stop = suburbs.get(i).getStops().get(j).getStop();

				if (stop.IsInFreeZone()) {
					len++;
				} else {
					tempRange = new Range(i, startIndex, len);
					arrlstRanges.add(tempRange);
					tempRange = null;
					startIndex = j + 1;
					len = 0;
				}
			}
			tempRange = new Range(i, startIndex, len);
			arrlstRanges.add(tempRange);
			tempRange = null;
			startIndex = 0;
			len = 0;
		}

		// Remove suburbs who has less then two repeating cells
		for (int i = 0; i < arrlstRanges.size(); i++) {

			if (arrlstRanges.get(i).lenght < 3) {

				arrlstRanges.remove(i);
				i--;
			}
		}

		for (int i = 0; i < arrlstRanges.size(); i++) {
			// System.out.println("--  "+ suburbs.get(i).getSuburb()
			// +": ranges  : " + arrlstRanges.get(i).toString());

			int length = arrlstRanges.get(i).lenght;
			int subIndex = arrlstRanges.get(i).suburbIndex;
			int startingIndex = arrlstRanges.get(i).location;

			switch (length) {
				case 3 :

					suburbs.get(subIndex).getStops().get(startingIndex + 0)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 1)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
					.getStop().setFtzImageId(R.drawable.ftz_free);
					break;

				case 4 :

					suburbs.get(subIndex).getStops().get(startingIndex + 0)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 1)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
					.getStop().setFtzImageId(R.drawable.ftz_free);
					break;

				case 5 :

					suburbs.get(subIndex).getStops().get(startingIndex + 1)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 3)
					.getStop().setFtzImageId(R.drawable.ftz_free);
					break;

				case 6 :

					// System.out.println("--calling 6 for suburb : " +
					// suburbs.get(i).getSuburb());
					suburbs.get(subIndex).getStops().get(startingIndex + 1)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 3)
					.getStop().setFtzImageId(R.drawable.ftz_free);
					break;

				case 7 :
					suburbs.get(subIndex).getStops().get(startingIndex + 0)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 1)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 4)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 5)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 6)
					.getStop().setFtzImageId(R.drawable.ftz_free);
					break;

				case 8 :
					suburbs.get(subIndex).getStops().get(startingIndex + 0)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 1)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 4)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 5)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 6)
					.getStop().setFtzImageId(R.drawable.ftz_free);
					break;

				case 9 :
					suburbs.get(subIndex).getStops().get(startingIndex + 1)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 3)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 5)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 6)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 7)
					.getStop().setFtzImageId(R.drawable.ftz_free);
					break;

				case 10 :
					suburbs.get(subIndex).getStops().get(startingIndex + 1)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 3)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 5)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 6)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 7)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					break;

				case 11 :

					suburbs.get(subIndex).getStops().get(startingIndex + 0)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 1)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 4)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 5)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 6)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 8)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 9)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 10)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					break;

				case 12 :

					suburbs.get(subIndex).getStops().get(startingIndex + 0)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 1)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 4)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 5)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 6)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 8)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 9)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 10)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					break;

				case 13 :

					suburbs.get(subIndex).getStops().get(startingIndex + 0)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 1)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 4)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 5)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 6)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 8)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 9)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 10)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					break;

				case 14 :

					suburbs.get(subIndex).getStops().get(startingIndex + 1)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 3)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 5)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 6)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 7)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 9)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 10)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 11)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					break;

				case 15 :

					suburbs.get(subIndex).getStops().get(startingIndex + 0)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 1)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 4)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 5)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 6)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 8)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 9)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 10)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 12)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 13)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 14)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					break;

				case 16 :

					suburbs.get(subIndex).getStops().get(startingIndex + 0)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 1)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 4)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 5)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 6)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 8)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 9)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 10)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 12)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 13)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 14)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					break;

				case 17 :

					suburbs.get(subIndex).getStops().get(startingIndex + 1)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 3)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 5)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 6)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 7)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 9)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 10)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 11)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 13)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 14)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 15)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					break;

				case 18 :

					suburbs.get(subIndex).getStops().get(startingIndex + 1)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 3)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 5)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 6)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 7)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 9)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 10)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 11)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 13)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 14)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 15)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					break;

				case 19 :

					suburbs.get(subIndex).getStops().get(startingIndex + 0)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 1)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 4)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 5)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 6)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 8)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 9)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 10)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 12)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 13)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 14)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 16)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 17)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 18)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					break;

				case 20 :

					suburbs.get(subIndex).getStops().get(startingIndex + 0)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 1)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 4)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 5)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 6)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 8)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 9)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 10)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 12)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 13)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 14)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 16)
					.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 17)
					.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 18)
					.getStop().setFtzImageId(R.drawable.ftz_free);

					break;

			}
		}

		// index = 0;
		//
		// for(int i = 0; i < suburbs.size(); i++)
		// {
		//
		// if (suburbs.get(i).getftzStopCount() > 3)
		// {
		// for (int j = 0; j < suburbs.get(i).getStops().size(); j++)
		// {
		// // if(suburbs.get(i).getStops().get(j).getStop().isCityStop())
		// if (suburbs.get(i).getStops().get(j).getStop()
		// .IsInFreeZone()) {
		// if (index == 0) {
		//
		// // mapFTZ.put(""+i+","+j, R.drawable.ftz_zone);
		// //
		// mapFTZ.put(suburbs.get(i).getStops().get(j).getStop().getStopNumber(),
		// R.drawable.ftz_zone);
		// suburbs.get(i).getStops().get(j).getStop()
		// .setFtzImageId(R.drawable.ftz_zone);
		// index++;
		// }
		// else if (index == 1)
		// {
		//
		// //
		// mapFTZ.put(suburbs.get(i).getStops().get(j).getStop().getStopNumber(),
		// R.drawable.ftz_tram);
		// suburbs.get(i).getStops().get(j).getStop()
		// .setFtzImageId(R.drawable.ftz_tram);
		// index++;
		// }
		// else if (index == 2)
		// {
		//
		// //
		// mapFTZ.put(suburbs.get(i).getStops().get(j).getStop().getStopNumber(),
		// R.drawable.ftz_free);
		// suburbs.get(i).getStops().get(j).getStop()
		// .setFtzImageId(R.drawable.ftz_free);
		// index++;
		// }
		// else if (index == 3)
		// {
		//
		// //
		// mapFTZ.put(suburbs.get(i).getStops().get(j).getStop().getStopNumber(),
		// 0);
		// suburbs.get(i).getStops().get(j).getStop()
		// .setFtzImageId(0);
		// index++;
		// }
		// else
		// {
		// index = 0;
		// }
		// }
		// else
		// {
		//
		// }
		//
		// // System.out.println("ftz  :  "
		// // + mapFTZ.get(suburbs.get(i).getStops().get(j)
		// // .getStop().getStopNumber()));
		//
		// // System.out.println("-- gp: " + i + " cp: " + j + "isCity:" +
		// suburbs.get(i).getStops().get(j).getStop().isCityStop() + " text: " +
		// text );
		// }
		// }
		//
		// }

	}

	private void calculateSelectedIndex(int newIndex) {
		// System.out.println("............ FROM WS");
		if (newIndex == -1) {
			startingIndex = 0;
			isAccurateIndex = false;
			if (onBoardAdapter != null) {
				onBoardAdapter.setStartingIndex(startingIndex, isAccurateIndex);
				updateMapStartingIndex();
				setListPosition();
			}

		} else if (startingIndex == -1 || newIndex > startingIndex) {
			startingIndex = newIndex;
			isAccurateIndex = false;
			if (onBoardAdapter != null) {
				onBoardAdapter.setStartingIndex(startingIndex, isAccurateIndex);
				updateMapStartingIndex();
				setListPosition();
			}
		} else {
			// manualTimesUpdate(startingIndex);
		}
	}

	public void updateStartingIndex(int newIndex) {
		// System.out.println("............ FROM GPS");
		if (startingIndex == -1 || newIndex >= startingIndex) {
			int oldStartingIndex = startingIndex;
			startingIndex = newIndex;
			isAccurateIndex = true;
			manualTimesUpdate(startingIndex);
			if (onBoardAdapter != null) {
				Log.e("", "kickoff stop = "
						+ stops.get(startingIndex).getStop().getStopName()
						+ " startingIndex = " + startingIndex);
				onBoardAdapter.setStartingIndex(results, startingIndex,
						isAccurateIndex);
				updateMapStartingIndex();
				if (newIndex > oldStartingIndex)
					setListPosition();
			}
		} else {
			manualTimesUpdate(startingIndex);
		}
	}

	public void setListPosition() {
		ArrayList<String> alSuburb = new ArrayList<String>();
		Log.w("setListPosition", "stops.size() = " + stops.size());
		for (int i = 0; i < stops.size() && i <= startingIndex; i++) {
			if (alSuburb.size() > 0) {
				// Log.w("setListPosition","alSuburb.get(alSuburb.size() - 1) = "
				// + alSuburb.get(alSuburb.size() - 1));
				// Log.w("setListPosition","stops.get(i).getStop().getSuburb() = "
				// + stops.get(i).getStop().getSuburb());
				if (!alSuburb.get(alSuburb.size() - 1).equalsIgnoreCase(
						stops.get(i).getStop().getSuburb())) {
					alSuburb.add(stops.get(i).getStop().getSuburb());
				}
			} else
				alSuburb.add(stops.get(i).getStop().getSuburb());
		}
		Log.w("setListPosition", "startingIndex = " + startingIndex);
		Log.w("setListPosition", "alSuburb = " + alSuburb);
		int finalIndex = startingIndex + alSuburb.size() + 1;
		Log.w("setListPosition",
				"stopsList.getCount() = " + stopsList.getCount());
		Log.w("setListPosition",
				"setSelectionFromTop  = startingIndex + alSuburb.size() + 1 = "
						+ finalIndex);
		stopsList.setSelectionFromTop(finalIndex, 0);
	}

	private void getListPosition() {
		if (stopsList == null) {
			return;
		}
		firstPosition = stopsList.getFirstVisiblePosition();
		View vfirst = stopsList.getChildAt(0);
		scrollingPosition = 0;
		if (vfirst != null)
			scrollingPosition = vfirst.getTop();
	}

	private void restoreListPosition() {
		if (stopsList == null) {
			return;
		}
		stopsList.setSelectionFromTop(firstPosition, scrollingPosition);
	}

	public void updateStopToNow() {
		if (onBoardAdapter != null) {
			onBoardAdapter.setNowIndex();
		}
	}

	private void manualTimesUpdate(int index) {
		if (results != null) {
			ArrayList<PredictedArrivalTime> times = results
					.getAlPredictedArrivalTime();
			for (int i = 0; i < index; i++) {
				PredictedArrivalTime stop = times.get(index);
				stop.setArrivalTime(null);
			}
		}
	}
	private void noTimesUpdate(PredictedTimeResult result) {
		if (result != null) {
			ArrayList<PredictedArrivalTime> times = result
					.getAlPredictedArrivalTime();
			for (int i = 0; i < times.size(); i++) {
				PredictedArrivalTime stop = times.get(i);
				stop.setArrivalTime(null);
			}
		}
	}

	private void updateMapStartingIndex() {
		// System.out.println("............ STOPS" + stops.size());
		if (stops.isEmpty()) {
			return;
		}

		ArrayList<PredictedArrivalTime> normalStops = new ArrayList<PredictedArrivalTime>();
		normalStops.addAll(stops);
		PredictedArrivalTime stop = stops.get(startingIndex);
		normalStops.remove(startingIndex);

		ArrayList<PredictedArrivalTime> currentStops = new ArrayList<PredictedArrivalTime>();
		currentStops.add(stop);

		if (!isAccurateIndex) {
			if (startingIndex < stops.size() - 1) {
				stop = stops.get(startingIndex + 1);
				normalStops.remove(startingIndex);
				currentStops.add(stop);
			}
		}

		mapOverlays.clear();

		MapMyTramOverlay overlay = new MapMyTramOverlay(this, stopsMap);
		overlay.updateStopsList(normalStops);
		mapOverlays.add(overlay);

		MapMyTramCurrentOverlay currentOverlay = new MapMyTramCurrentOverlay(
				this, stopsMap);
		currentOverlay.updateStopsList(currentStops);
		mapOverlays.add(currentOverlay);

		stopsMap.invalidate();
	}

	private String getRouteName() {
		String text = "";

		text = results.getRoute().getRouteNumber();
		text = text.concat(getResources().getString(
				R.string.routes_entry_name_dash));
		if (results.getRoute().isUpDestination()) {
			if (results.getRoute().getDownDestination() != null) {
				if (results.getRoute().getDownDestination().length() > 0) {
					text = text.concat(results.getRoute().getDownDestination());
				}
			}
			if (results.getRoute().getUpDestination() != null) {
				if (results.getRoute().getUpDestination().length() > 0) {
					text = text.concat(getResources().getString(
							R.string.routes_entry_name_to));
					text = text.concat(results.getRoute().getUpDestination());
				}
			}
		} else {
			if (results.getRoute().getUpDestination() != null) {
				if (results.getRoute().getUpDestination().length() > 0) {
					text = text.concat(results.getRoute().getUpDestination());
				}
			}
			if (results.getRoute().getDownDestination() != null) {
				if (results.getRoute().getDownDestination().length() > 0) {
					text = text.concat(getResources().getString(
							R.string.routes_entry_name_to));
					text = text.concat(results.getRoute().getDownDestination());
				}
			}
		}

		return text;
	}

	private String getTramNumber() {
		String text = "";
		text = getResources().getString(R.string.onboard_title_tram);
		text = text
				.concat(String.valueOf(results.getTram().getVehicleNumber()));
		return text;
	}

	private void expandAlert() {
		if (isDisruptionAlertDisplayed) {
			switchChangeAlertState(false);
			switchDisruptionAlertState(true);
		} else if (isChangeAlertDisplayed) {
			switchChangeAlertState(true);
		}
		showAlerts = false;
	}

	public void switchDisruptionAlertState() {
		LinearLayout list = (LinearLayout) findViewById(R.id.disruption_list);
		TextView showText = (TextView) findViewById(R.id.disruption_show);
		ImageView showIcon = (ImageView) findViewById(R.id.disruption_show_image);
		TextView hideText = (TextView) findViewById(R.id.disruption_hide);
		ImageView hideIcon = (ImageView) findViewById(R.id.disruption_hide_image);
		if (isDisruptionAlertExpanded) {
			TramTrackerMainActivity.getAppManager().callSelection(
					getString(R.string.accessibility_click_pid_hide));
			isDisruptionAlertExpanded = false;
			list.setVisibility(View.GONE);
			hideText.setVisibility(View.GONE);
			hideIcon.setVisibility(View.GONE);
			showText.setVisibility(View.VISIBLE);
			showIcon.setVisibility(View.VISIBLE);
		} else {
			TramTrackerMainActivity.getAppManager().callSelection(
					getString(R.string.accessibility_click_pid_show));
			isDisruptionAlertExpanded = true;
			list.setVisibility(View.VISIBLE);
			hideText.setVisibility(View.VISIBLE);
			hideIcon.setVisibility(View.VISIBLE);
			showText.setVisibility(View.GONE);
			showIcon.setVisibility(View.GONE);
		}
	}

	private void switchDisruptionAlertState(boolean expand) {
		LinearLayout list = (LinearLayout) findViewById(R.id.disruption_list);
		TextView showText = (TextView) findViewById(R.id.disruption_show);
		ImageView showIcon = (ImageView) findViewById(R.id.disruption_show_image);
		TextView hideText = (TextView) findViewById(R.id.disruption_hide);
		ImageView hideIcon = (ImageView) findViewById(R.id.disruption_hide_image);
		if (!expand) {
			isDisruptionAlertExpanded = false;
			list.setVisibility(View.GONE);
			hideText.setVisibility(View.GONE);
			hideIcon.setVisibility(View.GONE);
			showText.setVisibility(View.VISIBLE);
			showIcon.setVisibility(View.VISIBLE);
		} else {
			isDisruptionAlertExpanded = true;
			list.setVisibility(View.VISIBLE);
			hideText.setVisibility(View.VISIBLE);
			hideIcon.setVisibility(View.VISIBLE);
			showText.setVisibility(View.GONE);
			showIcon.setVisibility(View.GONE);
		}
	}

	public void switchChangeAlertState() {
		LinearLayout list = (LinearLayout) findViewById(R.id.change_list);
		TextView showText = (TextView) findViewById(R.id.change_show);
		ImageView showIcon = (ImageView) findViewById(R.id.change_show_image);
		TextView hideText = (TextView) findViewById(R.id.change_hide);
		ImageView hideIcon = (ImageView) findViewById(R.id.change_hide_image);
		if (isChangeAlertExpanded) {
			TramTrackerMainActivity.getAppManager().callSelection(
					getString(R.string.accessibility_click_pid_hide));
			isChangeAlertExpanded = false;
			list.setVisibility(View.GONE);
			hideText.setVisibility(View.GONE);
			hideIcon.setVisibility(View.GONE);
			showText.setVisibility(View.VISIBLE);
			showIcon.setVisibility(View.VISIBLE);
		} else {
			TramTrackerMainActivity.getAppManager().callSelection(
					getString(R.string.accessibility_click_pid_show));
			isChangeAlertExpanded = true;
			list.setVisibility(View.VISIBLE);
			hideText.setVisibility(View.VISIBLE);
			hideIcon.setVisibility(View.VISIBLE);
			showText.setVisibility(View.GONE);
			showIcon.setVisibility(View.GONE);
		}
	}

	private void switchChangeAlertState(boolean expand) {
		LinearLayout list = (LinearLayout) findViewById(R.id.change_list);
		TextView showText = (TextView) findViewById(R.id.change_show);
		ImageView showIcon = (ImageView) findViewById(R.id.change_show_image);
		TextView hideText = (TextView) findViewById(R.id.change_hide);
		ImageView hideIcon = (ImageView) findViewById(R.id.change_hide_image);
		if (!expand) {
			isChangeAlertExpanded = false;
			list.setVisibility(View.GONE);
			hideText.setVisibility(View.GONE);
			hideIcon.setVisibility(View.GONE);
			showText.setVisibility(View.VISIBLE);
			showIcon.setVisibility(View.VISIBLE);
		} else {
			isChangeAlertExpanded = true;
			list.setVisibility(View.VISIBLE);
			hideText.setVisibility(View.VISIBLE);
			hideIcon.setVisibility(View.VISIBLE);
			showText.setVisibility(View.GONE);
			showIcon.setVisibility(View.GONE);
		}
	}

	private void expandAll() {
		for (int i = 0; i < onBoardAdapter.getGroupCount(); i++) {
			stopsList.expandGroup(i);
		}
	}

	public void onResume() {
		
		super.onResume();
		pauseWebServiceUpdate = false;
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			tram = extras.getParcelable(INTENT_KEY);
		}

		if (mapCentre != null) {
			MapController mapController = stopsMap.getController();
			mapController.setCenter(mapCentre);
			mapController.setZoom(zoomLevel);
		}

		if (results != null) {
			updateUI(results, true);
			// System.out.println("........... ON RESUME");
			// System.out.println("..........." +
			// results.getAlPredictedArrivalTime());
			// System.out.println("..........." +
			// results.getAlPredictedArrivalTime().size());
			// onBoardAdapter.updateList(results, startingIndex);
			// stopsList.setAdapter(onBoardAdapter);
		} else {
			if (!loadDialog.isShowing()) {
				loadDialog = ProgressDialog.show(getDialogContext(), "",
						getResources().getString(R.string.dialog_loading),
						true, true);
				// Turned off user clicking outside screen or pressing back
				// button unlessd data has been loaded
				loadDialog.setCanceledOnTouchOutside(false);
				// loadDialog.setCancelable(false);

			}
		}

		if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER, 0, 0, locationListener);
		}

		// locationListener = new MyTramLocationListener(this);
		// locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
		// Constants.kLocationServicesMinUpdateTime,
		// Constants.kLocationServicesMinUpdateDistance, locationListener);
		updatesHandler.post(timeUpdater);
	}

	@Override
	public void onPause() {
		super.onPause();
		pauseWebServiceUpdate = true;
		locationManager.removeUpdates(locationListener);
		zoomLevel = stopsMap.getZoomLevel();
		mapCentre = stopsMap.getMapCenter();
		try {
			tempTutorialTooltip.closeTutorialTooltip(tooltipManager);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void switchViews() {
		ToggleButton listButton = (ToggleButton) findViewById(R.id.list_view_button);
		ToggleButton mapButton = (ToggleButton) findViewById(R.id.map_view_button);
		Button refreshButton = (Button) findViewById(R.id.refresh_button);
		if (isListViewSelected) {
			isListViewSelected = false;
			listButton.setChecked(false);
			mapButton.setChecked(true);
			refreshButton.setVisibility(View.VISIBLE);
			flipper.setDisplayedChild(VIEW_MAP);
			TramTrackerMainActivity.getAppManager().callSelection(
					getString(R.string.accessibility_click_header_mapview));
		} else {
			isListViewSelected = true;
			listButton.setChecked(true);
			mapButton.setChecked(false);
			refreshButton.setVisibility(View.INVISIBLE);
			flipper.setDisplayedChild(VIEW_LIST);
			TramTrackerMainActivity.getAppManager().callSelection(
					getString(R.string.accessibility_click_header_listview));
		}
	}

	// public void centerMapOn(GeoPoint center, int max, boolean islat){
	// MapController mapController = stopsMap.getController();
	//
	// if(islat){
	// if(max < 20000){
	// zoomLevel = 16; // Zoom 16 few blocks
	// } else if(max < 30000){
	// zoomLevel = 15;
	// } else if(max < 50000){
	// zoomLevel = 14;
	// } else if(max < 70000){
	// zoomLevel = 13;
	// } else if(max < 131000){
	// zoomLevel = 12;
	// } else if(max < 150000){
	// zoomLevel = 11;
	// } else {
	// zoomLevel = 10; // Zoom 10 is city view
	// }
	// } else {
	// if(max < 25000){
	// zoomLevel = 16; // Zoom 16 few blocks
	// } else if(max < 30000){
	// zoomLevel= 15;
	// } else if(max < 80000){
	// zoomLevel = 14;
	// } else if(max < 100000){
	// zoomLevel = 13;
	// } else if(max < 190000){
	// zoomLevel = 12;
	// } else if(max < 250000){
	// zoomLevel = 11;
	// } else {
	// zoomLevel = 10; // Zoom 10 is city view
	// }
	// }
	//
	// mapController.setZoom(zoomLevel);
	// if(center != null){
	// mapCentre = center;
	// mapController.animateTo(mapCentre);
	// }
	// }

	public void centerMapOnStops() {
		int maxLat = 0;
		int minLat = 0;
		int maxLon = 0;
		int minLon = 0;

		// phoenix added the if condition 16.Jan.2014
		if (results.getAlPredictedArrivalTime().size() > 0) {
			for (PredictedArrivalTime stop : results
					.getAlPredictedArrivalTime()) {
				if (maxLat == 0) {
					maxLat = stop.getStop().getLatitudeE6();
					minLat = stop.getStop().getLatitudeE6();
					maxLon = stop.getStop().getLongitudeE6();
					minLon = stop.getStop().getLongitudeE6();
				}
				maxLat = stop.getStop().getLatitudeE6() > maxLat ? stop
						.getStop().getLatitudeE6() : maxLat;
						minLat = stop.getStop().getLatitudeE6() < minLat ? stop
								.getStop().getLatitudeE6() : minLat;
								maxLon = stop.getStop().getLongitudeE6() > maxLon ? stop
										.getStop().getLongitudeE6() : maxLon;
										minLon = stop.getStop().getLongitudeE6() < minLon ? stop
												.getStop().getLongitudeE6() : minLon;
			}
		}

		if (maxLat == 0) {
			// The map is zoomed and centered at Flinders St Station
			maxLat = (int) (-37.817491 * 1E6);
			minLat = (int) (-37.817491 * 1E6);
			maxLon = (int) (144.967445 * 1E6);
			minLon = (int) (144.967445 * 1E6);
		}

		int dLat = maxLat - minLat;
		int dLon = maxLon - minLon;

		int cLat = minLat + (dLat / 2);
		int cLon = minLon + (dLon / 2);

		// int max = 0;
		// boolean isLat = false;
		// if(dLat == 0 && dLon == 0){
		// max = 150000;
		// } else {
		// if(dLat > dLon){
		// max = dLat;
		// isLat = true;
		// } else{
		// max = dLon;
		// isLat = false;
		// }
		// }

		GeoPoint centre = new GeoPoint(cLat, cLon);
		// centerMapOn(centre, max, isLat);
		MapController mapController = stopsMap.getController();
		mapController.zoomToSpan(dLat, dLon);
		if (centre != null) {
			mapCentre = centre;
			mapController.animateTo(mapCentre);
		}
	}

	private void refresh() {
		centerMapOnStops();
	}

	@Override
	public void onClick(View v) {
		ToggleButton listButton = (ToggleButton) findViewById(R.id.list_view_button);
		ToggleButton mapButton = (ToggleButton) findViewById(R.id.map_view_button);
		Button refreshButton = (Button) findViewById(R.id.refresh_button);
		RelativeLayout changeAlert = (RelativeLayout) findViewById(R.id.change_alert);
		RelativeLayout disruptionAlert = (RelativeLayout) findViewById(R.id.disruption_alert);

		if (v == listButton || v == mapButton) {
			switchViews();
		} else if (v == refreshButton) {
			TramTrackerMainActivity.getAppManager().callSelection(
					getString(R.string.accessibility_click_header_refresh));
			refresh();
		} else if (v == changeAlert) {
			switchChangeAlertState();
		} else if (v == disruptionAlert) {
			switchDisruptionAlertState();
		}
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	private void getTimesUpdate() {
		task = new MyTramTask(this, tram);
		task.execute();
	}

	// Tutorial Methods
	@Override
	public void onClosing(int id, boolean fromUser, boolean containsTouch) {
		// TODO Auto-generated method stub
		if (isTutorialRunning) {
			isTutorialRunning = false;
			tutorialPreferences.setMyTramTutorial(true);
			Toast.makeText(
					this,
					"Thank you for completing this tutorial. Get more tutorials in the tram arrival screen, Timetables and my tramTRACKER.",
					1).show();
		}
	}

	public void createTutorialToolTips() {

		//String disruptionsText = "You can now set alarms for when your stop is approaching by long-pressing on the stop cell.\nTAP HERE to see how.";
		//		String alarmText = "<b>Long-press</b> here to set your alarm.";
		String alarmText = "LONG-PRESS here to receive an alarm when you are due to arrive at your destination.<br/><br/>Or TAP HERE to dismiss this tooltip.";

		alTutorialTooltips = new ArrayList<TutorialTooltip>();

		// mytram tutorial tip #0
		tempTutorialTooltip = new TutorialTooltip();
		int visibleChildCount = (stopsList.getLastVisiblePosition() - stopsList
				.getFirstVisiblePosition()) + 1;
		System.out.println("-- tool tip onboard pos: " + visibleChildCount);

		WindowManager manager = (WindowManager) this
				.getSystemService(Activity.WINDOW_SERVICE);
		int width, height;

		if (Build.VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB_MR2) {
			DisplayMetrics metrics = new DisplayMetrics();
			manager.getDefaultDisplay().getMetrics(metrics);
			width = metrics.widthPixels;
		} else {
			width = manager.getDefaultDisplay().getWidth();
		}

		try {

			if(stopsList.getChildAt(2)!=null)
			{
				//				tempTutorialTooltip.createTutorialTooltipNoAnchor(tooltipManager,
				//						stopsList.getChildAt(2), this, Gravity.TOP, disruptionsText,width *95 / 100);
				//				alTutorialTooltips.add(tempTutorialTooltip);
				// tempTutorialTooltip.createTutorialTooltip( tooltipManager,
				// onBoardAdapter.get, this, Gravity.BOTTOM, disruptionsText);
				
				
				
				View v;
				
				if(stopsList.getChildAt(3)!=null)
				{
					v=  stopsList.getChildAt(3);
				}
				else if(stopsList.getChildAt(4)!=null)
				{
					v=  stopsList.getChildAt(4);
				} 
				else
				{
					v=  stopsList.getChildAt(2);
				}


				tempTutorialTooltip = new TutorialTooltip();
				tempTutorialTooltip.createTutorialTooltip(tooltipManager,
						v, this, Gravity.TOP, alarmText);
				alTutorialTooltips.add(tempTutorialTooltip);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

	public void showNextTutorialTooltip() {
		try {

			alTutorialTooltips.get(tooltipIndex).closeTutorialTooltip(
					tooltipManager);
		} catch (Exception e) {
			// TODO: handle exception
		}

		tooltipIndex++;
		if (tooltipIndex < alTutorialTooltips.size()) {
			try {
				alTutorialTooltips.get(tooltipIndex).showTooltip();
			} catch (Exception e) {
				// TODO: handle exception
				isTutorialRunning = false;
			}
		} else {
			isTutorialRunning = false;
		}
	}

	public void dismissTutorial(){
		try {

			alTutorialTooltips.get(0).closeTutorialTooltip(
					tooltipManager);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void showTutorialDialog() {

		//		String msg = "On a tram and want to be notified when you should prepare to alight? Tap 'View Now' to learn how.\n\nIf you would like to skip the tutorial and view it next time, please select the 'Remind Me Later' option below.";
		String msg = "To improve your tramTRACKER\nexperience, there are quick tutorials to guide you through the new features.\n\nTo turn off this reminder go to More > my tramTRACKER > Interactive Tutorials.";
		AlertDialog.Builder builder = new Builder(
				TramTrackerMainActivity.instance);
		builder.setTitle("Interactive Tutorial");
		builder.setMessage(msg);

		builder.setPositiveButton("View Now",
				new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				createTutorialToolTips();
				tooltipIndex = 0;
				if (alTutorialTooltips != null) {
					isTutorialRunning = true;
					alTutorialTooltips.get(tooltipIndex).showTooltip();
				}
				dialog.dismiss();

			}
		});
		builder.setNegativeButton("Remind Me Later",
				new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();

			}
		});

		AlertDialog alertDialog = builder.create();
		alertDialog.setCanceledOnTouchOutside(false);
		alertDialog.setCancelable(false);


		if(onBoardAdapter!=null)
		{
			if(onBoardAdapter.getGroupCount() > 0)
			{

				alertDialog.show();
			}
		}

	}

}
