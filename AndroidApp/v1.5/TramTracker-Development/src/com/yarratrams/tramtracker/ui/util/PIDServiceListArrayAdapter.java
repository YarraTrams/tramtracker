package com.yarratrams.tramtracker.ui.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import android.app.ActivityGroup;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.DestinationGroup;
import com.yarratrams.tramtracker.objects.Reminders;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.objects.Tram;
import com.yarratrams.tramtracker.ui.OnBoardActivity;
import com.yarratrams.tramtracker.ui.PIDActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;

public class PIDServiceListArrayAdapter extends ArrayAdapter<DestinationGroup> {
	private final PIDActivity context;
	private ArrayList<DestinationGroup> groups;
	private ArrayList<String> expanded;
	Stop stop;
	// int minutes;
	// int currMinutes = 0;

	// Adil Added
	AlarmDialog alarmDialog;
	boolean OKbuttonDismiss = false;
	int selectedInterval;
	ReminderPreferences reminderPreferences;
	Reminders reminder = null;

	public PIDServiceListArrayAdapter(PIDActivity context,
			ArrayList<DestinationGroup> groups, Stop stop) {
		super(context, R.layout.pid_routes_detail, groups);
		this.context = context;
		this.groups = groups;
		this.stop = stop;

		Console.print("stop in constructors:" + stop);

		reminderPreferences = new ReminderPreferences(context);
		this.expanded = new ArrayList<String>();
	}

	public void updateList(ArrayList<DestinationGroup> groups) {

		this.groups.clear();
		this.groups.addAll(groups);

		/*
		 * adil added
		 */
		// generateGroupNearestTimeList(this.groups);
		// Collections.sort(this.groups, new PIDTimeSort());

		/*
		 * 
		 */

		super.notifyDataSetChanged();

		System.out.println("-- data set changed");
	}

	static class ServiceGroupViewHolder {
		TableLayout routeTable;
		TableRow routeCell1;
		TableRow routeCell2;
		ImageView expandArrow;
		ImageView retractArrow;
		// ImageView expandDivider;

		ServiceViewHolder service;
		ServiceViewHolder service1;
		ServiceViewHolder service2;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ServiceGroupViewHolder viewHolder;

		if (convertView == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(R.layout.pid_routes_detail, parent,
					false);

			viewHolder = new ServiceGroupViewHolder();
			viewHolder.routeTable = (TableLayout) convertView
					.findViewById(R.id.service_table);
			viewHolder.routeCell1 = (TableRow) convertView
					.findViewById(R.id.service_row1);
			viewHolder.routeCell2 = (TableRow) convertView
					.findViewById(R.id.service_row2);
			viewHolder.expandArrow = (ImageView) convertView
					.findViewById(R.id.imageView_expand_cell);
			viewHolder.retractArrow = (ImageView) convertView
					.findViewById(R.id.imageView_retract_cell);
			// viewHolder.expandDivider = (ImageView)
			// convertView.findViewById(R.id.imageView_expand_cell_divider);

			TableRow row = (TableRow) convertView
					.findViewById(R.id.service_row);
			viewHolder.service = createServiceViewHolder(row);
			TableRow row1 = (TableRow) convertView
					.findViewById(R.id.service_row1);
			viewHolder.service1 = createServiceViewHolder(row1);
			TableRow row2 = (TableRow) convertView
					.findViewById(R.id.service_row2);
			viewHolder.service2 = createServiceViewHolder(row2);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ServiceGroupViewHolder) convertView.getTag();
		}

		final DestinationGroup group = groups.get(position);

		if (groups.size() <= 1
				&& viewHolder.routeCell1.getVisibility() == View.GONE) {
			switchCellState(viewHolder, group, position);
		}

		setServiceInfo(viewHolder.service, group, 0);
		setServiceInfo(viewHolder.service1, group, 1);
		setServiceInfo(viewHolder.service2, group, 2);

		if (group.isGroup()) {
			expandGroup(viewHolder, group);
			viewHolder.routeTable.setOnClickListener(null);

		} else {
			// viewHolder.expandDivider.setVisibility(View.VISIBLE);
			viewHolder.routeTable
					.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							switchCellState(viewHolder, group, position);
						}
					});

			if (expanded(group)) {
				setCellState(viewHolder, true);
			} else {
				setCellState(viewHolder, false);
			}

		}

		/*
		 * Adil added : long listener
		 */
		// convertView.setOnLongClickListener(new OnLongClickListener() {
		//
		// @Override
		// public boolean onLongClick(View v) {
		// // TODO Auto-generated method stub
		// TextView tramMins = (TextView)
		// v.findViewById(R.id.service_route_mins);
		// Toast.makeText(context, "long pressed at pos : " + position +
		// "group: " + tramMins.getText().toString() , 0).show();
		//
		//
		//
		//
		// return true;
		// }
		// });

		/*
		 * ends
		 */

		return convertView;
	}

	// /*
	// * Adil added
	// */
	//
	// private void generateGroupNearestTimeList(ArrayList<DestinationGroup>
	// groups) {
	// // TODO Auto-generated method stub
	//
	//
	//
	// long min = 0;
	//
	// for(int i = 0 ; i < this.groups.size(); i++)
	// {
	//
	//
	//
	// for(int r = 0 ; r< this.groups.get(i).getRoutes().size() ; r++)
	// {
	// //setting inital min value
	// min =
	// this.groups.get(i).getRoutes().get(r).getNextServices().get(0).getArrivalTime().getTime();
	//
	// for(int t = 0 ; t <
	// this.groups.get(i).getRoutes().get(r).getNextServices().size(); t++)
	// {
	// //System.err.println("-- data: " + groups.get(i).getDestination() +
	// " routes: " + groups.get(i).getRoutes().get(r).getRouteNumber() +
	// " tramtime: " +
	// groups.get(i).getRoutes().get(r).getNextServices().get(t).getArrivalTime());
	//
	// if(this.groups.get(i).getRoutes().get(r).getNextServices().get(t).getArrivalTime().getTime()
	// < min)
	// {
	// min =
	// this.groups.get(i).getRoutes().get(r).getNextServices().get(t).getArrivalTime().getTime()
	// ;
	//
	//
	//
	// }
	//
	// }
	// }
	//
	// System.out.println(" --  group : " + this.groups.get(i).getDestination()
	// + " min : " + min );
	// this.groups.get(i).setTramEarliestTime(new Date(min));
	// System.out.println(" --  group time saved : " +
	// this.groups.get(i).getTramEarliestTime().getTime() );
	//
	//
	// }
	// }
	//
	// /*
	// * Ends
	// */

	public void goToOnBoardScreen(Tram service) {
		ActivityGroup group = TramTrackerMainActivity.getAppManager()
				.requestDifferentTab(TramTrackerMainActivity.TAB_MYTRAM);
		Intent intent = new Intent(group, OnBoardActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(OnBoardActivity.INTENT_KEY, service);
		Log.e("goToOnBoardScreen", "service = " + service);
		TramTrackerMainActivity.getAppManager().show(
				TramTrackerMainActivity.TAB_MYTRAM,
				context.getResources().getString(R.string.tag_onboard_screen),
				intent);
	}

	public void switchCellState(ServiceGroupViewHolder viewHolder,
			DestinationGroup group, int index) {
		if (expanded(group)) { // EXPANDED
			TramTrackerMainActivity
					.getAppManager()
					.callSelection(
							context.getString(R.string.accessibility_click_pid_collapse));
			setCellState(viewHolder, false);
			removeFromExpandList(group);
		} else { // RETRACTED
			TramTrackerMainActivity.getAppManager().callSelection(
					context.getString(R.string.accessibility_click_pid_expand));
			setCellState(viewHolder, true);
			addToExpandList(group);
			context.setCurrentIndex(index);
		}
	}

	public void setCellState(ServiceGroupViewHolder viewHolder, boolean expand) {
		if (expand) { // EXPAND
			viewHolder.expandArrow.setVisibility(View.INVISIBLE);
			viewHolder.retractArrow.setVisibility(View.VISIBLE);
			viewHolder.routeCell1.setVisibility(View.VISIBLE);
			viewHolder.routeCell2.setVisibility(View.VISIBLE);
		} else { // RETRACT
			viewHolder.expandArrow.setVisibility(View.VISIBLE);
			viewHolder.retractArrow.setVisibility(View.INVISIBLE);
			viewHolder.routeCell1.setVisibility(View.GONE);
			viewHolder.routeCell2.setVisibility(View.GONE);
		}
	}

	private void expandGroup(ServiceGroupViewHolder viewHolder,
			DestinationGroup group) {
		viewHolder.expandArrow.setVisibility(View.INVISIBLE);
		viewHolder.retractArrow.setVisibility(View.INVISIBLE);
		// viewHolder.expandDivider.setVisibility(View.INVISIBLE);
		viewHolder.routeCell1.setVisibility(View.VISIBLE);
		viewHolder.routeCell2.setVisibility(View.VISIBLE);

		addToExpandList(group);
	}

	public void addToExpandList(DestinationGroup group) {
		if (!expanded(group)) {
			if (group.isGroup()) {
				if (group.isGroupByDestination()) {
					expanded.add(group.getDestination());
				} else {
					expanded.add(group.getHeadboardRoute());
				}
			} else {
				expanded.add(group.getDestination());
			}
		}
	}

	public void removeFromExpandList(DestinationGroup group) {
		expanded.remove(group.getDestination());
		expanded.remove(group.getHeadboardRoute());
	}

	public boolean expanded(DestinationGroup group) {
		for (String cell : expanded) {
			if (cell.equals(group.getDestination())
					|| cell.equals(group.getHeadboardRoute())) {
				return true;
			}
		}
		return false;
	}

	static class ServiceViewHolder {
		TextView routeNumber;
		TextView tramDestination;
		ImageView tramType;
		ImageView hasAccess;
		ImageView hasAirCon;
		ImageView isServiceChanged;
		ImageView isDisrupted;
		TextView tramMins;
		TextView tramDay;
		TextView tramAM;
		RelativeLayout onboardButton;
		ImageView onboardChevron;
		RelativeLayout onBoardTime;
	}

	private ServiceViewHolder createServiceViewHolder(TableRow rowView) {
		ServiceViewHolder viewHolder = new ServiceViewHolder();
		viewHolder.routeNumber = (TextView) rowView
				.findViewById(R.id.service_route_number);
		viewHolder.tramDestination = (TextView) rowView
				.findViewById(R.id.service_route_direction);
		viewHolder.tramType = (ImageView) rowView
				.findViewById(R.id.service_tram_type);
		viewHolder.hasAccess = (ImageView) rowView
				.findViewById(R.id.service_tram_access);
		viewHolder.hasAirCon = (ImageView) rowView
				.findViewById(R.id.service_tram_aircon);
		viewHolder.isServiceChanged = (ImageView) rowView
				.findViewById(R.id.service_tram_change);
		viewHolder.isDisrupted = (ImageView) rowView
				.findViewById(R.id.service_tram_disruption);
		viewHolder.tramMins = (TextView) rowView
				.findViewById(R.id.service_route_mins);
		viewHolder.tramDay = (TextView) rowView
				.findViewById(R.id.service_route_day);
		viewHolder.tramAM = (TextView) rowView
				.findViewById(R.id.service_route_am);
		viewHolder.onboardButton = (RelativeLayout) rowView
				.findViewById(R.id.tram_onboard);
		viewHolder.onboardChevron = (ImageView) rowView
				.findViewById(R.id.tram_onboard_chevron);
		viewHolder.onBoardTime = (RelativeLayout) rowView
				.findViewById(R.id.tram_onboard_time);

		return viewHolder;
	}

	public void setServiceInfo(final ServiceViewHolder viewHolder,
			final DestinationGroup group, int next) {
		if (group.size() <= next) {
			setEmptyServiceInfo(viewHolder);
			return;
		}

		final Tram service = group.getNextServiceTram(next);

		if (next == 0) {
			viewHolder.routeNumber.setText(service.getHeadboardNo());
		} else {
			if (group.isGroup()) {
				viewHolder.routeNumber.setText(service.getHeadboardNo());
			} else {
				viewHolder.routeNumber.setText("");
			}
		}

		viewHolder.tramDestination.setText(service.getDestination());
		viewHolder.tramType.setVisibility(View.VISIBLE);
		// System.out.println("service.getTramClass() = " +
		// service.getVehicleNumber());
		if (service.getTramClass() == Tram.CLASS_Z1) {
			viewHolder.tramType.setImageResource(R.drawable.tram_z);
			scaleImage(viewHolder.tramType, 400 + 50);
		} else if (service.getTramClass() == Tram.CLASS_Z2) {
			viewHolder.tramType.setImageResource(R.drawable.tram_z);
			scaleImage(viewHolder.tramType, 400 + 50);
		} else if (service.getTramClass() == Tram.CLASS_Z3) {
			viewHolder.tramType.setImageResource(R.drawable.tram_z3);
			scaleImage(viewHolder.tramType, 400 + 50);
		} else if (service.getTramClass() == Tram.CLASS_A1) {
			viewHolder.tramType.setImageResource(R.drawable.tram_a);
			scaleImage(viewHolder.tramType, 370 + 50);
		} else if (service.getTramClass() == Tram.CLASS_A2) {
			viewHolder.tramType.setImageResource(R.drawable.tram_a);
			scaleImage(viewHolder.tramType, 370 + 50);
		} else if (service.getTramClass() == Tram.CLASS_W) {
			viewHolder.tramType.setImageResource(R.drawable.tram_w);
			scaleImage(viewHolder.tramType, 385 + 50);
		} else if (service.getTramClass() == Tram.CLASS_B1) {
			viewHolder.tramType.setImageResource(R.drawable.tram_b);
			scaleImage(viewHolder.tramType, 430 + 50);
		} else if (service.getTramClass() == Tram.CLASS_B2) {
			viewHolder.tramType.setImageResource(R.drawable.tram_b);
			scaleImage(viewHolder.tramType, 430 + 50);
		} else if (service.getTramClass() == Tram.CLASS_C1) {
			viewHolder.tramType.setImageResource(R.drawable.tram_c1);
			scaleImage(viewHolder.tramType, 420 + 50);
		} else if (service.getTramClass() == Tram.CLASS_D1) {
			viewHolder.tramType.setImageResource(R.drawable.tram_d1);
			scaleImage(viewHolder.tramType, 410 + 50);
		} else if (service.getTramClass() == Tram.CLASS_D2) {
			viewHolder.tramType.setImageResource(R.drawable.tram_d2);
			scaleImage(viewHolder.tramType, 480 + 50);
		} else if (service.getTramClass() == Tram.CLASS_C2) {
			viewHolder.tramType.setImageResource(R.drawable.tram_c2);
			scaleImage(viewHolder.tramType, 500 + 50);
		} else if (service.getTramClass() == Tram.CLASS_E) {
			viewHolder.tramType.setImageResource(R.drawable.tram_e);
			scaleImage(viewHolder.tramType, 490 + 50);
		} else {
			viewHolder.tramType.setVisibility(View.INVISIBLE);
		}
		
		
		if (service.isLowFloor()) {
			viewHolder.hasAccess.setVisibility(View.VISIBLE);
		} else {
			viewHolder.hasAccess.setVisibility(View.GONE);
		}
		if (service.isHasAirCon()) {
			viewHolder.hasAirCon.setVisibility(View.VISIBLE);
		} else {
			viewHolder.hasAirCon.setVisibility(View.GONE);
		}
		if (service.hasSpecialEvent()) {
			viewHolder.isServiceChanged.setVisibility(View.VISIBLE);
		} else {
			viewHolder.isServiceChanged.setVisibility(View.GONE);
		}
		if (service.isDisrupted()) {
			viewHolder.isDisrupted.setVisibility(View.VISIBLE);
		} else {
			viewHolder.isDisrupted.setVisibility(View.GONE);
		}

		/*
		 * Adil Changed
		 */
		// minutes = service.getMinutesFromServerResponseTime();
		final int minutes = service.getMinutesFromServerResponseTime();
		/*
		 * Ends
		 */

		// System.out.println("-- time of pid   "+ service.getInternalRouteNo()
		// + " == " + minutes);
		if (minutes == 0) {
			viewHolder.tramMins.setText(context.getResources().getString(
					R.string.route_route0Mins));
			viewHolder.tramMins.setTextSize(16);
			viewHolder.tramDay.setVisibility(View.GONE);
			viewHolder.tramAM.setVisibility(View.GONE);
		} else if (minutes > 60) {
			viewHolder.tramMins.setTextSize(13);
			viewHolder.tramMins.setText(getArrivalTimeString(service
					.getArrivalTime()));
			viewHolder.tramDay.setText(getArrivalDayString(service
					.getArrivalTime()));
			viewHolder.tramDay.setVisibility(View.VISIBLE);
			viewHolder.tramDay.setTextSize(11);
			if (android.text.format.DateFormat.is24HourFormat(context)) {
				viewHolder.tramAM.setVisibility(View.GONE);
			} else {
				viewHolder.tramAM.setText(getArrivalAMString(service
						.getArrivalTime()));
				viewHolder.tramAM.setVisibility(View.VISIBLE);
				viewHolder.tramAM.setTextSize(10);
			}
		} else {
			viewHolder.tramMins.setText(String.valueOf(minutes));
			viewHolder.tramMins.setTextSize(20);
			viewHolder.tramDay.setVisibility(View.GONE);
			viewHolder.tramAM.setVisibility(View.GONE);
		}

		if (service.getVehicleNumber() > 0) { // && route.isMainRoute()
			viewHolder.onboardChevron.setVisibility(View.VISIBLE);
			viewHolder.onboardButton
					.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							TramTrackerMainActivity.getAppManager()
									.callSelection(getDescription(service));
							goToOnBoardScreen(service);
						}
					});

			/*
			 * Adil added: long click listener
			 */

			viewHolder.onBoardTime
					.setOnLongClickListener(new OnLongClickListener() {

						@Override
						public boolean onLongClick(View v) {
							// TODO Auto-generated method stub
							// Toast.makeText(context, "Mins: " + minutes,
							// 0).show();

							final int currMins = getMinutes(viewHolder.tramMins);

							alarmDialog = new AlarmDialog(
									TramTrackerMainActivity.instance);

							// Start a auto dismiss timer
							final Timer t = new Timer();
							t.schedule(new TimerTask() {
								public void run() {
									alarmDialog.dismiss();
									t.cancel();
								}
							}, 30000);

							alarmDialog.rbOne.setText("2 minutes");
							alarmDialog.rbTwo.setText("5 minutes");
							alarmDialog.rbThree.setText("10 minutes");
							alarmDialog.setTitle("Tram Alarm");

							Console.print("Stop in pid adap:" + " --  "
									+ stop.toString());

							final String time;
							DateFormat format = null;

							if (android.text.format.DateFormat.is24HourFormat(context)) {
								format = new SimpleDateFormat(context.getResources().getString(
										R.string.onboard_time_format));
							} else {
								format = new SimpleDateFormat(context.getResources().getString(
										R.string.onboard_time_format_H));

							}
							/*
							 * Ended
							 */
							time = format.format(service.getArrivalTime());
							
							alarmDialog.tvAlarmDialogMsg
									.setText(AlarmDialog.STOP_ALARM_MSG_1
											+ stop.getStopDescription() + " at "
											+ time
											+ AlarmDialog.STOP_ALARM_MSG_2);
							
//							alarmDialog.tvAlarmDialogMsg
//							.setText(AlarmDialog.STOP_ALARM_MSG_1
//									+ stop.getStopDescription()
//									+ AlarmDialog.STOP_ALARM_MSG_2);

							alarmDialog.bAlarmDialogCancel
									.setOnClickListener(new OnClickListener() {

										@Override
										public void onClick(View arg0) {
											// TODO Auto-generated method stub
											alarmDialog.dismiss();
											t.cancel();

										}
									});

							alarmDialog.bAlarmDialogOk
									.setOnClickListener(new OnClickListener() {

										@Override
										public void onClick(View v) {
											// TODO Auto-generated method stub

											int id = alarmDialog.rgIntervals
													.getCheckedRadioButtonId();

											switch (id) {
												case R.id.rbOne :
													selectedInterval = 2;
													break;
												case R.id.rbTwo :
													selectedInterval = 5;
													break;

												case R.id.rbThree :
													selectedInterval = 10;
													break;

												default :
													break;
											}

											System.out.println("-- minutes: "
													+ currMins);
											if (isValidReminder(currMins)) {
												// Toast.makeText(TramTrackerMainActivity.instance,
												// "valid reminder: " +
												// selectedInterval , 0).show();

												// System.out.println("-- stop: "
												// + stop.getStopName());
												reminder = new Reminders();
												reminder.setActive(true);
												reminder.setDirection(stop
														.getCityDirection());
												reminder.setFreeTramZone(stop
														.IsInFreeZone());
												reminder.setREMINDER_TYPE(Reminders.TRAM_ALARM);
												reminder.setReminderTime(Reminders
														.getStopAlarmReminderTime(
																service.getArrivalTime(),
																selectedInterval));
												// Testing differnet logic

												reminder.setRoute(service
														.getHeadboardNo());
												reminder.setSetTime(service
														.getArrivalTime()
														.getTime());
												reminder.setStopName(stop
														.getStopName());
												reminder.setTrackerID(String.valueOf(stop
														.getTrackerID()));
												reminder.setTramClass(service
														.getTramClass());
												reminder.setStop(stop);

												if (reminderPreferences
														.isDuplicate(reminder)) {
													Toast.makeText(
															TramTrackerMainActivity.instance,
															"Duplicate alarm",
															1).show();
												} else {
													reminderPreferences
															.addNewReminder(reminder);

													OKbuttonDismiss = true;
													alarmDialog.dismiss();
													showReminderSetToast(
															minutes, reminder);
													
													final String msgs = "Thank you for completing this tutorial. Get more tutorials in my tramTRACKER, myTRAM and Timetables.";
													if (context.isTutorialRunning
															&& context.tooltipIndex >= 2) {
														context.isTutorialRunning = false;
														new TutorialPreferences(context)
																.setPidTutorial(true);
														context.dismissAlarmTooltip();
														Toast.makeText(context, msgs, 1).show();
													}

												}

												t.cancel();
											} else {

												// String msg =
												// "We’re unable to set an alarm for this stop as your tram is scheduled to arrive here within the next ";
												String msg = "You can't set this alarm as your tram is due to arrive within "
														+ minutes + " minutes.";
												Toast.makeText(
														TramTrackerMainActivity.instance,
														msg, 1).show();
											}

											// t.cancel();
										}

									});

							alarmDialog.show();

							return true;
						}
					});

			/*
			 * End
			 */

		}

		else {
			viewHolder.onboardChevron.setVisibility(View.INVISIBLE);
			viewHolder.onboardButton.setOnClickListener(null);
		}
	}

	protected int getMinutes(TextView tramMins) {
		// TODO Auto-generated method stub
		int mins = 0;
		try {
			mins = Integer.parseInt(tramMins.getText().toString());

		} catch (Exception e) {
			// TODO: handle exception
			mins = 0;
		}

		return mins;
	}

	public void setEmptyServiceInfo(ServiceViewHolder viewHolder) {
		viewHolder.routeNumber.setText("");
		viewHolder.tramDestination.setText("");
		viewHolder.tramType.setVisibility(View.INVISIBLE);
		viewHolder.hasAccess.setVisibility(View.GONE);
		viewHolder.hasAirCon.setVisibility(View.GONE);
		viewHolder.isServiceChanged.setVisibility(View.GONE);
		viewHolder.isDisrupted.setVisibility(View.GONE);
		viewHolder.tramMins.setText("");
		viewHolder.tramDay.setVisibility(View.GONE);
		viewHolder.tramAM.setVisibility(View.GONE);
		viewHolder.onboardChevron.setVisibility(View.INVISIBLE);
		viewHolder.onboardButton.setOnClickListener(null);
	}

	private String getDescription(Tram service) {
		String description = "";

		description = service.getHeadboardNo();
		description = description.concat(context
				.getString(R.string.route_filter_space));
		description = description.concat(service.getDestination());
		description = description.concat(context
				.getString(R.string.route_filter_space));
		description = description.concat(context
				.getString(R.string.accessibility_click_pid_onboard));

		return description;
	}

	public String getArrivalTimeString(Date date) {
		/*
		 * Adil added
		 */
		DateFormat timeFormat;
		if (android.text.format.DateFormat.is24HourFormat(context)) {
			timeFormat = new SimpleDateFormat(context.getResources().getString(
					R.string.route_time_format_24H));
		} else {
			timeFormat = new SimpleDateFormat(context.getResources().getString(
					R.string.route_time_format));
		}

		/*
		 * End
		 */
		String timeString = timeFormat.format(date);
		return timeString;
	}

	public String getArrivalDayString(Date date) {
		DateFormat timeFormat = new SimpleDateFormat(context.getResources()
				.getString(R.string.route_day_format));
		String timeString = timeFormat.format(date);
		return timeString;
	}
	public String getArrivalAMString(Date date) {
		DateFormat timeFormat = new SimpleDateFormat(context.getResources()
				.getString(R.string.route_am_format));
		String timeString = timeFormat.format(date);
		return timeString;
	}

	private boolean isValidReminder(int mins) {
		// TODO Auto-generated method stub

		System.out.println("-- is valid :   mins: " + mins
				+ "  > selectedInterval:" + selectedInterval + " : "
				+ (mins > selectedInterval));
		if (mins > selectedInterval) {
			return true;
		} else {

			return false;
		}
	}

	private String getTramArrivalTime(final Tram service) {
		String arrivalTime = "";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("h:mma");
		return arrivalTime = simpleDateFormat.format(service.getArrivalTime());
	}

	private String getTramArrivalTime(int min, Reminders reminder) {

		Calendar calendar = Calendar.getInstance();

		calendar.setTimeInMillis(System.currentTimeMillis());

		calendar.add(Calendar.MINUTE, min);

		Date date = new Date(calendar.getTimeInMillis());
		Console.print("time in pid alarm: " + calendar.getTimeInMillis());
		String arrivalTime = "";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("h:mma");
		return arrivalTime = simpleDateFormat.format(date);
	}

	private void showReminderSetToast(int min, Reminders reminder) {
//		Toast.makeText(
//				TramTrackerMainActivity.instance,
//				"You will be notified when " + selectedInterval
//						+ " minutes from Stop " + reminder.getRoute() + ": " + reminder.getStopName()
//						+ " (scheduled to arrive at "
//						+ getTramArrivalTime(min, reminder) + ").", 1).show();
	
		
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(reminder.getReminderTime());
		Date date = new Date(cal.getTimeInMillis());
		String time = "";
	
		DateFormat format = null;

		if (android.text.format.DateFormat.is24HourFormat(context)) {
			format = new SimpleDateFormat(context.getResources().getString(
					R.string.onboard_time_format));
		} else {
			format = new SimpleDateFormat(context.getResources().getString(
					R.string.onboard_time_format_H));

		}
		time = format.format(date);
		
		
		
		String msg = "You will be alerted at "
				+ time
				+ ". Please tap on the notification to get updated real-time information.";

		Toast.makeText(TramTrackerMainActivity.instance, msg, 1).show();
	}

	public void updateStop(Stop stop) {

		this.stop = stop;
	}
	
	private void scaleImage(ImageView view, int boundBoxInDp)
	{
	    // Get the ImageView and its bitmap
	    Drawable drawing = view.getDrawable();
	    Bitmap bitmap = ((BitmapDrawable)drawing).getBitmap();
	    view.setScaleType(ScaleType.FIT_XY);
	    // Get current dimensions
	    int width = bitmap.getWidth();
	    int height = bitmap.getHeight();

	    // Determine how much to scale: the dimension requiring less scaling is
	    // closer to the its side. This way the image always stays inside your
	    // bounding box AND either x/y axis touches it.
//	    float xScale = ((float) boundBoxInDp) / width;
//	    float yScale = ((float) boundBoxInDp) / height;
//	    float scale = (xScale <= yScale) ? xScale : yScale;
//
//	    // Create a matrix for the scaling and add the scaling data
//	    Matrix matrix = new Matrix();
//	    matrix.postScale(scale, scale);

	    // Create a new bitmap and convert it to a format understood by the ImageView
	    Bitmap scaledBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, null, true);
	    BitmapDrawable result = new BitmapDrawable(scaledBitmap);
	    width = scaledBitmap.getWidth();
	    height = scaledBitmap.getHeight();

	    // Apply the scaled bitmap
	    view.setImageDrawable(result);

	    // Now change ImageView's dimensions to match the scaled image
	    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
	    params.width = (int)(width * 1.5);
	    params.height = (int)(height * 2.2);
	    view.setLayoutParams(params);
	}

	private int dpToPx(int dp)
	{
	    float density = context.getResources().getDisplayMetrics().density;
	    return Math.round((float)dp * density);
	}

}
