package com.yarratrams.tramtracker.ui.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class TutorialPreferences {
	
	
	Context context;
	SharedPreferences sharedPreferences;
	SharedPreferences.Editor editor;
	
	final String MANUAL_SET = "MANUAL_SET";
	final String PREF_NAME = "tutorialpref";
	final String PID_TUTORIAL =	"pid";
	final String MY_TRAM_TUTORIAL =	"mytram";
	final String SETTINGS_TUTORIAL =	"settings";
	final String SCHEDULE_DEP_TUTORIAL =	"scheduledep";
	final String DISRUPTIONS_TUTORIAL =	"disruptions";
	final String ALARMS_TUTORIAL = "alarms";
	
	public TutorialPreferences(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
		sharedPreferences  = context.getSharedPreferences(PREF_NAME, Activity.MODE_PRIVATE);
		editor= sharedPreferences.edit();
	}
	
	public void setManualSet(boolean flag)
	{
		if(getManaualSet())
			return;
		editor.putBoolean(MANUAL_SET, flag);
		editor.commit();
	}
	
	public boolean getManaualSet()
	{
		return sharedPreferences.getBoolean(MANUAL_SET, false);
	}
	
	public void setPidTutorial(boolean flag)
	{
		editor.putBoolean(PID_TUTORIAL, flag);
		editor.commit();
	}


	public boolean getPidTutorial()
	{
		return sharedPreferences.getBoolean(PID_TUTORIAL, false);

	}
	
	
	
	public void setMyTramTutorial(boolean flag)
	{
		editor.putBoolean(MY_TRAM_TUTORIAL, flag);
		editor.commit();
	}


	public boolean getMyTramTutorial()
	{
		return sharedPreferences.getBoolean(MY_TRAM_TUTORIAL, false);

	}
	
	
	
	
	public void setSettingsTutorial(boolean flag)
	{
		editor.putBoolean(SETTINGS_TUTORIAL, flag);
		editor.commit();
	}


	public boolean getSettingsTutorial()
	{
		return sharedPreferences.getBoolean(SETTINGS_TUTORIAL, false);

	}
	
	
	
	public void setScheduleDepTutorial(boolean flag)
	{
		editor.putBoolean(SCHEDULE_DEP_TUTORIAL, flag);
		editor.commit();
	}


	public boolean getScheduleDepTutorial()
	{
		return sharedPreferences.getBoolean(SCHEDULE_DEP_TUTORIAL, false);

	}
	
	
	
	public void setDisruptionTutorial(boolean flag)
	{
		editor.putBoolean(DISRUPTIONS_TUTORIAL, flag);
		editor.commit();
	}


	public boolean getDisruptionTutorial()
	{
		return sharedPreferences.getBoolean(DISRUPTIONS_TUTORIAL, false);

	}
	
	public void setAlarmTutorial(boolean flag)
	{
		editor.putBoolean(ALARMS_TUTORIAL, flag);
		editor.commit();
	}


	public boolean getAlarmTutorial()
	{
		return sharedPreferences.getBoolean(ALARMS_TUTORIAL, false);

	}
	
	public void resetAll()
	{
		setMyTramTutorial(false);
		setPidTutorial(false);
		setScheduleDepTutorial(false);
		setSettingsTutorial(false);
		setDisruptionTutorial(true);
		setAlarmTutorial(true);
	}

	public void disableAll()
	{
		setMyTramTutorial(true);
		setPidTutorial(true);
		setScheduleDepTutorial(true);
		setSettingsTutorial(true);
		setDisruptionTutorial(true);
		setAlarmTutorial(true);
	}
	
	public boolean showTooltipCbStatus(){
		return !(getMyTramTutorial() && getPidTutorial() && getScheduleDepTutorial() && getSettingsTutorial() && getDisruptionTutorial() && getAlarmTutorial());
	}
}
