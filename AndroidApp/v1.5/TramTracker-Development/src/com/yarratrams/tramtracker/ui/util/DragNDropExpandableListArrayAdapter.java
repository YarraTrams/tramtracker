package com.yarratrams.tramtracker.ui.util;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.db.FavouriteManager;
import com.yarratrams.tramtracker.objects.Favourite;
import com.yarratrams.tramtracker.objects.FavouritesGroup;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.ui.AddFavouriteActivity;
import com.yarratrams.tramtracker.ui.ManageFavouritesActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;


public class DragNDropExpandableListArrayAdapter extends BaseExpandableListAdapter 
		implements DragNDropExpandableListView.RemoveListener, DragNDropExpandableListView.DropListener{
	private ManageFavouritesActivity context;
	private ArrayList<FavouritesGroup> groups;
	private DragNDropExpandableListView list;
	private boolean isFilterOn;
	
	private FavouriteManager favouriteManager;
	
	public DragNDropExpandableListArrayAdapter(DragNDropExpandableListView list, ManageFavouritesActivity context, ArrayList<FavouritesGroup> groups) {
		super();
		this.context = context;
		this.groups = groups;
		this.list = list;
		
		this.list.setDragListener(dragListener);
		this.list.setDropListener(this);
		this.list.setRemoveListener(this);
		
		favouriteManager = new FavouriteManager(this.context);
	}
    
    private DragNDropExpandableListView.DragListener dragListener =
    	new DragNDropExpandableListView.DragListener() {
    	
		public void onDrag(int x, int y, DragNDropExpandableListView listView) {

		}

		public void onStartDrag(View itemView) {
			itemView.setVisibility(View.INVISIBLE); 
			//itemView.setBackgroundColor(context.getResources().getColor(R.color.color_transparent_light));
			itemView.setBackgroundColor(context.getResources().getColor(R.color.color_gray_selected));
			ImageView dragIcon = (ImageView)itemView.findViewById(R.id.favourite_move);
			if (dragIcon != null) dragIcon.setVisibility(View.INVISIBLE);
		}

		public void onStopDrag(View itemView) {
			itemView.setVisibility(View.VISIBLE);
			itemView.setBackgroundColor(context.getResources().getColor(R.color.color_transparent));
			ImageView dragIcon = (ImageView)itemView.findViewById(R.id.favourite_move);
			if (dragIcon != null) dragIcon.setVisibility(View.VISIBLE);
		}
    };
	
	

	public void updateList(ArrayList<FavouritesGroup> groups) {
		this.groups.clear();
		this.groups.addAll(groups);
		super.notifyDataSetChanged();
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return getFavourite(groupPosition, childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return (groupPosition * 100) + childPosition;
	}

	
	static class FavouriteViewHolder{
		TextView favouriteName;
		TextView stopName;
		TextView route;
		ImageView lowFloor;
		Button delete;
		Button edit;
	}
	
	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
		FavouriteViewHolder viewHolder;
		
		if(convertView == null){
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(R.layout.favourites_manage_list_view_child, parent, false);
			
			viewHolder = new FavouriteViewHolder();
			viewHolder.favouriteName = (TextView)convertView.findViewById(R.id.favourite_name);
			viewHolder.stopName = (TextView)convertView.findViewById(R.id.stop_name);
			viewHolder.route = (TextView)convertView.findViewById(R.id.stop_routes);
			viewHolder.lowFloor = (ImageView)convertView.findViewById(R.id.favourite_lowfloor);
			viewHolder.delete = (Button)convertView.findViewById(R.id.favourite_delete);
			viewHolder.edit = (Button)convertView.findViewById(R.id.favourite_edit);
			convertView.setTag(viewHolder);
			
		} else {
			viewHolder = (FavouriteViewHolder) convertView.getTag();
		}
		
		final Favourite favourite = (Favourite) getChild(groupPosition, childPosition);
		final FavouritesGroup group = (FavouritesGroup) getGroup(groupPosition);
		
		viewHolder.favouriteName.setText(favourite.getName());
		viewHolder.stopName.setText(getStopDescription(favourite.getStop()));
		viewHolder.route.setText(getRoutesDescription(favourite));
		
		if(isLowFloor(favourite)){
			viewHolder.lowFloor.setVisibility(View.VISIBLE);
		} else {
			viewHolder.lowFloor.setVisibility(View.INVISIBLE);
		}
		
		viewHolder.delete.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				validateDelete(favourite, group);
			}
		});
		viewHolder.edit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				TramTrackerMainActivity.getAppManager().callSelection(editDescription(getStopDescription(favourite.getStop())));
				editFavourite(favourite, group);
			}
		});
		
		return convertView;
	}

	private String editDescription(String stop){
		String description = "";
		
		description = context.getString(R.string.accessibility_click_edit);
		description = description.concat(stop);
		
		return description; 
	}
	
	@Override
	public int getChildrenCount(int groupPosition) {
		FavouritesGroup group = getFavouriteGroup(groupPosition);
		return group.getFavourites().size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return getFavouriteGroup(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return groups.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	static class SuburbViewHolder{
		TextView suburbName;
	}
	
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		SuburbViewHolder viewHolder;
		
		if(convertView == null){
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(R.layout.favourites_list_view_group, parent, false);
			
			viewHolder = new SuburbViewHolder();
			viewHolder.suburbName = (TextView) convertView.findViewById(R.id.group_name);
			convertView.setTag(viewHolder);
			
		} else {
			viewHolder = (SuburbViewHolder) convertView.getTag();
		}
		
		FavouritesGroup group = (FavouritesGroup) getGroup(groupPosition);
		viewHolder.suburbName.setText(group.getName());
		
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

    @Override
    public boolean areAllItemsEnabled(){
        return true;
    }
	
    
	public void validateDelete(final Favourite favourite, final FavouritesGroup group){
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
		    @Override
		    public void onClick(DialogInterface dialog, int which) {
		        switch (which){
		        case DialogInterface.BUTTON_POSITIVE:
		        	deleteFavourite(favourite, group);
		            break;

		        case DialogInterface.BUTTON_NEGATIVE:
		            break;
		        }
		    }
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(context.getDialogContext());
		builder.setMessage(context.getResources().getString(R.string.manage_favourites_delete_message))
			.setPositiveButton(context.getResources().getString(R.string.manage_favourites_delete_yes), dialogClickListener)
		    .setNegativeButton(context.getResources().getString(R.string.manage_favourites_delete_cancel), dialogClickListener)
		    .setTitle(favourite.getName())
		    .show();
	}

    private void deleteFavourite(Favourite favourite, FavouritesGroup group){
    	group.getFavourites().remove(favourite);
    	reassignGroupOrder(group);
    	favouriteManager.removeFavourite(group, favourite);  
    	context.updateUI();
    }
    
    private void editFavourite(Favourite favourite, FavouritesGroup group){
		Intent intent = new Intent(context, AddFavouriteActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(AddFavouriteActivity.INTENT_KEY_EDIT_FAVOURITE, favourite);
		intent.putExtra(AddFavouriteActivity.INTENT_KEY_EDIT_GROUP, group);
		TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_FAVOURITES, context.getResources().getString(R.string.tag_addfavourite_screen), intent);
    }
    
	private void reassignGroupOrder(FavouritesGroup group){
		ArrayList<Favourite> list = group.getFavourites(); 
		for(int i = 0; i < list.size(); i++){
			Favourite favourite = list.get(i);
			favourite.setOrder(i);
			favouriteManager.editFavourite(group, favourite);
		}
	}
    
	private FavouritesGroup getFavouriteGroup(int groupPosition){
		return groups.get(groupPosition);
	}
	
	private Favourite getFavourite(int groupPosition, int childPosition){
		FavouritesGroup group = getFavouriteGroup(groupPosition);
		return group.getFavourites().get(childPosition);
	}
	
	
	private String getStopDescription(Stop stop){
		String text = "";
		
		text = text.concat(String.valueOf(stop.getStopNumber()));
		//phoenix changed 20.Jun.2014
		text = text.concat(": ");
//		text = text.concat(context.getResources().getString(R.string.stop_name_colon));
		text = text.concat(stop.getStopName());
		
		return text;
	}
	
	
	private String getRoutesDescription(Favourite favourite){
		String text = "";
		String[] array = new String[0];
		
		if(favourite.getAlRoutes() == null){
			array = favourite.getStop().getRoutes();
		} else {
			array = getRoutesList(favourite);
		}
		
		if(isFilterOn){
			text = context.getResources().getString(R.string.stop_direction_showing);
		} else {
			text = context.getResources().getString(R.string.stop_direction_routes);
		}
		for(int i = 0; i < array.length; i++){
			text = text.concat(array[i]);
			if(i < array.length-1){
				text = text.concat(context.getResources().getString(R.string.stop_routes_coma));
			}
		}
		text = text.concat(context.getResources().getString(R.string.stop_name_space));
		text = text.concat(favourite.getStop().getCityDirection());
		
		return text;
	}
	
	private String[] getRoutesList(Favourite favourite){
		ArrayList<String> list = new ArrayList<String>();
		
		for(String option: favourite.getAlRoutes()){
			if(option.equalsIgnoreCase(context.getResources().getString(R.string.route_filter_dbtag_all))){
				isFilterOn = false;
				return favourite.getStop().getRoutes();
			} else if(!option.equalsIgnoreCase(context.getResources().getString(R.string.route_filter_dbtag_lowfloor))) {
				isFilterOn = true;
				list.add(option);
			}
		}
		
		return list.toArray(new String[0]);
	}
	
	private boolean isLowFloor(Favourite favourite){
		for(String option: favourite.getAlRoutes()){
			if(option.equalsIgnoreCase(context.getResources().getString(R.string.route_filter_dbtag_lowfloor))){
				return true;
			}
		}
		return false;
	}

	
	public int getChildrenCount(){
		int children = 0; 
		for(int i = 0; i < groups.size(); i++){
			children = children + groups.get(i).getFavourites().size();
		}
		return children;
	}
	
	
	@Override
	public void onDrop(int[] from, int[] to) {
		if(to[0] >= getGroupCount() || to[0] < 0 || to[1] < 0){
			return;
		}
		
		// Remove favourite from old location and set to new location
		Favourite favourite = groups.get(from[0]).getFavourites().remove(from[1]);
		groups.get(to[0]).getFavourites().add(to[1], favourite);
		notifyDataSetChanged();
		list.invalidateViews();
		
		if(from[0] == to[0]){
			FavouritesGroup group = groups.get(from[0]);
			int first = from[1] < to[1]? from[1]:to[1];
			saveGroup(group, first);
			
		} else {
			FavouritesGroup group = groups.get(from[0]);
			int first = from[1];
			saveGroup(group, first);
			
			group = groups.get(to[0]);
			first = to[1];
			saveGroup(group, first);
		}
	}
	
	private void saveGroup(FavouritesGroup group, int first){
		int last = group.getFavourites().size();
		if(last > 0){
			for(; first < last; first++){
				Favourite favourite = group.getFavourites().get(first);
				favourite.setOrder(first);
				favouriteManager.editFavourite(group, favourite);
			}
		}
	}

	@Override
	public void onRemove(int[] which) {
		if (which[0] < 0 || which[0] > groups.size() || which[1] < 0 || which[1] > getChildrenCount(which[0])) {
			return;		
		}
		FavouritesGroup group = groups.get(which[0]);
		group.getFavourites().remove(which[1]);
		
		list.invalidateViews();
	}

}
