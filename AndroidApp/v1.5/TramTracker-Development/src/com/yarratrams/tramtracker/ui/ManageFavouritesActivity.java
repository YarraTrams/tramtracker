package com.yarratrams.tramtracker.ui;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.db.FavouriteManager;
import com.yarratrams.tramtracker.objects.FavouritesGroup;
import com.yarratrams.tramtracker.ui.util.DragNDropExpandableListArrayAdapter;
import com.yarratrams.tramtracker.ui.util.DragNDropExpandableListView;
import com.yarratrams.tramtracker.ui.util.FeaturesPreferences;
import com.yarratrams.tramtracker.ui.util.OtherLevelAds;


public class ManageFavouritesActivity extends Activity implements OnClickListener{
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_FAVOURITES;
	public static final String INTENT_KEY = "favourites_list";
	
	private DragNDropExpandableListView favouritesList;
	private DragNDropExpandableListArrayAdapter favouritesAdapter;
	private ArrayList<FavouritesGroup> groups;
	private RelativeLayout noFavouritesMessage;
	
	private ProgressDialog loadDialog;
	
	private FavouriteManager favouriteManager;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.favourites_manage_screen);

		favouriteManager = new FavouriteManager(getApplicationContext());
		
		groups = new ArrayList<FavouritesGroup>();
		favouritesList = (DragNDropExpandableListView) findViewById(R.id.expandable_list);
		
		Display display = getWindowManager().getDefaultDisplay();
		Drawable groupIndicator = getResources().getDrawable(R.drawable.icn_list_expandable);
		favouritesList.setGroupIndicator(groupIndicator);
		favouritesList.setChildIndicator(null);
		
		if(android.os.Build.VERSION.SDK_INT < 18)
			favouritesList.setIndicatorBounds(display.getWidth()-GetDipsFromPixel(30), display.getWidth());
		else
			favouritesList.setIndicatorBoundsRelative(display.getWidth()-GetDipsFromPixel(30), display.getWidth());
		
		favouritesAdapter = new DragNDropExpandableListArrayAdapter(favouritesList, this, groups);
		favouritesList.setAdapter(favouritesAdapter);
		favouritesList.setContext(getDialogContext());

		noFavouritesMessage = (RelativeLayout)findViewById(R.id.nofavourite_label);
        
		Button manageGroups = (Button) findViewById(R.id.groups_button);
		manageGroups.setOnClickListener(this);
		
        loadDialog = new ProgressDialog(this);
        
        //cals ads
        OtherLevelAds.showAds(this, R.id.rich_banner_fragment1022,FeaturesPreferences.getAdsFlag(TramTrackerMainActivity.instance));
	}
	
	public Context getDialogContext() {
		Context context;
		if (getParent() != null) {
			context = getParent();
		} else {
			context = this;
		}
		return context;
	}
	
	public int GetDipsFromPixel(float pixels) {
		final float scale = getResources().getDisplayMetrics().density;
		return (int) (pixels * scale + 0.5f);
	}
	
	public void updateUI() {
		if(!loadDialog.isShowing()){
			loadDialog = ProgressDialog.show(getDialogContext(), "", getResources().getString(R.string.dialog_loading), true, true);
		}
		
		groups = favouriteManager.getAllFavourites();

		if(!isFavouriteListEmpty()){
			noFavouritesMessage.setVisibility(View.GONE);
			favouritesList.setVisibility(View.VISIBLE);
			
			// Setup list
			favouritesAdapter = new DragNDropExpandableListArrayAdapter(favouritesList, this, groups);
			favouritesList.setAdapter(favouritesAdapter);
			expandAll();
	        
		} else {
			noFavouritesMessage.setVisibility(View.VISIBLE);
			favouritesList.setVisibility(View.INVISIBLE);
			TramTrackerMainActivity.getAppManager().back();
		}
		
		
		if(loadDialog.isShowing()){
			loadDialog.dismiss();
		}
	}

	private boolean isFavouriteListEmpty(){
		if(groups != null){
			for(FavouritesGroup group : groups){
				if(group.getFavourites() != null && !group.getFavourites().isEmpty()){
					return false;
				}
			}
		}
		return true;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		updateUI();
	}
	@Override
	public void onPause() {
		super.onPause();
	}
	
	private void expandAll(){
		for(int i = 0; i < groups.size(); i ++){
			favouritesList.expandGroup(i);
		}
	}
	
	
	@Override
	public void onClick(View v) {
		Button manageGroups = (Button) findViewById(R.id.groups_button);
		if (v == manageGroups) {
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_header_groups));
			Intent intent = new Intent(getDialogContext(), ManageFavouriteGroupsActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.putExtra(ManageFavouriteGroupsActivity.INTENT_KEY, groups);
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_FAVOURITES, 
					getResources().getString(R.string.tag_managefavouritegroups_screen), intent);
		}
	}
	
	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}
	
}
