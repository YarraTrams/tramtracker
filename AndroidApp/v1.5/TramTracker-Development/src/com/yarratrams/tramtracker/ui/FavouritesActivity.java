package com.yarratrams.tramtracker.ui;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.ActivityGroup;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.ViewFlipper;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.google.android.maps.Overlay;
import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.db.FavouriteManager;
import com.yarratrams.tramtracker.objects.Favourite;
import com.yarratrams.tramtracker.objects.FavouritesGroup;
import com.yarratrams.tramtracker.ui.util.FavouritesExpandableListArrayAdapter;
import com.yarratrams.tramtracker.ui.util.FeaturesPreferences;
import com.yarratrams.tramtracker.ui.util.MapFavouriteOverlay;
import com.yarratrams.tramtracker.ui.util.MenuFavouritesAdapter;
import com.yarratrams.tramtracker.ui.util.OtherLevelAds;
import com.yarratrams.tramtracker.ui.util.WrappingSlidingDrawer;


public class FavouritesActivity extends MapActivity implements OnClickListener {
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_FAVOURITES;
	public static final int VIEW_LIST = 0;
	public static final int VIEW_MAP = 1;
	
	private FavouriteManager favouriteManager;
	
	private boolean isListViewSelected;
	private ViewFlipper flipper;
	
	private ExpandableListView favouritesList;
	private FavouritesExpandableListArrayAdapter favouritesAdapter;
	private ArrayList<FavouritesGroup> groups;
	private RelativeLayout noFavouritesMessage;
	
	private MapView favouritesMap;
	private List<Overlay> mapOverlays;
	private MyLocationOverlay myOverlay;
	private GeoPoint mapCentre;
	private int zoomLevel;
	
	private ProgressDialog loadDialog;

	private WrappingSlidingDrawer slidingMenu;
	
	
	
	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.favourites_screen);
		
		favouriteManager = new FavouriteManager(getApplicationContext());
		
		isListViewSelected = true;
		flipper = (ViewFlipper) findViewById(R.id.view_flipper);
		
		ToggleButton listButton = (ToggleButton) findViewById(R.id.list_view_button);
		listButton.setOnClickListener(this);
		ToggleButton mapButton = (ToggleButton) findViewById(R.id.map_view_button);
		mapButton.setOnClickListener(this);
		Button refreshButton = (Button) findViewById(R.id.refresh_button);
		refreshButton.setOnClickListener(this);
		refreshButton.setVisibility(View.INVISIBLE);
		
		groups = new ArrayList<FavouritesGroup>();
		favouritesList = (ExpandableListView) findViewById(R.id.expandable_list);
		
		Display display = getWindowManager().getDefaultDisplay();
		Drawable groupIndicator = getResources().getDrawable(R.drawable.icn_list_expandable);
		favouritesList.setGroupIndicator(groupIndicator);
		favouritesList.setChildIndicator(null);
		
		if(android.os.Build.VERSION.SDK_INT < 18)
			favouritesList.setIndicatorBounds(display.getWidth()-GetDipsFromPixel(30), display.getWidth());
		else
			favouritesList.setIndicatorBoundsRelative(display.getWidth()-GetDipsFromPixel(30), display.getWidth());

		
		favouritesAdapter = new FavouritesExpandableListArrayAdapter(this, groups);
		favouritesList.setAdapter(favouritesAdapter);

		noFavouritesMessage = (RelativeLayout)findViewById(R.id.nofavourite_label);
		
		favouritesMap = (MapView)findViewById(R.id.map);
		favouritesMap.setBuiltInZoomControls(true);
		myOverlay = new MyLocationOverlay(this, favouritesMap);
        mapOverlays = favouritesMap.getOverlays();
		
        slidingMenu = (WrappingSlidingDrawer) findViewById(R.id.sliding_menu);
        GridView menuContainer = (GridView) findViewById(R.id.sliding_menu_container);
        MenuFavouritesAdapter menuAdapter = new MenuFavouritesAdapter(this);
        menuContainer.setAdapter(menuAdapter);
        
        
        loadDialog = new ProgressDialog(this);
        
        
        //call ads
		OtherLevelAds.showAds(this, R.id.rich_banner_fragment1055,FeaturesPreferences.getAdsFlag(TramTrackerMainActivity.instance));
	}
	
	public Context getDialogContext() {
		Context context;
		if (getParent() != null) {
			context = getParent();
		} else {
			context = this;
		}
		return context;
	}
	
	public int GetDipsFromPixel(float pixels) {
		final float scale = getResources().getDisplayMetrics().density;
		return (int) (pixels * scale + 0.5f);
	}
	
	private void updateUI() {
		if(!loadDialog.isShowing()){
			loadDialog = ProgressDialog.show(getDialogContext(), "", getResources().getString(R.string.dialog_loading), true, true);
		}
		
		groups = favouriteManager.getAllFavourites();
		
		// Setup map
		mapOverlays.clear();
		mapOverlays.add(myOverlay);

		if(!isFavouriteListEmpty()){
			noFavouritesMessage.setVisibility(View.GONE);
			favouritesList.setVisibility(View.VISIBLE);
			
			// Setup list
			favouritesAdapter = new FavouritesExpandableListArrayAdapter(this, groups);
			favouritesList.setAdapter(favouritesAdapter);
			expandAll();
			
			MapFavouriteOverlay overlay = new MapFavouriteOverlay(this, favouritesMap);
			overlay.updateFavouritesStopsList(groups);
	        mapOverlays.add(overlay);
	        
		} else {
			noFavouritesMessage.setVisibility(View.VISIBLE);
			favouritesList.setVisibility(View.INVISIBLE);
		}
		
		if(mapCentre != null){
			MapController mapController = favouritesMap.getController();
			mapController.setCenter(mapCentre);
			mapController.setZoom(zoomLevel);
		} else {
			centerMapOnStops();
		}
		
		favouritesMap.invalidate();
		
		if(loadDialog.isShowing()){
			loadDialog.dismiss();
		}
	}
	
	private boolean isFavouriteListEmpty(){
		if(groups != null){
			for(FavouritesGroup group : groups){
				if(group.getFavourites() != null && !group.getFavourites().isEmpty()){
					return false;
				}
			}
		}
		return true;
	}
	
//	public void centerxMapOn(GeoPoint center, int max, boolean islat){
//		MapController mapController = favouritesMap.getController();
//		
//		if(islat){
//			if(max < 20000){
//				zoomLevel = 16; // Zoom 16 few blocks
//			} else if(max < 30000){
//				zoomLevel = 15; 
//			} else if(max < 50000){
//				zoomLevel = 14; 
//			} else if(max < 70000){
//				zoomLevel = 13; 
//			} else if(max < 131000){
//				zoomLevel = 12; 
//			} else if(max < 150000){
//				zoomLevel = 11; 
//			} else {
//				zoomLevel = 10; // Zoom 10 is city view
//			}
//		} else {
//			if(max < 25000){
//				zoomLevel = 16; // Zoom 16 few blocks
//			} else if(max < 30000){
//				zoomLevel= 15; 
//			} else if(max < 80000){
//				zoomLevel = 14; 
//			} else if(max < 100000){
//				zoomLevel = 13; 
//			} else if(max < 190000){
//				zoomLevel = 12; 
//			} else if(max < 250000){
//				zoomLevel = 11; 
//			} else {
//				zoomLevel = 10; // Zoom 10 is city view
//			}
//		}
//		
//		mapController.setZoom(zoomLevel);
//		if(center != null){
//			mapCentre = center;
//			mapController.animateTo(mapCentre);
//		}
//	}
	
	public void centerMapOnStops(){
		int maxLat = 0;
		int minLat = 0;
		int maxLon = 0;
		int minLon = 0;
		
		for(FavouritesGroup group: groups){
			for(Favourite favourite: group.getFavourites()){
				if(maxLat == 0){
					maxLat = favourite.getStop().getLatitudeE6();
					minLat = favourite.getStop().getLatitudeE6();
					maxLon = favourite.getStop().getLongitudeE6();
					minLon = favourite.getStop().getLongitudeE6();
				}
				maxLat = favourite.getStop().getLatitudeE6() > maxLat? favourite.getStop().getLatitudeE6(): maxLat;
				minLat = favourite.getStop().getLatitudeE6() < minLat? favourite.getStop().getLatitudeE6(): minLat;
				maxLon = favourite.getStop().getLongitudeE6() > maxLon? favourite.getStop().getLongitudeE6(): maxLon;
				minLon = favourite.getStop().getLongitudeE6() < minLon? favourite.getStop().getLongitudeE6(): minLon;
			}
		}
		if(maxLat == 0){
			// The map is zoomed and centered at Flinders St Station
			maxLat = (int) (-37.817491 * 1E6);
			minLat = (int) (-37.817491 * 1E6);
			maxLon = (int) (144.967445 * 1E6);
			minLon = (int) (144.967445 * 1E6);
		}
		
		int dLat = maxLat - minLat;
		int dLon = maxLon - minLon;
		
		int cLat = minLat + (dLat/2);
		int cLon = minLon + (dLon/2);
		
//		int max = 0;
//		boolean isLat = false;
//		if(dLat == 0 && dLon == 0){
//			max = 150000;
//		} else {
//			if(dLat > dLon){
//				max = dLat;
//				isLat = true;
//			} else{
//				max = dLon;
//				isLat = false;
//			}
//		}
		
		GeoPoint centre = new GeoPoint(cLat, cLon);
		
		//centerMapOn(centre, max, isLat);
		MapController mapController = favouritesMap.getController();
		mapController.zoomToSpan(dLat, dLon);
		if(centre != null){
			mapCentre = centre;
			mapController.animateTo(mapCentre);
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		if(!isListViewSelected){
			myOverlay.enableMyLocation();
		}
		
		updateUI();
	}
	
	@Override
	public void onPause() {
		super.onPause();
		
		if(!isListViewSelected){
			myOverlay.disableMyLocation();
		}
		zoomLevel = favouritesMap.getZoomLevel();
		mapCentre = favouritesMap.getMapCenter();
	}
	
	
	private void switchViews(){
		ToggleButton listButton = (ToggleButton) findViewById(R.id.list_view_button);
		ToggleButton mapButton = (ToggleButton) findViewById(R.id.map_view_button);
		Button refreshButton = (Button) findViewById(R.id.refresh_button);
		if (isListViewSelected) {
			isListViewSelected = false;
			listButton.setChecked(false);
			mapButton.setChecked(true);
			refreshButton.setVisibility(View.VISIBLE);
	        flipper.setDisplayedChild(VIEW_MAP);
	        myOverlay.enableMyLocation();
	        TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_header_mapview));
		} else {
			isListViewSelected = true;
			listButton.setChecked(true);
			mapButton.setChecked(false);
			refreshButton.setVisibility(View.INVISIBLE);
	        flipper.setDisplayedChild(VIEW_LIST);
	        myOverlay.disableMyLocation();
	        TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_header_listview));
		}
	}
	
	private void expandAll(){
		for(int i = 0; i < groups.size(); i ++){
			favouritesList.expandGroup(i);
		}
	}
	
	private void refresh(){
		centerMapOnStops();
	}
	
	@Override
	public void onClick(View v) {
		ToggleButton listButton = (ToggleButton) findViewById(R.id.list_view_button);
		ToggleButton mapButton = (ToggleButton) findViewById(R.id.map_view_button);
		Button refreshButton = (Button) findViewById(R.id.refresh_button);
		if (v == listButton || v == mapButton) {
			switchViews();
			refresh();
		} else if(v == refreshButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_header_refresh));
			refresh();
		}
	}
	
	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}
	
	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
//		MenuInflater inflater = getMenuInflater();
//		inflater.inflate(R.menu.favourites_menu, menu);
//		TramTrackerMainActivity.getAppManager().setMenuBackground(this);
		return true;
	}
	
	@Override
	public void openOptionsMenu() {
		slidingMenu.toggle();
	}
	@Override
	public void closeOptionsMenu() {
		slidingMenu.close();
	}
	
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_MENU){
			openOptionsMenu();
			return true;
		} else if(keyCode == KeyEvent.KEYCODE_BACK){
			onBackPressed();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent;
		switch (item.getItemId()) {
			case R.id.menu_favourites_managefavourites :
				
					intent = new Intent(getDialogContext(), ManageFavouritesActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.putExtra(ManageFavouritesActivity.INTENT_KEY, groups);

					TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_FAVOURITES, 
							getResources().getString(R.string.tag_managefavourites_screen), intent);

				return true;
				
			case R.id.menu_favourites_help :
				intent = new Intent(getDialogContext(), HelpActivity.class);
				TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_FAVOURITES, getResources().getString(R.string.tag_help_screen), intent);
				return true;
				
			case R.id.menu_favourites_search :
				intent = new Intent(getDialogContext(), SearchMainActivity.class);
				TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_FAVOURITES, getResources().getString(R.string.tag_search_main_screen), intent);
				return true;
			default :
				return super.onOptionsItemSelected(item);
		}
	}
	

	public void runManageFavouritesMenuOption(){
		closeOptionsMenu();
		if(!isFavouriteListEmpty()){
			Intent intent = new Intent(getDialogContext(), ManageFavouritesActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.putExtra(ManageFavouritesActivity.INTENT_KEY, groups);

			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_FAVOURITES, 
					getResources().getString(R.string.tag_managefavourites_screen), intent);
		}
		else{
			Toast.makeText(this, getResources().getString(R.string.favourites_no_favourite_manage_favourites_tapped), Toast.LENGTH_LONG).show();
		}
	}
	public void runTrackerIDMenuOption(){
		closeOptionsMenu();
		ActivityGroup group = TramTrackerMainActivity.getAppManager().requestDifferentTab(TramTrackerMainActivity.TAB_MORE);
		Intent intent = new Intent(group, SearchTrackerIDActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, getResources().getString(R.string.tag_trackerid_screen), intent);
	}
	public void runSearchMenuOption(){
		closeOptionsMenu();
		Intent intent = new Intent(getDialogContext(), SearchMainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_FAVOURITES, getResources().getString(R.string.tag_search_main_screen), intent);
	}

	
}
