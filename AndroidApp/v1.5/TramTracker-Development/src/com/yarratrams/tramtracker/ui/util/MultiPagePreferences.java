package com.yarratrams.tramtracker.ui.util;

import java.lang.reflect.Type;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.yarratrams.tramtracker.objects.MultiPageUrlModel;

public class MultiPagePreferences {



	Context context;
	SharedPreferences sharedPreferences;
	SharedPreferences.Editor editor;


	final String PREF_NAME = "multiurlpref";
	final String MULTI_URLS =	"urls";
	final String VIEWED_ID = "viewedid";
	final String IS_NEW_AVAILABLE = "new";
	final String IS_FIRST_TIME = "firstime";
	final String LAST_OPEN = "last_open";


	public MultiPagePreferences(Context context) {
		// TODO Auto-generated constructor stub

		this.context  = context;
		sharedPreferences  = context.getSharedPreferences(PREF_NAME, Activity.MODE_PRIVATE);
		editor= sharedPreferences.edit();
	}



	//
	//	public boolean getIsNewAvailable()
	//	{
	//		return sharedPreferences.getBoolean(IS_NEW_AVAILABLE, false);
	//	}
	//
	//
	//	public void setIsNewAvailable(boolean flag)
	//	{
	//		editor.putBoolean(IS_NEW_AVAILABLE, flag);
	//		editor.commit();
	//	}
	//
	//	
	//	
	//	public boolean getIsFirstTime()
	//	{
	//		return sharedPreferences.getBoolean(IS_FIRST_TIME, false);
	//	}
	//
	//
	//	public void setIsFirstTime(boolean flag)
	//	{
	//		editor.putBoolean(IS_FIRST_TIME, flag);
	//		editor.commit();
	//	}
	//	




	public void setLastOpenTime(long time)
	{
		editor.putLong(LAST_OPEN, time);
		editor.commit();
	}

	public long getLastOpenTime()
	{
		return sharedPreferences.getLong(LAST_OPEN, 0);
	}

	public void addViewedId(int id){
		ArrayList<Integer> ids = getViewedId();
		if(!ids.contains(id)){
			ids.add(id);
		}
		Gson gson = new Gson();
		String s = gson.toJson(ids);
		editor.putString(VIEWED_ID, s);
		editor.commit();
	}

	public ArrayList<Integer> getViewedId(){
		String s = sharedPreferences.getString(VIEWED_ID, null);
		if(s!= null){
			Gson gson = new Gson();
			Type t = new TypeToken<ArrayList<Integer>>() {}.getType();
			return gson.fromJson(s, t);
		}
		else{
			return new ArrayList<Integer>();
		}
	}

	public boolean hasNewUrl(){
		String s = getMultiUrls();
		if(s!= null){
			try {
				JSONArray ja = new JSONArray(s);
				return ja.length() != 0;

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return false;
	}



	public void setMultiUrls(String content)
	{
		try {
			JSONArray ja = new JSONArray(content);
			ArrayList<JSONObject> list = new ArrayList<JSONObject>();
			//change json array to arraylist
			for (int i = 0; i < ja.length(); i ++){
				list.add(ja.getJSONObject(i));
			}

			//remove duplicate id
			for (int i = 0; i < list.size() ; i ++){
				if(getViewedId().contains(list.get(i).getInt("id"))){
					list.remove(i);
					i--;
				}
			}

			//arraylist to string
			JSONArray jaNew = new JSONArray();
			for (int i = 0; i < list.size() ; i ++){

				//logic to add only both and android pages
				if(list.get(i).getInt("deviceType") == MultiPageUrlModel.ANDROID || list.get(i).getInt("deviceType") == MultiPageUrlModel.BOTH)
				{


					jaNew.put(list.get(i));
				}
			}

			//save rest to shared preference

			editor.putString(MULTI_URLS, jaNew.toString());
			editor.commit();

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}




		//		String currMultiUrls = getMultiUrls();
		//		
		//		
		//		
		//		if(currMultiUrls==null)
		//		{
		//			
		//			System.out.println("-- no pref exists adding from scratch");
		//			editor.putString(MULTI_URLS, content);
		//			editor.commit();
		//			setIsNewAvailable(true);
		//		}
		//		else
		//		{
		//			JsonParser parser = new JsonParser();
		//			JsonElement jeNew = parser.parse(content);
		//			JsonArray jaNew = jeNew.getAsJsonArray();
		//			
		//			
		//			JsonElement jeOld = parser.parse(currMultiUrls);
		//			JsonArray jaOld = jeOld.getAsJsonArray();
		//			
		//			
		//			
		//			for(int i = 0 ; i < jaNew.size(); i++)
		//			{
		//				if(isExistingUrl(jaNew.get(i).getAsJsonObject(), jaOld))					
		//				{
		//					
		//				}
		//				else
		//				{
		//					System.out.println("-- adding new urls");
		//					editor.putString(MULTI_URLS, content);
		//					editor.commit();
		//					setIsNewAvailable(false);
		//					break;
		//				}
		//			}
		//			
		//			
		//			
		//		}
		//			



	}



	private boolean isExistingUrl(JsonObject JsonObject, JsonArray jaOld) {
		// TODO Auto-generated method stub

		boolean isExisting = false;

		for(int i = 0 ; i < jaOld.size(); i++)
		{
			if(JsonObject.get("id").getAsInt() == jaOld.get(i).getAsJsonObject().get("id").getAsInt())
			{
				isExisting = true;
				break;
			}
		}



		return isExisting;
	}




	public String getMultiUrls()
	{
		return sharedPreferences.getString(MULTI_URLS, null);
	}


	public void print(){
		//		editor.remove(MULTI_URLS);
		//		editor.remove(VIEWED_ID);
		//		editor.commit();
		System.out.println("Display Url : " + getMultiUrls());
		System.out.println("Stored ids  : " + getViewedId());
		System.out.println("has new ids : " + hasNewUrl());
	}

	public void removeStoredUrl(){
		editor.remove(MULTI_URLS);
		editor.commit();
	}






	public ArrayList<MultiPageUrlModel> getMultiUrlList()
	{
		ArrayList<MultiPageUrlModel> alPageUrlModels;
		Gson gson = new Gson();

		Type listType = new TypeToken<ArrayList<MultiPageUrlModel>>() {}.getType();
		alPageUrlModels =  gson.fromJson(sharedPreferences.getString(MULTI_URLS, ""), listType);


		return alPageUrlModels;
	}








	/*
	public void setMultiUrls(String content)
	{
		ArrayList<MultiPageUrlModel> alPageUrlModels, alCurrPageUrlModels;
		Gson gson = new Gson();

		alCurrPageUrlModels = getMultiUrlList();

		if(alCurrPageUrlModels!=null)
		{
			Type listType = new TypeToken<ArrayList<MultiPageUrlModel>>() {}.getType();
			alPageUrlModels =  gson.fromJson(content, listType);




		}
		else
		{
			editor.putString(MULTI_URLS, content);
			editor.commit();

		}








	}




	 */

}
