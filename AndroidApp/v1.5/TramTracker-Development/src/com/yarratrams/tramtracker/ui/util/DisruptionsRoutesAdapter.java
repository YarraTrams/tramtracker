package com.yarratrams.tramtracker.ui.util;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.ui.DisruptionsActivity;

public class DisruptionsRoutesAdapter extends ArrayAdapter<String> {

	// variables
	Activity context;
	ArrayList<String> alRoutes;
	DisruptionsPreference disruptionsPreference;

	View v;

	public View getIndexView() {
		return v;
	}

	public DisruptionsRoutesAdapter(Activity context, ArrayList<String> objects) {
		super(context, R.layout.disruption_routes_list_child, objects);
		// TODO Auto-generated constructor stub

		this.context = context;
		alRoutes = objects;
		disruptionsPreference = new DisruptionsPreference(context);
		Console.print("Routes adapter setup");
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		final ViewHolderClass viewHolder;

		if (convertView == null) {
			LayoutInflater layoutInflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater.inflate(
					R.layout.disruption_routes_list_child, parent, false);

			viewHolder = new ViewHolderClass();
			initUI(convertView, viewHolder);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolderClass) convertView.getTag();
		}

		viewHolder.ctvRoutes.setText(alRoutes.get(position));

		viewHolder.ctvRoutes.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				System.out.println("-- selected route: "
						+ removeDestination(alRoutes.get(position)));
				if (viewHolder.ctvRoutes.isChecked()) {
					viewHolder.ctvRoutes.setChecked(false);
					String routeNumber = removeDestination(alRoutes.get(position));
					if(routeNumber.equals("3a")){
						routeNumber = "4";
					}
					disruptionsPreference
							.deleteRoute(routeNumber);
				} else {
					viewHolder.ctvRoutes.setChecked(true);
					String routeNumber = removeDestination(alRoutes.get(position));
					if(routeNumber.equals("3a")){
						routeNumber = "4";
					}
					disruptionsPreference
							.addNewRoutes(routeNumber);
				}

				System.out.println("-- routes in array: "
						+ disruptionsPreference.getRoutes());
				if (disruptionsPreference.getRoutesArrayList() != null
						&& disruptionsPreference.getRoutesArrayList().size() > 0) {
					if (((DisruptionsActivity) context).tooltipIndex == 1) {
						((DisruptionsActivity) context).showNextTutorialTooltip();
					}
				}

				
			}
		});

		ArrayList<String> alRoutesSaved = disruptionsPreference
				.getRoutesArrayList();

		if (alRoutesSaved != null) {
			// System.out.println("-- route compare : " + alRoutesSaved + " : "
			// + alRoutes.get(position) + " compare: " +
			// (alRoutesSaved.contains(removeDestination(alRoutes.get(position)))));
			String routeNumber = removeDestination(alRoutes.get(position));
			if(routeNumber.equals("3a")){
				routeNumber = "4";
			}
			if (alRoutesSaved
					.contains(routeNumber)) {
				viewHolder.ctvRoutes.setChecked(true);
			} else {
				viewHolder.ctvRoutes.setChecked(false);
			}
		}

		if (position == 1) {
			v = convertView;
		}

		return convertView;
	}

	private void initUI(View convertView, ViewHolderClass viewHolder) {
		// TODO Auto-generated method stub
		viewHolder.ctvRoutes = (CheckedTextView) convertView
				.findViewById(R.id.ctvRoutes);
	}

	static class ViewHolderClass {
		CheckedTextView ctvRoutes;

	}

	public String removeDestination(String route) {
		return route.substring(0, (route.indexOf("-") - 1)).trim();
	}

}
