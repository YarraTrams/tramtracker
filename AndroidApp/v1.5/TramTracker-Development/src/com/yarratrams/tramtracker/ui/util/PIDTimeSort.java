package com.yarratrams.tramtracker.ui.util;

import java.util.Comparator;

import com.yarratrams.tramtracker.objects.DestinationGroup;

public class PIDTimeSort implements Comparator<DestinationGroup> {

	@Override
	public int compare(DestinationGroup lhs, DestinationGroup rhs) {
		// TODO Auto-generated method stub
		//		if(lhs.getTramEarliestTime().getTime() > rhs.getTramEarliestTime().getTime())
		//		{
		//			return 1;
		//		}
		//		else
		//		{
		//			return -1;
		//		}
		//System.out.println("-- compare result: " +lhs.getTramEarliestTime().compareTo(rhs.getTramEarliestTime()) );
		return lhs.getTramEarliestTime().compareTo(rhs.getTramEarliestTime());


	}

}
