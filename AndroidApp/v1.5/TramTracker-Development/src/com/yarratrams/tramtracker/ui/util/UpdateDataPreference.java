package com.yarratrams.tramtracker.ui.util;

import android.app.Activity;
import android.content.SharedPreferences;

/*
 * Added by Adil
 */
public class UpdateDataPreference {

	//variables
	Activity activity;
	SharedPreferences sharedPreferences;
	SharedPreferences.Editor editor;

	final String PREF_NAME = "updatepref";
	final String UPDATE_DATA = "updatedata";
	final String IS_UPDATE_AVAILABLE = "isupdateavailable";
	final String IS_UPDATE_COMPLETED = "isupdatecompleted";


	public UpdateDataPreference(Activity _activity) {
		// TODO Auto-generated constructor stub

		activity = _activity;
		sharedPreferences = activity.getSharedPreferences(PREF_NAME, Activity.MODE_PRIVATE);
		editor = sharedPreferences.edit();

	}


	public void setUpdateData(String data)
	{
		editor.putString(UPDATE_DATA, data);
		editor.commit();
	}

	public String getUpdateData()
	{
		return sharedPreferences.getString(UPDATE_DATA, "");
	}




	public void setIsUpdateAvailable(boolean data)
	{
		editor.putBoolean(IS_UPDATE_AVAILABLE, data);
		editor.commit();
	}

	public boolean getIsUpdateAvailable()
	{
		return sharedPreferences.getBoolean(IS_UPDATE_AVAILABLE, false);
	}





	public void setIsUpdateCompleted(boolean data)
	{
		editor.putBoolean(IS_UPDATE_COMPLETED, data);
		editor.commit();
	}

	public boolean getIsUpdateCompleted()
	{
		return sharedPreferences.getBoolean(IS_UPDATE_COMPLETED, false);
	}

}
