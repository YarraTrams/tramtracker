package com.yarratrams.tramtracker.ui;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.androidtooltip.TooltipManager;
import com.android.androidtooltip.TooltipManager.Gravity;
import com.android.androidtooltip.TutorialTooltip;
import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.db.TTDB;
import com.yarratrams.tramtracker.objects.Route;
import com.yarratrams.tramtracker.tasks.DisruptionNotificationTask;
import com.yarratrams.tramtracker.ui.util.Console;
import com.yarratrams.tramtracker.ui.util.DisruptionsPreference;
import com.yarratrams.tramtracker.ui.util.DisruptionsRoutesAdapter;
import com.yarratrams.tramtracker.ui.util.FeaturesPreferences;
import com.yarratrams.tramtracker.ui.util.OtherLevelAds;
import com.yarratrams.tramtracker.ui.util.TutorialPreferences;

public class DisruptionsActivity extends Activity
		implements
			TooltipManager.onTooltipClosingCallback {

	// variables
	Activity activity;
	ArrayList<String> alRoutes;
	TTDB ttdb;
	DisruptionsRoutesAdapter disruptionsRoutesAdapter;
	DisruptionsPreference disruptionsPreference;

	public static String MORNING_WEEKDAY_PEAK = "1";
	public static String EVENING_WEEKDAY_PEAK = "2";
	public static String WEEKDAY_ALL_DAY = "3";
	public static String WEEKEND = "4";

	// Tutorial Variables
	TooltipManager tooltipManager;
	ArrayList<TutorialTooltip> alTutorialTooltips;
	public int tooltipIndex = 0;
	TutorialTooltip tempTutorialTooltip;
	TutorialPreferences tutorialPreferences;

	// ui elements
	CheckedTextView ctvWeekdayMorningPeak, ctvWeekdayEveningPeak,
			ctvWeekdayAllDay, ctvWeekend;
	RelativeLayout settings_one;
	ImageView ivIndexImage;
	ListView lvDisruptionRoutes;
	View headerList;
	Button bSave;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.disruption_screen);

		activity = this;

		disruptionsPreference = new DisruptionsPreference(activity);
		tutorialPreferences = new TutorialPreferences(this);

		// reset all for test
		// tutorialPreferences.resetAll();

		tooltipManager = new TooltipManager(TramTrackerMainActivity.instance);
		lvDisruptionRoutes = (ListView) findViewById(R.id.lvDisruptionRoutes);

		bSave = (Button) findViewById(R.id.save_button);
		bSave.setVisibility(View.GONE);

		loadRoutesList();

		if (tutorialPreferences.getDisruptionTutorial() == false) {
			createTutorialToolTips();
			tooltipIndex = 0;
			if (alTutorialTooltips != null) {
				alTutorialTooltips.get(tooltipIndex).showTooltip();
				// showNextTutorialTooltip();
				// tooltipIndex++;

			}
		}

		// cals ads
		OtherLevelAds.showAds(activity, R.id.rich_banner_fragment105577,
				FeaturesPreferences
						.getAdsFlag(TramTrackerMainActivity.instance));

	}

	private void loadSaveTimes() {
		// TODO Auto-generated method stub

		ArrayList<String> alTimmings = disruptionsPreference
				.getTimmingsArrayList();

		if (alTimmings != null) {
			if (alTimmings.contains(EVENING_WEEKDAY_PEAK)) {
				ctvWeekdayEveningPeak.setChecked(true);
			}

			if (alTimmings.contains(MORNING_WEEKDAY_PEAK)) {
				ctvWeekdayMorningPeak.setChecked(true);
			}

			if (alTimmings.contains(WEEKDAY_ALL_DAY)) {
				ctvWeekdayAllDay.setChecked(true);
			}

			if (alTimmings.contains(WEEKEND)) {
				ctvWeekend.setChecked(true);
			}
		}

	}

	private void initUI() {
		// TODO Auto-generated method stub

		ctvWeekdayAllDay = (CheckedTextView) headerList
				.findViewById(R.id.ctvWeekdayAllDay);
		ctvWeekdayMorningPeak = (CheckedTextView) headerList
				.findViewById(R.id.ctvWeekdayMorningPeak);
		ctvWeekdayEveningPeak = (CheckedTextView) headerList
				.findViewById(R.id.ctvWeekdayEveningPeak);
		ctvWeekend = (CheckedTextView) headerList.findViewById(R.id.ctvWeekend);

		settings_one = (RelativeLayout) headerList
				.findViewById(R.id.settings_one);
		ivIndexImage = (ImageView) headerList.findViewById(R.id.ivIndexImage);
		// lvDisruptionRoutes = (ListView)
		// findViewById(R.id.lvDisruptionRoutes);

	}

	public void loadRoutesList() {
		ArrayList<Route> arrayList = null;
		ttdb = new TTDB(activity);
		arrayList = ttdb.getAllRoutes();

		if (arrayList != null) {
			// remove sub routes

//			for (int i = 0; i < arrayList.size(); i++) {
//				try {
//					int num = Integer.parseInt(arrayList.get(i)
//							.getRouteNumber());
//				} catch (NumberFormatException e) {
//					// TODO: handle exception
//					arrayList.remove(i);
//					i--;
//				}
//			}

			alRoutes = new ArrayList<String>();

			for (int i = 0; i < arrayList.size(); i++) {
				alRoutes.add(arrayList.get(i).getRouteNumber() + " - "
						+ arrayList.get(i).getUpDestination() + " to "
						+ arrayList.get(i).getDownDestination());
			}

		}

		if (alRoutes != null) {
			// ArrayAdapter<String> arrayAdapter = new
			// ArrayAdapter<String>(activity,
			// android.R.layout.simple_list_item_checked, alRoutes);

			bSave.setVisibility(View.VISIBLE);

			bSave.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					if (disruptionsPreference.getRoutesArrayList() == null
							|| disruptionsPreference.getRoutesArrayList()
									.size() < 1) {
						Toast.makeText(TramTrackerMainActivity.instance,
								"Please select a route for notifications", 0)
								.show();
					} else if (disruptionsPreference.getTimmingsArrayList() == null
							|| disruptionsPreference.getTimmingsArrayList()
									.size() < 1) {
						Toast.makeText(TramTrackerMainActivity.instance,
								"Please select travel times for notifications",
								0).show();
					} else {
						DisruptionNotificationTask disruptionNotificationTask = new DisruptionNotificationTask(
								activity);
						disruptionNotificationTask.execute();

					}

					if (tooltipIndex == 2) {
						dismissTutorial();
						tutorialPreferences.setDisruptionTutorial(true);
					}
				}
			});

			headerList = getLayoutInflater().inflate(
					R.layout.disruptionlist_header, null);

			initUI();

			checkBoxListeners();

			// loadRoutesList();

			loadSaveTimes();

			lvDisruptionRoutes.addHeaderView(headerList, null, false);

			disruptionsRoutesAdapter = new DisruptionsRoutesAdapter(activity,
					alRoutes);
			lvDisruptionRoutes.setAdapter(disruptionsRoutesAdapter);
		}

	}

	private void checkBoxListeners() {
		ctvWeekdayMorningPeak.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (ctvWeekdayMorningPeak.isChecked()) {
					ctvWeekdayMorningPeak.setChecked(false);
					disruptionsPreference.deleteTimmings(MORNING_WEEKDAY_PEAK);

				} else {
					ctvWeekdayMorningPeak.setChecked(true);
					disruptionsPreference.addNewTimmings(MORNING_WEEKDAY_PEAK);
				}
				checkTimeCb();
			}
		});

		ctvWeekdayAllDay.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (ctvWeekdayAllDay.isChecked()) {
					ctvWeekdayAllDay.setChecked(false);
					disruptionsPreference.deleteTimmings(WEEKDAY_ALL_DAY);
					// ctvWeekdayAllDay.setCheckMarkDrawable(R.drawable.icn_alarm);
				} else {
					ctvWeekdayAllDay.setChecked(true);
					// ctvWeekdayAllDay.setCheckMarkDrawable(R.drawable.ftz_zone);
					disruptionsPreference.addNewTimmings(WEEKDAY_ALL_DAY);
				}
				checkTimeCb();
			}
		});

		ctvWeekdayEveningPeak.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (ctvWeekdayEveningPeak.isChecked()) {
					ctvWeekdayEveningPeak.setChecked(false);
					disruptionsPreference.deleteTimmings(EVENING_WEEKDAY_PEAK);
				} else {
					ctvWeekdayEveningPeak.setChecked(true);
					disruptionsPreference.addNewTimmings(EVENING_WEEKDAY_PEAK);
				}
				checkTimeCb();
			}
		});

		ctvWeekend.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (ctvWeekend.isChecked()) {
					ctvWeekend.setChecked(false);
					disruptionsPreference.deleteTimmings(WEEKEND);
				} else {
					ctvWeekend.setChecked(true);
					disruptionsPreference.addNewTimmings(WEEKEND);
				}
				checkTimeCb();
			}
		});

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		TramTrackerMainActivity.getAppManager().back();
	}

	public String removeDestination(String route) {
		return route.substring(0, (route.indexOf("-") - 1));
	}

	public void checkTimeCb() {
		if (ctvWeekdayMorningPeak.isChecked() || ctvWeekdayAllDay.isChecked()
				|| ctvWeekdayEveningPeak.isChecked() || ctvWeekend.isChecked()) {
			if (tooltipIndex == 0) {
				showNextTutorialTooltip();
			}
		}
	}

	@Override
	public void onClosing(int id, boolean fromUser, boolean containsTouch) {
		// TODO Auto-generated method stub

		switch (id) {
			case R.id.ctvWeekdayMorningPeak :
				// tooltipIndex = 0;
				// showNextTutorialTooltip();
				break;

			case R.id.ctvWeekdayAllDay :
				// tooltipIndex = 0;
				// showNextTutorialTooltip();
				break;

			case R.id.ctvWeekdayEveningPeak :
				// tooltipIndex = 0;
				// showNextTutorialTooltip();
				break;

			case R.id.ctvWeekend :
				// tooltipIndex = 0;
				// showNextTutorialTooltip();
				break;

			case R.id.save_button :

				break;

			default :

				// showNextTutorialTooltip();
				break;
		}

	}

	public void createTutorialToolTips() {

		// String disruptionsText =
		// "Choose your ideal Travel Time and select the Routes that you would like to be notified of disruptions.<br/><br/>Tap on a <b>Travel Time</b> and your favourite <b>Route</b> to set one up now.";
		String disruptionsText = "To receive notifications, SELECT at least one travel time";
		String routeText = " …and SELECT at least one route.";
		// String saveText = "Tap <b>Save</b> to finish your set up.";
		String saveText = " Tap SAVE to finish.";

		alTutorialTooltips = new ArrayList<TutorialTooltip>();

		tempTutorialTooltip = new TutorialTooltip();
		tempTutorialTooltip.createTutorialTooltipNoDismiss(tooltipManager,
				ctvWeekdayMorningPeak, this, Gravity.TOP, disruptionsText);
		alTutorialTooltips.add(tempTutorialTooltip);

		tempTutorialTooltip = new TutorialTooltip();
		tempTutorialTooltip.createTutorialTooltipNoDismiss(tooltipManager,
				ivIndexImage, this, Gravity.TOP, routeText);
		alTutorialTooltips.add(tempTutorialTooltip);

		tempTutorialTooltip = new TutorialTooltip();
		tempTutorialTooltip.createTutorialTooltipNoDismiss(tooltipManager,
				bSave, this, Gravity.BOTTOM, saveText);
		alTutorialTooltips.add(tempTutorialTooltip);

	}

	public void showNextTutorialTooltip() {

		Console.print("tooltipIndex: " + tooltipIndex);
		try {

			alTutorialTooltips.get(tooltipIndex).closeTutorialTooltip(
					tooltipManager);
			tooltipIndex++;
		} catch (Exception e) {
			// TODO: handle exception
			Console.print("1 Exception : " + e.getMessage());
		}
		try {
			if (tooltipIndex < alTutorialTooltips.size()) {

				alTutorialTooltips.get(tooltipIndex).showTooltip();
			}
		} catch (Exception e) {
			// TODO: handle exception
			Console.print("2 Exception : " + e.getMessage());
		}
	}
	@Override
	public void onPause() {
		super.onPause();
		dismissTutorial();
	}

	public void dismissTutorial() {
		if (alTutorialTooltips != null) {
			for (int i = 0; i < alTutorialTooltips.size(); i++) {
				try {
					alTutorialTooltips.get(i).closeTutorialTooltip(
							tooltipManager);
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}

	}

}
