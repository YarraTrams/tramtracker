package com.yarratrams.tramtracker.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.ui.util.OtherLevelAds;

public class SpecialOffersActivity extends Activity {

	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_MORE;

	Activity activity;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.specialoffer_screen);
		activity =  this;


		
		//call special offer grid
		OtherLevelAds.showSpecialOfferGrid(activity, R.id.rich_grid_fragment);
		
		
		//call ads
		//OtherLevelAds.showAds(activity, R.id.rich_banner_fragment105566);


	}

	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

}
