package com.yarratrams.tramtracker.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.ViewFlipper;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.google.android.maps.Overlay;
import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.OnBoardStopsBySuburb;
import com.yarratrams.tramtracker.objects.PredictedArrivalTime;
import com.yarratrams.tramtracker.objects.Range;
import com.yarratrams.tramtracker.objects.Route;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.tasks.ScheduleDepartureTask;
import com.yarratrams.tramtracker.ui.util.FeaturesPreferences;
import com.yarratrams.tramtracker.ui.util.MapMyTramCurrentOverlay;
import com.yarratrams.tramtracker.ui.util.MapMyTramOverlay;
import com.yarratrams.tramtracker.ui.util.OtherLevelAds;
import com.yarratrams.tramtracker.ui.util.ScheduleDepartureExpandableListArrayAdapter;

/*
 * Added by adil for schedule departures
 */

public class ScheduleDepartureActivity extends MapActivity
		implements
			OnClickListener {

	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_MORE;
	public static final int VIEW_LIST = 0;
	public static final int VIEW_MAP = 1;
	public static final String ROUTE_INTENT_KEY = "route_info";
	public static final String TRIP_ID_INTENT_KEY = "trip_id";
	public static final String TRIP_TIME_INTENT_KEY = "trip_time";
	public static int GROUP_POS = 0, CHILD_POS = 0, MAP_INDEX = 0;
	public static String STOP_NUM = "";

	int tripId;
	long tripTime;

	private boolean isListViewSelected;
	private ViewFlipper flipper;

	private ExpandableListView stopsList;

	private ArrayList<PredictedArrivalTime> stops;

	private MapView stopsMap;
	private List<Overlay> mapOverlays;
	private MyLocationOverlay myOverlay;
	private GeoPoint mapCentre;
	private int zoomLevel;

	Activity activity;

	ScheduleDepartureTask scheduleDepartureTask;

	ScheduleDepartureExpandableListArrayAdapter scheduleDepartureExpandableListArrayAdapter;

	Route route;

	private int startingIndex;

	/*
	 * FREE TRAM ZONE
	 */
	public static Map<String, Integer> mapFTZ = new HashMap<String, Integer>();

	/*
	 * FREE TRAM ZONE
	 */
	private ArrayList<OnBoardStopsBySuburb> suburbs;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		activity = this;
		// setContentView(R.layout.scheduledeparture_screen);
		setContentView(R.layout.scheduledeparture_screen);

		isListViewSelected = true;
		flipper = (ViewFlipper) findViewById(R.id.view_flipper);

		ToggleButton listButton = (ToggleButton) findViewById(R.id.list_view_button);
		listButton.setOnClickListener(this);
		ToggleButton mapButton = (ToggleButton) findViewById(R.id.map_view_button);
		mapButton.setOnClickListener(this);
		Button refreshButton = (Button) findViewById(R.id.refresh_button);

		refreshButton.setVisibility(View.INVISIBLE);

		stopsList = (ExpandableListView) findViewById(R.id.expandable_list);
		stopsList.addHeaderView(getTableView());

		Display display = getWindowManager().getDefaultDisplay();
		Drawable groupIndicator = getResources().getDrawable(
				R.drawable.icn_list_expandable);
		stopsList.setGroupIndicator(groupIndicator);
		stopsList.setChildIndicator(null);
		if (android.os.Build.VERSION.SDK_INT < 18)
			stopsList.setIndicatorBounds(display.getWidth()
					- GetDipsFromPixel(30), display.getWidth());
		else
			stopsList.setIndicatorBoundsRelative(display.getWidth()
					- GetDipsFromPixel(30), display.getWidth());

		stopsMap = (MapView) findViewById(R.id.map);
		stopsMap.setBuiltInZoomControls(true);
		myOverlay = new MyLocationOverlay(this, stopsMap);
		mapOverlays = stopsMap.getOverlays();

		startingIndex = -1;

		// call ads
		OtherLevelAds.showAds(activity, R.id.rich_banner_fragment1018,FeaturesPreferences.getAdsFlag(TramTrackerMainActivity.instance));
	}

	public int GetDipsFromPixel(float pixels) {
		final float scale = getResources().getDisplayMetrics().density;
		return (int) (pixels * scale + 0.5f);
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();

		route = activity.getIntent().getExtras()
				.getParcelable(ROUTE_INTENT_KEY);
		tripId = activity.getIntent().getExtras().getInt(TRIP_ID_INTENT_KEY);
		tripTime = activity.getIntent().getExtras()
				.getLong(TRIP_TIME_INTENT_KEY);
		scheduleDepartureTask = new ScheduleDepartureTask(activity,
				getApplicationContext(), tripId, tripTime);
		scheduleDepartureTask.execute();

	}

	public void updateUI(
			ArrayList<PredictedArrivalTime> arrayListPredictedArrivalTimes) {
		this.stops = arrayListPredictedArrivalTimes;

		generateSuburbsList();

		// System.out.println("-- stops len in schedule dep: " + stops.size() );
		// System.out.println("-- route is: " + route.getDescription());

		scheduleDepartureExpandableListArrayAdapter = new ScheduleDepartureExpandableListArrayAdapter(
				activity, stops, route);
		stopsList.setAdapter(scheduleDepartureExpandableListArrayAdapter);

		expandAll();

		retrieveStops();

		if (mapCentre != null) {
			MapController mapController = stopsMap.getController();
			mapController.setCenter(mapCentre);
			mapController.setZoom(zoomLevel);
		}

		// checking list scrolling
		// setListPosition();

		stopsList.setSelectedGroup(GROUP_POS);
		stopsList.setSelectedChild(GROUP_POS, CHILD_POS, true);

		try {

			for (int i = 0; i < stops.size(); i++) {
				if (stops.get(i).getStop().getStopNumber().equals(STOP_NUM)) {
					MAP_INDEX = i;
				}
			}

			MapMyTramCurrentOverlay currentOverlay = new MapMyTramCurrentOverlay(
					this, stopsMap);
			ArrayList<PredictedArrivalTime> alCurrStop = new ArrayList<PredictedArrivalTime>();
			alCurrStop.add(stops.get(MAP_INDEX));
			currentOverlay.updateStopsList(alCurrStop);
			mapOverlays.add(currentOverlay);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void retrieveStops() {

		TextView routeName = (TextView) findViewById(R.id.route_name);
		routeName.setText(getRouteName());
		// Setup map
		mapOverlays.clear();
		mapOverlays.add(myOverlay);

		MapMyTramOverlay overlay = new MapMyTramOverlay(
				TramTrackerMainActivity.instance, stopsMap);
		overlay.updateStopsList(stops);
		mapOverlays.add(overlay);

		centerMapOnStops();

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		ToggleButton listButton = (ToggleButton) findViewById(R.id.list_view_button);
		ToggleButton mapButton = (ToggleButton) findViewById(R.id.map_view_button);

		if (v == listButton || v == mapButton) {
			switchViews();
		}

	}

	// private void refresh(){
	// centerMapOnStops();
	// }

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		TramTrackerMainActivity.getAppManager().back();
	}

	private View getTableView() {
		LayoutInflater inflater = getLayoutInflater();
		View rowView = inflater.inflate(R.layout.routes_stops_list_view_table,
				null, true);
		return rowView;
	}

	private String getRouteName() {
		String text = "";

		text = route.getRouteNumber();
		text = text.concat(getResources().getString(
				R.string.routes_entry_name_dash));
		if (route.isUpDestination()) {
			if (route.getDownDestination() != null) {
				if (route.getDownDestination().length() > 0) {
					text = text.concat(route.getDownDestination());
				}
			}
			if (route.getUpDestination() != null) {
				if (route.getUpDestination().length() > 0) {
					text = text.concat(getResources().getString(
							R.string.routes_entry_name_to));
					text = text.concat(route.getUpDestination());
				}
			}
		} else {
			if (route.getUpDestination() != null) {
				if (route.getUpDestination().length() > 0) {
					text = text.concat(route.getUpDestination());
				}
			}
			if (route.getDownDestination() != null) {
				if (route.getDownDestination().length() > 0) {
					text = text.concat(getResources().getString(
							R.string.routes_entry_name_to));
					text = text.concat(route.getDownDestination());
				}
			}
		}

		return text;
	}

	// private String getTramNumber(){
	// String text = "";
	// text = getResources().getString(R.string.onboard_title_tram);
	// text = text.concat(String.valueOf(results.getTram().getVehicleNumber()));
	// return text;
	// }

	private void switchViews() {
		ToggleButton listButton = (ToggleButton) findViewById(R.id.list_view_button);
		ToggleButton mapButton = (ToggleButton) findViewById(R.id.map_view_button);
		Button refreshButton = (Button) findViewById(R.id.refresh_button);
		if (isListViewSelected) {
			if (stops != null) {
				isListViewSelected = false;
				listButton.setChecked(false);
				mapButton.setChecked(true);
				refreshButton.setVisibility(View.VISIBLE);
				myOverlay.enableMyLocation();
				flipper.setDisplayedChild(VIEW_MAP);
				centerMapOnStops();
				TramTrackerMainActivity.getAppManager().callSelection(
						getString(R.string.accessibility_click_header_mapview));
			} else{
				mapButton.setChecked(false);
				listButton.setChecked(true);
				Toast.makeText(activity, "Unable to open maps.", 1).show();
			}
		} else {
			isListViewSelected = true;
			listButton.setChecked(true);
			mapButton.setChecked(false);
			refreshButton.setVisibility(View.INVISIBLE);
			flipper.setDisplayedChild(VIEW_LIST);
			TramTrackerMainActivity.getAppManager().callSelection(
					getString(R.string.accessibility_click_header_listview));
		}
	}

	public void centerMapOn(GeoPoint center, int max, boolean islat) {
		MapController mapController = stopsMap.getController();

		if (islat) {
			if (max < 20000) {
				zoomLevel = 16; // Zoom 16 few blocks
			} else if (max < 30000) {
				zoomLevel = 15;
			} else if (max < 50000) {
				zoomLevel = 14;
			} else if (max < 70000) {
				zoomLevel = 13;
			} else if (max < 131000) {
				zoomLevel = 12;
			} else if (max < 150000) {
				zoomLevel = 11;
			} else {
				zoomLevel = 10; // Zoom 10 is city view
			}
		} else {
			if (max < 25000) {
				zoomLevel = 16; // Zoom 16 few blocks
			} else if (max < 30000) {
				zoomLevel = 15;
			} else if (max < 80000) {
				zoomLevel = 14;
			} else if (max < 100000) {
				zoomLevel = 13;
			} else if (max < 190000) {
				zoomLevel = 12;
			} else if (max < 250000) {
				zoomLevel = 11;
			} else {
				zoomLevel = 10; // Zoom 10 is city view
			}
		}

		mapController.setZoom(zoomLevel);
		if (center != null) {
			mapCentre = center;
			mapController.animateTo(mapCentre);
		}
	}

	public void centerMapOnStops() {
		int maxLat = 0;
		int minLat = 0;
		int maxLon = 0;
		int minLon = 0;

		for (PredictedArrivalTime stop : stops) {
			if (maxLat == 0) {
				maxLat = stop.getStop().getLatitudeE6();
				minLat = stop.getStop().getLatitudeE6();
				maxLon = stop.getStop().getLongitudeE6();
				minLon = stop.getStop().getLongitudeE6();
			}
			maxLat = stop.getStop().getLatitudeE6() > maxLat ? stop.getStop()
					.getLatitudeE6() : maxLat;
			minLat = stop.getStop().getLatitudeE6() < minLat ? stop.getStop()
					.getLatitudeE6() : minLat;
			maxLon = stop.getStop().getLongitudeE6() > maxLon ? stop.getStop()
					.getLongitudeE6() : maxLon;
			minLon = stop.getStop().getLongitudeE6() < minLon ? stop.getStop()
					.getLongitudeE6() : minLon;
		}
		if (maxLat == 0) {
			maxLat = (int) (-37.817491 * 1E6);
			minLat = (int) (-37.817491 * 1E6);
			maxLon = (int) (144.967445 * 1E6);
			minLon = (int) (144.967445 * 1E6);
		}

		int dLat = maxLat - minLat;
		int dLon = maxLon - minLon;

		int cLat = minLat + (dLat / 2);
		int cLon = minLon + (dLon / 2);

		int max = 0;
		boolean isLat = false;
		if (dLat == 0 && dLon == 0) {
			max = 150000;
		} else {
			if (dLat > dLon) {
				max = dLat;
				isLat = true;
			} else {
				max = dLon;
				isLat = false;
			}
		}

		GeoPoint centre = new GeoPoint(cLat, cLon);
		centerMapOn(centre, max, isLat);

		MapController mapController = stopsMap.getController();
		mapController.zoomToSpan(dLat, dLon);
		if (centre != null) {
			mapCentre = centre;
			mapController.animateTo(mapCentre);
		}

		// stopsMap.invalidate();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		if (!isListViewSelected) {
			myOverlay.enableMyLocation();
		}

		if (mapCentre != null) {
			MapController mapController = stopsMap.getController();
			mapController.setCenter(mapCentre);
			mapController.setZoom(zoomLevel);
		}
	}

	// private void updateMapStartingIndex(){
	// // System.out.println("............ STOPS" + stops.size());
	// if(stops.isEmpty()){
	// return;
	// }
	//
	// ArrayList<PredictedArrivalTime> normalStops = new
	// ArrayList<PredictedArrivalTime>();
	// normalStops.addAll(stops);
	// PredictedArrivalTime stop = stops.get(startingIndex);
	// normalStops.remove(startingIndex);
	//
	// ArrayList<PredictedArrivalTime> currentStops = new
	// ArrayList<PredictedArrivalTime>();
	// currentStops.add(stop);
	//
	// if(!isAccurateIndex){
	// if(startingIndex < stops.size()-1){
	// stop = stops.get(startingIndex + 1);
	// normalStops.remove(startingIndex);
	// currentStops.add(stop);
	// }
	// }
	//
	// mapOverlays.clear();
	//
	// MapMyTramOverlay overlay = new MapMyTramOverlay(this, stopsMap);
	// overlay.updateStopsList(normalStops);
	// mapOverlays.add(overlay);
	//
	// MapMyTramCurrentOverlay currentOverlay = new
	// MapMyTramCurrentOverlay(this, stopsMap);
	// currentOverlay.updateStopsList(currentStops);
	// mapOverlays.add(currentOverlay);
	//
	// stopsMap.invalidate();
	// }

	/*
	 * FREE TRAM ZONE
	 */

	public void generateSuburbsList() {

		// System.out.println("-- In generate suburb list");

		ArrayList<PredictedArrivalTime> ftzstops = stops;
		suburbs = new ArrayList<OnBoardStopsBySuburb>();
		OnBoardStopsBySuburb suburb;

		// Get unique suburbs
		for (PredictedArrivalTime stop : ftzstops) {

			if (suburbs.size() >= 1) {
				suburb = suburbs.get(suburbs.size() - 1);

				// System.out.println("-- suburb.getSuburb():"+suburb.getSuburb()+" stop.getStop():"+stop.getStop()+" stop.getStop().getSuburb():"+stop.getStop().getSuburb());
				try {
					if (!suburb.getSuburb().equalsIgnoreCase(
							stop.getStop().getSuburb())) {
						suburb = new OnBoardStopsBySuburb();
						suburbs.add(suburb);
					}
				} catch (NullPointerException e) {
					e.printStackTrace();
				}
			} else {
				suburb = new OnBoardStopsBySuburb();
				suburbs.add(suburb);
			}

			suburb.setSuburb(stop.getStop().getSuburb());
			suburb.addStop(stop);
		}

		/*
		 * Free Tram Zone
		 */

		// Get count of free tram zones inside a suburb
		for (int i = 0; i < suburbs.size(); i++) {
			int counts = 0;
			for (int j = 0; j < suburbs.get(i).getStops().size(); j++) {
				// if(suburbs.get(i).getStops().get(j).getStop().isCityStop())
				if (suburbs.get(i).getStops().get(j).getStop().IsInFreeZone())
					counts++;
			}
			suburbs.get(i).setftzStopCount(counts);
		}

		// for(int i = 0; i < suburbs.size();i++)
		// {
		// System.out.println("-- " + suburbs.get(i).getSuburb() + " count: " +
		// suburbs.get(i).getftzStopCount());
		// }

		// Set ranges of FTZ logo
		// int index = 0;
		ArrayList<Range> arrlstRanges = new ArrayList<Range>();
		Range tempRange;
		int startIndex = 0, len = 0;

		for (int i = 0; i < suburbs.size(); i++) {
			for (int j = 0; j < suburbs.get(i).getStops().size(); j++) {
				Stop stop = suburbs.get(i).getStops().get(j).getStop();

				if (stop.IsInFreeZone()) {
					len++;
				} else {
					tempRange = new Range(i, startIndex, len);
					arrlstRanges.add(tempRange);
					tempRange = null;
					startIndex = j + 1;
					len = 0;
				}
			}
			tempRange = new Range(i, startIndex, len);
			arrlstRanges.add(tempRange);
			tempRange = null;
			startIndex = 0;
			len = 0;
		}

		// Remove suburbs who has less then two repeating cells
		for (int i = 0; i < arrlstRanges.size(); i++) {

			if (arrlstRanges.get(i).lenght < 3) {

				arrlstRanges.remove(i);
				i--;
			}
		}

		for (int i = 0; i < arrlstRanges.size(); i++) {
			// System.out.println("--  "+ suburbs.get(i).getSuburb()
			// +": ranges  : " + arrlstRanges.get(i).toString());

			int length = arrlstRanges.get(i).lenght;
			int subIndex = arrlstRanges.get(i).suburbIndex;
			int startingIndex = arrlstRanges.get(i).location;

			switch (length) {
				case 3 :

					suburbs.get(subIndex).getStops().get(startingIndex + 0)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 1)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
							.getStop().setFtzImageId(R.drawable.ftz_free);
					break;

				case 4 :

					suburbs.get(subIndex).getStops().get(startingIndex + 0)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 1)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
							.getStop().setFtzImageId(R.drawable.ftz_free);
					break;

				case 5 :

					suburbs.get(subIndex).getStops().get(startingIndex + 1)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 3)
							.getStop().setFtzImageId(R.drawable.ftz_free);
					break;

				case 6 :

					// System.out.println("--calling 6 for suburb : " +
					// suburbs.get(i).getSuburb());
					suburbs.get(subIndex).getStops().get(startingIndex + 1)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 3)
							.getStop().setFtzImageId(R.drawable.ftz_free);
					break;

				case 7 :
					suburbs.get(subIndex).getStops().get(startingIndex + 0)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 1)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 4)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 5)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 6)
							.getStop().setFtzImageId(R.drawable.ftz_free);
					break;

				case 8 :
					suburbs.get(subIndex).getStops().get(startingIndex + 0)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 1)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 4)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 5)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 6)
							.getStop().setFtzImageId(R.drawable.ftz_free);
					break;

				case 9 :
					suburbs.get(subIndex).getStops().get(startingIndex + 1)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 3)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 5)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 6)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 7)
							.getStop().setFtzImageId(R.drawable.ftz_free);
					break;

				case 10 :
					suburbs.get(subIndex).getStops().get(startingIndex + 1)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 3)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 5)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 6)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 7)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					break;

				case 11 :

					suburbs.get(subIndex).getStops().get(startingIndex + 0)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 1)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 4)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 5)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 6)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 8)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 9)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 10)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					break;

				case 12 :

					suburbs.get(subIndex).getStops().get(startingIndex + 0)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 1)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 4)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 5)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 6)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 8)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 9)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 10)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					break;

				case 13 :

					suburbs.get(subIndex).getStops().get(startingIndex + 0)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 1)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 4)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 5)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 6)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 8)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 9)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 10)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					break;

				case 14 :

					suburbs.get(subIndex).getStops().get(startingIndex + 1)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 3)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 5)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 6)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 7)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 9)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 10)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 11)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					break;

				case 15 :

					suburbs.get(subIndex).getStops().get(startingIndex + 0)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 1)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 4)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 5)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 6)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 8)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 9)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 10)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 12)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 13)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 14)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					break;

				case 16 :

					suburbs.get(subIndex).getStops().get(startingIndex + 0)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 1)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 4)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 5)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 6)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 8)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 9)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 10)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 12)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 13)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 14)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					break;

				case 17 :

					suburbs.get(subIndex).getStops().get(startingIndex + 1)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 3)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 5)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 6)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 7)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 9)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 10)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 11)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 13)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 14)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 15)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					break;

				case 18 :

					suburbs.get(subIndex).getStops().get(startingIndex + 1)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 3)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 5)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 6)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 7)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 9)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 10)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 11)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 13)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 14)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 15)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					break;

				case 19 :

					suburbs.get(subIndex).getStops().get(startingIndex + 0)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 1)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 4)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 5)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 6)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 8)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 9)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 10)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 12)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 13)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 14)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 16)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 17)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 18)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					break;

				case 20 :

					suburbs.get(subIndex).getStops().get(startingIndex + 0)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 1)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 2)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 4)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 5)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 6)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 8)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 9)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 10)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 12)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 13)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 14)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					suburbs.get(subIndex).getStops().get(startingIndex + 16)
							.getStop().setFtzImageId(R.drawable.ftz_zone);
					suburbs.get(subIndex).getStops().get(startingIndex + 17)
							.getStop().setFtzImageId(R.drawable.ftz_tram);
					suburbs.get(subIndex).getStops().get(startingIndex + 18)
							.getStop().setFtzImageId(R.drawable.ftz_free);

					break;

			}
		}

	}

	private void expandAll() {
		for (int i = 0; i < scheduleDepartureExpandableListArrayAdapter
				.getGroupCount(); i++) {
			stopsList.expandGroup(i);
		}

	}

	public void setListPosition() {
		ArrayList<String> alSuburb = new ArrayList<String>();
		Log.w("setListPosition", "stops.size() = " + stops.size());
		for (int i = 0; i < stops.size() && i <= startingIndex; i++) {
			if (alSuburb.size() > 0) {
				// Log.w("setListPosition","alSuburb.get(alSuburb.size() - 1) = "
				// + alSuburb.get(alSuburb.size() - 1));
				// Log.w("setListPosition","stops.get(i).getStop().getSuburb() = "
				// + stops.get(i).getStop().getSuburb());
				if (!alSuburb.get(alSuburb.size() - 1).equalsIgnoreCase(
						stops.get(i).getStop().getSuburb())) {
					alSuburb.add(stops.get(i).getStop().getSuburb());
				}
			} else
				alSuburb.add(stops.get(i).getStop().getSuburb());
		}
		Log.w("setListPosition", "startingIndex = " + startingIndex);
		Log.w("setListPosition", "alSuburb = " + alSuburb);
		int finalIndex = startingIndex + alSuburb.size() + 1;
		Log.w("setListPosition",
				"stopsList.getCount() = " + stopsList.getCount());
		Log.w("setListPosition",
				"setSelectionFromTop  = startingIndex + alSuburb.size() + 1 = "
						+ finalIndex);
		stopsList.setSelectionFromTop(finalIndex, 0);
	}

}
