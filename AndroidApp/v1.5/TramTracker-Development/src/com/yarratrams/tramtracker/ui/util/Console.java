package com.yarratrams.tramtracker.ui.util;

public class Console {

	
	
	// custom method to print data in log cat using "--" to easily filter custom messages
	
	public static void print(String data)
	{
		System.out.println("-- " + data);
	}
}
