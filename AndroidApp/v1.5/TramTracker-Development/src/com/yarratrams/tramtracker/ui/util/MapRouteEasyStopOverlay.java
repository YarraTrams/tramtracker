package com.yarratrams.tramtracker.ui.util;

import android.app.Activity;
import android.graphics.drawable.Drawable;

import com.google.android.maps.MapView;
import com.yarratrams.tramtracker.R;


public class MapRouteEasyStopOverlay extends MapRouteStopOverlay {

	private static Drawable getDefaultMarker(Activity context){
		return context.getResources().getDrawable(R.drawable.icn_search_easyaccess);
	}
	
	public MapRouteEasyStopOverlay(Activity context, MapView mapView) {
		super(getDefaultMarker(context), context, mapView);
	}
	
}
