package com.yarratrams.tramtracker.ui.util;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.Route;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;


public class RoutesFilterDialog extends Dialog implements android.content.DialogInterface.OnKeyListener{
	private Activity activity; 
	private ArrayList<Route> routes;
	private ArrayList<String> selected;
	private boolean filtered;

	public RoutesFilterDialog(Context context, final ArrayList<Route> routes, ArrayList<String> preSelected) {
		super(context);
        
		this.setOnKeyListener(this);
		activity = (Activity) context;
		this.routes = routes;
		this.filtered = false;
		if(preSelected != null){
			this.selected = preSelected;
		} else {
			this.selected = new ArrayList<String>();
		}
		
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.setContentView(R.layout.routes_filter_dialog);
        
        LinearLayout list = (LinearLayout)findViewById(R.id.routes_filter_list);
        final RoutesFilterAdapter routeFilterAdapter = new RoutesFilterAdapter(list, activity, this.routes, this.selected);
        
        Button filter = (Button)findViewById(R.id.route_filter_button);
        filter.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ArrayList<String> list = routeFilterAdapter.getSelected(); 
				if(list !=  null ){
					if(validateSelected(list)){
						TramTrackerMainActivity.getAppManager().callSelection(activity.getString(R.string.accessibility_click_filter));
						selected = routeFilterAdapter.getSelected();
						filtered = true;
						dismiss();
					} else {
						TramTrackerMainActivity.getAppManager().displayErrorMessage(activity.getResources().getString(R.string.error_routes_filter));
					}
				}
			}
		});
	}
	
	private boolean validateSelected(ArrayList<String> list){
		if(list != null){
			for(String option : list){
				if(!option.equalsIgnoreCase(activity.getResources().getString(R.string.route_filter_dbtag_lowfloor))){
					return true;
				}
			}
		}
		return false;
	}
	

	@Override
	public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
		if(event.getAction() == KeyEvent.ACTION_UP){
			if(keyCode == KeyEvent.KEYCODE_BACK){
				this.dismiss();
				return true;
			}
		}
		return false;
	}
	
	public boolean isFiltered(){
		return filtered;
	}
	
	public ArrayList<String> getFiltered(){
		return this.selected;
	}
}