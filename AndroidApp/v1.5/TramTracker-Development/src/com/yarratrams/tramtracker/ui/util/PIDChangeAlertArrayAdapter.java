package com.yarratrams.tramtracker.ui.util;

import java.util.ArrayList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.ServiceChange;
import com.yarratrams.tramtracker.ui.PIDActivity;

public class PIDChangeAlertArrayAdapter extends ArrayAdapter<ServiceChange> {
	private final PIDActivity context;
	private final ArrayList<ServiceChange> changes;

	public PIDChangeAlertArrayAdapter(PIDActivity context, ArrayList<ServiceChange> changes) {
		super(context, R.layout.pid_changes_detail, changes);
		this.context = context;
		this.changes = changes;
	}

	static class ChangeViewHolder{
		TextView routeNumber;
		TextView changeDescription;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ChangeViewHolder viewHolder;
		
		if(convertView == null){
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(R.layout.pid_changes_detail, parent, false);
			
			viewHolder = new ChangeViewHolder();
			viewHolder.routeNumber = (TextView) convertView.findViewById(R.id.change_route_number);
			viewHolder.changeDescription = (TextView) convertView.findViewById(R.id.change_route_description);
			convertView.setTag(viewHolder);
		
		} else {
			viewHolder = (ChangeViewHolder) convertView.getTag();
		}
		
		ServiceChange change = changes.get(position);
		viewHolder.routeNumber.setVisibility(View.GONE);
		viewHolder.changeDescription.setText(change.getChangeDescription());
		viewHolder.changeDescription.setFocusable(false);
		convertView.setFocusable(false);

		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				context.switchChangeAlertState();
			}
		});
		
		return convertView;
	}
}
