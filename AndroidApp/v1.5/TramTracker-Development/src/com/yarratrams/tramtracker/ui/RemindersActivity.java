package com.yarratrams.tramtracker.ui;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.androidtooltip.TooltipManager;
import com.android.androidtooltip.TooltipManager.Gravity;
import com.android.androidtooltip.TutorialTooltip;
import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.Reminders;
import com.yarratrams.tramtracker.ui.util.FeaturesPreferences;
import com.yarratrams.tramtracker.ui.util.OtherLevelAds;
import com.yarratrams.tramtracker.ui.util.ReminderPreferences;
import com.yarratrams.tramtracker.ui.util.RemindersAdapter;
import com.yarratrams.tramtracker.ui.util.TutorialPreferences;

public class RemindersActivity extends Activity
		implements
			TooltipManager.onTooltipClosingCallback {

	// Variables
	Activity activity;
	RemindersAdapter remindersAdapter;
	ReminderPreferences reminderPreferences;
	ArrayList<Reminders> alReminders;
	HashMap<String, ArrayList<Reminders>> hmReminders;
	ArrayList<String> alReminderHeaders;

	// UI Elements
	ExpandableListView lvReminders;

	// tooltip
	TooltipManager tooltipManager;
	TutorialTooltip reminderTutorialTooltip;
	TutorialPreferences tutorialPreferences;

	boolean tutorialPointFlag = false;
	View tutorialPoint1, tutorialPoint2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reminders_screen);
		activity = this;

		tutorialPreferences = new TutorialPreferences(this);

		reminderPreferences = new ReminderPreferences(activity);

		tooltipManager = new TooltipManager(TramTrackerMainActivity.instance);
		initUI();

		// calls ads
		OtherLevelAds.showAds(activity, R.id.rich_banner_fragment105555,
				FeaturesPreferences
						.getAdsFlag(TramTrackerMainActivity.instance));

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();

		populateReminders();

		if (tutorialPreferences.getAlarmTutorial() == false) {
			if (reminderPreferences.getRemindersArrayList() != null
					&& reminderPreferences.getRemindersArrayList().size() > 0) {

				createTutorialToolTips();
				reminderTutorialTooltip.showTooltip();

			} else {
				Toast.makeText(
						activity,
						"Thank you for completing this tutorial. Get more tutorials in the tram arrival screen, myTRAM and Timetables.",
						1).show();
				tutorialPreferences.setAlarmTutorial(true);
				tutorialPreferences.setDisruptionTutorial(true);
				tutorialPreferences.setSettingsTutorial(true);
			}
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		onStart();
	}

	public void refreshRemindersList() {

		alReminders = reminderPreferences.getRemindersArrayList();
		ArrayList<Reminders> alOnStop, alOnTram;

		if (alReminders != null) {
			if (alReminders.size() > 0) {

				hmReminders = new HashMap<String, ArrayList<Reminders>>();
				alReminderHeaders = new ArrayList<String>();
				alReminderHeaders.add("On tram, approaching stop");
				alReminderHeaders.add("At stop, waiting for tram");

				alOnStop = new ArrayList<Reminders>();
				alOnTram = new ArrayList<Reminders>();

				for (int i = 0; i < alReminders.size(); i++) {
					Reminders currReminder = alReminders.get(i);

					if (currReminder.getREMINDER_TYPE() == Reminders.STOP_ALARM) {
						alOnTram.add(currReminder);
					} else {
						alOnStop.add(currReminder);
					}

					currReminder = null;
				}
				// Adding both array lists to hasp map

				hmReminders.put(alReminderHeaders.get(0), alOnTram);
				hmReminders.put(alReminderHeaders.get(1), alOnStop);

				remindersAdapter = new RemindersAdapter(activity, hmReminders,
						alReminderHeaders);
				lvReminders.setAdapter(remindersAdapter);

				int count = remindersAdapter.getGroupCount();
				for (int position = 1; position <= count; position++)
					lvReminders.expandGroup(position - 1);

				lvReminders.setOnGroupClickListener(new OnGroupClickListener() {

					@Override
					public boolean onGroupClick(ExpandableListView parent,
							View v, int groupPosition, long id) {
						// TODO Auto-generated method stub
						return true;
					}
				});
			} else {
				alReminderHeaders = null;
				alReminders = null;
				remindersAdapter.notifyDataSetChanged();
				remindersAdapter = null;
				lvReminders.setAdapter((BaseExpandableListAdapter) null);

				// Toast.makeText(TramTrackerMainActivity.instance,
				// getResources().getString(R.string.error_no_reminders),
				// 0).show();
			}
		} else {

			alReminderHeaders = null;
			alReminders = null;
			remindersAdapter.notifyDataSetChanged();
			remindersAdapter = null;
			lvReminders.setAdapter((BaseExpandableListAdapter) null);
			// Toast.makeText(TramTrackerMainActivity.instance,
			// getResources().getString(R.string.error_no_reminders), 0).show();

		}

		// System.out.println("-- refresh called");
		// alReminders = reminderPreferences.getRemindersArrayList();
		//
		// if(alReminders!=null)
		// {
		// if(alReminders.size() > 0)
		// {
		// if(remindersAdapter!=null)
		// {
		// remindersAdapter.notifyDataSetChanged();
		// }
		// }
		// else
		// {
		//
		// Toast.makeText(TramTrackerMainActivity.instance,
		// getResources().getString(R.string.error_no_reminders), 0).show();
		// }
		// }
		// else
		// {
		// Toast.makeText(TramTrackerMainActivity.instance,
		// getResources().getString(R.string.error_no_reminders), 0).show();
		// }
	}

	private void populateReminders() {
		// TODO Auto-generated method stub

		alReminders = reminderPreferences.getRemindersArrayList();
		ArrayList<Reminders> alOnStop, alOnTram;

		if (alReminders != null) {
			if (alReminders.size() > 0) {

				hmReminders = new HashMap<String, ArrayList<Reminders>>();
				alReminderHeaders = new ArrayList<String>();
				alReminderHeaders.add("On tram, approaching stop");
				alReminderHeaders.add("At stop, waiting for tram");

				alOnStop = new ArrayList<Reminders>();
				alOnTram = new ArrayList<Reminders>();

				for (int i = 0; i < alReminders.size(); i++) {
					Reminders currReminder = alReminders.get(i);

					if (currReminder.getREMINDER_TYPE() == Reminders.STOP_ALARM) {
						alOnTram.add(currReminder);
					} else {
						alOnStop.add(currReminder);
					}

					currReminder = null;
				}
				if (alOnTram.size() == 0) {
					tutorialPointFlag = true;
				} else {
					tutorialPointFlag = false;
				}
				// Adding both array lists to hasp map

				hmReminders.put(alReminderHeaders.get(0), alOnTram);
				hmReminders.put(alReminderHeaders.get(1), alOnStop);

				remindersAdapter = new RemindersAdapter(activity, hmReminders,
						alReminderHeaders);
				lvReminders.setAdapter(remindersAdapter);

				int count = remindersAdapter.getGroupCount();
				for (int position = 1; position <= count; position++)
					lvReminders.expandGroup(position - 1);

				lvReminders.setOnGroupClickListener(new OnGroupClickListener() {

					@Override
					public boolean onGroupClick(ExpandableListView parent,
							View v, int groupPosition, long id) {
						// TODO Auto-generated method stub
						return true;
					}
				});
			} else {

				// Toast.makeText(TramTrackerMainActivity.instance,
				// getResources().getString(R.string.error_no_reminders),
				// 0).show();
			}
		} else {
			// Toast.makeText(TramTrackerMainActivity.instance,
			// getResources().getString(R.string.error_no_reminders), 0).show();
		}

	}

	private void initUI() {
		// TODO Auto-generated method stub

		lvReminders = (ExpandableListView) findViewById(R.id.lvReminders);
		TextView empty = (TextView) findViewById(R.id.ivEmpty);
		lvReminders.setEmptyView(empty);

		tutorialPoint1 = (View) findViewById(R.id.tutorialPoint1);
		tutorialPoint2 = (View) findViewById(R.id.tutorialPoint2);
	}

	@Override
	public void onClosing(int id, boolean fromUser, boolean containsTouch) {
		// TODO Auto-generated method stub

		Toast.makeText(
				activity,
				"Thank you for completing this tutorial. Get more tutorials in the tram arrival screen, myTRAM and Timetables.",
				1).show();

		tutorialPreferences.setAlarmTutorial(true);
		tutorialPreferences.setDisruptionTutorial(true);
		tutorialPreferences.setSettingsTutorial(true);

	};

	public void createTutorialToolTips() {

		String reminderText = "To delete an alarm, tap on the X button and confirm the action.<br/><br/>PRESS HERE to continue or tap on the X button.";

		reminderTutorialTooltip = new TutorialTooltip();
		// if(remindersAdapter.getTutorialView() == null){
		if (tutorialPointFlag) {
			reminderTutorialTooltip.createTutorialTooltip(tooltipManager,
					tutorialPoint2, this, Gravity.TOP, reminderText);
		} else {
			reminderTutorialTooltip.createTutorialTooltip(tooltipManager,
					tutorialPoint1, this, Gravity.TOP, reminderText);
		}
		// }
	}

	public void closeToolTip() {
		try {
			reminderTutorialTooltip.closeTutorialTooltip(tooltipManager);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		TramTrackerMainActivity.getAppManager().back();
	}

	@Override
	public void onPause() {
		super.onPause();
		closeToolTip();
	}

}
