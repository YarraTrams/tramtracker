package com.yarratrams.tramtracker.ui.util;

import android.app.Activity;
import android.app.ActivityGroup;

public class ScreenDescriptor {
	private int tabID;
	private String screenID;
	private Activity activity;
	private ActivityGroup group;
	
	public ScreenDescriptor() {

	}

	public int getTabID() {
		return this.tabID;
	}

	public void setTabID(int tabID) {
		this.tabID = tabID;
	}

	public ActivityGroup getGroup() {
		return this.group;
	}

	public void setGroup(ActivityGroup group) {
		this.group = group;
	}

	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	public String getScreenID() {
		return screenID;
	}

	public void setScreenID(String screenID) {
		this.screenID = screenID;
	}
	
}
