package com.yarratrams.tramtracker.ui.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class SettingsPreference {
	
	
	//variables
	Context context;
	
	SharedPreferences sharedPreferences;
	SharedPreferences.Editor editor;


	final String PREF_NAME = "settingspref";
	final String RECEIVE_NOTIFICATIONS =	"receivenotifications";
	
	
	
	public SettingsPreference(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
		sharedPreferences  = context.getSharedPreferences(PREF_NAME, Activity.MODE_PRIVATE);
		editor= sharedPreferences.edit();
	}
	
	
	
	public void setReciveNotifications(boolean status)
	{
		editor.putBoolean(RECEIVE_NOTIFICATIONS, status);
		editor.commit();
	}
	
	public boolean getReciveNotifications()
	{
		return sharedPreferences.getBoolean(RECEIVE_NOTIFICATIONS, false);
	}

}
