package com.yarratrams.tramtracker.ui.util;

import java.text.DecimalFormat;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.google.android.maps.GeoPoint;
import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.NearbyTicketOutlet;
import com.yarratrams.tramtracker.ui.OutletActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;


public class MapOutletDialog extends MapDialog{
	private Context context;
	private NearbyTicketOutlet outlet;

	public MapOutletDialog(Context context, NearbyTicketOutlet outlet) {
		super(context);
		this.context = context;
		this.outlet = outlet;
		
		createDialog();
	}
	
	
	private void createDialog(){
		setTicketOutletInfo();
		
		setOnDirectionsClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				TramTrackerMainActivity.getAppManager().callSelection(context.getString(R.string.accessibility_click_goto_directions));
				GeoPoint destination = new GeoPoint(outlet.getTicketOutlet().getLatitudeE6(), outlet.getTicketOutlet().getLongitudeE6());
				TramTrackerMainActivity.getAppManager().requestDirectionsService(destination);
			}
		});
		setOnGoClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				TramTrackerMainActivity.getAppManager().callSelection(context.getString(R.string.accessibility_click_goto_outlet));
				Intent intent = new Intent(context, OutletActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra(OutletActivity.INTENT_KEY, outlet.getTicketOutlet());
				TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_NEARBY, 
						context.getResources().getString(R.string.tag_outlet_screen), intent);
			}
		});
	}
	
	
	private void setTicketOutletInfo(){
		setNameText(outlet.getTicketOutlet().getName());
		setDetailText(getFullAddress());
		setDistanceText(getDistance());
	}

	
	public String getFullAddress(){
		String text = "";
		
		text = outlet.getTicketOutlet().getAddress();

		return text;
	}
	
	
	private String getDistance(){
		String text = "";
		
		if(outlet.getIntDistance() < 1000){
			text = String.valueOf(outlet.getIntDistance());
			text = text.concat(context.getResources().getString(R.string.ticketOutlets_distance_meters));
		} else {
			double distance = (double)outlet.getIntDistance()/1000;
			DecimalFormat df = new DecimalFormat("##.0");
			text = df.format(distance);
			text = text.concat(context.getResources().getString(R.string.ticketOutlets_distance_kilometers));
		}
		
		return text;
	}
	
}