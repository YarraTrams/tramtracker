package com.yarratrams.tramtracker.ui.util;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.google.android.maps.GeoPoint;
import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.Favourite;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.ui.PIDActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;


public class MapFavouriteDialog extends MapDialog{
	private Context context;
	private Favourite favourite;
	private Stop stop;
	private boolean isFilterOn;

	public MapFavouriteDialog(Context context, Favourite favourite) {
		super(context, MapDialog.TYPE_FAVOURITE);
		this.context = context;
		this.favourite = favourite;
		this.stop = favourite.getStop();
		
		createDialog();
	}
	
	
	private void createDialog(){
		setTramStopInfo();
		
		setOnDirectionsClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				TramTrackerMainActivity.getAppManager().callSelection(context.getString(R.string.accessibility_click_goto_directions));
				GeoPoint destination = new GeoPoint(stop.getLatitudeE6(), stop.getLongitudeE6());
				TramTrackerMainActivity.getAppManager().requestDirectionsService(destination);
			}
		});
		setOnGoClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				TramTrackerMainActivity.getAppManager().callSelection(context.getString(R.string.accessibility_click_goto_pid));
				Intent intent = new Intent(context, PIDActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra(PIDActivity.INTENT_KEY, stop);
				TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_FAVOURITES, 
						context.getResources().getString(R.string.tag_pid_screen), intent);
			}
		});
		
	}
	
	
	private void setTramStopInfo(){
		setNameText(getStopDescription());
		setDetailText(getRoutesDescription());
		
		
			setDistanceText(getFavouriteName());
		
		
		
		setLowFloor(isLowFloor());
		
		setFreeTramZoneLogo(stop);
	}
	
	private String getStopDescription(){
		String text = "";
		
		text = text.concat(String.valueOf(stop.getStopNumber()));
		//phoenix changed 20.Jun.2014
		text = text.concat(": ");
//		text = text.concat(context.getResources().getString(R.string.stop_name_colon));
		text = text.concat(stop.getStopName());
		
		return text;
	}
	
	private String getRoutesDescription(){
		String text = "";
		String[] array = getRoutesList();
		
		if(isFilterOn){
			text = context.getResources().getString(R.string.stop_direction_showing);
		} else {
			text = context.getResources().getString(R.string.stop_direction_routes);
		}
		for(int i = 0; i < array.length; i++){
			text = text.concat(array[i]);
			if(i < array.length-1){
				text = text.concat(context.getResources().getString(R.string.stop_routes_coma));
			}
		}
		text = text.concat(context.getResources().getString(R.string.stop_name_space));
		text = text.concat(stop.getCityDirection());
		
		return text;
	}
	
	private String[] getRoutesList(){
		ArrayList<String> list = new ArrayList<String>();
		
		for(String option: favourite.getAlRoutes()){
			if(option.equalsIgnoreCase(context.getResources().getString(R.string.route_filter_dbtag_all))){
				isFilterOn = false;
				return stop.getRoutes();
			} else if(!option.equalsIgnoreCase(context.getResources().getString(R.string.route_filter_dbtag_lowfloor))) {
				isFilterOn = true;
				list.add(option);
			}
		}
		return list.toArray(new String[0]);
	}
	
	private String getFavouriteName(){
		String text = "";

		text = text.concat(context.getResources().getString(R.string.favourites_map_dialog_name));
		text = text.concat(favourite.getName());
		
		return text;
	}
	
	private boolean isLowFloor(){
		if(favourite.getAlRoutes() != null){
			for(String option: favourite.getAlRoutes()){
				if(option.equalsIgnoreCase(context.getResources().getString(R.string.route_filter_dbtag_lowfloor))){
					return true;
				}
			}
		}
		return false;
	}

}