package com.yarratrams.tramtracker.ui.util;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.drawable.Drawable;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;
import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.PredictedArrivalTime;
import com.yarratrams.tramtracker.objects.Stop;


public class MapMyTramCurrentOverlay extends ItemizedOverlay<OverlayItem> {
	private ArrayList<OverlayItem> overlays = new ArrayList<OverlayItem>();
	private final Activity context;
	final MapController mc;
	
	private ArrayList<PredictedArrivalTime> stops;
	private MapMyTramDialog dialog;
	
	
	private static Drawable getDefaultMarker(Activity context){
		return context.getResources().getDrawable(R.drawable.icn_search_stop_red);
	}
	
	public MapMyTramCurrentOverlay(Drawable defaultMarker, Activity context, MapView mapView) {
		super(boundCenterBottom(defaultMarker));
		this.context = context;
		mc = mapView.getController();
		
		stops = new ArrayList<PredictedArrivalTime>();
	}

	public MapMyTramCurrentOverlay(Activity context, MapView mapView) {
		super(boundCenterBottom(getDefaultMarker(context)));
		this.context = context;
		mc = mapView.getController();
		
		stops = new ArrayList<PredictedArrivalTime>();
	}
	
	public void updateStopsList(ArrayList<PredictedArrivalTime> stops) {
		this.stops.clear();
		this.stops.addAll(stops);
		
		resetOverlays();
		if(dialog != null){
			dialog.dismiss();
		}
		
		for(PredictedArrivalTime routeStop : stops){
			Stop stop = routeStop.getStop();
	        GeoPoint point = new GeoPoint(stop.getLatitudeE6(), stop.getLongitudeE6());
	        OverlayItem overlayitem = new OverlayItem(point, null, null);
	        addOverlay(overlayitem);
		}
	}
	
	
	
	public void addOverlay(OverlayItem overlay) {
		overlays.add(overlay);
		populate();
	}
	
	public void resetOverlays(){
		overlays.clear();
		populate();
	}

	@Override
	protected OverlayItem createItem(int i) {
		return overlays.get(i);
	}

	@Override
	public int size() {
		return overlays.size();
	}
	
	@Override
	protected boolean onTap(int index) {
		mc.animateTo(createItem(index).getPoint());
		PredictedArrivalTime stop = stops.get(index);
		if(index == 0){
			dialog = new MapMyTramDialog(context, stop, stops, MapDialog.TYPE_ALARM, true);
		} else {
			dialog = new MapMyTramDialog(context, stop, stops, MapDialog.TYPE_ALARM, false);
		}
		
		dialog.show();
		return true;
	}
	
	
}
