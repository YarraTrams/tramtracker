package com.yarratrams.tramtracker.ui.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class FeaturesPreferences {
	
	

	Context context;
	SharedPreferences sharedPreferences;
	SharedPreferences.Editor editor;
	
	
	private static final String PREF_NAME = "featurepref";
	private static final String ADS =	"ads";
	
	
	
	
	
	public FeaturesPreferences(Context  context) {
		// TODO Auto-generated constructor stub
		this.context = context;
		
		sharedPreferences = context.getApplicationContext().getSharedPreferences(PREF_NAME, Activity.MODE_PRIVATE);
		editor = sharedPreferences.edit();
	}
	
	
	
	public void setAdsflag(boolean flag)
	{
		editor.putBoolean(ADS, flag);
		editor.commit();
	}


	public boolean getAdsflag()
	{
		// by default it should return true
		return sharedPreferences.getBoolean(ADS, false);

	}
	
	
	
	
	
	public static boolean getAdsFlag(Context context)
	{
		
		
		SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Activity.MODE_PRIVATE);
		return sharedPreferences.getBoolean(ADS, false);
	}


}
