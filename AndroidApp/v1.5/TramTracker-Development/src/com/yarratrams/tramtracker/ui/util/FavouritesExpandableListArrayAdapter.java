package com.yarratrams.tramtracker.ui.util;

import java.util.ArrayList;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.Favourite;
import com.yarratrams.tramtracker.objects.FavouritesGroup;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.ui.FavouritesActivity;
import com.yarratrams.tramtracker.ui.PIDActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;


public class FavouritesExpandableListArrayAdapter extends BaseExpandableListAdapter {
	private FavouritesActivity context;
	private ArrayList<FavouritesGroup> groups;
	private boolean isFilterOn;
	
	public FavouritesExpandableListArrayAdapter(FavouritesActivity context, ArrayList<FavouritesGroup> groups) {
		super();
		this.context = context;
		this.groups = groups;
	}

	public void updateList(ArrayList<FavouritesGroup> groups) {
		this.groups.clear();
		this.groups.addAll(groups);
		super.notifyDataSetChanged();
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return getFavourite(groupPosition, childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return (groupPosition*100) + childPosition;
	}

	static class FavouriteViewHolder{
		TextView favouriteName;
		TextView stopName;
		TextView route;
		ImageView lowFloor;
		ImageView ivFavFreeTramZone;
	}
	
	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
		FavouriteViewHolder viewHolder;
		
		if(convertView == null){
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(R.layout.favourites_list_view_child, parent, false);
			
			viewHolder = new FavouriteViewHolder();
			viewHolder.favouriteName = (TextView)convertView.findViewById(R.id.favourite_name);
			viewHolder.stopName = (TextView)convertView.findViewById(R.id.stop_name);
			viewHolder.route = (TextView)convertView.findViewById(R.id.stop_routes);
			viewHolder.lowFloor = (ImageView)convertView.findViewById(R.id.favourite_lowfloor);
			viewHolder.ivFavFreeTramZone = (ImageView)convertView.findViewById(R.id.ivFavFreeTramZone);
			
			convertView.setTag(viewHolder);
			
		} else {
			viewHolder = (FavouriteViewHolder) convertView.getTag();
		}
		
		final Favourite favourite = (Favourite) getChild(groupPosition, childPosition);
		viewHolder.favouriteName.setText(favourite.getName());
		viewHolder.stopName.setText(getStopDescription(favourite.getStop()));
		viewHolder.route.setText(getRoutesDescription(favourite));
		
		if(isLowFloor(favourite)){
			viewHolder.lowFloor.setVisibility(View.VISIBLE);
		} else {
			viewHolder.lowFloor.setVisibility(View.INVISIBLE);
		}
		
		if(favourite.getStop().IsInFreeZone())
		{
			viewHolder.ivFavFreeTramZone.setVisibility(View.VISIBLE);
		}
		else
		{
			viewHolder.ivFavFreeTramZone.setVisibility(View.INVISIBLE);
		}
		
		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				TramTrackerMainActivity.getAppManager().callSelection(getStopDescription(favourite.getStop()));
				goToPIDScreen(favourite);
			}
		});
		
		convertView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				v.setPressed(true);
				
				return false;
			}
		});
		
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		FavouritesGroup group = getFavouriteGroup(groupPosition);
		return group.getFavourites().size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return getFavouriteGroup(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return groups.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	
	static class SuburbViewHolder{
		TextView suburbName;
	}
	
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		SuburbViewHolder viewHolder;
		
		if(convertView == null){
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(R.layout.favourites_list_view_group, parent, false);
			
			viewHolder = new SuburbViewHolder();
			viewHolder.suburbName = (TextView) convertView.findViewById(R.id.group_name);
			convertView.setTag(viewHolder);
		
		} else {
			viewHolder = (SuburbViewHolder) convertView.getTag();
		}
		
		FavouritesGroup group = (FavouritesGroup) getGroup(groupPosition);
		viewHolder.suburbName.setText(group.getName());
		
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

    @Override
    public boolean areAllItemsEnabled(){
        return true;
    }
	
    
	private void goToPIDScreen(Favourite favourite){
		Intent intent = new Intent(this.context, PIDActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(PIDActivity.INTENT_KEY, favourite.getStop());
		
		TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_FAVOURITES, 
				context.getResources().getString(R.string.tag_pid_screen), intent);
	}

	private FavouritesGroup getFavouriteGroup(int groupPosition){
		return groups.get(groupPosition);
	}
	
	private Favourite getFavourite(int groupPosition, int childPosition){
		FavouritesGroup group = getFavouriteGroup(groupPosition);
		return group.getFavourites().get(childPosition);
	}
	
	
	private String getStopDescription(Stop stop){
		String text = "";
		
		text = text.concat(String.valueOf(stop.getStopNumber()));
		//phoenix changed 20.Jun.2014
		text = text.concat(": ");
//		text = text.concat(context.getResources().getString(R.string.stop_name_colon));
		text = text.concat(stop.getStopName());
		
		return text;
	}
	
	
	private String getRoutesDescription(Favourite favourite){
		String text = "";
		String[] array = new String[0];
		
		if(favourite.getAlRoutes() == null){
			array = favourite.getStop().getRoutes();
		} else {
			array = getRoutesList(favourite);
		}
		
		if(isFilterOn){
			text = context.getResources().getString(R.string.stop_direction_showing);
		} else {
			text = context.getResources().getString(R.string.stop_direction_routes);
		}
		for(int i = 0; i < array.length; i++){
			text = text.concat(array[i]);
			if(i < array.length-1){
				text = text.concat(context.getResources().getString(R.string.stop_routes_coma));
			}
		}
		text = text.concat(context.getResources().getString(R.string.stop_name_space));
		text = text.concat(favourite.getStop().getCityDirection());
		
		return text;
	}
	
	private String[] getRoutesList(Favourite favourite){
		ArrayList<String> list = new ArrayList<String>();
		
		for(String option: favourite.getAlRoutes()){
			if(option.equalsIgnoreCase(context.getResources().getString(R.string.route_filter_dbtag_all))){
				isFilterOn = false;
				return favourite.getStop().getRoutes();
			} else if(!option.equalsIgnoreCase(context.getResources().getString(R.string.route_filter_dbtag_lowfloor))) {
				isFilterOn = true;
				list.add(option);
			}
		}
		return list.toArray(new String[0]);
	}
	
	private boolean isLowFloor(Favourite favourite){
		if(favourite.getAlRoutes() != null){
			for(String option: favourite.getAlRoutes()){
				if(option.equalsIgnoreCase(context.getResources().getString(R.string.route_filter_dbtag_lowfloor))){
					return true;
				}
			}
		}
		return false;
	}

}
