package com.yarratrams.tramtracker.ui;

import java.util.Date;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.otherlevels.android.sdk.OlAndroidLibrary;
import com.otherlevels.android.sdk.Options;
import com.yarratrams.tramtracker.backgroundservice.AlaramReceiver;
import com.yarratrams.tramtracker.objects.Constants;

public class MainApplication extends Application {


	/*
	 * Adil added
	 */

	//for background service and alarm manager

	AlarmManager alarmManager;
	Intent intentTooBroadcastRec;
	PendingIntent pendingIntentBroadcastRec;

	/*
	 * Ends
	 */

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();

		//initilazing the ads sdk
		initOtherLevelAds();

		//start service
		startAlarm();
	}



	public void initOtherLevelAds()
	{
//		if(android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)
//		{	


			Options options = new Options(); 
			options.appKey = Constants.OTHER_LEVEL_API_KEY; // mandatory
//			options.trackingId = "@OL@c08fb4b34861a5a87fa4c609c749";
			//options.trackingId = "@OL@3c00502a418187d8170bbfeb24f0";
			OlAndroidLibrary.init(this, options);


//		}



	}


	/*
	 * adil added: for starting backgorund service as alarm manager
	 */

	public void startAlarm()
	{

		System.out.println("-- staring service.");
		// get alarm params
		Date d = new Date();
		long timeTilMinuteChange = 60*1000-d.getSeconds()*1000;
		long startTime = System.currentTimeMillis() + timeTilMinuteChange;


		alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

		intentTooBroadcastRec = new Intent(this, AlaramReceiver.class);

		intentTooBroadcastRec.putExtra(AlaramReceiver.ACTION_ALARM, AlaramReceiver.ACTION_ALARM);

		pendingIntentBroadcastRec = PendingIntent.getBroadcast(this, 99150, intentTooBroadcastRec, PendingIntent.FLAG_UPDATE_CURRENT);

		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP	, startTime, Constants.SERVICE_REPEAT_INTERVAL, pendingIntentBroadcastRec);



	}

	/*
	 * ends
	 */
}
