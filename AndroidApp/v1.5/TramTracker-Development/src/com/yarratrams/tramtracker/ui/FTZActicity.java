package com.yarratrams.tramtracker.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.os.Bundle;
import android.text.Html;
import android.text.Html.ImageGetter;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.ui.util.FeaturesPreferences;
import com.yarratrams.tramtracker.ui.util.OtherLevelAds;

public class FTZActicity extends Activity implements ImageGetter {

	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_MORE;
	TextView tvFreeTramDialogText;
	public ImageButton bFreeTramDialogContinue;
	ImageView ivFtzYaraTram;

	String freeTramText = "";
	Activity activity;




	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ftz_screen);
		activity =  this;
		createDialog();

		
		//call ads
		OtherLevelAds.showAds(activity, R.id.rich_banner_fragment99,FeaturesPreferences.getAdsFlag(TramTrackerMainActivity.instance));


	}



	private void createDialog() {
		// TODO Auto-generated method stub

		initUI();

		setInfoText();

		bFreeTramDialogContinue.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//				dismiss();
			}
		});


	}

	private void setInfoText() {
		// TODO Auto-generated method stub



		freeTramText  = "Tram travel within the CBD and Docklands is free. If your entire trip is within the Free Tram Zone you do not need to touch on with your myki, but if your journey is entirely within the paid zone or begins or ends outside the CBD/Docklands free zone you will need to touch on as normal. Stops in the Free Tram Zone are highlighted on tramTRACKER with <br/><br/> <img src = 'ftz_more_logo_big.png' alt=\"Free Tram Zone\"></a><br/><br/>"+

						"Free travel in the CBD only applies to trams. Bus and train passengers need to touch on within this area. For more information visit  <b><a href=\"http://ptv.vic.gov.au\">ptv.vic.gov.au</a></b>.";



		//		freeTramText = "From 1 January 2015 tram travel within the CBD and Docklands is free. If your entire trip is within the Free Tram Zone you do not need to touch on. " + 
		//				"If your journey begins or ends outside the zone you need to touch on as normal. Stops in the Free Tram Zone are highlighted on tramTRACKER with <br/><br/> <a href=\"http://ptv.vic.gov.au\"><img src = 'icn_more_freetram_dialog.png'></a><br/><br/>, and are shown on maps as a shaded area." + 
		//				"<br/> <br/>Free travel in the CBD only applies to trams. Bus and train passengers need to touch on within this area. " + 
		//				"For more information visit <b><a href=\"http://ptv.vic.gov.au\">Public Transport Victoria </a></b> or call <b>1800  800 007</b>.";

		Spanned spanned = Html.fromHtml(freeTramText, this, null);
		tvFreeTramDialogText.setText(spanned);
		tvFreeTramDialogText.setMovementMethod(LinkMovementMethod.getInstance());
		tvFreeTramDialogText.setLinkTextColor(Color.BLACK);
		

		//		tvFreeTramDialogText.setText(Html.fromHtml(freeTramText));

	}

	private void initUI() {
		tvFreeTramDialogText = (TextView) findViewById(R.id.tvFreeTramDialogText);
		bFreeTramDialogContinue = (ImageButton) findViewById(R.id.bFreeTramDialogContinue);
		bFreeTramDialogContinue.setVisibility(View.GONE);
		ivFtzYaraTram = (ImageView) findViewById(R.id.ivFtzYaraTram);
		ivFtzYaraTram.setVisibility(View.INVISIBLE);

	}

	@Override
	public Drawable getDrawable(String source) {
		// TODO Auto-generated method stub
		int id = 0;

		if(source.equals("ftz_more_logo_big.png")){
			id = R.drawable.ftz_more_logo_big;
		}


		LevelListDrawable d = new LevelListDrawable();
		Drawable freetram = activity.getResources().getDrawable(id);
		d.addLevel(0, 0, freetram);
		d.setBounds(0, 0, freetram.getIntrinsicWidth(), freetram.getIntrinsicHeight());
		


		return d;
	}



 

	public Context getDialogContext() {
		Context context;
		if (getParent() != null) {
			context = getParent();
		} else {
			context = this;
		}
		return context;
	}


	@Override
	public void onResume() {
		super.onResume();

	}
	@Override
	public void onPause() {
		super.onPause();
	}




	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

}
