package com.yarratrams.tramtracker.ui.util;

/*
 * Free Tram  Zone Dialog added by Adil
 * To be displayed when user clicks on Free Tram Zone in More Screen.
 */

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.text.Html;
import android.text.Html.ImageGetter;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import com.yarratrams.tramtracker.R;

public class FreeTramZoneDialog extends Dialog implements DialogInterface.OnKeyListener,ImageGetter {

	//UI Elements
	TextView tvFreeTramDialogText;
	public ImageButton bFreeTramDialogContinue;

	//Global Variables
	Activity activity;
	String freeTramText = "";

	public FreeTramZoneDialog(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		activity = (Activity) context;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.more_free_tram_dialog);
		setCanceledOnTouchOutside(false);

		createDialog();
	}

	private void createDialog() {
		// TODO Auto-generated method stub

		initUI();

		setInfoText();

		bFreeTramDialogContinue.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dismiss();
			}
		});


	}

	private void setInfoText() {
		// TODO Auto-generated method stub



		freeTramText  = "Tram travel within the CBD and Docklands is free. If your entire trip is within the Free Tram Zone you do not need to touch on with your myki, but if your journey is entirely within the paid zone or begins or ends outside the CBD/Docklands free zone you will need to touch on as normal. Stops in the Free Tram Zone are highlighted on tramTRACKER with <br/><br/><img src = 'ftz_more_logo_big.png'></a><br/><br/>"+

						"Free travel in the CBD only applies to trams. Bus and train passengers need to touch on within this area. For more information visit  <b><a href=\"http://ptv.vic.gov.au\">ptv.vic.gov.au</a></b>.";



		//		freeTramText = "From 1 January 2015 tram travel within the CBD and Docklands is free. If your entire trip is within the Free Tram Zone you do not need to touch on. " + 
		//				"If your journey begins or ends outside the zone you need to touch on as normal. Stops in the Free Tram Zone are highlighted on tramTRACKER with <br/><br/> <a href=\"http://ptv.vic.gov.au\"><img src = 'icn_more_freetram_dialog.png'></a><br/><br/>, and are shown on maps as a shaded area." + 
		//				"<br/> <br/>Free travel in the CBD only applies to trams. Bus and train passengers need to touch on within this area. " + 
		//				"For more information visit <b><a href=\"http://ptv.vic.gov.au\">Public Transport Victoria </a></b> or call <b>1800  800 007</b>.";

		Spanned spanned = Html.fromHtml(freeTramText, this, null);
		tvFreeTramDialogText.setText(spanned);
		tvFreeTramDialogText.setMovementMethod(LinkMovementMethod.getInstance());
		tvFreeTramDialogText.setLinkTextColor(Color.BLACK);

		//		tvFreeTramDialogText.setText(Html.fromHtml(freeTramText));

	}

	private void initUI() {
		tvFreeTramDialogText = (TextView) findViewById(R.id.tvFreeTramDialogText);
		bFreeTramDialogContinue = (ImageButton) findViewById(R.id.bFreeTramDialogContinue);

	}

	@Override
	public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Drawable getDrawable(String source) {
		// TODO Auto-generated method stub
		int id = 0;

		if(source.equals("ftz_more_logo_big.png")){
			id = R.drawable.ftz_more_logo_big;
		}


		LevelListDrawable d = new LevelListDrawable();
		Drawable freetram = activity.getResources().getDrawable(id);
		d.addLevel(0, 0, freetram);
		d.setBounds(0, 0, freetram.getIntrinsicWidth(), freetram.getIntrinsicHeight());


		return d;
	}

}
