package com.yarratrams.tramtracker.ui;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.db.FavouriteManager;
import com.yarratrams.tramtracker.objects.Favourite;
import com.yarratrams.tramtracker.objects.FavouritesGroup;
import com.yarratrams.tramtracker.objects.Route;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.ui.util.FeaturesPreferences;
import com.yarratrams.tramtracker.ui.util.OtherLevelAds;
import com.yarratrams.tramtracker.ui.util.RoutesFilterAdapter;


public class AddFavouriteActivity extends Activity implements OnClickListener, OnItemSelectedListener{
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_FAVOURITES;
	public static final String INTENT_KEY_STOP = "stop_info";
	public static final String INTENT_KEY_EDIT_FAVOURITE = "favourite_info";
	public static final String INTENT_KEY_EDIT_GROUP = "group_info";
	public static final String INTENT_KEY_NEW_GROUP = "new_group";
	public static final String INTENT_KEY_GROUP_SIZE = "group_size";
	public static final String INTENT_KEY_GROUP_INDEX = "group_index";

	private FavouriteManager favouriteManager;

	private boolean isEditing;
	private ArrayList<FavouritesGroup> groups;
	private Favourite favourite;
	private FavouritesGroup group;
	private Stop stop; 
	private boolean newGroup;
	private int groupSize;
	private int groupIndex;

	private ArrayList<Route> routes;
	private ArrayList<String> selected;
	private RoutesFilterAdapter routeFilterAdapter;

	private InputMethodManager imm;


	//Adil added
	RelativeLayout RelativeLayout1;
	FrameLayout rich_banner_fragment10010;



	//	public void addAdsFragment()
	//	{
	//		FragmentManager fragmentManager = getFragmentManager();
	//		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
	//
	//		//add the fragment to a parent view container
	//		RichCardBannerFragment richBannerFragment = new RichCardBannerFragment();
	//		fragmentTransaction.add(R.id.parent_view_container, richBannerFragment);
	//		fragmentTransaction.commit();
	//	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}





	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/*
		 * Adil Changed
		 */
		View viewToLoad = LayoutInflater.from(this.getParent()).inflate(R.layout.favourites_add_screen, null);
		setContentView(viewToLoad);


		//adil added
		RelativeLayout1 = (RelativeLayout) findViewById(R.id.RelativeLayout1);
		rich_banner_fragment10010 = (FrameLayout) findViewById(R.id.rich_banner_fragment10010);


		//call ads here
		//addAdsFragment();
		OtherLevelAds.showAds(this, R.id.rich_banner_fragment10010,FeaturesPreferences.getAdsFlag(TramTrackerMainActivity.instance));


		RelativeLayout1.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {
				Rect r = new Rect();
				RelativeLayout1.getWindowVisibleDisplayFrame(r);
				int heightDiff = RelativeLayout1.getRootView().getHeight() - (r.bottom - r.top);

				if (heightDiff > 100) { // if more than 100 pixels, its probably a keyboard...
					//ok now we know the keyboard is up...
					rich_banner_fragment10010.setVisibility(View.INVISIBLE);
					// view_two.setVisibility(View.GONE);

				}else{
					//ok now we know the keyboard is down...
					rich_banner_fragment10010.setVisibility(View.VISIBLE);
					//   view_two.setVisibility(View.VISIBLE);

				}
			}
		});





		/*
		 * Ends
		 */

		favouriteManager = new FavouriteManager(getApplicationContext());

		isEditing = false;
		favourite = new Favourite();
		groups = new ArrayList<FavouritesGroup>();

		selected = new ArrayList<String>();

		Button saveButton = (Button)findViewById(R.id.save_button);
		saveButton.setOnClickListener(this);
		Button newGroupButton = (Button)findViewById(R.id.newfavourite_group_button);
		newGroupButton.setOnClickListener(this);

		TextView name = (TextView)findViewById(R.id.newfavourite_name_textbox);
		name.setOnEditorActionListener(new OnEditorActionListener() {        
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if(actionId==EditorInfo.IME_ACTION_DONE || event.getKeyCode() == KeyEvent.KEYCODE_ENTER){
					save();
				}
				return false;
			}
		});
	}



	@Override
	public void onClick(View v) {
		Button saveButton = (Button)findViewById(R.id.save_button);
		Button newGroupButton = (Button)findViewById(R.id.newfavourite_group_button);
		Spinner groupSelector = (Spinner) findViewById(R.id.newfavourite_group_combobox);
		if(v == saveButton){
			save();

		} else if(v == newGroupButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_favourites_newgroup));
			Intent intent = new Intent(getDialogContext(), AddFavouriteGroupActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_FAVOURITES, getResources().getString(R.string.tag_addfavouritegroup_screen), intent);
			Intent screenIntent = getIntent();
			screenIntent.putExtra(AddFavouriteActivity.INTENT_KEY_NEW_GROUP, true);
			screenIntent.putExtra(AddFavouriteActivity.INTENT_KEY_GROUP_SIZE, groups.size());
			screenIntent.putExtra(AddFavouriteActivity.INTENT_KEY_GROUP_INDEX, groupSelector.getSelectedItemPosition());
		}
	}

	private void save(){
		Spinner groupSelector = (Spinner) findViewById(R.id.newfavourite_group_combobox);
		TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_header_save));
		TextView name = (TextView)findViewById(R.id.newfavourite_name_textbox);
		//		if(name.getText().length() < 1){
		//			TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.error_favourite_noname));
		//			return;
		//		}

		if(groupSelector.getSelectedItemPosition() < 0){
			if(groupSelector.getChildCount() < 1){
				TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.error_favourite_nogroup));
				return;
			}
			groupSelector.setSelection(0);
		}

		favourite.setName(name.getText().toString());

		ArrayList<String> list = routeFilterAdapter.getSelected();
		if(list !=  null ){
			if(validateSelected(list)){
				selected = list;
				favourite.setAlRoutes(selected);
			} else {
				TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.error_routes_filter));
				return;
			}
		}

		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(name.getWindowToken(), 0);

		if(isEditing){
			try{
				group = groups.get(groupSelector.getSelectedItemPosition());
				favouriteManager.editFavourite(group, favourite);
				TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.notify_favourite_saved));
				TramTrackerMainActivity.getAppManager().back();
			}catch (SQLiteConstraintException e) {
				TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.error_favourite_edit_duplication));
			}
		} else {
			favourite.setStop(stop);
			group = groups.get(groupSelector.getSelectedItemPosition());				
			favourite.setOrder(group.getFavourites().size());
			try{
				favouriteManager.addFavourite(group, favourite);
				TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.notify_favourite_saved));
				TramTrackerMainActivity.getAppManager().back();
			}catch (SQLiteConstraintException e) {
				TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.error_favourite_create_duplication));
			}
		}
	}

	private boolean validateSelected(ArrayList<String> list){
		if(list != null){
			for(String option : list){
				if(!option.equalsIgnoreCase(getResources().getString(R.string.route_filter_dbtag_lowfloor))){
					return true;
				}
			}
		}
		return false;
	}

	@Override
	protected void onResume() {
		super.onResume();

		imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		EditText nameField = (EditText)findViewById(R.id.newfavourite_name_textbox);
		nameField.clearFocus();

		Bundle extras = getIntent().getExtras(); 
		if(extras != null){
			favourite = (Favourite)extras.getParcelable(INTENT_KEY_EDIT_FAVOURITE);
			group = (FavouritesGroup)extras.getParcelable(INTENT_KEY_EDIT_GROUP);
			newGroup = extras.getBoolean(INTENT_KEY_NEW_GROUP);
			groupSize = extras.getInt(INTENT_KEY_GROUP_SIZE);
			groupIndex = extras.getInt(INTENT_KEY_GROUP_INDEX);
			if(favourite != null){
				stop = favourite.getStop();
				nameField.setText(favourite.getName());
				isEditing = true;

			} else {
				favourite = new Favourite();
				group = new FavouritesGroup();
				stop = extras.getParcelable(INTENT_KEY_STOP);
				isEditing = false;
			}
		}

		updateUI();
	}

	@Override
	protected void onPause() {
		EditText nameField = (EditText)findViewById(R.id.newfavourite_name_textbox);
		imm.hideSoftInputFromWindow(nameField.getWindowToken(), 0);
		super.onPause();



	}

	public void updateUI(){
		TextView stopName = (TextView) findViewById(R.id.favourite_stop_name);
		TextView stopID = (TextView) findViewById(R.id.favourite_stop_trackerid_number);
		stopName.setText(getStopDescription());
		stopID.setText(String.valueOf(stop.getTrackerID()));

		ArrayList<String> list = getListOfFavouriteGroups();
		Spinner groupSelector = (Spinner) findViewById(R.id.newfavourite_group_combobox);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
		adapter.setDropDownViewResource(R.layout.spinner_list_view_detail);
		groupSelector.setAdapter(adapter);
		groupSelector.setOnItemSelectedListener(this);

		routes = favouriteManager.getRoutesForStop(stop);
		if(isEditing){
			selected = favourite.getAlRoutes();
			if(selected == null){
				selected = new ArrayList<String>();
				initSelected();
			}
		} else {
			initSelected();
		}
		if(newGroup){
			if(groupSize < groups.size()){
				groupSelector.setSelection(groupSelector.getCount()-1);
			} else {
				groupSelector.setSelection(groupIndex);
			}
			Intent screenIntent = getIntent();
			screenIntent.removeExtra(AddFavouriteActivity.INTENT_KEY_NEW_GROUP);
		} else if(isEditing){
			groupSelector.setSelection(group.getOrder());
		}
		LinearLayout routeList = (LinearLayout)findViewById(R.id.routes_filter_list);
		routeFilterAdapter = new RoutesFilterAdapter(routeList, this, routes, selected);
	}

	private void initSelected(){
		selected = new ArrayList<String>();
		selected.add(getResources().getString(R.string.route_filter_dbtag_all));
	}


	private String getStopDescription(){
		String text = "";

		text = text.concat(String.valueOf(stop.getStopNumber()));
		text = text.concat(getResources().getString(R.string.stop_name_colon));
		text = text.concat(stop.getStopName());

		return text;
	}

	private ArrayList<String> getListOfFavouriteGroups(){
		ArrayList<String> list = new ArrayList<String>();

		groups = favouriteManager.getAllFavourites();
		if(groups != null){
			for(FavouritesGroup group : groups){
				list.add(group.getName());
			}
		}

		return list;
	}

	private Context getDialogContext() {
		Context context;
		if (getParent() != null){
			context = getParent();
		} else {
			context = this;
		}
		return context;
	}

	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return true;
	}



	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
	}


}
