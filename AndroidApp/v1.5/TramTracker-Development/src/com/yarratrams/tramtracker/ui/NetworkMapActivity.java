package com.yarratrams.tramtracker.ui;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.NetworkMap;
import com.yarratrams.tramtracker.ui.util.FeaturesPreferences;
import com.yarratrams.tramtracker.ui.util.NetworkMapPreferences;
import com.yarratrams.tramtracker.ui.util.OtherLevelAds;


public class NetworkMapActivity extends Activity {
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_ROUTES;

	private WebView webView;
	private ProgressBar progessBar;
	Activity activity;
	ArrayList<NetworkMap> arrlstNetworkMaps;
	NetworkMapPreferences networkMapPreferences;

	NetworkMap selectedNetworkMap;
	Handler handler;
	int mapIndex = 0;
	public static String bundleMap ="network_map_201501010000.png";

	boolean showDefaultMap = true;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.networkmap_screen);
		activity = this;
		progessBar = (ProgressBar) findViewById(R.id.ProgressBar);

		networkMapPreferences = new NetworkMapPreferences(activity);
		webView = (WebView) findViewById(R.id.web_view);
		startWebView();



		handler = new Handler()
		{
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				super.handleMessage(msg);


				/*
				 * Change the name of bundled map from below url.
				 */
				webView.loadDataWithBaseURL("file:///android_asset/", "<img src='file:///android_asset/network_map_201501010000.png' />", "text/html", "utf-8", null);
			}
		};


		try {
			arrlstNetworkMaps  = networkMapPreferences.getNetworkMapList();
			LoadWebView loadWebView = new LoadWebView();
			loadWebView.execute();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			arrlstNetworkMaps = null;
			handler.sendEmptyMessage(0);
			e.printStackTrace();
		}






		//call ads
		OtherLevelAds.showAds(activity, R.id.rich_banner_fragment1097,FeaturesPreferences.getAdsFlag(TramTrackerMainActivity.instance));





	}


	public void startWebView() {



		WebSettings webSettings = webView.getSettings();
		//
		webSettings.setUserAgentString(webSettings.getUserAgentString());
		webSettings.setJavaScriptEnabled(true);
		webSettings.setSupportZoom(true);
		webSettings.setBuiltInZoomControls(true);
		webSettings.setLoadsImagesAutomatically(true);
		webSettings.setBlockNetworkImage(false);

		/*
		 * FREE TRAM ZONE
		 */
		webSettings.setLoadWithOverviewMode(true);
		webSettings.setUseWideViewPort(true);

		webView.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				//				progessBar.setVisibility(View.VISIBLE);
				super.onPageStarted(view, url, favicon);
			}
			@Override
			public void onPageFinished(WebView view, String url) {
				//				progessBar.setVisibility(View.GONE);
			}
		});

	}




	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}



	@SuppressLint("NewApi")
	public void displayBitmapInWebview()
	{

		//Load bundled map default



		try {
			selectedNetworkMap = arrlstNetworkMaps.get(mapIndex);
			Log.v("TT","1 selected map: "+selectedNetworkMap.getUrl());

			if(selectedNetworkMap!=null)
			{
				if(selectedNetworkMap.getIsMapAvailable()!=1)
				{
					handler.sendEmptyMessage(0);
				}
				else if (selectedNetworkMap.getFileName().equalsIgnoreCase(bundleMap))
				{
					handler.sendEmptyMessage(0);
				}
				else
				{
					String html="<html><body><img src='{IMAGE_URL}' /></body></html>";
					Bitmap bitmap = loadBitmap(activity, selectedNetworkMap.getFileName());

					// Convert bitmap to Base64 encoded image for web
					ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
					bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
					byte[] byteArray = byteArrayOutputStream.toByteArray();
					String imgageBase64 = Base64.encodeToString(byteArray, Base64.DEFAULT);
					String image = "data:image/png;base64," + imgageBase64;

					// Use image for the img src parameter in your html and load to webview
					html = html.replace("{IMAGE_URL}", image);
					final String finalHTML = html;
					webView.post(new Runnable(){

						@Override
						public void run() {
							// TODO Auto-generated method stub
							webView.loadDataWithBaseURL("file:///android_asset/", finalHTML, "text/html", "utf-8", "");
						}
						
					});
					
				}

			}
			else
			{
				handler.sendEmptyMessage(0);
			}
		}
		catch(IndexOutOfBoundsException e)
		{
			handler.sendEmptyMessage(0);
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}


	public  Bitmap loadBitmap(Context context, String picName){ 
		Bitmap b = null; 
		FileInputStream fis; 
		try { 
			fis = context.openFileInput(picName); 
			b = BitmapFactory.decodeStream(fis); 
			fis.close(); 

		}  
		catch (FileNotFoundException e) { 
			Log.d("TT", "file not found"); 
			selectedNetworkMap.setIsMapAvailable(0);
			handler.sendEmptyMessage(0);
			e.printStackTrace(); 
		}  
		catch (IOException e) { 
			Log.d("TT", "io exception"); 
			e.printStackTrace(); 
		}  
		return b; 
	} 


	class LoadWebView extends AsyncTask<String, String, Boolean>
	{

		Date currDate;
		Date mapDate;
		long dateDiff = 0;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progessBar.setVisibility(View.VISIBLE);


			try {

				currDate = new Date(System.currentTimeMillis());
				mapDate =  new Date(Long.parseLong(arrlstNetworkMaps.get(0).getActivateDate()));
				dateDiff =currDate.getTime() - mapDate.getTime();
				mapIndex=0;
				for(int i = 0; i < arrlstNetworkMaps.size() ;i++)

				{
					mapDate =  new Date(Long.parseLong(arrlstNetworkMaps.get(i).getActivateDate()));
				
					if((currDate.getTime() - mapDate.getTime() < 0))
					{
						arrlstNetworkMaps.remove(i);
						i--;
						
					}
					




				}
			


				mapDate =  new Date(Long.parseLong(arrlstNetworkMaps.get(0).getActivateDate()));
				
				mapIndex = 0;


				long minDiff = currDate.getTime() - mapDate.getTime();                                                           
				long curDiff = currDate.getTime() - mapDate.getTime();


				for(int i = 1 ; i < arrlstNetworkMaps.size() ; i++)             
				{

					mapDate=  new Date(Long.parseLong(arrlstNetworkMaps.get(i).getActivateDate()));
					curDiff =currDate.getTime() - mapDate.getTime();
					

					if(curDiff < minDiff) 
					{
						minDiff = curDiff;
						mapIndex = i;
					}


					

				}




			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();Log.v("TT", "error:" + e.getMessage());
				handler.sendEmptyMessage(0);

			}




		}
		@Override
		protected Boolean doInBackground(String... params) {
			// TODO Auto-generated method stub
			//startWebView();
			displayBitmapInWebview();
			//			getBitmapFromAsset("");
			return null;
		}
		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progessBar.setVisibility(View.GONE);




		}



	}


}
