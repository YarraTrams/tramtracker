package com.yarratrams.tramtracker.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import android.app.Activity;
import android.app.ActivityGroup;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.androidtooltip.TooltipManager;
import com.android.androidtooltip.TooltipManager.Gravity;
import com.android.androidtooltip.TutorialTooltip;
import com.google.android.maps.GeoPoint;
import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.db.FavouriteManager;
import com.yarratrams.tramtracker.objects.DestinationGroup;
import com.yarratrams.tramtracker.objects.Favourite;
import com.yarratrams.tramtracker.objects.Route;
import com.yarratrams.tramtracker.objects.ServiceChange;
import com.yarratrams.tramtracker.objects.ServiceDisruption;
import com.yarratrams.tramtracker.objects.ServiceInfo;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.tasks.NearbyFavouriteTask;
import com.yarratrams.tramtracker.tasks.NextTramsForRouteTask;
import com.yarratrams.tramtracker.tasks.PIDTask;
import com.yarratrams.tramtracker.ui.util.Console;
import com.yarratrams.tramtracker.ui.util.FeaturesPreferences;
import com.yarratrams.tramtracker.ui.util.MenuPIDAdapter;
import com.yarratrams.tramtracker.ui.util.MultiPageDialog;
import com.yarratrams.tramtracker.ui.util.OtherLevelAds;
import com.yarratrams.tramtracker.ui.util.PIDChangeAlertArrayAdapter;
import com.yarratrams.tramtracker.ui.util.PIDDisruptionAlertArrayAdapter;
import com.yarratrams.tramtracker.ui.util.PIDServiceListArrayAdapter;
import com.yarratrams.tramtracker.ui.util.PIDTimeSort;
import com.yarratrams.tramtracker.ui.util.RoutesFilterDialog;
import com.yarratrams.tramtracker.ui.util.TutorialPreferences;
import com.yarratrams.tramtracker.ui.util.WrappingSlidingDrawer;

public class PIDActivity extends Activity
		implements
			OnClickListener,
			TooltipManager.onTooltipClosingCallback {
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_NEARBY;
	public static final String INTENT_KEY = "stop_info";
	private Stop stop;
	private ServiceInfo info;

	private boolean isChangeAlertDisplayed;
	private boolean isChangeAlertExpanded;
	private boolean isDisruptionAlertDisplayed;
	private boolean isDisruptionAlertExpanded;
	private boolean showAlerts;
	private boolean pauseUpdate;

	private RelativeLayout headerView;
	private ListView tramServicesList;
	private ListView changeAlertList;
	private ListView disruptionAlertList;
	private View footerView;
	private View tutorialPoint;

	private ArrayList<String> filter;
	private ArrayList<Route> stopRoutes;
	private FavouriteManager favouriteManager;
	private Favourite favourite;

	private PIDServiceListArrayAdapter serviceAdapter;
	private PIDDisruptionAlertArrayAdapter disruptionAdapter;
	private PIDChangeAlertArrayAdapter changeAdapter;

	private ProgressDialog loadDialog;
	PIDTask taskForAll;
	NextTramsForRouteTask taskForRoute;
	private boolean isOneRoute;

	private WrappingSlidingDrawer slidingMenu;

	// Tutorial Tooltip
	TooltipManager tooltipManager;
	TutorialTooltip tempTutorialTooltip;
	ArrayList<TutorialTooltip> alTutorialTooltips;
	public int tooltipIndex = 0;
	public boolean isTutorialRunning = false;
	boolean isFirstTime = true;
	TutorialPreferences tutorialPreferences;

	// For static header of filter
	int static_header_height;
	LinearLayout layPIDTop;

	public boolean isFilterByTime = false;

	/*
	 * Adil added
	 */
	View headerList;
	RelativeLayout stop_description_layout;
	AlertDialog adTutorial;

	// TextToSpeech textToSpeech;

	ImageView tram_list_title_route, tram_list_title_mins;

	/*
	 * 
	 */

	private Handler updatesHandler;
	private Runnable timeUpdater = new Runnable() {
		public void run() {
			if (!pauseUpdate) {
				if (stop != null) {
					if (filter != null) {
						setLowFloorIcon();
						if (areAllRoutesSelected()) {
							isOneRoute = false;
							getTimesUpdate();
						} else {
							isOneRoute = true;
							retrieveNextThree(getSelectedRoute());
						}
					}
				}
				updatesHandler.postDelayed(this, 30000);
			}
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pid_screen);

		info = new ServiceInfo();
		updatesHandler = new Handler();
		favouriteManager = new FavouriteManager(getApplicationContext());
		stopRoutes = new ArrayList<Route>();

		/*
		 * adil added : Adding headerview to list
		 */

		tooltipManager = new TooltipManager(TramTrackerMainActivity.instance);
		tutorialPreferences = new TutorialPreferences(this);
		isFirstTime = true;

		// temp reset pid pref
		// tutorialPreferences.resetAll();

		// headerList =(ViewGroup)
		// getLayoutInflater().inflate(R.layout.pidheader_view, null);
		headerList = getLayoutInflater().inflate(R.layout.pidheader_view, null);

		// set textviews for filter option
		tram_list_title_mins = (ImageView) headerList
				.findViewById(R.id.tram_list_title_mins);
		tram_list_title_route = (ImageView) headerList
				.findViewById(R.id.tram_list_title_route);
		stop_description_layout = (RelativeLayout) headerList
				.findViewById(R.id.stop_description_layout);
		tram_list_title_mins.setOnClickListener(this);
		tram_list_title_route.setOnClickListener(this);

		/*
		 * Ends
		 */

		RelativeLayout changeAlert = (RelativeLayout) headerList
				.findViewById(R.id.change_alert);
		changeAlert.setOnClickListener(this);
		changeAlertList = (ListView) headerList
				.findViewById(R.id.change_list_view);
		changeAdapter = new PIDChangeAlertArrayAdapter(this,
				info.getServiceChanges());
		changeAlertList.setAdapter(changeAdapter);
		createChangeAlert(info.getServiceChanges());

		RelativeLayout disruptionAlert = (RelativeLayout) headerList
				.findViewById(R.id.disruption_alert);
		disruptionAlert.setOnClickListener(this);
		disruptionAlertList = (ListView) headerList
				.findViewById(R.id.disruption_list_view);
		disruptionAdapter = new PIDDisruptionAlertArrayAdapter(this,
				info.getServiceDisruptions());
		disruptionAlertList.setAdapter(disruptionAdapter);
		createDisruptionAlert(info.getServiceDisruptions());

		expandAlert();

		/*
		 * ADIL ADDED
		 */
		TextView hideText = (TextView) headerList
				.findViewById(R.id.change_hide);
		hideText.setOnClickListener(this);
		/*
		 * ENDS
		 */

		headerView = (RelativeLayout) headerList
				.findViewById(R.id.pid_list_header);
		tramServicesList = (ListView) findViewById(R.id.service_list_view);
		ArrayList<DestinationGroup> groups = generateGroups(info.getRoutes());
		Console.print("stop before sending to constructor in pid: " + stop);
		serviceAdapter = new PIDServiceListArrayAdapter(this, groups, stop);
		headerView.setVisibility(View.VISIBLE);
		footerView = getLayoutInflater().inflate(R.layout.pid_filter_detail,
				null);
		/*
		 * adil added
		 */

		tramServicesList.addHeaderView(headerList, null, false);

		/*
		 * Ends
		 */
		tramServicesList.addFooterView(footerView, null, false);
		tramServicesList.setAdapter(serviceAdapter);

		setFilterDescription();

		// a invisible view to help position tutorial.
		tutorialPoint = (View) headerList.findViewById(R.id.tutorialPoints);

		slidingMenu = (WrappingSlidingDrawer) findViewById(R.id.sliding_menu);
		GridView menuContainer = (GridView) findViewById(R.id.sliding_menu_container);
		MenuPIDAdapter menuAdapter = new MenuPIDAdapter(this);
		menuContainer.setAdapter(menuAdapter);

		loadDialog = new ProgressDialog(this);

		// call ads
		OtherLevelAds.showAds(this, R.id.rich_banner_fragment1014,
				FeaturesPreferences
						.getAdsFlag(TramTrackerMainActivity.instance));

	}

	/*
	 * Adil added
	 */

	private void generateGroupNearestTimeList(ArrayList<DestinationGroup> groups) {
		// TODO Auto-generated method stub

		long min = 0;

		for (int i = 0; i < groups.size(); i++) {

			for (int r = 0; r < groups.get(i).getRoutes().size(); r++) {
				// setting inital min value
				min = groups.get(i).getRoutes().get(r).getNextServices().get(0)
						.getArrivalTime().getTime();

				for (int t = 0; t < groups.get(i).getRoutes().get(r)
						.getNextServices().size(); t++) {
					// System.err.println("-- data: " +
					// groups.get(i).getDestination() + " routes: " +
					// groups.get(i).getRoutes().get(r).getRouteNumber() +
					// " tramtime: " +
					// groups.get(i).getRoutes().get(r).getNextServices().get(t).getArrivalTime());

					if (groups.get(i).getRoutes().get(r).getNextServices()
							.get(t).getArrivalTime().getTime() < min) {
						min = groups.get(i).getRoutes().get(r)
								.getNextServices().get(t).getArrivalTime()
								.getTime();

					}

				}
			}

			// System.out.println(" --  group : " +
			// this.groups.get(i).getDestination() + " min : " + min );
			groups.get(i).setTramEarliestTime(new Date(min));
			// System.out.println(" --  group time saved : " +
			// this.groups.get(i).getTramEarliestTime().getTime() );

		}
	}

	/*
	 * Ends
	 */

	private Context getDialogContext() {
		Context context;
		if (getParent() != null) {
			context = getParent();
		} else {
			context = this;
		}
		return context;
	}

	private void setFilterDescription() {
		TextView description = (TextView) footerView
				.findViewById(R.id.filter_description);

		if (areAllRoutesSelected()) {
			// description.setText(getString(R.string.filter_showing_all));

			if (isLowFloor()) {
				try {
					if (stopRoutes.size() == 0) {
						description
								.setText(getString(R.string.filter_showing_no_low_floor_all_route));
					} else {
						description
								.setText(getString(R.string.filter_showing_low_floor));
					}
				} catch (NullPointerException e) {
					description
							.setText(getString(R.string.filter_showing_low_floor));
				}
			} else {
				description.setText(getString(R.string.filter_showing_all));
			}
		} else {
			Route route = getSelectedRoute();
			description.setText(getString(R.string.filter_showing_route)
					+ route.getRouteNumber());

			if (isLowFloor()) {
				try {
					if (stopRoutes.size() == 0) {
						description
								.setText(getString(R.string.filter_showing_no_low_floor_route)
										+ route.getRouteNumber());
					} else {
						description
								.setText(getString(R.string.filter_showing_low_floor_route)
										+ route.getRouteNumber());
					}
				} catch (NullPointerException e) {
					description
							.setText(getString(R.string.filter_showing_low_floor_route)
									+ route.getRouteNumber());
				}
			} else {
				try {
					if (stopRoutes.size() == 0) {
						description
								.setText(getString(R.string.filter_showing_no_route)
										+ route.getRouteNumber());
					} else {
						description
								.setText(getString(R.string.filter_showing_route)
										+ route.getRouteNumber());
					}
				} catch (NullPointerException e) {
					description
							.setText(getString(R.string.filter_showing_route)
									+ route.getRouteNumber());
				}
			}
		}
	}

	private void setStopMainInfo(Stop stop) {
		TextView stopName = (TextView) headerList
				.findViewById(R.id.stop_description_name);
		TextView stopDirection = (TextView) headerList
				.findViewById(R.id.stop_description_direction);
		TextView stopId = (TextView) footerView
				.findViewById(R.id.stop_description_id);
		TextView stopNumber = (TextView) footerView
				.findViewById(R.id.stop_description_number);
		ImageView ivPIDFreeTramZone = (ImageView) headerList
				.findViewById(R.id.ivPIDFreeTramZone);
		stopName.setText(getStopName(stop));
		stopDirection.setText(getCityDirection(stop));
		stopId.setText(String.valueOf(stop.getTrackerID()));
		stopNumber.setText(String.valueOf(stop.getStopNumber()));
		stopFTZ(ivPIDFreeTramZone, stop);
	}

	private void stopFTZ(ImageView ivPIDFreeTramZone, Stop stop2) {
		// TODO Auto-generated method stub
		if (stop2.IsInFreeZone()) {
			ivPIDFreeTramZone.setVisibility(View.VISIBLE);
		} else {
			ivPIDFreeTramZone.setVisibility(View.INVISIBLE);
		}
	}

	private String getStopName(Stop stop) {
		String name = "";
		String[] streets;

		streets = stop.getStopName().split(
				getResources().getString(R.string.pid_stopName_ampersand));
		name = streets[0];

		return name;
	}

	private String getCityDirection(Stop stop) {
		String direction = "";
		String[] streets;

		streets = stop.getStopName().split(
				getResources().getString(R.string.pid_stopName_ampersand));
		if (streets.length > 1) {
			direction = streets[1];
			direction = direction.concat(getResources().getString(
					R.string.pid_stopName_dash));
		}
		direction = direction.concat(stop.getCityDirection());

		return direction;
	}

	private void update(Intent intent) {
		// info = intent.getExtras().getParcelable(PIDUpdateService.INTENT_KEY);
		if (isOneRoute) {
			info = taskForRoute.getInfo();
		} else {
			info = taskForAll.getInfo();
		}

		updateUI();

		if (loadDialog.isShowing()) {
			loadDialog.dismiss();
		}
	}

	public void update() {
		update(null);
	}

	private void updateUI() {
		if (info != null) {
			if (info.getRoutes() != null) {
				headerView.setVisibility(View.VISIBLE);

				// System.out.println("info.getRoutes() = " + info.getRoutes());

				// ArrayList<Route> filteredList = filterRoutes(); // Removed
				// for version 1.01
				ArrayList<DestinationGroup> groups = generateGroups(info
						.getRoutes());

				if (isFilterByTime) {
					generateGroupNearestTimeList(groups);
					Collections.sort(groups, new PIDTimeSort());
				} else {

				}

				serviceAdapter.updateList(groups);

				ArrayList<ServiceChange> filteredChanges = filterChanges(info
						.getServiceChanges());
				changeAdapter = new PIDChangeAlertArrayAdapter(this,
						filteredChanges);
				changeAlertList.setAdapter(changeAdapter);

				// System.out.println("...... info.getServiceDisruptions() = " +
				// info.getServiceDisruptions());
				ArrayList<ServiceDisruption> filteredDisruptions = filterDisruptions(info
						.getServiceDisruptions());
				disruptionAdapter = new PIDDisruptionAlertArrayAdapter(this,
						filteredDisruptions);
				disruptionAlertList.setAdapter(disruptionAdapter);

				createChangeAlert(filteredChanges);
				createDisruptionAlert(filteredDisruptions);
				if (showAlerts) {
					expandAlert();
				}

				if (info.getRoutes() != null && !info.getRoutes().isEmpty()) {
					// call tutorial tip dialog after checking tutorial
					// preferences
					if (tutorialPreferences.getPidTutorial() == false
							&& isFirstTime) {

						boolean isMultiVisible;

						try {
							isMultiVisible = MultiPageDialog.isVisible();
						} catch (Exception e) {
							// TODO: handle exception
							isMultiVisible = false;
						}

						if (!isMultiVisible) {
							showTutorialDialog();
							isFirstTime = false;
						}
					}

					headerView.setVisibility(View.VISIBLE);
				} else {
					headerView.setVisibility(View.GONE);
				}

			} else {
				if (!(tramServicesList != null && tramServicesList
						.getChildCount() > 0)) {
					headerView.setVisibility(View.GONE);
				}
			}
		} else {
			if (!(tramServicesList != null && tramServicesList.getChildCount() > 0)) {
				headerView.setVisibility(View.GONE);
			}
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		pauseUpdate = false;
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			stop = extras.getParcelable(INTENT_KEY);
		} else {
			NearbyFavouriteTask task = new NearbyFavouriteTask(this);
			task.execute();
			return;
		}
		setStopMainInfo(stop);

		// Added by Adil
		serviceAdapter.updateStop(stop);

		stopRoutes = favouriteManager.getRoutesForStop(stop);

		// System.out.println("-- fav stop: " + stop.getStopName() + "-" +
		// stop.getTrackerID());

		if (filter == null) {
			filter = new ArrayList<String>();

			System.out.println("is stop fav: "
					+ favouriteManager.isStopFavourite(stop.getTrackerID()));

			if (favouriteManager.isStopFavourite(stop.getTrackerID())) {
				favourite = favouriteManager.getFavouriteForStop(stop
						.getTrackerID());

				filter = favourite.getAlRoutes();
				// System.out.println("-- filter list: " +
				// filter.get(0).toString());
			} else {
				resetFilter(stopRoutes);
			}
		}
		setFilterDescription();

		if (!loadDialog.isShowing() && info != null
				&& info.getRoutes().size() < 1) {
			loadDialog = ProgressDialog.show(getDialogContext(), "",
					getResources().getString(R.string.dialog_loading), true,
					true);
		}

		updatesHandler.post(timeUpdater);
	}

	public void updateWithNearestFavourite(Stop stop) {

		Console.print("stop info: " + stop);
		serviceAdapter.updateStop(stop);
		if (pauseUpdate) {
			return;
		}
		if (stop == null) {
			TramTrackerMainActivity.getAppManager().displayErrorMessage(
					getResources().getString(
							R.string.error_nearestfavourite_nofavourite));
			ActivityGroup group = TramTrackerMainActivity.getAppManager()
					.requestDifferentTab(TramTrackerMainActivity.TAB_ROUTES);
			Intent intent = new Intent(group, RoutesEntryActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			TramTrackerMainActivity.getAppManager().show(
					TramTrackerMainActivity.TAB_ROUTES,
					getResources().getString(R.string.tag_routes_entry_screen),
					intent);
			return;
		}
		this.stop = stop;

		setStopMainInfo(stop);
		stopRoutes = favouriteManager.getRoutesForStop(stop);

		if (filter == null) {
			filter = new ArrayList<String>();
		}
		if (favouriteManager.isStopFavourite(stop.getTrackerID())) {
			favourite = favouriteManager.getFavouriteForStop(stop
					.getTrackerID());
			filter = favourite.getAlRoutes();
		} else {
			resetFilter(stopRoutes);
		}
		setFilterDescription();

		if (!loadDialog.isShowing()) {
			loadDialog = ProgressDialog.show(getDialogContext(), "",
					getResources().getString(R.string.dialog_loading), true,
					true);
		}

		updatesHandler.post(timeUpdater);
	}

	@Override
	public void onPause() {
		super.onPause();
		pauseUpdate = true;
		isTutorialRunning = false;
		if (alTutorialTooltips != null) {
			for (int i = 0; i < alTutorialTooltips.size(); i++) {
				try {
					alTutorialTooltips.get(i).closeTutorialTooltip(
							tooltipManager);
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}
		stopTasks();

		try {
			if (adTutorial.isShowing()) {
				adTutorial.dismiss();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void createChangeAlert(ArrayList<ServiceChange> serviceChanges) {
		RelativeLayout changeAlert = (RelativeLayout) headerList
				.findViewById(R.id.change_alert);
		TextView routeTitle = (TextView) headerList
				.findViewById(R.id.pid_change_title);
		routeTitle.setVisibility(View.GONE);
		if (serviceChanges.isEmpty()) {
			changeAlert.setVisibility(View.GONE);
			isChangeAlertDisplayed = false;
		} else {
			if (!isChangeAlertDisplayed) {
				showAlerts = true;
			}
			changeAlert.setVisibility(View.VISIBLE);
			isChangeAlertDisplayed = true;
		}
	}

	private void createDisruptionAlert(
			ArrayList<ServiceDisruption> serviceDisruptions) {

		RelativeLayout disruptionAlert = (RelativeLayout) headerList
				.findViewById(R.id.disruption_alert);
		TextView routeTitle = (TextView) findViewById(R.id.pid_disruption_title);
		if (serviceDisruptions.isEmpty()) {
			disruptionAlert.setVisibility(View.GONE);
			isDisruptionAlertDisplayed = false;
		} else {
			if (!isDisruptionAlertDisplayed) {
				showAlerts = true;
			}
			disruptionAlert.setVisibility(View.VISIBLE);
			isDisruptionAlertDisplayed = true;
			if (serviceDisruptions.get(0).isText()) {
				routeTitle.setVisibility(View.GONE);
			} else {
				routeTitle.setVisibility(View.VISIBLE);
			}
		}
	}

	private void expandAlert() {
		if (isDisruptionAlertDisplayed) {
			switchChangeAlertState(false);
			switchDisruptionAlertState(true);
		} else if (isChangeAlertDisplayed) {
			switchChangeAlertState(true);
		}
		showAlerts = false;
	}

	public void switchChangeAlertState() {
		LinearLayout list = (LinearLayout) headerList
				.findViewById(R.id.change_list);
		TextView showText = (TextView) headerList
				.findViewById(R.id.change_show);
		ImageView showIcon = (ImageView) headerList
				.findViewById(R.id.change_show_image);
		TextView hideText = (TextView) headerList
				.findViewById(R.id.change_hide);
		ImageView hideIcon = (ImageView) headerList
				.findViewById(R.id.change_hide_image);
		if (isChangeAlertExpanded) {
			TramTrackerMainActivity.getAppManager().callSelection(
					getString(R.string.accessibility_click_pid_hide));
			isChangeAlertExpanded = false;
			list.setVisibility(View.GONE);
			hideText.setVisibility(View.GONE);
			hideIcon.setVisibility(View.GONE);
			showText.setVisibility(View.VISIBLE);
			showIcon.setVisibility(View.VISIBLE);
		} else {
			TramTrackerMainActivity.getAppManager().callSelection(
					getString(R.string.accessibility_click_pid_show));
			isChangeAlertExpanded = true;
			list.setVisibility(View.VISIBLE);
			hideText.setVisibility(View.VISIBLE);
			hideIcon.setVisibility(View.VISIBLE);
			showText.setVisibility(View.GONE);
			showIcon.setVisibility(View.GONE);
		}

	}

	private void switchChangeAlertState(boolean expand) {
		LinearLayout list = (LinearLayout) headerList
				.findViewById(R.id.change_list);
		TextView showText = (TextView) headerList
				.findViewById(R.id.change_show);
		ImageView showIcon = (ImageView) headerList
				.findViewById(R.id.change_show_image);
		TextView hideText = (TextView) headerList
				.findViewById(R.id.change_hide);
		// ListView changeList = (ListView)
		// headerList.findViewById(R.id.change_list_view);
		ImageView hideIcon = (ImageView) headerList
				.findViewById(R.id.change_hide_image);
		if (!expand) {
			isChangeAlertExpanded = false;
			list.setVisibility(View.GONE);

			// changeList.setVisibility(View.GONE);
			hideText.setVisibility(View.GONE);
			hideIcon.setVisibility(View.GONE);
			showText.setVisibility(View.VISIBLE);
			showIcon.setVisibility(View.VISIBLE);
		} else {
			isChangeAlertExpanded = true;
			list.setVisibility(View.VISIBLE);
			// changeList.setVisibility(View.VISIBLE);
			hideText.setVisibility(View.VISIBLE);
			hideIcon.setVisibility(View.VISIBLE);
			showText.setVisibility(View.GONE);
			showIcon.setVisibility(View.GONE);
		}

	}

	public void switchDisruptionAlertState() {
		LinearLayout list = (LinearLayout) headerList
				.findViewById(R.id.disruption_list);
		TextView showText = (TextView) headerList
				.findViewById(R.id.disruption_show);
		ImageView showIcon = (ImageView) headerList
				.findViewById(R.id.disruption_show_image);
		TextView hideText = (TextView) headerList
				.findViewById(R.id.disruption_hide);
		ImageView hideIcon = (ImageView) headerList
				.findViewById(R.id.disruption_hide_image);
		if (isDisruptionAlertExpanded) {
			TramTrackerMainActivity.getAppManager().callSelection(
					getString(R.string.accessibility_click_pid_hide));
			isDisruptionAlertExpanded = false;
			list.setVisibility(View.GONE);
			hideText.setVisibility(View.GONE);
			hideIcon.setVisibility(View.GONE);
			showText.setVisibility(View.VISIBLE);
			showIcon.setVisibility(View.VISIBLE);
		} else {
			TramTrackerMainActivity.getAppManager().callSelection(
					getString(R.string.accessibility_click_pid_show));
			isDisruptionAlertExpanded = true;
			list.setVisibility(View.VISIBLE);
			hideText.setVisibility(View.VISIBLE);
			hideIcon.setVisibility(View.VISIBLE);
			showText.setVisibility(View.GONE);
			showIcon.setVisibility(View.GONE);
		}
	}

	private void switchDisruptionAlertState(boolean expand) {
		LinearLayout list = (LinearLayout) findViewById(R.id.disruption_list);
		TextView showText = (TextView) headerList
				.findViewById(R.id.disruption_show);
		ImageView showIcon = (ImageView) headerList
				.findViewById(R.id.disruption_show_image);
		TextView hideText = (TextView) headerList
				.findViewById(R.id.disruption_hide);
		ImageView hideIcon = (ImageView) headerList
				.findViewById(R.id.disruption_hide_image);
		if (!expand) {
			isDisruptionAlertExpanded = false;
			list.setVisibility(View.GONE);
			hideText.setVisibility(View.GONE);
			hideIcon.setVisibility(View.GONE);
			showText.setVisibility(View.VISIBLE);
			showIcon.setVisibility(View.VISIBLE);
		} else {
			isDisruptionAlertExpanded = true;
			list.setVisibility(View.VISIBLE);
			hideText.setVisibility(View.VISIBLE);
			hideIcon.setVisibility(View.VISIBLE);
			showText.setVisibility(View.GONE);
			showIcon.setVisibility(View.GONE);
		}
	}

	@Override
	public void onClick(View v) {
		RelativeLayout disruptionAlert = (RelativeLayout) findViewById(R.id.disruption_alert);
		RelativeLayout changeAlert = (RelativeLayout) findViewById(R.id.change_alert);
		TextView hideText = (TextView) headerList
				.findViewById(R.id.change_hide);

		// Added by Adil
		int id = v.getId();

		if (v == changeAlert || v == hideText) {
			switchChangeAlertState();
		}

		else if (v == disruptionAlert) {
			switchDisruptionAlertState();
		}
		// Added by Adil
		else if (id == R.id.tram_list_title_route) {

			isFilterByTime = false;
			// Toast.makeText(TramTrackerMainActivity.instance, "Route",
			// 0).show();
			// switchFilterBg(tram_list_title_route, tram_list_title_mins);
			tram_list_title_route.setImageResource(R.drawable.route_active);
			tram_list_title_mins.setImageResource(R.drawable.minutes_idle);
			updateUI();
			if (isTutorialRunning && tooltipIndex == 1) {
				showNextTutorialTooltip();
			}
		} else if (id == R.id.tram_list_title_mins) {
			// Toast.makeText(getApplicationContext(), "Mins", 0).show();
			isFilterByTime = true;
			// switchFilterBg(tram_list_title_mins, tram_list_title_route);
			tram_list_title_route.setImageResource(R.drawable.route_idle);
			tram_list_title_mins.setImageResource(R.drawable.minutes_active);
			updateUI();
			if (isTutorialRunning && tooltipIndex == 0) {
				showNextTutorialTooltip();
			}
		}

	}

	/*
	 * Adil added
	 */

	// public void switchFilterBg(ImageView imageView, ImageView imageView2)
	// {
	// textView.setBackgroundColor(Color.parseColor("#eaeaea"));
	// textView2.setBackgroundColor(Color.WHITE);
	// }

	/*
	 * Ends
	 */

	public void setCurrentIndex(int index) {
		tramServicesList.setSelection(index);
	}

	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// MenuInflater inflater = getMenuInflater();
		// inflater.inflate(R.menu.pid_menu, menu);
		// TramTrackerMainActivity.getAppManager().setMenuBackground(this);
		return true;
	}

	@Override
	public void openOptionsMenu() {
		slidingMenu.toggle();
	}
	@Override
	public void closeOptionsMenu() {
		slidingMenu.close();
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			openOptionsMenu();
			return true;
		} else if (keyCode == KeyEvent.KEYCODE_BACK) {
			onBackPressed();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		ActivityGroup group;
		Intent intent;
		switch (item.getItemId()) {
			case R.id.menu_pid_favourites :
				if (favouriteManager.isStopFavourite(stop.getTrackerID())) {
					TramTrackerMainActivity.getAppManager()
							.displayErrorMessage(
									getResources().getString(
											R.string.error_favourite_added));

				} else {
					group = TramTrackerMainActivity.getAppManager()
							.requestDifferentTab(
									TramTrackerMainActivity.TAB_FAVOURITES);
					intent = new Intent(group, AddFavouriteActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.putExtra(AddFavouriteActivity.INTENT_KEY_STOP, stop);
					TramTrackerMainActivity.getAppManager().show(
							TramTrackerMainActivity.TAB_FAVOURITES,
							getResources().getString(
									R.string.tag_addfavourite_screen), intent);
				}
				return true;

			case R.id.menu_pid_outlets :
				intent = new Intent(this, NearbyActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra(NearbyActivity.INTENT_CENTRE_KEY, stop);
				TramTrackerMainActivity.getAppManager().show(
						TramTrackerMainActivity.getAppManager()
								.getCurrentTabID(),
						getResources().getString(R.string.tag_nearby_screen),
						intent);
				return true;

			case R.id.menu_pid_directions :
				GeoPoint destination = new GeoPoint(stop.getLatitudeE6(),
						stop.getLongitudeE6());
				TramTrackerMainActivity.getAppManager()
						.requestDirectionsService(destination);
				return true;

			case R.id.menu_pid_filter :
				final RoutesFilterDialog dialog = new RoutesFilterDialog(
						getDialogContext(), stopRoutes, filter);
				dialog.setOnDismissListener(new OnDismissListener() {
					@Override
					public void onDismiss(DialogInterface dialogInterface) {
						if (dialog.isFiltered()) {
							if (!loadDialog.isShowing()) {
								loadDialog = ProgressDialog.show(
										getDialogContext(),
										"",
										getResources().getString(
												R.string.dialog_loading), true,
										true);
							}
							filter = dialog.getFiltered();
							setFilterDescription();
							updatesHandler.removeCallbacks(timeUpdater);
							updatesHandler.post(timeUpdater);
						}
					}
				});
				dialog.show();
				return true;

			case R.id.menu_pid_help :
				intent = new Intent(getDialogContext(), HelpActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				TramTrackerMainActivity.getAppManager().show(
						TramTrackerMainActivity.getAppManager()
								.getCurrentTabID(),
						getResources().getString(R.string.tag_help_screen),
						intent);
				return true;

			case R.id.menu_pid_search :
				intent = new Intent(getDialogContext(),
						SearchMainActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				TramTrackerMainActivity.getAppManager().show(
						TramTrackerMainActivity.getAppManager()
								.getCurrentTabID(),
						getResources().getString(
								R.string.tag_search_main_screen), intent);
				return true;
			default :
				return super.onOptionsItemSelected(item);
		}
	}

	public void runFavouriteStopMenuOption() {
		closeOptionsMenu();
		if (favouriteManager.isStopFavourite(stop.getTrackerID())) {
			TramTrackerMainActivity.getAppManager().displayErrorMessage(
					getResources().getString(R.string.error_favourite_added));

		} else {
			ActivityGroup group = TramTrackerMainActivity
					.getAppManager()
					.requestDifferentTab(TramTrackerMainActivity.TAB_FAVOURITES);
			Intent intent = new Intent(group, AddFavouriteActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.putExtra(AddFavouriteActivity.INTENT_KEY_STOP, stop);
			TramTrackerMainActivity.getAppManager().show(
					TramTrackerMainActivity.TAB_FAVOURITES,
					getResources().getString(R.string.tag_addfavourite_screen),
					intent);
		}
	}
	public void runOutletsMenuOption() {
		closeOptionsMenu();
		Intent intent = new Intent(this, NearbyActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(NearbyActivity.INTENT_CENTRE_KEY, stop);
		TramTrackerMainActivity.getAppManager().show(
				TramTrackerMainActivity.getAppManager().getCurrentTabID(),
				getResources().getString(R.string.tag_nearby_screen), intent);
	}
	public void runDirectionsMenuOption() {
		closeOptionsMenu();
		GeoPoint destination = new GeoPoint(stop.getLatitudeE6(),
				stop.getLongitudeE6());
		TramTrackerMainActivity.getAppManager().requestDirectionsService(
				destination);
	}
	public void runFilterMenuOption() {
		closeOptionsMenu();
		final RoutesFilterDialog dialog = new RoutesFilterDialog(
				getDialogContext(), stopRoutes, filter);
		dialog.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialogInterface) {
				if (dialog.isFiltered()) {
					if (!loadDialog.isShowing()) {
						loadDialog = ProgressDialog.show(
								getDialogContext(),
								"",
								getResources().getString(
										R.string.dialog_loading), true, true);
					}
					filter = dialog.getFiltered();
					setFilterDescription();
					updatesHandler.removeCallbacks(timeUpdater);
					updatesHandler.post(timeUpdater);
				}
			}
		});
		dialog.show();
	}
	public void runSearchMenuOption() {
		closeOptionsMenu();
		Intent intent = new Intent(getDialogContext(), SearchMainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		TramTrackerMainActivity.getAppManager().show(
				TramTrackerMainActivity.getAppManager().getCurrentTabID(),
				getResources().getString(R.string.tag_search_main_screen),
				intent);
	}
	public void runTrackerIDMenuOption() {
		closeOptionsMenu();
		ActivityGroup group = TramTrackerMainActivity.getAppManager()
				.requestDifferentTab(TramTrackerMainActivity.TAB_MORE);
		Intent intent = new Intent(group, SearchTrackerIDActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		TramTrackerMainActivity
				.getAppManager()
				.show(TramTrackerMainActivity.TAB_MORE,
						getResources().getString(R.string.tag_trackerid_screen),
						intent);
	}

	// Filter not applied any more for version 1.01
	// private ArrayList<Route> filterRoutes(ArrayList<Route> list){
	// ArrayList<Route> filteredList = new ArrayList<Route>();
	//
	// for(Route route : list){
	// if(route.getDestination() != null){
	// if(route.getNextServices() != null &&
	// !route.getNextServices().isEmpty()){
	// //if(isSelected(route.getRouteNumber())){
	// filteredList.add(route);
	// //}
	// }
	// }
	// }
	// return filteredList;
	// }

	private boolean isSelected(String route) {
		if (!filter.isEmpty()) {
			for (String option : filter) {
				if (option.equalsIgnoreCase(getResources().getString(
						R.string.route_filter_dbtag_all))
						|| option.equalsIgnoreCase(route)) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean isLowFloor() {
		if (filter != null) {
			if (!filter.isEmpty()) {
				for (String option : filter) {
					if (option.equalsIgnoreCase(getResources().getString(
							R.string.route_filter_dbtag_lowfloor))) {
						return true;
					}
				}
			}
		}
		return false;
	}

	private void setLowFloorIcon() {
		ImageView lowFloorIndicator = (ImageView) headerList
				.findViewById(R.id.lowfloor_icon);

		if (isLowFloor()) {
			lowFloorIndicator.setVisibility(View.VISIBLE);
		} else {
			lowFloorIndicator.setVisibility(View.GONE);
		}

	}

	private Route getSelectedRoute() {
		if (filter != null) {
			if (!filter.isEmpty()) {
				for (Route route : stopRoutes) {
					if (isSelected(route.getRouteNumber())) {
						return route;
					}
				}
			}
		}
		return new Route();
	}

	private boolean areAllRoutesSelected() {
		if (filter != null) {
			if (!filter.isEmpty()) {
				for (String option : filter) {
					if (option.equalsIgnoreCase(getResources().getString(
							R.string.route_filter_dbtag_all))) {
						return true;
					}
				}
			}
		} else {
			return true;
		}
		return false;
	}

	private ArrayList<ServiceChange> filterChanges(ArrayList<ServiceChange> list) {
		ArrayList<ServiceChange> filteredList = new ArrayList<ServiceChange>();

		for (ServiceChange service : list) {
			if (service.getRouteNumber() != null)
				// if(isSelected(service.getRouteNumber())){
				filteredList.add(service);
			// }
		}

		return filteredList;
	}

	private ArrayList<ServiceDisruption> filterDisruptions(
			ArrayList<ServiceDisruption> list) {
		ArrayList<ServiceDisruption> filteredList = new ArrayList<ServiceDisruption>();

		for (ServiceDisruption service : list) {
			if (service.getRouteNumber() != null) {
				// if(isSelected(service.getRouteNumber())){
				filteredList.add(service);
				// }
			}
		}

		return filteredList;
	}

	private void resetFilter(ArrayList<Route> routes) {
		filter.clear();
		filter.add(getResources().getString(R.string.route_filter_dbtag_all));
	}

	private ArrayList<DestinationGroup> generateGroups(ArrayList<Route> routes) {
		ArrayList<DestinationGroup> groups = new ArrayList<DestinationGroup>();

		for (Route route : routes) {
			addToGroup(groups, route);
		}
		for (DestinationGroup group : groups) {
			group.generateServicesOrderedList();
		}

		return groups;
	}

	private void addToGroup(ArrayList<DestinationGroup> groups, Route route) {
		// Change for version 1.01
		// for(DestinationGroup group : groups){
		// if(group.belongsToGroup(route)){
		// group.addToGroup(route);
		// group.setGroup(true);
		// return;
		// }
		// }
		DestinationGroup group = new DestinationGroup();
		group.addToGroup(route);
		group.checkForGroupType();
		groups.add(group);
	}

	private void stopTasks() {
		if (taskForAll != null) {
			taskForAll.cancel(true);
		}
		if (taskForRoute != null) {
			taskForRoute.cancel(true);
		}
	}

	private void getTimesUpdate() {
		taskForAll = new PIDTask(this, isLowFloor());
		taskForAll.execute(stop);
	}

	public void retrieveNextThree(Route route) {
		taskForRoute = new NextTramsForRouteTask(this, stop, route,
				isLowFloor());
		taskForRoute.execute();
	}

	public void createTutorialToolTips() {

		// String minutesText =
		// "You can now sort your predictions by arrival time. Tap on <b>Minutes</b> to update the sort order. <br/><br/>This feature works best when two or more routes pass through the stop.";
		String minutesText = "TAP Minutes to order by arrival time.";
		// String routesText =
		// "Tap on <b>Routes</b> to return the sort order to default.";
		String routesText = "TAP Route to sort by route number.";
		String scrollablepidText = "This section will become scrollable if there are two or more routes expanded to allow you to view more tram arrival predictions.\nTAP HERE to continue.";
		// String reminderText =
		// "You can now set alarms for when your tram is due to arrive by long-pressing on the arrival time. tramTRACKER will send you a notification when your tram is approaching the stop.<br/><br/><b>Long-press</b> here to set your first alarm.";
		String reminderText = "LONG-PRESS here to receive an alarm when your tram is due to arrive at this stop.<br/><br/>Or TAP HERE to dismiss this tooltip";

		alTutorialTooltips = new ArrayList<TutorialTooltip>();

		// Minutes tutorial tip #0
		tempTutorialTooltip = new TutorialTooltip();
		tempTutorialTooltip.createTutorialTooltipNoDismiss(tooltipManager,
				tram_list_title_mins, this, Gravity.BOTTOM, minutesText);
		alTutorialTooltips.add(tempTutorialTooltip);

		// Routes tutorial tip #1
		tempTutorialTooltip = new TutorialTooltip();
		tempTutorialTooltip.createTutorialTooltipNoDismiss(tooltipManager,
				tram_list_title_route, this, Gravity.BOTTOM, routesText);
		alTutorialTooltips.add(tempTutorialTooltip);

		// // Scrollable PID tutorial tip #2
		// tempTutorialTooltip = new TutorialTooltip();
		// tempTutorialTooltip.createTutorialTooltip(tooltipManager,
		// stop_description_layout, this, Gravity.BOTTOM,
		// scrollablepidText);
		// alTutorialTooltips.add(tempTutorialTooltip);

		try {
			if (serviceAdapter != null) {
				// Reminder tutorial tip #3
				tempTutorialTooltip = new TutorialTooltip();
				tempTutorialTooltip.createTutorialTooltip(tooltipManager,
						tutorialPoint, this, Gravity.TOP, reminderText);
				alTutorialTooltips.add(tempTutorialTooltip);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void onClosing(int id, boolean fromUser, boolean containsTouch) {
		// TODO Auto-generated method stub

		switch (id) {
			case R.id.tram_list_title_mins :
				// showNextTutorialTooltip();
				break;

			case R.id.tram_list_title_route :

				// showNextTutorialTooltip();
				break;

			case R.id.stop_description_layout :
				// showNextTutorialTooltip();
				break;

			case R.id.tutorialPoints :
				isTutorialRunning = false;
				tutorialPreferences.setPidTutorial(true);
				Toast.makeText(
						this,
						"Thank you for completing this tutorial. Get more tutorials in my tramTRACKER, myTRAM and Timetables.",
						1).show();

				break;

			default :
				break;
		}

	}

	public void showTutorialDialog() {

		// String msg =
		// "To make your experience with tramTRACKER better, we have introduced an interactive tutorial to help guide you through the new features on the tram arrival screen.\n\nIf you would like to skip the tutorial and view it next time, please select the 'Remind Me Later' option below.";
		String msg = "To improve your tramTRACKER\nexperience, there are quick tutorials to guide you through the new features.\n\nTo turn off this reminder go to More > my tramTRACKER > Interactive Tutorials.";
		AlertDialog.Builder builder = new Builder(
				TramTrackerMainActivity.instance);
		builder.setTitle("Interactive Tutorial");
		builder.setMessage(msg);
		builder.setPositiveButton("View Now",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						createTutorialToolTips();
						tooltipIndex = 0;
						if (alTutorialTooltips != null) {
							isTutorialRunning = true;
							alTutorialTooltips.get(tooltipIndex).showTooltip();
						}
						// tutorialPreferences.setPidTutorial(true);
						dialog.dismiss();

						// textToSpeech = new
						// TextToSpeech(getApplicationContext() , new
						// OnInitListener() {
						//
						// @Override
						// public void onInit(int status) {
						// // TODO Auto-generated method stub
						// if(status!=TextToSpeech.ERROR)
						// textToSpeech.setLanguage(Locale.US);
						// }
						// });

					}
				});
		builder.setNegativeButton("Remind Me Later",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();

					}
				});

		adTutorial = builder.create();
		adTutorial.setCanceledOnTouchOutside(false);
		adTutorial.setCancelable(false);

		adTutorial.show();

	}

	public void showNextTutorialTooltip() {
		try {

			alTutorialTooltips.get(tooltipIndex).closeTutorialTooltip(
					tooltipManager);
		} catch (Exception e) {
			// TODO: handle exception
		}

		tooltipIndex++;
		if (tooltipIndex < alTutorialTooltips.size()) {
			try {
				alTutorialTooltips.get(tooltipIndex).showTooltip();
				// if(textToSpeech!=null)
				// {
				// textToSpeech.speak(alTutorialTooltips.get(tooltipIndex).getText(),
				// TextToSpeech.QUEUE_FLUSH, null);
				// }

			} catch (Exception e) {
				// TODO: handle exception
				isTutorialRunning = false;
			}
		} else {
			isTutorialRunning = false;
		}
	}

	public void dismissAlarmTooltip() {

		try {

			// alTutorialTooltips.get(3).closeTutorialTooltip(tooltipManager);
			for (int i = 0; i < alTutorialTooltips.size(); i++) {
				alTutorialTooltips.get(i).closeTutorialTooltip(tooltipManager);
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
