package com.yarratrams.tramtracker.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.ToggleButton;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.SearchResult;
import com.yarratrams.tramtracker.tasks.SearchStopsTicketOutletPOITask;
import com.yarratrams.tramtracker.ui.util.FeaturesPreferences;
import com.yarratrams.tramtracker.ui.util.OtherLevelAds;


public class SearchMainActivity extends Activity implements OnClickListener{
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_NEARBY;

	private ProgressDialog loadDialog;
	private InputMethodManager imm;


	// Adil added
	LinearLayout laySearchMainScreen;
	FrameLayout rich_banner_fragment1021;

	private SearchStopsTicketOutletPOITask searchTask;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_main_screen);

		//adil added

		laySearchMainScreen = (LinearLayout) findViewById(R.id.laySearchMainScreen);
		rich_banner_fragment1021 = (FrameLayout) findViewById(R.id.rich_banner_fragment1021);

		ToggleButton stopSelect = (ToggleButton) findViewById(R.id.search_stop_checkButton);
		ToggleButton shelterSelect = (ToggleButton) findViewById(R.id.search_shelter_checkButton);
		ToggleButton accessSelect = (ToggleButton) findViewById(R.id.search_accessible_checkButton);
		ToggleButton outletSelect = (ToggleButton) findViewById(R.id.search_outlets_checkButton);
		ToggleButton poiSelect = (ToggleButton) findViewById(R.id.search_poi_checkButton);
		stopSelect.setOnClickListener(this);
		shelterSelect.setOnClickListener(this);
		accessSelect.setOnClickListener(this);
		outletSelect.setOnClickListener(this);
		poiSelect.setOnClickListener(this);

		Button searchButton = (Button) findViewById(R.id.search_button);
		searchButton.setOnClickListener(this);

		EditText searchText = (EditText) findViewById(R.id.search_text);
		searchText.setOnEditorActionListener(new OnEditorActionListener() {        
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if(actionId==EditorInfo.IME_ACTION_DONE || event.getKeyCode() == KeyEvent.KEYCODE_ENTER){
					search();
				}
				return false;
			}
		});


		//call ads
		OtherLevelAds.showAds(this, R.id.rich_banner_fragment1021,FeaturesPreferences.getAdsFlag(TramTrackerMainActivity.instance));


		laySearchMainScreen.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {
				Rect r = new Rect();
				laySearchMainScreen.getWindowVisibleDisplayFrame(r);
				int heightDiff = laySearchMainScreen.getRootView().getHeight() - (r.bottom - r.top);

				if (heightDiff > 100) { // if more than 100 pixels, its probably a keyboard...
					//ok now we know the keyboard is up...
					rich_banner_fragment1021.setVisibility(View.INVISIBLE);
					//                    view_two.setVisibility(View.GONE);

				}else{
					//ok now we know the keyboard is down...
					rich_banner_fragment1021.setVisibility(View.VISIBLE);
					//                    view_two.setVisibility(View.VISIBLE);

				}
			}
		});
	}

	public Context getDialogContext() {
		Context context;
		if (getParent() != null) {
			context = getParent();
		} else {
			context = this;
		}
		return context;
	}


	@Override
	public void onClick(View v) {
		ToggleButton stopSelect = (ToggleButton) findViewById(R.id.search_stop_checkButton);
		ToggleButton shelterSelect = (ToggleButton) findViewById(R.id.search_shelter_checkButton);
		ToggleButton accessSelect = (ToggleButton) findViewById(R.id.search_accessible_checkButton);
		ToggleButton outletSelect = (ToggleButton) findViewById(R.id.search_outlets_checkButton);
		ToggleButton poiSelect = (ToggleButton) findViewById(R.id.search_poi_checkButton);
		Button searchButton = (Button) findViewById(R.id.search_button);

		if(v == stopSelect){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_search_stops));
			setSearchFilter(true, stopSelect.isChecked());
		} else if(v == shelterSelect){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_search_shelters));
			setSearchFilter(false, shelterSelect.isChecked());
		} else if(v == accessSelect){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_search_access));
			setSearchFilter(false, accessSelect.isChecked());
		} else if(v == outletSelect){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_search_outlets));
		} else if(v == poiSelect){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_search_poi));
		} else if(v == searchButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_search));
			search();
		}

	}

	private void setSearchFilter(boolean isStopOption, boolean search){
		ToggleButton stopSelect = (ToggleButton) findViewById(R.id.search_stop_checkButton);
		ToggleButton shelterSelect = (ToggleButton) findViewById(R.id.search_shelter_checkButton);
		ToggleButton accessSelect = (ToggleButton) findViewById(R.id.search_accessible_checkButton);
		if(isStopOption){
			if(!search){
				shelterSelect.setChecked(false);
				accessSelect.setChecked(false);
			}
		} else {
			if(search){
				stopSelect.setChecked(true);
			}
		}
	}

	private void search(){
		EditText searchText = (EditText) findViewById(R.id.search_text);
		String keyword = searchText.getText().toString();
		if(keyword.equals("")){
			TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.error_search_nokeyword));
			return;
		}

		if(keyword.length() < 3){
			TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.error_search_shortkeyword));
			return;
		}

		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(searchText.getWindowToken(), 0);

		if(loadDialog ==  null || !loadDialog.isShowing()){
			loadDialog = ProgressDialog.show(getDialogContext(), "", getResources().getString(R.string.dialog_loading), true, true);
		}

		ToggleButton stopSelect = (ToggleButton) findViewById(R.id.search_stop_checkButton);
		ToggleButton shelterSelect = (ToggleButton) findViewById(R.id.search_shelter_checkButton);
		ToggleButton accessSelect = (ToggleButton) findViewById(R.id.search_accessible_checkButton);
		ToggleButton outletSelect = (ToggleButton) findViewById(R.id.search_outlets_checkButton);
		ToggleButton poiSelect = (ToggleButton) findViewById(R.id.search_poi_checkButton);

		if(searchTask != null){
			searchTask.cancel(true);
		}
		searchTask = new SearchStopsTicketOutletPOITask(this, keyword, stopSelect.isChecked(), 
				shelterSelect.isChecked(), accessSelect.isChecked(), 
				outletSelect.isChecked(), poiSelect.isChecked()); 
		searchTask.execute();
	}

	public void updateUI(SearchResult result){
		if(loadDialog.isShowing()){
			loadDialog.dismiss();
		}

		if(result == null){
			TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.error_search_noresults));
			return;
		}

		Intent intent = new Intent(this, SearchResultsActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(SearchResultsActivity.INTENT_RESULTS_KEY, result);

		TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.getAppManager().getCurrentTabID(), 
				getResources().getString(R.string.tag_search_results_screen), intent);
	}


	@Override
	protected void onResume() {
		imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		EditText searchText = (EditText) findViewById(R.id.search_text);
		searchText.clearFocus();

		super.onResume();
	}

	@Override
	protected void onPause() {
		EditText searchText = (EditText) findViewById(R.id.search_text);
		imm.hideSoftInputFromWindow(searchText.getWindowToken(), 0);

		if(searchTask != null){
			searchTask.cancel(true);
		}

		super.onPause();
	}


	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

}
