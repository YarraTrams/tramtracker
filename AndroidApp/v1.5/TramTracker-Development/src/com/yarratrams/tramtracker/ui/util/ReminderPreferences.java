package com.yarratrams.tramtracker.ui.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.yarratrams.tramtracker.objects.Reminders;

public class ReminderPreferences {


	Context context;
	SharedPreferences sharedPreferences;
	SharedPreferences.Editor editor;


	final String PREF_NAME = "reminderpref";
	final String REMINDERS =	"reminders";

	public ReminderPreferences(Context context) {
		// TODO Auto-generated constructor stub

		this.context  = context;
		sharedPreferences  = context.getSharedPreferences(PREF_NAME, Activity.MODE_PRIVATE);
		editor= sharedPreferences.edit();


	}


	public void setReminders(String reminders)
	{
		editor.putString(REMINDERS, reminders);
		editor.commit();
	}


	public String getReminders()
	{
		return sharedPreferences.getString(REMINDERS, null);

	}


	public ArrayList<Reminders> getRemindersArrayList()
	{
		ArrayList<Reminders> arrlst = null;
		Reminders[] arr = null;

		String reminders = getReminders();

		if(reminders!=null)
		{
			Gson gson = new Gson();

			arr = gson.fromJson(reminders, Reminders[].class);
		}

		if(arr!=null)
		{
			arrlst = new ArrayList<Reminders>(Arrays.asList(arr));
			return arrlst;
		}
		else
		{
			return null;
		}

	}


	public void setRemindersFromArrayList(ArrayList<Reminders> arrlst)
	{
		if(arrlst!=null)
		{
			Gson gson = new Gson();

			JsonElement jsonElement = gson.toJsonTree(arrlst, new TypeToken<ArrayList<Reminders>>() {}.getType());

			if(jsonElement.isJsonArray())
			{
				JsonArray jsonArray = jsonElement.getAsJsonArray();
				editor.putString(REMINDERS, jsonArray.toString());
				editor.commit();
			}
			else
			{

			}

		}

	}


	public void addNewReminder(Reminders reminders)
	{
		ArrayList<Reminders> arrlst = getRemindersArrayList();

		if(arrlst!=null)
		{
			arrlst.add(reminders);
		}
		else
		{
			arrlst = new ArrayList<Reminders>();
			arrlst.add(reminders);
		}


		setRemindersFromArrayList(arrlst);
	}

	public void updateReminder(Reminders reminders, boolean status)
	{

		int index = 0;
		ArrayList<Reminders> arrlst = getRemindersArrayList();

		System.out.println("-- reminder available in arrlst: " +  arrlst.indexOf(reminders));

		//		if(arrlst.contains(reminders))
		//		{
		//			index = arrlst.indexOf(reminders);
		//			reminders.setActive(status);
		//			arrlst.set(index, reminders);
		//			setRemindersFromArrayList(arrlst);
		//			
		//		}
		//		else
		//		{
		//			System.out.println("-- udpate reminder failed");
		//		}


		for(int i = 0 ; i < arrlst.size(); i++)
		{
			if(reminders.getReminderID().equals(  arrlst.get(i).getReminderID() ))
			{
				index = i;
			}
		}

		reminders.setActive(status);
		arrlst.set(index, reminders);
		setRemindersFromArrayList(arrlst);


	}
	
	
	
	public void deleteReminder(Reminders reminder)
	{
		try {
			int index = 0;
			ArrayList<Reminders> arrlst = getRemindersArrayList();




			for(int i = 0 ; i < arrlst.size(); i++)
			{
				if(reminder.getReminderID().equals(  arrlst.get(i).getReminderID() ))
				{
					index = i;
				}
			}

			arrlst.remove(index);
			setRemindersFromArrayList(arrlst);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public boolean isDuplicate(Reminders reminders)
	
	{
		
		boolean status = false;
		
		
		ArrayList<Reminders> alReminders = getRemindersArrayList();
		
		if(alReminders!=null)
		{
			
			Reminders currReminders;
			for(int i = 0 ; i < alReminders.size(); i++)
			{
				currReminders = alReminders.get(i);
				System.out.println("-- is null currReminders: " + ((currReminders==null) ? "NUll" : "not null"));

				//if(reminders.getTrackerID().equals(currReminders.getTrackerID())   && (reminders.getREMINDER_TYPE() == currReminders.getREMINDER_TYPE())   &&     isSameTime(reminders,currReminders)    )
				if(reminders.getTrackerID().equals(currReminders.getTrackerID())   && (reminders.getREMINDER_TYPE() == currReminders.getREMINDER_TYPE())   &&     isSameTime(reminders,currReminders)   && (reminders.getRoute().equals(currReminders.getRoute()))  )
				{
					return true;
				}
				
				
				
			}
		}
		
		
		
		
		
		
		return status;
	}


	private boolean isSameTime(Reminders reminders2, Reminders currReminders) {
		// TODO Auto-generated method stub
		
		//SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date date1,date2;
		date1 = new Date(reminders2.getReminderTime());
		date2 = new Date(currReminders.getReminderTime());
		
		
		if(date1.getMinutes() == date2.getMinutes() && date1.getDay() == date2.getDay() && date1.getMonth() == date2.getMonth() && date1.getHours() == date2.getHours())
		{
			return true;
		}
		
		
		
		
		
		return false;
	}


}
