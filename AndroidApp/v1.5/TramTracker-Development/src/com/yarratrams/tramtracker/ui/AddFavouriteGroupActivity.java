package com.yarratrams.tramtracker.ui;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.db.FavouriteManager;
import com.yarratrams.tramtracker.objects.FavouritesGroup;
import com.yarratrams.tramtracker.ui.util.FeaturesPreferences;
import com.yarratrams.tramtracker.ui.util.OtherLevelAds;


public class AddFavouriteGroupActivity extends Activity implements OnClickListener{
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_FAVOURITES;
	public static final String INTENT_KEY = "group_info";
	
	private boolean isEditing;
	private FavouritesGroup newGroup;
	private FavouritesGroup previousGroup;
	
	private InputMethodManager imm;
	
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.favourites_add_group_screen);
		
		newGroup = new FavouritesGroup();
		previousGroup = new FavouritesGroup();
		isEditing = false; 
		
		Button saveButton = (Button)findViewById(R.id.save_button);
		saveButton.setOnClickListener(this);
		
		final EditText nameField = (EditText)findViewById(R.id.favourite_form_name);
		nameField.setOnClickListener(this);
		nameField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if(hasFocus){
					getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
				}
			}
		});
		
		nameField.setOnEditorActionListener(new OnEditorActionListener() {        
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if(actionId==EditorInfo.IME_ACTION_DONE || event.getKeyCode() == KeyEvent.KEYCODE_ENTER){
					save();
				}
				return false;
			}
		});
		
		
		
		
		//call ads
		OtherLevelAds.showAds(this, R.id.rich_banner_fragment33,FeaturesPreferences.getAdsFlag(TramTrackerMainActivity.instance));
	}
	
	
	@Override
	public void onClick(View v) {
		EditText nameField = (EditText)findViewById(R.id.favourite_form_name);
		Button saveButton = (Button)findViewById(R.id.save_button);
		if(v == saveButton){
			save();
		}
		else if(v == nameField){
			nameField.clearFocus();
			imm.showSoftInput(nameField, InputMethodManager.SHOW_FORCED);
		}
	}

	private void save(){
		TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_header_save));
		EditText nameField = (EditText)findViewById(R.id.favourite_form_name);
		Editable nameEditable = nameField.getText();
		
		if(nameEditable.length() < 1){
			TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.error_favouritegroup_noname));
			return;
		}
		
		imm.hideSoftInputFromWindow(nameField.getWindowToken(), 0);
		
		newGroup.setName(nameEditable.toString());
		
		FavouriteManager favouriteManager = new FavouriteManager(getApplicationContext());
		
		ArrayList<FavouritesGroup> groups = favouriteManager.getAllFavourites();
		if(groups != null){
			if(isEditing){
				newGroup.setOrder(previousGroup.getOrder());
			} else {
				newGroup.setOrder(groups.size());
			}
		} else {
			newGroup.setOrder(0);
		}
		
		if(isEditing){
			try{
				favouriteManager.editFavouriteGroup(previousGroup, newGroup);
				TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.notify_favouritegroup_saved));
				TramTrackerMainActivity.getAppManager().back();
			}catch (SQLiteConstraintException e) {
				TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.error_favouritegroup_edit_duplication));
			}
		} else {
			try{
				favouriteManager.addFavouriteGroup(newGroup);
				TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.notify_favouritegroup_saved));
				TramTrackerMainActivity.getAppManager().back();
			}catch (SQLiteConstraintException e) {
				TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.error_favouritegroup_create_duplication));
			}
		}
	}
	
	@Override
	protected void onResume() {
		
		imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		EditText nameField = (EditText)findViewById(R.id.favourite_form_name);
		nameField.clearFocus();
		
		Bundle extras = getIntent().getExtras(); 
		if(extras != null){
			previousGroup = extras.getParcelable(INTENT_KEY);
			nameField.setText(previousGroup.getName());
			isEditing = true;
		}
		
		super.onResume();
	}

	@Override
	protected void onPause() {
		EditText nameField = (EditText)findViewById(R.id.favourite_form_name);
		imm.hideSoftInputFromWindow(nameField.getWindowToken(), 0);
		super.onPause();
	}

	
	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return true;
	}
	
	
}
