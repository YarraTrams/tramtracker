package com.yarratrams.tramtracker.ui.util;

import java.util.ArrayList;
import java.util.Arrays;

import org.json.JSONArray;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

public class DisruptionsPreference {


	Context context;
	SharedPreferences sharedPreferences;
	SharedPreferences.Editor editor;

	final String PREF_NAME = "disruptionpref";
	final String ROUTES =	"routes";
	final String TIMMINGS = "timmings";

	public DisruptionsPreference(Context context) {
		// TODO Auto-generated constructor stub

		this.context  = context;
		sharedPreferences  = context.getSharedPreferences(PREF_NAME, Activity.MODE_PRIVATE);
		editor= sharedPreferences.edit();


	}


	
	// handle routes


	public void setRoutes(String routes)
	{
		editor.putString(ROUTES, routes);
		editor.commit();
	}


	public String getRoutes()
	{
		return sharedPreferences.getString(ROUTES, null);

	}
	
	
	public JSONArray getRoutesArr()
	{
		
		ArrayList<String> arrayList = getRoutesArrayList();
		JSONArray jsonArray = new JSONArray(arrayList);
		return jsonArray;
		
		
		
	}
	
	public JSONArray getTimmingsArr()
	{
		ArrayList<String> arrayList = getTimmingsArrayList();
		if(arrayList.contains("3")){
			arrayList.remove("1");
			arrayList.remove("2");
		}
		JSONArray jsonArray = new JSONArray(arrayList);
		return jsonArray;
	}
	
	
	public ArrayList<String> getRoutesArrayList()
	{
		ArrayList<String> arrlst = null;
		String[] arr = null;

		String routes = getRoutes();

		if(routes!=null)
		{
			Gson gson = new Gson();

			arr = gson.fromJson(routes, String[].class);
		}

		if(arr!=null)
		{
			arrlst = new ArrayList<String>(Arrays.asList(arr));
			return arrlst;
		}
		else
		{
			return null;
		}

	}


	public void setRoutesFromArrayList(ArrayList<String> arrlst)
	{
		if(arrlst!=null)
		{
			Gson gson = new Gson();

			JsonElement jsonElement = gson.toJsonTree(arrlst, new TypeToken<ArrayList<String>>() {}.getType());

			if(jsonElement.isJsonArray())
			{
				JsonArray jsonArray = jsonElement.getAsJsonArray();
				editor.putString(ROUTES, jsonArray.toString());
				editor.commit();
			}
			else
			{

			}

		}

	}


	public void addNewRoutes(String routes)
	{
		ArrayList<String> arrlst = getRoutesArrayList();

		if(arrlst!=null)
		{
			arrlst.add(routes);
		}
		else
		{
			arrlst = new ArrayList<String>();
			arrlst.add(routes);
		}


		setRoutesFromArrayList(arrlst);
	}





	public void deleteRoute(String route)
	{
		try {
			int index = 0;
			ArrayList<String> arrlst = getRoutesArrayList();




			for(int i = 0 ; i < arrlst.size(); i++)
			{
				if(route.equals(  arrlst.get(i) ))
				{
					index = i;
				}
			}

			arrlst.remove(index);
			setRoutesFromArrayList(arrlst);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	// handle times
	
	
	
	
	
	public void setTimmings(String timming)
	{
		editor.putString(TIMMINGS, timming);
		editor.commit();
	}


	public String getTimmings()
	{
		return sharedPreferences.getString(TIMMINGS, null);

	}
	
	
	
	public ArrayList<String> getTimmingsArrayList()
	{
		ArrayList<String> arrlst = null;
		String[] arr = null;

		String timmings = getTimmings();

		if(timmings!=null)
		{
			Gson gson = new Gson();

			arr = gson.fromJson(timmings, String[].class);
		}

		if(arr!=null)
		{
			arrlst = new ArrayList<String>(Arrays.asList(arr));
			return arrlst;
		}
		else
		{
			return null;
		}

	}


	public void setTimmingsFromArrayList(ArrayList<String> arrlst)
	{
		if(arrlst!=null)
		{
			Gson gson = new Gson();

			JsonElement jsonElement = gson.toJsonTree(arrlst, new TypeToken<ArrayList<String>>() {}.getType());

			if(jsonElement.isJsonArray())
			{
				JsonArray jsonArray = jsonElement.getAsJsonArray();
				editor.putString(TIMMINGS, jsonArray.toString());
				editor.commit();
			}
			else
			{

			}

		}

	}


	public void addNewTimmings(String timmings)
	{
		ArrayList<String> arrlst = getTimmingsArrayList();

		if(arrlst!=null)
		{
			arrlst.add(timmings);
		}
		else
		{
			arrlst = new ArrayList<String>();
			arrlst.add(timmings);
		}


		setTimmingsFromArrayList(arrlst);
	}





	public void deleteTimmings(String timmings)
	{
		try {
			int index = 0;
			ArrayList<String> arrlst = getTimmingsArrayList();




			for(int i = 0 ; i < arrlst.size(); i++)
			{
				if(timmings.equals(  arrlst.get(i) ))
				{
					index = i;
				}
			}

			arrlst.remove(index);
			setTimmingsFromArrayList(arrlst);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	
	public String removeDestination(String route)
	{
		return route.substring(0, (route.indexOf("-")-1));
	}
	
}
