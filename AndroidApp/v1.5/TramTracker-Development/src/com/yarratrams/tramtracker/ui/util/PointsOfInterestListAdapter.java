package com.yarratrams.tramtracker.ui.util;

import java.util.ArrayList;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.PointOfInterest;


public class PointsOfInterestListAdapter {
	private LinearLayout list;
	private final Activity context;
	private final ArrayList<PointOfInterest> pois;

	public PointsOfInterestListAdapter(LinearLayout list, Activity context, ArrayList<PointOfInterest> pois) {
		this.list = list;
		this.context = context;
		this.pois = pois;
		populate();
	}
	

	public void populate(){
		list.removeAllViews();
		
		View view;
		for(int i = 0; i < pois.size(); i++){
			view = getView(i);
			list.addView(view);
		}
	}
	
	private View getView(final int position) {
		LayoutInflater inflater = context.getLayoutInflater();
		final View rowView = inflater.inflate(R.layout.stop_poi_list_detail, null, true);
		
		PointOfInterest poi = pois.get(position);
		
		TextView poiName = (TextView)rowView.findViewById(R.id.poi_name);
		TextView poiDescription = (TextView)rowView.findViewById(R.id.poi_description);
		poiName.setText(poi.getName());
		poiDescription.setText(poi.getDescription());

		rowView.setFocusable(false);
		rowView.setFocusableInTouchMode(false);
		
		return rowView;
	}
		
}
