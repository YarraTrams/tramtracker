package com.yarratrams.tramtracker.ui.util;

import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.Route;
import com.yarratrams.tramtracker.objects.RouteStopsBySuburb;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.ui.RoutesActivity;
import com.yarratrams.tramtracker.ui.StopActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;


public class RouteStopsExpandableListArrayAdapter extends BaseExpandableListAdapter {
	private static final int TYPE_START = 0;
	private static final int TYPE_MIDDLE = 1;
	private static final int TYPE_FINISH = 2;
	
	private RoutesActivity context;
	private Route route;
	private ArrayList<Stop> stops;
	private ArrayList<RouteStopsBySuburb> suburbs;
	
	/*
	 * FREE TRAM ZONE
	 */
	int ftzCount = 0;
	
	public RouteStopsExpandableListArrayAdapter(RoutesActivity context, Route route, ArrayList<Stop> stops) {
		super();
		this.context = context;
		this.route = route;
		this.stops = stops;
		this.suburbs = new ArrayList<RouteStopsBySuburb>();
		generateSuburbsList();
	}

	public void updateList(Route route, ArrayList<Stop> stops) {
		this.route = route;
		this.stops.clear();
		this.stops.addAll(stops);
		this.suburbs.clear();
		generateSuburbsList();
		super.notifyDataSetChanged();
	}
	
	public void generateSuburbsList(){
		RouteStopsBySuburb suburb;

		for(Stop stop : stops){
			if(suburbs.size() >= 1){
				suburb = suburbs.get(suburbs.size()-1);
				if(!suburb.getSuburb().equalsIgnoreCase(stop.getSuburb())){
					suburb = new RouteStopsBySuburb();
					suburbs.add(suburb);
				}
			} else{
				suburb = new RouteStopsBySuburb();
				suburbs.add(suburb);
			}
			
			suburb.setSuburb(stop.getSuburb());
			suburb.addStop(stop);
		}
	}
	
	public ArrayList<Stop> getStopsInSuburb(int index){
		RouteStopsBySuburb suburb = suburbs.get(index);
		return suburb.getStops(); 
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return getStop(groupPosition, childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return (groupPosition * 100) + childPosition;
	}

	
	static class RouteStopViewHolder{
		TextView stopName;
		TextView poi;
		ImageView routeCBD;
		ImageView routeZone2;
		ImageView connectingTrain;
		ImageView connectingTrams;
		ImageView connectingBuses;
		ImageView routePlatform;
		ImageView routeColor;
		ImageView gotoIcon;
		ImageView ivFreeTramZoneRouteList;
		ImageView ivOnboardFreeTramText;
	}
	
	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
		RouteStopViewHolder viewHolder;
		
		if(convertView == null){
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(R.layout.route_stops_list_view_child, parent, false);
			
			viewHolder = new RouteStopViewHolder();
			viewHolder.stopName = (TextView)convertView.findViewById(R.id.stop_name);
			viewHolder.poi = (TextView)convertView.findViewById(R.id.points_interest);
			viewHolder.routeCBD = (ImageView)convertView.findViewById(R.id.route_cbd);
			viewHolder.routeZone2 = (ImageView)convertView.findViewById(R.id.route_zone2);
			viewHolder.connectingTrain = (ImageView)convertView.findViewById(R.id.connecting_train);
			viewHolder.connectingTrams = (ImageView)convertView.findViewById(R.id.connecting_tram);
			viewHolder.connectingBuses = (ImageView)convertView.findViewById(R.id.connecting_bus);
			viewHolder.routePlatform = (ImageView)convertView.findViewById(R.id.route_platform);
			viewHolder.routeColor = (ImageView)convertView.findViewById(R.id.route_colour);
			viewHolder.gotoIcon = (ImageView)convertView.findViewById(R.id.route_goto);
			
			/*
			 * FREE TRAM ZONE
			 */
			viewHolder.ivFreeTramZoneRouteList = (ImageView)convertView.findViewById(R.id.ivFreeTramZoneRouteList);
			viewHolder.ivOnboardFreeTramText = (ImageView)convertView.findViewById(R.id.ivOnboardFreeTramText);
			
			convertView.setTag(viewHolder);
			
		} else {
			viewHolder = (RouteStopViewHolder) convertView.getTag();
		}
		
		final Stop stop = (Stop) getChild(groupPosition, childPosition);
		
		
		
		/*
		 * FREE TRAM ZONE
		 */
		
		if(stop.getFtzImageId() == 0)
		{
			viewHolder.ivOnboardFreeTramText.setVisibility(View.INVISIBLE);
		}
		else
		{
			viewHolder.ivOnboardFreeTramText.setVisibility(View.VISIBLE);
			viewHolder.ivOnboardFreeTramText.setImageResource(stop.getFtzImageId());
			viewHolder.ivOnboardFreeTramText.setBackgroundColor(Color.TRANSPARENT);
//			viewHolder.ivOnboardFreeTramText.setVisibility(View.VISIBLE);
//			viewHolder.ivOnboardFreeTramText.setImageResource(stop.getFtzImageId());
//			viewHolder.ivOnboardFreeTramText.setBackgroundColor(Color.TRANSPARENT);
//			viewHolder.ivOnboardFreeTramText.setAdjustViewBounds(true);
			//				viewHolder.ivOnboardFreeTramText.setScaleType(ScaleType.)

		}
		
		
		int type = TYPE_MIDDLE;
		if(isLastChild && (groupPosition == suburbs.size()-1)){
			type = TYPE_FINISH;
		} else if(groupPosition == 0 && childPosition == 0){
			type = TYPE_START;
		}
		setRouteColour(viewHolder.routeColor, route.getColour(), type);
		
		
		viewHolder.stopName.setText(getStopDescription(stop));
		if(stop.getPointsOfInterest() == null){
			viewHolder.poi.setVisibility(View.GONE);
		} else {
//			viewHolder.poi.setText(stop.getPointsOfInterest());
			viewHolder.poi.setText(stop.getPointsOfInterest().replace(",",", ")); // replace used to add space after comma
			viewHolder.poi.setVisibility(View.VISIBLE);
		}

		if(stop.getConnectingTrains() != null){
			if(!stop.getConnectingTrains().trim().equalsIgnoreCase("")){
				viewHolder.connectingTrain.setVisibility(View.VISIBLE);
			} else {
				viewHolder.connectingTrain.setVisibility(View.INVISIBLE);
			}
		} else {
			viewHolder.connectingTrain.setVisibility(View.INVISIBLE);
		}
		if(stop.getConnectingTrams() != null){
			if(!stop.getConnectingTrams().trim().equalsIgnoreCase("")){
				viewHolder.connectingTrams.setVisibility(View.VISIBLE);
			} else {
				viewHolder.connectingTrams.setVisibility(View.INVISIBLE);
			}
		} else {
			viewHolder.connectingTrams.setVisibility(View.INVISIBLE);
		}
		if(stop.getConnectingBuses() != null){
			if(!stop.getConnectingBuses().trim().equalsIgnoreCase("")){
				viewHolder.connectingBuses.setVisibility(View.VISIBLE);
			} else {
				viewHolder.connectingBuses.setVisibility(View.INVISIBLE);
			}
		} else {
			viewHolder.connectingBuses.setVisibility(View.INVISIBLE);
		}

		if(stop.isEasyAccessStop()){
			viewHolder.routePlatform.setVisibility(View.VISIBLE);
		} else {
			viewHolder.routePlatform.setVisibility(View.INVISIBLE);
		}
		
//		if(stop.isCityStop()){
		
		/*
		 * FREE TRAM ZONE
		 */
		if(stop.IsInFreeZone()){
			viewHolder.routeCBD.setVisibility(View.VISIBLE);
			viewHolder.routeZone2.setVisibility(View.INVISIBLE);
			viewHolder.ivFreeTramZoneRouteList.setVisibility(View.VISIBLE);
		} else {
			if(stop.getZone() == null || stop.getZone().equalsIgnoreCase("null"))
				stop.setZone("1");
			if(stop.getZone().contains("1") && !stop.getZone().contains("2")){
				viewHolder.routeCBD.setVisibility(View.INVISIBLE);
				/*
				 * Adil Changed
				 */
				//viewHolder.routeZone2.setVisibility(View.VISIBLE);
				viewHolder.routeZone2.setVisibility(View.INVISIBLE);
				viewHolder.ivFreeTramZoneRouteList.setVisibility(View.INVISIBLE);
			} else if(stop.getZone().contains("2")){
				viewHolder.routeCBD.setVisibility(View.INVISIBLE);
				/*
				 * Adil Changed
				 */
				//viewHolder.routeZone2.setVisibility(View.INVISIBLE);
				viewHolder.routeZone2.setVisibility(View.VISIBLE);
				viewHolder.ivFreeTramZoneRouteList.setVisibility(View.INVISIBLE);
			}
		}
		
		viewHolder.gotoIcon.setVisibility(View.VISIBLE);
		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				TramTrackerMainActivity.getAppManager().callSelection(getStopDescription(stop));
				goToStopScreen(stop);
			}
		});
		
		convertView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				v.setPressed(true);
				
				return false;
			}
		});
		
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return getStopsInSuburb(groupPosition).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return suburbs.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return suburbs.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	
	static class SuburbViewHolder{
		TextView suburbName;
	}
	
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		SuburbViewHolder viewHolder;
		
		if(convertView == null){
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(R.layout.route_stops_list_view_group, parent, false);
			
			viewHolder = new SuburbViewHolder();
			viewHolder.suburbName = (TextView) convertView.findViewById(R.id.group_name);
			convertView.setTag(viewHolder);
			
		} else {
			viewHolder = (SuburbViewHolder) convertView.getTag();
		}
			
		RouteStopsBySuburb suburb = (RouteStopsBySuburb) getGroup(groupPosition);
		viewHolder.suburbName.setText(suburb.getSuburb());
		
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

    @Override
    public boolean areAllItemsEnabled(){
        return true;
    }
	
    
	private void goToStopScreen(Stop stop){
		Intent intent = new Intent(this.context, StopActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(StopActivity.INTENT_STOP_KEY, stop);
		
		TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_ROUTES, 
				context.getResources().getString(R.string.tag_stop_screen), intent);
	}

	
	private Stop getStop(int groupPosition, int childPosition){
		ArrayList<Stop> suburb = getStopsInSuburb(groupPosition); 
		return suburb.get(childPosition);
	}
	
	
	private String getStopDescription(Stop stop){
		String text = "";
		
		text = text.concat(String.valueOf(stop.getStopNumber()));
		//phoenix changed 20.Jun.2014
		text = text.concat(": ");
//		text = text.concat(context.getResources().getString(R.string.stop_name_colon));
		if(stop.getStopName() == null )
			stop.setStopName("abc");
		text = text.concat(stop.getStopName());
		
		return text;
	}

	
	private void setRouteColour(ImageView routeColor, String colour, int type){
		int resourceID = 0;
		if(colour == null)
			colour = "GRAY";
		if(colour.equalsIgnoreCase("GREEN")){
			if(type == TYPE_START){
				resourceID = R.drawable.icn_route_green_start;
			} else if(type == TYPE_FINISH){
				resourceID = R.drawable.icn_route_green_end;
			} else {
				resourceID = R.drawable.icn_route_green_mid;
			}
		} else if(colour.equalsIgnoreCase("CYAN")){
			if(type == TYPE_START){
				resourceID = R.drawable.icn_route_cyan_start;
			} else if(type == TYPE_FINISH){
				resourceID = R.drawable.icn_route_cyan_end;
			} else {
				resourceID = R.drawable.icn_route_cyan_mid;
			}
		} else if(colour.equalsIgnoreCase("YELLOW")){
			if(type == TYPE_START){
				resourceID = R.drawable.icn_route_yellow_start;
			} else if(type == TYPE_FINISH){
				resourceID = R.drawable.icn_route_yellow_end;
			} else {
				resourceID = R.drawable.icn_route_yellow_mid;
			}
		} else if(colour.equalsIgnoreCase("PINK")){
			if(type == TYPE_START){
				resourceID = R.drawable.icn_route_pink_start;
			} else if(type == TYPE_FINISH){
				resourceID = R.drawable.icn_route_pink_end;
			} else {
				resourceID = R.drawable.icn_route_pink_mid;
			}
		} else if(colour.equalsIgnoreCase("ORANGE")){
			if(type == TYPE_START){
				resourceID = R.drawable.icn_route_orange_start;
			} else if(type == TYPE_FINISH){
				resourceID = R.drawable.icn_route_orange_end;
			} else {
				resourceID = R.drawable.icn_route_orange_mid;
			}
		} else if(colour.equalsIgnoreCase("TEAL")){
			if(type == TYPE_START){
				resourceID = R.drawable.icn_route_teal_start;
			} else if(type == TYPE_FINISH){
				resourceID = R.drawable.icn_route_teal_end;
			} else {
				resourceID = R.drawable.icn_route_teal_mid;
			}
		} else {
			if(type == TYPE_START){
				resourceID = R.drawable.icn_route_grey_start;
			} else if(type == TYPE_FINISH){
				resourceID = R.drawable.icn_route_grey_end;
			} else {
				resourceID = R.drawable.icn_route_grey_mid;
			}
		}
		routeColor.setImageResource(resourceID);
	}
}
