package com.yarratrams.tramtracker.ui.util;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.MultiPageUrlModel;

public class MultiPageAdapter extends PagerAdapter {

	Activity activity;
	ArrayList<MultiPageUrlModel> alPageUrlModels;

	

	public MultiPageAdapter(Activity activity,
			ArrayList<MultiPageUrlModel> alPageUrlModels) {
		// TODO Auto-generated constructor stub
		this.activity = activity;
		this.alPageUrlModels = alPageUrlModels;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return alPageUrlModels.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		// TODO Auto-generated method stub
		return view == ((LinearLayout) object);
	}

	@Override
	public Object instantiateItem(ViewGroup container, final int position) {

		LayoutInflater layoutInflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View viewLayout = layoutInflater.inflate(R.layout.multipage_cell,
				container, false);

		final WebView wvTutorialUrl = (WebView) viewLayout.findViewById(R.id.wvTutorialUrl);
		final ProgressBar pbWebView = (ProgressBar) viewLayout
				.findViewById(R.id.pbWebViewMultiPage);
		// pbWebView.setVisibility(View.VISIBLE);

		try {
			wvTutorialUrl.setWebViewClient(new WebViewClient() {
				@Override
		        public void onPageFinished(WebView view, String url) {
					pbWebView.setVisibility(View.GONE);

		        }
			});
			wvTutorialUrl.loadUrl(alPageUrlModels.get(position).getUrl());
			wvTutorialUrl.getSettings().setJavaScriptEnabled(true);
			wvTutorialUrl.getSettings().setLayoutAlgorithm(
					LayoutAlgorithm.SINGLE_COLUMN);
			wvTutorialUrl.setHorizontalScrollBarEnabled(false);

		} catch (Exception e) {
			// TODO: handle exception
		}

		((ViewPager) container).addView(viewLayout);

		return viewLayout;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		// ((ViewPager) container).removeView((LinearLayout) object);

	}

}
