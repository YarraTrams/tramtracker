package com.yarratrams.tramtracker.ui;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.ServiceChangeRSSItem;
import com.yarratrams.tramtracker.tasks.ServiceChangesFromWebServiceTask;
import com.yarratrams.tramtracker.ui.util.FeaturesPreferences;
import com.yarratrams.tramtracker.ui.util.OtherLevelAds;
import com.yarratrams.tramtracker.ui.util.ServiceFeedListArrayAdapter;


public class ServiceFeedActivity extends Activity {
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_MORE;
	
	private ListView feedList;
	private ServiceFeedListArrayAdapter feedAdapter;
	private ArrayList<ServiceChangeRSSItem> feed;

	private ProgressDialog loadDialog;
	
	private int listIndex;
	private int listTop;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.service_feed_screen);
		
		feed = new ArrayList<ServiceChangeRSSItem>();
		feedList = (ListView) findViewById(R.id.simple_list);
		
		feedAdapter = new ServiceFeedListArrayAdapter(this, feed);
		feedList.setAdapter(feedAdapter);
		
		listIndex = 0;
		listTop = 0;
		
        loadDialog = new ProgressDialog(this);

		OtherLevelAds.showAds(this, R.id.rich_banner_fragment1025,FeaturesPreferences.getAdsFlag(TramTrackerMainActivity.instance));
	}
	
	public Context getDialogContext() {
		Context context;
		if (getParent() != null) {
			context = getParent();
		} else {
			context = this;
		}
		return context;
	}

	
	@Override
	protected void onStart() {
		super.onStart();
		retrieveFeed();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		if(feed != null){
			if(!feed.isEmpty()){
				feedList.setSelectionFromTop(listIndex, listTop);
				return;
			}
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		
		listIndex = feedList.getFirstVisiblePosition();
		View v = feedList.getChildAt(0);
		listTop = (v == null)? 0 : v.getTop();
	}
	
	public void retrieveFeed(){
		if(!loadDialog.isShowing()){
			loadDialog = ProgressDialog.show(getDialogContext(), "", getResources().getString(R.string.dialog_loading), true, true);
		}
		
		ServiceChangesFromWebServiceTask feedTask = new ServiceChangesFromWebServiceTask(this);
		feedTask.execute();
	}
	
	public void updateUI(ArrayList<ServiceChangeRSSItem> newFeed){
		if(newFeed != null){
			feed = newFeed;
			feedAdapter.updateList(feed);
			feedList.setAdapter(feedAdapter);
		}
		if(loadDialog.isShowing()){
			loadDialog.dismiss();
		}
	}
	
	
	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}
	
}
