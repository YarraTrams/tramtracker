package com.yarratrams.tramtracker.ui.util;

import java.text.DecimalFormat;
import java.util.ArrayList;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.NearbyTicketOutlet;
import com.yarratrams.tramtracker.objects.TicketOutlet;
import com.yarratrams.tramtracker.ui.NearbyActivity;
import com.yarratrams.tramtracker.ui.OutletActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;

public class NearbyOutletListArrayAdapter extends ArrayAdapter<NearbyTicketOutlet> {
	private final NearbyActivity context;
	private final ArrayList<NearbyTicketOutlet> outlets;

	public NearbyOutletListArrayAdapter(NearbyActivity context, ArrayList<NearbyTicketOutlet> outlets) {
		super(context, R.layout.nearby_list_view_detail, outlets);
		this.context = context;
		this.outlets = outlets;
	}

	public void updateList(ArrayList<NearbyTicketOutlet> outlets) {
		this.outlets.clear();
		this.outlets.addAll(outlets);
		super.notifyDataSetChanged();
	}
	
	static class OutletViewHolder{
		TextView name;
		TextView address;
		TextView distance;
//		ImageView ivNearbyFreeTramZone;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		OutletViewHolder viewHolder;
		
		if(convertView == null){
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(R.layout.nearby_list_view_detail, parent, false);
			
			viewHolder = new OutletViewHolder();
			viewHolder.name = (TextView)convertView.findViewById(R.id.stop_name);
			viewHolder.address = (TextView)convertView.findViewById(R.id.stop_routes);
			viewHolder.distance = (TextView)convertView.findViewById(R.id.stop_meters);
//			viewHolder.ivNearbyFreeTramZone = (ImageView)convertView.findViewById(R.id.ivNearbyFreeTramZone);
			
			convertView.setTag(viewHolder);
		
		} else {
			viewHolder = (OutletViewHolder) convertView.getTag();
		}
		
		final NearbyTicketOutlet nearbyOutlet = outlets.get(position);
		final TicketOutlet outlet = nearbyOutlet.getTicketOutlet();
		
		viewHolder.name.setText(outlet.getName());
		viewHolder.address.setText(getFullAddress(outlet));
		viewHolder.distance.setText(getDistance(nearbyOutlet));
		
		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				TramTrackerMainActivity.getAppManager().callSelection(outlet.getName());
				goToOutletScreen(position);
			}
		});
		
		convertView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				v.setPressed(true);
				
				return false;
			}
		});
		
		return convertView;
	}
	
	public String getFullAddress(TicketOutlet outlet){
		String text = "";
		
		text = outlet.getAddress();

		return text;
	}
	
	
	private void goToOutletScreen(int position){
		Intent intent = new Intent(this.context, OutletActivity.class);
		TicketOutlet outlet = outlets.get(position).getTicketOutlet();
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(OutletActivity.INTENT_KEY, outlet);
		
		TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_NEARBY, 
				context.getResources().getString(R.string.tag_outlet_screen), intent);
	}
	
	
	private String getDistance(NearbyTicketOutlet outlet){
		String text = "";
		
		if(outlet.getIntDistance() < 1000){
			text = String.valueOf(outlet.getIntDistance());
			text = text.concat(context.getResources().getString(R.string.ticketOutlets_distance_meters));
		} else {
			double distance = (double)outlet.getIntDistance()/1000;
			DecimalFormat df = new DecimalFormat("##.0");
			text = df.format(distance);
			text = text.concat(context.getResources().getString(R.string.ticketOutlets_distance_kilometers));
		}
		
		return text;
	}
}
