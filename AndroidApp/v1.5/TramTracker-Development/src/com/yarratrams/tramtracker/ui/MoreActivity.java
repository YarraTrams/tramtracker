package com.yarratrams.tramtracker.ui;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.ui.util.FeaturesPreferences;
import com.yarratrams.tramtracker.ui.util.OtherLevelAds;

public class MoreActivity extends Activity implements OnClickListener, OnTouchListener{
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_MORE;
	Activity activity;

	public int googlePlayServiceAvailablityCode;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.more_screen);

		activity = this;

		RelativeLayout trackerButton = (RelativeLayout) findViewById(R.id.more_tracker);
		RelativeLayout feedbackButton = (RelativeLayout) findViewById(R.id.more_feedback);
		RelativeLayout timetableButton = (RelativeLayout) findViewById(R.id.more_timetables);
		RelativeLayout serviceFeedButton = (RelativeLayout) findViewById(R.id.more_servicefeed);
		RelativeLayout twitterButton = (RelativeLayout) findViewById(R.id.more_twitter);
		RelativeLayout searchButton = (RelativeLayout) findViewById(R.id.more_search);
		RelativeLayout entryButton = (RelativeLayout) findViewById(R.id.more_entryscreen);
		RelativeLayout helpButton = (RelativeLayout) findViewById(R.id.more_help);
		RelativeLayout outletsButton = (RelativeLayout) findViewById(R.id.more_outlets);
		RelativeLayout updateButton = (RelativeLayout) findViewById(R.id.more_update);

		/*
		 * Free tram zone
		 */

		RelativeLayout freetramButton = (RelativeLayout) findViewById(R.id.more_freetramzone);


		/*
		 * Adil Added
		 */

		RelativeLayout privacyButton = (RelativeLayout) findViewById(R.id.more_privacy);
		RelativeLayout specialOfferButton = (RelativeLayout) findViewById(R.id.more_specialoffer);
		RelativeLayout ptvButton = (RelativeLayout) findViewById(R.id.more_ptv);
		RelativeLayout settingsButton = (RelativeLayout) findViewById(R.id.more_settings);
		RelativeLayout morePlayServicesButton = (RelativeLayout) findViewById(R.id.more_play_services);
		ImageView dividerMorePlayService = (ImageView) findViewById(R.id.divider_more_playservvices);
		ImageView dividerSpecialoffer = (ImageView) findViewById(R.id.divider_more_specialoffer);

		trackerButton.setOnClickListener(this);
		feedbackButton.setOnClickListener(this);
		timetableButton.setOnClickListener(this);
		serviceFeedButton.setOnClickListener(this);
		twitterButton.setOnClickListener(this);
		searchButton.setOnClickListener(this);
		entryButton.setOnClickListener(this);
		helpButton.setOnClickListener(this);
		outletsButton.setOnClickListener(this);
		updateButton.setOnClickListener(this);

		/*
		 * Free tram zone
		 */
		freetramButton.setOnClickListener(this);
		/*
		 * Adil Added
		 */

		privacyButton.setOnClickListener(this);
		ptvButton.setOnClickListener(this);
		specialOfferButton.setOnClickListener(this);
		settingsButton.setOnClickListener(this);
		morePlayServicesButton.setOnClickListener(this);
		
		

		trackerButton.setOnTouchListener(this);
		feedbackButton.setOnTouchListener(this);
		timetableButton.setOnTouchListener(this);
		serviceFeedButton.setOnTouchListener(this);
		twitterButton.setOnTouchListener(this);
		searchButton.setOnTouchListener(this);
		entryButton.setOnTouchListener(this);
		helpButton.setOnTouchListener(this);
		outletsButton.setOnTouchListener(this);
		updateButton.setOnTouchListener(this);

		/*
		 * Free tram zone
		 */
		freetramButton.setOnTouchListener(this);

		/*
		 * Adil Added
		 */

		privacyButton.setOnTouchListener(this);
		specialOfferButton.setOnTouchListener(this);
		ptvButton.setOnTouchListener(this);
		settingsButton.setOnTouchListener(this);
		morePlayServicesButton.setOnTouchListener(this);
		
		
		//call ads
		OtherLevelAds.showAds(activity, R.id.rich_banner_fragment77,FeaturesPreferences.getAdsFlag(TramTrackerMainActivity.instance));
		
		
		//hide special offers if lower version then ics
		if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)
		{
			specialOfferButton.setVisibility(View.GONE);
			dividerSpecialoffer.setVisibility(View.GONE);
			
		}
		
		if(isGooglePlayServicesAvailable())
		{
			morePlayServicesButton.setVisibility(View.GONE);
			dividerMorePlayService.setVisibility(View.GONE);
		}	
		
		
		
		if(FeaturesPreferences.getAdsFlag(TramTrackerMainActivity.instance)==false)
		{
			specialOfferButton.setVisibility(View.GONE);
			dividerSpecialoffer.setVisibility(View.GONE);
		}
		
	}		


	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}


	@Override
	public void onClick(View v) {
		Intent intent;
		RelativeLayout trackerButton = (RelativeLayout) findViewById(R.id.more_tracker);
		RelativeLayout feedbackButton = (RelativeLayout) findViewById(R.id.more_feedback);
		RelativeLayout timetableButton = (RelativeLayout) findViewById(R.id.more_timetables);
		RelativeLayout serviceFeedButton = (RelativeLayout) findViewById(R.id.more_servicefeed);
		RelativeLayout twitterButton = (RelativeLayout) findViewById(R.id.more_twitter);
		RelativeLayout searchButton = (RelativeLayout) findViewById(R.id.more_search);
		RelativeLayout entryButton = (RelativeLayout) findViewById(R.id.more_entryscreen);
		RelativeLayout helpButton = (RelativeLayout) findViewById(R.id.more_help);
		RelativeLayout outletsButton = (RelativeLayout) findViewById(R.id.more_outlets);
		RelativeLayout updateButton = (RelativeLayout) findViewById(R.id.more_update);

		//free tram zone
		RelativeLayout freetramButton = (RelativeLayout) findViewById(R.id.more_freetramzone);

		//Adil Added
		RelativeLayout privacyButton = (RelativeLayout) findViewById(R.id.more_privacy);
		RelativeLayout specialOfferButton = (RelativeLayout) findViewById(R.id.more_specialoffer);
		RelativeLayout ptvButton = (RelativeLayout) findViewById(R.id.more_ptv);
		RelativeLayout settingsButton = (RelativeLayout) findViewById(R.id.more_settings);
		RelativeLayout morePlayServicesButton = (RelativeLayout) findViewById(R.id.more_play_services);


		if(v == trackerButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.more_tracker));
			intent = new Intent(this, SearchTrackerIDActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, 
					getResources().getString(R.string.tag_trackerid_screen), intent);

		} else if(v == feedbackButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.more_feedback));
			intent = new Intent(this, FeedbackActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, 
					getResources().getString(R.string.tag_feedback_screen), intent);

		} else if(v == timetableButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.more_timetables));
			intent = new Intent(this, TimetableRoutesActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, 
					getResources().getString(R.string.tag_timetable_routes_screen), intent);

		} else if(v == serviceFeedButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.more_servicefeed));
			intent = new Intent(this, ServiceFeedActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, 
					getResources().getString(R.string.tag_service_feed_screen), intent);

		} else if(v == twitterButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.more_twitter));
			intent = new Intent(Intent.ACTION_VIEW);
			intent.setType("application/twitter");
			intent.setData(Uri.parse(getString(R.string.twitter_url_base)));
			startActivity(intent);

		} else if(v == searchButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.more_search));
			intent = new Intent(this, SearchMainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, 
					getResources().getString(R.string.tag_search_main_screen), intent);

		} else if(v == entryButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.more_entryscreen));
			intent = new Intent(this, DefaultEntryActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, 
					getResources().getString(R.string.tag_defaultentry_screen), intent);

		} else if(v == helpButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.more_help));
			intent = new Intent(this, HelpActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, 
					getResources().getString(R.string.tag_help_screen), intent);

		} else if(v == outletsButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.more_outlets));
			intent = new Intent(this, TicketOutletsListActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, 
					getResources().getString(R.string.tag_ticketoutlets_list_screen), intent);

		} else if(v == updateButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.more_update));
			intent = new Intent(this, UpdateActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, 
					getResources().getString(R.string.tag_update_screen), intent);
		}
		//free tram zone
		else if(v == freetramButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.more_freetramzone));
			intent = new Intent(this, FTZActicity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, 
					getResources().getString(R.string.tag_ftz_screen), intent);




		}
		/*
		 * Adil Added
		 */
		else if(v == privacyButton){

			intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.privacy_policy_url)));
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);




		}
		else if(v == specialOfferButton){

			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.more_specialoffer));
			intent = new Intent(this, SpecialOffersActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, 
					getResources().getString(R.string.tag_special_offer_screen), intent);




		}
		else if(v == ptvButton){

			intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.ptv_url)));
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);

		}
		
		else if(v == settingsButton){
			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.more_settings));
			intent = new Intent(this, SettingsActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, 
					getResources().getString(R.string.tag_settings_screen), intent);




		}
		else if(v == morePlayServicesButton){
//			TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.more_settings));
//			intent = new Intent(this, SettingsActivity.class);
//			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//			TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, 
//					getResources().getString(R.string.tag_settings_screen), intent);
			getGooglePlayServices();




		}

		/*
		 * ENDS
		 */



	}

	@Override
	protected void onResume() { 
		super.onResume();

	}


	@Override
	public boolean onTouch(View v, MotionEvent event) {
		v.setPressed(true);
		return false;
	}
	
	public void getGooglePlayServices()
	{
		 Dialog dialog = GooglePlayServicesUtil.getErrorDialog(googlePlayServiceAvailablityCode, TramTrackerMainActivity.instance, 99150);
         if (dialog != null) {
             dialog.show();
         }
	}

	
	public boolean isGooglePlayServicesAvailable()
	{
		googlePlayServiceAvailablityCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		
		if(googlePlayServiceAvailablityCode == ConnectionResult.SUCCESS)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	

}
