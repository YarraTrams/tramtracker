package com.yarratrams.tramtracker.ui;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.db.TTDBUpdate;
import com.yarratrams.tramtracker.objects.Constants;
import com.yarratrams.tramtracker.objects.NetworkMap;
import com.yarratrams.tramtracker.singleton.TTDBUpdateSingleton;
import com.yarratrams.tramtracker.tasks.UpdateFeatureTask;
import com.yarratrams.tramtracker.ui.util.MultiPagePreferences;
import com.yarratrams.tramtracker.ui.util.NetworkMapPreferences;
import com.yarratrams.tramtracker.ui.util.TutorialPreferences;
import com.yarratrams.tramtracker.ui.util.UpdateDataPreference;

/*
 * Added by Adil
 */

public class SplashScreenActivity extends Activity {

	// variables
	private static int SPLASH_TIME_OUT = 2300;
	Activity activity;
	Handler splashHandler;

	boolean result = true;

	UpdateDataPreference updateDataPreference;
	PerformUpdateTask performUpdateTask;

	private SharedPreferences appSharedPrefs;
	private Editor prefsEditor;

	TTDBUpdate ttdbUpdate;

	JSONArray jarrUdateNetworkMaps;
	NetworkMapPreferences networkMapPreferences;
	ArrayList<NetworkMap> arrlstNetworkMaps;
	boolean flag = false;
	MultiPagePreferences multiPagePreferences;

	boolean feature_got = false;
	int get_feature_tries = 0;

	// ui elements

	ImageView imgSplash;
	LinearLayout layLoading;

	Bitmap bitmap;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_screen);
		activity = this;

		ttdbUpdate = TTDBUpdateSingleton.getTTDBUpdateInstance(activity);

		initUI();

		// check is there is need to update
		// checkUpdateData();

		performUpdateTask = new PerformUpdateTask();
		performUpdateTask.execute();

		// setting app to display multi page dialog if user opens it for first
		// time if they select remind me later
		// multiPagePreferences = new MultiPagePreferences(activity);
		// multiPagePreferences.setIsFirstTime(true);
		// multiPagePreferences.setIsNewAvailable(true);

		// setup preference to update date

		appSharedPrefs = activity.getSharedPreferences(
				Constants.kSharedPrefIdentifier, Activity.MODE_PRIVATE);
		networkMapPreferences = new NetworkMapPreferences(activity);
		System.out.println("-- network saved maps: "
				+ networkMapPreferences.getNetworkMaps());
		prefsEditor = appSharedPrefs.edit();
		disableTutorialByDefault();
		new UpdateFeatureTask(activity) {
			@Override
			protected void onPostExecute(Void result) {
				super.onPostExecute(result);
				feature_got = true;
			}
		}.execute();
	}

	private void disableTutorialByDefault()
	{
		TutorialPreferences tutorialPre = new TutorialPreferences(this);
		if(!tutorialPre.getManaualSet())
		 tutorialPre.disableAll();
	}
	
	private void splashHandler() {
		get_feature_tries = 0;
		splashHandler = new Handler();
		Runnable r = new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (feature_got || get_feature_tries > 2 ) {
					toMainActivity();
				} else {
					get_feature_tries ++;
					splashHandler.postDelayed(this, SPLASH_TIME_OUT);
				}
			}
		};
		splashHandler.postDelayed(r, SPLASH_TIME_OUT);
	}

	private void initUI() {
		// TODO Auto-generated method stub

		if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			imgSplash = (ImageView) findViewById(R.id.imgSplash);

			imgSplash.setBackgroundResource(R.drawable.splash_animation);
			AnimationDrawable splashAnimation = (AnimationDrawable) imgSplash
					.getBackground();

			splashAnimation.start();
		} else {
			imgSplash = (ImageView) findViewById(R.id.imgSplash);

			imgSplash.setBackgroundResource(R.drawable.splash_animation);
			final AnimationDrawable splashAnimation = (AnimationDrawable) imgSplash
					.getBackground();
			imgSplash.post(new Runnable() {
				public void run() {
					splashAnimation.start();
				}
			});
		}

		layLoading = (LinearLayout) findViewById(R.id.layLoading);
		layLoading.setVisibility(View.INVISIBLE);

	}

	public void checkUpdateData() {
		JSONObject jsonMain = null;
		JSONArray jsonArrUpdatedStops, jsonArrDeletedStops, jsonArrUpdateRoutes, jsonArrDeleteRoutes, jsonArrUpdateTicketOutlets, jsonArrDeletedTicketOutels, jsonArrUpdatePOI, jsonArrDeletdPOI, jsonArrAddNetworkMaps;

		updateDataPreference = new UpdateDataPreference(activity);

//		if (updateDataPreference.getIsUpdateAvailable()) {
			try {
				jsonMain = new JSONObject(updateDataPreference.getUpdateData());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (jsonMain != null) {

				try {

					// ROUTES
					// check for routes update
					if (jsonMain.getJSONArray("updatedRoutes").length() > 0) {
						jsonArrUpdateRoutes = new JSONArray(jsonMain
								.getJSONArray("updatedRoutes").toString());

						// System.out.println("-- updating routes");

						result = ttdbUpdate.updateRoutes(jsonArrUpdateRoutes);
					}

					// check for routes to delete
					if (jsonMain.getJSONArray("deletedRoutes").length() > 0) {
						jsonArrDeleteRoutes = new JSONArray(jsonMain
								.getJSONArray("deletedRoutes").toString());

						System.out.println("-- deleting routes");

						result = ttdbUpdate.deleteRoutes(jsonArrDeleteRoutes);
					}

					// STOPS
					// check for update stops array
					if (jsonMain.getJSONArray("updatedStops").length() > 0) {
						jsonArrUpdatedStops = new JSONArray(jsonMain
								.getJSONArray("updatedStops").toString());

						System.out.println("-- updating stops");

						result = ttdbUpdate.updateStops(jsonArrUpdatedStops);

					}

					// check for stops to delete
					if (jsonMain.getJSONArray("deletedStops").length() > 0) {
						jsonArrDeletedStops = new JSONArray(jsonMain
								.getJSONArray("deletedStops").toString());

						System.out.println("-- deleting stops");

						result = ttdbUpdate.deleteStops(jsonArrDeletedStops);
					}

					// TICKET OUTLETS
					// check for ticket outlets update
					if (jsonMain.getJSONArray("updatedTicketOutlets").length() > 0) {
						jsonArrUpdateTicketOutlets = new JSONArray(jsonMain
								.getJSONArray("updatedTicketOutlets")
								.toString());

						System.out.println("-- updating ticket outlets");

						result = ttdbUpdate
								.updateTicketOutlets(jsonArrUpdateTicketOutlets);

					}

					// check for ticket outlets to delete

					if (jsonMain.getJSONArray("deletedTicketOutlets").length() > 0) {
						jsonArrDeletedTicketOutels = new JSONArray(jsonMain
								.getJSONArray("deletedTicketOutlets")
								.toString());

						System.out.println("-- deleting ticket outlets");

						result = ttdbUpdate
								.deleteTicketOutlets(jsonArrDeletedTicketOutels);
					}

					// POI
					// check for pot update
					if (jsonMain.getJSONArray("updatedPoi").length() > 0) {
						jsonArrUpdatePOI = new JSONArray(jsonMain.getJSONArray(
								"updatedPoi").toString());

						System.out.println("-- updating poi");

						result = ttdbUpdate.updatePOI(jsonArrUpdatePOI);

						System.out.println("-- poi update/insert result: "
								+ result);
					}

					// check for ticket outlets to delete
					if (jsonMain.getJSONArray("deletedPoi").length() > 0) {
						jsonArrDeletdPOI = new JSONArray(jsonMain.getJSONArray(
								"deletedPoi").toString());

						System.out.println("-- deleting poi");

						result = ttdbUpdate.deletePOI(jsonArrDeletdPOI);
					}

					// NETWORK MAPS
					// add network maps
					if (jsonMain.getJSONArray("addedNetworkMaps").length() > 0) {
						jsonArrAddNetworkMaps = new JSONArray(jsonMain
								.getJSONArray("addedNetworkMaps").toString());

						System.out.println("-- adding network maps");

						deleteOldMapsandData(jsonArrAddNetworkMaps);

						storeNetworkMaps(jsonArrAddNetworkMaps);

					}

					System.out.println("-- result: " + result);

					// ********Turn on for date saving when update compelted
					if (result) {
						Calendar calNow = Calendar.getInstance();
						calNow.add(Calendar.DAY_OF_MONTH, 1);
						prefsEditor.putLong(Constants.kLastUpdateDate,
								calNow.getTimeInMillis());

						prefsEditor.commit();

						System.out.println("-- updated date : "
								+ appSharedPrefs.getLong(
										Constants.kLastUpdateDate, 0));
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println("-- error here 101: " + e.getMessage());
				}
			}

//		}
	}

	private void deleteOldMapsandData(JSONArray jsonArrAddNetworkMaps) {
		// TODO Auto-generated method stub

		networkMapPreferences = new NetworkMapPreferences(activity);

		boolean isMapToDel;

		if (networkMapPreferences.getNetworkMaps().length() > 1) {
			ArrayList<NetworkMap> arrlstNetworkMaps = networkMapPreferences
					.getNetworkMapList();

			for (int i = 0; i < arrlstNetworkMaps.size(); i++) {
				isMapToDel = checkMapInUpdateArray(arrlstNetworkMaps.get(i),
						jsonArrAddNetworkMaps);
				if (isMapToDel) {
					System.out.println("-- delete: "
							+ arrlstNetworkMaps.get(i).getFileName());
					System.out.println("-- detlete file: "
							+ activity.deleteFile(arrlstNetworkMaps.get(i)
									.getFileName()));
					arrlstNetworkMaps.remove(i);
					i--;
				}
			}

		} else {

		}

	}

	private boolean checkMapInUpdateArray(NetworkMap networkMap,
			JSONArray jsonArrAddNetworkMaps) {
		// TODO Auto-generated method stub
		boolean isNotAvailable = true;

		for (int i = 0; i < jsonArrAddNetworkMaps.length(); i++) {
			try {
				if (jsonArrAddNetworkMaps.getJSONObject(i)
						.getString("fileName")
						.equalsIgnoreCase(networkMap.getFileName())) {
					isNotAvailable = false;
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return isNotAvailable;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
	}

	private void toMainActivity() {
		Intent intent = new Intent(activity, TramTrackerMainActivity.class);
		// intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		// intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		startActivity(intent);
		// overridePendingTransition (0, 0);
		activity.overridePendingTransition(android.R.anim.fade_in,
				android.R.anim.fade_out);

		// activity.finish();
	}

	public class PerformUpdateTask extends AsyncTask<String, String, Boolean> {
		public PerformUpdateTask() {
			if(new UpdateDataPreference(activity).getIsUpdateAvailable()){
				layLoading.setVisibility(View.VISIBLE);
			}
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			// TODO Auto-generated method stub
			checkUpdateData();
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			layLoading.setVisibility(View.INVISIBLE);
			new UpdateDataPreference(activity).setIsUpdateAvailable(false);
			new UpdateDataPreference(activity).setIsUpdateCompleted(false);
			splashHandler();

		}
	}

	/*
	 * Netework Map methods, taken from UpdateManager class and shifted here
	 */

	public void storeNetworkMaps(JSONArray jsonArrAddNetworkMaps) {

		jarrUdateNetworkMaps = jsonArrAddNetworkMaps;

		try {
			networkMapPreferences = new NetworkMapPreferences(activity);

			if (networkMapPreferences.getNetworkMaps().length() < 1) {

				Log.v("TT", "in if");

				networkMapPreferences.setNetworkMapList(jarrUdateNetworkMaps
						.toString());
				arrlstNetworkMaps = networkMapPreferences.getNetworkMapList();

				Log.v("TT", "saved arrlst: " + arrlstNetworkMaps.size());
				Log.v("TT",
						"saved data: " + networkMapPreferences.getNetworkMaps());

				if (arrlstNetworkMaps.size() == 1
						&& arrlstNetworkMaps.get(0).getFileName()
								.equalsIgnoreCase(NetworkMapActivity.bundleMap)) {
					flag = false;
				} else {
					flag = true;
				}
			} else {
				Log.v("TT", "in else");
				ArrayList<NetworkMap> arrlstTempUpdateNetworkMaps;

				arrlstTempUpdateNetworkMaps = NetworkMap
						.getArraylistFromJSOnArray(jarrUdateNetworkMaps
								.toString());

				arrlstNetworkMaps = networkMapPreferences.getNetworkMapList();

				Log.v("TT",
						"temp  arrlst: " + arrlstTempUpdateNetworkMaps.size());
				Log.v("TT", "saved arrlst: " + arrlstNetworkMaps.size());
				Log.v("TT",
						"saved data: " + networkMapPreferences.getNetworkMaps());

				addNewMaps(arrlstTempUpdateNetworkMaps);
				networkMapPreferences.setNetworkMapList(arrlstNetworkMaps);

				Log.v("TT",
						"2temp  arrlst: " + arrlstTempUpdateNetworkMaps.size());
				Log.v("TT", "2saved arrlst: " + arrlstNetworkMaps.size());
				Log.v("TT",
						"2saved data: "
								+ networkMapPreferences.getNetworkMaps());

				// After comparing response, (if applicable), recheck for any
				// new maps added
				for (int i = 0; i < arrlstNetworkMaps.size(); i++) {

					System.out.println("-- map checker: "
							+ !arrlstNetworkMaps
									.get(i)
									.getFileName()
									.equalsIgnoreCase(
											NetworkMapActivity.bundleMap));
					if (arrlstNetworkMaps.get(i).getIsMapAvailable() == 0
							&& !arrlstNetworkMaps
									.get(i)
									.getFileName()
									.equalsIgnoreCase(
											NetworkMapActivity.bundleMap)) {
						flag = true;
					}

				}

			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO: handle exception
		}

		try {
			downloadNetworkMaps();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// flag = true;
	}

	public void addNewMaps(ArrayList<NetworkMap> arrlstTempUpdateNetworkMaps) {
		// TODO Auto-generated method stub

		for (int o = 0; o < arrlstTempUpdateNetworkMaps.size(); o++) {

			boolean isNew = true;

			for (int i = 0; i < arrlstNetworkMaps.size(); i++) {
				if (arrlstNetworkMaps
						.get(i)
						.getFileName()
						.equals(arrlstTempUpdateNetworkMaps.get(o)
								.getFileName())) {
					Log.v("TT",
							"compare: "
									+ arrlstNetworkMaps
											.get(i)
											.getFileName()
											.equals(arrlstTempUpdateNetworkMaps
													.get(o).getFileName()));
					isNew = false;

				}
			}
			if (isNew) {
				arrlstNetworkMaps.add(arrlstTempUpdateNetworkMaps.get(o));
			}

		}

	}

	public void downloadNetworkMaps() {

		for (int i = 0; i < arrlstNetworkMaps.size(); i++) {

			if (arrlstNetworkMaps.get(i).getIsMapAvailable() == 0) {

				HttpURLConnection connection = null;
				InputStream is = null;

				try {

					// String url =
					// "http://extranetdev.yarratrams.com.au/TTMetaContent/assets/network_map_201412081141.png";
					URL get_url = new URL(arrlstNetworkMaps.get(i).getUrl());

					// URL get_url = new URL(url);

					Log.v("TT", "-- url for image : "
							+ arrlstNetworkMaps.get(i).getUrl());

					connection = (HttpURLConnection) get_url.openConnection();
					// connection.setDoInput(true);
					connection.setDoOutput(false);
					connection.connect();

					BitmapFactory.Options opts = new BitmapFactory.Options();
					opts.inSampleSize = 2;

					is = new BufferedInputStream(connection.getInputStream());
					bitmap = BitmapFactory.decodeStream(is, null, opts);

					saveFile(activity, bitmap, arrlstNetworkMaps.get(i)
							.getFileName(), i);
					bitmap.recycle();
					bitmap = null;

					connection.disconnect();
					is.close();

				}

				// catch (MalformedURLException e)
				// {
				// e.printStackTrace();
				// }
				// catch (IOException e)
				// {
				// e.printStackTrace();
				// }

				catch (FileNotFoundException e) {

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

			}

		}

		try {
			networkMapPreferences.setNetworkMapList(arrlstNetworkMaps);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Log.v("TT", "error in saving bk data: " + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public void saveFile(Context context, Bitmap b, String picName, int i) {
		FileOutputStream fos;
		try {
			fos = context.openFileOutput(picName, Context.MODE_PRIVATE);
			b.compress(Bitmap.CompressFormat.PNG, 100, fos);
			fos.close();
			arrlstNetworkMaps.get(i).setIsMapAvailable(1);

		} catch (FileNotFoundException e) {
			Log.d("TT", "file not found");
			e.printStackTrace();
		} catch (IOException e) {
			Log.d("TT", "io exception");
			e.printStackTrace();
		} catch (Exception e) {
			// TODO: handle exception
			Log.d("TT", "  exception");
		}

	}

}
