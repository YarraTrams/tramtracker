package com.yarratrams.tramtracker.ui;

import android.app.ActivityGroup;
import android.app.TabActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.TabHost;

public class MyTramTabActivityManager extends ActivityGroup {
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_MYTRAM;
	public static MyTramTabActivityManager instance;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			TabHost tabHost = ((TabActivity) getCurrentActivity()).getTabHost();
			for (int i = 0; i < 5; i++) {
				tabHost.setCurrentTab(i);
			}
			tabHost.setCurrentTab(GROUP_TAB);
		} catch (Exception e) {
			e.printStackTrace();
		}
		instance = this;
	}

	static public MyTramTabActivityManager getGroup() {
		return instance;
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			getLocalActivityManager().getCurrentActivity().openOptionsMenu();
			return true;
		} else if (keyCode == KeyEvent.KEYCODE_BACK) {
			getLocalActivityManager().getCurrentActivity().onBackPressed();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

}