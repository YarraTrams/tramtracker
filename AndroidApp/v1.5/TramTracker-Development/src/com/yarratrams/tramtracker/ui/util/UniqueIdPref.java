package com.yarratrams.tramtracker.ui.util;

import java.util.UUID;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class UniqueIdPref {
	
	Context context;
	SharedPreferences sharedPreferences;
	SharedPreferences.Editor editor;


	final String PREF_NAME = "uniqueidpref";
	final String UNIQUE_ID =	"uniqueid";
	final String FIRST_TIME_REGISTRATION = "first_time_registration";
	
	
	public UniqueIdPref(Context context) {
		// TODO Auto-generated constructor stub
		
		
		this.context  = context;
		sharedPreferences  = context.getSharedPreferences(PREF_NAME, Activity.MODE_PRIVATE);
		editor= sharedPreferences.edit();
	}

	
	
	
	public void setUniqueId(String id)
	{
		editor.putString(UNIQUE_ID, id);
		editor.commit();
	}


	public String getUniqueId()
	{
		return sharedPreferences.getString(UNIQUE_ID, "");

	}
	
	
	
	public void setFirstTimeRegistration(boolean ftg)
	{
		editor.putBoolean(FIRST_TIME_REGISTRATION, ftg);
		editor.commit();
	}


	public Boolean getFirstTimeRegistration()
	{
		return sharedPreferences.getBoolean(FIRST_TIME_REGISTRATION, true);

	}
	
	
	
	
	
	
	public String createUniqueID()
	{
		UUID uuid = UUID.randomUUID();
		return uuid.toString().replace("-", "");
	}
	
}
