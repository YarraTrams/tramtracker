package com.yarratrams.tramtracker.ui.util;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.db.FavouriteManager;
import com.yarratrams.tramtracker.objects.FavouritesGroup;
import com.yarratrams.tramtracker.ui.AddFavouriteGroupActivity;
import com.yarratrams.tramtracker.ui.ManageFavouriteGroupsActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;


public class DragNDropListArrayAdapter extends BaseAdapter 
		implements DragNDropListView.RemoveListener, DragNDropListView.DropListener{
	private ManageFavouriteGroupsActivity context;
	private ArrayList<FavouritesGroup> groups;
	private DragNDropListView list;
	
	private FavouriteManager favouriteManager;
	
	public DragNDropListArrayAdapter(DragNDropListView list, ManageFavouriteGroupsActivity context, ArrayList<FavouritesGroup> groups) {
		super();
		this.context = context;
		this.groups = groups;
		this.list = list;
		
		this.list.setDragListener(dragListener);
		this.list.setDropListener(this);
		this.list.setRemoveListener(this);
		
		favouriteManager = new FavouriteManager(this.context);
	}
    
    private DragNDropListView.DragListener dragListener =
    	new DragNDropListView.DragListener() {
    	
		public void onDrag(int x, int y, DragNDropListView listView) {

		}

		public void onStartDrag(View itemView) {
			itemView.setVisibility(View.INVISIBLE); 
			itemView.setBackgroundColor(context.getResources().getColor(R.color.color_transparent_light));
			ImageView dragIcon = (ImageView)itemView.findViewById(R.id.favourite_move);
			if (dragIcon != null) dragIcon.setVisibility(View.INVISIBLE);
		}

		public void onStopDrag(View itemView) {
			itemView.setVisibility(View.VISIBLE);
			itemView.setBackgroundColor(context.getResources().getColor(R.color.color_transparent));
			ImageView dragIcon = (ImageView)itemView.findViewById(R.id.favourite_move);
			if (dragIcon != null) dragIcon.setVisibility(View.VISIBLE);
		}
    };
	
	

	public void updateList(ArrayList<FavouritesGroup> groups) {
		this.groups.clear();
		this.groups.addAll(groups);
		super.notifyDataSetChanged();
	}

	static class FavouriteGroupViewHolder{
		TextView groupName;
		Button delete;
		Button edit;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		FavouriteGroupViewHolder viewHolder;
		
		if(convertView == null){
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(R.layout.favourites_manage_groups_list_view_child, parent, false);
			
			viewHolder = new FavouriteGroupViewHolder();
			viewHolder.groupName = (TextView)convertView.findViewById(R.id.favourite_name);
			viewHolder.delete = (Button)convertView.findViewById(R.id.favourite_delete);
			viewHolder.edit = (Button)convertView.findViewById(R.id.favourite_edit);
			convertView.setTag(viewHolder);
			
		} else {
			viewHolder = (FavouriteGroupViewHolder) convertView.getTag();
		}
		
		final FavouritesGroup group = (FavouritesGroup) getItem(position);
		viewHolder.groupName.setText(group.getName());
		
		viewHolder.delete.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(groups.size() == 1){
					TramTrackerMainActivity.getAppManager().displayErrorMessage(context.getResources().getString(R.string.error_favourite_lastgroup));
					return;
				}
				validateDelete(group);
			}
		});
		viewHolder.edit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				TramTrackerMainActivity.getAppManager().callSelection(editDescription(group.getName()));
				editFavouriteGroup(group);
			}
		});
		
		return convertView;
	}

	private String editDescription(String stop){
		String description = "";
		
		description = context.getString(R.string.accessibility_click_edit);
		description = description.concat(stop);
		
		return description; 
	}
	
	
	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
    @Override
    public boolean areAllItemsEnabled(){
        return true;
    }
	

    private void deleteFavouriteGroup(FavouritesGroup group){
    	groups.remove(group);
    	favouriteManager.removeFavouriteGroup(group);
    	reassignGroupsOrder();
    	context.updateUI();
    }
    
    private void editFavouriteGroup(FavouritesGroup group){
		Intent intent = new Intent(context, AddFavouriteGroupActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(AddFavouriteGroupActivity.INTENT_KEY, group);
		TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_FAVOURITES, context.getResources().getString(R.string.tag_addfavouritegroup_screen), intent);
    }
	
	
    public int getCount() {
        return groups.size();
    }
    
    public FavouritesGroup getItem(int position) {
        return groups.get(position);
    }
	
	
	public void onDrop(int from, int to) {
		if(to > getCount() || to < 0){
			return;
		}

		// Remove favourite from old location and set to new location
		FavouritesGroup group = groups.remove(from);
		groups.add(to, group);
		notifyDataSetChanged();
		list.invalidateViews();
		
		int first = from <= to? from: to;
		int last = from >= to? from: to;
		FavouritesGroup newGroup = new FavouritesGroup();
		for(; first <= last; first++){
			group = groups.get(first);
			newGroup = group;
			newGroup.setOrder(first);
			favouriteManager.editFavouriteGroup(group, newGroup);
		}		
	}

	private void reassignGroupsOrder(){
		for(int i = 0; i < groups.size(); i++){
			FavouritesGroup group = groups.get(i);
			group.setOrder(i);
			favouriteManager.editFavouriteGroup(group, group);
		}
	}

	public void onRemove(int which) {
		if (which < 0 || which > groups.size()) {
			return;		
		}
		groups.remove(which);
		
		list.invalidateViews();
	}
	
	public void validateDelete(final FavouritesGroup group){
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
		    @Override
		    public void onClick(DialogInterface dialog, int which) {
		        switch (which){
		        case DialogInterface.BUTTON_POSITIVE:
		        	deleteFavouriteGroup(group);
		            break;

		        case DialogInterface.BUTTON_NEGATIVE:
		            break;
		        }
		    }
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(context.getDialogContext());
		builder.setMessage(context.getResources().getString(R.string.manage_favourites_delete_group_message))
			.setPositiveButton(context.getResources().getString(R.string.manage_favourites_delete_yes), dialogClickListener)
		    .setNegativeButton(context.getResources().getString(R.string.manage_favourites_delete_cancel), dialogClickListener)
		    .setTitle(group.getName())
		    .show();
	}

}
