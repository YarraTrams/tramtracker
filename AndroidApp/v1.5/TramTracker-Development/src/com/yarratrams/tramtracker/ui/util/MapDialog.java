package com.yarratrams.tramtracker.ui.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PixelFormat;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.Stop;

public class MapDialog extends Dialog
		implements
			android.content.DialogInterface.OnKeyListener {
	public static final int TYPE_PID = 0;
	public static final int TYPE_FAVOURITE = 1;
	public static final int TYPE_TIMETABLE = 2;
	public static final int TYPE_ALARM = 3;

	private int type;
	private Activity activity;
	private View popupView;

	public MapDialog(Context context) {
		super(context, android.R.style.Theme_Translucent_NoTitleBar);

		type = TYPE_PID;
		activity = (Activity) context;
		initDialog();
	}

	public MapDialog(Context context, int type) {
		super(context, android.R.style.Theme_Translucent_NoTitleBar);

		this.type = type;
		activity = (Activity) context;
		initDialog();
	}

	private void initDialog() {
		this.setOnKeyListener(this);

		int resource = 0;
		if (type == TYPE_PID) {
			resource = R.layout.map_view_dialog;
		} else if (type == TYPE_FAVOURITE) {
			resource = R.layout.map_view_favourite_dialog;
		} else if (type == TYPE_TIMETABLE) {
			resource = R.layout.map_view_timetable_dialog;
		} else if (type == TYPE_ALARM) {
			resource = R.layout.map_view_alarm_dialog;
		}

		LayoutInflater li = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		popupView = li.inflate(resource, null);
		WindowManager.LayoutParams params = new WindowManager.LayoutParams(
				WindowManager.LayoutParams.WRAP_CONTENT,
				WindowManager.LayoutParams.WRAP_CONTENT,
				WindowManager.LayoutParams.TYPE_APPLICATION_SUB_PANEL,
				WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
				PixelFormat.TRANSLUCENT);

		params.gravity = Gravity.CENTER;
		this.setContentView(popupView);
		this.getWindow().setAttributes(params);
		this.getWindow().setFlags(
				WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
				WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);
		this.setCanceledOnTouchOutside(true);
	}

	public void setNameText(String text) {
		TextView name = (TextView) popupView.findViewById(R.id.map_dialog_name);
		name.setText(text);
	}

	public void setDetailText(String text) {
		TextView detail = (TextView) popupView
				.findViewById(R.id.map_dialog_detail);
		detail.setText(text);
	}

	public void setDistanceText(String text) {
		/*
		 * FREE TRAM ZONE
		 */

		TextView distance = (TextView) popupView
				.findViewById(R.id.map_dialog_distance);

		distance.setText(text);

	}

	public void setOnDirectionsClickListener(View.OnClickListener listener) {
		Button directionsButton = (Button) popupView
				.findViewById(R.id.map_dialog_left_button);
		directionsButton.setOnClickListener(listener);
	}

	public void setOnGoClickListener(View.OnClickListener listener) {
		Button goButton = (Button) popupView
				.findViewById(R.id.map_dialog_right_button);
		goButton.setOnClickListener(listener);
	}

	public void setLeftButtonOnClickListener(View.OnClickListener listener) {
		setOnDirectionsClickListener(listener);
	}

	public void setRightButtonOnClickListener(View.OnClickListener listener) {
		setOnGoClickListener(listener);
	}

	/*
	 * Free Tram Zone
	 */
	public void setFreeTramZoneLogo(Stop stop) {
		ImageView ivMapFreeTramZone = (ImageView) popupView
				.findViewById(R.id.ivMapFreeTramZone);
		if (stop.IsInFreeZone())
			ivMapFreeTramZone.setVisibility(View.VISIBLE);
		else
			ivMapFreeTramZone.setVisibility(View.INVISIBLE);
	}

	public void disableGoButton() {
		Button goButton = (Button) popupView
				.findViewById(R.id.map_dialog_right_button);
		goButton.setVisibility(View.INVISIBLE);
	}

	public void setLowFloor(boolean isLowFloor) {
		if (type == TYPE_FAVOURITE) {
			ImageView lowFloor = (ImageView) popupView
					.findViewById(R.id.favourite_lowfloor);
			if (isLowFloor) {
				lowFloor.setVisibility(View.VISIBLE);
			} else {
				lowFloor.setVisibility(View.INVISIBLE);
			}
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		this.dismiss();
		return super.onTouchEvent(event);
	}

	@Override
	public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			this.dismiss();
			activity.openOptionsMenu();
		}
		return true;
	}

}