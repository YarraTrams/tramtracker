package com.yarratrams.tramtracker.ui.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.Reminders;
import com.yarratrams.tramtracker.objects.Route;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.objects.Tram;
import com.yarratrams.tramtracker.ui.ScheduleDepartureActivity;
import com.yarratrams.tramtracker.ui.TimetableActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;

public class TimetableListArrayAdapter extends ArrayAdapter<Tram> {
	private final TimetableActivity context;
	private Route route;
	private Stop stop;
	private final ArrayList<Tram> services;
	Activity activity;

	/*
	 * Adil added
	 */

	Reminders reminders = null;
	ReminderPreferences reminderPreferences;

	// AlertDialog alertDialog;
	// AlertDialog.Builder dialogBuilder;

	AlarmDialog alarmDialog;

	long selectedInterval;

	/*
	 * ends
	 */
	// "activity" field added by Adil Bhatti
	public TimetableListArrayAdapter(TimetableActivity context, Stop stop,
			Route route, ArrayList<Tram> services, Activity activity) {
		super(context, R.layout.timetable_list_view_detail, services);
		this.context = context;
		this.route = route;
		this.stop = stop;
		this.services = services;
		this.activity = activity;

		reminderPreferences = new ReminderPreferences(context);

	}

	public void updateList(ArrayList<Tram> services) {
		this.services.clear();
		this.services.addAll(services);
		super.notifyDataSetChanged();
	}

	static class TimetableViewHolder {
		TextView destination;
		TextView routeNumber;
		TextView date;
		TextView time;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		TimetableViewHolder viewHolder;

		if (convertView == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(R.layout.timetable_list_view_detail,
					parent, false);

			viewHolder = new TimetableViewHolder();
			viewHolder.destination = (TextView) convertView
					.findViewById(R.id.route_destination);
			viewHolder.routeNumber = (TextView) convertView
					.findViewById(R.id.route_number);
			viewHolder.date = (TextView) convertView
					.findViewById(R.id.predicted_date);
			viewHolder.time = (TextView) convertView
					.findViewById(R.id.predicted_time);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (TimetableViewHolder) convertView.getTag();
		}

		final Tram service = services.get(position);

		viewHolder.destination.setText(route.getDestination());
		viewHolder.routeNumber.setText(getRouteDescription());
		if (isTodayDate(service.getArrivalTime())) {
			viewHolder.date.setVisibility(View.GONE);
			viewHolder.date.setText("");
		} else {
			viewHolder.date.setVisibility(View.VISIBLE);
			viewHolder.date.setText(getArrivalDateString(service
					.getArrivalTime()));
		}
		viewHolder.time.setText(getArrivalTimeString(service.getArrivalTime()));

		/*
		 * Adil Changed
		 */
		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				// Intent intent = new Intent(context, PIDActivity.class);
				// intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				// intent.putExtra(PIDActivity.INTENT_KEY, stop);
				//
				// TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE,
				// context.getResources().getString(R.string.tag_pid_screen),
				// intent);

				Intent intent = new Intent(context,
						ScheduleDepartureActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra(ScheduleDepartureActivity.ROUTE_INTENT_KEY,
						route);
				intent.putExtra(ScheduleDepartureActivity.TRIP_ID_INTENT_KEY,
						service.getTripID());
				intent.putExtra(ScheduleDepartureActivity.TRIP_TIME_INTENT_KEY,
						service.getArrivalTime().getTime());

				TramTrackerMainActivity.getAppManager().show(
						TramTrackerMainActivity.TAB_MORE,
						context.getResources().getString(
								R.string.tag_schedule_departures), intent);

				// ScheduleDepartureTask scheduleDepartureTask = new
				// ScheduleDepartureTask(activity);
				// scheduleDepartureTask.execute();
				// Toast.makeText(context, "Time :" + service.getArrivalTime(),
				// 1).show();

			}
		});

		convertView.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View arg0) {
				// TODO Auto-generated method stub

				// Start a auto dismiss timer
				final Timer t = new Timer();
				t.schedule(new TimerTask() {
					public void run() {
						alarmDialog.dismiss();
						t.cancel();
					}
				}, 30000);

				alarmDialog = new AlarmDialog(TramTrackerMainActivity.instance);
				alarmDialog.setTitle("Tram Alarm");
				// alarmDialog.tvAlarmDialogMsg.setText(AlarmDialog.STOP_ALARM_MSG_1
				// + stop.getStopDescription() + AlarmDialog.STOP_ALARM_MSG_2);

				final String time;
				DateFormat format = null;

				if (android.text.format.DateFormat.is24HourFormat(context)) {
					format = new SimpleDateFormat(context.getResources().getString(
							R.string.onboard_time_format));
				} else {
					format = new SimpleDateFormat(context.getResources().getString(
							R.string.onboard_time_format_H));

				}
				/*
				 * Ended
				 */
				time = format.format(service.getArrivalTime());
				
				alarmDialog.tvAlarmDialogMsg
						.setText(AlarmDialog.STOP_ALARM_MSG_1
								+ stop.getStopDescription() + " at " + time
								+ AlarmDialog.STOP_ALARM_MSG_2);

				alarmDialog.bAlarmDialogCancel
						.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								alarmDialog.cancel();
								alarmDialog.dismiss();
								t.cancel();
							}
						});

				alarmDialog.bAlarmDialogOk
						.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub

								int id = alarmDialog.rgIntervals
										.getCheckedRadioButtonId();
								switch (id) {
									case R.id.rbOne :
										selectedInterval = 5;
										break;

									case R.id.rbTwo :
										selectedInterval = 10;
										break;

									case R.id.rbThree :
										selectedInterval = 20;
										break;

									default :
										break;
								}

								// Toast.makeText(TramTrackerMainActivity.instance,
								// "selected interval: " + selectedInterval,
								// 0).show();

								if (isValidReminder(service)) {
									reminders = new Reminders();
									reminders.setActive(true);
									reminders.setDirection(route
											.getDestination());
									reminders
											.setREMINDER_TYPE(Reminders.SCHEDULE_DEP_ALARM);
									reminders.setReminderTime(Reminders
											.getScheduleAlarmReminderTime(
													service.getArrivalTime(),
													selectedInterval));
									reminders.setRoute(route.getRouteNumber());
									reminders.setSetTime(service
											.getArrivalTime().getTime());
									reminders.setStopName(stop.getStopName());
									reminders.setTrackerID(String.valueOf(stop
											.getTrackerID()));
									reminders.setTramClass(0);
									reminders.setFreeTramZone(stop
											.IsInFreeZone());
									reminders.setStop(stop);

									reminderPreferences = new ReminderPreferences(
											context);

									if (reminderPreferences
											.isDuplicate(reminders)) {
										Toast.makeText(
												TramTrackerMainActivity.instance,
												"Duplicate alarm", 0).show();
									} else {

										reminderPreferences
												.addNewReminder(reminders);

										alarmDialog.dismiss();

//										showReminderSetToast(service);
										showReminderSetToast(reminders);

										if (context.isTutorialRunning) {
											context.dismissTutorial();
											context.isTutorialRunning = false;
											new TutorialPreferences(context)
													.setScheduleDepTutorial(true);
											Toast.makeText(
													context,
													"Thank you for completing this tutorial. Get more tutorials in the tram arrival screen, my tramTRACKER and myTRAM.",
													1).show();

										}
									}
								} else {
									// Toast.makeText(TramTrackerMainActivity.instance,
									// "We’re unable to set an alarm for this stop as your tram is scheduled to arrive here within the  "+
									// selectedInterval +" minutes.", 0).show();
									Toast.makeText(
											TramTrackerMainActivity.instance,
											"You can't set this alarm as your tram is due to arrive within "
													+ selectedInterval
													+ " minutes.", 0).show();
								}

								t.cancel();
							}

						});

				alarmDialog.show();

				/*
				 * final CharSequence[] arrTimes = {"5 Minutes",
				 * "10 Minutes","20 Minutes"};
				 * 
				 * 
				 * dialogBuilder = new
				 * Builder(TramTrackerMainActivity.instance);
				 * 
				 * 
				 * 
				 * dialogBuilder.setTitle("Schedule Stop Alarm");
				 * 
				 * //dialogBuilder.setMessage(
				 * "msg to show user \n Notify me before arriving");
				 * 
				 * dialogBuilder.setSingleChoiceItems(arrTimes, -1, new
				 * DialogInterface.OnClickListener() {
				 * 
				 * @Override public void onClick(DialogInterface dialog, int
				 * which) { // TODO Auto-generated method stub
				 * 
				 * switch (which) { case 0 : selectedInterval = 5; break;
				 * 
				 * case 1 : selectedInterval = 10; break;
				 * 
				 * case 2 : selectedInterval = 20; break;
				 * 
				 * default : selectedInterval = 0; break; }
				 * 
				 * System.out.println("-- selected interval: " +
				 * selectedInterval);
				 * 
				 * } });
				 * 
				 * 
				 * dialogBuilder.setPositiveButton("OK", new
				 * DialogInterface.OnClickListener() {
				 * 
				 * @Override public void onClick(DialogInterface dialog, int
				 * which) { // TODO Auto-generated method stub
				 * Toast.makeText(TramTrackerMainActivity.instance, "Alarm set",
				 * 0).show(); dialog.dismiss();
				 * 
				 * } });
				 * 
				 * dialogBuilder.setNegativeButton("Cancel", new
				 * DialogInterface.OnClickListener() {
				 * 
				 * @Override public void onClick(DialogInterface dialog, int
				 * which) { // TODO Auto-generated method stub dialog.dismiss();
				 * Toast.makeText(TramTrackerMainActivity.instance,
				 * "Alarm cancelled", 0).show(); } });
				 * 
				 * 
				 * alertDialog = dialogBuilder.create();
				 * alertDialog.setCanceledOnTouchOutside(false);
				 * alertDialog.show();
				 */
				// Toast.makeText(context, "selected route: " +
				// route.getRouteNumber() + " and selected time: " +
				// service.getArrivalTime().getTime() , 0).show();
				return true;
			}
		});

		/*
		 * Ends
		 */

		return convertView;
	}

	private void showReminderSetToast(final Tram service) {
		Toast.makeText(
				TramTrackerMainActivity.instance,
				"You will be notified when " + selectedInterval
						+ " minutes from Stop : " + reminders.getStopName()
						+ " (scheduled to arrive at "
						+ getTramArrivalTime(service) + ").", 1).show();
	}
	
	private void showReminderSetToast(Reminders reminder) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(reminder.getReminderTime());
		Date date = new Date(cal.getTimeInMillis());
		String time = "";
	
		DateFormat format = null;

		if (android.text.format.DateFormat.is24HourFormat(context)) {
			format = new SimpleDateFormat(context.getResources().getString(
					R.string.onboard_time_format));
		} else {
			format = new SimpleDateFormat(context.getResources().getString(
					R.string.onboard_time_format_H));

		}
		time = format.format(date);
		
		
		
		String msg = "You will be alerted at "
				+ time
				+ ". Please tap on the notification to get updated real-time information.";

		Toast.makeText(TramTrackerMainActivity.instance, msg, 1).show();
	}

	private boolean isValidReminder(Tram service) {
		// TODO Auto-generated method stub

		if ((service.getArrivalTime().getTime() - System.currentTimeMillis()) > (selectedInterval * 60 * 1000)) {
			return true;
		} else {

			return false;
		}
	}

	private String getRouteDescription() {
		String text = "";

		text = context.getResources().getString(R.string.timetable_route);
		text = text.concat(route.getRouteNumber());

		if (stop.getCityDirection() != null) {
			if (stop.getCityDirection().contains(
					context.getResources().getString(
							R.string.timetable_route_direction))) {
				text = text.concat(context.getResources().getString(
						R.string.timetable_via_city));
			}
		}

		return text;
	}

	public String getArrivalTimeString(Date date) {
		/*
		 * Adil Added
		 */
		DateFormat timeFormat;

		if (android.text.format.DateFormat.is24HourFormat(context)) {
			timeFormat = new SimpleDateFormat(context.getResources().getString(
					R.string.timetable_result_time_format_24H));
		} else {
			timeFormat = new SimpleDateFormat(context.getResources().getString(
					R.string.timetable_result_time_format));
		}

		/*
		 * End
		 */
		String timeString = timeFormat.format(date);
		return timeString;
	}
	public String getArrivalDateString(Date date) {
		DateFormat dateFormat = new SimpleDateFormat(context.getResources()
				.getString(R.string.timetable_result_date_format));
		String dateString = dateFormat.format(date);
		return dateString;
	}
	public boolean isTodayDate(Date timetableDate) {
		Calendar today = Calendar.getInstance();
		Calendar date = Calendar.getInstance();
		date.setTime(timetableDate);
		if (today.get(Calendar.DAY_OF_YEAR) == date.get(Calendar.DAY_OF_YEAR)) {
			return true;
		}
		return false;
	}

	private String getTramArrivalTime(final Tram service) {
		String arrivalTime = "";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("h:mma");
		return arrivalTime = simpleDateFormat.format(service.getArrivalTime());
	}
}
