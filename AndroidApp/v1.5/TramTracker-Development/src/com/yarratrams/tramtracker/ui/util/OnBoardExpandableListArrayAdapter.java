package com.yarratrams.tramtracker.ui.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Minutes;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.OnBoardStopsBySuburb;
import com.yarratrams.tramtracker.objects.PredictedArrivalTime;
import com.yarratrams.tramtracker.objects.PredictedTimeResult;
import com.yarratrams.tramtracker.objects.Reminders;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.ui.OnBoardActivity;
import com.yarratrams.tramtracker.ui.OnBoardStopActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;

public class OnBoardExpandableListArrayAdapter
		extends
			BaseExpandableListAdapter {
	private static final int TYPE_START = 0;
	private static final int TYPE_MIDDLE = 1;
	private static final int TYPE_FINISH = 2;

	private OnBoardActivity context;
	private PredictedTimeResult results;
	private ArrayList<PredictedArrivalTime> stops;
	private ArrayList<OnBoardStopsBySuburb> suburbs;
	private int selectedIndex;
	private boolean isNowIndex;
	private boolean isAccurateIndex;
	View currentView;
	ImageView tutorialTimeLayout;

	/*
	 * Adil added
	 */

	AlertDialog.Builder dialogBuilder;
	ReminderPreferences reminderPreferences;

	// final String ALARM_ERROR_MSG =
	// "We’re unable to set an alarm for this stop as your tram is scheduled to arrive here within the next 2 minutes.";
	final String ALARM_ERROR_MSG = "You can't set an alarm within 2 minutes of your destination.";

	/*
	 * ends
	 */

	/*
	 * FREE TRAM ZONE
	 */
	// int ftzCount = 0;

	public static Map<String, Integer> mapFTZ = new HashMap<String, Integer>();

	public OnBoardExpandableListArrayAdapter(OnBoardActivity context,
			PredictedTimeResult results) {
		super();
		this.context = context;
		this.results = results;
		System.out.println("-- in constructor: "
				+ results.getTram().getHeadboardNo());
		this.isAccurateIndex = false;
		this.stops = results.getAlPredictedArrivalTime();
		this.suburbs = new ArrayList<OnBoardStopsBySuburb>();
		generateSuburbsList();
		this.selectedIndex = results.getStartingIndex();
		mapFTZ = OnBoardActivity.mapFTZ;

		reminderPreferences = new ReminderPreferences(context);

		/*
		 * FREE TRAM ZONE
		 */

		// for(int i = 0 ; i < stops.size(); i++)
		// {
		// if(stops.get(i).getStop().isCityStop())
		// {
		// ftzCount++;
		// }
		// }
		//
		//
		// System.out.println("-- total cbd stops : " + ftzCount);
		// Toast.makeText(context, "ftz stops  : " + ftzCount, 1).show();
	}

	public void updateList(PredictedTimeResult results, int selectedIndex) {
		this.stops.clear();
		this.results = results;
		this.stops.addAll(results.getAlPredictedArrivalTime());
		this.suburbs.clear();
		generateSuburbsList();
		this.selectedIndex = selectedIndex;
		super.notifyDataSetChanged();

	}

	public void updateResults(PredictedTimeResult results) {
		this.results = results;
	}

	public void generateSuburbsList() {
		OnBoardStopsBySuburb suburb;

		for (PredictedArrivalTime stop : stops) {

			try {
				if (suburbs.size() >= 1) {
					suburb = suburbs.get(suburbs.size() - 1);
					// System.out.println("suburb.getSuburb():"+suburb.getSuburb()+" stop.getStop():"+stop.getStop()+" stop.getStop().getSuburb():"+stop.getStop().getSuburb());
					if (suburb != null) {
						if (!suburb.getSuburb().equalsIgnoreCase(
								stop.getStop().getSuburb())) {
							suburb = new OnBoardStopsBySuburb();
							suburbs.add(suburb);
						}
					} else {
						suburb = new OnBoardStopsBySuburb();
						suburbs.add(suburb);
					}
				} else {
					suburb = new OnBoardStopsBySuburb();
					suburbs.add(suburb);
				}

				suburb.setSuburb(stop.getStop().getSuburb());
				suburb.addStop(stop);
			} catch (Exception e) {

			}
		}

		/*
		 * Free Tram Zone
		 */

		// for(int i = 0; i < suburbs.size(); i++)
		// {
		// int counts = 0;
		// for(int j = 0; j < suburbs.get(i).getStops().size(); j++)
		// {
		// if(suburbs.get(i).getStops().get(j).getStop().isCityStop())
		// counts++;
		// }
		// suburbs.get(i).setftzStopCount(counts);
		// }
		//
		//
		// for(int i = 0 ; i < suburbs.size(); i++)
		// {
		// System.out.println("-- num of ftz stop in : " +
		// suburbs.get(i).getSuburb() + " count : " +
		// suburbs.get(i).getftzStopCount());
		// }

	}

	public void setStartingIndex(PredictedTimeResult results, int index,
			boolean isAccurate) {
		// this.stops.clear();
		// this.results = results;
		// this.stops.addAll(results.getAlPredictedArrivalTime());
		// this.suburbs.clear();
		// generateSuburbsList();
		this.selectedIndex = index;
		this.isAccurateIndex = isAccurate;
		this.isNowIndex = false;
		super.notifyDataSetChanged();
	}

	public void setStartingIndex(int index, boolean isAccurate) {

		this.selectedIndex = index;
		this.isAccurateIndex = isAccurate;
		this.isNowIndex = false;
		super.notifyDataSetChanged();
	}

	public void setNowIndex() {
		isNowIndex = true;
		super.notifyDataSetChanged();
	}

	public boolean isCurrentIndex(Stop selected) {
		if (selectedIndex >= stops.size()) {
			return false;
		}

		PredictedArrivalTime stop = stops.get(selectedIndex);
		if (stop.getStop().getTrackerID() == selected.getTrackerID()) {
			return true;
		}

		if (!isAccurateIndex) {
			if (selectedIndex < stops.size() - 1) {
				stop = stops.get(selectedIndex + 1);
				if (stop.getStop().getTrackerID() == selected.getTrackerID()) {
					return true;
				}
			}
		}

		return false;
	}

	public ArrayList<PredictedArrivalTime> getStopsInSuburb(int index) {
		OnBoardStopsBySuburb suburb = suburbs.get(index);
		return suburb.getStops();
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return getStop(groupPosition, childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return (groupPosition * 100) + childPosition;
	}

	static class RouteStopViewHolder {
		TextView stopName;
		TextView poi;
		TextView time;
		ImageView stopOverlay;
		ImageView routeCBD;
		ImageView routeZone2;
		ImageView connectingTrain;
		ImageView connectingTrams;
		ImageView connectingBuses;
		ImageView turnIcon;
		ImageView routePlatform;
		ImageView alarmIcon;
		ImageView routeColor;
		ImageView ivFreeTramZoneList;
		ImageView ivOnboardFreeTramText;
		ImageView predicted_time_background;
	}

	@Override
	public View getChildView(final int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		RouteStopViewHolder viewHolder;

		final PredictedArrivalTime stop = (PredictedArrivalTime) getChild(
				groupPosition, childPosition);

		if (convertView == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(
					R.layout.onboard_stops_list_view_child, parent, false);

			viewHolder = new RouteStopViewHolder();
			viewHolder.stopName = (TextView) convertView
					.findViewById(R.id.stop_name);
			viewHolder.poi = (TextView) convertView
					.findViewById(R.id.points_interest);
			viewHolder.time = (TextView) convertView
					.findViewById(R.id.predicted_time);
			viewHolder.stopOverlay = (ImageView) convertView
					.findViewById(R.id.stop_overlay);
			viewHolder.routeCBD = (ImageView) convertView
					.findViewById(R.id.route_cbd);
			viewHolder.routeZone2 = (ImageView) convertView
					.findViewById(R.id.route_zone2);
			viewHolder.connectingTrain = (ImageView) convertView
					.findViewById(R.id.connecting_train);
			viewHolder.connectingTrams = (ImageView) convertView
					.findViewById(R.id.connecting_tram);
			viewHolder.connectingBuses = (ImageView) convertView
					.findViewById(R.id.connecting_bus);
			viewHolder.turnIcon = (ImageView) convertView
					.findViewById(R.id.turn);
			viewHolder.routePlatform = (ImageView) convertView
					.findViewById(R.id.route_platform);
			viewHolder.alarmIcon = (ImageView) convertView
					.findViewById(R.id.alarm_icon);
			viewHolder.routeColor = (ImageView) convertView
					.findViewById(R.id.route_colour);
			viewHolder.predicted_time_background = (ImageView) convertView
					.findViewById(R.id.predicted_time_background);

			/*
			 * Free Tram Zone
			 */
			viewHolder.ivFreeTramZoneList = (ImageView) convertView
					.findViewById(R.id.ivFreeTramZoneList);
			viewHolder.ivOnboardFreeTramText = (ImageView) convertView
					.findViewById(R.id.ivOnboardFreeTramText);

			convertView.setTag(viewHolder);

		} else {
			viewHolder = (RouteStopViewHolder) convertView.getTag();

		}

		tutorialTimeLayout = viewHolder.predicted_time_background;

		// String pos = ""+groupPosition+","+childPosition;
		// String pos =
		// suburbs.get(i).getStops().get(j).getStop().getStopNumber();
		// System.out.println("-- pos  : " + pos);

		// if(mapFTZ.get(stop.getStop().getStopNumber()) != null)
		// {
		if (stop.getStop().getFtzImageId() == 0) {
			viewHolder.ivOnboardFreeTramText.setVisibility(View.INVISIBLE);
		} else {
			viewHolder.ivOnboardFreeTramText.setVisibility(View.VISIBLE);
			viewHolder.ivOnboardFreeTramText.setImageResource(stop.getStop()
					.getFtzImageId());
			viewHolder.ivOnboardFreeTramText
					.setBackgroundColor(Color.TRANSPARENT);
			// viewHolder.ivOnboardFreeTramText.setBackgroundResource(stop.getStop().getFtzImageId());
			// viewHolder.ivOnboardFreeTramText.setAdjustViewBounds(true);
			// viewHolder.ivOnboardFreeTramText.setScaleType(ScaleType.);

		}
		// }

		/*
		 * Free Tram Zone Ends
		 */

		// System.out.println("-- gp: " + groupPosition + " cp: " +
		// childPosition + "isCity:" + stop.getStop().isCityStop() );

		int type = TYPE_MIDDLE;
		if (isLastChild && (groupPosition == suburbs.size() - 1)) {
			type = TYPE_FINISH;
		} else if (groupPosition == 0 && childPosition == 0) {
			type = TYPE_START;
		}
		setRouteColour(viewHolder.routeColor, results.getRoute().getColour(),
				type);

		viewHolder.stopName.setText(getStopDescription(stop.getStop()));
		if (stop.getStop().getPointsOfInterest() != null) {
			viewHolder.poi.setText(stop.getStop().getPointsOfInterest()
					.replace(",", ", ")); // replace used to add space after
			// comma
			viewHolder.poi.setVisibility(View.VISIBLE);
		} else {
			viewHolder.poi.setVisibility(View.GONE);
		}

		viewHolder.time.setText(getArrivalTime(stop));
		if (stop.getStop().getConnectingTrains() != null) {
			if (!stop.getStop().getConnectingTrains().trim()
					.equalsIgnoreCase("")) {
				viewHolder.connectingTrain.setVisibility(View.VISIBLE);
			} else {
				viewHolder.connectingTrain.setVisibility(View.INVISIBLE);
			}
		} else {
			viewHolder.connectingTrain.setVisibility(View.INVISIBLE);
		}
		if (stop.getStop().getConnectingTrams() != null) {
			if (!stop.getStop().getConnectingTrams().trim()
					.equalsIgnoreCase("")) {
				viewHolder.connectingTrams.setVisibility(View.VISIBLE);
			} else {
				viewHolder.connectingTrams.setVisibility(View.INVISIBLE);
			}
		} else {
			viewHolder.connectingTrams.setVisibility(View.INVISIBLE);
		}
		if (stop.getStop().getConnectingBuses() != null) {
			if (!stop.getStop().getConnectingBuses().trim()
					.equalsIgnoreCase("")) {
				viewHolder.connectingBuses.setVisibility(View.VISIBLE);
			} else {
				viewHolder.connectingBuses.setVisibility(View.INVISIBLE);
			}
		} else {
			viewHolder.connectingBuses.setVisibility(View.INVISIBLE);
		}

		if (stop.getStop().isEasyAccessStop()) {
			viewHolder.routePlatform.setVisibility(View.VISIBLE);
		} else {
			viewHolder.routePlatform.setVisibility(View.INVISIBLE);
		}

		if (isCurrentIndex(stop.getStop())
				&& !this.results.getRoute().getRouteNumber().equals("35")) {
			viewHolder.stopOverlay.setVisibility(View.VISIBLE);
		} else {
			viewHolder.stopOverlay.setVisibility(View.GONE);
		}

		if (TramTrackerMainActivity.getAppManager().hasAlarm(stop.getStop())) {
			viewHolder.alarmIcon.setVisibility(View.VISIBLE);
		} else {
			viewHolder.alarmIcon.setVisibility(View.GONE);
		}

		/*
		 * Free Tram Zone CBD Logic implemented
		 */
		Resources r = context.getResources();
		float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 69,
				r.getDisplayMetrics());
		LayoutParams layoutParams = new LayoutParams(LayoutParams.FILL_PARENT,
				(int) px);

		// if(stop.getStop().isCityStop())
		if (stop.getStop().IsInFreeZone()) {
			px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20,
					r.getDisplayMetrics());

			viewHolder.routeCBD.setVisibility(View.VISIBLE);
			viewHolder.routeZone2.setVisibility(View.INVISIBLE);
			viewHolder.ivFreeTramZoneList.setVisibility(View.VISIBLE);
			viewHolder.ivFreeTramZoneList
					.setContentDescription("Free Tram Zone");
			layoutParams.setMargins((int) px, 0, 0, 0);
			viewHolder.stopOverlay.setLayoutParams(layoutParams);
		} else {

			layoutParams.setMargins(0, 0, 0, 0);
			viewHolder.stopOverlay.setLayoutParams(layoutParams);

			if (stop.getStop().getZone() == null
					|| stop.getStop().getZone().equalsIgnoreCase("null"))
				stop.getStop().setZone("1");
			if (stop.getStop().getZone().contains("1")
					&& !stop.getStop().getZone().contains("2")) {
				viewHolder.routeCBD.setVisibility(View.INVISIBLE);
				viewHolder.ivFreeTramZoneList.setVisibility(View.INVISIBLE);
				/*
				 * ADIL CHANGES
				 */
				// viewHolder.routeZone2.setVisibility(View.VISIBLE);
				viewHolder.routeZone2.setVisibility(View.INVISIBLE);
			} else if (stop.getStop().getZone().contains("2")) {
				viewHolder.routeCBD.setVisibility(View.INVISIBLE);
				viewHolder.ivFreeTramZoneList.setVisibility(View.INVISIBLE);
				/*
				 * ADIL CHANGES
				 */
				// viewHolder.routeZone2.setVisibility(View.INVISIBLE);
				viewHolder.routeZone2.setVisibility(View.VISIBLE);
			}
		}

		String turnType = stop.getStop().getTurnType();
		// System.out.println("-- stop:" + stop.getStop().getStopName()
		// +"turntype: " + turnType);
		if (turnType != null) {
			viewHolder.turnIcon.setVisibility(View.INVISIBLE);
			// setTurn(viewHolder.turnIcon, stop.getStop().getTurnType());
		} else {
			viewHolder.turnIcon.setVisibility(View.INVISIBLE);
		}

		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				TramTrackerMainActivity.getAppManager().callSelection(
						getStopDescription(stop.getStop()));
				goToStopDescriptionScreen(stop);
			}
		});

		convertView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				v.setPressed(true);

				return false;
			}
		});

		/*
		 * Adil Added : On long click listener
		 */

		convertView.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				// TODO Auto-generated method stub

				// Toast.makeText(context, "Long Stop pressed", 0).show();

				if (isTwoMinValid(stop)) {
					dialogBuilder = new Builder(
							TramTrackerMainActivity.instance);
					dialogBuilder.setTitle("Stop Alarm");

					String msg = "You tram is predicted to arrive at "
							+ stop.getStop().getStopDescription()
							+ " at "
							+ getArrivalTime(stop)
							+ ".\n\nWould you like to be alerted 2 minutes before the estimated arrival time?\n\nIf there are disruptions on your route, please check and rely on real-time information via myTRAM.";

					// String msg =
					// "Would you like to be alerted 2 minutes before Stop "
					// + stop.getStop().getStopNumber()
					// + " "
					// + stop.getStop().getStopName()
					// + " (scheduled to arrive at "
					// + getArrivalTime(stop) + ")?";

					dialogBuilder.setMessage(msg);
					dialogBuilder.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub

									if (isTwoMinValid(stop)) {
										Reminders reminders = new Reminders();
										reminders.setActive(true);
										reminders.setDirection(stop.getStop()
												.getCityDirection());
										reminders
												.setREMINDER_TYPE(Reminders.STOP_ALARM);
										reminders.setRoute(results.getRoute()
												.getRouteNumber());
										reminders.setSetTime(stop
												.getArrivalTime().getTime());
										reminders.setStopName(stop.getStop()
												.getStopName());
										reminders.setTrackerID(stop.getStop()
												.getStopNumber());
										reminders.setReminderTime(Reminders
												.getStopAlarmReminderTime(stop
														.getArrivalTime()));
										reminders.setFreeTramZone(stop
												.getStop().IsInFreeZone());
										System.out
												.println("-- tram headboard number: "
														+ results
																.getTram()
																.getHeadboardNo());
										reminders.setTram(results.getTram());
										reminders.setTramClass(results
												.getTram().getTramClass());

										reminderPreferences = new ReminderPreferences(
												context);

										if (reminderPreferences
												.isDuplicate(reminders)) {
											Toast.makeText(
													TramTrackerMainActivity.instance,
													"Duplicate Reminder", 1)
													.show();
										} else {

											reminderPreferences
													.addNewReminder(reminders);
											alarmSetMsg(stop);

											if (context.isTutorialRunning) {
												context.dismissTutorial();
												context.isTutorialRunning = false;
												new TutorialPreferences(context)
														.setMyTramTutorial(true);
												Toast.makeText(
														context,
														"Thank you for completing this tutorial. Get more tutorials in the tram arrival screen, Timetables and my tramTRACKER.",
														1).show();
											}

										}
									} else {
										Toast.makeText(
												TramTrackerMainActivity.instance,
												ALARM_ERROR_MSG, 1).show();
									}

								}

							});
					dialogBuilder.setNegativeButton("Cancel",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
									dialog.cancel();

									// ArrayList<Reminders> arrlst =
									// reminderPreferences
									// .getRemindersArrayList();
									//
									// System.out
									// .println("-- reminders size: "
									// + arrlst.size());
									//
									// for (int i = 0; i < arrlst.size(); i++) {
									// System.out.println("-- reminder: "
									// + i + " => "
									// + arrlst.get(i).toString());
									// }
									//
								}
							});

					AlertDialog alertDialog = dialogBuilder.create();
					alertDialog.setCanceledOnTouchOutside(false);

					alertDialog.show();

				}

				else {
					Toast.makeText(TramTrackerMainActivity.instance,
							ALARM_ERROR_MSG, 1).show();
				}

				return true;
			}
		});

		/*
		 * End
		 */

		return convertView;
	}

	public boolean isTwoMinValid(PredictedArrivalTime stop) {

		try {
			int mins = Minutes.minutesBetween(new DateTime(),
					new DateTime(stop.getArrivalTime().getTime())).getMinutes();

			Duration duration = new Duration(new DateTime(stop.getArrivalTime()
					.getTime()), new DateTime());

			Console.print("isTwoMinValid: " + mins);

			if (mins <= 2) {
				return false;
			} else {
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

	}

	// @SuppressLint("NewApi")
	// public boolean isTwoMinValid(PredictedArrivalTime stop) {
	// // Date currData, stopDate;
	// //
	// // stopDate = stop.getArrivalTime();
	// // currData = new Date(System.currentTimeMillis());
	// //
	// // if(stopDate.)
	//
	// try {
	//
	// // Calendar currTime,arrivalTime;
	// //
	// // currTime = Calendar.getInstance();
	// // arrivalTime = Calendar.getInstance();
	// //
	// // currTime.setTimeInMillis(System.currentTimeMillis());
	// // arrivalTime.setTimeInMillis(stop.getArrivalTime().getTime());
	//
	// final Date currDate = new Date(System.currentTimeMillis());
	// final Date arrivalDate = new Date(stop.getArrivalTime().getTime());
	// final long millis = arrivalDate.getTime() - currDate.getTime();
	// System.out.println("-- onboard time diff: " + arrivalDate.getTime() +"-"
	// + currDate.getTime());
	//
	// //int minutes = (int) TimeUnit.MILLISECONDS.toMinutes(millis);
	// // int minutes = (int) ((millis / (1000*60)) % 60);
	//
	//
	// Toast.makeText(TramTrackerMainActivity.instance, "diff:" + millis ,
	// 1).show();
	//
	// //System.out.println("-- time for mytram: " +
	// stop.getArrivalTime().getMinutes() + " - " + new
	// Date(System.currentTimeMillis()).getMinutes());
	// //int diff = stop.getArrivalTime(). - new
	// Date(System.currentTimeMillis()).getMinutes();
	//
	// System.out.println("-- difference: " + (millis > 120000));
	//
	// // if(minutes > 2)
	// // {
	// // return true;
	// // }
	// if(millis > 120000)
	// {
	// return true;
	// }
	//
	// // long min = TimeUnit.MILLISECONDS.toMinutes(diff);
	// // // System.out.println("-- minutes onstop: " + min);
	// // if (min > 2) {
	// // return true;
	// // }
	//
	// } catch (Exception e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// return false;
	// }
	//
	// return false;
	// }

	private void alarmSetMsg(PredictedArrivalTime stop) {
		// TODO Auto-generated method stub

		// String msg = "You will be notified when 2 minutes from Stop "
		// + stop.getStop().getStopNumber() + ": "
		// + stop.getStop().getStopName() + " (scheduled to arrive at "
		// + getArrivalTime(stop) + ").";

		Calendar cal = Calendar.getInstance();
		cal.setTime(stop.getArrivalTime());
		Date date = new Date(cal.getTimeInMillis()- 120000);
		String time = "";
	
		DateFormat format = null;

		if (android.text.format.DateFormat.is24HourFormat(context)) {
			format = new SimpleDateFormat(context.getResources().getString(
					R.string.onboard_time_format));
		} else {
			format = new SimpleDateFormat(context.getResources().getString(
					R.string.onboard_time_format_H));

		}
		time = format.format(date);
		
		
		String msg = "You will be alerted at "
				+ time
				+ ". Please tap on the notification to get updated real-time information.";

		Toast.makeText(TramTrackerMainActivity.instance, msg, 1).show();

	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return getStopsInSuburb(groupPosition).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return suburbs.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return suburbs.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	static class SuburbViewHolder {
		TextView suburbName;
	}

	@Override
	public View getGroupView(final int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		SuburbViewHolder viewHolder;

		if (convertView == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(
					R.layout.route_stops_list_view_group, parent, false);

			viewHolder = new SuburbViewHolder();
			viewHolder.suburbName = (TextView) convertView
					.findViewById(R.id.group_name);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (SuburbViewHolder) convertView.getTag();
		}

		OnBoardStopsBySuburb suburb = (OnBoardStopsBySuburb) getGroup(groupPosition);
		viewHolder.suburbName.setText(suburb.getSuburb());

		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	@Override
	public boolean areAllItemsEnabled() {
		return true;
	}

	private void goToStopDescriptionScreen(PredictedArrivalTime stop) {
		Intent intent = new Intent(this.context, OnBoardStopActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(OnBoardStopActivity.INTENT_KEY, stop.getStop());
		intent.putExtra(OnBoardStopActivity.INTENT_KEY_PREDICTED, stops);

		TramTrackerMainActivity.getAppManager().show(
				TramTrackerMainActivity.TAB_MYTRAM,
				context.getResources().getString(
						R.string.tag_onboard_stop_screen), intent);
	}

	private PredictedArrivalTime getStop(int groupPosition, int childPosition) {
		ArrayList<PredictedArrivalTime> suburb = getStopsInSuburb(groupPosition);
		return suburb.get(childPosition);
	}

	private String getStopDescription(Stop stop) {
		String text = "";

		text = text.concat(String.valueOf(stop.getStopNumber()));
		// phoenix changed 20.Jun.2014

		// text =
		// text.concat(context.getResources().getString(R.string.stop_name_colon));
		if (stop.getStopName() == null) {
			Log.e("getStopDescription",
					"Stop name not available for trackerid = "
							+ stop.getStopNumber());
		} else {
			text = text.concat(": ");
			text = text.concat(stop.getStopName());
		}
		return text;
	}

	private String getArrivalTime(PredictedArrivalTime stop) {
		String string = "";
		Date date = stop.getArrivalTime();

		if (isAccurateIndex && isCurrentIndex(stop.getStop())) {
			if (isNowIndex) {
				string = context.getResources().getString(
						R.string.onboard_current_now);
			} else {
				string = context.getResources().getString(
						R.string.onboard_current_next);
			}
		} else if (date == null) {
			string = context.getResources().getString(
					R.string.onboard_notime_format);
		} else {

			/*
			 * Adil Added
			 */
			DateFormat format = null;

			if (android.text.format.DateFormat.is24HourFormat(context)) {
				format = new SimpleDateFormat(context.getResources().getString(
						R.string.onboard_time_format));
			} else {
				format = new SimpleDateFormat(context.getResources().getString(
						R.string.onboard_time_format_H));

			}
			/*
			 * Ended
			 */
			string = format.format(date);
		}
		return string;
	}

	private void setTurn(ImageView turnIcon, String turn) {
		int resourceID = 0;
		if (turn.equalsIgnoreCase("right")) {
			resourceID = R.drawable.icn_mytram_arrowright;
		} else if (turn.equalsIgnoreCase("straight")) {
			resourceID = R.drawable.icn_mytram_arrowstraight;
		} else if (turn.equalsIgnoreCase("veer_left")) {
			resourceID = R.drawable.icn_mytram_veerleft;
		} else if (turn.equalsIgnoreCase("left")) {
			resourceID = R.drawable.icn_mytram_arrowleft;
		} else if (turn.equalsIgnoreCase("s_to_right")) {
			resourceID = R.drawable.icn_mytram_sturn;
		} else if (turn.equalsIgnoreCase("veer_right")) {
			resourceID = R.drawable.icn_mytram_veerright;
		}
		turnIcon.setImageResource(resourceID);
		turnIcon.setVisibility(View.VISIBLE);
	}

	private void setRouteColour(ImageView routeColor, String colour, int type) {
		int resourceID = 0;
		if (colour == null)
			colour = "GRAY";
		if (colour.equalsIgnoreCase("GREEN")) {
			if (type == TYPE_START) {
				resourceID = R.drawable.icn_route_green_start;
			} else if (type == TYPE_FINISH) {
				resourceID = R.drawable.icn_route_green_end;
			} else {
				resourceID = R.drawable.icn_route_green_mid;
			}
		} else if (colour.equalsIgnoreCase("CYAN")) {
			if (type == TYPE_START) {
				resourceID = R.drawable.icn_route_cyan_start;
			} else if (type == TYPE_FINISH) {
				resourceID = R.drawable.icn_route_cyan_end;
			} else {
				resourceID = R.drawable.icn_route_cyan_mid;
			}
		} else if (colour.equalsIgnoreCase("YELLOW")) {
			if (type == TYPE_START) {
				resourceID = R.drawable.icn_route_yellow_start;
			} else if (type == TYPE_FINISH) {
				resourceID = R.drawable.icn_route_yellow_end;
			} else {
				resourceID = R.drawable.icn_route_yellow_mid;
			}
		} else if (colour.equalsIgnoreCase("PINK")) {
			if (type == TYPE_START) {
				resourceID = R.drawable.icn_route_pink_start;
			} else if (type == TYPE_FINISH) {
				resourceID = R.drawable.icn_route_pink_end;
			} else {
				resourceID = R.drawable.icn_route_pink_mid;
			}
		} else if (colour.equalsIgnoreCase("ORANGE")) {
			if (type == TYPE_START) {
				resourceID = R.drawable.icn_route_orange_start;
			} else if (type == TYPE_FINISH) {
				resourceID = R.drawable.icn_route_orange_end;
			} else {
				resourceID = R.drawable.icn_route_orange_mid;
			}
		} else if (colour.equalsIgnoreCase("TEAL")) {
			if (type == TYPE_START) {
				resourceID = R.drawable.icn_route_teal_start;
			} else if (type == TYPE_FINISH) {
				resourceID = R.drawable.icn_route_teal_end;
			} else {
				resourceID = R.drawable.icn_route_teal_mid;
			}
		} else {
			if (type == TYPE_START) {
				resourceID = R.drawable.icn_route_grey_start;
			} else if (type == TYPE_FINISH) {
				resourceID = R.drawable.icn_route_grey_end;
			} else {
				resourceID = R.drawable.icn_route_grey_mid;
			}
		}
		routeColor.setImageResource(resourceID);
	}

	public int getSelectedIndex() {
		return selectedIndex;

	}

	public ImageView getTutorialTimeLayout() {
		return tutorialTimeLayout;
	}
}
