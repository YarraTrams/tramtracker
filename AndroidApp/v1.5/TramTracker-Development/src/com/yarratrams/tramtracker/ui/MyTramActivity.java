package com.yarratrams.tramtracker.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.PredictedTimeResult;
import com.yarratrams.tramtracker.objects.Tram;
import com.yarratrams.tramtracker.tasks.MyTramTask;
import com.yarratrams.tramtracker.ui.util.FeaturesPreferences;
import com.yarratrams.tramtracker.ui.util.OtherLevelAds;


public class MyTramActivity extends Activity implements OnClickListener {
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_MYTRAM;

	private Tram tram;
	private ProgressDialog loadDialog;
	private InputMethodManager imm;


	//Adil added
	LinearLayout layMyTramScreen;
	FrameLayout rich_banner_fragment;



	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mytram_entry_screen);

		//adil added
		layMyTramScreen = (LinearLayout) findViewById(R.id.layMyTramScreen);
		rich_banner_fragment = (FrameLayout) findViewById(R.id.rich_banner_fragment);

		Button onBoardButton = (Button) findViewById(R.id.search_button);
		onBoardButton.setOnClickListener(this);

		final EditText tramNoEditText = (EditText) findViewById(R.id.mytram_tramno);
		tramNoEditText.setOnClickListener(this);
		tramNoEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if(hasFocus){
					getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
				}
			}
		});
		tramNoEditText.setOnEditorActionListener(new OnEditorActionListener() {        
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if(actionId==EditorInfo.IME_ACTION_DONE || event.getKeyCode() == KeyEvent.KEYCODE_ENTER){
					search(tramNoEditText);
				}
				return false;
			}
		});

		tram = new Tram();
		loadDialog = new ProgressDialog(this);


		/*
		 * Adil Added
		 */
		//loadFromTimeTable();






		/*
		 * Ends
		 */

		//call ads
		OtherLevelAds.showAds(this, R.id.rich_banner_fragment,FeaturesPreferences.getAdsFlag(TramTrackerMainActivity.instance));



		layMyTramScreen.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {
				Rect r = new Rect();
				layMyTramScreen.getWindowVisibleDisplayFrame(r);
				int heightDiff = layMyTramScreen.getRootView().getHeight() - (r.bottom - r.top);

				if (heightDiff > 100) { // if more than 100 pixels, its probably a keyboard...
					//ok now we know the keyboard is up...
					rich_banner_fragment.setVisibility(View.INVISIBLE);
					//view_two.setVisibility(View.GONE);

				}else{
					//ok now we know the keyboard is down...
					rich_banner_fragment.setVisibility(View.VISIBLE);
					//  view_two.setVisibility(View.VISIBLE);

				}
			}
		});

	}


	private Context getDialogContext() {
		Context context;
		if (getParent() != null){
			context = getParent();
		} else {
			context = this;
		}
		return context;
	}


	@Override
	public void onClick(View v) {
		EditText tramNoEditText = (EditText) findViewById(R.id.mytram_tramno);
		Button onBoardButton = (Button) findViewById(R.id.search_button);
		if(v == onBoardButton){
			search(tramNoEditText);

		} else if(v == tramNoEditText){
			tramNoEditText.clearFocus();
			imm.showSoftInput(tramNoEditText, InputMethodManager.SHOW_FORCED);
		}
	}

	private void search(EditText tramNoEditText){
		TramTrackerMainActivity.getAppManager().callSelection(getString(R.string.accessibility_click_mytram));
		if(tramNoEditText.length() < 1){
			TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.error_mytram_nonumber));
			return;
		}

		imm.hideSoftInputFromWindow(tramNoEditText.getWindowToken(), 0);

		if(!loadDialog.isShowing()){
			loadDialog = ProgressDialog.show(getDialogContext(), "", getResources().getString(R.string.dialog_loading), true, true);
		}
		tram.setVehicleNumber(Integer.parseInt(tramNoEditText.getText().toString()));

		System.out.println("-- after first setting tram no: " + tram.getVehicleNumber());
		MyTramTask task = new MyTramTask(this, tram);
		System.out.println("-- tram in my tram: " + tram.getHeadboardNo() + tram.getDestination());
		task.execute();
	}


	public void updateUI(PredictedTimeResult results) {
		if(loadDialog.isShowing()){
			loadDialog.dismiss();
		}
		if(results == null){
			//			TramTrackerMainActivity.getAppManager().displayErrorMessage(getResources().getString(R.string.error_mytram_invalidtram));
		} else {
			System.out.println("-- tram from results: " + results.getTram().getHeadboardNo());
			goToOnBoardScreen(tram);
		}
	}


	private void goToOnBoardScreen(Tram tram){
		Intent intent = new Intent(this, OnBoardActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(OnBoardActivity.INTENT_KEY, tram);
		System.out.println("-- tram sent from mytram act: " + tram.getHeadboardNo());

		TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MYTRAM, 
				getResources().getString(R.string.tag_onboard_screen), intent);
	}

	@Override
	protected void onResume() {
		
		EditText tramNoEditText = (EditText) findViewById(R.id.mytram_tramno);
		tramNoEditText.clearFocus();
		imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		super.onResume();
	}


	@Override
	protected void onPause() {
		EditText tramNoEditText = (EditText) findViewById(R.id.mytram_tramno);
		imm.hideSoftInputFromWindow(tramNoEditText.getWindowToken(), 0);
		super.onPause();
	}


	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}


	/*
	 * Added by Adil
	 */






	/*
	 * Ends
	 */


}
