package com.yarratrams.tramtracker.ui.util;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources.NotFoundException;
import android.view.View;

import com.google.android.maps.GeoPoint;
import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.db.TTDB;
import com.yarratrams.tramtracker.objects.Route;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.singleton.TTDBSingleton;
import com.yarratrams.tramtracker.ui.TimetableStopActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;


public class MapTimetableRouteStopDialog extends MapDialog{
	private Context context;
	private Route route;
	private Stop stop;

	public MapTimetableRouteStopDialog(Context context, Route route, Stop stop) {
		super(context);
		this.context = context;
		this.stop = stop;
		this.route = route;
		
		createDialog();
	}
	
	public MapTimetableRouteStopDialog(Context context, Route route, Stop stop, int type) {
		super(context, type);
		this.context = context;
		this.stop = stop;
		this.route = route;
		
		createDialog();
	}
	
	
	private void createDialog(){
		setTramStopInfo();
		
		setLeftButtonOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				TramTrackerMainActivity.getAppManager().callSelection(context.getString(R.string.accessibility_click_goto_directions));
				GeoPoint destination = new GeoPoint(stop.getLatitudeE6(), stop.getLongitudeE6());
				TramTrackerMainActivity.getAppManager().requestDirectionsService(destination);
			}
		});
		
		setRightButtonOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				TramTrackerMainActivity.getAppManager().callSelection(context.getString(R.string.accessibility_click_goto_stop));
				Intent intent = new Intent(context, TimetableStopActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra(TimetableStopActivity.INTENT_STOP_KEY, stop);
				intent.putExtra(TimetableStopActivity.INTENT_ROUTE_KEY, route);
				
				TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_MORE, 
						context.getResources().getString(R.string.tag_timetable_stop_screen), intent);
			}
		});

	}
	
	
	private void setTramStopInfo(){
		setNameText(getStopDescription());
		setDetailText(getRoutesDescription());
		setDistanceText(getSuburb());
		
		
		setFreeTramZoneLogo(stop);
	}
	
	private String getStopDescription(){
		String text = "";
		
		text = text.concat(String.valueOf(stop.getStopNumber()));
		//phoenix changed 20.Jun.2014
		text = text.concat(": ");
//		text = text.concat(context.getResources().getString(R.string.stop_name_colon));
		text = text.concat(stop.getStopName());
		
		return text;
	}
	
	
	private String getRoutesDescription() {
		try {
			String text = "";

			text = context.getResources().getString(
					R.string.stop_direction_routes);
			String[] array;
			if (stop.getRoutes()[0] != null) {
				array = stop.getRoutes();
			} else {
				TTDB ttdb = TTDBSingleton.getInstance(context);
				array = ttdb.getRoutesArrayOfStringsForStop(stop);
			}
			for (int i = 0; i < array.length; i++) {
				text = text.concat(array[i]);
				if (i < array.length - 1) {
					text = text.concat(context.getResources().getString(
							R.string.stop_routes_coma));
				}
			}
			text = text.concat(context.getResources().getString(
					R.string.stop_name_space));
			text = text.concat(stop.getCityDirection());

			return text;
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return "";
	}

	private String getSuburb(){
		String text = "";

		text = text.concat(context.getResources().getString(R.string.routes_map_dialog_suburb));
		text = text.concat(stop.getSuburb());
		
		return text;
	}
	
}