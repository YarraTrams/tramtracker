package com.yarratrams.tramtracker.ui.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.ImageView;


public class DragNDropExpandableListView extends ExpandableListView {
	boolean dragMode;

	int[] startPosition = new int[2];
	int[] endPosition = new int[2];
	int dragPointOffset;
	int startFlatPosition;
	ImageView dragView;
	GestureDetector gestureDetector;
	Context context;
	
	DragListener dragListener;
	DropListener dropListener;
	RemoveListener removeListener;


	public DragNDropExpandableListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
	}
	
	public void setContext(Context context){
		this.context = context;
	}
	
	public interface DragListener {
		void onStartDrag(View itemView);
		void onDrag(int x, int y, DragNDropExpandableListView listView);
		void onStopDrag(View itemView);
	}
	
	public interface DropListener {
		void onDrop(int from[], int to[]);
	}
	
	public interface RemoveListener {
		void onRemove(int[] which);
	}
	
	
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		final int action = event.getAction();
		final int x = (int) event.getX();
		final int y = (int) event.getY();	
		long packagedPosition = 0;
		int flatPosition = 0;
		flatPosition = pointToPosition(x,y);
		
		packagedPosition = getExpandableListPosition(flatPosition);
		if (action == MotionEvent.ACTION_DOWN && x > this.getWidth()-50) {
			if(getPackedPositionType(packagedPosition) == 1)  
				dragMode = true;
		}
		if(!dragMode){
			return super.onTouchEvent(event);
		}
		
		switch (action) {
			case MotionEvent.ACTION_DOWN:
				startFlatPosition = flatPosition;
				startPosition[0] = getPackedPositionGroup(packagedPosition);
				startPosition[1] = getPackedPositionChild(packagedPosition);
				
				if (packagedPosition != PACKED_POSITION_VALUE_NULL) {
					int itemPosition = flatPosition - getFirstVisiblePosition();
                    dragPointOffset = y - getChildAt(itemPosition).getTop();
                    dragPointOffset -= ((int)event.getRawY()) - y;
					startDrag(itemPosition,y);
					drag(x,y);
				}	
				break;
			case MotionEvent.ACTION_MOVE:
				drag(x,y);
				break;
			case MotionEvent.ACTION_CANCEL:
			case MotionEvent.ACTION_UP:
			default:
				dragMode = false; 
				if(getPackedPositionType(packagedPosition) == PACKED_POSITION_TYPE_GROUP && 
						getPackedPositionGroup(packagedPosition) < 1000)
				{
					endPosition[0] = getPackedPositionGroup(packagedPosition);
					endPosition[1] = 0;
				}
				else if(getPackedPositionType(packagedPosition) == PACKED_POSITION_TYPE_CHILD)
				{
					endPosition[0] = getPackedPositionGroup(packagedPosition);
					endPosition[1] = getPackedPositionChild(packagedPosition);
				}
				else 
				{
					if(y < 0){
						long lastPackagedPosition = getExpandableListPosition(getFirstVisiblePosition());
						if(getPackedPositionType(lastPackagedPosition) != PACKED_POSITION_TYPE_NULL){
							if(getPackedPositionType(lastPackagedPosition) == PACKED_POSITION_TYPE_GROUP){
								endPosition[0] = getPackedPositionGroup(lastPackagedPosition);
								endPosition[1] = 0;
							} else {
								endPosition[0] = getPackedPositionGroup(lastPackagedPosition);
								endPosition[1] = getPackedPositionChild(lastPackagedPosition);
							}
						}
					}
					else if(getLastVisiblePosition() != -1 && getChildAt(getLastVisiblePosition()) != null)	//phoenix changed to this 20.Jan.2014
					{
						if(y > getChildAt(getLastVisiblePosition()).getBottom()){
							long lastPackagedPosition = getExpandableListPosition(getLastVisiblePosition());
							if(getPackedPositionType(lastPackagedPosition) != PACKED_POSITION_TYPE_NULL){
								if(getPackedPositionType(lastPackagedPosition) == PACKED_POSITION_TYPE_GROUP){
									endPosition[0] = getPackedPositionGroup(lastPackagedPosition);
									endPosition[1] = 0;
								} else {
									if(getPackedPositionType(lastPackagedPosition) == PACKED_POSITION_TYPE_CHILD){
										endPosition[0] = getPackedPositionGroup(lastPackagedPosition);
										if(endPosition[0] == startPosition[0]){
											endPosition[1] = getPackedPositionChild(lastPackagedPosition);
										} else {
											endPosition[1] = getPackedPositionChild(lastPackagedPosition)+1;
										}
									}
								}
							}
						} 
						else
						{
							endPosition[0] = -1;
							endPosition[1] = -1;
						}
					}
					else {
						endPosition[0] = -1;
						endPosition[1] = -1;
					}
				}
				
				stopDrag(startFlatPosition - getFirstVisiblePosition());
				if (dropListener != null) 
					dropListener.onDrop(startPosition, endPosition);
				break;
		}
		return true;
	}	
	
	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
	    return super.onInterceptTouchEvent(ev);
	}
	
	private void drag(int x, int y) {
		if (dragView != null) {
			WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams) dragView.getLayoutParams();
			layoutParams.x = 0;
			layoutParams.y = y - dragPointOffset;
			WindowManager manager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
			manager.updateViewLayout(dragView, layoutParams);

			if (dragListener != null)
				dragListener.onDrag(x, y, this);
		}
	}
	
	
	private void startDrag(int itemIndex, int y) {
		stopDrag(itemIndex);

		View item = getChildAt(itemIndex);
		if (item == null) return;
		item.setDrawingCacheEnabled(true);
		if (dragListener != null)
			dragListener.onStartDrag(item);
		
        Bitmap bitmap = Bitmap.createBitmap(item.getDrawingCache());
        
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.gravity = Gravity.TOP;
        layoutParams.x = 0;
        layoutParams.y = y - dragPointOffset;

        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
                | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS;
        layoutParams.format = PixelFormat.TRANSLUCENT;
        layoutParams.windowAnimations = 0;
        
        ImageView view = new ImageView(context);
        view.setImageBitmap(bitmap);      

        WindowManager manager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        manager.addView(view, layoutParams);
        dragView = view;
	}	
	
	private void stopDrag(int itemIndex) {
		if (dragView != null) {
			if (dragListener != null)
				dragListener.onStopDrag(getChildAt(itemIndex));
            dragView.setVisibility(GONE);
            WindowManager manager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
            manager.removeView(dragView);
            dragView.setImageDrawable(null);
            dragView = null;
        }
	}
	
	
	
	public void setDropListener(DropListener listener) {
		dropListener = listener;
	}

	public void setRemoveListener(RemoveListener listener) {
		removeListener = listener;
	}
	
	public void setDragListener(DragListener listener) {
		dragListener = listener;
	}
	
}