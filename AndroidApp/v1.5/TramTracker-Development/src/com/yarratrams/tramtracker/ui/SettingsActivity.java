package com.yarratrams.tramtracker.ui;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.android.androidtooltip.TooltipManager;
import com.android.androidtooltip.TooltipManager.Gravity;
import com.android.androidtooltip.TutorialTooltip;
import com.google.android.gcm.GCMRegistrar;
import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.Constants;
import com.yarratrams.tramtracker.ui.util.FeaturesPreferences;
import com.yarratrams.tramtracker.ui.util.OtherLevelAds;
import com.yarratrams.tramtracker.ui.util.SettingsPreference;
import com.yarratrams.tramtracker.ui.util.TutorialPreferences;

public class SettingsActivity extends Activity
		implements
			TooltipManager.onTooltipClosingCallback {

	// Variables
	public static final int GROUP_TAB = TramTrackerMainActivity.TAB_MORE;
	Activity activity;
	SettingsPreference settingsPreference;
	public static ProgressDialog progressDialog;

	// Tutorial Variables
	TooltipManager tooltipManager;
	TutorialTooltip tempTutorialTooltip;
	ArrayList<TutorialTooltip> alTutorialTooltips;
	int tooltipIndex = 0;
	boolean isTutorialRunning = false;
	boolean backShow0 = false;
	boolean backShow1 = false;
	boolean backShow2 = false;
	TutorialPreferences tutorialPreferences;

	// UIElements
	public static CheckBox cbReceiveNotifications, cbShowTooltip;
	RelativeLayout settings_manage_notifications, settings_manage_reminders;
	ImageView divider;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings_page);
		activity = this;

		settingsPreference = new SettingsPreference(activity);

		tutorialPreferences = new TutorialPreferences(activity);

		initUI();

		// tutorial init
		tooltipManager = new TooltipManager(TramTrackerMainActivity.instance);
		// tutorialPreferences.resetAll();

		// show tutorial if first time
		if (tutorialPreferences.getSettingsTutorial() == false) {

			showTutorialDialog();

		}

		cbReceiveNotifications
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						// TODO Auto-generated method stub

						settingsPreference.setReciveNotifications(isChecked);

						progressDialog = new ProgressDialog(
								TramTrackerMainActivity.instance);
						progressDialog.setTitle("Please Wait");
						if (isChecked) {
							progressDialog
									.setMessage("Registering device for notifications");

							GCMRegistrar
									.register(activity, Constants.SENDER_ID);
							settings_manage_notifications
									.setVisibility(View.VISIBLE);
							divider.setVisibility(View.VISIBLE);

						} else {
							settings_manage_notifications
									.setVisibility(View.GONE);
							divider.setVisibility(View.GONE);
							progressDialog
									.setMessage("Unregistering device for notifications");
							GCMRegistrar.unregister(activity);

						}

						progressDialog.setCanceledOnTouchOutside(false);
						progressDialog.show();
						System.out.println("-- I got id: "
								+ GCMRegistrar.getRegistrationId(activity));

						if (isTutorialRunning) {
							if (tooltipIndex == 0) {
								if (!isChecked) {
									dismissAllTooltip();
									tooltipIndex = 1;
									showNextTutorialTooltip();
								} else {
									dismissAllTooltip();
									tooltipIndex = 0;
									showNextTutorialTooltip();
								}
							}
							if (tooltipIndex == 1) {
								if (!isChecked) {
									dismissAllTooltip();
									tooltipIndex = -1;
									showNextTutorialTooltip();
								} else {
									dismissAllTooltip();
									tooltipIndex = 0;
									showNextTutorialTooltip();
								}
							}
							if (tooltipIndex == 2) {
								dismissAllTooltip();
								tooltipIndex = 1;
								showNextTutorialTooltip();
							}
						}

					}
				});

		cbShowTooltip.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked) {
					tutorialPreferences.resetAll();
					tutorialPreferences.setManualSet(true);
				} else {
					tutorialPreferences.disableAll();
				}
			}
		});

		settings_manage_notifications.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (isTutorialRunning) {
					if (tooltipIndex == 1) {
						tutorialPreferences.setDisruptionTutorial(false);
					} else {
						tutorialPreferences.setDisruptionTutorial(true);
					}
					backShow2 = true;
				}
				Intent intent = new Intent(activity, DisruptionsActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				TramTrackerMainActivity.getAppManager().show(
						TramTrackerMainActivity.TAB_MORE,
						getResources().getString(
								R.string.tag_manage_notifications), intent);

			}
		});

		settings_manage_reminders.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (isTutorialRunning) {
					if (tooltipIndex == 0) {
						tutorialPreferences.setAlarmTutorial(true);
						backShow0 = true;
					} else if (tooltipIndex == 1) {
						tutorialPreferences.setAlarmTutorial(true);
						backShow1 = true;
					} else if (tooltipIndex == 2) {
						tutorialPreferences.setAlarmTutorial(false);
						isTutorialRunning = false;
					} else {
						tutorialPreferences.setAlarmTutorial(true);
					}
				}

				Intent intent = new Intent(activity, RemindersActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

				TramTrackerMainActivity.getAppManager()
						.show(TramTrackerMainActivity.TAB_MORE,
								getResources().getString(
										R.string.tag_manage_reminders), intent);

			}
		});

		// call ads
		OtherLevelAds.showAds(activity, R.id.rich_banner_fragment105555,
				FeaturesPreferences
						.getAdsFlag(TramTrackerMainActivity.instance));
	}

	private void initUI() {
		// TODO Auto-generated method stub
		cbReceiveNotifications = (CheckBox) findViewById(R.id.cbReceiveNotifications);
		cbShowTooltip = (CheckBox) findViewById(R.id.cbShowTooltip);

		settings_manage_notifications = (RelativeLayout) findViewById(R.id.settings_manage_notifications);
		settings_manage_reminders = (RelativeLayout) findViewById(R.id.settings_manage_reminders);

		divider = (ImageView) findViewById(R.id.divider);

		cbReceiveNotifications.setChecked(settingsPreference
				.getReciveNotifications());
		if (settingsPreference.getReciveNotifications()) {
			settings_manage_notifications.setVisibility(View.VISIBLE);
			divider.setVisibility(View.VISIBLE);
		} else {
			settings_manage_notifications.setVisibility(View.GONE);
			divider.setVisibility(View.GONE);
		}
		boolean check = tutorialPreferences.showTooltipCbStatus();
		cbShowTooltip.setChecked(tutorialPreferences.showTooltipCbStatus());
	}

	// Override back press
	@Override
	public void onBackPressed() {
		TramTrackerMainActivity.getAppManager().back();
		isTutorialRunning = false;
	}

	@Override
	public void onResume() {
		super.onResume();
		if (isTutorialRunning && backShow0) {
			dismissAllTooltip();
			tooltipIndex = -1;
			showNextTutorialTooltip();
			backShow0 = false;
		}
		if (isTutorialRunning && backShow1) {
			dismissAllTooltip();
			tooltipIndex = 0;
			showNextTutorialTooltip();
			backShow1 = false;
		}
		if (isTutorialRunning && backShow2) {
			dismissAllTooltip();
			tooltipIndex = 1;
			showNextTutorialTooltip();
			backShow2 = false;
		}
	}

	@Override
	public void onClosing(int id, boolean fromUser, boolean containsTouch) {
		// TODO Auto-generated method stub

		switch (id) {
			case R.id.cbReceiveNotifications :

				// showNextTutorialTooltip();

				break;

			case R.id.settings_manage_notifications :

				// showNextTutorialTooltip();

				break;

			case R.id.settings_manage_reminders :
				isTutorialRunning = false;

			default :
				break;
		}

	}

	public void createTutorialToolTips() {

		// String disruptionsText =
		// "To receive disruption notifications, you must first switch on notifications by checking this box.";
		String disruptionsText = "To get disruption notifications, TAP this box.";
		// String manageText =
		// "Tap <b>Manage Notifications</b> to set up your Travel Times and Routes.";
		String manageText = "TAP Manage Notifications to select your travel times and routes.";
		String alarmText = "TAP here to manage your alarms.";
		// String alarmText =
		// "If you have alarms set up, you can manage them from the 'Manage Alarms' section.";

		alTutorialTooltips = new ArrayList<TutorialTooltip>();

		tempTutorialTooltip = new TutorialTooltip();
		tempTutorialTooltip.createTutorialTooltipNoDismiss(tooltipManager,
				cbReceiveNotifications, this, Gravity.TOP, disruptionsText);
		alTutorialTooltips.add(tempTutorialTooltip);

		tempTutorialTooltip = new TutorialTooltip();
		tempTutorialTooltip
				.createTutorialTooltipNoDismiss(tooltipManager,
						settings_manage_notifications, this, Gravity.BOTTOM,
						manageText);
		alTutorialTooltips.add(tempTutorialTooltip);

		tempTutorialTooltip = new TutorialTooltip();
		tempTutorialTooltip.createTutorialTooltip(tooltipManager,
				settings_manage_reminders, this, Gravity.TOP, alarmText);
		alTutorialTooltips.add(tempTutorialTooltip);

	}

	public void showTutorialDialog() {

		// String msg =
		// "You can now subscribe to disruption notifications and be notified when there is a disruption on your tram route. There's also a section for you to manage your tram and stop arrival alarms.\n\nIf you would like to skip the tutorial and view it next time, please select the 'Remind Me Later' option below.";
		String msg = "To improve your tramTRACKER\nexperience, there are quick tutorials to guide you through the new features.\n\nTo turn off this reminder go to More > my tramTRACKER > Interactive Tutorials.";
		AlertDialog.Builder builder = new Builder(
				TramTrackerMainActivity.instance);
		builder.setTitle("Interactive Tutorial");
		builder.setMessage(msg);
		builder.setPositiveButton("View Now",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						createTutorialToolTips();
						if (cbReceiveNotifications.isChecked()) {
							tooltipIndex = 1;
						} else {
							tooltipIndex = 0;
						}
						if (alTutorialTooltips != null) {
							isTutorialRunning = true;
							alTutorialTooltips.get(tooltipIndex).showTooltip();
						}
						dialog.dismiss();
						// tutorialPreferences.setSettingsTutorial(true);

					}
				});
		builder.setNegativeButton("Remind Me Later",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						tutorialPreferences.setDisruptionTutorial(true);
						tutorialPreferences.setAlarmTutorial(true);
					}
				});

		AlertDialog alertDialog = builder.create();
		alertDialog.setCanceledOnTouchOutside(false);
		alertDialog.setCancelable(false);
		alertDialog.show();

	}

	public void showNextTutorialTooltip() {
		try {

			alTutorialTooltips.get(tooltipIndex).closeTutorialTooltip(
					tooltipManager);
		} catch (Exception e) {
			// TODO: handle exception
		}
		tooltipIndex++;
		if (tooltipIndex < alTutorialTooltips.size()) {
			try {
				alTutorialTooltips.get(tooltipIndex).showTooltip();
			} catch (Exception e) {
				// TODO: handle exception
				isTutorialRunning = false;
			}
		} else {
			isTutorialRunning = false;
		}
	}

	public void dismissAllTooltip() {

		for (int i = 0; i < alTutorialTooltips.size(); i++) {
			try {
				alTutorialTooltips.get(i).closeTutorialTooltip(tooltipManager);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

	}

	@Override
	public void onPause() {
		super.onPause();
		if (alTutorialTooltips != null) {
			for (int i = 0; i < alTutorialTooltips.size(); i++) {
				try {
					alTutorialTooltips.get(i).closeTutorialTooltip(
							tooltipManager);
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}
	}

}
