package com.yarratrams.tramtracker.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.Executors;

import android.app.Activity;
import android.app.ActivityGroup;
import android.app.AlarmManager;
import android.app.LocalActivityManager;
import android.app.PendingIntent;
import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.yarratrams.tramtracker.GCMIntentService;
import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.backgroundservice.AlaramReceiver;
import com.yarratrams.tramtracker.backgroundservice.TaskService;
import com.yarratrams.tramtracker.db.PopulateDatabase;
import com.yarratrams.tramtracker.objects.Constants;
import com.yarratrams.tramtracker.objects.PredictedArrivalTime;
import com.yarratrams.tramtracker.objects.Reminders;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.objects.Tram;
import com.yarratrams.tramtracker.tasks.DeviceTokenTask;
import com.yarratrams.tramtracker.tasks.MultiPageUrlsTask;
import com.yarratrams.tramtracker.ui.util.Console;
import com.yarratrams.tramtracker.ui.util.FeaturesPreferences;
import com.yarratrams.tramtracker.ui.util.FreeTramZoneDialog;
import com.yarratrams.tramtracker.ui.util.MultiPagePreferences;
import com.yarratrams.tramtracker.ui.util.ScreenDescriptor;
import com.yarratrams.tramtracker.ui.util.TutorialPreferences;
import com.yarratrams.tramtracker.ui.util.UniqueIdPref;
import com.yarratrams.tramtracker.webserviceinteraction.UpdateManager;
import com.yarratrams.tramtracker.widget.TramTrackerWidget;

public class TramTrackerMainActivity extends TabActivity
implements
OnTabChangeListener {
	public static final String SHARED_PREFERENCES = "com.yarratrams.tramtracker";
	public static final String DEFAULT_ENTRY_SCREEN = "com.yarratrams.tramtracker.DefaultEntryScreen";
	public static final String FAVOURITES_NEWFAVOURITES = "com.yarratrams.tramtracker.FavouritesActivity";
	public static final int DEFAULT_ENTRY_NEARBY = 0;
	public static final int DEFAULT_ENTRY_FAVOURITES = 1;
	public static final int DEFAULT_ENTRY_NEARESTFAVOURITE = 2;
	public static final int DEFAULT_ENTRY_TRACKERID = 3;

	public static final int TAB_NEARBY = 0;
	public static final int TAB_FAVOURITES = 1;
	public static final int TAB_ROUTES = 2;
	public static final int TAB_MYTRAM = 3;
	public static final int TAB_MORE = 4;

	public static TramTrackerMainActivity instance;
	public TabHost tabHost;
	private boolean onTabChanged;

	private ArrayList<ScreenDescriptor> activityHistory;

	private Stop currentAlarmStop;
	private Intent alarmServiceIntent;

	private SharedPreferences preferences;
	private int selectedEntryScreen;

	/*
	 * Adil added
	 */

	// for managing updates
	private boolean updateAvailable;
	private UpdateManager updateManager;
	private UpdateActivity updateActivity;
	MultiPageUrlsTask multiPageUrlsTask;

	// for background service and alarm manager

	AlarmManager alarmManager;
	Intent intentTooBroadcastRec;
	PendingIntent pendingIntentBroadcastRec;
	
	FeaturesPreferences featuresPreferences;

	/*
	 * Ends
	 */

	/*
	 * FREE TRAM ZONE
	 */
	public final String SHOW_FTZ_DIALOG = "ftzdialog";

	public Handler handlerToast = new Handler() {
		public void handleMessage(Message m) {
			TramTrackerMainActivity.getAppManager().displayErrorMessage(
					m.getData().getString("error"));
		}
	};

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_screen);

		instance = this;

		System.out.println("-- Files saved: "
				+ Arrays.deepToString(instance.fileList()));

//		 This is how we download the Tram Tracker database
//		 PopulateDatabase pd = new PopulateDatabase(this);
//		 pd.populateAll();
		
		
		
		exportDB();

		/*
		 * FREE TRAM ZONE
		 */

		/*
		 * Perform update check a async task is called, all data fetched is
		 * saved and installed on next run, from splash screen activity.
		 */

		UpdateManager updateManager = new UpdateManager();
		updateManager.checkForUpdate(this);

		// crete unique id for device, will be reset on new installtion
		setUniqueId();

		DeviceTokenTask dtt = new DeviceTokenTask();
		dtt.execute();

		updateAvailable = false;
		activityHistory = new ArrayList<ScreenDescriptor>();
		onTabChanged = false;

		tabHost = getTabHost();
		tabHost.addTab(createTab(NearbyTabActivityManager.class, getResources()
				.getString(R.string.tag_nearby_tab),
				getResources().getString(R.string.tab_nearby),
				R.drawable.tab_nearby));
		tabHost.addTab(createTab(FavouritesTabActivityManager.class,
				getResources().getString(R.string.tag_favourites_tab),
				getResources().getString(R.string.tab_favourites),
				R.drawable.tab_favourites));
		tabHost.addTab(createTab(RoutesTabActivityManager.class, getResources()
				.getString(R.string.tag_routes_tab),
				getResources().getString(R.string.tab_routes),
				R.drawable.tab_routes));
		tabHost.addTab(createTab(MyTramTabActivityManager.class, getResources()
				.getString(R.string.tag_mytram_tab),
				getResources().getString(R.string.tab_mytram),
				R.drawable.tab_mytram));
		tabHost.addTab(createTab(MoreTabActivityManager.class, getResources()
				.getString(R.string.tag_more_tab),
				getResources().getString(R.string.tab_more),
				R.drawable.tab_more));

		tabHost.setOnTabChangedListener(this);

		preferences = getSharedPreferences(SHARED_PREFERENCES, MODE_PRIVATE);
		selectedEntryScreen = preferences.getInt(DEFAULT_ENTRY_SCREEN, 0);
		setFirstScreen(selectedEntryScreen);
		
		disableTutorialByDefault();

		featuresPreferences = new FeaturesPreferences(instance);

		//showFTZDialog();
		// }

		/*
		 * FREE TRAM ZONE
		 */

		// disable interactive tooltip

		// TutorialPreferences tutorialPreferences = new
		// TutorialPreferences(this);



		// final MultiPagePreferences multiPagePreferences = new
		// MultiPagePreferences(instance);

		// if(multiPagePreferences.getIsNewAvailable())
		// {
		// if(multiPagePreferences.getIsFirstTime())
		// {
		// multiPagePreferences.print();
		// if(multiPagePreferences.hasNewUrl()){
		// MultiPageDialog multiPageDialog;
		// multiPageDialog = new MultiPageDialog(instance, instance);
		// multiPageDialog.setOnDismissListener(new OnDismissListener(){
		//
		// @Override
		// public void onDismiss(DialogInterface arg0) {
		// // TODO Auto-generated method stub
		// multiPagePreferences.removeStoredUrl();
		// }
		//
		// });
		// multiPageDialog.show();
		//
		// }
		// }

		// }

		Bundle bundle = getIntent().getExtras();
		
		System.out.println("has bundle: " + bundle != null);

		if (bundle != null) {
			if (bundle.containsKey(TaskService.TASK_MESSAGE)) {
				System.out.println("-- reached here from notifications");
				if (bundle.getInt(TaskService.TASK_MESSAGE) == Reminders.TRAM_ALARM) {
					Stop stop = bundle.getParcelable(PIDActivity.INTENT_KEY);
					TramTrackerMainActivity.getAppManager().callSelection(
							getStopDescription(stop));
					goToPIDScreen(stop);
				} else if (bundle.getInt(TaskService.TASK_MESSAGE) == Reminders.SCHEDULE_DEP_ALARM) {
					Stop stop = bundle.getParcelable(PIDActivity.INTENT_KEY);
					TramTrackerMainActivity.getAppManager().callSelection(
							getStopDescription(stop));
					goToPIDScreen(stop);
				} else if (bundle.getInt(TaskService.TASK_MESSAGE) == Reminders.STOP_ALARM) {
					Tram tram = bundle
							.getParcelable(OnBoardActivity.INTENT_KEY);
					System.out.println("-- tram object: "
							+ tram.getHeadboardNo());
					TramTrackerMainActivity.getAppManager().callSelection(
							getDescription(tram));
					goToOnBoardScreen(tram);
				}

			} else if (bundle.containsKey(TramTrackerWidget.FROM_WIDGET)) {
				System.out.println("-- i am here from widget");
				Stop stop = bundle.getParcelable(PIDActivity.INTENT_KEY);
				TramTrackerMainActivity.getAppManager().callSelection(
						getStopDescription(stop));
				goToPIDScreen(stop);
			}
			
			else if (bundle.containsKey(TramTrackerWidget.FROM_WIDGET_TO_NEARBY))
			{
				tabHost.setCurrentTab(TAB_NEARBY);
				Intent intent = new Intent(getTabGroup(TAB_NEARBY), NearbyActivity.class);
				show(TAB_NEARBY,
						getResources().getString(R.string.tag_nearby_screen),
						intent);
			}
			
			else if (bundle.containsKey(GCMIntentService.FROM_NOTIFICATION)) {

				tabHost.setCurrentTab(TAB_MORE);
				// onTabChanged(
				// getResources().getString(R.string.tag_more_tab));
				//
				TramTrackerMainActivity.getAppManager().callSelection(
						getString(R.string.more_servicefeed));
				Intent intent = new Intent(instance, ServiceFeedActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				TramTrackerMainActivity.getAppManager().show(
						TramTrackerMainActivity.TAB_MORE,
						getResources().getString(
								R.string.tag_service_feed_screen), intent);
				// try {
				//
				// System.out.println("-- intent: " +( (intent==null) ? "NUll" :
				// "Not null" ));
				//
				//
				//
				// } catch (Exception e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }

			}
		}
		else
		{
			
			Console.print("I am here to start multi page");

			MultiPagePreferences multiPagePreferences;
			multiPagePreferences = new MultiPagePreferences(instance);


			if(  (System.currentTimeMillis() -  multiPagePreferences.getLastOpenTime() > 86400000) )
			{

				multiPageUrlsTask = new MultiPageUrlsTask(instance, instance);
				multiPageUrlsTask.execute();
			}


			//showFTZDialog();
		}

	}

	private void disableTutorialByDefault()
	{
		TutorialPreferences tutorialPre = new TutorialPreferences(this);
		if(!tutorialPre.getManaualSet())
		 tutorialPre.disableAll();
	}
	/*
	 * FREE TRAM ZONE
	 */
	private void showFTZDialog() {
		// TODO Auto-generated method stub

		final FreeTramZoneDialog freeTramZoneDialog;

		if (getFTZDialogCheck()) {
			freeTramZoneDialog = new FreeTramZoneDialog(instance);
			freeTramZoneDialog.show();

			freeTramZoneDialog.bFreeTramDialogContinue
			.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					setFTZDialogCheck(false);
					freeTramZoneDialog.dismiss();

				}
			});
		}
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();

		if (getIntent().getExtras() != null) {
			// System.out.println("-- inside if");
			System.out.println("-- from widget: "
					+ getIntent().getExtras().getBoolean(
							TramTrackerWidget.FROM_WIDGET));
			// if(getIntent().getExtras().containsKey(TramTrackerWidget.FROM_WIDGET))
			// {
			// if(getIntent().getExtras().getBoolean(TramTrackerWidget.FROM_WIDGET))
			// {
			//
			// Intent intent = new Intent(instance, PIDActivity.class);
			// intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			// intent.putExtra(PIDActivity.INTENT_KEY,
			// getIntent().getExtras().getParcelable(PIDActivity.INTENT_KEY));
			//
			// TramTrackerMainActivity.getAppManager().show(TramTrackerMainActivity.TAB_NEARBY,
			// getResources().getString(R.string.tag_pid_screen), intent);
			//
			// Toast.makeText(instance, "is from widget", 0).show();
			// }
			// }
		}
		else
		{
			// call multi page url task
			//			multiPageUrlsTask = new MultiPageUrlsTask(instance, instance);
			//			multiPageUrlsTask.execute();
			//			
			//			
			//			showFTZDialog();
		}
		// else
		//
	}

	/*
	 * adil added: for starting backgorund service as alarm manager
	 */

	public void startAlarm() {
		// get alarm params
		Date d = new Date();
		long timeTilMinuteChange = 60 * 1000 - d.getSeconds() * 1000;
		long startTime = System.currentTimeMillis() + +timeTilMinuteChange;

		alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

		intentTooBroadcastRec = new Intent(instance, AlaramReceiver.class);

		intentTooBroadcastRec.putExtra(AlaramReceiver.ACTION_ALARM,
				AlaramReceiver.ACTION_ALARM);

		pendingIntentBroadcastRec = PendingIntent.getBroadcast(instance, 99150,
				intentTooBroadcastRec, PendingIntent.FLAG_UPDATE_CURRENT);

		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, startTime,
				Constants.SERVICE_REPEAT_INTERVAL, pendingIntentBroadcastRec);

	}

	/*
	 * ends
	 */

	public void savePreferences(int selectedEntryScreen) {
		this.selectedEntryScreen = selectedEntryScreen;
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt(DEFAULT_ENTRY_SCREEN, selectedEntryScreen);
		editor.commit();
	}

	private TabSpec createTab(final Class<?> intentClass, final String tag,
			final String title, final int drawable) {
		final Intent intent = new Intent().setClass(this, intentClass);
		final View tab = LayoutInflater.from(getTabHost().getContext())
				.inflate(R.layout.tab, null);
		((TextView) tab.findViewById(R.id.tab_text)).setText(title);
		((ImageView) tab.findViewById(R.id.tab_icon))
		.setImageResource(drawable);

		return getTabHost().newTabSpec(tag).setIndicator(tab)
				.setContent(intent);
	}

	static public TramTrackerMainActivity getAppManager() {
		return instance;
	}

	public void showTab(int activityID) {
		tabHost.setCurrentTab(activityID);
	}

	public void back() {
		if (activityHistory.size() > 1) {
			activityHistory.remove(activityHistory.size() - 1);

			ScreenDescriptor screen = activityHistory.get(activityHistory
					.size() - 1);
			if (tabHost.getCurrentTab() != screen.getTabID()) {
				onTabChanged = true;
				tabHost.setCurrentTab(screen.getTabID());
			}
			System.out.println("screen.getTabID:" + screen.getTabID()
					+ " screen.getScreenID:" + screen.getScreenID());
			Intent intent = screen.getActivity().getIntent()
					.setFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY);
			Window window = screen.getGroup().getLocalActivityManager()
					.startActivity(screen.getScreenID(), intent);
			screen.getGroup().setContentView(window.getDecorView());
		} else {
			finish();
		}
	}

	public ActivityGroup requestDifferentTab(int tabID) {
		onTabChanged = true;
		showTab(tabID);
		ActivityGroup group = getTabGroup(tabID);
		if (group == null) {
			group = getTabGroup(tabID);
		}
		return group;
	}

	public int getCurrentTabID() {
		return tabHost.getCurrentTab();
	}

	public void show(int tabID, String screenID, Intent intent) {
		if (tabHost.getCurrentTab() != tabID) {
			onTabChanged = true;
			tabHost.setCurrentTab(tabID);
		}

		ActivityGroup group = getTabGroup(tabID);
		LocalActivityManager lav = group.getLocalActivityManager();
		if (lav != null) {
			View view = lav.startActivity(screenID, intent).getDecorView();
			addToHistory(tabID, screenID, group);
			group.setContentView(view);
			view.requestFocus();
		}else{
			for (int i = 0; i < 5; i++) {
				tabHost.setCurrentTab(i);
			}
			tabHost.setCurrentTab(tabID);
			ActivityGroup atgroup = getTabGroup(tabID);
			LocalActivityManager atlav = atgroup.getLocalActivityManager();
			View view = atlav.startActivity(screenID, intent).getDecorView();
			addToHistory(tabID, screenID, atgroup);
			atgroup.setContentView(view);
			view.requestFocus();
		}
	}

	public void addToHistory(int tabID, String screenID, ActivityGroup group) {
		ScreenDescriptor screen = new ScreenDescriptor();
		screen.setTabID(tabID);
		screen.setScreenID(screenID);
		screen.setGroup(group);
		screen.setActivity(group.getCurrentActivity());

		activityHistory.add(screen);
	}

	public void clearHistory() {
		activityHistory.clear();
	}

	public ActivityGroup getTabGroup(int tabID) {
		ActivityGroup group = null;
		switch (tabID) {
			case TAB_NEARBY :
				group = NearbyTabActivityManager.getGroup();
				break;
			case TAB_FAVOURITES :
				group = FavouritesTabActivityManager.getGroup();
				break;
			case TAB_ROUTES :
				group = RoutesTabActivityManager.getGroup();
				break;
			case TAB_MYTRAM :
				group = MyTramTabActivityManager.getGroup();
				break;
			case TAB_MORE :
				group = MoreTabActivityManager.getGroup();
				break;
		}
		return group;
	}

	@Override
	public void onTabChanged(String tabId) {

		System.out.println("-- tabId: " + tabId);
		if (onTabChanged) {
			onTabChanged = false;
			return;
		}
		clearHistory();
		setTabFirstScreen(tabId);
	}

	private void setFirstScreen(int entryScreen) {
		Intent intent;
		if (entryScreen == DEFAULT_ENTRY_NEARBY) {
			tabHost.setCurrentTab(TAB_NEARBY);
			intent = new Intent(getTabGroup(TAB_NEARBY), NearbyActivity.class);
			show(TAB_NEARBY,
					getResources().getString(R.string.tag_nearby_screen),
					intent);
		} else if (entryScreen == DEFAULT_ENTRY_FAVOURITES) {
			tabHost.setCurrentTab(TAB_FAVOURITES);
			intent = new Intent(getTabGroup(TAB_FAVOURITES),
					FavouritesActivity.class);
			show(TAB_FAVOURITES,
					getResources().getString(R.string.tag_favourites_screen),
					intent);
		} else if (entryScreen == DEFAULT_ENTRY_NEARESTFAVOURITE) {
			tabHost.setCurrentTab(TAB_FAVOURITES);
			intent = new Intent(getTabGroup(TAB_FAVOURITES), PIDActivity.class);
			show(TAB_FAVOURITES,
					getResources().getString(R.string.tag_pid_screen), intent);
		} else if (entryScreen == DEFAULT_ENTRY_TRACKERID) {
			tabHost.setCurrentTab(TAB_MORE);
			intent = new Intent(getTabGroup(TAB_MORE),
					SearchTrackerIDActivity.class);
			show(TAB_MORE,
					getResources().getString(R.string.tag_trackerid_screen),
					intent);
		}
	}

	private void setTabFirstScreen(String tabID) {
		if (tabID.equalsIgnoreCase(getResources().getString(
				R.string.tag_nearby_tab))) {
			Intent intent = new Intent(getTabGroup(TAB_NEARBY),
					NearbyActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
			show(TAB_NEARBY,
					getResources().getString(R.string.tag_nearby_screen),
					intent);

		} else if (tabID.equalsIgnoreCase(getResources().getString(
				R.string.tag_favourites_tab))) {
			Intent intent = new Intent(getTabGroup(TAB_FAVOURITES),
					FavouritesActivity.class);
			show(TAB_FAVOURITES,
					getResources().getString(R.string.tag_favourites_screen),
					intent);

		} else if (tabID.equalsIgnoreCase(getResources().getString(
				R.string.tag_routes_tab))) {
			Intent intent = new Intent(getTabGroup(TAB_ROUTES),
					RoutesEntryActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
			show(TAB_ROUTES,
					getResources().getString(R.string.tag_routes_entry_screen),
					intent);

		} else if (tabID.equalsIgnoreCase(getResources().getString(
				R.string.tag_mytram_tab))) {
			Intent intent = new Intent(getTabGroup(TAB_MYTRAM),
					MyTramActivity.class);
			show(TAB_MYTRAM,
					getResources().getString(R.string.tag_mytram_screen),
					intent);

		} else if (tabID.equalsIgnoreCase(getResources().getString(
				R.string.tag_more_tab))) {
			Intent intent = new Intent(getTabGroup(TAB_MORE),
					MoreActivity.class);
			show(TAB_MORE, getResources().getString(R.string.tag_more_screen),
					intent);
		}
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			getLocalActivityManager().getCurrentActivity().openOptionsMenu();
			return true;
		} else if (keyCode == KeyEvent.KEYCODE_BACK) {
			this.back();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	public void setMenuBackground(final Activity activity) {
		activity.getLayoutInflater().setFactory(new LayoutInflater.Factory() {
			@Override
			public View onCreateView(String name, Context context,
					AttributeSet attributeSet) {
				if (name.equalsIgnoreCase("com.android.internal.view.menu.IconMenuItemView")) {
					try {
						final LayoutInflater f = activity.getLayoutInflater();
						final View view = f
								.createView(name, null, attributeSet);
						new Handler().post(new Runnable() {
							public void run() {
								view.setBackgroundResource(R.drawable.darkgrey_cell_background);
								// ((TextView) view).setTextColor(Color.WHITE);
								setTextColor(view);
							}
						});
						return view;
					} catch (Exception e) {
						// System.out.println("OPTIONS BACKGROUND ERROR");
					}
				}
				return null;
			}
		});
	}

	private void setTextColor(final View view) {
		try {
			final Method setTextColor = view.getClass().getMethod(
					"setTextColor", int.class);
			setTextColor.invoke(view, "0xFFFFFFFF");
		} catch (Exception e) {
			// System.out.println("OPTIONS TEXT ERROR");
			try {
				((TextView) view).setTextColor(Color.WHITE);
			} catch (Exception ex) {
			}
		}
	}

	public void requestDirectionsService(GeoPoint destination) {
		String daddr = String.valueOf(destination.getLatitudeE6() / 1E6) + ","
				+ String.valueOf(destination.getLongitudeE6() / 1E6);
		String url = getResources().getString(R.string.directions_url_base);
		url = url.concat(daddr);

		Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
				Uri.parse(url));
		startActivity(intent);
	}

	public void displayErrorMessage(String message) {
		Toast.makeText(this, message, Toast.LENGTH_LONG).show();
		// AlertDialog.Builder builder = new AlertDialog.Builder(this);
		// builder.setMessage(message).setCancelable(true);
		// AlertDialog alert = builder.create();
		// try{
		// alert.show();
		// } catch(Exception e){
		// }
	}

	public void startAlarmService(Stop stop,
			ArrayList<PredictedArrivalTime> stops, Tram tram) {
		currentAlarmStop = stop;
	}

	public void stopAlarmService() {
		stopService(alarmServiceIntent);
		currentAlarmStop = null;
	}

	public boolean hasAlarm(Stop stop) {
		if (currentAlarmStop != null) {
			if (stop.getTrackerID() == currentAlarmStop.getTrackerID()) {
				return true;
			}
		}
		return false;
	}

	public void notifyUpdateAvailable(UpdateManager updateManager) {
		updateAvailable = true;
		this.updateManager = updateManager;
		displayErrorMessage(getResources().getString(
				R.string.notify_update_available));
	}

	public boolean isUpdateAvailable() {
		return updateAvailable;
	}

	public void update(UpdateActivity activity) {
		this.updateActivity = activity;
		updateManager.update();
	}

	public void notifyFinishUpdate(boolean successfull) {
		updateActivity.notifyFinishUpdate(successfull);
		if (successfull) {
			updateAvailable = false;
		}
	}

	public void callSelection(String selection) {
		AccessibilityManager accessibilityManager = (AccessibilityManager) getSystemService(Context.ACCESSIBILITY_SERVICE);
		if (accessibilityManager.isEnabled()) {
			AccessibilityEvent event = AccessibilityEvent
					.obtain(AccessibilityEvent.TYPE_VIEW_FOCUSED);
			event.setContentDescription(selection);
			accessibilityManager.sendAccessibilityEvent(event);
		}
	}

	public Calendar getLastUpdateDate() {
		preferences = getSharedPreferences(Constants.kSharedPrefIdentifier,
				Activity.MODE_PRIVATE);
		Calendar presetCalendar = new GregorianCalendar(
				Constants.kLastUpdateYear, Constants.kLastUpdateMonth,
				Constants.kLastUpdateDayOfMonth);
		Calendar lastUpdateCalendar = new GregorianCalendar();
		lastUpdateCalendar.setTime(new Date(preferences.getLong(
				Constants.kLastUpdateDate, presetCalendar.getTimeInMillis())));

		Calendar realDatabaseCalendar = new GregorianCalendar();
		if (presetCalendar.after(lastUpdateCalendar)) {
			realDatabaseCalendar = presetCalendar;
		} else {
			realDatabaseCalendar = lastUpdateCalendar;
		}

		return realDatabaseCalendar;
	}

	/*
	 * FREE TRAM ZONE
	 */

	public void setFTZDialogCheck(boolean status) {
		SharedPreferences.Editor editor = preferences.edit();
		editor.putBoolean(SHOW_FTZ_DIALOG, status);
		editor.commit();
	}

	public boolean getFTZDialogCheck() {
		return preferences.getBoolean(SHOW_FTZ_DIALOG, true);
	}

	/*
	 * Free Tram Zone - To Get Database from phone
	 */

	public void exportDB() {
		File sd = Environment.getExternalStorageDirectory();
		File data = Environment.getDataDirectory();
		FileChannel source = null;
		FileChannel destination = null;
		String currentDBPath = "/data/com.yarratrams.tramtracker/databases/"
				+ Constants.kDBName;
		String backupDBPath = "TTDB.sqlite";
		File currentDB = new File(data, currentDBPath);
		File backupDB = new File(sd, backupDBPath);
		try {
			source = new FileInputStream(currentDB).getChannel();
			destination = new FileOutputStream(backupDB).getChannel();
			destination.transferFrom(source, 0, source.size());
			source.close();
			destination.close();
			// Toast.makeText(this, "DB Exported!", Toast.LENGTH_LONG).show();
			System.out.println("-- no error  in export : ");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("-- error in export : " + e.getMessage());
		}
	}

	// public String getMACAddress(Context context)
	// {
	// WifiManager manager = (WifiManager)
	// context.getSystemService(Context.WIFI_SERVICE);
	// WifiInfo info = manager.getConnectionInfo();
	// String address = info.getMacAddress();
	// return address;
	// }

	// public String getUUID()
	// {
	// UUID uuid = UUID.randomUUID();
	// return uuid.toString().replace("-", "");
	// }
	//
	//
	public void setUniqueId() {
		UniqueIdPref uniqueIdPref = new UniqueIdPref(instance);
		if (uniqueIdPref.getUniqueId().equals("")) {
			uniqueIdPref.setUniqueId(uniqueIdPref.createUniqueID());
		}

		System.out.println("-- unique id set: " + uniqueIdPref.getUniqueId());
	}

	private String getStopDescription(Stop stop) {
		String text = "";

		System.out.println("stop is here: "
				+ (stop == null ? "null" : stop.getStopDescription()));

		text = text.concat(String.valueOf(stop.getStopNumber()));
		// phoenix changed 20.Jun.2014
		text = text.concat(": ");
		// text =
		// text.concat(context.getResources().getString(R.string.stop_name_colon));
		text = text.concat(stop.getStopName());

		return text;
	}

	private void goToPIDScreen(Stop stop) {
		Intent intent = new Intent(instance, PIDActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(PIDActivity.INTENT_KEY, stop);

		TramTrackerMainActivity.getAppManager().show(
				TramTrackerMainActivity.TAB_NEARBY,
				instance.getResources().getString(R.string.tag_pid_screen),
				intent);
	}

	private String getDescription(Tram service) {
		String description = "";

		
		System.out.println("-- instance: " + instance);
		
		description = service.getHeadboardNo();
		description = description.concat(instance
				.getString(R.string.route_filter_space));
		description = description.concat(service.getDestination());
		description = description.concat(instance
				.getString(R.string.route_filter_space));
		description = description.concat(instance
				.getString(R.string.accessibility_click_pid_onboard));

		return description;
	}

	public void goToOnBoardScreen(Tram service) {
		ActivityGroup group = TramTrackerMainActivity.getAppManager()
				.requestDifferentTab(TramTrackerMainActivity.TAB_MYTRAM);
		Intent intent = new Intent(group, OnBoardActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(OnBoardActivity.INTENT_KEY, service);
		Log.e("goToOnBoardScreen", "service = " + service);
		TramTrackerMainActivity.getAppManager().show(
				TramTrackerMainActivity.TAB_MYTRAM,
				instance.getResources().getString(R.string.tag_onboard_screen),
				intent);
	}

}
