package com.yarratrams.tramtracker.widget;

public class WidgetPredictionModel {





	String routeNo;
	String headboardRouteNo;
	int tramNo;
	String destination;
	boolean isLowFloor;
	String arrivalTime;
	boolean hasSpecialEvent;
	boolean hasDisruption;
	boolean hasAirCon;



	public String getRouteNo() {
		return routeNo;
	}
	public void setRouteNo(String routeNo) {
		this.routeNo = routeNo;
	}
	public String getHeadboardRouteNo() {
		return headboardRouteNo;
	}
	public void setHeadboardRouteNo(String headboardRouteNo) {
		this.headboardRouteNo = headboardRouteNo;
	}
	public int getTramNo() {
		return tramNo;
	}
	public void setTramNo(int tramNo) {
		this.tramNo = tramNo;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public boolean isLowFloor() {
		return isLowFloor;
	}
	public void setLowFloor(boolean isLowFloor) {
		this.isLowFloor = isLowFloor;
	}
	public String getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public boolean isHasSpecialEvent() {
		return hasSpecialEvent;
	}
	public void setHasSpecialEvent(boolean hasSpecialEvent) {
		this.hasSpecialEvent = hasSpecialEvent;
	}
	public boolean isHasDisruption() {
		return hasDisruption;
	}
	public void setHasDisruption(boolean hasDisruption) {
		this.hasDisruption = hasDisruption;
	}
	public boolean isHasAirCon() {
		return hasAirCon;
	}
	public void setHasAirCon(boolean hasAirCon) {
		this.hasAirCon = hasAirCon;
	}
	
	
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getDestination() + "--" + getArrivalTime();
	}
}
