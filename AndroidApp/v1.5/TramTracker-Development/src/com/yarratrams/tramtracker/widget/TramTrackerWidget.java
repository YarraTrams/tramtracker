package com.yarratrams.tramtracker.widget;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.TabHost;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.db.TTDB;
import com.yarratrams.tramtracker.objects.Favourite;
import com.yarratrams.tramtracker.objects.NearbyFavourite;
import com.yarratrams.tramtracker.objects.Stop;
import com.yarratrams.tramtracker.ui.PIDActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;
//Fused Location


public class TramTrackerWidget extends AppWidgetProvider implements  LocationListener,GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
	//Fused Location
	FusedLocationProviderApi fusedLocationProviderApi = LocationServices.FusedLocationApi;

	public int googlePlayServiceAvailablityCode;

	LocationRequest mLocationRequest;
	GoogleApiClient mGoogleApiClient;
	Location mCurrentLocation;
	ArrayList<NearbyFavourite> arrayList;

	private static final long INTERVAL = 1000 * 10;
	private static final long FASTEST_INTERVAL = 1000 * 5;
	WidgetStopPreference widgetStopPreference;

	final Handler handler = new Handler();

	//Constants
	private static final String 	REFRESH_WIDGET=  "Refresh";
	private static final String 	PID_SCREEN_ONE=  "PID_One";
	private static final String 	PID_SCREEN_TWO=  "PID_Two";
	private static final String 	PID_SCREEN_THREE=  "PID_Three";
	private static final String 	OPEN_APP=  "Open_App";
	public static  final int NO_FAVORITES = 1;
	public static  final int NO_FAVORITES_NEARBY = 2;
	public static  final int NO_PREDICTIONS = 3;
	public static  final int FAVORITE_PREDICTIONS = 4;
	public static  final int FUSED_LOCATION_LIB_NOT_AVAILABLE = 5;
	public final String  NO_FAV_STOPS = "You currently have no favourites saved.\n\n Tap here to open tramTRACKER to set up favourites";
	public final String  NO_GOOGLE_PLAY_SERVIES = "Google Play Services unavailable, please open tramTRACKER to install Google Play Services.";
	public final String  NO_FAV_STOPS_NEARBY = "You have no favourites within 15 minutes walking distance.\n\n Tap here to open tramTRACKER to view nearby stops";
//	public final String  NO_STOP_PREDICTIONS = "Service changes affect trams at this stop. Please consider alternative travel options";
	public final String  NO_STOP_PREDICTIONS = "There may be no trams on your route.";
	public final String  NO_STOP_PREDICTIONS_LOW = "There may be no low floor trams on your route.";
	public static final String FROM_WIDGET = "from_widget";
	public static final String FROM_WIDGET_TO_NEARBY = "from_widget_to_nearby";

	//Variables
	boolean hasLocation = false;
	Timer selfCloseGPSTimer;
	NearbyFavourite  nearbyFavourite;
	static Stop stop;
	WidgetTimeTask widgetTimeTask;
	static double currLat,currLong;
	TTDB ttdb;
	public static int Widget_Status;
	//	Thread currThread;
	TabHost tabHost;

	//UI
	//RemoteViews widgetRemoteViews;

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		super.onUpdate(context, appWidgetManager, appWidgetIds); 


		/*
		 * Adding update tram method  body here
		 */





		/*
		 * Body ends
		 */
		



		//value++;

		ComponentName thisAppWidgetComponentName;

		//	for (int appWidgetID : appWidgetIds) {
		RemoteViews remoteViews;

		thisAppWidgetComponentName = new ComponentName(
				context.getPackageName(), getClass().getName());
		remoteViews = new RemoteViews(context.getPackageName(),
				R.layout.widget_view);

		if (isGooglePlayServicesAvailable(context)) {

			//turned off for testing

			//setupLocation(context);

			System.out.println("-- I am here for getting stop info :)");

			ttdb = new TTDB(context);		
			
			if(currLat == 0 || currLong == 0)
			{
				widgetStopPreference = new WidgetStopPreference(context);
				currLat = widgetStopPreference.getLat();
				currLong =  widgetStopPreference.getLong();
			}
			
			
			Location myLocation = new Location("");
			myLocation.setLatitude(currLat);
			myLocation.setLongitude(currLong);

			System.out.println("--currLat: " + currLat);
			arrayList = ttdb.getNearbyFavouriteGivenLocation(myLocation);
			if (arrayList == null) {
				Widget_Status = NO_FAVORITES;

			} else {

				System.out.println("-- routes for stop: " + getRoutes());

				nearbyFavourite = arrayList.get(0);

				//System.out.println("-- nearby stop distance: " + nearbyFavourite.getDistance());

				if (nearbyFavourite.getDistance() > 2000) {
					Widget_Status = NO_FAVORITES_NEARBY;
				} else {
					stop = ttdb.getStop(nearbyFavourite.getFavourite()
							.getStop().getTrackerID());
					Widget_Status = FAVORITE_PREDICTIONS;
				}

			}

			Intent refreshIntent = new Intent(context, getClass());
			refreshIntent.setAction(REFRESH_WIDGET);
			PendingIntent refreshPendingIntent = PendingIntent.getBroadcast(
					context, 0, refreshIntent, 0);
			remoteViews.setOnClickPendingIntent(R.id.bRefreshWidget,
					refreshPendingIntent);
		}




		switch (Widget_Status) {
			case NO_FAVORITES :


				remoteViews.setViewVisibility(R.id.layMain, View.GONE);
				remoteViews.setViewVisibility(R.id.layFooter, View.GONE);
				remoteViews.setViewVisibility(R.id.layBlank, View.VISIBLE);

				remoteViews.setTextViewText(R.id.tvStopName, "No favourite stops");
				remoteViews.setTextViewText(R.id.tvStopDirection, "Open tramTRACKER to set up");
				remoteViews.setTextViewText(R.id.tvWidgetNote, NO_FAV_STOPS);
				remoteViews.setViewVisibility(R.id.ivFTZ, View.INVISIBLE);

				Intent intent = new Intent(context, getClass());
				intent.setAction(OPEN_APP);
				PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
				remoteViews.setOnClickPendingIntent(R.id.layBlank, pendingIntent);

				break;

			case NO_FAVORITES_NEARBY :



				remoteViews.setViewVisibility(R.id.layMain, View.GONE);
				remoteViews.setViewVisibility(R.id.layFooter, View.GONE);
				remoteViews.setViewVisibility(R.id.layBlank, View.VISIBLE);

				remoteViews.setTextViewText(R.id.tvStopName, "No favourite stops nearby");
				remoteViews.setTextViewText(R.id.tvStopDirection, "Open tramTRACKER to view nearby stops");
				remoteViews.setTextViewText(R.id.tvWidgetNote, NO_FAV_STOPS_NEARBY);
				remoteViews.setViewVisibility(R.id.ivFTZ, View.INVISIBLE);

				Intent intent2 = new Intent(context, getClass());
				intent2.setAction(OPEN_APP);
				PendingIntent pendingIntent2 = PendingIntent.getBroadcast(context, 0, intent2, 0);
				remoteViews.setOnClickPendingIntent(R.id.layBlank, pendingIntent2);




				break;




			case FAVORITE_PREDICTIONS:



				if(stop!=null)
				{
					remoteViews.setTextViewText(R.id.tvStopName, stop.getStopName());
					remoteViews.setTextViewText(R.id.tvStopDirection, stop.getCityDirection());
					widgetStopPreference = new  WidgetStopPreference(context);
					widgetStopPreference.saveStop(stop);
					if(stop.IsInFreeZone())
					{
						remoteViews.setViewVisibility(R.id.ivFTZ, View.VISIBLE);
					}
					else
					{
						remoteViews.setViewVisibility(R.id.ivFTZ, View.INVISIBLE);
					}
					
				
					
					remoteViews.setTextViewText(R.id.tvRoutes, displayRoutes(nearbyFavourite.getFavourite(),nearbyFavourite.getFavourite().getAlRoutes()));
				}
				else
				{
					//widgetStopPreference.saveStop(null);
				}




				widgetTimeTask = new WidgetTimeTask(context,remoteViews, thisAppWidgetComponentName, appWidgetManager, TramTrackerWidget.this,stop,nearbyFavourite);
				widgetTimeTask.execute();





				break;



			case FUSED_LOCATION_LIB_NOT_AVAILABLE:



				remoteViews.setViewVisibility(R.id.layMain, View.GONE);
				remoteViews.setViewVisibility(R.id.layFooter, View.GONE);
				remoteViews.setViewVisibility(R.id.layBlank, View.VISIBLE);

				remoteViews.setTextViewText(R.id.tvStopName, "Install Google Play Services");
				remoteViews.setTextViewText(R.id.tvStopDirection, "");
				remoteViews.setTextViewText(R.id.tvWidgetNote, NO_GOOGLE_PLAY_SERVIES);
				remoteViews.setViewVisibility(R.id.ivFTZ, View.INVISIBLE);

				remoteViews.setViewVisibility(	R.id.bRefreshWidget, View.INVISIBLE);

				Intent intent5 = new Intent(context, getClass());
				intent5.setAction(OPEN_APP);
				PendingIntent pendingIntent5 = PendingIntent.getBroadcast(context, 0, intent5, 0);
				remoteViews.setOnClickPendingIntent(R.id.layBlank, pendingIntent5);

				break;
			default :
				break;
		}
		appWidgetManager.updateAppWidget(thisAppWidgetComponentName, remoteViews);
	}



	public  void updateTimeTable(Context context,RemoteViews remoteViews,ComponentName thisAppWidgetComponentName,AppWidgetManager appWidgetManager, ArrayList<WidgetPredictionModel> arrlst)
	{
		//	System.out.println("-- data in arrlst to show: " + arrlst.size());
		if(arrlst == null || arrlst.size() == 0)
		{
			boolean isLowFloor = false;
			try{
				for (int i = 0; i < nearbyFavourite.getFavourite().getAlRoutes().size(); i ++){
					if(nearbyFavourite.getFavourite().getAlRoutes().get(i).toLowerCase().contains("low")){
						isLowFloor = true;
					}
				}
			} catch (Exception e){
				isLowFloor = false;
			}
			
			remoteViews.setViewVisibility(R.id.layMain, View.GONE);
			remoteViews.setViewVisibility(R.id.layFooter, View.GONE);
			remoteViews.setViewVisibility(R.id.layBlank, View.VISIBLE);

			if(isLowFloor){
				remoteViews.setTextViewText(R.id.tvWidgetNote, NO_STOP_PREDICTIONS_LOW);
			}else {
				remoteViews.setTextViewText(R.id.tvWidgetNote, NO_STOP_PREDICTIONS);
			}

			Intent intent2 = new Intent(context, getClass());
			intent2.setAction(OPEN_APP);
			PendingIntent pendingIntent2 = PendingIntent.getBroadcast(context, 0, intent2, 0);
			remoteViews.setOnClickPendingIntent(R.id.layBlank, pendingIntent2);

		}
		else
		{
			System.out.println("-- routes in stop: " + getRouteCount(arrlst));
			int noOfRoutes =  getRouteCount(arrlst);
			switch (noOfRoutes) {
				case 1 :
					populateFirstRow(context, remoteViews, arrlst);
					populateSecondRow(context, remoteViews, arrlst);
					populateThirdRow(context, remoteViews, arrlst);
					break;

				case 2:


					ArrayList<WidgetPredictionModel> tempArrayList = new ArrayList<WidgetPredictionModel>();
					ArrayList<String> alRoutes = new ArrayList<String>();

					for(int i = 0 ; i < arrlst.size(); i++)
					{
						if( alRoutes.contains(arrlst.get(i).getRouteNo()))
						{

						}
						else
						{
							alRoutes.add(arrlst.get(i).getRouteNo());
							tempArrayList.add(arrlst.get(i));
						}
					}




					populateFirstRow(context, remoteViews, tempArrayList);
					populateSecondRow(context, remoteViews, tempArrayList);


					remoteViews.setTextViewText(R.id.tvRouteThird, "");
					remoteViews.setTextViewText(R.id.tvDestThird, "");
					remoteViews.setTextViewText(R.id.tvMinThird, "");

					hideTramIcons(remoteViews, null, R.id.ivThirdHasAirCon,R.id.ivThirdHasDisruption,R.id.ivThirdHasSpecialEvent, R.id.ivThirdIsLowFloor);

					//						Intent pidThreeIntent = new Intent(context, getClass());
					//						pidThreeIntent.setAction(PID_SCREEN_THREE);
					//						PendingIntent pidThreePendingIntent = PendingIntent.getBroadcast(context, 0, pidThreeIntent, 0);


					remoteViews.setOnClickPendingIntent(R.id.layThird, null);




					break;

				default :
					populateFirstRow(context, remoteViews, arrlst);
					populateSecondRow(context, remoteViews, arrlst);
					populateThirdRow(context, remoteViews, arrlst);
					break;
			}


			remoteViews.setViewVisibility(R.id.layMain, View.VISIBLE);
			remoteViews.setViewVisibility(R.id.layFooter, View.VISIBLE);
			remoteViews.setViewVisibility(R.id.layBlank, View.GONE);




			if(nearbyFavourite.getFavourite().getAlRoutes().contains("All Routes"))
			{
				/*
				ArrayList<String> alShownRoutes = new  ArrayList<String>();
				int count = 0;
				alShownRoutes = new ArrayList<String>();

				for(int i = 0; i < arrlst.size(); i++ )
				{
					System.out.println("-- route saved: " + alShownRoutes.toString());
					if(alShownRoutes.contains(arrlst.get(i).routeNo))
					{


					}
					else
					{
						if(count==0)
						{
							populateFirstRow(context, remoteViews, arrlst);
							count++;
							alShownRoutes.add(arrlst.get(i).routeNo);
						}
						else if (count ==1)
						{
							populateSecondRow(context, remoteViews, arrlst);
							count++;
							alShownRoutes.add(arrlst.get(i).routeNo);
						}
						else if(count ==2)
						{
							populateThirdRow(context, remoteViews, arrlst);
							count++;
							alShownRoutes.add(arrlst.get(i).routeNo);
						}
					}
				}
				 */
			}
			else
			{

				//First Row of Widget

				//populateFirstRow(context, remoteViews, arrlst);



				//Second Row of Widget
				//				try {
				//					WidgetPredictionModel widgetPredictionModel = arrlst.get(1);
				//
				//					remoteViews.setTextViewText(R.id.tvRouteSecond, widgetPredictionModel.getRouteNo());
				//					remoteViews.setTextViewText(R.id.tvDestSecond, widgetPredictionModel.getDestination());
				//					remoteViews.setTextViewText(R.id.tvMinSecond, getDestinationTime(widgetPredictionModel.getArrivalTime()));
				//
				//					showIcons(remoteViews, widgetPredictionModel, R.id.ivSecondHasAirCon,R.id.ivSecondHasDisruption,R.id.ivSecondHasSpecialEvent, R.id.ivSecondIsLowFloor);
				//
				//					Intent pidTwoIntent = new Intent(context, getClass());
				//					pidTwoIntent.setAction(PID_SCREEN_TWO);
				//					PendingIntent pidTwoPendingIntent = PendingIntent.getBroadcast(context, 0, pidTwoIntent, 0);
				//
				//					remoteViews.setOnClickPendingIntent(R.id.laySecond, pidTwoPendingIntent);
				//
				//
				//				} catch (Exception e) {
				//					// TODO: handle exception
				//				}
				//populateSecondRow(context, remoteViews, arrlst);


				//Third Row of Widget

				//populateThirdRow(context, remoteViews, arrlst);
			}
		}
		appWidgetManager.updateAppWidget(thisAppWidgetComponentName, remoteViews);
	}

	public int getRouteCount(ArrayList<WidgetPredictionModel> arrlst)
	{
		int count = 0;

		ArrayList<String> alRouteNo = new ArrayList<String>();
		for(int i = 0 ; i < arrlst.size(); i++)
		{
			alRouteNo.add(arrlst.get(i).routeNo);
		}

		Set<String> hs = new HashSet<String>();
		hs.addAll(alRouteNo);

		count = hs.size();


		return count;
	}

	private void populateThirdRow(Context context, RemoteViews remoteViews,
			ArrayList<WidgetPredictionModel> arrlst) {
		try {
			WidgetPredictionModel widgetPredictionModel = arrlst.get(2);

			remoteViews.setTextViewText(R.id.tvRouteThird, widgetPredictionModel.getRouteNo());
			remoteViews.setTextViewText(R.id.tvDestThird, widgetPredictionModel.getDestination());
			remoteViews.setTextViewText(R.id.tvMinThird, getDestinationTime(widgetPredictionModel.getArrivalTime()));

			showIcons(remoteViews, widgetPredictionModel, R.id.ivThirdHasAirCon,R.id.ivThirdHasDisruption,R.id.ivThirdHasSpecialEvent, R.id.ivThirdIsLowFloor);

			Intent pidThreeIntent = new Intent(context, getClass());
			pidThreeIntent.setAction(PID_SCREEN_THREE);
			PendingIntent pidThreePendingIntent = PendingIntent.getBroadcast(context, 0, pidThreeIntent, 0);


			remoteViews.setOnClickPendingIntent(R.id.layThird, pidThreePendingIntent);


		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void populateSecondRow(Context context, RemoteViews remoteViews,
			ArrayList<WidgetPredictionModel> arrlst) {
		try {
			WidgetPredictionModel widgetPredictionModel = arrlst.get(1);

			remoteViews.setTextViewText(R.id.tvRouteSecond, widgetPredictionModel.getRouteNo());
			remoteViews.setTextViewText(R.id.tvDestSecond, widgetPredictionModel.getDestination());
			remoteViews.setTextViewText(R.id.tvMinSecond, getDestinationTime(widgetPredictionModel.getArrivalTime()));

			showIcons(remoteViews, widgetPredictionModel, R.id.ivSecondHasAirCon,R.id.ivSecondHasDisruption,R.id.ivSecondHasSpecialEvent, R.id.ivSecondIsLowFloor);

			Intent pidTwoIntent = new Intent(context, getClass());
			pidTwoIntent.setAction(PID_SCREEN_TWO);
			PendingIntent pidTwoPendingIntent = PendingIntent.getBroadcast(context, 0, pidTwoIntent, 0);

			remoteViews.setOnClickPendingIntent(R.id.laySecond, pidTwoPendingIntent);


		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void populateFirstRow(Context context, RemoteViews remoteViews,
			ArrayList<WidgetPredictionModel> arrlst) {
		//First Row of Widget

		try {
			WidgetPredictionModel widgetPredictionModel = arrlst.get(0);

			remoteViews.setTextViewText(R.id.tvRouteFirst, widgetPredictionModel.getRouteNo());
			remoteViews.setTextViewText(R.id.tvDestFirst, widgetPredictionModel.getDestination());

			remoteViews.setTextViewText(R.id.tvMinFirst, getDestinationTime(widgetPredictionModel.getArrivalTime()));



			showIcons(remoteViews, widgetPredictionModel, R.id.ivFirstHasAirCon,R.id.ivFirstHasDisruption,R.id.ivFirstHasSpecialEvent, R.id.ivFirstIsLowFloor);




			Intent pidOneIntent = new Intent(context, getClass());
			pidOneIntent.setAction(PID_SCREEN_ONE);
			PendingIntent pidOnePendingIntent = PendingIntent.getBroadcast(context, 0, pidOneIntent, 0);

			remoteViews.setOnClickPendingIntent(R.id.layFirst, pidOnePendingIntent);



		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void showIcons(RemoteViews remoteViews,
			WidgetPredictionModel widgetPredictionModel, int ivhasaircon,
			int ivhasdisruption, int ivhasspecialevent,
			int ivislowfloor) {
		// TODO Auto-generated method stub

		hideTramIcons(remoteViews, widgetPredictionModel, ivhasaircon, ivhasdisruption, ivhasspecialevent, ivislowfloor);

		if(widgetPredictionModel.isHasAirCon())
		{
			remoteViews.setViewVisibility(ivhasaircon, View.VISIBLE);
		}
		if(widgetPredictionModel.isHasDisruption())
		{
			remoteViews.setViewVisibility(ivhasdisruption, View.VISIBLE);
		}
		if(widgetPredictionModel.isHasSpecialEvent())
		{
			remoteViews.setViewVisibility(ivhasspecialevent, View.VISIBLE);
		}
		if(widgetPredictionModel.isLowFloor())
		{
			remoteViews.setViewVisibility(ivislowfloor, View.VISIBLE);
		}

	}

	public void hideTramIcons(RemoteViews remoteViews,
			WidgetPredictionModel widgetPredictionModel, int ivhasaircon,
			int ivhasdisruption, int ivhasspecialevent,
			int ivislowfloor)
	{

		remoteViews.setViewVisibility(ivhasaircon, View.GONE);

		remoteViews.setViewVisibility(ivhasdisruption, View.GONE);

		remoteViews.setViewVisibility(ivhasspecialevent, View.GONE);

		remoteViews.setViewVisibility(ivislowfloor, View.GONE);

	}



	private String getDestinationTime(String arrivalTime) {
		// TODO Auto-generated method stub

		//2015-07-01T14:24:36

		Calendar currCalendar , destCalendar;
		int mins= 0 ;



		String pattern = "yyyy-MM-dd'T'HH:mm:ss";
		Date date =null;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		try {
			date = simpleDateFormat.parse(arrivalTime);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			date = null;
		}

		//		Calendar currCalendar = Calendar.getInstance();
		//		currCalendar.setTime(date);


		if(date !=null)
		{
			currCalendar = Calendar.getInstance();
			destCalendar = Calendar.getInstance();


			//set date of destination calendar
			destCalendar.setTime(date);


			//get time difference
			long minsDiff;


			System.out.println("time diff: " + (destCalendar.getTimeInMillis() - currCalendar.getTimeInMillis() ));
			minsDiff =       destCalendar.getTimeInMillis() - currCalendar.getTimeInMillis();

			Date date2 = new Date(minsDiff);
			mins = date2.getMinutes();

			System.out.println("-- mins : " + mins);

			if(mins < 60)
			{

				//Calendar calendar = Calendar.getInstance();
				//int currMin =calendar.get(Calendar.MINUTE);
				//int distMin = date.getMinutes();
				//System.out.println("-- min: " + date.getMinutes() + " now min: " + calendar.get(Calendar.MINUTE) + "diff:" + String.valueOf(calendar.get(Calendar.MINUTE) - date.getMinutes()));


				if(mins == 0)
				{
					return "NOW";
				}
				else 
				{
					return String.valueOf(mins);
				}



			}
			else
			{
				return destCalendar.get(Calendar.DATE) + "/" + (destCalendar.get(Calendar.MONTH) + 1);
			}




		}
		else
		{
			return "--";
		}



	}

	public String displayRoutes(Favourite favourite, ArrayList<String> arrayList2)
	{
		boolean isLowFloor = false;
		boolean isAllRoute = false;
		String route = "";
		for (int i = 0; i < arrayList2.size(); i ++){
			if(arrayList2.get(i).toLowerCase().contains("all")){
				isAllRoute = true;
			}else if(arrayList2.get(i).toLowerCase().contains("low")){
				isLowFloor = true;
			}else{
				route = arrayList2.get(i);
			}
		}
	
			
			if(isAllRoute && !isLowFloor){
				return "Showing all routes";
			} else if (isAllRoute && isLowFloor){
				return "Showing low floor trams only.";
			}else if (!isAllRoute && isLowFloor){
				return "Showing low floor trams on route "+ route;
			}else {
				return "Showing trams on route " + route + ".";
			}

	}

	public void updateTramTime(Context context)
	{
		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);

		ComponentName thisAppWidgetComponentName = new ComponentName(context.getPackageName(),getClass().getName());

		int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidgetComponentName);

		setupLocation(context);
		onUpdate(context, appWidgetManager, appWidgetIds);

//		Toast.makeText(context, "clicked", 0).show();
	}	

	@Override
	public void onReceive(final Context context, Intent intent) {
		// TODO Auto-generated method stub
		super.onReceive(context, intent);

		widgetStopPreference = new  WidgetStopPreference(context);
		if (intent.getAction().equals(REFRESH_WIDGET)) {
			//do some really cool stuff here


			updateTramTime(context);

		}
		else if(intent.getAction().equals(PID_SCREEN_ONE) || intent.getAction().equals(PID_SCREEN_TWO) || intent.getAction().equals(PID_SCREEN_THREE) )
		{

			widgetStopPreference = new WidgetStopPreference(context);
			Stop stop = widgetStopPreference.getStop();
			goToPIDScreen(context,stop);
		}
		//		else if(intent.getAction().equals(PID_SCREEN_TWO))
		//		{
		//			Toast.makeText(context, "PID TWO", 0).show();
		//		}
		//		else if(intent.getAction().equals(PID_SCREEN_THREE))
		//		{
		//			Toast.makeText(context, "PID THREE", 0).show();
		//		}
		else if (intent.getAction().equals(OPEN_APP))
		{
			Intent intent2 = new Intent(context, TramTrackerMainActivity.class);
			intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
			intent2.putExtra(FROM_WIDGET_TO_NEARBY, true);
			context.startActivity(intent2);
		}
	}


	private void goToPIDScreen(Context context,Stop stop) {
		// TODO Auto-generated method stub
		System.out.println("is stop null " +  stop);
		Intent intent = new Intent(context, TramTrackerMainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		intent.putExtra(PIDActivity.INTENT_KEY, stop);
		intent.putExtra(FROM_WIDGET, true);
		context.startActivity(intent);
	}

	private String getStopDescription(Stop stop){
		String text = "";
		text = text.concat(String.valueOf(stop.getStopNumber()));
		//phoenix changed 20.Jun.2014
		text = text.concat(": ");
		//		text = text.concat(context.getResources().getString(R.string.stop_name_colon));
		text = text.concat(stop.getStopName());
		return text;
	}

	@Override
	public void onDisabled(Context context) {
	    Context appContext = context.getApplicationContext();
	    super.onDisabled(context);
	}
	
	@Override
	public void onEnabled(final Context context) {
		// TODO Auto-generated method stub
		super.onEnabled(context);
//		Toast.makeText(context, "Widget added", 0).show();
		if(isGooglePlayServicesAvailable(context))
		{
			updateTramTime(context);
		}
		else
		{
			Widget_Status = FUSED_LOCATION_LIB_NOT_AVAILABLE;
			System.out.println("-- lib not available");
		}
	}

	public void updateLocation(Context context)
	{

	}

	public void setupLocation(Context context)
	{
		System.out.println("-- setup loc");
		mLocationRequest = new LocationRequest();
		mLocationRequest.setInterval(INTERVAL);
		mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		mGoogleApiClient =new GoogleApiClient.Builder(context)
		.addApi(LocationServices.API)
		.addConnectionCallbacks(this)
		.addOnConnectionFailedListener(this)
		.build();
		hasLocation = false;
		mGoogleApiClient.connect();
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub

	}



	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
		com.google.android.gms.common.api.PendingResult<Status> pendingResult 
		= LocationServices
		.FusedLocationApi
		.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
	}



	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onLocationChanged(Location arg0) {
		// TODO Auto-generated method stub

		mCurrentLocation = arg0;
		System.out.println("-- current loc: " + mCurrentLocation.getLatitude());
		currLat = mCurrentLocation.getLatitude();
		currLong =  mCurrentLocation.getLongitude();
		//		if(currLat==0)
		//		{
		//			currLat =  widgetStopPreference.getLat();
		//			currLong = widgetStopPreference.getLong();
		//		}
		//		else
		//		{
		widgetStopPreference.setLat(currLat);
		widgetStopPreference.setLong(currLong);
		//		}
		System.out.println("-- current loc after: " + mCurrentLocation.getLatitude());
		hasLocation = true;
		if(null != selfCloseGPSTimer)
			return;
		selfCloseGPSTimer = new Timer();
		selfCloseGPSTimer.schedule(new TimerTask(){
			public void run()
			{
				mGoogleApiClient.disconnect();
				selfCloseGPSTimer = null;
			}
		}, 30000);

	}

	public boolean isGooglePlayServicesAvailable(Context context)
	{
		googlePlayServiceAvailablityCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);

		if(googlePlayServiceAvailablityCode == ConnectionResult.SUCCESS)
		{
			return true;
		}
		else
		{
			return false;
		}
	}



	public String getRoutes()
	{
		String r = "";
		System.out.println("-- size of favorites:  " + arrayList.size());

		for(int i = 0; i < arrayList.size(); i++)
		{
			r = r +"," +  arrayList.get(i).getFavourite().getAlRoutes().toString();
		}

		return r;
	}






	//	public void RefreshView(Context context)
	//	{
	//		Toast.makeText(context, "Refreshed", 0).show();
	//		MainActivity.Value = MainActivity.Value + 1;
	//		//Set remote view
	//		widgetRemoteViews =  new RemoteViews(context.getPackageName(), R.layout.widget_view);
	//		widgetRemoteViews.setTextViewText(R.id.textView55, "val: " + String.valueOf(MainActivity.Value));
	//
	//		pushWidgetUpdate(context, widgetRemoteViews);
	//		System.out.println("-- value: " + MainActivity.Value);
	//
	//
	//	}

	//	public void pushWidgetUpdate(Context context, RemoteViews remoteViews)
	//	{
	//		ComponentName myWidget = new ComponentName(context,TramTrackerWidget.class);
	//		AppWidgetManager manager = AppWidgetManager.getInstance(context);
	//		manager.updateAppWidget(myWidget, remoteViews);
	//	}
}
