package com.yarratrams.tramtracker.widget;

import java.io.IOException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.RemoteViews;

import com.google.gson.Gson;
import com.yarratrams.tramtracker.R;
import com.yarratrams.tramtracker.objects.Constants;
import com.yarratrams.tramtracker.objects.NearbyFavourite;
import com.yarratrams.tramtracker.objects.Stop;

public class WidgetTimeTask extends AsyncTask<String, Integer, Boolean> {

	String url = "";
	private DefaultHttpClient httpClient;
	private HttpGet httpGet;
	private HttpResponse httpResponse;
	String content;
	Calendar calendar;
	Date date;
	SimpleDateFormat simpleDateFormat;
	Context context;

	JSONObject jsonObject;
	private RemoteViews views;
	private ComponentName thisAppWidgetComponentName;
	private AppWidgetManager WidgetManager;
	TramTrackerWidget tramTrackerWidget;
	WidgetPredictionModel[] arrWidgetPredictionModels;
	ArrayList<WidgetPredictionModel> arrlstWidgetPredictionModels;
	private Stop stop;
	private NearbyFavourite nearbyFavourite;
	String routes, lowFloor;

	public WidgetTimeTask(Context context, RemoteViews views,
			ComponentName thisAppWidgetComponentName,
			AppWidgetManager appWidgetManager,
			TramTrackerWidget tramTrackerWidget, Stop stop,
			NearbyFavourite nearbyFavourite) {
		// TODO Auto-generated constructor stub
		this.stop = stop;
		this.nearbyFavourite = nearbyFavourite;
		this.tramTrackerWidget = tramTrackerWidget;
		this.context = context;
		this.views = views;
		this.thisAppWidgetComponentName = thisAppWidgetComponentName;
		this.WidgetManager = appWidgetManager;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		views.setViewVisibility(R.id.bRefreshWidget, View.GONE);
		views.setViewVisibility(R.id.pbLoading, View.VISIBLE);
		WidgetManager.updateAppWidget(thisAppWidgetComponentName, views);

	}

	@Override
	protected Boolean doInBackground(String... params) {
		// TODO Auto-generated method stub

		try {

			httpClient = new DefaultHttpClient();

			System.out.println("-- routes: "
					+ nearbyFavourite.getFavourite().getAlRoutes()
					+ " low flor: "
					+ nearbyFavourite.getFavourite().getStop()
							.isEasyAccessStop());

			ArrayList<String> arrDetails = nearbyFavourite.getFavourite()
					.getAlRoutes();

			try {

				routes = arrDetails.get(0);

			} catch (Exception e) {
				// TODO: handle exception
				routes = "";
			}

			try {
				lowFloor = arrDetails.get(1);
				lowFloor = ((arrDetails.get(1).equalsIgnoreCase("Low Floor")))
						? "true"
						: "false";
			} catch (Exception e) {
				// TODO: handle exception
				lowFloor = "false";
			}

			System.out.println("-- routes after filter: " + routes
					+ " low Floor: " + lowFloor);

			url = Constants.strUpdateURLBase
					+ "predictions/widget/"
					+ nearbyFavourite.getFavourite().getStop().getTrackerID()
					+ "?authid=4997245C-6D85-11E1-8E28-84224924019B&token=3e982c6b-d57a-43fe-bff7-a44e9adb9e1e&lowFloorOnly="
					+ lowFloor + "&routeNo=" + URLEncoder.encode(routes);
			System.out.println("-- widget url: " + url);

			httpGet = new HttpGet(url);

			httpResponse = httpClient.execute(httpGet);

			content = EntityUtils.toString(httpResponse.getEntity());

			Gson gson = new Gson();

			arrWidgetPredictionModels = gson.fromJson(content,
					WidgetPredictionModel[].class);

			System.out.println("-- data : "
					+ Arrays.deepToString(arrWidgetPredictionModels));

			return true;

		}

		catch (IOException e) {
			System.out.println("-- 2 error: " + e.getMessage());
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("-- 3 error: " + e.getMessage());
			e.printStackTrace();
		}

		return false;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if (result) {
			try {

				// set last update time
				views.setTextViewText(R.id.tvLastUpdateTime, "Last Update: "
						+ getLastUdpateDate());

				arrlstWidgetPredictionModels = new ArrayList<WidgetPredictionModel>(
						Arrays.asList(arrWidgetPredictionModels));
				tramTrackerWidget.updateTimeTable(context, views,
						thisAppWidgetComponentName, WidgetManager,
						arrlstWidgetPredictionModels);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				// views.setTextViewText(R.id.tvDestFirst, "error");
				e.printStackTrace();
			}

			System.out.println("-- all good 1");
			// WidgetManager.updateAppWidget(thisAppWidgetComponentName, views);
		} else {

			views.setTextViewText(R.id.tvLastUpdateTime, "Last Update: "
					+ "  ---  ");
			tramTrackerWidget.updateTimeTable(context, views,
					thisAppWidgetComponentName, WidgetManager, null);

		}
		views.setViewVisibility(R.id.bRefreshWidget, View.VISIBLE);
		views.setViewVisibility(R.id.pbLoading, View.GONE);
		WidgetManager.updateAppWidget(thisAppWidgetComponentName, views);
	}

	public String getLastUdpateDate() {
		calendar = Calendar.getInstance();
		date = new Date(calendar.getTimeInMillis());
		simpleDateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm a");
		return simpleDateFormat.format(date);
	}

	public void pushWidgetUpdate(Context context, RemoteViews remoteViews) {
		ComponentName myWidget = new ComponentName(context,
				TramTrackerWidget.class);
		AppWidgetManager manager = AppWidgetManager.getInstance(context);
		manager.updateAppWidget(myWidget, remoteViews);
	}
}
