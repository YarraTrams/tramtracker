package com.yarratrams.tramtracker.widget;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.yarratrams.tramtracker.objects.Stop;

public class WidgetStopPreference {



	Context context;
	SharedPreferences sharedPreferences;
	SharedPreferences.Editor editor;

	final String PREF_NAME = "widgetstoppref";
	final String STOP =	"stop";
	final String LAT = "lat";
	final String LONG = "long";


	public WidgetStopPreference(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
		sharedPreferences = context.getSharedPreferences(PREF_NAME, Activity.MODE_PRIVATE);
		editor = sharedPreferences.edit();
	}





	public void setLat(double lat)
	{
		editor.putString( LAT, String.valueOf(lat));
		editor.commit();
	}
	
	public double getLat()
	{
		return Double.parseDouble(sharedPreferences.getString(LAT, "0"));
	}


	public void setLong(double lon)
	{
		editor.putString( LONG, String.valueOf(lon));
		editor.commit();
	}
	
	public double getLong()
	{
		return Double.parseDouble(sharedPreferences.getString(LONG, "0"));
	}
	

	public void saveStop(Stop stop)	
	{
		Gson gson = new Gson();
		String json = gson.toJson(stop);
		editor.putString(STOP, json);
		editor.commit();

	}



	public Stop getStop()
	{
		Gson gson = new Gson();
		String json = sharedPreferences.getString(STOP, "");
		if(json.equals(""))
		{
			return null;
		}
		else
		{
			return gson.fromJson(json, Stop.class);
		}
	}
}
