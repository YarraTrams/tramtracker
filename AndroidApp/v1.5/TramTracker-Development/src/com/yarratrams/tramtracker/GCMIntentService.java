package com.yarratrams.tramtracker;

import java.util.Random;
import java.util.concurrent.Executors;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.v4.app.NotificationCompat;

import com.google.android.gcm.GCMBaseIntentService;
import com.yarratrams.tramtracker.objects.Constants;
import com.yarratrams.tramtracker.tasks.PushNotificationTask;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;

public class GCMIntentService extends GCMBaseIntentService {

	PushNotificationTask pushNotificationTask;
	int notificationIndex = 0;

	public static final String FROM_NOTIFICATION = "from_notification";

	public GCMIntentService() {
		// TODO Auto-generated constructor stub
		super(Constants.SENDER_ID);
	}

	@Override
	protected void onError(Context arg0, String errorId) {
		// TODO Auto-generated method stub

		System.out.println("-- error gcm : " + errorId);

	}

	@Override
	protected void onMessage(Context arg0, Intent data) {
		// TODO Auto-generated method stub

		System.out.println("-- I got a push notifications: "
				+ data.getExtras().toString());
		//notificationIndex++;


		if(data.getExtras().getString("message") == null)
		{

		}
		else
		{
			generateNotification(data);
		}

	}

	@Override
	protected void onRegistered(Context context, String registrationId) {
		// TODO Auto-generated method stub

		PushNotificationTask pushNotificationTask;
		pushNotificationTask = new PushNotificationTask(context, true);
		pushNotificationTask.execute();

	}

	@Override
	protected void onUnregistered(Context context, String registrationId) {
		// TODO Auto-generated method stub

		System.out.println("-- unregistration id: " + registrationId);
		PushNotificationTask pushNotificationTask;
		pushNotificationTask = new PushNotificationTask(context, false);
		pushNotificationTask.execute();

	}

	public void generateNotification(Intent data) {

		// PendingIntent resultPendingIntent = null;
		// Intent resultIntent;
		String title = "tramTRACKER", content = "", ticker = "tramTRACKER", routeNo = "";
		int messageType = 0;
		Uri sound = Uri.parse("android.resource://" + getPackageName() + "/"
				+ R.raw.a);

		WakeLock screenLock = ((PowerManager) getSystemService(POWER_SERVICE))
				.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK
						| PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");
		screenLock.acquire();

		// later
		screenLock.release();

		try {

			content = data.getExtras().getString("message");

		} catch (Exception e) {
			// TODO: handle exception
			content = "No msg/content";
		}

		try {
			routeNo = data.getExtras().getString("routeNo");
		} catch (Exception e) {
			// TODO: handle exception
			routeNo = "no route found";
		}
		
		try {
			String messageTypeStr = data.getExtras().getString("messageType");
			if(messageTypeStr.equals("2")){
				messageType = 2;
			}else {
				messageType = 1;
			}
		} catch (Exception e) {
			// TODO: handle exception
			messageType = 0;
		}
		
		System.out.println("notification type: " + messageType);
		
		NotificationCompat.Builder builder = new NotificationCompat.Builder(
				this);
		builder.setContentTitle(title);
		builder.setContentText(routeNo + "-" + content);
//		builder.setSmallIcon(R.drawable.icn_app);
		builder.setSmallIcon(R.drawable.notcenter);
		builder.setLights(Color.GREEN, 1000, 1000);
		builder.setTicker(ticker);
		builder.setPriority(NotificationCompat.PRIORITY_HIGH);
		builder.setAutoCancel(true);

		sound = Uri.parse("android.resource://" + getPackageName() + "/"
				+ R.raw.w);

		// count++;

		builder.setSound(sound);

		Random random = new Random();
		notificationIndex = random.nextInt(999999);
		System.out.println("-- pN count: " + notificationIndex);

		// Intent intent = new Intent(context, TramTrackerMainActivity.class);
		//
		// intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		// intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		// intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		// intent.putExtra(PIDActivity.INTENT_KEY, stop);
		// intent.putExtra(FROM_WIDGET, true);

		Intent resultIntent = new Intent(this, TramTrackerMainActivity.class);

		resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		if(messageType == 2){
			resultIntent.putExtra(FROM_NOTIFICATION, FROM_NOTIFICATION);
		}
		PendingIntent resultPendingIntent = PendingIntent.getActivity(this, notificationIndex,
				resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		builder.setContentIntent(resultPendingIntent);

		NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		mNotificationManager.cancel(notificationIndex);

		// notificationID allows you to update the notification later on.
		mNotificationManager.notify(notificationIndex, builder.build());
	}

}
