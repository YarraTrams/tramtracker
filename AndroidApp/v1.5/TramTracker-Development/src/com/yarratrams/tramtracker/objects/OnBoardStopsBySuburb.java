package com.yarratrams.tramtracker.objects;

import java.util.ArrayList;

public class OnBoardStopsBySuburb {
	String suburb;
	int ftzStopCount  =0 ;
	ArrayList<PredictedArrivalTime> stops;
	
	public OnBoardStopsBySuburb() {
		this.suburb = "";
		this.stops = new ArrayList<PredictedArrivalTime>();
	}
	
	public String getSuburb() {
		return suburb;
	}
	
	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}
	
	public ArrayList<PredictedArrivalTime> getStops() {
		return stops;
	}
	
	public void setStops(ArrayList<PredictedArrivalTime> stops) {
		this.stops = stops;
	}
	
	public void addStop(PredictedArrivalTime stop){
		stops.add(stop);
	}
	
	public String toString() {
		return "Suburb = " + suburb + ", Stop = " + stops;
	}
	
	/*
	 * Free Tram Zone
	 */
	
	public void setftzStopCount(int count)
	{
		this.ftzStopCount  = count;
	}
	
	public int getftzStopCount()
	{
		return ftzStopCount;
	}
	
}
