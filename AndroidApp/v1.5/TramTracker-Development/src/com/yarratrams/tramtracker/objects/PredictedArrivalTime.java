package com.yarratrams.tramtracker.objects;

import java.util.Date;

import android.os.Parcel;
import android.os.Parcelable;


public class PredictedArrivalTime implements Parcelable{
	Date arrivalTime;
	Stop stop;
	
	public PredictedArrivalTime(Date arrivalTime, Stop stop){
		this.arrivalTime = arrivalTime;
		this.stop = stop;
	}
	
	private PredictedArrivalTime(Parcel in) {
		readFromParcel(in);
	}
	
	public Date getArrivalTime() {
		
		/*
		 * Adil Added
		 */
		//System.out.println("-- arrival time: " + arrivalTime);
		return arrivalTime;
	}
	
	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	
	public Stop getStop() {
		return stop;
	}
	
	public void setStop(Stop stop) {
		this.stop = stop;
	}
	
	@Override
	public String toString() {
		return "Date = " + arrivalTime + ", stop =  " + stop;
	}
	
	
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeParcelable(stop, 0);
		out.writeLong(arrivalTime == null?0:arrivalTime.getTime());
	}
	
	public void readFromParcel(Parcel in) {
		stop = (Stop)in.readParcelable(Stop.class.getClassLoader());
		arrivalTime = new Date(in.readLong());
	}
	
	public static final Parcelable.Creator<PredictedArrivalTime> CREATOR = new Parcelable.Creator<PredictedArrivalTime>() {
		public PredictedArrivalTime createFromParcel(Parcel in) {
			return new PredictedArrivalTime(in);
		}

		public PredictedArrivalTime[] newArray(int size) {
			return new PredictedArrivalTime[size];
		}
		
	};
}
