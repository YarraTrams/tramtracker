package com.yarratrams.tramtracker.objects;

import java.util.ArrayList;


public class NearbyStopsLimitedByNoOfStopsGivenLocation {
	public ArrayList<NearbyStop> nearbyStops;
	public ArrayList<NearbyStop> easyAccessStops;
	public ArrayList<NearbyStop> shelterStops;
	public ArrayList<NearbyStop> getNearbyStops() {
		return nearbyStops;
	}
	public void setNearbyStops(ArrayList<NearbyStop> nearbyStops) {
		this.nearbyStops = nearbyStops;
	}
	public ArrayList<NearbyStop> getEasyAccessStops() {
		return easyAccessStops;
	}
	public void setEasyAccessStops(ArrayList<NearbyStop> easyAccessStops) {
		this.easyAccessStops = easyAccessStops;
	}
	public ArrayList<NearbyStop> getShelterStops() {
		return shelterStops;
	}
	public void setShelterStops(ArrayList<NearbyStop> shelterStops) {
		this.shelterStops = shelterStops;
	}
	
	
}
