package com.yarratrams.tramtracker.objects;

import java.util.ArrayList;

public class TicketOutletBySuburb {
	String suburb;
	ArrayList<TicketOutlet> alTicketOutlets;
	
	public TicketOutletBySuburb(String suburb, ArrayList<TicketOutlet> alTicketOutlets) {
		if(suburb != null){
			this.suburb = suburb;
		} else {
			this.suburb = "";
		}
		if(alTicketOutlets != null){
			this.alTicketOutlets = alTicketOutlets;
		} else {
			this.alTicketOutlets = new ArrayList<TicketOutlet>();
		}
	}
	
	public String getSuburb() {
		return suburb;
	}
	
	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}
	
	public ArrayList<TicketOutlet> getTicketOutlets() {
		return alTicketOutlets;
	}
	
	public void setTicketOutlets(ArrayList<TicketOutlet> alTicketOutlets) {
		this.alTicketOutlets = alTicketOutlets;
	}
	
	public String toString() {
		return "Suburb = " + suburb + ", Ticket Outlet = " + alTicketOutlets;
	}
}
