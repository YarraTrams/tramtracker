package com.yarratrams.tramtracker.objects;

import android.os.Parcel;
import android.os.Parcelable;

public class TicketOutlet implements Parcelable{
	private long outletId;
	private String name;
	private String address;
	private String suburb;
//	private int postCode;
	private boolean is24Hours;
	private boolean hasMykiCard;
	private boolean hasMykiTopUp;
	private boolean hasMetcard;
	
	private int latitudeE6;
	private int longitudeE6;
	private int distance;
	
	public TicketOutlet(){
		
	}
	
	private TicketOutlet(Parcel in) {
		readFromParcel(in);
	}
	
	public long getOutletId() {
		return this.outletId;
	}

	public void setOutletId(long outletId) {
		this.outletId = outletId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSuburb() {
		return this.suburb;
	}

	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}

//	public int getPostCode() {
//		return this.postCode;
//	}
//
//	public void setPostCode(int postCode) {
//		this.postCode = postCode;
//	}

	public boolean isIs24Hours() {
		return this.is24Hours;
	}

	public void setIs24Hours(boolean is24Hours) {
		this.is24Hours = is24Hours;
	}

	public boolean isHasMykiCard() {
		return this.hasMykiCard;
	}

	public void setHasMykiCard(boolean hasMykiCard) {
		this.hasMykiCard = hasMykiCard;
	}

	public boolean isHasMykiTopUp() {
		return this.hasMykiTopUp;
	}

	public void setHasMykiTopUp(boolean hasMykiTopUp) {
		this.hasMykiTopUp = hasMykiTopUp;
	}

	public boolean isHasMetcard() {
		return this.hasMetcard;
	}

	public void setHasMetcard(boolean hasMetcard) {
		this.hasMetcard = hasMetcard;
	}
	
	public int getLatitudeE6() {
		return this.latitudeE6;
	}

	public void setLatitudeE6(int latitudeE6) {
		this.latitudeE6 = latitudeE6;
	}

	public int getLongitudeE6() {
		return this.longitudeE6;
	}

	public void setLongitudeE6(int longitudeE6) {
		this.longitudeE6 = longitudeE6;
	}

	public int getDistance() {
		return this.distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeLong(outletId);
		out.writeString(name);
		out.writeString(address);
		out.writeString(suburb);
//		out.writeInt(postCode);
		out.writeByte((byte)(is24Hours == true?1:0));
		out.writeByte((byte)(hasMykiCard == true?1:0));
		out.writeByte((byte)(hasMykiTopUp == true?1:0));
		out.writeByte((byte)(hasMetcard == true?1:0));
		out.writeInt(latitudeE6);
		out.writeInt(longitudeE6);
		out.writeInt(distance);
	}
	
	public void readFromParcel(Parcel in) {
		outletId = in.readLong();
		name = in.readString();
		address = in.readString();
		suburb = in.readString();
//		postCode = in.readInt();
		is24Hours = in.readByte()==1?true:false;
		hasMykiCard = in.readByte()==1?true:false;
		hasMykiTopUp = in.readByte()==1?true:false;
		hasMetcard = in.readByte()==1?true:false;
		latitudeE6 = in.readInt();
		longitudeE6 = in.readInt();
		distance = in.readInt();
	}
	
	public static final Parcelable.Creator<TicketOutlet> CREATOR = new Parcelable.Creator<TicketOutlet>() {
		public TicketOutlet createFromParcel(Parcel in) {
			return new TicketOutlet(in);
		}

		public TicketOutlet[] newArray(int size) {
			return new TicketOutlet[size];
		}
	};
	
	public String toString() {
		return "name = " + name + ", latitude = " + latitudeE6/1E6 + ", longitude = " + longitudeE6/1E6; 
	};
	
}
