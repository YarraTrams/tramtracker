package com.yarratrams.tramtracker.objects;

import java.util.ArrayList;
import java.util.Date;

import android.os.Parcel;
import android.os.Parcelable;

public class DestinationGroup implements Parcelable{
	private ArrayList<Route> routes;
	private boolean isGroup;
	private boolean isGroupByDestination;
	private Date tramEarliestTime;
	

	public Date getTramEarliestTime() {
		return tramEarliestTime;
	}

	public void setTramEarliestTime(Date tramEarliestTime) {
		this.tramEarliestTime = tramEarliestTime;
	}

	private ArrayList<int[]> orderIndex;	// Ordered list of references of type [route index, tram index]


	public DestinationGroup() {
		isGroup = false;
		isGroupByDestination = false;
		routes = new ArrayList<Route>();
		orderIndex = new ArrayList<int[]>();
	}

	private DestinationGroup(Parcel in) {
		this();
		readFromParcel(in);
	}

	public ArrayList<Route> getRoutes() {
		return routes;
	}

	public void setRoutes(ArrayList<Route> routes) {
		this.routes = routes;
	}

	public boolean isGroup() {
		return isGroup;
	}

	public boolean isGroupByDestination() {
		return isGroupByDestination;
	}

	public void setGroupByDestination(boolean isGroupByDestination) {
		this.isGroupByDestination = isGroupByDestination;
	}

	public void setGroup(boolean isGroup) {
		this.isGroup = isGroup;
	}

	public int size(){
		return orderIndex.size();
	}

	public String getDestination(){
		if(size() > 0){
			Route route = routes.get(0); 
			ArrayList<Tram> services = route.getNextServices();
			if(services != null && !services.isEmpty()){
				Tram tram = services.get(0);
				return tram.getDestination();
			}
		}
		return "";
	}
	
	public String getHeadboardRoute(){
		if(size() > 0){
			Route route = routes.get(0); 
			ArrayList<Tram> services = route.getNextServices();
			if(services != null && !services.isEmpty()){
				Tram tram = services.get(0);
				return tram.getHeadboardNo();
			}
		}
		return "";
	}

	// Change for version 1.01
//	public boolean belongsToGroup(Route route){
//		for(Route inGroup : routes){
//			if(inGroup.getDestination().equalsIgnoreCase(route.getDestination())){
//				isGroupByDestination = true;
//				return true; 
//			} else if (inGroup.getMainRouteNo() != null && route.getMainRouteNo() != null){
//				if(inGroup.getMainRouteNo().equalsIgnoreCase(route.getMainRouteNo())){
//					isGroupByDestination = false;
//					return true;
//				}
//			}
//		}
//		return false;
//	}
	
	public void checkForGroupType(){
		String routeNumber = "";
		String destination = "";
		if(routes != null && !routes.isEmpty()){
			Route route = routes.get(0);
			if(route.getNextServices() != null && !route.getNextServices().isEmpty()){
				Tram first = route.getNextServices().get(0);
				routeNumber = first.getHeadboardNo();
				destination = first.getDestination();
				for(Tram tram : route.getNextServices()){
					if(!tram.getHeadboardNo().equals(routeNumber)){
						isGroup = true;
						isGroupByDestination = true;
						return;
					} else if(!tram.getDestination().equals(destination)){
						isGroup = true;
						return;
					}
				}
			}
		}
	}

	public void addToGroup(Route route){
		routes.add(route);
	}

	public void generateServicesOrderedList(){
		for(int i = 0; i < routes.size(); i++){
			Route route = routes.get(i);
			for(int j = 0; j < route.getNextServices().size(); j++){
				Tram tram = route.getNextServices().get(j);
				int[] reference = {i,j};
				insertReference(tram, reference, 0);
			}
		}
	}

	public void insertReference(Tram tram, int[] reference, int pointer){
		if(orderIndex.isEmpty()){
			orderIndex.add(reference);
		} else {
			if(pointer == orderIndex.size()){
				orderIndex.add(reference);
			} else {
				Tram tram2 = getNextServiceTram(pointer);
				if(tram.getArrivalTime().before(tram2.getArrivalTime())){
					orderIndex.add(pointer, reference);
				} else {
					pointer++;
					insertReference(tram, reference, pointer);
				}
			}
		}
	}

	public Route getNextServiceRoute(int index){
		int[] reference = orderIndex.get(index);
		return routes.get(reference[0]);
	}
	public Tram getNextServiceTram(int index){
		int[] reference = orderIndex.get(index);
		Route route = routes.get(reference[0]);
		return route.getNextServices().get(reference[1]);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeByte((byte)(isGroup == true?1:0));
		out.writeByte((byte)(isGroupByDestination == true?1:0));
		out.writeTypedList(routes);
		out.writeSerializable(tramEarliestTime);
	}

	public void readFromParcel(Parcel in) {
		isGroup = in.readByte()==1?true:false;
		isGroupByDestination = in.readByte()==1?true:false;
		in.readTypedList(routes, Route.CREATOR);
		tramEarliestTime = (Date) in.readSerializable();
	}

	public static final Parcelable.Creator<DestinationGroup> CREATOR = new Parcelable.Creator<DestinationGroup>() {
		public DestinationGroup createFromParcel(Parcel in) {
			return new DestinationGroup(in);
		}

		public DestinationGroup[] newArray(int size) {
			return new DestinationGroup[size];
		}

	};

}