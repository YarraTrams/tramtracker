package com.yarratrams.tramtracker.objects;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import org.joda.time.DateTime;

public class Reminders {


	long reminderTime;
	long setTime;

	UUID reminderID;

	String route;
	String trackerID;
	String direction;
	String stopName;
	int tramClass = 0;

	Stop stop;
	Tram tram;

	boolean isFreeTramZone = false;


	public Reminders() {
		// TODO Auto-generated constructor stub

		reminderID = UUID.randomUUID();
	}

	public Tram getTram() {
		return tram;
	}

	public void setTram(Tram tram) {
		this.tram = tram;
	}

	public Stop getStop() {
		return stop;
	}

	public void setStop(Stop stop) {
		this.stop = stop;
	}

	boolean isActive;

	int REMINDER_TYPE;


	public final static int STOP_ALARM = 0000;
	public final static int TRAM_ALARM = 1111;
	public final static int SCHEDULE_DEP_ALARM = 2222;


	// 2 min delay from the time, user sets stop alarm from my tram screen
	private final static int STOP_ALARM_DURATION = -2;



	public boolean isFreeTramZone() {
		return isFreeTramZone;
	}

	public void setFreeTramZone(boolean isFreeTramZone) {
		this.isFreeTramZone = isFreeTramZone;
	}

	public UUID getReminderID()
	{
		return reminderID;
	}

	public int getTramClass() {
		return tramClass;
	}

	public void setTramClass(int tramClass) {
		this.tramClass = tramClass;
	}

	public String getStopName() {
		return stopName;
	}
	public void setStopName(String stopName) {
		this.stopName = stopName;
	}

	public long getReminderTime() {
		return reminderTime;
	}
	public void setReminderTime(long reminderTime) {
		this.reminderTime = reminderTime;
	}
	public long getSetTime() {
		return setTime;
	}
	public void setSetTime(long setTime) {
		this.setTime = setTime;
	}
	public String getRoute() {
		return route;
	}
	public void setRoute(String route) {
		this.route = route;
	}
	public String getTrackerID() {
		return trackerID;
	}
	public void setTrackerID(String trackerID) {
		this.trackerID = trackerID;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	public int getREMINDER_TYPE() {
		return REMINDER_TYPE;
	}
	public void setREMINDER_TYPE(int rEMINDER_TYPE) {
		REMINDER_TYPE = rEMINDER_TYPE;
	}



	public static long getStopAlarmReminderTime(Date date)
	{
		long reminderTime = 0;

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MINUTE, STOP_ALARM_DURATION );

		reminderTime = calendar.getTimeInMillis();


		return reminderTime;
	}


	public static long 	getScheduleAlarmReminderTime(Date date, long interval)
	{
		long reminderTime = 0;

		reminderTime = date.getTime() - (interval*60*1000);

		System.out.println("-- reminder time:" + reminderTime);
		return reminderTime;


	}

	//new method for testing 10 mins millis

//	public static long getStopAlarmReminderTime(long millis, int mins)
//	{
//		long reminderTime= 0;
//
//		//System.out.println("-- date mins: " + date.getMinutes() + "- " + mins   +  "diff:" + (date.getMinutes() - mins));
//
//
//		reminderTime = millis + (mins * 60 * 1000);
//		System.out.println("-- reminder time:" + reminderTime);
//
//
//		return reminderTime;
//	}

	public static long getStopAlarmReminderTime(Date date, int mins)
	{
		
		
		/*
		 * 
		 */
		
		DateTime dateTime = new DateTime(date.getTime());
		//dateTime.min
		
		/*
		 * 
		 */
		long reminderTime= 0;

		System.out.println("-- date mins: " + date.getMinutes() + "- " + mins   +  "diff:" + (date.getMinutes() - mins));


		reminderTime = date.getTime() - (mins * 60 * 1000);


		return reminderTime;
	}


	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return stopName + " - " + " type: " + REMINDER_TYPE + " reminding time: " + reminderTime + "settime: " + setTime + " isActive: " + isActive;
	}

}
