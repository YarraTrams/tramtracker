package com.yarratrams.tramtracker.objects;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

public class FavouritesGroup implements Parcelable{
	private String name;
	private int order;
	private ArrayList<Favourite> favourites;
	
	
	public FavouritesGroup() {
		favourites = new ArrayList<Favourite>();
	}
	
	public FavouritesGroup(FavouritesGroup favGroup) {
		name = favGroup.getName();
		order = favGroup.getOrder();
		favourites = favGroup.getFavourites();
	}
	
	private FavouritesGroup(Parcel in) {
		this();
		readFromParcel(in);
	}
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Favourite> getFavourites() {
		return favourites;
	}

	public void setFavourites(ArrayList<Favourite> favourites) {
		this.favourites = favourites;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}
	
	public void addFavourite(Favourite favourite){
		this.favourites.add(favourite);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(name);
		out.writeInt(order);
		out.writeTypedList(favourites);
	}
	
	public void readFromParcel(Parcel in) {
		name = in.readString();
		order = in.readInt();
		in.readTypedList(favourites, Favourite.CREATOR);
	}
	
	public static final Parcelable.Creator<FavouritesGroup> CREATOR = new Parcelable.Creator<FavouritesGroup>() {
		public FavouritesGroup createFromParcel(Parcel in) {
			return new FavouritesGroup(in);
		}

		public FavouritesGroup[] newArray(int size) {
			return new FavouritesGroup[size];
		}
		
	};
	
	public String toString() {
		return "name = " + name + " order = " + order + " favourites = " + favourites;       
	};
	
}
