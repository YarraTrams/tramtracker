package com.yarratrams.tramtracker.objects;

import android.os.Parcel;
import android.os.Parcelable;

public class ServiceDisruption implements Parcelable{
	private String routeNumber;
	private String disruptionDescription;
	private boolean isText;
	private boolean hasAdditionalInfoOnWebsite;
	private String messageId;
	
	public ServiceDisruption(){
	}
	
	private ServiceDisruption(Parcel in) {
		readFromParcel(in);
	}

	
	public boolean isText() {
		return isText;
	}

	public void setText(boolean isText) {
		this.isText = isText;
	}
	
	public boolean isHasAdditionalInfoOnWebsite() {
		return hasAdditionalInfoOnWebsite;
	}

	public void setHasAdditionalInfoOnWebsite(boolean hasAdditionalInfoOnWebsite) {
		this.hasAdditionalInfoOnWebsite = hasAdditionalInfoOnWebsite;
	}

	public String getRouteNumber() {
		return this.routeNumber;
	}
	public void setRouteNumber(String routeNumber) {
		this.routeNumber = routeNumber;
	}

	public String getDisruptionDescription() {
		return this.disruptionDescription;
	}
	public void setDisruptionDescription(String disruptionDescription) {
		this.disruptionDescription = disruptionDescription;
	}
	
	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(routeNumber);
		out.writeString(disruptionDescription);
	}
	
	public void readFromParcel(Parcel in) {
		routeNumber = in.readString();
		disruptionDescription = in.readString();
	}
	
	public static final Parcelable.Creator<ServiceDisruption> CREATOR = new Parcelable.Creator<ServiceDisruption>() {
		public ServiceDisruption createFromParcel(Parcel in) {
			return new ServiceDisruption(in);
		}

		public ServiceDisruption[] newArray(int size) {
			return new ServiceDisruption[size];
		}
	};
	
	public String toString() {
		return "RouteNo = " + routeNumber + ", disrutionDisruption = " + disruptionDescription;
	};
}
