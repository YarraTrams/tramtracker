package com.yarratrams.tramtracker.objects;

import android.os.Parcel;
import android.os.Parcelable;

public class ServiceChange implements Parcelable{
	private String routeNumber;
	private String changeDescription;
	
	public ServiceChange() {

	}
	
	private ServiceChange(Parcel in) {
		readFromParcel(in);
	}
	

	
	public String getRouteNumber() {
		return this.routeNumber;
	}
	public void setRouteNumber(String routeNumber) {
		this.routeNumber = routeNumber;
	}

	public String getChangeDescription() {
		return this.changeDescription;
	}
	public void setChangeDescription(String changeDescription) {
		this.changeDescription = changeDescription;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(routeNumber);
		out.writeString(changeDescription);
	}
	
	public void readFromParcel(Parcel in) {
		routeNumber = in.readString();
		changeDescription = in.readString();
	}
	
	public static final Parcelable.Creator<ServiceChange> CREATOR = new Parcelable.Creator<ServiceChange>() {
		public ServiceChange createFromParcel(Parcel in) {
			return new ServiceChange(in);
		}

		public ServiceChange[] newArray(int size) {
			return new ServiceChange[size];
		}
		
	};
	
	public String toString() {
		return "RouteNo = " + routeNumber + ", change description = " + changeDescription;
	};
	
}
