package com.yarratrams.tramtracker.objects;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

public class Favourite implements Parcelable{
	private String name;
	private int order;
	private Stop stop;
	private ArrayList<String> alRoutes;
	
	public Favourite() {
		stop = new Stop();
	}
	
	public Favourite(Favourite old){
		this.name = old.getName();
		this.order = old.getOrder();
		this.stop = old.getStop();
		this.alRoutes = old.getAlRoutes();
	}
	
	private Favourite(Parcel in) {
		this();
		readFromParcel(in);
	}
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public Stop getStop() {
		return stop;
	}

	public void setStop(Stop stop) {
		this.stop = stop;
	}
	
	public ArrayList<String> getAlRoutes() {
		return alRoutes;
	}

	public void setAlRoutes(ArrayList<String> alRoutes) {
		this.alRoutes = alRoutes;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(name);
		out.writeInt(order);
		out.writeParcelable(stop, Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
	}
	
	public void readFromParcel(Parcel in) {
		name = in.readString();
		order = in.readInt();
		stop = in.readParcelable(Stop.class.getClassLoader());
	}
	
	public static final Parcelable.Creator<Favourite> CREATOR = new Parcelable.Creator<Favourite>() {
		public Favourite createFromParcel(Parcel in) {
			return new Favourite(in);
		}

		public Favourite[] newArray(int size) {
			return new Favourite[size];
		}
		
	};
	
//	public static final Parcelable.Creator<String> STRCREATOR = new Parcelable.Creator<String>() {
//		public String createFromParcel(Parcel in) {
//			return in.readString();
//		}
//
//		public String[] newArray(int size) {
//			return new String[size];
//		}
//		
//	};
	
	public String toString() {
		return "name = " + name + " order = " + order + " stop = " + stop;
	};
	
}
