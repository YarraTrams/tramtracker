package com.yarratrams.tramtracker.objects;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;


public class SearchResult implements Parcelable{
	private String keyword;
	private boolean searchStop;
	private boolean searchShelter;
	private boolean searchAccessible;
	private boolean searchShelterAccessible;
	private boolean searchOutlet;
	private boolean searchPOI; 
	private ArrayList<Stop> alStops;
	private ArrayList<TicketOutlet> alTicketOutlet;
	private ArrayList<PointOfInterest> alPointOfInterest;
	
	public SearchResult() {
		alStops = new ArrayList<Stop>();
		alTicketOutlet = new ArrayList<TicketOutlet>();
		alPointOfInterest = new ArrayList<PointOfInterest>();
	}
	
	public SearchResult(Parcel in) {
		this();
		readFromParcel(in);
	}
	
	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public boolean isSearchStop() {
		return searchStop;
	}

	public void setSearchStop(boolean searchStop) {
		this.searchStop = searchStop;
	}

	public boolean isSearchShelter() {
		return searchShelter;
	}

	public void setSearchShelter(boolean searchShelter) {
		this.searchShelter = searchShelter;
	}

	public boolean isSearchAccessible() {
		return searchAccessible;
	}

	public void setSearchAccessible(boolean searchAccessible) {
		this.searchAccessible = searchAccessible;
	}

	public boolean isSearchShelterAccessible() {
		return searchShelterAccessible;
	}

	public void setSearchShelterAccessible(boolean searchShelterAccessible) {
		this.searchShelterAccessible = searchShelterAccessible;
	}

	public boolean isSearchOutlet() {
		return searchOutlet;
	}

	public void setSearchOutlet(boolean searchOutlet) {
		this.searchOutlet = searchOutlet;
	}

	public boolean isSearchPOI() {
		return searchPOI;
	}

	public void setSearchPOI(boolean searchPOI) {
		this.searchPOI = searchPOI;
	}

	/**
	 * A returned value of null = no results 
	 * @return ArrayList<Stop>
	 */
	public ArrayList<Stop> getAlStops() {
		return alStops;
	}
	public void setAlStops(ArrayList<Stop> alStops) {
		this.alStops = alStops;
	}
	
	/**
	 * A returned value of null = no results
	 * @return ArrayList<TicketOutlet>
	 */
	public ArrayList<TicketOutlet> getAlTicketOutlet() {
		return alTicketOutlet;
	}
	public void setAlTicketOutlet(ArrayList<TicketOutlet> alTicketOutlet) {
		this.alTicketOutlet = alTicketOutlet;
	}
	
	/**
	 * A returned value of null = no results
	 * @return ArrayList<PointOfInterest>
	 */
	public ArrayList<PointOfInterest> getAlPointOfInterest() {
		return alPointOfInterest;
	}
	public void setAlPointOfInterest(ArrayList<PointOfInterest> alPointOfInterest) {
		this.alPointOfInterest = alPointOfInterest;
	}
	
	
	public ArrayList<Object> getAllResults(){
		ArrayList<Object> results = new ArrayList<Object>();
		results.addAll(alStops);
		results.addAll(alTicketOutlet);
		results.addAll(alPointOfInterest);
		return results;
	}
	
	public ArrayList<Object> get150Results(){
		ArrayList<Object> results = getAllResults();
		ArrayList<Object> shortList = new ArrayList<Object>();
		
		for(int i = 0; i < 150; i++){
			shortList.add(results.get(i));
		}
		return shortList;
	}
	
	public void limitTo150Results(){
		int total = 0;
		if(alStops.size() > 150){
			removeFrom(alStops, 150);
		}
		total = alStops.size();
		
		if(total >= 150){
			alTicketOutlet.clear();
		} else if((total + alTicketOutlet.size()) > 150){
			removeFrom(alTicketOutlet, (150 - total));
		}
		total = total + alTicketOutlet.size();
		
		if(total >= 150){
			alPointOfInterest.clear();
		} else if((total + alPointOfInterest.size()) > 150){
			removeFrom(alPointOfInterest, (150 - total));
		}
		total = total + alPointOfInterest.size();
	}
	
	public void removeFrom(ArrayList<?> list, int index){
		while(list.size() > index){
			list.remove(list.size()-1);
		}
	}
	
	
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(keyword);
		out.writeByte((byte)(searchStop == true?1:0));
		out.writeByte((byte)(searchShelter == true?1:0));
		out.writeByte((byte)(searchAccessible == true?1:0));
		out.writeByte((byte)(searchShelterAccessible == true?1:0));
		out.writeByte((byte)(searchOutlet == true?1:0));
		out.writeByte((byte)(searchPOI == true?1:0));
		
		out.writeTypedList(alStops);
		out.writeTypedList(alTicketOutlet);
		out.writeTypedList(alPointOfInterest);
	}
	
	public void readFromParcel(Parcel in) {
		keyword = in.readString();
		searchStop = in.readByte()==1?true:false;
		searchShelter = in.readByte()==1?true:false;
		searchAccessible = in.readByte()==1?true:false;
		searchShelterAccessible = in.readByte()==1?true:false;
		searchOutlet = in.readByte()==1?true:false;
		searchPOI = in.readByte()==1?true:false;
		
		in.readTypedList(alStops, Stop.CREATOR);
		in.readTypedList(alTicketOutlet, TicketOutlet.CREATOR);
		in.readTypedList(alPointOfInterest, PointOfInterest.CREATOR);
	}
	
	public static final Parcelable.Creator<SearchResult> CREATOR = new Parcelable.Creator<SearchResult>() {
		public SearchResult createFromParcel(Parcel in) {
			return new SearchResult(in);
		}

		public SearchResult[] newArray(int size) {
			return new SearchResult[size];
		}
	};
	
	@Override
	public String toString() {
		return "alStops = " + alStops + ", alTicketOutlet = " + alTicketOutlet + ", alPointOfInterest = " + alPointOfInterest;
	}

}
