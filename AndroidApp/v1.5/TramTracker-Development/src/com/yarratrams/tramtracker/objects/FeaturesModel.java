package com.yarratrams.tramtracker.objects;

public class FeaturesModel {
	
	
	int ID;
	String featureName;
	boolean isEnabled;
	
	
	
	public FeaturesModel() {
		// TODO Auto-generated constructor stub
	}



	public int getID() {
		return ID;
	}



	public void setID(int iD) {
		ID = iD;
	}



	public String getFeatureName() {
		return featureName;
	}



	public void setFeatureName(String featureName) {
		this.featureName = featureName;
	}



	public boolean isEnabled() {
		return isEnabled;
	}



	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}
	
	

}
