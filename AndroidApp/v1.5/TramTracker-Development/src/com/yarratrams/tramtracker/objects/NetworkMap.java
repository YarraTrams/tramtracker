package com.yarratrams.tramtracker.objects;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;


public class NetworkMap   {

	String url = "";
	String fileName = "";
	String activateDate = "";
	int isMapAvailable = 0; 


	public int getIsMapAvailable() {
		return isMapAvailable;
	}
	public void setIsMapAvailable(int isMapAvailable) {
		this.isMapAvailable = isMapAvailable;
	}



	public static final String URL = "url";
	public static final String FILE_NAME = "fileName";
	public static final String ACTIVE_DATE = "activeDate";
	public static final String IsMapAvailable = "IsMapAvailable";


	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}


	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}




	public String getActivateDate() {
		return activateDate;
	}



	public void setActivateDate(String activateDate) {

		
		Log.v("TT", "activation date ori: " + activateDate);
//		if(activateDate.length() < 14)
//		{
//			this.activateDate =  activateDate;
//		}
//		else
//		{	
//
//
//			String date = "";
//			date = activateDate.substring(activateDate.indexOf("(")+1, activateDate.indexOf("+"));
//			Log.v("TT", "activation date: " + date);
//			this.activateDate = date;
//		}
		
		//new logic after the change in date format
		
		if(activateDate.contains("T"))
		{
			activateDate = activateDate.substring(activateDate.indexOf("(")+1, activateDate.indexOf("T"));
			
			
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			
			try {
				date = simpleDateFormat.parse(activateDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			this.activateDate = String.valueOf(date.getTime());
			
		}
		else
		{
			this.activateDate = activateDate;
		}
		
		Log.v("TT", "activation date: " + this.activateDate);
	}




	public static ArrayList<NetworkMap> getArraylistFromJSOnArray(String data) 
	{


		ArrayList<NetworkMap> arrlstNetworkMaps = new ArrayList<NetworkMap>();
		NetworkMap temp = null;
		JSONArray jsonArray = null;


		try {
			jsonArray =  new JSONArray(data);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			jsonArray = null;
			e.printStackTrace();
			return null;
		}

		if(jsonArray!=null)
		{
			for(int i = 0 ; i < jsonArray.length() ;i++)
			{
				JSONObject jsonObject ;

				try {
					jsonObject = new JSONObject(jsonArray.getString(i));
					temp = new NetworkMap();

					temp.setActivateDate(jsonObject.getString(NetworkMap.ACTIVE_DATE));
					temp.setFileName(jsonObject.getString(NetworkMap.FILE_NAME));
					temp.setUrl(jsonObject.getString(NetworkMap.URL));
					temp.setIsMapAvailable(jsonObject.getInt(NetworkMap.IsMapAvailable));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


				arrlstNetworkMaps.add(temp);
				temp = null;
				jsonObject = null;
			}
		}





		return arrlstNetworkMaps;
	}

@Override
public String toString() {
	// TODO Auto-generated method stub
	return activateDate;
}



}
