package com.yarratrams.tramtracker.objects;

import com.google.gson.annotations.SerializedName;

public class MultiPageUrlModel {

	
	
	
	public final static int ANDROID = 3;
	public final static int BOTH = 1;
	
	@SerializedName("id")
	int ID;
	int sortOrder;
	String url;
	boolean isNew;
	boolean isActive;
	int deviceType;
	
	public MultiPageUrlModel() {
		// TODO Auto-generated constructor stub
	}
	
	
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public int getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public boolean isNew() {
		return isNew;
	}
	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}


	public int getDeviceType() {
		return deviceType;
	}


	public void setDeviceType(int deviceType) {
		this.deviceType = deviceType;
	}
	
	
	
	
}
