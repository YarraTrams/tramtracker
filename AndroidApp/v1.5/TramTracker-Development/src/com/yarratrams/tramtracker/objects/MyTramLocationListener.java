package com.yarratrams.tramtracker.objects;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.yarratrams.tramtracker.singleton.PredictedTimeResultSingleton;
import com.yarratrams.tramtracker.ui.OnBoardActivity;
import com.yarratrams.tramtracker.ui.TramTrackerMainActivity;

public class MyTramLocationListener implements LocationListener{
	Location oldLocation=null;
	LocationManager locationManager = null;
	int offset;
	short startingIndex,startingIndexDelayed;
	OnBoardActivity onboardActivity;
	PredictedArrivalTime atStop, nextStop;
	String message;
	PredictedTimeResult result;
	boolean firstTime = true;
	
	private Handler handlerUpdates = new Handler();
	private Runnable indexUpdater = new Runnable() {
		public void run() {
			onboardActivity.updateStartingIndex(startingIndex);
			atStop = null;
			offset = 0;
//			Toast.makeText(onboardActivity, "Change to next stop", Toast.LENGTH_LONG).show();
//			if(PredictedTimeResultSingleton.getResult() != null){
//				if(PredictedTimeResultSingleton.getResult().getStartingIndex() < PredictedTimeResultSingleton.getResult().getAlPredictedArrivalTime().size() - 1)
////					if(startingIndex >= PredictedTimeResultSingleton.getResult().getStartingIndex())
////						PredictedTimeResultSingleton.getResult().setStartingIndex(startingIndex);
////				onboardActivity.updateStartingIndex(PredictedTimeResultSingleton.getResult().getStartingIndex());
//				
//			}
		}
	};
	
	
	private Handler handlerDelayedUpdates = new Handler();
	private Runnable indexUpdaterDelayed = new Runnable() {
		public void run() {
			onboardActivity.updateStartingIndex(startingIndexDelayed);
			atStop = null;
			offset = 0;
//			Toast.makeText(onboardActivity, "Change to next stop", Toast.LENGTH_LONG).show();
		}
	};
	
//	private Handler handlerUpdateToNow = new Handler();
//	private Runnable indexUpdateToNow = new Runnable() {
//		public void run() {
//			onboardActivity.updateStopToNow();
//		}
//	};
	
//	private Handler handlerToast = new Handler();
//	private Runnable toaster = new Runnable() {
//		public void run() {
////			Toast.makeText(onboardActivity, message, Toast.LENGTH_SHORT).show();
//		}
//	};
	
	public MyTramLocationListener(OnBoardActivity onboardActivity, LocationManager manager) {
		this.onboardActivity = onboardActivity;
		this.locationManager = manager;
	}
	
	@Override
	public void onLocationChanged(Location newLocation) {
		try {
			if(PredictedTimeResultSingleton.getResult() != null){
				if(atStop == null && nextStop == null && firstTime){
					int index = PredictedTimeResultSingleton.getResult().getStartingIndex();
					if(index == -1)
						index = 0;
					this.atStop = PredictedTimeResultSingleton.getResult().getAlPredictedArrivalTime().get(index);
					this.nextStop = PredictedTimeResultSingleton.getResult().getAlPredictedArrivalTime().get(index + 1);
					firstTime = false;
				}
			}
			// has the location actually changed?
			//NSLog(@"Received location update.");
//			Log.w("onLocationChanged","start");
			// the location manager might have a more recent location
			if (locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER) != null) 
				if(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getTime() < newLocation.getTime())
					newLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

			// first thing we do is discard the first result, the cell tower location just isn't accurate enough
			if (oldLocation == null)
			{
				oldLocation = newLocation;
				//NSLog(@"Discarding first location");
				return;
			}
//			Log.w("onLocationChanged","70");
			// is it more than 2 minutes old?
//			Date nowDate = new Date();
//			if (Math.abs(newLocation.getTime() - nowdate.getTime())/1000 > 120)
//			{
//				//NSLog(@"Discarding location more than 2 minutes old");
//				return;
//			}
//			Log.w("onLocationChanged","77");
			if(PredictedTimeResultSingleton.getResult() != null)
				result = PredictedTimeResultSingleton.getResult();

			if(result == null){
//				message = "result = null";
//				handlerToast.post(toaster);
				Log.w("onLocationChanged","result = null");
				return;
			}
//			Log.w("onLocationChanged","86");
			// have we moved at all? are we now more than 50m from the terminus
			if (PredictedTimeResultSingleton.isAtLayover && result.getStartingIndex() > 0)
			{
				Stop firstStop = result.getAlPredictedArrivalTime().get(0).getStop();
				Stop secondStop = result.getAlPredictedArrivalTime().get(1).getStop();
				Location locFirstStop = new Location("");
				locFirstStop.setLatitude(firstStop.getLatitudeE6()/1E6);
				locFirstStop.setLongitude(firstStop.getLongitudeE6()/1E6);
				Location locSecondStop = new Location("");
				locSecondStop.setLatitude(secondStop.getLatitudeE6()/1E6);
				locSecondStop.setLongitude(secondStop.getLongitudeE6()/1E6);
				if (PredictedTimeResultSingleton.isAtLayover && newLocation.distanceTo(locFirstStop) > (firstStop.getLength() + 50) &&
						isLocationBetweenTwoLocations(newLocation, locFirstStop, locSecondStop))
				{
					message = "Located between first and second stop";
//					handlerToast.post(toaster);
					Log.w("onLocationchanged",message);
					PredictedTimeResultSingleton.isAtLayover = false;
				}
			}
			

			// update the accuracy indicator
			int accuracyLevel;
			if (newLocation.getAccuracy() <= 50)
				accuracyLevel = 1;
			else if (newLocation.getAccuracy() <= 150)
				accuracyLevel = 2;
			else if (newLocation.getAccuracy() <= 250)
				accuracyLevel = 3;
			else if (newLocation.getAccuracy() <= 500)
				accuracyLevel = 4;
			else
				accuracyLevel = 5;
//			Log.w("onLocationChanged","118");
			// has the accuracy level changed? If so reload the table, the next/now may have been disabled.
			//		if (self.parent.accuracyLevel != accuracyLevel)
			//			[self.tableView reloadData];

			// save the new accuracy level
			//		[self.parent setAccuracyLevel:accuracyLevel];

			// if we're still at layover then discard this result
			if (PredictedTimeResultSingleton.isAtLayover)
			{
				//			//NSLog(@"Discarding location due to being at layover");
				//			[self.parent updateHeadboardMessage:YES];
				Log.w("onLocationchanged","isAtLayover");
				return;
			}

			// throw out anything thats too far away
			if (newLocation.getAccuracy() >= 500)
			{
				//NSLog(@"Discarding location because the accuracy is too bad");
				//			[self.parent updateHeadboardMessage:YES];
				message = "newLocation.getAccuracy() >= 500";
				Log.w("onLocationchanged", message);
				return;
			}
//			Log.w("onLocationChanged","141");
			// if this is a non-public trip then we don't have scheduled times (and we're in free-flow mode)
			if (!PredictedTimeResultSingleton.isAvailable)
			{
				// Find out where the next stop is
				PredictedArrivalTime pat = getNextStopNearLocation(newLocation); 
				if (pat == null)
				{
					message = "Could not find the next stop value";
					Log.w("onLocationchanged", message);
//					handlerToast.post(toaster);
					//				[self.parent updateHeadboardMessage:YES];
					return;
				}
				startingIndex = (short)result.getAlPredictedArrivalTime().indexOf(pat);
				handlerUpdates.post(indexUpdater);
				//			[self scrollToStop:next];
				//			[self.tableView reloadData];
				//			[self.parent updateHeadboardMessage:YES];
				message = "!PredictedTimeResultSingleton.isAvailable, set next stop and scroll to it";
				Log.w("onLocationchanged", message);
//				handlerToast.post(toaster);
				return;
			}
			
//			Log.w("onLocationChanged","160");
			// if we don't have a full data set we discard this again
			if (result.getAlPredictedArrivalTime().size() == 0)
			{
				//NSLog(@"Discarding location due to insufficient setup");
				//			[self.parent updateHeadboardMessage:YES];
				message = "empty result";
				Log.w("onLocationchanged",message);
				
//				handlerToast.post(toaster);
				return;
			}

			// are we in the CBD?
			int index = result.getStartingIndex() == -1?0:result.getStartingIndex();
			if (result.getAlPredictedArrivalTime().get(index).getStop().isCityStop())
			{
				//NSLog(@"Discarding GPS location because we're in the CBD.");
//				if(PredictedTimeResultSingleton.getResult() != null)
//					PredictedTimeResultSingleton.getResult().setAccurate(true);
				message = "isCityStop() = " + result.getAlPredictedArrivalTime().get(index).getStop().getStopName();
				Log.w("onLocationchanged",message);
//				handlerToast.post(toaster);
				return;
			}
			
//			Log.w("onLocationChanged","179");

			// if we're more than 500m from what is considered the current stop then discard this result
			
//			handlerToast.post(toaster);
			Location locNextStop = new Location("");
			nextStop = result.getAlPredictedArrivalTime().get(index);
			if(nextStop != null){
				locNextStop.setLatitude(nextStop.getStop().getLatitudeE6()/1E6);
				locNextStop.setLongitude(nextStop.getStop().getLongitudeE6()/1E6);
				float distanceTo = newLocation.distanceTo(locNextStop);
				if (distanceTo > 500)
				{
					//NSLog(@"Discarding location because its more than 500m from the \"next stop\"");
					message = "newLocation too far from next stop = " + distanceTo + " > 500";
					Log.w("onLocationchanged",message);
					//				handlerToast.post(toaster);
					return;
				}
			}

			// Find out where the next stop is
			PredictedArrivalTime nextPat = getNextStopNearLocation(newLocation);
			if (nextPat == null)
			{
				message = "pat == null";
				Log.w("onLocationchanged",message);
//				handlerToast.post(toaster);
				return;
			}
			else{
				message = "next stop set as = " + nextPat.getStop().getStopName();
				Log.w("onLocationchanged",message);
			}
			Date dateNow = new Date();
			// if the predicted arrival time at that stop is more than 60 seconds away then something isn't working, unless its the next stop
			long offsetPredictedArrivalTime = (nextPat.getArrivalTime().getTime() - dateNow.getTime())/1000 + offset; 
			Log.w("onLocationChanged","offsetPredictedArrivalTime = " + offsetPredictedArrivalTime);
			
			//NSLog(@"Predicted Arrival Time: %@, With Offset: %@, Travel Time: %.2f", jstop.predicatedArrivalDateTime, offsetPredictedArrivalDate, [offsetPredictedArrivalDate timeIntervalSinceNow]);
			if (Math.abs(offsetPredictedArrivalTime) > 60 && result.getAlPredictedArrivalTime().indexOf(nextPat) > index + 1)
			{
				//"Discarding location due to impossible travel times."
				//			[self.parent updateHeadboardMessage:YES];
				Log.w("onLocationchanged","time to pat > 60 && pat index is more than 2 away");
				message = "Discarding location due to impossible travel times.";
//				handlerToast.post(toaster);
				return;
			}

			// update the offset
			int secondsToNextStop = secondsToNextStopWithLocation(newLocation, nextPat);
			int secondsPastPreviousStop = (int)((nextPat.getArrivalTime().getTime() - dateNow.getTime())/1000 - secondsToNextStop);
			Log.w("onLocationChanged","secondsToNextStop = " + secondsToNextStop);
			Log.w("onLocationChanged","secondsPastPreviousStop = " + secondsPastPreviousStop);
			//NSLog(@"%.2f - %.2f", secondsToNextStop, secondsPastPreviousStop);
			offset = secondsPastPreviousStop*-1;
			Log.w("onLocationChanged","offset = " + offset);
			//		PredictedArrivalTime patAt = result.getAlPredictedArrivalTime().get(result.getStartingIndex());
			// calculate the "now" value
			if (accuracyLevel <= 3 && !nextPat.getStop().isCityStop())
			{
				message = "accurate and not in the city";
				Log.w("onLocationChanged", message);
//				handlerToast.post(toaster);
				locNextStop.setLatitude(nextPat.getStop().getLatitudeE6()/1E6);
				locNextStop.setLongitude(nextPat.getStop().getLongitudeE6()/1E6);
				int distanceForNow = nextPat.getStop().getLength() > 20 ? nextPat.getStop().getLength() : 20;
				if (offsetPredictedArrivalTime <= 120 &&								// closer than 2 minutes
						locNextStop.distanceTo(newLocation) <= distanceForNow &&						// distance is close enough
						newLocation.getSpeed() < 40)																	// tram isn't hurtling past the stop
				{
//					Log.e("onLocationChanged","eligible for kickoff");
					if (atStop == null)
					{
						atStop = nextPat;
						startingIndexDelayed = (short)result.getAlPredictedArrivalTime().indexOf(nextPat);
						if(startingIndexDelayed + 1 < result.getAlPredictedArrivalTime().size())
							startingIndexDelayed++;
						// start a timer to kick us on from "now", assuming it hasn't already moved
//						handlerUpdates.postAtTime(indexUpdater, 0);
//						handlerUpdateToNow.post(indexUpdateToNow);
						handlerDelayedUpdates.postAtTime(indexUpdaterDelayed,10000);
						message = "now stop = " + nextPat.getStop().getStopName() + " jumping to " + result.getAlPredictedArrivalTime().get(startingIndex).getStop().getStopName();
						Log.w("onLocationchanged",message);
//						handlerToast.post(toaster);
						return;
					}
					else if(atStop.getStop().getTrackerID() != nextPat.getStop().getTrackerID())
					{
						atStop = nextPat;
						startingIndexDelayed = (short)result.getAlPredictedArrivalTime().indexOf(nextPat);
						if(startingIndexDelayed + 1 < result.getAlPredictedArrivalTime().size())
							startingIndexDelayed++;
						// start a timer to kick us on from "now", assuming it hasn't already moved
//						handlerUpdates.postAtTime(indexUpdater, 0);
//						handlerUpdateToNow.post(indexUpdateToNow);
						handlerDelayedUpdates.postAtTime(indexUpdaterDelayed,10000);
						message = "now stop = " + nextPat.getStop().getStopName() + " jumping to " + result.getAlPredictedArrivalTime().get(startingIndexDelayed).getStop().getStopName();
						Log.w("onLocationchanged",message);
//						handlerToast.post(toaster);
						return;
					}
					else if(atStop.getStop().getTrackerID() == nextPat.getStop().getTrackerID()){
						message = "AtStop == NextStop";
						Log.w("",message);
//						handlerToast.post(toaster);
					}
				}
				// are we at the stop but no longer close enough for "next" ?
				else if (atStop != null && atStop.getStop().getTrackerID() != nextPat.getStop().getTrackerID())
				{
					// yep, clear the at stop and move to the next stop - we can never go from now->next on the same stop
					atStop = null;
					index = result.getAlPredictedArrivalTime().indexOf(nextPat);
					if (index + 1 < result.getAlPredictedArrivalTime().size())
					{
						// we can move on to the next stop, reset the offset
						offset = 0;
						nextPat = result.getAlPredictedArrivalTime().get(index + 1);
						message = "are we at the stop but no longer close enough for next? skip a stop to set next";
						Log.w("onLocationchanged",message);
//						handlerToast.post(toaster);
						if(index + 1 < result.getAlPredictedArrivalTime().size())
						startingIndex = (short)(index + 1);
						handlerUpdates.post(indexUpdater);
						return;
					}
				}
			}
			message = "Move to stop nextPat = " + nextPat.getStop().getStopName();
			Log.w("onLocationChanged", message);
//			handlerToast.post(toaster);
			startingIndex = (short)result.getAlPredictedArrivalTime().indexOf(nextPat);
			handlerUpdates.post(indexUpdater);
			message = "";
			
			// move to that stop
//			onboardActivity.updateStartingIndex(result.getAlPredictedArrivalTime().indexOf(nextStop));
			//		[self moveToStop:next];
			//		[self.parent updateHeadboardMessage:YES];
		} catch (Exception e) {
			e.printStackTrace();
			try {
				BufferedWriter bos = new BufferedWriter(new FileWriter("/sdcard/ab.txt"));
				bos.write("Error starts");
				for(StackTraceElement el : e.getStackTrace()){
					bos.append(el.toString());
					bos.append("\n");
					bos.append("Translated : Class = " + el.getClassName() + " Method = " + el.getMethodName() + " Line number = " + el.getLineNumber());
					bos.append("\n");
				}
				bos.flush();
				bos.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
//	private boolean isLocationBetweenTwoLocations(Location location, Location stop1, Location stop2){
//		if(location.distanceTo(stop1) == 0 || location.distanceTo(stop2) == 0)
//			return true;
//		// calculate the bearings between ourselves and the other two locations
//		double bearingLocToStop2 = bearingToLocation(location, stop2);
//		double bearingStop1ToLoc = bearingToLocation(stop1, location);
//		double angle = Math.abs(bearingStop1ToLoc - bearingLocToStop2);
//		
//		// if the angle between those two bearings is more than 90 degrees then the stop is behind us
//		if (angle > 90)
//			return false;
//		
//		// so the stop is in front of us, work out the bearings from the remote end
//		double bearingFromStop2ToStop1 = bearingToLocation(stop2, stop1);
//		double bearingFromStop2ToLoc = bearingToLocation(stop2, location);
//		double remoteAngle = Math.abs(bearingFromStop2ToLoc - bearingFromStop2ToStop1);
//		
//		// if its more than 90 degrees then it is beyond the remote stop
//		if (remoteAngle > 90)
//			return false;
//		Log.d("MyTramLocationListener", "isLocationBetweenTwoLocations");
//		// its between us!
//		return true;
//	}
	
	
	private boolean isLocationBetweenTwoLocations(Location location, Location stop1, Location stop2){
		if(location.distanceTo(stop1) == 0 || location.distanceTo(stop2) == 0)
			return true;
		// calculate the bearings between ourselves and the other two locations
		double bearingStop1ToStop2 = bearingToLocation(stop1, stop2);
		double bearingStop1ToLoc = bearingToLocation(stop1, location);
		double angle = Math.abs(bearingStop1ToLoc - bearingStop1ToStop2);
		
		// if the angle between those two bearings is more than 90 degrees then the stop is behind us
		if (angle > 90)
			return false;
		
		// so the stop is in front of us, work out the bearings from the remote end
		double bearingFromStop2ToStop1 = bearingToLocation(stop2, stop1);
		double bearingFromStop2ToLoc = bearingToLocation(stop2, location);
		double remoteAngle = Math.abs(bearingFromStop2ToLoc - bearingFromStop2ToStop1);
		
		// if its more than 90 degrees then it is beyond the remote stop
		if (remoteAngle > 90)
			return false;
		Log.d("MyTramLocationListener", "isLocationBetweenTwoLocations");
		// its between us!
		return true;
	}
	
	private double bearingToLocation(Location location1, Location location2){
		// Convert these to radians
		double lat1 = (location1.getLatitude() * Math.PI / 180);
		double lat2 = (location2.getLatitude() * Math.PI / 180);
		double dLon = (location2.getLongitude() - location1.getLongitude()) * Math.PI / 180;

		// Calculate and return the bearing
		double y = Math.sin(dLon) * Math.cos(lat2);
		double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(dLon);
//		Log.d(tag, "bearingToLocation");
		return (Math.atan2(y, x) * 180 / Math.PI) + 360 % 360;
	}
	
	
	private PredictedArrivalTime getNextStopNearLocation(Location location){
		if(result != null){
			int index = result.getStartingIndex() == -1?0:result.getStartingIndex();
			for(int i=index; i < result.getAlPredictedArrivalTime().size()-2; i++){
//				Log.w("getNextStopNearLocation", "i = " + i);
				PredictedArrivalTime pat1 = result.alPredictedArrivalTime.get(i);
				PredictedArrivalTime pat2 = result.alPredictedArrivalTime.get(i+1);
				if(pat1 != null && pat2 != null){
					if(pat1.getArrivalTime() != null && pat2.getArrivalTime() != null){
						Stop stop1 = pat1.getStop();
						Location stop1Location = new Location("");
						stop1Location.setLatitude(stop1.getLatitudeE6()/1E6);
						stop1Location.setLongitude(stop1.getLongitudeE6()/1E6);
						Stop stop2 = pat2.getStop();
						Location stop2Location = new Location("");
						stop2Location.setLatitude(stop2.getLatitudeE6()/1E6);
						stop2Location.setLongitude(stop2.getLongitudeE6()/1E6);
						if(isLocationBetweenTwoLocations(location, stop1Location, stop2Location)){
							// this is our next stop then. Are we so-close to the previous stop that we're likely to be parked at it?
							if (stop1Location.distanceTo(location) <= 20){
								message = "getNextStopNearLocation located, Name = " + stop1.getStopName();
//								handlerToast.post(toaster);
								Log.w("getNextStopNearLocation",message);
								return pat1;
							}
							message = "getNextStopNearLocation located, Name = " + stop2.getStopName();
//							handlerToast.post(toaster);
							Log.w("getNextStopNearLocation",message);
							return pat2;
						}
						else{
							message = "not located between two locations";
//							handlerToast.post(toaster);
//							Log.w("getNextStopNearLocation",message);
						}
					}
				}
			}
		}
		else{
			message = "result null";
			Log.w("getNextStopNearLocation",message);
		}
		//The locations is somewhere other than between all the stops ie. beginning/end/stop previous to our last call to the web service
		message = "getNextStopNearLocation not located";
		Log.w("getNextStopNearLocation",message);
//		handlerToast.post(toaster);
		return null;
	}
	
	/**
	 * Find out how far away we are from the next stop using the distance to it
	 **/
	private int secondsToNextStopWithLocation(Location location, PredictedArrivalTime pat)
	{
		message = "secondsToNextStopWithLocation";
//		handlerToast.post(toaster);		
		// find the next stop
		Stop next = pat.getStop();
		if (next == null)
			return 0;
		
		// find the previous stop in the list
		int locationInStopList = result.alPredictedArrivalTime.indexOf(pat);
		if (locationInStopList -1 < 0)
			return 0;
		PredictedArrivalTime patPrevious = result.alPredictedArrivalTime.get(locationInStopList-1);
		Stop previous = patPrevious.getStop();
		
		message = "previous trackerid = " + previous.getTrackerID() + " ,next trackerid = " + next.getTrackerID();
//		handlerToast.post(toaster);
		
		// find the distance that we've travelled
		DistanceTravelled distance = distanceTravelledToStop(previous,next,location);
		if (distance.totalDistance == -1 || distance.totalDistance == 0)
			return 0;
		
		// so how much is left to go?
		double percentageLeftToNextStop = (distance.totalDistance - distance.travelledDistance) / distance.totalDistance;
		
//		// work that out
//		JourneyStop *jstop = [self.journey journeyStopForStop:next];
//		if (jstop == nil)
//			return 0;
		
		// so this is the interval until we reach that next stop
		int secondsToGo = (int)((pat.getArrivalTime().getTime() - new Date().getTime())/1000 * percentageLeftToNextStop);
		Log.d("", "secondsToGo = " + secondsToGo);
		// all done
		message = "secondsToGo = " + secondsToGo;
//		handlerToast.post(toaster);
		return secondsToGo;
	}
	
	
	// find the distance travelled between two stops
	private DistanceTravelled distanceTravelledToStop(Stop previous, Stop next, Location loc)
	{
		// work out the distance between the two stops
		DistanceTravelled distance = new DistanceTravelled();
		Location loc1 = new Location("");
		Location loc2 = new Location("");
		loc1.setLatitude(previous.getLatitudeE6()/1E6);
		loc1.setLongitude(previous.getLongitudeE6()/1E6);
		loc2.setLatitude(next.getLatitudeE6()/1E6);
		loc2.setLongitude(next.getLongitudeE6()/1E6);
		distance.totalDistance = loc1.distanceTo(loc2);
		
		// lets check to see whether this point falls between the two stops if we're not then set the distance travelled to -1
		if (!isLocationBetweenTwoLocations(loc, loc1, loc2))
		{
			distance.travelledDistance = -1;
			return distance;
		}
		
		// this is definitely not the most elegant way to do this, but it should work
		double bearingPrevToNext = bearingToLocation(loc1, loc2);
		double bearingPrevToLoc = bearingToLocation(loc1, loc);
		
		// work out the angle between the two bearings
		double angle = Math.abs(bearingPrevToLoc - bearingPrevToNext);
		
		// and the distance to the location
		double distanceToLoc = loc1.distanceTo(loc);
		
		// so if we project the point onto our line between the two stops we end up with a right-angled triangle
		// use trig to find the length of the projection
		double lengthOfProjection = Math.sin(angle) * distanceToLoc;
		
		// And then pythagoras to find the distance we're looking for!
		distance.travelledDistance = (float)Math.sqrt(Math.pow(distanceToLoc, 2) - Math.pow(lengthOfProjection, 2));
		Log.d("", "travelledDistance = " + distance.travelledDistance);
		return distance;
	}

	@Override
	public void onProviderDisabled(String provider) {
		if(provider.equalsIgnoreCase(LocationManager.GPS_PROVIDER))
			displayErrorMessage("Please enable location functionality in the device and try again");
	}

	@Override
	public void onProviderEnabled(String provider) {
		if(provider.equalsIgnoreCase(LocationManager.GPS_PROVIDER))
		
			displayErrorMessage("GPS available again.");
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		if(provider.equalsIgnoreCase(LocationManager.GPS_PROVIDER)){
			if(status == LocationProvider.OUT_OF_SERVICE){
				displayErrorMessage("tramTracker may be inaccurate because the GPS out of service.");
			}
			else if(status == LocationProvider.TEMPORARILY_UNAVAILABLE){
				displayErrorMessage("tramTracker may be inaccurate because the GPS is temporarily unavailable.");
			}
			else if(status == LocationProvider.AVAILABLE){
//				displayErrorMessage("GPS available again.");
			}
		}
	}
	
	public void displayErrorMessage(String strMessage){
		Message message = new Message();
		Bundle bundle = new Bundle();
		bundle.putString("error", strMessage);
		message.setData(bundle);
		TramTrackerMainActivity.getAppManager().handlerToast.sendMessage(message); 
	}

}
