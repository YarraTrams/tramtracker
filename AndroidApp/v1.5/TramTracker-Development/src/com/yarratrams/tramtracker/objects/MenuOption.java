package com.yarratrams.tramtracker.objects;

import android.os.Parcel;
import android.os.Parcelable;

public class MenuOption implements Parcelable{
	private int menuID;
	private int menuResourceIcon;
	private int menuResourceName;
	
	public MenuOption(){
		
	}

	public MenuOption(int menuID, int menuResourceIcon, int menuResourceName){
		this.menuID = menuID;
		this.menuResourceIcon = menuResourceIcon;
		this.menuResourceName = menuResourceName;
	}
	
	private MenuOption(Parcel in) {
		readFromParcel(in);
	}

	public int getMenuID() {
		return menuID;
	}

	public void setMenuID(int menuID) {
		this.menuID = menuID;
	}

	public int getMenuResourceIcon() {
		return menuResourceIcon;
	}

	public void setMenuResourceIcon(int menuResourceIcon) {
		this.menuResourceIcon = menuResourceIcon;
	}

	public int getMenuResourceName() {
		return menuResourceName;
	}

	public void setMenuResourceName(int menuResourceName) {
		this.menuResourceName = menuResourceName;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeInt(menuID);
		out.writeInt(menuResourceIcon);
		out.writeInt(menuResourceName);
	}
	
	public void readFromParcel(Parcel in) {
		menuID = in.readInt();
		menuResourceIcon = in.readInt();
		menuResourceName = in.readInt();
	}
	
	public static final Parcelable.Creator<MenuOption> CREATOR = new Parcelable.Creator<MenuOption>() {
		public MenuOption createFromParcel(Parcel in) {
			return new MenuOption(in);
		}

		public MenuOption[] newArray(int size) {
			return new MenuOption[size];
		}
	};
	
}
