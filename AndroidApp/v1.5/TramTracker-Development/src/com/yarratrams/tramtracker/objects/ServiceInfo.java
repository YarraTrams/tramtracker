package com.yarratrams.tramtracker.objects;

import java.util.ArrayList;
import java.util.Date;

import android.os.Parcel;
import android.os.Parcelable;

public class ServiceInfo implements Parcelable{
	private ArrayList<ServiceChange> serviceChanges;
	private ArrayList<ServiceDisruption> serviceDisruptions;
	private ArrayList<Route> routes;
	private boolean emptyResponse;
	private Date serverReferenceDate; 	//Time sent back by server
	
	public ServiceInfo(){
		serviceChanges = new ArrayList<ServiceChange>();
		serviceDisruptions = new ArrayList<ServiceDisruption>();
		routes = new ArrayList<Route>();
	}
	
	private ServiceInfo(Parcel in) {
		this();
		readFromParcel(in);
	}
	
	
	public ArrayList<ServiceChange> getServiceChanges() {
		return this.serviceChanges;
	}
	
	public void setServiceChanges(ArrayList<ServiceChange> serviceChanges) {
		this.serviceChanges = serviceChanges;
	}
	
	public ArrayList<ServiceDisruption> getServiceDisruptions() {
		return this.serviceDisruptions;
	}
	
	public void setServiceDisruptions(
			ArrayList<ServiceDisruption> serviceDisruptions) {
		this.serviceDisruptions = serviceDisruptions;
	}
	
	public ArrayList<Route> getRoutes() {
		return this.routes;
	}
	
	public void setRoutes(ArrayList<Route> routes) {
		this.routes = routes;
	}
	
	public Date getServerReferenceDate() {
		return serverReferenceDate;
	}

	public void setServerReferenceDate(Date serverReferenceDate) {
		this.serverReferenceDate = serverReferenceDate;
	}
	

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeTypedList(serviceChanges);
		out.writeTypedList(serviceDisruptions);
		out.writeTypedList(routes);
	}
	
	public void readFromParcel(Parcel in) {
		in.readTypedList(serviceChanges, ServiceChange.CREATOR);
		in.readTypedList(serviceDisruptions, ServiceDisruption.CREATOR);
		in.readTypedList(routes, Route.CREATOR);
	}
	
	public static final Parcelable.Creator<ServiceInfo> CREATOR = new Parcelable.Creator<ServiceInfo>() {
		public ServiceInfo createFromParcel(Parcel in) {
			return new ServiceInfo(in);
		}

		public ServiceInfo[] newArray(int size) {
			return new ServiceInfo[size];
		}
	};
	
	public String toString() {
		return "serviceChanges = " + serviceChanges + ", serviceDisruptions = " + serviceDisruptions + ", routes = " + routes + ", serverReferenceDate = " + serverReferenceDate;
	}

	public boolean hasEmptyResponse() {
		return emptyResponse;
	}

	public void setHasEmptyResponse(boolean hasResponse) {
		this.emptyResponse = hasResponse;
	};
	
	
}
