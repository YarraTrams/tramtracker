package com.android.androidtooltip;

import android.view.View;

import com.android.androidtooltip.TooltipManager.Builder;


/*
 * CUSTOM CLASS CREATED FOR SHOWING TOOLTIPS, MAKING EASY FOR DEVELOPER TO CREATE OWN TUTORIAL TOOL TIP OBJECT.
 */

public class TutorialTooltip {

	Builder builder;



	public TutorialTooltip() {
		// TODO Auto-generated constructor stub
	}

	
	//different tooltip style
	
	public void createTutorialTooltip(TooltipManager tooltipManager, View v, TooltipManager.onTooltipClosingCallback tooltipClosingCallback,  TooltipManager.Gravity gravity, String text)
	{

		builder = new Builder(tooltipManager, v.getId());

		builder.anchor(v, gravity);
		builder.text(text);
		builder.toggleArrow(true);
		builder.fitToScreen(true);
		builder.withCallback(tooltipClosingCallback);
		builder.withStyleId(R.style.ToolTipLayoutDefaultStyle2);
		
		builder.closePolicy(TooltipManager.ClosePolicy.TouchInside, 0);	
		
	}

	public void createTutorialTooltipNoAnchor(TooltipManager tooltipManager, View v, TooltipManager.onTooltipClosingCallback tooltipClosingCallback,  TooltipManager.Gravity gravity, String text, int width)
	{

		builder = new Builder(tooltipManager, v.getId());

		builder.anchor(v, gravity);
		builder.text(text);
		builder.toggleArrow(false);
		builder.maxWidth(width);
		builder.fitToScreen(true);
		builder.withCallback(tooltipClosingCallback);
		builder.withStyleId(R.style.ToolTipLayoutDefaultStyle2);
		
		builder.closePolicy(TooltipManager.ClosePolicy.TouchInside, 0);	
	}

	
	public void createTutorialTooltipNoDismiss(TooltipManager tooltipManager, View v, TooltipManager.onTooltipClosingCallback tooltipClosingCallback,  TooltipManager.Gravity gravity, String text)
	{

		builder = new Builder(tooltipManager, v.getId());

		builder.anchor(v, gravity);
		builder.text(text);
		builder.toggleArrow(true);
		builder.fitToScreen(true);
		builder.withCallback(tooltipClosingCallback);
		builder.withStyleId(R.style.ToolTipLayoutDefaultStyle2);
		
		builder.closePolicy(TooltipManager.ClosePolicy.None, 0);	
	}
	
	public void showTooltip()
	{
		try {
			if(builder!=null)
			{
				builder.show();
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	public Builder getTutorialTooltipById()
	{

		return builder;

	}


	
	public String getText()
	{
		return builder.text.toString();
	}

	public void closeTutorialTooltip(TooltipManager tooltipManager)
	{
			tooltipManager.hide(builder.id);
	}
}
