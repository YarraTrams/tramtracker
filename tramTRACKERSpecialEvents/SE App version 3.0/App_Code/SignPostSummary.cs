﻿    using System;
    using System.IO;
    using System.Text;
    using System.Data;
    using System.Data.SqlClient;
    using System.Collections;
    using System.Collections.Generic;
using System.Diagnostics;

    public class SignPostSummary : Savable
    {
        public Int32 spInfoID {get; set; }
     
        public string createdBy { get; set; }
        public string modifiedBy { get; set; }
        public DateTime createdDate { get; set; }
        public DateTime modifiedDate { get; set; }
        public DateTime publishedDate { get; set; }
        public DateTime unpublishedDate { get; set; }
        public string publishedBy { get; set; }
        public string unpublishedBy { get; set; }

    public string spDescription { get; set; }
	    public Boolean isPublished { get; set; }
        public string affectedSignpostCode { get; set; }

        public List<SignPostUpdates> spUpdates { get; set; }
      

        

        public int SaveSignPostInfoAndGetSID(string description, string addedBy)
        {
            int sID = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SpecialEvent_SaveSignPostInfo";
                cmd.Parameters.Add("@signPostDescription", SqlDbType.VarChar).Value = description;
                cmd.Parameters.Add("@createdBy", SqlDbType.VarChar).Value = addedBy;
                DataSet result = ExecuteCommand(cmd);
                sID = Convert.ToInt32(result.Tables[0].Rows[0][0].ToString());

            }

            return sID;
        }
    
         public bool CheckEndDateValid(int _SID)
         {
            bool isExist;
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SpecialEvent_CheckEndDateValid";
                cmd.Parameters.Add("@spInfoID", SqlDbType.Int).Value = _SID;
                DataSet result = ExecuteCommand(cmd);
                isExist = Convert.ToBoolean(result.Tables[0].Rows[0][0].ToString());

            }

            return isExist;
         }

    public static List<SignPostSummary> GetSignPostsInfo()
        {
            List<SignPostSummary> spInfoSummaries = new List<SignPostSummary>();
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SpecialEvent_GetSignpostInfo";

                cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();

                DataSet children = new DataSet("table");
                SqlDataAdapter myAdapter = new SqlDataAdapter();
                myAdapter.SelectCommand = cmd;
                myAdapter.Fill(children);

                foreach (DataRow dr in children.Tables[0].Rows)
                {
                    SignPostSummary temp = new SignPostSummary();
                    temp.spInfoID = Convert.ToInt16(dr["sID"]);
                    temp.spDescription = dr["signPostDescription"].ToString();
                    temp.createdDate = Convert.ToDateTime(dr["createdDate"]);
                    temp.modifiedDate = Convert.ToDateTime(dr["modifiedDate"]);
                    temp.modifiedBy = SpecialEvent.FormatName(dr["modifiedBy"].ToString());
                    temp.createdBy = SpecialEvent.FormatName(dr["createdBy"].ToString());
                    temp.affectedSignpostCode = dr["affectedSignposts"].ToString();
                    if (dr["isPublished"] != DBNull.Value)
                            temp.isPublished = true;
                        else
                            temp.isPublished = false;
                    if (dr["publishedDate"] != DBNull.Value)
                        temp.publishedDate = Convert.ToDateTime(dr["publishedDate"]);
                    if (dr["unpublishedDate"] != DBNull.Value)
                        temp.unpublishedDate = Convert.ToDateTime(dr["unpublishedDate"]);
                    if (!DBNull.Equals(dr["publishedBy"], DBNull.Value))
                        temp.publishedBy = SpecialEvent.FormatName(dr["publishedBy"].ToString());
                    if (!DBNull.Equals(dr["unpublishedBy"], DBNull.Value))
                        temp.unpublishedBy = SpecialEvent.FormatName(dr["unpublishedBy"].ToString());
                    else
                        temp.unpublishedBy = "";
                    spInfoSummaries.Add(temp);
                }
            }
            return spInfoSummaries;
        }

    public int UpdateSignPostInfo(int spInfo,string description, string modifiedBy)
    {
        int sID = 0;
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SpecialEvent_UpdateSignPostInfo";
            cmd.Parameters.Add("@sID", SqlDbType.Int).Value = spInfo;
            cmd.Parameters.Add("@signPostDescription", SqlDbType.VarChar).Value = description;
            cmd.Parameters.Add("@modifiedBy", SqlDbType.VarChar).Value = modifiedBy;
            DataSet result = ExecuteCommand(cmd);
            

        }

        return sID;
    }



    public void GetSignPostsSpecificInfo(int spInfoID)
        {
             using (SqlCommand cmd = new SqlCommand())
             {
                 cmd.CommandTimeout = 200;
                 cmd.CommandType = CommandType.StoredProcedure;
                 cmd.CommandText = "SpecialEvent_GetSignPostsSpecificInfo";
                 cmd.Parameters.Add("@spInfoID", SqlDbType.Int).Value = spInfoID;
                

                 cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
                 DataSet result = ExecuteCommand(cmd);
                 this.spInfoID = Convert.ToInt16(result.Tables[0].Rows[0]["sID"]);
                 this.spDescription = result.Tables[0].Rows[0]["signPostDescription"].ToString();
                 this.createdDate = Convert.ToDateTime(result.Tables[0].Rows[0]["createdDate"]);
                 this.modifiedDate = Convert.ToDateTime(result.Tables[0].Rows[0]["modifiedDate"]);
                 this.modifiedBy = SpecialEvent.FormatName(result.Tables[0].Rows[0]["modifiedBy"].ToString());
                 this.createdBy = SpecialEvent.FormatName(result.Tables[0].Rows[0]["createdBy"].ToString());
                 SignPostUpdates sp = new SignPostUpdates();
                 this.spUpdates = sp.GetSignpostUpdates(spInfoID);
                
              }
             
            }
        public void PublishSignPost(int _sID,string _publishedBy)
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandTimeout = 200;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SpecialEvent_PublishSignpostUpdatesToProd";
                cmd.Parameters.Add("@spInfoID", SqlDbType.Int).Value = _sID;
                cmd.Parameters.Add("@publishedBy", SqlDbType.VarChar).Value = _publishedBy;
                cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
                DataSet result = ExecuteCommand(cmd);

            }


         }
    
        public void UnpublishSignPost(int _sID, string _unpublishedBy)
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandTimeout = 200;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SpecialEvent_UnpublishSignpostUpdatesFromProd";
                cmd.Parameters.Add("@spInfoID", SqlDbType.Int).Value = _sID;
                cmd.Parameters.Add("@unpublishedBy", SqlDbType.VarChar).Value = _unpublishedBy;
                cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
                DataSet result = ExecuteCommand(cmd);

            }


        }
         public void DeleteSignPost(int _sID, string _deletedBy)
         {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandTimeout = 200;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SpecialEvent_DeleteSignpost";
                cmd.Parameters.Add("@spInfoID", SqlDbType.Int).Value = _sID;
                cmd.Parameters.Add("@deletedBy", SqlDbType.VarChar).Value = _deletedBy;
                cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
                DataSet result = ExecuteCommand(cmd);

            }


         }
    
}
