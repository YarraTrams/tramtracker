﻿using System;
using System.IO;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class Stop : Savable
{
    public int RouteNumber { get; set; }
    public int TrackerID { get; set; }
    public int StopSequence { get; set; }
    public int NumberOfServices { get; set; }
    public bool TramTrackerAvailable { get; set; }
    public MessagesSummary Message { get; set; }
    public bool IsExtraStop { get; set; }
    public bool HasShuttleTrams { get; set; }
    public bool IsUpStop { get; set; }
    public string Description { get; set; }
    public int EventID { get; set; }
    public List<Int32> MissingRoutes { get; set; }

    public override void Load()
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "GetRouteStopByTrackerID";
            cmd.Parameters.Add("@stopNo", SqlDbType.Int).Value = this.TrackerID;
            cmd.Parameters.Add("@routeNo", SqlDbType.Int).Value = this.RouteNumber;
            DataSet result = ExecuteCommand(cmd);
            this.RouteNumber = Convert.ToInt32(result.Tables[0].Rows[0]["RouteNo"]);
            this.IsUpStop = Convert.ToBoolean(result.Tables[0].Rows[0]["UpStop"].ToString());
            this.Description = result.Tables[0].Rows[0]["Description"].ToString();
            this.StopSequence = Convert.ToInt32(result.Tables[0].Rows[0]["StopSequence"]);
            this.TrackerID = Convert.ToInt32(result.Tables[0].Rows[0]["stopNo"]);
            LoadSpecialEventDetails();
            LoadTempSpecialEventDetails();
        }
    }

    public void LoadMissingRoutes()
    {
        this.MissingRoutes = new List<Int32>();

        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SpecialEvent_GetSpecialEventsMissingRoutesForStop";
            cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = this.EventID;
            cmd.Parameters.Add("@stopNo", SqlDbType.Int).Value = this.TrackerID;
            cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();

            DataSet children = new DataSet("table");
            SqlDataAdapter myAdapter = new SqlDataAdapter();
            myAdapter.SelectCommand = cmd;
            myAdapter.Fill(children);

            foreach (DataRow dr in children.Tables[0].Rows)
            {
                Int32 r = Convert.ToInt32(dr["RouteNo"]);
                this.MissingRoutes.Add(r);
            }
        }
    }

    public static List<Stop> LoadAllStops(int routeNo, bool isUpStop, int eventID, DateTime startDate, DateTime endDate)
    {
        int startInSeconds = startDate.Hour * 3600 + startDate.Minute * 60;
        TimeSpan ts_diff = endDate.Subtract(startDate);
        int endInSeconds = startInSeconds + Convert.ToInt32(ts_diff.TotalSeconds);

        List<Stop> stops = new List<Stop>();
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandTimeout = 200;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SpecialEvent_GetSpecialEventRouteStopDetails";
            cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = eventID;
            cmd.Parameters.Add("@routeNo", SqlDbType.Int).Value = routeNo;
            cmd.Parameters.Add("@upStop", SqlDbType.Bit).Value = isUpStop;
            int dayOfWeek = EventUtil.GetDayOfWeek(startDate.DayOfWeek);
            cmd.Parameters.Add("@dayOfWeek", SqlDbType.TinyInt).Value = dayOfWeek;
            cmd.Parameters.Add("@startTime", SqlDbType.Int).Value = startInSeconds;
            cmd.Parameters.Add("@endTime", SqlDbType.Int).Value = endInSeconds;
            cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
            Debug.WriteLine(eventID + " " + routeNo + " " + dayOfWeek + " " + startInSeconds + " " + endInSeconds);
            DataSet children = new DataSet("table");
            SqlDataAdapter myAdapter = new SqlDataAdapter();
            myAdapter.SelectCommand = cmd;
            myAdapter.Fill(children);

            foreach (DataRow dr in children.Tables[0].Rows)
            {
                Stop stop = new Stop();
                stop.EventID = eventID;
                stop.RouteNumber = Convert.ToInt32(dr["RouteNo"]);
                stop.StopSequence = Convert.ToInt32(dr["StopSequence"]);
                stop.TrackerID = Convert.ToInt32(dr["StopNo"]);
                stop.IsUpStop = Convert.ToBoolean(dr["UpStop"]);
                stop.Description = dr["Description"].ToString();
                stop.NumberOfServices = Convert.ToInt32(dr["SchCount"]);
                stop.HasShuttleTrams = Convert.ToBoolean(dr["shuttletrams"]);
                stop.TramTrackerAvailable = Convert.ToBoolean(dr["TramTrackerAvailable"]);
                stop.LoadMissingRoutes();
                //stop.LoadSpecialEventDetails();
                stops.Add(stop);
            }
        }
        
        return stops;
    }

    public void LoadSpecialEventDetails()
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandTimeout = 200;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "GetSpecialEventDetailsByStop";
            cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = this.EventID;
            cmd.Parameters.Add("@routeNo", SqlDbType.SmallInt).Value = this.RouteNumber;
            cmd.Parameters.Add("@stopId", SqlDbType.Int).Value = this.TrackerID;

            cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
            DataSet result = ExecuteCommand(cmd);
            if (result.Tables[0].Rows.Count > 0)
            {
                this.TramTrackerAvailable = Convert.ToBoolean(result.Tables[0].Rows[0]["tramtrackeravailable"].ToString());
                this.IsExtraStop = Convert.ToBoolean(result.Tables[0].Rows[0]["extrastop"].ToString());
                this.HasShuttleTrams = Convert.ToBoolean(result.Tables[0].Rows[0]["shuttletrams"].ToString());
                this.StopSequence = Convert.ToInt32(result.Tables[0].Rows[0]["stopsequence"]);
            }
            else
            {
                //makes sure TramTracker available is set to true by default
                this.TramTrackerAvailable = true;
            }
        }  
    }

    public void LoadSpecialEventMessage()
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandTimeout = 200;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "GetRouteStopSpecialEventMessage";
            cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = this.EventID;
            cmd.Parameters.Add("@stopNo", SqlDbType.SmallInt).Value = this.TrackerID;
            cmd.Parameters.Add("@routeNo", SqlDbType.SmallInt).Value = this.RouteNumber;
            cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
            DataSet children = new DataSet("table");
            SqlDataAdapter myAdapter = new SqlDataAdapter();
            myAdapter.SelectCommand = cmd;
            myAdapter.Fill(children);
            DataRow dr = children.Tables[0].Rows[0];

            this.Message = new MessagesSummary();
            this.Message.MessageID = Convert.ToInt32(dr["MessageID"]);
            this.Message.RouteNumbers = dr["RouteNo"].ToString();
            this.Message.SMSMessage = dr["SMSMessage"].ToString();
            this.Message.FormattedIVRMessage = dr["FormattedIVRMessage"].ToString();
        }
    }

    public void LoadTempSpecialEventDetails()
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "GetTempSpecialEventDetails";
            cmd.Parameters.Add("@eventId", SqlDbType.Int).Value = this.EventID;
            cmd.Parameters.Add("@stopNo", SqlDbType.Int).Value = this.TrackerID;
            cmd.Parameters.Add("@routeNo", SqlDbType.Int).Value = this.RouteNumber;
            DataSet result = ExecuteCommand(cmd);
            if (result.Tables[0].Rows.Count > 0)
            {
                this.TramTrackerAvailable = Convert.ToBoolean(result.Tables[0].Rows[0]["TramTrackerAvailable"]);
                this.IsUpStop = Convert.ToBoolean(result.Tables[0].Rows[0]["UpStop"]);
                this.HasShuttleTrams = Convert.ToBoolean(result.Tables[0].Rows[0]["ShuttleTrams"]);
            }
        }
    }

    public void SaveSpecialEventStopDetails()
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 36000;
            if (!TramTrackerAvailable)
            {
                cmd.CommandText = "SpecialEvent_AddSpecialEventEffectedRouteStops";
                cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = this.EventID;
                cmd.Parameters.Add("@routeNo", SqlDbType.SmallInt).Value = this.RouteNumber;
                cmd.Parameters.Add("@stopNo", SqlDbType.SmallInt).Value = this.TrackerID;
                cmd.Parameters.Add("@upStop", SqlDbType.Bit).Value = this.IsUpStop;
            }
            else
            {
                cmd.CommandText = "AddSpecialEventUnaffectedRouteStops";
                cmd.Parameters.Add("@EventId", SqlDbType.Int).Value = this.EventID;
            }
            DataSet result = ExecuteCommand(cmd);
        }
        
    }

    public void UpdateHasShuttles()
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "UpdateTempSpecialEventRouteStops";
            cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = this.EventID;
            cmd.Parameters.Add("@routeNo", SqlDbType.Int).Value = this.RouteNumber;
            cmd.Parameters.Add("@stopNo", SqlDbType.Int).Value = this.TrackerID;
            cmd.Parameters.Add("@upStop", SqlDbType.Bit).Value = this.IsUpStop;
            cmd.Parameters.Add("@shuttleTrams", SqlDbType.Bit).Value = this.HasShuttleTrams;
            DataSet result = ExecuteCommand(cmd);
        }
    }

    public void DeleteSpecialeventStopDetails()
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SpecialEvent_DeleteSpecialEventEffectedRouteStops";
            cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = this.EventID;
            cmd.Parameters.Add("@routeNo", SqlDbType.SmallInt).Value = this.RouteNumber;
            cmd.Parameters.Add("@stopNo", SqlDbType.SmallInt).Value = this.TrackerID;
            cmd.Parameters.Add("@upStop", SqlDbType.Bit).Value = this.IsUpStop;
            DataSet result = ExecuteCommand(cmd);
        }
    }

    public List<string> GetMissingRoutesForSpecialEvent()
    {
        List<string> missingRtes = new List<string>();
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "GetMissingRoutesForAffectedSpecialEventStop";
            cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = this.EventID;
            cmd.Parameters.Add("@stopNo", SqlDbType.SmallInt).Value = this.TrackerID;
            DataSet result = ExecuteCommand(cmd);
            if (result.Tables.Count > 0)
            {
                for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                {
                    missingRtes.Add(result.Tables[0].Rows[i][0].ToString());
                }
            }
        }
        return missingRtes;
    }
}
