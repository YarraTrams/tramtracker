﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for EventUtil
/// </summary>
public class EventUtil
{
    public static int GetDayOfWeek(DayOfWeek dow)
    {
        int dayofweek = 0;

        switch (dow)
        {
            case DayOfWeek.Monday:
                dayofweek = 1;
                break;
            case DayOfWeek.Tuesday:
                dayofweek = 2;
                break;
            case DayOfWeek.Wednesday:
                dayofweek = 3;
                break;
            case DayOfWeek.Thursday:
                dayofweek = 4;
                break;
            case DayOfWeek.Friday:
                dayofweek = 5;
                break;
            case DayOfWeek.Saturday:
                dayofweek = 6;
                break;
            case DayOfWeek.Sunday:
                dayofweek = 0;
                break;

            default:
                dayofweek = 0;
                break;
        }
        return dayofweek;
    }
}