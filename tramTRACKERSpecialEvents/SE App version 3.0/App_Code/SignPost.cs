﻿using System;
using System.IO;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class SignPost : Savable
{
    public string DestinationCode { get; set; }
    public string Destination { get; set; }


    public override void Load()
    {
        
    }

    public static List<SignPost> LoadAllSignPosts()
    {
        List<SignPost> existingSignPostList = new List<SignPost>();
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SpecialEvent_GetAllSignPosts";
            cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
            DataSet children = new DataSet("table");
            SqlDataAdapter myAdapter = new SqlDataAdapter();
            myAdapter.SelectCommand = cmd;
            myAdapter.Fill(children);
            foreach (DataRow dr in children.Tables[0].Rows)
            
            {
                SignPost sp = new SignPost();
                sp.DestinationCode = Convert.ToString(dr["DestinationCode"]);
                sp.Destination = Convert.ToString(dr["Destination"]);
                existingSignPostList.Add(sp);
               
            }
        }

        return existingSignPostList;
    }

   

   
}
