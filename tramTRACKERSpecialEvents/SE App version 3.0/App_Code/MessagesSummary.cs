﻿    using System;
    using System.IO;
    using System.Text;
    using System.Data;
    using System.Data.SqlClient;
    using System.Collections;
    using System.Collections.Generic;
using System.Diagnostics;

    public class MessagesSummary : Savable
    {
        public Int32 EventID {get; set; }
        public string RouteNumbers { get; set; }
        public Int32 StopNo { get; set; }
        public string SMSMessage { get; set; }
        public string FormattedIVRMessage { get; set; }
        public Int32 MessageID { get; set; }
        public string tempMessageID { get; set; }
        public string IVRMessage { get; set; }

        public string tempIVRFileNameID { get; set; }


        public override void Save()
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SpecialEvent_AddSpecialEventMessages";
                cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = this.EventID;
                cmd.Parameters.Add("@MessageID", SqlDbType.VarChar).Value = this.MessageID;
                cmd.Parameters.Add("@SMSMessage", SqlDbType.VarChar).Value = this.SMSMessage;
                cmd.Parameters.Add("@IVRMessage", SqlDbType.VarChar).Value = this.FormattedIVRMessage;
                cmd.Parameters.Add("@IVRFileName", SqlDbType.VarChar).Value = this.IVRMessage;
                Debug.WriteLine("IVR filename: " + this.IVRMessage);
                DataSet result = ExecuteCommand(cmd);
            }
        }

        public void SavePublicHoliday(){
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "AddPublicHolidaySpecialEvent";
                cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = this.EventID;
                DataSet result = ExecuteCommand(cmd);
            }
        }

        // [GetSpecialEventsSummaryInformation] @eventID int ,@routeNo smallint , @upStop bit
        public static List<MessagesSummary> LoadMessagesSummary(Int32 eventid, Int16 stopNo, Int16 routeNumber)
        {
            List<MessagesSummary> messagesSummaries = new List<MessagesSummary>();

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SpecialEvent_GetRouteStopSpecialEventMessage";
                cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = eventid;
                cmd.Parameters.Add("@stopNo", SqlDbType.SmallInt).Value = stopNo;
                cmd.Parameters.Add("@routeNo", SqlDbType.SmallInt).Value = routeNumber;
                cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
                DataSet children = new DataSet("table");
                SqlDataAdapter myAdapter = new SqlDataAdapter();
                myAdapter.SelectCommand = cmd;
                myAdapter.Fill(children);
                foreach (DataRow dr in children.Tables[0].Rows)
                {
                    MessagesSummary temp = new MessagesSummary();
                    temp.RouteNumbers = dr["RouteNo"].ToString();
                    temp.SMSMessage = dr["SMSMessage"].ToString();
                    temp.FormattedIVRMessage = dr["FormattedIVRMessage"].ToString();
                    temp.IVRMessage = dr["IVRMessage"].ToString();
                    messagesSummaries.Add(temp);
                }
            }
            return messagesSummaries;
        }

        public static List<MessagesSummary> LoadPreEventMessagesSummary(Int32 eventid)
        {
            List<MessagesSummary> messagesSummaries = new List<MessagesSummary>();

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SpecialEvent_GetPreEventMessages";
                cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = eventid;
                cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
                DataSet children = new DataSet("table");
                SqlDataAdapter myAdapter = new SqlDataAdapter();
                myAdapter.SelectCommand = cmd;
                myAdapter.Fill(children);
                foreach (DataRow dr in children.Tables[0].Rows)
                {
                    MessagesSummary temp = new MessagesSummary();
                    temp.MessageID = Convert.ToInt32(dr["MessageID"]);
                    temp.SMSMessage = dr["SMSMessage"].ToString();
                    temp.FormattedIVRMessage = dr["FormattedIVRMessage"].ToString();
                    temp.IVRMessage = dr["IVRMessage"].ToString();
                    messagesSummaries.Add(temp);
                }
            }
            return messagesSummaries;
        }

        public static List<MessagesSummary> LoadPublicEventMessagesSummary(Int32 eventid)
        {
            List<MessagesSummary> messagesSummaries = new List<MessagesSummary>();

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SpecialEvent_GetPreEventMessages";
                cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = eventid;
                cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
                DataSet children = new DataSet("table");
                SqlDataAdapter myAdapter = new SqlDataAdapter();
                myAdapter.SelectCommand = cmd;
                myAdapter.Fill(children);
                foreach (DataRow dr in children.Tables[0].Rows)
                {
                    MessagesSummary temp = new MessagesSummary();
                    temp.MessageID = Convert.ToInt32(dr["MessageID"]);
                    temp.SMSMessage = dr["SMSMessage"].ToString();
                    temp.FormattedIVRMessage = dr["FormattedIVRMessage"].ToString();
                    temp.IVRMessage = dr["IVRMessage"].ToString();
                    messagesSummaries.Add(temp);
                }
            }
            return messagesSummaries;
        }

        public override void Delete()
        {
            /*
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteSpecialEvent";
            cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = this.EventID;
            DataSet result = ExecuteCommand(cmd);
             * */
        }

        public void GetLastMessageID()
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "[SpecialEvent_GetNewMessageID]";
                DataSet result = ExecuteCommand(cmd);
                this.tempMessageID = result.Tables[0].Rows[0]["MessageID"].ToString();
            }
        }

        public void GetTempIVRFileNameID()
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "[SpecialEvent_GetTempIVRFileNameID]";
                DataSet result = ExecuteCommand(cmd);
                this.tempIVRFileNameID = result.Tables[0].Rows[0]["IVRId"].ToString();
            }
        }

        
    }
