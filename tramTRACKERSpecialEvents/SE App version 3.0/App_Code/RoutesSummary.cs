﻿

    using System;
    using System.IO;
    using System.Text;
    using System.Data;
    using System.Data.SqlClient;
    using System.Collections;
    using System.Collections.Generic;
using System.Diagnostics;

    public class RoutesSummary : Savable
    {
        public int RouteNumber { get; set; }
        public string UpDestination { get; set; }
        public string DownDestination { get; set; }
        public string Description { get; set; }

        // This method loads the person from the database based on the TableId value
        public override void Load()
        {

        }

        public static List<RoutesSummary> LoadRoutesSummary(int eventid, bool isUpStop)
        {
            List<RoutesSummary> routeSummaries = new List<RoutesSummary>();
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SpecialEvent_GetSpecialEventRoutesDetails";
                cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = eventid;
                cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();

                DataSet children = new DataSet("table");
                SqlDataAdapter myAdapter = new SqlDataAdapter();
                myAdapter.SelectCommand = cmd;
                myAdapter.Fill(children);

                foreach (DataRow dr in children.Tables[0].Rows)
                {
                    RoutesSummary temp = new RoutesSummary();
  
                    temp.RouteNumber = Convert.ToInt16(dr["RouteNo"]);
                    
                    String _upDest = dr["UpDestination"].ToString();
                    String _downDest = dr["DownDestination"].ToString();

                    //Modify the Up Destination values and down destination values to reflect the direction chosen by the user

                    if (isUpStop)
                    {
                        temp.UpDestination = _upDest;
                        temp.DownDestination = _downDest;
                    }
                    else
                    {
                        temp.UpDestination = _downDest;
                        temp.DownDestination = _upDest;
                    }
                    routeSummaries.Add(temp);
                }
            }
            return routeSummaries;
        }

        public static List<RoutesSummary> LoadPreEventRoutesSummary(int eventid)
        {
            List<RoutesSummary> routeSummaries = new List<RoutesSummary>();
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SpecialEvent_GetPreEventRoutesDetails";
                cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = eventid;
                cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();

                DataSet children = new DataSet("table");
                SqlDataAdapter myAdapter = new SqlDataAdapter();
                myAdapter.SelectCommand = cmd;
                myAdapter.Fill(children);

                foreach (DataRow dr in children.Tables[0].Rows)
                {
                    RoutesSummary temp = new RoutesSummary();
                    temp.RouteNumber = Convert.ToInt16(dr["RouteNo"]);

                    temp.Description=dr["RouteDescription"].ToString();
                  
                    routeSummaries.Add(temp);
                }
            }
            return routeSummaries;
        }

    }
