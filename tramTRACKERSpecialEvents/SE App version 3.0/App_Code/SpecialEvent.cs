﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Collections;
using System.Diagnostics;

/// <summary>
/// Summary description for SpecialEvent
/// </summary>
public class SpecialEvent : Savable
{
    public Int32 EventID { get; set; }
    public string EventDesc { get; set; }
    public DateTime EventStartDate { get; set; }
    public DateTime EventEndDate { get; set; }
    public bool IsPublicHoliday { get; set; }
    public bool IsPreEvent { get; set; }
    public bool IsPublished { get; set; }
    public string CreatedBy { get; set; }
    public string AffectedRouteList { get; set; }
    public string UpdatedBy { get; set; }
    public string LastPublishedBy { get; set; }
    public string LastUnPublishedBy { get; set; }
    public Int32 SortOrder { get; set; }
    public DateTime LastUpdate { get; set; }
    public DateTime LastPublishedUpdate { get; set; }
    public DateTime LastUnPublishedUpdate { get; set; }
    public List<Route> AffectedRoutes { get; set; }
    public List<Stop> AffectedStops { get; set; }
    public List<MessagesSummary> SpecialEventMessages { get; set; }

    //Work around to get around issues with teh AddPublicHolidaySpecialEvent
    //stored procedure
    public string IVRHolidayMessage { get; set; }
    public string SMSHolidayMessage { get; set; }

    public string IVRFileName { get; set; }
    public SpecialEvent() { AffectedRoutes = new List<Route>(); }

    public override void Load()
    {    
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SpecialEvent_GetSpecialEvent";
            cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = this.EventID;
            DataSet result = ExecuteCommand(cmd);
            this.EventID = Convert.ToInt32(result.Tables[0].Rows[0]["EventID"]);
            this.EventDesc = Convert.ToString(result.Tables[0].Rows[0]["EventDesc"].ToString());
            this.EventStartDate = Convert.ToDateTime(result.Tables[0].Rows[0]["EventStartDate"].ToString());
            this.EventEndDate = Convert.ToDateTime(result.Tables[0].Rows[0]["EventEndDate"].ToString());
            this.IsPublicHoliday = Convert.ToBoolean(result.Tables[0].Rows[0]["PublicHoliday"]);
            this.CreatedBy = result.Tables[0].Rows[0]["CreatedBy"].ToString();
            this.UpdatedBy = result.Tables[0].Rows[0]["UpdatedBy"].ToString();
            if (!DBNull.Equals(result.Tables[0].Rows[0]["LastUpdate"], DBNull.Value))
                this.LastUpdate = Convert.ToDateTime(result.Tables[0].Rows[0]["LastUpdate"]);
            this.IVRHolidayMessage = result.Tables[0].Rows[0]["IVRMessage"].ToString();
            this.SMSHolidayMessage = result.Tables[0].Rows[0]["SMSMessage"].ToString();
            if (result.Tables[0].Rows[0]["PreEvent"] != DBNull.Value)
            {
                this.IsPreEvent = Convert.ToBoolean(result.Tables[0].Rows[0]["PreEvent"]);
            }
            else
            {
                this.IsPreEvent = false;
            }
              
        }
    }


    public override void Save()
    {
        if (this.EventID == 0)
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SpecialEventSet";
                cmd.Parameters.Add("@EventDesc", SqlDbType.VarChar).Value = this.EventDesc;
                cmd.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = this.EventStartDate;
                cmd.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = this.EventEndDate;
                cmd.Parameters.Add("@publicHoliday", SqlDbType.Bit).Value = this.IsPublicHoliday;
                cmd.Parameters.Add("@createBy", SqlDbType.NVarChar).Value = this.CreatedBy;
                cmd.Parameters.Add("@SMSMessage", SqlDbType.NVarChar).Value = this.SMSHolidayMessage;
                cmd.Parameters.Add("@IVRMessage", SqlDbType.VarChar).Value = this.IVRHolidayMessage;
                cmd.Parameters.Add("@IVRFileName", SqlDbType.VarChar).Value = this.IVRFileName;
                DataSet result = ExecuteCommand(cmd);
                this.EventID = Convert.ToInt32(result.Tables[0].Rows[0][0].ToString());
               
            }
        }
        else
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SpecialEventSet";
                cmd.Parameters.Add("@EventID", SqlDbType.Int).Value = this.EventID;
                cmd.Parameters.Add("@EventDesc", SqlDbType.VarChar).Value = this.EventDesc;
                cmd.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = this.EventStartDate;
                cmd.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = this.EventEndDate;
                cmd.Parameters.Add("@publicHoliday", SqlDbType.Bit).Value = this.IsPublicHoliday;
                cmd.Parameters.Add("@createBy", SqlDbType.NVarChar).Value = this.CreatedBy;
                cmd.Parameters.Add("@SMSMessage", SqlDbType.NVarChar).Value = this.SMSHolidayMessage;
                cmd.Parameters.Add("@IVRMessage", SqlDbType.VarChar).Value = this.IVRHolidayMessage;
                cmd.Parameters.Add("@IVRFileName", SqlDbType.VarChar).Value = this.IVRFileName;
                DataSet result = ExecuteCommand(cmd);
            }
        }
    }

    //The stored procedure called here also deletes the message from the data
    public override void Delete()
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();
            cmd.CommandTimeout = 300; 
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection(); 
            cmd.CommandText = "SpecialEvent_DeleteSpecialEvent";
            cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = this.EventID;
            DataSet result = ExecuteCommand(cmd);
        }
        
    }

    //Get all the events order by sort order
    public static List<SpecialEvent> GetAllEventsOrderBySortOrder()
    {
        List<SpecialEvent> items = new List<SpecialEvent>();
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SpecialEvent_GetAllEventsOrderBySortOrder";
            cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
            DataSet children = new DataSet("table");
            SqlDataAdapter myAdapter = new SqlDataAdapter();
            myAdapter.SelectCommand = cmd;
            myAdapter.Fill(children);
            foreach (DataRow dr in children.Tables[0].Rows)
            {
                SpecialEvent temp = new SpecialEvent();
                temp.EventID = Convert.ToInt32(dr["EventID"]);
                temp.EventDesc = dr["EventDesc"].ToString();
                temp.EventStartDate = Convert.ToDateTime(dr["EventStartDate"]);
                temp.EventEndDate = Convert.ToDateTime(dr["EventEndDate"]);
                temp.IsPublicHoliday = Convert.ToBoolean(dr["PublicHoliday"]);
                temp.CreatedBy = FormatName(dr["CreatedBy"].ToString());
                temp.UpdatedBy = FormatName(dr["UpdatedBy"].ToString());
                temp.LastPublishedBy = FormatName(dr["LastPublishedBy"].ToString());
                temp.LastUnPublishedBy = FormatName(dr["LastUnPublishedBy"].ToString());

                if (dr["LastPublishedDate"] != DBNull.Value)
                    temp.LastPublishedUpdate = Convert.ToDateTime(dr["LastPublishedDate"]);

                if ((dr["LastUnPublishedDate"] != DBNull.Value))
                    temp.LastUnPublishedUpdate = Convert.ToDateTime(dr["LastUnPublishedDate"]);
                   
                if (dr["PreEvent"] != DBNull.Value)
                    temp.IsPreEvent = Convert.ToBoolean(dr["PreEvent"]);
                else
                   temp.IsPreEvent = false;
               
                if (dr["isPublished"] != DBNull.Value)
                    temp.IsPublished = Convert.ToBoolean(dr["isPublished"]);
                else
                    temp.IsPublished = false;
               
                if (!DBNull.Equals(dr["LastUpdate"], DBNull.Value))
                    temp.LastUpdate = Convert.ToDateTime(dr["LastUpdate"]);

                if (!DBNull.Equals(dr["Sortorder"], DBNull.Value))
                    temp.SortOrder = Convert.ToInt32(dr["Sortorder"]);

                if (!DBNull.Equals(dr["AffectedRoutes"], DBNull.Value))
                    temp.AffectedRouteList = Convert.ToString(dr["AffectedRoutes"]);
                else
                    temp.AffectedRouteList = "Event still needs to be finalised";
                items.Add(temp);
            }
        }
        return items;
    }

    //Update the sortorder of the events
    public  void UpdatePriority(int eventID,int sortOrder)
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SpecialEvent_UpdateEventSortOrder";
            cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = eventID;
            cmd.Parameters.Add("@sortOrder", SqlDbType.Int).Value = sortOrder;
            DataSet result = ExecuteCommand(cmd);
        }
        
    }

    public void LoadTempStops(int routeNo, bool isUpStop)
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "GetTempSelectedStops";
            cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = EventID;
            cmd.Parameters.Add("@routeNo", SqlDbType.Int).Value = routeNo;
            cmd.Parameters.Add("@upStop", SqlDbType.Bit).Value = isUpStop;
        }
    }

    public static List<SpecialEvent> LoadAllEvents(DateTime startTime, DateTime endTime)
    {
        List<SpecialEvent> items = new List<SpecialEvent>();
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandTimeout = 300; 
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SpecialEvent_GetSpecialEventsByDateRange";
            cmd.Parameters.Add("@startDate", SqlDbType.DateTime).Value = startTime;
            cmd.Parameters.Add("@endDate", SqlDbType.DateTime).Value = endTime;
            cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
            DataSet children = new DataSet("table");
            SqlDataAdapter myAdapter = new SqlDataAdapter();
            myAdapter.SelectCommand = cmd;
            myAdapter.Fill(children);
            foreach (DataRow dr in children.Tables[0].Rows)
            {
                SpecialEvent temp = new SpecialEvent();
                temp.EventID = Convert.ToInt32(dr["EventID"]);
                temp.EventDesc = dr["EventDesc"].ToString();
                temp.EventStartDate = Convert.ToDateTime(dr["EventStartDate"]);
                temp.EventEndDate = Convert.ToDateTime(dr["EventEndDate"]);
                temp.IsPublicHoliday = Convert.ToBoolean(dr["PublicHoliday"]);
                temp.CreatedBy = FormatName(dr["CreatedBy"].ToString());
                temp.UpdatedBy = FormatName(dr["UpdatedBy"].ToString());
                temp.LastPublishedBy = FormatName(dr["LastPublishedBy"].ToString());
                temp.LastUnPublishedBy = FormatName(dr["LastUnPublishedBy"].ToString());
                
                if (dr["PreEvent"] != DBNull.Value)
                {
                    temp.IsPreEvent = Convert.ToBoolean(dr["PreEvent"]);
                    Debug.WriteLine("temp.IsPreEvent" + temp.IsPreEvent);
                }
                else
                {
                    temp.IsPreEvent = false;
                }
                if (!DBNull.Equals(dr["LastUpdate"], DBNull.Value))
                    temp.LastUpdate = Convert.ToDateTime(dr["LastUpdate"]);

                if (dr["LastPublishedDate"] != DBNull.Value)
                {
                   
                    temp.LastPublishedUpdate = Convert.ToDateTime(dr["LastPublishedDate"]);
                    Debug.WriteLine("LastPublishedDate:-" + temp.LastPublishedUpdate);
                }


                if ((dr["LastUnPublishedDate"] != DBNull.Value))
                {
                   
                    temp.LastUnPublishedUpdate = Convert.ToDateTime(dr["LastUnPublishedDate"]);
                    Debug.WriteLine("LastUnPublishedDate :-" + temp.LastUnPublishedUpdate);
                }
                    

                if (dr["isPublished"] != DBNull.Value)
                {
                    temp.IsPublished = Convert.ToBoolean(dr["IsPublished"]);
                    
                }
                else
                {
                    temp.IsPublished = false;
                }

                items.Add(temp);
            }
        }
        return items;
    }

    public static string FormatName(string name)
    {
        string formattedName = null;
        //Debug.WriteLine("Name length "+name.Length);
        if (!string.IsNullOrEmpty(name) || !(name.Length ==0 ))
        {
            char delim = '\\';
            string[] stringParts = name.Split(delim);
            formattedName = stringParts[1];
        }
        else
        {
            formattedName = "-";
        }
        return formattedName;
    }


    //Does not load persistant objects, just loads the stops from a temp table for validation purposes
    public void LoadAffectedStops()
    {
        this.AffectedStops = new List<Stop>();
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "GetSpecialEventAffectedStops";
            cmd.Parameters.Add("@eventId", SqlDbType.Int).Value = this.EventID;
            cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
            DataSet children = new DataSet("table");
            SqlDataAdapter myAdapter = new SqlDataAdapter();
            myAdapter.SelectCommand = cmd;
            myAdapter.Fill(children);
            foreach (DataRow dr in children.Tables[0].Rows)
            {
                Stop s = new Stop();
                s.RouteNumber = Convert.ToInt32(dr["RouteNo"]);
                s.TrackerID = Convert.ToInt32(dr["StopNo"]);
                s.TramTrackerAvailable = Convert.ToBoolean(dr["TramTrackerAvailable"]);
                s.IsUpStop = Convert.ToBoolean(dr["UpStop"]);
                this.AffectedStops.Add(s);
            }
        }
    }

    public void LoadAffectedRoutes()
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "GetSpecialEventRoutes";
            cmd.Parameters.Add("@eventId", SqlDbType.Int).Value = this.EventID;
            cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
            DataSet children = new DataSet("table");
            SqlDataAdapter myAdapter = new SqlDataAdapter();
            myAdapter.SelectCommand = cmd;
            myAdapter.Fill(children);
            foreach (DataRow dr in children.Tables[0].Rows)
            {
                Route r = new Route();
                r.RouteNumber = Convert.ToInt32(dr["EventID"]);
                r.Load();

                this.AffectedRoutes.Add(r);
            }
        }
    }

    public void LoadTempSpecialEventMessages(bool isTTAvailable, bool isShuttleTram)
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SpecialEvent_GetTempSpecialEventMessages";
            cmd.Parameters.Add("@eventId", SqlDbType.Int).Value = this.EventID;
            cmd.Parameters.Add("@tramTrackerAvailable", SqlDbType.Int).Value = isTTAvailable;
            cmd.Parameters.Add("@isShuttle", SqlDbType.Bit).Value = isShuttleTram;
            cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
            DataSet children = new DataSet("table");
            SqlDataAdapter myAdapter = new SqlDataAdapter();
            myAdapter.SelectCommand = cmd;
            myAdapter.Fill(children);
            this.SpecialEventMessages = new List<MessagesSummary>();
            foreach (DataRow dr in children.Tables[0].Rows)
            {
                MessagesSummary ms = new MessagesSummary();
                ms.MessageID = Convert.ToInt32(dr["MessageID"]);
                ms.RouteNumbers = dr["RouteNo"].ToString();
                ms.SMSMessage = dr["SMSMessage"].ToString();
                ms.FormattedIVRMessage = dr["IVRMessage"].ToString();
                ms.IVRMessage = dr["IVRFileName"].ToString();
                this.SpecialEventMessages.Add(ms);
            }
        }
    }

    public void ConsolidateSpecialEvent()
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandTimeout = 300;      
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SpecialEvent_GetSpecialEventsConsolidated";
            cmd.Parameters.Add("@eventid", SqlDbType.Int).Value = EventID;
            //Stopwatch sw = Stopwatch.StartNew();
            ExecuteCommand(cmd);
           // sw.Stop();

            //Console.WriteLine("Time taken: {0}ms", sw.Elapsed.TotalMilliseconds);
            
        }
    }

    public void FinaliseSpecialEventMessages()
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandTimeout = 300;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SpecialEvent_GetFinalSpecialEventMessages";
            cmd.Parameters.Add("@eventid", SqlDbType.Int).Value = EventID;
            ExecuteCommand(cmd);
        }
    }

    public void ReadyToPublish(int eventID)
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandTimeout = 200;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SpecialEvent_ReadyToPublish";
            cmd.Parameters.Add("@eventId", SqlDbType.Int).Value = eventID;
            ExecuteCommand(cmd); 
        }
    }




    public void DeleteFromLive()
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TTProductionConnectionString"].ToString();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
            cmd.CommandText = "DeleteSpecialEventFromLive";
            cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = this.EventID;
            DataSet result = ExecuteCommand(cmd);
        }
    }



    public void changeToInActive(int eventID)
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandTimeout = 200;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SpecialEvent_GetMessageIDToBeDeleted";
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();
            cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
            cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = eventID;
            DataSet children = new DataSet("table");
            SqlDataAdapter myAdapter = new SqlDataAdapter();
            myAdapter.SelectCommand = cmd;
            myAdapter.Fill(children);
            foreach (DataRow dr in children.Tables[0].Rows)
            {
                RouteStop seRouteStop = new RouteStop();
                seRouteStop.UpdateSERouteStops(Convert.ToInt32(dr["MessageID"]));
                seRouteStop.deleteSEMessages(Convert.ToInt32(dr["MessageID"]));
            }
        }
    }

    public void UpdateStatusAfterOrdering(int eventID)
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            
            cmd.CommandTimeout = 200;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SpecialEvent_GetIsPublishedSE";
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();
            cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
            cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = eventID;
            DataSet result = ExecuteCommand(cmd);
            if (!DBNull.Equals(result.Tables[0].Rows[0][0], DBNull.Value))
                IsPublished= Convert.ToBoolean(result.Tables[0].Rows[0][0]);
            if (IsPublished)
            {
                RouteStop seRouteStop = new RouteStop();
                changeToInActive(eventID);
                seRouteStop.CreateWeeklySpecialEventData();
                string unPublishedBy = HttpContext.Current.User.Identity.Name; ;
                seRouteStop.UpdatePublishStatus(eventID, 0, unPublishedBy);
            }
        }
    }
    public string GetEventDesc(int eventId)
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SpecialEvent_GetSpecialEvent";
            cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = eventId;
            DataSet result = ExecuteCommand(cmd);
            Debug.WriteLine(this.EventID);
            return Convert.ToString(result.Tables[0].Rows[0][1].ToString());
            
        }
    }



    public void  UpdateAllEventsOrder()
    {
         using (SqlCommand cmd = new SqlCommand())
        {
             cmd.CommandTimeout = 200;
             cmd.CommandType = CommandType.StoredProcedure;
             cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
             cmd.CommandText = "SpecialEvent_UpdateAllEventsOrder";
             DataSet result = ExecuteCommand(cmd);
         }
    }
}