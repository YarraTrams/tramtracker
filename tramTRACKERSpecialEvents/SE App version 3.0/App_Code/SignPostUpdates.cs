﻿    using System;
    using System.IO;
    using System.Text;
    using System.Data;
    using System.Data.SqlClient;
    using System.Collections;
    using System.Collections.Generic;
using System.Diagnostics;

public class SignPostUpdates : Savable
    {
        public Int32 id { get; set; }
        public Int32 spInfoID {get; set; }
        public string dCode { get; set; }
        public string existingDestination { get; set; }
        public string destinationChange { get; set; }
        public DateTime changeStartDate { get; set; }
        public DateTime changeEndDate { get; set; }


      

        public override void Save()
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SpecialEvent_SaveSignpostUpdates";
                cmd.Parameters.Add("@spInfoID", SqlDbType.Int).Value = this.spInfoID;
                cmd.Parameters.Add("@dCode", SqlDbType.Char,4).Value = this.dCode;
                cmd.Parameters.Add("@existingDestination", SqlDbType.VarChar).Value = this.existingDestination;
                cmd.Parameters.Add("@destinationChange", SqlDbType.VarChar).Value = this.destinationChange;
                cmd.Parameters.Add("@changeStartDate", SqlDbType.DateTime).Value = this.changeStartDate;
                cmd.Parameters.Add("@changeEndDate", SqlDbType.DateTime).Value = this.changeEndDate;
                Debug.WriteLine("");
                DataSet result = ExecuteCommand(cmd);
            }
        }



        public List<SignPostUpdates> GetSignpostUpdates(int spInfoID)
        {
            List<SignPostUpdates> spUpdate=new List<SignPostUpdates>();
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SpecialEvent_GetSignpostUpdateSpecificInfo";
                cmd.Parameters.Add("@spInfoID", SqlDbType.Int).Value = spInfoID;
                cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();

                DataSet children = new DataSet("table");
                SqlDataAdapter myAdapter = new SqlDataAdapter();
                myAdapter.SelectCommand = cmd;
                myAdapter.Fill(children);

                foreach (DataRow dr in children.Tables[0].Rows)
                {
                    SignPostUpdates temp = new SignPostUpdates();
                    temp.id = Convert.ToInt16(dr["id"]);
                    temp.spInfoID = Convert.ToInt16(dr["spInfoID"]);
                    temp.dCode = dr["DestinationCode"].ToString();
                    temp.existingDestination = dr["ExistingDestination"].ToString();
                    temp.destinationChange = dr["DestinationChange"].ToString();
                    temp.changeStartDate = Convert.ToDateTime(dr["ChangeStartDate"]);
                    temp.changeEndDate =Convert.ToDateTime(dr["ChangeEndDate"]);

                    spUpdate.Add(temp);
                }
            }
            return spUpdate;
            
        }
       

        
    }
