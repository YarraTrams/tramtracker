﻿    using System;
    using System.IO;
    using System.Text;
    using System.Data;
    using System.Data.SqlClient;
    using System.Collections;
    using System.Collections.Generic;

    public class Route : Savable
    {
        public int RouteNumber { get; set; }
        public string UpDestination { get; set; }
        public string DownDestination { get; set; }
        public string Description { get; set; }
        public List<Stop> UpStops { get; set; }
        public List<Stop> DownStops { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int EventID { get; set; }

        // This method loads the person from the database based on the TableId value
        public override void Load()
        {

        }

        public static List<Route> LoadAllMainRoutes()
        {
            List<Route> routes = new List<Route>();
            SqlCommand cmd = new SqlCommand();
            using (cmd)
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetAllMainRoutes";
                cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();

                DataSet children = new DataSet("table");
                SqlDataAdapter myAdapter = new SqlDataAdapter();
                myAdapter.SelectCommand = cmd;
                myAdapter.Fill(children);

                foreach (DataRow dr in children.Tables[0].Rows)
                {
                    Route temp = new Route();
                    temp.RouteNumber = Convert.ToInt16(dr["RouteNo"]);
                    temp.Description = dr["Description"].ToString();
                    routes.Add(temp);
                }
            }
            
            return routes;
        }
       
        
        public void LoadUpStops()
        {
            this.UpStops = Stop.LoadAllStops(this.RouteNumber, true, this.EventID, this.StartTime, this.EndTime);
        }

        public void LoadDownStops()
        {
            List<Stop> tempDownStops = Stop.LoadAllStops(this.RouteNumber, false, this.EventID, this.StartTime, this.EndTime);
            tempDownStops.Reverse();
            this.DownStops = tempDownStops;
        }

        public static List<Route> CheckSpecialEventRoutes(int eventid, bool isUpStop)
        {
            List<Route> routeSummaries = new List<Route>();
            SqlCommand cmd = new SqlCommand();
            using (cmd)
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SpecialEvent_GetSpecialEventRoutesDetails";
                cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = eventid;
                cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();

                DataSet children = new DataSet("table");
                SqlDataAdapter myAdapter = new SqlDataAdapter();
                myAdapter.SelectCommand = cmd;
                myAdapter.Fill(children);

                foreach (DataRow dr in children.Tables[0].Rows)
                {
                    Route temp = new Route();
                    temp.RouteNumber = Convert.ToInt16(dr["RouteNo"]);
                    temp.UpDestination = dr["UpDestination"].ToString();
                    temp.DownDestination = dr["DownDestination"].ToString();
                    routeSummaries.Add(temp);
                }
            }
            
            return routeSummaries;
        }

    }
