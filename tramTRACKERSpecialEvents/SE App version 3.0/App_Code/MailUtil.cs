﻿using YarraTrams.Library.Mailer;
using System.Configuration;
using System;

public class MailUtil
{
    public static void SendEventCreatedNotification(int ID, string description)
    {

        string server = ConfigurationManager.AppSettings["MailServer"].ToString();
        string from = ConfigurationManager.AppSettings["EventNotifierSender"].ToString();
        string to = ConfigurationManager.AppSettings["EventNotifierReceiver"].ToString();

        string body = "Marketing has created / updated a Special Event (during event)." + Environment.NewLine + "Special Event ID: " + ID + Environment.NewLine + "Description: " + description + Environment.NewLine;

        Emailer.SendEmail(server, from, to, "tramTRACKER Special Event (during) Created / Updated", body);
    }

    public static void SendPreEventCreatedNotification(int ID, string description)
    {

        string server = ConfigurationManager.AppSettings["MailServer"].ToString();
        string from = ConfigurationManager.AppSettings["EventNotifierSender"].ToString();
        string to = ConfigurationManager.AppSettings["EventNotifierReceiver"].ToString();

        string body = "Marketing has created / updated a Special Event(Pre-event)." + Environment.NewLine + "Special Event ID: " + ID + Environment.NewLine + "Description: " + description + Environment.NewLine;

        Emailer.SendEmail(server, from, to, "tramTRACKER Special Event (Pre-event) Created / Updated", body);
    }

    public static void SendPublicEventCreatedNotification(int ID, string description)
    {

        string server = ConfigurationManager.AppSettings["MailServer"].ToString();
        string from = ConfigurationManager.AppSettings["EventNotifierSender"].ToString();
        string to = ConfigurationManager.AppSettings["EventNotifierReceiver"].ToString();

        string body = "Marketing has created / updated a Special Event(Public holiday)." + Environment.NewLine + "Special Event ID: " + ID + Environment.NewLine + "Description: " + description + Environment.NewLine;

        Emailer.SendEmail(server, from, to, "tramTRACKER Special Event (Public holiday) Created / Updated", body);
    }

    public static void SendEventPublishedNotification(int ID, string description,string published)
    {

        string server = ConfigurationManager.AppSettings["MailServer"].ToString();
        string from = ConfigurationManager.AppSettings["EventNotifierSender"].ToString();
        string to = ConfigurationManager.AppSettings["EventNotifierReceiver"].ToString();

        string body = "Marketing has Published a Special Event." + Environment.NewLine + "Special Event ID: " + ID + Environment.NewLine + "Description: " + description + Environment.NewLine + "Published by: " + published + Environment.NewLine;

        Emailer.SendEmail(server, from, to, "A new tramTRACKER Special Event has been published", body);
    }
    public static void SendEventUnPublishedNotification(int ID, string description, string unPublished)
    {

        string server = ConfigurationManager.AppSettings["MailServer"].ToString();
        string from = ConfigurationManager.AppSettings["EventNotifierSender"].ToString();
        string to = ConfigurationManager.AppSettings["EventNotifierReceiver"].ToString();

        string body = "Marketing has Unpublished a Special Event." + Environment.NewLine + "Special Event ID: " + ID + Environment.NewLine + "Description: " + description + Environment.NewLine + "unPublished by: " + unPublished + Environment.NewLine;

        Emailer.SendEmail(server, from, to, "A tramTRACKER Special Event has been unpublished", body);
    }

    public static void SendSignpostPublishedNotification(int ID, string published)
    {

        string server = ConfigurationManager.AppSettings["MailServer"].ToString();
        string from = ConfigurationManager.AppSettings["EventNotifierSender"].ToString();
        string to = ConfigurationManager.AppSettings["EventNotifierReceiver"].ToString();

        string body = "Marketing has Published a Signpost Update Change ." + Environment.NewLine + "Signpost ID: " + ID + Environment.NewLine  + Environment.NewLine + "Published by: " + published + Environment.NewLine;

        Emailer.SendEmail(server, from, to, "A new tramTRACKER Sinpost update change has been published", body);
    }
    public static void SendSignpostUnPublishedNotification(int ID, string unPublished)
    {

        string server = ConfigurationManager.AppSettings["MailServer"].ToString();
        string from = ConfigurationManager.AppSettings["EventNotifierSender"].ToString();
        string to = ConfigurationManager.AppSettings["EventNotifierReceiver"].ToString();

        string body = "Marketing has unpublished a Signpost Update Change ." + Environment.NewLine + "Signpost ID: " + ID + Environment.NewLine + "unpublished by: " + unPublished + Environment.NewLine;

        Emailer.SendEmail(server, from, to, "A new tramTRACKER Sinpost update change has been unpublished", body);
    }

    public static void SendEventMessageEditedNotification(int ID)
    {

        string server = System.Configuration.ConfigurationManager.AppSettings["MailServer"].ToString();
        string from = ConfigurationManager.AppSettings["EventNotifierSender"].ToString();
        string to = ConfigurationManager.AppSettings["EventNotifierReceiver"].ToString();

        string body = "Marketing has edited the SMS/IVR messages for Special Event ID: " + ID + Environment.NewLine;

        Emailer.SendEmail(server, from, to, "Special Event Messages updated", body);
    }
}
