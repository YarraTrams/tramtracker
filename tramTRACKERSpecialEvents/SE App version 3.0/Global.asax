﻿<%@ Application Language="C#" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        // Code that runs on application startup

    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs
        Exception ex = Server.GetLastError();
        string mailserver = ConfigurationManager.AppSettings["MailServer"].ToString();
        string receiver_config = ConfigurationManager.AppSettings["ExceptionRecipients"].ToString();
        string[] receivers = receiver_config.Split(';');
        YarraTrams.Library.Mailer.ExceptionMailer mailer = new YarraTrams.Library.Mailer.ExceptionMailer();
        YarraTrams.Library.Mailer.ExceptionMailer.EmailException("Special Event Admin", mailserver, receivers, ex);
    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
       
</script>
