﻿
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Newtonsoft.Json;
using System.Collections.Generic;

public partial class controllers_GetRoutes : System.Web.UI.Page
{
    // connection string 

    protected void Page_Load(object sender, EventArgs e)
    {
        ResponseHelper r = new ResponseHelper();
        try
        {
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();

            int _eventId = Convert.ToInt32(Request["e"]);
            bool _isup = Request["u"] == "1" ? true : false;
            List<RoutesSummary> summaries = RoutesSummary.LoadRoutesSummary(_eventId, _isup);
            r.responseObject = summaries;
        }
        catch (Exception ex)
        {
            r.isError = true;
            r.responseObject = ex;
        }
        finally
        {
            Response.Write(JsonConvert.SerializeObject(r));
        }
    }
}

