﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Newtonsoft.Json;
using System.Diagnostics;


public partial class controllers_SaveSignpostUpdateList : System.Web.UI.Page
{
    ResponseHelper r = new ResponseHelper();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            string startTme = "",endTme="";

            startTme = Request.Form["changeStartHours"] + ":" + Request.Form["changeStartMins"];
            DateTime startChangeDate = new DateTime();
            startChangeDate = Convert.ToDateTime(Request.Form["startChangeDate"].ToString() + " " + startTme);

            endTme = Request.Form["changeEndHours"] + ":" + Request.Form["changeEndMins"];
            DateTime endChangeDate = new DateTime();
            endChangeDate = Convert.ToDateTime(Request.Form["endChangeDate"].ToString() + " " + endTme);


            string selectedcode = Request.Form["selectedcode"];
            string selectedDestination = Request.Form["selectedDestination"];
            string updatedDestination = Request.Form["updatedDestination"];
            int spInfoId = Convert.ToInt32(Request.Form["spInfoID"]);

            

            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();

            SignPostUpdates sps = new SignPostUpdates();
            sps.spInfoID = Convert.ToInt32(Request.Form["spInfoID"]);
            sps.dCode =selectedcode;
            sps.existingDestination = selectedDestination;
            sps.destinationChange = updatedDestination;
            sps.changeStartDate = startChangeDate;
            sps.changeEndDate = endChangeDate;
            sps.Save();
            r.responseObject = sps;

        }
        catch (Exception ex)
        {
            r.isError = true;
            r.responseObject = ex;
        }
        finally
        {
            Response.Write(JsonConvert.SerializeObject(r));
        }
    }
}
