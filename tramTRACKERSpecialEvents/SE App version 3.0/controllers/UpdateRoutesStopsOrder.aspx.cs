﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class controllers_UpdateRoutesStopsOrder : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ResponseHelper r = new ResponseHelper();
        try{

        RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();
        SpecialEvent se = new SpecialEvent();
        se.UpdateAllEventsOrder();
        r.responseString="SUCCESS";
        r.isError = false;
        }
        catch (Exception ex)
        {
            r.responseObject = ex;
            r.isError = true;
        }
        finally
        {
            Response.Write(JsonConvert.SerializeObject(r));
        }
       
    }
}