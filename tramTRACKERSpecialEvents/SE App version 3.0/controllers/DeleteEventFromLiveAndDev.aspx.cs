﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class controllers_DeleteEventFromLiveAndDev : System.Web.UI.Page
{
    ResponseHelper r = new ResponseHelper();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            int eventID = Convert.ToInt32(Request.QueryString["e"]);

            SpecialEvent se = new SpecialEvent();
            RouteStop seRouteStop = new RouteStop();
            se.changeToInActive(eventID);
            seRouteStop.CreateWeeklySpecialEventData();
            string unPublishedBy = HttpContext.Current.User.Identity.Name;
            seRouteStop.UpdatePublishStatus(eventID, 0, unPublishedBy);
            MailUtil.SendEventUnPublishedNotification(eventID, se.GetEventDesc(eventID), unPublishedBy);
            r.isError = false;
        }
        catch (Exception ex)
        {
            r.responseObject = ex;
            r.isError = true;
        }
        finally
        {
            Response.Write(JsonConvert.SerializeObject(r));
        }
    }
}