﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Newtonsoft.Json;


public partial class controllers_SpecialEventMessageAdd : System.Web.UI.Page
{
    ResponseHelper r = new ResponseHelper();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            string smsString = Request.Form["smsText"];
            if (smsString.Length == 0)
                throw new Exception("Please enter an SMS message");
            string ivrString = Request.Form["ivrText"];
            if (ivrString.Length == 0)
                throw new Exception("Please enter an IVR message");

            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();

            MessagesSummary ms = new MessagesSummary();
            ms.EventID = Convert.ToInt32(Request.Form["eventId"]);
            ms.MessageID = Convert.ToInt32(Request.Form["msgId"]);
            ms.SMSMessage = smsString;
            ms.IVRMessage = Request.Form["ivrFN"];
            ms.FormattedIVRMessage = ivrString;
            ms.Save();
            r.responseObject = ms;

        }
        catch (Exception ex)
        {
            r.isError = true;
            r.responseObject = ex;
        }
        finally
        {
            Response.Write(JsonConvert.SerializeObject(r));
        }
    }
}
