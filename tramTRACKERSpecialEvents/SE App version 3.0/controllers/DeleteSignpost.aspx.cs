﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Xml.Linq;
using Newtonsoft.Json;

public partial class controllers_DeleteSignpost : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        ResponseHelper r = new ResponseHelper();
        SignPostSummary sp = new SignPostSummary();
        try
        {
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();
           
            int sID = Convert.ToInt32(Request.QueryString["sp"]);

            string deletedBy = HttpContext.Current.User.Identity.Name;
            if (deletedBy.Length == 0)
                deletedBy = "";
            sp.DeleteSignPost(sID, deletedBy);
            r.responseObject = sp;
        }
        catch (Exception ex)
        {
            r.isError = true;
            r.responseObject = ex;
        }
        finally 
        {
            Response.Write(JsonConvert.SerializeObject(r));
        }
        
    }
}
