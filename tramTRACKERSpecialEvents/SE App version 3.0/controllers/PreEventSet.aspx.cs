﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Xml.Linq;
using Newtonsoft.Json;
using System.Diagnostics;
public partial class controllers_PreEventSet : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ResponseHelper r = new ResponseHelper();

        try
        {
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();

            // server Validation
            if (Request.Form["txtdescription"] == null || Request.Form["txtdescription"] == "")
            {
                throw new Exception("Please supply an Event description");
            }
            if (Request.Form["startdate"] == null || Request.Form["startdate"] == "")
                throw new Exception("Start date cannot be blank");

            if (Request.Form["enddate"] == null || Request.Form["enddate"] == "")
                throw new Exception("End date cannot be blank");

            string tme = "";

            if (Request.Form["stHr"] == null || Request.Form["stHr"] == "HH")
                throw new Exception("Please enter a start time");

            if (Request.Form["stMn"] == null || Request.Form["stMn"] == "MM")
                throw new Exception("Please enter a start time");

            tme = Request.Form["stHr"] + ":" + Request.Form["stMn"];
            DateTime StartDate = new DateTime();

            StartDate = Convert.ToDateTime(Request.Form["startdate"].ToString() + " " + tme);


            tme = Request.Form["endHr"] + ":" + Request.Form["enMn"];
            DateTime EndDate = new DateTime();


            EndDate = Convert.ToDateTime(Request.Form["enddate"].ToString() + " " + tme);

            if (DateTime.Now >= StartDate)
                throw new Exception("Start date cannot earlier than today");

            if (DateTime.Now.AddDays(8) < StartDate)
                throw new Exception("Start date cannot be later than Seven days from today");

            if (StartDate > EndDate)
                throw new Exception("Please ensure the Start date and time is earler than the end date and time");

            if (Request.Form["txtIVR"] == null || Request.Form["txtIVR"] == "")
            {
                throw new Exception("Please supply IVR message");
            }

            if (Request.Form["txtSMS"] == null || Request.Form["txtSMS"] == "")
            {
                throw new Exception("Please supply SMS message");
            }

            if (Request.Form["txtSMS"].Length > 117)
            {
                throw new Exception("The SMS message cannot exceed 117 characters");
            }

            if (Request.Form["selectedRoute"] == null ||Request.Form["selectedRoute"] =="")
            {
                throw new Exception("Please ensure to select atleast one route"); 
            }

            if (Request.Form["ivrFileName"] == null || Request.Form["ivrFileName"] == "")
            {
                throw new Exception("Please ensure to generate the voice file"); 
            }
            
            
            PreEvent pe = new PreEvent();

            if (Request.Form["eventId"] != null && Request.Form["eventId"].Length > 0)
            {
                pe.EventID = Convert.ToInt32(Request.Form["eventId"]); 
            }

            else
            {
                pe.EventID = 0;
            }
                
            pe.CreatedBy = HttpContext.Current.User.Identity.Name;
            bool x = User.Identity.IsAuthenticated; //true
            Debug.WriteLine("x : " + Request.ServerVariables[5].ToString());
            Debug.WriteLine("created by : " + HttpContext.Current.User.Identity.Name);
            pe.EventDesc = Request.Form["txtdescription"];
            pe.EventStartDate = StartDate;

            pe.EventEndDate = EndDate;
            pe.SMSMessage = Request.Form["txtSMS"];
            pe.IVRMessage = Request.Form["txtIVR"];
            String selectedRoute = Request.Form["selectedRoute"];
            pe.affectedRouteNo = selectedRoute.Replace("3a", "4");
            pe.IVRFileName = Request.Form["ivrFileName"];
            Debug.WriteLine("pe.IVRFileName :" + pe.IVRFileName);
            Debug.WriteLine("Calling Save()");
            pe.Save();
            
            pe.AddPreEventsForAffectedRoutes();
            Debug.WriteLine("Calling AddPreEventsForAffectedRoutes()");

            MailUtil.SendPreEventCreatedNotification(pe.EventID, pe.EventDesc);
            r.responseObject = pe;
           

        }
        catch (Exception ex)
        {
            r.isError = true;
            r.responseString = ex.Message;
        }
        finally
        {
            Response.Write(JsonConvert.SerializeObject(r));
        }

    }
}