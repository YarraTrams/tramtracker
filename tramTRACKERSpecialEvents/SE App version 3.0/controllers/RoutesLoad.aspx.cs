﻿using System;
using System.Collections;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Xml.Linq;
using Newtonsoft.Json;

public partial class controllers_RoutesLoad : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();
        Response.Write(JsonConvert.SerializeObject(Route.LoadAllMainRoutes()));
    }
}
