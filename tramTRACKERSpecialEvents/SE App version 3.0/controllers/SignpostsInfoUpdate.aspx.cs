﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Xml.Linq;
using Newtonsoft.Json;

public partial class controllers_SignpostsInfoUpdate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        ResponseHelper r = new ResponseHelper();
        SignPostSummary sp = new SignPostSummary();
        try
        { 
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();
            int sid = Convert.ToInt32(Request["sp"]);
            string _spDescription = Request["spDescription"].ToString();
            string modifiedBy = HttpContext.Current.User.Identity.Name;
            if (modifiedBy.Length == 0)
                modifiedBy = "";
            int sID = sp.UpdateSignPostInfo(sid,_spDescription, modifiedBy);
            r.responseObject = sID;
        }
        catch (Exception ex)
        {
            r.isError = true;
            r.responseObject = ex;
        }
        finally 
        {
            Response.Write(JsonConvert.SerializeObject(r));
        }
        
    }
}
