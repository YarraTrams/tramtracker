﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Xml.Linq;
using Newtonsoft.Json;
using System.Diagnostics;


public partial class controllersSpecialEventSet : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ResponseHelper r = new ResponseHelper();

        try
        {
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();

            // validation
            if (Request.Form["txtdescription"] == null || Request.Form["txtdescription"] == "")
            {
                throw new Exception("Please supply an Event description");
            }
            Debug.WriteLine("checkHoliday: " + Request.Form["checkHoliday"]);
            if (Request.Form["checkHoliday"] == "true")
            {
                if (Request.Form["txtIVR"] == null || Request.Form["txtIVR"] == "")
                {
                    throw new Exception("Please supply a Public holiday IVR message");
                }

                if (Request.Form["ivrFile"] == null || Request.Form["ivrFile"] == "")
                {
                    throw new Exception("Please generate voice file for IVR message");
                }

                if (Request.Form["txtSMS"] == null || Request.Form["txtSMS"] == "")
                {
                    throw new Exception("Please supply a Public holiday SMS message");
                }

                if (Request.Form["txtSMS"].Length>117)
                {
                    throw new Exception("The Public holiday SMS message cannot exceed 117 characters");
                }
            
            }

            if (Request.Form["startdate"] == null || Request.Form["startdate"]=="")
                throw new Exception("Start date cannot be blank");

            if (Request.Form["enddate"] == null || Request.Form["enddate"] == "")
                throw new Exception("End date cannot be blank");

            string tme = "";

            if (Request.Form["stHr"] == null || Request.Form["stHr"] == "HH")
                throw new Exception("Please enter a start time");

            if (Request.Form["stMn"] == null || Request.Form["stMn"] == "MM")
                throw new Exception("Please enter a start time");

            tme = Request.Form["stHr"] + ":" + Request.Form["stMn"];
            DateTime StartDate = new DateTime();
            
            StartDate = Convert.ToDateTime(Request.Form["startdate"].ToString() + " " + tme);
                        

            tme = Request.Form["endHr"] + ":" + Request.Form["enMn"];
            DateTime EndDate = new DateTime();


            EndDate = Convert.ToDateTime(Request.Form["enddate"].ToString() + " " + tme);

            if (DateTime.Now >= StartDate)
                throw new Exception("Start date cannot earlier than today");

            if (DateTime.Now.AddDays(8) < StartDate)
                throw new Exception("Start date cannot be later than Seven days from today");
            
            if (StartDate > EndDate)
                throw new Exception("Please ensure the Start date and time is earler than the end date and time");

            SpecialEvent se = new SpecialEvent();
            if (Request.Form["eventId"] != null && Request.Form["eventId"].Length > 0)
                se.EventID = Convert.ToInt32(Request.Form["eventId"]);
            else
                se.EventID = 0;

            se.CreatedBy = HttpContext.Current.User.Identity.Name;
            if (se.CreatedBy.Length==0)
                se.CreatedBy = "";
            se.EventDesc = Request.Form["txtdescription"];
            se.EventStartDate = StartDate;
            se.EventEndDate = EndDate;

            if (Request.Form["checkHoliday"] == "true")
            {
                se.IsPublicHoliday = true;
                se.SMSHolidayMessage = Request.Form["txtSMS"];
                se.IVRHolidayMessage = Request.Form["txtIVR"];
                se.IVRFileName = Request.Form["ivrFile"];
                se.Save();

                MailUtil.SendPublicEventCreatedNotification(se.EventID, se.EventDesc);
            }
            else
            {
                se.Save();
                MailUtil.SendEventCreatedNotification(se.EventID, se.EventDesc);
            }
           
            r.responseObject = se;

        }
        catch (Exception ex)
        {
            r.isError = true;
            r.responseString = ex.Message;
        }
        finally
        {
            Response.Write(JsonConvert.SerializeObject(r));
        }

    }
}
