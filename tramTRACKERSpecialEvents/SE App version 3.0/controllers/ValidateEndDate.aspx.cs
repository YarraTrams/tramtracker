﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using System.Diagnostics;

public partial class controllers_ValidateEndDate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ResponseHelper r = new ResponseHelper();

        try
        {       
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();

            SignPostSummary sp = new SignPostSummary();

            int spInfoID = Convert.ToInt32(Request.QueryString["sp"]);
            bool valid=sp.CheckEndDateValid(spInfoID);
            r.responseObject = valid;
        }
        catch (Exception ex)
        {
            r.isError = true;
            r.responseObject = ex;
        }
        finally
        {
            Response.Write(JsonConvert.SerializeObject(r));
        }
    }
}