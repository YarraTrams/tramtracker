﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Xml.Linq;
using Newtonsoft.Json;

public partial class controllers_AffectedMultipleStopsSave : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        ResponseHelper r = new ResponseHelper();
        Stop s = new Stop();
        try
        {
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();

            int eventID = Convert.ToInt32(Request["e"].ToString());
            int routeNo = Convert.ToInt16(Request["r"].ToString());
            string[] stopNumbers = Request["s"].ToString().Split(new string[]{","}, StringSplitOptions.RemoveEmptyEntries);

            foreach (var item in stopNumbers)
            {
                int stopNo = Convert.ToInt16(item);
                bool isUpstop = Request["d"] == "true";

                if (Request["on"] == "true")
                {
                    s.EventID = eventID;
                    s.RouteNumber = routeNo;
                    s.TrackerID = Convert.ToInt32(stopNo);
                    s.IsUpStop = isUpstop;
                    s.TramTrackerAvailable = false;
                    s.SaveSpecialEventStopDetails();
                    s.LoadMissingRoutes();
                }
                else if (Request["on"] == "false")
                {
                    s.EventID = eventID;
                    s.RouteNumber = routeNo;
                    s.TrackerID = Convert.ToInt32(stopNo);
                    s.IsUpStop = isUpstop;
                    s.DeleteSpecialeventStopDetails();

                }
            }

            r.responseObject = s;
        }
        catch (Exception ex)
        {
            r.isError = true;
            r.responseObject = ex;
        }
        finally 
        {
            Response.Write(JsonConvert.SerializeObject(r));
        }
        
    }
}
