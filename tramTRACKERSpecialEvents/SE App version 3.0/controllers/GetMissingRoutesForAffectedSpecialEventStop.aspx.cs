﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

public partial class controllers_GetMissingRoutesForAffectedSpecialEventStop : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ResponseHelper r = new ResponseHelper();
        Stop s = new Stop();
        try
        {
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();

            int eventID = Convert.ToInt32(Request["e"].ToString());
            int stopNo = Convert.ToInt16(Request["s"].ToString());

            s.EventID = eventID;
            s.TrackerID = stopNo;

            List<string> missingRtes = s.GetMissingRoutesForSpecialEvent();
            
            r.responseObject = missingRtes;
        }
        catch (Exception ex)
        {
            r.isError = true;
            r.responseObject = ex;
        }
        finally
        {
            Response.Write(JsonConvert.SerializeObject(r));
        }

    }
}
