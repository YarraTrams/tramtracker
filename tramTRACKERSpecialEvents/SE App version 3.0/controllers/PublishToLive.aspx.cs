﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class controllers_PublishToLive : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ResponseHelper r = new ResponseHelper();
        try
        {
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();
            int eventID = Convert.ToInt32(Request.QueryString["e"]);

            SpecialEvent se = new SpecialEvent();
            se.ReadyToPublish(eventID);
            deleteRecordAccordingPriority();
            publishToLiveDB(eventID);
            MailUtil.SendEventPublishedNotification(se.EventID, se.GetEventDesc(eventID), HttpContext.Current.User.Identity.Name);
            r.isError = false;
            r.responseString = "SUCCESS";
        }
        catch (Exception ex)
        {
            r.responseObject = ex;
            r.isError = true;
            r.responseString = "FAILED";
        }
        finally
        {
            Response.Write(JsonConvert.SerializeObject(r));
        }
    }

    private void deleteRecordAccordingPriority()
    {
        RouteStop seRouteStop = new RouteStop();
        seRouteStop.deleteRouteStopsFromLive();
    }

    private void publishToLiveDB(int eventID)
    {

            RouteStop seRouteStop = new RouteStop();
            seRouteStop.exportRouteStopsToLiveBySpecialEventID(eventID);
            seRouteStop.exportMessagesToLiveBySpecialEventID(eventID);
            seRouteStop.publishEventsThatBeenDeleted();
            seRouteStop.CreateWeeklySpecialEventData();
            string publishedBy=HttpContext.Current.User.Identity.Name;
            seRouteStop.UpdatePublishStatus(eventID, 1, publishedBy);
        
    }
}