﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Newtonsoft.Json;
using System.Collections.Generic;

public partial class controllers_GetSpecialEventMessages : System.Web.UI.Page
{
    // connection string 

    protected void Page_Load(object sender, EventArgs e)
    {
        ResponseHelper r = new ResponseHelper();
        try
        {
            int _eventid = Convert.ToInt32(Request["e"]);
            short _stopno = Convert.ToInt16(Request["s"]);
            short _routeno = Convert.ToInt16(Request["r"]);

            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();

            List<MessagesSummary> summaries = MessagesSummary.LoadMessagesSummary(_eventid, _stopno, _routeno);

            r.responseObject = summaries;


        }
        catch (Exception ex)
        {
            r.isError = true;
            r.responseObject = ex;
        }
        finally
        {
            Response.Write(JsonConvert.SerializeObject(r));
        }
        
    }
}

