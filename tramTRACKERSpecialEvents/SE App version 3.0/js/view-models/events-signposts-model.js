﻿var my = my || {};

$(function () {

    my.Event = function (data) {



        this.eventId = data.EventID;
       
        this.startDate = ko.computed(function () {
            var startDate = new Date(parseInt(data.EventStartDate.substr(6)));
            return dateFormat(startDate, "dddd, d mmmm yyyy HH:MM");
        });

        this.endDate = ko.computed(function () {
            var endDate = new Date(parseInt(data.EventEndDate.substr(6)));
            return dateFormat(endDate, "dddd, d mmmm yyyy HH:MM");
        });

        this.eventSummaryUrl = ko.computed(function () {
            return "SpecialEventSummary.html?e=" + data.EventID;
        });

        this.editEventUrl = ko.computed(function () {
            return "specialeventcreate.step1.html?e=" + data.EventID;
        });

        this.editStopsUrl = ko.computed(function () {
            return "specialeventcreate.step2.html?e=" + data.EventID;
        });

        this.editMessagesUrl = ko.computed(function () {
            return "specialeventcreate.step3.html?e=" + data.EventID;
        });

        this.editPreEventUrl = ko.computed(function () {
            return "createPreEvent.html?e=" + data.EventID;
        });


        this.eventDescription = ko.observable(data.EventDesc);
        this.createdBy = ko.observable(data.CreatedBy);
        this.lastUpdatedBy = ko.observable(data.UpdatedBy);
        this.sortorder = ko.observable(data.sortorder);
        this.isPublicHoliday = ko.observable(data.IsPublicHoliday);
        this.isPreEvent = ko.observable(data.IsPreEvent);
        this.isPublished = ko.observable(data.IsPublished);
        this.affectedRouteList = ko.observable(data.AffectedRouteList);
        this.lastPublishedBy = ko.observable(data.LastUnPublishedBy);
        this.lastUnPublishedBy = ko.observable(data.LastUnPublishedBy);
        this.lastUpdate = ko.computed(function () {
            var updatedDate = new Date(parseInt(data.LastUpdate.substr(6)));
            return dateFormat(updatedDate, "dddd, mmmm d, yyyy HH:MM");
        });


        this.lastPublishedUpdate = ko.computed(function () {
            var updatedDate = new Date(parseInt(data.LastPublishedUpdate.substr(6)));
            return dateFormat(updatedDate, "dddd, mmmm d, yyyy HH:MM");
        });


        this.lastUnPublishedUpdate = ko.computed(function () {
            var updatedDate = new Date(parseInt(data.LastUnPublishedUpdate.substr(6)));
            if (dateFormat(updatedDate, "yyyy") == "1")
                return " -";
            else
                return dateFormat(updatedDate, "dddd, mmmm d, yyyy HH:MM");
        });

        this.lastUpdate = ko.computed(function () {
            var updatedDate = new Date(parseInt(data.LastUpdate.substr(6)));
            return dateFormat(updatedDate, "dddd, mmmm d, yyyy HH:MM");
        });



    };
    my.scheduleChange = function (data) {
        var self = this;
        self.sID = data.spInfoID;
        self.signPostDescription = data.spDescription;
        self.createdBy = data.createdBy;
        self.modifiedBy = data.modifiedBy;
        self.createdDate = dateFormat(new Date(parseInt(data.createdDate.substr(6))),"dddd, mmmm d, yyyy HH:MM");
        self.modifiedDate = dateFormat(new Date(parseInt(data.modifiedDate.substr(6))), "dddd, mmmm d, yyyy HH:MM");
        self.publishedDate = dateFormat(new Date(parseInt(data.publishedDate.substr(6))), "dd/mm/yyyy HH:MM");
        self.publishedBy = data.publishedBy;
        self.unpublishedBy = ko.computed(function () {
            if (data.unpublishedBy == null || data.unpublishedBy =="") {
                return "none";
            }
            else {
                return data.unpublishedBy;
            }
        });

        this.spSummaryUrl = ko.computed(function () {
            return "signpostSummary.html?spID=" + self.sID;
        });
        this.spEditUrl = ko.computed(function () {
            return "editSignpostList.html?spID=" + self.sID;
		});
        self.isSignpostPublished = data.isPublished;
        self.unpublishedDate = ko.computed(function () {
            var updatedDate = new Date(parseInt(data.unpublishedDate.substr(6)));
            if (dateFormat(updatedDate, "yyyy") == "1")
                return " -";
            else
                return dateFormat(updatedDate, "dd/mm/yyyy HH:MM");
        });

       
   
       
    };
    my.SignPosts = function (d) {
        var self = this;
        self.destinationCode = ko.observable(d.DestinationCode);
        self.destination = ko.observable(d.Destination);
    };
    my.SignpostData = function (_spInfoID,_startChangeDate, _changeStartHours, _changeStartMins, _changeEndHours, _changeEndMins, _endChangeDate, _selectedSignPosts, _selectedDestination, _updatedDestination) {
        var self = this;
        self.spInfoID = _spInfoID;
        self.startChangeDate = _startChangeDate;
        self.changeStartHours = _changeStartHours;
        self.changeStartMins = _changeStartMins;
        self.changeEndHours = _changeEndHours;
        self.changeEndMins = _changeEndMins;
        self.endChangeDate = _endChangeDate;
        self.selectedcode = _selectedSignPosts;
        self.selectedDestination = _selectedDestination;
        self.updatedDestination = _updatedDestination;



    };

    my.SignpostFormElement = function () {
        var self = this;
        self.startChangeDate = ko.observable(new Date());
        self.changeHours = ko.computed(function () {
            var hoursArray = ko.observableArray([]);
            for (var h = 0; h < 24; h++) {
                if (h < 10)
                    h = "0" + h;
                hoursArray.push(h);
            }
            return hoursArray;
        });
        self.changeMins = ko.computed(function () {
            var minsArray = ko.observableArray([]);

            for (var m = 0; m < 60; m += 5) {
                var minString = "";
                if (m < 10)
                    minString = "0" + m;
                else
                    minString = m;
                minsArray.push(minString);
            }
            return minsArray;
        }),
        self.changeStartHours = ko.observable();
        self.changeStartMins = ko.observable();
        self.changeEndHours = ko.observable();
        self.changeEndMins = ko.observable();
        self.endChangeDate = ko.observable(new Date().addDays(14));
        self.selectedSignPosts = ko.observable();
        self.selectedDestination = ko.computed(function () {
            var sp = self.selectedSignPosts();
            if (sp) { return sp.destination(); }
        });
        self.selectedcode = ko.computed(function () {
            var sp = self.selectedSignPosts();
            if (sp) { return sp.destinationCode(); }
        });
        self.updatedDestination = ko.observable('');
        
       
        
    };

    my.eventListVM = (function () {
        
       
        this.signpostLists = ko.observableArray([]);
        this.scheduleInfos = ko.observableArray([]);
        this.testUrl = ko.observable("http://buzzapps/SpecialEventTestPidApp");
        this.ytUrl = ko.observable("http://www.yarratrams.com.au/");
       

        this.removeRow = function (data) {
            signpostFormElements.remove(data);
        };
        
        //SAVE THE SIGNPOST CHANGE
        this.scheduleSignpost = function () {
            validationErrors.removeAll();
            var index = 0, ref = 0, todaysDate = new Date();//.format("yyyy-mm-ddTHH:MM"); //todaysHour = new Date().getHours(), todaysMins = new Date().getMinutes();
            //todaysDate = new Date(todaysDate + "T" + todaysHour + ":" + todaysMins);
            //alert(signpostFormElements().length);
            if (signpostDescription() == null) {
                validationErrors.push("The description is required.");
                $('#spName').focus().css("background-color", "#f2dede");
                setTimeout(function () {
                    $('#spName').focus().css("background-color","White");
                }, 10000);
            }
            if (signpostFormElements().length == 0) {
                validationErrors.push("Please add atleast one signpost change.");
            }
            ko.utils.arrayForEach(signpostFormElements(), function (item) {
                ref = ref + 1;
                var cStartDateTime =new Date(item.startChangeDate().format("yyyy-mm-dd") + "T" + item.changeStartHours() + ":" + item.changeStartMins());
                var cEndDateTime = new Date(item.endChangeDate().format("yyyy-mm-dd") + "T" + item.changeEndHours() + ":" + item.changeEndMins());
                //alert(todaysDate);
                //alert(cStartDateTime);
                //alert(cEndDateTime);
                
                if (item.changeStartHours() == null) {
                    validationErrors.push("The Change Start Time 'Hour' is required.Check the row no:"+ref);
                }
                if (item.changeStartMins() == null) {

                    validationErrors.push("The Change Start Time 'Minute' is required.Check the row no:" + ref);
                }
                if (item.changeEndHours() == null) {
                    validationErrors.push("The Change End Time 'Hour' is required.Check the row no:" + ref);
                }
                if (item.changeEndMins() == null) {
                    validationErrors.push("The Change End Time 'Minute' is required.Check the row no:" + ref);
                }

                if (item.selectedcode() == null) {
                    validationErrors.push("Please Choose the Signpost code.Check the row no:" + ref);
                }
                if (item.updatedDestination() == "") {
                    validationErrors.push("The Destination Change is required.Check the row no:" + ref);
                }
                if (todaysDate >= cStartDateTime)
                    validationErrors.push("Start change date cannot be earlier than today.Check the row no:" + ref);

                if (cStartDateTime > cEndDateTime)
                    validationErrors.push("Please ensure the start change date and time cannot be earlier than the end change date and time.Check the row no:" + ref);

                if (cStartDateTime.getTime() == cEndDateTime.getTime())
                    validationErrors.push("Please ensure the start change date and time could not be same as the end change date and time.Check the row no:" + ref);

                if (item.changeStartHours() == null || item.changeStartMins() == null || item.changeEndHours() == null || item.changeEndMins() == null || item.selectedcode() == null || item.updatedDestination() == "" || todaysDate >= cStartDateTime || cStartDateTime > cEndDateTime || cStartDateTime.getTime() == cEndDateTime.getTime()) {
                    $("#mySPtable tr").eq(index).css("background-color", "#f2dede");
                }
                index = index + 1;

                //var sp = new my.SignpostData(item.startChangeDate(), item.changeStartHours(), item.changeStartMins(), item.changeEndHours(), item.changeEndMins(), item.endChangeDate(), item.selectedSignPosts(), item.selectedDestination(), item.updatedDestination());
            });
            if (validationErrors().length == 0) {
                my.dataService.savesignPostsInfo(signpostDescription()).done(function (data) {
                     if (!data.isError) {
                       var id = data.responseObject;
                            ko.utils.arrayForEach(signpostFormElements(), function (item) {
                                var formatedChangeStartDate = dateFormat(item.startChangeDate(), "dd-mm-yyyy");
                                var formatedChangeEndDate = dateFormat(item.endChangeDate(), "dd-mm-yyyy");

                                var spList = new my.SignpostData(id, formatedChangeStartDate, item.changeStartHours(), item.changeStartMins(), item.changeEndHours(), item.changeEndMins(), formatedChangeEndDate, item.selectedcode(), item.selectedDestination(), removeMsWordChars(item.updatedDestination()));
                                my.dataService.savesignPostSchedule(spList).done(function (response) {
                                   var result = JSON.parse(response);

                                   if (result.isError) {
                                        $('#enterAllErrorMsgAlert').html("<div class='alert alert-danger'>" + result.responseObject.Message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span> </div>");
                                        $('#enterAllErrorMsgAlert').show();
                                        setTimeout(function () {
                                            $('#enterAllErrorMsgAlert').hide();
                                        }, 8000);
                                   }
                                });

                         });
                         resetTheSchedulePage();
                         getSPScheduleList();
                         

                       
                     }
                    else {
                        $('#enterAllErrorMsgAlert').html("<div class='alert alert-danger'>" + data.responseObject.Message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span> </div>");
                        $('#enterAllErrorMsgAlert').show();
                        setTimeout(function () {
                            $('#enterAllErrorMsgAlert').hide();
                        }, 8000);
                    }
                });
               
            }
            else {
                setTimeout(function () {
                    $('#mySPtable tr').css('background-color', 'white');
                }, 10000);


            }
            

        };

        var addMoreSignpostForm = function () {
            signpostFormElements.push(new my.SignpostFormElement());
            },
            startDate = ko.observable(new Date()),
            endDate = ko.observable(new Date().addDays(14)),
            hours = ko.computed(function () {
                var hoursArray = ko.observableArray([]);
                for (var h = 0; h < 24; h++) {
                    if (h < 10)
                        h = "0" + h;
                    hoursArray.push(h);
                }
                return hoursArray;
            }),
            
            signpostDescription = ko.observable(),
            signpostFormElements = ko.observableArray([new my.SignpostFormElement()]),
            mins = ko.computed(function () {
             var minsArray = ko.observableArray([]);

             for (var m = 0; m < 60; m += 5) {
                 var minString = "";
                 if (m < 10)
                     minString = "0" + m;
                 else
                     minString = m;
                 minsArray.push(minString);
             }
             return minsArray;
        }),
            
        validationErrors = ko.observableArray([]);


        getSignPostLists = function () {
            my.dataService.getAllSignPostList().done(function (data) {
                if (data.responseObject) {
                    var mappedSP = $.map(data.responseObject, function (item) {
                        return new my.SignPosts(item);
                    });
                    signpostLists(mappedSP);
                }

            })
        },

        resetTheSchedulePage = function () {
            $("#signpostform")[0].reset();
            signpostFormElements.removeAll();
            validationErrors.removeAll();
            $('#spName').focus().css("background-color", "white");
            signpostFormElements.push(new my.SignpostFormElement());
            $('#openSignPostchangeModal').modal('hide');
        }

        
       
        sPForUnschedule = ko.observable(),
        setSPForUnschedule= function (sid) {
               sPForUnschedule(sid);
        },


        setSpDelete = function (signpost) {
            //alert(signpost.sID);
            sPToDelete(signpost.sID);
        };
        sPToDelete = ko.observable();

        deleteSpecificSignpost = function () {
            $('#deleteSpecificSPModal').modal('hide');
            
            my.dataService.deleteSignPost(sPToDelete())
                .done(function (data) {
                    if (!data.isError) {
                        $('#enterAllErrorsAlert').html("<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Successfully deleted !!</div>");
                        my.eventListVM.getSPScheduleList();
                    }
                    else {
                        $('#enterAllErrorsAlert').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + data.responseObject.Message + "</div>");
                    }

                    $('#enterAllErrorsAlert').show();
                    setTimeout(function () {
                        $('#enterAllErrorsAlert').hide();
                    }, 8000);
                    $('#enterAllErrorsAlert').modal('hide');


           });
            
        },

        /*
        * Remove special characters from text input.
        */
        removeMsWordChars = function (text) {
            var s = text;

            if (s) {
                // smart single quotes and apostrophe
                s = s.replace(/[\u2018\u2019\u201A]/g, "\'");
                // smart double quotes
                s = s.replace(/[\u201C\u201D\u201E]/g, "\"");
                // ellipsis
                s = s.replace(/\u2026/g, "...");
                // dashes
                s = s.replace(/[\u2013\u2014]/g, "-");
                // circumflex
                s = s.replace(/\u02C6/g, "^");
                // open angle bracket
                s = s.replace(/\u2039/g, "<");
                // close angle bracket
                s = s.replace(/\u203A/g, ">");
                // spaces
                s = s.replace(/[\u02DC\u00A0]/g, " ");
                // enter or newline
                s = s.replace(/\u000a/g, "");
            }
            return s;
        },

        getSPScheduleList = function () {
            my.dataService.getSPScheduleInfo()
                                .done(function (data) {
                                    if (!data.isError) {
                                        var mappedscheduleInfo = $.map(data.responseObject, function (item) {
                                            //alert((JSON.stringify(item)));
                                            return new my.scheduleChange(item);

                                        });

                                        scheduleInfos(mappedscheduleInfo);


                                    }
                                   

                                });

        };

       

       
        currentYear = ko.computed(function () {
            return new Date().getFullYear();
        });

       
        return {
            hours: hours,
            mins: mins,
            currentYear: currentYear,
            getSignPostLists: getSignPostLists,
            addMoreSignpostForm: addMoreSignpostForm,
            signpostFormElements: signpostFormElements,
            signpostDescription: signpostDescription,
            validationErrors: validationErrors,
            getSPScheduleList: getSPScheduleList,
            scheduleInfos: scheduleInfos
            

        }

    })();

    ko.validation.configure({
        registerExtenders: true,
        messagesOnModified: true,
        insertMessages: true,
        parseInputAttributes: true,
        messageTemplate: null
    });
    
   

    ko.bindingHandlers.bootstrapPopover = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var options = valueAccessor();
            var defaultOptions = {};
            options = $.extend(true, { trigger: "hover" }, defaultOptions, options);
            $(element).popover(options);
        }
    };

   
    my.eventListVM.getSPScheduleList();
    ko.applyBindings(my.eventListVM);
   
    ko.bindingHandlers.bootstrapPopover = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var options = valueAccessor();
            var defaultOptions = {};
            options = $.extend(true, { trigger: "hover" }, defaultOptions, options);
            $(element).popover(options);
        }
    };

    

});