﻿var my = my || {};


$(function () {
   
    my.SpecialEventMessage = function (message) {
        var _this = this;
        _this.routeNumbers = ko.observable(message.RouteNumbers);
        _this.smsMessage = ko.observable(message.SMSMessage);
        _this.formattedIVRMessage = ko.observable(message.FormattedIVRMessage);
        _this.messageID = ko.observable(message.MessageID);
        _this.ivrFileName = ko.observable(message.IVRMessage);
        _this.smsMsgCount = ko.computed(function () 
        {
            var countNum = this.smsMessage().length;
            return 117 - _this.smsMessage().length;
        }, this);

       
    };

    



    my.MessageData = function (eventid, msgId, smsText, ivrText, ivrFN) {
        this.eventid = eventid;
        this.msgId = msgId;
        this.smsText = smsText;
        this.ivrText = ivrText;
        this.ivrFN = ivrFN;
    };

    my.enterMessagesVM = (function () {
       
        
        var eventId = ko.observable(''),
        rawStartDate = ko.observable(''),
        rawEndDate = ko.observable(''),
        eventDescription = ko.observable(),
        selectedMessage = ko.observable(),
        duplicateMsgChecked = ko.observable(),
        selectedDuplicateMessageOption = ko.observable(),
        routes = ko.observableArray([]),
        unAffectedMessages = ko.observableArray([]),
        affectedMessages = ko.observableArray([]),
        affectedShuttleMessages = ko.observableArray([]),
        duplicateMessageOptions = ko.observableArray(['All Messages', 'All Affected', 'All Unaffected']),
        smsMsgCount = ko.observableArray([]),
        
       

        startDate = ko.computed(function () {
            if (rawStartDate()) {
                var parsedStartDate = new Date(parseInt(rawStartDate().substr(6)));
                return dateFormat(parsedStartDate, "dddd, mmmm d, yyyy HH:MM");
            }
        });

        
        endDate = ko.computed(function () {
            if (rawEndDate()) {
                var parsedEndDate = new Date(parseInt(rawEndDate().substr(6)));
                return dateFormat(parsedEndDate, "dddd, mmmm d, yyyy HH:MM");
            }
        });

        // getUnaffectedMessages(): get the unaffected messages by passing the boolean values (isAffectedStops,isShuttleTrams)

        getUnaffectedMessages = function (affectedStops, shuttleTrams) {
            my.dataService.getTempSpecialEventMessage(eventId(), affectedStops, shuttleTrams)
                        .done(function (data) {
                            if (data.responseObject) {
                                var mappedMessages = $.map(data.responseObject, function (item) {
                                    return new my.SpecialEventMessage(item);
                                });

                                unAffectedMessages(mappedMessages);
                            }
                        });
        },


        // getAffectedMessages(): get the affected messages by passing the boolean values (isAffectedStops,isShuttleTrams)

        getAffectedMessages = function (affectedStops, shuttleTrams) {
            my.dataService.getTempSpecialEventMessage(eventId(), affectedStops, shuttleTrams)
            .done(function (data) {
                if (data.responseObject) {
                    var mappedMessages = $.map(data.responseObject, function (item) {
                        return new my.SpecialEventMessage(item);
                    });

                    affectedMessages(mappedMessages);
                }
            });
        },

        //currently not using this functionality

        getAffectedShuttleMessages = function (affectedStops, shuttleTrams) {
            my.dataService.getTempSpecialEventMessage(eventId(), affectedStops, shuttleTrams)
            .done(function (data) {
                if (data.responseObject) {
                    var mappedMessages = $.map(data.responseObject, function (item) {
                        return new my.SpecialEventMessage(item);
                    });

                    affectedShuttleMessages(mappedMessages);
                }
            });
        },

        // backToStopsClicked(): got back to editing stops page

        backToStopsClicked = function () {
            window.location = "specialeventcreate.step2.html?e=" + eventId();
        },

        //finishEventClicked(): Finalise the event

        finishEventClicked = function () {
           
            $("#finishMsgButton").text("Finishing...");
            
            var affectedSMSMsgCount = $('.hasSMSMsg').length;
            var affectedIVRMsgCount = $('.hasIVRMsg').length;
            var totalAffectedMsg = affectedMessages().length;

            var unAffectedSMSMsgCount = $('.hasUnaffectedSMSMsg').length;
            var unAffectedIVRMsgCount = $('.hasUnaffectedIVRMsg').length;
            var totalUnaffectedMsg = unAffectedMessages().length;
            var goFinalisingAffectedMsg = 0;
            var goFinalisingUnaffectedMsg = 0;

            if ((affectedSMSMsgCount == totalAffectedMsg) && (affectedIVRMsgCount == totalAffectedMsg)) {
                goFinalisingAffectedMsg = 1;

            }

            if ((unAffectedSMSMsgCount == totalUnaffectedMsg) && (unAffectedIVRMsgCount == totalUnaffectedMsg)) {
                goFinalisingUnaffectedMsg = 1;
            }

            if (goFinalisingAffectedMsg == 1 && goFinalisingUnaffectedMsg == 1) {
                $('#processingMsgModal').modal('show');
                my.dataService.finshSpecialEvent(eventId())
                   .done(function (data) {
                       if (!data.isError) {
                           window.location = "SpecialEventsummary.html?e=" + eventId();
                       }
                       $("#finishMsgButton").html("Finish <span class='glyphicon glyphicon-ok-sign'></span>");
                       $('#processingMsgModal').modal('hide');
                   });
               
                
            }
            else {
                $("#finishMsgButton").html("Finish <span class='glyphicon glyphicon-ok-sign'></span>");
                $('#enterAllMsgAlert').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Please enter all the messages</div>");
                $('#enterAllMsgAlert').show();
                setTimeout(function () {
                    $('#enterAllMsgAlert').hide();
                }, 8000);
               
                
            }
        },

        //enterMessage(): 
        enterMessage = function(message){
            selectedMessage(message);
            
            $('#enterMessagesModal').modal('show');
            

        },

        saveMessage = function (msg) {
           my.dataService.saveMessage(msg)
                .done(function (data) {
                    

                    if (!data.isError) {

                            //Get affected stops with no shuttle
                            getAffectedMessages(true, false);

                            //Get unaffected messages
                            getUnaffectedMessages(false, false);

                            //Get affected stops with shuttle
                            getAffectedShuttleMessages(true, true);
                    }
                    $('#continueSavingMsgButton').text('Continue');

                });
            $('#continueSavingMsgButton').hide();
            $('#saveMsgButton').show();
            $('#enterMessagesModal').modal('hide');
            
        },

        saveMsgAll = function (smsMsg, ivrMsg, ivrFileName) {
            ko.utils.arrayForEach(affectedMessages(), function(item){
                var msg = new my.MessageData(eventId(), item.messageID(), smsMsg, ivrMsg,ivrFileName);
                saveMessage(msg);
                //console.log(msg);
            });

            ko.utils.arrayForEach(unAffectedMessages(), function(item){
                var msg = new my.MessageData(eventId(), item.messageID(), smsMsg, ivrMsg,ivrFileName);
                saveMessage(msg);
                //console.log(msg);
            });

            ko.utils.arrayForEach(affectedShuttleMessages (), function(item){
                var msg = new my.MessageData(eventId(), item.messageID(), smsMsg, ivrMsg,ivrFileName);
                saveMessage(msg);
                //console.log(msg);
            });
            //$('#saveMsgButton').button('reset');
           
        },

        saveMsgAllAffected = function(smsMsg, ivrMsg){
            ko.utils.arrayForEach(affectedMessages(), function(item){
                var msg = new my.MessageData(eventId(), item.messageID(), smsMsg, ivrMsg,item.ivrFileName);
                saveMessage(msg);
                //console.log(msg);
            });
          
        },

        saveMsgAllUnaffected = function(smsMsg, ivrMsg,ivrFileName){
            ko.utils.arrayForEach(unAffectedMessages(), function(item){
                var msg = new my.MessageData(eventId(), item.messageID(), smsMsg, ivrMsg, ivrFileName);
                saveMessage(msg);
                //console.log(msg);
            });
           
        },

        saveMsgAllShuttle = function (smsMsg, ivrMsg, ivrFileName) {
            ko.utils.arrayForEach(affectedShuttleMessages (), function(item){
                var msg = new my.MessageData(eventId(), item.messageID(), smsMsg, ivrMsg, ivrFileName);
                saveMessage(msg);
                //console.log(msg);
            });
           
        },

        clearMsgBoxes = function (message) {
           
            message.selectedMessage().smsMessage(null);
            message.selectedMessage().formattedIVRMessage(null);
            //ko.cleanNode($("#smsMessage"));
            //ko.cleanNode($("#smsMessage"));
           


        },
        goBackToEnterMessagesModal = function (message) {
            $("#yesNoModal").modal('hide');
            $('#enterMessagesModal').modal('show');
            $('#saveMsgButton').hide();
            $('#continueSavingMsgButton').show();
            

        },

        /*
        * Remove special characters from text input.
        */
        removeMsWordChars = function(text) {
            var s = text;

            if (s) {
                // smart single quotes and apostrophe
                s = s.replace(/[\u2018\u2019\u201A]/g, "\'");
                // smart double quotes
                s = s.replace(/[\u201C\u201D\u201E]/g, "\"");
                // ellipsis
                s = s.replace(/\u2026/g, "...");
                // dashes
                s = s.replace(/[\u2013\u2014]/g, "-");
                // circumflex
                s = s.replace(/\u02C6/g, "^");
                // open angle bracket
                s = s.replace(/\u2039/g, "<");
                // close angle bracket
                s = s.replace(/\u203A/g, ">");
                // spaces
                s = s.replace(/[\u02DC\u00A0]/g, " ");
                // enter or newline
                s = s.replace(/\u000a/g, "");
            }
            return s;
        },


        saveMessageClicked = function (message) {
            message.selectedMessage().smsMessage(removeMsWordChars(message.selectedMessage().smsMessage()));

            var msgId = message.selectedMessage().messageID();
            var smsMsg = message.selectedMessage().smsMessage();
            var ivrMsg = message.selectedMessage().formattedIVRMessage();
            var ivrFileName = message.selectedMessage().ivrFileName();
            if (smsMsg == "" || smsMsg == null) {
                $('#emptyMessage').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Please add the SMS message</div>");
                $('#emptyMessage').show();
                setTimeout(function () {
                    $('#emptyMessage').hide();
                }, 8000);

            }
            else if (ivrMsg == "" || ivrMsg == null) {
                $('#emptyMessage').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Please add the IVR message</div>");
                $('#emptyMessage').show();
                setTimeout(function () {
                    $('#emptyMessage').hide();
                }, 8000);
            }
            else if (ivrFileName == "" || ivrFileName == null) {
                $('#emptyMessage').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Please generate the voice file</div>");
                $('#emptyMessage').show();
                setTimeout(function () {
                    $('#emptyMessage').hide();
                }, 8000);

            }
            else {

                /*if (duplicateMsgChecked()) {
                    if (selectedDuplicateMessageOption() == "All Messages") {
                        $('#continueSavingMsgButton').text('Saving..');
                        saveMsgAll(smsMsg, ivrMsg, ivrFileName);
                        $('#saveMsgButton').show();
                        $('#continueSavingMsgButton').hide();
                        $('#continueSavingMsgButton').text('Continue');
                        $('#enterMessagesModal').modal('hide');
                    }
                    else if (selectedDuplicateMessageOption() == "All Affected") {
                        $('#continueSavingMsgButton').text('Saving..');
                        saveMsgAllAffected(smsMsg, ivrMsg, ivrFileName);
                        $('#saveMsgButton').show();
                        $('#continueSavingMsgButton').hide();
                        $('#continueSavingMsgButton').text('Continue');
                        $('#enterMessagesModal').modal('hide');
                    }
                    else if (selectedDuplicateMessageOption() == "All Unaffected") {
                        $('#continueSavingMsgButton').text('Saving..');
                        saveMsgAllUnaffected(smsMsg, ivrMsg, ivrFileName);
                        $('#saveMsgButton').show();
                        $('#continueSavingMsgButton').hide();
                        $('#continueSavingMsgButton').text('Continue');
                        $('#enterMessagesModal').modal('hide');
                    }
               }*/
                $('#continueSavingMsgButton').text('Saving...');

                if (duplicateMsgChecked()) {
                   if (selectedDuplicateMessageOption() == "All Messages") {
                      saveMsgAll(smsMsg, ivrMsg, ivrFileName);
                       
                   }
                   else if (selectedDuplicateMessageOption() == "All Affected") {
                       saveMsgAllAffected(smsMsg, ivrMsg, ivrFileName);
                       
                   }
                   else if (selectedDuplicateMessageOption() == "All Unaffected") {
                       saveMsgAllUnaffected(smsMsg, ivrMsg, ivrFileName);
                      
                   }
               }
                else {
                    var msg = new my.MessageData(eventId(), msgId, smsMsg, ivrMsg, ivrFileName);
                    saveMessage(msg);
                    
                }
            }
        },

        openYesNoModal = function () {
            $('#enterMessagesModal').modal('hide');
            $("#yesNoModal").modal('show');
        },

        generateVoiceFile = function () {
            $('#enterMessagesModal').modal('hide');
            $("#yesNoModal").modal('hide');
            $("#voiceFileModal").modal('show');


        },

        speakIVRMessage = function (message) {
            $('#speakIVRMessage').text('Speaking...');
            /*my.dataService.speakIVRVoiceFile(message.formattedIVRMessage())
                         .done(function (data) {
                             $('#speakIVRMessage').text('Speak');
                         });
                         */
            var msg = new SpeechSynthesisUtterance();
            var voices = window.speechSynthesis.getVoices();
            msg.text = message.formattedIVRMessage();
            msg.lang = 'en-US';
            msg.onend = function (e) {
                console.log('Finished in ' + event.elapsedTime + ' seconds.');
                $('#speakIVRMessage').text('Speak');
            };
            speechSynthesis.speak(msg);

        },

        clearIVRMsgBoxes = function (message) {
            message.selectedMessage().formattedIVRMessage("");
        },

        saveGeneratedIVRFile = function (message) {
            $('#saveGeneratedIVRFile').text('Saving...');
            var msgId = message.selectedMessage().messageID();
            var ivrFileName = message.selectedMessage().ivrFileName();
            var ivrMsg = message.selectedMessage().formattedIVRMessage();
            $(".validationMessage").css({ "color": " Red" });
            //var e = getQueryStringVariable('e');
            if (msgId != null) {
                var ivrGenData = {
                    ivrMsg: ivrMsg,
                    msgId: msgId
                };
                my.dataService.generateVoiceFile(ivrGenData)
                        .done(function (data) {
                            var jsonData = JSON.parse(data);
                            if (jsonData.isError) {
                                //serverSideError(data.responseString);
                                if (jsonData.responseString != null || jsonData.responseString != "") {
                                    $('#emptyIVRMessage').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + jsonData.responseString + "</div>");


                                }
                                else {
                                    $('#emptyIVRMessage').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + jsonData.responseObject + "</div>");
                                }
                                $('#emptyIVRMessage').show();
                                setTimeout(function () {
                                    $('#emptyIVRMessage').hide();
                                }, 8000);
                                $('#saveGeneratedIVRFile').text('Save Changes');
                               
                            }
                            else {
                                message.selectedMessage().ivrFileName(jsonData.responseObject);
                                $('#saveGeneratedIVRFile').text('Save Changes');
                                $("#voiceFileModal").modal('hide');
                                $('#enterMessagesModal').modal('show');

                            }
                        });
            }
            /*else if (ivrFileName != "" && ivrFileName !=null)
            {
                my.dataService.generateVoiceFile(ivrMsg, "", "", ivrFileName)
                        .done(function (data) {
                            if (data.isError) {
                                //serverSideError(data.responseString);
                                //$('#emptyMessage').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + data.responseObject + "</div>");
                                
                                if (data.responseString != null || data.responseString != "")
                                {
                                    $('#emptyIVRMessage').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + data.responseString + "</div>");
                                    
                                    
                                }
                                else {
                                    $('#emptyIVRMessage').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + data.responseObject + "</div>");
                                }
                                $('#emptyIVRMessage').show();
                                setTimeout(function () {
                                    $('#emptyIVRMessage').hide();
                                }, 8000);
                                $('#saveGeneratedIVRFile').text('Save Changes');

                            }
                            else {
                                message.selectedMessage().ivrFileName(data.responseObject);
                                $('#saveGeneratedIVRFile').text('Save Changes');
                                $("#voiceFileModal").modal('hide');
                                $('#enterMessagesModal').modal('show');

                            }
                        });

            }*/

        },
       

        getEvent = function () {
            var e = getQueryStringVariable('e');
            
            $("#loadingMsgModal").modal('show');
            my.dataService.getEvent(e)
                    .done(function (data) {
                        if (data.responseObject) {
                            eventId(data.responseObject.EventID);
                            rawStartDate(data.responseObject.EventStartDate);
                            rawEndDate(data.responseObject.EventEndDate);
                            eventDescription(data.responseObject.EventDesc);

                            //Get unaffected messages
                            getUnaffectedMessages(false, false);

                            //Get affected stops with no shuttle
                            getAffectedMessages(true, false);

                            //Get affected stops with shuttle
                            getAffectedShuttleMessages(true, true);
                        }
                        setTimeout(function () {
                            $("#loadingMsgModal").modal('hide');
                        }, 2000);
                        
                    });
            
        };

        return {
            getEvent: getEvent,
            eventId: eventId,
            startDate: startDate,
            endDate: endDate,
            eventDescription: eventDescription,
            unAffectedMessages: unAffectedMessages,
            affectedMessages: affectedMessages,
            affectedShuttleMessages: affectedShuttleMessages,
            duplicateMessageOptions: duplicateMessageOptions,
            selectedDuplicateMessageOption: selectedDuplicateMessageOption,
            enterMessage: enterMessage,
            selectedMessage: selectedMessage,
            duplicateMsgChecked: duplicateMsgChecked,
            saveMessageClicked: saveMessageClicked,
            backToStopsClicked: backToStopsClicked,
            finishEventClicked: finishEventClicked,
            smsMsgCount: smsMsgCount
        }
    })();

  
    my.enterMessagesVM.getEvent();
    ko.applyBindings(my.enterMessagesVM);
   
  
});