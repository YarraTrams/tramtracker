﻿var my = my || {};

$(function () {
    my.SignPosts = function (d) {
        var self = this;
        self.destinationCode = ko.observable(d.DestinationCode);
        self.destination = ko.observable(d.Destination);
    };

   

    my.SignpostFormElement = function (id, formatedChangeStartDate, changeStartHours, changeStartMins, changeEndHours, changeEndMins, formatedChangeEndDate, selectedcode, selectedDestination, updatedDestination) {


        var self = this;
        //alert("format : " + formatedChangeStartDate);
        if (typeof (formatedChangeStartDate) != "undefined") {
            self.startChangeDate = ko.observable(formatedChangeStartDate);
        }
        else {
            self.startChangeDate = ko.observable(new Date());
        }



        self.changeHours = ko.computed(function () {
            var hoursArray = ko.observableArray([]);
            for (var h = 0; h < 24; h++) {
                if (h < 10)
                    h = "0" + h;
                hoursArray.push(h);
            }
            return hoursArray;
        });
        self.changeMins = ko.computed(function () {
            var minsArray = ko.observableArray([]);

            for (var m = 0; m < 60; m += 5) {
                var minString = "";
                if (m < 10)
                    minString = "0" + m;
                else
                    minString = m;
                minsArray.push(minString);
            }
            return minsArray;
        }),
        self.changeStartHours = ko.observable(changeStartHours);
        self.changeStartMins = ko.observable(changeStartMins);
        self.changeEndHours = ko.observable(changeEndHours);
        self.changeEndMins = ko.observable(changeEndMins);
        if (typeof (formatedChangeEndDate) != "undefined") {
            self.endChangeDate = ko.observable(formatedChangeEndDate);
        }
        else {
            self.endChangeDate = ko.observable(new Date().addDays(14));
        }


        
        var getIndex = 0,indexofDCode=0; 
        ko.utils.arrayForEach(signpostLists(), function (item) {
           
           if(item.destinationCode()==selectedcode)
                indexofDCode=getIndex;
            getIndex++;
        });
        self.selectedSignPosts = ko.observable(signpostLists()[indexofDCode]);
        //console.log("signpostLists() " + JSON.stringify(signpostLists()));
        //console.log("indexofDCode " + indexofDCode);

        self.selectedcode = ko.computed(function () {
            var sp = self.selectedSignPosts();
            if (sp) {
                return sp.destinationCode();
            }
        });

        self.selectedDestination = ko.computed(function () {
            var sp = self.selectedSignPosts();
            if (sp) {
                return sp.destination();
            }
        });

        
        
        
       
        //self.selectedcode = ko.observable(selectedcode);
        //self.selectedDestination = ko.observable(selectedDestination);
        
        
        

        self.updatedDestination = ko.observable(updatedDestination);//ko.observable('');
       // alert(JSON.stringify(self.));






    };

    my.SignpostData = function (data) {
        var self = this;
        self.id = data.id;
        self.spInfoID = data.spInfoID;
        self.startChangeDate = ko.computed(function () {
            var startChangeDate = new Date(parseInt(data.changeStartDate.substr(6)));
            return startChangeDate;//dateFormat(startChangeDate, "dd/mm/yyyy");
        });
        self.changeStartHours = ko.computed(function () {
            var startChangeHH = new Date(parseInt(data.changeStartDate.substr(6)));
            return dateFormat(startChangeHH, "HH");
        });
        self.changeStartMins = ko.computed(function () {
            var startChangeMM = new Date(parseInt(data.changeStartDate.substr(6)));
            return dateFormat(startChangeMM, "MM");
        });
        self.changeEndHours = ko.computed(function () {
            var endChangeHH = new Date(parseInt(data.changeEndDate.substr(6)));
            return dateFormat(endChangeHH, "HH");
        });
        self.changeEndMins = ko.computed(function () {
            var endChangeMM = new Date(parseInt(data.changeEndDate.substr(6)));
            return dateFormat(endChangeMM, "MM");
        });
        self.endChangeDate = ko.computed(function () {
                var endChangeDate = new Date(parseInt(data.changeEndDate.substr(6)));
            return endChangeDate;//dateFormat(endChangeDate, "dd/mm/yyyy");
        });
        self.dcode = data.dCode;
        self.existingDestination = data.existingDestination;
        self.updatedDestination = data.destinationChange;
       
    };

    my.SignpostDataLists = function(_spInfoID, _startChangeDate, _changeStartHours, _changeStartMins, _changeEndHours, _changeEndMins, _endChangeDate, _selectedSignPosts, _selectedDestination, _updatedDestination) {
        var self = this;
        self.spInfoID = _spInfoID;
        self.startChangeDate = _startChangeDate;
        self.changeStartHours = _changeStartHours;
        self.changeStartMins = _changeStartMins;
        self.changeEndHours = _changeEndHours;
        self.changeEndMins = _changeEndMins;
        self.endChangeDate = _endChangeDate;
        self.selectedcode = _selectedSignPosts;
        self.selectedDestination = _selectedDestination;
        self.updatedDestination = _updatedDestination;



    };
   
    my.spSpecificInfoVM = (function () {
       
        var spInfoID = ko.observable('');
        
        var createdBy = ko.observable('');
        var createdDate = ko.observable('');
        var modifiedBy = ko.observable('');
        var modifiedDate = ko.observable('');
        var signpostItems = ko.observableArray([]);
        var c = ko.observableArray([]);
        var validationErrors = ko.observableArray([]);
        this.signpostLists = ko.observableArray([]);
        this.signpostFormElements = ko.observableArray([]);
        this.spDescription = ko.observable('');
       
      

        var getSPInfo = function () {
            var spID = getQueryStringVariable('spID');
            my.dataService.getSPSpecificInfo(spID).done(function (data) {
                if (!data.isError) {
                    //alert(JSON.stringify(data.responseObject));
                    spInfoID(data.responseObject.spInfoID);
                    spDescription(data.responseObject.spDescription);
                    createdBy(data.responseObject.createdBy);
                    createdDate(dateFormat(dateFormat(new Date(parseInt(data.responseObject.createdDate.substr(6)))), "dd/mm/yyyy HH:MM"));
                    modifiedBy(data.responseObject.modifiedBy);
                    modifiedDate(dateFormat(dateFormat(new Date(parseInt(data.responseObject.modifiedDate.substr(6)))), "dd/mm/yyyy HH:MM"));
                    var mappedSPLists = $.map(data.responseObject.spUpdates, function (item) {
                       return new my.SignpostData(item);


                    });

                    signpostItems(mappedSPLists);
                    ko.utils.arrayForEach(signpostItems(), function (item) {
                       signpostFormElements.push(new my.SignpostFormElement(item.id, item.startChangeDate(), item.changeStartHours(), item.changeStartMins(), item.changeEndHours(), item.changeEndMins(), item.endChangeDate(), item.dcode, item.existingDestination, item.updatedDestination));
                        
                    });

                }
            });
        };
        var addMoreSignpostForm = function () {
            signpostFormElements.push(new my.SignpostFormElement());
        };

        this.removeRow = function (data) {
            signpostFormElements.remove(data);
        };

        //UPDATE THE SIGNPOST CHANGE
        this.updateSignpost = function () {
            validationErrors.removeAll();
            var index = 0, ref = 0, todaysDate = new Date();//.format("yyyy-mm-dd"), todaysHour = new Date().getHours(), todaysMins = new Date().getMinutes();
            //todaysDate = new Date(todaysDate + "T" + todaysHour + ":" + todaysMins);
            if ((spDescription() == null) || (spDescription() == "")){
                validationErrors.push("The description is required.");
                $('#spName').focus().css("background-color", "#f2dede");
                setTimeout(function () {
                    $('#spName').focus().css("background-color", "#eee");
                }, 10000);
            }
            if (signpostFormElements().length == 0) {
                validationErrors.push("Please add atleast one signpost change.");
            }
            ko.utils.arrayForEach(signpostFormElements(), function (item) {
                ref = ref + 1;
                var cStartDateTime = new Date(item.startChangeDate().format("yyyy-mm-dd") + "T" + item.changeStartHours() + ":" + item.changeStartMins());
                var cEndDateTime = new Date(item.endChangeDate().format("yyyy-mm-dd") + "T" + item.changeEndHours() + ":" + item.changeEndMins());
                //console.log(item.startChangeDate());
                //console.log(item.changeStartHours());
                //console.log(item.changeStartMins());
                //console.log(item.endChangeDate());
                //console.log(item.changeEndMins());
                //console.log(item.changeEndHours());
                //console.log(item.selectedcode());
                //console.log(item.updatedDestination());
                //console.log(item.selectedDestination());
                //console.log("End Date" +cEndDateTime);
                //console.log("Start Date "+cStartDateTime);
                //console.log("Todays Date "+todaysDate);

                if (item.changeStartHours() == null) {
                    validationErrors.push("The Change Start Time 'Hour' is required.Check the row no:" + ref);
                }
                if (item.changeStartMins() == null) {

                    validationErrors.push("The Change Start Time 'Minute' is required.Check the row no:" + ref);
                }
                if (item.changeEndHours() == null) {
                    validationErrors.push("The Change End Time 'Hour' is required.Check the row no:" + ref);
                }
                if (item.changeEndMins() == null) {
                    validationErrors.push("The Change End Time 'Minute' is required.Check the row no:" + ref);
                }

                if (item.selectedcode() == null) {
                    validationErrors.push("Please Choose the Signpost code.Check the row no:" + ref);
                }
                if (item.updatedDestination() == "") {
                    validationErrors.push("The Destination Change is required.Check the row no:" + ref);
                }

                if (todaysDate >= cStartDateTime)
                    validationErrors.push("Start change date cannot earlier than today.Check the row no:" + ref);

                if (cStartDateTime > cEndDateTime)
                    validationErrors.push("Please ensure the Start change date and time is earler than the end change date and time.Check the row no:" + ref);

                if (cStartDateTime.getTime() == cEndDateTime.getTime())
                    validationErrors.push("Please ensure the Start change date and time could not be same as the end change date and time.Check the row no:" + ref);

                if (item.changeStartHours() == null || item.changeStartMins() == null || item.changeEndHours() == null || item.changeEndMins() == null || item.selectedcode() == null || item.updatedDestination() == "" || todaysDate >= cStartDateTime || cStartDateTime > cEndDateTime || cStartDateTime.getTime() == cEndDateTime.getTime()) {
                    $("#mySPtable tr").eq(index).css("background-color", "#f2dede");
                }
               
               
                index = index + 1;

                //var sp = new my.SignpostData(item.startChangeDate(), item.changeStartHours(), item.changeStartMins(), item.changeEndHours(), item.changeEndMins(), item.endChangeDate(), item.selectedSignPosts(), item.selectedDestination(), item.updatedDestination());
            });
            if (validationErrors().length == 0) {
                my.dataService.updateSignPostsInfo(spInfoID(),spDescription()).done(function (data) {
                    if (!data.isError) {
                        ko.utils.arrayForEach(signpostFormElements(), function (item) {
                            var formatedChangeStartDate = dateFormat(item.startChangeDate(), "dd-mm-yyyy");
                            var formatedChangeEndDate = dateFormat(item.endChangeDate(), "dd-mm-yyyy");

                            var spList = new my.SignpostDataLists(spInfoID(), formatedChangeStartDate, item.changeStartHours(), item.changeStartMins(), item.changeEndHours(), item.changeEndMins(), formatedChangeEndDate, item.selectedcode(), item.selectedDestination(), removeMsWordChars(item.updatedDestination()));
                            my.dataService.savesignPostSchedule(spList).done(function (response) {
                                var result = JSON.parse(response);

                                if (result.isError) {
                                    $('#enterAllErrorMsgAlert').html("<div class='alert alert-danger'>" + result.responseObject.Message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span> </div>");
                                    $('#enterAllErrorMsgAlert').show();
                                    setTimeout(function () {
                                        $('#enterAllErrorMsgAlert').hide();
                                    }, 8000);
                                }
                            });

                        });
                        window.location = "signPosts.html";   
                        
                    }
                    else {
                        $('#enterAllErrorMsgAlert').html("<div class='alert alert-danger'>" + data.responseObject.Message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span> </div>");
                        $('#enterAllErrorMsgAlert').show();
                        setTimeout(function () {
                            $('#enterAllErrorMsgAlert').hide();
                        }, 8000);
                    }
                });
               // 
            }
            else {
                setTimeout(function () {
                    $('#mySPtable tr').css('background-color', 'white');
                }, 10000);


            }


        };
        /*
       * Remove special characters from text input.
       */
        var removeMsWordChars = function (text) {
            var s = text;

            if (s) {
                // smart single quotes and apostrophe
                s = s.replace(/[\u2018\u2019\u201A]/g, "\'");
                // smart double quotes
                s = s.replace(/[\u201C\u201D\u201E]/g, "\"");
                // ellipsis
                s = s.replace(/\u2026/g, "...");
                // dashes
                s = s.replace(/[\u2013\u2014]/g, "-");
                // circumflex
                s = s.replace(/\u02C6/g, "^");
                // open angle bracket
                s = s.replace(/\u2039/g, "<");
                // close angle bracket
                s = s.replace(/\u203A/g, ">");
                // spaces
                s = s.replace(/[\u02DC\u00A0]/g, " ");
                // enter or newline
                s = s.replace(/\u000a/g, "");
            }
            return s;
        };
        
        var getSignPostLists = function () {
            my.dataService.getAllSignPostList().done(function (data) {
                if (data.responseObject) {
                    var mappedSP = $.map(data.responseObject, function (item) {
                        return new my.SignPosts(item);
                    });
                    signpostLists(mappedSP);
                }

            })
        };

        return {
            spInfoID: spInfoID,
            spDescription: spDescription,
            createdBy: createdBy,
            createdDate: createdDate,
            modifiedBy: modifiedBy,
            modifiedDate: modifiedDate,
            signpostItems: signpostItems,
            signpostFormElements: signpostFormElements,
            signpostLists:signpostLists,
            getSPInfo: getSPInfo,
            addMoreSignpostForm: addMoreSignpostForm,
            removeRow: removeRow,
            getSignPostLists: getSignPostLists,
            validationErrors: validationErrors
           
        }
    })();

    my.spSpecificInfoVM.getSignPostLists();
    my.spSpecificInfoVM.getSPInfo();

    ko.applyBindings(my.spSpecificInfoVM);

    ko.bindingHandlers.bootstrapPopover = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var options = valueAccessor();
            var defaultOptions = {};
            options = $.extend(true, { trigger: "hover" }, defaultOptions, options);
            $(element).popover(options);
        }
    };
});