﻿var my = my || {};

$(function () {

    my.eventCreateVM = (function (event) {

        var hours = ko.computed(function () {
            var hoursArray = ko.observableArray([]);
            for (var h = 0; h < 24; h++) {
                if (h < 10)
                    h = "0" + h;
                hoursArray.push(h);
            }
            return hoursArray;
        }),

        mins = ko.computed(function () {
            var minsArray = ko.observableArray([]);

            for (var m = 0; m < 60; m += 5) {
                var minString = "";
                if (m < 10)
                    minString = "0" + m;
                else
                    minString = m;
                minsArray.push(minString);
            }
            return minsArray;
        }),
        eventId = ko.observable(''),
        eventDescription = ko.observable('').extend({ required: {message: "Please include an event description" } }),
        isPublicHolidayEvent = ko.observable(),
        publicHolidayIVRMessage = ko.observable().extend({ required: { message: "Please include an IVR message" } }),
        ivrFileName = ko.observable(''),
        publicHolidaySMSMessage = ko.observable('').extend({ required: {message: "Please include SMS message" } }),
        startDate = ko.observable(''),
        startHours = ko.observable('').extend({ required: { message: "Please enter the start hour"} }),
        startMins = ko.observable('').extend({ required: { message: "Please enter the start minute" } }),

        endDate = ko.observable(''),
        endHours = ko.observable('').extend({ required: { message: "Please enter the end hour" } }),
        endMins = ko.observable('').extend({ required: { message: "Please enter the end minute" } }),
        

        validationGroup = [
            eventDescription,
            startHours,
            startMins,
            endHours,
            endMins
        ],

        serverSideError = ko.observable(),
        validationErrors = ko.validation.group(validationGroup),
        smsMsgCount = ko.computed(function () {
            var countNum = 117 - publicHolidaySMSMessage().length;
            return countNum;
        }),
     

        getEvent = function () {
            var e = getQueryStringVariable('e');
           
            if (e) {
                my.dataService.getEvent(e)
                    .done(function (data) {
                        if (data.responseObject) {
                            eventId(data.responseObject.EventID);
                            eventDescription(data.responseObject.EventDesc);
                            startDate(new Date(parseInt(data.responseObject.EventStartDate.substr(6))));
                            endDate(new Date(parseInt(data.responseObject.EventEndDate.substr(6))));
                            startHours(startDate().getHours());
                            startMins(startDate().getMinutes());
                            endHours(endDate().getHours());
                            endMins(endDate().getMinutes());

                            if (data.responseObject.IsPublicHoliday) {
                                isPublicHolidayEvent(data.responseObject.IsPublicHoliday);
                                publicHolidayIVRMessage(data.responseObject.IVRHolidayMessage);
                                publicHolidaySMSMessage(data.responseObject.SMSHolidayMessage);
                                getIVRFileForPublicEvent(e);
                            }
                        }
                    });
            }
        },

        getIVRFileForPublicEvent = function (eventId) {
            my.dataService.getIVRFileForPublicEvent(eventId)
                    .done(function (data) {
                        if (data.responseObject) {
                            ivrFileName(data.responseObject[0].IVRMessage);
                        }

           });
        },

        /*
        * Remove special characters from text input.
        */
        removeMsWordChars = function (text) {
            var s = text;

            if (s) {
                // smart single quotes and apostrophe
                s = s.replace(/[\u2018\u2019\u201A]/g, "\'");
                // smart double quotes
                s = s.replace(/[\u201C\u201D\u201E]/g, "\"");
                // ellipsis
                s = s.replace(/\u2026/g, "...");
                // dashes
                s = s.replace(/[\u2013\u2014]/g, "-");
                // circumflex
                s = s.replace(/\u02C6/g, "^");
                // open angle bracket
                s = s.replace(/\u2039/g, "<");
                // close angle bracket
                s = s.replace(/\u203A/g, ">");
                // spaces
                s = s.replace(/[\u02DC\u00A0]/g, " ");
                // enter or newline
                s = s.replace(/\u000a/g, "");
            }
            return s;
        },

     /*** 
        function() to save the generated voice file
      ***/
    saveGeneratedIVRFile = function (message) {
        $('#saveGeneratedIVRFile').text('Saving...');
        $(".validationMessage").css({ "color": " Red" });
        var e = getQueryStringVariable('e');
        if (e) {
            var ivrGenData = {
                ivrMsg: message.publicHolidayIVRMessage(),
                e: e,
                checkHoliday: isPublicHolidayEvent()
            };
            my.dataService.generateVoiceFile(ivrGenData)
                    .done(function (data) {
                        var jsonData = JSON.parse(data);
                        if (jsonData.isError) {
                            //serverSideError(data.responseString);

                            if (jsonData.responseString != null || jsonData.responseString != "") {

                                $('#emptyIVRMessage').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + jsonData.responseString + "</div>");
                            }
                            else {
                                $('#emptyIVRMessage').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + jsonData.responseObject + "</div>");
                            }
                            $('#emptyIVRMessage').show();
                            setTimeout(function () {
                                $('#emptyIVRMessage').hide();
                            }, 8000);

                            $('#saveGeneratedIVRFile').text('Save Changes');

                        }
                        else {
                            ivrFileName(jsonData.responseObject);
                            $('#saveGeneratedIVRFile').text('Save Changes');
                            $("#voiceFileModal").modal('hide');

                        }
                    });
        }
        else {
            var ivrGenData = {
                ivrMsg: message.publicHolidayIVRMessage(),
                checkHoliday: isPublicHolidayEvent()
            };
            my.dataService.generateVoiceFile(ivrGenData)
                    .done(function (data) {
                        var jsonData = JSON.parse(data);
                        if (jsonData.isError) {
                            //serverSideError(data.responseString);

                            if (jsonData.responseString != null || jsonData.responseString != "") {

                                $('#emptyIVRMessage').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + jsonData.responseString + "</div>");
                            }
                            else {
                                $('#emptyIVRMessage').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + jsonData.responseObject + "</div>");
                            }
                            $('#emptyIVRMessage').show();
                            setTimeout(function () {
                                $('#emptyIVRMessage').hide();
                            }, 8000);


                            $('#saveGeneratedIVRFile').text('Save Changes');

                        }
                        else {
                            ivrFileName(jsonData.responseObject);
                            $('#saveGeneratedIVRFile').text('Save Changes');
                            $("#voiceFileModal").modal('hide');

                        }
                    });
        }

    },
    
    generateVoiceFile = function () {
        $("#yesNoModal").modal('hide');
        $("#voiceFileModal").modal('show');


    },

    createEvent = function () {
            //$("#yesNoModal").modal('hide');
            console.log(eventDescription());
            console.log(isPublicHolidayEvent());
            $(".validationMessage").css({ "color": " Red" });

            var formattedStartDate = dateFormat(startDate(), "dd-mm-yyyy");
            var formattedEndDate = dateFormat(endDate(), "dd-mm-yyyy");
            publicHolidaySMSMessage(removeMsWordChars(publicHolidaySMSMessage()));

            console.log(formattedStartDate);
            console.log(startHours());
            console.log(startMins());
            console.log(formattedEndDate);
            console.log(endHours());
            console.log(endMins());
            console.log(ivrFileName());

            


            if (validationErrors().length == 0) {
                
                $('#createPublicEvent').html('Creating...');
                if (isPublicHolidayEvent()) {
                    var ivrGenData = {
                        ivrMsg: publicHolidayIVRMessage(),
                        e: eventId()
                    };
                    my.dataService.generateVoiceFile(ivrGenData)
                                        .done(function (data) {
                                            var jsonData = JSON.parse(data);
                                            //alert(jsonData.responseString);
                                            if (jsonData.isError) {

                                                serverSideError(jsonData.responseObject);
                                                $('#server-validation-errors').show();
                                                setTimeout(function () {
                                                    $('#server-validation-errors').hide();
                                                }, 5000);

                                            }
                                            else {
                                                ivrFileName(jsonData.responseObject);


                                            }
                                        });
                }
                var event = {
                    eventId: eventId(),
                    txtdescription: eventDescription(),
                    checkHoliday: isPublicHolidayEvent(),
                    txtIVR: publicHolidayIVRMessage(),
                    txtSMS: publicHolidaySMSMessage(),
                    ivrFile: ivrFileName(),
                    startdate: formattedStartDate,
                    enddate: formattedEndDate,
                    stHr: startHours(),
                    stMn: startMins(),
                    endHr: endHours(),
                    enMn: endMins()
                };
                my.dataService.createEvent(event)
                                           .done(function (data) {
                            var jsonData = JSON.parse(data);
                            if (!jsonData.isError) {
                                eventId(jsonData.responseObject.EventID);
                                if (event.checkHoliday) {
                                   
                                    window.location = "SpecialEventSummary.html?e=" + eventId();
                                }
                                else {
                                    window.location = "specialeventcreate.step2.html?e=" + eventId();
                                }
                                
                            }
                            else {
                                serverSideError(jsonData.responseString);
                                $('#createPublicEvent').html('Create Event <span class="glyphicon glyphicon-chevron-right"></span>');
                            }
                        });
               
            }
            else {
                validationErrors.showAllMessages(true);
            }
            

        };

        return {
            hours: hours,
            mins: mins,
            getEvent: getEvent,
            eventDescription: eventDescription,
            isPublicHolidayEvent: isPublicHolidayEvent,
            publicHolidayIVRMessage: publicHolidayIVRMessage,
            publicHolidaySMSMessage: publicHolidaySMSMessage,
            ivrFileName:ivrFileName,
            startDate: startDate,
            startHours: startHours,
            startMins: startMins,
            endDate: endDate,
            endHours: endHours,
            endMins: endMins,
            createEvent: createEvent,
            saveGeneratedIVRFile: saveGeneratedIVRFile,
            generateVoiceFile: generateVoiceFile,
            getIVRFileForPublicEvent:getIVRFileForPublicEvent,
            validationErrors: validationErrors,
            serverSideError: serverSideError,
            smsMsgCount: smsMsgCount
        }
    })();

    ko.validation.configure({
        registerExtenders: true,
        messagesOnModified: true,
        insertMessages: true,
        parseInputAttributes: true,
        messageTemplate: null
    });

    clearMsgBoxes = function (message) {

        message.publicHolidaySMSMessage("");
        message.publicHolidayIVRMessage("");
        //ko.cleanNode($("#smsMessage"));
        //ko.cleanNode($("#smsMessage"));

    };
   

    /*** 
      function() to speak the text IVR message
     ***/

    speakIVRMessage = function (message) {
        $('#speakIVRMessage').text('Speaking...');
        /*my.dataService.speakIVRVoiceFile(message.publicHolidayIVRMessage())
                    .done(function (data) {
                        $('#speakIVRMessage').text('Speak');
        });*/
        var msg = new SpeechSynthesisUtterance();
        var voices = window.speechSynthesis.getVoices();
        msg.text = message.publicHolidayIVRMessage();
        msg.lang = 'en-US';
        msg.onend = function (e) {
            console.log('Finished in ' + event.elapsedTime + ' seconds.');
            $('#speakIVRMessage').text('Speak');
        };
        speechSynthesis.speak(msg);

    };

    /*** 
       function() to clear IVR textarea message from the generate voice file IVR textarea
      ***/
    clearIVRMsgBoxes = function (message) {
        message.publicHolidayIVRMessage("");
    };

    openYesNoModal = function () {
        $("#yesNoModal").modal('show');
    };

    ko.bindingHandlers.smsMsgLimitCharacters = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            element.value = element.value.substr(0, valueAccessor());
            allBindingsAccessor().value(element.value.substr(0, valueAccessor()));
        }
    };

    my.eventCreateVM.getEvent();
    ko.applyBindings(my.eventCreateVM);
});