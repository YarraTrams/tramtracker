﻿var my = my || {};

$(function () {

    my.Event = function (data) {



        this.eventId = data.EventID;
       
        this.startDate = ko.computed(function () {
            var startDate = new Date(parseInt(data.EventStartDate.substr(6)));
            return dateFormat(startDate, "dddd, d mmmm yyyy HH:MM");
        });

        this.endDate = ko.computed(function () {
            var endDate = new Date(parseInt(data.EventEndDate.substr(6)));
            return dateFormat(endDate, "dddd, d mmmm yyyy HH:MM");
        });

        this.eventSummaryUrl = ko.computed(function () {
            return "SpecialEventSummary.html?e=" + data.EventID;
        });

        this.editEventUrl = ko.computed(function () {
            return "specialeventcreate.step1.html?e=" + data.EventID;
        });

        this.editStopsUrl = ko.computed(function () {
            return "specialeventcreate.step2.html?e=" + data.EventID;
        });

        this.editMessagesUrl = ko.computed(function () {
            return "specialeventcreate.step3.html?e=" + data.EventID;
        });

        this.editPreEventUrl = ko.computed(function () {
            return "createPreEvent.html?e=" + data.EventID;
        });


        this.eventDescription = ko.observable(data.EventDesc);
        this.createdBy = ko.observable(data.CreatedBy);
        this.lastUpdatedBy = ko.observable(data.UpdatedBy);
        this.sortorder = ko.observable(data.sortorder);
        this.isPublicHoliday = ko.observable(data.IsPublicHoliday);
        this.isPreEvent = ko.observable(data.IsPreEvent);
        this.isPublished = ko.observable(data.IsPublished);
        this.affectedRouteList = ko.observable(data.AffectedRouteList);
        this.lastPublishedBy = ko.observable(data.LastUnPublishedBy);
        this.lastUnPublishedBy = ko.observable(data.LastUnPublishedBy);
        this.lastUpdate = ko.computed(function () {
            var updatedDate = new Date(parseInt(data.LastUpdate.substr(6)));
            return dateFormat(updatedDate, "dddd, mmmm d, yyyy HH:MM");
        });


        this.lastPublishedUpdate = ko.computed(function () {
            var updatedDate = new Date(parseInt(data.LastPublishedUpdate.substr(6)));
            return dateFormat(updatedDate, "dddd, mmmm d, yyyy HH:MM");
        });


        this.lastUnPublishedUpdate = ko.computed(function () {
            var updatedDate = new Date(parseInt(data.LastUnPublishedUpdate.substr(6)));
            if (dateFormat(updatedDate, "yyyy") == "1")
                return " -";
            else
                return dateFormat(updatedDate, "dddd, mmmm d, yyyy HH:MM");
        });

        this.lastUpdate = ko.computed(function () {
            var updatedDate = new Date(parseInt(data.LastUpdate.substr(6)));
            return dateFormat(updatedDate, "dddd, mmmm d, yyyy HH:MM");
        });



    };
    
    my.scheduleChange = function (data) {
        var self = this;
        self.sID = data.spInfoID;
        self.signPostDescription = data.spDescription;
        self.affectedSignpostCode = data.affectedSignpostCode;
        self.createdBy = data.createdBy;
        self.modifiedBy = data.modifiedBy;
        self.createdDate = dateFormat(new Date(parseInt(data.createdDate.substr(6))), "dd/mm/yyyy HH:MM");
        self.modifiedDate = dateFormat(new Date(parseInt(data.modifiedDate.substr(6))), "dd/mm/yyyy HH:MM");
        this.spSummaryUrl = ko.computed(function () {
            return "signpostSummary.html?spID=" + self.sID;
        });
        this.spEditUrl = ko.computed(function () {
            return "editSignpostList.html?spID=" + self.sID;
        });
        self.isSignpostPublished = data.isPublished;



    };
    my.eventListVM = (function () {


        this.signpostLists = ko.observableArray([]);
        this.scheduleInfos = ko.observableArray([]);


        var startDate = ko.observable(new Date()),
            endDate = ko.observable(new Date().addDays(14)),
            hours = ko.computed(function () {
                var hoursArray = ko.observableArray([]);
                for (var h = 0; h < 24; h++) {
                    if (h < 10)
                        h = "0" + h;
                    hoursArray.push(h);
                }
                return hoursArray;
            }),

            mins = ko.computed(function () {
                var minsArray = ko.observableArray([]);

                for (var m = 0; m < 60; m += 5) {
                    var minString = "";
                    if (m < 10)
                        minString = "0" + m;
                    else
                        minString = m;
                    minsArray.push(minString);
                }
                return minsArray;
            }),
            startHours = ko.observable('').extend({ required: { message: "Please enter the start hour" } }),
            startMins = ko.observable('').extend({ required: { message: "Please enter the start minute" } }),
            endHours = ko.observable('').extend({ required: { message: "Please enter the end hour" } }),
            endMins = ko.observable('').extend({ required: { message: "Please enter the end minute" } }),
            events = ko.observableArray([]),

            eventToDelete = ko.observable(),
            setEventToDelete = function (event) {
                eventToDelete(event);
            },
            validationErrors = ko.observableArray([]);



        deleteEvent = function () {
            $('#deleteEventModal').modal('hide');
            $('#deleteProcessModal').modal('show');
            my.dataService.deleteEventFromLiveAndDev(eventToDelete().eventId)
                .done(function (data) {
                    if (!data.isError) {

                        $('#messageDiv').html("<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Successfully deleted the event !!</div>");

                        my.eventListVM.getEventList();
                    }
                    else {
                        $('#messageDiv').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + data.responseObject + "</div>");

                    }
                    $('#messageDiv').show();
                    setTimeout(function () {
                        $('#messageDiv').hide();
                    }, 8000);

                    $('#deleteProcessModal').modal('hide');
                });

        },

            publishToLive = function (eventId) {

                $('#publishSignPostModal').modal('show');
                var currentEventID = JSON.stringify(eventId.eventId);
                my.dataService.publishToLive(currentEventID)
                    .done(function (data) {
                        if (!data.isError) {
                            $('#messageDiv').html("<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Successfully published to live !!</div>");
                            my.eventListVM.getEventList();
                        }
                        else {
                            $('#messageDiv').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + data.responseObject + "</div>");

                        }
                        $('#messageDiv').show();
                        setTimeout(function () {
                            $('#messageDiv').hide();
                        }, 8000);
                        $('#publishSignPostModal').modal('hide');
                    });


            },

            sPForUnschedule = ko.observable(),
            setSPForUnschedule = function (sid) {
                sPForUnschedule(sid);
            },


            setSpDelete = function (signpost) {
                //alert(signpost.sID);
                sPToDelete(signpost.sID);
            };
        sPToDelete = ko.observable();

        deleteSpecificSignpost = function () {
            $('#deleteSpecificSPModal').modal('hide');
            alert(sPToDelete());
            my.dataService.deleteSignPost(sPToDelete())
                .done(function (data) {
                    if (!data.isError) {
                        $('#enterAllErrorsAlert').html("<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Successfully deleted !!</div>");
                        my.eventListVM.getSPScheduleList();
                    }
                    else {
                        $('#enterAllErrorsAlert').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + data.responseObject.Message + "</div>");
                    }

                    $('#enterAllErrorsAlert').show();
                    setTimeout(function () {
                        $('#enterAllErrorsAlert').hide();
                    }, 8000);
                    $('#enterAllErrorsAlert').modal('hide');


                });

        },
            deleteSignpost = function () {
                //alert(sPForUnschedule().sID);
                $('#deleteSPModal').modal('hide');
                //$('#deleteSignpostProcessModal').modal('show');
                my.dataService.unscheduleSignPost(sPForUnschedule().sID)
                    .done(function (data) {
                        if (!data.isError) {
                            $('#enterAllErrorsAlert').html("<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Successfully unpublished signpost from live !!</div>");
                            my.eventListVM.getSPScheduleList();
                        }
                        else {
                            $('#enterAllErrorsAlert').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + data.responseObject.Message + "</div>");
                        }

                        $('#enterAllErrorsAlert').show();
                        setTimeout(function () {
                            $('#enterAllErrorsAlert').hide();
                        }, 8000);
                        $('#enterAllErrorsAlert').modal('hide');


                    });
                //$('#deleteSignpostProcessModal').modal('hide');
            },

            publishSignpost = function (signpost) {

                //$('#publishModal').modal('show');
                var currentspID = JSON.stringify(signpost.sID);
                my.dataService.validateEndDate(currentspID).done(function (data) {
                    if (!data.isError) {
                        
                        if (data.responseObject) {


                            my.dataService.publishSignpostToProd(currentspID).done(function (data) {

                                    if (!data.isError) {
                                        $('#enterAllErrorsAlert').html("<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Successfully published signpost to live !!</div>");
                                        my.eventListVM.getSPScheduleList();
                                    }
                                    else {
                                        $('#enterAllErrorsAlert').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + data.responseObject.Message + "</div>");
                                    }

                                    $('#enterAllErrorsAlert').show();
                                    setTimeout(function () {
                                        $('#enterAllErrorsAlert').hide();
                                    }, 8000);
                                    $('#enterAllErrorsAlert').modal('hide');
                            });
                            //$('#publishModal').modal('hide');
                        }
                        else {
                            $('#enterAllErrorsAlert').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Please check the change end date of this event. It cannot be lesser than today's date. So please update before publishing it.</div>");
                        }
                    }
                    else {
                        $('#enterAllErrorsAlert').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + data.responseObject.Message + "</div>");
                    }
            });


        }


        getSPScheduleList = function () {
            my.dataService.getSPScheduleInfo()
                                .done(function (data) {
                                    if (!data.isError) {
                                        var mappedscheduleInfo = $.map(data.responseObject, function (item) {
                                            //alert((JSON.stringify(item)));
                                            return new my.scheduleChange(item);

                                        });

                                        scheduleInfos(mappedscheduleInfo);


                                    }
                                   

                                });

        };

        getEventList = function () {
            var formattedStartDate = dateFormat(startDate(), "dd-mm-yyyy");
            var formattedEndDate = dateFormat(endDate(), "dd-mm-yyyy");
            $('#set-filter-dates').val('Filtering..');
            my.dataService.getEventsBySortOrder()
                                .done(function (data) {
                                    if (data.responseObject) {
                                        var mappedEvents = $.map(data.responseObject, function (item) {
                                            //alert((JSON.stringify(item)));
                                            return new my.Event(item);

                                        });

                                        events(mappedEvents);


                                    }
                                    $('#set-filter-dates').val('Filter Events');
                                    
                                });
            
        };

       
        currentYear = ko.computed(function () {
            return new Date().getFullYear();
        });

       
        return {
            hours: hours,
            mins: mins,
            startDate: startDate,
            startHours: startHours,
            startMins: startMins,
            endDate: endDate,
            endHours: endHours,
            endMins: endMins,
            events: events,
            currentYear: currentYear,
            getEventList: getEventList,
            setEventToDelete: setEventToDelete,
            eventToDelete: eventToDelete,
            deleteEvent: deleteEvent,
            publishToLive: publishToLive,
            validationErrors: validationErrors,
            getSPScheduleList: getSPScheduleList,
            scheduleInfos: scheduleInfos
            

        }

    })();

    ko.validation.configure({
        registerExtenders: true,
        messagesOnModified: true,
        insertMessages: true,
        parseInputAttributes: true,
        messageTemplate: null
    });
    
    ko.bindingHandlers.uiSortableList = {
        init: function (element, valueAccessor, allBindingsAccesor, context,arg) {
            var $element = $(element),
              list = valueAccessor();
              $element.sortable({
                update: function (event, ui) {
                    var item = ko.dataFor(ui.item[0]),
                     newIndex = ko.utils.arrayIndexOf(ui.item.parent().children(), ui.item[0]);
                    var oldIndex = list.indexOf(item);
                    var isFailed = 0;
                    var changeStatus = false;
                    var startIndex = 0;
                    var endIndex = 0;

                    //alert(oldIndex);
                    
                    if (newIndex >= list().length)
                        newIndex = list().length - 1;
                    if (newIndex < 0)
                        newIndex = 0;
                    //$('#eventsTB').hide();
                    ui.item.remove();
                    list.remove(item);
                    list.splice(newIndex, 0, item);
                    //alert(JSON.stringify(list()[newIndex].eventId));
                    //alert(JSON.stringify(list()));
                    //alert(newIndex);
                    //alert(oldIndex);
                    // for (var i = 0; i < list().length; i++) {
                    if (oldIndex > newIndex) {
                        startIndex = newIndex;
                        endIndex = oldIndex
                    }
                    else {
                        startIndex = oldIndex;
                        endIndex =newIndex ;
                    }
                    for (var i = startIndex; i <= endIndex; i++) {
                        var id = JSON.stringify(list()[i].eventId);
                        //alert(id);
                        var sortOrder = i;
                        my.dataService.updateEventSortOrder(id,sortOrder)
                            .done(function (data) {
                                if (data.isError) {
                                    isFailed = 1;
                                }
                                else {
                                    //changeStatus = JSON.stringify(data.responseObject.IsPublished);
                                    //if (changeStatus) {

                                        my.eventListVM.getEventList();

                                    //}
                                }
                                   
                       });
                    }
                    my.dataService.updateRoutesStopsOrder()
                    .done(function (data) {
                        if (!data.isError) {
                            if (isFailed != 1)
                                $('#updatedSortOrder').html("<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Succesfully updated !!</div>");
                            console.log("Update");
                        }
                        else
                            $('#updatedSortOrder').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Something went wrong !!</div>");

                    });
                  /*  if (isFailed ==1) {
                        
                        $('#updatedSortOrder').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Something went wrong !!</div>");
                        
                    }
                    else {
                        $('#updatedSortOrder').html("<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Succesfully updated !!</div>");

                    }*/
                    
                   

                    $('#updatedSortOrder').show();
                    setTimeout(function () {
                        $('#updatedSortOrder').hide();
                    }, 8000);
                    
                }
            });
        }
    };

    ko.bindingHandlers.bootstrapPopover = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var options = valueAccessor();
            var defaultOptions = {};
            options = $.extend(true, { trigger: "hover" }, defaultOptions, options);
            $(element).popover(options);
        }
    };

   
    my.eventListVM.getEventList();
    my.eventListVM.getSPScheduleList();
    ko.applyBindings(my.eventListVM);
   
    

    

});