﻿var my = my || {};

$(function () {

    my.SignpostData = function (data) {
        var self = this;
        self.id = data.id;
        self.spInfoID = data.spInfoID;
        self.startChangeDate = ko.computed(function () {
            var startChangeDate = new Date(parseInt(data.changeStartDate.substr(6)));
            return dateFormat(startChangeDate, "dd/mm/yyyy HH:MM");
        });
        //self.changeStartHours = data.changeStartHours;
        //self.changeStartMins = data.changeStartMins;
        //self.changeEndHours = data.changeEndHours;
        //self.changeEndMins = data.changeEndMins;
        self.endChangeDate = ko.computed(function () {
                var startChangeDate = new Date(parseInt(data.changeEndDate.substr(6)));
                return dateFormat(startChangeDate, "dd/mm/yyyy HH:MM");
        });
        self.dcode = data.dCode;
        self.existingDestination = data.existingDestination;
        self.updatedDestination = data.destinationChange;



    };
   
    my.spSpecificInfoVM = (function () {
       
        var spInfoID = ko.observable('');
        var spDescription = ko.observable('');
        var createdBy = ko.observable('');
        var createdDate = ko.observable('');
        var modifiedBy = ko.observable('');
        var modifiedDate = ko.observable('');
        var signpostItems = ko.observableArray([]);
      

        var getSPInfo = function () {
            var spID = getQueryStringVariable('spID');
            my.dataService.getSPSpecificInfo(spID).done(function (data) {
                if (!data.isError) {
                    //alert(JSON.stringify(data.responseObject));
                    spInfoID(data.responseObject.spInfoID);
                    spDescription(data.responseObject.spDescription);
                    createdBy(data.responseObject.createdBy);
                    createdDate(dateFormat(dateFormat(new Date(parseInt(data.responseObject.createdDate.substr(6)))), "dd/mm/yyyy HH:MM"));
                    modifiedBy(data.responseObject.modifiedBy);
                    modifiedDate(dateFormat(dateFormat(new Date(parseInt(data.responseObject.modifiedDate.substr(6)))), "dd/mm/yyyy HH:MM"));
                    var mappedSPLists = $.map(data.responseObject.spUpdates, function (item) {
                        return new my.SignpostData(item);
                    });

                    signpostItems(mappedSPLists);
                    

                }
            });
        };

        return {
            spInfoID: spInfoID,
            spDescription: spDescription,
            createdBy: createdBy,
            createdDate: createdDate,
            modifiedBy: modifiedBy,
            modifiedDate: modifiedDate,
            signpostItems: signpostItems,
            getSPInfo: getSPInfo,
           
        }
    })();
    my.spSpecificInfoVM.getSPInfo();
    ko.applyBindings(my.spSpecificInfoVM);
});