﻿var my = my || {};


my.dataService = function () {
    var baseURL = "controllers/",


        generateVoiceFile = function (ivrData) {
            return $.post(baseURL + "GenerateVoiceFile.aspx", ivrData);
        },
        speakIVRVoiceFile = function (message) {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "SpeakIVRVoiceFile.aspx?ivrMsg=" + message + "&ts=" + noCache);
        },

        getEvents = function (startDate, endDate) {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "GetAllEvents.aspx?sd=" + startDate + "&ed=" + endDate + "&ts=" + noCache);
        },

        getEventsBySortOrder = function (startDate, endDate) {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "GetAllEventsBySortOrder.aspx?ts=" + noCache);
        },

        getEvent = function (eventId) {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "SpecialEventLoad.aspx?e=" + eventId + "&ts=" + noCache);
        },

        getIVRFileForPublicEvent = function (eventId) {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "GetIVRFileForPublicEvent.aspx?e=" + eventId + "&ts=" + noCache);
        },

        getSpecialEventRoutes = function (eventId) {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "GetRoutes.aspx?e=" + eventId + "&u=true&ts=" + noCache);
        },

        getPreEventRouteDetails = function (eventId) {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "GetPreEventRoutes.aspx?e=" + eventId + "&ts=" + noCache);
        },

        getPreEventMessages = function (eventId) {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "GetPreEventMessages.aspx?e=" + eventId + "&ts=" + noCache);
        },

        getAllRoutes = function (eventId) {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "RoutesLoad.aspx?ts=" + noCache);
        },

        getMainRoutes = function () {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "RoutesLoad.aspx?ts=" + noCache);
        },


        updateRoutesStopsOrder = function () {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "UpdateRoutesStopsOrder.aspx?ts=" + noCache);
        },

        getStops = function (eventId, routeNo) {
            var noCache = new Date().getTime();
            //return $.getJSON(baseURL + "StopsLoad.aspx?e=" + eventId + "&r=" + routeNo + "&ts=" + noCache);
            return $.ajax({
                url: baseURL + "StopsLoad.aspx?e=" + eventId + "&r=" + routeNo + "&ts=" + noCache,
                dataType: "json",
                async: true
            });
        },

        getStopsSynchronously = function (eventId, routeNo) {
            var noCache = new Date().getTime();
            //return $.getJSON(baseURL + "StopsLoad.aspx?e=" + eventId + "&r=" + routeNo + "&ts=" + noCache);
            return $.ajax({
                url: baseURL + "StopsLoad.aspx?e=" + eventId + "&r=" + routeNo + "&ts=" + noCache,
                dataType: "json",
                async: false
            });
        },

        getTempSpecialEventMessage = function (eventId, affectedStops, shuttleTrams) {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "TempMessageLoad.aspx?e=" + eventId + "&a=" + affectedStops + "&s=" + shuttleTrams + "&ts=" + noCache);
        },

        saveMessage = function (message) {
            return $.ajax({
                url: baseURL + "SpecialEventMessageAdd.aspx",
                type: "POST",
                async: false,
                data: message,
                success: function (msg) {
                    alert("Data Saved: " + msg);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("some error");
                }
            });
        },

        getSpecialEventMessages = function (eventId, stopNo, routeNo) {
            var noCache = new Date().getTime()
            return $.getJSON(baseURL + "GetSpecialEventMessages.aspx?e=" + eventId + "&s=" + stopNo + "&r=" + routeNo + "&ts=" + noCache);
        },

        createEvent = function (event) {

            return $.post(baseURL + "specialEventSet.aspx", event);
        },

        createPreEvent = function (event) {

            return $.post(baseURL + "preEventSet.aspx", event);
        },

        deleteEvent = function (eventId) {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "SpecialEventDelete.aspx?e=" + eventId + "&ts=" + noCache);
        },

        deleteEventFromLiveAndDev = function (eventId) {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "DeleteEventFromLiveAndDev.aspx?e=" + eventId + "&ts=" + noCache);
        },

        shuttleSet = function (eventId, routeNo, isUpStop, hasShuttle, stopNo) {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "ShuttleSet.aspx?isup=" + isUpStop + "&r=" + routeNo + "&s=" + stopNo + "&ischecked=" + hasShuttle + "&e=" + eventId + "&ts=" + noCache);
        },

        checkEventRoutes = function (eventId) {
            var noCache = new Date().getTime();
            return $.ajax({
                url: baseURL + "CheckEventRoutes.aspx?e=" + eventId + "&ts=" + noCache,
                dataType: "json",
                async: false
            });
            //return $.getJSON("controllers/CheckEventRoutes.aspx?e=" + eventId + "&ts=" + noCache);
        },

        consolidateSpecialEvent = function (eventId) {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "ConsolidateSpecialEvent.aspx?e=" + eventId + "&ts=" + noCache);
        },

        finshSpecialEvent = function (eventId) {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "FinalConsolidation.aspx?e=" + eventId + "&ts=" + noCache);
        },

        updateEventSortOrder = function (eventId, sortOrder) {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "PrioritizeEvents.aspx?e=" + eventId + "&order=" + sortOrder + "&ts=" + noCache);
        },

        publishToLive = function (eventId) {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "PublishToLive.aspx?e=" + eventId + "&ts=" + noCache);
        },

        getAllSignPostList = function () {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "LoadSignPosts.aspx?ts=" + noCache);
        },

        saveStop = function (route, eventId, stop, on, isUpStop) {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "AffectedStopSave.aspx?r=" + route + "&e=" + eventId + "&s=" + stop + "&on=" + on + "&d=" + isUpStop + "&ts=" + noCache);
        },

        saveMultipleStops = function (route, eventId, stopList, on, isUpStop) {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "AffectedMultipleStopsSave.aspx?r=" + route + "&e=" + eventId + "&s=" + stopList + "&on=" + on + "&d=" + isUpStop + "&ts=" + noCache);
        },

        savesignPostsInfo = function (data) {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "SignpostsInfoSave.aspx?spDescription=" + data + "&ts=" + noCache);
        },
        updateSignPostsInfo = function (id, description) {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "SignpostsInfoUpdate.aspx?sp=" + id + "&spDescription=" + description + "&ts=" + noCache);
        },
        savesignPostSchedule = function (spList) {
            return $.post(baseURL + "SaveSignpostUpdateList.aspx", spList);
        },

        getSPScheduleInfo = function () {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "GetSignPostChangeInfo.aspx?ts=" + noCache);
        },

        getSPSpecificInfo = function (spID) {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "GetSPSpecificInfo.aspx?sp=" + spID + "&ts=" + noCache);
        },

        publishSignpostToProd = function (spID) {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "PublishSignpost.aspx?sp=" + spID + "&ts=" + noCache);
        },
        validateEndDate = function (spID) {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "ValidateEndDate.aspx?sp=" + spID + "&ts=" + noCache);
        },
        unscheduleSignPost=function (spID) {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "UnpublishSignpost.aspx?sp=" + spID + "&ts=" + noCache);
        },
        deleteSignPost = function (spID) {
            var noCache = new Date().getTime();
            return $.getJSON(baseURL + "DeleteSignpost.aspx?sp=" + spID + "&ts=" + noCache);
        };



    return {
        getEvents: getEvents,
        getEventsBySortOrder:getEventsBySortOrder,
        getEvent: getEvent,
        getIVRFileForPublicEvent:getIVRFileForPublicEvent,
        getSpecialEventRoutes: getSpecialEventRoutes,
        getPreEventRouteDetails: getPreEventRouteDetails,
        getPreEventMessages:getPreEventMessages,
        getAllRoutes: getAllRoutes,
        getMainRoutes: getMainRoutes,
        updateRoutesStopsOrder:updateRoutesStopsOrder,
        getStops: getStops,
        getStopsSynchronously: getStopsSynchronously,
        getTempSpecialEventMessage: getTempSpecialEventMessage,
        getSpecialEventMessages: getSpecialEventMessages,
        saveMessage: saveMessage,
        createEvent: createEvent,
        createPreEvent:createPreEvent,
        saveStop: saveStop,
        shuttleSet: shuttleSet,
        checkEventRoutes: checkEventRoutes,
        deleteEvent: deleteEvent,
        consolidateSpecialEvent: consolidateSpecialEvent,
        finshSpecialEvent: finshSpecialEvent,
        generateVoiceFile: generateVoiceFile,
        speakIVRVoiceFile: speakIVRVoiceFile,
        updateEventSortOrder: updateEventSortOrder,
        publishToLive: publishToLive,
        deleteEventFromLiveAndDev: deleteEventFromLiveAndDev,
        saveMultipleStops: saveMultipleStops,
        getAllSignPostList: getAllSignPostList,
        savesignPostsInfo: savesignPostsInfo,
        updateSignPostsInfo: updateSignPostsInfo,
        savesignPostSchedule: savesignPostSchedule,
        getSPScheduleInfo: getSPScheduleInfo,
        getSPSpecificInfo: getSPSpecificInfo,
        publishSignpostToProd: publishSignpostToProd,
        unscheduleSignPost: unscheduleSignPost,
        deleteSignPost: deleteSignPost,
        validateEndDate: validateEndDate 
    };
} ();