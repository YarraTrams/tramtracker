﻿var my = my || {};

$(function () {
    var lastChecked = null;
    my.Route = function (route) {
        if (route.RouteNumber == 4)
            this.routeNumber = ko.observable("3a");
        else
            this.routeNumber = ko.observable(route.RouteNumber);
        this.description = ko.observable(route.Description);
    };

    my.RouteStop = function (stop) {
        var self = this;
        if (self.routeNumber == 4)
            self.routeNumber = ko.observable("3a");
        else
            self.routeNumber = ko.observable(stop.RouteNumber);
        self.trackerId = ko.observable(stop.TrackerID);
        self.stopSequence = ko.observable(stop.StopSequence);
        self.tramTrackerAvailable = ko.observable(stop.TramTrackerAvailable);
        self.hasShuttleTrams = ko.observable(stop.HasShuttleTrams);
        self.description = ko.observable(stop.Description);
        self.isUpStop = ko.observable(stop.IsUpStop);
        self.numberOfServices = ko.observable(stop.NumberOfServices);
        self.missingRoutes = ko.observable(stop.MissingRoutes);
        self.highlighted = ko.observable(false);
    };

    my.selectStopsVM = (function () {
       
        var eventId = ko.observable(''),
        rawStartDate = ko.observable(''),
        rawEndDate = ko.observable(''),
        routes = ko.observableArray([]),
        selectedRoute = ko.observable(),
        upStops = ko.observableArray([]),
        downStops = ko.observableArray([]),
        affectedStopsMismatches = ko.observableArray([]),
        allUpStopsSelected = ko.observable(false),
        allDownStopsSelected = ko.observable(false),
        lastSelectedStop = ko.observable(),
        selectedStops = [],

        startDate = ko.computed(function () {
            if (rawStartDate()) {
                var parsedStartDate = new Date(parseInt(rawStartDate().substr(6)));
                return dateFormat(parsedStartDate, "dddd, mmmm d, yyyy HH:MM");
            }
        }),

        endDate = ko.computed(function () {
            if (rawEndDate()) {
                var parsedEndDate = new Date(parseInt(rawEndDate().substr(6)));
                return dateFormat(parsedEndDate, "dddd, mmmm d, yyyy HH:MM");
            }
        }),

        eventDescription = ko.observable(),

        saveStop = function (stop, event) {

            if (stop.tramTrackerAvailable()) {
                stop.tramTrackerAvailable(false);
            }
            else {

                stop.tramTrackerAvailable(true);
            }
            
            //if (event.shiftKey && lastSelectedStop() && stop.isUpStop() == lastSelectedStop().isUpStop()) {
            if (event.shiftKey && stop.isUpStop() == lastSelectedStop().isUpStop()) {
                shiftSelectStops(stop, lastSelectedStop());
            }
            else {
                saveSelectedStop(stop);
            }

            lastSelectedStop(stop);

            return true;
        },

        /*setShuttleTram = function (stop) {
            my.dataService.shuttleSet(eventId(), stop.routeNumber(), stop.isUpStop(), stop.hasShuttleTrams(), stop.trackerId())
                    .done(function (data) {
                        if (data) {

                        }
                    });
            return true;
        },*/

        getRoutes = function () {
            my.dataService.getAllRoutes()
                    .done(function (data) {
                        if (data) {
                            var mappedRoutes = $.map(data, function (item) {
                                return new my.Route(item);
                            });
                            routes(mappedRoutes);
                        }
                    });
        },

     getSpecialEventStops = function (routeNo) {
         if (routeNo() == "3a")
             routeNo = 4;
         else
             routeNo = routeNo();

         $('#noStopSelectedAlert').hide();
         $('#loadingStopModal').modal('show');
         my.dataService.getStops(eventId(), routeNo)
                        .done(function (data) {
                            if (data.responseObject) {
                                var mappedUpStops = $.map(data.responseObject.UpStops, function (item) {
                                    return new my.RouteStop(item);
                                });

                                upStops(mappedUpStops);

                                var mappedDownStops = $.map(data.responseObject.DownStops, function (item) {
                                    return new my.RouteStop(item);
                                });

                                downStops(mappedDownStops);

                                setUpDownAllStopsSelected();

                                $('#upStops').tab('show');


                            }
                            $('#loadingStopModal').modal('hide');
                        });


     },

        //checks to ensure stops in both directions of a route have been selected
        getMismatches = function (data) {
            $('#noStopSelectedAlert').hide();
            var foundMismatches = [];
            $.each(data.responseObject, function (i, item) {

                my.dataService.getStopsSynchronously(eventId(), item.RouteNumber).done(function (data) {
                    if (data.responseObject) {

                        var numAffectedUpStops = 0;
                        var numAffectedDownStops = 0;
                        $.each(data.responseObject.UpStops, function (j, stop) {
                            if (stop.TramTrackerAvailable == false) {
                                numAffectedUpStops++;
                            }
                        });


                        $.each(data.responseObject.DownStops, function (j, stop) {
                            if (stop.TramTrackerAvailable == false) {
                                numAffectedDownStops++;
                            }
                        });


                        if (numAffectedUpStops == 0 && numAffectedDownStops > 0) {
                            if (item.RouteNumber == 4)
                                item.RouteNumber = "3a";
                            var upStopsMismatch = {
                                routeNo: item.RouteNumber,
                                isMissingUpStops: true,
                                isMissingDownStops: false
                            };

                            foundMismatches.push(upStopsMismatch);
                        }
                        else if (numAffectedDownStops == 0 && numAffectedUpStops > 0) {
                            if (item.RouteNumber == 4)
                                item.RouteNumber = "3a";
                            var downStopsMismatch = {
                                routeNo: item.RouteNumber,
                                isMissingUpStops: false,
                                isMissingDownStops: true
                            };

                            foundMismatches.push(downStopsMismatch);
                        }

                    }
                });
            });


            return foundMismatches;
        },

        enterMessagesClicked = function () {
            //$("#enterMsgButton").html("Loading...");
            $('#processingStopsModal').modal('show');
            my.dataService.checkEventRoutes(eventId()).done(function (data) {
                if (!data.isError) {
                    if (data.responseObject.length == 0) {
                        $('#processingStopsModal').modal('hide');
                        $('#noStopSelectedAlert').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Please ensure you have selected at least one stop in one route for this event.</div>");
                        $('#noStopSelectedAlert').show();

                    }
                    else {
                        //var misMatches = getMismatches(data);
                        //// $('#noStopSelectedAlert').hide();
                        //if (misMatches.length > 0) {
                        //    affectedStopsMismatches(misMatches);
                        //    $('#stopsMissingModal').modal('show');

                        //}
                        //else {
                        //$('#processingStopsModal').modal('show');
                        my.dataService.consolidateSpecialEvent(eventId())
                                    .done(function (data) {
                                        if (!data.isError) {
                                            //$('#processingStopsModal').modal('hide');
                                            //$('#noStopSelectedAlert').hide();                                          
                                            window.location = "specialeventcreate.step3.html?e=" + eventId();
                                        }
                                        /*else {
                                            $('#processingStopsModal').modal('hide');
                                            //$('#noStopSelectedAlert').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Error: " + data.responseObject.Errors[0].Message + "</div>");
                                            //$('#noStopSelectedAlert').show();
                                           

                                        }*/
                                    });
                        //}
                    }
                }
                $('#processingStopsModal').modal('hide');
                //$("#enterMsgButton").html("Enter Messages <span class='glyphicon glyphicon-chevron-right'></span>");
            });



        },

        modifyEventClicked = function () {
            window.location = "specialeventcreate.step1.html?e=" + eventId();
        },

        getEvent = function () {
            var e = getQueryStringVariable('e');
            my.dataService.getEvent(e)
                    .done(function (data) {
                        if (data.responseObject) {
                            eventId(data.responseObject.EventID);
                            rawStartDate(data.responseObject.EventStartDate);
                            rawEndDate(data.responseObject.EventEndDate);
                            eventDescription(data.responseObject.EventDesc);
                            getRoutes();
                        }
                    });

        },

        selectUpStops = function () {
            selectStops(upStops(), allUpStopsSelected());
            saveMultiplsStops(upStops()[0], upStops()[0].tramTrackerAvailable());
            return true;
        },

        selectDownStops = function () {
            selectStops(downStops(), allDownStopsSelected());
            return true;
        },

        selectStops = function (stops, isSelected) {
            selectedStops = [];
            ko.utils.arrayForEach(stops, function (stop) {
                stop.tramTrackerAvailable(isSelected);
                selectedStops.push(stop);
            });

            saveMultiplsStops(stops[0], isSelected);
        },

        setLastSelectedStopIndex = function (stop) {
            if (upStops().indexOf(lastSelectedStop) != -1) {
                lastSelectedStopIndex(upStops().indexOf(stop));
            }
            else {
                lastSelectedStopIndex(downStop().indexOf(stop));
            }
        },

        shiftSelectStops = function (currentStop, lastSelectedStop) {
            $('#loadingMissingRoutesModal').modal('show');

            var indexOfCurrentStop = upStops().indexOf(currentStop);
            var stopsToShiftSelect = null;

            if (indexOfCurrentStop != -1) {
                stopsToShiftSelect = upStops();
            }
            else {
                indexOfCurrentStop = downStops().indexOf(currentStop);
                if (indexOfCurrentStop != -1) {
                    stopsToShiftSelect = downStops();
                }
            }

            selectedStops = [];
            ko.utils.arrayForEach(stopsToShiftSelect, function (stop) {
                if ((stop.stopSequence() >= lastSelectedStop.stopSequence() && stop.stopSequence() <= currentStop.stopSequence()) ||
                    (stop.stopSequence() <= lastSelectedStop.stopSequence() && stop.stopSequence() >= currentStop.stopSequence())) {
                    stop.tramTrackerAvailable(lastSelectedStop.tramTrackerAvailable());
                    selectedStops.push(stop);
                }
            });
            saveMultiplsStops(currentStop, currentStop.tramTrackerAvailable());
        },

        saveMultiplsStops = function (stop, tramTrackerAvailable) {
            $('#loadingMissingRoutesModal').modal('show');

            var routeNumber = (stop.routeNumber() == "3a") ? 4 : stop.routeNumber();

            var stopList = "";
            ko.utils.arrayForEach(selectedStops, function (stop) {
                stopList += stop.trackerId() + ",";
            });

            my.dataService.saveMultipleStops(routeNumber, eventId(), stopList, !tramTrackerAvailable, stop.isUpStop())
                .done(function (data) {
                    if (data.responseObject) {
                        my.RouteStop(data.responseObject);
                        my.dataService.getStops(eventId(), data.responseObject.RouteNumber).done(function (data) {
                            if (data.responseObject) {
                                var mappedUpStops = $.map(data.responseObject.UpStops, function (item) {
                                    return new my.RouteStop(item);
                                });

                                upStops(mappedUpStops);
                                var mappedDownStops = $.map(data.responseObject.DownStops, function (item) {
                                    return new my.RouteStop(item);
                                });

                                downStops(mappedDownStops);

                                setUpDownAllStopsSelected();
                                $('#upStops').tab('show');
                                $('#loadingMissingRoutesModal').modal('hide');

                            }
                        });
                    }
                });
        },

        saveSelectedStop = function (stop) {
            $('#loadingMissingRoutesModal').modal('show');
            var routeNumber = (stop.routeNumber() == "3a") ? 4 : stop.routeNumber();
            my.dataService.saveStop(routeNumber, eventId(), stop.trackerId(), !stop.tramTrackerAvailable(), stop.isUpStop())
                .done(function (data) {
                    if (data.responseObject) {
                        my.RouteStop(data.responseObject);
                        my.dataService.getStops(eventId(), data.responseObject.RouteNumber).done(function (data) {
                            if (data.responseObject) {
                                var mappedUpStops = $.map(data.responseObject.UpStops, function (item) {
                                    return new my.RouteStop(item);
                                });
                                upStops(mappedUpStops);

                                var mappedDownStops = $.map(data.responseObject.DownStops, function (item) {
                                    return new my.RouteStop(item);
                                });
                                downStops(mappedDownStops);

                                setUpDownAllStopsSelected();
                                $('#upStops').tab('show');
                                $('#loadingMissingRoutesModal').modal('hide');

                            }
                        });
                    }
                });
        },

        setUpDownAllStopsSelected = function () {
            setAllStopsSelected(upStops(), allUpStopsSelected);
            setAllStopsSelected(downStops(), allDownStopsSelected);
        },

        setAllStopsSelected = function (stops, allStopsSelected) {
            if (stops.length == 0) {
                return;
            }

            var allSelected = true;
            ko.utils.arrayForEach(stops, function (stop) {
                allSelected = allSelected && !stop.tramTrackerAvailable();
            });
            allStopsSelected(allSelected);
        };

        selectedRoute.subscribe(function (newValue) {
            if (newValue) {
                var routeNumber = (newValue.routeNumber() == "3a") ? ko.observable(4) : ko.observable(newValue.routeNumber());
                getSpecialEventStops(routeNumber);
            }
        });

        return {
            getEvent: getEvent,
            eventId: eventId,
            startDate: startDate,
            endDate: endDate,
            eventDescription: eventDescription,
            routes: routes,
            getSpecialEventStops: getSpecialEventStops,
            selectedRoute: selectedRoute,
            upStops: upStops,
            downStops: downStops,
            saveStop: saveStop,
            enterMessagesClicked: enterMessagesClicked,
            modifyEventClicked: modifyEventClicked,
            affectedStopsMismatches: affectedStopsMismatches,
            selectUpStops: selectUpStops,
            selectDownStops: selectDownStops,
            allUpStopsSelected: allUpStopsSelected,
            allDownStopsSelected: allDownStopsSelected
        }
    })();
    my.selectStopsVM.getEvent();
    ko.applyBindings(my.selectStopsVM);
});