﻿var my = my || {};

$(function () {

    my.PreEventRoute = function (route) {
        var self = this;
        self.eventId = ko.observable(route.EventID);
        if (route.RouteNumber == 4)
            this.routeNumber = ko.observable("3a");
        else
            this.routeNumber = ko.observable(route.RouteNumber);
        self.routeDescription = ko.observable(route.Description);
    };
    my.PreEventMessages = function (msg) {
        var self = this;
        self.SMSMessage = ko.observable(msg.SMSMessage);
        self.IVRMessage = ko.observable(msg.FormattedIVRMessage);
       
    };

    my.Route = function (route) {
        var self = this;
        if (route.RouteNumber == 4)
            self.routeNumber = ko.observable("3a");
        else
            self.routeNumber = ko.observable(route.RouteNumber);
        self.eventId = ko.observable(route.EventID);
        self.upDestination = ko.observable(route.UpDestination);
        self.downDestination = ko.observable(route.DownDestination);
        self.routeDescription = ko.computed(function () {
            return self.routeNumber() + " : " + self.upDestination() + " - " + self.downDestination();
        });
    };

    my.RouteStop = function (stop) {
        var self = this;
        if (self.routeNumber == 4)
            self.routeNumber = ko.observable("3a");
        else
            self.routeNumber = ko.observable(stop.RouteNumber);
        self.trackerId = ko.observable(stop.TrackerID);
        self.stopSequence = ko.observable(stop.StopSequence);
        self.tramTrackerAvailable = ko.observable(stop.TramTrackerAvailable);
        self.hasShuttleTrams = ko.observable(stop.HasShuttleTrams);
        self.description = ko.observable(stop.Description);
        self.longDescription = ko.computed(function () {
            return self.trackerId() + " - " + self.description();
        });
        self.eventId = ko.observable(stop.EventID);
        self.imagePath = ko.computed(function () {
            if (stop.TramTrackerAvailable) {
                return 'images/tram_yes.gif';
            }
            else if (!self.TramTrackerAvailable) {
                return 'images/tram_no.gif';
            }
            else if (self.TramTrackerAvailable && self.HasShuttleTrams) {
                return 'images/tram_shuttle.gif';
            }
        });
    },

    my.RouteMessage = function (message) {
        var self = this;
        self.routeNumber = ko.computed(function () {
            return message.RouteNumbers == "0" ? "All Routes" : message.RouteNumbers;
        });
        self.smsMessage = ko.observable(message.SMSMessage);
        self.formattedIVRMessage = ko.observable(message.FormattedIVRMessage);
    },

    my.eventSummaryVM = (function () {
        var eventId = ko.observable(''),
        rawStartDate = ko.observable(''),
        rawEndDate = ko.observable(''),
        routes = ko.observableArray([]),
        preEventRoutes = ko.observableArray([]),
        preEventMessages = ko.observableArray([]),
        selectedRoute = ko.observable(),
        upStops = ko.observableArray([]),
        downStops = ko.observableArray([]),
        selectedMessage = ko.observable(''),
        selectedStop = ko.observable(''),
        stopMessages = ko.observableArray([]),
        startDate = ko.computed(function () {
            if (rawStartDate()) {
                var parsedStartDate = new Date(parseInt(rawStartDate().substr(6)));
                return dateFormat(parsedStartDate, "dddd, mmmm d, yyyy HH:MM");
            }
        });

        selectedRoute.subscribe(function (newValue) {
            
            if (newValue) {
                var routeNumber = 0;
                if(newValue.routeNumber() == "3a") 
                    routeNumber = 4;
                else
                    routeNumber=newValue.routeNumber();
                getSpecialEventStops(routeNumber);
            }
        }),

        endDate = ko.computed(function () {
            if (rawEndDate()) {
                var parsedEndDate = new Date(parseInt(rawEndDate().substr(6)));
                return dateFormat(parsedEndDate, "dddd, mmmm d, yyyy HH:MM");
            }
        });

        eventDescription = ko.observable().extend({ required: true }),
        isPublicHoliday = ko.observable(),
        isPreEvent=ko.observable(),
        ivrHolidayMessage = ko.observable(),
        smsHolidayMessage = ko.observable(),
        isEventFinalised = function () {
            if (isPreEvent()) {
                return true;
            }
            else if (isPublicHoliday() && ivrHolidayMessage() && smsHolidayMessage()) {
                return true;
            }
            else if (routes().length > 0) {
                return true;
            }
            else {
                return false;
            }

        },

        getRoutes = function () {
            my.dataService.getSpecialEventRoutes(eventId())
                    .done(function (data) {
                        if (data.responseObject) {
                            var mappedRoutes = $.map(data.responseObject, function (item) {
                                return new my.Route(item);
                            });
                            routes(mappedRoutes);
                        }
                    });
        },

      getPreEventRouteDetails = function () {
          
          my.dataService.getPreEventRouteDetails(eventId())
                  .done(function (data) {
                      if (data.responseObject) {
                          var mappedRoutes = $.map(data.responseObject, function (item) {
                              return new my.PreEventRoute(item);
                          });
                          preEventRoutes(mappedRoutes);
                          
                      }
                  });
      },

     getPreEventMessages = function () {
          my.dataService.getPreEventMessages(eventId())
                    .done(function (data) {
                        if (data.responseObject) {
                            var mappedMsgs = $.map(data.responseObject, function (item) {
                                return new my.PreEventMessages(item);
                            });
                            preEventMessages(mappedMsgs);
                        }
                        $('#loadingPreEventModal').modal('hide');
                    });
          
        },

     getSpecialEventStops = function (routeNo) {
         $('#loadingModal').modal('show');
         my.dataService.getStops(eventId(), routeNo)
                        .done(function (data) {
                            if (data.responseObject) {
                                var mappedUpStops = $.map(data.responseObject.UpStops, function (item) {
                                    return new my.RouteStop(item);
                                });

                                upStops(mappedUpStops);

                                var mappedDownStops = $.map(data.responseObject.DownStops, function (item) {
                                    return new my.RouteStop(item);
                                });

                                downStops(mappedDownStops);
                                $('#loadingModal').modal('hide');
                            }
                        });
     },

     displaySpecialEventMessage = function (stop) {
         var e = eventId();
         var stopNo = stop.trackerId();
         var routeNo = stop.routeNumber();
         selectedStop(stop);

         my.dataService.getSpecialEventMessages(e, stopNo, routeNo)
                    .done(function (data) {
                        if (data.responseObject) {
                            var mappedMessages = $.map(data.responseObject, function (item) {
                                return new my.RouteMessage(item);
                            });
                            stopMessages(mappedMessages);
                        }

                        $('#eventMessageModal').modal('show');
                    });


     },

        getEvent = function () {
            var e = getQueryStringVariable('e');
            my.dataService.getEvent(e)
                    .done(function (data) {
                        if (data.responseObject) {
                            eventId(data.responseObject.EventID);
                            rawStartDate(data.responseObject.EventStartDate);
                            rawEndDate(data.responseObject.EventEndDate);
                            eventDescription(data.responseObject.EventDesc);
                            isPublicHoliday(data.responseObject.IsPublicHoliday);
                            ivrHolidayMessage(data.responseObject.IVRHolidayMessage);
                            smsHolidayMessage(data.responseObject.SMSHolidayMessage);
                            isPreEvent(data.responseObject.IsPreEvent);
                            if (isPreEvent()) {
                                $('#loadingPreEventModal').modal('show');
                                getPreEventRouteDetails();
                                getPreEventMessages();
                            }
                            else {
                                getRoutes();
                            }
                            
                            
                            
                        }
                    })
        };

        return {
            getEvent: getEvent,
            eventId: eventId,
            startDate: startDate,
            endDate: endDate,
            eventDescription: eventDescription,
            isPublicHoliday: isPublicHoliday,
            isPreEvent:isPreEvent,
            ivrHolidayMessage: ivrHolidayMessage,
            smsHolidayMessage: smsHolidayMessage,
            routes: routes,
            preEventRoutes: preEventRoutes,
            preEventMessages:preEventMessages,
            selectedRoute: selectedRoute,
            upStops: upStops,
            downStops: downStops,
            selectedStop: selectedStop,
            displaySpecialEventMessage: displaySpecialEventMessage,
            stopMessages: stopMessages,
            isEventFinalised: isEventFinalised
        }
    })();
    my.eventSummaryVM.getEvent();
    ko.applyBindings(my.eventSummaryVM);
});