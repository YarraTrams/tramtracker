﻿var my = my || {};

$(function () {

    my.Event = function (data) {

        

        this.eventId = data.EventID;
        this.startDate = ko.computed(function () {
            var startDate = new Date(parseInt(data.EventStartDate.substr(6)));
            return dateFormat(startDate, "dddd, d mmmm yyyy HH:MM");
        });

        this.endDate = ko.computed(function () {
            var endDate = new Date(parseInt(data.EventEndDate.substr(6)));
            return dateFormat(endDate, "dddd, d mmmm yyyy HH:MM");
        });

        this.eventSummaryUrl = ko.computed(function () {
            return "SpecialEventSummary.html?e=" + data.EventID;
        });

        this.eventReviewUrl = ko.computed(function () {
            return "SpecialEventReview.html?e=" + data.EventID;
        });

        this.editEventUrl = ko.computed(function () {
            return "specialeventcreate.step1.html?e=" + data.EventID;
        });

        this.editStopsUrl = ko.computed(function () {
            return "specialeventcreate.step2.html?e=" + data.EventID;
        });

        this.editMessagesUrl = ko.computed(function () {
            return "specialeventcreate.step3.html?e=" + data.EventID;
        });
       
        this.testUrl = ko.computed(function () {
            return "http://tst-gen-web-01/SpecialEventTestPidApp";
        });

        this.editPreEventUrl = ko.computed(function () {
            return "createPreEvent.html?e=" + data.EventID;
        });

        this.publishEventUrl = ko.computed(function () {
            return "publishSettings.html";
        });

        this.eventDescription = ko.observable(data.EventDesc);
        this.createdBy = ko.observable(data.CreatedBy);
        this.lastUpdatedBy = ko.observable(data.UpdatedBy);
        this.lastPublishedBy = ko.observable(data.LastUnPublishedBy);
        this.lastUnPublishedBy = ko.observable(data.LastUnPublishedBy);
        this.isPublicHoliday = ko.observable(data.IsPublicHoliday);
        this.isPreEvent = ko.observable(data.IsPreEvent);
        this.isPublished = ko.observable(data.IsPublished);

        //this.detailsEnabled = ko.observable();

        /*this.enablePublishedDetails = function () {
            this.detailsEnabled(true);
          
        };

        this.disablePublishedDetails = function () {
            this.detailsEnabled(false);

        };*/
        

        this.lastUpdate = ko.computed(function () {
            var updatedDate = new Date(parseInt(data.LastUpdate.substr(6)));
            return dateFormat(updatedDate, "dddd, d mmmm yyyy HH:MM");
        });

        
        this.lastPublishedUpdate = ko.computed(function () {
                var updatedDate = new Date(parseInt(data.LastPublishedUpdate.substr(6)));
                return dateFormat(updatedDate, "dddd, d mmmm yyyy HH:MM");
        });
       

        this.lastUnPublishedUpdate = ko.computed(function () {
            var updatedDate = new Date(parseInt(data.LastUnPublishedUpdate.substr(6)));
            if (dateFormat(updatedDate, "yyyy") == "1")
                return " -";
            else
                return dateFormat(updatedDate, "dddd, d mmmm yyyy HH:MM");
           });
        
        
        
           


    };

    my.eventListVM = (function () {
        
        var startDate = ko.observable(new Date()),
            endDate = ko.observable(new Date().addDays(14)),
            events = ko.observableArray([]),
            eventToDelete = ko.observable(),
            setEventToDelete = function (event) {
                eventToDelete(event);
            },
            
        
            
        deleteEvent = function () {
              my.dataService.deleteEvent(eventToDelete().eventId)
                            .done(function (data) {
                                if (!data.isError) {
                                    $('#deleteEventModal').modal('hide')
                                    my.eventListVM.getEventList();
                                }
                            });

        },

       

        getEventList = function () {
            var formattedStartDate = dateFormat(startDate(), "dd-mm-yyyy");
            var formattedEndDate = dateFormat(endDate(), "dd-mm-yyyy");
            $('#set-filter-dates').val('Filtering..');
            my.dataService.getEvents(formattedStartDate, formattedEndDate)
                                .done(function (data) {
                                    if (data.responseObject) {
                                        var mappedEvents = $.map(data.responseObject, function (item) {
                                            //alert((JSON.stringify(item)));
                                            return new my.Event(item);
                                           
                                        });
                                       
                                        events(mappedEvents);
                                      
                                        
                                    }
                                    $('#set-filter-dates').val('Filter Events');
                                });
        };
        

        currentYear = ko.computed(function () {
            return new Date().getFullYear();
        });

      
        return {
            startDate: startDate,
            endDate: endDate,
            events: events,
            currentYear:currentYear,
            getEventList: getEventList,
            setEventToDelete: setEventToDelete,
            eventToDelete: eventToDelete,
            deleteEvent: deleteEvent
        }
    })();

   ko.bindingHandlers.uiSortableList = {
        init: function (element, valueAccessor, allBindingsAccesor, context) {
            var $element = $(element),
              list = valueAccessor();
            $element.sortable({
                update: function (event, ui) {
                    var item = ko.dataFor(ui.item[0]),
                     newIndex = ko.utils.arrayIndexOf(ui.item.parent().children(), ui.item[0]);


                    if (newIndex >= list().length) 
                        newIndex = list().length - 1;
                    if (newIndex < 0) 
                        newIndex = 0;
                    //$('#eventsTB').hide();
                    ui.item.remove();
                    list.remove(item);
                    list.splice(newIndex, 0, item);
                }
            });
        }
   };

   ko.bindingHandlers.bootstrapPopover = {
       init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
           var options = valueAccessor();
           var defaultOptions = {};
           options = $.extend(true, { trigger: "hover" }, defaultOptions, options);
           $(element).popover(options);
       }
   };
    
   /* //connect items with observableArrays
    ko.bindingHandlers.uiSortableList = {
        init: function (element, valueAccessor) {
            var list = valueAccessor();
            $(element).sortable({
                update: function (event, ui) {
                    //retrieve our actual data item
                    //alert(JSON.stringify(ui.item[0]));
                    var item = ui.item[0];//tmplItem().data;
                    //figure out its new position
                    var position = ko.utils.arrayIndexOf(ui.item.parent().children(), ui.item[0]);
                    //remove the item and add it back in the right spot
                    alert(position);
                    if (position >= 0) {
                        ui.item.remove();
                        list.remove(item);
                        list.splice(position, 0, item);
                    }
                }
            });
        }
    };*/

    editEventList = function () {
        $('#eventsTB').hide();
        $('#sortable-events').show();
        $('#editEventList').hide();
        $('#saveCancelButton').show();
        $('#set-filter-dates').prop('disabled', true);
    };
    
    cancelSorting = function () {
        $('#eventsTB').show();
        $('#sortable-events').hide();
        $('#editEventList').show();
        $('#saveCancelButton').hide();
        $('#set-filter-dates').prop('disabled', false);

    };
    my.eventListVM.getEventList();
    ko.applyBindings(my.eventListVM);
    
});