﻿var my = my || {};

$(function () {

    my.Route = function (route) {
        if (route.RouteNumber == 4)
            this.routeNumber = ko.observable("3a");
        else
            this.routeNumber = ko.observable(route.RouteNumber);
        this.description = ko.observable(route.Description);
       
        
    };
   
    my.eventCreateVM = (function () {
        var hours = ko.computed(function () {
            var hoursArray = ko.observableArray([]);
            for (var h = 0; h < 24; h++) {
                if (h < 10)
                    h = "0" + h;
                hoursArray.push(h);
            }
            return hoursArray;
        }),

        mins = ko.computed(function () {
            var minsArray = ko.observableArray([]);

            for (var m = 0; m < 60; m += 5) {
                var minString = "";
                if (m < 10)
                    minString = "0" + m;
                else
                    minString = m;
                minsArray.push(minString);
            }
            return minsArray;
        }),

        eventId = ko.observable(''),
        eventDescription = ko.observable('').extend({ required: { message: "Please include an event description" } }),
        smsPreEventMsg = ko.observable('').extend({ required: { message: "Please include an SMS message" } }),
        ivrPreEventMsg = ko.observable('').extend({ required: { message: "Please include an IVR message" } }),
        ivrFileName = ko.observable('').extend({ required: { message: "Please generate voice file" } }),
        routes = ko.observableArray([]),
        startDate = ko.observable(''),
        startHours = ko.observable('').extend({ required: { message: "Please enter the start hour"} }),
        startMins = ko.observable('').extend({ required: { message: "Please enter the start minute" } }),
        selectedRoute = ko.observable([]),
        endDate = ko.observable(''),
        endHours = ko.observable('').extend({ required: { message: "Please enter the end hour" } }),
        endMins = ko.observable('').extend({ required: { message: "Please enter the end minute" } }),
        isPreEvent = ko.observable(),

        validationGroup = [
            eventDescription,
            smsPreEventMsg,
            ivrPreEventMsg,
            selectedRoute,
            startHours,
            startMins,
            endHours,
            endMins
        ],

        serverSideError = ko.observable(),
        validationErrors = ko.validation.group(validationGroup),
        smsMsgCount = ko.computed(function(){
            var countNum = 117 - smsPreEventMsg().length;
            return countNum;
        }),


        /*** 
          function() to get Main Routes, pre-event Route Details,pre-event messages if an event id exists
          or else just get the main routes
        ***/
        getRoutes = function () {
            
             var e = getQueryStringVariable('e');
             if (e) {
                
                my.dataService.getEvent(e)
                    .done(function (data) {
                        if (data.responseObject) {
                            eventId(data.responseObject.EventID);
                            startDate(new Date(parseInt(data.responseObject.EventStartDate.substr(6))));
                            endDate(new Date(parseInt(data.responseObject.EventEndDate.substr(6))));
                            startHours(startDate().getHours());
                            startMins(startDate().getMinutes());
                            endHours(endDate().getHours());
                            endMins(endDate().getMinutes());
                            eventDescription(data.responseObject.EventDesc);
                            isPreEvent(data.responseObject.IsPreEvent);
                            if (isPreEvent) {
                                getMainRoutes();
                                getPreEventRouteDetails();
                                getPreEventMessages();
                            }

                        }
                       
                    });
               
            }
             else {
                 getMainRoutes();
             }
            
          
            
        },


        /***
          function() to get Main Routes
        ***/
        getMainRoutes = function () {
            $("#loadingPreEventMsgModal").modal('show');

            my.dataService.getMainRoutes()
                   .done(function (data) {
                       if (data) {
                           var mappedRoutes = $.map(data, function (item) {
                               return new my.Route(item);
                           });
                           routes(mappedRoutes);
                       }
                       $("#loadingPreEventMsgModal").modal('hide');
                   });
           
        },

        /***
          function() to get pre-event Route Details
        ***/
        getPreEventRouteDetails = function () {
            my.dataService.getPreEventRouteDetails(eventId())
                    .done(function (data) {
                        var selectedOptionData = [];
                        if (data.responseObject) {
                            for(var i=0;i<data.responseObject.length;i++){
                                selectedOptionData[i] = data.responseObject[i].RouteNumber;
                                //alert(selectedOptionData[i]);
                                if (selectedOptionData[i] == 4)
                                    selectedOptionData[i] = "3a";
                              
                            }
                            selectedRoute(selectedOptionData);
      
                        }
                    });
        },

      
      /*** 
           function() to get pre-event messages 
           or else just get the main routes
       ***/
      getPreEventMessages = function () {
          my.dataService.getPreEventMessages(eventId())
                    .done(function (data) {
                        if (data.responseObject) {
                            smsPreEventMsg(data.responseObject[0].SMSMessage);
                            ivrPreEventMsg(data.responseObject[0].FormattedIVRMessage);
                            ivrFileName(data.responseObject[0].IVRMessage);
                            
                        }
                    });
      },

      /*
       * Remove special characters from text input.
       */
      removeMsWordChars = function (text) {
            var s = text;
            
            if (s) {
                // smart single quotes and apostrophe
                s = s.replace(/[\u2018\u2019\u201A]/g, "\'");
                // smart double quotes
                s = s.replace(/[\u201C\u201D\u201E]/g, "\"");
                // ellipsis
                s = s.replace(/\u2026/g, "...");
                // dashes
                s = s.replace(/[\u2013\u2014]/g, "-");
                // circumflex
                s = s.replace(/\u02C6/g, "^");
                // open angle bracket
                s = s.replace(/\u2039/g, "<");
                // close angle bracket
                s = s.replace(/\u203A/g, ">");
                // spaces
                s = s.replace(/[\u02DC\u00A0]/g, " ");
                // enter or newline
                s = s.replace(/\u000a/g, "");
            }
            return s;
      },

      /*** 
             function() to save the generated voice file
      ***/
      saveGeneratedIVRFile = function (message) {
          $('#saveGeneratedIVRFile').text('Saving...');
          $(".validationMessage").css({ "color": " Red" });
          var e = getQueryStringVariable('e');
          if (e) {
              var ivrGenData = {
                  ivrMsg: message.ivrPreEventMsg(),
                  e: e
              };
              my.dataService.generateVoiceFile(ivrGenData)
                      .done(function (data) {
                          var jsonData = JSON.parse(data);
                          //alert(jsonData.responseString);
                          if (jsonData.isError) {
                             
                              $('#emptyIVRMessage').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + jsonData.responseObject + "</div>");
                             
                              $('#emptyIVRMessage').show();
                              setTimeout(function () {
                                  $('#emptyIVRMessage').hide();
                              }, 8000);

                              $('#saveGeneratedIVRFile').text('Save Changes');

                          }
                          else {
                              ivrFileName(jsonData.responseObject);
                              $('#saveGeneratedIVRFile').text('Save Changes');
                              $("#voiceFileModal").modal('hide');

                          }
                      });
          }
          else {
              var ivrGenData = {
                  ivrMsg: message.ivrPreEventMsg()
              };
              my.dataService.generateVoiceFile(ivrGenData)
                      .done(function (data) {
                          var jsonData = JSON.parse(data);
                          if (jsonData.isError) {
                              //serverSideError(data.responseString);
                              
                              if (jsonData.responseString != null || jsonData.responseString != "") {

                                  $('#emptyIVRMessage').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + jsonData.responseString + "</div>");
                              }
                              else {
                                  $('#emptyIVRMessage').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + jsonData.responseObject + "</div>");
                              }
                              $('#emptyIVRMessage').show();
                              setTimeout(function () {
                                  $('#emptyIVRMessage').hide();
                              }, 8000);


                              $('#saveGeneratedIVRFile').text('Save Changes');

                          }
                          else {
                              ivrFileName(jsonData.responseObject);
                              $('#saveGeneratedIVRFile').text('Save Changes');
                              $("#voiceFileModal").modal('hide');

                          }
                      });
          }

      },

        
        /*** 
               function() to save the pre-event details
        ***/
      createPreEvent = function () {
           
            
            //$("#yesNoModal").modal('hide');
            $(".validationMessage").css({ "color": " Red" });

            var formattedStartDate = dateFormat(startDate(), "dd-mm-yyyy");
            var formattedEndDate = dateFormat(endDate(), "dd-mm-yyyy");
            smsPreEventMsg(removeMsWordChars(smsPreEventMsg()));

            console.log("eventDescription : " + eventDescription());
            console.log("formattedStartDate : " + formattedStartDate);
            console.log("startHours : " + startHours());
            console.log("startMins : " + startMins());
            console.log("formattedEndDate : " + formattedEndDate);
            console.log("endHours : " + endHours());
            console.log("endMins : " + endMins());
            console.log("Routes : " + selectedRoute());
            console.log("SMS msg : " + smsPreEventMsg());
            console.log("IVR msg : " + ivrPreEventMsg());
            console.log("ivrFileName : " + ivrFileName());
            var selectedRouteLocal = selectedRoute;
            var event = {
                eventId: eventId(),
                txtdescription: eventDescription(),
                txtIVR: ivrPreEventMsg(),
                ivrFileName: ivrFileName(),
                txtSMS: smsPreEventMsg(),
                startdate: formattedStartDate,
                enddate: formattedEndDate,
                selectedRoute: selectedRouteLocal,
                stHr: startHours(),
                stMn: startMins(),
                endHr: endHours(),
                enMn: endMins()
            };

            if (selectedRoute().length == 0) {
                //$("#creatingPreEventMsgModal").modal('hide');
                serverSideError("Please ensure to select atleast one route");
                $('#server-validation-errors').show();
                setTimeout(function () {
                    $('#server-validation-errors').hide();
                }, 5000);
                
            }
            if (validationErrors().length == 0) {
                if (ivrFileName() == "" || ivrFileName() == null) {

                    serverSideError("Please ensure to generate voice file");
                    $('#server-validation-errors').show();
                    setTimeout(function () {
                        $('#server-validation-errors').hide();
                    }, 5000);
                        
                }

                else {
                    $('#createPreEventButton').html('Creating...');
                    var ivrGenData = {
                        ivrMsg: ivrPreEventMsg(),
                        e: eventId()
                    };
                    my.dataService.generateVoiceFile(ivrGenData)
                            .done(function (data) {
                                  var jsonData = JSON.parse(data);
                                  //alert(jsonData.responseString);
                                  if (jsonData.isError) {

                                      serverSideError(jsonData.responseObject);
                                      $('#server-validation-errors').show();
                                      setTimeout(function () {
                                          $('#server-validation-errors').hide();
                                      }, 5000);

                                  }
                                  else {
                                      ivrFileName(jsonData.responseObject);
                                      

                                  }
                            });
                    event = {
                        eventId: eventId(),
                        txtdescription: eventDescription(),
                        txtIVR: ivrPreEventMsg(),
                        ivrFileName: ivrFileName(),
                        txtSMS: smsPreEventMsg(),
                        startdate: formattedStartDate,
                        enddate: formattedEndDate,
                        selectedRoute: selectedRouteLocal,
                        stHr: startHours(),
                        stMn: startMins(),
                        endHr: endHours(),
                        enMn: endMins()
                    };
                     my.dataService.createPreEvent(event)
                                .done(function (data) {
                                    var jsonData = JSON.parse(data);
                                    if (!jsonData.isError) {
                                        eventId(jsonData.responseObject.EventID);
                                        window.location = "SpecialEventSummary.html?e=" + eventId();

                                    }
                                    else {
                                        serverSideError(jsonData.responseString);
                                        $('#server-validation-errors').show();
                                        setTimeout(function () {
                                            $('#server-validation-errors').hide();
                                        }, 5000);
                                        $('#createPreEventButton').html("Create Pre-Event <span class='glyphicon glyphicon-chevron-right'></span>");
                                    }
                                });
                      
                    }
              }
            else {

                validationErrors.showAllMessages(true);
                $('#createPreEventButton').html("Create Pre-Event <span class='glyphicon glyphicon-chevron-right'></span>");
              }
            
            

          };


        return {
            hours: hours,
            mins: mins,
            getRoutes: getRoutes,
            isPreEvent:isPreEvent,
            eventDescription: eventDescription,
            smsPreEventMsg:smsPreEventMsg,
            ivrPreEventMsg: ivrPreEventMsg,
            ivrFileName:ivrFileName,
            routes: routes,
            selectedRoute: selectedRoute,
            startDate: startDate,
            startHours: startHours,
            startMins: startMins,
            endDate: endDate,
            endHours: endHours,
            endMins: endMins,
            createPreEvent: createPreEvent,
            validationErrors: validationErrors,
            saveGeneratedIVRFile:saveGeneratedIVRFile,
            serverSideError: serverSideError,
            smsMsgCount: smsMsgCount
        }
    })();

    ko.validation.configure({
        registerExtenders: true,
        messagesOnModified: true,
        insertMessages: true,
        parseInputAttributes: true,
        messageTemplate: null
    });


    /*** 
       function() to clear all the message text areas
      ***/
    clearAllMsgClicked = function (message) {
        message.ivrPreEventMsg("");
        message.smsPreEventMsg("");
    };

    /*** 
      function() to open yes/no modal popup
     ***/
    createEvent = function () {
        
        $("#yesNoModal").modal('show');
    };


    /*** 
      function() to open generate voice file modal popup
     ***/
    generateVoiceFile = function () {
        $("#yesNoModal").modal('hide');
        $("#voiceFileModal").modal('show');


    };


    /*** 
      function() to speak the text IVR message
     ***/
    speakIVRMessage = function (message) {
        $('#speakIVRMessage').text('Speaking...');
        var msg = new SpeechSynthesisUtterance();
        var voices = window.speechSynthesis.getVoices();
        /* msg.voice = voices[10]; // Note: some voices don't support altering params
        msg.voiceURI = 'native';
        msg.volume = 1; // 0 to 1
        msg.rate = 1; // 0.1 to 10
        msg.pitch = 2; //0 to 2 */
        if (message.ivrPreEventMsg() != null || message.ivrPreEventMsg()!="")
            msg.text = message.ivrPreEventMsg();
        else
            msg.text="Please enter the IVR message"
        msg.lang = 'en-US';
        msg.onend = function (e) {
            console.log('Finished in ' + event.elapsedTime + ' seconds.');
            $('#speakIVRMessage').text('Speak');
        };
        speechSynthesis.speak(msg);
        

        
        
        /*my.dataService.speakIVRVoiceFile(message.ivrPreEventMsg())
                    .done(function (data) {
                        $('#speakIVRMessage').text('Speak');
                    });*/

    };

    /*** 
       function() to clear IVR textarea message from the generate voice file IVR textarea
      ***/
    clearIVRMsgBoxes = function (message) {
        message.ivrPreEventMsg("");
    };


    ko.bindingHandlers.smsMsgLimitCharacters = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            element.value = element.value.substr(0, valueAccessor());
            allBindingsAccessor().value(element.value.substr(0, valueAccessor()));
        }
    };
    

    my.eventCreateVM.getRoutes();
    ko.applyBindings(my.eventCreateVM);
});