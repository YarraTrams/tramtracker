﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Xml.Linq;
using Newtonsoft.Json;


public partial class controllers_SpecialEventLoad : System.Web.UI.Page
{
    ResponseHelper r = new ResponseHelper();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();

            if (Request.QueryString["e"] == null || Request.QueryString["e"] == "")
                throw new Exception("Unable to load Special Event");

            SpecialEvent se = new SpecialEvent();

            int eventID = Convert.ToInt32(Request.QueryString["e"]);
            se.EventID = eventID;
            se.Load();
            r.responseObject = se;

        }
        catch (Exception ex)
        {
            r.isError = true;
            r.responseObject = ex;
        }
        finally
        {
            Response.Write(JsonConvert.SerializeObject(r));
        }
        

    }
}
