﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class controllers_PrioritizeEvents : System.Web.UI.Page
{
    ResponseHelper r = new ResponseHelper();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();

            int eventID = Convert.ToInt32(Request.QueryString["e"]);
            int sortOrder = Convert.ToInt32(Request.QueryString["order"]);

            SpecialEvent se = new SpecialEvent();
            se.UpdatePriority(eventID, sortOrder);
           
            se.UpdateStatusAfterOrdering(eventID);
            r.responseObject=se;
            r.isError = false;
        }
        catch (Exception ex)
        {
            r.responseObject = ex;
            r.isError = true;
        }
        finally
        {
            Response.Write(JsonConvert.SerializeObject(r));
        }
    }
}