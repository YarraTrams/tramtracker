﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Newtonsoft.Json;


public partial class controllers_StopsLoad : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ResponseHelper r = new ResponseHelper();

        RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();

        try
        {
            if (Request.QueryString["r"] == null || Request.QueryString["r"] == "")
                throw new Exception("Please ensure you have selected a valid route");

            if (Request.QueryString["e"] == null || Request.QueryString["e"] == "")
                throw new Exception("Unable to load Special Event");

            SpecialEvent se = new SpecialEvent();

            int eventID = Convert.ToInt32(Request.QueryString["e"]);
            se.EventID = eventID;

            se.Load();

            short routeNo = (short)Convert.ToInt16(Request.QueryString["r"].ToString());
            Route rt = new Route();
            rt.RouteNumber = routeNo;
            rt.StartTime = se.EventStartDate;
            rt.EndTime = se.EventEndDate;
            rt.EventID = eventID;

            rt.LoadUpStops();
            rt.LoadDownStops();

            r.responseObject = rt;

        }
        catch (Exception ex)
        {
            r.isError = true;
            r.responseObject = ex;
        }
        finally
        {
            Response.Write(JsonConvert.SerializeObject(r));
        }

    }
}
