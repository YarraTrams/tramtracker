﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class controllers_GetPreEventMessages : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       ResponseHelper r = new ResponseHelper();
        try
        {
            int _eventid = Convert.ToInt32(Request["e"]);
            
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();

            List<MessagesSummary> summaries = MessagesSummary.LoadPreEventMessagesSummary(_eventid);

            r.responseObject = summaries;


        }
        catch (Exception ex)
        {
            r.isError = true;
            r.responseObject = ex;
        }
        finally
        {
            Response.Write(JsonConvert.SerializeObject(r));
        }
        
    }
   
}