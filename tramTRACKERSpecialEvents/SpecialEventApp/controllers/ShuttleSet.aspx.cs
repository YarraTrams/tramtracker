﻿using System;
using System.Collections;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Xml.Linq;
using Newtonsoft.Json;

public partial class controllers_ShuttleSet: System.Web.UI.Page
{
    ResponseHelper r = new ResponseHelper();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();

            int eventID = Convert.ToInt32(Request.QueryString["e"]);
            short routeNo = Convert.ToInt16(Request.QueryString["r"]);
            short stopNo = Convert.ToInt16(Request.QueryString["s"]);
            bool isUpStop = Convert.ToBoolean(Request.QueryString["isup"]);
            bool isShuttleTram = Convert.ToBoolean(Request.QueryString["ischecked"]);

            Stop s = new Stop();
            s.EventID = eventID;
            s.RouteNumber = routeNo;
            s.TrackerID = Convert.ToInt32(stopNo);
            s.IsUpStop = isUpStop;
            s.HasShuttleTrams = isShuttleTram;
            s.UpdateHasShuttles();
            r.responseObject = s;
        }
        catch (Exception ex)
        {
            r.responseObject = ex;
        }
        finally
        {
            Response.Write(JsonConvert.SerializeObject(r));
        }
    }
}
