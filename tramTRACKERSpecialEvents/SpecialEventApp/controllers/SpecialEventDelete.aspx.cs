﻿using System;
using System.Collections;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Xml.Linq;
using Newtonsoft.Json;

public partial class controllers_SpecialEventDelete : System.Web.UI.Page
{
    ResponseHelper r = new ResponseHelper();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();

            int eventID = Convert.ToInt32(Request.QueryString["e"]);

            SpecialEvent se = new SpecialEvent();
            se.EventID = eventID;
            se.Delete();
            r.isError = false;
        }
        catch (Exception ex)
        {
            r.responseObject = ex;
            r.isError = true;
        }
        finally
        {
            Response.Write(JsonConvert.SerializeObject(r));
        }
    }
}
