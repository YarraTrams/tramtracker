﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Speech.Synthesis;
using System.Diagnostics;
using System.Threading;
using System.Speech.AudioFormat;


public partial class controllers_GenerateVoiceFile : System.Web.UI.Page
{
    public static int _speechRate = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["_speechRate"]);
    public static int _speechVolume = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["_speechVolume"]);
    public string destination1File = System.Configuration.ConfigurationManager.AppSettings["destination1File"];
    public string destination2File = System.Configuration.ConfigurationManager.AppSettings["destination2File"];
    public string destination3File = System.Configuration.ConfigurationManager.AppSettings["destination3File"];
    public string destination4File = System.Configuration.ConfigurationManager.AppSettings["destination4File"];
    public string destination5File = System.Configuration.ConfigurationManager.AppSettings["destination5File"];
    public string tempWavFilePath = System.Configuration.ConfigurationManager.AppSettings["tempWavFilePath"];
    protected void Page_Load(object sender, EventArgs e)
    {

        string waveFilePath = System.Configuration.ConfigurationManager.AppSettings["waveFilePath"];
        string voxFilePath = System.Configuration.ConfigurationManager.AppSettings["voxFilePath"];
        string fileName = "";
        bool isCopyVoxFiles = false;

        ResponseHelper r = new ResponseHelper();
        try
        {
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();
            //Pre-Event/Public Event with eventID
            if (Request.Form["e"] != null && Request.Form["e"].Length > 0)
            {
                //Public Event with eventID
                if (Request.Form["checkHoliday"] != null && Request.Form["checkHoliday"] != "")
                {
                    Debug.WriteLine("checkHoliday:" + Request.Form["checkHoliday"]);
                    int _eventid = Convert.ToInt32(Request.Form["e"]);
                    Debug.WriteLine("Event ID: " + _eventid);

                    List<MessagesSummary> summaries = MessagesSummary.LoadPublicEventMessagesSummary(_eventid);

                    string[] fN = summaries[0].IVRMessage.Split('.');
                    Debug.WriteLine(fN[0]);
                    fileName = fN[0];
                }
                else //Pre-Event with eventID
                {
                    int _eventid = Convert.ToInt32(Request.Form["e"]);
                    Debug.WriteLine("Event ID: " + _eventid);
                    List<MessagesSummary> summaries = MessagesSummary.LoadPreEventMessagesSummary(_eventid);

                    string[] fN = summaries[0].IVRMessage.Split('.');
                    Debug.WriteLine(fN[0]);
                    fileName = fN[0];
                }




            }
            //Special event
            else if (Request.Form["msgId"] != null && Request.Form["msgId"] != "")
            {
                Debug.WriteLine("MessageID:" + Request.Form["msgId"]);
                MessagesSummary ms = new MessagesSummary();
                ms.GetTempIVRFileNameID();
                fileName = "TTS00" + ms.tempIVRFileNameID;
                Debug.WriteLine("fileName for the special event:" + fileName);

            }


            //Pre-Event/Public Event/special Event(new event)
            else
            {

                MessagesSummary ms = new MessagesSummary();
                ms.GetLastMessageID();
                fileName = "TTS00" + ms.tempMessageID;


            }



            //now got the filename from summaries[0].IVRMessage
            //next convert text to wav and then wav to vox file
            if (string.IsNullOrEmpty(Request["ivrMsg"]) || Request["ivrMsg"] == "")
            {
                r.isError = true;
                r.responseObject = "Unsuccessful";
                r.responseString = "Please include an IVR";
            }
            else
            {
                Thread t = new Thread(() =>
                {
                    Debug.WriteLine("ivrMsg: " + Request["ivrMsg"]);



                    string textToConvert = Request["ivrMsg"];

                    //text to wav 
                    waveFilePath = waveFilePath + fileName + ".wav";
                    try
                    {
                        convertTextToWave(textToConvert, waveFilePath);

                        //wav to vox
                        voxFilePath = voxFilePath + fileName + ".vox";
                        convertWaveToVox(waveFilePath, voxFilePath);
                    }
                    catch (Exception ex)
                    {
                        r.isError = true;
                        r.responseObject = ex.ToString();
                        return;
                    }
                });

                t.Start();

                t.Join();

                //using (var reader = new WaveFileReader("input.wav"))
                //{
                //    var newFormat = new WaveFormat(8000, 16, 1);
                //    using (var conversionStream = new WaveFormatConversionStream(newFormat, reader))
                //    {
                //        WaveFileWriter.CreateWaveFile("output.wav", conversionStream);
                //    }
                //}

                Thread.Sleep(1000);

                isCopyVoxFiles = copyVoxFiles(voxFilePath, fileName + ".vox");
                if (isCopyVoxFiles)
                {
                    r.isError = false;
                    r.responseObject = fileName + ".vox";
                }
                else
                {
                    r.isError = true;
                    r.responseObject = "Something went wrong while copying the voice file to the targeted path";
                }


            }

        }
        catch (Exception ex)
        {
            r.isError = true;
            r.responseObject = ex.Message;
        }
        finally
        {
            Response.Write(JsonConvert.SerializeObject(r));
        }







    }

    //Copy Vox file to 3 different location specific location
    private bool copyVoxFiles(string voxFilePath, string fileName)
    {
        bool isCopyVoxFiles = true;

        if (!System.IO.Directory.Exists(destination1File) || !System.IO.Directory.Exists(destination2File) || !System.IO.Directory.Exists(destination3File))
        {
            isCopyVoxFiles = false;
        }
        else
        {
            destination1File = destination1File + @"\" + fileName;
            destination2File = destination2File + @"\" + fileName;
            destination3File = destination3File + @"\" + fileName;
            destination4File = destination4File + @"\" + fileName;
            destination5File = destination5File + @"\" + fileName;


            // To copy a file to another location and 
            // overwrite the destination file if it already exists.
            System.IO.File.Copy(voxFilePath, destination1File, true);
            System.IO.File.Copy(voxFilePath, destination2File, true);
            System.IO.File.Copy(voxFilePath, destination3File, true);
            System.IO.File.Copy(voxFilePath, destination4File, true);
            System.IO.File.Copy(voxFilePath, destination5File, true);

        }


        return isCopyVoxFiles;

    }

    private void convertTextToWave(string textToConvert, string waveFilePath)
    {
        SpeechSynthesizer synth = new SpeechSynthesizer();
        synth.Volume = _speechVolume;
        synth.Rate = _speechRate;

        // Configure the audio output.
        synth.SetOutputToWaveFile(waveFilePath);

        //get details of all the installed voices
        /*foreach (InstalledVoice voice in synth.GetInstalledVoices())
        {
            VoiceInfo info = voice.VoiceInfo;
            Debug.WriteLine(" Voice Name: " + info.Name);
        }*/


        // Speak the prompt
        synth.Speak(textToConvert);
        synth.SetOutputToNull();
    }

    private void convertWaveToVox(string waveFPath, string voxFPath)
    {
        if (System.IO.File.Exists(tempWavFilePath))
        {
            System.IO.File.Delete(tempWavFilePath);

        }
        Process proc = new Process
        {
            StartInfo = new ProcessStartInfo
            {
                FileName = System.Configuration.ConfigurationManager.AppSettings["soxPath"],
                //FileName = @"C:\Program Files (x86)\Acarda\Acarda VoxConverter\Acarda VOXConverter.exe",
                //Arguments = waveFilePath + " -t raw -r 6000 -c 1 -U -b 8 " + voxFilePath,
                //Arguments = waveFilePath + " -r 8000 -c 1 -t raw -U " + voxFilePath,
                //Arguments = "E:\\TTS16465.wav" + " -t raw -r 6000 " + voxFPath,
                Arguments = waveFPath + " -r 8000 -c 1 -e u-law " + tempWavFilePath,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                CreateNoWindow = true
            }
        };
        proc.Start();
        Thread.Sleep(1000);
        Process proc2 = new Process
        {
            StartInfo = new ProcessStartInfo
            {
                FileName = System.Configuration.ConfigurationManager.AppSettings["soxPath"],
                //FileName = @"C:\Program Files (x86)\Acarda\Acarda VoxConverter\Acarda VOXConverter.exe",
                //Arguments = waveFilePath + " -t raw -r 6000 -c 1 -U -b 8 " + voxFilePath,
                //Arguments = waveFilePath + " -r 8000 -c 1 -t raw -U " + voxFilePath,
                //Arguments = "E:\\TTS16465.wav" + " -t raw -r 6000 " + voxFPath,
                Arguments = tempWavFilePath + " -t raw -U " + voxFPath,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                CreateNoWindow = true
            }
        };
        proc2.Start();
        Thread.Sleep(1000);
    }
}