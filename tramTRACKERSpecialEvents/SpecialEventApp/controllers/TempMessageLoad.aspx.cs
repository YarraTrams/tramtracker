﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Xml.Linq;
using Newtonsoft.Json;


public partial class controllers_TempMessageLoad : System.Web.UI.Page
{
    ResponseHelper r = new ResponseHelper();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();

            if (Request.QueryString["e"] == null || Request.QueryString["e"] == "")
                throw new Exception("Unable to load Special Event Summary");
            if(Request.QueryString["a"] == null || Request.QueryString["a"] == "")
                throw new Exception("Unable to load Special Event Summary");
            if (Request.QueryString["s"] == null || Request.QueryString["s"] == "")
                throw new Exception("Unable to load Special Event Summary");

            int eventID = Convert.ToInt32(Request.QueryString["e"]);
            bool isTTAvailable =!Convert.ToBoolean(Request.QueryString["a"]);
            bool isShuttle = Convert.ToBoolean(Request.QueryString["s"]);

            SpecialEvent se = new SpecialEvent();
            se.EventID = eventID;
            se.LoadTempSpecialEventMessages(isTTAvailable, isShuttle);
            
            r.responseObject = se.SpecialEventMessages;
        }
        catch (Exception ex)
        {
            r.isError = true;
            r.responseObject = ex;
        }
        finally
        {
            Response.Write(JsonConvert.SerializeObject(r));
        }
    }
}
