﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Newtonsoft.Json;

public partial class controllers_FinalConsolidation : System.Web.UI.Page
{
    ResponseHelper r = new ResponseHelper();

    protected void Page_Load(object sender, EventArgs e)
    {
        int eventID = Convert.ToInt32(Request.QueryString["e"]);
        try
        {
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();

            SpecialEvent se = new SpecialEvent();
            se.EventID = eventID;
            //MailUtil.SendEventCreatedNotification(se.EventID, se.EventDesc);
            se.FinaliseSpecialEventMessages();
            
        }
        catch (Exception ex)
        {
            r.isError = true;
            r.responseObject = ex;
        }
        finally
        {
            Response.Write(JsonConvert.SerializeObject(r));
        }
    }
}