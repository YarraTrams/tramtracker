﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Speech.Synthesis;
using System.Diagnostics;
using System.Threading;
using System.Speech.AudioFormat;


public partial class controllers_SpeakIVRVoiceFile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();
        int _speechRate = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["_speechRate"]);
        int _speechVolume = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["_speechVolume"]);

        Thread t = new Thread(() =>
        {
            Debug.WriteLine("ivrMsg: " + Request["ivrMsg"]);
            SpeechSynthesizer synth = new SpeechSynthesizer();
            synth.Volume = _speechVolume;
            synth.Rate = _speechRate;
            if (string.IsNullOrEmpty(Request["ivrMsg"]) || Request["ivrMsg"] == "")
            {
                synth.Speak("Please include an I V R Message");
            }
            else
            {
                synth.Speak(Request["ivrMsg"]);
            }


        });
        t.Start();

        t.Join();


        Response.Write(JsonConvert.SerializeObject("Success"));
    }
}