﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using System.Diagnostics;

public partial class controllers_GetAllEvents : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ResponseHelper r = new ResponseHelper();

        try
        {       
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();

            DateTime startTime = DateTime.Parse(Request["sd"]);
            DateTime endTime = DateTime.Parse(Request["ed"]);

            List<SpecialEvent> specialEvents = SpecialEvent.LoadAllEvents(startTime, endTime);
            r.responseObject = specialEvents;
        }
        catch (Exception ex)
        {
            r.isError = true;
            r.responseObject = ex;
        }
        finally
        {
            Response.Write(JsonConvert.SerializeObject(r));
        }
    }
}