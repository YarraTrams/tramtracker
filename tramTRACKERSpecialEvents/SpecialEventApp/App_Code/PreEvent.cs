﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Collections;
using System.Diagnostics;

/// <summary>
/// Summary description for PreEvent
/// </summary>
public class PreEvent : Savable
{
    public Int32 EventID { get; set; }
    public Int32 MessageID { get; set; }
    public String IVRFileName { get; set; }
    public string EventDesc { get; set; }
    public DateTime EventStartDate { get; set; }
    public DateTime EventEndDate { get; set; }
    public bool IsPublicHoliday { get; set; }
    public string CreatedBy { get; set; }
    public string UpdatedBy { get; set; }
    public DateTime LastUpdate { get; set; }
    
    public string IVRMessage { get; set; }
    public string SMSMessage { get; set; }
    public string affectedRouteNo { get; set; }
	public PreEvent()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public override void Save()
    {
        if (this.EventID == 0)
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                Debug.WriteLine(this.EventDesc + " " + this.EventStartDate + " " + this.EventEndDate + " " + this.IsPublicHoliday + " " + this.CreatedBy + " " + this.SMSMessage + " " + this.IVRMessage + " " + this.IVRFileName);
                cmd.CommandText = "[SpecialEvent_AddPreEvents]";
                cmd.Parameters.Add("@EventDesc", SqlDbType.VarChar).Value = this.EventDesc;
                cmd.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = this.EventStartDate;
                cmd.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = this.EventEndDate;
                cmd.Parameters.Add("@publicHoliday", SqlDbType.Bit).Value = this.IsPublicHoliday;
                cmd.Parameters.Add("@createBy", SqlDbType.NVarChar).Value = this.CreatedBy;
                cmd.Parameters.Add("@SMSMessage", SqlDbType.NVarChar).Value = this.SMSMessage;
                cmd.Parameters.Add("@IVRMessage", SqlDbType.VarChar).Value = this.IVRMessage;
                cmd.Parameters.Add("@IVRFileName", SqlDbType.VarChar).Value = this.IVRFileName;
                DataSet result = ExecuteCommand(cmd);
                this.EventID = Convert.ToInt32(result.Tables[0].Rows[0][0].ToString());
                this.MessageID = Convert.ToInt32(result.Tables[0].Rows[0][1].ToString());
            }
        }
        else
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "[SpecialEvent_AddPreEvents]";
                cmd.Parameters.Add("@EventID", SqlDbType.Int).Value = this.EventID;
                cmd.Parameters.Add("@EventDesc", SqlDbType.VarChar).Value = this.EventDesc;
                cmd.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = this.EventStartDate;
                cmd.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = this.EventEndDate;
                cmd.Parameters.Add("@publicHoliday", SqlDbType.Bit).Value = this.IsPublicHoliday;
                cmd.Parameters.Add("@createBy", SqlDbType.NVarChar).Value = this.CreatedBy;
                cmd.Parameters.Add("@SMSMessage", SqlDbType.NVarChar).Value = this.SMSMessage;
                cmd.Parameters.Add("@IVRMessage", SqlDbType.VarChar).Value = this.IVRMessage;
                cmd.Parameters.Add("@IVRFileName", SqlDbType.VarChar).Value = this.IVRFileName;
                DataSet result = ExecuteCommand(cmd);
                this.MessageID = Convert.ToInt32(result.Tables[0].Rows[0][0].ToString());
            }
        }
    }

    public void AddPreEventsForAffectedRoutes()
    {
            string[] routeNoList = this.affectedRouteNo.Split(',');
            if (this.MessageID!=0)
                deleteAllPreEvent();
            for (int i = 0; i < routeNoList.Length; i++)
            {



                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "[SpecialEvent_AddPreEventAffectedRouteStops]";
                    cmd.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = this.EventStartDate;
                    cmd.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = this.EventEndDate;
                    cmd.Parameters.Add("@routeNo", SqlDbType.Int).Value = Convert.ToInt32(routeNoList[i]);
                    cmd.Parameters.Add("@messageID", SqlDbType.Int).Value = this.MessageID;
                    DataSet result = ExecuteCommand(cmd);
                   
                }
            }        
    }

    private void deleteAllPreEvent()
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "[SpecialEvent_DeletePreEvent]";
            cmd.Parameters.Add("@messageID", SqlDbType.Int).Value = this.MessageID;
            DataSet result = ExecuteCommand(cmd);

        }
    }
}