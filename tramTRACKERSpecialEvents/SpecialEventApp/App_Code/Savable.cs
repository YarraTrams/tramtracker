﻿using System;
using System.IO;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

/// <summary>
/// Summary description for Savable
/// </summary>



public class Savable
{

    private Int32 tableId;

    public virtual Int32 TableId
    {
        get
        {
            return tableId;
        }
        set
        {
            tableId = value;
        }
    }

    public virtual void Save()
    {
    }

    public virtual void Delete()
    {
    }

    public virtual void Load()
    {
    }

    public virtual DataSet ExecuteCommand(SqlCommand cmd)
    {
        cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
        DataSet result = new DataSet("table");
        SqlDataAdapter myAdapter = new SqlDataAdapter();
        myAdapter.SelectCommand = cmd;
        myAdapter.Fill(result);
        return result;

    }
}