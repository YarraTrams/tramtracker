﻿
    using System;
    using System.IO;
    using System.Text;
    using System.Data;
    using System.Data.SqlClient;
    using System.Collections;
    using System.Collections.Generic;

    public class SpecialEventFile : Savable
    {

        public Int32 RouteNo { get; set; }
        public string AffectedStop { get; set; }
        public Int32 EventId { get; set; }
        public string IVRMessage { get; set; }
        public Int32 MessageID { get; set; }

        // This method loads the person from the database based on the TableId value
        public override void Load()
        {

        }

        // [GetSpecialEventsSummaryInformation] @eventID int ,@routeNo smallint , @upStop bit
        public static List<SpecialEventFile> LoadSpecialEventFile(Int32 eventid, Int16 stopNo, Int16 routeNumber)
        {
            List<SpecialEventFile> messageFiles = new List<SpecialEventFile>();

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetSpecialEventVoiceFileDetails";
                cmd.Parameters.Add("@eventId", SqlDbType.Int).Value = eventid;
                cmd.Parameters.Add("@affectedStop", SqlDbType.SmallInt).Value = stopNo;
                cmd.Parameters.Add("@routeNo", SqlDbType.SmallInt).Value = routeNumber;

                cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
                DataSet children = new DataSet("table");
                SqlDataAdapter myAdapter = new SqlDataAdapter();
                myAdapter.SelectCommand = cmd;
                myAdapter.Fill(children);
                foreach (DataRow dr in children.Tables[0].Rows)
                {
                    SpecialEventFile temp = new SpecialEventFile();
                    temp.RouteNo = Convert.ToInt16(dr["RouteNo"]);
                    temp.AffectedStop = dr["AffectedStop"].ToString();
                    temp.IVRMessage = dr["IVRMessage"].ToString();
                    temp.MessageID = Convert.ToInt32(dr["routeNo"]);
                    messageFiles.Add(temp);
                }
            }
            return messageFiles;

        }
    }
