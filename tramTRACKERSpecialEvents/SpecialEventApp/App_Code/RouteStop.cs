﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Collections;
using System.Diagnostics;

/// <summary>
/// Summary description for RouteStop
/// </summary>
public class RouteStop: Savable
{
    public Int32 EventID { get; set; }
    public DateTime EventStartDate { get; set; }
    public DateTime EventEndDate { get; set; }
    public int RouteNumber { get; set; }
    public int AffectedStops { get; set; }
    public bool TramTrackerAvailable { get; set; }
    public Int32 MessageID { get; set; }
    public String IVRMessage { get; set; }
    public String SMSMessage { get; set; }
    public String OriginalIVRMessage { get; set; }
    public String FormattedIVRMessage { get; set; }





    public void exportRouteStopsToLiveBySpecialEventID(int eventID)
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandTimeout = 200;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SpecialEvent_GetAllInfoBySpecialEventID";
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();
            cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
            cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = eventID;
            DataSet children = new DataSet("table");
            SqlDataAdapter myAdapter = new SqlDataAdapter();
            myAdapter.SelectCommand = cmd;
            myAdapter.Fill(children);
            foreach (DataRow dr in children.Tables[0].Rows)
            {
                RouteStop temp = new RouteStop();
                temp.EventID = Convert.ToInt32(dr["EventID"]);
                temp.EventStartDate = Convert.ToDateTime(dr["StartDate"]);
                temp.EventEndDate = Convert.ToDateTime(dr["EndDate"]);
                temp.RouteNumber = Convert.ToInt32(dr["RouteNo"]);
                temp.AffectedStops = Convert.ToInt32(dr["AffectedStop"]);
                temp.TramTrackerAvailable = Convert.ToBoolean(dr["TramTrackerAvailable"]);
                temp.MessageID = Convert.ToInt32(dr["MessageID"]);

                SaveRouteStopsToLiveDB(temp);
            }
        }
        
    }

    private void SaveRouteStopsToLiveDB(RouteStop temp)
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandTimeout = 200;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SpecialEvent_SaveSpecialEventRouteStops";
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();
            cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
            cmd.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = temp.EventStartDate;
            cmd.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = temp.EventEndDate;
            cmd.Parameters.Add("@RouteNo", SqlDbType.Int).Value = temp.RouteNumber;
            cmd.Parameters.Add("@AffectedStop", SqlDbType.Int).Value = temp.AffectedStops;
            cmd.Parameters.Add("@TramTrackerAvailable", SqlDbType.Bit).Value = temp.TramTrackerAvailable;
            cmd.Parameters.Add("@MessageID", SqlDbType.Int).Value = temp.MessageID;
            DataSet result = ExecuteCommand(cmd);
            
        }
       
    }

    public void exportMessagesToLiveBySpecialEventID(int eventID)
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandTimeout = 200;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SpecialEvent_GetAllMsgsBySpecialEventID";
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();
            cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
            cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = eventID;
            DataSet children = new DataSet("table");
            SqlDataAdapter myAdapter = new SqlDataAdapter();
            myAdapter.SelectCommand = cmd;
            myAdapter.Fill(children);
            foreach (DataRow dr in children.Tables[0].Rows)
            {
                RouteStop temp = new RouteStop();
                temp.EventID = Convert.ToInt32(dr["EventID"]);
                temp.MessageID = Convert.ToInt32(dr["MessageID"]);
                temp.IVRMessage = Convert.ToString(dr["IVRMessage"]);
                temp.SMSMessage = Convert.ToString(dr["SMSMessage"]);
                temp.OriginalIVRMessage = Convert.ToString(dr["OriginalIVRMessage"]);
                temp.FormattedIVRMessage = Convert.ToString(dr["FormattedIVRMessage"]);
                
                SaveSEMsgsToLiveDB(temp);
            }
        }
    }

    private void SaveSEMsgsToLiveDB(RouteStop temp)
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandTimeout = 200;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SpecialEvent_SaveSpecialEventMessages";
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();
            cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
            cmd.Parameters.Add("@MessageID", SqlDbType.Int).Value = temp.MessageID;
            cmd.Parameters.Add("@IVRMessage", SqlDbType.NVarChar).Value = temp.IVRMessage;
            cmd.Parameters.Add("@SMSMessage", SqlDbType.VarChar).Value = temp.SMSMessage;
            cmd.Parameters.Add("@OriginalIVRMessage", SqlDbType.VarChar).Value = temp.OriginalIVRMessage;
            //cmd.Parameters.Add("@FormattedIVRMessage", SqlDbType.VarChar).Value = temp.FormattedIVRMessage;
            DataSet result = ExecuteCommand(cmd);

        }
    }

    public void CreateWeeklySpecialEventData()
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandTimeout = 200;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "CreateWeeklySpecialEventData";
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();
            cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
            DataSet result = ExecuteCommand(cmd);
        }
    }

    public void UpdatePublishStatus(int eventID,int status,string publishUpdatedBy)
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandTimeout = 200;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SpecialEvent_UpdateSpecialEventPublishStatus";
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();
            cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
            cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = eventID;
            cmd.Parameters.Add("@isPublished", SqlDbType.Int).Value = status;
            cmd.Parameters.Add("@publishUpdatedBy", SqlDbType.NVarChar).Value = publishUpdatedBy;
            DataSet result = ExecuteCommand(cmd);
        }
    }

    public void deleteRouteStopsFromLive()
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandTimeout = 200;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SpecialEvent_GetEventIDToBeDeleted";
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();
            cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
            DataSet children = new DataSet("table");
            SqlDataAdapter myAdapter = new SqlDataAdapter();
            myAdapter.SelectCommand = cmd;
            myAdapter.Fill(children);
            foreach (DataRow dr in children.Tables[0].Rows)
            {
                getMessageID(Convert.ToInt32(dr["EventID"]));
                //deleteSERouteStops(Convert.ToInt32(dr["EventID"]));
                //deleteSEMessages(Convert.ToInt32(dr["EventID"]));
            }
        }
    }

    private void getMessageID(int eventID)
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandTimeout = 200;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SpecialEvent_GetMessageIDToBeDeleted";
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();
            cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
            cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = eventID; 
            DataSet children = new DataSet("table");
            SqlDataAdapter myAdapter = new SqlDataAdapter();
            myAdapter.SelectCommand = cmd;
            myAdapter.Fill(children);
            foreach (DataRow dr in children.Tables[0].Rows)
            {
                deleteSERouteStops(Convert.ToInt32(dr["MessageID"]));
                deleteSEMessages(Convert.ToInt32(dr["MessageID"]));
            }
        }
    }

    public void deleteSEMessages(int messageID)
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandTimeout = 200;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SpecialEvent_DeleteSpecialEventMessages";
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();
            cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
            cmd.Parameters.Add("@messageID", SqlDbType.Int).Value = messageID;
            DataSet result = ExecuteCommand(cmd);
        }
    }

    private void deleteSERouteStops(int messageID)
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandTimeout = 200;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SpecialEvent_DeleteSpecialEventRouteStops";
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();
            cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
            cmd.Parameters.Add("@messageID", SqlDbType.Int).Value = messageID;
            Debug.WriteLine("MessageID:" + messageID);
            DataSet result = ExecuteCommand(cmd);
        }
    }

    public void UpdateSERouteStops(int messageID)
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandTimeout = 200;
            cmd.CommandType = CommandType.StoredProcedure;
            Debug.WriteLine("@messageID" + messageID);
            cmd.CommandText = "SpecialEvent_UpdateSpecialEventRouteStops";
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();
            cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
            cmd.Parameters.Add("@messageID", SqlDbType.Int).Value = messageID;
            DataSet result = ExecuteCommand(cmd);
        }
    }

    public void publishEventsThatBeenDeleted()
    {  
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandTimeout = 200;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SpecialEvent_GetEventIDToBeDeleted";
            RuntimeSettings.GetInstance().ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString();
            cmd.Connection = RuntimeSettings.GetInstance().GetDatabaseConnection();
            DataSet children = new DataSet("table");
            SqlDataAdapter myAdapter = new SqlDataAdapter();
            myAdapter.SelectCommand = cmd;
            myAdapter.Fill(children);
            foreach (DataRow dr in children.Tables[0].Rows)
            {
                exportRouteStopsToLiveBySpecialEventID(Convert.ToInt32(dr["EventID"]));
                exportMessagesToLiveBySpecialEventID(Convert.ToInt32(dr["EventID"]));
                
            }
        }
    }
}