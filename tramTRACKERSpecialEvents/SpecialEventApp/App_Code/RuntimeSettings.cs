﻿using System;
using System.IO;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;


public class RuntimeSettings
{

    private String connectionString;

    private static RuntimeSettings settingsInstance;

    public virtual String ConnectionString
    {
        get
        {
            return connectionString;
        }
        set
        {
            connectionString = value;
        }
    }

    public static RuntimeSettings GetInstance()
    {
        if (settingsInstance == null)
            settingsInstance = new RuntimeSettings();
        return settingsInstance;

    }

    public virtual SqlConnection GetDatabaseConnection()
    {
        SqlConnection dbcon = new SqlConnection();
        dbcon.ConnectionString = this.connectionString;
        return dbcon;

    }
}