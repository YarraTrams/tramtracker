﻿using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TramTrackerWebAPI.Controllers;
using TramTrackerWebAPI.Dapper;
using TramTrackerWebAPI.Services;

namespace TramTrackerWebAPI.Tests
{
    [TestClass]
    public class TestFeaturesController
    {
        [TestMethod]
        public async Task GetFeatureStatusCollection_ShouldReturnListOfFeaturesAndStatusInformation()
        {
            var testFeaturesRepository = new TestFeaturesRepository();
            var mockFeaturesRepository = new Mock<IFeatureStatusRepository>();
            mockFeaturesRepository.Setup(f => f.GetAllFeatureStatus())
                .ReturnsAsync(await testFeaturesRepository.GetFeaturesStatus());

            var controller = new FeaturesController(new FeatureStatusService(mockFeaturesRepository.Object));
            var result = await controller.Get("TTIOSJSON", "Test123");

            Assert.IsNotNull(result);
        }
    }
}
