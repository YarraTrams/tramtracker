﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TramTrackerWebAPI.Controllers;
using TramTrackerWebAPI.Dapper;
using TramTrackerWebAPI.Models;
using TramTrackerWebAPI.Services;

namespace TramTrackerWebAPI.Tests
{
    [TestClass]
    public class TestRouteUpdatesController
    {
        [TestMethod]
        public async Task GetRouteUpdatesCollection_ShouldReturnUpdatesWithUpdateInformation()
        {
            DateTime since = DateTime.Parse("2014-01-01");

            var testRouteUpdatesRepository = new TestRouteUpdatesRepository();
            var mockRouteUpdatesRepository = new Mock<IRouteUpdatesRepository>();
            mockRouteUpdatesRepository.Setup(r => r.GetUpdatedStopsCollection(since)).ReturnsAsync(await testRouteUpdatesRepository.GetUpdatedStopsCollection(since));
            mockRouteUpdatesRepository.Setup(r => r.GetUpdatedRoutesCollection(since)).ReturnsAsync(await testRouteUpdatesRepository.GetUpdatedRoutesCollection(since));
            mockRouteUpdatesRepository.Setup(r => r.GetUpdatedTicketOutletsCollection(since)).ReturnsAsync(await testRouteUpdatesRepository.GetUpdatedTicketOutletsCollection(since));
            mockRouteUpdatesRepository.Setup(r => r.GetUpdatedPointsOfInterestCollection(since)).ReturnsAsync(await testRouteUpdatesRepository.GetUpdatedPointsOfInterestCollection(since));
            mockRouteUpdatesRepository.Setup(r => r.GetUpdatedNetworkMapsCollection(since)).ReturnsAsync(await testRouteUpdatesRepository.GetUpdatedNetworkMapsCollection(since));

            var controller = new RouteUpdatesController(new RouteUpdatesService(mockRouteUpdatesRepository.Object));
            var actionResult = await controller.GetRouteUpdatesCollection(since);

            var result = actionResult as OkNegotiatedContentResult<RouteUpdates>;

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Content.UpdatedStops != null && result.Content.UpdatedStops.Any());
            Assert.IsTrue(result.Content.DeletedStops != null && result.Content.DeletedStops.Any());
            Assert.IsTrue(result.Content.UpdatedRoutes != null && result.Content.UpdatedRoutes.Any());
            Assert.IsTrue(result.Content.DeletedRoutes != null && result.Content.DeletedRoutes.Any());
            Assert.IsTrue(result.Content.UpdatedTicketOutlets != null && result.Content.UpdatedTicketOutlets.Any());
            Assert.IsTrue(result.Content.DeletedTicketOutlets != null && result.Content.DeletedTicketOutlets.Any());
            Assert.IsTrue(result.Content.UpdatedPoi != null && result.Content.UpdatedPoi.Any());
            Assert.IsTrue(result.Content.DeletedPoi != null && result.Content.DeletedPoi.Any());
            Assert.IsTrue(result.Content.AddedNetworkMaps != null && result.Content.AddedNetworkMaps.Any());

            //var result = await controller.GetRouteUpdatesCollection(since);

            //Assert.IsNotNull(result);
            //Assert.IsTrue(result.UpdatedStops != null && result.UpdatedStops.Any());
            //Assert.IsTrue(result.DeletedStops != null && result.DeletedStops.Any());
            //Assert.IsTrue(result.UpdatedRoutes != null && result.UpdatedRoutes.Any());
            //Assert.IsTrue(result.DeletedRoutes != null && result.DeletedRoutes.Any());
            //Assert.IsTrue(result.UpdatedTicketOutlets != null && result.UpdatedTicketOutlets.Any());
            //Assert.IsTrue(result.DeletedTicketOutlets != null && result.DeletedTicketOutlets.Any());
            //Assert.IsTrue(result.UpdatedPoi != null && result.UpdatedPoi.Any());
            //Assert.IsTrue(result.DeletedPoi != null && result.DeletedPoi.Any());
            //Assert.IsTrue(result.AddedNetworkMaps != null && result.AddedNetworkMaps.Any());

        }

        [TestMethod]
        public async Task GetRouteUpdatesCollection_ShouldReturnUpdatesWithoutUpdateInformation()
        {
            DateTime since = DateTime.Parse("2014-01-01");
            var token = "TestToken";
            var authId = "TTIOSJSON";

            var testUpdatesRepository = new TestRouteUpdatesRepository();
            var mockUpdatesRepository = new Mock<IRouteUpdatesRepository>();
            mockUpdatesRepository.Setup(r => r.GetUpdatedStopsCollection(since)).ReturnsAsync(await Task.Run(() => Enumerable.Empty<Stop>()));
            mockUpdatesRepository.Setup(r => r.GetUpdatedRoutesCollection(since)).ReturnsAsync(await Task.Run(() => Enumerable.Empty<Route>()));
            mockUpdatesRepository.Setup(r => r.GetUpdatedTicketOutletsCollection(since)).ReturnsAsync(await Task.Run(() => Enumerable.Empty<TicketOutlet>()));
            mockUpdatesRepository.Setup(r => r.GetUpdatedPointsOfInterestCollection(since)).ReturnsAsync(await Task.Run(() => Enumerable.Empty<PointOfInterest>()));
            mockUpdatesRepository.Setup(r => r.GetUpdatedNetworkMapsCollection(since)).ReturnsAsync(await Task.Run(() => Enumerable.Empty<NetworkMap>()));

            var controller = new RouteUpdatesController(new RouteUpdatesService(mockUpdatesRepository.Object));


            var actionResult = await controller.GetRouteUpdatesCollection(since, authId, token);

            var result = actionResult as OkNegotiatedContentResult<RouteUpdates>;

            Assert.IsNotNull(result);

            Assert.IsTrue(result.Content.UpdatedStops != null && !result.Content.UpdatedStops.Any());
            Assert.IsTrue(result.Content.DeletedStops != null && !result.Content.DeletedStops.Any());
            Assert.IsTrue(result.Content.UpdatedRoutes != null && !result.Content.UpdatedRoutes.Any());
            Assert.IsTrue(result.Content.DeletedRoutes != null && !result.Content.DeletedRoutes.Any());
            Assert.IsTrue(result.Content.UpdatedTicketOutlets != null && !result.Content.UpdatedTicketOutlets.Any());
            Assert.IsTrue(result.Content.DeletedTicketOutlets != null && !result.Content.DeletedTicketOutlets.Any());
            Assert.IsTrue(result.Content.UpdatedPoi != null && !result.Content.UpdatedPoi.Any());
            Assert.IsTrue(result.Content.DeletedPoi != null && !result.Content.DeletedPoi.Any());
            Assert.IsTrue(result.Content.AddedNetworkMaps != null && !result.Content.AddedNetworkMaps.Any());

            //var result = await controller.GetRouteUpdatesCollection(since, authId, token);

            //Assert.IsNotNull(result);

            //Assert.IsTrue(result.UpdatedStops != null && !result.UpdatedStops.Any());
            //Assert.IsTrue(result.DeletedStops != null && !result.DeletedStops.Any());
            //Assert.IsTrue(result.UpdatedRoutes != null && !result.UpdatedRoutes.Any());
            //Assert.IsTrue(result.DeletedRoutes != null && !result.DeletedRoutes.Any());
            //Assert.IsTrue(result.UpdatedTicketOutlets != null && !result.UpdatedTicketOutlets.Any());
            //Assert.IsTrue(result.DeletedTicketOutlets != null && !result.DeletedTicketOutlets.Any());
            //Assert.IsTrue(result.UpdatedPoi != null && !result.UpdatedPoi.Any());
            //Assert.IsTrue(result.DeletedPoi != null && !result.DeletedPoi.Any());
            //Assert.IsTrue(result.AddedNetworkMaps != null && !result.AddedNetworkMaps.Any());
        }
    }
}
