﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TramTrackerWebAPI.Dapper;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Tests
{
    public class TestRouteUpdatesRepository : IRouteUpdatesRepository
    {

        public TestRouteUpdatesRepository()
        {
        }

        public async Task<IEnumerable<Stop>> GetUpdatedStopsCollection(DateTime? since = null)
        {
            return await Task.Run<IEnumerable<Stop>>(() => new List<Stop>() { 
                new Stop() { StopNo = 1 , Removed = false },
                new Stop() { StopNo = 2 , Removed = false },
                new Stop() { StopNo = 3 , Removed = false },
                new Stop() { StopNo = 4, Removed = true },
                new Stop() { StopNo = 5, Removed = true} 
            });
        }

        public async Task<IEnumerable<Route>> GetUpdatedRoutesCollection(DateTime? since = null)
        {
            return await Task.Run(() => new List<Route>() { 
                new Route() { RouteNo = 1, RouteAvailable = true,
                    UpStops = new List<RouteStop> { new RouteStop(), new RouteStop() }, DownStops = new List<RouteStop> { new RouteStop(), new RouteStop() } },
                new Route() { RouteNo = 2, RouteAvailable = true,
                    UpStops = new List<RouteStop> { new RouteStop(), new RouteStop() }, DownStops = new List<RouteStop> { new RouteStop(), new RouteStop() } },
                new Route() { RouteNo = 3, RouteAvailable = true,
                    UpStops = new List<RouteStop> { new RouteStop(), new RouteStop() }, DownStops = new List<RouteStop> { new RouteStop(), new RouteStop() } },
                new Route() { RouteNo = 4, RouteAvailable = false,
                    UpStops = new List<RouteStop> { new RouteStop(), new RouteStop() }, DownStops = new List<RouteStop> { new RouteStop(), new RouteStop() } },
                new Route() { RouteNo = 5, RouteAvailable = false,
                    UpStops = new List<RouteStop> { new RouteStop(), new RouteStop() }, DownStops = new List<RouteStop> { new RouteStop(), new RouteStop() } } });
        }

        public async Task<IEnumerable<TicketOutlet>> GetUpdatedTicketOutletsCollection(DateTime? since = null)
        {
            return await Task.Run(() => new List<TicketOutlet>() { 
                new TicketOutlet() { ID = 1, IsDeleted = false }, 
                new TicketOutlet() { ID = 2, IsDeleted = false }, 
                new TicketOutlet() { ID = 3, IsDeleted = false }, 
                new TicketOutlet() { ID = 4, IsDeleted = true },
                new TicketOutlet() { ID = 5, IsDeleted = true }
            });
        }

        public async Task<IEnumerable<PointOfInterest>> GetUpdatedPointsOfInterestCollection(DateTime? since = null)
        {
            return await Task.Run(() => new List<PointOfInterest>() { 
                new PointOfInterest() { POIId = 1, IsDeleted = false }, 
                new PointOfInterest() { POIId = 2, IsDeleted = false }, 
                new PointOfInterest() { POIId = 3, IsDeleted = false }, 
                new PointOfInterest() { POIId = 4, IsDeleted = true }, 
                new PointOfInterest()  { POIId = 5, IsDeleted = true }
            });
        }

        public async Task<IEnumerable<NetworkMap>> GetUpdatedNetworkMapsCollection(DateTime? since = null)
        {
            return await Task.Run(() => new List<NetworkMap> { 
                new NetworkMap() { Id = 1 },
                new NetworkMap() { Id = 2 },
                new NetworkMap() { Id = 3 },
                new NetworkMap() { Id = 4 },
                new NetworkMap() { Id = 5 }
            });
        }

    }
}
