﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Tests
{
    [TestClass]
    public class TestFeaturesRepository
    {

        public TestFeaturesRepository()
        {    
        }

        [TestMethod]
        public async Task<IEnumerable<FeatureStatus>> GetFeaturesStatus()
        {
            return await Task.Run<IEnumerable<FeatureStatus>>(() => new List<FeatureStatus>()
            {
                new FeatureStatus(){ FeatureName = "Advertising", IsEnabled = true},
                new FeatureStatus(){ FeatureName = "Foo", IsEnabled = false},
                new FeatureStatus(){ FeatureName = "Bar", IsEnabled = true}
            });
        }
    }
}
