﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Timers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PushSharp;
using PushSharp.Android;
using PushSharp.Apple;
using PushSharp.Core;
using TramTrackerWebAPI.PushServer.Interfaces;
using TramTrackerWebAPI.PushServer.Models;
using ILogger = Ninject.Extensions.Logging.ILogger;

namespace TramTrackerWebAPI.PushServer
{
    public class PushNotificationsProcessor
    {
        private static readonly string _gcmAuthKey = ConfigurationManager.AppSettings["tramTrackerAndroidKey"];

        private static readonly bool _apnsProd = Convert.ToBoolean(ConfigurationManager.AppSettings["apnsProd"]);

        private static readonly string _apnsDevCertPath = ConfigurationManager.AppSettings["apnsDevCertPath"];
        private static readonly string _apnsDevCertPwd = ConfigurationManager.AppSettings["apnsDevCertPwd"];

        private static readonly string _apnsProdCertPath = ConfigurationManager.AppSettings["apnsProdCertPath"];
        private static readonly string _apnsProdCertPwd = ConfigurationManager.AppSettings["apnsProdCertPwd"];

        private static readonly bool _isApnsProd = Convert.ToBoolean(ConfigurationManager.AppSettings["apnsProd"]);
        private static readonly double _frequencyInMs = Convert.ToDouble(ConfigurationManager.AppSettings["frequencyInMs"]);
   
        private static readonly string _iosMsgSound = ConfigurationManager.AppSettings["iosMsgSound"];

        private static readonly int _msgExpiryInHours = Convert.ToInt32(ConfigurationManager.AppSettings["msgExpiryInHours"]);

        private static INotificationMessagesService _notificationMessagesService;
        private static INotificationSubscribersService _notificationSubscribersService;

        private static ILogger _logger;

        private PushBroker _pushBroker = null;
        private Timer _processorTimer;


        public PushNotificationsProcessor(ILogger logger, INotificationMessagesService notificationMessagesService,
            INotificationSubscribersService notificationSubscribersService)
        {
            _notificationMessagesService = notificationMessagesService;
            _notificationSubscribersService = notificationSubscribersService;

            _logger = logger;
        }

        private void InitPushBroker()
        {
            _pushBroker = _pushBroker ?? new PushBroker();

            _pushBroker.OnNotificationSent += NotificationSent; 
            _pushBroker.OnNotificationFailed += NotificationFailed;

            _pushBroker.OnServiceException += ServiceException;
            _pushBroker.OnNotificationRequeue += OnNotificationRequeue;
            _pushBroker.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
            _pushBroker.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;

            _pushBroker.OnChannelCreated += ChannelCreated;
            _pushBroker.OnChannelException += ChannelException;
            _pushBroker.OnChannelDestroyed += ChannelDestroyed;

            if (_apnsProd)
            {
                _pushBroker.RegisterAppleService(new ApplePushChannelSettings(_isApnsProd, _apnsProdCertPath, _apnsProdCertPwd, true));
            }
            else
            {
                _pushBroker.RegisterAppleService(new ApplePushChannelSettings(_isApnsProd, _apnsDevCertPath, _apnsDevCertPwd, true));
            }

        
            _pushBroker.RegisterGcmService(new GcmPushChannelSettings(_gcmAuthKey));
            _logger.Info("Push Broker successsfully started");
        }

        private void DestroyPushBroker()
        {
            _pushBroker.StopAllServices();

            _pushBroker.OnNotificationSent -= NotificationSent; 
            _pushBroker.OnNotificationFailed -= NotificationFailed;

            _pushBroker.OnServiceException -= ServiceException;
            _pushBroker.OnNotificationRequeue -= OnNotificationRequeue;
            _pushBroker.OnDeviceSubscriptionExpired -= DeviceSubscriptionExpired;
            _pushBroker.OnDeviceSubscriptionChanged -= DeviceSubscriptionChanged;

            _pushBroker.OnChannelCreated -= ChannelCreated;
            _pushBroker.OnChannelException -= ChannelException;
            _pushBroker.OnChannelDestroyed -= ChannelDestroyed;

            _logger.Info("Push Broker successsfully stopped");

        }

        public void Start()
        {
            _logger.Info("Notification Service processor started");
            InitPushBroker();
            if (!Environment.UserInteractive)
            {
                _processorTimer = new Timer();
                _processorTimer.Interval = _frequencyInMs;
                _processorTimer.Elapsed += _processorTimer_Elapsed;
                _processorTimer.Start();
            }
            else
            {
                ProcessNotifications();
            }
        }

        void _processorTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _logger.Debug("_processorTimer_Elapsed tick");
            ProcessNotifications();
        }

        public void Stop()
        {
            try
            {
                DestroyPushBroker();
                if (_processorTimer != null)
                {
                    _processorTimer.Stop();
                }
                _logger.Info("Notification processor stopping...");
            }
            catch (Exception ex)
            {
                _logger.ErrorException("Stopping the push notification caused an exception", ex);
            }
        }

        private void ProcessNotifications()
        {
            _logger.Info("Notifcation processor started");

            try
            {
                var notificationMessages = _notificationMessagesService.GetNotificationMessages();
    
                _logger.Debug("There are currently {0} messages to be processed", notificationMessages.Count());

                if (notificationMessages.Count > 0)
                {
                    SendMessages(notificationMessages);
                }
            }
            catch (Exception ex)
            {
                _logger.ErrorException("Exception when processing notifications: ", ex);
            }
        }

        private void SendMessages(IEnumerable<NotificationMessage> notificationMessages)
        {
            foreach (var msg in notificationMessages)
            {
                var debugString = string.Format("Queing notification - ID: {0}, DeviceType: {1}", msg.DeviceId, msg.DeviceType);


                if (string.Equals(msg.DeviceType.Trim(), "iOS", StringComparison.CurrentCultureIgnoreCase))
                {
                    var apsnNotification = new AppleNotification() { Tag = msg };
                    var messagePayload = new AppleNotificationPayload(msg.Message);
                    apsnNotification.ForDeviceToken(msg.NotificationToken)
                        .WithPayload(messagePayload)
                        .WithSound(_iosMsgSound)
                        .WithExpiry(DateTime.Now.AddHours(_msgExpiryInHours))
                        .WithCustomItem("messageType", msg.MessageType);
                    _pushBroker.QueueNotification(apsnNotification);
                }
                else if (string.Equals(msg.DeviceType.Trim(), "Android", StringComparison.CurrentCultureIgnoreCase))
                {
                    var gcmNotification = new GcmNotification() { Tag = msg };

                    var settings = new JsonSerializerSettings
                    {
                        ContractResolver = new CamelCasePropertyNamesContractResolver()
                    };

                    string msgString = JsonConvert.SerializeObject(msg, settings);

                    int ttlSeconds = _msgExpiryInHours * 60 * 60;

                    gcmNotification.ForDeviceRegistrationId(msg.NotificationToken)
                        .WithJson(msgString)
                        .WithTimeToLive(ttlSeconds);
                    
                    _pushBroker.QueueNotification(gcmNotification);
                }
                else
                {
                    _logger.Warn("Unsupported device: {0}", JsonConvert.SerializeObject(msg));
                }

                AddToNotificationHistory(msg);
                _logger.Debug("Notification Queued: {0}", debugString);
            }
        }

        private void NotificationSent(object sender, INotification notification)
        {
            _logger.Debug("Notification Sent");
        }

        private static void AddToNotificationHistory(NotificationMessage msg)
        {
            try
            {
                if (msg != null)
                {
                    msg.NotificationSentTime = DateTime.Now;
                    _notificationMessagesService.AddNotificationSent(msg);
                }
            }
            catch (Exception ex)
            {
                _logger.ErrorException("An exception occurred", ex);
            }
        }

        private void NotificationFailed(object sender, INotification notification, Exception error)
        {
            NotificationMessage msg = notification.Tag as NotificationMessage;

            _logger.Debug("Notification Failed with error. Notification: {0}, Error: {1}", msg, error);
        }

        private void ChannelCreated(object sender, IPushChannel pushchannel)
        {
            _logger.Debug("Channel created!");
        }


        private void ChannelDestroyed(object sender)
        {
            _logger.Debug("Channel destroyed!");
        }

        private void DeviceSubscriptionChanged(object sender, string oldsubscriptionid, 
            string newsubscriptionid, INotification notification)
        {
            var msg = notification.Tag as NotificationMessage;
            try
            {
                _logger.Debug("Device subscription changed from {0} to {1}. Notification Message: {2}",
                    oldsubscriptionid, newsubscriptionid, msg);

                if (msg != null)
                {
                    _notificationSubscribersService.UpdateNotificationToken(msg.DeviceId, newsubscriptionid);
                }
                

            }
            catch (Exception ex)
            {
                var exceptionString = string.Format("An exception occured in DeviceSubscriptionChanged: {0}", msg);
                _logger.ErrorException(exceptionString, ex);
            }

        }

        private void DeviceSubscriptionExpired(object sender, string expiredsubscriptionid, DateTime expirationdateutc, 
            INotification notification)
        {
            var msg = notification.Tag as NotificationMessage;

            try
            {
                

                _logger.Warn("Push Notification service subscription ({0}) expired at: {1}. Notification Message: {2}",
                    expiredsubscriptionid, expirationdateutc, msg);

                if (msg != null)
                {
                    _notificationSubscribersService.DeregisterDevice(msg.DeviceId);
                }
            }
            catch (Exception ex)
            {
                var exceptionString = string.Format("An exception occured in DeviceSubscriptionExpired: {0}", msg);
                _logger.ErrorException(exceptionString, ex);
            }

        }

        private void ServiceException(object sender, Exception error)
        {
            _logger.ErrorException("A push notification service exception occured: {0)", error);
        }

        private void ChannelException(object sender, IPushChannel pushchannel, Exception error)
        {
            _logger.ErrorException("A push notification channel exception occured: {0)", error);
        }

        private void OnNotificationRequeue(object sender, NotificationRequeueEventArgs e)
        {
            _logger.Debug("Notification requeued!");
        }

    }
}