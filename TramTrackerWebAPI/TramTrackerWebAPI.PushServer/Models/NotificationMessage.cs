﻿using System;
using Newtonsoft.Json;

namespace TramTrackerWebAPI.PushServer.Models
{
    public class NotificationMessage : TtNotification
    {
        [JsonIgnore]
        public string DeviceId { get; set; }

        [JsonIgnore]
        public string NotificationToken { get; set; }

        [JsonIgnore]
        public string DeviceType { get; set; }
        [JsonIgnore]
        public DateTime NotificationSentTime { get; set; }

        [JsonIgnore]
        public string TravelTimeDesc { get; set; }

        public override string ToString()
        {
            return string.Format("DeviceId: {0}, NotificationToken: {1}, DeviceType: {2}", DeviceId, NotificationToken,
                DeviceType);
        }
    }
}