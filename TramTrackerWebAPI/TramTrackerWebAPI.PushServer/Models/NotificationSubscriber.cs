﻿using System;

namespace TramTrackerWebAPI.PushServer.Models
{
    public class NotificationSubscriber
    {
        public string DeviceId { get; set; }
        public string NotificationToken { get; set; }
        public string DeviceType { get; set; }
        public int TravelTimeId { get; set; }
        public string TimeDescription { get; set; }
        public DateTime DateModified { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            NotificationSubscriber subscriber = obj as NotificationSubscriber;

            if ((Object) subscriber == null)
            {
                return false;
            }

            return (this.DeviceId == subscriber.DeviceId);
        }

        public bool Equals(NotificationSubscriber subscriber)
        {
            if ((object) subscriber == null)
            {
                return false;
            }

            return (this.DeviceId == subscriber.DeviceId);
        }

        public override int GetHashCode()
        {
            int hash = 15;
            hash = (hash*7) + DeviceId.GetHashCode();
            return hash;
        }
    }
}