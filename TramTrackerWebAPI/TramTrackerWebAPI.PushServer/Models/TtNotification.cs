﻿using System;
using Newtonsoft.Json;
using TramTrackerWebAPI.Common.Enums;

namespace TramTrackerWebAPI.PushServer.Models
{
    public class TtNotification
    {
        public int Id { get; set; }
        public int RouteNo { get; set; }
        public string Message { get; set; }
        public MessageType MessageType { get; set; }

        [JsonIgnore]
        public DateTime StartTime { get; set; }

        [JsonIgnore]
        public DateTime EndTime { get; set; }

        [JsonIgnore]
        public int DisruptionId { get; set; }

        [JsonIgnore]
        public int MessageId { get; set; }
    }
}