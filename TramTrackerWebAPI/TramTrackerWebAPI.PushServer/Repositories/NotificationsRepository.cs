﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using TramTrackerWebAPI.PushServer.Interfaces;
using TramTrackerWebAPI.PushServer.Models;

namespace TramTrackerWebAPI.PushServer.Repositories
{
    public class NotificationsRepository : INotificationsRepository
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;

        public NotificationsRepository(IDbConnectionFactory dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }

        public IList<TtNotification> GetAllNotifications()
        {
            using (var cn = _dbConnectionFactory.CreateConnection())
            {
                const string storedProcedure = "[dbo].[PushNotifications_GetNotifications]";

                return cn.Query<TtNotification>(storedProcedure, null,
                            commandType: CommandType.StoredProcedure).ToList();
            }
        }
    }
}