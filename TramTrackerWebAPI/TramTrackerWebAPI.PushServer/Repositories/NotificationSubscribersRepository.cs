﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using TramTrackerWebAPI.Common.Enums;
using TramTrackerWebAPI.PushServer.Interfaces;
using TramTrackerWebAPI.PushServer.Models;

namespace TramTrackerWebAPI.PushServer.Repositories
{
    public class NotificationSubscribersRepository : INotificationSubscribersRepository
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;

        public NotificationSubscribersRepository(IDbConnectionFactory dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }

        public IList<NotificationSubscriber> GetSubscribersByRouteAndTravelTime(int notificationId, TravelTimeFrame travelTimeFrame)
        {
            using (var cn = _dbConnectionFactory.CreateConnection())
            {
                const string storedProcedure = "[dbo].[PushNotifications_GetUsersForNotificationByTravelTime]";
                var parameters = new DynamicParameters();
                parameters.Add("@notificationId", notificationId, dbType: DbType.Int32);
                parameters.Add("@travelTimeId", (int)travelTimeFrame, dbType: DbType.Int32);
                return cn.Query<NotificationSubscriber>(storedProcedure, parameters,
                            commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public NotificationSubscriber GetNotificationSubscriberByDeviceId(string deviceId)
        {
            using (var cn = _dbConnectionFactory.CreateConnection())
            {
                const string storedProcedure = "dbo.PushNotifications_GetNotificationSubscriberByDeviceId";

                var parameters = new DynamicParameters();
                parameters.Add("@deviceId", deviceId, dbType: DbType.String);

                var notificationUsers = cn.Query<NotificationSubscriber>(storedProcedure, parameters, commandType: CommandType.StoredProcedure);

                return notificationUsers.SingleOrDefault();
            }
        }

        //public NotificationSubscriber GetNotificationUserByToken(string notificationToken)
        //{
        //    using (var cn = _dbConnectionFactory.CreateConnection())
        //    {
        //        const string storedProcedure = "dbo.PushNotifications_GetNotificationSubscriberByToken";

        //        var parameters = new DynamicParameters();
        //        parameters.Add("@notificationToken", notificationToken, dbType: DbType.String);

        //        var notificationUsers = cn.Query<NotificationSubscriber>(storedProcedure, parameters, commandType: CommandType.StoredProcedure);

        //        return notificationUsers.SingleOrDefault();
        //    }
        //}

        public void UpdateNotificationSubscriber(NotificationSubscriber _notificationSubscriber)
        {
            using (var cn = _dbConnectionFactory.CreateConnection())
            {
                const string storedProcedure = "dbo.PushNotifications_UpdateNotificationSubscriber";
                _notificationSubscriber.DateModified = DateTime.Now;

                var parameters = new DynamicParameters();
                parameters.Add("@deviceId", _notificationSubscriber.DeviceId, dbType: DbType.String);
                parameters.Add("@notificationToken", _notificationSubscriber.NotificationToken, dbType: DbType.String);
                parameters.Add("@dateModified", _notificationSubscriber.DateModified, dbType: DbType.DateTime);
                cn.Execute(storedProcedure, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        //public void UpdateNotificationToken(string oldNotificationToken, string newNotificationToken)
        //{
        //    using (var cn = _dbConnectionFactory.CreateConnection())
        //    {
        //        const string storedProcedure = "dbo.PushNotifications_UpdateNotificationToken";

        //        var parameters = new DynamicParameters();
        //        parameters.Add("@oldNotificationToken", oldNotificationToken, dbType: DbType.String);
        //        parameters.Add("@newNotificationToken", newNotificationToken, dbType: DbType.String);
        //        parameters.Add("@dateModified", DateTime.Now, dbType: DbType.DateTime);
        //        cn.Execute(storedProcedure, parameters, commandType: CommandType.StoredProcedure);
        //    }
        //}

        public void DeleteNotificationSubscriber(string deviceId)
        {
            using (var cn = _dbConnectionFactory.CreateConnection())
            {
                const string storedProcedure = "dbo.PushNotifications_DeleteNotificationSubscriber";
                 cn.Execute(storedProcedure, new { DeviceId = deviceId }, commandType: CommandType.StoredProcedure);
            }
        }

        public void AddNotificationSent(NotificationMessage notificationMessage)
        {
            using (var cn = _dbConnectionFactory.CreateConnection())
            {
                const string storedProcedure = "[dbo].[PushNotifications_AddSentNotification]";
                var parameters = new DynamicParameters();
                parameters.Add("@deviceId", notificationMessage.DeviceId, dbType: DbType.String);
                parameters.Add("@notificationToken", notificationMessage.NotificationToken, dbType: DbType.String);
                parameters.Add("@sent", notificationMessage.NotificationSentTime,
                    dbType: DbType.DateTime);
                parameters.Add("@notificationId", notificationMessage.Id, dbType:DbType.Int32);

                cn.Execute(storedProcedure, parameters, commandType: CommandType.StoredProcedure);
            }
        }

    }
}