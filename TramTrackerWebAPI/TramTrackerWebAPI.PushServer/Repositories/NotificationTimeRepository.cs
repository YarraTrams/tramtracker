﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using TramTrackerWebAPI.PushServer.Interfaces;


namespace TramTrackerWebAPI.PushServer.Repositories
{
    public class NotificationTimeRepository : INotificationTimeRepository
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;

        public NotificationTimeRepository(IDbConnectionFactory dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }

        public void DeleteAllNotificationTimes(string deviceId)
        {
            using (var cn = _dbConnectionFactory.CreateConnection())
            {
                const string storedProcedure = "dbo.PushNotifications_DeleteAllNotificationTimes";
                cn.Execute(storedProcedure, new { DeviceId = deviceId }, commandType: CommandType.StoredProcedure);
            }
        }
    }
}