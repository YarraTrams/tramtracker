﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using TramTrackerWebAPI.PushServer.Interfaces;


namespace TramTrackerWebAPI.PushServer.Repositories
{
    public class NotificationRequestRepository : INotificationRequestRepository
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;

        public NotificationRequestRepository(IDbConnectionFactory dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }

        public void DeleteAllNotificationRequests(string deviceId)
        {
            using (var cn = _dbConnectionFactory.CreateConnection())
            {
                const string storedProcedure = "dbo.PushNotifications_DeleteAllNotificationRequests";
                cn.Execute(storedProcedure, new { DeviceId = deviceId }, commandType: CommandType.StoredProcedure);
            }
        }
    }
}