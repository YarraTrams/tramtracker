﻿using System.Data;
using System.Data.SqlClient;
using TramTrackerWebAPI.PushServer.Interfaces;

namespace TramTrackerWebAPI.PushServer.Dapper
{
    public class SqlConnectionFactory : IDbConnectionFactory
    {
        private readonly string _connectionString;

        public SqlConnectionFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IDbConnection CreateConnection()
        {
            var connection = new SqlConnection(_connectionString);
            connection.Open();
            return connection;
        }
    }
}
