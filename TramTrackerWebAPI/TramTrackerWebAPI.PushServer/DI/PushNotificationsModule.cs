﻿using System.Configuration;
using Ninject.Modules;
using TramTrackerWebAPI.PushServer.Dapper;
using TramTrackerWebAPI.PushServer.Interfaces;
using TramTrackerWebAPI.PushServer.Repositories;
using TramTrackerWebAPI.PushServer.Services;

namespace TramTrackerWebAPI.PushServer.DI
{
    public class PushNotificationsModule : NinjectModule
    {
        public override void Load()
        {
            Bind<INotificationMessagesService>().To<NotificationMessagesService>();
            Bind<INotificationsRepository>().To<NotificationsRepository>();
            Bind<INotificationSubscribersRepository>().To<NotificationSubscribersRepository>();
            Bind<INotificationSubscribersService>().To<NotificationSubscribersService>();
            Bind<INotificationTimeRepository>().To<NotificationTimeRepository>();
            Bind<INotificationRequestRepository>().To<NotificationRequestRepository>();

            Bind<IDbConnectionFactory>().To<SqlConnectionFactory>()
                .WithConstructorArgument("connectionString", ConfigurationManager.ConnectionStrings["tramTrackerDB"].ConnectionString);
            
        }
    }
}