namespace TramTrackerWebAPI.PushServer.Interfaces
{
    public interface INotificationSubscribersService
    {
        void UpdateNotificationToken(string oldAuthToken, string newAuthToken);
        void DeregisterDevice(string notificationToken);
    }
}