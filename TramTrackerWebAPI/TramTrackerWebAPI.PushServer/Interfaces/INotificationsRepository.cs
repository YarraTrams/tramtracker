﻿using System.Collections.Generic;
using TramTrackerWebAPI.PushServer.Models;

namespace TramTrackerWebAPI.PushServer.Interfaces
{
    public interface INotificationsRepository
    {
        IList<TtNotification> GetAllNotifications();
    }
}