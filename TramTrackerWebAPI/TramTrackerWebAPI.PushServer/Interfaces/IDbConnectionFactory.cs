﻿using System.Data;

namespace TramTrackerWebAPI.PushServer.Interfaces
{
    public interface IDbConnectionFactory
    {
        IDbConnection CreateConnection();
    }
}

