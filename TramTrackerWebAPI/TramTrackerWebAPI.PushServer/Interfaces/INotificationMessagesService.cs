﻿using System.Collections.Generic;
using TramTrackerWebAPI.PushServer.Models;

namespace TramTrackerWebAPI.PushServer.Interfaces
{
    public interface INotificationMessagesService
    {
        //IList<NotificationMessage> GetMessagesForDisruptions();
        //IList<NotificationMessage> GetMessagesForSpecialEvents();

        IList<NotificationMessage> GetNotificationMessages();
 
        void AddNotificationSent(NotificationMessage notificationMessage);
    }
}