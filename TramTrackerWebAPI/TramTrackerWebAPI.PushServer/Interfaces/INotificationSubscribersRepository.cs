﻿using System.Collections.Generic;
using TramTrackerWebAPI.Common.Enums;
using TramTrackerWebAPI.PushServer.Models;

namespace TramTrackerWebAPI.PushServer.Interfaces
{
    public interface INotificationSubscribersRepository
    {
        NotificationSubscriber GetNotificationSubscriberByDeviceId(string deviceId);
        void UpdateNotificationSubscriber(NotificationSubscriber notificationSubscriber);

        //void UpdateNotificationToken(string oldNotificationToken, string newNotificationToken);
        void DeleteNotificationSubscriber(string deviceId);

        IList<NotificationSubscriber> GetSubscribersByRouteAndTravelTime(int notificationId, TravelTimeFrame travelTimeFrame);

        void AddNotificationSent(NotificationMessage notificationMessage);
    }
}