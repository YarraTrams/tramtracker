﻿using System.Collections.Generic;
using System.Threading.Tasks;


namespace TramTrackerWebAPI.PushServer.Interfaces
{
    public interface INotificationRequestRepository
    {
        void DeleteAllNotificationRequests(string deviceId);
    }
}