﻿using System.Collections.Generic;
using System.Threading.Tasks;


namespace TramTrackerWebAPI.PushServer.Interfaces
{
    public interface INotificationTimeRepository
    {
        void DeleteAllNotificationTimes(string deviceId);
    }
}