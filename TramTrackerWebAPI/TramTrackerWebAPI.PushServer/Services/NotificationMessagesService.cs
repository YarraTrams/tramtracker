﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using TramTrackerWebAPI.Common.Enums;
using TramTrackerWebAPI.PushServer.Interfaces;
using TramTrackerWebAPI.PushServer.Models;

namespace TramTrackerWebAPI.PushServer.Services
{
    public class NotificationMessagesService : INotificationMessagesService
    {
        private static INotificationsRepository _notificationsRepository;
        private static INotificationSubscribersRepository _notificationSubscribersRepository;

        private static readonly int WEEKDAY_MORNING_PEAK_START =
            Convert.ToInt32(ConfigurationManager.AppSettings["weekdayMorningPeakStart"]);
        private static readonly int WEEKDAY_MORNING_PEAK_END =
            Convert.ToInt32(ConfigurationManager.AppSettings["weekdayMorningPeakEnd"]);
        private static readonly int WEEKDAY_AFTERNOON_PEAK_START =
            Convert.ToInt32(ConfigurationManager.AppSettings["weekdayAfternoonPeakStart"]);
        private static readonly int WEEKDAY_AFTERNOON_PEAK_END =
            Convert.ToInt32(ConfigurationManager.AppSettings["weekdayAfternoonPeakEnd"]);

        public NotificationMessagesService(INotificationSubscribersRepository notificationSubscribersRepository,
            INotificationsRepository notificationsRepository)
        {
            _notificationsRepository = notificationsRepository;
            _notificationSubscribersRepository = notificationSubscribersRepository;
        }

        public IList<NotificationMessage> GetNotificationMessages()
        {
            var currentNotifications = _notificationsRepository.GetAllNotifications();
            var notificationMessages = new List<NotificationMessage>();

            foreach (var notification in currentNotifications)
            {
                var subscribers = GetSubscribersForMessage(notification);
                foreach (var subscriber in subscribers)
                {
                    var notificationMessage = new NotificationMessage
                    {
                        Id = notification.Id,
                        DeviceId = subscriber.DeviceId,
                        DeviceType = subscriber.DeviceType,
                        DisruptionId = notification.DisruptionId,
                        EndTime = notification.EndTime,
                        StartTime = notification.StartTime,
                        MessageId = notification.MessageId,
                        Message = notification.Message,
                        MessageType = notification.MessageType,
                        NotificationToken = subscriber.NotificationToken,
                        RouteNo = notification.RouteNo
                    };
                    notificationMessages.Add(notificationMessage);
                }
            }
            return notificationMessages;
        }

        private IList<NotificationSubscriber> GetSubscribersForMessage(TtNotification message)
        {
            IList<TravelTimeFrame> travelTimes = new List<TravelTimeFrame>();

            var notificationSubscribers = new List<NotificationSubscriber>();

            if (message.MessageType == MessageType.Disruption)
            {
                travelTimes = GetTravelTimesForDateTime(DateTime.Now);
            }
            else if(message.MessageType == MessageType.ServiceUpdate)
            {
                travelTimes = GetTravelTimesForTimeSpan(message.StartTime, message.EndTime);
            }

            foreach (var travelTime in travelTimes)
            {
                var users = _notificationSubscribersRepository.GetSubscribersByRouteAndTravelTime(message.Id, travelTime);
                notificationSubscribers.AddRange(users);
            }

            return notificationSubscribers.Distinct().ToList();
        }

        public void AddNotificationSent(NotificationMessage notificationMessage)
        {
            _notificationSubscribersRepository.AddNotificationSent(notificationMessage);
        }

        private static IList<TravelTimeFrame> GetTravelTimesForTimeSpan(DateTime startDate, DateTime endDate)
        {
            var timeFrames = new HashSet<TravelTimeFrame>();
            for (var date = startDate; date <= endDate; date = date.AddHours(1))
            {
                var travelTimeFrame = GetTravelTimesForDateTime(date);
                timeFrames.UnionWith(travelTimeFrame);
            }
            return timeFrames.ToList();
        }

        private static IList<TravelTimeFrame> GetTravelTimesForDateTime(DateTime travelDate)
        {
            var timeFrames = new List<TravelTimeFrame>();


            if (travelDate.DayOfWeek == DayOfWeek.Saturday || travelDate.DayOfWeek == DayOfWeek.Sunday)
            {
                timeFrames.Add(TravelTimeFrame.Weekend);
            }
            else
            {
                timeFrames.Add(TravelTimeFrame.WeekdayAllDay);

                if (travelDate.Hour >= WEEKDAY_MORNING_PEAK_START && travelDate.Hour <= WEEKDAY_MORNING_PEAK_END)
                {
                    timeFrames.Add(TravelTimeFrame.WeekdayMorningPeak);
                }
                else if (travelDate.Hour >= WEEKDAY_AFTERNOON_PEAK_START && travelDate.Hour <= WEEKDAY_AFTERNOON_PEAK_END)
                {
                    timeFrames.Add(TravelTimeFrame.WeekdayAfternoonPeak);
                }
            }
            return timeFrames;
        }

        //public IList<NotificationMessage> GetMessagesForDisruptions()
        //{
        //    var disruptions = _disruptionsRepository.GetDisruptions();
        //    var notificationMessages = new List<NotificationMessage>();

        //    foreach (var disruption in disruptions)
        //    {
        //        foreach (var disruptedRoute in disruption.DisruptedRoutes)
        //        {
        //            var notificationUsers = GetUsersForDisruptedRoute(disruption.Id, disruptedRoute);
        //            var messages = GetMessagesForDisruption(notificationUsers, disruption.Id,
        //                disruptedRoute.RouteNo, disruptedRoute.Message);
        //            notificationMessages.AddRange(messages);
        //        }
        //    }
        //    return notificationMessages;
        //}


        //public IList<NotificationMessage> GetMessagesForSpecialEvents()
        //{
        //    var eventMessages = GetSpecialEventMessages();
        //    var notificationMessages = new List<NotificationMessage>();

        //    foreach (var eventMessage in eventMessages)
        //    {
        //        var notificationUsers = GetUsersForUpcomingSpecialEvent(eventMessage);
        //        foreach (var notificationUser in notificationUsers)
        //        {
        //            var msg = new NotificationMessage
        //            {
        //                DeviceId = notificationUser.DeviceId,
        //                DeviceType = notificationUser.DeviceType,
        //                DisruptionId = 0,
        //                Message = eventMessage.Message,
        //                MessageType = MessageType.ServiceUpdate,
        //                NotificationSentTime = DateTime.Now,
        //                NotificationToken = notificationUser.NotificationToken,
        //                RouteNo = eventMessage.RouteNo,
        //                TravelTimeDesc = notificationUser.TimeDescription,
        //                EventId = eventMessage.MessageId
        //            };
        //            notificationMessages.Add(msg);
        //        }
        //    }
        //    return notificationMessages;
        //}



        //private IEnumerable<NotificationMessage> GetMessagesForDisruption(
        //    IEnumerable<NotificationSubscriber> notificationUsers, int disruptionId,
        //    int routeNo, string disruptionMessage)
        //{
        //    var notificationMessages = new List<NotificationMessage>();

        //    foreach (var notificationUser in notificationUsers)
        //    {
        //        var msg = new NotificationMessage
        //        {
        //            DeviceId = notificationUser.DeviceId,
        //            DeviceType = notificationUser.DeviceType,
        //            Message = disruptionMessage,
        //            MessageType = MessageType.Disruption,
        //            NotificationToken = notificationUser.NotificationToken,
        //            NotificationSentTime = DateTime.Now,
        //            RouteNo = routeNo,
        //            TravelTimeDesc = notificationUser.TimeDescription,
        //            DisruptionId = disruptionId
        //        };
        //        notificationMessages.Add(msg);
        //    }
        //    return notificationMessages;
        //} 

        //private IEnumerable<NotificationSubscriber> GetUsersForDisruptedRoute(int disruptionId, DisruptedRoute disruptedRoute)
        //{
        //    var travelTimeFrames = GetTravelTimesForDateTime(DateTime.Now);

        //    var notificationUsers = new List<NotificationSubscriber>();

        //    foreach (var travelTimeFrame in travelTimeFrames)
        //    {
        //        //message id is set to zero because we're only logging message id's for event messages at this time.
        //        var users = _notificationSubscribersRepository.GetDisruptionNotificationsByRouteAndTravelTime(routeNo:disruptedRoute.RouteNo,
        //            disruptionId: disruptionId, travelTimeFrame: travelTimeFrame);
        //        notificationUsers.AddRange(users);
        //    }

        //    return notificationUsers;
        //}

        //private IEnumerable<NotificationSubscriber> GetUsersForUpcomingSpecialEvent(SpecialEventMessage eventMessage)
        //{
        //    var travelTimeFrames = GetTravelTimesForTimeSpan(eventMessage.StartDate, eventMessage.EndDate);

        //    var notificationUsers = new List<NotificationSubscriber>();

        //    foreach (var travelTimeFrame in travelTimeFrames)
        //    {
        //        //message id is set to zero because we're only logging message id's for event messages at this time.
        //        var users = _notificationSubscribersRepository.GetSpecialEventsNotificationsByRouteAndTravelTime(routeNo: eventMessage.RouteNo,
        //            messageId: eventMessage.MessageId, travelTimeFrame: travelTimeFrame);
        //        notificationUsers.AddRange(users);
        //    }

        //    return notificationUsers.Distinct().ToList();

        //} 

    }
}