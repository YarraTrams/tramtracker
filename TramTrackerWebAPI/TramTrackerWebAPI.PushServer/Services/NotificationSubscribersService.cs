﻿using TramTrackerWebAPI.PushServer.Interfaces;

namespace TramTrackerWebAPI.PushServer.Services
{
    public class NotificationSubscribersService : INotificationSubscribersService
    {
        private readonly INotificationRequestRepository _notificationRequestRepository;
        private readonly INotificationTimeRepository _notificationTimeRepository;
        private readonly INotificationSubscribersRepository _notificationSubscribersRepository;

        public NotificationSubscribersService(INotificationRequestRepository notificationRequestRepository, 
            INotificationTimeRepository notificationTimeRepository,
            INotificationSubscribersRepository notificationSubscribersRepository)
        {
            _notificationRequestRepository = notificationRequestRepository;
            _notificationTimeRepository = notificationTimeRepository;
            _notificationSubscribersRepository = notificationSubscribersRepository;   
        }

        public void UpdateNotificationToken(string deviceId, string newAuthToken)
        {
            var user = _notificationSubscribersRepository.GetNotificationSubscriberByDeviceId(deviceId);

            if (user != null)
            {
                user.NotificationToken = newAuthToken;
                _notificationSubscribersRepository.UpdateNotificationSubscriber(user);
            }
        }

        public void DeregisterDevice(string deviceId)
        {
            _notificationRequestRepository.DeleteAllNotificationRequests(deviceId);

            _notificationTimeRepository.DeleteAllNotificationTimes(deviceId);

            _notificationSubscribersRepository.DeleteNotificationSubscriber(deviceId);
        } 
    }
}