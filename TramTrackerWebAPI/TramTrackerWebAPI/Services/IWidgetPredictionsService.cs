﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Services
{
    public interface IWidgetPredictionsService
    {
        Task<IEnumerable<WidgetPrediction>> GetWidgetPredictions(int routeNo, int trackerId, bool isLowFloorOnly);
    }
}