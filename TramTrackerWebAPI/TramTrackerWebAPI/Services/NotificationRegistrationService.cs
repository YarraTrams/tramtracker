﻿using System.Threading.Tasks;
using TramTrackerWebAPI.Dapper;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Services
{
    public class NotificationRegistrationService : INotificationRegistrationService
    {
        private readonly INotificationSubscriberRepository _notificationSubscriberRepository;
        private readonly INotificationRequestRepository _notificationRequestRepository;
        private readonly INotificationTimeRepository _notificationTimeRepository;

        public NotificationRegistrationService(INotificationSubscriberRepository notificationSubscriberRepository, INotificationTimeRepository notificationTimeRepository,
             INotificationRequestRepository notificationRequestRepository)
        {
            _notificationSubscriberRepository = notificationSubscriberRepository;
            _notificationRequestRepository = notificationRequestRepository;
            _notificationTimeRepository = notificationTimeRepository;
        }

        public async Task<NotificationSubscriber> GetDevice(string deviceId)
        {
            if (string.IsNullOrEmpty(deviceId)) return null;

            return await _notificationSubscriberRepository.GetNotificationSubscriberByDeviceId(deviceId);
        }

       
        public async Task RegisterDevice(NotificationSubscriber notificationSubscriber)
        {
            if (notificationSubscriber == null) return;

            //Device could have multiple tokens or device id's associated with it
            var subscribers = await _notificationSubscriberRepository.GetNotificationSubscriberByDeviceIdOrToken(notificationSubscriber.DeviceId, notificationSubscriber.NotificationToken);

            foreach (var subscriber in subscribers)
            {
                await _notificationTimeRepository.DeleteAllNotificationTimes(subscriber.DeviceId);
                await _notificationRequestRepository.DeleteAllNotificationRequests(subscriber.DeviceId);
                await _notificationSubscriberRepository.DeleteNotificationSubscriber(subscriber.DeviceId);
            }

            await _notificationSubscriberRepository.CreateNotificationSubsciber(notificationSubscriber);
        }

        //public async Task<NotificationSubscriber> UpdateDevice(NotificationSubscriber notificationSubscriber)
        //{
        //    if (notificationSubscriber == null) return null;

        //    var device =
        //        await _notificationSubscriberRepository.GetNotificationSubscriberByDeviceId(notificationSubscriber.DeviceId);

        //    if (device != null)
        //    {
        //        await _notificationSubscriberRepository.UpdateNotificationSubscriber(notificationSubscriber);
        //    }
        //    return await _notificationSubscriberRepository.GetNotificationSubscriberByDeviceId(notificationSubscriber.DeviceId);
        //}

        public async Task DeregisterDevice(string deviceId)
        {
            if (!string.IsNullOrEmpty(deviceId))
            {
                await _notificationRequestRepository.DeleteAllNotificationRequests(deviceId);

                await _notificationTimeRepository.DeleteAllNotificationTimes(deviceId);

                await _notificationSubscriberRepository.DeleteNotificationSubscriber(deviceId);
            }
        }
    }
}