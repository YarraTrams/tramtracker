﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TramTrackerWebAPI.Dapper;
using TramTrackerWebAPI.Helpers;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Services
{
    public class WidgetPredictionsService : IWidgetPredictionsService
    {
        private readonly IPredictionsRepository _predictionsRepository;

        public WidgetPredictionsService(IPredictionsRepository predictionsRepository)
        {
            _predictionsRepository = predictionsRepository;
        }

        public async Task<IEnumerable<WidgetPrediction>> GetWidgetPredictions(int routeNo, int trackerId, bool isLowFloorOnly)
        {
            var rawPredictions = await _predictionsRepository.GetPredictionsForStop(routeNo, trackerId, isLowFloorOnly);

            var widgetPredictions = new List<WidgetPrediction>();

            foreach (var p in rawPredictions)
            {
                var wp = new WidgetPrediction();
                wp.RouteNo = p.RouteNo;
                wp.TramNo = p.VehicleNo;
                wp.HeadboardRouteNo = p.HeadboardRouteNo;
                wp.Destination = p.Destination;
                wp.IsLowFloor = p.LowFloor;
                wp.HasAirCon = p.DisplayAirCondition;
                wp.HasDisruption = (p.TtDmsMessage != "") || p.DisplayFocMessage;
                wp.HasSpecialEvent = (p.SpecialEventMessage != "") && (widgetPredictions.Count != 1);
                wp.ArrivalTime = PredictionHelpers.GetArrivalDateTime(p.Schedule, p.Adjustment);
                widgetPredictions.Add(wp);
            }

            var numDistinctRoutes = widgetPredictions.Select(p => p.RouteNo).Distinct().Count();

            if (numDistinctRoutes != 1)
            {
                return widgetPredictions.OrderBy(p => p.ArrivalTime)
                    .GroupBy(p => p.RouteNo).Select(p => p.First()).Take(3);
            }

            return widgetPredictions.OrderBy(p => p.ArrivalTime).Take(3);
        }
    }
}