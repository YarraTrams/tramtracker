﻿using System.Threading.Tasks;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Services
{
    public interface INotificationRegistrationService
    {
        Task<NotificationSubscriber> GetDevice(string deviceId);
        Task RegisterDevice(NotificationSubscriber notificationSubscriber);
        //Task<NotificationSubscriber> UpdateDevice(NotificationSubscriber notificationSubscriber);
        Task DeregisterDevice(string deviceId);
    }
}