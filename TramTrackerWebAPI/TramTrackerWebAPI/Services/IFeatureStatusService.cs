﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Services
{
    public interface IFeatureStatusService
    {
        Task<IEnumerable<FeatureStatus>> GetAllFeatureStatus();
    }
}