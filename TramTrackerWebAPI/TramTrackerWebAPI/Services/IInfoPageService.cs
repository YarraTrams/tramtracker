﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TramTrackerWebAPI.Enums;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Services
{
    public interface IInfoPageService
    {
        Task<IEnumerable<InfoPage>> GetPublishedInfoPages();
    }
}