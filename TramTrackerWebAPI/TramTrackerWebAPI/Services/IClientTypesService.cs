﻿using System.Threading.Tasks;

namespace TramTrackerWebAPI.Services
{
    public interface IClientTypesService
    {
        Task<bool> IsValidClientType(string clientTypeId);
    }
}
