﻿using System;
using System.Linq;
using System.Threading.Tasks;
using TramTrackerWebAPI.Dapper;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Services
{
    public class RouteUpdatesService : IRouteUpdatesService
    {
        IRouteUpdatesRepository _routeUpdatesRepository;
        public RouteUpdatesService(IRouteUpdatesRepository routeUpdatesRepository)
        {
            _routeUpdatesRepository = routeUpdatesRepository;
        }

        public async Task<RouteUpdates> GetRouteUpdatesCollection(DateTime? since = null)
        {

            RouteUpdates routeUpdates = new RouteUpdates();

            //--- Get updated stops information
            var resultStops = await _routeUpdatesRepository.GetUpdatedStopsCollection(since);
            routeUpdates.UpdatedStops = resultStops.Where(r => !r.Removed);
            routeUpdates.DeletedStops = resultStops.Where(r => r.Removed).Select(r => new DeletedStop() { TrackerId = r.StopNo });

            //--- Get updated routes information
            var resultRoutes = await _routeUpdatesRepository.GetUpdatedRoutesCollection(since);
            routeUpdates.UpdatedRoutes = resultRoutes.Where(r => r.RouteAvailable);
            routeUpdates.DeletedRoutes = resultRoutes.Where(r => !r.RouteAvailable).Select(r => new DeletedRoute() { RouteNo = r.RouteNo });

            //--- Get updated ticket outlets information
            var resultOutlets = await _routeUpdatesRepository.GetUpdatedTicketOutletsCollection(since);
            routeUpdates.UpdatedTicketOutlets = resultOutlets.Where(r => !r.IsDeleted);
            routeUpdates.DeletedTicketOutlets = resultOutlets.Where(r => r.IsDeleted).Select(r => new DeletedTicketOutlet() { Id = r.ID });

            //--- Get updated points of interest information
            var resultPoi = await _routeUpdatesRepository.GetUpdatedPointsOfInterestCollection(since);
            routeUpdates.UpdatedPoi = resultPoi.Where(r => !r.IsDeleted);
            routeUpdates.DeletedPoi = resultPoi.Where(r => r.IsDeleted).Select(r => new DeletedPointOfInterest { Id = r.POIId });

            //--- Get updated network maps
            var resultNetworkMaps = await _routeUpdatesRepository.GetUpdatedNetworkMapsCollection(since);
            routeUpdates.AddedNetworkMaps = resultNetworkMaps;

            return routeUpdates;
        }
    }
}
