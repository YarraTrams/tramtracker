﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TramTrackerWebAPI.Dapper;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Services
{
    public class FeatureStatusService : IFeatureStatusService
    {
        private readonly IFeatureStatusRepository _featureStatusRepository;

        public FeatureStatusService(IFeatureStatusRepository featureStatusRepository)
        {
            _featureStatusRepository = featureStatusRepository;
        }

        public async Task<IEnumerable<FeatureStatus>> GetAllFeatureStatus()
        {
            var featureStatus = await _featureStatusRepository.GetAllFeatureStatus();
            return featureStatus;
        }
    }
}