﻿using System.Threading.Tasks;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Services
{
    public interface INotificationRequestService
    {
        //Task<IEnumerable<NotificationRequest>> GetNotificationRequestsByDeviceId(string deviceId);
        //Task<NotificationRequest> GetNotificationRequestById(int requestId);
        Task<NotificationRequest> CreateNotificationRequest(NotificationRequest notificationRequest);
        //Task DeleteNotificationRequest(int requestId);
    }
}