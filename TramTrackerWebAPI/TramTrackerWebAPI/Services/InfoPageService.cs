﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TramTrackerWebAPI.Dapper;
using TramTrackerWebAPI.Enums;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Services
{
    public class InfoPageService : IInfoPageService
    {
        private readonly IInfoPagesRepository _infoPagesRepository;

        public InfoPageService(IInfoPagesRepository infoPagesRepository)
        {
            _infoPagesRepository = infoPagesRepository;
        }

        public async Task<IEnumerable<InfoPage>> GetPublishedInfoPages()
        {
            return await _infoPagesRepository.GetPublishedInfoPages();
        }
    }
}