﻿using System;
using System.Threading.Tasks;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Services
{
    public interface IRouteUpdatesService
    {
        Task<RouteUpdates> GetRouteUpdatesCollection(DateTime? since = null);
    }
}
