﻿using System;
using System.Threading.Tasks;
using TramTrackerWebAPI.Common.Enums;
using TramTrackerWebAPI.Dapper;
using TramTrackerWebAPI.Enums;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Services
{
    public class NotificationRequestService : INotificationRequestService
    {

        private readonly INotificationRequestRepository _notificationRequestRepository;
        private readonly INotificationTimeRepository _notificationTimeRepository;
        private readonly INotificationSubscriberRepository _notificationSubscriberRepository;

        public NotificationRequestService(INotificationRequestRepository notificationRequestRepository, INotificationSubscriberRepository notificationSubscriberRepository,
            INotificationTimeRepository notificationTimeRepository)
        {
            _notificationRequestRepository = notificationRequestRepository;
            _notificationTimeRepository = notificationTimeRepository;
            _notificationSubscriberRepository = notificationSubscriberRepository;
        }


        //public async Task<IEnumerable<RouteNotification>> GetNotificationRequestsByDeviceId(string deviceId)
        //{
        //    return await _notificationRequestRepository.GetNotificationsRequestByDeviceId(deviceId);
        //}

        //public async Task<NotificationRequest> GetNotificationRequestById(int requestId)
        //{
        //    return await _notificationRequestRepository.GetNotificationRequestById(requestId);
        //}

        public async Task<NotificationRequest> CreateNotificationRequest(NotificationRequest notificationRequest)
        {
            if (notificationRequest == null) return null;

            var device = await _notificationSubscriberRepository.GetNotificationSubscriberByDeviceId(notificationRequest.DeviceId);

            if (device == null) return null;

            await _notificationTimeRepository.DeleteAllNotificationTimes(device.DeviceId);
            await _notificationRequestRepository.DeleteAllNotificationRequests(device.DeviceId);

            await CreateNotificationTimes(notificationRequest);
            await CreateRouteNotifications(notificationRequest);

            return notificationRequest;
        }

        private async Task CreateRouteNotifications(NotificationRequest notificationRequest)
        {
            foreach (var route in notificationRequest.Routes)
            {
                var routeNotification = new RouteNotification
                {
                    DateCreated = DateTime.Now,
                    DeviceId = notificationRequest.DeviceId,
                    RouteNo = route
                };

                await _notificationRequestRepository.CreateNotificationRequest(routeNotification);
            }
        }


        private async Task CreateNotificationTimes(NotificationRequest notificationRequest)
        {
            foreach (var travelTime in notificationRequest.TravelTimeFrames)
            {
                if(Enum.IsDefined(typeof(TravelTimeFrame), travelTime))
                {
                    var notifyTime = new NotificationTimeFrame
                    {
                        NotificationDevice = notificationRequest.DeviceId,
                        TravelTimeId = (int)travelTime
                    };
                    await _notificationTimeRepository.CreateNotificationTime(notifyTime);
                }
            }
        }
    }
}