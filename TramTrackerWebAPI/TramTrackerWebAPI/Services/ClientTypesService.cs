﻿using System;
using System.Threading.Tasks;
using TramTrackerWebAPI.Dapper;

namespace TramTrackerWebAPI.Services
{
    public class ClientTypesService : IClientTypesService
    {
        private readonly IClientTypeRepository _clientTypeRepository;

        public ClientTypesService(IClientTypeRepository clientTypeRepository)
        {
            _clientTypeRepository = clientTypeRepository;
        }

        public async Task<bool> IsValidClientType(string clientTypeId)
        {
            var clientType = await _clientTypeRepository.GetClientTypeById(clientTypeId);
            bool isValidClient = false;

            if (clientType != null)
            {
                isValidClient = String.Equals(clientType.ClientType.Trim(), clientTypeId.Trim(), StringComparison.CurrentCultureIgnoreCase);
            }

            return isValidClient;
        }
    }
}