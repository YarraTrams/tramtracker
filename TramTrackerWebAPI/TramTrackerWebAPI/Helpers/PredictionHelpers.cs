﻿using System;

namespace TramTrackerWebAPI.Helpers
{
    public static class PredictionHelpers
    {
        public static DateTime GetArrivalDateTime(DateTime scheduledArrival, int adjustment)
        {
            DateTime arrivalDateTime = ((DateTime)scheduledArrival).AddSeconds(Convert.ToDouble(adjustment));
            return (arrivalDateTime > DateTime.Now) ? arrivalDateTime : DateTime.Now.AddSeconds(10);
        }
    }
}