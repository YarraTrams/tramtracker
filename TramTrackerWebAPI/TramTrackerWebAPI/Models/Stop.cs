﻿using Newtonsoft.Json;

namespace TramTrackerWebAPI.Models
{
    public class Stop
    {
        [JsonProperty(PropertyName = "trackerId")]
        public int StopNo { get; set; }
        public string Direction { get; set; }
        public string ConnectingBuses { get; set; }
        public string ConnectingTrains { get; set; }
        public string ConnectingTrams { get; set; }
        public string FlagStopNo { get; set; }
        public bool IsCityStop { get; set; }
        public bool IsEasyAccess { get; set; }
        public bool IsInFreeZone { get; set; }
        public bool IsShelter { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string StopName { get; set; }
        public int PostCode { get; set; }
        public string SuburbName  { get; set; }
        public string Zones { get; set; }
        [JsonIgnore]
        public bool Removed { get; set; }
        [JsonIgnore]
        public bool UpStop { get; set; }
    }
}