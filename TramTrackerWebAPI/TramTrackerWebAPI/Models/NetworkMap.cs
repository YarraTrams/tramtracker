﻿using System;

namespace TramTrackerWebAPI.Models
{
    public class NetworkMap
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string Url { get; set; }
        public DateTime ActiveDate { get; set; }
    }
}