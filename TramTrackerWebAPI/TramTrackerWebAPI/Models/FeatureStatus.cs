﻿namespace TramTrackerWebAPI.Models
{
    public class FeatureStatus
    {
        public int Id { get; set; }
        public string FeatureName { get; set; }
        public bool IsEnabled { get; set; }
    }
}