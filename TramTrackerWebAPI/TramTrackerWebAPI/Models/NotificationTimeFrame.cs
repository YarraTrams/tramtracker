﻿namespace TramTrackerWebAPI.Models
{
    public class NotificationTimeFrame
    {
        public int TravelTimeId { get; set; }
        public string NotificationDevice { get; set; }
    }
}