﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using TramTrackerWebAPI.Common.Enums;
using TramTrackerWebAPI.Helpers;

namespace TramTrackerWebAPI.Models
{
    public class NotificationSubscriber
    {
        public NotificationSubscriber()
        {
            this.TravelTimeFrames = new List<TravelTimeFrame>();
        }
        [Required]
        public string DeviceId { get; set; }
        [Required]
        public string NotificationToken { get; set; }
        [Required]
        public string DeviceType { get; set; }

        [JsonIgnore]
        public ICollection<TravelTimeFrame> TravelTimeFrames { get; set; }

        [JsonConverter(typeof(CustomDateTimeConverter))] 
        public DateTime DateCreated { get; set; }
          
        [JsonConverter(typeof(CustomDateTimeConverter))] 
        public DateTime DateModified { get; set; } 
    }
}