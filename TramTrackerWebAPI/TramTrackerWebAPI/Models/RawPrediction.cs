﻿using System;

namespace TramTrackerWebAPI.Models
{
    public class RawPrediction
    {
        public int Prediction { get; set; }
        public string RunNo { get; set; }
        public string StopId { get; set; }
        public int StopNo { get; set; }
        public int InternalRouteNo { get; set; }
        public int RouteNo { get; set; }
        public string HeadboardRouteNo { get; set; }
        public string Destination { get; set; }
        public float StopDistance { get; set; }
        public float TramDistance { get; set; }
        public int Deviation { get; set; }
        public DateTime AvmTime { get; set; }
        public int VehicleNo { get; set; }
        public bool LowFloor { get; set; }
        public bool Down { get; set; }
        public DateTime Schedule { get; set; }
        public int Adjustment { get; set; }
        public bool DisplayPrediction { get; set; }
        public string SpecialEventMessage { get; set; }
        public string TtDmsMessage { get; set; }
        public bool DisplayFocMessage { get; set; }
        public bool DisplayAirCondition { get; set; }
    }
}