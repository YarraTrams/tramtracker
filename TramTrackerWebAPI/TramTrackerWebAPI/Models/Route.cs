﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace TramTrackerWebAPI.Models
{
    public class Route
    {
        public Route()
        {
            this.UpStops = new List<RouteStop>();
            this.DownStops = new List<RouteStop>();
        }

        public int RouteNo { get; set; }
        public string HeadboardNo { get; set; }
        public string UpDestination { get; set; }
        public string DownDestination { get; set; }
        public bool IsMainRoute { get; set; }
        public string Colour { get; set; }
        public ICollection<RouteStop> UpStops { get; set; }
        public ICollection<RouteStop> DownStops { get; set; }
        [JsonIgnore]
        public bool RouteAvailable { get; set; }
    }
}