﻿using System;
using Newtonsoft.Json;
using TramTrackerWebAPI.Helpers;

namespace TramTrackerWebAPI.Models
{
    public class WidgetPrediction
    {
        public int RouteNo { get; set; }
        public string HeadboardRouteNo { get; set; }
        public int TramNo { get; set; }
        public string Destination { get; set; }
        public bool IsLowFloor { get; set; }
        public bool HasAirCon { get; set; } 

        [JsonConverter(typeof(CustomDateTimeConverter))] 
        public DateTime ArrivalTime { get; set; }
        public bool HasSpecialEvent { get; set; }
        public bool HasDisruption { get; set; }
    }
}