﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace TramTrackerWebAPI.Models
{
    public class PointOfInterest
    {
        public PointOfInterest()
        {
            this.Stops = new List<PoiStop>();
        }
        public string CategoryName { get; set; }
        public bool DisabledAccess { get; set; }
        public string EmailAddress { get; set; }
        public bool HasEntryFee { get; set; }
        public bool HasToilets { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string MoreInfo { get; set; }
        public string Name { get; set; }
        public string OpeningHours { get; set; }
        public string PoiDescription { get; set; }
        public string PhoneNumber { get; set; }
        public string StreetAddress { get; set; }
        public string Suburb { get; set; }
        public string WebAddress { get; set; }
        public int POIId { get; set; }
        [JsonIgnore]
        public bool IsDeleted { get; set; }
        public ICollection<PoiStop> Stops { get; set; } 
    }
}