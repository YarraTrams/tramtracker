﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using TramTrackerWebAPI.Common.Enums;
using TramTrackerWebAPI.Validation;

namespace TramTrackerWebAPI.Models
{
    public class NotificationRequest
    {
        public NotificationRequest()
        {
            this.TravelTimeFrames = new List<TravelTimeFrame>();
        }
        [JsonIgnore]
        public int Id { get; set; }

        public string DeviceId { get; set; }
        [Required]
        [MinCollectionLength(1)]
        public ICollection<int> Routes { get; set; }

        [Required]
        [MinCollectionLength(1)]
        public ICollection<TravelTimeFrame> TravelTimeFrames { get; set; }
        
        [JsonIgnore]
        public DateTime DateCreated { get; set; }
    }
}