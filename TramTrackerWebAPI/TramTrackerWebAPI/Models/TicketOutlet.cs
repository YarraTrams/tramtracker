﻿using Newtonsoft.Json;

namespace TramTrackerWebAPI.Models
{
    public class TicketOutlet
    {
        public string Address { get; set; }
        public bool HasMyki { get; set; }
        public bool HasMykiTopUp { get; set; }
        public bool Is24Hour { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Name { get; set; }
        public string Suburb { get; set; }
        public int ID { get; set; }
        [JsonIgnore]
        public bool IsDeleted { get; set; }
    }
}