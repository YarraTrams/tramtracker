﻿using TramTrackerWebAPI.Enums;

namespace TramTrackerWebAPI.Models
{
    public class InfoPage
    {
        public int Id { get; set; }
        public int SortOrder { get; set; }
        public string Url { get; set; }
        public DeviceType DeviceType { get; set; }
    }
}