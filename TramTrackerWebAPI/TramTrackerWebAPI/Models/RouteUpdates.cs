﻿using System.Collections.Generic;

namespace TramTrackerWebAPI.Models
{
    public class RouteUpdates
    {
        public IEnumerable<Stop> UpdatedStops { get; set; }
        public IEnumerable<DeletedStop> DeletedStops { get; set; }
        public IEnumerable<Route> UpdatedRoutes { get; set; }
        public IEnumerable<DeletedRoute> DeletedRoutes { get; set; }
        public IEnumerable<TicketOutlet> UpdatedTicketOutlets { get; set; }
        public IEnumerable<DeletedTicketOutlet> DeletedTicketOutlets { get; set; }
        public IEnumerable<PointOfInterest> UpdatedPoi { get; set; }
        public IEnumerable<DeletedPointOfInterest> DeletedPoi { get; set; }
        public IEnumerable<NetworkMap> AddedNetworkMaps { get; set; }
    }
}