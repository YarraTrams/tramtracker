﻿using System;

namespace TramTrackerWebAPI.Models
{
    public class RouteNotification
    {
        public int Id { get; set; }
        public string DeviceId { get; set; }        
        public int RouteNo { get; set; }
        public DateTime DateCreated { get; set; }
    }
}