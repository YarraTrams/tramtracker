﻿namespace TramTrackerWebAPI.Models
{
    public class RouteStop : Stop
    {
        public int StopSequence { get; set; }
        public string TurnType { get; set; }
        public string TurnMessage { get; set; }
    }
}