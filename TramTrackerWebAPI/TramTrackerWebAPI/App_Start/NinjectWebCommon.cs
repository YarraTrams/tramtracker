

using System;
using System.Configuration;
using System.Web;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Web.Common;
using TramTrackerWebAPI.App_Start;
using TramTrackerWebAPI.Dapper;
using TramTrackerWebAPI.Services;
using WebActivatorEx;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NinjectWebCommon), "Start")]
[assembly: ApplicationShutdownMethod(typeof(NinjectWebCommon), "Stop")]

namespace TramTrackerWebAPI.App_Start
{
    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);

        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IRouteUpdatesRepository>().To<RouteUpdatesRepository>();
            kernel.Bind<IRouteUpdatesService>().To<RouteUpdatesService>();
            kernel.Bind<IClientTypesService>().To<ClientTypesService>();
            kernel.Bind<IClientTypeRepository>().To<ClientTypeRepository>();
            kernel.Bind<IFeatureStatusRepository>().To<FeatureStatusRepository>();
            kernel.Bind<IFeatureStatusService>().To<FeatureStatusService>();
            kernel.Bind<IPredictionsRepository>().To<PredictionsRepository>();
            kernel.Bind<IWidgetPredictionsService>().To<WidgetPredictionsService>();
            kernel.Bind<INotificationRequestRepository>().To<NotificationRequestRepository>();
            kernel.Bind<INotificationSubscriberRepository>().To<NotificationSubscriberRepository>();
            kernel.Bind<INotificationRequestService>().To<NotificationRequestService>();
            kernel.Bind<INotificationRegistrationService>().To<NotificationRegistrationService>();
            kernel.Bind<INotificationTimeRepository>().To<NotificationTimeRepository>();
            kernel.Bind<IInfoPagesRepository>().To<InfoPagesRepository>();
            kernel.Bind<IInfoPageService>().To<InfoPageService>();

            kernel.Bind<IDbConnectionFactory>().To<SqlConnectionFactory>()
                .WithConstructorArgument("connectionString", ConfigurationManager.ConnectionStrings["tramTrackerDB"].ConnectionString);
        }      
  
    }
}

    