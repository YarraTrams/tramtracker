﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Ninject;
using TramTrackerWebAPI.Services;

namespace TramTrackerWebAPI.Filters
{
    public class ApiAuthorisationFilter : ActionFilterAttribute
    {
        [Inject]
        public IClientTypesService ClientTypesService { get; set; }

        public async override Task OnActionExecutingAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            var queryParameters = actionContext.Request.GetQueryNameValuePairs();

            var token = queryParameters.Where(kvp => string.Equals(kvp.Key, "token", StringComparison.CurrentCultureIgnoreCase)).Select(kvp => kvp.Value).FirstOrDefault();
            var authId = queryParameters.Where(kvp => string.Equals(kvp.Key, "authId", StringComparison.CurrentCultureIgnoreCase)).Select(kvp => kvp.Value).FirstOrDefault();


            if (string.IsNullOrEmpty(token) || string.IsNullOrEmpty(authId))
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }

            var isValidClient =  await ClientTypesService.IsValidClientType(authId);

            if (!isValidClient)
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }
            await base.OnActionExecutingAsync(actionContext, cancellationToken);
        }
    }
}