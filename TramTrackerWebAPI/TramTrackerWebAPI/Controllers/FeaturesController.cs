﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TramTrackerWebAPI.Filters;
using TramTrackerWebAPI.Models;
using TramTrackerWebAPI.Services;

namespace TramTrackerWebAPI.Controllers
{
    //[JsonOnlyAttribute]
    public class FeaturesController : ApiController
    {
        private readonly IFeatureStatusService _featureStatusService;

        public FeaturesController(IFeatureStatusService featureStatusService)
        {
            _featureStatusService = featureStatusService;
        }

        // GET: api/Features
        /// <summary>
        /// Get status of tramTRACKER features
        /// </summary>
        /// <remarks>
        /// Get the status of tramTRACKER features (eg. Advertising, BlueCats etc).
        /// </remarks>
        /// <param name="authId">Auth ID/Client Type</param>
        /// <param name="token">Device Token</param>
        /// <response code="403">Unauthorised</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorisationFilter]
        [ResponseType(typeof(FeatureStatus))]
        public async Task<IHttpActionResult> Get(string authId = null, string token = null)
        {
            try
            {
                var featureStatus = await _featureStatusService.GetAllFeatureStatus();
                return Ok(featureStatus);
            }
            catch (Exception ex)
            {
                return InternalServerError();
            }
        }
    }
}
