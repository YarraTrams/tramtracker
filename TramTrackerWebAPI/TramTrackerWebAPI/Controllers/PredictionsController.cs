﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TramTrackerWebAPI.Filters;
using TramTrackerWebAPI.Models;
using TramTrackerWebAPI.Services;

namespace TramTrackerWebAPI.Controllers
{
   // [JsonOnlyAttribute]
    public class PredictionsController : ApiController
    {
        private readonly IWidgetPredictionsService _widgetPredictionsService;

        public PredictionsController(IWidgetPredictionsService widgetPredictionsService)
        {
            _widgetPredictionsService = widgetPredictionsService;
        }

        // GET: api/Predictions

        /// <summary>
        /// Get predictions for tramTRACKER widget
        /// </summary>
        /// <remarks>
        /// Gets a list of predictions for tramTRACKER widget
        /// </remarks>
        /// <param name="authId">Auth ID/Client Type</param>
        /// <param name="token">Device Token</param>
        /// <response code="400">Bad Request</response>
        /// <response code="403">Unauthorised</response>
        /// <response code="500">Internal Server Error</response>
        /// 
        [ApiAuthorisationFilter]
        [ResponseType(typeof(IEnumerable<WidgetPrediction>))]
        [Route("api/predictions/widget/{trackerId}")]
        public async Task<IHttpActionResult> Get(int trackerId, string authId, string token, int? routeNo = 0, bool? lowFloorOnly = false)
        {
            try
            {
                if (trackerId == 0) return BadRequest();

                var result =
                    await _widgetPredictionsService.GetWidgetPredictions(trackerId, routeNo.Value, lowFloorOnly.Value);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError();
            }
        }

    }
}
