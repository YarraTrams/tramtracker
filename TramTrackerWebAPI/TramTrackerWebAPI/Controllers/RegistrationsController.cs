﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TramTrackerWebAPI.Filters;
using TramTrackerWebAPI.Models;
using TramTrackerWebAPI.Services;

namespace TramTrackerWebAPI.Controllers
{
    public class RegistrationsController : ApiController
    {
        private readonly INotificationRegistrationService _notificationRegistrationService;

        public RegistrationsController(INotificationRegistrationService notificationRegistrationService)
        {
            _notificationRegistrationService = notificationRegistrationService;
        }

        // POST: api/Registrations

        [ApiAuthorisationFilter]
        public async Task<IHttpActionResult> Post([FromBody]NotificationSubscriber notificationSubscriber)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest();
                }

                await _notificationRegistrationService.RegisterDevice(notificationSubscriber);

                return Ok(new
                {
                    msg = "success"
                });
            }
            catch (Exception e)
            {
                return InternalServerError();
            }
        }

        // DELETE: api/Registrations/5
        [ApiAuthorisationFilter]
        [Route("api/Registrations/{deviceId}")]
        public async Task<IHttpActionResult> Delete(string deviceId)
        {
            try
            {
                if (string.IsNullOrEmpty(deviceId))
                {
                    return BadRequest();
                }

                //var device = await _notificationRegistrationService.GetDevice(deviceId);

                //if (device == null)
                //{
                //    return NotFound();
                //}

                await _notificationRegistrationService.DeregisterDevice(deviceId);

                return Ok(new
                {
                    msg = "success"
                });
            }
            catch (Exception e)
            {
                return InternalServerError();
            }
        }
    }
}
