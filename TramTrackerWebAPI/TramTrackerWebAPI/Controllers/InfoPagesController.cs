﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Ninject.Extensions.Logging;
using TramTrackerWebAPI.Enums;
using TramTrackerWebAPI.Filters;
using TramTrackerWebAPI.Models;
using TramTrackerWebAPI.Services;

namespace TramTrackerWebAPI.Controllers
{
    public class InfoPagesController : ApiController
    {
        private IInfoPageService _infoPageService;

        public InfoPagesController(IInfoPageService infoPageService)
        {
            _infoPageService = infoPageService;
        }


        /// <summary>
        /// Get a list of info pages for tramTRACKER
        /// </summary>
        /// <remarks>
        /// Returns a list of info pages that are to be displayed when tramTRACKER is launched.
        /// </remarks>
        /// <param name="authId">Auth ID/Client Type</param>
        /// <param name="token">Device Token</param>
        /// <response code="403">Unauthorised</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorisationFilter]
        [ResponseType(typeof(InfoPage))]
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                var result = await _infoPageService.GetPublishedInfoPages();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.ErrorException(ex.Message, ex);
                return InternalServerError();
            }
        }

    }
}
