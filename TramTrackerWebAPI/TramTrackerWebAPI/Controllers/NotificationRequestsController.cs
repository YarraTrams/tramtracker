﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TramTrackerWebAPI.Filters;
using TramTrackerWebAPI.Models;
using TramTrackerWebAPI.Services;

namespace TramTrackerWebAPI.Controllers
{
    //[JsonOnlyAttribute]
    public class NotificationRequestsController : ApiController
    {
        private readonly INotificationRequestService _notificationRequestService;

        public NotificationRequestsController(INotificationRequestService notificationRequestService)
        {
            _notificationRequestService = notificationRequestService;
        }

        // POST: api/NotificationRequests
        [ApiAuthorisationFilter]
        [ResponseType(typeof(NotificationRequest))]
        public async Task<IHttpActionResult> Post([FromBody]NotificationRequest notificationRequest)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest();
                }


                var request = await _notificationRequestService.CreateNotificationRequest(notificationRequest);

                //request will be null if the device is not registerd.
                if (request == null)
                {
                    return BadRequest();
                }

            }
            catch (Exception ex)
            {
                return InternalServerError();
            }
            return Ok(new
            {
                msg = "success"
            });
        }
    }
}
