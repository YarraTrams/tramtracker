﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TramTrackerWebAPI.Filters;
using TramTrackerWebAPI.Models;
using TramTrackerWebAPI.Services;
using WebApi.OutputCache.V2;

namespace TramTrackerWebAPI.Controllers
{
    //[JsonOnlyAttribute]
    public class RouteUpdatesController : ApiController
    {
        IRouteUpdatesService _routeUpdatesService;
        public RouteUpdatesController(IRouteUpdatesService routeUpdatesService)
        {
            _routeUpdatesService = routeUpdatesService;
        }


        /// <summary>
        /// Get status of tramTRACKER updates
        /// </summary>
        /// <remarks>
        /// Get the status of tramTRACKER updates to routes, stops, ticket outlets, network maps and points of interest.
        /// </remarks>
        /// <param name="authId">Auth ID/Client Type</param>
        /// <param name="token">Device Token</param>
        /// <response code="403">Unauthorised</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorisationFilter]
        [CacheOutput(ClientTimeSpan = 86400, ServerTimeSpan = 86400)]
        [ResponseType(typeof(RouteUpdates))]
        [Route("api/updates/{since:datetime}")]
        public async Task<IHttpActionResult> GetRouteUpdatesCollection(DateTime since, string authId = null, string token = null)
        {
            try
            {
                var routeUpdates = await _routeUpdatesService.GetRouteUpdatesCollection(since);
                return Ok(routeUpdates);
            }
            catch (Exception ex)
            {
                return InternalServerError();
            }
        }
    }
}
