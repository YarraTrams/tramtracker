﻿using System.Collections;
using System.ComponentModel.DataAnnotations;

namespace TramTrackerWebAPI.Validation
{
    public class MinCollectionLengthAttribute : ValidationAttribute
    {
        private readonly int _minElements;

        public MinCollectionLengthAttribute(int minElements)
        {
            _minElements = minElements;
        }

        public override bool IsValid(object value)
        {
            var list = value as ICollection;
            if (list != null)
            {
                return list.Count >= _minElements;
            }
            return false;
        }
    }
}