﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Dapper
{
    public interface IFeatureStatusRepository
    {
        Task<IEnumerable<FeatureStatus>> GetAllFeatureStatus();
    }
}