﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Dapper
{
    public interface IRouteUpdatesRepository
    {
        Task<IEnumerable<Stop>> GetUpdatedStopsCollection(DateTime? since = null);

        Task<IEnumerable<Route>> GetUpdatedRoutesCollection(DateTime? since = null);

        Task<IEnumerable<TicketOutlet>> GetUpdatedTicketOutletsCollection(DateTime? since = null);

        Task<IEnumerable<PointOfInterest>> GetUpdatedPointsOfInterestCollection(DateTime? since = null);

        Task<IEnumerable<NetworkMap>> GetUpdatedNetworkMapsCollection(DateTime? since = null);
    }
}

 