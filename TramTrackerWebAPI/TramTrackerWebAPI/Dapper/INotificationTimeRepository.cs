﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Dapper
{
    public interface INotificationTimeRepository
    {
        //Task<IEnumerable<NotificationTimeFrame>> GetNotificationTimes(string deviceId);
        //Task<NotificationTimeFrame> GetNotificationTime(NotificationTimeFrame notificationTime);
        Task CreateNotificationTime(NotificationTimeFrame notificationTime);
        //Task DeleteNotificationTime(NotificationTimeFrame notificationTime);
        Task DeleteAllNotificationTimes(string deviceId);
    }
}