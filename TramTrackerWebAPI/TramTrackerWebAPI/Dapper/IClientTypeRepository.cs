﻿using System.Threading.Tasks;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Dapper
{
    public interface IClientTypeRepository
    {
        Task<Client> GetClientTypeById(string clientTypeId);
    }
}