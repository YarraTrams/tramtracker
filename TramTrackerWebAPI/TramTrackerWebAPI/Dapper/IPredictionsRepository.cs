﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Dapper
{
    public interface IPredictionsRepository
    {
        Task<IEnumerable<RawPrediction>> GetPredictionsForStop(int trackerId, int routeNo, bool isLowFloorOnly);
    }
}