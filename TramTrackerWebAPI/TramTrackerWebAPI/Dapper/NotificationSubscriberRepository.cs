﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Dapper
{
    public class NotificationSubscriberRepository : INotificationSubscriberRepository
    {

        private readonly IDbConnectionFactory _dbConnectionFactory;

        public NotificationSubscriberRepository(IDbConnectionFactory dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }

        public async Task<NotificationSubscriber> GetNotificationSubscriberByDeviceId(string deviceId)
        {
            using (var cn = await _dbConnectionFactory.CreateConnection())
            {
                const string storedProcedure = "dbo.PushNotifications_GetNotificationSubscriberByDeviceId";

                var notificationSubscribers = await cn.QueryAsync<NotificationSubscriber>(storedProcedure, new { DeviceId = deviceId }, commandType: CommandType.StoredProcedure);

                return notificationSubscribers.SingleOrDefault();
            }
        }

        //public async Task<NotificationSubscriber> GetNotificationSubscriberByToken(string notificationToken)
        //{
        //    using (var cn = await _dbConnectionFactory.CreateConnection())
        //    {
        //        const string storedProcedure = "dbo.PushNotifications_GetNotificationSubscriberByToken";

        //        var parameters = new DynamicParameters();
        //        parameters.Add("@notificationToken", notificationToken, dbType: DbType.String);

        //        var notificationSubscribers = await cn.QueryAsync<NotificationSubscriber>(storedProcedure, parameters, commandType: CommandType.StoredProcedure);

        //        return notificationSubscribers.SingleOrDefault();
        //    }
        //}

        //public async Task<NotificationSubscriber> GetNotificationSubscriberByDeviceIdOrToken(string deviceId, string notificationToken)
        //{
        //    using (var cn = await _dbConnectionFactory.CreateConnection())
        //    {
        //        const string storedProcedure = "dbo.PushNotifications_GetNotificationSubscriberByDeviceIdOrToken";

        //        var parameters = new DynamicParameters();
        //        parameters.Add("@deviceId", deviceId, dbType: DbType.String);
        //        parameters.Add("@notificationToken", notificationToken, dbType: DbType.String);

        //        var notificationSubscribers = await cn.QueryAsync<NotificationSubscriber>(storedProcedure, parameters, commandType: CommandType.StoredProcedure);

        //        return notificationSubscribers.SingleOrDefault();
        //    }
        //}

        public async Task<IList<NotificationSubscriber>> GetNotificationSubscriberByDeviceIdOrToken(string deviceId, string notificationToken)
        {
            using (var cn = await _dbConnectionFactory.CreateConnection())
            {
                const string storedProcedure = "dbo.PushNotifications_GetNotificationSubscriberByDeviceIdOrToken";

                var parameters = new DynamicParameters();
                parameters.Add("@deviceId", deviceId, dbType: DbType.String);
                parameters.Add("@notificationToken", notificationToken, dbType: DbType.String);

                var notificationSubscribers = await cn.QueryAsync<NotificationSubscriber>(storedProcedure, parameters, commandType: CommandType.StoredProcedure);

                return notificationSubscribers.ToList();
            }
        }

        public async Task<NotificationSubscriber> CreateNotificationSubsciber(NotificationSubscriber notificationSubscriber)
        {
            using (var cn = await _dbConnectionFactory.CreateConnection())
            {
                const string storedProcedure = "dbo.PushNotifications_CreateNotificationSubscriber";
                notificationSubscriber.DateCreated = DateTime.Now;
                notificationSubscriber.DateModified = DateTime.Now;

                var parameters = new DynamicParameters();
                parameters.Add("@deviceId", notificationSubscriber.DeviceId, dbType: DbType.String);
                parameters.Add("@notificationToken", notificationSubscriber.NotificationToken, dbType: DbType.String);
                parameters.Add("@deviceType", notificationSubscriber.DeviceType, dbType: DbType.String);
                parameters.Add("@dateCreated", notificationSubscriber.DateCreated, dbType: DbType.DateTime);
                parameters.Add("@dateModified", notificationSubscriber.DateModified, dbType: DbType.DateTime);

                await cn.ExecuteAsync(storedProcedure, parameters, commandType: CommandType.StoredProcedure);
            }

            return notificationSubscriber;
        }

        public async Task UpdateNotificationSubscriber(NotificationSubscriber notificationSubscriber)
        {
            using (var cn = await _dbConnectionFactory.CreateConnection())
            {
                const string storedProcedure = "dbo.PushNotifications_UpdateNotificationSubscriber";
                notificationSubscriber.DateModified = DateTime.Now;

                var parameters = new DynamicParameters();
                parameters.Add("@deviceId", notificationSubscriber.DeviceId, dbType: DbType.String);
                parameters.Add("@notificationToken", notificationSubscriber.NotificationToken, dbType: DbType.String);
                parameters.Add("@dateModified", notificationSubscriber.DateModified, dbType: DbType.DateTime);
                await cn.ExecuteAsync(storedProcedure, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task DeleteNotificationSubscriber(string deviceId)
        {
            using (var cn = await _dbConnectionFactory.CreateConnection())
            {
                const string storedProcedure = "dbo.PushNotifications_DeleteNotificationSubscriber";
                await
                    cn.ExecuteAsync(storedProcedure, new { DeviceId = deviceId }, commandType: CommandType.StoredProcedure);
            }
        }


    }
}