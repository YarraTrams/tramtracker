﻿
using System.Data;
using System.Threading.Tasks;

namespace TramTrackerWebAPI.Dapper
{
    public interface IDbConnectionFactory
    {
        Task<IDbConnection> CreateConnection();
    }
}

