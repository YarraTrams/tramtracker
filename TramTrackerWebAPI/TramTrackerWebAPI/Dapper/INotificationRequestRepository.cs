﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Dapper
{
    public interface INotificationRequestRepository
    {
        //Task<RouteNotification> GetRouteNotificationById(int requestId);
        Task<IEnumerable<RouteNotification>> GetNotificationsRequestByDeviceId(string deviceId);
        Task<RouteNotification> CreateNotificationRequest(RouteNotification routeNotification);
        //Task DeleteRouteNotification(int requestId);
        Task DeleteAllNotificationRequests(string deviceId);
    }
}