﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Dapper
{
    public class FeatureStatusRepository : IFeatureStatusRepository
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;

        public FeatureStatusRepository(IDbConnectionFactory dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }

        public async Task<IEnumerable<FeatureStatus>> GetAllFeatureStatus()
        {
            using (var cn = await _dbConnectionFactory.CreateConnection())
            {
                const string storedProcedure = "dbo.TTWebAPI_GetAllFeatureStatus";
                return
                    await cn.QueryAsync<FeatureStatus>(storedProcedure, null, commandType: CommandType.StoredProcedure);
            }
        }
    }
}