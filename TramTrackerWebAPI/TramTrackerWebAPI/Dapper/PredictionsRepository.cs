﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Dapper
{
    public class PredictionsRepository : IPredictionsRepository
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;

        public PredictionsRepository(IDbConnectionFactory dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }

        public async Task<IEnumerable<RawPrediction>> GetPredictionsForStop(int trackerId, int routeNo, bool isLowFloorOnly)
        {
            using (var cn = await _dbConnectionFactory.CreateConnection())
            {
                const string storedProcedure = "dbo.GetPredictionsForStop";

                var parameters = new DynamicParameters();
                parameters.Add("@stopNo", value: trackerId, dbType: DbType.Int32);
                parameters.Add("@routeNo", value: routeNo, dbType:DbType.Int32);
                parameters.Add("@lowFloor", value: isLowFloorOnly, dbType: DbType.Boolean);

                return
                    await
                        cn.QueryAsync<RawPrediction>(storedProcedure, parameters,
                            commandType: CommandType.StoredProcedure);
            }
        }
    }
}