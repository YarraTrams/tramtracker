﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Dapper
{
    public class NotificationTimeRepository : INotificationTimeRepository
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;

        public NotificationTimeRepository(IDbConnectionFactory dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }



        public async Task CreateNotificationTime(NotificationTimeFrame notificationTimeFrame)
        {
            using (var cn = await _dbConnectionFactory.CreateConnection())
            {
                const string storedProcedure = "dbo.PushNotifications_CreateNotificationTime";

                var parameters = new DynamicParameters();
                parameters.Add("@deviceId", notificationTimeFrame.NotificationDevice, dbType: DbType.String);
                parameters.Add("@travelTimeId", notificationTimeFrame.TravelTimeId, dbType: DbType.Int32);

                await cn.ExecuteAsync(storedProcedure, parameters, commandType: CommandType.StoredProcedure);
            }
        }



        public async Task DeleteAllNotificationTimes(string deviceId)
        {
            using (var cn = await _dbConnectionFactory.CreateConnection())
            {
                const string storedProcedure = "dbo.PushNotifications_DeleteAllNotificationTimes";
                await cn.ExecuteAsync(storedProcedure, new { DeviceId = deviceId }, commandType: CommandType.StoredProcedure);
            }
        }

        //public async Task<IEnumerable<NotificationTimeFrame>> GetNotificationTimes(string deviceId)
        //{
        //    using (var cn = await _dbConnectionFactory.CreateConnection())
        //    {
        //        const string storedProcedure = "dbo.PushNotifications_GetAllNotificationTimes";
        //        return
        //            await
        //                cn.QueryAsync<NotificationTimeFrame>(storedProcedure, new { DeviceId = deviceId },
        //                    commandType: CommandType.StoredProcedure);
        //    }
        //}

        //public async Task<NotificationTimeFrame> GetNotificationTime(NotificationTimeFrame notificationTimeFrame)
        //{
        //    using (var cn = await _dbConnectionFactory.CreateConnection())
        //    {
        //        const string storedProcedure = "dbo.PushNotifications_GetNotificationTime";
        //        var result =
        //            await
        //                cn.QueryAsync<NotificationTimeFrame>(storedProcedure, notificationTimeFrame,
        //                    commandType: CommandType.StoredProcedure);
        //        return result.SingleOrDefault();

        //    }
        //}

        //public async Task DeleteNotificationTime(NotificationTimeFrame notificationTimeFrame)
        //{
        //    using (var cn = await _dbConnectionFactory.CreateConnection())
        //    {
        //        const string storedProcedure = "dbo.PushNotifications_DeleteNotificationTime";
        //        await cn.ExecuteAsync(storedProcedure, notificationTimeFrame, commandType: CommandType.StoredProcedure);
        //    }
        //}
    }
}