﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TramTrackerWebAPI.Enums;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Dapper
{
    public interface IInfoPagesRepository
    {
        Task<IEnumerable<InfoPage>> GetPublishedInfoPages();
    }
}