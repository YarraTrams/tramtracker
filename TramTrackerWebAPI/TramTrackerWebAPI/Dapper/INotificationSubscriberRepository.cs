﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Dapper
{
    public interface INotificationSubscriberRepository
    {
        Task<NotificationSubscriber> GetNotificationSubscriberByDeviceId(string deviceId);
        //Task<NotificationSubscriber> GetNotificationSubscriberByToken(string notificationToken);
        Task<IList<NotificationSubscriber>> GetNotificationSubscriberByDeviceIdOrToken(string deviceId, string notificationToken);
        Task<NotificationSubscriber> CreateNotificationSubsciber(NotificationSubscriber notificationSubscriber);
        Task UpdateNotificationSubscriber(NotificationSubscriber notificationSubscriber);
        Task DeleteNotificationSubscriber(string deviceId);
    }
}