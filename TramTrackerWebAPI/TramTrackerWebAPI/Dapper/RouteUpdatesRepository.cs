﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Dapper
{
    public class RouteUpdatesRepository : IRouteUpdatesRepository
    {

        private readonly IDbConnectionFactory _dbConnectionFactory;
        public RouteUpdatesRepository(IDbConnectionFactory dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }

        public async Task<IEnumerable<Stop>> GetUpdatedStopsCollection(DateTime? since = null)
        {
            using (var cn = await _dbConnectionFactory.CreateConnection())
            {
                const string storedProcedure = "dbo.GetUpdatedStopsCollection";
                return await cn.QueryAsync<Stop>(storedProcedure, new { Since = since }, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Route>> GetUpdatedRoutesCollection(DateTime? since = null)
        {
            using (var cn = await _dbConnectionFactory.CreateConnection())
            {
                const string storedProcedure = "dbo.GetUpdatedRoutesCollection";
                var routes = new List<Route>();
                Route current = null;
                await cn.QueryAsync<Route, RouteStop, Route>(storedProcedure, (route, stop) =>
                {
                    if (current == null || current.RouteNo != route.RouteNo)
                    {
                        routes.Add(route);
                        current = route;
                    }

                    if (!stop.Removed)
                    {
                        if (stop.UpStop)
                        {
                            current.UpStops.Add(stop);
                        }
                        else
                        {
                            current.DownStops.Add(stop);
                        }
                    }

                    return current;
                }, new { Since = since }, splitOn: "StopNo", commandType: CommandType.StoredProcedure);

                return routes;
            }
        }

        public async Task<IEnumerable<TicketOutlet>> GetUpdatedTicketOutletsCollection(DateTime? since = null)
        {
            using (var cn = await _dbConnectionFactory.CreateConnection())
            {
                const string storedProcedure = "dbo.GetUpdatedTicketOutletsCollection";
                return await cn.QueryAsync<TicketOutlet>(storedProcedure, new { Since = since }, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<PointOfInterest>> GetUpdatedPointsOfInterestCollection(DateTime? since = null)
        {
            using (var cn = await _dbConnectionFactory.CreateConnection())
            {
                const string storedProcedure = "dbo.GetUpdatedPointsOfInterestCollection";
                var pointsOfInterest = new List<PointOfInterest>();
                PointOfInterest current = null;
                await cn.QueryAsync<PointOfInterest, PoiStop, PointOfInterest>(storedProcedure,
                    (poi, poiStop) =>
                    {
                        if (current == null || current.POIId != poi.POIId)
                        {
                            pointsOfInterest.Add(poi);
                            current = poi;

                        }

                        if (poiStop != null)
                        {
                            current.Stops.Add(poiStop);
                        }
                        return current;
                    }, new { Since = since }, splitOn: "StopNo", commandType: CommandType.StoredProcedure);
                return pointsOfInterest;
            }
        }

        public async Task<IEnumerable<NetworkMap>> GetUpdatedNetworkMapsCollection(DateTime? since = null)
        {
            using (var cn = await _dbConnectionFactory.CreateConnection())
            {
                const string storedProcedure = "dbo.GetUpdatedNetworkMapsCollection";
                return await cn.QueryAsync<NetworkMap>(storedProcedure, new { Since = since }, commandType: CommandType.StoredProcedure);
            }
        }
    }
}