﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using TramTrackerWebAPI.Enums;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Dapper
{
    public class InfoPagesRepository : IInfoPagesRepository
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;

        public InfoPagesRepository(IDbConnectionFactory dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }

        public async Task<IEnumerable<InfoPage>> GetPublishedInfoPages()
        {
            using (var cn = await _dbConnectionFactory.CreateConnection())
            {
                const string storedProcedure = "dbo.TTWebAPI_GetPublishedInfoPages";
                return
                    await
                        cn.QueryAsync<InfoPage>(storedProcedure, null,
                            commandType: CommandType.StoredProcedure);
            }
        }
    }
}