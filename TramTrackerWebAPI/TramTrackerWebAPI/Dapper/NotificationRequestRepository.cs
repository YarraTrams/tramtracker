﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Dapper
{
    public class NotificationRequestRepository : INotificationRequestRepository
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;

        public NotificationRequestRepository(IDbConnectionFactory dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }

        public async Task<IEnumerable<RouteNotification>> GetNotificationsRequestByDeviceId(string deviceId)
        {
            using (var cn = await _dbConnectionFactory.CreateConnection())
            {
                const string storedProcedure = "dbo.PushNotifications_GetRouteNotificationsByDeviceId";
                return await cn.QueryAsync<RouteNotification>(storedProcedure, new { DeviceId = deviceId },
                    commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<RouteNotification> CreateNotificationRequest(RouteNotification routeNotification)
        {
            using (var cn = await _dbConnectionFactory.CreateConnection())
            {
                const string storedProcedure = "dbo.PushNotifications_CreateNotificationRequest";

                var parameters = new DynamicParameters();
                parameters.Add("@deviceId", routeNotification.DeviceId, dbType: DbType.String);
                parameters.Add("@routeNo", routeNotification.RouteNo, dbType: DbType.Int32);
                parameters.Add("@dateCreated", routeNotification.DateCreated, dbType: DbType.DateTime);
                parameters.Add("@requestId", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

                await cn.ExecuteAsync(storedProcedure, parameters, commandType: CommandType.StoredProcedure);

                routeNotification.Id = parameters.Get<int>("@requestId");

                return routeNotification;
            }
        }


        public async Task DeleteAllNotificationRequests(string deviceId)
        {
            using (var cn = await _dbConnectionFactory.CreateConnection())
            {
                const string storedProcedure = "dbo.PushNotifications_DeleteAllNotificationRequests";
                await cn.ExecuteAsync(storedProcedure, new { DeviceId = deviceId }, commandType: CommandType.StoredProcedure);
            }
        }
    }
}