﻿using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using TramTrackerWebAPI.Models;

namespace TramTrackerWebAPI.Dapper
{
    public class ClientTypeRepository : IClientTypeRepository
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;
        
        public ClientTypeRepository(IDbConnectionFactory dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }

        public async Task<Client> GetClientTypeById(string clientTypeId)
        {
            Client clientType = null;
            using (var cn = await _dbConnectionFactory.CreateConnection())
            {
                const string storedProcedure = "dbo.CheckAuthToken";
                var result = await cn.QueryAsync<Client>(storedProcedure, new {authToken = clientTypeId}, commandType: CommandType.StoredProcedure);
                clientType = result.SingleOrDefault();
            }
            return clientType;
        }
    }
}