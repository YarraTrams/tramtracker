﻿namespace TramTrackerWebAPI.Enums
{
    public enum DeviceType
    {
        Invalid = 0,
        Both,
        iOS,
        Android
    }
}