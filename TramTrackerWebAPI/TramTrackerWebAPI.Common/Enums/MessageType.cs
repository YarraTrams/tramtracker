﻿namespace TramTrackerWebAPI.Common.Enums
{
    public enum MessageType
    {
        Invalid = 0,
        Disruption = 1,
        ServiceUpdate = 2
    }
}