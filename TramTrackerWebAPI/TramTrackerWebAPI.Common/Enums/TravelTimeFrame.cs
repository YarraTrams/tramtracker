﻿namespace TramTrackerWebAPI.Common.Enums
{
    public enum TravelTimeFrame
    {
        Invalid = 0,
        WeekdayMorningPeak,
        WeekdayAfternoonPeak,
        WeekdayAllDay,
        Weekend
    }
}