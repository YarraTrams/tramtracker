﻿using System;
using System.ServiceProcess;
using log4net.Config;
using Ninject;
using Ninject.Extensions.Logging;
using Ninject.Extensions.Logging.Log4net;
using TramTrackerWebAPI.PushServer.DI;
using TramTrackerWebAPI.PushServer.Interfaces;

namespace TramTrackerWebAPI.TTPushNotificationService
{
    static class Program
    {


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 
        static void Main(string[] args)
        {
            XmlConfigurator.Configure();
            var kernel = CreateKernel();
            
            var notificationMessagesService = kernel.Get<INotificationMessagesService>();
            var notificationUsersService = kernel.Get<INotificationSubscribersService>();

            var loggerFactory = kernel.Get<ILoggerFactory>();
            var logger = loggerFactory.GetCurrentClassLogger();
          
            if (Environment.UserInteractive)
            {
                logger.Debug("Starting in UserInteractive mode");
                TtPushNotificationService pushNotificationService
                    = new TtPushNotificationService(logger, notificationMessagesService, notificationUsersService);
                pushNotificationService.TestStartupAndStop(args);
            }
            else
            {
                logger.Debug("Starting in Windows Service mode");
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
            { 
                new TtPushNotificationService(logger, notificationMessagesService, notificationUsersService) 
            };
                ServiceBase.Run(ServicesToRun);
            }
        }

        private static IKernel CreateKernel()
        {
            var settings = CreateSettings();
            return new StandardKernel(settings, new Log4NetModule(), new PushNotificationsModule());
        }

        private static NinjectSettings CreateSettings()
        {
            var settings = new NinjectSettings();
            settings.LoadExtensions = false;
            return settings;
        }
    }
}
