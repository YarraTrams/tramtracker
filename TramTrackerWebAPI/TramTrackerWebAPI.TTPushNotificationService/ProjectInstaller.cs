﻿using System.ComponentModel;
using System.Configuration.Install;

namespace TramTrackerWebAPI.TTPushNotificationService
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }
    }
}
