using System.Configuration;
namespace YarraTrams.PIDSDAL
{
    partial class tramTRACKERDWDataContext
    {
        public tramTRACKERDWDataContext()
            : base(ConfigurationManager.ConnectionStrings["TramTrackerDWConnectionString"].ToString())
        {
            OnCreated();
        }
    }
}
