﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class CheckRouteInfo
    {
        public CheckRouteInfo() { }

        [DataMember()]
        public string RouteNo { get; set; }

    }
}
