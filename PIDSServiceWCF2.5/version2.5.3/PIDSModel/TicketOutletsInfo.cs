﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class TicketOutletsInfo
    {
        [DataMember()]
        public Int32 TicketOutletId { get; set; }

        [DataMember()]
        public string Name { get; set; }

        [DataMember()]
        public string Address { get; set; }

        [DataMember()]
        public string Suburb { get; set; }

        [DataMember()]
        public bool HasMyki { get; set; }

        [DataMember()]
        public bool HasMetcard { get; set; }

        [DataMember()]
        public bool Is24Hour { get; set; }

        [DataMember()]
        public bool HasMykiTopUp { get; set; }

        [DataMember()]
        public decimal Latitude { get; set; }

        [DataMember()]
        public decimal Longitude { get; set; }

        [DataMember()]
        public DateTime? DateAdded { get; set; }

        [DataMember()]
        public DateTime? DateModified { get; set; }
    }
}