﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class NextPredictedStopsDetailsTableInfo
    {
        [DataMember()]
        public string StopSeq { get; set; }

        [DataMember()]
        public short StopNo { get; set; }

        [DataMember()]
        public DateTime PredictedArrivalDateTime { get; set; }
    }
}