﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YarraTrams.Model
{
    public enum ClientType
    {
        SMS = 0,
        Web = 1,
        SmartPhone = 2,
        Tram=3
        
    }


    public enum DisplayType
    {
        Text = 1,
        TabularColumn = 2

    }

}
