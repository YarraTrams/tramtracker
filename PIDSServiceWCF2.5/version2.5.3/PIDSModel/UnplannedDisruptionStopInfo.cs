﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class UnplannedDisruptionStopInfo
    {
        [DataMember()]
        public string ShortMessage { get; set; }

        [DataMember()]
        public string LongMessage { get; set; }

        [DataMember()]
        public bool IsAffected { get; set; }

    }
}
