﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class DisruptedStop
    {
        [DataMember()]
        public short StopNo { get; set; }

    }
}
