﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class RouteStopsByRouteInfo
    {


        [DataMember()]
        public short StopNo { get; set; }

        [DataMember()]
        public string Description { get; set; }

        [DataMember()]
        public decimal Latitude { get; set; }

        [DataMember()]
        public decimal Longitude { get; set; }

        [DataMember()]
        public bool UpStop { get; set; }

        [DataMember()]
        public bool IsCityStop { get; set; }

        [DataMember()]
        public bool IsPlatformStop { get; set; }

        [DataMember()]
        public bool HasConnectingBuses { get; set; }

        [DataMember()]
        public bool HasConnectingTrains { get; set; }

        [DataMember()]
        public bool HasConnectingTrams { get; set; }

        [DataMember()]
        public byte StopSequence { get; set; }

        [DataMember()]
        public string SuburbName { get; set; }

        [DataMember()]
        public short StopLength { get; set; }

        [DataMember()]
        public string StopName { get; set; }

        [DataMember()]
        public string TurnType { get; set; }

        [DataMember()]
        public string TurnMessage { get; set; }
    }
}