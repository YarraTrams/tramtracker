﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class ListOfStopsByRouteNoAndDirectionInfo
    {
        public ListOfStopsByRouteNoAndDirectionInfo() { }

        [DataMember()]
        public int StopNo { get; set; }
        [DataMember()]
        public string Name { get; set; }
        [DataMember()]
        public string Description { get; set; }
        [DataMember()]
        public decimal Latitude { get; set; }
        [DataMember()]
        public decimal Longitude { get; set; }
        [DataMember()]
        public string SuburbName { get; set; }
        [DataMember()]
        public string TurnType { get; set; }
        [DataMember()]
        public string TurnMessage { get; set; }
    }
}