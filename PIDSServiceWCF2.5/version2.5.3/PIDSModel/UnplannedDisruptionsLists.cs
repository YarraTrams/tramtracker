﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class UnplannedDisruptionsLists
    {
        [DataMember()]
        public string RouteNo { get; set; }

        [DataMember()]
        public List<DisruptedStop> AffectedStops { get; set; }

        [DataMember()]
        public List<DisruptedStop> UnAffectedStops { get; set; }

        [DataMember()]
        public string AffectedShortMessage { get; set; }

        [DataMember()]
        public string AffectedLongMessage { get; set; }

        [DataMember()]
        public string UnAffectedShortMessage { get; set; }

        [DataMember()]
        public string UnAffectedLongMessage { get; set; }
    }
}
