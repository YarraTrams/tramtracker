﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class DestinationsForAllRoutesInfo
    {
        public DestinationsForAllRoutesInfo() { }

        [DataMember()]
        public string RouteNumber { get; set; }
        [DataMember()]
        public string Name { get; set; }
        [DataMember()]
        public bool IsUpStop { get; set; }


    }

    /// <summary>
    /// Drive error exception class. Thrown when a drive selection error has occured.
    /// </summary>
    [Serializable]
    public class DriveException : ApplicationException
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public DriveException()
        {
        }



        [DataMember()]
        public bool hasError { get; set; }
        [DataMember()]
        public string errorMessage { get; set; }
        [DataMember()]
        public string webMethodCalled { get; set; }
        public DateTime timeRequested { get; set; }
        [DataMember()]
        public DateTime timeResponded { get; set; }

        /// <summary>
        /// Constructor used with a message.
        /// </summary>
        /// <param name="message">String message of exception.</param>
        public DriveException(string message)
            : base(message)
        {
        }


        public DriveException(bool hasError,string message)
            : base(message)
        {
            this.hasError = hasError;
            this.errorMessage = message;
        }


        /// <summary>
        /// Constructor used with a message and an inner exception.
        /// </summary>
        /// <param name="message">String message of exception.</param>
        /// <param name="inner">Reference to inner exception.</param>
        public DriveException(string message, Exception inner)
            : base(message, inner)
        {
        }
        /// <summary>
        /// Constructor used in serializing the data.
        /// </summary>
        /// <param name="info">Data stored to serialize/de-serialize</param>
        /// <param name="context">Defines the source/destinantion of the straeam.</param>
        public DriveException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }


}
