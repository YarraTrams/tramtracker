﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class TicketOutletSinceInfo
    {
        public TicketOutletSinceInfo() { }

        [DataMember()]
        public int ID { get; set; }

        [DataMember()]
        public string Action { get; set; }

    }
}
