﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class ServerTimeInfo
    {
        public ServerTimeInfo() { }

        [DataMember()]
        public DateTime ServerTime { get; set; }
    }
}