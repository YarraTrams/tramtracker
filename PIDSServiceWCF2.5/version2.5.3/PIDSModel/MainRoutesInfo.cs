﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class MainRoutesInfo
    {
        public MainRoutesInfo() { }

        [DataMember()]
        public string RouteNo { get; set; }
        
    }
}