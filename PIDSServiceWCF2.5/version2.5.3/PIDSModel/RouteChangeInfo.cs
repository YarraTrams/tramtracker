﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class RouteChangeInfo
    {
        public RouteChangeInfo() { }

        [DataMember()]
        public short ID { get; set; }

        [DataMember()]
        public string HeadboardRouteNo { get; set; }

        [DataMember()]
        public string RouteNo { get; set; }

        [DataMember()]
        public bool IsMainRoute { get; set; }

        [DataMember()]
        public string Action { get; set; }

        [DataMember()]
        public string Colour { get; set; }
    }
}