﻿using System;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.Routing;
using System.Web.Configuration;
using YarraTrams.Library.Mailer;

namespace YarraTrams.PIDSRestService
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes();
        }

        private void RegisterRoutes()
        {
            RouteTable.Routes.Add(new ServiceRoute("RestService", new WebServiceHostFactory(), typeof(RestService)));
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            string mailserver = WebConfigurationManager.AppSettings["MailServer"].ToString();
            string receiver_config = WebConfigurationManager.AppSettings["ExceptionRecipients"].ToString();
            string[] receivers = receiver_config.Split(';');
            ExceptionMailer mailer = new YarraTrams.Library.Mailer.ExceptionMailer();
            string sourceDetails = "WCF Web Service";
            ExceptionMailer.EmailException(sourceDetails, mailserver, receivers, ex);
        }
    }
}
