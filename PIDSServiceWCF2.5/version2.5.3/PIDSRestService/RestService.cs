﻿using System;
using System.EnterpriseServices.Internal;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.ServiceModel;
using System.ComponentModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.ServiceModel.Description;
using System.Web.Script.Serialization;
using System.Text;
using YarraTrams.Model;
using YarraTrams.PIDSRestService.pidsservicecore;
using YarraTrams.PIDSDAL;

namespace YarraTrams.PIDSRestService
{

    [ServiceContract]
    [ServiceKnownType("GetKnownTypes", typeof(ServiceKnownTypesDiscovery))]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall,IncludeExceptionDetailInFaults=true)]
    public class RestService
    {

        [WebInvoke(Method = "GET", UriTemplate = "GetDeviceToken/?aid={aid}&devInfo={devInfo}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        [Description("Returns a device token - /GetDeviceToken/?aid={aid}&devInfo={devInfo}")]
        public ServiceResponse GetDeviceToken(string aid, string devInfo)
        {
            ServiceResponse resp = ServiceHelper.GetDeviceToken(aid, devInfo);
            return resp;
        }

        [WebInvoke(Method = "POST", UriTemplate = "AddDeviceUsage/?aid={aid}&tkn={tkn}", ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public ServiceResponse AddDeviceUsage(Stream dataStream, string aid, string tkn)
        {
            ServiceResponse resp = ServiceHelper.AddFeatureUsage(dataStream, aid, tkn);
            return resp;
        }

        [WebInvoke(Method = "GET", UriTemplate = "GetLatestNetworkMaps/?aid={aid}&tkn={tkn}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        [Description("Returns the either one of two of the latest network maps - /GetLatestNetworkMaps/?aid={aid}&tkn={tkn}")]
        public ServiceResponse GetLatestNetworkMaps(string aid, string tkn)
        {
            ServiceResponse resp = ServiceHelper.GetLatestNetworkMaps(aid, tkn);
            return resp;

        }


        [WebInvoke(Method = "GET", UriTemplate = "GetDestinationsForAllRoutes/?aid={aid}&tkn={tkn}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        [Description("Returns destinations for All Routes - /GetDestinationsForAllRoutes/?aid={aid}&tkn={tkn}")]
        public ServiceResponse GetDestinationsForAllRoutes(string aid, string tkn)
        {
            ServiceResponse resp = ServiceHelper.GetDestinationsForAllRoutes(aid, tkn);
            return resp;

        }

        [WebInvoke(Method = "GET", UriTemplate = "GetDestinationsForRoute/{routeId}/?aid={aid}&tkn={tkn}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        [FaultContract(typeof(FaultException))]
        [Description("Returns destinations for a specific route - /GetDestinationsForRoute/{routeId}/?aid={aid}&tkn={tkn}")]
        public ServiceResponse GetDestinationsForRoute(string aid, string routeId, string tkn)
        {
            ServiceResponse resp = ServiceHelper.GetDestinationsForRoute(aid, routeId, tkn);
            return resp;

        }


        [WebInvoke(Method = "GET", UriTemplate = "GetListOfStopsByRouteNoAndDirection/{routeNo}/{isUpDirection}/?aid={aid}&tkn={tkn}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        [Description("Returns list of stops for specified Route and direction - /GetListOfStopsByRouteNoAndDirection/{routeNo}/{isUpDirection}/?aid={aid}&tkn={tkn}")]
        public ServiceResponse GetListOfStopsByRouteNoAndDirection(string aid, string routeNo, string isUpDirection, string tkn)
        {
            ServiceResponse resp = ServiceHelper.GetListOfStopsByRouteNoAndDirection(aid, routeNo, isUpDirection, tkn);
            return resp;
        }

        [WebInvoke(Method = "GET", UriTemplate = "GetMainRoutesForStop/{stopNo}/?aid={aid}&tkn={tkn}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        [Description("Returns route numbers for all routes passing a specified stop - /GetMainRoutesForStop/{stopNo}/?aid={aid}&tkn={tkn}")]
        public ServiceResponse GetMainRoutesForStop(string aid, string stopNo, string tkn)
        {
            ServiceResponse resp = ServiceHelper.GetMainRoutesForStop(aid, stopNo, tkn);
            return resp;
        }


        [WebInvoke(Method = "GET", UriTemplate = "GetNextPredictedArrivalTimeAtStopsForTramNo/{tramNo}/?aid={aid}&tkn={tkn}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        [Description("Returns predictions for all remaining stops of a specified tram - /GetNextPredictedArrivalTimeAtStopsForTramNo/{tramNo}/?aid={aid}&tkn={tkn}")]
        public ServiceResponse GetNextPredictedArrivalTimeAtStopsForTramNo(string aid, string tramNo, string tkn)
        {
            ServiceResponse resp = ServiceHelper.GetNextPredictedArrivalTimeAtStopsForTramNo(aid, tramNo, tkn);
            return resp;
        }


        [WebGet(UriTemplate = "GetNextPredictedRoutesCollection/{stopNo}/{routeNo}/{lowFloor}/?aid={aid}&cid={cid}&tkn={tkn}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        [Description("Returns predictions for a stop and route - /GetNextPredictedRoutesCollection/{stopNo}/{routeNo}/{lowFloor}/?aid={aid}&cid={cid}&tkn={tkn}")]
        public ServiceResponse GetNextPredictedRoutesCollection(string aid, string cid, string stopNo, string routeNo, string lowFloor, string tkn)
        {
            ServiceResponse resp = ServiceHelper.GetNextPredictedRoutesCollection(aid, cid, stopNo, routeNo, lowFloor, tkn);
            return resp;
        }

        [WebInvoke(Method = "GET", UriTemplate = "GetSchedulesCollection/{stopNo}/{routeNo}/{lowFloor}/{clientRequestDateTime}/?aid={aid}&tkn={tkn}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        [Description("Returns schedules for a specified route, stop and datetime - /GetSchedulesCollection/{stopNo}/{routeNo}/{lowFloor}/{clientRequestDateTime}/?aid={aid}&tkn={tkn}")]
        public ServiceResponse GetSchedulesCollection(string aid, string stopNo, string routeNo, string lowFloor, string clientRequestDateTime, string tkn)
        {
            ServiceResponse resp = ServiceHelper.GetSchedulesCollection(aid, stopNo, routeNo, lowFloor, clientRequestDateTime, tkn);
            return resp;
        }

        [WebInvoke(Method = "GET", UriTemplate = "GetRouteStopsByRoute/{routeNo}/?aid={aid}&tkn={tkn}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        [Description("Returns stops for a specified route - /GetRouteStopsByRoute/{routeNo}/?aid={aid}&tkn={tkn}")]
        public ServiceResponse GetRouteStopsByRoute(string aid, string routeNo, string tkn)
        {
            ServiceResponse resp = ServiceHelper.GetRouteStopsByRoute(aid, routeNo, tkn);
            return resp;
        }

        [WebInvoke(Method = "GET", UriTemplate = "GetPlatformStopsByRouteAndDirection/{routeNo}/{isUpDirection}/?aid={aid}&tkn={tkn}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        [Description("Returns stops for a specified route - /GetPlatformStopsByRouteAndDirection/{routeNo}/{isUpDirection}/?aid={aid}&tkn={tkn}")]
        public ServiceResponse GetPlatformStopsByRouteAndDirection(string aid, string routeNo, string isUpDirection, string tkn)
        {
            ServiceResponse resp = ServiceHelper.GetPlatformStopsByRouteAndDirection(aid, routeNo, isUpDirection, tkn);
            return resp;
        }


        [WebInvoke(Method = "GET", UriTemplate = "GetRouteSummaries/?aid={aid}&tkn={tkn}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        [Description("Returns route summaries - /GetRouteSummaries/?aid={aid}&tkn={tkn}")]
        public ServiceResponse GetRouteSummaries(string aid, string tkn)
        {
            ServiceResponse resp = ServiceHelper.GetRouteSummaries(aid, tkn);
            return resp;
        }

        [WebInvoke(Method = "GET", UriTemplate = "GetSchedulesForTrip/{tripID}/{scheduleDateTime}/?aid={aid}&tkn={tkn}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        [Description("Returns schedules for a trip - /GetSchedulesForTrip/{tripID}/{scheduleDateTime}/?aid={aid}&tkn={tkn}")]
        public ServiceResponse GetSchedulesForTrip(string aid, string tripID, string scheduleDateTime, string tkn)
        {
            ServiceResponse resp = ServiceHelper.GetSchedulesForTrip(aid, tripID, scheduleDateTime, tkn);
            return resp;
        }

        [WebInvoke(Method = "GET", UriTemplate = "GetStopInformation/{stopNo}/?aid={aid}&tkn={tkn}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        [Description("Returns information for a stop - /GetStopInformation/{stopNo}/?aid={aid}&tkn={tkn}")]
        public ServiceResponse GetStopInformation(string aid, string stopNo, string tkn)
        {
            ServiceResponse resp = ServiceHelper.GetStopInformation(aid, stopNo, tkn);
            return resp;
        }

        [WebInvoke(Method = "GET", UriTemplate = "GetTicketOutlets/?aid={aid}&tkn={tkn}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        [Description("Returns ticket outlets - /GetTicketOutlets/?aid={aid}&tkn={tkn}")]
        public ServiceResponse GetTicketOutlets(string aid, string tkn)
        {
            ServiceResponse resp = ServiceHelper.GetTicketOutlets(aid, tkn);
            return resp;
        }

        [WebInvoke(Method = "GET", UriTemplate = "GetTicketOutletById/{ticketOutletId}/?aid={aid}&tkn={tkn}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        [Description("Returns a ticket outlet based on id - /GetTicketOutletById/{ticketOutletId}/?aid={aid}&tkn={tkn}")]
        public ServiceResponse GetTicketOutletById(string ticketOutletId, string aid, string tkn)
        {
            ServiceResponse resp = ServiceHelper.GetTicketOutletById(Convert.ToInt32(ticketOutletId), aid, tkn);
            return resp;
        }

        [WebInvoke(Method = "GET", UriTemplate = "GetTicketOutletChangesSince/{lastUpdateDate}/?aid={aid}&tkn={tkn}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        [Description("Returns ticket outlet updates since a given date - /GetTicketOutletChangesSince/{lastUpdateDate}?/aid={aid}&tkn={tkn}")]
        public ServiceResponse GetTicketOutletChangesSince(string aid, string lastUpdateDate, string tkn)
        {
            ServiceResponse resp = ServiceHelper.GetTicketOutletChangesSince(aid, DateTime.Parse(lastUpdateDate), tkn);
            return resp;
        }

        [WebInvoke(Method = "GET", UriTemplate = "GetPointsOfInterest/?aid={aid}&tkn={tkn}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        [Description("Returns a list of all points of interest- /GetPointsOfInterest/?aid={aid}&tkn={tkn}")]
        public ServiceResponse GetPointsOfInterest(string aid, string tkn)
        {
            ServiceResponse resp = ServiceHelper.GetPointsOfInterest(aid, tkn);
            return resp;
        }

        [WebInvoke(Method = "GET", UriTemplate = "GetPointsOfInterestByStopNo/{stopNo}/?aid={aid}&tkn={tkn}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        [Description("Returns a list of points of interest based on stop number - /GetPointsOfInterestByStopNo/{stopNo}/?aid={aid}&tkn={tkn}")]
        public ServiceResponse GetPointsOfInterestByStopNo(string aid, string stopNo, string tkn)
        {
            ServiceResponse resp = ServiceHelper.GetPointsOfInterestByStopNo(aid, Convert.ToInt16(stopNo), tkn);
            return resp;
        }

        [WebInvoke(Method = "GET", UriTemplate = "GetPointOfInterestById/{poiId}/?aid={aid}&tkn={tkn}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        [Description("Returns a list of stops based on a point of interest- /GetPointOfInterestById/{poiId}/?aid={aid}&tkn={tkn}")]
        public ServiceResponse GetPointOfInterestById(string aid, string poiId, string tkn)
        {
            ServiceResponse resp = ServiceHelper.GetPointOfInterestById(aid, Convert.ToInt16(poiId), tkn);
            return resp;
        }


        [WebInvoke(Method = "GET", UriTemplate = "GetStopsByPointOfInterestId/{poiId}/?aid={aid}&tkn={tkn}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        [Description("Returns a list of stops based on a point of interest- /GetStopsByPointOfInterestId/{poiId}/?aid={aid}&tkn={tkn}")]
        public ServiceResponse GetStopsByPointOfInterestId(string aid, string poiId, string tkn)
        {
            ServiceResponse resp = ServiceHelper.GetStopsByPointOfInterestId(aid, Convert.ToInt16(poiId), tkn);
            return resp;
        }

        [WebInvoke(Method = "GET", UriTemplate = "GetPointsOfInterestChangesSince/{lastUpdateDate}/?aid={aid}&tkn={tkn}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        [Description("Returns points of interest updates since a given date - /GetPointsOfInterestChangesSince/{lastUpdateDate}?/aid={aid}&tkn={tkn}")]
        public ServiceResponse GetPointsOfInterestChangesSince(string aid, string lastUpdateDate, string tkn)
        {
            ServiceResponse resp = ServiceHelper.GetPointsOfInterestChangesSince(aid, DateTime.Parse(lastUpdateDate), tkn);
            return resp;
        }

        [WebInvoke(Method = "GET", UriTemplate = "GetStopsAndRoutesUpdatesSince/{lastUpdateDate}/?aid={aid}&tkn={tkn}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        [Description("Returns information for a stop - /GetStopsAndRoutesUpdatesSince/{lastUpdateDate}/?aid={aid}&tkn={tkn}")]
        public ServiceResponse GetStopsAndRoutesUpdatesSince(string aid, string lastUpdateDate, string tkn)
        {
            ServiceResponse resp = ServiceHelper.GetStopsAndRoutesUpdatesSince(aid, DateTime.Parse(lastUpdateDate), tkn);
            return resp;
        }


        [WebInvoke(Method = "GET", UriTemplate = "ValidateInput/{stopNo}/{routeNo}/{lowFloor}/?aid={aid}&tkn={tkn}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        [Description("Check if a input is valid - /ValidateInput/{stopNo}/{routeNo}/{lowFloor}/?aid={aid}&tkn={tkn}")]
        public ServiceResponse ValidateInput(string aid, string stopNo, string routeNo, string lowFloor, string tkn)
        {
            ServiceResponse resp = ServiceHelper.ValidateInput(aid, short.Parse(stopNo), routeNo, bool.Parse(lowFloor), tkn);
            return resp;
        }


        [WebGet(UriTemplate = "GetPredictionsCollection/{stopNo}/{routeNo}/{lowFloor}/?aid={aid}&cid={cid}&tkn={tkn}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        [Description("Returns predictions for a stop and route - /GetPredictionsCollection/{stopNo}/{routeNo}/{lowFloor}/?aid={aid}&cid={cid}&tkn={tkn}")]
        public ServiceResponse GetPredictionsCollection(string aid, string cid, string stopNo, string routeNo, string lowFloor, string tkn)
        {
            ServiceResponse resp = ServiceHelper.GetPredictionsCollection(aid, cid, stopNo, routeNo, lowFloor, tkn);
            return resp;
        }

        /*[WebGet(UriTemplate = "GetUnplannedDisruptionByRoute/{routeNo}/?aid={aid}&cid={cid}&tkn={tkn}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        [Description("Returns unplanned disruption for a route - /GetUnplannedDisruptionByRoute/{routeNo}/?aid={aid}&cid={cid}&tkn={tkn}")]
        public ServiceResponse GetUnplannedDisruptionByRoute(string aid, string cid, string routeNo, string tkn)
        {
            ServiceResponse resp = ServiceHelper.GetUnplannedDisruptionByRoute(aid, cid, routeNo, tkn);
            return resp;
        }*/
        [WebGet(UriTemplate = "GetUnplannedDisruptionByRoute/{routeNo}/?aid={aid}&cid={cid}&tkn={tkn}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        [Description("Returns unplanned disruption for a route - /GetUnplannedDisruptionByRoute/{routeNo}/?aid={aid}&cid={cid}&tkn={tkn}")]
        public ServiceResponse GetUnplannedDisruptionByRoute(string aid, string cid, string routeNo, string tkn)
        {
            ServiceResponse resp = ServiceHelper.GetUnplannedDisruptionByRoute(aid, cid, routeNo, tkn);
            return resp;
        }

        [WebGet(UriTemplate = "GetAllUnplannedDisruptionsForAllRoutes/?aid={aid}&cid={cid}&tkn={tkn}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        [Description("Returns all the unplanned disruptions - /GetUnplannedDisruptionByRoutes/?aid={aid}&cid={cid}&tkn={tkn}")]
        public ServiceResponse GetAllUnplannedDisruptionsForAllRoutes(string aid, string cid, string tkn)
        {
            ServiceResponse resp = ServiceHelper.GetAllUnplannedDisruptionsForAllRoutes(aid, cid, tkn);
            return resp;
        }

        [WebGet(UriTemplate = "GetUnplannedDisruptionByStop/{stopNo}/?aid={aid}&cid={cid}&tkn={tkn}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        [Description("Returns unplanned disruption for a stop - /GetUnplannedDisruptionByStop/{stopNo}/?aid={aid}&cid={cid}&tkn={tkn}")]
        public ServiceResponse GetUnplannedDisruptionByStop(string stopNo, string aid, string cid, string tkn)
        {
            ServiceResponse resp = ServiceHelper.GetUnplannedDisruptionByStop(aid, cid, stopNo, tkn);
            return resp;
        }
    }
}
