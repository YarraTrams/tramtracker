﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YarraTrams.Model;
using YarraTrams.PIDSDAL;
using System.Configuration;
using System.Data.SqlClient;

namespace YarraTrams.PIDSRestService
{
    public class Validator
    {
        public static ServiceResponse Validate(ServiceResponse response, params object[] p)
        {
            bool debugOn = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["DebugOn"]);
            Dictionary<string, object> validate = new Dictionary<string, object>();
            for (int i = 0; i < p.Length; )
            {
                validate.Add(p[i].ToString(), p[i + 1]);
                i = i + 2;
            }


            bool result;
            foreach (KeyValuePair<string, object> pair in validate)
            {
                switch (pair.Key)
                {
                    case "auth":
                        if (pair.Value == null)
                        {
                            response.hasError = true;
                            response.errorMessage = ConfigurationManager.AppSettings["NoAuthTokenError"];
                        }
                        else
                        {
                            if(!DalHelper.CheckAuthToken(pair.Value.ToString().Trim()))
                            {
                                response.hasError = true;
                                response.errorMessage = ConfigurationManager.AppSettings["InvalidAuthTokenError"];
                            }
                        }
                        break;
                    case "deviceInfo":
                        if (pair.Value == null)
                        {
                            response.hasError = true;
                            response.errorMessage = ConfigurationManager.AppSettings["NoDeviceInfoError"];
                        }
                        break;
                    case "deviceToken":
                        if (pair.Value == null)
                        {
                            response.hasError = true;
                            response.errorMessage = ConfigurationManager.AppSettings["NoDeviceTokenError"];
                        }
                        break;
                    case "clientType":
                        if (pair.Value == null)
                        {
                            response.hasError = true;
                            response.errorMessage = ConfigurationManager.AppSettings["NoClientTypeError"];
                        }
                        else
                        {
                            int cid = 0;
                            result = Int32.TryParse(pair.Value.ToString(), out cid);
                            if(!result || (cid < 1 || cid > 3))
                            {
                                response.hasError = true;
                                response.errorMessage = ConfigurationManager.AppSettings["NoClientTypeError"];
                            }
                        }
                        break;

                    case "routeId":
                        short number;
                        result = Int16.TryParse(pair.Value.ToString(), out number);
                        if (!result)
                        {
                            response.hasError = true;
                            response.errorMessage = ConfigurationManager.AppSettings["RouteIdFormatError"];
                        }
                        else
                        {
                            if (response.ValidateInputs)
                            {
                                try
                                {
                                    if (!DalHelper.CheckRoute(number))
                                    {
                                        response.hasError = true;
                                        response.errorMessage = ConfigurationManager.AppSettings["RouteNotFoundError"];
                                    }
                                }
                                catch (Exception e)
                                {
                                    response.hasError = true;
                                    response.errorMessage = ExceptionHelper.GetExceptionString(e, debugOn);
                                }
                            }
                        }
                        break;
                    case "stopNo":
                        short stopNo;
                        result = Int16.TryParse(pair.Value.ToString(), out stopNo);
                        if (!result)
                        {
                            response.hasError = true;
                            response.errorMessage = ConfigurationManager.AppSettings["StopNumberFormatError"];
                        }
                        else
                        {
                            if (response.ValidateInputs)
                            {
                                try
                                {
                                    if (!DalHelper.CheckStop(stopNo))
                                    {
                                        response.hasError = true;
                                        response.errorMessage = ConfigurationManager.AppSettings["StopNotFoundError"];
                                    }
                                }
                                catch (Exception e)
                                {
                                    response.hasError = true;
                                    response.errorMessage = ExceptionHelper.GetExceptionString(e, debugOn);
                                }
                            }
                        }
                        break;
                    case "tramNo":
                        short tramNo;
                        result = Int16.TryParse(pair.Value.ToString(), out tramNo);
                        if (!result)
                        {
                            response.hasError = true;
                            response.errorMessage = ConfigurationManager.AppSettings["TramNumberFormatError"];
                        }
                        break;
                    case "poiId":
                        int poiId;
                        result = Int32.TryParse(pair.Value.ToString(), out poiId);
                        if (!result)
                        {
                            response.hasError = true;
                            response.errorMessage = ConfigurationManager.AppSettings["PointOfInterestIdFormatError"];
                        }
                        break;
                    case "tripID":
                        int tripId;
                        result = int.TryParse(pair.Value.ToString(), out tripId);
                        if (!result)
                        {
                            response.hasError = true;
                            response.errorMessage = ConfigurationManager.AppSettings["TripIdFormatError"];
                        }
                        break;
                    case "ticketOutletId":
                        int ticketOutletId;
                        result = int.TryParse(pair.Value.ToString(), out ticketOutletId);
                        if (!result)
                        {
                            response.hasError = true;
                            response.errorMessage = ConfigurationManager.AppSettings["TicketOutletIdMessage"];
                        }
                        break;
                    case "isUpDirection":
                        bool isUp;
                        result = bool.TryParse(pair.Value.ToString(), out isUp);
                        if (!result)
                        {
                            response.hasError = true;
                            response.errorMessage = ConfigurationManager.AppSettings["UpDirectionFormatError"];
                        }
                        break;
                    case "lowFloor":
                        bool lowFloor;
                        result = bool.TryParse(pair.Value.ToString(), out lowFloor);
                        if (!result)
                        {
                            response.hasError = true;
                            response.errorMessage = ConfigurationManager.AppSettings["LowFloorFormatError"];
                        }
                        break;
                    case "dateTime":
                        DateTime validDate;
                        result = DateTime.TryParse(pair.Value.ToString(), out validDate);
                        if (!result)
                        {
                            response.hasError = true;
                            response.errorMessage = ConfigurationManager.AppSettings["DateTimeFormatError"];
                        }
                        break;
                }
            }
            response.timeResponded = DateTime.Now;
            return response;
        }
    }
}