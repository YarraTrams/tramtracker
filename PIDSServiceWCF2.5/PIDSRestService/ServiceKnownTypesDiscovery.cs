﻿using System;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.ServiceModel;
using System.ComponentModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.ServiceModel.Description;
using System.Web.Script.Serialization;
using System.Text;
using System.Reflection;
using System.Runtime.Serialization;



namespace YarraTrams.PIDSRestService
{
    public static class ServiceKnownTypesDiscovery
    {

        public static object CreateGeneric(Type generic, Type innerType, params object[] args)
        {
            System.Type specificType = generic.MakeGenericType(new System.Type[] { innerType });
            return Activator.CreateInstance(specificType, args);
        }

        public static IEnumerable<Type> GetKnownTypes(ICustomAttributeProvider provider)
        {
            var types = new List<Type>();
            var listTypes = new List<Type>();

            foreach (var asmFile in Directory.GetFiles(AppDomain.CurrentDomain.RelativeSearchPath ?? AppDomain.CurrentDomain.BaseDirectory, "YarraTrams.Model.dll"))
            {
                Assembly asm = Assembly.LoadFrom(asmFile);
                types.AddRange(asm.GetTypes().Where(p => Attribute.IsDefined(p, typeof(DataContractAttribute))).Distinct());
            }

            foreach (Type tp in types)
            {
                var item = CreateGeneric(typeof(List<>), tp);
                listTypes.Add(item.GetType());
            }
            types.AddRange(listTypes);

            return types;
        }
    }
}