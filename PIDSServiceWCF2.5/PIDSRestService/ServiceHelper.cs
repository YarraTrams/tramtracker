﻿using System;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.ServiceModel;
using System.ComponentModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.ServiceModel.Description;
using System.Web.Script.Serialization;
using System.Text;
using YarraTrams.Model;
using YarraTrams.PIDSRestService.pidsservicecore;
using System.Reflection;
using System.Runtime.Serialization;
using System.Configuration;
using System.Net;
using YarraTrams.PIDSDAL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using YarraTrams.Library.Mailer;


namespace YarraTrams.PIDSRestService
{
    public class ServiceHelper
    {
        private static bool debugOn = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["DebugOn"]);

        private static void CheckResponseFormat()
        {

            string xmlFormat = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["format"];
            if (!string.IsNullOrEmpty(xmlFormat))
            {
                if (xmlFormat == "xml")
                    WebOperationContext.Current.OutgoingResponse.Format = WebMessageFormat.Xml;
            }
        }

        private static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        private static UsageSession GetSessionFromJson(string jsonString)
        {
            var results = JsonConvert.DeserializeObject<dynamic>(jsonString);

            var session = new UsageSession();

            string allowsAnalytics = results.AllowsAnalytics;

            if (string.Equals(allowsAnalytics, "Yes", StringComparison.OrdinalIgnoreCase))
            {
                session.AllowsAnalytics = true;
            }

            session.TramTrackerVersion = results.tramTrackerVersion;
            session.OSVersion = results.iOSVersion;
            session.DeviceId = results.DeviceID;

            var launchesResult = results.Launches;

            if (launchesResult != null)
            {
                IList<AppLaunch> launches = new List<AppLaunch>();

                foreach (var launch in launchesResult)
                {
                    var al = new AppLaunch();

                    double launchStartTime = launch.TimestampStart;
                    double launchEndTime = launch.TimestampEnd;
                    al.startTime = UnixTimeStampToDateTime(launchStartTime);
                    al.endTime = UnixTimeStampToDateTime(launchEndTime);

                    var featuresResult = launch.Features;

                    IList<Feature> features = new List<Feature>();

                    foreach (var feature in featuresResult)
                    {
                        Feature f = new Feature();
                        f.Name = feature.Name;
                        double featureUsedTime = feature.Timestamp;
                        f.StartTimestamp = UnixTimeStampToDateTime(featureUsedTime);
                        features.Add(f);
                    }
                    features = features.OrderBy(f => f.StartTimestamp).ToList();
                    for (int i = 1; i < features.Count; i++)
                    {
                        features[i - 1].EndTimestamp = features[i].StartTimestamp;
                    }
                    features[features.Count - 1].EndTimestamp = al.endTime;
                    al.Features = features;
                    launches.Add(al);
                }

                session.AppLaunches = launches;
            }
            return session;
        }

        public static void LogRequest(string deviceToken, string authToken, string methodName, string tramNo, string stopNo, string routeNo,
                                                                        string isLowFloor, string requestStatus, string dateSince, string tripID)
        {
            OperationContext context = OperationContext.Current;
            MessageProperties msgProperties = context.IncomingMessageProperties;
            RemoteEndpointMessageProperty endpointProperty = msgProperties[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
            Assembly assemply = Assembly.GetExecutingAssembly();
            System.Diagnostics.FileVersionInfo wsVersion = System.Diagnostics.FileVersionInfo.GetVersionInfo(assemply.Location);

            PIDSRequest request = new PIDSRequest();
            request.ReqDateTime = DateTime.Now;
            request.IPAddr = endpointProperty.Address;
            request.GUID = deviceToken;
            request.Type = authToken;
            request.WSVersion = wsVersion.FileVersion;
            request.MethodName = methodName;
            request.StopNo = stopNo;
            request.RouteNo = routeNo;
            request.Version = "-";
            request.TramNo = tramNo;
            request.LowFloor = isLowFloor;
            request.Status = requestStatus;
            request.DateSince = dateSince;
            request.TripID = tripID;
            DalHelper.LogPIDSServiceRequest(request);
        }

        public static ServiceResponse AddFeatureUsage(Stream dataStream, string authToken, string deviceToken)
        {
            ServiceResponse response = new ServiceResponse("AddFeatureUsage", true);
            CheckResponseFormat();
            string jsonString = "";
            //put into try-catch block
            //NonClosingStreamReader reader = new NonClosingStreamReader(dataStream);
            using (var reader = new StreamReader(dataStream))
            {
                jsonString = reader.ReadToEnd();
                //reader.Close();
                //reader.Dispose();
            }
            Validator.Validate(response, "auth", authToken, "deviceToken", deviceToken);
            UsageSession session = GetSessionFromJson(jsonString);

            try
            {
                int analyticsDeviceID = -1;
                if (!string.IsNullOrEmpty(session.DeviceId))
                {
                    analyticsDeviceID = DalHelper.SetAnalyticsDevice(session.DeviceId, session.AllowsAnalytics, session.TramTrackerVersion, session.OSVersion);
                    session.AnalyticsDeviceID = analyticsDeviceID;
                }
                if (analyticsDeviceID > 0)
                {
                    if (DalHelper.CheckAllowsAnalytics(analyticsDeviceID))
                    {
                        DataTable dtAppLaunches = session.CreateAppLaunchTable();

                        DalHelper.AddFeatureUsage(dtAppLaunches);

                        //foreach (AppLaunch launch in session.AppLaunches)
                        //{
                        //    foreach (Feature feature in launch.Features)
                        //    {
                        //        if (!string.IsNullOrEmpty(session.DeviceId))
                        //        {
                        //            DalHelper.AddFeatureUsage(analyticsDeviceID, launch.startTime, launch.endTime, session.AppLaunches.Count, feature.Name, feature.StartTimestamp, feature.EndTimestamp);
                        //        }
                        //    }
                        //}

                        response.hasError = false;
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception e)
            {
                response.hasError = true;
                response.errorMessage = ConfigurationManager.AppSettings["AddUsageError"];
            }

            return response;
        }

        public static ServiceResponse GetDeviceToken(string authToken, string deviceInfo)
        {
            ServiceResponse response = new ServiceResponse("GetDeviceToken", true);
            CheckResponseFormat();
            Validator.Validate(response, "auth", authToken, "deviceInfo", deviceInfo);

            if (!DalHelper.CheckAuthToken(authToken))
            {
                response.hasError = true;
                response.errorMessage = ConfigurationManager.AppSettings["InvalidAuthTokenError"];
            }

            if (response.hasError)
            {
                return response;
            }
            else
            {
                try
                {
                    IList<AddDeviceTokenInfo> list = DalHelper.AddDeviceToken(authToken, deviceInfo);
                    response.responseObject = (list.Count() > 0) ? list : null;
                    response.hasError = false;
                    response.hasResponse = (list.Count() > 0) ? true : false;
                }
                catch (Exception e)
                {
                    response.hasError = true;
                    response.errorMessage = ExceptionHelper.GetExceptionString(e, debugOn);
                }
            }
            return response;
        }

        public static ServiceResponse GetDestinationsForRoute(string authToken, string routeId, string deviceToken)
        {
            ServiceResponse response = new ServiceResponse("GetDestinationsForRoute", true);
            CheckResponseFormat();
            Validator.Validate(response, "auth", authToken, "routeId", routeId, "deviceToken", deviceToken);

            if (response.hasError)
            {
                return response;
            }
            else
            {
                try
                {
                    IList<DestinationsForRouteInfo> list = DalHelper.GetDestinationsForRoute(short.Parse(routeId));
                    response.responseObject = (list.Count() > 0) ? list : null;
                    response.hasError = false;
                    response.hasResponse = (list.Count() > 0) ? true : false;
                }
                catch (Exception e)
                {
                    response.hasError = true;
                    response.errorMessage = ExceptionHelper.GetExceptionString(e, debugOn);
                }
            }
            return response;
        }


        public static ServiceResponse GetDestinationsForAllRoutes(string authToken, string deviceToken)
        {

            ServiceResponse response = new ServiceResponse("GetDestinationsForAllRoutes", true);
            CheckResponseFormat();
            Validator.Validate(response, "auth", authToken, "deviceToken", deviceToken);

            if (response.hasError)
            {
                return response;
            }
            else
            {
                try
                {
                    //lookup and db auth code
                    IList<DestinationsForAllRoutesInfo> list = DalHelper.GetDestinationsForAllRoutes();
                    response.responseObject = (list.Count() > 0) ? list : null;
                    response.hasError = false;
                    response.hasResponse = (list.Count() > 0) ? true : false;

                }
                catch (Exception e)
                {
                    response.hasError = true;
                    response.errorMessage = ExceptionHelper.GetExceptionString(e, debugOn);
                }
            }
            response.timeResponded = DateTime.Now;
            return response;
        }


        public static ServiceResponse GetListOfStopsByRouteNoAndDirection(string authToken, string routeNo, string isUpDirection, string deviceToken)
        {
            
            ServiceResponse response = new ServiceResponse("GetListOfStopsByRouteNoAndDirection", true);
            CheckResponseFormat();

            Validator.Validate(response, "auth", authToken, "routeId", routeNo, "isUpDirection", isUpDirection, "deviceToken", deviceToken);

            if (response.hasError)
            {
                return response;
            }
            else
            {
                try
                {
                   IList<ListOfStopsByRouteNoAndDirectionInfo> list = DalHelper.GetListOfStopsByRouteNoAndDirection(Convert.ToInt16(routeNo), bool.Parse(isUpDirection));
                    response.responseObject = (list.Count() > 0) ? list : null;
                    response.hasError = false;
                    response.hasResponse = (list.Count() > 0) ? true : false;
                }
                catch (Exception e)
                {
                    response.hasError = true;
                    response.errorMessage = ExceptionHelper.GetExceptionString(e, debugOn);
                }
            }
            response.timeResponded = DateTime.Now;
            return response;
           
        }

        public static ServiceResponse GetMainRoutesForStop(string authToken, string stopNo, string deviceToken)
        {

            ServiceResponse response = new ServiceResponse("GetMainRoutesForStop", true);
            CheckResponseFormat();

            Validator.Validate(response, "auth", authToken, "stopNo", stopNo, "deviceToken", deviceToken);

            if (response.hasError)
            {
                return response;
            }
            else
            {
                try
                {
                    IList<MainRoutesForStopInfo>  list = DalHelper.GetMainRoutesForStop(Int16.Parse(stopNo), false, null);
                    response.responseObject = (list.Count() > 0) ? list : null;
                    response.hasError = false;
                    response.hasResponse = (list.Count() > 0) ? true : false;
                }
                catch (Exception e)
                {
                    response.hasError = true;
                    response.errorMessage = ExceptionHelper.GetExceptionString(e, debugOn);
                }
            }
            response.timeResponded = DateTime.Now;
            return response;
        }

        public static ServiceResponse GetNextPredictedArrivalTimeAtStopsForTramNo(string authToken, string tramNo, string deviceToken)
        {

            ServiceResponse response = new ServiceResponse("GetNextPredictedArrivalTimeAtStopsForTramNo", false);
            CheckResponseFormat();
            Validator.Validate(response, "auth", authToken, "tramNo", tramNo, "deviceToken", deviceToken);

            string clientTypeId = "2";

            ClientType clientType = (ClientType)Int32.Parse(clientTypeId);

            if (response.hasError)
            {
                LogRequest(deviceToken, authToken, response.webMethodCalled, tramNo, null, null, null, "FAILED", null, null);
                return response;
            }
            else
            {
                //lookup and db auth code

                DataSet ds = null;
                string resp;
                try
                {
                    ds = SoapService.Instance.GetNextPredictedArrivalTimeAtStopsForTramNo(Int16.Parse(tramNo), out resp);
                    response.hasError = false;


                    if (ds != null)
                    {
                        DataTable dt = ds.Tables["TramNoRunDetailsTable"];
                        if (dt != null)
                        {
                            NextPredictedArrivalTimeAtStopsForTramNoInfo prediction = new NextPredictedArrivalTimeAtStopsForTramNoInfo
                            {
                                VehicleNo = short.Parse(dt.Rows[0]["VehicleNo"].ToString()),
                                AtLayover = bool.Parse(dt.Rows[0]["AtLayover"].ToString()),
                                RouteNo = dt.Rows[0]["RouteNo"].ToString(),
                                HeadBoardRouteNo = dt.Rows[0]["HeadBoardRouteNo"].ToString(),
                                Available = bool.Parse(dt.Rows[0]["Available"].ToString()),
                                Up = bool.Parse(dt.Rows[0]["Up"].ToString()),
                                HasSpecialEvent = bool.Parse(dt.Rows[0]["HasSpecialEvent"].ToString()),
                                SpecialEventMessage = dt.Rows[0]["SpecialEventMessage"].ToString(),
                                HasDisruption = bool.Parse(dt.Rows[0]["HasDisruption"].ToString()),
                            };

                            DisruptionMessageInfo msg = DalHelper.GetRouteDisruptionMessageByRouteNoAndDirection(Convert.ToInt16(prediction.RouteNo), prediction.Up, clientType);
                            if (msg != null)
                            {
                                prediction.HasDisruption = true; //need to set HasDisruption to true here in case FOC DNS is not set

                                prediction.DisruptionMessage = msg;
                            }

                            DataTable dt2 = ds.Tables["NextPredictedStopsDetailsTable"];
                            if (dt2 != null)
                            {
                                foreach (DataRow dr in dt2.Rows)
                                {
                                    NextPredictedStopsDetailsTableInfo predictedStopsDetails = new NextPredictedStopsDetailsTableInfo { StopNo = short.Parse(dr["StopNo"].ToString()), PredictedArrivalDateTime = DateTime.Parse(dr["PredictedArrivalDateTime"].ToString()) };
                                    prediction.NextPredictedStopsDetails.Add(predictedStopsDetails);
                                }
                            }
                            response.hasResponse = true;
                            response.responseObject = prediction;
                        }
                    }
                    LogRequest(deviceToken, authToken, response.webMethodCalled, tramNo, null, null, null, "PASSED", null, null);
                }
                catch (Exception e)
                {
                    response.hasError = true;
                    response.errorMessage = ExceptionHelper.GetExceptionString(e, debugOn);
                    LogRequest(deviceToken, authToken, response.webMethodCalled, tramNo, null, null, null, "FAILED", null, null);
                }
            }
            response.timeResponded = DateTime.Now;
            return response;
        }

        
        public static ServiceResponse GetSchedulesCollection(string authToken, string stopNo, string routeNo, string lowFloor, string clientRequestDateTime, string deviceToken)
        {

            ServiceResponse response = new ServiceResponse("GetSchedulesCollection", false);
            CheckResponseFormat();

            Validator.Validate(response, "auth", authToken, "stopNo", stopNo, "routeId", routeNo, "lowFloor", lowFloor, "dateTime", clientRequestDateTime, "deviceToken", deviceToken);

            if (response.hasError)
            {
                LogRequest(deviceToken, authToken, response.webMethodCalled, null, stopNo, routeNo, lowFloor, "FAILED", null, null);
                return response;
            }
            else
            {
                //lookup and db auth code
                IList<SchedulesCollectionInfo> list = new List<SchedulesCollectionInfo>();
                DataTable dt = null;
                string resp;
                try
                {

                    dt = SoapService.Instance.GetSchedulesCollection(Int16.Parse(stopNo), routeNo, bool.Parse(lowFloor), DateTime.Parse(clientRequestDateTime), out resp);
                    response.hasError = false;

                    if (dt != null)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            SchedulesCollectionInfo schedule = new SchedulesCollectionInfo();

                            schedule.InternalRouteNo = short.Parse(dr["InternalRouteNo"].ToString());
                            schedule.RouteNo = dr["RouteNo"].ToString();
                            schedule.HeadBoardRouteNo = dr["HeadBoardRouteNo"].ToString();
                            schedule.VehicleNo = short.Parse(dr["VehicleNo"].ToString());
                            schedule.Destination = dr["Destination"].ToString();
                            schedule.HasDisruption = bool.Parse(dr["HasDisruption"].ToString());
                            schedule.IsTTAvailable = bool.Parse(dr["IsTTAvailable"].ToString());
                            schedule.IsLowFloorTram = bool.Parse(dr["IsLowFloorTram"].ToString());
                            schedule.AirConditioned = bool.Parse(dr["AirConditioned"].ToString());
                            schedule.DisplayAC = bool.Parse(dr["DisplayAC"].ToString());
                            schedule.HasSpecialEvent = bool.Parse(dr["HasSpecialEvent"].ToString());
                            schedule.SpecialEventMessage = dr["SpecialEventMessage"].ToString();
                            schedule.PredictedArrivalDateTime = DateTime.Parse(dr["PredictedArrivalDateTime"].ToString());
                            schedule.RequestDateTime = DateTime.Parse(dr["RequestDateTime"].ToString());
                            schedule.BaseTimestamp = DateTime.Parse(dr["RequestDateTime"].ToString());
                            schedule.TripID = Convert.ToInt32(dr["TripID"]);
                            list.Add(schedule);
                        }
                        response.responseObject = (list.Count() > 0) ? list : null;
                        response.hasResponse = (list.Count() > 0) ? true : false;
                    }
                    LogRequest(deviceToken, authToken, response.webMethodCalled, null, stopNo, routeNo, lowFloor, "PASSED", null, null);
                }
                catch (Exception e)
                {
                    response.hasError = true;
                    response.errorMessage = ExceptionHelper.GetExceptionString(e, debugOn);
                    LogRequest(deviceToken, authToken, response.webMethodCalled, null, stopNo, routeNo, lowFloor, "FAILED", null, null);
                }
            }
            response.timeResponded = DateTime.Now;
            return response;
        }



        public static ServiceResponse GetRouteStopsByRoute(string authToken, string routeNo, string deviceToken)
        {

            ServiceResponse response = new ServiceResponse("GetRouteStopsByRoute", true);
            CheckResponseFormat();

            Validator.Validate(response, "auth", authToken, "routeId", routeNo, "deviceToken", deviceToken);

            if (response.hasError)
            {
                return response;  
            }
            else
            {
                try
                {
                        //lookup and db auth code
                        IList<RouteStopsByRouteInfo> list = DalHelper.GetRouteStopsByRoute(routeNo);
                        response.responseObject = (list.Count() > 0) ? list : null;
                        response.hasError = false;
                        response.hasResponse = (list.Count() > 0) ? true : false;
                   
                }
                catch (Exception e)
                {
                    response.hasError = true;
                    response.errorMessage = ExceptionHelper.GetExceptionString(e, debugOn);
                }
            }
            response.timeResponded = DateTime.Now;
            return response;
        }

        public static ServiceResponse GetPlatformStopsByRouteAndDirection(string authToken, string routeNo, string isUpDirection, string deviceToken)
        {
            ServiceResponse response = new ServiceResponse("GetPlatformStopsByRouteAndDirection", true);
            CheckResponseFormat();

            Validator.Validate(response, "auth", authToken, "routeId", routeNo, "isUpDirection", isUpDirection, "deviceToken", deviceToken);

            if (response.hasError)
            {
                return response;
            }
            else
            {
                try
                {
                    IList<PlatformStopInfo> list = DalHelper.GetPlatformStopsByRouteAndDirection(Convert.ToInt16(routeNo), Convert.ToBoolean(isUpDirection));

                    response.responseObject = (list.Count() > 0) ? list : null;
                    response.hasError = false;
                    response.hasResponse = (list.Count() > 0) ? true : false;
                }
                catch (Exception e)
                {
                    response.hasError = true;
                    response.errorMessage = ExceptionHelper.GetExceptionString(e, debugOn);
                }
            }
            response.timeResponded = DateTime.Now;
            return response;
        }

        public static ServiceResponse GetRouteSummaries(string authToken, string deviceToken)
        {

            ServiceResponse response = new ServiceResponse("GetRouteSummaries", true);
            CheckResponseFormat();

            Validator.Validate(response, "auth", authToken, "deviceToken", deviceToken);

            if (response.hasError)
            {
                return response;
            }
            else
            {
                try
                {
                    //lookup and db auth code
                    IList<RouteSummariesInfo> list = DalHelper.GetRouteSummaries();
                    response.responseObject = (list.Count() > 0) ? list : null;
                    response.hasError = false;
                    response.hasResponse = (list.Count() > 0) ? true : false;
                }
                catch (Exception e)
                {
                    response.hasError = true;
                    response.errorMessage = ExceptionHelper.GetExceptionString(e, debugOn);
                }
            }
            response.timeResponded = DateTime.Now;
            return response;
        }

        public static ServiceResponse GetSchedulesForTrip(string authToken, string tripID, string scheduleDateTime, string deviceToken)
        {

            ServiceResponse response = new ServiceResponse("GetSchedulesForTrip", false);
            CheckResponseFormat();


            Validator.Validate(response, "auth", authToken, "tripID", tripID, "dateTime", scheduleDateTime, "deviceToken", deviceToken);

            if (response.hasError)
            {
                LogRequest(deviceToken, authToken, response.webMethodCalled, null, null, null, null, "FAILED", null, tripID);
                return response;
            }
            else
            {
                //lookup and db auth code
                IList<SchedulesForTripInfo> list = null;
                DataTable dt = null;
                string resp;
                try
                {

                    dt = SoapService.Instance.GetSchedulesForTrip(int.Parse(tripID), DateTime.Parse(scheduleDateTime), out resp);
                    response.hasError = false;

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        list = new List<SchedulesForTripInfo>();

                        foreach (DataRow dr in dt.Rows)
                        {
                            SchedulesForTripInfo schedule = new SchedulesForTripInfo();


                            schedule.StopNo = short.Parse(dr["StopNo"].ToString());
                            schedule.Time = int.Parse(dr["Time"].ToString());
                            schedule.ScheduledArrivalDateTime = DateTime.Parse(dr["ScheduledArrivalDateTime"].ToString());

                            list.Add(schedule);
                        }
                        response.responseObject = (list.Count() > 0) ? list : null;
                        response.hasResponse = (list.Count() > 0) ? true : false;
                    }
                    LogRequest(deviceToken, authToken, response.webMethodCalled, null, null, null, null, "PASSED", null, tripID);
                }
                catch (Exception e)
                {
                    response.hasError = true;
                    response.errorMessage = ExceptionHelper.GetExceptionString(e, debugOn);
                    LogRequest(deviceToken, authToken, response.webMethodCalled, null, null, null, null, "FAILED", null, tripID);
                }
            }
            response.timeResponded = DateTime.Now;
            return response;
        }

        public static ServiceResponse GetStopInformation(string authToken, string stopNo, string deviceToken)
        {

            ServiceResponse response = new ServiceResponse("GetStopInformation", true);
            CheckResponseFormat();


            Validator.Validate(response, "auth", authToken, "stopNo", stopNo, "deviceToken", deviceToken);

            if (response.hasError)
            {
                return response;
            }
            else
            {
                try
                {
                    IList<StopInformationInfo> list = DalHelper.GetStopInformation(Int16.Parse(stopNo));
                    response.responseObject = (list.Count() > 0) ? list : null;
                    response.hasError = false;
                    response.hasResponse = (list.Count() > 0) ? true : false;
                }
                catch (Exception e)
                {
                    response.hasError = true;
                    response.errorMessage = ExceptionHelper.GetExceptionString(e, debugOn);
                }
            }
            response.timeResponded = DateTime.Now;
            return response;
        }

        public static ServiceResponse GetTicketOutlets(string authToken, string deviceToken)
        {
            ServiceResponse response = new ServiceResponse("GetTicketOutlets", true);
            CheckResponseFormat();

            Validator.Validate(response, "auth", authToken, "deviceToken", deviceToken);

            if (response.hasError)
            {
                return response;
            }
            else
            {
                try
                {
                    IList<TicketOutletsInfo> list = DalHelper.GetTicketOutlets();
                    response.responseObject = (list.Count() > 0) ? list : null;
                    response.hasError = false;
                    response.hasResponse = (list.Count() > 0) ? true : false;
                }
                catch (Exception e)
                {
                    response.hasError = true;
                    response.errorMessage = ExceptionHelper.GetExceptionString(e, debugOn);
                }
            }
            response.timeResponded = DateTime.Now;
            return response;
        }

        public static ServiceResponse GetTicketOutletById(int ticketOutletId, string authToken, string deviceToken)
        {
            ServiceResponse response = new ServiceResponse("GetTicketOutletById", true);
            CheckResponseFormat();

            Validator.Validate(response, "ticketOutletId", ticketOutletId, "auth", authToken, "deviceToken", deviceToken);

            if (response.hasError)
            {
                return response;
            }
            else
            {
                try
                {
                    IList<TicketOutletByIdInfo> list = DalHelper.GetTicketOutletById(ticketOutletId);
                    response.responseObject = (list.Count() > 0) ? list : null;
                    response.hasError = false;
                    response.hasResponse = (list.Count() > 0) ? true : false;
                }
                catch (Exception e)
                {
                    response.hasError = true;
                    response.errorMessage = ExceptionHelper.GetExceptionString(e, debugOn);
                }
            }
            response.timeResponded = DateTime.Now;
            return response;
        }

        public static ServiceResponse GetPointsOfInterestChangesSince(string authToken, DateTime lastUpdateDate, string deviceToken)
        {
            ServiceResponse response = new ServiceResponse("GetPointsOfInterestChangesSince", false); //change to true
            CheckResponseFormat();

            Validator.Validate(response, "lastUpdateDate", lastUpdateDate, "auth", authToken, "deviceToken", deviceToken);

            if (response.hasError)
            {
                return response;
            }
            else
            {
                try
                {
                    DateTime cutOffDate = Convert.ToDateTime(System.Configuration.ConfigurationManager.AppSettings["CoreUpdateCutoffDate"]);

                    if (lastUpdateDate > cutOffDate && lastUpdateDate <= DateTime.Now)
                    {
                        DateTime sinceDate;
                        DateTime endDate;
                        DateTime currentDateTime = DateTime.Now;
                        DateTime currentDayAt4AM;
                        DateTime nextDayAt4AM;
                        DateTime lastUpdateDateAt4AM;

                        string[] recyclePoolTime = System.Configuration.ConfigurationManager.AppSettings["PoolRecycleTime"].Split(':');

                        int rptHour = 4; // by default
                        int rptMin = 0; // by default
                        int rptSec = 0; // by default

                        if (recyclePoolTime != null && recyclePoolTime.Length == 3)
                        {
                            int.TryParse(recyclePoolTime[0], out rptHour);
                            int.TryParse(recyclePoolTime[1], out rptMin);
                            int.TryParse(recyclePoolTime[2], out rptSec);
                        }

                        currentDayAt4AM = new DateTime(currentDateTime.Year, currentDateTime.Month, currentDateTime.Day, rptHour, rptMin, rptSec);
                        nextDayAt4AM = new DateTime(currentDateTime.AddDays(1).Year, currentDateTime.AddDays(1).Month, currentDateTime.AddDays(1).Day, rptHour, rptMin, rptSec);

                        // when debug flag is on, we set the current time to anything we want using the value in the config file
                        if (debugOn)
                        {
                            currentDateTime = Convert.ToDateTime(System.Configuration.ConfigurationManager.AppSettings["CurrentTimeDebug"]);
                            lastUpdateDate = lastUpdateDate.AddHours(Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["OffSetHours"]));
                        }

                        currentDayAt4AM = new DateTime(currentDateTime.Year, currentDateTime.Month, currentDateTime.Day, rptHour, rptMin, rptSec);
                        nextDayAt4AM = new DateTime(currentDateTime.AddDays(1).Year, currentDateTime.AddDays(1).Month, currentDateTime.AddDays(1).Day, rptHour, rptMin, rptSec);

                        // set the pool recycle date time on the last update datetime for comparison
                        lastUpdateDateAt4AM = new DateTime(lastUpdateDate.Year, lastUpdateDate.Month, lastUpdateDate.Day, rptHour, rptMin, rptSec);

                        // when last update date is before 4am, then we have to use the since date the day before at 4am. otherwise we can use the last update
                        // date time at 4am
                        if (lastUpdateDate < lastUpdateDateAt4AM)
                            sinceDate = lastUpdateDateAt4AM.AddDays(-1);
                        else
                            sinceDate = lastUpdateDateAt4AM;

                        if (currentDateTime < currentDayAt4AM)
                            endDate = currentDayAt4AM.AddDays(-1);
                        else
                            endDate = currentDayAt4AM;

                        /*
                        if (debugOn)
                        {
                            StringBuilder eventLog = new StringBuilder();
                            eventLog.AppendLine();
                            eventLog.AppendLine("Last Update DateTime From iPhone: " + lastUpdateDate.ToString());
                            eventLog.AppendLine("Last Update DateTime At 4AM: " + lastUpdateDateAt4AM.ToString());
                            eventLog.AppendLine("Current DateTime: " + currentDateTime.ToString());
                            eventLog.AppendLine("Current DateTime At 4AM: " + currentDayAt4AM.ToString());
                            eventLog.AppendLine("Next Day DateTime At 4AM: " + nextDayAt4AM.ToString());
                            eventLog.AppendLine("Date Since: " + sinceDate.ToString());
                            eventLog.AppendLine("End Date: " + endDate.ToString());

                            validationResult = eventLog.ToString();
                        }
                        */

                        IList<PointsOfInterestChangesSinceInfo> list = DalHelper.GetPointsOfInterestChangesSince(sinceDate, endDate);
                        response.responseObject = (list.Count() > 0) ? list : null;
                        response.hasError = false;
                        response.hasResponse = (list.Count() > 0) ? true : false;
                    }
                }
                catch (Exception e)
                {
                    response.hasError = true;
                    response.errorMessage = ExceptionHelper.GetExceptionString(e, debugOn);
                }
            }
            response.timeResponded = DateTime.Now;
            return response;
        }


        public static ServiceResponse GetTicketOutletChangesSince(string authToken, DateTime lastUpdateDate, string deviceToken)
        {
            ServiceResponse response = new ServiceResponse("GetTicketOutletChangesSince", false); //change to true
            CheckResponseFormat();

            Validator.Validate(response, "lastUpdateDate", lastUpdateDate, "auth", authToken, "deviceToken", deviceToken);

            if (response.hasError)
            {
                return response;
            }
            else
            {
                try
                {
                    DateTime cutOffDate = Convert.ToDateTime(System.Configuration.ConfigurationManager.AppSettings["CoreUpdateCutoffDate"]);

                    if (lastUpdateDate > cutOffDate && lastUpdateDate <= DateTime.Now)
                    {
                        DateTime sinceDate;
                        DateTime endDate;
                        DateTime currentDateTime = DateTime.Now;
                        DateTime currentDayAt4AM;
                        DateTime nextDayAt4AM;
                        DateTime lastUpdateDateAt4AM;
                        
                        string[] recyclePoolTime = System.Configuration.ConfigurationManager.AppSettings["PoolRecycleTime"].Split(':');

                        int rptHour = 4; // by default
                        int rptMin = 0; // by default
                        int rptSec = 0; // by default

                        if (recyclePoolTime != null && recyclePoolTime.Length == 3)
                        {
                            int.TryParse(recyclePoolTime[0], out rptHour);
                            int.TryParse(recyclePoolTime[1], out rptMin);
                            int.TryParse(recyclePoolTime[2], out rptSec);
                        }

                        currentDayAt4AM = new DateTime(currentDateTime.Year, currentDateTime.Month, currentDateTime.Day, rptHour, rptMin, rptSec);
                        nextDayAt4AM = new DateTime(currentDateTime.AddDays(1).Year, currentDateTime.AddDays(1).Month, currentDateTime.AddDays(1).Day, rptHour, rptMin, rptSec);   

                        // when debug flag is on, we set the current time to anything we want using the value in the config file
                        if (debugOn)
                        {
                            currentDateTime = Convert.ToDateTime(System.Configuration.ConfigurationManager.AppSettings["CurrentTimeDebug"]);
                            lastUpdateDate = lastUpdateDate.AddHours(Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["OffSetHours"]));
                        }

                        currentDayAt4AM = new DateTime(currentDateTime.Year, currentDateTime.Month, currentDateTime.Day, rptHour, rptMin, rptSec);
                        nextDayAt4AM = new DateTime(currentDateTime.AddDays(1).Year, currentDateTime.AddDays(1).Month, currentDateTime.AddDays(1).Day, rptHour, rptMin, rptSec);  

                        // set the pool recycle date time on the last update datetime for comparison
                        lastUpdateDateAt4AM = new DateTime(lastUpdateDate.Year, lastUpdateDate.Month, lastUpdateDate.Day, rptHour, rptMin, rptSec);

                        // when last update date is before 4am, then we have to use the since date the day before at 4am. otherwise we can use the last update
                        // date time at 4am
                        if (lastUpdateDate < lastUpdateDateAt4AM)
                            sinceDate = lastUpdateDateAt4AM.AddDays(-1);
                        else
                            sinceDate = lastUpdateDateAt4AM;

                        if (currentDateTime < currentDayAt4AM)
                            endDate = currentDayAt4AM.AddDays(-1);
                        else
                            endDate = currentDayAt4AM;

                        /*
                        if (debugOn)
                        {
                            StringBuilder eventLog = new StringBuilder();
                            eventLog.AppendLine();
                            eventLog.AppendLine("Last Update DateTime From iPhone: " + lastUpdateDate.ToString());
                            eventLog.AppendLine("Last Update DateTime At 4AM: " + lastUpdateDateAt4AM.ToString());
                            eventLog.AppendLine("Current DateTime: " + currentDateTime.ToString());
                            eventLog.AppendLine("Current DateTime At 4AM: " + currentDayAt4AM.ToString());
                            eventLog.AppendLine("Next Day DateTime At 4AM: " + nextDayAt4AM.ToString());
                            eventLog.AppendLine("Date Since: " + sinceDate.ToString());
                            eventLog.AppendLine("End Date: " + endDate.ToString());

                            validationResult = eventLog.ToString();
                        }
                        */

                        IList<TicketOutletSinceInfo> list = DalHelper.GetTicketOutletChangesSince(sinceDate, endDate);
                        response.responseObject = (list.Count() > 0) ? list : null;
                        response.hasError = false;
                        response.hasResponse = (list.Count() > 0) ? true : false;
                    }
                }
                catch (Exception e)
                {
                    response.hasError = true;
                    response.errorMessage = ExceptionHelper.GetExceptionString(e, debugOn);
                }
            }
            response.timeResponded = DateTime.Now;
            return response;
        }

        public static ServiceResponse GetPointsOfInterest(string authToken, string deviceToken)
        {
            ServiceResponse response = new ServiceResponse("GetPointsOfInterest", true);
            CheckResponseFormat();

            Validator.Validate(response, "auth", authToken, "deviceToken", deviceToken);

            if (response.hasError)
            {
                return response;
            }
            else
            {
                try
                {
                    IList<PointsOfInterestInfo> list = DalHelper.GetPointsOfInterest();
                    response.responseObject = (list.Count() > 0) ? list : null;
                    response.hasError = false;
                    response.hasResponse = (list.Count() > 0) ? true : false;
                }
                catch (Exception e)
                {
                    response.hasError = true;
                    response.errorMessage = ExceptionHelper.GetExceptionString(e, debugOn);
                }
            }
            response.timeResponded = DateTime.Now;
            return response;
        }

        public static ServiceResponse GetPointsOfInterestByStopNo(string authToken, short stopNo, string deviceToken)
        {
            ServiceResponse response = new ServiceResponse("GetPointsOfInterestByStopNo", true);
            CheckResponseFormat();

            Validator.Validate(response, "auth", authToken, "deviceToken", deviceToken);

            if (response.hasError)
            {
                return response;
            }
            else
            {
                try
                {
                    IList<PointsOfInterestInfo> list = DalHelper.GetPointsOfInterestByStopNo(stopNo);
                    response.responseObject = (list.Count() > 0) ? list : null;
                    response.hasError = false;
                    response.hasResponse = (list.Count() > 0) ? true : false;
                }
                catch (Exception e)
                {
                    response.hasError = true;
                    response.errorMessage = ExceptionHelper.GetExceptionString(e, debugOn);
                }
            }
            response.timeResponded = DateTime.Now;
            return response;
        }

        public static ServiceResponse GetPointOfInterestById(string authToken, int poiId, string deviceToken)
        {
            ServiceResponse response = new ServiceResponse("GetPointOfInterestById", true);
            CheckResponseFormat();

            Validator.Validate(response, "auth", authToken, "poiId", poiId, "deviceToken", deviceToken);

            if (response.hasError)
            {
                return response;
            }
            else
            {
                try
                {
                    IList<PointOfInterestByIDInfo> list = DalHelper.GetPointOfInterestById(poiId);
                    response.responseObject = (list.Count() > 0) ? list : null;
                    response.hasError = false;
                    response.hasResponse = (list.Count() > 0) ? true : false;
                }
                catch (Exception e)
                {
                    response.hasError = true;
                    response.errorMessage = ExceptionHelper.GetExceptionString(e, debugOn);
                }
            }
            response.timeResponded = DateTime.Now;
            return response;
        }

        public static ServiceResponse GetStopsByPointOfInterestId(string authToken, int poiId, string deviceToken)
        {
            ServiceResponse response = new ServiceResponse("GetStopsByPointsOfInterestId", true);
            CheckResponseFormat();

            Validator.Validate(response, "auth", authToken, "poiId", poiId, "deviceToken", deviceToken);

            if (response.hasError)
            {
                return response;
            }
            else
            {
                try
                {
                    IList<ListOfStopsByPOIIdInfo> list = DalHelper.GetListOfStopsByPOIId(poiId);
                    response.responseObject = (list.Count() > 0) ? list : null;
                    response.hasError = false;
                    response.hasResponse = (list.Count() > 0) ? true : false;
                }
                catch (Exception e)
                {
                    response.hasError = true;
                    response.errorMessage = ExceptionHelper.GetExceptionString(e, debugOn);
                }
            }
            response.timeResponded = DateTime.Now;
            return response;
        }

        public static ServiceResponse GetStopsAndRoutesUpdatesSince(string authToken, DateTime lastUpdateDate, string deviceToken)
        {

            ServiceResponse response = new ServiceResponse("GetStopsAndRoutesUpdatesSince", false);
            CheckResponseFormat();

            Validator.Validate(response, "lastUpdateDate", lastUpdateDate, "auth", authToken, "deviceToken", deviceToken);

            if (authToken == null)
            {
                LogRequest(deviceToken, authToken, response.webMethodCalled, null, null, null, null, "FAILED", lastUpdateDate.ToShortDateString(), null);
                response.hasError = true;
                response.errorMessage = "No or invalid authentication token provided";
            }
            else
            {
                //lookup and db auth code

                DataSet ds = null;
                string resp;
                try
                {

                    ds = SoapService.Instance.GetStopsAndRoutesUpdatesSince(lastUpdateDate, out resp);

                    if (ds != null)
                    {
                        StopsAndRoutesUpdatesSinceInfo updates = new StopsAndRoutesUpdatesSinceInfo();
                        response.hasError = false;

                        DataTable dt1 = ds.Tables["dtRoutesChanges"];
                        if (dt1 != null && dt1.Rows.Count > 0)
                        {
                            List<RouteChangeInfo> routeChanges = new List<RouteChangeInfo>();
                            foreach (DataRow dr in dt1.Rows)
                            {
                                RouteChangeInfo r = new RouteChangeInfo();
                                r.ID = short.Parse(dr["ID"].ToString());
                                r.HeadboardRouteNo = dr["HeadboardRouteNo"].ToString();
                                r.RouteNo = dr["RouteNo"].ToString();
                                r.IsMainRoute = bool.Parse(dr["IsMainRoute"].ToString());
                                r.Action = dr["Action"].ToString();
                                r.Colour = dr["Colour"].ToString();
                                routeChanges.Add(r);
                            }
                            updates.RouteChanges = routeChanges;
                        }
                        DataTable dt2 = ds.Tables["dtStopsChanges"];
                        if (dt2 != null && dt2.Rows.Count > 0)
                        {
                            List<StopChangeInfo> stopChanges = new List<StopChangeInfo>();
                            foreach (DataRow dr in dt2.Rows)
                            {
                                StopChangeInfo s = new StopChangeInfo();
                                s.StopNo = short.Parse(dr["StopNo"].ToString());
                                s.Action = dr["Action"].ToString();
                                stopChanges.Add(s);
                            }
                            updates.StopChanges = stopChanges;

                        }
                        DataTable dt3 = ds.Tables["dtServerTime"];
                        if (dt3 != null && dt3.Rows.Count > 0)
                        {
                            List<ServerTimeInfo> serverTime = new List<ServerTimeInfo>();
                            ServerTimeInfo st = new ServerTimeInfo();
                            st.ServerTime = DateTime.Parse(dt3.Rows[0]["ServerTime"].ToString());
                            serverTime.Add(st);
                            updates.ServerTime = new List<ServerTimeInfo>();
                            updates.ServerTime.Add(st);
                        }
                        response.responseObject = updates;
                        response.hasResponse = true;
                    }
                    LogRequest(deviceToken, authToken, response.webMethodCalled, null, null, null, null, "PASSED", lastUpdateDate.ToShortDateString(), null);
                }
                catch (Exception e)
                {
                    LogRequest(deviceToken, authToken, response.webMethodCalled, null, null, null, null, "FAILED", lastUpdateDate.ToShortDateString(), null);
                    response.hasError = true;
                    response.errorMessage = ExceptionHelper.GetExceptionString(e, debugOn);
                }
            }
            response.timeResponded = DateTime.Now;
            return response;
        }

        public static ServiceResponse ValidateInput(string authToken, short stopNo, string routeNo, bool lowFloor, string deviceToken)
        {

            ServiceResponse response = new ServiceResponse("ValidateInput", false);
            CheckResponseFormat();

            if (authToken == null)
            {
                response.hasError = true;
                response.errorMessage = ConfigurationManager.AppSettings["NoAuthTokenError"];
            }
            else
            {
                //lookup and db auth code

                bool valid = false;
                string resp;
                try
                {
                    valid = SoapService.Instance.ValidateInput(stopNo, routeNo, lowFloor, out resp);
                    response.hasError = false;

                    response.responseObject = valid;

                }
                catch (Exception e)
                {
                    response.hasError = true;
                    response.errorMessage = ExceptionHelper.GetExceptionString(e, debugOn);
                }
            }
            response.timeResponded = DateTime.Now;
            return response;
        }

        //public static ServiceResponse GetPredictions(string authToken, string clientTypeId, string stopNo, string routeNo, string lowFloor, string deviceToken)
        //{

        //    ServiceResponse response = new ServiceResponse("GetPredictions", false);
        //    CheckResponseFormat();
        //    Validator.Validate(response, "auth", authToken, "clientType", clientTypeId, "stopNo", stopNo, "routeId", routeNo, "lowFloor", lowFloor, "deviceToken", deviceToken);

        //    if (response.hasError)
        //    {
        //        return response;
        //    }
        //    else
        //    {
        //        //lookup and db auth code
        //        IList<PredictionInfo> list = null;
        //        ClientType clientType = (ClientType)Int32.Parse(clientTypeId);

        //        try
        //        {
        //            list = DalHelper.GetPredictions(Convert.ToInt16(stopNo), Convert.ToInt16(routeNo), Convert.ToBoolean(lowFloor));
        //            response.responseObject = (list.Count() > 0) ? list : null;
        //            response.hasError = false;
        //            response.hasResponse = (list.Count() > 0) ? true : false;
        //        }
        //        catch (Exception e)
        //        {
        //            response.hasError = true;
        //            response.errorMessage = ExceptionHelper.GetExceptionString(e, debugOn);
        //        }
        //    }
        //    response.timeResponded = DateTime.Now;
        //    return response;
        //}

        public static ServiceResponse GetPredictionsCollection(string authToken, string clientTypeId, string stopNo, string routeNo, string lowFloor, string deviceToken)
        {

            ServiceResponse response = new ServiceResponse("GetPredictions", false);
            CheckResponseFormat();
            Validator.Validate(response, "auth", authToken, "clientType", clientTypeId, "stopNo", stopNo, "routeId", routeNo, "lowFloor", lowFloor, "deviceToken", deviceToken);

            if (response.hasError)
            {
                return response;
            }
            else
            {
                //lookup and db auth code
                IList<PredictionInfo> list = null;
                ClientType clientType = (ClientType)Int32.Parse(clientTypeId);

                try
                {
                    list = DalHelper.GetPredictions(Convert.ToInt16(stopNo), Convert.ToInt16(routeNo), Convert.ToBoolean(lowFloor));
                    response.responseObject = (list.Count() > 0) ? list : null;
                    response.hasError = false;
                    response.hasResponse = (list.Count() > 0) ? true : false;
                }
                catch (Exception e)
                {
                    response.hasError = true;
                    //response.errorMessage = ExceptionHelper.GetExceptionString(e, debugOn);
                    response.errorMessage = e.Message;
                }
            }
            response.timeResponded = DateTime.Now;
            return response;
        }

        public static ServiceResponse GetNextPredictedRoutesCollection(string authToken, string clientTypeId, string stopNo, string routeNo, string lowFloor, string deviceToken)
        {
            ServiceResponse response = new ServiceResponse("GetNextPredictedRoutesCollection", false);
            CheckResponseFormat();
            Validator.Validate(response, "auth", authToken, "clientType", clientTypeId, "stopNo", stopNo, "routeId", routeNo, "lowFloor", lowFloor, "deviceToken", deviceToken);

            if (response.hasError)
            {
                LogRequest(deviceToken, authToken, response.webMethodCalled, null, stopNo, routeNo, lowFloor, "FAILED", null, null);
                return response;
            }
            else
            {
                //lookup and db auth code
                IList<NextPredictedRoutesCollectionInfo> list = null;
                ClientType clientType = (ClientType)Int32.Parse(clientTypeId);

                try
                {
                    list = DalHelper.GetPredictionsCollection(Convert.ToInt16(stopNo), Convert.ToInt16(routeNo), Convert.ToBoolean(lowFloor));
                    response.responseObject = (list.Count() > 0) ? list : null;
                    response.hasError = false;
                    response.hasResponse = true;
                    LogRequest(deviceToken, authToken, response.webMethodCalled, null, stopNo, routeNo, lowFloor, "PASSED", null, null);
                }
                catch (Exception e)
                {
                    response.hasError = true;
                    //response.errorMessage = ExceptionHelper.GetExceptionString(e, debugOn);
                    response.errorMessage = e.Message;
                    LogRequest(deviceToken, authToken, response.webMethodCalled, null, stopNo, routeNo, lowFloor, "FAILED", null, null);
                }
            }
            response.timeResponded = DateTime.Now;
            return response;
        }
    }

}

