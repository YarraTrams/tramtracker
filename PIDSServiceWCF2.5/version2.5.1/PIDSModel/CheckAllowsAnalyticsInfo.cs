﻿using System;
using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class CheckAllowsAnalyticsInfo
    {
        [DataMember()]
        public bool AllowsAnalytics { get; set; }
    }
}
