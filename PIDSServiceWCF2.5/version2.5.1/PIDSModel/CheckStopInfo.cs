﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class CheckStopInfo
    {
        public CheckStopInfo() { }

        [DataMember()]
        public bool IsValid { get; set; }

    }
}
