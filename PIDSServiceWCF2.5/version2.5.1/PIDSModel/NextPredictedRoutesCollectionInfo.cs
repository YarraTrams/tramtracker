﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class NextPredictedRoutesCollectionInfo
    {

        [DataMember()]
        public int TripID { get; set; }

        [DataMember()]
        public short InternalRouteNo { get; set; }

        [DataMember()]
        public string RouteNo { get; set; }

        [DataMember()]
        public string HeadBoardRouteNo { get; set; }

        [DataMember()]
        public short VehicleNo { get; set; }

        [DataMember()]
        public string Destination { get; set; }

        [DataMember()]
        public bool HasDisruption { get; set; }
        
        [DataMember()]
        public bool IsTTAvailable { get; set; }

        [DataMember()]
        public bool IsLowFloorTram { get; set; }

        [DataMember()]
        public bool AirConditioned { get; set; }

        [DataMember()]
        public bool DisplayAC { get; set; }

        [DataMember()]
        public bool HasSpecialEvent { get; set; }

        [DataMember()]
        public string SpecialEventMessage { get; set; }

        [DataMember()]
        public DateTime PredictedArrivalDateTime { get; set; }

        [DataMember()]
        public DisruptionMessageListInfo DisruptionMessage { get; set; }




    }
}