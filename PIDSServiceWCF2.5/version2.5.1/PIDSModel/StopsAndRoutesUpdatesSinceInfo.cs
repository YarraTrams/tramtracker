﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class StopsAndRoutesUpdatesSinceInfo
    {
        [DataMember()]
        public List<StopChangeInfo> StopChanges { get; set; }

        [DataMember()]
        public List<RouteChangeInfo> RouteChanges { get; set; }

        [DataMember()]
        public List<ServerTimeInfo> ServerTime { get; set; }
    }
}