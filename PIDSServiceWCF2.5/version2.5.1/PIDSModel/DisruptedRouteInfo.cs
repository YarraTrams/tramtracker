﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class DisruptedRouteInfo
    {

        [DataMember()]
        public int RouteNo { get; set; }

        [DataMember()]
        public DisruptionMessageInfo Message { get; set; }

    }
}
