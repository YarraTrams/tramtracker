﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class DisruptionMessageListInfo
    {

        public DisruptionMessageListInfo()
        {
            Messages = new List<DisruptionMessageInfo>();
            DisplayType = "Text";
            MessageCount = 0;
        }

        [DataMember()]
        public string DisplayType { get; set; }

        [DataMember()]
        public int MessageCount { get; set; }

        [DataMember()]
        public List<DisruptionMessageInfo> Messages { get; set; }





    }
}
