﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class DisruptedStopInfo
    {
        [DataMember()]
        public bool IsSingleRoute { get; set; }

        [DataMember()]
        public string RouteCombination { get; set; }

        [DataMember()]
        public DisruptionMessageInfo Message { get; set; }

    }
}
