﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace YarraTrams.PIDSRestService
{
    public class UsageSession
    {
        public int AnalyticsDeviceID { get; set; }
        public string OSVersion { get; set; }
        public string TramTrackerVersion{ get; set; }
        public string DeviceId { get; set; }
        public bool AllowsAnalytics { get; set; }
        public IList<AppLaunch> AppLaunches { get; set; }

        public DataTable CreateAppLaunchTable()
        {
            DataTable dtAppLaunches = new DataTable();

            DataColumn colDeviceID = new DataColumn("AnalyticsDeviceID", System.Type.GetType("System.Int32"));

            DataColumn colStartTime = new DataColumn();
            colStartTime.ColumnName = "StartTime";
            colStartTime.DataType = System.Type.GetType("System.DateTime");

            DataColumn colEndTime = new DataColumn();
            colEndTime.ColumnName = "EndTime";
            colEndTime.DataType = System.Type.GetType("System.DateTime");

            DataColumn colLaunchCount = new DataColumn();
            colLaunchCount.ColumnName = "LaunchCount";
            colLaunchCount.DataType = System.Type.GetType("System.Int32");

            DataColumn colFeatureUsed = new DataColumn();
            colFeatureUsed.ColumnName = "FeatureUsed";
            colFeatureUsed.DataType = System.Type.GetType("System.String");

            DataColumn colFeatureStartTimeStamp = new DataColumn();
            colFeatureStartTimeStamp.ColumnName = "FeatureStartTimeStamp";
            colFeatureStartTimeStamp.DataType = System.Type.GetType("System.DateTime");

            DataColumn colFeatureEndTimeStamp = new DataColumn();
            colFeatureEndTimeStamp.ColumnName = "FeatureEndTimeStamp";
            colFeatureEndTimeStamp.DataType = System.Type.GetType("System.DateTime");

            dtAppLaunches.Columns.Add(colDeviceID);
            dtAppLaunches.Columns.Add(colStartTime);
            dtAppLaunches.Columns.Add(colEndTime);
            dtAppLaunches.Columns.Add(colLaunchCount);
            dtAppLaunches.Columns.Add(colFeatureUsed);
            dtAppLaunches.Columns.Add(colFeatureStartTimeStamp);
            dtAppLaunches.Columns.Add(colFeatureEndTimeStamp);


            foreach (AppLaunch launch in AppLaunches)
            {
                foreach (Feature feature in launch.Features)
                {
                    if (!string.IsNullOrEmpty(DeviceId))
                    {
                        DataRow row = dtAppLaunches.NewRow();
                        row["AnalyticsDeviceID"] = AnalyticsDeviceID;
                        row["StartTime"] = launch.startTime;
                        row["EndTime"] = launch.endTime;
                        row["LaunchCount"] = AppLaunches.Count;
                        row["FeatureUsed"] = feature.Name;
                        row["FeatureStartTimeStamp"] = feature.StartTimestamp;
                        row["FeatureEndTimeStamp"] = feature.EndTimestamp;

                        dtAppLaunches.Rows.Add(row);
                    }
                }
            }

            return dtAppLaunches;
        }
    }

    public class AppLaunch
    {
        public DateTime startTime { get; set; }
        public DateTime endTime { get; set; }
        public IList<Feature> Features { get; set; }
    }

    public class Feature
    {
        public string Name { get; set; }
        public DateTime StartTimestamp { get; set; }
        public DateTime EndTimestamp { get; set; }
    }
}