﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using YarraTrams.Model;
using System.Data.SqlClient;
using System.Configuration;
using System.Threading;
using YarraTrams.Library.Mailer;

namespace YarraTrams.PIDSDAL
{
    public class DalHelper
    {

        public static bool CheckRoute(short routeId)
        {
            bool routeExists = false;

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var x = ctx.CheckRouteNumber(routeId).ToList();
                if (x.Count() != 0)
                {
                    routeExists = true;
                }
            }
            return routeExists;
        }

        public static bool CheckUpStop(short stopId)
        {
            bool isUpStop = false;

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var x = ctx.CheckUpStop(stopId).ToList();
                if (x.Count() != 0)
                {
                    isUpStop = true;
                }
            }
            return isUpStop;
        }

        public static bool RouteHasTTDMSMessage(int routeNo)
        {
            bool hasDisruption = false;

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var x = ctx.GetTTMDisruptionsForRoute(routeNo).ToList();
                if (x.Count() != 0)
                {
                    hasDisruption = true;
                }
            }
            return hasDisruption;
        }

        /*
        public static bool CheckDeviceToken(Guid deviceToken)
        {
            bool tokenExists = false;

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var x = ctx.CheckDeviceToken(deviceToken).ToList();
                if (x.Count() != 0)
                {
                    tokenExists = true;
                }
            }
            return tokenExists;
        }
        */

        public static bool CheckStop(short stopNo)
        {
            bool stopExists = false;  

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var x = ctx.CheckStopNumberWithLastStop(stopNo).Single();
                if (x.Valid == true)
                {
                    stopExists = true;
                }

            }
            return stopExists;
        }

        public static bool CheckAllowsAnalytics(int analyticsDeviceID)
        {
            bool allowsAnalytics = false;

            using (tramTRACKERDWDataContext ctx = new tramTRACKERDWDataContext())
            {
                var x = ctx.CheckAllowsAnalytics(analyticsDeviceID).Single();
                if (x.AllowsAnalytics == true)
                {
                    allowsAnalytics = true;
                }
            }

            return allowsAnalytics;
        }

        public static void LogPIDSServiceRequest(PIDSRequest request)
        {
            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                ctx.LogPIDSServiceRequest(request.ReqDateTime, request.IPAddr, request.GUID, request.Type, request.Version,
                    request.WSVersion, request.MethodName, request.TramNo, request.StopNo, request.RouteNo, request.LowFloor, request.Status, request.DateSince, request.TripID);
            }
        }

        private static IList<RouteTranslationsInfo> GetRouteTranslations()
        {
            IList<RouteTranslationsInfo> results = new List<RouteTranslationsInfo>();

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var x = ctx.GetRouteNoTranslations();
                foreach (GetRouteNoTranslationsResult r in x)
                {
                    RouteTranslationsInfo rt = new RouteTranslationsInfo();
                    rt.AlphanumericRouteNo = r.AlphanumericRouteNo;
                    rt.InternalRouteNo = r.InternalRouteNo;
                    rt.VariantDestination = r.VariantDestination;
                    results.Add(rt);
                }
            }
            return results;
        }

        public static bool CheckAuthToken(string authToken)
        {
            bool tokenExists = false;
            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var x = ctx.CheckAuthToken(authToken).FirstOrDefault();
                if (x != null)
                {
                    tokenExists = true;
                }
            }
            return tokenExists;
        }

        public static IList<AddDeviceTokenInfo> AddDeviceToken(string authToken, string deviceInfo)
        {
            IList<AddDeviceTokenInfo> results = new List<AddDeviceTokenInfo>();

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var x = ctx.AddDeviceToken(authToken, deviceInfo);
                foreach (AddDeviceTokenResult r in x)
                {
                    AddDeviceTokenInfo ad = new AddDeviceTokenInfo();
                    ad.DeviceToken = r.DeviceID;
                    results.Add(ad);
                }
            }
            return results;
        }

        public static int SetAnalyticsDevice(string deviceId, bool allowsAnalytics, string ttClientVersion, string osVersion)
        {
            using (tramTRACKERDWDataContext ctx = new tramTRACKERDWDataContext())
            {
                return Convert.ToInt32(ctx.SetDeviceForAnalytics(deviceId, allowsAnalytics, ttClientVersion, osVersion).FirstOrDefault().AnalyticsDeviceID);
            }
        }

        public static void AddFeatureUsage(int analyticsDeviceId, DateTime startTime, DateTime endTime, int launchCount,
            string featureUsed, DateTime featureStartTimeStamp, DateTime featureEndTimeStamp)
        {
            using(tramTRACKERDWDataContext ctx = new tramTRACKERDWDataContext())
            {
                ctx.AddFeatureUsage(analyticsDeviceId, startTime, endTime, launchCount, featureUsed, featureStartTimeStamp, featureEndTimeStamp);
            }
        }

        public static void AddFeatureUsage(DataTable dtFeatureUsage)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TramTrackerDWConnectionString"].ToString()))
            {
                SqlBulkCopy bulkCopy = new SqlBulkCopy(connection);

                // set the destination table name
                bulkCopy.DestinationTableName = "Features";
                connection.Open();
                // write the data in the "dataTable"
                bulkCopy.WriteToServer(dtFeatureUsage);
                connection.Close();
            }
        }

        public static IList<DestinationsForRouteInfo> GetDestinationsForRoute(short routeId)
        {
            IList<DestinationsForRouteInfo> results = new List<DestinationsForRouteInfo>();
            IList<RouteTranslationsInfo> routeTranslations = GetRouteTranslations();

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var x = ctx.GetDestinationsByRoute(routeId);
                
                foreach (GetDestinationsByRouteResult r in x)
                {
                    DestinationsForRouteInfo dr = new DestinationsForRouteInfo();
                    IList<RouteTranslationsInfo> routeTranslation = routeTranslations.Where(rt => rt.InternalRouteNo == routeId).ToList();
                    dr.UpDestination = (routeTranslation.Count() > 0 && routeTranslation.FirstOrDefault().VariantDestination != null) ? r.UpDestination + " " + routeTranslation.FirstOrDefault().VariantDestination : r.UpDestination;
                    dr.DownDestination = (routeTranslation.Count() > 0 && routeTranslation.FirstOrDefault().VariantDestination != null) ? r.DownDestination + " " + routeTranslation.FirstOrDefault().VariantDestination : r.DownDestination;
                    results.Add(dr);
                }
            }

            return results;
        }

        public static IList<DestinationsForAllRoutesInfo> GetDestinationsForAllRoutes()
        {
            IList<DestinationsForAllRoutesInfo> results =  new List<DestinationsForAllRoutesInfo>();
            IList<RouteTranslationsInfo> routeTranslations = GetRouteTranslations();

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var x = ctx.GetDestinationsForAllRoutes();

                foreach (GetDestinationsForAllRoutesResult r in x)
                {
                    DestinationsForAllRoutesInfo dr = new DestinationsForAllRoutesInfo();
                    IList<RouteTranslationsInfo> alphaNumericRoute = routeTranslations.Where(rt => rt.InternalRouteNo == r.RouteNo).ToList();
                    dr.IsUpStop = r.UpStop;
                    dr.Name = (alphaNumericRoute.Count() > 0 && alphaNumericRoute.FirstOrDefault().VariantDestination != null) ? r.Destination + " " + alphaNumericRoute.FirstOrDefault().VariantDestination : r.Destination;
                    dr.RouteNumber = ( alphaNumericRoute.Count() > 0 ) ? alphaNumericRoute.FirstOrDefault().AlphanumericRouteNo : r.RouteNo.ToString();
                    results.Add(dr);
                }
            }
            return results;
        }

        public static IList<ListOfStopsByRouteNoAndDirectionInfo> GetListOfStopsByRouteNoAndDirection(short routeNo, bool isUpDirection)
        {
            IList<ListOfStopsByRouteNoAndDirectionInfo> results = new List<ListOfStopsByRouteNoAndDirectionInfo>();

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var x = ctx.GetStopsByRouteAndDirectionWithLastStop(routeNo, isUpDirection);

                foreach (GetStopsByRouteAndDirectionWithLastStopResult r in x)
                {
                    ListOfStopsByRouteNoAndDirectionInfo sl = new ListOfStopsByRouteNoAndDirectionInfo();
                    sl.Description = r.Description;
                    sl.Latitude = Convert.ToDecimal(r.Latitude);
                    sl.Longitude = Convert.ToDecimal(r.Longitude);
                    sl.Name = r.Name;
                    sl.SuburbName = r.SuburbName;
                    sl.StopNo = r.TID;
                    sl.TurnMessage = r.TurnMessage;
                    sl.TurnType = r.TurnType;
                    results.Add(sl);
                }
            }
            return results;
        }

        public static IList<PlatformStopInfo> GetPlatformStopsByRouteAndDirection(short routeNo, bool isUpDirection)
        {
            IList<PlatformStopInfo> results = new List<PlatformStopInfo>();

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var x = ctx.GetPlatformStopsByRouteNumberAndDirection(routeNo, isUpDirection);

                    foreach (GetPlatformStopsByRouteNumberAndDirectionResult r in x)
                    {
                        PlatformStopInfo ps = new PlatformStopInfo();
                        ps.Description = r.Description;
                        ps.StopNo = r.StopNo;
                        ps.FlagStopNo = r.FlagStopNo;
                        ps.StopName = r.StopName;
                        ps.StopSequence = r.StopSequence;
                        results.Add(ps);
                    }
            }
            return results;
        }

        public static IList<StopInformationInfo> GetStopInformation(short stopNo)
        {
            IList<StopInformationInfo> results = new List<StopInformationInfo>();

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var x = ctx.GetStopInformationAndMetaData(stopNo);

                foreach (GetStopInformationAndMetaDataResult r in x)
                {
                    StopInformationInfo si = new StopInformationInfo();
                    si.CityDirection = r.CityDirection;
                    si.FlagStopNo = r.FlagStopNo;
                    si.HasConnectingBuses = r.HasConnectingBuses;
                    si.HasConnectingTrains = r.HasConnectingTrains;
                    si.HasConnectingTrams = r.HasConnectingTrams;
                    si.IsCityStop = r.IsCityStop;
                    si.IsPlatformStop = r.IsPlatformStop;
                    si.Latitude = Convert.ToDecimal(r.Latitude);
                    si.Longitude = Convert.ToDecimal(r.Longitude);
                    si.StopLength = r.StopLength;
                    si.StopName = r.StopName;
                    si.IsEasyAccess = r.IsEasyAccess;
                    si.ConnectingTrains = r.ConnectingTrains;
                    si.ConnectingBus = r.ConnectingBus;
                    si.ConnectingTrams = r.ConnectingTrams;
                    si.IsShelter = r.IsShelter;
                    si.IsYTShelter = r.IsYTShelter;
                    si.Zones = r.Zones;
                    results.Add(si);
                }
            }
            return results;
        }

        public static IList<MainRoutesForStopInfo> GetMainRoutesForStop(short stopNo, bool lowFloor, DayOfWeek? dayOfWeek)
        {
            IList<MainRoutesForStopInfo> results = new List<MainRoutesForStopInfo>();
            byte? dayOfWeekInByte = null;
            IList<RouteTranslationsInfo> routeTranslations = GetRouteTranslations();

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                if (dayOfWeek.HasValue)
					dayOfWeekInByte = Convert.ToByte(dayOfWeek);

                var x = ctx.GetListOfMainRoutesAtStop(stopNo, lowFloor, dayOfWeekInByte);

                foreach (GetListOfMainRoutesAtStopResult r in x)
                {
                    MainRoutesForStopInfo mri = new MainRoutesForStopInfo();
                    IList<RouteTranslationsInfo> alphaNumericRoute = routeTranslations.Where(rt => rt.InternalRouteNo == r.RouteNo).ToList();
                    mri.RouteNo = (alphaNumericRoute.Count() > 0) ? alphaNumericRoute.FirstOrDefault().AlphanumericRouteNo : r.RouteNo.ToString();
                    results.Add(mri);
                }
            }
           return results;
        }

        public static IList<RouteStopsByRouteInfo> GetRouteStopsByRoute(string routeNo)
        {
            IList<RouteStopsByRouteInfo> results = new List<RouteStopsByRouteInfo>();
            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var x = ctx.GetStopsInformationByRoute(Convert.ToInt32(routeNo));

                foreach (GetStopsInformationByRouteResult r in x)
                {
                    RouteStopsByRouteInfo rsi = new RouteStopsByRouteInfo();
                    rsi.Description = r.Description;
                    rsi.HasConnectingBuses = r.HasConnectingBuses;
                    rsi.HasConnectingTrains = r.HasConnectingTrains;
                    rsi.HasConnectingTrams = r.HasConnectingTrams;
                    rsi.IsCityStop = r.IsCityStop;
                    rsi.IsPlatformStop = r.IsPlatformStop;
                    rsi.Latitude = Convert.ToDecimal(r.Latitude);
                    rsi.Longitude = Convert.ToDecimal(r.Longitude);
                    rsi.StopLength = r.StopLength;
                    rsi.StopName = r.StopName;
                    rsi.StopSequence = r.StopSequence;
                    rsi.SuburbName = r.SuburbName;
                    rsi.StopNo = r.TID;
                    rsi.TurnMessage = r.TurnMessage;
                    rsi.TurnType = r.TurnType;
                    rsi.UpStop = r.UpStop;
                    results.Add(rsi);
                }
            }
            return results;
        }

        public static IList<RouteSummariesInfo> GetRouteSummaries()
        {
            IList<RouteSummariesInfo> results = new List<RouteSummariesInfo>();
            IList<RouteTranslationsInfo> routeTranslations = GetRouteTranslations();

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var x = ctx.GetListOfRouteSummaries();

                foreach (GetListOfRouteSummariesResult r in x)
                {
                    RouteSummariesInfo rs = new RouteSummariesInfo();
                    rs.AlphaNumericRouteNo = r.AlphaNumericRouteNo;
                    rs.Description = r.Description;
                    rs.DownDestination = r.DownDestination;
                    rs.HeadBoardRouteNo = r.HeadboardRouteNo;
                    rs.InternalRouteNo = r.InternalRouteNo;
                    rs.RouteColour = r.Colour;
                    rs.IsMainRoute = Convert.ToBoolean(r.IsMainRoute);
                    rs.LastModified = r.LastModified;
                    rs.MainRouteNo = r.MainRouteNo;
                    IList<RouteTranslationsInfo> alphaNumericRoute = routeTranslations.Where(rt => rt.InternalRouteNo == r.RouteNo).ToList();
                    rs.RouteNo = (alphaNumericRoute.Count() > 0) ? alphaNumericRoute.FirstOrDefault().AlphanumericRouteNo : r.RouteNo.ToString();
                    rs.UpDestination = r.UpDestination;
                    rs.VariantDestination = r.VariantDestination;
                    results.Add(rs);
                }
            }
            return results;
        }

        public static IList<TicketOutletsInfo> GetTicketOutlets()
        {
            IList<TicketOutletsInfo> results = new List<TicketOutletsInfo>();

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var x = ctx.GetTicketOutlets();
                foreach (GetTicketOutletsResult r in x)
                {
                    TicketOutletsInfo to = new TicketOutletsInfo();
                    to.TicketOutletId = r.ID;
                    to.Name = r.Name;
                    to.Address = r.Address;
                    to.Suburb = r.Suburb;
                    to.Is24Hour = Convert.ToBoolean(r._24Hour);
                    to.HasMetcard = Convert.ToBoolean(r.Metcard);
                    to.HasMyki = Convert.ToBoolean(r.Myki);
                    to.HasMykiTopUp = Convert.ToBoolean(r.HasMykiTopUp);
                    to.Latitude = Convert.ToDecimal(r.Latitude);
                    to.Longitude = Convert.ToDecimal(r.Longitude);
                    to.DateAdded = r.DateAdded;
                    to.DateModified = Convert.ToDateTime(r.DateModified);
                    results.Add(to);
                }
            }
            return results;
        }

        public static IList<TicketOutletByIdInfo> GetTicketOutletById(int ticketOutletId)
        {
            IList<TicketOutletByIdInfo> results = new List<TicketOutletByIdInfo>();

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var x = ctx.GetTicketOutletById(ticketOutletId);
                foreach (GetTicketOutletByIdResult r in x)
                {
                    TicketOutletByIdInfo to = new TicketOutletByIdInfo();
                    to.Name = r.Name;
                    to.Address = r.Address;
                    to.Suburb = r.Suburb;
                    to.Is24Hour = Convert.ToBoolean(r._24hour);
                    to.HasMetcard = Convert.ToBoolean(r.Metcard);
                    to.HasMyki = Convert.ToBoolean(r.Myki);
                    to.HasMykiTopUp = Convert.ToBoolean(r.HasMykiTopUp);
                    to.Latitude = Convert.ToDecimal(r.Latitude);
                    to.Longitude = Convert.ToDecimal(r.Longitude);
                    to.DateAdded = r.DateAdded;
                    to.DateModified = Convert.ToDateTime(r.DateModified);
                    results.Add(to);
                }
            }
            return results;
        }

        public static IList<TicketOutletSinceInfo> GetTicketOutletChangesSince(DateTime sinceDate, DateTime beforeDate)
        {
            IList<TicketOutletSinceInfo> results = new List<TicketOutletSinceInfo>();

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var x = ctx.GetTicketOutletChangesSince(sinceDate, beforeDate);
                foreach (GetTicketOutletChangesSinceResult r in x)
                {
                    TicketOutletSinceInfo ts = new TicketOutletSinceInfo();
                    ts.ID = r.ID;
                    ts.Action = r.Action;
                    results.Add(ts);
                }
            }
            return results;
        }

        public static IList<PointsOfInterestInfo> GetPointsOfInterest()
        {
            IList<PointsOfInterestInfo> results = new List<PointsOfInterestInfo>();

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var x = ctx.GetListOfPointsOfInterest();
                foreach (GetListOfPointsOfInterestResult r in x)
                {
                    PointsOfInterestInfo pi = new PointsOfInterestInfo();
                    pi.CategoryName = r.CategoryName;
                    pi.DisabledAccess = r.DisabledAccess.GetValueOrDefault();
                    pi.EmailAddress = r.EmailAddress;
                    pi.HasEntryFee = r.EntryFee.GetValueOrDefault();
                    pi.HasToilets = r.HasToilets.GetValueOrDefault();
                    pi.Latitude = Convert.ToDecimal(r.Latitude);
                    pi.Longitude = Convert.ToDecimal(r.Longitude);
                    pi.Name = r.Name;
                    pi.OpeningHours = r.OpeningHours;
                    pi.PhoneNumber = r.PhoneNumber;
                    pi.POIDescription = r.POIDescription;
                    pi.MoreInfo = r.MoreInfo;
                    pi.POIId = r.POIId;
                    pi.Postcode = r.Postcode;
                    pi.StreetAddress = r.StreetAddress;
                    pi.Suburb = r.Suburb;
                    pi.WebAddress = r.WebAddress;
                    results.Add(pi);
                }
            }
            return results;
        }

        public static IList<PointsOfInterestInfo> GetPointsOfInterestByStopNo(int stopNo)
        {
            IList<PointsOfInterestInfo> results = new List<PointsOfInterestInfo>();

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var x = ctx.GetPointOfInterestByStopNo(stopNo);
                foreach (GetPointOfInterestByStopNoResult r in x)
                {
                    PointsOfInterestInfo pi = new PointsOfInterestInfo();
                    pi.CategoryName = r.CategoryName;
                    pi.DisabledAccess = r.DisabledAccess.GetValueOrDefault();
                    pi.EmailAddress = r.EmailAddress;
                    pi.HasEntryFee = r.EntryFee.GetValueOrDefault();
                    pi.HasToilets = r.HasToilets.GetValueOrDefault();
                    pi.Latitude = Convert.ToDecimal(r.Latitude);
                    pi.Longitude = Convert.ToDecimal(r.Longitude);
                    pi.Name = r.Name;
                    pi.OpeningHours = r.OpeningHours;
                    pi.PhoneNumber = r.PhoneNumber;
                    pi.POIDescription = r.POIDescription;
                    pi.MoreInfo = r.MoreInfo;
                    pi.POIId = r.POIId;
                    pi.Postcode = r.Postcode;
                    pi.StreetAddress = r.StreetAddress;
                    pi.Suburb = r.Suburb;
                    pi.WebAddress = r.WebAddress;
                    results.Add(pi);
                }
            }
            return results;
        }

        public static IList<PointOfInterestByIDInfo> GetPointOfInterestById(int poiid)
        {
            IList<PointOfInterestByIDInfo> results = new List<PointOfInterestByIDInfo>();

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var x = ctx.GetPointOfInterestById(poiid);
   
                foreach (GetPointOfInterestByIdResult r in x)
                {
                    PointOfInterestByIDInfo pi = new PointOfInterestByIDInfo();
                    pi.CategoryName = r.CategoryName;
                    pi.DisabledAccess = r.DisabledAccess.GetValueOrDefault();
                    pi.EmailAddress = r.EmailAddress;
                    pi.HasEntryFee = r.EntryFee.GetValueOrDefault();
                    pi.HasToilets = r.HasToilets.GetValueOrDefault();
                    pi.Latitude = Convert.ToDecimal(r.Latitude);
                    pi.Longitude = Convert.ToDecimal(r.Longitude);
                    pi.Name = r.Name;
                    pi.OpeningHours = r.OpeningHours;
                    pi.PhoneNumber = r.PhoneNumber;
                    pi.POIDescription = r.POIDescription;
                    pi.MoreInfo = r.MoreInfo;
                    pi.Postcode = r.Postcode;
                    pi.StreetAddress = r.StreetAddress;
                    pi.Suburb = r.Suburb;
                    pi.WebAddress = r.WebAddress;
                    results.Add(pi);
                }
            }
            return results;
        }

        public static IList<ListOfStopsByPOIIdInfo> GetListOfStopsByPOIId(int poiId)
        {
            IList<ListOfStopsByPOIIdInfo> results = new List<ListOfStopsByPOIIdInfo>();

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var x = ctx.GetListOfStopsByPOIId(poiId);
                foreach (GetListOfStopsByPOIIdResult r in x)
                {
                    ListOfStopsByPOIIdInfo s = new ListOfStopsByPOIIdInfo();
                    s.StopNo = r.StopNo;
                    results.Add(s);
                }
            }
            return results;
        }

        public static IList<PointsOfInterestChangesSinceInfo> GetPointsOfInterestChangesSince(DateTime sinceDate, DateTime beforeDate)
        {
            IList<PointsOfInterestChangesSinceInfo> results = new List<PointsOfInterestChangesSinceInfo>();

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var x = ctx.GetPointOfInterestChangesSince(sinceDate, beforeDate);
                foreach (GetPointOfInterestChangesSinceResult r in x)
                {
                    PointsOfInterestChangesSinceInfo pi = new PointsOfInterestChangesSinceInfo();
                    pi.POIId = r.POIId;
                    pi.Action = r.Action;
                    results.Add(pi);
                }
            }
            return results;
        }

        public static DisruptionMessageInfo GetRouteDisruptionMessageByRouteNo(short routeNo, ClientType clientType)
        {
            DisruptionMessageInfo msg = new DisruptionMessageInfo { TTAvailable = true, AdditionalInfoOnWebsite = false, Route = routeNo.ToString(), Message = "Route " + routeNo + " is disrupted. Delays may occur" };

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var result = ctx.GetActiveRouteDisruptionMessage(routeNo).FirstOrDefault();
                if (result != null)
                {
                    msg = new DisruptionMessageInfo();
                    msg.TTAvailable = true;
                    msg.Route = routeNo.ToString();
                    msg.AdditionalInfoOnWebsite = result.UnaffectedAdditionalInfoOnWebSite;
                    msg.MessageId = result.Id;
                    msg.InfoOnWebSiteCategory = System.Configuration.ConfigurationManager.AppSettings.Get("DisruptionInfoOnWebSiteCategory");
                    msg.Message = result.UnaffectedLongMessage;

                    //switch (clientType)
                    //{
                    //    case ClientType.Web:
                    //        msg.Message = result.UnaffectedMediumMessage;
                    //        break;
                    //    case ClientType.SmartPhone:
                    //        msg.Message = result.UnaffectedLongMessage;
                    //        break;
                    //    default:
                    //        msg.Message = result.UnaffectedMediumMessage;
                    //        break;
                    //}
                }
            }
            return msg;
        }

        public static DisruptionMessageInfo GetRouteDisruptionMessageByRouteNoAndDirection(short routeNo, bool isUp, ClientType clientType)
        {
            //DisruptionMessageInfo msg = new DisruptionMessageInfo { TTAvailable = true, AdditionalInfoOnWebsite = false, Route = routeNo.ToString(), Message = "Route " + routeNo + " is disrupted. Delays may occur" };
            DisruptionMessageInfo msg = null;
            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var result = ctx.GetActiveRouteAndDirectionDisruptionMessage(routeNo, isUp).FirstOrDefault();
                if (result != null)
                {
                    msg = new DisruptionMessageInfo();
                    msg.TTAvailable = true;
                    msg.Route = routeNo.ToString();
                    msg.AdditionalInfoOnWebsite = result.UnaffectedAdditionalInfoOnWebSite;
                    msg.MessageId = result.Id;
                    msg.InfoOnWebSiteCategory = System.Configuration.ConfigurationManager.AppSettings.Get("DisruptionInfoOnWebSiteCategory");
                    msg.Message = result.UnaffectedLongMessage;

                }
            }
            return msg;
        }

        public static DisruptionMessageInfo GetRouteDisruptionMessageByStopNoAndRouteNo(short stopNo, short routeNo, ClientType clientType)
        {
            bool isUpStop = DalHelper.CheckUpStop(stopNo);
           // DisruptionMessageInfo msg = new DisruptionMessageInfo {TTAvailable = true, AdditionalInfoOnWebsite = false, Message = "Route " + routeNo + " is disrupted. Delays may occur" };
            DisruptionMessageInfo msg = null;
            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                //there should only ever be one row returned;
                var results = ctx.GetTTMRouteDisruptionMessageByStopNoAndRouteNo(stopNo, routeNo);

                foreach (GetTTMRouteDisruptionMessageByStopNoAndRouteNoResult result in results)
                {
                    bool isAffected = (result.stopIsAffected > 0);
                    msg = new DisruptionMessageInfo();
                    msg.TTAvailable = (result.stopIsAffected == 0 || result.DisplayPredictions == true);
                    msg.Route = routeNo.ToString();
                    msg.AdditionalInfoOnWebsite = (isAffected) ? result.AffectedAdditionalInfoOnWebSite : result.UnaffectedAdditionalInfoOnWebSite;
                    msg.MessageId = result.Id;
                    msg.InfoOnWebSiteCategory = System.Configuration.ConfigurationManager.AppSettings.Get("DisruptionInfoOnWebSiteCategory");
                    msg.Message = (isAffected) ? result.AffectedLongMessage : result.UnaffectedLongMessage;
                    msg.HideMessage = (isUpStop) ? result.HideUpStopMessages: result.HideDownStopMessages;
                }
            }
            return msg;
        }


        public static DisruptionMessageInfo GetTTMRouteCombinationDisruptionMessageByStopNoAndRouteCombo(short stopNo, string routeComboVal, ClientType clientType)
        {
            //DisruptionMessageInfo msg = new DisruptionMessageInfo { TTAvailable = true, AdditionalInfoOnWebsite = false, Message = "Disruptions are occuring. Delays may occur" };
            DisruptionMessageInfo msg = null;

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                //there should only ever be one row returned;
                var results = ctx.GetTTMRouteCombinationDisruptionMessageByStopNoAndRouteCombo(stopNo, routeComboVal);

                foreach (GetTTMRouteCombinationDisruptionMessageByStopNoAndRouteComboResult result in results)
                {
                    bool isAffected = (result.stopIsAffected > 0);
                    msg = new DisruptionMessageInfo();
                    msg.TTAvailable = (result.stopIsAffected == 0 || result.DisplayPredictions == true);
                    msg.Route = "All Routes";
                    msg.AdditionalInfoOnWebsite = (isAffected) ? result.AffectedAdditionalInfoOnWebsite : result.UnaffectedAdditionalInfoOnWebsite;
                    msg.DisplayType = result.AffectedDisplayType;
                    msg.MessageId = result.Id;
                    msg.InfoOnWebSiteCategory = System.Configuration.ConfigurationManager.AppSettings.Get("DisruptionInfoOnWebSiteCategory");
                    msg.Message = (isAffected) ? result.AffectedLongMessage : result.UnaffectedLongMessage;

                    //switch (clientType)
                    //{
                    //    case ClientType.Web:
                    //        msg.Message = (isAffected) ? result.AffectedMediumMessage : result.UnaffectedMediumMessage;
                    //        break;
                    //    case ClientType.SmartPhone:
                    //        msg.Message = (isAffected) ? result.AffectedLongMessage : result.UnaffectedLongMessage;
                    //        break;

                    //    default:
                    //        msg.Message = (isAffected) ? result.AffectedMediumMessage : result.UnaffectedMediumMessage;
                    //        break;
                    //}
                }
            }
            return msg;
        }


        
        public static IList<DisruptionMessageInfo> GetRouteDisruptionMessagesByRouteComboValue(string routeComboValue, bool isAffected, ClientType clientType)
        {
            //DisruptionMessageInfo msg = new DisruptionMessageInfo {TTAvailable = true, AdditionalInfoOnWebsite = false, Message = "Disruptions are occuring .Delays may occur" };

            DisruptionMessageInfo msg = null;

            IList<DisruptionMessageInfo> messages = new List<DisruptionMessageInfo>();

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var results = ctx.GetTTMRouteDisruptionMessagesByRouteComboValue(routeComboValue);

                foreach (GetTTMRouteDisruptionMessagesByRouteComboValueResult result in results)
                {
                    msg = new DisruptionMessageInfo();

                    msg.MessageId = result.Id;
                    msg.InfoOnWebSiteCategory = System.Configuration.ConfigurationManager.AppSettings.Get("DisruptionInfoOnWebSiteCategory");

                    msg.Route = result.RouteNo.ToString();

                    msg.AdditionalInfoOnWebsite = (isAffected) ? result.AffectedAdditionalInfoOnWebSite : result.UnaffectedAdditionalInfoOnWebSite;
                    msg.Message = (isAffected) ? result.AffectedLongMessage : result.UnaffectedLongMessage;
        
                    //switch (clientType)
                    //{
                    //    case ClientType.Web:
                    //        msg.Message = (isAffected) ? result.AffectedMediumMessage : result.UnaffectedMediumMessage;
                    //        break;
                    //    case ClientType.SmartPhone:
                    //        msg.Message = (isAffected) ? result.AffectedLongMessage : result.UnaffectedLongMessage;
                    //        break;
                    //    default:
                    //        msg.Message = (isAffected) ? result.AffectedMediumMessage : result.UnaffectedMediumMessage;
                    //        break;
                    //}
                    messages.Add(msg);
                }
            }
            return messages;
        }

        public static string GetRouteCombinationsForStop(int stopNo)
        {
            string routeComboVal = "";

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var results = ctx.GetRouteCombinationsForStop(stopNo);

                foreach (GetRouteCombinationsForStopResult result in results)
                {
                    routeComboVal = result.RouteCombination;
                }

            }
            return routeComboVal;


        }


        public static DisruptedStopInfo GetTTMAllRouteDisruptionMessageByStopNo(short stopNo, ClientType clientType)
        {
            bool isUpStop = DalHelper.CheckUpStop(stopNo);

            DisruptedStopInfo stop = new DisruptedStopInfo();

            //DisruptionMessageInfo msg = new DisruptionMessageInfo { TTAvailable = true, AdditionalInfoOnWebsite = false, Message = "Disruptions are occuring. Delays may occur" };
            DisruptionMessageInfo msg = null;

            string routeComb = "";
            bool ?isSingleRoute = true;

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                //there should only ever be one row returned;
                
                var results = ctx.GetTTMAllRouteDisruptionMessageByStopNo(stopNo, ref isSingleRoute, ref routeComb);


                foreach (GetTTMAllRouteDisruptionMessageByStopNoResult result in results)
                {   
                    bool isAffected = (result.stopIsAffected > 0);
                    msg = new DisruptionMessageInfo();
                    msg.TTAvailable = (result.stopIsAffected == 0 || result.DisplayPredictions == true);
                    msg.IsAffected = isAffected;
                    msg.Route = "All Routes";
                    msg.AdditionalInfoOnWebsite = (isAffected) ? result.AffectedAdditionalInfoOnWebsite : result.UnaffectedAdditionalInfoOnWebsite;
                    msg.MessageId = result.ID;
                    msg.InfoOnWebSiteCategory = System.Configuration.ConfigurationManager.AppSettings.Get("DisruptionInfoOnWebSiteCategory");
                    msg.HideMessage = (isUpStop) ? result.HideUpStopMessages : result.HideDownStopMessages;

                    if (isAffected)
                    {
                        msg.DisplayType = result.AffectedDisplayType;
                    }
                    else
                    {
                        msg.DisplayType = result.UnaffectedDisplayType;
                    }

                    msg.Message = (isAffected) ? result.AffectedLongMessage : result.UnaffectedLongMessage;

                    //switch (clientType)
                    //{
                    //    case ClientType.Web:
                    //        msg.Message = (isAffected) ? result.AffectedMediumMessage : result.UnaffectedMediumMessage;
                    //        break;
                    //    case ClientType.SmartPhone:   
                    //        msg.Message = (isAffected) ? result.AffectedLongMessage : result.UnaffectedLongMessage;
                    //        break;
                    //    default:
                    //        msg.Message = (isAffected) ? result.AffectedMediumMessage : result.UnaffectedMediumMessage;
                    //        break;
                    //}
                }

            }
            stop.Message = msg;
            stop.IsSingleRoute = Convert.ToBoolean(isSingleRoute);
            stop.RouteCombination = routeComb;

            return stop;
        }

        public static IList<PredictionInfo> GetPredictions(short stopNo, short routeNo, bool isLowFloor)
        {
            IList<PredictionInfo> results = new List<PredictionInfo>();

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var x = ctx.GetPredictionsForStop(stopNo, routeNo, isLowFloor);

                foreach (GetPredictionsForStopResult p in x)
                {
                    PredictionInfo dr = new PredictionInfo();
                    dr.Prediction = (int)p.Prediction;
                    dr.RunNo = p.RunNo;
                    dr.StopID = p.StopID;
                    dr.StopNo = (short)p.StopNo;
                    dr.InternalRouteNo = (short)p.InternalRouteNo;
                    dr.RouteNo = p.RouteNo.ToString();
                    dr.HeadBoardRouteNo = p.HeadboardRouteNo;
                    dr.Destination = p.Destination;
                    dr.StopDistance = (double)p.StopDistance;
                    dr.TramDistance = (double)p.TramDistance;
                    dr.Deviation = (int)p.Deviation;
                    dr.AVMTime = (DateTime)p.AVMTime;
                    dr.VehicleNo = (int)p.VehicleNo;
                    dr.LowFloor = (bool)p.LowFloor;
                    dr.Down = (bool)p.Down;
                    dr.Schedule = (DateTime)p.Schedule;
                    dr.Adjustment = (int)p.Adjustment;
                    dr.DisplayPrediction = (bool)(p.DisplayPrediction == null ? true : p.DisplayPrediction);
                    dr.SpecialEventMessage = (p.SpecialEventMessage == null ? "" : p.SpecialEventMessage);
                    dr.TTDMSMessage = (p.TTDMSMessage == null ? "" : p.TTDMSMessage);
                    dr.DisplayFOCMessage = (bool)(p.DisplayFOCMessage == null ? false : p.DisplayFOCMessage);
                    dr.DisplayAirCondition = (bool)p.DisplayAirCondition;
                    results.Add(dr);
                }
            }
            return results;
        }

        public static IList<NextPredictedRoutesCollectionInfo> GetPredictionsCollection(short stopNo, short routeNo, bool isLowFloor)
        {
            IList<NextPredictedRoutesCollectionInfo> results = new List<NextPredictedRoutesCollectionInfo>();

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                List<GetPredictionsForStopResult> predictionsResult = ctx.GetPredictionsForStop(stopNo, routeNo, isLowFloor).OrderBy(r => r.Prediction).ToList();

                int retry = 0;
                if (predictionsResult.Count == 0 && retry < Convert.ToInt32(ConfigurationManager.AppSettings["GetPredictionsMaxRetry"].ToString()))
                {
                    predictionsResult = ctx.GetPredictionsForStop(stopNo, routeNo, isLowFloor).OrderBy(r => r.Prediction).ToList();
                    Thread.Sleep(Convert.ToInt32(ConfigurationManager.AppSettings["GetPredictionsRetryWait"].ToString()));
                }

                if (predictionsResult.Count() == 0)
                {
                    return results;
                }
                else
                {

                    int routeCount = predictionsResult.Select(a => a.RouteNo).Distinct().Count();

                    int destinationCount = predictionsResult.Where(a => a.InternalRouteNo == a.RouteNo).Select(a => a.Destination).Distinct().Count();

                    if (destinationCount > 2)
                    {

                        foreach (GetPredictionsForStopResult p in predictionsResult)
                        {


                            string mainRouteDestination = predictionsResult.Where(a => a.RouteNo == p.RouteNo).Select(a => a.Destination).FirstOrDefault();
                            if (mainRouteDestination == null || mainRouteDestination == "")
                                mainRouteDestination = p.Destination;
                            int predictonsCount = results.Where(result => result.Destination == mainRouteDestination).Count();

                            //RouteStopsByRouteInfo routeStop = GetRouteStopsByRoute(p.RouteNo.ToString()).Where(rs => rs.StopNo == stopNo).FirstOrDefault();
                            //string mainRouteDestination = "";
                            //List<DestinationsForRouteInfo> destinationsForRoute = GetDestinationsForRoute(p.RouteNo).ToList();
                            //if (destinationsForRoute.Count > 0 && routeStop.UpStop)
                            //{
                            //    mainRouteDestination = destinationsForRoute[0].UpDestination;
                            //}
                            //else if (destinationsForRoute.Count > 0)
                            //{
                            //    mainRouteDestination = destinationsForRoute[0].DownDestination;
                            //}
                            //int predictonsCount = results.Where(result => result.Destination == mainRouteDestination).Count();

                            if (predictonsCount < 3)
                            {
                                if (results.Where(r => r.RouteNo == p.RouteNo.ToString()).Count() < 3)
                                {
                                    NextPredictedRoutesCollectionInfo pr = new NextPredictedRoutesCollectionInfo();
                                    pr.Destination = p.Destination;
                                    pr.AirConditioned = (bool)p.DisplayAirCondition;
                                    pr.DisplayAC = (bool)p.DisplayAirCondition;
                                    pr.HasDisruption = (p.TTDMSMessage != "") ? true : (bool)p.DisplayFOCMessage;
                                    pr.HasSpecialEvent = (p.SpecialEventMessage == "") ? false : (predictionsResult.Count == 1) ? false : true;

                                    pr.DisruptionMessage = new DisruptionMessageListInfo();

                                    pr.DisruptionMessage.DisplayType = "Text";

                                    if (p.TTDMSMessage != "")
                                    {

                                        DisruptionMessageInfo msg = new DisruptionMessageInfo();
                                        msg.InfoOnWebSiteCategory = "Disruption";
                                        msg.Message = p.TTDMSMessage;
                                        msg.Route = (routeNo == 0) ? "All Routes" : routeNo.ToString();
                                        msg.TTAvailable = (bool)p.DisplayPrediction;
                                        pr.DisruptionMessage.Messages.Add(msg);
                                    }
                                    pr.DisruptionMessage.MessageCount = pr.DisruptionMessage.Messages.Count();


                                    pr.HeadBoardRouteNo = p.HeadboardRouteNo;
                                    pr.InternalRouteNo = (short)p.InternalRouteNo;
                                    pr.IsLowFloorTram = (bool)p.LowFloor;
                                    pr.IsTTAvailable = (bool)p.DisplayPrediction;
                                    pr.PredictedArrivalDateTime = ((DateTime)p.Schedule).AddSeconds(Convert.ToDouble(p.Adjustment));
                                    pr.PredictedArrivalDateTime = (pr.PredictedArrivalDateTime > DateTime.Now) ? pr.PredictedArrivalDateTime : DateTime.Now.AddSeconds(10);
                                    pr.RouteNo = p.RouteNo.ToString();
                                    pr.SpecialEventMessage = p.SpecialEventMessage;
                                    pr.TripID = 0;
                                    pr.VehicleNo = (short)p.VehicleNo;

                                    results.Add(pr);
                                }
                            }
                        }
                    }
                    else
                    {
                        List<GetPredictionsForStopResult> preds = predictionsResult.OrderBy(a => ((DateTime)a.Schedule).AddSeconds(Convert.ToDouble(a.Adjustment))).ToList();


                        foreach (GetPredictionsForStopResult p in preds)
                        {

                            string mainRouteDestination = predictionsResult.Where(a => a.RouteNo == p.RouteNo).Select(a => a.Destination).FirstOrDefault();
                            if (mainRouteDestination == null || mainRouteDestination == "")
                                mainRouteDestination = p.Destination;
                            int predictonsCount = results.Where(result => result.Destination == mainRouteDestination).Count();
                            if (predictonsCount < 3)
                            {
                                if (results.Where(result => result.RouteNo == p.RouteNo.ToString()).Count() < 3)
                                {
                                    NextPredictedRoutesCollectionInfo pr = new NextPredictedRoutesCollectionInfo();

                                    pr.Destination = p.Destination;
                                    pr.AirConditioned = (bool)p.DisplayAirCondition;
                                    pr.DisplayAC = (bool)p.DisplayAirCondition;
                                    pr.HasDisruption = (p.TTDMSMessage != "") ? true : (bool)p.DisplayFOCMessage;
                                    pr.DisruptionMessage = new DisruptionMessageListInfo();
                                    pr.HasSpecialEvent = (p.SpecialEventMessage == "") ? false : (predictionsResult.Count == 1) ? false : true;

                                    pr.DisruptionMessage = new DisruptionMessageListInfo();
                                    pr.DisruptionMessage.DisplayType = "Text";

                                    if (p.TTDMSMessage != "")
                                    {
                                        DisruptionMessageInfo msg = new DisruptionMessageInfo();
                                        msg.InfoOnWebSiteCategory = "Disruption";
                                        msg.Message = p.TTDMSMessage;
                                        msg.Route = (routeNo == 0) ? "All Routes" : routeNo.ToString();
                                        msg.TTAvailable = (bool)p.DisplayPrediction;
                                        pr.DisruptionMessage.Messages.Add(msg);
                                    }
                                    pr.DisruptionMessage.MessageCount = pr.DisruptionMessage.Messages.Count();

                                    pr.HeadBoardRouteNo = p.HeadboardRouteNo;
                                    pr.InternalRouteNo = (short)p.InternalRouteNo;
                                    pr.IsLowFloorTram = (bool)p.LowFloor;
                                    pr.IsTTAvailable = (bool)p.DisplayPrediction;
                                    pr.PredictedArrivalDateTime = ((DateTime)p.Schedule).AddSeconds(Convert.ToDouble(p.Adjustment));
                                    pr.PredictedArrivalDateTime = (pr.PredictedArrivalDateTime > DateTime.Now) ? pr.PredictedArrivalDateTime : DateTime.Now.AddSeconds(10);
                                    pr.RouteNo = p.RouteNo.ToString();
                                    pr.SpecialEventMessage = p.SpecialEventMessage;
                                    pr.TripID = 0;
                                    pr.VehicleNo = (short)p.VehicleNo;
                                    results.Add(pr);
                                }
                            }
                            predictonsCount++;
                        }

                    }
                }
            }
            List<string> destinations = results.Select(r => r.Destination).Distinct().ToList();
            
            bool isOrderByDestination = false;
            foreach (string dest in destinations)
            {
                int destRouteNoCount = results.Where(r => r.Destination == dest).Select(r => r.RouteNo).Distinct().Count();
                if (destRouteNoCount > 1)
                    isOrderByDestination = true;
            }

            if (isOrderByDestination)
            {
                return results.OrderBy(r => r.Destination).ThenBy(r => r.PredictedArrivalDateTime).ToList();
            }
            else
                return results.OrderBy(r => Convert.ToInt32(r.RouteNo)).ThenBy(r => r.PredictedArrivalDateTime).ToList();
        }


        public static IList<DisruptedRouteInfo> GetTTMDisruptedRouteMessagesByStopNo(short stopNo, bool isAffected, bool displayPrediction)
        {
            List<DisruptedRouteInfo> disruptedRoutesInfo = new List<DisruptedRouteInfo>();

            bool isUpStop = DalHelper.CheckUpStop(stopNo);

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var x = ctx.GetTTMDisruptedRouteMessagesByStopNo(stopNo);

                List<GetTTMDisruptedRouteMessagesByStopNoResult> disruptedRoutes =  x.ToList();
                foreach (GetTTMDisruptedRouteMessagesByStopNoResult disruptedRoute in disruptedRoutes)
                {
                    DisruptedRouteInfo dri = new DisruptedRouteInfo();
                    dri.RouteNo = disruptedRoute.RouteNo;
                    DisruptionMessageInfo msg = new DisruptionMessageInfo();
                    msg.IsAffected = isAffected;
                    msg.TTAvailable = displayPrediction;
                    msg.Route = disruptedRoute.RouteNo.ToString();;
                    msg.MessageId = disruptedRoute.Id;
                    msg.AdditionalInfoOnWebsite = Convert.ToBoolean((isAffected) ? disruptedRoute.AffectedAdditionalInfoOnWebSite : disruptedRoute.UnaffectedAdditionalInfoOnWebSite);
                    msg.InfoOnWebSiteCategory = System.Configuration.ConfigurationManager.AppSettings.Get("DisruptionInfoOnWebSiteCategory");
                    msg.Message = (isAffected) ? disruptedRoute.AffectedLongMessage : disruptedRoute.UnaffectedLongMessage;
                    msg.HideMessage = (isUpStop) ? disruptedRoute.HideUpStopMessages: disruptedRoute.HideDownStopMessages;
                    dri.Message = msg;
                    disruptedRoutesInfo.Add(dri);
                }
            }

            return disruptedRoutesInfo;
        }

        public static IList<TTMDisruptedStop> GetTTMDisruptedStops()
        {
            List<TTMDisruptedStop> TTMDisruptedStops = new List<TTMDisruptedStop>();

            using (tramTRACKERDataContext ctx = new tramTRACKERDataContext())
            {
                var x = ctx.GetTTMDisruptedStops();

                List<GetTTMDisruptedStopsResult> ttmDisruptedStops = x.ToList();
                foreach(GetTTMDisruptedStopsResult dsr in ttmDisruptedStops)
                {
                    TTMDisruptedStop ds = new TTMDisruptedStop();
                    ds.StopNo = dsr.StopNo;
                    ds.IsUp = dsr.IsUpStop;
                    ds.DisplayPrediction = dsr.DisplayPredictions;

                    TTMDisruptedStops.Add(ds);
                }
            }

            return TTMDisruptedStops;
        }
    }
}
