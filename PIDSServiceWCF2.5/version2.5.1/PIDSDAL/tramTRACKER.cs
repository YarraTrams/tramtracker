using System.Configuration;
namespace YarraTrams.PIDSDAL
{
    public partial class tramTRACKERDataContext : System.Data.Linq.DataContext
    {
        public tramTRACKERDataContext()
            : base(ConfigurationManager.ConnectionStrings["TramTrackerConnectionString"].ToString())
        {
            OnCreated();
        }

    }
}
