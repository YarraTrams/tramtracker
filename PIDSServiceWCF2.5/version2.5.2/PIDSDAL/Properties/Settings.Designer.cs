﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace YarraTrams.PIDSDAL.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "12.0.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=yt-cc-db-pids;Initial Catalog=TTDev;Integrated Security=True")]
        public string TTDevConnectionString {
            get {
                return ((string)(this["TTDevConnectionString"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=yt-sm-db-pids;Initial Catalog=tramTRACKERDW;User ID=ttuser;Password=k" +
            "3c04n4k4l")]
        public string TramTrackerDWConnectionString {
            get {
                return ((string)(this["TramTrackerDWConnectionString"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=yt-cc-db-pids;Initial Catalog=TTAndroidDev;Persist Security Info=True" +
            ";User ID=ttuser;Password=k3c04n4k4l")]
        public string TTProductionConnectionString {
            get {
                return ((string)(this["TTProductionConnectionString"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=YT-CC-DB-PIDS;Initial Catalog=TTStaging;Persist Security Info=True;Us" +
            "er ID=pids")]
        public string TTProductionConnectionString1 {
            get {
                return ((string)(this["TTProductionConnectionString1"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=yt-cc-db-pids;Initial Catalog=TTDev;Persist Security Info=True;User I" +
            "D=ttuser;Password=k3c04n4k4l")]
        public string TTDevConnectionString1 {
            get {
                return ((string)(this["TTDevConnectionString1"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=yt-cc-db-pids;Initial Catalog=TTProduction;Integrated Security=True")]
        public string TTProductionConnectionString2 {
            get {
                return ((string)(this["TTProductionConnectionString2"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=yt-cc-db-pids;Initial Catalog=TTProduction;Persist Security Info=True" +
            ";User ID=ttuser")]
        public string TTProductionConnectionString3 {
            get {
                return ((string)(this["TTProductionConnectionString3"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=yt-cc-db-pids;Initial Catalog=TTProduction;Persist Security Info=True" +
            ";User ID=ttuser;Password=k3c04n4k4l")]
        public string TTProductionConnectionString4 {
            get {
                return ((string)(this["TTProductionConnectionString4"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=yt-cc-db-pids;Initial Catalog=TTAndroidDev;Persist Security Info=True" +
            ";User ID=ttuser;Password=k3c04n4k4l")]
        public string TTAndroidDevConnectionString {
            get {
                return ((string)(this["TTAndroidDevConnectionString"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=yt-cc-db-pids;Initial Catalog=TTAndroidDev;Integrated Security=True")]
        public string TTAndroidDevConnectionString2 {
            get {
                return ((string)(this["TTAndroidDevConnectionString2"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=dev-ttr-sql-01;Initial Catalog=tramTRACKER;Integrated Security=True")]
        public string tramTRACKERConnectionString {
            get {
                return ((string)(this["tramTRACKERConnectionString"]));
            }
        }
    }
}
