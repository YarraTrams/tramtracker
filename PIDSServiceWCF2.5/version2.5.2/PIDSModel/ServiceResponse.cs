﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YarraTrams.Model
{
    [DataContract]
    public class ServiceResponse
    {

        public ServiceResponse() { }
        public ServiceResponse(string operationName, bool applyInputValidation)
        {
            this.webMethodCalled = operationName;
            this.timeRequested = DateTime.Now;
            this.ValidateInputs = applyInputValidation;
        }


        [DataMember()]
        public object responseObject{get;set;}
        [DataMember()]
        public bool hasError { get; set; }
        [DataMember()]
        public bool hasResponse { get; set; }
        [DataMember()]
        public string errorMessage { get; set; }
        [DataMember()]
        public string webMethodCalled { get; set; }

        [DataMember()]
        public DateTime timeRequested { get; set; }
        [DataMember()]
        public DateTime timeResponded { get; set; }
        [IgnoreDataMember]
        public bool ValidateInputs { get; set; }
    }
}
