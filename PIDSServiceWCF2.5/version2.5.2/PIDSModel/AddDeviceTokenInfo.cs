﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class AddDeviceTokenInfo
    {
        public AddDeviceTokenInfo() { }

        [DataMember()]
        public Guid? DeviceToken { get; set; }

    }
}
