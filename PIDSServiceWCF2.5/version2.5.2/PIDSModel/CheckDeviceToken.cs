﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class CheckDeviceTokenInfo
    {
        public CheckDeviceTokenInfo() { }

        [DataMember()]
        public Guid DeviceId { get; set; }

    }
}
