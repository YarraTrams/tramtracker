﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace YarraTrams.Model
{

    [Serializable]
    [DataContract(Namespace = "")]
    public class NetworkMapInfo
    {
        public NetworkMapInfo()
        {

        }
        [DataMember()]
        public string FileName { get; set; }
        [DataMember()]
        public string Url { get; set; }
        [DataMember()]
        public DateTime ActiveDate { get; set; }
    }
}
