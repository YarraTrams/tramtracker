﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class TTMDisruptedStop
    {
        [DataMember()]
        public short StopNo { get; set; }

        [DataMember()]
        public bool IsUp { get; set; }

        [DataMember()]
        public bool DisplayPrediction { get; set; }

    }
}
