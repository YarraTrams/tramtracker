﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class StopChangeInfo
    {
        public StopChangeInfo() { }

        [DataMember()]
        public short StopNo { get; set; }

        [DataMember()]
        public string Action { get; set; }
    }
}