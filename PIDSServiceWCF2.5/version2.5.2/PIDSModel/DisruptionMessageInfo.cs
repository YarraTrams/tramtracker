﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class DisruptionMessageInfo
    {

        [DataMember()]
        public string Route { get; set; }

        [DataMember()]
        public string Message { get; set; }

        [DataMember()]
        public bool AdditionalInfoOnWebsite { get; set; }

        [DataMember()]
        public int MessageId { get; set; }

        [DataMember()]
        public string InfoOnWebSiteCategory { get; set; }

        [IgnoreDataMember]
        public int DisplayType { get; set; }

        [IgnoreDataMember]
        public bool TTAvailable { get; set; }

        [IgnoreDataMember]
        public bool IsAffected { get; set; }

        [IgnoreDataMember]
        public string RouteStr { get; set; }

        [IgnoreDataMember]
        public bool HideMessage { get; set; }

    }
}
