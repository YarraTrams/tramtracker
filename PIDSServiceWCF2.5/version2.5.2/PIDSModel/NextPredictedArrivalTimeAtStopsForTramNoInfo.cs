﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class NextPredictedArrivalTimeAtStopsForTramNoInfo
    {
        public NextPredictedArrivalTimeAtStopsForTramNoInfo() { NextPredictedStopsDetails = new List<NextPredictedStopsDetailsTableInfo>(); }

        [DataMember()]
        public short VehicleNo { get; set; }

        [DataMember()]
        public bool AtLayover { get; set; }

        [DataMember()]
        public bool Available { get; set; }

        [DataMember()]
        public string RouteNo { get; set; }

        [DataMember()]
        public string HeadBoardRouteNo { get; set; }

        [DataMember()]
        public bool Up { get; set; }

        [DataMember()]
        public bool HasSpecialEvent { get; set; }

        [DataMember()]
        public string SpecialEventMessage { get; set; }

        [DataMember()]
        public bool HasDisruption { get; set; }

        [DataMember()]
        public DisruptionMessageInfo DisruptionMessage { get; set; }

        [DataMember()]
        public List<NextPredictedStopsDetailsTableInfo> NextPredictedStopsDetails { get; set; }
    }
}