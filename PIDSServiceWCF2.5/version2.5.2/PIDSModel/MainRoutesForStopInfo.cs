﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class MainRoutesForStopInfo
    {
        public MainRoutesForStopInfo() { }

        [DataMember()]
        public string RouteNo { get; set; }
    }
}