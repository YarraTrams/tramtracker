﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class StopInformationInfo
    {
        [DataMember()]
        public string FlagStopNo { get; set; }

        [DataMember()]
        public string StopName { get; set; }

        [DataMember()]
        public string CityDirection { get; set; }

        [DataMember()]
        public decimal Latitude { get; set; }

        [DataMember()]
        public decimal Longitude { get; set; }

        [DataMember()]
        public bool IsCityStop { get; set; }

        [DataMember()]
        public bool IsPlatformStop { get; set; }

        [DataMember()]
        public bool HasConnectingBuses { get; set; }

        [DataMember()]
        public bool HasConnectingTrains { get; set; }

        [DataMember()]
        public bool HasConnectingTrams { get; set; }

        [DataMember()]
        public short StopLength { get; set; }

        [DataMember()]
        public bool? IsEasyAccess { get; set; }

        [DataMember()]
        public string ConnectingTrains { get; set; }

        [DataMember()]
        public string ConnectingBus { get; set; }

        [DataMember()]
        public string ConnectingTrams { get; set; }

        [DataMember()]
        public bool? IsShelter { get; set; }

        [DataMember()]
        public bool? IsYTShelter { get; set; }

        [DataMember()]
        public bool IsInFreeZone { get; set; }

        [DataMember()]
        public string Zones { get; set; }
    }
}