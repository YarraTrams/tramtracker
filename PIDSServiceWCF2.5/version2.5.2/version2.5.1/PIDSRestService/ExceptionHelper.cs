﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace YarraTrams.PIDSRestService
{
    public static class ExceptionHelper
    {
        public static string GetExceptionString(Exception e, bool debugEnabled)
        {
            string exceptionString = "";
            string fullException = e.GetType().FullName;

            if (e.GetType().FullName == "System.Data.SqlClient.SqlException" && debugEnabled == false)
            {
                exceptionString = ConfigurationManager.AppSettings["SQLErrorMessage"];            
            }
            else if (e.GetType().FullName == "System.Data.SqlClient.SqlException" && debugEnabled == true)
            {
                exceptionString = e.Message + "\n " + e.StackTrace;
            }
            else if (e.GetType().FullName == "System.Net.WebException" && debugEnabled == false)
            {
                exceptionString = ConfigurationManager.AppSettings["WebServiceExceptionError"];
            }
            else if (e.GetType().FullName == "System.Net.WebException" && debugEnabled == true)
            {
                exceptionString = e.Message + "\n " + e.StackTrace;
            }
            else if (debugEnabled == false)
            {
                exceptionString = ConfigurationManager.AppSettings["GeneralErrorMessage"];
            }
            else if (debugEnabled == true)
            {
                exceptionString = e.Message + "\n " + e.StackTrace;
            }
            return exceptionString;
        }
    }
}