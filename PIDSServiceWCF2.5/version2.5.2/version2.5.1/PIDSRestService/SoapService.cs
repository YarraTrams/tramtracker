﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YarraTrams.PIDSRestService.pidsservicecore;

namespace YarraTrams.PIDSRestService
{
    public static class SoapService
    {
        private static object sync = new object();
        private static tramTRACKERPIDSWebServiceCore _Instance;

        public static tramTRACKERPIDSWebServiceCore Instance
        {
            get
            {
                if (_Instance == null)
                    lock (sync)
                        if (_Instance == null)
                            _Instance = new tramTRACKERPIDSWebServiceCore();
                return _Instance;
            }
        }


    }
}