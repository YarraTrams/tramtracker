﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class RouteSummariesInfo
    {

        [DataMember()]
        public string RouteNo { get; set; }

        [DataMember()]
        public short HeadBoardRouteNo { get; set; }

        [DataMember()]
        public short InternalRouteNo { get; set; }

        [DataMember()]
        public string AlphaNumericRouteNo { get; set; }

        [DataMember()]
        public string RouteColour { get; set; }

        [DataMember()]
        public string VariantDestination { get; set; }

        [DataMember()]
        public bool IsMainRoute { get; set; }
        
        [DataMember()]
        public string MainRouteNo { get; set; }

        [DataMember()]
        public string Description { get; set; }

        [DataMember()]
        public string UpDestination { get; set; }

        [DataMember()]
        public string DownDestination { get; set; }

        [DataMember()]
        public DateTime LastModified { get; set; }
    }
}