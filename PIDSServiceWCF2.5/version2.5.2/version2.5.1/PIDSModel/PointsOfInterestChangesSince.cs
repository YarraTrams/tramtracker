﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class PointsOfInterestChangesSinceInfo
    {
        public PointsOfInterestChangesSinceInfo() { }

        [DataMember()]
        public int POIId { get; set; }

        [DataMember()]
        public string Action { get; set; }

    }
}
