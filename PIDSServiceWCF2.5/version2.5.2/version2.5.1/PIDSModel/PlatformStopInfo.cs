﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class PlatformStopInfo
    {

        [DataMember()]
        public string Description { get; set; }

        [DataMember()]
        public short StopNo { get; set; }

        [DataMember()]
        public byte StopSequence { get; set; }

        [DataMember()]
        public string FlagStopNo { get; set; }

        [DataMember()]
        public string StopName { get; set; }
    }
}
