﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class ListOfRouteAvailableInfo
    {
        public ListOfRouteAvailableInfo() { }

        [DataMember()]
        public string RouteNo { get; set; }
        [DataMember()]
        public bool RouteAvailable { get; set; }
    }
}