﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YarraTrams.Model
{
    public class PIDSRequest
    {
        public DateTime ReqDateTime { get; set; }
        public string IPAddr { get; set; }
        public string GUID { get; set; }
        public string Type { get; set; }
        public string Version { get; set; }
        public string WSVersion { get; set; }
        public string MethodName { get; set; }
        public string TramNo { get; set; }
        public string StopNo { get; set; }
        public string RouteNo { get; set; }
        public string LowFloor { get; set; }
        public string Status { get; set; }
        public string DateSince { get; set; }
        public string TripID { get; set; }
    }
}
