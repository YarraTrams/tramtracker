﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class SchedulesForTripInfo
    {

        [DataMember()]
        public short StopNo { get; set; }

        [DataMember()]
        public int Time { get; set; }

        [DataMember()]
        public DateTime ScheduledArrivalDateTime { get; set; }
    }
}