﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class PointsOfInterestInfo
    {
        public PointsOfInterestInfo() { }

        [DataMember()]
        public int POIId { get; set; }

        [DataMember()]
        public string Name { get; set; }

        [DataMember()]
        public string POIDescription { get; set; }

        [DataMember()]
        public string MoreInfo { get; set; }

        [DataMember()]
        public string PhoneNumber { get; set; }

        [DataMember()]
        public string WebAddress { get; set; }

        [DataMember()]
        public string EmailAddress { get; set; }

        [DataMember()]
        public bool? DisabledAccess { get; set; }

        [DataMember()]
        public bool? HasToilets { get; set; }

        [DataMember()]
        public string OpeningHours { get; set; }

        [DataMember()]
        public bool? HasEntryFee { get; set; }

        [DataMember()]
        public string StreetAddress { get; set; }

        [DataMember()]
        public string Suburb { get; set; }

        [DataMember()]
        public int? Postcode { get; set; }

        [DataMember()]
        public decimal Latitude { get; set; }

        [DataMember()]
        public decimal Longitude { get; set; }

        [DataMember()]
        public string CategoryName { get; set; }
    }
}