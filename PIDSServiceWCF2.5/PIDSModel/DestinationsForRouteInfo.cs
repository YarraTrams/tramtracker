﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class DestinationsForRouteInfo
    {
        public DestinationsForRouteInfo() { }

        [DataMember()]
        public string UpDestination { get; set; }
        [DataMember()]
        public string DownDestination { get; set; }

    }
}
