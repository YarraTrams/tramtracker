﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Runtime.Serialization;


namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class PredictionInfo
    {
        public PredictionInfo() { }

        [DataMember()]
        public int Prediction { get; set; }

        [DataMember()]
        public string RunNo { get; set; }

        [DataMember()]
        public string StopID { get; set; }

        [DataMember()]
        public int StopNo { get; set; }

        [DataMember()]
        public short InternalRouteNo { get; set; }

        [DataMember()]
        public string RouteNo { get; set; }

        [DataMember()]
        public string HeadBoardRouteNo { get; set; }

        [DataMember()]
        public string Destination { get; set; }

        [DataMember()]
        public double StopDistance { get; set; }

        [DataMember()]
        public double TramDistance { get; set; }

        [DataMember()]
        public int Deviation { get; set; }

        [DataMember()]
        public DateTime AVMTime { get; set; }

        [DataMember()]
        public int VehicleNo { get; set; }

        [DataMember()]
        public bool LowFloor { get; set; }

        [DataMember()]
        public bool Down { get; set; }

        [DataMember()]
        public DateTime Schedule { get; set; }

        [DataMember()]
        public int Adjustment { get; set; }

        [DataMember()]
        public bool DisplayPrediction { get; set; }

        [DataMember()]
        public string SpecialEventMessage { get; set; }

        [DataMember()]
        public string TTDMSMessage { get; set; }

        [DataMember()]
        public bool DisplayFOCMessage { get; set; }

        [DataMember()]
        public bool DisplayAirCondition { get; set; }
    }
}
