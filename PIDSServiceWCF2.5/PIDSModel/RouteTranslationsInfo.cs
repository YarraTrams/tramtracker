﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace YarraTrams.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class RouteTranslationsInfo
    {
        public RouteTranslationsInfo() { }

        [DataMember()]
        public int InternalRouteNo { get; set; }

        [DataMember()]
        public string AlphanumericRouteNo { get; set; }

        [DataMember()]
        public string VariantDestination { get; set; }
    }
}
