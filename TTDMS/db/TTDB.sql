/*This script will create the new TTDMS database
WARNING: This script will clear all TTDMS data from the database when it is run on so use with caution!!!

NOTES: This script should be run on the TTDMS database. 
To function the TTDMS Database must access the database containing FOC disruption data.
Make sure that you do a search and replace on TTDev.dbo to replace it with the identifier for the FOC database.

*/

/*-------------- TABLES ---------------*/


DROP TABLE [dbo].[ActiveDisruptedCombinationRouteMessages]

GO

CREATE TABLE [dbo].[ActiveDisruptedCombinationRouteMessages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RouteNoCombination] [nvarchar](256) NOT NULL,
	[AffectedShortMessage] [nvarchar](128) NULL,
	[AffectedLongMessage] [nvarchar](512) NULL,
	[UnaffectedShortMessage] [nvarchar](128) NULL,
	[UnaffectedLongMessage] [nvarchar](512) NULL,
	[AffectedAdditionalInfoOnWebsite] [bit] NOT NULL,
	[UnaffectedAdditionalInfoOnWebSite] [bit] NOT NULL,
	[AffectedDisplayType] [smallint] NOT NULL,
	[UnaffectedDisplayType] [smallint] NOT NULL,
	[CreatedBy] [nvarchar](128) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[HideDownStopMessages] [bit] NOT NULL DEFAULT 0,
	[HideUpStopMessages] [bit] NOT NULL  DEFAULT 0,
 CONSTRAINT [PK_ActiveDisruptedCombinationRouteMessages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

DROP TABLE [dbo].[ActiveDisruptedStops]

GO

CREATE TABLE [dbo].[ActiveDisruptedStops](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[RouteNo] [smallint] NOT NULL,
	[StopNo] [smallint] NOT NULL,
	[IsUpStop] [bit] NOT NULL,
	[CreatedBy] [nvarchar](128) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[DisplayPredictions] [bit] NOT NULL,
 CONSTRAINT [PK_ActiveDisruptedStops] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ActiveDisruptedStops] ADD  CONSTRAINT [DispPredictionsDefault]  DEFAULT ((1)) FOR [DisplayPredictions]
GO

GO

DROP TABLE [dbo].[ActiveDisruptedRouteMessages]

GO

CREATE TABLE [dbo].[ActiveDisruptedRouteMessages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RouteNo] [smallint] NOT NULL,
	[AffectedShortMessage] [nvarchar](128) NULL,
	[AffectedLongMessage] [nvarchar](512) NULL,
	[UnaffectedShortMessage] [nvarchar](128) NULL,
	[UnaffectedLongMessage] [nvarchar](512) NULL,
	[AffectedAdditionalInfoOnWebSite] [bit] NULL,
	[UnaffectedAdditionalInfoOnWebSite] [bit] NOT NULL,
	[CreatedBy] [nvarchar](128) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[HideDownStopMessages] [bit] NOT NULL DEFAULT 0,
	[HideUpStopMessages] [bit] NOT NULL  DEFAULT 0,
 CONSTRAINT [PK_ActiveDisruptedRouteMessages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

DROP TABLE [dbo].[ActiveDisruptedRoutes]

GO

CREATE TABLE [dbo].[ActiveDisruptedRoutes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RouteNo] [smallint] NOT NULL,
	[CreatedBy] [nvarchar](128) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_ActiveDisruptedRoutes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

DROP TABLE [dbo].[DisruptedCombinationRouteMessages]

GO

CREATE TABLE [dbo].[DisruptedCombinationRouteMessages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RouteNoCombination] [nvarchar](256) NOT NULL,
	[AffectedShortMessage] [nvarchar](128) NULL,
	--[AffectedMediumMessage] [nvarchar](256) NULL,
	[AffectedLongMessage] [nvarchar](512) NULL,
	[UnaffectedShortMessage] [nvarchar](128) NULL,
	--[UnaffectedMediumMessage] [nvarchar](256) NULL,
	[UnaffectedLongMessage] [nvarchar](512) NULL,
	[AffectedAdditionalInfoOnWebsite] [bit] NOT NULL,
	[UnaffectedAdditionalInfoOnWebSite] [bit] NULL,
	[AffectedDisplayType] [smallint] NOT NULL,
	[UnaffectedDisplayType] [smallint] NULL,
	[LastUpdatedDisruptionId] [int] NULL,
	[UpdatedBy] [nvarchar](128) NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
	[HideDownStopMessages] [bit] NOT NULL DEFAULT 0,
	[HideUpStopMessages] [bit] NOT NULL  DEFAULT 0,
 CONSTRAINT [PK_DisruptedCombinationRouteMessages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

DROP TABLE [dbo].[DisruptedRouteMapping]

GO

CREATE TABLE [dbo].[DisruptedRouteMapping](
	[RouteNo] [int] NULL,
	[MessageID] [int] NULL
) ON [PRIMARY]

GO

DROP TABLE [dbo].[DisruptedRouteMessages]

GO

CREATE TABLE [dbo].[DisruptedRouteMessages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RouteNo] [smallint] NOT NULL,
	[AffectedShortMessage] [nvarchar](128) NULL,
	--[AffectedMediumMessage] [nvarchar](256) NULL,
	[AffectedLongMessage] [nvarchar](512) NULL,
	[UnaffectedShortMessage] [nvarchar](128) NULL,
	--[UnaffectedMediumMessage] [nvarchar](256) NULL,
	[UnaffectedLongMessage] [nvarchar](512) NULL,
	[AffectedAdditionalInfoOnWebSite] [bit] NOT NULL,
	[UnaffectedAdditionalInfoOnWebSite] [bit] NULL,
	[LastUpdatedDisruptionId] [int] NULL,
	[UpdatedBy] [nvarchar](128) NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,	
	[HideDownStopMessages] [bit] NOT NULL DEFAULT 0,
	[HideUpStopMessages] [bit] NOT NULL  DEFAULT 0,
 CONSTRAINT [PK_DisruptedRouteMessages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

DROP TABLE [dbo].[DisruptedRoutes]

GO
CREATE TABLE [dbo].[DisruptedRoutes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DisruptionId] [int] NOT NULL,
	[RouteNo] [smallint] NOT NULL,
	[CreatedBy] [nvarchar](128) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_DisruptedRoutes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

DROP TABLE [dbo].[DisruptedStops]

GO

CREATE TABLE [dbo].[DisruptedStops](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DisruptionId] [int] NOT NULL,
	[RouteNo] [smallint] NOT NULL,
	[StopNo] [smallint] NOT NULL,
	[IsUpStop] [bit] NOT NULL,
	[CreatedBy] [nvarchar](128) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[DisplayPredictions] [bit] NULL,
 CONSTRAINT [PK_DisruptedStops] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

DROP TABLE [dbo].[Disruptions]

GO

CREATE TABLE [dbo].[Disruptions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](1024) NOT NULL,
	[Description] [nvarchar](1024) NOT NULL,
	[TT_DisruptionId] [int],
	[StatusId] [int] NOT NULL,
	[PrimaryDisruptedRouteNo] [smallint] NOT NULL,
	[StartDateTime] [datetime] NULL,
	[ClearedDateTime] [datetime] NULL,
	[ClearedBy] [nvarchar](128) NULL,
	[CreatedBy] [nvarchar](128) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[PublishedDateTime] [datetime] NULL,
	[PublishedBy] [nvarchar](128) NULL,
	[MessageTTL] [int] NULL DEFAULT 0,
	[ExpiryTime] [datetime] NULL,
	[Valid] [bit] NULL DEFAULT 0,
	[UpdatedBy] [nvarchar](128) NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Disruptions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

DROP TABLE [dbo].[DisruptionsActivityLog]

GO

CREATE TABLE [dbo].[DisruptionsActivityLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DisruptionId] [int] NULL,
	[RouteNo] [int] NULL,
	[StopNo] [int] NULL,
	[Activity] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](128) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_DisruptionsActivityLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

DROP TABLE [dbo].[DisruptionStatus]

GO

CREATE TABLE [dbo].[DisruptionStatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_DisruptionStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

DROP TABLE [dbo].[ArchivedDisruptedCombinationRouteMessages]

GO

CREATE TABLE [dbo].[ArchivedDisruptedCombinationRouteMessages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RouteNoCombination] [nvarchar](256) NOT NULL,
	[AffectedShortMessage] [nvarchar](128) NULL,
	[AffectedLongMessage] [nvarchar](512) NULL,
	[UnaffectedShortMessage] [nvarchar](128) NULL,
	[UnaffectedLongMessage] [nvarchar](512) NULL,
	[AffectedAdditionalInfoOnWebsite] [bit] NOT NULL,
	[UnaffectedAdditionalInfoOnWebSite] [bit] NOT NULL,
	[AffectedDisplayType] [smallint] NOT NULL,
	[UnaffectedDisplayType] [smallint] NOT NULL,
	[CreatedBy] [nvarchar](128) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_ArchivedDisruptedCombinationRouteMessages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

DROP TABLE [dbo].[ArchivedDisruptedRouteCombinationStops]

GO

CREATE TABLE [dbo].[ArchivedDisruptedRouteCombinationStops](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RouteCombination] [varchar](255) NOT NULL,
	[StopNo] [int] NOT NULL,
	[MessageID] [int] NOT NULL,
	[IsDisrupted] [bit] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
 CONSTRAINT [PK_ArchivedDisruptedRouteCombinationStops] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

DROP TABLE [dbo].[ArchivedDisruptedRouteMessages]

GO

CREATE TABLE [dbo].[ArchivedDisruptedRouteMessages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RouteNo] [smallint] NOT NULL,
	[AffectedShortMessage] [nvarchar](128) NOT NULL,
	--[AffectedMediumMessage] [nvarchar](256) NOT NULL,
	[AffectedLongMessage] [nvarchar](512) NOT NULL,
	[UnaffectedShortMessage] [nvarchar](128) NOT NULL,
	--[UnaffectedMediumMessage] [nvarchar](256) NOT NULL,
	[UnaffectedLongMessage] [nvarchar](512) NOT NULL,
	[AffectedAdditionalInfoOnWebSite] [bit] NOT NULL,
	[UnaffectedAdditionalInfoOnWebSite] [bit] NOT NULL,
	[CreatedBy] [nvarchar](128) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_ArchivedDisruptedRouteMessages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

DROP TABLE [dbo].[ArchivedDisruptedRouteStops]

GO

CREATE TABLE [dbo].[ArchivedDisruptedRouteStops](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RouteNo] [int] NOT NULL,
	[StopNo] [int] NOT NULL,
	[MessageID] [int] NOT NULL,
	[IsDisrupted] [bit] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[DisplayPredictions] [bit] NOT NULL,
 CONSTRAINT [PK_ArchivedDisruptedRouteStops] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ArchivedDisruptedRouteStops] ADD  CONSTRAINT [ArchivedDispPredictionsDefault]  DEFAULT ((1)) FOR [DisplayPredictions]
GO

/*-------------- END TABLES ---------------*/

/*-------------------- INDEXES -------------------*/
CREATE NONCLUSTERED INDEX Disrupted_Stops_StopNo_Index
    ON dbo.DisruptedStops (StopNo)

GO


/*DROP INDEX StopToRouteCombinationMapping_StopNo_Index
ON dbo.StopToRouteCombinationMapping

GO

CREATE NONCLUSTERED INDEX StopToRouteCombinationMapping_StopNo_Index
    ON dbo.StopToRouteCombinationMapping (StopNo)

GO*/

/*-------------------- END INDEXES -------------------*/

/*-------------- VIEWS ---------------*/


DROP VIEW RouteStops

GO

CREATE VIEW RouteStops AS
SELECT * FROM TTDev.dbo.T_Routes_Stops

GO

DROP VIEW Stops

GO

CREATE VIEW Stops AS
SELECT * FROM TTDev.dbo.T_Stops

GO

DROP VIEW RouteLookUps

GO

CREATE VIEW RouteLookUps AS
SELECT * FROM TTDev.dbo.T_RouteLookUps

GO

DROP VIEW Routes

GO

CREATE VIEW Routes AS
SELECT * FROM TTDev.dbo.T_Routes

GO

GO

DROP VIEW FOC_Disruptions 

GO

CREATE VIEW FOC_Disruptions AS
SELECT ds.Id,ds.Name,ds.Description,ds.TT_DisruptionId,ds.StatusId,ds.PrimaryDisruptedRouteNo,tr.Description as PrimaryDisruptedRouteName,ds.StartDateTime, ds.ClearedDateTime,--foc.DisruptionCleared as ClearedDateTime,
	   ds.CreatedBy, ds.CreatedDate, ds.UpdatedBy,ds.UpdatedDate
	   FROM Disruptions ds

	   INNER JOIN TTDev.dbo.T_Routes tr ON
	   tr.RouteNo = ds.PrimaryDisruptedRouteNo
	   --INNER JOIN TTDev.dbo.T_Disruptions foc ON
	   --foc.DisruptionId = ds.TT_DisruptionId
GO

DROP VIEW FOC_Disruptions_Full

GO

CREATE VIEW FOC_Disruptions_Full AS
SELECT TD.DisruptionID,TD.DisruptionDateTime, TD.DisruptionCleared, TD.NormalServiceResumed,
TD.UpFromStopNo,TD.UpToStopNo,TD.DownFromStopNo,TD.DownToStopNo, TDM.Message,FOC.RouteNo,
upfromst.Description AS UpFromStopName,uptost.Description AS UpToStopName,
upfromdir.Direction AS UpFromStopDirection, uptodir.Direction AS UpToStopDirection,
downfromst.Description AS DownFromStopName,downtost.Description AS DownToStopName,
downfromdir.Direction AS DownFromStopDirection, downtodir.Direction AS DownToStopDirection

FROM TTDev.dbo.T_Disruptions TD

INNER JOIN TTDev.dbo.T_DisruptionMessages TDM ON 
TDM.DisruptionID = TD.DisruptionID

INNER JOIN TTDev.dbo.FOCDisruptedRoutes FOC ON 
FOC.DisruptionID = TD.DisruptionID

LEFT JOIN TTDev.dbo.T_Stops upfromst ON  
upfromst.StopNo = TD.UpFromStopNo

LEFT JOIN TTDev.dbo.T_Stops uptost ON  
uptost.StopNo = TD.UpToStopNo

LEFT JOIN TTDev.dbo.T_DirectionLookUps upfromdir  ON
upfromdir.DirectionId = upfromst.DirectionID

LEFT JOIN TTDev.dbo.T_DirectionLookUps uptodir  ON
uptodir.DirectionId = uptost.DirectionID

LEFT JOIN TTDev.dbo.T_Stops downfromst ON  
downfromst.StopNo = TD.DownFromStopNo

LEFT JOIN TTDev.dbo.T_Stops downtost ON  
downtost.StopNo = TD.DownToStopNo

LEFT JOIN TTDev.dbo.T_DirectionLookUps downfromdir  ON
downfromdir.DirectionId = downfromst.DirectionID

LEFT JOIN TTDev.dbo.T_DirectionLookUps downtodir  ON
downtodir.DirectionId = downtost.DirectionID

GO

--these may need to be removed and the tables referenced in the DB moved into here
--just waiting on the response from Matt or Brendan so I have quickly created views so 
--I can continue working
DROP VIEW RouteCombinations

GO

CREATE VIEW RouteCombinations AS
SELECT * FROM TTDev.dbo.RouteCombinations

GO
/*DROP TABLE [dbo].[RouteCombinations]

GO

CREATE TABLE [dbo].[RouteCombinations](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[RouteCombination] [nvarchar](255) NOT NULL,
	[Colour] [nvarchar](50) NULL,
 CONSTRAINT [PK_RouteCombinations] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]*/

GO

DROP VIEW StopToRouteCombinationMapping

GO

CREATE VIEW [dbo].[StopToRouteCombinationMapping] AS
SELECT * FROM TTDev.dbo.StopToRouteCombinationMapping

GO

/*DROP TABLE [dbo].[StopToRouteCombinationMapping]

GO

CREATE TABLE [dbo].[StopToRouteCombinationMapping](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[StopNo] [int] NOT NULL,
	[RouteCombinationID] [int] NOT NULL,
 CONSTRAINT [PK_StopToRouteCombinationMapping] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]*/

GO

DROP VIEW [dbo].[DirectionLookUps]

GO

CREATE VIEW [dbo].[DirectionLookUps]
AS
SELECT * FROM TTDev.dbo.T_DirectionLookUps

GO


DROP VIEW [dbo].[StopRouteDirectionMaps]

GO

CREATE VIEW [dbo].[StopRouteDirectionMaps] AS
SELECT s.StopNo, rs.RouteNo, rs.UpStop from Stops s INNER JOIN RouteStops rs ON s.StopId = rs.StopId

GO

DROP VIEW [dbo].[StopRouteCombinationDirectionMaps]

GO

CREATE VIEW [dbo].[StopRouteCombinationDirectionMaps] AS
SELECT sm.StopNo, rc.RouteCombination, m.UpStop FROM [dbo].[StopToRouteCombinationMapping] sm 
INNER JOIN [dbo].RouteCombinations rc ON sm.RouteCombinationId = rc.Id
INNER JOIN [dbo].StopRouteDirectionMaps m ON m.StopNo = sm.StopNo

GO


/*-------------- END VIEWS ---------------*/

/*-------------------- FUNCTIONS -------------------*/

DROP FUNCTION [dbo].[udf_GetAffectedRoutesList]

GO

CREATE FUNCTION [dbo].[udf_GetAffectedRoutesList]()
RETURNS VARCHAR(1000) AS

BEGIN
   DECLARE @RouteList varchar(1000)

   SELECT @RouteList = COALESCE(@RouteList + ', ', '') + CAST(dr.RouteNo AS VARCHAR)
   FROM DisruptedRoutes dr

   RETURN @RouteList
END

GO


DROP FUNCTION [dbo].[udf_GetAffectedRoutesListForDisruption]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[udf_GetAffectedRoutesListForDisruption](@DisruptionId int)
RETURNS VARCHAR(1000) AS

BEGIN
   DECLARE @RouteList varchar(1000)

   SELECT @RouteList = COALESCE(@RouteList + ', ', '') + CAST(dr.RouteNo AS VARCHAR)
   FROM DisruptedRoutes dr
   WHERE dr.DisruptionId = @DisruptionId

   RETURN @RouteList
END
GO

DROP FUNCTION dbo.Split

GO

CREATE FUNCTION dbo.Split
(
    @RowData nvarchar(2000),
    @SplitOn nvarchar(5)
)  
RETURNS @RtnValue table 
(
    Id int identity(1,1),
    Data nvarchar(100)
) 
AS  
BEGIN 
    Declare @Cnt int
    Set @Cnt = 1

    While (Charindex(@SplitOn,@RowData)>0)
    Begin
        Insert Into @RtnValue (data)
        Select 
            Data = ltrim(rtrim(Substring(@RowData,1,Charindex(@SplitOn,@RowData)-1)))

        Set @RowData = Substring(@RowData,Charindex(@SplitOn,@RowData)+1,len(@RowData))
        Set @Cnt = @Cnt + 1
    End

    Insert Into @RtnValue (data)
    Select Data = ltrim(rtrim(@RowData))

    Return
END

GO


DROP FUNCTION [dbo].[udf_CreateIntTableFromList]

GO

CREATE FUNCTION [dbo].[udf_CreateIntTableFromList] (@list nvarchar(MAX))
   RETURNS @tbl TABLE (number int NOT NULL) AS
BEGIN
   DECLARE @pos        int,
           @nextpos    int,
           @valuelen   int

   SELECT @pos = 0, @nextpos = 1

   WHILE @nextpos > 0
   BEGIN
      SELECT @nextpos = charindex(',', @list, @pos + 1)
      SELECT @valuelen = CASE WHEN @nextpos > 0
                              THEN @nextpos
                              ELSE len(@list) + 1
                         END - @pos - 1
      INSERT @tbl (number)
         VALUES (convert(int, substring(@list, @pos + 1, @valuelen)))
      SELECT @pos = @nextpos
   END
  RETURN
END

GO


/*-------------------- END FUNCTIONS -------------------*/


/*-------------------- STORED PROCS -------------------*/

DROP PROCEDURE [dbo].[UpdateTTMUnaffectedCombinationRouteDisruptionMessages] 

GO

CREATE PROCEDURE [dbo].[UpdateTTMUnaffectedCombinationRouteDisruptionMessages] 

@Id int,
@UnaffectedShortMessage nvarchar(128),
@UnaffectedLongMessage nvarchar(512),
@UnaffectedAdditionalInfoOnWebsite bit,
@UnaffectedDisplayType int,
@UpdatedBy  nvarchar(128),
@LastUpdatedDisruptionId int,
@HideDownStopMessages bit,
@HideUpStopMessages bit

AS
BEGIN
DECLARE @rowsAffected int

UPDATE DisruptedCombinationRouteMessages
SET 
	UnaffectedShortMessage = @UnaffectedShortMessage,
	UnaffectedLongMessage = @UnaffectedLongMessage,
	UnaffectedAdditionalInfoOnWebsite = @UnaffectedAdditionalInfoOnWebsite,
	UnaffectedDisplayType = @UnaffectedDisplayType,
	LastUpdatedDisruptionId = @LastUpdatedDisruptionId,
	UpdatedBy = @UpdatedBy,
	UpdatedDate = GETDATE(),
	HideDownStopMessages = @HideDownStopMessages,
	HideUpStopMessages = @HideUpStopMessages

WHERE Id = @Id

INSERT INTO DisruptionsActivityLog(DisruptionId,RouteNo ,StopNo ,Activity ,CreatedBy ,CreatedDate)
VALUES  ( NULL ,NULL,NULL, 'Updated Unaffected CombinationRouteDisruptionMessage Id : ' + CONVERT(VARCHAR(8),@Id) ,@UpdatedBy,GETDATE())

SELECT @rowsAffected = @@Rowcount
SELECT @rowsAffected

END

GO

DROP PROCEDURE [dbo].[GetTTMRouteCombinationDisruptionMessageByStopNoAndRouteCombo]

GO

CREATE PROCEDURE [dbo].[GetTTMRouteCombinationDisruptionMessageByStopNoAndRouteCombo]
	@StopNo smallint,
	@RouteComboVal varchar(256)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @stopIsAffected int
	DECLARE @DisplayPredictions bit


	SET @stopIsAffected = (Select Count(Id) FROM ActiveDisruptedStops where StopNo = @StopNo)
	SET @DisplayPredictions = (SELECT DisplayPredictions FROM ActiveDisruptedStops WHERE StopNo = @StopNo)

	IF @DisplayPredictions IS NULL
			SET @DisplayPredictions = 0

	SELECT Id,RouteNoCombination,RouteNo=0, AffectedShortMessage,AffectedLongMessage,UnaffectedShortMessage,UnaffectedLongMessage,AffectedAdditionalInfoOnWebsite,UnaffectedAdditionalInfoOnWebsite,AffectedDisplayType,UnaffectedDisplayType,@DisplayPredictions as DisplayPredictions,@stopIsAffected as stopIsAffected
	FROM ActiveDisruptedCombinationRouteMessages WHERE RouteNoCombination = @RouteComboVal
    
END

GO

DROP PROCEDURE [dbo].[GetTTMAllRouteDisruptionMessageByStopNo]

GO

CREATE PROCEDURE [dbo].[GetTTMAllRouteDisruptionMessageByStopNo]
	-- Add the parameters for the stored procedure here

	@StopNo smallint,
	@IsSingleRoute bit OUTPUT,
	@RteComboList varchar(128) OUTPUT


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @stopIsAffected int
	DECLARE @RouteCombinationId int
	DECLARE @RouteCombinationIdList varchar(500)
	DECLARE @RouteNo smallint
	DECLARE @DisplayPredictions bit

	DECLARE @RouteCount int
	
	SELECT @RouteCount = COUNT(*) FROM RouteStops RS INNER JOIN Stops S
	ON RS.StopID = S.StopID
	INNER JOIN Routes R ON RS.RouteNo = R.RouteNo
	INNER JOIN RouteLookUps RL ON RS.RouteNo = RL.InternalRouteNo
	WHERE StopNo = @StopNo AND RouteAvailable = 1 AND IsMainRoute = 1
	
	IF @RouteCount > 1
	BEGIN
		SET @IsSingleRoute = 0
		
		SET @stopIsAffected = (Select Count(Id) FROM ActiveDisruptedStops where StopNo = @StopNo)

		SET @DisplayPredictions = (SELECT DisplayPredictions FROM ActiveDisruptedStops WHERE StopNo = @StopNo)

		IF @DisplayPredictions IS NULL
			SET @DisplayPredictions = 0

		SET @RouteCombinationId = (Select RouteCombinationId from StopToRouteCombinationMapping where StopNo = @StopNo)
		SET @RouteCombinationIdList = (SELECT RouteCombination FROM RouteCombinations WHERE Id = @RouteCombinationId)

		SET @RteComboList = @RouteCombinationIdList

		SELECT 
		Id AS ID, 
		AffectedShortMessage AS AffectedShortMessage,		
		AffectedLongMessage,
		UnaffectedShortMessage,		
		UnaffectedLongMessage,
		AffectedAdditionalInfoOnWebsite,
		UnaffectedAdditionalInfoOnWebsite,
		AffectedDisplayType,
		UnaffectedDisplayType,
		@stopIsAffected as stopIsAffected,
		@DisplayPredictions AS DisplayPredictions
		FROM ActiveDisruptedCombinationRouteMessages WHERE RouteNoCombination = @RouteCombinationIdList

	END
	
	ELSE
	BEGIN
		SET @IsSingleRoute = 1
		SELECT @RouteNo = RS.RouteNo FROM RouteStops RS INNER JOIN Stops S
		ON RS.StopID = S.StopID
		INNER JOIN Routes R ON RS.RouteNo = R.RouteNo
		INNER JOIN RouteLookUps RL ON RS.RouteNo = RL.InternalRouteNo
		WHERE StopNo = @StopNo AND RouteAvailable = 1 AND IsMainRoute = 1
		SET @stopIsAffected = (Select Count(Id) FROM ActiveDisruptedStops where StopNo = @StopNo)
		SET @DisplayPredictions = (SELECT DisplayPredictions FROM ActiveDisruptedStops WHERE StopNo = @StopNo)
		
		IF @DisplayPredictions IS NULL
			SET @DisplayPredictions = 0

		SET @RouteCombinationIdList = NULL

		DECLARE @AffectedDisplayType SMALLINT = 1
		DECLARE @UnaffectedDisplayType SMALLINT = 1
		SELECT 
		Id AS ID, 
		AffectedShortMessage AS AffectedShortMessage,
		AffectedLongMessage AS AffectedLongMessage,
		UnaffectedShortMessage AS UnaffectedShortMessage,
		UnaffectedLongMessage AS UnaffectedLongMessage,
		AffectedAdditionalInfoOnWebsite AS AffectedAdditionalInfoOnWebsite,
		UnaffectedAdditionalInfoOnWebsite AS UnaffectedAdditionalInfoOnWebsite,
		@AffectedDisplayType as AffectedDisplayType,
		@UnaffectedDisplayType as UnaffectedDisplayType,
		@stopIsAffected as stopIsAffected,
		@DisplayPredictions AS DisplayPredictions
		FROM ActiveDisruptedRouteMessages WHERE RouteNo = @RouteNo
		
	END
END

GO

DROP PROCEDURE [dbo].[GetTTMRouteDisruptionMessageByStopNoAndRouteNo]

GO

CREATE PROCEDURE [dbo].[GetTTMRouteDisruptionMessageByStopNoAndRouteNo]
	-- Add the parameters for the stored procedure here

	
	@StopNo smallint,
	@RouteNo smallint


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @stopIsAffected int
	DECLARE @DisplayPredictions bit

	SET @stopIsAffected = (Select Count(Id) FROM ActiveDisruptedStops where StopNo = @StopNo)
	SET @DisplayPredictions = (SELECT DisplayPredictions FROM ActiveDisruptedStops WHERE StopNo = @StopNo)

	IF @DisplayPredictions IS NULL
			SET @DisplayPredictions = 0

	SELECT Id,RouteNo,AffectedShortMessage,AffectedLongMessage,UnaffectedShortMessage,UnaffectedLongMessage,AffectedAdditionalInfoOnWebSite,UnaffectedAdditionalInfoOnWebSite, @DisplayPredictions as DisplayPredictions, @stopIsAffected as stopIsAffected
	FROM ActiveDisruptedRouteMessages

	WHERE RouteNo = @RouteNo

END

GO


DROP PROCEDURE [dbo].[AddActivityLog] 

GO

CREATE PROCEDURE [dbo].[AddActivityLog] 

@DisruptionId int,
@Type [nvarchar](128),
@Message [text],
@CreatedBy [nvarchar](128)

AS
BEGIN
INSERT INTO ActivityLogs(DisruptionId,[Type] ,[Message] ,CreatedBy ,CreatedDate)
VALUES  ( @DisruptionId ,@Type,@Message ,@CreatedBy,GETDATE())

SELECT CAST(SCOPE_IDENTITY() as int) as Id

END

GO

DROP PROCEDURE [dbo].[GetActivityLogs] 

GO

CREATE PROCEDURE [dbo].[GetActivityLogs] 

@DisruptionId int,
@Type [nvarchar](128)

AS
BEGIN
SELECT * FROM ActivityLogs WHERE DisruptionId = @DisruptionId AND [Type] = @Type

END

GO



DROP PROCEDURE [dbo].[GetTTMDisruptionsByStatus]

GO

CREATE PROCEDURE [dbo].[GetTTMDisruptionsByStatus]
@StatusId int
AS
SELECT Distinct Id,[Name],Description,TT_DisruptionId,StatusId,PrimaryDisruptedRouteNo,dbo.udf_GetAffectedRoutesListForDisruption(id) as AffectedRouteList,StartDateTime,ClearedDateTime,CreatedBy, CreatedDate, UpdatedBy,UpdatedDate
FROM Disruptions
WHERE StatusId = @StatusId
ORDER BY CreatedDate desc

GO

DROP PROCEDURE [dbo].[GetTTMActiveDisruptions]

GO

CREATE PROCEDURE [dbo].[GetTTMActiveDisruptions]
AS
SELECT Distinct Id,[Name],Description,TT_DisruptionId,StatusId,PrimaryDisruptedRouteNo,dbo.udf_GetAffectedRoutesListForDisruption(id) as AffectedRouteList,StartDateTime,ClearedDateTime,CreatedBy, CreatedDate, UpdatedBy,UpdatedDate, ExpiryTime, Valid
FROM Disruptions
WHERE (StatusId = 1 OR StatusId = 2 OR  StatusId = 3  OR  StatusId = 6)
ORDER BY CreatedDate desc

GO



/****** Object:  StoredProcedure [dbo].[GetAllRoutesList]    Script Date: 28/06/2013 5:49:04 PM ******/
DROP PROCEDURE [dbo].[GetAllRoutesList]
GO

-- =============================================
CREATE PROCEDURE [dbo].[GetAllRoutesList]
	-- Add the parameters for the stored procedure here
AS
BEGIN	
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT RouteNo, Description
	FROM Routes
	WHERE RouteAvailable = 1
END


GO

DROP PROCEDURE [dbo].[GetTTMFOCDisruptionById] 

GO

CREATE PROCEDURE [dbo].[GetTTMFOCDisruptionById] 

@DisruptionId int

AS
BEGIN

SELECT DisruptionID,DisruptionDateTime, DisruptionCleared, NormalServiceResumed,
UpFromStopNo,UpToStopNo,DownFromStopNo,DownToStopNo, Message,RouteNo,
UpFromStopName,UpToStopName,
UpFromStopDirection, UpToStopDirection,
DownFromStopName,DownToStopName,
DownFromStopDirection, DownToStopDirection
FROM FOC_Disruptions_Full
WHERE DisruptionID = @DisruptionId
END

GO

DROP PROCEDURE [dbo].[GetTTMIsFocDisruptionAssigned] 

GO

CREATE PROCEDURE [dbo].[GetTTMIsFocDisruptionAssigned] 

@DisruptionId int

AS
BEGIN

SELECT COUNT(Id) AS [Count] FROM dbo.Disruptions WHERE TT_DisruptionId = @DisruptionId

END

GO

DROP PROCEDURE [dbo].[AddTTMDisruption]

GO

CREATE PROCEDURE [dbo].[AddTTMDisruption] 

@Name nvarchar(256),
@Description nvarchar(1024),
@TT_DisruptionId int,
@PrimaryDisruptedRouteNo smallint,
@StatusId int,
@CreatedBy  nvarchar(128)


AS
BEGIN

DECLARE @disruptionID int

INSERT INTO Disruptions
([Name],Description,TT_DisruptionId,StatusId,PrimaryDisruptedRouteNo, CreatedBy, CreatedDate, UpdatedBy,UpdatedDate)

VALUES
(@Name, @Description, @TT_DisruptionId,@StatusId,@PrimaryDisruptedRouteNo,@CreatedBy, GETDATE(),@CreatedBy,GETDATE())

select SCOPE_IDENTITY()

END

GO

DROP PROCEDURE [dbo].[GetTTMDisruptionById] 

GO

CREATE PROCEDURE [dbo].[GetTTMDisruptionById] 

@DisruptionId int

AS
BEGIN

SELECT ds.Id,ds.Name,ds.Description,ds.TT_DisruptionId,ds.StatusId,ds.PrimaryDisruptedRouteNo,tr.Description as PrimaryDisruptedRouteName, dbo.udf_GetAffectedRoutesListForDisruption(ds.Id) as AffectedRouteList,ds.StartDateTime,ds.ClearedDateTime,
	   ds.CreatedBy, ds.CreatedDate, ds.UpdatedBy,ds.UpdatedDate
	   FROM Disruptions ds

	   LEFT JOIN FOC_Disruptions tr ON
	   tr.PrimaryDisruptedRouteNo = ds.PrimaryDisruptedRouteNo

WHERE ds.Id = @DisruptionId

END

GO

DROP PROCEDURE [dbo].[UpdateTTMDisruption] 

GO

CREATE PROCEDURE [dbo].[UpdateTTMDisruption] 

@DisruptionId int,
@TT_DisruptionId int,
@Name nvarchar(512),
@Description nvarchar(1024),
@StatusId int,
@PrimaryDisruptedRouteNo smallint,
@UpdatedBy  nvarchar(128)

AS
BEGIN

DECLARE @rowsAffected int

UPDATE Disruptions
SET 
	TT_DisruptionId = @TT_DisruptionId,
	[Name] = @Name,
	Description = @Description,
	StatusId = @StatusId,
	UpdatedBy = @UpdatedBy,
	PrimaryDisruptedRouteNo = @PrimaryDisruptedRouteNo,
	UpdatedDate = GETDATE()

WHERE Id = @DisruptionId

INSERT INTO DisruptionsActivityLog(DisruptionId,RouteNo ,StopNo ,Activity ,CreatedBy ,CreatedDate)
VALUES  ( @DisruptionId ,NULL,NULL, 'Updated Disruption details',@UpdatedBy,GETDATE())

SELECT @rowsAffected = @@Rowcount
SELECT @rowsAffected

END

GO

DROP PROCEDURE [dbo].[DeleteAllTTMDisruptionEffectedRouteStops]

GO

CREATE PROCEDURE [dbo].[DeleteAllTTMDisruptionEffectedRouteStops]
	-- Add the parameters for the stored procedure here
	@DisruptionID int,
	@RouteNo smallint,
	@isUpStop BIT,
	@DeletedBy nvarchar(128)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    delete from DisruptedStops where RouteNo = @RouteNo and IsUpStop = @isUpStop and DisruptionID = @DisruptionID

INSERT INTO DisruptionsActivityLog(DisruptionId,RouteNo ,StopNo ,Activity ,CreatedBy ,CreatedDate)
VALUES  ( @DisruptionID ,@RouteNo,NULL, 'Deleted all affected stops for Route ' + CONVERT(VARCHAR(8),@RouteNo), @DeletedBy,GETDATE())

END

GO

DROP PROCEDURE [dbo].[GetTTMDisruptionsRouteStopDetails]

GO

CREATE PROCEDURE [dbo].[GetTTMDisruptionsRouteStopDetails]
	-- Add the parameters for the stored procedure here
	@disruptionId int,
	@routeNo int,
	@upStop bit

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	  SELECT trs.RouteNo, Description, StopSequence, trs.UpStop, tc.DisplayPredictions, ts.StopNo,	  
	  isNull(tc.StopNo, 0) as AffectedStopNo -- return 0 if tc.StopNo does no exist
      FROM RouteStops trs
      INNER JOIN Stops ts ON trs.StopID = ts.StopID  
      left join DisruptedStops tc
      on ts.StopNo = tc.StopNo-- and tc.RouteNo in (select InternalRouteNo from RouteLookUps where RouteNo = @routeNo)
      and tc.DisruptionId = @disruptionId
      WHERE trs.UpStop = @upStop
      and trs.RouteNo = @routeNo
      GROUP BY ts.StopNo,  trs.RouteNo, Description, StopSequence, trs.UpStop,tc.StopNo, tc.DisplayPredictions--,  TramTrackerAvailable
      ORDER BY StopSequence
END

GO

DROP PROCEDURE [dbo].[AddTTMDisruptionEffectedRouteStop] 

GO

CREATE PROCEDURE [dbo].[AddTTMDisruptionEffectedRouteStop] 
@disruptionID int  ,
@routeNo smallint ,
@stopNo smallint,
@upStop bit,
@tramTrackerAvailable bit = 0,
@CreatedBy nvarchar(128),
@displayPredictions bit
AS

BEGIN

--I din't write this and am just moving it accross but it worries me a bit performance wise
--it is doing a delete by looking up a bunch of non-indexed columns, then doing an insert 
--where an update would be much more appropriate, I could be missing something but it warrants investigation

DELETE FROM DisruptedStops 
where DisruptionId = @disruptionID AND RouteNo = @routeNo AND StopNo = @stopNo AND IsUpStop = @upStop

INSERT INTO DisruptedStops(DisruptionId,RouteNo,StopNo,IsUpStop,CreatedBy,CreatedDate, DisplayPredictions)
VALUES (@disruptionID,@routeNo,@stopNo,@upStop,@CreatedBy,GETDATE(), @displayPredictions)

INSERT INTO DisruptionsActivityLog(DisruptionId,RouteNo ,StopNo ,Activity ,CreatedBy ,CreatedDate)
VALUES  ( @disruptionID ,@routeNo,@stopNo, 'Added affected stop ' +  CONVERT(VARCHAR(8),@stopNo) + ' for Route ' + CONVERT(VARCHAR(8),@RouteNo),@CreatedBy,GETDATE())

END


GO


DROP PROCEDURE [dbo].[GetRouteCombinationsForStop]

GO

CREATE PROCEDURE [dbo].[GetRouteCombinationsForStop]
 @stopNo int 
AS
BEGIN
DECLARE @RouteNo int

SET @RouteNo = (SELECT RouteCombinationId from StopToRouteCombinationMapping where StopNo = @stopNo)

SELECT RouteCombination from RouteCombinations where ID = @RouteNo

END

GO

DROP PROCEDURE [dbo].[DeleteTTMDisruptionEffectedRouteStop]

GO

CREATE PROCEDURE [dbo].[DeleteTTMDisruptionEffectedRouteStop] 
@disruptionID int  ,
@routeNo smallint ,
@stopNo smallint,
@upStop bit,
@DeletedBy nvarchar(128)

AS

BEGIN

DELETE FROM DisruptedStops 
where DisruptionId = @disruptionID AND RouteNo = @routeNo AND StopNo = @stopNo AND IsUpStop = @upStop

INSERT INTO DisruptionsActivityLog(DisruptionId,RouteNo ,StopNo ,Activity ,CreatedBy ,CreatedDate)
VALUES  ( @disruptionID ,@routeNo,@stopNo, 'Deleted/Removed affected stop ' +  CONVERT(VARCHAR(8),@stopNo) + ' for Route ' + CONVERT(VARCHAR(8),@routeNo) ,@DeletedBy,GETDATE())

END

GO

DROP PROCEDURE [dbo].[GetTTMGetAffectedStopsByDisruptionId] 

GO

CREATE PROCEDURE [dbo].[GetTTMGetAffectedStopsByDisruptionId] 
	-- Add the parameters for the stored procedure here
	@DisruptionId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT DISTINCT  ds.Id, ds.DisruptionId, ds.RouteNo, ds.StopNo, ds.IsUpStop, st.StopName,st.Description,dl.Direction, rc.RouteCombination --,rs.StopSequence
FROM         DisruptedStops AS ds

INNER JOIN Stops st ON  
st.StopNo = ds.StopNo

INNER JOIN RouteStops rs ON
rs.StopID = st.StopID

INNER JOIN dbo.DirectionLookUps dl ON
dl.DirectionID = st.DirectionID

INNER JOIN dbo.StopToRouteCombinationMapping src ON
st.StopNo = src.StopNo

INNER JOIN dbo.RouteCombinations rc ON
rc.Id = src.RouteCombinationID

WHERE     ds.DisruptionId = @DisruptionId and rs.UpStop = ds.IsUpStop

ORDER BY ds.IsUpStop,ds.StopNo


END

GO

DROP PROCEDURE [dbo].[AddAllTTMDisruptionEffectedRouteStops]

GO

CREATE PROCEDURE [dbo].[AddAllTTMDisruptionEffectedRouteStops]
	-- Add the parameters for the stored procedure here
	@DisruptionID int,
	@RouteNo smallint,
	@isUpStop bit,
	@CreatedBy nvarchar(128),
	@displayPredictions bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	/* delete existing items if they exist */

    delete from DisruptedStops where RouteNo = @RouteNo and IsUpStop = @isUpStop and DisruptionID = @DisruptionID
   
	insert into DisruptedStops select @DisruptionID, @RouteNo, stopno, @isUpStop, @CreatedBy, GETDATE(), @displayPredictions from
	RouteStops rs inner join Stops s on rs.StopID = s.StopID where RouteNo = @RouteNo and UpStop = @isUpStop

	INSERT INTO DisruptionsActivityLog(DisruptionId,RouteNo ,StopNo ,Activity ,CreatedBy ,CreatedDate)
	VALUES  ( @DisruptionID ,@RouteNo,NULL, 'Added all affected stops for Route ' + CONVERT(VARCHAR(8),@RouteNo), @CreatedBy,GETDATE())

END

GO

DROP PROCEDURE [dbo].[GetTTMDisruptionRoutesDetails] 

GO

CREATE PROCEDURE [dbo].[GetTTMDisruptionRoutesDetails] 
	-- Add the parameters for the stored procedure here
	@disruptionID int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	  select distinct drs.RouteNo, r.UpDestination, r.DownDestination 
		FROM dbo.DisruptedStops   drs
		INNER JOIN Routes r on r.RouteNo = drs.RouteNo
		where drs.DisruptionId = @disruptionID and drs.RouteNo > 0
END

GO

DROP PROCEDURE [dbo].[AddTTMDisruptionEffectedRoutes]

GO

CREATE PROCEDURE [dbo].[AddTTMDisruptionEffectedRoutes]
	-- Add the parameters for the stored procedure here
	@DisruptionID int,
	@createdBy nvarchar(128)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM DisruptedRoutes where  DisruptionID =  @DisruptionID

	insert into DisruptedRoutes 

	SELECT DISTINCT @DisruptionID, trs.RouteNo,@createdBy,GETDATE() from RouteStops trs
	INNER JOIN Stops s ON
	trs.StopId = s.StopId
	
	INNER JOIN RouteLookUps rl ON
	rl.InternalRouteNo = trs.RouteNo

	INNER JOIN Routes r ON
	r.RouteNo = trs.RouteNo

	WHERE rl.IsMainRoute = 1 AND r.RouteAvailable = 1 AND
	 s.StopNo in (Select StopNo from DisruptedStops where DisruptionId = @DisruptionID)
   
INSERT INTO DisruptionsActivityLog(DisruptionId,RouteNo ,StopNo ,Activity ,CreatedBy ,CreatedDate)
SELECT  @DisruptionID,RouteNo,NULL,'Added affected Route ' + CONVERT(VARCHAR(8),RouteNo),@createdBy,GETDATE()
FROM DisruptedRoutes WHERE DisruptionId = @DisruptionID

END

GO



DROP PROCEDURE [dbo].[GetTTMDisruptedRoutesMessages]

GO

CREATE PROCEDURE [dbo].[GetTTMDisruptedRoutesMessages]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT R.*,
	(SELECT count(Id) FROM DisruptedStops ds INNER JOIN StopRouteDirectionMaps srdm ON ds.StopNo = srdm.StopNo WHERE srdm.RouteNo = D.RouteNo AND UpStop = 1 AND D.RouteNo <> 35) AS UpStopsCount,
	(SELECT count(Id) FROM DisruptedStops ds INNER JOIN StopRouteDirectionMaps srdm ON ds.StopNo = srdm.StopNo WHERE srdm.RouteNo = D.RouteNo AND UpStop = 0 AND D.RouteNo <> 35) AS DownStopsCount
	
	FROM DisruptedRoutes D INNER JOIN DisruptedRouteMessages R ON D.RouteNo = R.RouteNo --WHERE DisruptionId = @DisruptionID
END

GO

DROP PROCEDURE [dbo].[GetTTMAffectedDisruptionRouteCombinationMessages]

GO

CREATE PROCEDURE [dbo].[GetTTMAffectedDisruptionRouteCombinationMessages]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @RouteComb VARCHAR(255)
	
	DECLARE  @CombTable TABLE (
		RouteComb VARCHAR(255)
	)
	--TODO: change database collation
	SELECT DISTINCT DM.*, 
	(SELECT count(Distinct Id) FROM DisruptedStops ds INNER JOIN StopRouteCombinationDirectionMaps srdm ON ds.StopNo = srdm.StopNo WHERE srdm.RouteCombination COLLATE DATABASE_DEFAULT = DM.RouteNoCombination COLLATE DATABASE_DEFAULT AND UpStop = 1) AS UpStopsCount,
	(SELECT count(Distinct Id) FROM DisruptedStops ds INNER JOIN StopRouteCombinationDirectionMaps srdm ON ds.StopNo = srdm.StopNo WHERE srdm.RouteCombination COLLATE DATABASE_DEFAULT = DM.RouteNoCombination COLLATE DATABASE_DEFAULT AND UpStop = 0) AS DownStopsCount

	FROM DisruptedStops DS

	INNER JOIN StopToRouteCombinationMapping M
	ON DS.StopNo = M.StopNo
	INNER JOIN RouteCombinations RC
	ON M.RouteCombinationID = RC.ID
	INNER JOIN DisruptedCombinationRouteMessages DM
	ON RC.RouteCombination COLLATE DATABASE_DEFAULT = DM.RouteNoCombination COLLATE DATABASE_DEFAULT
	ORDER BY DM.RouteNoCombination asc

END

GO

DROP PROCEDURE [dbo].[GetTTMUnaffectedDisruptionRouteCombinationMessages]

GO

CREATE PROCEDURE [dbo].[GetTTMUnaffectedDisruptionRouteCombinationMessages]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @RouteComb VARCHAR(255)
	DECLARE @StopID VARCHAR(10)
	
	DECLARE  @CombTable TABLE (
		RouteComb VARCHAR(255)
	)
	
	SELECT DISTINCT DM.* FROM RouteStops RS
	INNER JOIN Stops S
	ON RS.StopID = S.StopID
	INNER JOIN StopToRouteCombinationMapping M
	ON S.StopNo = M.StopNo
	INNER JOIN RouteCombinations RC
	ON M.RouteCombinationID = RC.ID
	INNER JOIN DisruptedCombinationRouteMessages DM
	ON RC.RouteCombination COLLATE DATABASE_DEFAULT = DM.RouteNoCombination COLLATE DATABASE_DEFAULT
	WHERE S.StopNo NOT IN 
	(
		SELECT StopNo FROM DisruptedStops DS 
	)
	AND UpStop IN 
	(
		SELECT IsUpStop FROM DisruptedStops
	)
	AND RS.RouteNo IN
	(
		SELECT RouteNo FROM DisruptedRoutes 
	)

	ORDER BY DM.RouteNoCombination asc

END

GO

GO

DROP PROCEDURE [dbo].[GetTTMUnaffectedDisruptionRouteCombinationMessagesByDisruptionId]

GO


CREATE PROCEDURE [dbo].[GetTTMUnaffectedDisruptionRouteCombinationMessagesByDisruptionId]
@DisruptionId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @RouteComb VARCHAR(255)
	DECLARE @StopID VARCHAR(10)
	
	DECLARE  @CombTable TABLE (
		RouteComb VARCHAR(255)
	)

	/*select routes and combos into a temp table to speed up the querying of up and down stop dounts*/
	SELECT DISTINCT srdm.StopNo, UpStop, srdm.RouteNo AS 'Data' INTO #stops_by_combo_and_message
	FROM StopRouteDirectionMaps srdm  INNER JOIN DisruptedStops DS 
	ON DS.StopNo = srdm.StopNo
	--cross apply (SELECT Data from dbo.Split(srdm.RouteCombination, ',')) b

	SELECT DISTINCT DM.*, dbo.udf_GetAffectedRoutesListForDisruption(@DisruptionId) as AffectedRouteList INTO #unaffected_routes_temp
	FROM RouteStops RS
	INNER JOIN Stops S
	ON RS.StopID = S.StopID
	INNER JOIN StopToRouteCombinationMapping M
	ON S.StopNo = M.StopNo
	INNER JOIN RouteCombinations RC
	ON M.RouteCombinationID = RC.ID
	INNER JOIN DisruptedCombinationRouteMessages DM
	ON RC.RouteCombination COLLATE DATABASE_DEFAULT = DM.RouteNoCombination COLLATE DATABASE_DEFAULT	
	WHERE S.StopNo NOT IN (
		SELECT StopNo FROM DisruptedStops DS WHERE DisruptionId = @DisruptionId
	)
	AND UpStop IN 
	(
		SELECT IsUpStop FROM DisruptedStops
	)
	AND RS.RouteNo IN
	(
		SELECT RouteNo FROM DisruptedRoutes 
	)	
	
	select distinct urm.*,
	(SELECT count(Distinct t.StopNo) FROM #stops_by_combo_and_message t
		where t.UpStop = 1  AND t.Data IN (SELECT Data FROM dbo.Split(RouteNoCombination, ','))) AS UpStopsCount,
		(SELECT count(Distinct t.StopNo) FROM #stops_by_combo_and_message t
		where t.UpStop = 0  AND  t.Data IN (SELECT Data FROM dbo.Split(RouteNoCombination, ','))) AS DownStopsCount
		 From #unaffected_routes_temp urm
		
		 cross apply (SELECT Data from dbo.Split(RouteNoCombination, ',')) c
		 WHERE c.Data IN (SELECT Data from dbo.Split(AffectedRouteList, ','))		 
	ORDER BY RouteNoCombination asc

DROP TABLE #unaffected_routes_temp

DROP TABLE #stops_by_combo_and_message
	
END

GO

DROP PROCEDURE [dbo].[GetTTMDisruptionRouteCombinationMessages]

GO

CREATE PROCEDURE [dbo].[GetTTMDisruptionRouteCombinationMessages]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @RouteComb VARCHAR(255)
	
	DECLARE  @CombTable TABLE (
		RouteComb VARCHAR(255)
	)
	--TODO: change database collation
	SELECT DISTINCT DM.*, 
	(SELECT count(Distinct Id) FROM DisruptedStops ds INNER JOIN StopRouteCombinationDirectionMaps srdm ON ds.StopNo = srdm.StopNo WHERE srdm.RouteCombination COLLATE DATABASE_DEFAULT = DM.RouteNoCombination COLLATE DATABASE_DEFAULT AND UpStop = 1) AS UpStopsCount,
	(SELECT count(Distinct Id) FROM DisruptedStops ds INNER JOIN StopRouteCombinationDirectionMaps srdm ON ds.StopNo = srdm.StopNo WHERE srdm.RouteCombination COLLATE DATABASE_DEFAULT = DM.RouteNoCombination COLLATE DATABASE_DEFAULT AND UpStop = 0) AS DownStopsCount

	FROM DisruptedStops DS

	INNER JOIN StopToRouteCombinationMapping M
	ON DS.StopNo = M.StopNo
	INNER JOIN RouteCombinations RC
	ON M.RouteCombinationID = RC.ID
	INNER JOIN DisruptedCombinationRouteMessages DM
	ON RC.RouteCombination COLLATE DATABASE_DEFAULT = DM.RouteNoCombination COLLATE DATABASE_DEFAULT
	ORDER BY DM.RouteNoCombination asc

END

GO



DROP PROCEDURE [dbo].[GetTTMDisruptionRouteCombinationMessagesByDisruptionId]

GO

CREATE PROCEDURE [dbo].[GetTTMDisruptionRouteCombinationMessagesByDisruptionId]
@DisruptionId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @RouteComb VARCHAR(255)
	
	DECLARE  @CombTable TABLE (
		RouteComb VARCHAR(255)
	)
	--TODO: change database collation
	SELECT DISTINCT DM.*, 
	(SELECT count(Distinct Id) FROM DisruptedStops ds INNER JOIN StopRouteCombinationDirectionMaps srdm ON ds.StopNo = srdm.StopNo WHERE srdm.RouteCombination COLLATE DATABASE_DEFAULT = DM.RouteNoCombination COLLATE DATABASE_DEFAULT AND UpStop = 1) AS UpStopsCount,
	(SELECT count(Distinct Id) FROM DisruptedStops ds INNER JOIN StopRouteCombinationDirectionMaps srdm ON ds.StopNo = srdm.StopNo WHERE srdm.RouteCombination COLLATE DATABASE_DEFAULT = DM.RouteNoCombination COLLATE DATABASE_DEFAULT AND UpStop = 0) AS DownStopsCount

	FROM DisruptedStops DS

	INNER JOIN StopToRouteCombinationMapping M
	ON DS.StopNo = M.StopNo
	INNER JOIN RouteCombinations RC
	ON M.RouteCombinationID = RC.ID
	INNER JOIN DisruptedCombinationRouteMessages DM
	ON RC.RouteCombination COLLATE DATABASE_DEFAULT = DM.RouteNoCombination COLLATE DATABASE_DEFAULT
	WHERE DS.DisruptionId = @DisruptionId
	ORDER BY DM.RouteNoCombination asc

	/*UNION		
	
	select DISTINCT  DM.*,
	(SELECT count(Distinct Id) FROM DisruptedStops ds INNER JOIN StopRouteCombinationDirectionMaps srdm ON ds.StopNo = srdm.StopNo WHERE srdm.RouteCombination COLLATE DATABASE_DEFAULT = DM.RouteNoCombination COLLATE DATABASE_DEFAULT AND UpStop = 1) AS UpStopsCount,
	(SELECT count(Distinct Id) FROM DisruptedStops ds INNER JOIN StopRouteCombinationDirectionMaps srdm ON ds.StopNo = srdm.StopNo WHERE srdm.RouteCombination COLLATE DATABASE_DEFAULT = DM.RouteNoCombination COLLATE DATABASE_DEFAULT AND UpStop = 0) AS DownStopsCount
	FROM RouteCombinations RC 
	INNER JOIN StopToRouteCombinationMapping M ON M.RouteCombinationID = RC.ID
	INNER JOIN DisruptedCombinationRouteMessages DM ON RC.RouteCombination COLLATE DATABASE_DEFAULT = DM.RouteNoCombination COLLATE DATABASE_DEFAULT
	WHERE (select RouteCombination FROM RouteCombinations RC 
	INNER JOIN StopToRouteCombinationMapping M ON M.RouteCombinationID = RC.ID
	WHERE StopNo IN (SELECT StopNo FROM DisruptedStops DS WHERE DisruptionId = @DisruptionId)
	) 
	IN (SELECT number from udf_CreateIntTableFromList(DM.RouteNoCombination))
	ORDER BY DM.RouteNoCombination asc*/

END

GO

DROP PROCEDURE [dbo].[GetTTMDisruptionsForRoute] 

GO

CREATE PROCEDURE [dbo].[GetTTMDisruptionsForRoute] 

@RouteNo int

AS
BEGIN

SELECT     ds.Id, ds.Name
FROM         DisruptedRoutes AS dr INNER JOIN
             Disruptions AS ds ON ds.Id = dr.DisruptionId
WHERE     dr.RouteNo = @RouteNo

END

GO

DROP PROCEDURE [dbo].[GetTTMDisruptedRoutesMessagesByDisruptionId]

GO

CREATE PROCEDURE [dbo].[GetTTMDisruptedRoutesMessagesByDisruptionId]
	-- Add the parameters for the stored procedure here

@DisruptionId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT R.*,
	(SELECT count(Id) FROM DisruptedStops ds INNER JOIN StopRouteDirectionMaps srdm ON ds.StopNo = srdm.StopNo WHERE srdm.RouteNo = D.RouteNo AND UpStop = 1 AND D.RouteNo <> 35) AS UpStopsCount,
	(SELECT count(Id) FROM DisruptedStops ds INNER JOIN StopRouteDirectionMaps srdm ON ds.StopNo = srdm.StopNo WHERE srdm.RouteNo = D.RouteNo AND UpStop = 0 AND D.RouteNo <> 35) AS DownStopsCount
	 FROM DisruptedRoutes D INNER JOIN DisruptedRouteMessages R ON D.RouteNo = R.RouteNo WHERE D.DisruptionId = @DisruptionID
END

GO



DROP PROCEDURE [dbo].[GetTTMDisruptionsByRouteCombination]

GO

CREATE PROCEDURE [dbo].[GetTTMDisruptionsByRouteCombination]
	@RouteCombMsgId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @ROUTECOMB VARCHAR(50)
	
	SELECT @ROUTECOMB = RouteNoCombination FROM DisruptedCombinationRouteMessages WHERE Id = @RouteCombMsgId
	
	SELECT DISTINCT DisruptionId, Name FROM DisruptedRoutes DR INNER JOIN Disruptions D ON DR.DisruptionId = D.Id WHERE RouteNo IN (
		select number from udf_CreateIntTableFromList(@ROUTECOMB)
	)    
	
END

GO

DROP PROCEDURE [dbo].[UpdateTTMRouteDisruptionMessages] 

GO

CREATE PROCEDURE [dbo].[UpdateTTMRouteDisruptionMessages] 
@Id int,
@AffectedShortMessage nvarchar(128),
--@AffectedMediumMessage nvarchar(256),
@AffectedLongMessage nvarchar(512),
@AffectedAdditionalInfoOnWebsite bit,
@UnaffectedShortMessage nvarchar(128),
--@UnaffectedMediumMessage nvarchar(256),
@UnaffectedLongMessage nvarchar(512),
@UnaffectedAdditionalInfoOnWebsite bit,
@UpdatedBy  nvarchar(128),
@LastUpdatedDisruptionId int,
@HideDownStopMessages bit,
@HideUpStopMessages bit
AS
BEGIN
DECLARE @rowsAffected int

UPDATE DisruptedRouteMessages
SET 
	AffectedShortMessage = @AffectedShortMessage,
	--AffectedMediumMessage = @AffectedMediumMessage,
	AffectedLongMessage = @AffectedLongMessage,
	AffectedAdditionalInfoOnWebsite = @AffectedAdditionalInfoOnWebsite,

	UnaffectedShortMessage = @UnaffectedShortMessage,
	--UnaffectedMediumMessage = @UnaffectedMediumMessage,
	UnaffectedLongMessage = @UnaffectedLongMessage,
	UnaffectedAdditionalInfoOnWebsite = @UnaffectedAdditionalInfoOnWebsite,
	LastUpdatedDisruptionId = @LastUpdatedDisruptionId,
	UpdatedBy = @UpdatedBy,
	UpdatedDate = GETDATE(),
	HideDownStopMessages = @HideDownStopMessages,
	HideUpStopMessages = @HideUpStopMessages

WHERE Id = @Id

INSERT INTO DisruptionsActivityLog(DisruptionId,RouteNo ,StopNo ,Activity ,CreatedBy ,CreatedDate)
VALUES  ( NULL ,NULL,NULL, 'Updated RouteDisruptionMessage Id : ' + CONVERT(VARCHAR(8),@Id) ,@UpdatedBy,GETDATE())

SELECT @rowsAffected = @@Rowcount
SELECT @rowsAffected

END

GO

DROP PROCEDURE [dbo].[UpdateTTMRouteCombinationDisruptionMessage] 

GO

CREATE PROCEDURE [dbo].[UpdateTTMRouteCombinationDisruptionMessage] 

@Id int,
@AffectedShortMessage nvarchar(128),
@AffectedLongMessage nvarchar(512),
@AffectedAdditionalInfoOnWebsite bit,
@AffectedDisplayType int,
@UnaffectedShortMessage nvarchar(128),
@UnaffectedLongMessage nvarchar(512),
@UnaffectedAdditionalInfoOnWebsite bit,
@UnaffectedDisplayType int,
@UpdatedBy  nvarchar(128),
@LastUpdatedDisruptionId int,
@HideDownStopMessages bit,
@HideUpStopMessages bit

AS
BEGIN
DECLARE @rowsAffected int

UPDATE DisruptedCombinationRouteMessages
SET 
	AffectedShortMessage = @AffectedShortMessage,
	AffectedLongMessage = @AffectedLongMessage,
	AffectedAdditionalInfoOnWebsite = @AffectedAdditionalInfoOnWebsite,
	AffectedDisplayType = @AffectedDisplayType,
	UnaffectedShortMessage = @UnaffectedShortMessage,
	UnaffectedLongMessage = @UnaffectedLongMessage,
	UnaffectedAdditionalInfoOnWebsite = @UnaffectedAdditionalInfoOnWebsite,
	UnaffectedDisplayType = @UnaffectedDisplayType,
	LastUpdatedDisruptionId = @LastUpdatedDisruptionId,
	UpdatedBy = @UpdatedBy,
	UpdatedDate = GETDATE(),
	HideDownStopMessages = @HideDownStopMessages,
	HideUpStopMessages = @HideUpStopMessages
WHERE Id = @Id

INSERT INTO DisruptionsActivityLog(DisruptionId,RouteNo ,StopNo ,Activity ,CreatedBy ,CreatedDate)
VALUES  ( NULL ,NULL,NULL, 'Updated Affected CombinationRouteDisruptionMessage Id : ' + CONVERT(VARCHAR(8),@Id) ,@UpdatedBy,GETDATE())

SELECT @rowsAffected = @@Rowcount
SELECT @rowsAffected

END

GO

DROP PROCEDURE [dbo].[GetTTMValidateDisruptionMessagesByDisruptionId]

GO

CREATE PROCEDURE [dbo].[GetTTMValidateDisruptionMessagesByDisruptionId]
@DisruptionId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	--for unaffected combinations
	SELECT DISTINCT srdm.StopNo, UpStop, Data INTO #stops_by_combo_and_message
	FROM StopRouteCombinationDirectionMaps srdm  INNER JOIN DisruptedStops DS 
	ON DS.StopNo = srdm.StopNo
	cross apply (SELECT Data from dbo.Split(srdm.RouteCombination, ',')) b

	SELECT DISTINCT DM.*, dbo.udf_GetAffectedRoutesListForDisruption(@DisruptionId) as AffectedRouteList INTO #unaffected_routes_temp
	FROM RouteStops RS
	INNER JOIN Stops S
	ON RS.StopID = S.StopID
	INNER JOIN StopToRouteCombinationMapping M
	ON S.StopNo = M.StopNo
	INNER JOIN RouteCombinations RC
	ON M.RouteCombinationID = RC.ID
	INNER JOIN DisruptedCombinationRouteMessages DM
	ON RC.RouteCombination COLLATE DATABASE_DEFAULT = DM.RouteNoCombination COLLATE DATABASE_DEFAULT	
	WHERE S.StopNo NOT IN (
		SELECT StopNo FROM DisruptedStops DS WHERE DisruptionId = @DisruptionId
	)
	AND UpStop IN 
	(
		SELECT IsUpStop FROM DisruptedStops
	)
	AND RS.RouteNo IN
	(
		SELECT RouteNo FROM DisruptedRoutes 
	)	



	SELECT distinct R.id AS 'Id',  'route' AS 'type'  FROM DisruptedRoutes D INNER JOIN DisruptedRouteMessages R ON D.RouteNo = R.RouteNo 
	WHERE D.DisruptionId = @DisruptionId AND (R.UnaffectedShortMessage IS NULL OR R.AffectedLongMessage IS NULL OR R.UnaffectedShortMessage IS NULL OR R.AffectedLongMessage IS NULL) 

	UNION

	SELECT DISTINCT DM.Id AS 'Id', 'combo' AS 'type'
	FROM DisruptedStops DS
	INNER JOIN StopToRouteCombinationMapping M
	ON DS.StopNo = M.StopNo
	INNER JOIN RouteCombinations RC
	ON M.RouteCombinationID = RC.ID
	INNER JOIN DisruptedCombinationRouteMessages DM
	ON RC.RouteCombination COLLATE DATABASE_DEFAULT = DM.RouteNoCombination COLLATE DATABASE_DEFAULT
	WHERE DS.DisruptionId = @DisruptionId
	AND (DM.UnaffectedShortMessage IS NULL OR DM.AffectedLongMessage IS NULL OR DM.UnaffectedShortMessage IS NULL OR DM.AffectedLongMessage IS NULL) 

	UNION	
	
	select distinct urm.Id as 'Id', 'combo' AS 'type'
	From #unaffected_routes_temp urm
		
	cross apply (SELECT Data from dbo.Split(RouteNoCombination, ',')) c
	WHERE c.Data IN (SELECT Data from dbo.Split(AffectedRouteList, ','))	
	AND urm.UnaffectedShortMessage IS NULL	 	

	DROP TABLE #unaffected_routes_temp

	DROP TABLE #stops_by_combo_and_message

END

GO


DROP PROCEDURE [dbo].[UpdateDisruptionValidStatus]

GO

CREATE PROCEDURE [dbo].[UpdateDisruptionValidStatus]
@DisruptionId int,
@Valid Bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	UPDATE Disruptions SET Valid = @Valid WHERE Id = @DisruptionId
	select @Valid as 'valid'
END

GO


DROP PROCEDURE [dbo].[GetTTMOtherDisruptionAndRoutes] 

GO

CREATE PROCEDURE [dbo].[GetTTMOtherDisruptionAndRoutes] 

@DisruptionId int

AS
BEGIN

SELECT * FROM DisruptedRoutes
WHERE RouteNo in (SELECT DISTINCT RouteNo FROM DisruptedRoutes WHERE DisruptionId = @DisruptionId)
and DisruptionId <> @DisruptionId


END

GO


DROP PROCEDURE [dbo].[GetTTMRouteMessagesForDisruption]

GO

CREATE PROCEDURE [dbo].[GetTTMRouteMessagesForDisruption]
@DisruptionId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @RouteComb VARCHAR(255)
	DECLARE @StopID VARCHAR(10)
	
	SELECT DISTINCT DRM.* FROM DisruptedRoutes DR
	INNER JOIN DisruptedRouteMessages DRM
	ON DR.RouteNo = DRM.RouteNo
	INNER JOIN RouteStops RS
	ON DR.RouteNo = RS.RouteNo
	INNER JOIN Stops S
	ON RS.StopID = S.StopID
	WHERE DisruptionId = @DisruptionId
	ORDER BY DRM.RouteNo
END

GO

DROP PROCEDURE [dbo].[ClearTTMRouteDisruptionMessages]

GO

CREATE PROCEDURE [dbo].[ClearTTMRouteDisruptionMessages] 

@Id int,
@UpdatedBy  nvarchar(128)

AS
BEGIN
DECLARE @rowsAffected int

UPDATE DisruptedRouteMessages
SET 
	AffectedShortMessage = NULL,	
	AffectedLongMessage = NULL,
	AffectedAdditionalInfoOnWebsite = 0,
	UnaffectedShortMessage = NULL,	
	UnaffectedLongMessage = NULL,
	UnaffectedAdditionalInfoOnWebsite = 0,
	HideUpStopMessages = 0,
	HideDownStopMessages = 0,
	UpdatedBy = @UpdatedBy,
	UpdatedDate = GETDATE()
WHERE Id = @Id

END

GO

DROP PROCEDURE [dbo].[GetTTMRouteCombinationMessagesForDisruption]

GO

CREATE PROCEDURE [dbo].[GetTTMRouteCombinationMessagesForDisruption]
@DisruptionId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @RouteComb VARCHAR(255)
	DECLARE @StopID VARCHAR(10)
	
	SELECT DISTINCT DM.* FROM RouteStops RS
	INNER JOIN Stops S
	ON RS.StopID = S.StopID
	INNER JOIN StopToRouteCombinationMapping M
	ON S.StopNo = M.StopNo
	INNER JOIN RouteCombinations RC
	ON M.RouteCombinationID = RC.ID
	INNER JOIN DisruptedCombinationRouteMessages DM
	ON RC.RouteCombination COLLATE DATABASE_DEFAULT = DM.RouteNoCombination COLLATE DATABASE_DEFAULT
	WHERE S.StopNo IN 
	(
		SELECT DISTINCT StopNo FROM RouteStops RS INNER JOIN Stops S ON RS.StopID = S.StopID
		WHERE RouteNo IN (SELECT DISTINCT RouteNo FROM DisruptedRoutes WHERE DisruptionId = @DisruptionId)
	)
	
	ORDER BY DM.RouteNoCombination asc

END

GO

DROP PROCEDURE [dbo].[ClearTTMCombinationRouteDisruptionMessages] 

GO

CREATE PROCEDURE [dbo].[ClearTTMCombinationRouteDisruptionMessages] 

@Id int,
@UpdatedBy varchar(128)

AS
BEGIN
DECLARE @rowsAffected int

UPDATE DisruptedCombinationRouteMessages
SET 
	AffectedShortMessage = NULL,	
	AffectedLongMessage = NULL,
	AffectedAdditionalInfoOnWebsite = 0,
	AffectedDisplayType =1,
	UnaffectedShortMessage = NULL,	
	UnaffectedLongMessage = NULL,
	UnaffectedAdditionalInfoOnWebsite = 0,
	UnaffectedDisplayType =1,
	HideUpStopMessages = 0,
	HideDownStopMessages = 0,
	UpdatedBy = @UpdatedBy,
	UpdatedDate = GETDATE()

WHERE Id = @Id

END

GO

DROP PROCEDURE [dbo].[ClearTTMDisruption]

GO

CREATE PROCEDURE [dbo].[ClearTTMDisruption] 

@DisruptionId int,
@UpdatedBy  nvarchar(128)

AS
BEGIN

DELETE FROM DisruptedStops where DisruptionId = @DisruptionId
DELETE FROM DisruptedRoutes where DisruptionId = @DisruptionId

UPDATE Disruptions 
SET StatusId = 4,
	ClearedDateTime = GETDATE(),
	ClearedBy = @UpdatedBy,
	UpdatedBy = @UpdatedBy,
	UpdatedDate = GETDATE()
WHERE Id = @DisruptionId

END

GO

DROP PROCEDURE [dbo].[ArchiveTTMDisruptedRouteCombinationStops]

GO

CREATE PROCEDURE [dbo].[ArchiveTTMDisruptedRouteCombinationStops]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO ArchivedDisruptedRouteCombinationStops
    SELECT DISTINCT RouteCombination, S.StopNo, AD.Id, 1, AD.CreatedDate, GETDATE() FROM RouteStops RS INNER JOIN Stops S ON RS.StopID = S.StopID 
    INNER JOIN StopToRouteCombinationMapping MP ON S.StopNo = MP.StopNo
    INNER JOIN RouteCombinations RC ON MP.RouteCombinationID = RC.ID
    INNER JOIN ActiveDisruptedCombinationRouteMessages AD
    ON RC.RouteCombination COLLATE DATABASE_DEFAULT = AD.RouteNoCombination COLLATE DATABASE_DEFAULT
    WHERE RS.RouteNo IN (SELECT RouteNo FROM ActiveDisruptedRoutes) AND S.StopNo IN (SELECT StopNo FROM ActiveDisruptedStops)
    
    INSERT INTO ArchivedDisruptedRouteCombinationStops
    SELECT DISTINCT RouteCombination, S.StopNo, AD.Id, 0, AD.CreatedDate, GETDATE() FROM RouteStops RS INNER JOIN Stops S ON RS.StopID = S.StopID 
    INNER JOIN StopToRouteCombinationMapping MP ON S.StopNo = MP.StopNo
    INNER JOIN RouteCombinations RC ON MP.RouteCombinationID = RC.ID
    INNER JOIN ActiveDisruptedCombinationRouteMessages AD
    ON RC.RouteCombination COLLATE DATABASE_DEFAULT = AD.RouteNoCombination COLLATE DATABASE_DEFAULT
    WHERE RS.RouteNo IN (SELECT RouteNo FROM ActiveDisruptedRoutes) AND S.StopNo NOT IN (SELECT StopNo FROM ActiveDisruptedStops)
    
END

GO

DROP PROCEDURE [dbo].[ArchiveTTMDisruptedRouteStops]

GO

CREATE PROCEDURE [dbo].[ArchiveTTMDisruptedRouteStops]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO ArchivedDisruptedRouteStops
    SELECT DISTINCT RS.RouteNo, StopNo, M.Id, 1, M.CreatedDate, GETDATE(), 0 FROM RouteStops RS INNER JOIN Stops S ON RS.StopID = S.StopID 
    INNER JOIN ActiveDisruptedRouteMessages M ON RS.RouteNo = M.RouteNo
    WHERE RS.RouteNo IN (SELECT RouteNo FROM ActiveDisruptedRoutes) AND StopNo IN (SELECT StopNo FROM ActiveDisruptedStops)
    
	INSERT INTO ArchivedDisruptedRouteStops
    SELECT DISTINCT RS.RouteNo, StopNo, M.Id, 0, M.CreatedDate, GETDATE(), 0 FROM RouteStops RS INNER JOIN Stops S ON RS.StopID = S.StopID 
    INNER JOIN ActiveDisruptedRouteMessages M ON RS.RouteNo = M.RouteNo
    WHERE RS.RouteNo IN (SELECT RouteNo FROM ActiveDisruptedRoutes) AND StopNo NOT IN (SELECT StopNo FROM ActiveDisruptedStops)
END

GO

DROP PROCEDURE [dbo].[UpdateTTMTTL]

GO

CREATE PROCEDURE [dbo].[UpdateTTMTTL]
	@DisruptionId int,
	@UpdatedBy  nvarchar(128),
	@MessageTTL  int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @FOC_Disruption_Id int
	SET @FOC_Disruption_Id = (SELECT TT_DisruptionId FROM Disruptions WHERE Id = @DisruptionId)

	DECLARE @FocClearance DATETIME
	SET @FocClearance = (SELECT DisruptionCleared FROM FOC_Disruptions_Full WHERE DisruptionId = @FOC_Disruption_Id)

	IF @FOC_Disruption_Id IS NOT NULL AND @FocClearance IS NULL 
	BEGIN
		UPDATE Disruptions SET MessageTTL = @MessageTTL, ExpiryTime = NULL, StatusId = 6 WHERE Id = @DisruptionId
		--PRINT 'expiry = NULL'
	END
	ELSE BEGIN
		UPDATE Disruptions SET MessageTTL = @MessageTTL, ExpiryTime = DATEADD(minute ,@MessageTTL,GETDATE()), StatusId = 6 WHERE Id = @DisruptionId
		--PRINT 'expiry IS NOT NULL'
	END

	--UPDATE Disruptions SET MessageTTL = @MessageTTL, ExpiryTime = DATEADD(minute ,@MessageTTL,GETDATE()), StatusId = 6 WHERE Id = @DisruptionId

	INSERT INTO DisruptionsActivityLog(DisruptionId,RouteNo ,StopNo ,Activity ,CreatedBy ,CreatedDate)
	VALUES  ( @DisruptionId ,NULL,NULL, 'Set disruption clear in the future' ,@UpdatedBy,GETDATE())

	SELECT ExpiryTime FROM Disruptions WHERE Id = @DisruptionId
END

GO

DROP PROCEDURE [dbo].[GetAllTTMDisruptedRoutes] 

GO

CREATE PROCEDURE [dbo].[GetAllTTMDisruptedRoutes] 
AS
BEGIN
SELECT DISTINCT dr.RouteNo,tr.Description 
	   FROM DisruptedRoutes dr
	   INNER JOIN Routes tr ON
	   tr.RouteNo = dr.RouteNo
END

GO

DROP PROCEDURE [dbo].[GetTTMDisruptedRouteStopDetails]

GO

CREATE PROCEDURE [dbo].[GetTTMDisruptedRouteStopDetails]
	-- Add the parameters for the stored procedure here
	@routeNo int,
	@upStop bit

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	  SELECT trs.RouteNo, Description, StopSequence, trs.UpStop, ts.StopNo, 
	  isNull(tc.StopNo, 0) as AffectedStopNo -- return 0 if tc.StopNo does not exist in DisruptedStops
      FROM RouteStops trs
      INNER JOIN Stops ts ON trs.StopID = ts.StopID  
      left join DisruptedStops tc
      on ts.StopNo = tc.StopNo --and tc.RouteNo in (select InternalRouteNo from T_RouteLookUps where RouteNo = @routeNo)
      WHERE trs.UpStop = @upStop
      and trs.RouteNo = @routeNo
      GROUP BY ts.StopNo,  trs.RouteNo, Description, StopSequence, trs.UpStop,tc.StopNo--,  TramTrackerAvailable
      ORDER BY StopSequence

END

GO

DROP PROCEDURE [dbo].[GetTTMRouteMessagesByStopNo]

GO

CREATE PROCEDURE [dbo].[GetTTMRouteMessagesByStopNo]
	-- Add the parameters for the stored procedure here
	@StopNo smallint

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @RouteCombinationId int
	DECLARE @RouteCombinationIdList varchar(500)

	SET @RouteCombinationId = (Select RouteCombinationId from StopToRouteCombinationMapping where StopNo = @StopNo)
	SET @RouteCombinationIdList = (SELECT RouteCombination FROM RouteCombinations WHERE Id = @RouteCombinationId)


	SELECT ID,RouteNo, AffectedShortMessage,AffectedLongMessage,UnaffectedShortMessage,UnaffectedLongMessage,AffectedAdditionalInfoOnWebsite,UnaffectedAdditionalInfoOnWebsite, HideDownStopMessages, HideUpStopMessages
	FROM DisruptedRouteMessages

	WHERE RouteNo IN (SELECT number from dbo.udf_CreateIntTableFromList(@RouteCombinationIdList) WHERE RouteNo IN (SELECT DISTINCT RouteNo from DisruptedRoutes))
	--WHERE RouteNo IN (SELECT DISTINCT RouteNo from DisruptedRoutes)  
END


GO

DROP PROCEDURE [dbo].[GetTTMRouteCombinationMessageByStopNo]

GO

CREATE PROCEDURE [dbo].[GetTTMRouteCombinationMessageByStopNo]
	-- Add the parameters for the stored procedure here
	@StopNo smallint

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @RouteCombinationId int
	DECLARE @RouteCombinationIdList varchar(500)

	SET @RouteCombinationId = (Select RouteCombinationId from StopToRouteCombinationMapping where StopNo = @StopNo)
	SET @RouteCombinationIdList = (SELECT RouteCombination FROM RouteCombinations WHERE Id = @RouteCombinationId)

	SELECT ID,RouteNoCombination, AffectedShortMessage,AffectedLongMessage,UnaffectedShortMessage,UnaffectedLongMessage,AffectedAdditionalInfoOnWebsite,UnaffectedAdditionalInfoOnWebsite,AffectedDisplayType,UnaffectedDisplayType, HideDownStopMessages, HideUpStopMessages
	FROM DisruptedCombinationRouteMessages
	WHERE RouteNoCombination = @RouteCombinationIdList
END

GO

DROP PROCEDURE [dbo].[GetTTMDisruptionPrimaryRouteMessages]

GO

CREATE PROCEDURE [dbo].[GetTTMDisruptionPrimaryRouteMessages]	
	@DisruptionId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT AffectedShortMessage, AffectedLongMessage, UnaffectedShortMessage, UnaffectedLongMessage
	FROM DisruptedRouteMessages m 
	INNER JOIN Disruptions d ON d.PrimaryDisruptedRouteNo = m.RouteNo
	WHERE d.Id = @DisruptionId
END

GO

DROP PROCEDURE [dbo].[PublishTTMDisruptionsData] 

GO

CREATE PROCEDURE [dbo].[PublishTTMDisruptionsData] 

@CreatedBy  nvarchar(128)


AS
BEGIN


DELETE FROM ActiveDisruptedStops
DELETE FROM ActiveDisruptedRoutes
DELETE FROM ActiveDisruptedRouteMessages
DELETE FROM ActiveDisruptedCombinationRouteMessages


INSERT INTO ActiveDisruptedStops (RouteNo,StopNo,IsUpStop,CreatedBy,CreatedDate, DisplayPredictions)
SELECT RouteNo,StopNo,IsUpStop,@CreatedBy,GETDATE(), DisplayPredictions
FROM DisruptedStops 


INSERT INTO ActiveDisruptedRoutes (RouteNo, CreatedBy,CreatedDate)
SELECT DISTINCT RouteNo,@CreatedBy,GETDATE() 
FROM DisruptedRoutes

INSERT INTO ActiveDisruptedRouteMessages(RouteNo,AffectedShortMessage,AffectedLongMessage,UnaffectedShortMessage,UnaffectedLongMessage,
AffectedAdditionalInfoOnWebSite,UnaffectedAdditionalInfoOnWebSite,CreatedBy,CreatedDate, HideUpStopMessages, HideDownStopMessages )
SELECT RouteNo,AffectedShortMessage,AffectedLongMessage,UnaffectedShortMessage,UnaffectedLongMessage,
AffectedAdditionalInfoOnWebSite,UnaffectedAdditionalInfoOnWebSite,@CreatedBy,GETDATE(), HideUpStopMessages, HideDownStopMessages
FROM DisruptedRouteMessages WHERE RouteNo IN (SELECT RouteNo FROM ActiveDisruptedRoutes) 

-- Refer Below
-- Logic required, get all DisruptedCombinations that are affected by all Disruptions
-- and then get all the associated DisruptedCombinationRouteMessages
INSERT INTO ActiveDisruptedCombinationRouteMessages(RouteNoCombination,AffectedShortMessage,AffectedLongMessage,UnaffectedShortMessage,UnaffectedLongMessage,
AffectedAdditionalInfoOnWebSite,UnaffectedAdditionalInfoOnWebSite,AffectedDisplayType,UnaffectedDisplayType,CreatedBy,CreatedDate, HideUpStopMessages, HideDownStopMessages )
SELECT RouteNoCombination,AffectedShortMessage,AffectedLongMessage,UnaffectedShortMessage,UnaffectedLongMessage,
AffectedAdditionalInfoOnWebSite,UnaffectedAdditionalInfoOnWebSite,AffectedDisplayType,UnaffectedDisplayType,@CreatedBy,GETDATE(), HideUpStopMessages, HideDownStopMessages
FROM DisruptedCombinationRouteMessages WHERE (AffectedShortMessage IS NOT NULL OR UnAffectedShortMessage IS NOT NULL) 


--set status to published
UPDATE Disruptions 
	SET StatusId = 3,
	StartDateTime = GETDATE(),
	PublishedDateTime = GETDATE(),
	PublishedBy = @CreatedBy

WHERE StatusId = 1 OR StatusId = 2
 
 --if disruptions were cleared, set status to Archived
 UPDATE Disruptions 
	SET StatusId = 5,
	ClearedBy = @CreatedBy
WHERE StatusId = 4

INSERT INTO DisruptionsActivityLog(DisruptionId,RouteNo ,StopNo ,Activity ,CreatedBy ,CreatedDate)
VALUES  ( NULL ,NULL,NULL, 'Published all Disruptions data' ,@CreatedBy,GETDATE())

EXEC dbo.ArchiveTTMDisruptedRouteStops
EXEC dbo.ArchiveTTMDisruptedRouteCombinationStops


END

GO

DROP PROCEDURE [dbo].[GetTTMFOCDisruption] 

GO

CREATE PROCEDURE [dbo].[GetTTMFOCDisruption] 

@DisruptionId int

AS
BEGIN
DECLARE @focDisruptionId int


SET @focDisruptionId = (SELECT TT_DisruptionId FROM Disruptions WHERE Id = @DisruptionId)


--SELECT * FROM dbo.FOC_Disruptions WHERE TT_DisruptionId = @focDisruptionId
SELECT * FROM dbo.FOC_Disruptions_Full WHERE DisruptionId = @focDisruptionId

END

GO

DROP PROCEDURE [dbo].[CheckPendingDisrruptionClearance] 

GO

CREATE PROCEDURE [dbo].[CheckPendingDisrruptionClearance] 
	@ClearedBy  nvarchar(128)
AS
BEGIN
SET NOCOUNT ON;
--ClearTTMDisruption

--SELECT * FROM Disruptions WHERE StatusId <> 4 AND ExpiryTime > GETDATE()
DECLARE @focDisruptionId int
DECLARE @focCleared DATETIME
DECLARE @ExpiryTime DATETIME
DECLARE @MessageTTL INT


DECLARE @DisruptionId INT
DECLARE @pending CURSOR
DECLARE @DisruptionCleared DATETIME
DECLARE @InvalidDisruptionsCount INT

SET @pending = CURSOR FOR
SELECT Id, TT_DisruptionId, MessageTTL, ExpiryTime
FROM Disruptions
WHERE (StatusId <> 4 AND StatusId <> 5 AND ExpiryTime < GETDATE()) OR 
(StatusId <> 4 AND StatusId <> 5 AND TT_DisruptionId IS NOT NULL AND MessageTTL IS NOT NULL AND ExpiryTime IS NULL)
OPEN @pending
FETCH NEXT
FROM @pending INTO @DisruptionId, @focDisruptionId, @MessageTTL, @ExpiryTime
WHILE @@FETCH_STATUS = 0
BEGIN
--PRINT @DisruptionId
--PRINT @focDisruptionId
SET @InvalidDisruptionsCount = (SELECT count(Id) FROM Disruptions WHERE StatusId <> 4 AND StatusId <> 5 AND Valid = 0)

IF @focDisruptionId IS NULL OR @ExpiryTime IS NOT NULL BEGIN	
	IF @InvalidDisruptionsCount = 0 BEGIN
		--print 'clear this disruption'
		exec [ClearTTMDisruption] @DisruptionId, @ClearedBy
		exec [AutoClearDisruptionMessages] @ClearedBy
		exec [AutoClearDisruptionComboMessages] @ClearedBy
		exec [PublishTTMDisruptionsData] @ClearedBy
		INSERT INTO DisruptionsActivityLog(DisruptionId,RouteNo ,StopNo ,Activity ,CreatedBy ,CreatedDate)
		VALUES  ( @DisruptionId ,NULL,NULL, 'Disruption TTL expired and was cleared automatically' ,@ClearedBy,GETDATE())
	END
	ELSE BEGIN
		INSERT INTO DisruptionsActivityLog(DisruptionId,RouteNo ,StopNo ,Activity ,CreatedBy ,CreatedDate)
		VALUES  ( @DisruptionId ,NULL,NULL, 
		'Could not automatically clear disruption ' + CAST(@DisruptionId AS VARCHAR) + ' because there are currently ' + CAST(@InvalidDisruptionsCount AS VARCHAR) + ' invalid disruptions',
		@ClearedBy,GETDATE())		
	END
END
ELSE BEGIN
	--print 'there is an associated foc'
	SET @DisruptionCleared = (select DisruptionCleared from FOC_Disruptions_Full where DisruptionId = @focDisruptionId)
	IF @DisruptionCleared IS NULL BEGIN
		INSERT INTO DisruptionsActivityLog(DisruptionId,RouteNo ,StopNo ,Activity ,CreatedBy ,CreatedDate)
		VALUES  ( @DisruptionId ,NULL,NULL, 'Disruption associated FOC is not cleared, ignoring' ,@ClearedBy,GETDATE())
		--print 'ignoring'
	END
	ELSE BEGIN 
		--PRINT 'FOC is cleared set expiry time'
		UPDATE Disruptions SET ExpiryTime = DATEADD(minute, @MessageTTL, @DisruptionCleared), StatusId = 6 WHERE Id = @DisruptionId
		INSERT INTO DisruptionsActivityLog(DisruptionId,RouteNo ,StopNo ,Activity ,CreatedBy ,CreatedDate)
		VALUES  ( @DisruptionId ,NULL,NULL, 'Disruption associated FOC has been cleared, setting ExpiryTime automatically' ,@ClearedBy,GETDATE())
	END	
END

--SET @focDisruptionId = (SELECT TT_DisruptionId FROM Disruptions WHERE Id = @DisruptionId)



FETCH NEXT
FROM @pending INTO @DisruptionId, @focDisruptionId, @MessageTTL, @ExpiryTime
END
CLOSE @pending
DEALLOCATE @pending


END

GO

/*DROP PROCEDURE [dbo].[ClearRouteMessagesForDisruption] 

GO

CREATE PROCEDURE [dbo].[ClearRouteMessagesForDisruption] 
	@DisruptionId int,
	@ClearedBy  nvarchar(128)
AS
BEGIN
	SET NOCOUNT ON;
	--GetTTMRouteMessagesForDisruption
	--GetTTMOtherDisruptionAndRoutes
	DECLARE @ctest int

	CREATE TABLE #SharedRoutes
	(
	  ID int, DisruptionId int, RouteNo int, CreatedBy varchar(500), CreatedDate DateTime
	)
	INSERT #SharedRoutes EXEC GetTTMOtherDisruptionAndRoutes @DisruptionId

	CREATE TABLE #RouteMessages
	(
	  ID int, 
	  RouteNo int, 
	  AffectedShortMessage varchar(1000),
	  AffectedLongMessage varchar(1000),
	  UnaffectedShortMessage varchar(1000),
	  UnaffectedLongMessage varchar(1000),
	  AffectedAdditionalInfoOnWebsite int,
	  UnaffectedAdditionalInfoOnWebsite int,
	  LastUpdatedDisruptionId int,
	  UpdatedBy varchar(1000),
	  UpdatedDate DateTime,
	  HideDownStopMessages int,
	  HideUpStopMessages int
	)
	INSERT #RouteMessages EXEC GetTTMRouteMessagesForDisruption @DisruptionId
	--SELECT * FROM #RouteMessages WHERE RouteNo NOT IN (SELECT RouteNo FROM #SharedRoutes)

	DECLARE @messageId int
	DECLARE @messagesToClear CURSOR
	SET @messagesToClear = CURSOR FOR
	SELECT ID
	FROM #RouteMessages WHERE RouteNo NOT IN (SELECT RouteNo FROM #SharedRoutes)
	OPEN @messagesToClear
	FETCH NEXT
	FROM @messagesToClear INTO @messageId
	WHILE @@FETCH_STATUS = 0
	BEGIN
		--exec [ClearTTMRouteDisruptionMessages] @messageId, @ClearedBy
		--SET @ctest = (SELECT count(id) from route)
		print @messageId
	FETCH NEXT
	FROM @messagesToClear INTO @messageId
	END
	CLOSE @messagesToClear
	DEALLOCATE @messagesToClear


	DROP TABLE #RouteMessages
	DROP TABLE #SharedRoutes
END

GO*/


/*DROP PROCEDURE [dbo].[AutoClearDisruptionMessages]

GO

CREATE PROCEDURE [dbo].[AutoClearDisruptionMessages]
	@DisruptionId int,
	@ClearedBy  nvarchar(128)
AS
BEGIN
	DECLARE @messageId int
	DECLARE @routeNo int
	DECLARE @routeCur CURSOR
	SET @routeCur = CURSOR FOR
		select routeNo from disruptedRoutes 
		where RouteNo in (SELECT DISTINCT RouteNo FROM DisruptedRoutes WHERE DisruptionId = 18)
		group by routeNo 
		having count(routeNo) = 1
	OPEN @routeCur
	FETCH NEXT
	FROM @routeCur INTO @routeNo
	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		--SET @ctest = (SELECT count(id) from route)
		SET @messageId = (SELECT id FROM DisruptedRouteMessages WHERE RouteNo = @routeNo)
		exec [ClearTTMRouteDisruptionMessages] @messageId, @ClearedBy
		print @messageId
		--print @routeNo
	FETCH NEXT
	FROM @routeCur INTO @routeNo
	END
	CLOSE @routeCur
	DEALLOCATE @routeCur
END
GO*/
GO

DROP PROCEDURE [dbo].[AutoClearDisruptionMessages]

GO

CREATE PROCEDURE [dbo].[AutoClearDisruptionMessages]	
	@ClearedBy  nvarchar(128)
AS
BEGIN
SET NOCOUNT ON;
SELECT DISTINCT RouteNo INTO #cleared_route_nums
	FROM DisruptedRouteMessages
	WHERE RouteNo NOT IN (SELECT RouteNo FROM DisruptedRoutes)
UPDATE DisruptedRouteMessages
SET 
	AffectedShortMessage = NULL,	
	AffectedLongMessage = NULL,
	AffectedAdditionalInfoOnWebsite = 0,
	UnaffectedShortMessage = NULL,	
	UnaffectedLongMessage = NULL,
	UnaffectedAdditionalInfoOnWebsite = 0,
	HideUpStopMessages = 0,
	HideDownStopMessages = 0,
	UpdatedBy = @ClearedBy,
	UpdatedDate = GETDATE()
WHERE RouteNo NOT IN (SELECT RouteNo FROM DisruptedRoutes)

SELECT * FROM #cleared_route_nums

DROP TABLE #cleared_route_nums

END

GO

GO

DROP PROCEDURE [dbo].[AutoClearDisruptionComboMessages]

GO

CREATE PROCEDURE [dbo].[AutoClearDisruptionComboMessages]	
	@ClearedBy  nvarchar(128)
AS
BEGIN
SET NOCOUNT ON;

UPDATE DisruptedCombinationRouteMessages
SET 
	AffectedShortMessage = NULL,	
	AffectedLongMessage = NULL,
	AffectedAdditionalInfoOnWebsite = 0,
	AffectedDisplayType =1,		
	HideUpStopMessages = 0,
	HideDownStopMessages = 0,
	UpdatedBy = @ClearedBy,
	UpdatedDate = GETDATE()

WHERE RouteNoCombination COLLATE DATABASE_DEFAULT NOT IN (
	SELECT DISTINCT RC.RouteCombination COLLATE DATABASE_DEFAULT FROM RouteStops RS
	INNER JOIN Stops S
	ON RS.StopID = S.StopID
	INNER JOIN StopToRouteCombinationMapping M
	ON S.StopNo = M.StopNo
	INNER JOIN RouteCombinations RC
	ON M.RouteCombinationID = RC.ID
	INNER JOIN DisruptedStops DS
	ON S.StopNo = DS.StopNo
	)


	SELECT DISTINCT srdm.StopNo, UpStop, Data INTO #stops_by_combo_and_message
	FROM StopRouteCombinationDirectionMaps srdm  INNER JOIN DisruptedStops DS 
	ON DS.StopNo = srdm.StopNo
	cross apply (SELECT Data from dbo.Split(srdm.RouteCombination, ',')) b

	SELECT DISTINCT DM.* INTO #unaffected_routes_temp
	FROM RouteStops RS
	INNER JOIN Stops S
	ON RS.StopID = S.StopID
	INNER JOIN StopToRouteCombinationMapping M
	ON S.StopNo = M.StopNo
	INNER JOIN RouteCombinations RC
	ON M.RouteCombinationID = RC.ID
	INNER JOIN DisruptedCombinationRouteMessages DM
	ON RC.RouteCombination COLLATE DATABASE_DEFAULT = DM.RouteNoCombination COLLATE DATABASE_DEFAULT	
	WHERE S.StopNo NOT IN (
		SELECT StopNo FROM DisruptedStops DS
	)
	AND UpStop IN 
	(
		SELECT IsUpStop FROM DisruptedStops
	)
	AND RS.RouteNo IN
	(
		SELECT RouteNo FROM DisruptedRoutes 
	)	


	UPDATE DisruptedCombinationRouteMessages
	SET 
		UnaffectedShortMessage = NULL,	
		UnaffectedLongMessage = NULL,
		UnaffectedAdditionalInfoOnWebsite = 0,
		UnaffectedDisplayType =1,		
		HideUpStopMessages = 0,
		HideDownStopMessages = 0,
		UpdatedBy = @ClearedBy,
		UpdatedDate = GETDATE()

	WHERE RouteNoCombination COLLATE DATABASE_DEFAULT NOT IN (
	select distinct urm.RouteNoCombination COLLATE DATABASE_DEFAULT
		 From #unaffected_routes_temp urm
		 cross apply (SELECT Data from dbo.Split(RouteNoCombination, ',')) c
	)
	AND RouteNoCombination COLLATE DATABASE_DEFAULT NOT IN (
		SELECT DISTINCT RC.RouteCombination COLLATE DATABASE_DEFAULT FROM RouteStops RS
		INNER JOIN Stops S
		ON RS.StopID = S.StopID
		INNER JOIN StopToRouteCombinationMapping M
		ON S.StopNo = M.StopNo
		INNER JOIN RouteCombinations RC
		ON M.RouteCombinationID = RC.ID
		INNER JOIN DisruptedStops DS
		ON S.StopNo = DS.StopNo
	)

	DROP TABLE #unaffected_routes_temp

	DROP TABLE #stops_by_combo_and_message
END
select 'ok' as status
GO

GO

DROP PROCEDURE [dbo].[SetMessageDirectionHidden]

GO

CREATE PROCEDURE [dbo].[SetMessageDirectionHidden]	
	@MessageId int,
	@HideUpStopMessages int,
	@HideDownStopMessages int
AS
BEGIN
SET NOCOUNT ON;

UPDATE DisruptedRouteMessages SET 
HideUpStopMessages = @HideUpStopMessages,
HideDownStopMessages = @HideDownStopMessages
WHERE Id = @MessageId

select 'ok' as status

END

GO

DROP PROCEDURE [dbo].[SetCombinationMessageDirectionHidden]

GO

CREATE PROCEDURE [dbo].[SetCombinationMessageDirectionHidden]	
	@MessageId int,
	@HideUpStopMessages int,
	@HideDownStopMessages int
AS
BEGIN
SET NOCOUNT ON;

UPDATE DisruptedCombinationRouteMessages SET 
HideUpStopMessages = @HideUpStopMessages,
HideDownStopMessages = @HideDownStopMessages
WHERE Id = @MessageId

select 'ok' as status

END

GO

DROP  TABLE [dbo].[ActivityLogs]

GO

CREATE TABLE [dbo].[ActivityLogs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DisruptionId] int NULL,
	[Type] [nvarchar](128) NULL,
	[Message] [text] NULL,
	[CreatedBy] [nvarchar](128) NOT NULL,
	[CreatedDate] [datetime] NOT NULL
 CONSTRAINT [PK_ActivityLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/*-------------------- END STORED PROCS -------------------*/