﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TTMarketing.BLL.Controllers.Disruptions;
using TTMarketing.BLL.Model.Disruptions;

namespace TTMarketing.WebUI.Controls.Disruptions
{
    public partial class DisruptionSummaryListing : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!this.IsPostBack)
                BindDetails();
        }

        private void BindDetails()
        {

            IList<Disruption> listing = DisruptionController.GetActiveDisruptions();

            disruptionCount.Text = listing.Count().ToString();
            disruptionListing.DataSource = listing;
            disruptionListing.DataBind();
        }

    }
}