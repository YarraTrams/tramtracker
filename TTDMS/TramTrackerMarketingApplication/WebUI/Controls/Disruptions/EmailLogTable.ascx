﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmailLogTable.ascx.cs" Inherits="TTMarketing.WebUI.Controls.Disruptions.EmailLogTable" %>

<table id="events-table" border="0" width="100%" class="table table-striped table-bordered">
    <asp:Repeater runat="server" ID="logListing" onitemdatabound="logListing_ItemDataBound">
        <HeaderTemplate>
            <thead>
                <tr>
                    <th>Subject</th>
                    <th>Time</th>                
                </tr>
            </thead>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td> <asp:literal runat="server" ID="subject" /></td>
                <td> <asp:literal runat="server" ID="time" /></td>
            </tr>
        </ItemTemplate>            
    </asp:Repeater>
</table>
