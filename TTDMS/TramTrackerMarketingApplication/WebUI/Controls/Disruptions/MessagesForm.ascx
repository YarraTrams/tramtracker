﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MessagesForm.ascx.cs" Inherits="TTMarketing.WebUI.Controls.Disruptions.MessagesForm" %>
<div id="disruption_form_<%=disruption.Id %>" class="messages-form-holder">
    <table  class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>FOC ID</th>
                <th>Primary Route</th>                
                <th>Affecting Routes</th>
                <th>Status</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><%= disruption.Id.ToString()%></td>
                <td><%= disruption.Name %></td>
                <td><a id='foc_<%= disruption.TT_DisruptionId.ToString()%>' class="viewfoc" href="javascript:;"> <%= disruption.TT_DisruptionId.ToString()%></a></td>
                <td><%= disruption.PrimaryDisruptedRouteNo%></td>
                <td><%= disruption.AffectedRoutesString%></td>
                <td><%= disruption.Status%></td>
                <td><a id='d_<%= disruption.Id.ToString()%>' class="viewa" href="javascript:;">View Affected Stops</a></td>
            </tr>    
        </tbody>
        </table>
            <div class="row">
                <div class="span4 message-input-holder">                    
                    <fieldset>
                        <legend>Affected Stops Message</legend>
                        <div class="messages-holder form-horizontal">
                            <div class="control-group">
                                <label class="control-label">
                                    Predefined Messages                                          
                                </label>
                                <div class="controls">
                                    <select class="message-dropdown affected-message-dropdown" for="affected-short-message-<%= disruption.Id %>" data-long-message="affected-long-message-<%= disruption.Id %>">                                        
                                    </select>      
                                </div>
                            </div>
                        </div>
                        <label class="the-fix">
                            <div class="control-group message-input">
                                <textarea class="message-textarea short-message" rows="3" id="affected-short-message-<%= disruption.Id %>" json-field="affected_short_message" message-type="affected-short-message" data-long-message="affected-long-message-<%= disruption.Id %>" data-long-message-checkbox="long-messages-checkbox-<%= disruption.Id %>"></textarea>
                                <span class="help-inline">You must enter a value to update messages</span>
                            </div>
                            <div class="count-holder">
                                <a href="javascript:void(0)" class="pid-link pull-left" data-for="affected-short-message-<%= disruption.Id %>">PID Preview</a><span class="pull-right count" id="affected-short-message-count-<%= disruption.Id.ToString()%>"></span>
                            </div>
                        </label>
                    </fieldset>                    

                    <div class="update-btn-holder">
                        <button type="button" class="btn btn-primary pull-left message-save-btn" for="affected-short-message-<%= disruption.Id %>">Update</button>
                        <label class="checkbox pull-left">
                            <input type="checkbox" id="affected-short-message-<%= disruption.Id %>-override" />
                            Overwrite all existing messages
                        </label>
                    </div>                                       
                </div>
                    
                <div class="span4 pull-right message-input-holder">                    
                    <fieldset>
                        <legend>Unaffected Stops Message</legend>
                        <div class="messages-holder form-horizontal">
                            <div class="control-group">
                                <label class="control-label">
                                    Predefined Messages
                                </label>
                                <div class="controls">
                                    <select class="message-dropdown unaffected-message-dropdown" for="unaffected-short-message-<%= disruption.Id %>" data-long-message="unaffected-long-message-<%= disruption.Id %>">                                        
                                    </select>
                                </div>
                            </div>
                        </div>
                        <label class="the-fix">
                            <div class="control-group message-input">
                                <textarea class="message-textarea short-message" rows="3" id="unaffected-short-message-<%= disruption.Id %>" json-field="unaffected_short_message" message-type="unaffected-short-message" data-long-message="unaffected-long-message-<%= disruption.Id %>" data-long-message-checkbox="long-messages-checkbox-<%= disruption.Id %>"></textarea>
                                <span class="help-inline">You must enter a value to update messages</span>
                            </div>
                            <div class="count-holder">
                                <a href="javascript:void(0)" class="pid-link pull-left" data-for="unaffected-short-message-<%= disruption.Id %>">PID Preview</a><span class="pull-right count" id="unaffected-short-message-count-<%= disruption.Id.ToString()%>"></span>
                            </div>
                        </label>                                            

                        <div class="update-btn-holder">
                            <button type="button" class="btn btn-primary pull-left message-save-btn" for="unaffected-short-message-<%= disruption.Id %>">Update</button>
                            <label class="checkbox pull-left">
                                <input type="checkbox" id="unaffected-short-message-<%= disruption.Id %>-override" />
                                Overwrite all existing messages
                            </label>
                        </div>
                    </fieldset>
                </div>                
            </div>

            <div class="row">
                <div class="span9 message-input-holder">
                    <div class="long-message-checkbox-holder">
                        <label class="checkbox pull-left">
                            <input type="checkbox" class="show-long-messages" data-for="long-messages-row-<%= disruption.Id %>" id="long-messages-checkbox-<%= disruption.Id %>" data-disruption-id="<%= disruption.Id %>" />
                            Add long messages
                        </label>
                    </div>
                </div>
            </div>

            <div class="row long-messages-row" id="long-messages-row-<%= disruption.Id %>" style="display:none">
                <div class="span4 message-input-holder">
                    <!------ AFFECTED LONG MESSAGE INPUT ------>
                    <fieldset>
                        <legend>Affected Stops Long Message</legend>
                                        
                        <label class="the-fix">
                            <div class="control-group message-input">
                                <textarea class="message-textarea long-message" rows="3" id="affected-long-message-<%= disruption.Id %>" json-field="affected_long_message" message-type="affected-long-message"></textarea>
                                <span class="help-inline">You must enter a value to update messages</span>
                            </div>
                            <div class="count-holder">
                                <span class="pull-right long-count" id="affected-long-message-count-<%= disruption.Id.ToString()%>"></span>
                            </div>
                        </label>

                        <div class="update-btn-holder">
                            <button type="button" class="btn btn-primary pull-left message-save-btn" for="affected-long-message-<%= disruption.Id %>">Update</button>
                            <label class="checkbox pull-left">
                                <input type="checkbox" id="affected-long-message-<%= disruption.Id %>-override" />
                                Overwrite all existing messages
                            </label>
                        </div>
                    </fieldset>                  
                    <!------ END AFFECTED LONG MESSAGE INPUT ------> 
                </div>

                <div class="span4 pull-right message-input-holder">
                    <!------ AFFECTED LONG MESSAGE INPUT ------>   
                    <fieldset>
                        <legend>Unaffected Stops Long Message</legend>                 
                        <label class="the-fix">
                            <div class="control-group message-input">
                                <textarea class="message-textarea long-message" rows="3" id="unaffected-long-message-<%= disruption.Id %>" json-field="unaffected_long_message" message-type="unaffected-long-message"></textarea>
                                <span class="help-inline">You must enter a value to update messages</span>
                            </div>
                            <div class="count-holder">
                                <span class="pull-right long-count" id="unaffected-long-message-count-<%= disruption.Id.ToString()%>"></span>
                            </div>
                        </label>
                        <div class="update-btn-holder">
                            <button type="button" class="btn btn-primary pull-left message-save-btn" for="unaffected-long-message-<%= disruption.Id %>">Update</button>
                            <label class="checkbox pull-left">
                                <input type="checkbox" id="unaffected-long-message-<%= disruption.Id %>-override" />
                                Overwrite all existing messages
                            </label>
                        </div>
                    </fieldset> 
                    <!------ END AFFECTED LONG MESSAGE INPUT ------>
                </div>

            </div><!--END long message row-->
     
        
        <div class="row">
            <div class="span10">
                <script type="text/javascript">
                    disruption_messages["disruption_" + <%= disruption.Id %>] = {};
                </script>
                <h4>Disrupted Routes</h4>
                <div>Click the first direcion messages checkbox hold the shift key and click the last message checkbox to select multiple messages</div>
                <table id="disrupted-table-<%= disruption.Id %>" class="table table-striped table-bordered stops-table disrupted-table">
                    <thead>
                        <tr>
                            <!--<th>&nbsp;</th>-->
                            <th>Route Specific/Stop Corridors</th>
                            <th>Direction</th>
                            <th>Affected Msg.</th>
                            <th>Unaffected Msg.</th>
                            <th>Direction Messages</th>
                            <th>Shared Disruptions</th>
                        </tr>
                    </thead>
                    <tbody>
                            <asp:Repeater runat="server" ID="routeList">
                            <ItemTemplate>
                                    <script type="text/javascript">
                                        disruption_messages["disruption_" + <%= disruption.Id %>]["route_" + <%# Eval("Id")%>] = {
                                            id : '<%# Eval("Id")%>',
                                            disruption_id : '<%= disruption.Id.ToString()%>',
                                            type: "route",
                                            route_name : <%# Eval("RouteNo")%>,
                                            affected_short_message : <%# JsonEncode(Eval("AffectedShortMessage"))%>,
                                            affected_long_message : <%# JsonEncode(Eval("AffectedLongMessage"))%>,
                                            unaffected_short_message : <%# JsonEncode(Eval("UnaffectedShortMessage"))%>,
                                            unaffected_long_message : <%# JsonEncode(Eval("UnaffectedLongMessage"))%>,
                                            affected_short_message_already_set : <%#JsonEncode((Eval("AffectedShortMessage") != null ? true : false)) %>,
                                            unaffected_short_message_already_set : <%#JsonEncode((Eval("UnaffectedShortMessage") != null ? true : false)) %>,
                                            affected_long_message_already_set : <%#JsonEncode((Eval("AffectedLongMessage") != null ? true : false)) %>,
                                            unaffected_long_message_already_set : <%#JsonEncode((Eval("UnaffectedLongMessage") != null ? true : false)) %>,
                                            hide_down_stop_messages : <%#JsonEncode(GetHideStopsValue(Eval("UpStopsCount"), Eval("DownStopsCount"), Eval("HideDownStopMessages"))) %>,
                                            hide_up_stop_messages : <%#JsonEncode(GetHideStopsValue(Eval("UpStopsCount"), Eval("DownStopsCount"), Eval("HideUpStopMessages"))) %>,
                                            unaffected : false
                                        };
                                    </script>
                                <tr class="<%#(Eval("AffectedShortMessage") != null && !IsOwner(Eval("LastUpdatedDisruptionId")) ? "warning" : "") %>" id='route_<%= disruption.Id%>_<%# Eval("Id")%>' json-field="route_<%# Eval("Id")%>" data-toggle="tooltip" data-placement="left" title="" data-original-title="">
                                    <!--<td><span class="icon-remove">&nbsp;</span></td>-->
                                    <td><%# Eval("RouteNo")%></td>
                                    <td>
                                        <%# GetDirection(Eval("UpStopsCount"), Eval("DownStopsCount"))%>
                                    </td>
                                    <td class="affected-short-message-status-column"><%#GetMessageStatus(Eval("AffectedShortMessage"), Eval("LastUpdatedDisruptionId")) %></td>
                                    <td class="unaffected-short-message-status-column"><%#GetMessageStatus(Eval("UnaffectedShortMessage"), Eval("LastUpdatedDisruptionId")) %></td>
                                    <td>
                                        <label class="checkbox" for="route_stops_<%= disruption.Id %>_<%# Eval("Id")%>">
                                            <input type="checkbox" <%# GetHideStopsCheckboxValue(Eval("UpStopsCount"), Eval("DownStopsCount"), Eval("HideDownStopMessages"), Eval("HideUpStopMessages")) %> json-field="<%# GetJsonField(Eval("UpStopsCount"), Eval("DownStopsCount")) %>" class="directions_checkbox" style="display:<%# DisplayDirectionCheckbox(Eval("UpStopsCount"), Eval("DownStopsCount")) %>" id="route_stops_<%= disruption.Id %>_<%# Eval("Id")%>" />
                                            <%# GetDirectionCheckboxText(Eval("UpStopsCount"), Eval("DownStopsCount")) %>
                                        </label>
                                    </td>
                                    <td><asp:literal runat="server" ID="disruptionStr" /></td>                                        
                                </tr>                                                                                
                            </ItemTemplate>
                            </asp:Repeater>
                            <asp:Repeater runat="server" ID="routeCombinationList">
                                <ItemTemplate>
                                    <script type="text/javascript">
                                        disruption_messages["disruption_" + <%= disruption.Id %>]["combo_" + <%# Eval("Id")%>] = {
                                            id : '<%# Eval("Id")%>',
                                            disruption_id : '<%= disruption.Id.ToString()%>',
                                            type: "combo",
                                            route_name : <%# JsonEncode(Eval("RouteNoCombination"))%>,
                                            affected_short_message : <%# JsonEncode(Eval("AffectedShortMessage"))%>,
                                            affected_long_message : <%# JsonEncode(Eval("AffectedLongMessage"))%>,
                                            unaffected_short_message : <%# JsonEncode(Eval("UnaffectedShortMessage"))%>,
                                            unaffected_long_message : <%# JsonEncode(Eval("UnaffectedLongMessage"))%>,
                                            affected_short_message_already_set : <%#JsonEncode((Eval("AffectedShortMessage") != null ? true : false)) %>,
                                            unaffected_short_message_already_set : <%#JsonEncode((Eval("UnaffectedShortMessage") != null ? true : false)) %>,
                                            affected_long_message_already_set : <%#JsonEncode((Eval("AffectedLongMessage") != null ? true : false)) %>,
                                            unaffected_long_message_already_set : <%#JsonEncode((Eval("UnaffectedLongMessage") != null ? true : false)) %>,
                                            hide_down_stop_messages : <%#JsonEncode(GetHideStopsValue(Eval("UpStopsCount"), Eval("DownStopsCount"), Eval("HideDownStopMessages"))) %>,
                                            hide_up_stop_messages : <%#JsonEncode(GetHideStopsValue(Eval("UpStopsCount"), Eval("DownStopsCount"), Eval("HideUpStopMessages"))) %>,
                                            unaffected : <%#JsonEncode(Eval("IsUnaffectedByCurrentDisruption"))%>
                                        };
                                    </script>
                                    <tr class="<%#(Eval("AffectedShortMessage") != null && !IsOwner(Eval("LastUpdatedDisruptionId")) ? "warning" : "") %>" id='combo_<%= disruption.Id%>_<%# Eval("Id")%>' json-field="combo_<%# Eval("Id")%>" data-toggle="tooltip" data-placement="left" title="" data-original-title="">
                                        <!--<td><span class="<%#(GetBool(Eval("IsUnaffectedByCurrentDisruption")) == false ? "icon-remove" : "icon-ok") %>">&nbsp;</span></td>-->
                                        <td><%# Eval("RouteNoCombination")%></td>
                                        <td>
                                            <%# GetDirection(Eval("UpStopsCount"), Eval("DownStopsCount"))%>
                                        </td>
                                        <td class="affected-short-message-status-column"><%#(Eval("IsUnaffectedByCurrentDisruption").ToString() == "True" ? "N/A" : GetMessageStatus(Eval("AffectedShortMessage"), Eval("LastUpdatedDisruptionId"))) %></td>
                                        <td class="unaffected-short-message-status-column"><%#GetMessageStatus(Eval("UnaffectedShortMessage"), Eval("LastUpdatedDisruptionId")) %></td>
                                        <td>
                                            <label class="checkbox" for="combo_stops_<%= disruption.Id %>_<%# Eval("Id")%>">
                                                <input type="checkbox" <%# GetHideStopsCheckboxValue(Eval("UpStopsCount"), Eval("DownStopsCount"), Eval("HideDownStopMessages"), Eval("HideUpStopMessages")) %> json-field="<%# GetJsonField(Eval("UpStopsCount"), Eval("DownStopsCount")) %>" class="directions_checkbox" style="display:<%# DisplayDirectionCheckbox(Eval("UpStopsCount"), Eval("DownStopsCount")) %>" id="combo_stops_<%= disruption.Id %>_<%# Eval("Id")%>" />
                                                <%# GetDirectionCheckboxText(Eval("UpStopsCount"), Eval("DownStopsCount")) %>
                                            </label>
                                        </td>    
                                        <td><asp:literal runat="server" ID="disruptionStr" /></td>                                        
                                    </tr>                                                    
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                </table>
            </div><!--END span for disruptions table-->
        </div><!--END row for disruptions table-->
</div>