﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DisruptionSummaryListing.ascx.cs" Inherits="TTMarketing.WebUI.Controls.Disruptions.DisruptionSummaryListing" %>
<h5>There are currently <asp:Literal runat="server" ID="disruptionCount"/> Disruptions within the system</h5>
<table  class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Id</th>
            <th>FOC ID</th>
            <th>Name</th>
            <th>Status</th>
            <th>Affecting Routes</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <asp:Repeater runat="server" ID="disruptionListing">
        <ItemTemplate>
            <tr>
                <td><%# Eval("Id")%></td>
                <td><a id="foc_<%# Eval("TT_DisruptionId")%>" class="viewfoc" href="javascript:;"> <%# Eval("TT_DisruptionId")%></a></td>
                <td><%# Eval("Name")%></td>
                <td><%# Eval("Status")%></td>
                <td><%# Eval("AffectedRoutesString")%></td>
                <td><a id="d_<%# Eval("Id")%>" class="viewa" href="javascript:;">View Affected Stops</a></td>
            </tr>    
        </ItemTemplate>
    </asp:Repeater>
</table>

 <div class="modal hide fade modal-wide" id="affected-stops">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>Affected Stops</h3>
        </div>
        <div class="modal-body">
            
            <table id="affectedStopData" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Route No</th>
                        <th>Stop</th>
                        <th>Tramtracker Id</th>            
                        <th>Direction</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>            
        </div>
    </div>

    <div class="modal hide fade modal-wide" id="foc-disruption">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>FOC Details</h3>
        </div>
        <div class="modal-body">
            
            <table id="focData" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>FOC Disruption Id</th>
                        <th>Created</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>            
        </div>
    </div>
       
