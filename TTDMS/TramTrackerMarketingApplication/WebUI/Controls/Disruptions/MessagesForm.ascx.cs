﻿using System;
using System.Text;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.UI;
using System.Web.UI.WebControls;
using TTMarketing.BLL.Controllers.Disruptions;
using TTMarketing.BLL.Model.Disruptions;

namespace TTMarketing.WebUI.Controls.Disruptions
{
    public partial class MessagesForm : System.Web.UI.UserControl
    {
        public string primaryRouteNo = "";
        public string _currentUser = "";
        public Disruption disruption = null;
        public IList<DisruptionStop> affected_stops = null;

        protected void Page_Load(object sender, EventArgs e)
        {            
                int id = Int32.Parse(Request.QueryString["e"].ToString());
                disruption = DisruptionController.GetDisruptionById(id);
                primaryRouteNo = disruption.PrimaryDisruptedRouteNo.ToString();
                affected_stops = DisruptionStopsController.GetAffectedStopsByDisruptionId(id);

                routeList.ItemDataBound += new RepeaterItemEventHandler(routeList_ItemDataBound);
                routeCombinationList.ItemDataBound += new RepeaterItemEventHandler(routeCombinationList_ItemDataBound);

                BindRouteMessages();

                _currentUser = "NullWindowsUser";

                
        }

        void routeCombinationList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                RouteCombinationMessage msg = e.Item.DataItem as RouteCombinationMessage;

                StringBuilder sb = new StringBuilder();
                foreach (Disruption dr in msg.CurrentDisruptions)
                {
                    if (dr.Id != this.disruption.Id)
                    {
                        sb.Append("#" + dr.Id.ToString() + ",");
                    }

                }

                string val = sb.ToString();
                if (val.Length > 1)
                    val = val.Substring(0, val.Length - 1);


                Literal disruptionStr = e.Item.FindControl("disruptionStr") as Literal;
                disruptionStr.Text = val;

            }
        }      

        void routeList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {             
                RouteMessage msg = e.Item.DataItem as RouteMessage;
                StringBuilder sb = new StringBuilder();
                foreach (Disruption dr in msg.CurrentDisruptions)
                {
                    if (dr.Id != this.disruption.Id)
                    {
                        sb.Append("#" + dr.Id.ToString() + ",");
                    }
                }

                string val = sb.ToString();
                if (val.Length > 1)
                    val = val.Substring(0, val.Length - 1);

                Literal disruptionStr = e.Item.FindControl("disruptionStr") as Literal;
                disruptionStr.Text = val;

            }



        }

        private void BindRouteMessages()
        {            
            
            IList<RouteMessage> list = DisruptionMessagesController.GetAllDisruptedRouteMessagesByDisruptionId(disruption.Id);
            routeList.DataSource = list;
            routeList.DataBind();

            //IList<RouteCombinationMessage> clist = DisruptionMessagesController.GetAllAffectedDisruptedRouteCombinationMessages();
            IList<RouteCombinationMessage> clist = DisruptionMessagesController.GetAllDisruptedRouteCombinationMessagesByDisruptionId(disruption.Id);

            IList<RouteCombinationMessage> alist = DisruptionMessagesController.GetAllUnaffectedDisruptedRouteCombinationMessagesByDisruptionId(disruption.Id);
            
            foreach (RouteCombinationMessage msg in alist)
            {               
                if (!clist.Any(m => m.Id == msg.Id))
                {                                            
                    clist.Add(msg);                        
                }
            }
            //clist.
            //clist.Union(alist).ToList();
            routeCombinationList.DataSource = clist;
            //routeCombinationList.DataSource = (IList<RouteCombinationMessage>)clist.Union(alist).ToList();
            //routeCombinationList.DataSource = clist;
            routeCombinationList.DataBind();            
        }

        public string GetDirection(object UpStopsCount, object DownStopsCount)
        {
            int UpStopsCountInt = (int)UpStopsCount;
            int DownStopsCountInt = (int)DownStopsCount;

            string direction = "";
            if (UpStopsCountInt > 0 && DownStopsCountInt > 0)
            {
                direction = "Both";
            }
            else if (UpStopsCountInt > 0)
            {
                direction = "Up";
            }
            else if (DownStopsCountInt > 0)
            {
                direction = "Down";
            }

            return direction;
        }

        public bool IsOwner(object LastUpdatedDisruptionIdObj)
        {
            int? LastUpdatedDisruptionId = (int?)LastUpdatedDisruptionIdObj;
            if (LastUpdatedDisruptionId == null)
            {
                return false;
            }

            if (LastUpdatedDisruptionId == this.disruption.Id)
            {
                return true;
            }
            
            return false;
        }

        public string GetMessageStatus(object val, object LastUpdatedDisruptionIdObj)
        {
            string message = (string)val;
            if (message == null || message == "")
            {
                return "Empty";
            }
            if (IsOwner(LastUpdatedDisruptionIdObj))
            {
                return "Message Saved";
            }

            return "Existing Message";
        }

        public string GetDirectionCheckboxText(object UpStopsCount, object DownStopsCount)
        {
            int UpStopsCountInt = (int)UpStopsCount;
            int DownStopsCountInt = (int)DownStopsCount;

            string direction = "";
            if (UpStopsCountInt > 0 && DownStopsCountInt > 0)
            {
                direction = "Both directions affected";
            }
            else if (UpStopsCountInt > 0)
            {
                direction = "Hide for down stops";
            }
            else if (DownStopsCountInt > 0)
            {
                direction = "Hide for up stops";
            }

            return direction;
        }

        public string DisplayDirectionCheckbox(object UpStopsCount, object DownStopsCount)
        {
            int UpStopsCountInt = (int)UpStopsCount;
            int DownStopsCountInt = (int)DownStopsCount;

            string direction = "";
            if (UpStopsCountInt > 0 && DownStopsCountInt > 0)
            {
                direction = "none";
            }
            else if (UpStopsCountInt > 0)
            {
                direction = "inline-block";
            }
            else if (DownStopsCountInt > 0)
            {
                direction = "inline-block";
            }

            return direction;
        }

        public string GetJsonField(object UpStopsCount, object DownStopsCount)
        {
            int UpStopsCountInt = (int)UpStopsCount;
            int DownStopsCountInt = (int)DownStopsCount;

            string direction = "";
            if (UpStopsCountInt > 0 && DownStopsCountInt > 0)
            {
                direction = "";
            }
            else if (UpStopsCountInt > 0)
            {
                direction = "hide_down_stop_messages";
            }
            else if (DownStopsCountInt > 0)
            {
                direction = "hide_up_stop_messages";
            }

            return direction;
        }

        public bool GetHideStopsValue(object UpStopsCount, object DownStopsCount, object CurrentVal)
        {
            int UpStopsCountInt = (int)UpStopsCount;
            int DownStopsCountInt = (int)DownStopsCount;

            if (UpStopsCountInt > 0 && DownStopsCountInt > 0)
            {
                return false;
            }
            else
            {
                return (bool)CurrentVal;
            }
        }

        public string GetHideStopsCheckboxValue(object UpStopsCount, object DownStopsCount, object CurrentDownVal, object CurrentUpVal)
        {
            int UpStopsCountInt = (int)UpStopsCount;
            int DownStopsCountInt = (int)DownStopsCount;

            string direction = "";
            if (UpStopsCountInt > 0 && DownStopsCountInt > 0)
            {
                direction = "";
            }
            else if (UpStopsCountInt > 0)
            {
                if ((bool)CurrentDownVal == true)
                {
                    direction = "checked=\"true\"";
                }                
            }
            else if (DownStopsCountInt > 0)
            {
                if ((bool)CurrentUpVal == true)
                {
                    direction = "checked=\"true\"";
                }     
            }

            return direction;
        }

        public bool GetBool(object val) {
            return (bool)val;
        }

        public string JsonEncode(object val)
        {
            //string value = (string)val;

            return Json.Encode(val);            
        }
    }
}