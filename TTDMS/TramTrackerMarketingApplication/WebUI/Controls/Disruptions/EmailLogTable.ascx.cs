﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.UI;
using System.Web.UI.WebControls;
using TTMarketing.BLL.Controllers.Disruptions;
using TTMarketing.BLL.Model.Disruptions;
using System.Web.Script.Serialization;

namespace TTMarketing.WebUI.Controls.Disruptions
{
    public partial class EmailLogTable : System.Web.UI.UserControl
    {
        public Disruption disruption = null;
        public List<dynamic> log_data = new List<dynamic>();

        protected void Page_Load(object sender, EventArgs e)
        {
            int id = Int32.Parse(Request.QueryString["disruption_id"].ToString());
            disruption = DisruptionController.GetDisruptionById(id);

            IList<ActivityLog> logs = ActivityLogController.GetActivityLogs(id, "Email");
            
            foreach (ActivityLog log in logs)
            {
                var json = Json.Decode(log.Message);
                json.time = log.CreatedDate;
                log_data.Add(json);
            }
            logListing.DataSource = log_data;
            logListing.DataBind();
        }

        protected void logListing_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {            
            if (e.Item.ItemIndex != -1)
            {
                dynamic json = log_data[e.Item.ItemIndex];                
                Literal subjectStr = e.Item.FindControl("subject") as Literal;
                subjectStr.Text = json.subject;

                Literal timeStr = e.Item.FindControl("time") as Literal;
                timeStr.Text = json.time.ToString() ;
            }            
        }
    }
}