﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TTMarketing.WebUI.Default" MasterPageFile="Template.Master" Title="Yarra Trams - TTDMS - Home" %>

<asp:Content ID="Content1" ContentPlaceHolderId="PageBody" runat="server">

    <div class="container">
        <div class="hero-unit text-center">
            <h1>Welcome to the Yarra Trams TTDMS</h1>
            <h4>To manage route disruption messages please click the button below</h4>
            <p>
                <a href="Disruptions/Listing.aspx" class="btn btn-primary btn-large">Manage Disruptions</a>
            </p>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content2" runat="server"  ContentPlaceHolderID="head">
    <script type="text/javascript">
        //this is to highlight the correct item in the bootstrap nav
        $(document).ready(function () {
            $('#home-nav').addClass("active");
        });
    </script>
</asp:Content>


