﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace TTMarketing.WebUI
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            AreaRegistration.RegisterAllAreas();
            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs
            Exception ex = Server.GetLastError();

            string mailserver = ConfigurationManager.AppSettings["MailServer"].ToString();
            string receiver_config = ConfigurationManager.AppSettings["ExceptionRecipients"].ToString();
            string[] receivers = receiver_config.Split(';');
            YarraTrams.Library.Mailer.ExceptionMailer mailer = new YarraTrams.Library.Mailer.ExceptionMailer();
            YarraTrams.Library.Mailer.ExceptionMailer.EmailException("TTDMS Admin", mailserver, receivers, ex);

            Server.Transfer("~/Disruptions/Error.aspx");            
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{resource}.aspx/{*pathInfo}");
            //routes.IgnoreRoute("*.js");
            routes.IgnoreRoute("{*staticfile}", new { staticfile = @".*/.(css|js|gif|jpg|map)(/.*)?" });

            //see if we can add a map route to default.aspx

            routes.MapRoute(
                "Default", // Route naem
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "TicketOutlets", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );
            
        }
    }
}