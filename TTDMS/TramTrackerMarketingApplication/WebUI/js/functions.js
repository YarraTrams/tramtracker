﻿function chk(obj)
{
var O = obj;
var pI = "";
for(var propertyName in O){
pI += propertyName + " = " + O[propertyName] + " <br /> ";
}
    if(document.all)
    {
        alert(pI);
    }
    else
    {
        document.write('<pre>' + pI + " <br /> _______________________________________________</pre><br />")
    }
}

function joinIgnoreNull(theArray, delimeter)
{
    var result = "";
    for(crazy = 0; crazy < theArray.size(); crazy++)
    {
        if(theArray[crazy] != null)
        {
            if(result == "")
            {
                result += theArray[crazy];
            }
            else
            {
                result += delimeter + theArray[crazy];
            }
        }
    }
    
    return result;
}


function getQueryStringVariable(name)
{
	var url = unescape(document.location.href);
	
	if(url.indexOf('?') > -1)
	{
		list1 = url.split('?');
		list2 = list1[1].split('&');
		
		for(p = 0; p < list2.length; p++)
		{
			list3 = list2[p].split('=');
			if(list3[0] == name)
			{
				return list3[1];
			}
			
		}
	}
	
	return '';
}

function indexof(searcharray, value) {
    for (i = 0; i < searcharray.length; i++) {
        if (searcharray[i].StopNo == value) {
            return i;
        }
    }

    return -1;
}

function removeChildrenFromNode(node)
{
	if(node != null)
	{
		return;
	}
	
	
	while(node.hasChildNodes())
	{
		node.removeChild(node.firstChild);
	}
}

function stripe(element, color)
{
    for(i = 0; i < element.childNodes.length; i++)
    {
        if(i % 2 == 0)
        {        
            element.childNodes[i].style.backgroundColor = color;
        }
        else
        {
            element.childNodes[i].style.backgroundColor = '#ffffff';
        }
    }        
}


Array.prototype.removeItems = function(itemsToRemove) {

    if (!/Array/.test(itemsToRemove.constructor)) {
        itemsToRemove = [ itemsToRemove ];
    }

    var j;
    for (var i = 0; i < itemsToRemove.length; i++) {
        j = 0;
        while (j < this.length) {
            if (this[j] == itemsToRemove[i]) {
                this.splice(j, 1);
            } else {
                j++;
            }
        }
    }
}


Array.prototype.removeAt = function (iIndex /*:int*/) /*:variant*/ {
    var vItem = this[iIndex];
    if (vItem) {
        this.splice(iIndex, 1);
    }
    return vItem;
};

var showModal = function ($dialog) {
    var $currentModals = $('.modal.in');
    if ($currentModals.length > 0) { // if we have active modals
        $currentModals.one('hidden', function () {
            // when they've finished hiding
            $dialog.modal('show');
            $dialog.one('hidden', function () {
                // when we close the dialog
                $currentModals.modal('show');

            });
        }).modal('hide');
    } else { // otherwise just simply show the modal
        $dialog.modal('show');
    }
};