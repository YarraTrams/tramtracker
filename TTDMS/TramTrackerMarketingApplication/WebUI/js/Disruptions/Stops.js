﻿var stopListView = {

    lastStopItemChecked: null,
    lastDisplayItemChecked: null,
    isUpDirection: true,
    disruptionId: getQueryStringVariable('e'),

    init: function () {        
        this.loadRouteList();
        this.loadStopsForRoute(primaryRouteNo);

        $(document.body).on('click', '#group-select', this.toggleAllStops);
        $(document.body).on('click', '.chkbox', this.handleStopClick);        
        $(document.body).on('click', "#next-btn", this.handleNextClick);
        $(document.body).on('click', '.displayChkbox', this.handleDisplayClick);

       

    }, //end init

    loadRouteList: function () {

        var urlJSON = "../Services/DisruptionsService.svc/GetAllRoutes";
        $.getJSON(urlJSON, function (data) {
            //added here to handel error
            if (data.isError == true) {
                alert(data.responseString);
                return false;
            }
            $('<option value="">Please Select</option>').appendTo("#cboRoute")
            var description = '';
            $.each(data.responseObject, function (i, item) {
                if (item.RouteNo == 35)
                    description = 'City Circle'
                else
                    description = item.DownDestination + ' to ' + item.UpDestination;

                var selected = (String(item.RouteNumber) === String(primaryRouteNo)) ? "selected" : "";
                $('<option ' + selected + ' value="' + item.RouteNumber + '">' + item.RouteNumber + ' ' + item.Description + '</option>').appendTo("#cboRoute")

            });
        });
    }, //end loadRouteList


    loadStopsForRoute: function (r) {

        $("#stops").show();
        var noCache = new Date().getTime();
        var strUrl = "../Services/DisruptionsService.svc/GetStopsForDisruptionRoute/?e=" + stopListView.disruptionId + "&r=" + r + "&ts=" + noCache;

        var upstops, downstops;
        stopListView.showStopMask();
        var jqxhr = $.getJSON(strUrl, function (data) {

            upstops = data.responseObject.UpStops;
            downstops = data.responseObject.DownStops;
            stopListView.loadStops(upstops);
        }).always(function () { stopListView.hideStopMask(); });

    }, //end loadStopsForRoute


    loadStops: function (stop_data) {

        var noCache = new Date().getTime();
        $("#message").html('');
        $("#selectable").empty();

        var htmlText = '<table class="table table-striped table-bordered stops-table">';
        htmlText += '<thead>';
        htmlText += '<tr><th><input type="checkbox" id="group-select" name="group-select" /></th><th>Stop name & Description</th><th>Tracker Id</th><th>Passing routes</th><th>Don\'t Display Predictions</th></tr>';
        htmlText += '</thead>';
        htmlText += '<tfoot>';
        htmlText += '<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>';
        htmlText += '</tfoot>';
        htmlText += '<tbody>';

        var isCheckedDisplay = "chkOff";
        var isCheckedStopChecked = "";
        var isCheckedShuttleCheckbox = "";

        var predictionCheckboxDisplay = "";
        var isPredictionsChecked = "";

        var isGray = "";

        for (var i = 0; i < stop_data.length; i++) {
            if (!stop_data[i].TramTrackerAvailable) {
                isCheckedDisplay = "chkOn";
                isCheckedStopChecked = "checked";
                predictionCheckboxDisplay = 'style="display:inline"';
            }
            else {
                isCheckedDisplay = "chkOff";
                isCheckedCheckbox = "";
                isCheckedStopChecked = "";
                predictionCheckboxDisplay = 'style="display:none"';
            }

            if (stop_data[i].DisplayPredictions) {               
                isPredictionsChecked = "";
            }
            else {               
                isPredictionsChecked = 'checked="checked"';
            }

            //create row
            var stop_row = '<tr style="' + isGray + '"><td id="c' + i + '" class="' + isCheckedDisplay + '"><div style="display:inline;"><input class="chkbox" ' +
            isCheckedStopChecked + ' " type="checkbox" name="' + stop_data[i].TrackerID + '" id="' + i + '" /></div></td><td id="l' + i + '" ><label class="selectlabel" for="' + i
            + '">' + stop_data[i].Description + '</label></td><td>' + stop_data[i].TrackerID + '</td><td id="m' + i + '">';

            var missingRte = '&nbsp;';
            if (!stop_data[i].TramTrackerAvailable) {
                missingRte = stop_data[i].MissingRoutes.join();
            }
            stop_row += missingRte + "</td>";
            stop_row += '<td><input class="displayChkbox" ' + predictionCheckboxDisplay + isPredictionsChecked + ' type="checkbox" name="' + stop_data[i].TrackerID + '" id="' + i + '" /></td></tr>';
            htmlText += stop_row;
        }
        htmlText += '</tbody>';
        htmlText += '</table>';

        $("#selectable").append(htmlText);
        $("#stops").show();
        stopListView.setCheckBox();

        
        //$(".stops-table").fixedHeaderTable("show");
        $(".stops-table").fixedHeaderTable({ footer: false, cloneHeadToFoot: false, fixedColumn: false });

    }, //end loadStops


    toggleAllStops: function (event) {
        
        var noCache = new Date().getTime();
        if (this.checked) {
            var mrUrl = "../Services/DisruptionsService.svc/AddAllDisruptionEffectedRouteStops/?e=" + stopListView.disruptionId + "&r=" + $("#cboRoute").val() + "&d=" + stopListView.isUpDirection + "&cu=" + ttmcurrentUser + "&ts=" + noCache;

            $.ajax({ url: mrUrl, dataType: "json", beforeSend: function () { stopListView.showStopMask() }, success: function (d) {
                //added here to handel error
                if (d.isError == true) {
                    alert(d.responseString);
                    return false;
                }
                var strUrl = "../Services/DisruptionsService.svc/GetStopsForDisruptionRoute/?e=" + stopListView.disruptionId + "&r=" + $("#cboRoute").val() + "&ts=" + noCache;

                $.getJSON(strUrl, function (data) {

                    if (stopListView.isUpDirection) {
                        upstops = data.responseObject.UpStops;
                        stopListView.loadStops(upstops);
                    }
                    else {
                        downstops = data.responseObject.DownStops;
                        stopListView.loadStops(downstops);
                    }
                    stopListView.hideStopMask();

                });

            }, async: true
            });
        }
        else {
            var mrUrl = "../Services/DisruptionsService.svc/DeleteAllDisruptionEffectedRouteStops/?e=" + stopListView.disruptionId + "&r=" + $("#cboRoute").val() + "&d=" + stopListView.isUpDirection + "&cu=" + ttmcurrentUser + "&ts=" + noCache;

            $.ajax({
                url: mrUrl, dataType: "json", beforeSend: function () { stopListView.showStopMask() }, success: function (d) {
                    //added here to handel error
                    if (d.isError == true) {
                        alert(d.responseString);
                        return false;
                    }
                var strUrl = "../Services/DisruptionsService.svc/GetStopsForDisruptionRoute/?e=" + stopListView.disruptionId + "&r=" + $("#cboRoute").val() + "&ts=" + noCache;

                $.getJSON(strUrl, function (data) {

                    if (stopListView.isUpDirection) {
                        upstops = data.responseObject.UpStops;
                        stopListView.loadStops(upstops);
                    }
                    else {
                        downstops = data.responseObject.DownStops;
                        stopListView.loadStops(downstops);
                    }
                    stopListView.hideStopMask();

                });

            }, async: true
            });
        }
    }, //end toggleAllStops

    handleStopClick: function (event) {

        if (!stopListView.lastStopItemChecked && event.shiftKey) {

            stopListView.lastStopItemChecked = this;
            return;
        }

        if (event.shiftKey) {

            var start = $('.chkbox').index(this);
            var end = $('.chkbox').index(stopListView.lastStopItemChecked);
            var t1 = new Date();
            for (m = Math.min(start, end); m <= Math.max(start, end); m++) {
                var index = m;
                $('.chkbox')[index].checked = stopListView.lastStopItemChecked.checked;

                if ($('.chkbox')[index].checked) {
                    stopListView.saveStop(index, true, $('.chkbox')[index].name);
                }
                else {
                    stopListView.saveStop(index, false, $('.chkbox')[index].name);
                }

            }
            var t2 = new Date();

            var dif = t1.getTime() - t2.getTime()

            var Seconds_from_T1_to_T2 = dif / 1000;
            var Seconds_Between_Dates = Math.abs(Seconds_from_T1_to_T2);

        }
        else {
            if (this.checked) {
                stopListView.saveStop(this.id, true, this.name);
            }
            else {
                stopListView.saveStop(this.id, false, this.name);
            }
        }
        stopListView.lastStopItemChecked = this;
        if (!this.checked) {
            stopListView.setCheckBox();
        }            
    }, //end handleStopClick

    handleDisplayClick: function (event) {
        if (!stopListView.lastDisplayItemChecked && event.shiftKey) {
            stopListView.lastDisplayItemChecked = this;
            return;
        }
        if (event.shiftKey) {

            var start = $('.displayChkbox').index(this);
            var end = $('.displayChkbox').index(stopListView.lastDisplayItemChecked);
            var boxes = $('.displayChkbox');
            var t1 = new Date();
            for (m = Math.min(start, end); m <= Math.max(start, end); m++) {
                var index = m;
                var the_box = $(boxes[index]);
                if (the_box.is(":visible")) {
                    boxes[index].checked = stopListView.lastDisplayItemChecked.checked;

                    if (boxes[index].checked) {
                        stopListView.saveDisplayPredictions(index, true, boxes[index].name);
                    }
                    else {
                        stopListView.saveDisplayPredictions(index, false, boxes[index].name);
                    }
                }                
            }
        }
        else {
            if (this.checked)
                stopListView.saveDisplayPredictions(this.id, false, this.name);
            else
                stopListView.saveDisplayPredictions(this.id, true, this.name);
        }
        stopListView.lastDisplayItemChecked = this;
    },

    handleNextClick: function (event) {
        $("#message").hide();
        showButtonWorking("next-btn");
        
        $("#message").html('');
        var noCache = new Date().getTime();
        var strUrl = "../Services/DisruptionsService.svc/GetDisruptionRoutesDetails/?e=" + stopListView.disruptionId + "&ts=" + noCache;
        $("#stops").show();

        stopListView.showStopMask();
        var jqxhr = $.getJSON(strUrl, function (data) {
            //added here to handel error
            if (data.isError == true) {
                alert(data.responseString);
                return false;
            }
            if (data.responseObject.length == 0) {
                $("#message").html('Please ensure you have selected at least one stop for this Disruption');
                $("#message").show();
                stopListView.hideStopMask();
                hideButtonWorking("next-btn");
                return;
            }

            var mrUrl = "../Services/DisruptionsService.svc/AddDisruptionEffectedRoutes/?e=" + stopListView.disruptionId + "&cu=" + ttmcurrentUser + "&ts=" + noCache;

            $.ajax({
                url: mrUrl, dataType: "json", error: function () { hideButtonWorking("next-btn"); }, success: function (d) {
                    //added here to handel error
                    if (d.isError == true)
                    {
                        alert(d.responseString);
                        return false;
                    }
                    var o = new Object();
                    o.disruption_id = stopListView.disruptionId;
                    var x = JSON.stringify(o);
                    showButtonWorking('next-btn');
                    $.ajax({
                        url: '../Disruptions/Messages.aspx/ValidateDisruptionMessages',
                        type: 'POST',
                        dataType: 'JSON',
                        contentType: 'application/json;charset=utf-8;',
                        data: x,
                        success: function (data) {
                            location = "Messages.aspx?e=" + stopListView.disruptionId;
                        },
                        error: function (a, b, c) {
                            alert(b);
                            hideButtonWorking('next-btn');
                        }
                    });                
            }, async: true
            });
        }).always(function () { stopListView.hideStopMask(); });
    }, //end handleNextClick

    saveStop: function (i, on, stop) {
        //console.log(on);
        var noCache = new Date().getTime();
        var isDisplayChecked = $('.displayChkbox')[i].checked;
        var strUrl = "../Services/DisruptionsService.svc/SaveAffectedStop/?r=" + $("#cboRoute").val() + "&e=" + stopListView.disruptionId + "&s=" + stop + "&on=" + on + "&d=" + stopListView.isUpDirection + "&cu=" + ttmcurrentUser + "&disp=" + !on + "&ts=" + noCache;
        stopListView.showStopMask();
        var t1 = new Date();
        var t2 = new Date();
        var dif = t1.getTime() - t2.getTime()

        var Seconds_from_T1_to_T2 = dif / 1000;
        var Seconds_Between_Dates = Math.abs(Seconds_from_T1_to_T2);


        var jqxhr = $.getJSON(strUrl, function (data) {
            if (data.isError == true) {
                alert(data.responseString);
                return false;
            }
            if (on) {
                var missingRte;
                $('#c' + i).addClass('chkOn').removeClass('chkOff')
                var mrUrl = "../Services/DisruptionsService.svc/GetMissingRoutesForAffectedDisruptionStop/?e=" + stopListView.disruptionId + "&r=" + $("#cboRoute").val() + "&s=" + stop + "&ts=" + noCache;
                $.ajax({
                    url: mrUrl, dataType: "json", success: function (d) {
                        //added here to handel error
                        if (d.isError == true) {
                            alert(d.responseString);
                            return false;
                        }
                        missingRte = d.responseObject.join();
                    }, async: false
                });

                $('#m' + i).html(missingRte);
                $('#' + i + 's').css('visibility', 'visible')
                $('.displayChkbox')[i].checked = true;
                $('.displayChkbox:eq(' + i + ')').css('display', 'inline');
            }
            else {
                $('#c' + i).addClass('chkOff').removeClass('chkOn')
                $('#m' + i).html("");
                $('#' + i + 's').css('visibility', 'hidden')
                $('.displayChkbox')[i].checked = false;
                $('.displayChkbox:eq(' + i + ')').css('display', 'none');
            }
            stopListView.setCheckBox();

        }).always(function () { stopListView.hideStopMask(); });


    }, //end saveStop

    saveDisplayPredictions: function (i, on, stop) {
        var noCache = new Date().getTime();
        var isDisplayChecked = $('.displayChkbox')[i].checked;
        var strUrl = "../Services/DisruptionsService.svc/SaveAffectedStop/?r=" + $("#cboRoute").val() + "&e=" + stopListView.disruptionId + "&s=" + stop + "&on=true&d=" + stopListView.isUpDirection + "&cu=" + ttmcurrentUser + "&disp=" + !isDisplayChecked + "&ts=" + noCache;

        stopListView.showStopMask();

        var jqxhr = $.getJSON(strUrl, function (data) {
            if (data.isError == true) {
                alert(data.responseString);
                return false;
            }
            if (on) {
                //$('.displayChkbox')[i].checked = false;
            }
            else {
                //$('.displayChkbox')[i].checked = true;
            }
            //stopListView.setCheckBox();

        }).always(function () { stopListView.hideStopMask(); });


    }, //end saveDisplayPredictions

    changeDirection: function (d) {

        var noCache = new Date().getTime();
        var strUrl = "../Services/DisruptionsService.svc/GetStopsForDisruptionRoute/?e=" + stopListView.disruptionId + "&r=" + $("#cboRoute").val() + "&ts=" + noCache;

        stopListView.showStopMask();
        var jqxhr = $.getJSON(strUrl, function (data) {

            upstops = data.responseObject.UpStops;
            downstops = data.responseObject.DownStops;

            if (d == 'dn') {               
                stopListView.loadStops(downstops);
                stopListView.isUpDirection = false;
            }
            else {                
                stopListView.loadStops(upstops);
                stopListView.isUpDirection = true;
            }

        }).always(function () { stopListView.hideStopMask(); });
    }, //end changeDirection

    setCheckBox: function () {
        var checkBoxes = $("input.chkbox");
        if (checkBoxes.not(":checked").length <= 0) {
            $('input[name=group-select]').attr('checked', true);
        }
        else {
            $('input[name=group-select]').attr('checked', false);
        }
        
    }, //end setCheckBox
    showStopMask: function () {
        $("#selectable").mask("Loading...");
    },
    hideStopMask: function () {
        $("#selectable").unmask();
    }

}


$(document).ready(function () {
    stopListView.init();
});





