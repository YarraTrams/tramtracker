﻿var selected_diruption_id = null;
var listingView = {

    notification_info : null,

    init: function () {        
        $(document.body).on("click", ".clr", function (e) {
            selected_diruption_id = $(e.target).attr("id");
            var valid = $(e.target).data("valid");
            //log(valid);
            
            hideButtonWorking("clear-disruption");
            $("#clear-close-button").removeClass("btn-primary");
            $("#clear-close-button").show();
            $("#clear-ok-button").hide();
            $("#clear-disruption").show();
            $("#clear-controls").show();
            $("#clear-response-output").hide();
            $("#clear-disruption").removeAttr("disabled");
            $("#ttl").val("0");
            $("#ttl").removeAttr("disabled");
            $("#shared-messages-warning").hide();
            if (valid == false) {
                $("#ttl").attr("disabled", "true");
                $("#invalid-warning").show();
                $("#not-all-valid").hide();
                $("#invalid-edit-link").attr("href", "Stops.aspx?e=" + selected_diruption_id);
            }
            else {                
                $("#invalid-warning").hide();
                if (!allValid) {
                    $("#ttl").attr("disabled", "true");
                    $("#not-all-valid").show();
                    $("#clear-disruption").attr("disabled", "true");
                }
                else {
                    $("#not-all-valid").hide();                    
                }
                listingView.showSharedDisruptionMessages(selected_diruption_id);
            }
            $("#clear-confirm").modal({});
            
        });

        $(document.body).on("click", "#clear-disruption", function (e) {
            
            showButtonWorking("clear-disruption");
            var noCache = new Date().getTime();
            var ttl = $("#ttl").val();
            $.ajax({
                url: "../Services/DisruptionsService.svc/ClearDisruption/?e=" + selected_diruption_id + "&cu=" + ttmcurrentUser + "&ttl=" + ttl + "&ts=" + noCache,
                type: 'GET',
                dataType: 'JSON',
                contentType: 'application/json;charset=utf-8;',
                success: function (data) {
                    //added here to handel error 
                    if (data.isError == true) {
                        alert(data.responseString);
                        return false;
                    }
                    if (data.responseString == "NOTCLEARED") {
                        $("#clear-response-output").html("The associated FOC has not been cleared yet.");
                    }
                    else if (data.responseString == "OK") {
                        $("#clear-response-output").html("The disruption has been cleared.");
                        $("#row_" + selected_diruption_id).fadeOut(1000);
                    }
                    else if (data.responseString == "List") {
                        $("#clear-response-output").html("This Disruption has been successfully cleared and published to the Live system.");
                        $("#row_" + selected_diruption_id).fadeOut(1000);
                    }
                    else {                       
                        $("#clear-response-output").html(data.responseString.nl2br());
                        if (ttl == 0) {
                            $("#row_" + selected_diruption_id).fadeOut(1000);
                        }
                        else {
                            $("#row_" + selected_diruption_id + " .status-column").html("Pending");
                        }                        
                    }
                    $("#shared-messages-warning").hide();
                    $("#invalid-warning").hide();
                    $("#clear-controls").fadeOut(1000, function(e) {
                        $("#clear-response-output").fadeIn(1000, function () {
                            $("#clear-close-button").addClass("btn-primary");
                            $("#clear-close-button").hide();
                            $("#clear-ok-button").show();
                            $("#clear-disruption").hide();
                            
                            hideButtonWorking("clear-disruption");                            
                        });
                    });
                    
                },
                error: function (a, b, c) {
                    alert(b);
                    hideButtonWorking("clear-disruption");                    
                }
            });
        });

        $(document.body).on("click", "#clear-ok-button", function (e) {
            window.location.href = window.location.href;
        });


        $(document.body).on("change", "#PageBody_routeListing", function (e) {
            $("#review-modal").addClass("stops-list").removeClass("messages-list");
            listingView.loadStopListing(e);
        });

        $(document.body).on("click", ".review", function (e) {
            var clicked_link = $(e.target);
            selected_diruption_id = clicked_link.data("disruption");            
            $("#review-modal").modal({});
            var selected_route_no = clicked_link.attr("route-no");
            if (0 != $('#PageBody_routeListing option[value=' + selected_route_no + ']').length) {
                $("#PageBody_routeListing").val(selected_route_no);
            }
            else {
                $($("#PageBody_routeListing options")[0]).attr("selected", "true");
            }
            
            $("#review-modal").addClass("stops-list").removeClass("messages-list");
            listingView.loadStopListing(e);            
        });

        $(document.body).on("click", "#stops-back", function (e) {            
            $("#review-modal").addClass("stops-list").removeClass("messages-list");
        });

        $(document.body).on("click", ".email", function (e) {
            $("#send-email").modal({});
            listingView.showNotificationsDialog($(e.target).attr("for"));
        });

        $(document.body).on("click", "#publish-btn", function (e) {            
            if (publishEnabled) {
                $("#publish-confirm").modal({});
            }
        });

        $(document.body).on("click", "#publish-confirm-btn", function (e) {
            listingView.publishDisruptions();            
        });

        $(document.body).on("click", "#send-email-btn", function (e) {
            listingView.sendNotifications();
        });

    }, //end init

    loadStopListing: function (event) {
        
        var routeNo = $("#PageBody_routeListing").val();
        $("#upstops-table tbody").html("");
        $("#downstops-table tbody").html("");
        $("#stops-review-div").mask("Loading...");
       
        var noCache = new Date().getTime();
        var strUrl = "../Services/DisruptionsService.svc/GetStopsForDisruptedRoute/?r=" + routeNo + "&ts=" + noCache;
        
        var jqxhr = $.getJSON(strUrl, function (data) {
            //added here to handel error 
            if (data.isError == true)
            {
                alert(data.responseString);
                return false;
            }
            log(data);
            var upStopsArray = data.responseObject.UpStops;
            var downStopsArray = data.responseObject.DownStops;

            $.each(upStopsArray, function (i, val) {
                //$("#upstops-table tbody").append('<tr><td>' + summaryView.createStopLink(val) + '</td><td>' + summaryView.createStatusIcon(val) + '</td></tr>');
                $("#upstops-table tbody").append('<tr><td><a class="stop-link" onclick="listingView.displayMessagesForStop(this)" href="javascript:void(0)" tt-available="' + val.TramTrackerAvailable + '" stop-no="' + val.TrackerID + '" data-direction="up">' + val.Description + '</a></td><td><span class="' + ((val.TramTrackerAvailable) ? "icon-ok" : "icon-remove") + '">&nbsp;</span></td></tr>');
            });

            $.each(downStopsArray, function (i, val) {
                // $("#downstops-table tbody").append('<tr><td>' + summaryView.createStopLink(val) + '</td><td>' + summaryView.createStatusIcon(val) + '</td></tr>');
                $("#downstops-table tbody").append('<tr><td><a class="stop-link" onclick="listingView.displayMessagesForStop(this)" href="javascript:void(0)" tt-available="' + val.TramTrackerAvailable + '" stop-no="' + val.TrackerID + '" data-direction="down">' + val.Description + '</a></td><td><span class="' + ((val.TramTrackerAvailable) ? "icon-ok" : "icon-remove") + '">&nbsp;</span></td></tr>');
            });
            $("#stops-review-div").unmask();
        });//.always(function () { summaryView.hideStopMask(); $('#route-table-div').height($('#downstops-table').height()) });
    },

    displayMessagesForStop: function (element) {
        var clicked_link = $(element);
        var stopNo = clicked_link.attr("stop-no");
        var direction = clicked_link.data("direction");
        var ttAvailable = clicked_link.attr("tt-available");

        $("#message-details tbody").html("");

        /*$("#message-view").show();
        $("#route-review").hide();
        $("#stops-back").show();
        $("#review-ok-btn").hide();*/
        $("#review-modal").removeClass("stops-list").addClass("messages-list");

        var noCache = new Date().getTime();
        var strUrl = "../Services/DisruptionsService.svc/GetDisruptionMessagesForStop/?s=" + stopNo + "&tta=" + ttAvailable + "&ts=" + noCache;

        var jqxhr = $.getJSON(strUrl, function (data) {
            //added here to handel error 
            if (data.isError == true) {
                alert(data.responseString);
                return false;
            }
            log(data);
            if (data.responseObject != null) {
                $.each(data.responseObject, function (i, msg) {
                    if (msg.ShortMessage != null) {
                        if ((direction == "up" && msg.HideUpStopMessages == true) || (direction == "down" && msg.HideDownStopMessages == true)) {
                            $("#message-details tbody").append('<tr><td>' + msg.RouteNoDisplayValue + '</td><td>Hidden for ' + direction + ' direction</td><td>Hidden for ' + direction + ' direction</td></tr>');
                        }
                        else {
                            $("#message-details tbody").append('<tr><td>' + msg.RouteNoDisplayValue + '</td><td>' + msg.ShortMessage + '</td><td>' + msg.LongMessage + '</td></tr>');
                        }
                    }                    
                });
            }

        });
    },

    showNotificationsDialog: function (disruption_id) {
        $("#send-email").addClass("not-sent").removeClass("sent");
        var noCache = new Date().getTime();
        var o = new Object();
        o.disruption_id = disruption_id;
        var x = JSON.stringify(o);
        $.ajax({
            url: "../Disruptions/Listing.aspx/GetEmailNotificationInfo?disruption_id=" + disruption_id + "&ts=" + noCache,
            type: 'POST',
            dataType: 'JSON',
            contentType: 'application/json;charset=utf-8;',
            data: x,
            success: function (data) {                
                listingView.notification_info = data.d;
                log(data);
                $("#notification-message").val(data.d.body);
                $("#notification-subject").val(data.d.subject);
                $("#notification-to").val(data.d.to);
                listingView.loadEmailLogsTable(disruption_id);
            },
            error: function (a, b, c) {
                alert(b);
            }
        });
    },

    showSharedDisruptionMessages: function (disruption_id) {
        
        var noCache = new Date().getTime();
        var o = new Object();
        o.disruption_id = disruption_id;
        var x = JSON.stringify(o);
        $.ajax({
            url: "../Disruptions/Listing.aspx/GetSharedMessages?disruption_id=" + disruption_id + "&ts=" + noCache,
            type: 'POST',
            dataType: 'JSON',
            contentType: 'application/json;charset=utf-8;',
            data: x,
            success: function (data) {                
                //log(data);
                //alert(data.d.message);
                var shared_message = data.d.message;
                if (shared_message != "") {
                    $("#shared-messages-warning").show();
                    $("#shared-messages-warning-body").html(shared_message.nl2br());
                }
                else {
                    $("#shared-messages-warning").hide();
                }
            },
            error: function (a, b, c) {
                alert(b);
            }
        });
    },

    loadEmailLogsTable: function (disruption_id) {
        var noCache = new Date().getTime();
        $("#email-logs").html();
        $.ajax({
            type: "GET",
            url: "/Disruptions/EmailLogTable.aspx",
            data: { disruption_id: disruption_id, noCache: noCache },
            contentType: "application/json; charset=utf-8",
            dataType: "html",
            success: function (response) {                
                $("#email-logs").html(response);                
            },
            failure: function (msg) {
                log(msg);                
            }
        });
    },

    sendNotifications: function () {
        var noCache = new Date().getTime();
        showButtonWorking("send-email-btn");
        
        listingView.notification_info.body = $("#notification-message").val();
        listingView.notification_info.subject = $("#notification-subject").val();
        listingView.notification_info.to = $("#notification-to").val();
        
        var x = JSON.stringify(listingView.notification_info);
        $.ajax({
            url: "../Disruptions/Listing.aspx/SendNotification?disruption_id=" + listingView.notification_info.disruption_id + "&ts=" + noCache,
            type: 'POST',
            dataType: 'JSON',
            contentType: 'application/json;charset=utf-8;',
            data: x,
            success: function (data) {
                hideButtonWorking("send-email-btn");
                log(data);
                if (data.d.status == "sent") {
                    $("#send-email").removeClass("not-sent").addClass("sent");
                    listingView.loadEmailLogsTable(listingView.notification_info.disruption_id);
                }
                
            },
            error: function (a, b, c) {
                alert(b);
                hideButtonWorking("send-email-btn");
            }
        });
    },

    publishDisruptions: function () {
        if (publishEnabled) {
            var noCache = new Date().getTime();
            var strUrl = "../Services/DisruptionsService.svc/PublishAllDisruptions/?cu=" + ttmcurrentUser + "&ts=" + noCache;
            showButtonWorking("publish-confirm-btn");
            $.ajax({
                url: strUrl,
                type: 'GET',
                dataType: 'JSON',
                contentType: 'application/json;charset=utf-8;',
                success: function (data) {
                    //added here to handel error 
                    if (data.isError == true) {
                        alert(data.responseString);
                        return false;
                    }
                    log(data);
                    hideButtonWorking("publish-confirm-btn");
                    $("#publish-confirm").removeClass("not-published").addClass("published");
                    //$("#notification-message").val(data.responseObject.AffectedLongMessage);
                    $("#publish-confirm").on('hidden', function () {
                            window.location.href = window.location.href;                        
                    });
                },
                error: function (a, b, c) {
                    alert(b);
                    hideButtonWorking("publish-confirm-btn");
                }
            });
        }
    }

}//end listingView

$(document).ready(function () {
    listingView.init();
});



