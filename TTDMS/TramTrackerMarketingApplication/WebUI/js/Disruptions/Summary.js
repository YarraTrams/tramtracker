﻿
var summaryView = {
    init: function () {
        $("#applicationformheading").corner("top");
        $("#applicationform").corner("bottom");
        $("#routeListing").change(this.loadStopListing);
        $(".details").live('click', this.displayMessagesForStop);

        $("#btnNext").click(this.handleNextClick);



    }, //end init


    loadStopListing: function (event) {
        var routeNo = $("#routeListing").val();
        $('#upstops-table').css('display', 'none');
        $('#downstops-table').css('display', 'none');
        $('#route-table-div').height(200);


        if (routeNo === "0") return;



        $('#upstops-table').find('tr:gt(0)').remove();
        $('#downstops-table').find('tr:gt(0)').remove();
        //$('#route-table-div').css('display', 'none');



        var noCache = new Date().getTime();
        var strUrl = "../Services/DisruptionsService.svc/GetStopsForDisruptedRoute/?r=" + routeNo + "&ts=" + noCache;
        summaryView.showStopMask();
        var jqxhr = $.getJSON(strUrl, function (data) {
            //added here to handel error 
            if (data.isError == true) {
                alert(data.responseString);
                return false;
            }
            var upStopsArray = data.responseObject.UpStops;
            var downStopsArray = data.responseObject.DownStops;

            $.each(upStopsArray, function (i, val) {
                $('#upstops-table').append('<tr><td>' + summaryView.createStopLink(val) + '</td><td>' + summaryView.createStatusIcon(val) + '</td></tr>');
            });

            $.each(downStopsArray, function (i, val) {
                $('#downstops-table').append('<tr><td>' + summaryView.createStopLink(val) + '</td><td>' + summaryView.createStatusIcon(val) + '</td></tr>');
            });
        }).always(function () { summaryView.hideStopMask(); $('#route-table-div').height($('#downstops-table').height()) });


        $('#upstops-table').css('display', 'block');
        $('#downstops-table').css('display', 'block');
        $('#upstops-table tr:odd').addClass('oddrow');
        $('#downstops-table tr:odd').addClass('oddrow');
    },
    displayMessagesForStop: function (event) {

        $("#message-details").attr("title", $(this).text());
        $("#ui-dialog-title-message-details").text($(this).text());
        $("#message-details").find("tr:gt(0)").remove();


        var args = String(this.id).split(":");
        var stopNo = args[0];
        var ttAvailable = args[1];

        var noCache = new Date().getTime();
        var strUrl = "../Services/DisruptionsService.svc/GetDisruptionMessagesForStop/?s=" + stopNo + "&tta=" + ttAvailable + "&ts=" + noCache;

        summaryView.showStopMask();
        var jqxhr = $.getJSON(strUrl, function (data) {
            //added here to handel error 
            if (data.isError == true) {
                alert(data.responseString);
                return false;
            }
            if (data.responseObject != null) {
                $.each(data.responseObject, function (i, msg) {
                    $("#message-details tbody").append('<tr><td>' + msg.RouteNoDisplayValue + '</td><td>' +
                    msg.ShortMessage + '</td><td>' + msg.MediumMessage + '</td><td>' + msg.LongMessage + '</td><td>' 
                    + msg.DisplayTypeString + '</td></tr>');
                });
            }
        }).always(function () { summaryView.hideStopMask(); });
        
        $("#message-details").dialog({ minWidth: 900,
            height: 400, modal: true
        });
    }, //end displayMessagesForStop


    handleNextClick: function (event) {

        //TODO - Save and validate messages here 
        var noCache = new Date().getTime();
        var strUrl = "../Services/DisruptionsService.svc/PublishAllDisruptions/?cu=" + ttmcurrentUser + "&ts=" + noCache;

        var jqxhr = $.getJSON(strUrl, function (data) {
            //added here to handel error 
            if (data.isError == true) {
                alert(data.responseString);
                return false;
            }

        }).always(function () { alert("All Disruptions have been successfully published"); location = "Listing.aspx"; });




        //alert("All Disruptions have been successfully published");
        //location = "Listing.aspx";

    }, //end handleNextClick


    createStopLink: function (stop) {
        var color = (stop.TramTrackerAvailable) ? "#279F2C" : "#000000";
        var htmlString = '<a href="javascript:;"  style="color:' + color + ';" class="details"  id="' + stop.TrackerID + ':' + stop.TramTrackerAvailable + '">' + stop.Description + '</a>';
        return htmlString;
    },
    createStatusIcon: function (stop) {
        var icon = (stop.TramTrackerAvailable) ? '<img src="../images/tram_yes.gif" />' : '<img src="../images/tram_no.gif" />';
        return icon;
    },


    showStopMask: function () {
        $("#route-table-div").mask("Loading...");
    },
    hideStopMask: function () {
        $("#route-table-div").unmask();
    }



}//end summaryView



$(document).ready(function () {

    summaryView.init();



});        //end document ready


