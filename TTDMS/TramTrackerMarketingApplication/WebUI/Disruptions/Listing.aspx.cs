﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using TTMarketing.BLL.Controllers.Disruptions;
using TTMarketing.BLL.Model.Disruptions;
using YarraTrams.Library.Mailer;

namespace TTMarketing.WebUI.Disruptions
{
    public partial class Listing : System.Web.UI.Page
    {

        public string _currentUser = "";
        public bool publishEnabled = false;
        public string publishReason = "";
        public bool allValid = true;

        protected void Page_Load(object sender, EventArgs e)
        {

            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            if (!this.IsPostBack)
            {
                LoadDisruptions();
            }

            string[] user = HttpContext.Current.User.Identity.Name.Split('\\');
            _currentUser = (user.Length ==2 ) ? user[1] : "NullWindowsUser";

            //Response.Write(DisruptionController.GetClearedDate(8));

            //for (int i = 10; i <= 120; i += 10)
            //{
               //ListItem item = new ListItem(i.ToString(), i.ToString());
               //timeList.Items.Insert(0, item);
            //}

            IList<DisruptionRoute> routes = DisruptionRoutesController.GetAllDisruptedRoutes();

            foreach (DisruptionRoute route in routes)
            {
                ListItem item = new ListItem(route.RouteNumber.ToString() + " - " + route.Description, route.RouteNumber.ToString());
                routeListing.Items.Add(item);
            }


        }

        private void LoadDisruptions()
        {            
            //disruptionListing.DataSource = DisruptionController.GetDisruptionByStatus(DisruptionStatus.Created);
            IList<Disruption> disruptions = DisruptionController.GetActiveDisruptions();
            this.publishReason = "This button is disabled because there are currently no unpublished changes";
            foreach(Disruption disruption in disruptions) {
                if (disruption.Status == DisruptionStatus.Updated || disruption.Status == DisruptionStatus.Created)
                {
                    this.publishEnabled = true;
                    this.publishReason = "";
                }
                if (!disruption.Valid)
                {
                    this.publishEnabled = false;
                    this.allValid = false;
                    this.publishReason = "This button is disabled because not all unpublished disruptions are valid, please update or clear any disruptions highlighted in red.";
                    break;
                }
            }
            disruptionListing.DataSource = disruptions;
            disruptionListing.DataBind();

        }

        protected void disruptionListing_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (disruptionListing.Items.Count < 1)
            {
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    Label lblFooter = (Label)e.Item.FindControl("lblEmptyData");
                    lblFooter.Visible = true;
                }
            }
        }

        protected string GetStatusVal(object status_obj, object expiry_obj)
        {
            DisruptionStatus status = (DisruptionStatus)status_obj;
            if (expiry_obj == null)
            {
                return status.ToString();
            }
            else
            {
                DateTime expiryTime = (DateTime)expiry_obj;
                if (status == DisruptionStatus.Pending || status == DisruptionStatus.Published)
                {
                    return "Clearing at " + expiryTime.ToString();
                }
                else
                {
                    return status.ToString() + ", Clearing at " + expiryTime.ToString();
                }
            }
            /*if (status != DisruptionStatus.Pending || expiry_obj == null)
            {
                return status.ToString();
            }
            else
            {                
                DateTime expiryTime = (DateTime)expiry_obj;
                return "Clearing at " + expiryTime.ToString();                
            }*/
        }

        [WebMethod]
        public static Dictionary<string, object> GetEmailNotificationInfo(string disruption_id)
        {
            Dictionary<string, object> results = new Dictionary<string, object>();
            int disruptionId = Int32.Parse(disruption_id);
            RouteMessage message = DisruptionController.GetPrimaryRouteMessages(disruptionId);
            results.Add("disruption_id", disruptionId);
            results.Add("body", message.AffectedLongMessage + "\n\n" + ConfigurationManager.AppSettings["DisruptionNotificationEmailSuffix"]);
            results.Add("subject", ConfigurationManager.AppSettings["DefaultNotificationSubject"]);
            results.Add("to", ConfigurationManager.AppSettings["DisruptionNotificationRecipients"]);

            return results;
        }

        [WebMethod]
        public static Dictionary<string, object> SendNotification(string disruption_id, string body, string subject, string to)
        {
            Dictionary<string, object> results = new Dictionary<string, object>();
            int disruptionId = Int32.Parse(disruption_id);            

            string mailServer = ConfigurationManager.AppSettings["MailServer"];
            string notificationSender = ConfigurationManager.AppSettings["NotificationSender"];            
            if (ConfigurationManager.AppSettings["SendEmails"] == "true")
            {
                Emailer.SendEmail(mailServer, notificationSender, to, subject, body);
            }

            Dictionary<string, string> log_data = new Dictionary<string, string>();            
            log_data.Add("disruption_id", disruption_id);
            log_data.Add("to", to);
            log_data.Add("subject", subject);
            log_data.Add("body", body);

            string[] user = HttpContext.Current.User.Identity.Name.Split('\\');
            string currentUser = (user.Length ==2 ) ? user[1] : "NullWindowsUser";
            ActivityLog log = ActivityLogController.Create((int?)disruptionId, Json.Encode(log_data), "Email", currentUser);


            results.Add("status", "sent");

            return results;
        }

        [WebMethod]
        public static Dictionary<string, object> GetSharedMessages(string disruption_id)
        {
            Dictionary<string, object> results = new Dictionary<string, object>();
            int disruptionId = Int32.Parse(disruption_id);

            string message = DisruptionController.GetSharedDisruptionMessages(disruptionId);

            results.Add("message", message);

            return results;
        }
    }
}