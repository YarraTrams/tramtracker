﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="TTMarketing.WebUI.Disruptions.Error"  MasterPageFile="../Template.Master" Title="Yarra Trams - TTDMS - Disruptions" %>

<asp:Content ID="Content1" ContentPlaceHolderId="PageBody" runat="server"> 
    <div style="height:30px">
        <asp:Label ID="errEmailLable" Text="" runat="server" BackColor="Cyan" ForeColor="Black" Font-Bold="true" Font-Size="12"></asp:Label>
    </div>
    <div>
        <asp:Literal ID="Error_Text" runat="server"></asp:Literal>
        <asp:button id="backButton" runat="server" text="Back" class="btn btn-primary messages" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
        <asp:button id="emailButton" runat="server" text="Report Issue to ICTServiceDesk" class="btn btn-primary messages" OnClick="emailButton_Click"></asp:button>        
    </div>    
</asp:Content>
