﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Summary.aspx.cs" Inherits="TTMarketing.WebUI.Disruptions.Summary" %>
<%@ Register TagPrefix="yt" TagName="DisruptionSummaryListing" src="~/Controls/Disruptions/DisruptionSummaryListing.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>Manage Disruptions</title>
    <script language="javascript" type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
    <script language="javascript" type="text/javascript" src="../js/jquery-ui-1.8.4.custom.min.js"></script>

    <script language="javascript" type="text/javascript" src="../js/jquery.corner.js"></script>
    
    <script language="javascript" type="text/javascript" src="../js/jquery.form.js" ></script> 
    <script language="javascript" type="text/javascript" src="../js/dateformat.js"></script> 
    <script language="javascript" type="text/javascript" src="../js/functions.js"></script> 
    <script language="javascript" type="text/javascript" src="../js/Disruptions/Summary.js"></script>
    <script language="javascript" src="../js/jquery.loadmask.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript" src="../js/Disruptions/Common.js"></script>

    <link type="text/css" href="../css/base/jquery.ui.all.css" rel="stylesheet" /> 
    <link type="text/css" href="../css/menu.css" rel="stylesheet" /> 
      <link type="text/css" href="../css/main.css" rel="stylesheet" /> 
          <link href="../css/jquery.loadmask.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        var ttmcurrentUser = "<%=_currentUser %>";
    </script>

</head>

<body>
<div id="heading"><img src="../images/aheader-bg2.gif" /></div>
<div id="menubar">

<ul class="nav">
<li><a href="../Default.aspx">Home</a></li>
<li><a href="Listing.aspx">View Disruptions</a></li>
<li><a href="Details.aspx">Create a Disruption</a></li>
</ul>
</div>




<div id="applicationformheading">Disruptions Review &amp; Publish (Step 4)</div>
<div id="applicationform">

<form id="Form1" runat="server">
    <table width="100%" border="0">
            <yt:DisruptionSummaryListing ID="DisruptionSummaryListing1" runat="server" />


           <tr>
                <td colspan="3">
                    Please review and ensure details of the Messages assigned for each affected/unaffected Routes  : 
                </td>
                <td>
                    <asp:DropDownList ID="routeListing" runat="server"></asp:DropDownList>
                </td>
           
           </tr>  
             <tr><td colspan="4" style="text-align:center;"><input type="button" value="Publish or Update All Disruptions" id="btnNext" style="width:200px;" /></td></tr>
            <tr><td colspan="4" style="height:20px;"></td></tr>

           <tr  id="results">
                <td colspan="4">
<div id="route-table-div" style="display:block;height:200px">
<table border="1" id="upstops-table" style="display:none">

<tr><th width="265">Upstops</th><th>Status</th></tr>

<!--
<tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="1201">1201 - Stop 135 Bell St &amp; Nicholson St</a></td><td><img src="/images/tram_yes.gif"></td></tr>

<tr><td><a href="#" style="color:#279F2C;" class="details" id="1202">1202 - Stop 134 Merribell Av &amp; Nicholson St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="1203">1203 - Stop 133 Harding St &amp; Nicholson St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="1204">1204 - Stop 132 Crozier St &amp; Nicholson St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="1205">1205 - Stop 131 Rennie St &amp; Nicholson St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="1206">1206 - Stop 130 The Avenue &amp; Nicholson St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="1555">1555 - Stop 129 Moreland Rd &amp; Nicholson St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="1208">1208 - Stop 128 Donald St &amp; Holmes St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="1209">1209 - Stop 127 Albion St &amp; Holmes St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="1210">1210 - Stop 126 Stewart St &amp; Lygon St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="1211">1211 - Stop 125 Blyth St &amp; Lygon St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="1212">1212 - Stop 124 Victoria St &amp; Lygon St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="1213">1213 - Stop 123 Albert St &amp; Lygon St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="1214">1214 - Stop 122 Glenlyon Rd &amp; Lygon St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="1215">1215 - Stop 121 Weston St &amp; Lygon St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#000000;" class="details" id="1216">1216 - Stop 120 Brunswick Rd &amp; Lygon St</a></td><td><img src="/images/tram_no.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="1217">1217 - Stop 119 Bougainville Place &amp; Lygon St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="1218">1218 - Stop 118 Pigdon St &amp; Lygon St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="1219">1219 - Stop 117 Richardson St &amp; Lygon St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="1220">1220 - Stop 116 Fenwick St &amp; Lygon St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="1221">1221 - Stop 115 Newry St &amp; Lygon St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="1222">1222 - Stop 114 Princes St &amp; Lygon St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="1223">1223 - Stop 113 Lytton St &amp; Lygon St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="1224">1224 - Stop 112 Lygon St &amp; Elgin St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="1225">1225 - Stop 111 Elgin Place &amp; Elgin St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="3001">3001 - Stop 1 Melbourne University</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="3002">3002 - Stop 2 Grattan St &amp; Swanston St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="3003">3003 - Stop 3 Pelham St &amp; Swanston St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="3004">3004 - Stop 4 Queensberry St &amp; Swanston St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="3006">3006 - Stop 6 Victoria St &amp; Swanston St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="3007">3007 - Stop 7 Franklin St &amp; Swanston St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="3008">3008 - Stop 8 Melbourne Central Station</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="3009">3009 - Stop 9 Lonsdale St &amp; Swanston St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="3010">3010 - Stop 10 Bourke St &amp; Swanston St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="3011">3011 - Stop 11 Collins St &amp; Swanston St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="3013">3013 - Stop 13 Federation Square</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="3014">3014 - Stop 14 Arts Precinct</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="2227">2227 - Stop 17 Sturt St &amp; Southbank Blvd</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="2228">2228 - Stop 18 West Gate Fwy &amp; Sturt St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="2229">2229 - Stop 19 Miles St &amp; Sturt St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="2230">2230 - Stop 20 Coventry St &amp; Sturt St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="2232">2232 - Stop 22 Dorcas St &amp; Eastern Rd</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="2233">2233 - Stop 23 Moray St &amp; Park St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="2234">2234 - Stop 24 Clarendon St &amp; Park St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="2235">2235 - Stop 25 Cecil St &amp; Park St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="2236">2236 - Stop 26 Ferrars St &amp; Park St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="2237">2237 - Stop 27 Park St &amp; Montague St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="2238">2238 - Stop 28 Montague St &amp; Bridport St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="2239">2239 - Stop 29 Victoria La &amp; Victoria Ave</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="2240">2240 - Stop 30 Richardson St &amp; Victoria Ave</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="2241">2241 - Stop 31 Graham St &amp; Victoria Ave</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="8001">8001 - Stop 32 Beaconsfield Pde &amp; Victoria Ave</a></td><td><img src="/images/tram_yes.gif"></td></tr></tbody>
-->

</table>

<table border="1" id="downstops-table" style="display:none">

<tr><th width="265">Downstops</th><th>Status</th></tr>

<!--
<tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="9001">9001 - Stop 135 Bell St &amp; Nicholson St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="2202">2202 - Stop 134 Merribell Ave &amp; Nicholson St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="2203">2203 - Stop 133 Harding St &amp; Nicholson St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="2204">2204 - Stop 132 Crozier St &amp; Nicholson St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="2205">2205 - Stop 131 Rennie St &amp; Nicholson St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="2206">2206 - Stop 130 The Avenue &amp; Nicholson St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="2207">2207 - Stop 129 Moreland Rd &amp; Nicholson St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="2208">2208 - Stop 128 Mitchell St &amp; Holmes St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="2209">2209 - Stop 127 Albion St &amp; Lygon St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="2210">2210 - Stop 126 Stewart St &amp; Lygon St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="2211">2211 - Stop 125 Blyth St &amp; Lygon St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="2212">2212 - Stop 124 Victoria St &amp; Lygon St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="2213">2213 - Stop 123 Albert St &amp; Lygon St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="2214">2214 - Stop 122 Glenlyon Rd &amp; Lygon St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="2215">2215 - Stop 121 Weston St &amp; Lygon St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="2216">2216 - Stop 120 Brunswick Rd &amp; Lygon St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="2217">2217 - Stop 119 Park St &amp; Lygon St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="2218">2218 - Stop 118 Pigdon St &amp; Lygon St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="2219">2219 - Stop 117 Richardson St &amp; Lygon St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="2220">2220 - Stop 116 Fenwick St &amp; Lygon St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="2221">2221 - Stop 115 Newry St &amp; Lygon St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="2222">2222 - Stop 114 Princess St &amp; Lygon St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="2223">2223 - Stop 113 Lytton St &amp; Lygon St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="2224">2224 - Stop 112 Lygon St &amp; Elgin St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="2225">2225 - Stop 111 Elgin Place &amp; Elgin St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="3101">3101 - Stop 1 Melbourne University</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="3102">3102 - Stop 2 Grattan St &amp; Swanston St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="3103">3103 - Stop 3 Canada La &amp; Swanston St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="3104">3104 - Stop 4 Queensberry St &amp; Swanston St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="3106">3106 - Stop 6 Victoria St &amp; Swanston St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="3107">3107 - Stop 7 Franklin St &amp; Swanston St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="3108">3108 - Stop 8 Melbourne Central Station</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="3109">3109 - Stop 9 Lonsdale St &amp; Swanston St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="3110">3110 - Stop 10 Bourke St &amp; Swanston St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="3111">3111 - Stop 11 Collins St &amp; Swanston St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="3113">3113 - Stop 13 Federation Square</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="3114">3114 - Stop 14 Arts Precinct</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="1226">1226 - Stop 16 St Kilda Rd &amp; Southbank Blvd</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="1227">1227 - Stop 17 Sturt St &amp; Southbank Blvd</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="1228">1228 - Stop 18 West Gate Fwy &amp; Sturt St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="1229">1229 - Stop 19 Miles St &amp; Sturt St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="1230">1230 - Stop 20 Coventry St &amp; Sturt St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="1231">1231 - Stop 21 Hanna La &amp; Sturt St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="1232">1232 - Stop 22 Dorcas St &amp; Eastern Rd</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="1233">1233 - Stop 23 Moray St &amp; Park St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="1234">1234 - Stop 24 Clarendon St &amp; Park St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="1235">1235 - Stop 25 Cecil St &amp; Park St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="1236">1236 - Stop 26 Ferrars St &amp; Park St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="1237">1237 - Stop 27 Park St &amp; Montague St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="1238">1238 - Stop 28 Montague St &amp; Bridport St</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="1239">1239 - Stop 29 Cardigan St &amp; Victoria Ave</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="1240">1240 - Stop 30 Richardson St &amp; Victoria Ave</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr class="oddrow"><td><a href="#" style="color:#279F2C;" class="details" id="1241">1241 - Stop 31 Little Page St &amp; Victoria Ave</a></td><td><img src="/images/tram_yes.gif"></td></tr><tr><td><a href="#" style="color:#279F2C;" class="details" id="1242">1242 - Stop 32 Beaconsfield Pde &amp; Victoria Ave</a></td><td><img src="/images/tram_yes.gif"></td></tr></tbody>
-->


</table>

<br class="clear">

</div>                
                
                </td>
           </tr>

    
    </table>

</form>
</div>
<div id="message-details" title="Special Event Message" style="display:none">
<table>
   <tbody>
      <tr><th>Route Number</th><th>tramTRACKER PIDS Message</th><th>Medium Message (Not used)</th><th>Smart Phone Message</th><th>Smart Phone Message Display Type</th></tr>
     
   </tbody>
</table>
</div>



<div id="affected-stops" title="Affected Stops" style="display:none">
	<table width="100%"  id="affectedStopData" border="0">
        <tr>
            <th>Route No</th>
            <th>Stop</th>
            <th>Tramtracker Id</th>
            <th>Direction</th>
        </tr>
    </table>
</div>

<div id="foc-disruption" title="FOC Disruption" style="display:none">
	<table width="100%"  id="focData" border="1">
        <tr>
            <th>FOC Disruption Id</th>
            <th>Created</th>
        </tr>
    </table>
</div>



</body>
</html>
