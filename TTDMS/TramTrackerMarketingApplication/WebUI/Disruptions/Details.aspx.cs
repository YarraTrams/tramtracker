﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using TTMarketing.BLL.Controllers.Disruptions;
using TTMarketing.BLL.Model.Disruptions;


namespace TTMarketing.WebUI.Disruptions
{
    public partial class Details : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            if(!this.IsPostBack)
            {
                BindRouteList();

                if (!string.IsNullOrEmpty(Request.QueryString["d"]))
                {
                    BindDisruption();
                }
            }
        }

        private void BindRouteList()
        {
            IList<DisruptionRoute> routes = DisruptionRoutesController.GetAllRoutes();
            ListItem item;

            foreach (DisruptionRoute route in routes)
            {
                string desc = route.RouteNumber + " - " + route.Description;
                item = new ListItem(desc, route.RouteNumber.ToString());
                routeList.Items.Add(item);
            }


            //routeList.DataSource = routes;
            //routeList.DataBind();

            item = new ListItem("Please select", "");
            routeList.Items.Insert(0, item);
        }



        private void BindDisruption()
        {
            int id = Int32.Parse(Request.QueryString["d"].ToString());

            Disruption dr = DisruptionController.GetDisruptionById(id);
            
            name.Value = dr.Name;
            disruptionId.Value = dr.Id.ToString();
            ttdisruptionid.Value = dr.TT_DisruptionId.ToString();
            if (dr.TT_DisruptionId != null) 
            {
                FOCDisruption disruption = DisruptionController.GetFOCDisruptionById((int)dr.TT_DisruptionId);

                focId.Text = disruption.Id.ToString();
                focMessage.Text = disruption.Message;
                focRouteNo.Text = disruption.RouteNo.ToString();
                focDate.Text = disruption.DisruptionDateTime.ToString();

                focUpFrom.Text = (disruption.UpFromStopNo > 0) ? disruption.UpFromStopNo.ToString() : "-";
                focUpTo.Text = (disruption.UpToStopNo > 0) ? disruption.UpToStopNo.ToString() : "-";
                focUpFromStopName.Text = disruption.UpFromStopName;
                focUpToStopName.Text = disruption.UpToStopName;
                focUpFromStopDirection.Text = disruption.UpFromStopDirection;
                focUpToStopDirection.Text = disruption.UpToStopDirection;

                focDownFrom.Text = (disruption.DownFromStopNo > 0) ? disruption.DownFromStopNo.ToString() : "-";
                focDownTo.Text = (disruption.DownToStopNo > 0) ? disruption.DownToStopNo.ToString() : "-";
                focDownFromStopName.Text = disruption.DownFromStopName;
                focDownToStopName.Text = disruption.DownToStopName;
                focDownFromStopDirection.Text = disruption.DownFromStopDirection;
                focDownToStopDirection.Text = disruption.DownToStopDirection;
            }
            ttdisruptionid.Attributes.Add("disabled", "disabled");
            txtdescription.Value = dr.Description;
            routeList.Items.FindByValue(dr.PrimaryDisruptedRouteNo.ToString()).Selected = true;
            currentprimaryRouteNo.Value = dr.PrimaryDisruptedRouteNo.ToString();            
        }


        [WebMethod]
        public static Dictionary<string, string> ValidateFOCId(string foc_id)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            int id = 0;
            bool valid = Int32.TryParse(foc_id, out id);
            if (!valid)
            {
                result.Add("valid", "false");
                result.Add("message", "The FOC ID must be an integer");
                return result;
            }            
            
            FOCDisruption disruption = DisruptionController.GetFOCDisruptionById(id);

            if (disruption != null)
            {

                if (DisruptionController.IsFocDisruptionAssigned(id))
                {
                    result.Add("valid", "false");
                    result.Add("message", "This FOC Disruption is already associated with a Disruption and cannot be associated  with another Disruption.");
                    return result;
                }

                if (disruption.DisruptionCleared.HasValue)
                {
                    if (disruption.DisruptionCleared <= DateTime.Now)
                    {
                        result.Add("valid", "false");
                        result.Add("message", "This Disruption has already been cleared in the FOC system.");
                        return result;                        
                    }
                }
                else
                {
                    result.Add("focMessage", disruption.Message);
                    result.Add("focDate", disruption.DisruptionDateTime.ToString());
                    result.Add("focRouteNo", disruption.RouteNo.ToString());
                    result.Add("focUpFrom", (disruption.UpFromStopNo > 0) ? disruption.UpFromStopNo.ToString() : "-");
                    result.Add("focUpTo", (disruption.UpToStopNo > 0) ? disruption.UpToStopNo.ToString() : "-");
                    result.Add("focUpFromStopName", disruption.UpFromStopName);
                    result.Add("focUpToStopName", disruption.UpToStopName);
                    result.Add("focUpFromStopDirection", disruption.UpFromStopDirection);
                    result.Add("focUpToStopDirection", disruption.UpToStopDirection);
                    result.Add("focDownFrom", (disruption.DownFromStopNo > 0) ? disruption.DownFromStopNo.ToString() : "-");
                    result.Add("focDownTo", (disruption.DownToStopNo > 0) ? disruption.DownToStopNo.ToString() : "-");
                    result.Add("focDownFromStopName", disruption.DownFromStopName);
                    result.Add("focDownToStopName", disruption.DownToStopName);
                    result.Add("focDownFromStopDirection", disruption.DownFromStopDirection);
                    result.Add("focDownToStopDirection", disruption.DownToStopDirection);
                    
                    result.Add("valid", "true");
                    result.Add("message", "Valid FOC");
                    return result;    
                }
            }
            result.Add("valid", "false");
            result.Add("message", "The specified FOC could not be found");
            return result;            
        }

        [WebMethod]
        public static Dictionary<string, string> Save(string foc_id, string name, string route_num, string disruption_id, string current_primary_route_no, string has_foc)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            try
            {
                int? focDisruptionId = 0;
                if (foc_id != "" && has_foc == "True")
                {
                    focDisruptionId = Int32.Parse(foc_id);
                }
                

                int disId = Int32.Parse(disruption_id);
                int routeNo = Int32.Parse(route_num);
                string currentUser = HttpContext.Current.User.Identity.Name; // System.Security.Principal.WindowsIdentity.GetCurrent().Name; 

                if (disId == 0)
                {
                    Disruption dis = DisruptionController.Create(name, "", (focDisruptionId == 0) ? null : focDisruptionId, (int)DisruptionStatus.Created, routeNo, currentUser);
                    result.Add("valid", "true");
                    result.Add("disruption_id", dis.Id.ToString());
                    //Response.Redirect("Stops.aspx?e=" + dis.Id.ToString());
                }
                else
                {
                    int savedRouteNo = Int32.Parse(current_primary_route_no);
                    if (routeNo != savedRouteNo)
                    {
                        DisruptionStopsController.DeleteAllDisruptionEffectedRouteStops(disId, (short)savedRouteNo, true, currentUser);
                        DisruptionStopsController.DeleteAllDisruptionEffectedRouteStops(disId, (short)savedRouteNo, false, currentUser);
                        DisruptionController.SetValidStatus(disId, false);
                        DisruptionController.ClearUnusedMessages(currentUser);
                    }

                    DisruptionController.Update(disId, name, "", (focDisruptionId == 0) ? null : focDisruptionId, (int)DisruptionStatus.Updated, routeNo, currentUser);
                    result.Add("valid", "true");
                    result.Add("disruption_id", disruption_id);
                    //Response.Redirect("Stops.aspx?e=" + disId.ToString());
                }
            }
            catch (Exception ex)
            {
                result.Add("valid", "false");
                result.Add("message", ex.Message);
            }

            return result;        
        }
    }
}