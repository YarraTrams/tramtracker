﻿using System;
using System.IO;
using System.Text;
using System.Configuration;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Helpers;
using TTMarketing.BLL.Controllers.Disruptions;
using TTMarketing.BLL.Model.Disruptions;

namespace TTMarketing.WebUI.Disruptions
{
    public partial class Messages : System.Web.UI.Page
    {
        public string primaryRouteNo = "";
        public string _currentUser = "";
        public Disruption disruption = null;
        public string AffectedMessages = null;
        public string UnaffectedMessages = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            int id = Int32.Parse(Request.QueryString["e"].ToString());
            disruption = DisruptionController.GetDisruptionById(id);
            primaryRouteNo = disruption.PrimaryDisruptedRouteNo.ToString();

            disruptionTabs.ItemDataBound += new RepeaterItemEventHandler(disruptionTabs_ItemDataBound);           

            IList<Disruption> list = DisruptionController.GetActiveDisruptions();
            disruptionTabs.DataSource = list;
            disruptionTabs.DataBind();

            string[] user = HttpContext.Current.User.Identity.Name.Split('\\');
            _currentUser = (user.Length == 2) ? user[1] : "NullWindowsUser";
            
            /*NameValueCollection AffectedMessagesCollection = ConfigurationManager.GetSection("AffectedMessages") as NameValueCollection;
            Dictionary<string, string> AffectedMessagesDict = new Dictionary<string, string>();
            foreach (String key in AffectedMessagesCollection.AllKeys)
            {
                AffectedMessagesDict.Add(key, AffectedMessagesCollection[key]);
            }

            var dictionaryTransformed = AffectedMessagesDict.Select(item =>
                        new { item.Key, Value = AffectedMessagesDict[item.Key] });
            AffectedMessages = Json.Encode(dictionaryTransformed);*/
            NameValueCollection AffectedMessagesCollection = ConfigurationManager.GetSection("AffectedMessages") as NameValueCollection;
            AffectedMessages = GetJsonFromNVP(AffectedMessagesCollection);

            NameValueCollection UnaffectedMessagesCollection = ConfigurationManager.GetSection("UnaffectedMessages") as NameValueCollection;
            UnaffectedMessages = GetJsonFromNVP(UnaffectedMessagesCollection);
        }

        private string GetJsonFromNVP(NameValueCollection col)
        {
            Dictionary<string, string> MessagesDict = new Dictionary<string, string>();
            foreach (String key in col.AllKeys)
            {
                MessagesDict.Add(key, col[key]);
            }

            var dictionaryTransformed = MessagesDict.Select(item =>
                        new { item.Key, Value = MessagesDict[item.Key] });
            return Json.Encode(dictionaryTransformed);
        }

        void disruptionTabs_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {                
                Disruption dr = e.Item.DataItem as Disruption;
                Literal tabText = e.Item.FindControl("tabText") as Literal;
                tabText.Text = "Disruption #" + dr.Id.ToString();

                if (disruption.Id == dr.Id)
                {
                    Literal tabClass = e.Item.FindControl("tabClass") as Literal;
                    tabClass.Text = "active";
                }
                
            }
        }

        [WebMethod]
        public static List<object> ValidateDisruptionMessages(string disruption_id)
        {
            int id = 0;
            bool valid = Int32.TryParse(disruption_id, out id);

            //this sets the hidden direction flags if the current disrupion invalidated the current values
            IList<RouteMessage> list = DisruptionMessagesController.GetAllDisruptedRouteMessagesByDisruptionId(id);

            foreach (RouteMessage msg in list)
            {
                if (msg.DownStopsCount > 0 && msg.UpStopsCount > 0)
                {
                    if (msg.HideDownStopMessages == true || msg.HideUpStopMessages == true)
                    {
                        DisruptionController.SetMessageDirectionHidden(msg.Id, false, false);
                    }
                }
            }
            IList<RouteCombinationMessage> clist = DisruptionMessagesController.GetAllDisruptedRouteCombinationMessagesByDisruptionId(id);
            IList<RouteCombinationMessage> alist = DisruptionMessagesController.GetAllUnaffectedDisruptedRouteCombinationMessagesByDisruptionId(id);
            foreach (RouteCombinationMessage msg in alist)
            {
                if (!clist.Any(m => m.Id == msg.Id))
                {
                    clist.Add(msg);
                }
            }
            foreach (RouteCombinationMessage msg in clist)
            {
                if (msg.DownStopsCount > 0 && msg.UpStopsCount > 0)
                {
                    if (msg.HideDownStopMessages == true || msg.HideUpStopMessages == true)
                    {
                        DisruptionController.SetCombinationMessageDirectionHidden(msg.Id, false, false);
                    }
                }
            }

            List<object> result = DisruptionMessagesController.GetDisruptionMessagesValidationResult(id);
            if (result.Count == 0)
            {
                DisruptionController.SetValidStatus(id, true);
            }
            else
            {
                DisruptionController.SetValidStatus(id, false);
            }
            return result;
        }
    }
}