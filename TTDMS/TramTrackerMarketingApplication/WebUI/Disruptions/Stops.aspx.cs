﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TTMarketing.BLL.Controllers.Disruptions;
using TTMarketing.BLL.Model.Disruptions;

namespace TTMarketing.WebUI.Disruptions
{
    public partial class Stops : System.Web.UI.Page
    {
        public string primaryRouteNo = "";
        public string _currentUser = "";
        public Disruption dr = null;

        protected void Page_Load(object sender, EventArgs e)
        {

            int id = Int32.Parse(Request.QueryString["e"].ToString());
            dr = DisruptionController.GetDisruptionById(id);
            primaryRouteNo = dr.PrimaryDisruptedRouteNo.ToString();

            //string[] user = HttpContext.Current.User.Identity.Name.Split('\\');
            _currentUser = "NullWindowsUser";

            bool testError = Convert.ToBoolean(ConfigurationManager.AppSettings["TestError"].ToString());
            if (testError)
            {
                throw new Exception("TestError is set to true in config file key: TestError; Please set it to false");
            }
        }
    }
}