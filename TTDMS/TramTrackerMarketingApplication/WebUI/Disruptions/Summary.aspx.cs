﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TTMarketing.BLL.Controllers.Disruptions;
using TTMarketing.BLL.Model.Disruptions;

namespace TTMarketing.WebUI.Disruptions
{
    public partial class Summary : System.Web.UI.Page
    {
        public string primaryRouteNo = "";
        public string _currentUser = "";


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                BindRouteList();
            }


            _currentUser = "NullWindowsUser";

        }

        private void BindRouteList()
        {
            IList<DisruptionRoute> routes = DisruptionRoutesController.GetAllDisruptedRoutes();

            foreach (DisruptionRoute route in routes)
            {
                ListItem item = new ListItem(route.RouteNumber.ToString() + " - " + route.Description, route.RouteNumber.ToString());
                routeListing.Items.Add(item);
            }

            ListItem li = new ListItem("Please select", "0");
            routeListing.Items.Insert(0, li);

        }

    }
}