﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using Newtonsoft.Json;
using TTMarketing.BLL.Controllers.Disruptions;
using TTMarketing.BLL.Model;
using TTMarketing.BLL.Model.Disruptions;
using System.Web.Script.Serialization;

namespace TTMarketing.WebUI.Handlers.Disruptions
{
    /// <summary>
    /// Summary description for UpdateAffectedRouteMessage
    /// </summary>
    public class UpdateAffectedRouteMessage : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {

                ResponseHelper r = new ResponseHelper();

                string data = context.Request.Form[0];
                data = HttpUtility.HtmlDecode(data);
                var json = new JavaScriptSerializer().Deserialize(data, Type.GetType("Object"));
                Dictionary<string, object> dict = (Dictionary<string, object>)json;

                //not the neatest idea, but get JSON packet, and extract values to list
                /*var values = dict.Values.ToList();

                Int32 id = Int32.Parse(values[0].ToString());
                string aff_sml = values[1].ToString();
                string aff_lrg = values[3].ToString();
                bool aff_hasAdditional = bool.Parse(values[4].ToString());

                string unaff_sml = values[5].ToString();
                string unaff_lrg = values[7].ToString();
                bool unaff_hasAdditional = bool.Parse(values[8].ToString());*/
                //string currentUser = values[9].ToString();

                Int32 id = Int32.Parse(dict["id"].ToString());
                string aff_sml = dict["aff_sml"].ToString();
                string aff_lrg = dict["aff_lrg"].ToString();
                bool aff_additionalInfo = bool.Parse(dict["aff_additionalInfo"].ToString());
                //int aff_displayType = Int32.Parse(dict["aff_displayType"].ToString());

                string unaff_sml = dict["unaff_sml"].ToString();
                string unaff_lrg = dict["unaff_lrg"].ToString();
                bool unaff_additionalInfo = bool.Parse(dict["unaff_additionalInfo"].ToString());
                //int unaff_displayType = Int32.Parse(dict["unaff_displayType"].ToString());

                bool hide_up_stop_messages = bool.Parse(dict["hide_up_stop_messages"].ToString());
                bool hide_down_stop_messages = bool.Parse(dict["hide_down_stop_messages"].ToString());

                int? disruption_id = Int32.Parse(dict["disruption_id"].ToString());

                string currentUser = "NullWindowsIndentity";
                try { currentUser = System.Web.HttpContext.Current.User.Identity.Name; }
                catch { Exception cuex; }                

                //System.Threading.Thread.Sleep(5000);

                DisruptionMessagesController.UpdateRouteDisruptionMessages(id, aff_sml, aff_lrg, aff_additionalInfo, unaff_sml, unaff_lrg, unaff_additionalInfo, currentUser, disruption_id, hide_down_stop_messages, hide_up_stop_messages);


                context.Response.Write(JsonConvert.SerializeObject(r));
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                result.Add("error", true);
                result.Add("message", ex.Message);
                context.Response.TrySkipIisCustomErrors = true;
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                context.Response.ContentType = "application/json";
                context.Response.Write(JsonConvert.SerializeObject(result));                                
                context.Response.End();

            }     
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}