﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using Newtonsoft.Json;
using TTMarketing.BLL.Controllers.Disruptions;
using TTMarketing.BLL.Model;
using TTMarketing.BLL.Model.Disruptions;
using System.Web.Script.Serialization;


namespace TTMarketing.WebUI.Handlers.Disruptions
{
    /// <summary>
    /// Summary description for UpdateRouteCombinationMessage
    /// </summary>
    public class UpdateRouteCombinationMessage : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            try 
            {
                ResponseHelper r = new ResponseHelper();
            
                string data = context.Request.Form[0];
                data = HttpUtility.HtmlDecode(data);
                var json = new JavaScriptSerializer().Deserialize(data, Type.GetType("Object"));
                Dictionary<string, object> dict = (Dictionary<string, object>)json;

                Int32 id = Int32.Parse(dict["id"].ToString());

                bool unaffected = bool.Parse(dict["unaffected"].ToString());               
            
                string unaff_sml = dict["unaff_sml"].ToString();            
                string unaff_lrg = dict["unaff_lrg"].ToString();
                bool unaff_additionalInfo = bool.Parse(dict["unaff_additionalInfo"].ToString());
                int unaff_displayType = Int32.Parse(dict["unaff_displayType"].ToString());

                bool hide_up_stop_messages = bool.Parse(dict["hide_up_stop_messages"].ToString());
                bool hide_down_stop_messages = bool.Parse(dict["hide_down_stop_messages"].ToString());

                int disruption_id = Int32.Parse(dict["disruption_id"].ToString());

                string currentUser = "NullWindowsIndentity";
                try { currentUser = System.Web.HttpContext.Current.User.Identity.Name; }
                catch { Exception cuex; }

                if (unaffected)
                {
                    DisruptionMessagesController.UpdateUnaffectedCombinationRouteDisruptionMessages(id, unaff_sml, unaff_lrg, unaff_additionalInfo, unaff_displayType, currentUser, disruption_id, hide_down_stop_messages, hide_up_stop_messages);
                }
                else
                {                    
                    string aff_sml = dict["aff_sml"].ToString();
                    string aff_lrg = dict["aff_lrg"].ToString();
                    bool aff_additionalInfo = bool.Parse(dict["aff_additionalInfo"].ToString());
                    int aff_displayType = Int32.Parse(dict["aff_displayType"].ToString());

                    DisruptionMessagesController.UpdateRouteCombinationDisruptionMessage(id, aff_sml, aff_lrg, aff_additionalInfo, aff_displayType, unaff_sml, unaff_lrg, unaff_additionalInfo, unaff_displayType, currentUser, disruption_id, hide_down_stop_messages, hide_up_stop_messages);
                }
                
                
                context.Response.Write(JsonConvert.SerializeObject(r));
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                result.Add("error", true);
                result.Add("message", ex.Message);
                context.Response.TrySkipIisCustomErrors = true;
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                context.Response.ContentType = "application/json";
                context.Response.Write(JsonConvert.SerializeObject(result));
                context.Response.End();

            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}