﻿/*
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using TTMarketing.BLL.Controllers.Disruptions;
using TTMarketing.BLL.Model;
using TTMarketing.BLL.Model.Disruptions;


namespace TTMarketing.WebUI.Handlers.Disruptions
{
    /// <summary>
    /// Summary description for SaveDisruption
    /// </summary>
    public class SaveDisruption : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            ResponseHelper r = new ResponseHelper();

            try
            {

                int disruptionId = -1;
                DateTime StartDate = new DateTime();

                if (context.Request.Form["ttdisruptionid"] == null || context.Request.Form["ttdisruptionid"] == "")
                {
                    throw new Exception("Please supply an valid Disruption Id");
                }

                bool isValidId = Int32.TryParse(context.Request.Form["ttdisruptionid"].ToString(), out disruptionId);
                if (!isValidId)
                {
                    throw new Exception("Please supply an valid numeric Disruption Id");
                }


                if (context.Request.Form["txtdescription"] == null || context.Request.Form["txtdescription"] == "")
                {
                    throw new Exception("Please supply an Disruption description");
                }





                if (context.Request.Form["startdate"] == null || context.Request.Form["startdate"] == "")
                    throw new Exception("Start date cannot be blank");

                if (context.Request.Form["stHr"] == null || context.Request.Form["stHr"] == "HH")
                    throw new Exception("Please enter a start time");


                string tme = context.Request.Form["stHr"] + ":" + context.Request.Form["stMn"];
                
                StartDate = Convert.ToDateTime(context.Request.Form["startdate"].ToString() + " " + tme);

                //if (DateTime.Now >= StartDate)
                //    throw new Exception("Start date cannot earlier than today");

                Disruption dr = DisruptionController.Create(context.Request.Form["ttdisruptionid"].ToString(), disruptionId, StartDate, context.User.Identity.Name);
                r.responseObject = dr;

            }
            catch (Exception ex)
            {
                r.isError = true;
                r.responseObject = ex;
            }
            context.Response.Write(JsonConvert.SerializeObject(r));


        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
*/
