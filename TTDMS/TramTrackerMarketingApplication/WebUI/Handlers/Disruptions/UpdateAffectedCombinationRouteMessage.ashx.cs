﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using TTMarketing.BLL.Controllers.Disruptions;
using TTMarketing.BLL.Model;
using TTMarketing.BLL.Model.Disruptions;
using System.Web.Script.Serialization;


namespace TTMarketing.WebUI.Handlers.Disruptions
{
    /// <summary>
    /// Summary description for UpdateAffectedCombinationRouteMessage
    /// </summary>
    public class UpdateAffectedCombinationRouteMessage : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            ResponseHelper r = new ResponseHelper();


            //not the neatest idea, but get JSON packet, and extract values to list
            string data = context.Request.Form[0];
            data = HttpUtility.HtmlDecode(data);
            var json = new JavaScriptSerializer().Deserialize(data, Type.GetType("Object"));
            Dictionary<string, object> dict = (Dictionary<string, object>)json;
            var values = dict.Values.ToList();

            Int32 id = Int32.Parse(values[0].ToString());
            string aff_sml = values[1].ToString();
            string aff_med = values[2].ToString();
            string aff_lrg = values[3].ToString();
            bool aff_hasAdditional = bool.Parse(values[4].ToString());
            int aff_displayType = Int32.Parse(values[5].ToString());
            //string currentUser = values[6].ToString();
            //System.Threading.Thread.Sleep(5000);

            string currentUser = "NullWindowsIndentity";
            try { currentUser = System.Web.HttpContext.Current.User.Identity.Name; }
            catch { Exception cuex; }



            //DisruptionMessagesController.UpdateAffectedCombinationRouteDisruptionMessages(id, aff_sml, aff_med, aff_lrg, aff_hasAdditional, aff_displayType, currentUser);
            context.Response.Write(JsonConvert.SerializeObject(r));




        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}