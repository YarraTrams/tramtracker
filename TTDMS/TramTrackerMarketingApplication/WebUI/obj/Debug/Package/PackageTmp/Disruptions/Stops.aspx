﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Stops.aspx.cs" Inherits="TTMarketing.WebUI.Disruptions.Stops" EnableViewState="false" MasterPageFile="../Template.Master" Title="Yarra Trams - TTDMS - Details" %>
<%@ Register TagPrefix="yt" TagName="DisruptionSummaryListing" src="~/Controls/Disruptions/DisruptionSummaryListing.ascx" %>

<asp:Content ID="Content2" runat="server"  ContentPlaceHolderID="head">

    <script type="text/javascript">
        var primaryRouteNo = new String("<%=primaryRouteNo %>");
        var ttmcurrentUser = "<%=_currentUser %>";
        $(document).ready(function () {
            $('#create-nav').addClass("active");
        });
    </script>
    <link href="<%= Page.ResolveClientUrl("~/css/jquery.loadmask.css")%>" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript" src="<%= Page.ResolveClientUrl("~/js/jquery.loadmask.js")%>"></script>
    <script language="javascript" type="text/javascript" src="<%= Page.ResolveClientUrl("~/js/Disruptions/Stops.js")%>"></script>
    <script language="javascript" type="text/javascript" src="<%= Page.ResolveClientUrl("~/js/Disruptions/Common.js")%>"></script>

    <link href="<%= Page.ResolveClientUrl("~/css/fixedHeader/defaultTheme.css")%>" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript" src="<%= Page.ResolveClientUrl("~/js/jquery.fixedheadertable.min.js")%>"></script>
    
    <style type="text/css">
        #selectable {
            height: 400px;          
            overflow-x: hidden;
            -ms-overflow-x: hidden;
        }
        .fht-table-wrapper {
            overflow-x: hidden;
            -ms-overflow-x: hidden;
        }        
        .fht-table th {
            font-weight: bold;
            height: 30px;
            padding-bottom: 5px;            
        }        
        body .modal-wide {
            /* new custom width */
            width: 750px;
            /* must be half of the width, minus scrollbar on the left (30px) */
            margin-left: -345px;
        }

        #message {
            margin-top: 20px;
        }
     
    </style>

</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderId="PageBody" runat="server">
    <div class="row">
        <div class="centred">
            <h3>Disruption Stops (Step 2)</h3>

            <yt:DisruptionSummaryListing ID="DisruptionSummaryListing1" runat="server" />
            <div class="text-center">
                Primary route: <select id="cboRoute" style="width:550px;" name="cboRoute" disabled="disabled"></select>
            </div>
            <ul class="nav nav-tabs">
                <li class="active"><a href="#home" data-toggle="tab" onclick="javascript:stopListView.changeDirection('up')">Up Stops</a></li>
                <li><a href="#profile" data-toggle="tab" onclick="javascript:stopListView.changeDirection('dn')">Down Stops</a></li>                    
            </ul>         

            <div id="stops">
                <div>Click the first affected stop then hold the shift key and click the last affected stop</div>
                <div id="selectable"></div>
            </div>            
        </div>
        <div id="message" class="alert alert-error" style="display:none"></div>

        <div class="form-actions">
            <a href="<%= Page.ResolveClientUrl("~/Disruptions/Details.aspx")%>?d=<%= dr.Id%>" class="btn pull-left">Back</a>                    
            <button type="button" onclick="" class="btn btn-primary pull-right" id="next-btn">
                <span class="static-text">Next</span>
                <span class="working-text" style="display:none">
                    <img src="<%= Page.ResolveClientUrl("~/images/loading.gif")%>" />
                    Validating...
                </span>
            </button>
        </div>
    </div>
</asp:Content>
