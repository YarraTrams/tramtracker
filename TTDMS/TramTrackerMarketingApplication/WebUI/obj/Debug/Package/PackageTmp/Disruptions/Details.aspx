﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Details.aspx.cs" Inherits="TTMarketing.WebUI.Disruptions.Details" EnableViewState="true" MasterPageFile="../Template.Master" Title="Yarra Trams - TTDMS - Details" %>

<asp:Content ID="Content2" runat="server"  ContentPlaceHolderID="head">
    <script language="javascript" type="text/javascript" src="<%= Page.ResolveClientUrl("~/js/Disruptions/Common.js")%>"></script>
    <script language="javascript" type="text/javascript" src="<%= Page.ResolveClientUrl("~/js/Disruptions/Details.js")%>"></script>
    <script type="text/javascript">
        var disruption_id = 0;
        var current_foc_id = 0;
        var current_primary_route_no = 0;
        
        $(document).ready(function () {
            //this is to highlight the correct item in the bootstrap nav
            $('#create-nav').addClass("active");
            $('#main_form').addClass('form-horizontal');            
        });        
    </script>
    <style type="text/css">
        .centred {
          margin-left: auto;
          margin-right: auto;
          float: none;
        }

        .form-horizontal .form-actions {
            padding-left: 19px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderId="PageBody" runat="server">
    <div class="row">
        <div class="span6 centred">
            <h3>Disruption Details (Step 1)</h3>

            <input type="hidden" runat="server" id="disruptionId" value="0" />
            <input type="hidden" runat="server" id="currentprimaryRouteNo" value="0" />

            <div class="alert alert-error" id="generic-error-output" style="display:none"></div>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="List" CssClass="alert alert-error" />

            <div class="control-group" id="foc-checkbox-holder">
                <label class="checkbox">
                    <input type="checkbox" id="has_foc_checkbox" value="" checked="true" name="has_foc_checkbox" />
                    This disruption has an associated FOC
                </label>
            </div>

            <div class="control-group" id="foc-holder">
                <label class="control-label" for="ttdisruptionid">FOC Disruption ID:</label>
                <div class="controls">
                    <div class="input-append">
                        <input type="text" runat="server" id="ttdisruptionid" />
                        <button class="btn btn-primary" type="button" onclick="detailsView.validateFOC()" id="validate-btn">
                            <span class="static-text">Validate FOC ID</span>
                            <span class="working-text" style="display:none">
                                <img src="<%= Page.ResolveClientUrl("~/images/loading.gif")%>" />
                                Validating...
                            </span>
                        </button>
                    </div>
                    <span class="help-inline" style="display:none;">The specified FOC is not valid</span>
                </div>
            </div>
            <div class="well well-large" id="foc-details" style="display:none;">
                <dl class="dl-horizontal">
                  <dt>FOC ID</dt>
                    <dd id="detail-foc_id"><asp:Literal runat="server" ID="focId"></asp:Literal></dd>
                    <dt>Primary Route</dt>
                    <dd id="detail-focRouteNo"><asp:Literal runat="server" ID="focRouteNo"></asp:Literal></dd>
                    <dt>Up From Stop</dt>
                    <dd id="detail-focUpFrom"><asp:Literal runat="server" ID="focUpFrom"></asp:Literal>&nbsp;-&nbsp;<asp:Literal runat="server" ID="focUpFromStopName"></asp:Literal>&nbsp;<asp:Literal runat="server" ID="focUpFromStopDirection"></asp:Literal></dd>
                    <dt>Up To Stop</dt>
                    <dd id="detail-focUpTo"><asp:Literal runat="server" ID="focUpTo"></asp:Literal>&nbsp;-&nbsp;<asp:Literal runat="server" ID="focUpToStopName"></asp:Literal>&nbsp;<asp:Literal runat="server" ID="focUpToStopDirection"></asp:Literal></dd>
                    <dt>Down From Stop</dt>
                    <dd id="detail-focDownFrom"><asp:Literal runat="server" ID="focDownFrom"></asp:Literal>&nbsp;-&nbsp;<asp:Literal runat="server" ID="focDownFromStopName"></asp:Literal>&nbsp;<asp:Literal runat="server" ID="focDownFromStopDirection"></asp:Literal></dd>
                    <dt>Down To Stop</dt>
                    <dd id="detail-focDownTo"><asp:Literal runat="server" ID="focDownTo"></asp:Literal>&nbsp;-&nbsp;<asp:Literal runat="server" ID="focDownToStopName"></asp:Literal>&nbsp;<asp:Literal runat="server" ID="focDownToStopDirection"></asp:Literal></dd>
                    <dt>Date</dt>
                    <dd id="detail-focDate"><asp:Literal runat="server" ID="focDate"></asp:Literal></dd>
                    <dt>Message</dt>
                    <dd id="detail-focMessage"><asp:Literal runat="server" ID="focMessage"></asp:Literal></dd>
                </dl>
            </div>

            <div id="disruption-fields" style="display:none;">
                <div class="control-group">
                    <label class="control-label" for="name">Name:</label>
                    <div class="controls">
                        <input class="span4" runat="server" type="text" id="name" />
                    </div>
                    <span class="help-inline" style="display:none;">The name field is required</span>
                </div>

                <div class="control-group">
                    <label class="control-label" for="routeList">Primary Route No:</label>
                    <div class="controls">
                        <asp:DropDownList class="span4" runat="server" ID="routeList" ></asp:DropDownList> 
                    </div>
                    <span class="help-inline" style="display:none;">The primary route field is required</span>
                </div>

                <textarea rows="2" style="display:none" runat="server" id="txtdescription" ></textarea>

                <div class="form-actions">
                    <a href="<%= Page.ResolveClientUrl("~/Disruptions/Listing.aspx")%>" class="btn pull-left">Cancel</a>                    
                    <button type="button" onclick="detailsView.saveDisruption()" class="btn btn-primary pull-right" id="save-btn">
                        <span class="static-text">Next</span>
                        <span class="working-text" style="display:none">
                            <img src="<%= Page.ResolveClientUrl("~/images/loading.gif")%>" />
                            Saving...
                        </span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
