﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Listing.aspx.cs" Inherits="TTMarketing.WebUI.Disruptions.Listing"  MasterPageFile="../Template.Master" Title="Yarra Trams - TTDMS - Disruptions" %>


<asp:Content ID="Content1" ContentPlaceHolderId="PageBody" runat="server">    
    <div class="row ie-row-fix">
        <div class="span12">
            <h3>Disruptions</h3>
            <p>This list shows all currently active disruptions, to make alterations to an existing disruption use the options in the far right column.</p>
            <table id="events-table" border="0" width="100%" class="table table-striped table-bordered">
               <asp:Repeater runat="server" ID="disruptionListing" onitemdatabound="disruptionListing_ItemDataBound">
                <HeaderTemplate>
                    <thead>
                       <tr>
                           <th>ID</th>
                           <th>FOC ID</th>
                           <th>Name</th>
                           <th>Primary Route No</th>
                           <th>Affecting Routes</th>
                           <th>Status</th>
                           <th>Actions</th>
                       </tr>
                    </thead>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr id="row_<%# Eval("Id")%>" class="<%#(Eval("Valid").ToString() != "True" ? "error" : "") %>">
                        <td> <%# Eval("Id")%></td>
                        <td> <%# Eval("TT_DisruptionId")%></td>
                        <td> <%# Eval("Name")%></td>
                        <td> <%# Eval("PrimaryDisruptedRouteNo")%></td>
                        <td> <%# Eval("AffectedRoutesString")%></td>
                        <td class="status-column"> <%# GetStatusVal(Eval("Status"), Eval("ExpiryTime"))%></td>
                        <td> 
                            <a href="Details.aspx?d=<%# Eval("Id")%>">Edit</a> | 
                            <a class="clr" href="javascript:;" id="<%# Eval("Id")%>" data-valid="<%#Eval("Valid").ToString().ToLower()%>" >Clear</a> | 
                            <a class="review" href="javascript:;" route-no="<%# Eval("PrimaryDisruptedRouteNo")%>" for="<%# Eval("Id")%>" data-disruption="<%# Eval("Id")%>" >Review</a> |
                            <a class="email" href="javascript:;" route-no="<%# Eval("PrimaryDisruptedRouteNo")%>" for="<%# Eval("Id")%>" >Email</a>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    <tr>
                        <td colspan="7">
                            <asp:Label ID="lblEmptyData" Text="There are currently no active disruptions in the system" runat="server" Visible="false" class="text-center"  width="100%"></asp:Label>
                        </td>
                    </tr>                          
                </FooterTemplate>
               </asp:Repeater>
            </table>
            <button type="button" class="btn btn-primary pull-right" id="publish-btn" data-toggle="tooltip" data-placement="left" title="" data-original-title="Publish Changes">Publish Disruptions</button>
        </div>
    </div>

    <div class="modal hide fade" id="clear-confirm">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>Confirm Clearance</h3>
        </div>
        <div class="modal-body form-horizontal">
            <div id="clear-response-output">

            </div>
            <div id="invalid-warning" class="alert alert-error">
                <b>This disruption is not valid, this is most likely because required messages are missing.</b><br /><br />
                If you wish to remove this disruption you can clear it now or click <a href="" id="invalid-edit-link">here</a> and ensure all required stops and messages are set.
            </div>
            <div id="not-all-valid" class="alert alert-warning">
                <b>This disruption cannot be cleared.</b><br /><br />
                There are other active disruptions which are missing required messages, please close this dialog and review any disruptions marked in red in the disruptions list.
            </div>
            <div id="clear-controls">
                <div class="control-group text-center">
                    <label class="control-label" for="ttl">Mark as cleared in:</label>
                    <div class="controls">                    
                        <select id="ttl">
                            <option value="0">Immediately</option>
                            <option value="10">10 Minutes</option>
                            <option value="20">20 Minutes</option>
                            <option value="30">30 Minutes</option>
                            <option value="40">40 Minutes</option>
                            <option value="50">50 Minutes</option>
                            <option value="60">1 Hour</option>
                            <option value="70">1 Hour, 10 Minutes</option>
                            <option value="80">1 Hour, 20 Minutes</option>
                            <option value="90">1 Hour, 30 Minutes</option>
                            <option value="100">1 Hour, 40 Minutes</option>
                            <option value="110">1 Hour, 50 Minutes</option>
                            <option value="120">2 Hours</option>
                        </select>                    
                    </div>                
                </div>
            </div>
            <div id="shared-messages-warning">
                <b>The following messages are shared with other disruptions, please make sure you amend any relevant shared messages after clearing:<br /></b>
                <div id="shared-messages-warning-body">

                </div>
            </div>
            
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" id="clear-ok-button">OK</button>            
            <button class="btn" data-dismiss="modal" aria-hidden="true" id="clear-close-button">Cancel</button>            
            <button type="button" class="btn btn-primary" id="clear-disruption">
                <span class="static-text">Clear Disruption</span>
                <span class="working-text" style="display:none">
                    <img src="<%= Page.ResolveClientUrl("~/images/loading.gif")%>" />
                    Clearing...
                </span>
            </button>
        </div>
    </div>

    <div class="modal hide fade modal-wide" id="review-modal">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>Review</h3>
        </div>
        <div class="modal-body form-horizontal">
            <div id="route-review" class="stops">
                <div class="control-group text-center">
                    <label class="control-label" for="ttl">Route:</label>
                    <div class="controls">   
                        <asp:DropDownList ID="routeListing" runat="server"></asp:DropDownList>
                    </div>                
                </div>
                <div class="row-fluid" id="stops-review-div">
                    <div class="span6">
                        <table class="table table-striped table-bordered" id="upstops-table">
                            <thead>
                                <tr>
                                    <th>Up Stops</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                    <div class="span6">
                        <table class="table table-striped table-bordered" id="downstops-table">
                            <thead>
                                <tr>
                                    <th>Down Stops</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div id="message-view" class="messages">
                <table class="table table-striped table-bordered" id="message-details">
                    <thead>
                        <tr>
                            <th>Route</th>
                            <th>Short Message</th>
                            <th>Long Message</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
                
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" id="stops-back" class="btn btn-primary messages">Back</button>
            <button class="btn btn-primary stops" data-dismiss="modal" aria-hidden="true">OK</button>            
        </div>
    </div>

    <div class="modal hide fade not-sent" id="send-email">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>Send Notifications</h3>
        </div>
        <div class="modal-body">     
            <div class="successfull" id="email-success-message">
                Notification sent successfully.
            </div>       
            <div class="unsent">
                <div class="control-group form-horizontal">
                    <label class="control-label" for="inputEmail">To</label>
                    <div class="controls">
                    <input type="text" id="notification-to" placeholder="To" />
                    </div>
                </div>
                <div class="control-group form-horizontal">
                    <label class="control-label" for="inputEmail">Subject</label>
                    <div class="controls">
                    <input type="text" id="notification-subject" placeholder="Subject" />
                    </div>
                </div>
                <div class="control-group email-input">
                    <label class="control-label" for="inputPassword">Notification Message</label>
                    <div class="controls">
                        <textarea id="notification-message" rows="4"></textarea>
                    </div>
                </div>            
            </div>
            <h5>Sent Notifications</h5>
            <div id="email-logs">

            </div>          
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary successfull" data-dismiss="modal" aria-hidden="true">OK</button>
            <button class="btn unsent" data-dismiss="modal" aria-hidden="true">Cancel</button>            
            <button type="button" class="btn btn-primary unsent" id="send-email-btn">
                <span class="static-text">Send Notifications</span>
                <span class="working-text" style="display:none">
                    <img src="<%= Page.ResolveClientUrl("~/images/loading.gif")%>" />
                    Sending...
                </span>
            </button>
        </div>
    </div>

    <div class="modal hide fade not-published" id="publish-confirm">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>Publish Messages</h3>
        </div>
        <div class="modal-body">
            <div class="unpublished">
                Are you sure you wish to publish all pending changes to the disruption messages?
            </div>
            <div class="successfull">
                All changes were published successfully.
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary successfull" data-dismiss="modal" aria-hidden="true">OK</button>            
            <button class="btn unpublished" data-dismiss="modal" aria-hidden="true">Cancel</button>            
            <button type="button" class="btn btn-primary unpublished" id="publish-confirm-btn">
                <span class="static-text">Publish Disruptions</span>
                <span class="working-text" style="display:none">
                    <img src="<%= Page.ResolveClientUrl("~/images/loading.gif")%>" />
                    Publishing...
                </span>
            </button>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content2" runat="server"  ContentPlaceHolderID="head">
    <style type="text/css">
        body .modal-wide {
            /* new custom width */
            width: 850px;
            /* must be half of the width, minus scrollbar on the left (30px) */
            margin-left: -395px;
        }

        #PageBody_routeListing {
            width:100%;
        }

        .email-input .controls {
            margin-right:15px;
        }

        .email-input .controls textarea {
            width:100%;
        }        

        #review-modal.stops-list .messages {
            display:none;
        }

        #review-modal.messages-list .stops {
            display:none;
        }

        #publish-confirm.published .unpublished {
            display:none;
        }

        #publish-confirm.not-published .successfull {
            display:none;
        }

        #send-email .form-horizontal .control-label{        
            text-align:left;            
        }

        #send-email .form-horizontal input{        
            width:100%;
        }

        #send-email .form-horizontal .controls {
            margin-right:15px;
        }

        #send-email.sent .unsent {
            display:none;
        }

        #send-email.not-sent .successfull {
            display:none;
        }

        #email-success-message {
            margin-bottom:30px;
            text-align:center;
        }

    </style>
    <link href="<%= Page.ResolveClientUrl("~/css/jquery.loadmask.css")%>" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript" src="<%= Page.ResolveClientUrl("~/js/jquery.loadmask.js")%>"></script>
    <script language="javascript" type="text/javascript" src="<%= Page.ResolveClientUrl("~/js/Disruptions/Common.js")%>"></script>
    <script language="javascript" type="text/javascript" src="<%= Page.ResolveClientUrl("~/js/Disruptions/Listing.js")%>"></script>
    <script type="text/javascript">
        var ttmcurrentUser = "<%=_currentUser %>";
        var publishEnabled = <%= publishEnabled.ToString().ToLower()%>;
        var publishReason = <%= Json.Encode(publishReason)%>;
        var allValid = <%= allValid.ToString().ToLower()%>;
        //this is to highlight the correct item in the bootstrap nav
        $(document).ready(function () {
            $('#view-nav').addClass("active");
            if(!publishEnabled) {
                $("#publish-btn").attr("title", publishReason);
                $("#publish-btn").removeClass("btn-primary").addClass("disabled");
                $('#publish-btn').tooltip();
            }
            var error_rows = $("#events-table tr.error");
            if(error_rows) {
                error_rows.attr("data-toggle", "tooltip");
                error_rows.attr("data-placement", "left");
                error_rows.attr("title", "This disruption is not valid and cannot be published, please ensure that all required messages are set or clear this disruption");
                error_rows.tooltip();
            }                       
        });
    </script>
</asp:Content>


