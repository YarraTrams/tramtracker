﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Messages.aspx.cs" Inherits="TTMarketing.WebUI.Disruptions.Messages" EnableViewState="false" MasterPageFile="../Template.Master" Title="Yarra Trams - TTDMS - Details" %>

<%@ Register TagPrefix="mf" TagName="MessagesForm" src="~/Controls/Disruptions/MessagesForm.ascx"  %>

<asp:Content ID="Content2" runat="server"  ContentPlaceHolderID="head">
    <style type="text/css">
        .message-textarea {
            width:100%;
        }
        .the-fix { padding-right: 10px; }
        .update-btn-holder {
            height: 30px;
        }
        .update-btn-holder .checkbox {
            margin-top: 6px;
            margin-left: 10px;
        }
        .count-holder {
            height: 25px;
        }
        .messages-holder label.control-label {
            text-align:left;
        }
        .messages-holder select {
            width: 195px;
        }

        .message-input span.help-inline {
            display:none;
        }
         .message-input.error span.help-inline {
            display:inline;
        }

        .disrupted-table tbody tr:hover td {
            background-color: #d9edf7;
            cursor:pointer;
            cursor:hand;
        }
        .disrupted-table tbody tr td label {
            margin-bottom:0px;
        }
        body .modal-wide {
            /* new custom width */
            width: 850px;
            /* must be half of the width, minus scrollbar on the left (30px) */
            margin-left: -395px;
        }

        body .modal-pid {
            /* new custom width */
            width: 1080px;
            height:800px;
            /* must be half of the width, minus scrollbar on the left (30px) */
            margin-left: -580px;
        }

        body .modal-pid .modal-body {
            max-height:none;
        }
        .centred {
          margin-left: auto;
          margin-right: auto;
          float: none;
        }

        .long-message-checkbox-holder {
            height:30px;
            margin-top:30px;
        }

        #messages-modal .long-message-checkbox-holder {
            height:30px;
            margin-top:0px;
        }

        .message-input, .message-textarea {
            margin-bottom:0px;
        }

        #previewPIDDiv {
            top:2%;
        }

        #pid-preview-text {
            margin-left:0px;
            line-height: 72px;
            color:#000;
            border: none;
            box-shadow: none;
            border-radius: 0px;
        }

        /*make table responsive on small screens*/
        @media (max-width: 767px) {
            .table {
                word-break: break-all;
                table-layout: fixed;
            }
        }
    </style>
    <link href="<%= Page.ResolveClientUrl("~/css/jquery.loadmask.css")%>" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript" src="<%= Page.ResolveClientUrl("~/js/jquery.charcounter.js")%>"></script>
    <script language="javascript" type="text/javascript" src="<%= Page.ResolveClientUrl("~/js/jquery.loadmask.js")%>"></script>
    <script language="javascript" type="text/javascript" src="<%= Page.ResolveClientUrl("~/js/Disruptions/Common.js")%>"></script>
    <script language="javascript" type="text/javascript" src="<%= Page.ResolveClientUrl("~/js/Disruptions/Messages.js")%>"></script>
    
    <script type="text/javascript">
        var disruption_messages = {}; //this object holds all the message data client side  so it can be manipulated and sent back to the server

        var selected_disruption_id = '<%=disruption.Id%>';//The id of the disruption in the active tab

        var selected_message = null; //the message selected by clicking a row in the messages table

        var ttmcurrentUser = "<%=_currentUser %>";

        var selected_row_id = null; //the id of the most recently clicked row element

        var pids_field_id = null; //the id of the textarea the pids preview was opened for

        $(document).ready(function () {
            $('#create-nav').addClass("active");
            messagesListView.bindPredefinedMessage();
        });

        var predefined_affected_messages = <%=AffectedMessages%>;
        var predefined_unaffected_messages = <%=UnaffectedMessages%>;
       
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderId="PageBody" runat="server">
    
       
    <div class="row">
       <div class="span10 centred">
           <h3>Disruption Messages (Step 3)</h3>
            
            <ul class="nav nav-tabs">
                <asp:Repeater runat="server" ID="disruptionTabs">
                    <ItemTemplate>
                        <li class="<asp:literal runat="server" ID="tabClass" />"><a href="#" data-toggle="tab" onclick="javascript:messagesListView.loadDisruption('<%# Eval("Id")%>')"><asp:literal runat="server" ID="tabText" /></a></li>                                        
                    </ItemTemplate>
                </asp:Repeater>
            </ul>    
            <div id="disruption-messages">
                <mf:MessagesForm ID="MessagesForm1" runat="server" />                   
            </div>
           <div id="validation-message" class="alert alert-error" style="display:none"></div>
            <div class="form-actions">
                <a href="<%= Page.ResolveClientUrl("~/Disruptions/Stops.aspx")%>?e=<%= disruption.Id%>" class="btn pull-left">Back</a>                    
                <button type="button" onclick="messagesListView.validateDisruption(selected_disruption_id)" class="btn btn-primary pull-right" id="next-btn">
                    <span class="static-text">Next</span>
                    <span class="working-text" style="display:none">
                    <img src="<%= Page.ResolveClientUrl("~/images/loading.gif")%>" />
                    Validating...
                    </span>
                </button>
            </div>
       </div>
    </div>

    <div id="messages-modal" class="modal hide fade modal-wide">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>Modal header</h3>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="span4 message-input-holder">
                    <h4>Affected Stops Message</h4>
                    <div class="messages-holder form-horizontal">
                        <div class="control-group">
                            <label class="control-label">
                                Predefined Messages                                          
                            </label>
                            <div class="controls">
                                <select class="message-dropdown affected-message-dropdown" for="modal-affected-short-message" data-long-message="modal-affected-long-message" data-modal-input="true">                                    
                                </select>      
                            </div>
                        </div>
                    </div>
                    <label class="the-fix">
                        <div class="control-group message-input">
                            <textarea class="message-textarea short-message" rows="3" id="modal-affected-short-message" data-long-message="modal-affected-long-message" data-long-message-checkbox="modal-long-message-checkbox"></textarea>
                            <span class="help-inline">You must enter a value to update messages</span>
                        </div>
                        <div class="count-holder">
                            <a href="javascript:void(0)" class="pid-link pull-left" data-for="modal-affected-short-message" id="modal-affected-pid-preview">PID Preview</a><span class="pull-right count" id="affected-short-message-modal-count"></span>
                        </div>
                    </label>                                  
                </div>
                    
                <div class="span4 pull-right message-input-holder">
                    <h4>Unaffected Stops Message</h4>
                    <div class="messages-holder form-horizontal">
                        <div class="control-group">
                            <label class="control-label">
                                Predefined Messages                                          
                            </label>
                            <div class="controls">
                                <select class="message-dropdown unaffected-message-dropdown" for="modal-unaffected-short-message"  data-long-message="modal-unaffected-long-message" data-modal-input="true">                                    
                                </select>      
                            </div>
                        </div>
                    </div>
                    <label class="the-fix">
                        <div class="control-group message-input">
                            <textarea class="message-textarea short-message" rows="3" id="modal-unaffected-short-message" data-long-message="modal-unaffected-long-message" data-long-message-checkbox="modal-long-message-checkbox"></textarea>
                            <span class="help-inline">You must enter a value to update messages</span>
                        </div>
                        <div class="count-holder">
                            <a href="javascript:void(0)" class="pid-link pull-left" data-for="modal-unaffected-short-message">PID Preview</a><span class="pull-right count" id="unaffected-short-message-modal-count"></span>
                        </div>
                    </label>
                                  
                </div>
            </div>   
            <div class="row">
                <div class="span4">
                    <div class="long-message-checkbox-holder">
                        <label class="checkbox pull-left">
                            <input type="checkbox" class="show-long-messages" data-for="modal-long-messages" id="modal-long-message-checkbox" />
                            Add long messages
                        </label>
                    </div>
                </div>
            </div>
            <div class="row long-messages-row" id="modal-long-messages" style="display:none">
                <div class="span4 message-input-holder">
                    <h4>Affected Long Message</h4>                    
                    <label class="the-fix">
                        <div class="control-group message-input">
                            <textarea class="message-textarea long-message" rows="3" id="modal-affected-long-message"></textarea>
                            <span class="help-inline">You must enter a value to update messages</span>
                        </div>
                        <div class="count-holder">
                            <span class="pull-right long-count" id="affected-long-message-modal-count"></span>
                        </div>
                    </label>                                  
                </div>
                    
                <div class="span4 pull-right message-input-holder">
                    <h4>Unaffected Long Message</h4>                    
                    <label class="the-fix">
                        <div class="control-group message-input">
                            <textarea class="message-textarea long-message" rows="3" id="modal-unaffected-long-message"></textarea>
                            <span class="help-inline">You must enter a value to update messages</span>
                        </div>
                        <div class="count-holder">
                            <span class="pull-right long-count" id="unaffected-long-message-modal-count"></span>
                        </div>
                    </label>
                                  
                </div>
            </div>           
        </div>        
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
            <button type="button" class="btn btn-primary" id="modal-save">Save changes</button>
        </div>
    </div>
                                                 
                    

      <div class="modal hide fade modal-wide" id="affected-stops">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>Affected Stops</h3>
        </div>
        <div class="modal-body">
            
            <table id="affectedStopData" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Route No</th>
                        <th>Stop</th>
                        <th>Tramtracker Id</th>            
                        <th>Direction</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>            
        </div>
    </div>

    <div class="modal hide fade modal-wide" id="foc-disruption">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>FOC Details</h3>
        </div>
        <div class="modal-body">
            
            <table id="focData" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>FOC Disruption Id</th>
                        <th>Created</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>            
        </div>
    </div>

    <div class="modal hide fade modal-pid" id="previewPIDDiv">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>PID Preview</h3>
        </div>
        <div class="modal-body">            
            <div id="preview-pid">
                <textarea id="pid-preview-text" name="pid-preview-text"></textarea>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>   
            <button type="button" class="btn btn-primary" id="pid-save">Save changes</button>         
        </div>
    </div>


        

    <!--<div id="affected-stops" title="Affected Stops" style="display:none">
	    <table width="100%"  id="affectedStopData" border="1">
            <tr>
                <th>Route No</th>
                <th>Stop</th>
                <th>Tramtracker Id</th>
                <th>Direction</th>
            </tr>
        </table>
    </div>-->

    <div id="previewCombo" title="Tabular Display" style="display:none">
	    <table width="100%"  id="previewComboDisplay" border="1">
            <tr>
                <th width="20%">Route No</th>
                <th>Message</th>
            </tr>
        </table>
    </div>

    <!--<div id="previewPIDDiv" title="PID Preview">
        <div id="preview-pid">
            <textarea id="pid-preview-text" name="pid-preview-text"></textarea>
        </div>
        <div class="prev-count" id="prev_chars"></div>
    </div>-->

    <!--<div id="foc-disruption" title="FOC Disruption" style="display:none">
	    <table width="100%"  id="focData" border="1">
            <tr>
                <th>FOC Disruption Id</th>
                <th>Created</th>
            </tr>
        </table>
    </div>-->

    <div id="msg-copy-warning" title="Warning">
        <p>
            <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span>
            Messages will only be copied to blank message boxes. If you need to update message boxes that currently
            have text in them, you will need to do so manually.
        </p>
        <p>
            <input type="checkbox" name="show-msg-copy-warning" id="show-msg-copy-warning" /> Don't show this message again
        </p>
    </div>



</asp:Content>