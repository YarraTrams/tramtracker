﻿
$(document).ready(function () {


    $(document.body).on("click", ".viewa", function () {

        var id = String(this.id);
        var arr = id.split("_");

        $('#affected-stops').modal({});
        $("#affectedStopData tbody").html('');

        $("#affected-stops").mask("Loading...");

        var noCache = new Date().getTime();

        var strUrl = "../Services/DisruptionsService.svc/GetAffectedStopsByDisruptionId/?d=" + arr[1] + "&ts=" + noCache; ;

        var str = "";

        var jqxhr = $.getJSON(strUrl, function (data) {
            $.each(data.responseObject, function (i, msg) {

                str = "<tr><td>" + msg.RouteNumber + "</td>";
                str += "<td>" + msg.Description + "</td>";
                str += "<td>" + msg.TrackerID + "</td>";
                str += "<td>" + msg.Direction + "</td></tr>";
                $("#affectedStopData tbody").append(str);
            });

        }).always(function () { $("#affected-stops").unmask(); });
    }); // end viewa click


    $(document.body).on("click", ".viewfoc", function () {
        var id = String(this.id);
        var arr = id.split("_");

        $('#foc-disruption').modal({});
        $("#focData tbody").html('');

        $("#foc-disruption").mask("Loading...");

        var noCache = new Date().getTime();
        var strUrl = "../Services/DisruptionsService.svc/GetFOCDisruptionById/?e=" + arr[1] + "&ts=" + noCache; ;
        var str = "";

        var jqxhr = $.getJSON(strUrl, function (data) {
            var f = data.responseObject;
            var dstring = String(f.DisruptionDateTime).substring(1, f.DisruptionDateTime.length - 1);
            var date = eval("new " + dstring);

            str = "<tr><td>" + f.Id + "</td><td>" + date + "</td></tr>";

            str += "<tr><td><b>Up From Stop No</b></td><td><b>Up To Stop No</b></td></tr>";
            str += "<tr><td>" + f.UpFromStopNo + "</td><td>" + f.UpToStopNo + "</td></tr>";
            str += "<tr><td><b>Up From Stop Name</b></td><td><b>Up To Stop Name</b></td></tr>";
            str += "<tr><td>" + f.UpFromStopName + "</td><td>" + f.UpToStopName + "</td></tr>";
            str += "<tr><td><b>Up From Stop Direction</b></td><td><b>Up To Stop Direction</b></td></tr>";
            str += "<tr><td>" + f.UpFromStopDirection + "</td><td>" + f.UpToStopDirection + "</td></tr>";

            str += "<tr><td><b>Down From Stop No</b></td><td><b>Down To Stop No</b></td></tr>";
            str += "<tr><td>" + f.DownFromStopNo + "</td><td>" + f.DownToStopNo + "</td></tr>";
            str += "<tr><td><b>Down From Stop Name</b></td><td><b>Down To Stop Name</b></td></tr>";
            str += "<tr><td>" + f.DownFromStopName + "</td><td>" + f.DownToStopName + "</td></tr>";
            str += "<tr><td><b>Down From Stop Direction</b></td><td><b>Down To Stop Direction</b></td></tr>";
            str += "<tr><td>" + f.DownFromStopDirection + "</td><td>" + f.DownToStopDirection + "</td></tr>";

            str += "<tr><td><b>Message</b></td><td valign='top'>" + f.Message + "</td></tr>";

            $("#focData tbody").append(str);

        }).always(function () { $("#foc-disruption").unmask(); });
    });
});

function showButtonWorking(element_id) {
    var button = $('#' + element_id);
    button.find(".static-text").hide();
    button.find(".working-text").show();
    button.attr("disabled", "true");
    button.removeClass("btn-primary");
}

function hideButtonWorking(element_id) {
    var button = $('#' + element_id);
    button.find(".static-text").show();
    button.find(".working-text").hide();
    button.removeAttr("disabled");
    button.addClass("btn-primary");
}

String.prototype.nl2br = function () {
    return this.replace(/\n/g, "<br />");
}


function log(obj1, obj2) {
    if (window.console) {
        if (obj2) {
            console.log(obj1, obj2);
        }
        else {
            console.log(obj1);
        }
    }
}

