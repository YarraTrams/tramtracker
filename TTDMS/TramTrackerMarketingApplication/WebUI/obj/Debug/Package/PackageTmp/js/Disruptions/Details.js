﻿var detailsView = {
    init: function () {        
        this.bindUIElements();
    },
    checkRouteChange: function (event) {
        /*var currentRouteId = $("#currentprimaryRouteNo").val();
        var newRouteId = $("#routeList").val();

        if (currentRouteId === "0") return;

        if (currentRouteId !== newRouteId) {
            var str = "Please note, if you change the Primary Route No for this Disruption, all affected Stops assigned for that Route will be removed, and you will need to add the affected Stops for the changed Route.";
            alert(str);
        }*/
    },
    
    bindUIElements: function () {
        disruption_id = $('#PageBody_disruptionId').val();
        current_foc_id = $('#PageBody_ttdisruptionid').val();
        if (current_foc_id == '') {
            current_foc_id = 0;
        }
        current_primary_route_no = $('#PageBody_currentprimaryRouteNo').val();

        $('#create-nav').addClass("active");
        $('#main_form').addClass('form-horizontal');

        if (disruption_id != 0) {
            $('#validate-btn').attr("disabled", "true");
            $('#has_foc_checkbox').attr("disabled", "true");
            $('#disruption-fields').slideDown();
            $('#foc-checkbox-holder').hide();
            if (current_foc_id != 0) {
                $('#foc-details').slideDown();
                $('#foc-holder').hide();
            }
            else {
                $('#foc-holder').hide();
                $('#has_foc_checkbox').removeAttr("checked");
            }
        }

        $('#has_foc_checkbox').on("change", function (e) {
            var has_foc = $(e.target).is(':checked');
            if (has_foc) {
                $('#foc-holder').slideDown();
                $('#disruption-fields').slideUp();
            }
            else {
                $('#foc-holder').slideUp();
                $('#disruption-fields').slideDown();
                $('#foc-details').slideUp();
            }
        });
    },

    validateFOC : function () {
        detailsView.hideMessage('PageBody_ttdisruptionid', 'success');
        var foc_id = $('#PageBody_ttdisruptionid').val();
        if (foc_id == '') {
            showError('PageBody_ttdisruptionid', "The FOC ID field is required if the disruption has an associated FOC");
            $('#foc-details').slideUp();
        }
        else {
            showButtonWorking('validate-btn');

            var o = new Object();
            o.foc_id = foc_id;
            var x = JSON.stringify(o);
            $.ajax({
                url: '../Disruptions/Details.aspx/ValidateFOCId',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json;charset=utf-8;',
                data: x,
                success: function (data) {                    
                    log(data);                                            
                    if (data.d.valid != "true") {
                        detailsView.showError('PageBody_ttdisruptionid', data.d.message);
                        $('#disruption-fields').slideUp();
                        $('#foc-details').slideUp();
                    }
                    else {
                        detailsView.hideError('PageBody_ttdisruptionid');
                        detailsView.showFieldMessage('PageBody_ttdisruptionid', 'FOC ID validated successfully', 'success');
                        $('#disruption-fields').slideDown();
                        $('#detail-foc_id').html(foc_id);
                        $('#detail-focRouteNo').html(data.d.focRouteNo);                            
                        $('#detail-focUpFrom').html(data.d.focUpFrom + ' - ' + data.d.focUpFromStopName + ' ' + data.d.focUpFromStopDirection);
                        $('#detail-focUpTo').html(data.d.focUpTo + ' - ' + data.d.focUpToStopName + ' ' + data.d.focUpToStopDirection);
                        $('#detail-focDownFrom').html(data.d.focDownFrom + ' - ' + data.d.focDownFromStopName + ' ' + data.d.focDownFromStopDirection);
                        $('#detail-focDownTo').html(data.d.focDownTo + ' - ' + data.d.focDownToStopName + ' ' + data.d.focDownToStopDirection);
                        $('#detail-focDate').html(data.d.focDate);
                        $('#detail-focMessage').html(data.d.focMessage);
                        $('#foc-details').slideDown();
                        $('#PageBody_routeList').val(data.d.focRouteNo);
                    }
                    hideButtonWorking('validate-btn');
                },
                error: function (a, b, c) {
                    alert(b);                        
                    hideButtonWorking('validate-btn');
                    $('#foc-details').slideUp();
                }
            });
        }
    },

    showFieldMessage : function (element_id, message, message_class) {
        var cgroup = $('#' + element_id).parents('.control-group');
        cgroup.addClass(message_class);

        if (message != '') {
            var mspan = cgroup.find('.help-inline');
            mspan.html(message);
            mspan.slideDown();
        }
    },

    showError : function (element_id, message) {
        detailsView.hideMessage(element_id, 'success');
        detailsView.showFieldMessage(element_id, message, "error");
    },

    hideError : function (element_id) {
        detailsView.hideMessage(element_id, "error")
    },

    hideMessage : function (element_id, message_class) {
        var cgroup = $('#' + element_id).parents('.control-group');
        cgroup.removeClass(message_class);
        var mspan = cgroup.find('.help-inline');
        mspan.slideUp();
    },

    saveDisruption : function () {
        var valid = true;
        $('#generic-error-output').hide();
        detailsView.hideError('PageBody_name');
        detailsView.hideError('PageBody_routeList');
        detailsView.hideError('PageBody_ttdisruptionid');
        var has_foc = $('#has_foc_checkbox').is(':checked');
        var foc_id = $('#PageBody_ttdisruptionid').val();
        if (has_foc) {                
            if (foc_id == '') {
                valid = false;
                detailsView.showError('PageBody_ttdisruptionid', "The FOC ID field is required if the disruption has an associated FOC");
                $('#foc-details').slideUp();
            }
        }

        var name = $('#PageBody_name').val();
        var primary_route = $('#PageBody_routeList').val();            
        if (name == '') {
            valid = false;
            detailsView.showError('PageBody_name', "The name field is required.");
        }
        if (primary_route == '') {
            valid = false;
            detailsView.showError('PageBody_routeList', "The Primary route field is required.");
        }
        if (valid) {
            showButtonWorking("save-btn");
            var o = new Object();
            o.foc_id = foc_id;
            o.name = name;
            o.route_num = primary_route;
            o.disruption_id = disruption_id;
            o.current_primary_route_no = current_primary_route_no;
            o.has_foc = has_foc;
            var x = JSON.stringify(o);
            log(x);
            $.ajax({
                url: '../Disruptions/Details.aspx/Save',
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json;charset=utf-8;',
                data: x,
                success: function (data) {
                    log(data);
                    if (data.d.valid != "true") {
                        $('#generic-error-output').html(data.d.message);
                        $('#generic-error-output').show();
                        hideButtonWorking("save-btn");
                    }
                    else {                            
                        disruption_id = data.d.disruption_id;
                        current_foc_id = foc_id;
                        current_primary_route_no = primary_route;
                        log(disruption_id);
                        window.location.href = "../Disruptions/Stops.aspx?e=" + disruption_id;
                    }                                               
                },
                error: function (a, b, c) {
                    alert(b);
                    hideButtonWorking("save-btn");
                }
            });
        }
    }
}

$(document).ready(function () {
    detailsView.init();
});


