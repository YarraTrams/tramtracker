﻿var messagesListView = {

    disruptionId: getQueryStringVariable('e'),
    lastClickedCheckbox: null, //holds the most recently clicked hide stops direction checkbox

    init: function () {
        this.bindUIElements();
    }, //end init

    bindUIElements: function () {
        //bind the event for the predefined message dropdown
        $(document.body).on("change", ".message-dropdown", function (e) {
            var other_field = $('#' + $(e.target).attr("for"));
            other_field.val($(e.target).val());

            var long_message_field = $('#' + $(e.target).data("long-message"));
            var is_modal = $(e.target).data("modal-input");
            if (is_modal) {//may need to handle this differently, need to confirm that it ok to overwrite
                long_message_field.val($(e.target).val());
            }
            else {
                long_message_field.val($(e.target).val());
            }


        });

        //this function handles a click on the update button
        $(document.body).on("click", ".message-save-btn", function (e) {
            var base_id = $(e.target).attr("for");

            var textarea = $("#" + base_id);
            var message_type = textarea.attr("message-type");
            var json_field = textarea.attr("json-field");
            var message_text = textarea.val();
            if (message_text != "") {
                textarea.parent(".control-group").removeClass("error");
                var override = $("#" + base_id + "-override").is(":checked");
                var row_selector = "tbody tr";

                var rows = $('#disrupted-table-' + selected_disruption_id).find(row_selector);
                for (var i = 0; i < rows.length; i++) {
                    var tr = $(rows[i]);
                    var message = disruption_messages["disruption_" + selected_disruption_id][tr.attr("json-field")];
                    tr.tooltip("destroy");
                    if (override) {
                        if ((json_field == "affected_short_message" || json_field == "affected_long_message") && message.unaffected == true) {
                            //don't set for unaffected route combos
                        }
                        else {
                            message[json_field] = message_text;
                            tr.find("." + message_type + "-status-column").html("Message Set");
                            messagesListView.validateRow(message);
                        }
                    }
                    else if (!message[json_field + "_already_set"]) {

                        if ((json_field == "affected_short_message" || json_field == "affected_long_message") && message.unaffected == true) {
                            //don't set for unaffected route combos
                        }
                        else {
                            message[json_field] = message_text;
                            tr.find("." + message_type + "-status-column").html("Message Set");
                            messagesListView.validateRow(message);
                        }
                    }
                    else {
                        tr.attr("title", "This row was not updated because a message already exists for the route, click this row or use the override checkbox to set the message for this route.");
                        tr.tooltip({});
                        tr.removeClass("error").removeClass("success").addClass("warning");
                    }
                }
            }
            else {
                textarea.parent(".control-group").addClass("error");
            }
        });

        //event handler for row clicks in the messages table
        $(document.body).on("click", ".disrupted-table tbody tr", function (e) {
            //this is to cancel the event if the checkbox is clicked
            if (e.target.type == "checkbox" || $(e.target)[0].tagName == "INPUT" || $(e.target)[0].tagName == "LABEL") {
                e.stopPropagation();
                return;
            }
            var row_id = $(e.target).parent("tr").attr("id");
            var message = disruption_messages["disruption_" + selected_disruption_id][$(e.target).parent("tr").attr("json-field")];
            log(message);
            $("#messages-modal .message-dropdown").val("");
            $("#messages-modal").modal({});
            var route = message.route_name;

            $("#messages-modal .modal-header h3").html("Route " + route + " messages");
            $("#modal-affected-short-message").val(message.affected_short_message);
            $("#modal-unaffected-short-message").val(message.unaffected_short_message);
            $("#modal-affected-long-message").val(message.affected_long_message);
            $("#modal-unaffected-long-message").val(message.unaffected_long_message);

            if (message.unaffected == true) {
                $("#modal-affected-short-message").attr("disabled", "true");
                $("#modal-affected-long-message").attr("disabled", "true");
                $("#messages-modal .affected-message-dropdown").attr("disabled", "true");
                $("#modal-affected-pid-preview").hide();
            }
            else {
                $("#modal-affected-short-message").removeAttr("disabled");
                $("#modal-affected-long-message").removeAttr("disabled");
                $("#messages-modal .affected-message-dropdown").removeAttr("disabled");
                $("#modal-affected-pid-preview").removeAttr("disabled");
                $("#modal-affected-pid-preview").show();
            }

            //decide to show the long messages by default
            if ((message.affected_long_message && message.affected_long_message != message.affected_short_message) || message.unaffected_long_message && message.unaffected_long_message != message.unaffected_short_message) {
                $("#modal-long-message-checkbox").attr("checked", "true");
                $("#modal-long-messages").slideDown();
            }
            else {
                $("#modal-long-message-checkbox").removeAttr("checked", "true");
                $("#modal-long-messages").slideUp();
            }

            selected_row_id = row_id;
            selected_message = message;
        });

        $(document.body).on("click", ".directions_checkbox", function (e) {
            /*var row_id = $(e.target).parents("tr").attr("id");
            var json_field = $(e.target).attr("json-field");            
            var message = disruption_messages["disruption_" + selected_disruption_id][$(e.target).parents("tr").attr("json-field")];            
            message[json_field] = $(e.target).is(':checked');            
            messagesListView.validateRow(message);*/

            if (!messagesListView.lastClickedCheckbox && e.shiftKey) {

                messagesListView.lastClickedCheckbox = this;
                return;
            }

            if (e.shiftKey) {

                var start = $('.directions_checkbox').index(this);
                var end = $('.directions_checkbox').index(messagesListView.lastClickedCheckbox);
                var t1 = new Date();
                for (m = Math.min(start, end); m <= Math.max(start, end); m++) {
                    var index = m;
                    var cbox = $('.directions_checkbox')[index];
                    if ($(cbox).is(":visible")) {
                        cbox.checked = messagesListView.lastClickedCheckbox.checked;
                        var row_id = $(cbox).parents("tr").attr("id");
                        var json_field = $(cbox).attr("json-field");
                        var message = disruption_messages["disruption_" + selected_disruption_id][$(cbox).parents("tr").attr("json-field")];
                        message[json_field] = $(cbox).is(':checked');
                        messagesListView.validateRow(message);
                    }
                }
            }
            else {
                var row_id = $(e.target).parents("tr").attr("id");
                var json_field = $(e.target).attr("json-field");
                var message = disruption_messages["disruption_" + selected_disruption_id][$(e.target).parents("tr").attr("json-field")];
                message[json_field] = $(e.target).is(':checked');
                messagesListView.validateRow(message);
            }
            messagesListView.lastClickedCheckbox = this;
        });

        $(document.body).on("click", "#modal-save", function (e) {
            var tr = $("#" + selected_row_id);
            //var message = disruption_messages["disruption_" + selected_disruption_id][selected_route];

            var short_message_affected = $("#modal-affected-short-message").val();
            if (short_message_affected != "") {
                selected_message.affected_short_message = short_message_affected;

                tr.find(".affected-short-message-status-column").html("Message Set");
            }

            var short_message_unaffected = $("#modal-unaffected-short-message").val();
            if (short_message_unaffected != "") {
                selected_message.unaffected_short_message = short_message_unaffected;
                tr.find(".unaffected-short-message-status-column").html("Message Set");
            }

            var long_message_affected = $("#modal-affected-long-message").val();
            if (long_message_affected != "") {
                selected_message.affected_long_message = long_message_affected;
                tr.find(".affected-short-message-status-column").html("Message Set");
            }

            var long_message_unaffected = $("#modal-unaffected-long-message").val();
            if (long_message_unaffected != "") {
                selected_message.unaffected_long_message = long_message_unaffected;
                tr.find(".unaffected-short-message-status-column").html("Message Set");
            }

            messagesListView.validateRow(selected_message);

            $('#messages-modal').modal("hide");
        });

        $(document.body).on("click", ".show-long-messages", function (e) {
            var long_message_div = $(e.target).data("for");
            if ($(e.target).is(':checked')) {
                $("#" + long_message_div).slideDown();
            }
            else {
                $("#" + long_message_div).slideUp();
            }
        });

        $(document.body).on("click", ".pid-link", function (e) {
            showModal($("#previewPIDDiv"));
            //$("#previewPIDDiv").modal({});
            var for_field = $(e.target).data("for");
            $("#pid-preview-text").val($("#" + for_field).val());
            pids_field_id = for_field;
        });

        $(document.body).on("click", "#pid-save", function (e) {
            $("#previewPIDDiv").modal("hide");
            $("#" + pids_field_id).val($("#pid-preview-text").val());
        });

        $(document.body).on("blur", ".short-message", function (e) {
            var text_input = $(this);

            var long_messages_cbox = $("#" + text_input.data("long-message-checkbox"));
            var long_messages_selected = long_messages_cbox.is(":checked");
            var long_message = $("#" + text_input.data("long-message"));
            if (long_message.val() === "" || !long_messages_selected) {
                long_message.val(text_input.val());
            }
        });

        $(document.body).on("click", ".show-long-messages", function (e) {
            var cbox = $(this);
            var checked = cbox.is(":checked");
            var disruption_id = cbox.data("disruption-id");
            if (!checked) {
                var aff_message_input = null;
                var unaff_message_input = null;

                var aff_long_message_input = null;
                var unaff_long_message_input = null;

                if (disruption_id) {
                    aff_message_input = $("#affected-short-message-" + disruption_id);
                    unaff_message_input = $("#unaffected-short-message-" + disruption_id);

                    aff_long_message_input = $("#affected-long-message-" + disruption_id);
                    unaff_long_message_input = $("#unaffected-long-message-" + disruption_id);
                }
                else {
                    aff_message_input = $("#modal-affected-short-message");
                    unaff_message_input = $("#modal-unaffected-short-message");

                    aff_long_message_input = $("#modal-affected-long-message");
                    unaff_long_message_input = $("#modal-unaffected-long-message");
                }
                if (!aff_long_message_input.is(":disabled")) {
                    aff_long_message_input.val(aff_message_input.val());
                }
                unaff_long_message_input.val(unaff_message_input.val());
            }
        });

        this.bindTextAreaCharCount();
    },

    //this function validates the message data and if it is valid saves it to the server        
    validateRow: function (message) {
        var tr = $("#" + message.type + "_" + selected_disruption_id + "_" + message.id);
        tr.tooltip("destroy");
        if ((message.affected_short_message !== null && message.unaffected_short_message !== null) || message.unaffected == true && message.unaffected_short_message !== null) {

            if (message.affected_long_message === null && message.unaffected != true) {
                message.affected_long_message = message.affected_short_message;
            }
            if (message.unaffected_long_message === null) {
                message.unaffected_long_message = message.unaffected_short_message;
            }
            messagesListView.saveMessage(message, function (result) {
                tr.removeClass("error").removeClass("warning").addClass("success");
                tr.attr("title", "Messages saved");
                tr.tooltip({});
                if (message.unaffected != true) {
                    tr.find(".affected-short-message-status-column").html("Message Saved");
                }
                tr.find(".unaffected-short-message-status-column").html("Message Saved");

                //all the other pages are invalidated now so remove them from the page so they reload 
                //when the tab is clicked
                $(".messages-form-holder:not(#disruption_form_" + selected_disruption_id + ")").remove();
            },
            function (error) {
                log(error);
                tr.removeClass("success").removeClass("warning").addClass("error");
                tr.find(".affected-short-message-status-column").html("Save Error");
                tr.find(".unaffected-short-message-status-column").html("Save Error");
                tr.attr("title", error.message);
                tr.tooltip("show");
            });
        }
        else {
            tr.removeClass("error").removeClass("success").addClass("warning");
            tr.attr("title", "Both affected and unaffected messages must be set to save this disruption");
            tr.tooltip({});
        }
    },

    //This function laods the disruption when its tab is clicked.  It lazy loads tabs when they are clicked
    loadDisruption: function (id) {
        if ($('#disruption_form_' + id).length) {
            $('#disruption_form_' + selected_disruption_id).hide();
            $('#disruption_form_' + id).show();
            selected_disruption_id = id;
        }
        else {
            var noCache = new Date().getTime();
            $('#disruption-messages').mask("Loading...");
            $.ajax({
                type: "GET",
                url: "/Disruptions/DisruptionMessagesForm.aspx",
                data: { e: id, noCache: noCache },
                contentType: "application/json; charset=utf-8",
                dataType: "html",
                success: function (response) {
                    $('#disruption_form_' + selected_disruption_id).hide();
                    selected_disruption_id = id;
                    $('#disruption-messages').append(response);
                    //$('#disruption-messages').html(response);
                    $('#disruption-messages').unmask();
                    messagesListView.bindTextAreaCharCount();
                    messagesListView.bindPredefinedMessage();
                },
                failure: function (msg) {
                    log(msg);
                    $('#disruption-messages').unmask();
                }
            });
        }
    },

    bindTextAreaCharCount: function () {

        $(".short-message").each(function () {
            var el = $(this);
            var parent = el.parents(".message-input-holder");
            var cnt = parent.find(".count");
            $(this).charCounter(117, {
                container: "#" + cnt.attr("id")
            });
        });

        $(".long-message").each(function () {
            var el = $(this);
            var parent = el.parents(".message-input-holder");
            var cnt = parent.find(".long-count");
            $(this).charCounter(512, {
                container: "#" + cnt.attr("id")
            });
        });
    }, // end bindTextAreaCharCount*/

    validateDisruption: function (disruption_id) {
        var o = new Object();
        o.disruption_id = disruption_id;
        var x = JSON.stringify(o);
        showButtonWorking('next-btn');
        $.ajax({
            url: '../Disruptions/Messages.aspx/ValidateDisruptionMessages',
            type: 'POST',
            dataType: 'JSON',
            contentType: 'application/json;charset=utf-8;',
            data: x,
            success: function (data) {
                log(data);
                if (data.d.length > 0) {
                    $("#validation-message").html("You have not saved messages for all the disrupted routes, please review the messages table and add the missing messages");
                    $("#validation-message").show();
                    for (var i = 0; i < data.d.length; i++) {
                        var result = data.d[i];
                        $("#" + result.Type + "_" + disruption_id + "_" + result.Id).addClass("error");
                    }
                    hideButtonWorking('next-btn');
                }
                else {
                    $("#validation-message").hide();
                    window.location.href = "Listing.aspx";
                }
            },
            error: function (a, b, c) {
                alert(b);
                hideButtonWorking('next-btn');
            }
        });
    },

    saveMessage: function (message, success, error) {
        var url = "";
        if (message.type == "route") {
            var data = {
                "id": message.id,
                "aff_sml": message.affected_short_message,
                "aff_lrg": message.affected_long_message,
                "aff_additionalInfo": "false",
                "unaff_sml": message.unaffected_short_message,
                "unaff_lrg": message.unaffected_long_message,
                "unaff_additionalInfo": "false",
                "cuser": ttmcurrentUser,
                "disruption_id": message.disruption_id,
                "hide_up_stop_messages": message.hide_up_stop_messages,
                "hide_down_stop_messages": message.hide_down_stop_messages
            };
            url = "../Handlers/Disruptions/UpdateAffectedRouteMessage.ashx";
        }
        else if (message.type == "combo") {
            var data = {
                "id": message.id,
                "aff_sml": message.affected_short_message,
                "aff_lrg": message.affected_long_message,
                "aff_additionalInfo": "false",
                "aff_displayType": 1,
                "unaff_sml": message.unaffected_short_message,
                "unaff_lrg": message.unaffected_long_message,
                "unaff_additionalInfo": "false",
                "unaff_displayType": 1,
                "cuser": ttmcurrentUser,
                "disruption_id": message.disruption_id,
                "hide_up_stop_messages": message.hide_up_stop_messages,
                "hide_down_stop_messages": message.hide_down_stop_messages,
                "unaffected": message.unaffected
            };
            url = "../Handlers/Disruptions/UpdateRouteCombinationMessage.ashx";
        }
        if (url) {
            $.ajax({
                url: url,
                dataType: "json",
                method: "POST",
                data: escape(JSON.stringify(data)),
                error: function (e) {
                    log(e);
                    error(e.responseJSON);
                },
                success: function (d) {
                    if (success) {
                        success(d);
                    }
                },
                async: true
            });
        }

    },

    bindPredefinedMessage: function () {
        var selects = $(".affected-message-dropdown:not(:has(option))");
        selects.append('<option value=""></option>');
        for (var i = 0; i < predefined_affected_messages.length; i++) {
            var msg = predefined_affected_messages[i];
            selects.append('<option value="' + msg.Value + '">' + msg.Key + '</option>');
        }

        var selects2 = $(".unaffected-message-dropdown:not(:has(option))");
        selects2.append('<option value=""></option>');
        for (var i = 0; i < predefined_unaffected_messages.length; i++) {
            var msg = predefined_unaffected_messages[i];
            selects2.append('<option value="' + msg.Value + '">' + msg.Key + '</option>');
        }
    }
}

$(document).ready(function () {
    messagesListView.init();
});//end document ready
