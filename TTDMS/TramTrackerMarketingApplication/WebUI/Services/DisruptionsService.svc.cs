﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using TTMarketing.BLL.Controllers.Disruptions;
using TTMarketing.BLL.Model;
using TTMarketing.BLL.Model.Disruptions;

namespace TTMarketing.WebUI.Services
{
    [ServiceContract(Namespace = "")]

    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class DisruptionsService
    {

        /*
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetDisruptions/?sd={sd}&ed={ed}", BodyStyle = WebMessageBodyStyle.Bare)]
        [ServiceKnownType(typeof(Disruption))]
        [ServiceKnownType(typeof(List<Disruption>))]
        public ResponseHelper GeDisruptions(string sd, string ed)
        {
            ResponseHelper r = new ResponseHelper();
            IList<Disruption> list = new List<Disruption>();
            DateTime startTime = DateTime.Parse(sd);
            DateTime endTime = DateTime.Parse(ed);

            list = DisruptionController.GetDisruptions(startTime, endTime);
            r.responseObject = list;
            return r;
        }*/



        [OperationContract]
        [OperationBehavior(Impersonation = ImpersonationOption.Allowed)]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetTest", BodyStyle = WebMessageBodyStyle.Bare)]
        public ResponseHelper GetTest()
        {
            ResponseHelper r = new ResponseHelper();
            string currentUser = "NullWindowsIndentity";
            try { currentUser = OperationContext.Current.ServiceSecurityContext.WindowsIdentity.Name; }catch { Exception cuex; }
            
            r.responseString = currentUser;
            return r;
        }


        [OperationContract]
        [OperationBehavior(Impersonation = ImpersonationOption.Allowed)]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetDisruptionById/?e={e}", BodyStyle = WebMessageBodyStyle.Bare)]
        [ServiceKnownType(typeof(Disruption))]
        public ResponseHelper GetDisruptionById(string e)
        {
            ResponseHelper r = new ResponseHelper();
            r.responseObject = DisruptionController.GetDisruptionById(Int32.Parse(e));
            return r;
        }


        [OperationContract]
        [OperationBehavior(Impersonation = ImpersonationOption.Allowed)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetAllRoutes", BodyStyle = WebMessageBodyStyle.Bare)]
        //[AspNetCacheProfile("SpecialEventsMaxCacheProfile")]
        [ServiceKnownType(typeof(DisruptionRoute))]
        [ServiceKnownType(typeof(List<DisruptionRoute>))]
        public ResponseHelper GetAllRoutes()
        {
            ResponseHelper r = new ResponseHelper();
            try
            {
                r.responseObject = DisruptionRoutesController.GetAllRoutes();
                return r;
            }
            catch (Exception ex)
            {
                r.isError = true;
                string innerMessage = ex.InnerException != null ? " - " + ex.InnerException.Message : "";
                r.responseString = ex.Message + innerMessage + " - An Email is also sent to application's exception recievers";
                return r;
            }
        }

        [OperationContract]
        [OperationBehavior(Impersonation = ImpersonationOption.Allowed)]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetStopsForDisruptionRoute/?e={e}&r={r}", BodyStyle = WebMessageBodyStyle.Bare)]
        [ServiceKnownType(typeof(DisruptionRoute))]
        [ServiceKnownType(typeof(DisruptionStop))]
        [ServiceKnownType(typeof(List<DisruptionStop>))]
        public ResponseHelper GetStopsForDisruptionRoute(string e, string r)
        {
            ResponseHelper rh = new ResponseHelper();
            int eventId = Int32.Parse(e);
            short routeNo = (short)Convert.ToInt16(r);
            rh.responseObject = DisruptionRoutesController.GetStopsForDisruptionRoute(eventId, routeNo);
            return rh;
        }


        [OperationContract]
        [OperationBehavior(Impersonation = ImpersonationOption.Allowed)]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveAffectedStop/?r={r}&e={e}&s={s}&on={on}&d={d}&cu={cu}&disp={disp}", BodyStyle = WebMessageBodyStyle.Bare)]
        [ServiceKnownType(typeof(DisruptionStop))]
        public ResponseHelper SaveAffectedStop(string r, string e, string s, string on, string d, string cu, string disp)
        {
            ResponseHelper rh = new ResponseHelper();
            try
            {
                int disruptionID = Convert.ToInt32(e);
                short routeNo = Convert.ToInt16(r);
                short stopNo = Convert.ToInt16(s);
                bool isUpstop = (d == "true");
                bool isOn = (on == "true");
                bool displayPredictions = (disp == "true");

                string currentUser = "NullWindowsIndentity";
                try { currentUser = OperationContext.Current.ServiceSecurityContext.WindowsIdentity.Name; }
                catch { Exception cuex; }

                DisruptionStop st = new DisruptionStop();
                if (isOn)
                {
                    st = DisruptionStopsController.AddDisruptionEffectedRouteStop(disruptionID, routeNo, stopNo, isUpstop, false, currentUser, displayPredictions);
                    DisruptionController.SetValidStatus(disruptionID, false);
                }
                else
                {
                    st = DisruptionStopsController.DeleteDisruptionEffectedRouteStops(disruptionID, routeNo, stopNo, isUpstop, currentUser);
                    DisruptionController.SetValidStatus(disruptionID, false);
                }


                if (st == null)
                {
                    rh.isError = true;
                    rh.responseString = "Unable to save effected stop";
                }
                else
                    rh.responseObject = st;
                return rh;
            }
            catch (Exception ex)
            {
                rh.isError = true;
                string innerMessage = ex.InnerException != null ? " - " + ex.InnerException.Message : "";
                rh.responseString = ex.Message + innerMessage + " - An Email is also sent to application's exception recievers";
                return rh;
            }
        }

        [OperationContract]
        [OperationBehavior(Impersonation = ImpersonationOption.Allowed)]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetMissingRoutesForAffectedDisruptionStop/?e={e}&r={r}&s={s}", BodyStyle = WebMessageBodyStyle.Bare)]
        [ServiceKnownType(typeof(List<int>))]
        public ResponseHelper GetMissingRoutesForAffectedDisruptionStop(string e, string r,string s)
        {
            ResponseHelper rh = new ResponseHelper();
            try
            {
                int disruptionId = Convert.ToInt32(e);
                short stopNo = Convert.ToInt16(s);
                int routeNo = Int32.Parse(r);
                rh.responseObject = DisruptionRoutesController.GetMissingRoutesForAffectedDisruptionStop(disruptionId, routeNo, (short)stopNo);
                return rh;
            }
            catch (Exception ex)
            {
                rh.isError = true;
                string innerMessage = ex.InnerException != null ? " - " + ex.InnerException.Message : "";
                rh.responseString = ex.Message + innerMessage + " - An Email is also sent to application's exception recievers";
                return rh;
            }
        }


        [OperationContract]
        [OperationBehavior(Impersonation = ImpersonationOption.Allowed)]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetDisruptionRoutesDetails/?e={e}&u={u}", BodyStyle = WebMessageBodyStyle.Bare)]
        [ServiceKnownType(typeof(RoutesSummary))]
        [ServiceKnownType(typeof(List<RoutesSummary>))]
        public ResponseHelper GetDisruptionRoutesDetails(string e, string u)
        {
            ResponseHelper r = new ResponseHelper();
            try
            {
                int disruptionId = Int32.Parse(e);
                bool isUp = (string.IsNullOrEmpty(u)) ? false : bool.Parse(u);
                IList<RoutesSummary> items = DisruptionRoutesController.GetDisruptionRoutesDetails(disruptionId, isUp);
                r.responseObject = items;
                return r;
            }
            catch (Exception ex)
            {
                r.isError = true;
                string innerMessage = ex.InnerException != null ? " - " + ex.InnerException.Message : "";
                r.responseString = ex.Message + innerMessage + " - An Email is also sent to application's exception recievers";
                return r;
            }
        }


        [OperationContract]
        [OperationBehavior(Impersonation = ImpersonationOption.Allowed)]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "AddAllDisruptionEffectedRouteStops/?r={r}&e={e}&d={d}&cu={cu}", BodyStyle = WebMessageBodyStyle.Bare)]
        public ResponseHelper AddAllDisruptionEffectedRouteStops(string r, string e, string d, string cu)
        {
            ResponseHelper rh = new ResponseHelper();
            try
            {
                int disruptionId = Convert.ToInt32(e);
                short routeNo = Convert.ToInt16(r);
                bool isUpstop = (d == "true");
                string currentUser = "NullWindowsIndentity";
                try { currentUser = OperationContext.Current.ServiceSecurityContext.WindowsIdentity.Name; }
                catch { Exception cuex; }

                DisruptionStopsController.AddAllDisruptionEffectedRouteStops(disruptionId, routeNo, isUpstop, currentUser, false);


                return rh;
            }
            catch (Exception ex)
            {
                rh.isError = true;
                string innerMessage = ex.InnerException != null ? " - " + ex.InnerException.Message : "";
                rh.responseString = ex.Message + innerMessage + " - An Email is also sent to application's exception recievers";
                return rh;
            }
        }


        [OperationContract]
        [OperationBehavior(Impersonation = ImpersonationOption.Allowed)]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "DeleteAllDisruptionEffectedRouteStops/?r={r}&e={e}&d={d}&cu={cu}", BodyStyle = WebMessageBodyStyle.Bare)]
        public ResponseHelper DeleteAllDisruptionEffectedRouteStops(string r, string e, string d, string cu)
        {
            ResponseHelper rh = new ResponseHelper();
            try
            {
                int disruptionId = Convert.ToInt32(e);
                short routeNo = Convert.ToInt16(r);
                bool isUpstop = (d == "true");
                string currentUser = "NullWindowsIndentity";
                try { currentUser = OperationContext.Current.ServiceSecurityContext.WindowsIdentity.Name; }
                catch { Exception cuex; }

                DisruptionStopsController.DeleteAllDisruptionEffectedRouteStops(disruptionId, routeNo, isUpstop, currentUser);
                return rh;
            }
            catch (Exception ex)
            {
                rh.isError = true;
                string innerMessage = ex.InnerException != null ? " - " + ex.InnerException.Message : "";
                rh.responseString = ex.Message + innerMessage + " - An Email is also sent to application's exception recievers";
                return rh;
            }
        }


        [OperationContract]
        [OperationBehavior(Impersonation = ImpersonationOption.Allowed)]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "AddDisruptionEffectedRoutes/?e={e}&cu={cu}", BodyStyle = WebMessageBodyStyle.Bare)]
        public ResponseHelper AddDisruptionEffectedRoutes(string e,string cu)
        {
            ResponseHelper rh = new ResponseHelper();
            try
            {
                int disruptionId = Convert.ToInt32(e);
                DisruptionRoutesController.AddDisruptionEffectedRoutes(disruptionId, cu);
                return rh;
            }
            catch(Exception ex)
            {
                rh.isError = true;
                string innerMessage = ex.InnerException != null ? " - " + ex.InnerException.Message : ""; 
                rh.responseString= ex.Message + innerMessage + " - An Email is also sent to application's exception recievers";
                return rh;
            }
        }


        [OperationContract]
        [OperationBehavior(Impersonation = ImpersonationOption.Allowed)]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetStopsForDisruptedRoute/?r={r}", BodyStyle = WebMessageBodyStyle.Bare)]
        [ServiceKnownType(typeof(DisruptionRoute))]
        [ServiceKnownType(typeof(DisruptionStop))]
        [ServiceKnownType(typeof(List<DisruptionStop>))]
        public ResponseHelper GetStopsForDisruptedRoute(string r)
        {
            ResponseHelper rh = new ResponseHelper();
            try
            {
                int routeNo = Int32.Parse(r);
                rh.responseObject = DisruptionRoutesController.GetStopsForDisruptedRoute(routeNo);
                return rh;
            }
            catch (Exception ex)
            {
                rh.isError = true;
                string innerMessage = ex.InnerException != null ? " - " + ex.InnerException.Message : "";
                rh.responseString = ex.Message + innerMessage + " - An Email is also sent to application's exception recievers";
                return rh;
            }
        }


        [OperationContract]
        [OperationBehavior(Impersonation = ImpersonationOption.Allowed)]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetDisruptionMessagesForStop/?s={s}&tta={tta}", BodyStyle = WebMessageBodyStyle.Bare)]
        [ServiceKnownType(typeof(RouteSummaryMessage))]
        [ServiceKnownType(typeof(List<RouteSummaryMessage>))]
        public ResponseHelper GetDisruptionMessagesForStop(string s, string tta)
        {
            ResponseHelper rh = new ResponseHelper();
            try
            {
                short stopNo = Int16.Parse(s);
                bool tramTrackerAvailable = (tta == "true");
                rh.responseObject = DisruptionMessagesController.GetDisruptionMessagesForStop(stopNo, tramTrackerAvailable);
                return rh;
            }
            catch (Exception ex)
            {
                rh.isError = true;
                string innerMessage = ex.InnerException != null ? " - " + ex.InnerException.Message : "";
                rh.responseString = ex.Message + innerMessage + " - An Email is also sent to application's exception recievers";
                return rh;
            }
        }


        [OperationContract]
        [OperationBehavior(Impersonation = ImpersonationOption.Allowed)]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "PublishAllDisruptions/?cu={cu}", BodyStyle = WebMessageBodyStyle.Bare)]
        public ResponseHelper PublishAllDisruptions(string cu)
        {
            ResponseHelper rh = new ResponseHelper();
            try
            {
                string currentUser = "NullWindowsIndentity";
                try { currentUser = OperationContext.Current.ServiceSecurityContext.WindowsIdentity.Name; }
                catch { Exception cuex; }
                DisruptionController.PublishAllDisruptions(currentUser);
                return rh;
            }
            catch (Exception ex)
            {
                rh.isError = true;
                string innerMessage = ex.InnerException != null ? " - " + ex.InnerException.Message : "";
                rh.responseString = ex.Message + innerMessage + " - An Email is also sent to application's exception recievers";
                return rh;
            }

        }


        [OperationContract]
        [OperationBehavior(Impersonation = ImpersonationOption.Allowed)]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "ClearDisruption/?e={e}&cu={cu}&ttl={ttl_str}", BodyStyle = WebMessageBodyStyle.Bare)]
        public ResponseHelper ClearDisruption(string e, string cu, string ttl_str)
        {
            ResponseHelper rh = new ResponseHelper();
            try
            {
                int disruptionId = Convert.ToInt32(e);
                int ttl = Convert.ToInt32(ttl_str);
                string currentUser = "NullWindowsIndentity";
                try { currentUser = OperationContext.Current.ServiceSecurityContext.WindowsIdentity.Name; }
                catch { Exception cuex; }

                rh.responseString = DisruptionController.ClearDisruption(disruptionId, currentUser, ttl);
                return rh;
            }
            catch (Exception ex)
            {
                rh.isError = true;
                string innerMessage = ex.InnerException != null ? " - " + ex.InnerException.Message : "";
                rh.responseString = ex.Message + innerMessage + " - An Email is also sent to application's exception recievers";
                return rh;
            }

        }

        [OperationContract]
        [OperationBehavior(Impersonation = ImpersonationOption.Allowed)]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetAffectedStopsByDisruptionId/?d={d}", BodyStyle = WebMessageBodyStyle.Bare)]
        [ServiceKnownType(typeof(DisruptionStop))]
        [ServiceKnownType(typeof(List<DisruptionStop>))]
        public ResponseHelper GetAffectedStopsByDisruptionId(string d)
        {
            ResponseHelper rh = new ResponseHelper();
            try
            {
                int disruptionId = Int32.Parse(d);
                rh.responseObject = DisruptionStopsController.GetAffectedStopsByDisruptionId(disruptionId);
                return rh;
            }
            catch (Exception ex)
            {
                rh.isError = true;
                string innerMessage = ex.InnerException != null ? " - " + ex.InnerException.Message : "";
                rh.responseString = ex.Message + innerMessage + " - An Email is also sent to application's exception recievers";
                return rh;
            }
        }


        [OperationContract]
        [OperationBehavior(Impersonation = ImpersonationOption.Allowed)]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetFOCDisruptionById/?e={e}", BodyStyle = WebMessageBodyStyle.Bare)]
        [ServiceKnownType(typeof(FOCDisruption))]
        public ResponseHelper GetFOCDisruptionById(string e)
        {
            ResponseHelper r = new ResponseHelper();
            r.responseObject = DisruptionController.GetFOCDisruptionById(Int32.Parse(e));
            return r;
        }

        [OperationContract]
        [OperationBehavior(Impersonation = ImpersonationOption.Allowed)]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetDisruptionPrimaryRouteMessages/?disruption_id={e}", BodyStyle = WebMessageBodyStyle.Bare)]
        [ServiceKnownType(typeof(RouteMessage))]
        public ResponseHelper GetDisruptionPrimaryRouteMessages(string e)
        {
            int disruptionId = Int32.Parse(e);
            ResponseHelper r = new ResponseHelper();
            r.responseObject = DisruptionController.GetPrimaryRouteMessages(disruptionId);
            return r;
        }

        /*[OperationContract]
        [OperationBehavior(Impersonation = ImpersonationOption.Allowed)]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetEmailNotificationInfo/?disruption_id={e}", BodyStyle = WebMessageBodyStyle.Bare)]
        [ServiceKnownType(typeof(object))]
        public ResponseHelper GetEmailNotificationInfo(string e)
        {
            Dictionary<string, object> results = new Dictionary<string,object>();
            int disruptionId = Int32.Parse(e);
            RouteMessage message = DisruptionController.GetPrimaryRouteMessages(disruptionId);
            results.Add("body", message.AffectedLongMessage);
            results.Add("subject", ConfigurationManager.AppSettings["DefaultNotificationSubject"]);
            results.Add("to", ConfigurationManager.AppSettings["DisruptionNotificationRecipients"]);
            ResponseHelper r = new ResponseHelper();
            r.responseObject = new { body = message.AffectedLongMessage };
            return r;

        }*/

    }
}
