using System.Configuration;

namespace TTMarketing.DAL
{
    public partial class DisruptionsDataContext : System.Data.Linq.DataContext
    {

        public DisruptionsDataContext()
            : base(ConfigurationManager.ConnectionStrings["DisruptionsConnectionString"].ToString())
        {
            OnCreated();
        }


    }
}
