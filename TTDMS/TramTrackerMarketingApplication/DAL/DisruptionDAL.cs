﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace TTMarketing.DAL
{
    public class DisruptionDAL
    {
        public string ConnectionString { get; set; }


        public void ReplicateUnclearedDisruptions(DataTable dtUnclearedDisruptions)
        {
            using (SqlConnection connection =
                   new SqlConnection(ConnectionString))
            {
                connection.Open();

                using (SqlCommand sqlCmd = new SqlCommand("TRUNCATE TABLE ttdms_Disruptions", connection))
                {
                    sqlCmd.ExecuteNonQuery();
                }


                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                {
                    bulkCopy.DestinationTableName =
                        "dbo.ttdms_Disruptions";

                    bulkCopy.WriteToServer(dtUnclearedDisruptions);
                }
            }
        }

        public void ReplicateDisruptedCombinationRouteMessages(DataTable dtCombMessages)
        {
            using (SqlConnection connection =
                  new SqlConnection(ConnectionString))
            {
                using (SqlCommand sqlCmd = new SqlCommand("TRUNCATE TABLE ttdms_DisruptedCombinationRouteMessages", connection))
                {
                    sqlCmd.ExecuteNonQuery();
                }


                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                {
                    bulkCopy.DestinationTableName =
                        "dbo.ttdms_DisruptedCombinationRouteMessages";

                    bulkCopy.WriteToServer(dtCombMessages);
                }
            }
        }

        public void ReplicateDisruptedRouteMessages(DataTable dtRouteMessages)
        {
            using (SqlConnection connection =
                  new SqlConnection(ConnectionString))
            {
                using (SqlCommand sqlCmd = new SqlCommand("TRUNCATE TABLE ttdms_DisruptedRouteMessages", connection))
                {
                    sqlCmd.ExecuteNonQuery();
                }


                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                {
                    bulkCopy.DestinationTableName =
                        "dbo.ttdms_DisruptedRouteMessages";

                    bulkCopy.WriteToServer(dtRouteMessages);
                }
            }
        }


        public void ReplicateDisruptedStops(DataTable dtDisruptedStops)
        {
            using (SqlConnection connection =
                  new SqlConnection(ConnectionString))
            {
                using (SqlCommand sqlCmd = new SqlCommand("TRUNCATE TABLE ttdms_DisruptedStops", connection))
                {
                    sqlCmd.ExecuteNonQuery();
                }


                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                {
                    bulkCopy.DestinationTableName =
                        "dbo.ttdms_DisruptedStops";

                    bulkCopy.WriteToServer(dtDisruptedStops);
                }
            }
        }

        public void ReplicateDisruptedRoutes(DataTable dtDisruptedRoutes)
        {
            using (SqlConnection connection =
                  new SqlConnection(ConnectionString))
            {
                using (SqlCommand sqlCmd = new SqlCommand("TRUNCATE TABLE ttdms_DisruptedRoutes", connection))
                {
                    sqlCmd.ExecuteNonQuery();
                }


                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                {
                    bulkCopy.DestinationTableName =
                        "dbo.ttdms_DisruptedRoutes";

                    bulkCopy.WriteToServer(dtDisruptedRoutes);
                }
            }
        }

        public void ReplicateActiveDisruptedCombinationRouteMessages(DataTable dtActiveDisruptedCombinationRouteMessages)
        {
            using (SqlConnection connection =
                   new SqlConnection(ConnectionString))
            {
                connection.Open();

                using (SqlCommand sqlCmd = new SqlCommand("TRUNCATE TABLE ActiveDisruptedCombinationRouteMessages", connection))
                {
                    sqlCmd.ExecuteNonQuery();
                }
                

                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                {
                    bulkCopy.DestinationTableName =
                        "dbo.ActiveDisruptedCombinationRouteMessages";

                    bulkCopy.WriteToServer(dtActiveDisruptedCombinationRouteMessages);
                }
            }
        }

        public void ReplicateActiveDisruptedRouteMessages(DataTable dtActiveDisruptedRouteMessages)
        {
            using (SqlConnection connection =
                   new SqlConnection(ConnectionString))
            {
                connection.Open();

                using (SqlCommand sqlCmd = new SqlCommand("TRUNCATE TABLE ActiveDisruptedRouteMessages", connection))
                {
                    sqlCmd.ExecuteNonQuery();
                }

                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                {
                    bulkCopy.DestinationTableName =
                        "dbo.ActiveDisruptedRouteMessages";

                    bulkCopy.WriteToServer(dtActiveDisruptedRouteMessages);
                }
            }
        }

        public void ReplicateActiveDisruptedRoutes(DataTable dtActiveDisruptedRoutes)
        {
            using (SqlConnection connection =
                   new SqlConnection(ConnectionString))
            {
                connection.Open();

                using (SqlCommand sqlCmd = new SqlCommand("TRUNCATE TABLE ActiveDisruptedRoutes", connection))
                {
                    sqlCmd.ExecuteNonQuery();
                }

                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                {
                    bulkCopy.DestinationTableName =
                        "dbo.ActiveDisruptedRoutes";

                    bulkCopy.WriteToServer(dtActiveDisruptedRoutes);
                }
            }
        }

        public void ReplicateActiveDisruptedStops(DataTable dtActiveDisruptedStops)
        {
            using (SqlConnection connection =
                   new SqlConnection(ConnectionString))
            {
                connection.Open();

                using (SqlCommand sqlCmd = new SqlCommand("TRUNCATE TABLE ActiveDisruptedStops", connection))
                {
                    sqlCmd.ExecuteNonQuery();
                }
                
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                {
                    bulkCopy.DestinationTableName =
                        "dbo.ActiveDisruptedStops";

                    bulkCopy.WriteToServer(dtActiveDisruptedStops);
                }
            }
        }


        public DataTable GetUnclearedDisruptions()
        {
            using (SqlConnection connection =
                   new SqlConnection(ConnectionString))
            {
                DataTable dtUnclearedDisruptions = new DataTable();
                using (SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM ttdms_Disruptions where ClearedDateTime IS NULL AND StatusId = 6", connection))
                {
                    adapter.Fill(dtUnclearedDisruptions);
                    return dtUnclearedDisruptions;
                }
            }
        }


        public DataTable GetDisruptedCombinationRouteMessages()
        {
            using (SqlConnection connection =
                   new SqlConnection(ConnectionString))
            {
                DataTable dtDisruptedCombMessages = new DataTable();
                using (SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM ttdms_DisruptedCombinationRouteMessages", connection))
                {
                    adapter.Fill(dtDisruptedCombMessages);
                    return dtDisruptedCombMessages;
                }
            }
        }


        public DataTable GetDisruptedRouteMessages()
        {
            using (SqlConnection connection =
                   new SqlConnection(ConnectionString))
            {
                DataTable dtDisruptedRouteMessages = new DataTable();
                using (SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM ttdms_DisruptedRouteMessages", connection))
                {
                    adapter.Fill(dtDisruptedRouteMessages);
                    return dtDisruptedRouteMessages;
                }
            }
        }


        public DataTable GetDisruptedRoutes()
        {
            using (SqlConnection connection =
                   new SqlConnection(ConnectionString))
            {
                DataTable dtDisruptedRoutes = new DataTable();
                using (SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM ttdms_DisruptedRoutes", connection))
                {
                    adapter.Fill(dtDisruptedRoutes);
                    return dtDisruptedRoutes;
                }
            }
        }


        public DataTable GetDisruptedStops()
        {
            using (SqlConnection connection =
                   new SqlConnection(ConnectionString))
            {
                DataTable dtDisruptedStops = new DataTable();
                using (SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM ttdms_DisruptedStops", connection))
                {
                    adapter.Fill(dtDisruptedStops);
                    return dtDisruptedStops;
                }
            }
        }


        public DataTable GetActiveDisruptedCombinationRouteMessages()
        {
            using (SqlConnection connection =
                   new SqlConnection(ConnectionString))
            {
                DataTable dtActiveDisruptedCombinationRouteMessages = new DataTable();
                using (SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM ActiveDisruptedCombinationRouteMessages", connection))
                {
                    adapter.Fill(dtActiveDisruptedCombinationRouteMessages);
                    return dtActiveDisruptedCombinationRouteMessages;
                }
            }
        }

        public DataTable GetActiveDisruptedRouteMessages()
        {
            using (SqlConnection connection =
                   new SqlConnection(ConnectionString))
            {
                DataTable dtActiveDisruptedRouteMessages = new DataTable();
                using (SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM ActiveDisruptedRouteMessages", connection))
                {
                    adapter.Fill(dtActiveDisruptedRouteMessages);
                    return dtActiveDisruptedRouteMessages;
                }
            }
        }

        public DataTable GetActiveDisruptedRoutes()
        {
            using (SqlConnection connection =
                   new SqlConnection(ConnectionString))
            {
                DataTable dtActiveDisruptedRoutes = new DataTable();
                using (SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM ActiveDisruptedRoutes", connection))
                {
                    adapter.Fill(dtActiveDisruptedRoutes);
                    return dtActiveDisruptedRoutes;
                }
            }
        }

        public DataTable GetActiveDisruptedStops()
        {
            using (SqlConnection connection =
                   new SqlConnection(ConnectionString))
            {
                DataTable dtActiveDisruptedStops = new DataTable();
                using (SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM ActiveDisruptedStops", connection))
                {
                    adapter.Fill(dtActiveDisruptedStops);
                    return dtActiveDisruptedStops;
                }
            }
        }
    }
}
