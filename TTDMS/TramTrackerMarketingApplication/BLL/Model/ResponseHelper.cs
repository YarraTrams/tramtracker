﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

/// <summary>
/// Summary description for Return
/// </summary>
/// 
namespace TTMarketing.BLL.Model
{
    [DataContract(Namespace = "")]
    public class ResponseHelper
    {
        [DataMember()]
        public object responseObject;
        [DataMember()]
        public string responseString;
        [DataMember()]
        public bool isError;

    }
}