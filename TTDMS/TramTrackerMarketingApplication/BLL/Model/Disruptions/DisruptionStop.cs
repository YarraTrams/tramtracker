﻿using System;
using System.IO;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;

namespace TTMarketing.BLL.Model.Disruptions
{

    public class DisruptionStop
    {
        public int RouteNumber { get; set; }
        public int TrackerID { get; set; }
        public int StopSequence { get; set; }
        public bool TramTrackerAvailable { get; set; }
        public bool DisplayPredictions { get; set; }
        //public MessagesSummary Message { get; set; }
        public bool IsExtraStop { get; set; }
        public bool IsUpStop { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Direction { get; set; }
        public string RouteCombination { get; set; }
        public int DisruptionID { get; set; }
        public IList<Int32> MissingRoutes { get; set; }
    }

 
}
