﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Collections;
using System.Runtime.Serialization;

namespace TTMarketing.BLL.Model.Disruptions
{
    [DataContract(Namespace = "")]
    public class FOCDisruption
    {
        [DataMember()]
        public int Id { get; set; }
        [DataMember()]
        public string Message { get; set; }

        [DataMember()]
        public DateTime DisruptionDateTime { get; set; }
        [DataMember()]
        [ScriptIgnore]
        public Nullable<DateTime> DisruptionCleared { get; set; }

        [DataMember()]
        [ScriptIgnore]
        public Nullable<DateTime> NormalServiceResumed { get; set; }


        [DataMember()]
        public Nullable<DateTime> Timestamp { get; set; }

        [DataMember()]
        public int RouteNo { get; set; }

        [DataMember()]
        public int UpFromStopNo { get; set; }

        [DataMember()]
        public int UpToStopNo { get; set; }

        [DataMember()]
        public int DownFromStopNo { get; set; }

        [DataMember()]
        public int DownToStopNo { get; set; }

        [DataMember()]
        public string UpFromStopName { get; set; }

        [DataMember()]
        public string UpToStopName { get; set; }

        [DataMember()]
        public string UpFromStopDirection { get; set; }

        [DataMember()]
        public string UpToStopDirection { get; set; }


        [DataMember()]
        public string DownFromStopName { get; set; }

        [DataMember()]
        public string DownToStopName { get; set; }

        [DataMember()]
        public string DownFromStopDirection { get; set; }

        [DataMember()]
        public string DownToStopDirection { get; set; }





    }
}
