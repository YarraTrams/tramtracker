﻿using System;
using System.IO;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;

namespace TTMarketing.BLL.Model.Disruptions
{

    public class DisruptionRoute 
    {
        public int RouteNumber { get; set; }
        public string UpDestination { get; set; }
        public string DownDestination { get; set; }
        public string Description { get; set; }
        public List<DisruptionStop> UpStops { get; set; }
        public List<DisruptionStop> DownStops { get; set; }
        //public DateTime StartTime { get; set; }
        //public DateTime EndTime { get; set; }
        public int DisruptionID { get; set; }

    }
     
}
