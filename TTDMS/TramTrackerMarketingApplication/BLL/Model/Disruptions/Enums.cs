﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TTMarketing.BLL.Model.Disruptions
{

    public enum DisruptionStatus
    {
        Created = 1,
        Updated = 2,
        Published = 3,
        Cleared = 4,
        Archived = 5,
        Pending = 6
    }


    public enum DisplayType
    {
        NA = 0,
        FreeText = 1,
        TabularColumn = 2

    }


}
