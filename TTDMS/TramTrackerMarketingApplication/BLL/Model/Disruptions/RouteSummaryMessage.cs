﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Collections;
using System.Runtime.Serialization;

namespace TTMarketing.BLL.Model.Disruptions
{

    public class RouteSummaryMessage
    {
        public int Id { get; set; }
        public string RouteNoDisplayValue { get; set; }
        public string ShortMessage { get; set; }
        public string MediumMessage { get; set; }
        public string LongMessage { get; set; }
        public bool AffectedAdditionalInfoOnWebSite { get; set; }
        public bool UnaffectedAdditionalInfoOnWebSite { get; set; }
        public DisplayType AffectedDisplayType { get; set; }
        public DisplayType UnaffectedDisplayType { get; set; }
        public string DisplayTypeString { get; set; }
        public bool HideDownStopMessages { get; set; }
        public bool HideUpStopMessages { get; set; }
    }
}
