﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Collections;
using System.Runtime.Serialization;

namespace TTMarketing.BLL.Model.Disruptions
{
    [DataContract(Namespace = "")]
    public class RouteMessage
    {

        public int Id { get; set; }
        public int RouteNo { get; set; }


        [DataMember()]
        public string AffectedShortMessage { get; set; }
        public string AffectedMediumMessage { get; set; }
        [DataMember()]
        public string AffectedLongMessage { get; set; }

        [DataMember()]
        public string UnaffectedShortMessage { get; set; }
        public string UnaffectedMediumMessage { get; set; }
        [DataMember()]
        public string UnaffectedLongMessage { get; set; }

        public bool AffectedAdditionalInfoOnWebSite { get; set; }
        public bool UnAffectedAdditionalInfoOnWebSite { get; set; }

        public int UpStopsCount { get; set; }
        public int DownStopsCount { get; set; }

        public int? LastUpdatedDisruptionId { get; set; }

        public bool? HideDownStopMessages { get; set; }
        public bool? HideUpStopMessages { get; set; }

        public string UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public IList<Disruption> CurrentDisruptions { get; set; }


    }
}
