﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Collections;
using System.Runtime.Serialization;

namespace TTMarketing.BLL.Model.Disruptions
{
    [DataContract(Namespace = "")]
    public class Disruption
    {
        [DataMember()]
        public int Id { get; set; }
        [DataMember()]
        public string Description { get; set; }
        [DataMember()]
        public string Name { get; set; }

        [DataMember()]
        public int? TT_DisruptionId { get; set; }
        [DataMember()]
        public Nullable<DateTime> StartDateTime { get; set; }
        [DataMember()]
        public Nullable<DateTime> ClearedDateTime { get; set; }

        public DisruptionStatus Status { get; set; }

        [DataMember()]
        public string CreatedBy { get; set; }
        [DataMember()]
        public DateTime CreatedDate { get; set; }
        [DataMember()]
        public string UpdatedBy { get; set; }
        [DataMember()]
        public DateTime UpdatedDate { get; set; }

        [DataMember()]
        public DateTime? ExpiryTime { get; set; }

        [DataMember()]
        public bool Valid { get; set; }

        public Int16 PrimaryDisruptedRouteNo { get; set; }
        public string PrimaryDisruptedRouteName { get; set; }

        public string AffectedRoutesString { get; set; }


    }
}
