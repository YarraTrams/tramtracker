﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Collections;
using System.Runtime.Serialization;
namespace TTMarketing.BLL.Model.Disruptions
{
    [DataContract(Namespace = "")]
    public class ActivityLog
    {
        [DataMember()]
        public int Id { get; set; }
        [DataMember()]
        public int? DisruptionId { get; set; }
        [DataMember()]
        public string Type { get; set; }
        [DataMember()]
        public string Message { get; set; }
        [DataMember()]
        public string CreatedBy { get; set; }
        [DataMember()]
        public DateTime CreatedDate { get; set; }
    }

}
