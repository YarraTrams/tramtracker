﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TTMarketing.DAL;
using TTMarketing.BLL.Model.Disruptions;
using TTMarketing.BLL.Helpers;

namespace TTMarketing.BLL.Controllers.Disruptions
{
    public class DisruptionStopsController
    {


        public static void AddAllDisruptionEffectedRouteStops(int disruptionId, short routeNo, bool upStop, string createdBy, bool displayPredictions)
        {
            try
            {
                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    ctx.ttdms_AddAllDisruptionAffectedRouteStops(disruptionId, routeNo, upStop, createdBy, displayPredictions);
                }
            }
            catch (Exception ex)
            {
                Util.LogError(disruptionId, ex);
                throw ex;
            }
        }

        public static void DeleteAllDisruptionEffectedRouteStops(int disruptionId, short routeNo, bool upStop, string deletedBy)
        {
            try
            {
                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    ctx.ttdms_DeleteAllDisruptionAffectedRouteStops(disruptionId, routeNo, upStop, deletedBy);
                }
            }
            catch (Exception ex)
            {
                Util.LogError(disruptionId, ex);
                throw ex;
            }
        }


        public static DisruptionStop AddDisruptionEffectedRouteStop(int disruptionId, short routeNo, short stopNo, bool upStop, bool tramTrackerAvailable, string createdBy, bool displayPredictions)
        {
            try
            {
                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    DisruptionStop st = new DisruptionStop();
                    ctx.ttdms_AddDisruptionAffectedRouteStop(disruptionId, routeNo, stopNo, upStop, tramTrackerAvailable, createdBy, displayPredictions);


                    st.DisruptionID = disruptionId;
                    st.RouteNumber = routeNo;
                    st.TrackerID = Convert.ToInt32(stopNo);
                    st.IsUpStop = upStop;
                    st.TramTrackerAvailable = false;
                    st.MissingRoutes = new List<int>();
                    st.DisplayPredictions = displayPredictions;
                    return st;
                }
            }
            catch (Exception ex)
            {
                Util.LogError(disruptionId, ex);
                throw ex;
            }
        }


        public static DisruptionStop DeleteDisruptionEffectedRouteStops(int disruptionId, short routeNo, short stopNo, bool upStop, string deletedBy)
        {
            try
            {
                DisruptionStop st = new DisruptionStop();

                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    ctx.ttdms_DeleteDisruptionAffectedRouteStop(disruptionId, routeNo, stopNo, upStop, deletedBy);
                }

                st.DisruptionID = disruptionId;
                st.RouteNumber = routeNo;
                st.TrackerID = Convert.ToInt32(stopNo);
                st.IsUpStop = upStop;
                st.MissingRoutes = new List<int>();
                return st;
            }
            catch (Exception ex)
            {
                Util.LogError(disruptionId, ex);
                throw ex;
            }

        }

        public static IList<DisruptionStop> GetAffectedStopsByDisruptionId(int disriptionId)
        {
            try
            {
                IList<DisruptionStop> list = new List<DisruptionStop>();

                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    var results = ctx.ttdms_GetGetAffectedStopsByDisruptionId(disriptionId);

                    foreach (ttdms_GetGetAffectedStopsByDisruptionIdResult result in results)
                    {
                        DisruptionStop stop = new DisruptionStop();
                        stop.Description = result.Description;
                        stop.Direction = result.Direction;
                        stop.RouteNumber = result.RouteNo;
                        stop.TrackerID = result.StopNo;
                        stop.IsUpStop = result.IsUpStop;
                        stop.RouteCombination = result.RouteCombination;
                        list.Add(stop);
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                Util.LogError(disriptionId, ex);
                throw ex;
            }
        }



    }
}
