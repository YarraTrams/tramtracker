﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TTMarketing.DAL;
using TTMarketing.BLL.Model.Disruptions;
using TTMarketing.BLL.Helpers;

namespace TTMarketing.BLL.Controllers.Disruptions
{
    public class DisruptionRoutesController
    {
        public static IList<DisruptionRoute> GetAllRoutes()
        {
            try
            {
                IList<DisruptionRoute> list = new List<DisruptionRoute>();

                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    var results = ctx.ttdms_GetAllRoutesList();
                    foreach (ttdms_GetAllRoutesListResult result in results)
                    {
                        //need to set dummay start/end time as JSON serializer errors if DateTime is not valid.
                        DisruptionRoute route = new DisruptionRoute { RouteNumber = result.RouteNo, Description = result.Description };
                        list.Add(route);
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                Util.LogError(0, ex);
                throw ex;
            }
        }

        public static IList<int> GetMissingRoutesForAffectedDisruptionStop(int disruptionId, int routeNo,short stopNo)
        {
            try
            {
                IList<int> results = new List<int>();

                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    var result = ctx.GetRouteCombinationsForStop((int)stopNo).First();
                    string val = result.RouteCombination;
                    string[] arr = val.Split(',');
                    foreach (string s in arr)
                    {
                        if (s != routeNo.ToString())
                        {
                            results.Add(Int32.Parse(s));
                        }
                    }
                }
                return results;
            }
            catch(Exception ex)
            {
                Util.LogError(disruptionId, ex);
                throw ex;
            }

        }

        public static DisruptionRoute GetStopsForDisruptedRoute(int routeNo)
        {
            try
            {
                DisruptionRoute route = new DisruptionRoute();
                route.RouteNumber = routeNo;

                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    //do up stops
                    var results = ctx.ttdms_GetDisruptedRouteStopDetails(routeNo, true);
                    route.UpStops = new List<DisruptionStop>();

                    foreach (ttdms_GetDisruptedRouteStopDetailsResult result in results)
                    {
                        DisruptionStop stop = new DisruptionStop();
                        stop.Description = result.Description;

                        stop.StopSequence = result.StopSequence;
                        stop.IsUpStop = result.UpStop;
                        stop.TrackerID = result.StopNo;
                        stop.RouteNumber = routeNo;
                        stop.TramTrackerAvailable = (result.AffectedStopNo == 0);
                        stop.MissingRoutes = new List<int>();
                        route.UpStops.Add(stop);
                    }

                    //do down stops
                    results = ctx.ttdms_GetDisruptedRouteStopDetails(routeNo, false);
                    route.DownStops = new List<DisruptionStop>();

                    foreach (ttdms_GetDisruptedRouteStopDetailsResult result in results)
                    {
                        DisruptionStop stop = new DisruptionStop();
                        stop.Description = result.Description;

                        stop.StopSequence = result.StopSequence;
                        stop.IsUpStop = result.UpStop;
                        stop.TrackerID = result.StopNo;
                        stop.RouteNumber = routeNo;
                        stop.TramTrackerAvailable = (result.AffectedStopNo == 0);
                        stop.MissingRoutes = new List<int>();
                        route.DownStops.Add(stop);
                    }
                }
                return route;
            }
            catch (Exception ex)
            {
                Util.LogError(0, ex);
                throw ex;
            }
        }

        public static DisruptionRoute GetStopsForDisruptionRoute(int disruptionId, short routeNo)
        {
            try
            {
                DisruptionRoute route = new DisruptionRoute();
                route.RouteNumber = routeNo;
                route.DisruptionID = disruptionId;

                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    var results = ctx.ttdms_GetDisruptionsRouteStopDetails(disruptionId, (int)routeNo, true);
                    route.UpStops = new List<DisruptionStop>();
                    foreach (ttdms_GetDisruptionsRouteStopDetailsResult result in results)
                    {
                        DisruptionStop stop = new DisruptionStop();
                        stop.Description = result.Description;
                        stop.DisruptionID = disruptionId;

                        stop.StopSequence = result.StopSequence;
                        stop.IsUpStop = result.UpStop;
                        stop.TrackerID = result.StopNo;
                        stop.RouteNumber = routeNo;
                        stop.DisplayPredictions = Convert.ToBoolean(result.DisplayPredictions);
                        stop.TramTrackerAvailable = (result.AffectedStopNo == 0);
                        stop.MissingRoutes = new List<int>();


                        if (!stop.TramTrackerAvailable)
                        {
                            var subResult = ctx.GetRouteCombinationsForStop(stop.TrackerID).First();
                            string val = subResult.RouteCombination;
                            string[] arr = val.Split(',');
                            foreach (string s in arr)
                            {
                                if (s != routeNo.ToString())
                                {
                                    stop.MissingRoutes.Add(Int32.Parse(s));
                                }
                            }
                        }
                        route.UpStops.Add(stop);

                    }//finish Up stops
                    //process down stops
                    results = ctx.ttdms_GetDisruptionsRouteStopDetails(disruptionId, (int)routeNo, false);
                    route.DownStops = new List<DisruptionStop>();
                    foreach (ttdms_GetDisruptionsRouteStopDetailsResult result in results)
                    {
                        DisruptionStop stop = new DisruptionStop();
                        stop.Description = result.Description;
                        stop.DisruptionID = disruptionId;

                        stop.StopSequence = result.StopSequence;
                        stop.IsUpStop = result.UpStop;
                        stop.TrackerID = result.StopNo;
                        stop.DisplayPredictions = Convert.ToBoolean(result.DisplayPredictions);
                        stop.RouteNumber = routeNo;
                        stop.TramTrackerAvailable = (result.AffectedStopNo == 0);
                        stop.MissingRoutes = new List<int>();


                        if (!stop.TramTrackerAvailable)
                        {
                            var subResult = ctx.GetRouteCombinationsForStop(stop.TrackerID).First();
                            string val = subResult.RouteCombination;
                            string[] arr = val.Split(',');
                            foreach (string s in arr)
                            {
                                if (s != routeNo.ToString())
                                {
                                    stop.MissingRoutes.Add(Int32.Parse(s));
                                }
                            }
                        }
                        route.DownStops.Add(stop);
                    }
                    route.DownStops.Reverse();


                }
                return route;
            }
            catch (Exception ex)
            {
                Util.LogError(disruptionId, ex);
                throw ex;
            }
        }

        public static IList<RoutesSummary> GetDisruptionRoutesDetails(int disruptionId, bool isUpStop)
        {
            IList<RoutesSummary> list = new List<RoutesSummary>();

            try
            {
                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    var results = ctx.ttdms_GetDisruptionRoutesDetails(disruptionId);
                    foreach (ttdms_GetDisruptionRoutesDetailsResult result in results)
                    {
                        RoutesSummary item = new RoutesSummary();
                        item.RouteNumber = result.RouteNo;
                        if (isUpStop)
                        {
                            item.UpDestination = result.UpDestination;
                            item.DownDestination = result.DownDestination;
                        }
                        else
                        {
                            item.UpDestination = result.DownDestination;
                            item.DownDestination = result.UpDestination;
                        }
                        list.Add(item);
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                Util.LogError(disruptionId, ex);
                throw ex;
            }
        }


        public static void AddDisruptionEffectedRoutes(int disruptionId, string createdBy)
        {
            try
            {
                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    ctx.ttdms_AddDisruptionAffectedRoutes(disruptionId, createdBy);
                }
            }
            catch(Exception ex)
            {
                Util.LogError(disruptionId, ex);
                throw ex;
            }
        }

        public static IList<DisruptionRoute> GetAllDisruptedRoutes()
        {
            IList<DisruptionRoute> list = new List<DisruptionRoute>();
            try
            {
                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    var results = ctx.ttdms_GetAllDisruptedRoutes();

                    foreach (ttdms_GetAllDisruptedRoutesResult result in results)
                    {
                        DisruptionRoute rt = new DisruptionRoute();
                        rt.RouteNumber = result.RouteNo;
                        rt.Description = result.Description;
                        list.Add(rt);
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                Util.LogError(0, ex);
                throw ex;
            }
        }
    }
}
