﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TTMarketing.DAL;
using TTMarketing.BLL.Model.Disruptions;
using YarraTrams.Library.Mailer;
using System.Configuration;
using System.Web.Helpers;
using TTMarketing.DAL;
using System.Data;
using TTMarketing.BLL.Helpers;

namespace TTMarketing.BLL.Controllers.Disruptions
{
    public class DisruptionController
    {

        public static FOCDisruption GetFOCDisruptionById(int id)
        {
            FOCDisruption dis = null;

            using (DisruptionsDataContext ctx = new DisruptionsDataContext())
            {


                IList<ttdms_GetFOCDisruptionByIdResult> result = ctx.ttdms_GetFOCDisruptionById(id).ToList();
                if (result.Count() > 0)
                {
                    dis = new FOCDisruption();
                    dis.Id = result[0].DisruptionID;
                    dis.DisruptionCleared = result[0].DisruptionCleared;
                    dis.DisruptionDateTime = result[0].DisruptionDateTime;
                    dis.Message = result[0].Message;
                    dis.NormalServiceResumed = result[0].NormalServiceResumed;
                    dis.RouteNo = result[0].RouteNo;

                    dis.UpFromStopNo = (result[0].UpFromStopNo.HasValue) ? (int)result[0].UpFromStopNo : 0;
                    dis.UpToStopNo = (result[0].UpToStopNo.HasValue) ? (int)result[0].UpToStopNo : 0;

                    dis.DownFromStopNo = (result[0].DownFromStopNo.HasValue) ? (int)result[0].DownFromStopNo : 0;
                    dis.DownToStopNo = (result[0].DownToStopNo.HasValue) ? (int)result[0].DownToStopNo : 0;

                    dis.UpFromStopName = !string.IsNullOrEmpty(result[0].UpFromStopName) ? result[0].UpFromStopName : "-";
                    dis.UpToStopName = !string.IsNullOrEmpty(result[0].UpToStopName) ? result[0].UpToStopName : "-";
                    dis.UpFromStopDirection = !string.IsNullOrEmpty(result[0].UpFromStopDirection) ? result[0].UpFromStopDirection : "-";
                    dis.UpToStopDirection = !string.IsNullOrEmpty(result[0].UpToStopDirection) ? result[0].UpToStopDirection : "-";

                    dis.DownFromStopName = !string.IsNullOrEmpty(result[0].DownFromStopName) ? result[0].DownFromStopName : "-";
                    dis.DownToStopName = !string.IsNullOrEmpty(result[0].DownToStopName) ? result[0].DownToStopName : "-";
                    dis.DownFromStopDirection = !string.IsNullOrEmpty(result[0].DownFromStopDirection) ? result[0].DownFromStopDirection : "-";
                    dis.DownToStopDirection = !string.IsNullOrEmpty(result[0].DownToStopDirection) ? result[0].DownToStopDirection : "-";
                    



                }
            }
            return dis;
        }




        public static Disruption GetDisruptionById(int id)
        {
            try
            {
                Disruption dr = null;

                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    var result = ctx.ttdms_GetDisruptionById((int)id).First();
                    dr = new Disruption();
                    dr.ClearedDateTime = result.ClearedDateTime;
                    dr.CreatedBy = result.CreatedBy;
                    dr.CreatedDate = result.CreatedDate;
                    dr.Description = result.Description;
                    dr.Name = result.Name;
                    dr.Id = result.Id;
                    dr.AffectedRoutesString = result.AffectedRouteList;
                    dr.PrimaryDisruptedRouteNo = result.PrimaryDisruptedRouteNo;
                    dr.PrimaryDisruptedRouteName = result.PrimaryDisruptedRouteName;
                    dr.Status = (DisruptionStatus)result.StatusId;
                    dr.StartDateTime = result.StartDateTime;
                    dr.TT_DisruptionId = result.TT_DisruptionId;
                    dr.UpdatedBy = result.UpdatedBy;
                    dr.UpdatedDate = result.UpdatedDate;
                }
                return dr;
            }
            catch (Exception ex)
            {
                Util.LogError(id, ex);
                throw ex;
            }
        }

        public static IList<Disruption> GetDisruptionByStatus(DisruptionStatus status)
        {
            try
            {
                IList<Disruption> list = new List<Disruption>();

                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {

                    var results = ctx.ttdms_GetDisruptionsByStatus((int)status);

                    foreach (ttdms_GetDisruptionsByStatusResult result in results)
                    {
                        Disruption dr = new Disruption();
                        dr.Id = result.Id;
                        dr.ClearedDateTime = result.ClearedDateTime;
                        dr.StartDateTime = result.StartDateTime;

                        dr.CreatedBy = result.CreatedBy;
                        dr.CreatedDate = result.CreatedDate;
                        dr.Description = result.Description;
                        dr.Name = result.Name;
                        dr.Status = (DisruptionStatus)result.StatusId;
                        dr.PrimaryDisruptedRouteNo = result.PrimaryDisruptedRouteNo;
                        dr.TT_DisruptionId = result.TT_DisruptionId;
                        dr.UpdatedBy = result.UpdatedBy;
                        dr.UpdatedDate = result.UpdatedDate;
                        dr.AffectedRoutesString = result.AffectedRouteList;

                        list.Add(dr);
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                Util.LogError(0, ex);
                throw ex;
            }

        }

        public static IList<Disruption> GetActiveDisruptions()
        {
            try
            {
                IList<Disruption> list = new List<Disruption>();

                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    var results = ctx.ttdms_GetActiveDisruptions();

                    foreach (ttdms_GetActiveDisruptionsResult result in results)
                    {
                        Disruption dr = new Disruption();
                        dr.Id = result.Id;
                        dr.ClearedDateTime = result.ClearedDateTime;
                        dr.StartDateTime = result.StartDateTime;

                        dr.CreatedBy = result.CreatedBy;
                        dr.CreatedDate = result.CreatedDate;
                        dr.Description = result.Description;
                        dr.Name = result.Name;
                        dr.Status = (DisruptionStatus)result.StatusId;
                        dr.PrimaryDisruptedRouteNo = result.PrimaryDisruptedRouteNo;
                        dr.TT_DisruptionId = result.TT_DisruptionId;
                        dr.UpdatedBy = result.UpdatedBy;
                        dr.UpdatedDate = result.UpdatedDate;
                        dr.AffectedRoutesString = result.AffectedRouteList;
                        dr.ExpiryTime = result.ExpiryTime;
                        dr.Valid = (bool)result.Valid;
                        list.Add(dr);
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                Util.LogError(0, ex);
                throw ex;
            }
        }



        public static Disruption Create(string name,string description, int? ttDisruptionId, int statusId, int primaryDisruptedRouteNo ,string createdBy)
        {
            try
            {
                Disruption dr = new Disruption();

                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    var result = ctx.ttdms_AddDisruption(name, description, ttDisruptionId, (short)primaryDisruptedRouteNo, statusId, createdBy).FirstOrDefault();
                    dr.Id = (int)result.Column1;
                }

                return dr;
            }
            catch (Exception ex)
            {
                Util.LogError(ttDisruptionId.GetValueOrDefault(0), ex);
                throw ex;
            }
        }

        public static void Update(int disruptionId, string name,string description, int? ttDisruptionId, int statusId,int primaryDisruptedRouteNo, string upDatedBy)
        {
            try
            {
                Disruption dr = new Disruption();

                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    ctx.ttdms_UpdateDisruption(disruptionId, ttDisruptionId, name, description, statusId, (short)primaryDisruptedRouteNo, upDatedBy);

                }
            }
            catch (Exception ex)
            {
                Util.LogError(ttDisruptionId.GetValueOrDefault(0), ex);
                throw ex;
            }
        }


        public static void PublishAllDisruptions(string createdBy)
        {
            try
            {
                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    ctx.ttdms_PublishDisruptionsData(createdBy);

                    string disruptionNotificationBody = "";
                    //disruption status is 3 is for all publised disruptions
                    /*var disruptionResult = ctx.GetTTMDisruptionsByStatus(3).OrderBy(d => d.CreatedDate).ToList();
                    foreach (GetTTMDisruptionsByStatusResult r in disruptionResult)
                    {*/
                    var disruptionResult = ctx.ttdms_GetActiveDisruptions().OrderBy(d => d.CreatedDate).ToList();
                    foreach (ttdms_GetActiveDisruptionsResult r in disruptionResult)
                    {
                        disruptionNotificationBody += "FOC Disruption ID: " + r.TT_DisruptionId + "\n\nName: " + r.Name +
                                            "\n\nAffected Routes: " + r.AffectedRouteList;
                        var affectedUpStopsResult = ctx.ttdms_GetDisruptionsRouteStopDetails(r.Id, r.PrimaryDisruptedRouteNo, true);

                        var affectedUpStops = affectedUpStopsResult.Where(s => s.AffectedStopNo != 0).ToList();

                        if (affectedUpStops.Count > 0)
                        {
                            var firstAffectedUpStop = affectedUpStops.First();
                            var lastAffectedUpStop = affectedUpStops.Last();

                            if (firstAffectedUpStop != null)
                            {
                                disruptionNotificationBody += "\n\nUp direction affected stop(s) from: " + firstAffectedUpStop.AffectedStopNo + " - " + firstAffectedUpStop.Description + " to " +
                                    lastAffectedUpStop.AffectedStopNo + " - " + lastAffectedUpStop.Description;
                            }
                        }


                        var affectedDownStopsResult = ctx.ttdms_GetDisruptionsRouteStopDetails(r.Id, r.PrimaryDisruptedRouteNo, false);

                        var affectedDownStops = affectedDownStopsResult.Where(s => s.AffectedStopNo != 0).ToList();

                        if (affectedDownStops.Count > 0)
                        {
                            var firstAffectedDownStop = affectedDownStops.First();
                            var lastAffectedDownStop = affectedDownStops.Last();

                            disruptionNotificationBody += "\n\nDown direction affected stop(s) from: " + firstAffectedDownStop.AffectedStopNo + " - " + firstAffectedDownStop.Description + " to " +
                                    lastAffectedDownStop.AffectedStopNo + " - " + lastAffectedDownStop.Description;
                        }
                        disruptionNotificationBody += "\n\n----------------------------------------------\n\n";

                        //replicate TTDMS active data to other databases
                        DisruptionDAL dalMainDB = new DisruptionDAL();
                        dalMainDB.ConnectionString = ConfigurationManager.ConnectionStrings["DisruptionsConnectionString"].ConnectionString;
                        DataTable dtActiveDisruptedCombRouteMsgs = dalMainDB.GetActiveDisruptedCombinationRouteMessages();
                        DataTable dtActiveDisruptedRouteMsgs = dalMainDB.GetActiveDisruptedRouteMessages();
                        DataTable dtActiveDisruptedRoutes = dalMainDB.GetActiveDisruptedRoutes();
                        DataTable dtActiveDisruptedStops = dalMainDB.GetActiveDisruptedStops();

                        foreach (ConnectionStringSettings connStringSettings in ConfigurationManager.ConnectionStrings)
                        {
                            if (connStringSettings.Name.Contains("DisruptionsReplicationConnectionString"))
                            {
                                try
                                {
                                    DisruptionDAL dalReplicationDB = new DAL.DisruptionDAL();
                                    dalReplicationDB.ConnectionString = connStringSettings.ConnectionString;
                                    dalReplicationDB.ReplicateActiveDisruptedCombinationRouteMessages(dtActiveDisruptedCombRouteMsgs);
                                    dalReplicationDB.ReplicateActiveDisruptedRouteMessages(dtActiveDisruptedRouteMsgs);
                                    dalReplicationDB.ReplicateActiveDisruptedRoutes(dtActiveDisruptedRoutes);
                                    dalReplicationDB.ReplicateActiveDisruptedStops(dtActiveDisruptedStops);
                                }
                                catch (Exception ex)
                                {
                                    disruptionNotificationBody += "TTDMS Data Replication failed for the following connection string:\n";
                                    disruptionNotificationBody += connStringSettings.ConnectionString + "\n";
                                    disruptionNotificationBody += "Exception Details: " + ex.Message + "\n\n";
                                }
                            }
                        }
                    }

                    string mailServer = ConfigurationManager.AppSettings["MailServer"];
                    string notificationSender = ConfigurationManager.AppSettings["NotificationSender"];
                    string notificationReceiver = ConfigurationManager.AppSettings["NotificationRecipients"];
                    if (ConfigurationManager.AppSettings["SendEmails"] == "true")
                    {
                        Emailer.SendEmail(mailServer, notificationSender, notificationReceiver, "Disruption published", disruptionNotificationBody);
                    }

                    Dictionary<string, string> log_data = new Dictionary<string, string>();
                    log_data.Add("disruption_id", null);
                    log_data.Add("to", notificationReceiver);
                    log_data.Add("subject", "Disruption published");
                    log_data.Add("body", disruptionNotificationBody);

                    ActivityLog log = ActivityLogController.Create(null, Json.Encode(log_data), "Email", createdBy);

                }
            }
            catch (Exception ex)
            {
                Util.LogError(0, ex);
                throw ex;
            }
        }

        public static bool IsFocDisruptionAssigned(int focDisruptionId)
        {
            bool isAssigned = false;

            using (DisruptionsDataContext ctx = new DisruptionsDataContext())
            {
                var result = ctx.ttdms_GetIsFocDisruptionAssigned(focDisruptionId).First();
                if (result.Count > 0)
                {
                    isAssigned = true;
                }

            }

            return isAssigned;

        }


        public static DateTime? GetClearedDate(int disruptionId)
        {

            DateTime? dt = DateTime.UtcNow;
            using (DisruptionsDataContext ctx = new DisruptionsDataContext())
            {
                var focDisruption = ctx.ttdms_GetFOCDisruption(disruptionId).First();
                dt = focDisruption.DisruptionCleared;
            }


            return dt;
        }



        public static string ClearDisruption(int disruptionId, string updatedBy, int ttl)
        {
            try
            {
                string disruptionNotificationBody = "";

                Disruption dr = DisruptionController.GetDisruptionById(disruptionId);
                disruptionNotificationBody += "Clearing Disruption for " + dr.Description + " (Primary Route: " + dr.PrimaryDisruptedRouteNo + ")\n\n";

                string responseText = "OK";
                StringBuilder sb = new StringBuilder();

                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    if (ttl == 0 && dr.TT_DisruptionId != null)
                    {
                        var focDisruption = ctx.ttdms_GetFOCDisruption(disruptionId).First();

                        if (!focDisruption.DisruptionCleared.HasValue)
                        {
                            //responseText = "NOTCLEARED";
                            //return responseText;
                            ttl = 1;
                        }
                    }

                    /*var sharedDisruptedRoutes = ctx.GetTTMOtherDisruptionAndRoutes(disruptionId);
                    var count = 0;

                    foreach (GetTTMOtherDisruptionAndRoutesResult result in sharedDisruptedRoutes)
                    {
                        sb.Append("Disruption Id " + result.DisruptionId.ToString() + " affecting Route " + result.RouteNo.ToString() + "\n"); 
                        count++;

                    }*/
                    string shared_messages = DisruptionController.GetSharedDisruptionMessages(disruptionId);

                    if (ttl == 0)
                    {

                        /*if (count == 0)
                        {
                            var routeMessages = ctx.GetTTMRouteMessagesForDisruption(disruptionId);
                            foreach (GetTTMRouteMessagesForDisruptionResult result in routeMessages)
                            {
                                ctx.ClearTTMRouteDisruptionMessages(result.Id, updatedBy);
                            }

                            var comboMessages = ctx.GetTTMRouteCombinationMessagesForDisruption(disruptionId);
                            foreach (GetTTMRouteCombinationMessagesForDisruptionResult result in comboMessages)
                            {
                                ctx.ClearTTMCombinationRouteDisruptionMessages(result.Id, updatedBy);
                            }
                        }
                        else
                        {
                            string response = "This Disruption had shared affected Routes with the following Disruptions\n\n" + sb.ToString() + "\n Please update the relevant Messages for these Disruptions";
                            responseText = response;
                        }*/
                        if (shared_messages != "")
                        {
                            responseText = "This Disruption had shared affected Routes with the following Disruptions\n\n" + shared_messages + "\n Please update the relevant Messages for these Disruptions";
                        }


                        var currentCount = 0;
                        /*var currentDisruptions = ctx.GetTTMDisruptionsByStatus(1);
                        foreach (GetTTMDisruptionsByStatusResult result in currentDisruptions)
                        {*/
                        var currentDisruptions = ctx.ttdms_GetActiveDisruptions();
                        foreach (ttdms_GetActiveDisruptionsResult result in currentDisruptions)
                        {
                            if (result.StatusId < 4)
                            {
                                currentCount++;
                            }

                        }
                        ctx.ttdms_ClearDisruption(disruptionId, updatedBy);
                        ctx.ttdms_AutoClearDisruptionMessages(updatedBy);
                        ctx.ttdms_AutoClearDisruptionComboMessages(updatedBy);
                        ctx.ttdms_PublishDisruptionsData(updatedBy);


                        //Replicate all TTDMS data to other databases
                        DisruptionDAL dalMainDB = new DisruptionDAL();
                        dalMainDB.ConnectionString = ConfigurationManager.ConnectionStrings["DisruptionsConnectionString"].ConnectionString;
                        DataTable dtActiveDisruptedCombRouteMsgs = dalMainDB.GetActiveDisruptedCombinationRouteMessages();
                        DataTable dtActiveDisruptedRouteMsgs = dalMainDB.GetActiveDisruptedRouteMessages();
                        DataTable dtActiveDisruptedRoutes = dalMainDB.GetActiveDisruptedRoutes();
                        DataTable dtActiveDisruptedStops = dalMainDB.GetActiveDisruptedStops();


                        foreach (ConnectionStringSettings connStringSettings in ConfigurationManager.ConnectionStrings)
                        {
                            if (connStringSettings.Name.Contains("DisruptionsReplicationConnectionString"))
                            {

                                DisruptionDAL dalReplicationDB = new DAL.DisruptionDAL();
                                dalReplicationDB.ConnectionString = connStringSettings.ConnectionString;
                                dalReplicationDB.ReplicateActiveDisruptedCombinationRouteMessages(dtActiveDisruptedCombRouteMsgs);
                                dalReplicationDB.ReplicateActiveDisruptedRouteMessages(dtActiveDisruptedRouteMsgs);
                                dalReplicationDB.ReplicateActiveDisruptedRoutes(dtActiveDisruptedRoutes);
                                dalReplicationDB.ReplicateActiveDisruptedStops(dtActiveDisruptedStops);
                            }
                        }

                        disruptionNotificationBody += "Disruption has been cleared successfully";
                        /*if (currentCount == 1)
                        {
                            ctx.PublishTTMDisruptionsData(updatedBy);
                            responseText = "List";
                        }*/

                    }
                    else
                    {
                        /* if (dr.TT_DisruptionId != null)
                         {
                             var focDisruption = ctx.GetTTMFOCDisruption(disruptionId).First();

                             if (!focDisruption.DisruptionCleared.HasValue)
                             {
                                 responseText = "This disruption has an associated unlceared FOC, it will be cleared " + ttl.ToString() + "minutes after FOC clearance";                            
                             }                        
                         }*/

                        try
                        {
                            var ttl_result = ctx.ttdms_UpdateTTL(disruptionId, updatedBy, ttl).First();
                            DisruptionDAL dalMainDB = new DisruptionDAL();
                            dalMainDB.ConnectionString = ConfigurationManager.ConnectionStrings["DisruptionsConnectionString"].ConnectionString;
                            DataTable dtUnclearedDisruptions = dalMainDB.GetUnclearedDisruptions();
                            DataTable dtDisruptedCombMessages = dalMainDB.GetDisruptedCombinationRouteMessages();
                            DataTable dtDisruptedMessages = dalMainDB.GetDisruptedRouteMessages();
                            DataTable dtDisruptedStops = dalMainDB.GetDisruptedStops();
                            DataTable dtDisruptedRoutes = dalMainDB.GetDisruptedRoutes();

                            foreach (ConnectionStringSettings connStringSettings in ConfigurationManager.ConnectionStrings)
                            {
                                if (connStringSettings.Name.Contains("DisruptionsReplicationConnectionString"))
                                {
                                    DisruptionDAL dalReplicationDB = new DAL.DisruptionDAL();
                                    dalReplicationDB.ConnectionString = connStringSettings.ConnectionString;
                                    dalReplicationDB.ReplicateUnclearedDisruptions(dtUnclearedDisruptions);
                                    dalReplicationDB.ReplicateDisruptedCombinationRouteMessages(dtDisruptedCombMessages);
                                    dalReplicationDB.ReplicateDisruptedRouteMessages(dtDisruptedMessages);
                                    dalReplicationDB.ReplicateDisruptedRoutes(dtDisruptedRoutes);
                                    dalReplicationDB.ReplicateDisruptedStops(dtDisruptedStops);
                                }
                            }

                            if (ttl_result.ExpiryTime == null)
                            {
                                disruptionNotificationBody += "This disruption has an associated uncleared FOC, it will be cleared " + ttl.ToString() + "\n\n";
                                responseText = "This disruption has an associated uncleared FOC, it will be cleared " + ttl.ToString() + " minutes after FOC clearance";
                            }
                            else
                            {
                                disruptionNotificationBody += "This disruption will be automatically cleared at " + ttl_result.ExpiryTime.ToString() + "\n\n";
                                responseText = "This disruption will be automatically cleared at " + ttl_result.ExpiryTime.ToString();
                            }

                            if (shared_messages != "")
                            {
                                disruptionNotificationBody += "\n\nIt shares affected routes with the following disruptions:\n\n" + shared_messages + "\n Please update the relevant Messages for these Disruptions once this disruption is cleared.\n\n";
                                responseText += "\n\nIt shares affected routes with the following disruptions:\n\n" + shared_messages + "\n Please update the relevant Messages for these Disruptions once this disruption is cleared.";
                            }
                        }
                        catch (Exception ex)
                        {
                            disruptionNotificationBody += "An exception occurred during clearing TTDMS disruption: \n\n";
                            disruptionNotificationBody += ex.Message;
                        }
                    }
                }
                string mailServer = ConfigurationManager.AppSettings["MailServer"];
                string notificationSender = ConfigurationManager.AppSettings["NotificationSender"];
                string notificationReceiver = ConfigurationManager.AppSettings["NotificationRecipients"];
                if (ConfigurationManager.AppSettings["SendEmails"] == "true")
                {
                    Emailer.SendEmail(mailServer, notificationSender, notificationReceiver, "Disruption cleared", disruptionNotificationBody);
                }

                return responseText;
            }
            catch (Exception ex)
            {
                Util.LogError(disruptionId, ex);
                throw ex;
            }
        }

        public static RouteMessage GetPrimaryRouteMessages(int disruptionId)
        {
            try
            {
                RouteMessage msg = new RouteMessage();
                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    var result = ctx.ttdms_GetDisruptionPrimaryRouteMessages(disruptionId).FirstOrDefault();
                    if (result != null)
                    {
                        msg.AffectedShortMessage = result.AffectedShortMessage;
                        msg.AffectedLongMessage = result.AffectedLongMessage;
                        msg.UnaffectedShortMessage = result.UnaffectedShortMessage;
                        msg.UnaffectedLongMessage = result.UnaffectedLongMessage;
                    }
                    return msg;
                }
            }
            catch (Exception ex)
            {
                Util.LogError(disruptionId, ex);
                throw ex;
            }
        }

        public static void SetMessageDirectionHidden(int MessageId, bool HideUpStopMessages, bool HideDownStopMessages)
        {
            try
            {
                Disruption dr = new Disruption();

                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    ctx.ttdms_SetMessageDirectionHidden(MessageId, HideUpStopMessages, HideDownStopMessages);
                }
            }
            catch (Exception ex)
            {
                Util.LogError(0, ex);
                throw ex;
            }
        }

        public static void SetCombinationMessageDirectionHidden(int MessageId, bool HideUpStopMessages, bool HideDownStopMessages)
        {
            try
            {
                Disruption dr = new Disruption();

                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    ctx.ttdms_SetCombinationMessageDirectionHidden(MessageId, HideUpStopMessages, HideDownStopMessages);
                }
            }
            catch (Exception ex)
            {
                Util.LogError(0, ex);
                throw ex;
            }
        }

        public static string GetSharedDisruptionMessages(int disruptionId) 
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    var sharedDisruptedRoutes = ctx.ttdms_GetOtherDisruptionAndRoutes(disruptionId);

                    foreach (ttdms_GetOtherDisruptionAndRoutesResult result in sharedDisruptedRoutes)
                    {
                        sb.Append("Disruption Id " + result.DisruptionId.ToString() + " affecting Route " + result.RouteNo.ToString() + "\n");
                    }
                }
                IList<RouteCombinationMessage> clist = DisruptionMessagesController.GetAllDisruptedRouteCombinationMessagesByDisruptionId(disruptionId);
                IList<RouteCombinationMessage> alist = DisruptionMessagesController.GetAllUnaffectedDisruptedRouteCombinationMessagesByDisruptionId(disruptionId);
                foreach (RouteCombinationMessage msg in alist)
                {
                    if (!clist.Any(m => m.Id == msg.Id))
                    {
                        clist.Add(msg);
                    }
                }
                foreach (RouteCombinationMessage msg in clist)
                {
                    foreach (Disruption dr in msg.CurrentDisruptions)
                    {
                        if (dr.Id != disruptionId)
                        {
                            sb.Append("Disruption Id " + dr.Id.ToString() + " stop corridor " + msg.RouteNoCombination + "\n");
                        }

                    }
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                Util.LogError(disruptionId, ex);
                throw ex;
            }
        }

        public static void SetValidStatus(int disruptionId, bool valid)
        {
            try
            {
                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    var status = ctx.ttdms_UpdateDisruptionValidStatus(disruptionId, valid);
                }
            }
            catch (Exception ex)
            {
                Util.LogError(disruptionId, ex);
                throw ex;
            }
        }

        public static void ClearUnusedMessages(string updatedBy)
        {
            try
            {
                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    ctx.ttdms_AutoClearDisruptionMessages(updatedBy);
                    ctx.ttdms_AutoClearDisruptionComboMessages(updatedBy);
                }
            }
            catch (Exception ex)
            {
                Util.LogError(0, ex);
                throw ex;
            }
        }

    }
}
