﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TTMarketing.DAL;
using TTMarketing.BLL.Model.Disruptions;
using TTMarketing.BLL.Helpers;

namespace TTMarketing.BLL.Controllers.Disruptions
{

    public class MessageSorter : IComparer<RouteCombinationMessage>
    {
        public int Compare(RouteCombinationMessage x, RouteCombinationMessage y)
        {
            string[] xArray = x.RouteNoCombination.Split(',');
            string[] yArray = y.RouteNoCombination.Split(',');

            int xId = Int32.Parse(xArray[0]);
            int yId = Int32.Parse(yArray[0]);
            return xId.CompareTo(yId);

        }
    }

    public class DisruptionMessagesController
    {
        public static IList<RouteMessage> GetAllDisruptedRouteMessages()
        {
            try
            {
                IList<RouteMessage> list = new List<RouteMessage>();

                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    var results = ctx.ttdms_GetDisruptedRoutesMessages();

                    foreach (ttdms_GetDisruptedRoutesMessagesResult result in results)
                    {
                        RouteMessage msg = new RouteMessage();
                        msg.AffectedAdditionalInfoOnWebSite = result.AffectedAdditionalInfoOnWebSite;
                        msg.UnAffectedAdditionalInfoOnWebSite = (bool)result.UnaffectedAdditionalInfoOnWebSite;


                        msg.AffectedLongMessage = result.AffectedLongMessage;

                        msg.AffectedShortMessage = result.AffectedShortMessage;
                        msg.Id = result.Id;
                        msg.RouteNo = result.RouteNo;
                        msg.UnaffectedLongMessage = result.UnaffectedLongMessage;
                        msg.UpStopsCount = (int)result.UpStopsCount;
                        msg.DownStopsCount = (int)result.DownStopsCount;
                        msg.UnaffectedShortMessage = result.UnaffectedShortMessage;
                        msg.UpdatedBy = result.UpdatedBy;
                        msg.UpdatedDate = result.UpdatedDate;



                        msg.CurrentDisruptions = new List<Disruption>();

                        var disruptions = ctx.ttdms_GetDisruptionsForRoute(msg.RouteNo);
                        foreach (ttdms_GetDisruptionsForRouteResult dis in disruptions)
                        {
                            Disruption dr = new Disruption { Id = dis.Id, Name = dis.Name };
                            msg.CurrentDisruptions.Add(dr);

                        }



                        list.Add(msg);
                    }
                }
                return list;
            }
            catch(Exception ex)
            {
                Util.LogError(0, ex);
                throw ex;
            }
        }

        public static IList<RouteMessage> GetAllDisruptedRouteMessagesByDisruptionId(int disruptionId)
        {
            try
            {
                IList<RouteMessage> list = new List<RouteMessage>();

                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    var results = ctx.ttdms_GetDisruptedRoutesMessagesByDisruptionId(disruptionId);

                    foreach (ttdms_GetDisruptedRoutesMessagesByDisruptionIdResult result in results)
                    {
                        RouteMessage msg = new RouteMessage();
                        msg.AffectedAdditionalInfoOnWebSite = result.AffectedAdditionalInfoOnWebSite;
                        msg.UnAffectedAdditionalInfoOnWebSite = (bool)result.UnaffectedAdditionalInfoOnWebSite;

                        msg.AffectedLongMessage = result.AffectedLongMessage;

                        msg.AffectedShortMessage = result.AffectedShortMessage;
                        msg.Id = result.Id;
                        msg.RouteNo = result.RouteNo;
                        msg.UnaffectedLongMessage = result.UnaffectedLongMessage;
                        msg.UnaffectedShortMessage = result.UnaffectedShortMessage;
                        msg.UpdatedBy = result.UpdatedBy;
                        msg.UpdatedDate = result.UpdatedDate;
                        msg.UpStopsCount = (int)result.UpStopsCount;
                        msg.DownStopsCount = (int)result.DownStopsCount;
                        msg.LastUpdatedDisruptionId = result.LastUpdatedDisruptionId;
                        msg.HideDownStopMessages = result.HideDownStopMessages;
                        msg.HideUpStopMessages = result.HideUpStopMessages;

                        msg.CurrentDisruptions = new List<Disruption>();

                        var disruptions = ctx.ttdms_GetDisruptionsForRoute(msg.RouteNo);
                        foreach (ttdms_GetDisruptionsForRouteResult dis in disruptions)
                        {
                            Disruption dr = new Disruption { Id = dis.Id, Name = dis.Name };
                            msg.CurrentDisruptions.Add(dr);

                        }

                        list.Add(msg);
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                Util.LogError(disruptionId, ex);
                throw ex;
            }
        }

        public static IList<RouteCombinationMessage> GetAllUnaffectedDisruptedRouteCombinationMessagesByDisruptionId(int disruptionId)
        {
            try
            {
                List<RouteCombinationMessage> list = new List<RouteCombinationMessage>();

                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    var results = ctx.ttdms_GetUnaffectedDisruptionRouteCombinationMessagesByDisruptionId(disruptionId);
                    foreach (ttdms_GetUnaffectedDisruptionRouteCombinationMessagesByDisruptionIdResult result in results)
                    {
                        RouteCombinationMessage msg = new RouteCombinationMessage();
                        msg.AffectedAdditionalInfoOnWebSite = result.AffectedAdditionalInfoOnWebsite;
                        msg.UnAffectedAdditionalInfoOnWebSite = (bool)result.UnaffectedAdditionalInfoOnWebSite;
                        msg.AffectedLongMessage = result.AffectedLongMessage;
                        msg.AffectedShortMessage = result.AffectedShortMessage;
                        msg.Id = (int)result.Id;
                        msg.RouteNoCombination = result.RouteNoCombination;
                        msg.UnaffectedLongMessage = result.UnaffectedLongMessage;
                        msg.UnaffectedShortMessage = result.UnaffectedShortMessage;
                        msg.UpdatedBy = result.UpdatedBy;
                        msg.UpdatedDate = result.UpdatedDate;
                        msg.UpStopsCount = (int)result.UpStopsCount;
                        msg.DownStopsCount = (int)result.DownStopsCount;
                        msg.AffectedDisplayType = (DisplayType)result.AffectedDisplayType;
                        msg.UnaffectedDisplayType = (DisplayType)result.UnaffectedDisplayType;
                        msg.LastUpdatedDisruptionId = result.LastUpdatedDisruptionId;
                        msg.HideDownStopMessages = result.HideDownStopMessages;
                        msg.HideUpStopMessages = result.HideUpStopMessages;
                        msg.IsUnaffectedByCurrentDisruption = true;

                        msg.CurrentDisruptions = new List<Disruption>();
                        var disruptions = ctx.ttdms_GetDisruptionsByRouteCombination(msg.Id);
                        foreach (ttdms_GetDisruptionsByRouteCombinationResult dis in disruptions)
                        {
                            Disruption dr = new Disruption { Id = dis.DisruptionId, Name = dis.Name };
                            msg.CurrentDisruptions.Add(dr);

                        }
                        list.Add(msg);
                    }
                }
                list.Sort(new MessageSorter());
                return list;
            }
            catch (Exception ex)
            {
                Util.LogError(disruptionId, ex);
                throw ex;
            }

        }

        public static IList<RouteCombinationMessage> GetAllAffectedDisruptedRouteCombinationMessages()
        {
            try
            {
                List<RouteCombinationMessage> list = new List<RouteCombinationMessage>();

                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    var results = ctx.ttdms_GetAffectedDisruptionRouteCombinationMessages();

                    foreach (ttdms_GetAffectedDisruptionRouteCombinationMessagesResult result in results)
                    {
                        RouteCombinationMessage msg = new RouteCombinationMessage();
                        msg.AffectedAdditionalInfoOnWebSite = result.AffectedAdditionalInfoOnWebsite;
                        msg.UnAffectedAdditionalInfoOnWebSite = (bool)result.UnaffectedAdditionalInfoOnWebSite;
                        msg.AffectedLongMessage = result.AffectedLongMessage;

                        msg.AffectedShortMessage = result.AffectedShortMessage;
                        msg.Id = result.Id;
                        msg.RouteNoCombination = result.RouteNoCombination;
                        msg.UnaffectedLongMessage = result.UnaffectedLongMessage;

                        msg.UnaffectedShortMessage = result.UnaffectedShortMessage;
                        msg.UpdatedBy = result.UpdatedBy;
                        msg.UpdatedDate = result.UpdatedDate;
                        msg.UpStopsCount = (int)result.UpStopsCount;
                        msg.DownStopsCount = (int)result.DownStopsCount;
                        msg.AffectedDisplayType = (DisplayType)result.AffectedDisplayType;
                        msg.UnaffectedDisplayType = (DisplayType)result.UnaffectedDisplayType;

                        msg.CurrentDisruptions = new List<Disruption>();
                        var disruptions = ctx.ttdms_GetDisruptionsByRouteCombination(msg.Id);
                        foreach (ttdms_GetDisruptionsByRouteCombinationResult dis in disruptions)
                        {
                            Disruption dr = new Disruption { Id = dis.DisruptionId, Name = dis.Name };
                            msg.CurrentDisruptions.Add(dr);

                        }


                        list.Add(msg);
                    }
                }
                list.Sort(new MessageSorter());
                return list;
            }
            catch (Exception ex)
            {
                Util.LogError(0, ex);
                throw ex;
            }
        }


        public static IList<RouteCombinationMessage> GetAllUnaffectedDisruptedRouteCombinationMessages()
        {
            try
            {
                List<RouteCombinationMessage> list = new List<RouteCombinationMessage>();

                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    var results = ctx.ttdms_GetUnaffectedDisruptionRouteCombinationMessages();

                    foreach (ttdms_GetUnaffectedDisruptionRouteCombinationMessagesResult result in results)
                    {
                        RouteCombinationMessage msg = new RouteCombinationMessage();
                        msg.AffectedAdditionalInfoOnWebSite = result.AffectedAdditionalInfoOnWebsite;
                        msg.UnAffectedAdditionalInfoOnWebSite = (bool)result.UnaffectedAdditionalInfoOnWebSite;
                        msg.AffectedLongMessage = result.AffectedLongMessage;

                        msg.AffectedShortMessage = result.AffectedShortMessage;
                        msg.Id = result.Id;
                        msg.RouteNoCombination = result.RouteNoCombination;
                        msg.UnaffectedLongMessage = result.UnaffectedLongMessage;

                        msg.UnaffectedShortMessage = result.UnaffectedShortMessage;
                        msg.UpdatedBy = result.UpdatedBy;
                        msg.UpdatedDate = result.UpdatedDate;

                        msg.AffectedDisplayType = (DisplayType)result.AffectedDisplayType;
                        msg.UnaffectedDisplayType = (DisplayType)result.UnaffectedDisplayType;
                        msg.LastUpdatedDisruptionId = result.LastUpdatedDisruptionId;
                        msg.HideDownStopMessages = result.HideDownStopMessages;
                        msg.HideUpStopMessages = result.HideUpStopMessages;

                        msg.CurrentDisruptions = new List<Disruption>();
                        var disruptions = ctx.ttdms_GetDisruptionsByRouteCombination(msg.Id);
                        foreach (ttdms_GetDisruptionsByRouteCombinationResult dis in disruptions)
                        {
                            Disruption dr = new Disruption { Id = dis.DisruptionId, Name = dis.Name };
                            msg.CurrentDisruptions.Add(dr);

                        }





                        list.Add(msg);
                    }
                }
                list.Sort(new MessageSorter());
                return list;
            }             
            catch(Exception ex)
            {
                Util.LogError(0, ex);
                throw ex;
            }
        }


        public static IList<RouteCombinationMessage> GetAllDisruptedRouteCombinationMessages()
        {
            try
            {
                List<RouteCombinationMessage> list = new List<RouteCombinationMessage>();

                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    var results = ctx.ttdms_GetDisruptionRouteCombinationMessages();

                    foreach (ttdms_GetDisruptionRouteCombinationMessagesResult result in results)
                    {
                        RouteCombinationMessage msg = new RouteCombinationMessage();
                        msg.AffectedAdditionalInfoOnWebSite = result.AffectedAdditionalInfoOnWebsite;
                        msg.UnAffectedAdditionalInfoOnWebSite = (bool)result.UnaffectedAdditionalInfoOnWebSite;
                        msg.AffectedLongMessage = result.AffectedLongMessage;

                        msg.AffectedShortMessage = result.AffectedShortMessage;
                        msg.Id = result.Id;
                        msg.RouteNoCombination = result.RouteNoCombination;
                        msg.UnaffectedLongMessage = result.UnaffectedLongMessage;

                        msg.UnaffectedShortMessage = result.UnaffectedShortMessage;
                        msg.UpdatedBy = result.UpdatedBy;
                        msg.UpdatedDate = result.UpdatedDate;
                        msg.UpStopsCount = (int)result.UpStopsCount;
                        msg.DownStopsCount = (int)result.DownStopsCount;
                        msg.AffectedDisplayType = (DisplayType)result.AffectedDisplayType;
                        msg.UnaffectedDisplayType = (DisplayType)result.UnaffectedDisplayType;

                        msg.CurrentDisruptions = new List<Disruption>();
                        var disruptions = ctx.ttdms_GetDisruptionsByRouteCombination(msg.Id);
                        foreach (ttdms_GetDisruptionsByRouteCombinationResult dis in disruptions)
                        {
                            Disruption dr = new Disruption { Id = dis.DisruptionId, Name = dis.Name };
                            msg.CurrentDisruptions.Add(dr);

                        }


                        list.Add(msg);
                    }
                }
                list.Sort(new MessageSorter());
                return list;
            }
            catch (Exception ex)
            {
                Util.LogError(0, ex);
                throw ex;
            }
        }

        public static IList<RouteCombinationMessage> GetAllDisruptedRouteCombinationMessagesByDisruptionId(int DisruptionId)
        {
            try
            {
                List<RouteCombinationMessage> list = new List<RouteCombinationMessage>();

                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    var results = ctx.ttdms_GetDisruptionRouteCombinationMessagesByDisruptionId(DisruptionId);
                    //var results = ctx.GetTTMDisruptionRouteCombinationMessages();

                    foreach (ttdms_GetDisruptionRouteCombinationMessagesByDisruptionIdResult result in results)
                    {
                        RouteCombinationMessage msg = new RouteCombinationMessage();
                        msg.AffectedAdditionalInfoOnWebSite = result.AffectedAdditionalInfoOnWebsite;
                        msg.UnAffectedAdditionalInfoOnWebSite = (bool)result.UnaffectedAdditionalInfoOnWebSite;
                        msg.AffectedLongMessage = result.AffectedLongMessage;

                        msg.AffectedShortMessage = result.AffectedShortMessage;
                        msg.Id = result.Id;
                        msg.RouteNoCombination = result.RouteNoCombination;
                        msg.UnaffectedLongMessage = result.UnaffectedLongMessage;

                        msg.UnaffectedShortMessage = result.UnaffectedShortMessage;
                        msg.UpdatedBy = result.UpdatedBy;
                        msg.UpdatedDate = result.UpdatedDate;
                        msg.UpStopsCount = (int)result.UpStopsCount;
                        msg.DownStopsCount = (int)result.DownStopsCount;
                        msg.AffectedDisplayType = (DisplayType)result.AffectedDisplayType;
                        msg.UnaffectedDisplayType = (DisplayType)result.UnaffectedDisplayType;

                        msg.LastUpdatedDisruptionId = result.LastUpdatedDisruptionId;
                        msg.HideDownStopMessages = result.HideDownStopMessages;
                        msg.HideUpStopMessages = result.HideUpStopMessages;
                        msg.IsUnaffectedByCurrentDisruption = false;

                        msg.CurrentDisruptions = new List<Disruption>();
                        var disruptions = ctx.ttdms_GetDisruptionsByRouteCombination(msg.Id);
                        foreach (ttdms_GetDisruptionsByRouteCombinationResult dis in disruptions)
                        {
                            Disruption dr = new Disruption { Id = dis.DisruptionId, Name = dis.Name };
                            msg.CurrentDisruptions.Add(dr);

                        }


                        list.Add(msg);
                    }
                }
                list.Sort(new MessageSorter());
                return list;
            }
            catch (Exception ex)
            {
                Util.LogError(DisruptionId, ex);
                throw ex;
            }
        }



        public static IList<RouteSummaryMessage> GetDisruptionMessagesForStop(short stopNo, bool tramTrackerAvailable)
        {
            try
            {
                IList<RouteSummaryMessage> list = new List<RouteSummaryMessage>();

                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    var results = ctx.ttdms_GetRouteMessagesByStopNo(stopNo);

                    foreach (ttdms_GetRouteMessagesByStopNoResult result in results)
                    {
                        RouteSummaryMessage msg = new RouteSummaryMessage();
                        msg.Id = result.ID;
                        msg.RouteNoDisplayValue = result.RouteNo.ToString();
                        msg.ShortMessage = (tramTrackerAvailable) ? result.UnaffectedShortMessage : result.AffectedShortMessage;
                        msg.LongMessage = (tramTrackerAvailable) ? result.UnaffectedLongMessage : result.AffectedLongMessage;
                        msg.AffectedAdditionalInfoOnWebSite = result.AffectedAdditionalInfoOnWebsite;
                        msg.UnaffectedAdditionalInfoOnWebSite = (bool)result.UnaffectedAdditionalInfoOnWebsite;
                        msg.AffectedDisplayType = DisplayType.NA;
                        msg.UnaffectedDisplayType = DisplayType.NA;
                        msg.DisplayTypeString = "N/A";
                        msg.HideDownStopMessages = (bool)result.HideDownStopMessages;
                        msg.HideUpStopMessages = (bool)result.HideUpStopMessages;
                        list.Add(msg);
                    }


                    //get route combination message
                    var cbresults = ctx.ttdms_GetRouteCombinationMessageByStopNo(stopNo);
                    foreach (ttdms_GetRouteCombinationMessageByStopNoResult result in cbresults)
                    {
                        RouteSummaryMessage msg = new RouteSummaryMessage();
                        msg.Id = result.ID;
                        //msg.RouteNoDisplayValue = result.RouteNoCombination;
                        msg.RouteNoDisplayValue = "All Routes";
                        msg.ShortMessage = (tramTrackerAvailable) ? result.UnaffectedShortMessage : result.AffectedShortMessage;
                        msg.LongMessage = (tramTrackerAvailable) ? result.UnaffectedLongMessage : result.AffectedLongMessage;
                        msg.AffectedAdditionalInfoOnWebSite = result.AffectedAdditionalInfoOnWebsite;
                        msg.UnaffectedAdditionalInfoOnWebSite = (bool)result.UnaffectedAdditionalInfoOnWebsite;
                        msg.AffectedDisplayType = (DisplayType)result.AffectedDisplayType;
                        msg.UnaffectedDisplayType = (DisplayType)result.UnaffectedDisplayType;
                        msg.HideDownStopMessages = (bool)result.HideDownStopMessages;
                        msg.HideUpStopMessages = (bool)result.HideUpStopMessages;

                        msg.DisplayTypeString = (tramTrackerAvailable) ? (msg.UnaffectedDisplayType == DisplayType.FreeText) ? "Free Text" : "Tabular Column" : (msg.AffectedDisplayType == DisplayType.FreeText) ? "Free Text" : "Tabular Column";

                        list.Add(msg);
                    }

                }

                return list;
            }
            catch (Exception ex)
            {
                Util.LogError(0, ex);
                throw ex;
            }
        }

        public static List<object> GetDisruptionMessagesValidationResult(int disruptionId)
        {
            try
            {
                List<object> result = new List<object>();
                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    var cbresults = ctx.ttdms_GetValidateDisruptionMessagesByDisruptionId(disruptionId);
                    foreach (ttdms_GetValidateDisruptionMessagesByDisruptionIdResult validation_result in cbresults)
                    {
                        Dictionary<string, string> dict = new Dictionary<string, string>();
                        dict.Add("Id", validation_result.Id.ToString());
                        dict.Add("Type", validation_result.Type.ToString());
                        result.Add(dict);
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                Util.LogError(disruptionId, ex);
                throw ex;
            }
        }

        public static void UpdateRouteDisruptionMessages(int id, string affShortMessage, string affLongMessage, bool affAdditionalInfo, string unaffShortMessage, string unaffLongMessage, bool unaffAdditionalInfo, string updateBy, int? lastUpdatedDisruptionId, bool? hideDownStopMessages, bool? hideUpStopMessages)
        {
            try
            {
                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    ctx.ttdms_UpdateRouteDisruptionMessages(id, affShortMessage, affLongMessage, affAdditionalInfo, unaffShortMessage, unaffLongMessage, unaffAdditionalInfo, updateBy, lastUpdatedDisruptionId, hideDownStopMessages, hideUpStopMessages);
                }
            }
            catch (Exception ex)
            {
                Util.LogError(lastUpdatedDisruptionId.GetValueOrDefault(0), ex);
                throw ex;
            }

        }


        //public static void UpdateAffectedCombinationRouteDisruptionMessages(int id, string affShortMessage, string affMediumMessage, string affLongMessage, bool affAdditionalInfo,int affDisplayType ,string updateBy)
        //{

        //    using (DisruptionsDataContext ctx = new DisruptionsDataContext())
        //    {
        //        ctx.ttdms_UpdateAffectedCombinationRouteDisruptionMessages(id, affShortMessage, affMediumMessage, affLongMessage, affAdditionalInfo, affDisplayType, updateBy);
        //    }

        //}


        public static void UpdateUnaffectedCombinationRouteDisruptionMessages(int id, string affShortMessage, string affLongMessage, bool affAdditionalInfo, int affDisplayType, string updateBy, int lastUpdatedDisruptionId, bool? hideDownStopMessages, bool? hideUpStopMessages)
        {
            try
            {
                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    ctx.ttdms_UpdateUnaffectedCombinationRouteDisruptionMessages(id, affShortMessage, affLongMessage, affAdditionalInfo, affDisplayType, updateBy, lastUpdatedDisruptionId, hideDownStopMessages, hideUpStopMessages);
                }
            }
            catch (Exception ex)
            {
                Util.LogError(lastUpdatedDisruptionId, ex);
                throw ex;
            }

        }

        public static void UpdateRouteCombinationDisruptionMessage(int id, string affShortMessage, string affLongMessage, bool affAdditionalInfo, int affDisplayType, string unaffShortMessage, string unaffLongMessage, bool unaffAdditionalInfo, int unaffDisplayType, string updateBy, int lastUpdatedDisruptionId, bool? hideDownStopMessages, bool? hideUpStopMessages)
        {
            try
            {
                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    ctx.ttdms_UpdateRouteCombinationDisruptionMessage(id, affShortMessage, affLongMessage, affAdditionalInfo, affDisplayType, unaffShortMessage, unaffLongMessage, unaffAdditionalInfo, unaffDisplayType, updateBy, lastUpdatedDisruptionId, hideDownStopMessages, hideUpStopMessages);
                }
            }
            catch (Exception ex)
            {
                Util.LogError(lastUpdatedDisruptionId, ex);
                throw ex;
            }

        }
    }
}
