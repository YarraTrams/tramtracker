﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TTMarketing.DAL;
using TTMarketing.BLL.Model.Disruptions;
using YarraTrams.Library.Mailer;
using System.Configuration;
using TTMarketing.BLL.Helpers;


namespace TTMarketing.BLL.Controllers.Disruptions
{
    public class ActivityLogController
    {
        public static IList<ActivityLog> GetActivityLogs(int? disruptionId, string type)
        {
            try
            {
                IList<ActivityLog> list = new List<ActivityLog>();

                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    var results = ctx.ttdms_GetActivityLogs(disruptionId, type);

                    foreach (ttdms_GetActivityLogsResult result in results)
                    {
                        ActivityLog log = new ActivityLog();
                        log.Id = result.Id;
                        log.CreatedBy = result.CreatedBy;
                        log.CreatedDate = result.CreatedDate;
                        log.Message = result.Message;
                        log.Type = result.Type;
                        list.Add(log);
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                Util.LogError(disruptionId.GetValueOrDefault(0), ex);
                throw ex;
            }
        }

        public static ActivityLog Create(int? disruptionId, string message, string type, string createdBy)
        {
            try
            {
                ActivityLog al = new ActivityLog();

                using (DisruptionsDataContext ctx = new DisruptionsDataContext())
                {
                    ttdms_AddActivityLogResult result = ctx.ttdms_AddActivityLog(disruptionId, message, type, createdBy).FirstOrDefault();
                    al.Id = (int)result.Id;
                }

                return al;
            }
            catch (Exception ex)
            {
                Util.LogError(disruptionId.GetValueOrDefault(0), ex);
                throw ex;
            }
        }
    }
}
